﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using fecherFoundation.Extensions;
using fecherFoundation.DataBaseLayer.ADO;
using fecherFoundation.DataBaseLayer;

namespace fecherFoundation
{
    /// <summary>
    /// truedbgrid.tdbgrid
    /// truedbgrid60.tdbgrid
    /// </summary>
    public partial class FCTDBGrid : DataGridView, ISupportInitialize
    {
        #region Public Members
        //double click on row (any cell including row header, excluding column header)
        public bool RowAreaDblClick = false;
        //double click on row header
        public bool RowHeaderDblClick = false;
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public FCSplitCollection Splits = null;
        public new FCDataGridViewColumnCollection Columns;
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public FCXArrayDB Array;
        #endregion

        #region Internal Members
        internal bool inReBind = false;
        internal bool refreshingData = false;
        internal DataTable DataTable
        {
            get
            {
                return this.dataTable;
            }
        }

        [DefaultValue(null)]
        internal Panel InvertedView
        {
            get;
            set;
        }

        [DefaultValue(false)]
        internal bool IsDropDownClosing
        {
            get;
            set;
        }

        [DefaultValue(false)]
        internal bool IsCellBeginEdit
        {
            get;
            set;
        }

        [DefaultValue(false)]
        internal bool SkipColResize
        {
            get;
            set;
        }

        [DefaultValue(false)]
        internal bool CellValidatingInProgress
        {
            get;
            set;
        }

        internal FCGraphics FCGraphics
        {
            get
            {
                return fcGraphics;
            }
        }
		#endregion

		#region Private Members
		private bool preventReleasingExtension = false;
		private bool initialized = false;
        private bool requiresCellClick = false;
        private DataGridViewCellEventArgs cellClickEventArgs = null;
        private Appearance appearance = Appearance.dbg3D;
        private System.Drawing.Color backColor = System.Drawing.SystemColors.Window;
        private ScrollBars scrollBars = ScrollBars.Automatic;
        private bool allowArrows = true;
        private bool wrapCellPointer = false;
        private DataViewType dataView = DataViewType.dbgNormalView;
        private bool disableEvents = false;
        //CNA
        private bool dataSourceIsSetting = false;
        private bool disableCellFormatting = false;
        private bool isDropDownOpened = false;
        private int cRow;
        private bool fetchRowStyle = false;
        private bool dataChanged;
        private int cCol = 0;
        private bool extendRightColumn = false;
        private MultipleLines multipleLines = MultipleLines.Disabled;
        private bool allowFocus = true;
        private bool allowUpdate = true;
        private string layoutFileName = "";
        private string layoutName = "";
        private bool allowColSelect = true;
        //CHE: in TrueDBGrid default is dbgFloatingEditor 
        private MarqueeStyle marqueeStyle = MarqueeStyle.FloatingEditor;
        private RowDividerStyle rowDividerStyle = RowDividerStyle.DarkGrayLine;
        //BAN
        private FCStyle captionStyle = new FCStyle(Alignment.Center, SystemColors.Control, SystemColors.ControlText, VerticalAlignment.VerticalCenter);
		private FCStyle editorStyle = new FCStyle(Alignment.Left, SystemColors.Window, SystemColors.WindowText, VerticalAlignment.Top);
		private FCStyle evenRowStyle = new FCStyle(Alignment.General, Color.Yellow, SystemColors.WindowText, VerticalAlignment.Top);
		//private FCStyle filterBarStyle = new FCStyle();
		private FCStyle headingStyle = new FCStyle(Alignment.Left, SystemColors.Control, SystemColors.ControlText, VerticalAlignment.VerticalCenter);
		private FCStyle inactiveStyle = new FCStyle(Alignment.Left, SystemColors.Control, SystemColors.ControlText, VerticalAlignment.VerticalCenter);
		private FCStyle oddRowStyle = new FCStyle(Alignment.General, SystemColors.WindowText, SystemColors.WindowText, VerticalAlignment.Top);
		private FCStyle style = new FCStyle(Alignment.Left, SystemColors.Window, SystemColors.WindowText, VerticalAlignment.Top);
		private FCStyle selectedStyle = new FCStyle(Alignment.Left, SystemColors.Highlight, SystemColors.HighlightText, VerticalAlignment.Top);
		private FCStyle highlightRowStyle = new FCStyle(Alignment.General, SystemColors.WindowText, SystemColors.Window, VerticalAlignment.Top);
		//BAN
		private System.Drawing.Color headForeColor = System.Drawing.SystemColors.ControlText;
		private System.Drawing.Color editForeColor = System.Drawing.SystemColors.ControlText;
        private System.Drawing.Color rowDividerColor = System.Drawing.Color.Black;
        private int recordSelectorWidth = 36; //540 twips
        private FCFont font = new FCFont();
		private FCFont headFont = new FCFont();
		private FCFont footerFont = new FCFont();
		private bool eof = false, bof = false;
        private bool editDropDown = true;
        private ExposeCellMode exposeCellMode = ExposeCellMode.ScrollOnSelect;
        private bool insertMode = true;
        private bool transparentRowPictures = false;
        private DataMode dataMode = DataMode.Bound;
        private bool marqueeUnique = true;
        private int viewColumnWidth = 0;
        private int viewColumnCaptionWidth = 0;
        private System.Drawing.Color inactiveBackColor = System.Drawing.SystemColors.ButtonFace;
        private int headLines = 1;
        private SelBookmarks selBookmarks;
        private bool isCancelEdit = false;
        private bool isCheckBoxValueChanged = false;
        private int cellValidatingRowIndex = -1;
        private int cellValidatingColIndex = -1;
        private bool needCellValidating = false;
        private bool cellValidatingCellInEditMode = false;
        private bool cellValidatingCellIsDirty = false;
        private bool shouldRaiseCellValidated = true;
        private DataTable dataTable;
		private object dataSource;
        /// <summary>
        /// lastRow is 1-indexed and is nullable
        /// </summary>
        private int? lastRow;
        /// <summary>
        /// lastColumn is 0-indexed
        /// </summary>
        private int lastColumn = 0;
        private bool isInvertedCtrlFocused = false;
        private DataGridViewColumn[] originalColumns = null;
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        private List<int> messageQueue;
        private bool invertedGridVisibility = true;
        private Coordinates objClickedCoordinates;
        private fecherFoundation.PointAt pointAt = fecherFoundation.PointAt.dbgNotInGrid;
        private bool disablePreRenderRegenerate;
        private bool beforeRowColChange = true;
        private bool isFromBeforeRowColChange = false;
        private bool beforeRowColChangeCancel = false;
        //CHE: when clicking another cell should consider validating cancel and remaining on previous cell if the case
        private bool validatingCanceled = false;
        private bool inMarqueeStyle = false;
        private bool isResizing = false;
        private bool showExtendedColumnHeader = false;
        //CHE: for disabled grid do not show current row selection, change/reset SelectionBackColor
        private Color? previousSelectionBackColor = null;
        // This value stores the original cell value before the user starts editing 
        // It is set in OnCellBeginEdit and is used to be passed to the BeforeColUpdate(CellValidating) event handler
        private object currentCellOldValue;
        //SBE: This variable stores the style applied in FetchRowStyle event, for first cell in the row. Used to set the style for the rest of the cells in row
        private DataGridViewCellStyle appliedRowStyle = null;
        //SBE: bool flag used to check if FetchRowStyle event should be raised
        private bool isRowStyleApplied = false;
        //CHE: avoid reentrant call, do not raise again CellFormatting if is already in progress 
        //(e.g. setting row ReadOnly in FetchRowStyle will enter in GetFormattedValue for DataGridViewCustomComboBoxCell and that should not call again grid.RaiseCellFormatting)
        private bool cellFormattingInProgress = false;
        //APE: do not set currentCellOldValue when focusing control from GenerateInvertedGrid()
        private bool generateInvertedGridInProgress = false;
        private FCGraphics fcGraphics;
        private int whatsThisHelpID;
		#endregion

		#region Properties
		[DefaultValue(typeof(System.Drawing.SystemColors), "Window")]
		public Color EditBackColor { get; set; } = System.Drawing.SystemColors.Window;

		/// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>		
		[Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(false)]
        public bool SuppressRedraw
        {
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new DataGridViewCell CurrentCell
        {
            get
            {
                return base.CurrentCell;
            }
            set
            {
                //CNA: Notify grid that the current cell has no Uncommited changes to avoid InvalidOperationException when setting CurrentCell
                if (IsCurrentCellDirty)
                {
                    NotifyCurrentCellDirty(false);
                }

                if (value == null || IsValidItem(value.RowIndex, value.ColumnIndex))
                {
                    base.CurrentCell = value;
                }

            }
        }

        ///
        /// This value stores the original cell value before the user starts editing 
        /// It is set in OnCellBeginEdit and is used to be passed to the BeforeColUpdate(CellValidating) event handler
        /// 
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public object CurrentCellOldValue
        {
            get
            {
                return currentCellOldValue;
            }
            set
            {
                currentCellOldValue = value;
            }
        }

        /// <summary>
        /// This property returns or sets the height of all grid rows in terms of the coordinate system of the grid's container. 
        /// Syntax
        /// object.RowHeight = single
        /// Remarks
        /// Read/Write at run time and design time.
        /// The RowHeight property accepts a floating point number from 0 to 10,000. The default value depends upon the character height of the current font. 
        /// A setting of 0 causes the grid to readjust its display so that each row occupies a single line of text in the current font. 
        /// Therefore, the following statements will set the row height so that exactly two lines of text are shown in each row: 
        /// TDBGrid1.RowHeight = 0
        /// TDBGrid1.RowHeight = TDBGrid1.RowHeight * 2  
        /// </summary>
        [DefaultValue(21)]
        public float RowHeight
        {
            get
            {
                return this.RowTemplate.Height;
            }
            set
            {
                if (value == 0)
                {
                    value = this.Font.Size + 11;
                }
                this.RowTemplate.Height = Convert.ToInt32(value);
            }
        }

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new DataGridViewRow RowTemplate
		{
			get
			{
				return base.RowTemplate;
			}
			set
			{
				base.RowTemplate = value;
			}
		}

		[DefaultValue(true)]
        public new bool Visible
        {
            get
            {
                if (IsInvertedOrFormViewGenerated())
                {
                    return invertedGridVisibility;
                }
                return base.Visible;
            }
            set
            {
                if (IsInvertedOrFormViewGenerated())
                {
                    invertedGridVisibility = value;
                    this.InvertedView.Visible = invertedGridVisibility;
                }
                else
                {
                    base.Visible = value;
                }
            }
        }

        /// <summary>
        /// in VB6, LastRow is used as argument in RowColChange event handler to identify the former current row
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int? LastRow
        {
            get
            {
                return lastRow;
            }
        }

        /// <summary>
        /// in VB6, LastColumn is used as argument in RowColChange event handler to identify the former current column
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(0)]
        public int LastColumn
        {
            get
            {
                return lastColumn;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public SelBookmarks SelBookmarks
        {
            get
            {
                return selBookmarks;
            }
            set
            {
                selBookmarks = value;
            }
        }

        [DefaultValue(false)]
        public new bool AllowUserToAddRows
        {
            get
            {
                return base.AllowUserToAddRows;
            }
            set
            {
                base.AllowUserToAddRows = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool IsCancelEdit
        {
            get
            {
                return isCancelEdit;
            }
            set
            {
                isCancelEdit = value;
            }
        }

        /// <summary>
        /// flag introduced for performance improvement, if merging is used or not
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(false)]
        public bool AllowMerging
        {
            get; set;
        }

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new bool IsCaptionVisible
        {
            get
            {
                return base.ColumnHeadersVisible;
            }
            set
            {
                base.ColumnHeadersVisible = value;
                RedrawInverted();
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool EOF
        {
            get
            {
                if (this.RowCount == 0)
                {
                    eof = bof = true;
                }
                return eof;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool BOF
        {
            get
            {
                if (this.RowCount == 0)
                {
                    eof = bof = true;
                }
                return bof;
            }
        }

        /// <summary>
        /// in VB6 Text property means CurrentCell value
        /// </summary>
        public new string Text
        {
            get
            {
                if (this.CurrentCell != null)
                {
                    return this.CurrentCell.Value.ToString();
                }
                return "";
            }
            set
            {
                if (this.CurrentCell != null)
                {
                    this.CurrentCell.Value = value;
                }
            }
        }

        /// <summary>
        /// in VB6 Caption property means grid title
        /// </summary>
        [DefaultValue("")]
        public string Caption
        {
            get
            {
                return base.Text;
            }
            set
            {
                if (this.DesignMode)
                {
                    base.Text = value;
                    return;
                }

                //CNA: Set IsCaptionVisible according to value. False if it's empty, true otherwise
                IsCaptionVisible = !(String.IsNullOrEmpty(value));
                base.Text = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [DefaultValue(true)]
        public bool EditDropDown
        {
            get
            {
                return editDropDown;
            }
            set
            {
                editDropDown = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(ExposeCellMode.ScrollOnSelect)]
        public ExposeCellMode ExposeCellMode
        {
            get
            {
                return exposeCellMode;
            }
            set
            {
                exposeCellMode = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(true)]
        public bool InsertMode
        {
            get
            {
                return insertMode;
            }
            set
            {
                insertMode = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(false)]
        public bool TransparentRowPictures
        {
            get
            {
                return transparentRowPictures;
            }
            set
            {
                transparentRowPictures = value;
            }
        }

        [DefaultValue(DataMode.Bound)]
        public DataMode DataMode
        {
            get
            {
                return dataMode;
            }
            set
            {
                dataMode = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(false)]
        public bool TabAcrossSplits
        {
            get;
            set;
        }

        /// <summary>
        /// This property returns the error message string from the underlying data source. 
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string ErrorText
        {
            get
            {
                if (this.CurrentRow != null)
                {
                    return this.CurrentRow.ErrorText;
                }
                return "";
            }
            set
            {
                if (this.CurrentRow != null)
                {
                    this.CurrentRow.ErrorText = value;
                }
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public PrintInfo PrintInfo
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ApproxCount
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(true)]
        public bool MarqueeUnique
        {
            get
            {
                return marqueeUnique;
            }
            set
            {
                marqueeUnique = value;
            }
        }

        /// <summary>
        /// Sets or returns modification status of the current cell.
        /// Syntax
        ///     TDBGrid.CurrentCellModified = boolean
        ///Remarks
        ///     Read/Write at run time. Not available at design time.
        ///     This property returns True if editing is in progress and the current cell (indicated by the Bookmark and Col properties) has been modified by the user. 
        ///     It returns False if the cell has not been modified or if editing is not in progress. 
        ///     You can use this property to cancel any changes the user has made to the current text. For example, to program a function key to discard the user's 
        ///     changes (like the Esc key), trap the key code in the grid's KeyDown event and set CurrentCellModified to False. This will revert the current cell to 
        ///     its original contents.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool CurrentCellModified
        {
            get
            {
                return IsCurrentCellDirty;
            }
            set
            {
                NotifyCurrentCellDirty(value);
            }
        }

        /// <summary>
        /// This property returns or sets the bookmark identifying the selected item in a TDBDropDown control.
        /// Use the value returned by the SelectedItem property to determine the current row in a TDBDropDown control. 
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int? SelectedItem
        {
            get
            {
                if (this.CurrentRow == null)
                {
                    return null;
                }

                return this.CurrentRow.Index + 1;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public double VScrollWidth
        {
            get;
            set;
        }

        //AM: implementation for VisibleRows
        /// <summary>
        /// This property returns the number of visible rows in the control. 
        /// The value returned includes both fully and partially displayed rows. 
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int VisibleRows
        {
            get
            {
                if (this.IsInvertedOrFormView())
                {
                    return this.Rows.Count;
                }

                int availableHeight = this.Height - this.ColumnHeadersHeight;

                int columnsWidth = 0;
                foreach (DataGridViewColumn col in this.Columns)
                {
                    if (col.Visible)
                    {
                        columnsWidth += col.Width;
                    }
                }
                if (columnsWidth > this.Width)
                {
                    availableHeight -= 18; //substract the height of the horizontal scrollbar
                }

                int visibleRows = (int)Math.Ceiling((double)availableHeight / this.RowHeight);
                if (visibleRows > this.Rows.Count)
                {
                    return this.Rows.Count;
                }
                return visibleRows;
            }
        }

        [DefaultValue(0)]
        public int ViewColumnWidth
        {
            get
            {
                return viewColumnWidth;
            }
            set
            {
                viewColumnWidth = value;
                this.RedrawInverted(true);
            }
        }

        //APE: when setting the column caption width, if the grid is inverted, regenerate the inverted view in order to reflect the new value
        //in the header label width
        [DefaultValue(0)]
        public int ViewColumnCaptionWidth
        {
            get
            {
                return viewColumnCaptionWidth;
            }
            set
            {
                viewColumnCaptionWidth = value;
                this.RedrawInverted(true);
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(1)]
        public int ScrollGroup
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public System.Drawing.Image PictureCurrentRow
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(MoveDirection.dbgMoveRight)]
        public MoveDirection DirectionAfterEnter
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelLength
        {
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int FirstRow
        {
            get
            {
                return base.FirstDisplayedScrollingRowIndex;
            }
            set
            {
                if (value >= 0 && value < Rows.Count)
                {
                    base.FirstDisplayedScrollingRowIndex = value;
                }
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string SelText
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelStart
        {
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftCol
        {
            get
            {
                return base.FirstDisplayedScrollingColumnIndex;
            }
            set
            {
                base.FirstDisplayedScrollingColumnIndex = value;
            }
        }

		public new MultiSelect MultiSelect
		{
			get
			{
				return base.MultiSelect ? MultiSelect.Extended : MultiSelect.None;
			}
			set
			{
				base.MultiSelect = (value != MultiSelect.None);
			}
		}

		public bool Is60 { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int RightCol
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(typeof(System.Drawing.SystemColors), "ButtonFace")]
        public System.Drawing.Color InactiveBackColor
        {
            get
            {
                return inactiveBackColor;
            }
            set
            {
                inactiveBackColor = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(fecherFoundation.SizeMode.Scalable)]
        public SizeMode SizeMode
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(false)]
        public bool EmptyRows
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FilterActive
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(false)]
        public bool FilterBar
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[DefaultValue(0)]
        public int DefColWidth
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(DividerStyle.NoDividers)]
        public DividerStyle DividerStyle
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(1)]
        public int HeadLines
        {
            get
            {
                return headLines;
            }
            set
            {
                headLines = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(false)]
        public bool AlternatingRowStyle
        {
            get;
            set;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(DividerStyle.DarkGrayLine)]
        public RowDividerStyle RowDividerStyle
        {
            get
            {
                return rowDividerStyle;
            }
            set
            {
                switch (value)
                {
                    case RowDividerStyle.NoDividers:
                        {
                            this.CellBorderStyle = DataGridViewCellBorderStyle.None;
                            break;
                        }
                }
                rowDividerStyle = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public List<FCStyle> Styles
        {
            get;
            set;
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public FCStyle CaptionStyle
        {
            get
            {
                return captionStyle;
            }
            set
            {
                captionStyle = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public FCStyle HeadingStyle
        {
            get
            {
                return headingStyle;
            }
            set
            {
                headingStyle = value;
            }
        }

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public FCStyle EditorStyle
		{
			get
			{
				return editorStyle;
			}
			set
			{
				editorStyle = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public FCStyle InactiveStyle
		{
			get
			{
				return inactiveStyle;
			}
			set
			{
				inactiveStyle = value;
			}
		}

		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public FCStyle Style
		{
			get
			{
				return style;
			}
			set
			{
				style = value;
			}
		}

		//[Browsable(false)]
  //      [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
  //      public FCStyle FilterBarStyle
  //      {
  //          get
  //          {
  //              return filterBarStyle;
  //          }
  //          set
  //          {
  //              filterBarStyle = value;
  //          }
  //      }

		public bool RecordSelectors { get; set; }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(typeof(System.Drawing.SystemColors), "ControlText")]
        public System.Drawing.Color HeadForeColor
        {
            get
            {
                return headForeColor;
            }
            set
            {
                headForeColor = value;
            }
        }

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		[DefaultValue(typeof(System.Drawing.SystemColors), "ControlText")]
		public System.Drawing.Color EditForeColor
		{
			get
			{
				return editForeColor;
			}
			set
			{
				editForeColor = value;
			}
		}

		[Obsolete("Not implemented. Added for migration compatibility")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public FCStyle EvenRowStyle
        {
            get
            {
                return evenRowStyle;
            }
            set
            {
                evenRowStyle = value;
            }
        }

		[Obsolete("Not implemented. Added for migration compatibility")]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
		public FCStyle FooterStyle { get; set; } = new FCStyle(Alignment.General, SystemColors.Control, SystemColors.ControlText, VerticalAlignment.VerticalCenter);

		[Obsolete("Not implemented. Added for migration compatibility")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public FCStyle OddRowStyle
        {
            get
            {
                return oddRowStyle;
            }
            set
            {
                oddRowStyle = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public FCStyle HighlightRowStyle
        {
            get
            {
                return highlightRowStyle;
            }
            set
            {
				highlightRowStyle = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public FCStyle SelectedStyle
        {
            get
            {
                return selectedStyle;
            }
            set
            {
				selectedStyle = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(typeof(System.Drawing.Color), "Black")]
        public System.Drawing.Color RowDividerColor
        {
            get
            {
                return rowDividerColor;
            }
            set
            {
                rowDividerColor = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(36)]
        public int RecordSelectorWidth
        {
            get
            {
                return recordSelectorWidth;
            }
            set
            {
                recordSelectorWidth = value;
            }
        }

		public double HScrollHeight { get; set; }

        public new FCFont Font
        {
            get
            {
                return font;
            }
            set
            {
                font = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public FCFont HeadFont
        {
            get
            {
                return headFont;
            }
            set
            {
                headFont = value;
            }
        }

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public FCFont FooterFont
		{
			get
			{
				return footerFont;
			}
			set
			{
				footerFont = value;
			}
		}

		/// <summary>
		/// This property specifies the zero-based index of the current row relative to the first displayed row.
		/// The Row property accepts values ranging from 0 to VisibleRows - 1. An error occurs if you attempt to set it to an invalid row index. 
		/// If the current row is not visible, then this property returns -1. 
		/// </summary>
		[Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Row
        {
            get
            {
                if (isDropDownOpened)
                {
                    //IPI: should be able to retrieve the current row while DropDownOpen, although the CurrentCell <> Row,Col values
                    return cRow;
                }

                if (this.IsInvertedOrFormView())
                {
                    if (CurrentRow == null)
                    {
                        return -1;
                    }
                    else
                    {
                        return this.CurrentRow.Index;
                    }
                }

                if (CurrentRow == null || !CurrentRow.Visible)
                {
                    return -1;
                }
                return this.GetRowVisibleIndex(this.CurrentRow.Index);
            }
            set
            {
                if (isDropDownOpened)
                {
                    //IPI: should be able to set the current row while DropDownOpen, although the CurrentCell <> Row,Col values
                    cRow = value;
                    return;
                }
                if (this.IsInvertedOrFormView())
                {
                    if (this.CurrentCell != null)
                    {
                        this.CurrentCell = this[this.CurrentCell.ColumnIndex, value];
                    }
                    else
                    {
                        //at least one column
                        if (this.ColumnCount > 0 && this.RowCount > 0)
                        {
                            this.CurrentCell = this[FirstVisibleColumn(), value];
                        }
                    }
                    return;
                }

                //PJ: changed from VisibleRows to Rows.Count because of an Exception when using a Filter for the Grid and the Result changes (e.g. RowCount=54 and VisibleRows=19). Otherwise an Exception will be thrown
                //if (value >= 0 && value < VisibleRows)
                if (value >= 0 && value < this.Rows.Count)
                {
                    //CNA: check if CurrentCell exists, if not use the first column to select the new CurrentCell
                    this.Focus();
                    // IPI: 'value' is the index in the visible rows of the table; must transform the visible row index to row index
                    int rowIndex = this.GetRowIndex(value);
                    if (this.CurrentCell != null)
                    {
                        this.CurrentCell = this[this.CurrentCell.ColumnIndex, rowIndex];
                    }
                    else
                    {
                        //at least one column
                        if (this.ColumnCount > 0)
                        {
                            this.CurrentCell = this[FirstVisibleColumn(), rowIndex];
                        }
                    }
                }
                else
                {
                    //IPI: if an error occurs, must enable events to be able to enter cells in edit mode 
                    disableEvents = false;
                    throw new ArgumentOutOfRangeException("Row", "Invalid row number");
                }
            }
        }

        /// <summary>
        /// FetchRowStyle Property (TDBDropDown)
        ///Controls whether the FetchRowStyle event will be fired.
        ///Syntax
        ///object.FetchRowStyle = boolean
        ///Remarks
        ///Read/Write at run time and design time.
        ///If True, the FetchRowStyle event will be fired whenever the grid is about to display a row of data.
        ///If False (the default), the FetchRowStyle event is not fired.
        ///Set this value to True when you need to perform complex per-row formatting operations that can only be done using the FetchRowStyle event. For example, if you want to apply fonts and/or colors to all rows that satisfy certain criteria, then you need to set the FetchRowStyle property to True and write code for the FetchRowStyle event.
        ///Note
        ///To display every other row in a different color or font, you can simply set the AlternatingRowStyle property to True.
        /// </summary>
        [DefaultValue(false)]
        public bool FetchRowStyle
        {
            get
            {
                return fetchRowStyle;
            }
            set
            {
                fetchRowStyle = value;
            }

        }

        /// <summary>
        /// Bookmark Property
        ///This property returns or sets the bookmark identifying the current row in a TDBGrid or TDBDropDown control.
        ///Syntax
        ///object.Bookmark = variant
        ///Remarks
        ///Read/Write at run time. Not available at design time.
        ///Use the value returned by the Bookmark property to save a reference to the current row that remains valid even after another row becomes current.
        ///When you set the Bookmark property to a valid value in code, the row associated with that value becomes the current row, and the grid adjusts its display to bring the new current row into view if necessary.
        ///The Bookmark property is defined as a Variant to accommodate user-defined bookmarks in unbound mode.
        ///Note
        ///In unbound mode, setting the Bookmark property to itself will force the current row to be updated via the UnboundWriteData event.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int? Bookmark
        {
            set
            {
                //BAN: Disable firing the BeforeRowColChange event
                beforeRowColChange = false;
                if (this.Rows.Count > 0)
                {
                    //CHE: grid Bookmark setter - find corresponding DataGridViewRow based on DataTable bookmark
                    foreach (DataGridViewRow dgr in this.Rows)
                    {
                        DataRow dr = dgr.DataBoundItem as DataRow;
                        if (dr != null && this.dataTable != null && this.dataTable.Rows.IndexOf(dr) == value - 1)
                        {
                            dgr.Selected = true;
                        }
                    }
                }
                //BAN: Disable firing the BeforeRowColChange event
                beforeRowColChange = true;
            }
            get
            {
                //CHE: must return the position of current grid row in bounded recordset 
                //     - if grid is empty should return null 
                //     - is not related with selected rows (SelBookmarks)
                //     - is not related with visual row position in grid
                //
                //      Dim rs As New ADODB.Recordset
                //      rs.Fields.Append "col1", adVarChar, 10
                //      rs.Fields.Append "col2", adVarChar, 10
                //      rs.Open
                //      rs.AddNew
                //      rs.Fields("col1").Value = "xxx"
                //      rs.Update
                //      rs.AddNew
                //      rs.Fields("col1").Value = "aaa"
                //      rs.Update
                //      rs.AddNew
                //      rs.Fields("col1").Value = "a"
                //      rs.Update

                //      rs.MoveFirst
                //      TDBGrid.DataSource = rs

                //      rs.Sort = "col1"
                //      TDBGrid.SelBookmarks.Add 1
                //      MsgBox TDBGrid.Bookmark 'returns 3 although visually the first row in grid is current ("a") and the third ("xxx") is selected

                //      rs.Filter = "col1='11'"
                //      MsgBox IsNull(TDBGrid.Bookmark) 'returns true

                if (this.CurrentRow != null)
                {
                    DataRowView dataRow = this.CurrentRow.DataBoundItem as DataRowView;
                    if (dataRow != null && this.dataTable != null)
                    {
                        return this.dataTable.Rows.IndexOf(dataRow.Row) + 1;
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// EditActive Property
        ///Returns the status or enters/exits the cell editor.
        ///Syntax
        ///TDBGrid.EditActive = boolean
        ///Remarks
        ///Read/Write at run time. Not available at design time.
        ///If this property returns True, then the current cell is currently being edited by the user. If False, then no editing is in progress.
        ///If the grid is not already in edit mode, setting EditActive to True will initiate editing on the current cell. The caret will be positioned at the end of the cell and the ColEdit event will be triggered.
        ///If the grid is already in edit mode, setting EditActive to False will exit edit mode. If the cell has been modified, this will trigger the following events: BeforeColUpdate, AfterColUpdate, and AfterColEdit.
        ///Note
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool EditActive
        {
            get
            {
                return this.IsCurrentCellInEditMode;
            }
            set
            {
                if (this.CurrentCell != null)
                {
                    if (value == true)
                    {
                        //IPI: if the cell is already in edit mode, do not execute BeginEdit
                        // executing BeginEdit when already in edit mode will delete the previous typed characters
                        if (!this.IsCurrentCellInEditMode)
                        {
                            this.BeginEdit(false);
                        }
                    }
                    else
                    {
                        this.EndEdit();
                    }
                }
            }
        }

        /// <summary>
        /// DataChanged Property
        ///Sets or returns modification status of the current row or cell.
        ///Syntax
        ///object.DataChanged = boolean
        ///Remarks
        ///Read/Write at run time (TDBGrid). Read-only at run time (Column). Not available at design time.
        ///For a TDBGrid control, the DataChanged property indicates the modification status of the current row. If True, then one or more columns in the current row have been modified. If False, then no changes have been made.
        ///When the DataChanged property of a TDBGrid control is True, you can use the DataChanged property of individual Column objects to determine the exact nature of the changes.
        ///For a TDBGrid control, setting this property to True has no effect. Setting this property to False exits editing, discards all changes to the current row, and refreshes the current row from the data source. Setting this property within the BeforeColUpdate event is disallowed, however.
        ///For a Column object, this property is read-only and cannot be set.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool DataChanged
        {
            get
            {
                return dataChanged;
            }
            set
            {
                dataChanged = value;
                if (!dataChanged && this.CurrentRow != null)
                {
                    if (this.CurrentRow.DataBoundItem != null)
                    {
                        if (((DataRowView)this.CurrentRow.DataBoundItem).IsNew)
                        {
                            this.Rows.RemoveAt(this.CurrentRow.Index);
                        }
                    }
                    else
                    {
                        //BAN:  if the current row has not databound item, it means that the row is newly added and is still not committed
                        //the only way to remove the uncommitted row is to disable AllowUserToAddRows and then set it back to it`s original value
                        this.disableEvents = true;
                        bool allowAddRow = AllowUserToAddRows;
                        this.AllowUserToAddRows = false;
                        //IPI: if only the new row is contained in the grid, setting AllowUserToAddRows to false will make CurrentRow = null
                        if (this.CurrentRow != null)
                        {
                            this.Rows.RemoveAt(this.CurrentRow.Index);
                        }
                        this.AllowUserToAddRows = allowAddRow;
                        this.disableEvents = false;
                    }
                }
            }
        }

        /// <summary>
        /// Split Object, Splits Collection
        ///True DBGrid supports Excel-like splits that divide the grid into vertical panes to provide users with different views of the data source. Each split is represented by a Split object and contains a group of adjacent columns that scroll as a unit.
        ///When a TDBGrid control is created, it contains one Split object by default. Many of the properties of the Split object also apply to the TDBGrid control as a whole, so you do not need to concern yourself with splits until you actually need to use them, such as when creating fixed, nonscrolling columns.
        ///The TDBGrid control maintains a Splits collection to hold and manipulate Split objects. A grid has one split by default, but may contain multiple splits. This collection is accessed using the Splits property.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Split
        {
            get;
            set;
        }

        /// <summary>
        /// Col Property
        ///Sets or returns current column number.
        ///Syntax
        ///object.Col = integer
        ///Remarks
        ///Read/Write at run time. Not available at design time.
        ///This property specifies the zero-based index of the current cell's column position. It may be set at run time to highlight a different cell within the current row. 
        ///If the column is visible, the caret or marquee will be moved to the selected column. 
        ///If the column is not visible, the grid will scroll to make it visible as a result of setting this property.
        ///Setting the Col property at run time does not affect selected columns. Use the SelEndCol and SelStartCol properties to specify a selected region.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Col
        {
            get
            {
                return cCol;
            }
            set
            {
                //BAN: Disable firing the BeforeRowColChange event
                beforeRowColChange = false;
                //move current cell
                if (value >= 0 && !this.disableEvents)
                {
                    if (this.CurrentCell != null)
                    {
                        this.CurrentCell = this[GetColumnIndexByDisplayIndex(value), this.CurrentCell.RowIndex];
                    }
                    else
                    {
                        if (this.Rows.Count > 0)
                        {
                            this.CurrentCell = this[GetColumnIndexByDisplayIndex(value), 0];
                        }
                    }
                }

                cCol = value;
                //BAN: Disable firing the BeforeRowColChange event
                beforeRowColChange = true;
            }
        }

        /// <summary>
        /// ExtendRightColumn Property
        ///This property allows the rightmost column of a grid or split to extend to the object's right edge, provided that the object can accommodate all of the visible columns.
        ///Syntax
        ///object.ExtendRightColumn = boolean
        ///Remarks
        ///Read/Write at run time and design time.
        ///If True, the last column will extend to the end of the grid or split.
        ///If False (the default), the area between the last column and the end of the grid or split will be filled using the system 3D Objects color (or the system Button Face color) as determined by your Control Panel settings.
        ///If a grid contains multiple splits, then setting its ExtendRightColumn property has the same effect as setting the ExtendRightColumn property of each split individually.
        ///Note
        ///This property now works even when the horizontal scroll bar is present. Prior to version 5.0, if a grid or split could not accommodate all of the visible columns, then setting this property to True had no effect.
        /// </summary>
        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(false)]
        public bool ExtendRightColumn
        {
            get
            {
                return extendRightColumn;
            }
            set
            {
                extendRightColumn = value;
            }
        }

        public new bool Enabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                //SBE: ForeColor is automatically changed when Enabled is false and color == Color.Black
                if (value == false)
                {
                    if (this.DefaultCellStyle.ForeColor.Name == System.Drawing.Color.Black.Name)
                    {
                        this.DefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(this.DefaultCellStyle.ForeColor.ToArgb());
                    }

                    foreach (DataGridViewColumn col in this.Columns)
                    {
                        if (col.DefaultCellStyle.ForeColor.Name == System.Drawing.Color.Black.Name)
                        {
                            col.DefaultCellStyle.ForeColor = System.Drawing.Color.FromArgb(col.DefaultCellStyle.ForeColor.ToArgb());
                        }
                    }
                    //CHE: for disabled grid do not show current row selection, change/reset SelectionBackColor
                    previousSelectionBackColor = this.DefaultCellStyle.SelectionBackColor;
                    this.DefaultCellStyle.SelectionBackColor = this.BackColor;
                }
                else
                {
                    //CHE: for disabled grid do not show current row selection, change/reset SelectionBackColor
                    if (previousSelectionBackColor != null)
                    {
                        this.DefaultCellStyle.SelectionBackColor = (Color)previousSelectionBackColor;
                        previousSelectionBackColor = null;
                    }
                }

                base.Enabled = value;
                //CHE: set Enabled to inverted panel, all subcontrols will be automatically set
                if (this.InvertedView != null)
                {
                    this.InvertedView.Enabled = value;
                }
            }
        }

        [DefaultValue(MultipleLines.Disabled)]
        public MultipleLines MultipleLines
        {
            get
            {
                return multipleLines;
            }
            set
            {
                multipleLines = value;
                if (multipleLines == MultipleLines.Enabled)
                {
                    DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                }
                else
                {
                    DefaultCellStyle.WrapMode = DataGridViewTriState.False;
                }
            }
        }

        [DefaultValue(true)]
        public bool AllowFocus
        {
            get
            {
                return allowFocus;
            }
            set
            {
                allowFocus = value;
            }
        }

        [DefaultValue(true)]
        public bool AllowUpdate
        {
            get
            {
                return allowUpdate;
            }
            set
            {
                allowUpdate = value;
                this.ReadOnly = !allowUpdate;
            }
        }

		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public new bool ReadOnly
		{
			get
			{
				return base.ReadOnly;
			}
			set
			{
				base.ReadOnly = value;
			}
		}

        [DefaultValue("")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string LayoutFileName
        {
            get
            {
                return layoutFileName;
            }
            set
            {
                layoutFileName = value;
            }
        }

        [DefaultValue("")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string LayoutName
        {
            get
            {
                return layoutName;
            }
            set
            {
                layoutName = value;
            }
        }

        /// <summary>
        ///shouldn't return dbgAddNewPending or dbgAddNewCurrent unless the current position is on the new row;
        ///implement to return values as follows:
        ///dbgNoAddNew - if current row is not the last one (new row)
        ///dbgAddNewCurrent - if current row is the last one (new row) and nothing has been entered (row is not dirty)
        ///dbgAddNewPending - if current row is the last one (new row) and there are changes (row is dirty)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public AddNewMode AddNewMode
        {
            get
            {
                if (this.AllowUserToAddRows)
                {
                    if (this.CurrentRow == null)
                    {
                        //CHE: in VB6 in DropDownOpen for "*" row the AddNewMode is 2 - dbgAddNewPending
                        return AddNewMode.dbgAddNewPending;
                    }
                    DataRowView dataRowView = this.CurrentRow.DataBoundItem as DataRowView;
                    //CNA- check CurrentCell
                    if (this.CurrentCell != null)
                    {
                        if (dataRowView != null && dataRowView.IsNew && dataRowView.IsEdit)
                        {
                            return fecherFoundation.AddNewMode.dbgAddNewPending;
                        }

                        if (!this.Rows[this.CurrentCell.RowIndex].IsNewRow)
                        {
                            return fecherFoundation.AddNewMode.dbgNoAddNew;
                        }
                        else
                        {
                            return fecherFoundation.AddNewMode.dbgAddNewCurrent;
                        }

                        //BAN: check if the row is the penulimate row
                        if (this.Rows[this.CurrentCell.RowIndex].IsNewRow || (dataRowView != null && dataRowView.IsNew) || this.Rows.Count - 2 == this.CurrentCell.RowIndex)
                        {
                            //If the current row is a new row, or if it`s data bound item is new, then the add new mode is at the current row
                            if (this.Rows[this.CurrentCell.RowIndex].IsNewRow || (dataRowView != null && dataRowView.IsNew))
                            {
                                return AddNewMode.dbgAddNewCurrent;
                            }
                            if (this.IsCurrentRowDirty || !this.Focused)
                            {
                                return AddNewMode.dbgAddNewPending;
                            }
                            else
                            {
                                //BAN: if the current cell is not in edit mode then NoAddNew should be returned. This case hapens
                                //for the penultimate row, when selecting the row should return NoAddNew because the row is not newly added
                                //but when the cell is in edit mode it means that the row was just added
                                if (this.IsCurrentCellInEditMode)
                                {
                                    return AddNewMode.dbgAddNewCurrent;
                                }
                                else
                                {
                                    return AddNewMode.dbgNoAddNew;
                                }
                            }
                        }
                        else
                        {
                            return AddNewMode.dbgNoAddNew;
                        }
                    }
                    else
                    {
                        return AddNewMode.dbgNoAddNew;
                    }
                }
                else
                {
                    return AddNewMode.dbgNoAddNew;
                }
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(true)]
        public bool AllowColSelect
        {
            get
            {
                return allowColSelect;
            }
            set
            {
                allowColSelect = value;
            }
        }

        [DefaultValue(MarqueeStyle.FloatingEditor)]
        public MarqueeStyle MarqueeStyle
        {
            get
            {
                return marqueeStyle;
            }
            set
            {
                //do not enter in RowColChange event
                inMarqueeStyle = true;
                marqueeStyle = value;
                switch (value)
                {
                    case MarqueeStyle.HighlightRow:
                        {
                            this.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                            break;
                        }
                    default:
                        {
                            //CHE: in WebGUI default SelectionMode is RowHeaderSelect
                            this.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
                            break;
                        }
                }
                inMarqueeStyle = false;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public TabAction TabAction
        {
            get
            {
                return TabAction.GridNavigation;
            }
            set { }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(Appearance.dbg3D)]
        public Appearance Appearance
        {
            get
            {
                return this.appearance;
            }
            set
            {
                this.appearance = value;
            }
        }

        [DefaultValue(typeof(System.Drawing.Color), "240, 240, 240")]
        public System.Drawing.Color DeadAreaBackColor
        {
            get
            {
                return base.BackgroundColor;
            }
            set
            {
                if (value == System.Drawing.Color.Empty)
                {
                    base.BackgroundColor = System.Drawing.SystemColors.Window;
                }
                else
                {
                    base.BackgroundColor = value;
                }

                if (DesignMode)
                {
                    return;
                }

                if (this.IsInvertedOrFormViewGenerated())
                {
                    this.InvertedView.BackColor = base.BackgroundColor;
                }
            }
        }

        [DefaultValue(typeof(System.Drawing.Color), "Window")]
        public new System.Drawing.Color BackgroundColor
        {
            get
            {
                return backColor;
            }
            set
            {
                backColor = value;
                this.DefaultCellStyle.BackColor = value;
            }
        }

        [DefaultValue(ScrollBars.Automatic)]
        public new ScrollBars ScrollBars
        {
            get
            {
                return this.scrollBars;
            }
            set
            {
                this.scrollBars = value;

                if (IsInvertedOrFormViewGenerated())
                {
                    this.InvertedView.AutoScroll = (this.ScrollBars != ScrollBars.None);
                }
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(true)]
        public bool AllowArrows
        {
            get
            {
                return allowArrows;
            }
            set
            {
                allowArrows = value;
            }
        }

        /// <summary>
        /// WrapCellPointer Property
        ///This property determines the behavior of the arrow keys if AllowArrows is True.
        ///Syntax
        ///TDBGrid.WrapCellPointer = boolean
        ///Remarks
        ///Read/Write at run time and design time.
        ///If True, the cell pointer will wrap from the last column to the first in the next row (or from the first column to the last in the previous row).
        ///If False (the default), the cell pointer will not wrap to the next (or previous) row, but will stop at the last (or first) column of the current row.
        ///If TabAcrossSplits is False, the cell pointer will wrap only within the current split. If TabAcrossSplits is True, the cell pointer will move from one split to the next before wrapping occurs.
        ///If TabAction is set to 2 - Grid Navigation, the TAB key will behave like the arrow keys, and will automatically wrap to the next or previous cell.
        /// </summary>
        [DefaultValue(false)]
        public bool WrapCellPointer
        {
            get
            {
                return wrapCellPointer;
            }
            set
            {
                wrapCellPointer = value;
            }
        }

        [DefaultValue(DataViewType.dbgNormalView)]
        public DataViewType DataView
        {
            get
            {
                return dataView;
            }
            set
            {
                dataView = value;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool CurrentCellVisible
        {
            get;
            set;
        }

        public new object DataSource
        {
            get
            {
                return base.DataSource;
            }
            set
            {
				if (dataSource == value)
				{
					return;
				}
				dataSource = value;
				if (initialized)
				{
					SetDataSource(value);
				}
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(false)]
        public bool DisableEvents
        {
            get
            {
                return disableEvents;
            }
            set
            {
                disableEvents = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool IsDropDownOpened
        {
            get
            {
                return isDropDownOpened;
            }
            set
            {
                isDropDownOpened = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool DisablePreRenderRegenerate
        {
            get
            {
                return disablePreRenderRegenerate;
            }
            set
            {
                bool newValue = value;
                if (this.Form != null && newValue != disablePreRenderRegenerate)
                {
                    disablePreRenderRegenerate = newValue;
                    List<Control> controlList = this.Form.GetAllControls();
                    foreach (Control ctrl in controlList)
                    {
                        if (ctrl.GetType() == typeof(FCTDBGrid) && ctrl != this)
                        {
                            FCTDBGrid frmGrid = ctrl as FCTDBGrid;
                            frmGrid.DisablePreRenderRegenerate = disablePreRenderRegenerate;
                        }
                    }
                }
            }
        }

        public Form Form
        {
            get
            {
                return this.Parent as Form;
            }

        }

        public float ScrollTop { get; private set; }
        public int ScrollLeft { get; private set; }

		[DefaultValue(false)]
        public bool AllowAddNew
        {
            get
            {
                return base.AllowUserToAddRows;
            }

            set
            {
                base.AllowUserToAddRows = value;
            }
        }

		[DefaultValue(false)]
        public bool AllowDelete
        {
            get
            {
                return AllowUserToDeleteRows;
            }
            set
            {
                AllowUserToDeleteRows = value;
            }
        }

		[DefaultValue(false)]
		public new bool AllowUserToDeleteRows
		{
			get
			{
				return base.AllowUserToDeleteRows;
			}
			set
			{
				base.AllowUserToDeleteRows = value;
			}
		}

		[DefaultValue(false)]
		public bool AllowColMove { get; set; }

		[DefaultValue(true)]
		public bool AllowRowSelect { get; set; } = true;

		[DefaultValue(true)]
		public bool AllowRowSizing { get; set; } = true;

        /// <summary>
        /// Specifies the context ID corresponding to a What's This Help topic or an HTML Help topic for an object. 
        /// Syntax
        /// Object.WhatsThisHelpID[ = nContextID]
        /// Return Value
        /// nContextID
        /// Specifies the context ID number of the corresponding topic in a Help file.
        /// The following table lists the values for nContextID and action performed when clicking the What's This Help button in the title bar.
        /// nContextID  Description
        /// Negative    Displays the What's This Help popup window with the text, "No Help topic is associated with this item" or the main Help page of an HTML Help file, if available. (Default, –1)
        /// Zero        Searches the object hierarchy for the first object with a positive WhatsThisHelpID value.If found, Visual FoxPro displays the corresponding What's This Help topic or HTML Help topic, if available.
        ///             If not found, Visual FoxPro does not display a What's This Help topic or an HTML Help topic.
        /// Positive    Displays the What's This Help topic or HTML Help topic matching the specified nContextID.
        ///             If no Help topic matches the specified nContextID, Visual FoxPro displays the What's This Help popup window with the text, "No Help topic is associated with this item" or main Help page of an HTML Help file, if available.
        /// </summary>
        /// <remarks>
        /// Applies To: CheckBox | ComboBox | CommandButton | CommandGroup | Container Object | Control Object | EditBox | Form | Grid | Header | Image | Label | Line | ListBox | OLE Bound Control | OLE Container Control | OptionButton | OptionGroup | Shape | Spinner | TextBox | Timer | ToolBar
        /// </remarks>
        [DefaultValue(0)]
        public int WhatsThisHelpID
        {
            get
            {
                return this.whatsThisHelpID;
            }
            set
            {
                if (value < 0)
                {
                    this.whatsThisHelpID = 0;
                }
                else
                {
                    this.whatsThisHelpID = value;
                }
            }
        }

        /// <summary>
        /// Returns or sets a ToolTip.
        /// Syntax
        /// object.ToolTipText [= string]
        /// The ToolTipText property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// string	A string associated with an object in the Applies To list. that appears in a small rectangle below the object when the user's cursor hovers over the object at run time for about one second.
        /// Remarks
        /// If you use only an image to label an object, you can use this property to explain each object with a few words.
        /// At design time you can set the ToolTipText property string in the control's properties dialog box.
        /// For the Toolbar and TabStrip controls, you must set the ShowTips property to True to display ToolTips.
        /// </summary>
        [DefaultValue("")]
        public string ToolTipText
        {
            get
            {
                return toolTip.GetToolTip(this);
            }
            set
            {
                toolTip.SetToolTip(this, value);
            }
        }

		[DefaultValue(DataGridViewColumnHeadersHeightSizeMode.AutoSize)]
		public DataGridViewColumnHeadersHeightSizeMode ColumnHeadersHeightSizeMode
		{
			get
			{
				return base.ColumnHeadersHeightSizeMode;
			}
			set
			{
				base.ColumnHeadersHeightSizeMode = value;
			}
		}

		[DefaultValue(23)]
		public new int RowHeadersWidth
		{
			get
			{
				return base.RowHeadersWidth;
			}
			set
			{
				base.RowHeadersWidth = value;
			}
		}
		#endregion

		#region Constructors
		public FCTDBGrid()
            : base()
        {
            using (Graphics g = this.CreateGraphics())
            {
                GraphicsFactory graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }

            //CHE: DataGridView default RowHeaderWidth is 43 but for TDBGrid is 23 - measured with Paint
            this.RowHeadersWidth = 23;
			//AllowDelete is false in original, by default
			base.AllowUserToDeleteRows = false;
			this.DeadAreaBackColor = Color.FromArgb(240, 240, 240);
			this.messageQueue = new List<int>();
            this.DataSourceChanged += new EventHandler(FCTrueDBGrid_DataSourceChanged);
            this.Splits = new FCSplitCollection(this);
            this.selBookmarks = new SelBookmarks(this);
            this.BindingContextChanged += new EventHandler(FCTrueDBGrid_BindingContextChanged);
            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            base.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(FCTrueDBGrid_CellValidating);
            base.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(FCTrueDBGrid_CellValidated);
            base.CellFormatting += FCTDBGrid_CellFormatting;
            this.Columns = new FCDataGridViewColumnCollection(this);
            this.columnHeaderHeight = this.ColumnHeadersHeight;
            this.AllowUserToAddRows = false;
            this.ColumnStateChanged += new DataGridViewColumnStateChangedEventHandler(FCTrueDBGrid_ColumnStateChanged);
            this.ColumnDividerDoubleClick += new DataGridViewColumnDividerDoubleClickEventHandler(FCTrueDBGrid_ColumnDividerDoubleClick);
            this.ColumnRemoved += new DataGridViewColumnEventHandler(FCTrueDBGrid_ColumnRemoved);
            this.RowsRemoved += FCTDBGrid_RowsRemoved;
			this.RowsAdded += FCTDBGrid_RowsAdded;
            this.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(FCTrueDBGrid_CellClick);
            //CHE: set font and rowheight based on font
            this.font.FontFamily = FontFamily.GenericSansSerif;
            this.font.Size = 8.25f;
            this.RowHeight = this.Font.Size + 11;
			this.headFont.FontFamily = FontFamily.GenericSansSerif;
			this.headFont.Size = 8.25f;
			this.footerFont.FontFamily = FontFamily.GenericSansSerif;
			this.footerFont.Size = 8.25f;
			//CHE: silent error for editable ComboBoxColumn when entered value is not in drop down list (System.ArgumentException - DataGridViewComboBoxCell value is not valid)
			this.DataError += FCTrueDBGrid_DataError;
        }
		#endregion

		#region Public Delegates
		public delegate void OnComboSelect(object sender, EventArgs e);
        public delegate void OnUnboundColumnFetch(object sender, DataGridViewCellFormattingEventArgs e);
        public delegate void OnButtonClick(object sender, EventArgs e);
        public delegate void OnColEdit(object sender, DataGridViewColumnEventArgs e);
        public delegate void OnAfterUpdate(object sender, EventArgs e);
        public delegate void OnAfterInsert(object sender, EventArgs e);
        public delegate void OnBeforeUpdate(object sender, DataGridViewRowCancelEventArgs e);
        public delegate void OnBeforeInsert(object sender, DataGridViewRowCancelEventArgs e);
        public delegate void OnPostEvent(object sender, int MsgId);
        public delegate void OnBeforeRowColChange(object sender, CancelEventArgs e);
        public delegate void OnRowColChange(object sender, EventArgs e);
        public delegate void OnEnterPressed(object sender, KeyEventArgs e);
        public delegate void OnColResize(object sender, DataGridViewColumnEventArgs e);
        public delegate void DataGridViewCellValidatingEventHandler(object sender, FCDataGridViewCellValidatingEventArgs e);
        public delegate void DataGridViewCellEventHandler(object sender, DataGridViewCellEventArgs e);
        #endregion

        #region Public Events
        public event OnComboSelect ComboSelect;
        /// The UnboundColumnFetch event is fired when a bound grid 
        /// (one with its DataMode property set to 0 - Bound) needs to display the value of a cell in an unbound column   
        public event OnUnboundColumnFetch UnboundColumnFetch;
        public event OnButtonClick ButtonClick;
        public event OnColEdit ColEdit;
        public event OnAfterUpdate AfterUpdate;
        public event OnAfterInsert AfterInsert;
        public event OnBeforeUpdate BeforeUpdate;
        public event OnBeforeInsert BeforeInsert;
        public event OnPostEvent PostEvent;
        public event OnBeforeRowColChange BeforeRowColChange;
        public event OnRowColChange RowColChange;
        public event OnEnterPressed EnterPressed;
        public event OnColResize ColResize;
        public event EventHandler<DataGridViewCellValueEventArgs> FormatText;
        public event EventHandler<CancelEventArgs> SelChange;
        public event EventHandler<ColEventArgs> HeadClick;
        public event EventHandler<FetchCellStyleEventArgs> FetchCellStyle;
        public event EventHandler<UnboundReadDataEventArgs> UnboundReadData;
        public event EventHandler<FetchRowStyleEventArgs> FetchRowStyleEvent;
		#endregion

		#region Public Methods
		public override void Refresh()
		{
            if (this.DataMode == DataMode.Unbound || this.DataMode == DataMode.Application || this.DataMode == DataMode.UnboundExtended)
            {
                Rows.Clear();
                RaiseUnboundReadData(null);
            }
			base.Refresh();
		}

		private void RaiseUnboundReadData(int? startLocation)
		{
			if (UnboundReadData != null)
			{
				UnboundReadDataEventArgs args = new UnboundReadDataEventArgs();
				args.ReadPriorRows = false;
				args.StartLocation = startLocation;
				args.RowBuf = new RowBuffer();
				args.RowBuf.RowCount = 10;
				args.RowBuf.ColumnCount = this.Columns.Count;
				args.RowBuf.Value = new object[10, this.Columns.Count];
				args.RowBuf.Bookmark = new int[10];
				UnboundReadData(this, args);
				
				for (int r = 0; r < args.RowBuf.RowCount; r++)
				{
					DataGridViewRow row = new DataGridViewRow();
					row.Height = (int)RowHeight;
					int rowIndex = this.Rows.Add(row);
					for (int i = 0; i < Columns.Count; i++)
					{
                        this[i, rowIndex].Value = args.RowBuf.Value[r, i];
					}
				}
                //All rows were supplied with data
                if(args.RowBuf.RowCount == 10)
                {
                    //Check for other rows again
                    if (startLocation == null)
                    {
                        startLocation = 9;
                    }
                    else
                    {
                        startLocation += 10;
                    }
                    RaiseUnboundReadData(startLocation);
                }
			}
		}

		public void BeginInit()
		{
			initialized = false;
		}

		public void EndInit()
		{
			if (DesignMode)
			{
				return;
			}

            //CHE: force Refresh to populate with UnboundReadData event
            this.Refresh();

			Form parentForm = this.FindForm();

            var ds = this.dataSource as FCData;
            if(ds != null)
            {
                ds.ParentGrid = this;
				//FC:FINAL:MW IIT#191 - Refreshed event of FCData should be connected before Load of form, ParentForm_load is too late (comes after form Load) 
                ds.Refreshed -= Data_Refreshed;
                ds.Refreshed += Data_Refreshed;
            }

			parentForm.Load += ParentForm_Load;
		}

		private void ParentForm_Load(object sender, EventArgs e)
		{
			initialized = true;
			HoldFields();
			SetDataSource(this.dataSource);
			//FC:FINAL:MW IIT#191 - Refreshed event of FCData should be connected before Load of form/grid, ParentForm_load is too late (comes after form Load) 
            //FCData data = dataSource as FCData;
			//if (data != null)
			//{
				//data.Refreshed += Data_Refreshed;
			//}
			//ClearFields();
		}

        //BAN
        /// <summary>
        /// RowContaining Method
        ///The RowContaining method returns the zero-based index of the display row containing the specified coordinate.
        ///Syntax
        ///object.RowContaining(coordinate)
        ///Arguments
        ///coordinate is a single that defines a vertical coordinate (Y value) in twips.
        ///Return Value
        ///An integer corresponding to the display row beneath the specified Y coordinate.
        ///Remarks
        ///This value ranges from 0 to VisibleRows - 1.
        ///When handling mouse and drag events, this method is useful when you need to determine where the user clicked or dropped another control in terms of a grid row.
        ///If coordinate is outside of the grid's data area, this method returns -1.
        ///Note
        ///You must convert the coordinate argument to twips, even if the container's ScaleMode (Visual Basic) setting specifies a different unit of measurement.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public int RowContaining(float Y)
        {
            //If the coordinate is outside of the grid return -1
            if (Y > this.Height || Y < this.ColumnHeadersHeight)
            {
                return -1;
            }

            return GetRowVisibleIndex((int)((Y - this.ColumnHeadersHeight + this.ScrollTop) / this.RowHeight));
        }

        /// <summary>
        /// The ColContaining method returns the ColIndex value of the grid column containing the specified coordinate. 
        /// This value ranges from 0 to Columns.Count - 1. 
        /// The ColContaining method returns the ColIndex of the column indicated, not the visible column position. 
        /// For example, if coordinate falls within the first visible column, but two columns have been scrolled off the left side of the control, 
        /// the ColContaining method returns 2, not 0 (assuming that the user did not move any columns previously). 
        /// </summary>
        /// <param name="X"></param>
        /// <returns></returns>
        public int ColContaining(float X)
        {
            //If the coordinate is outside of the grid return -1
            if (X > this.Width || X < this.RowHeadersWidth)
            {
                return -1;
            }

            int colLeft = 0;
            int colRight = this.RowHeadersWidth;
            DataGridViewColumn[] sortedCols = GetColumnsByDisplayIndex();

            //CHE: get first column visible
            int firstVisibleColumnDisplayIndex = this.Columns[this.FirstDisplayedScrollingColumnIndex].DisplayIndex;
            //get first one, even if it is partial
            firstVisibleColumnDisplayIndex = this.ScrollLeft > 0 ? firstVisibleColumnDisplayIndex - 1 : firstVisibleColumnDisplayIndex;

            //CHE: loop the columns currently visible
            for (int counter = firstVisibleColumnDisplayIndex; counter < sortedCols.Count(); counter++)
            {
                //calculate visible width of first partial column
                int firstColumnsWidth = 0;
                for (int i = 0; i <= firstVisibleColumnDisplayIndex; i++)
                {
                    if (sortedCols[i].Visible)
                    {
                        firstColumnsWidth += sortedCols[i].Width;
                    }
                }
                int partialVisibleWidth = firstColumnsWidth - this.ScrollLeft;

                //check position
                DataGridViewColumn col = sortedCols[counter];
                if (col != null && col.Visible == true)
                {
                    //is in first partial column
                    if (X <= partialVisibleWidth + this.RowHeadersWidth)
                    {
                        return col.Index;
                    }
                    //left limit
                    colLeft = colRight;
                    //right limit
                    colRight = colLeft + (counter == firstVisibleColumnDisplayIndex ? partialVisibleWidth : col.Width);
                    //check position
                    if (colLeft <= X && colRight >= X)
                    {
                        return col.Index;
                    }
                }
            }
            return -1;
        }

        public void GenerateInvertedGrid()
        {
            if (DesignMode || SuppressRedraw)
            {
                return;
            }
            if (IsInvertedOrFormView() && this.Parent != null)
            {
                #region Setup Inverted Panel
                Panel invertedPanel = new Panel();
                invertedPanel.Size = this.Size;
                invertedPanel.Location = this.Location;

                invertedPanel.AutoScroll = (this.ScrollBars != ScrollBars.None);

                invertedPanel.BackColor = this.DeadAreaBackColor;
                //IPI: need to save the reference to the grid 
                invertedPanel.Tag = this;

                invertedPanel.TabStop = this.TabStop;
                invertedPanel.TabIndex = this.TabIndex;

                // when invertedView is removed, it will make the grid and the invertedPanel invisible
                bool isPanelVisible = this.Visible;
                base.Visible = false;
                bool wasFocused = false;
                if (this.InvertedView != null)
                {
                    //CHE: check if it was focused before
                    wasFocused = isInvertedCtrlFocused;
                    //CHE: remove all controls for inverted
                    ClearInvertedPanel();
                }

                this.Parent.Controls.Add(invertedPanel);
                // must set the panel visibility after invertedView is removed and invertedPanel added back as contained control
                invertedPanel.Visible = isPanelVisible;
                #endregion

                bool showHeaders = this.ColumnHeadersVisible;

                Font ctrlFont = new Font(this.font.FontFamily, this.font.Size, FontStyle.Regular);
                Font ctrlFontBold = new Font(this.font.FontFamily, this.font.Size, FontStyle.Bold);

                int rows = GetInvertedRows();

                int headerWidth = this.ViewColumnCaptionWidth;

                #region Calculate Header Width
                if (showHeaders)
                {
                    int headerLabelWidth = 0;
                    // in VB6 when ViewColumnCaptionWidth = 0, 
                    // the header width is calculated as Max width of column headers text width
                    if (this.ViewColumnCaptionWidth == 0)
                    {
                        foreach (DataGridViewColumn col in this.Columns)
                        {

                            headerLabelWidth = System.Windows.Forms.TextRenderer.MeasureText(col.HeaderText, col.HeadFont().Bold ? ctrlFontBold : ctrlFont).Width;
                            if (headerWidth < headerLabelWidth)
                            {
                                headerWidth = headerLabelWidth;
                            }
                        }
                    }
                }
                else
                {
                    headerWidth = 0;
                }
                #endregion

                int ctrlWidth = this.ViewColumnWidth;

                int labelX = 0;
                int controlX = headerWidth;
                //int y = 5;
                int y = 0;

                #region Generate Caption
                if (this.IsCaptionVisible)
                {
                    Label captionLabel = new Label();
                    captionLabel.Text = this.Caption;
                    captionLabel.Location = new Point(labelX, y);
                    captionLabel.TextAlign = ContentAlignment.MiddleCenter;
                    captionLabel.Width = this.Width - 10;
                    captionLabel.Height = Convert.ToInt32(RowHeight);
                    invertedPanel.Controls.Add(captionLabel);

                    Panel captionUnderlign = new Panel();
                    captionUnderlign.Height = 1;
                    //captionUnderlign.Width = this.Width - 10;
                    captionUnderlign.Width = this.Width;
                    captionUnderlign.BackColor = Color.Black;
                    //captionUnderlign.Location = new Point(labelX, y + RowHeight + 1);
                    captionUnderlign.Location = new Point(labelX, y + Convert.ToInt32(RowHeight));
                    //captionUnderlign.BorderColor = Color.Black;
                    invertedPanel.Controls.Add(captionUnderlign);

                    //y += RowHeight + 2;
                    y += Convert.ToInt32(RowHeight);
                }
                #endregion

                int ctrlTabIndex = 0;

                foreach (DataGridViewColumn col in this.Columns)
                {
                    GenerateInvertedGridColumn(col, ref y, ref controlX, ref ctrlWidth, ref ctrlTabIndex, labelX, headerWidth, ctrlFont, ctrlFontBold, rows, invertedPanel, wasFocused);
                }

                //when formview center panel if controls sum height is smaller than grid height
                if (this.DataView == DataViewType.dbgFormView)
                {
                    //calculate required height for grid, resize and center controls
                    int dif = this.Height - y;
                    if (dif > 0)
                    {
                        //invertedPanel.Height -= dif;
                        //invertedPanel.Top += dif / 2;

                        //header remains on top
                        int skipRows = 0;
                        if (this.IsCaptionVisible)
                        {
                            skipRows = 2;
                        }
                        //center controls
                        for (int i = skipRows; i < invertedPanel.Controls.Count; i++)
                        {
                            Control control = invertedPanel.Controls[i];
                            control.Top += dif / 2;
                        }
                    }
                }

                this.InvertedView = invertedPanel;

                //CHE: set alignment for controls based on column alignment
                this.SetAlignment();
            }
        }

        //public override DataGridViewColumn GetColumnByPropertyDescriptor(System.ComponentModel.PropertyDescriptor objPropertyDescriptor)
        //{
        //    //CHE: avoid null exception when calling ShouldChangeTypeByUsingPropertyName
        //    if (objPropertyDescriptor == null)
        //    {
        //        return base.GetColumnByPropertyDescriptor(objPropertyDescriptor);
        //    }

        //    bool shouldChangeType = ShouldChangeTypeByUsingPropertyName(objPropertyDescriptor);

        //    //bool shouldChangeType = ShouldChangeTypeByUsingReflection(objPropertyDescriptor);

        //    if (shouldChangeType) // First column (For example)
        //    {
        //        return new DataGridViewCharacterCasingTextBoxColumn();
        //    }

        //    return base.GetColumnByPropertyDescriptor(objPropertyDescriptor);
        //}

        /// <summary>
        /// Identifies a row and column under an X, Y coordinate pair.
        ///Syntax
        ///TDBGrid.CellContaining(x, y, rowindex, colindex)
        ///Arguments
        ///x and y are singles that define a coordinate pair in twips.
        ///rowindex is a long that receives the zero-based index of the row beneath the specified Y coordinate.
        ///colindex is an integer that receives the zero-based index of the column beneath the specified X coordinate.
        ///Return Value
        ///A Boolean that indicates whether a data cell is beneath the specified coordinate pair.
        ///Remarks
        ///The CellContaining method combines the ColContaining and RowContaining methods into one call. If the coordinate pair specified by x and y points to a data cell, this method returns True, and the rowindex and colindex arguments receive zero-based indexes that identify the cell.
        ///This method is useful when working with mouse and drag events when you are trying to determine where the user clicked or dropped another control in terms of a grid cell.
        ///If the specified coordinate is outside of the grid's data area, this method returns False. You can use the PointAt method to determine what kind of grid element, if any, is beneath the specified coordinate.
        ///Note
        ///You must convert the x and y arguments to twips, even if the container's ScaleMode (Visual Basic) setting specifies a different unit of measurement.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="rowIndex"></param>
        /// <param name="colIndex"></param>
        /// <returns></returns>
        public bool CellContaining(float x, float y, ref int rowIndex, ref int colIndex)
        {
            rowIndex = this.RowContaining(y);
            colIndex = this.ColContaining(x);

            if (rowIndex >= 0 && colIndex >= 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// restores the default grid layout (with two blank columns) so that subsequent 
        /// ReBind operations will automatically derive new column bindings from the 
        /// (possibly changed) data source.
        /// </summary>
        public void ClearFields()
        {
            //CHE: preserve flag
            bool oldDisableEvents = this.disableEvents;

            originalColumns = null;

            //init columns as in VB6 (two empty columns)
            this.disableEvents = true;
            this.Columns.Clear();
            this.disableEvents = oldDisableEvents;
        }

        /// <summary>
        /// The RowBookmark method returns a bookmark for a visible row relative to the first displayed row.
        /// Allowable values for the rownumber argument range from 0 to VisibleRows - 1
        /// </summary>
        /// <param name="rownumber">rownumber is an integer denoting a displayed row</param>
        /// <returns>A variant containing a bookmark corresponding to the display row specified by rownumber</returns>
        public int? RowBookmark(int rownumber)
        {
            int bookmark = 0;
            foreach (DataGridViewRow gridRow in this.Rows)
            {
                if (gridRow.Visible)
                {
                    bookmark++;
                    if (gridRow.Index == rownumber)
                    {
                        return bookmark;
                    }
                }
            }
            return -1;
        }

        public int IsSelected(int? row)
        {
            if (selBookmarks == null)
            {
                return -1;
            }

            return selBookmarks.IndexOf(row);
        }

        public int SplitContaining(float X, float Y)
        {
            if (X < 0 || X > this.Width || Y < 0 || Y > this.Height)
            {
                return -1;
            }
            int columnsWidth = 0;
            foreach (DataGridViewColumn column in this.Columns)
            {
                columnsWidth += column.Visible ? column.Width : 0;
                if (X <= columnsWidth)
                {
                    return column.GetAttachedSplit();
                }
            }
            return -1;
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public void ExportToFile(string filePath, bool append, int rows = -1, string width = null)
        {
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public void ExportToDelimitedFile(string filePath, string delimiter = "", int rows = -1, string prefix = "", string suffix = "")
        {
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public void RefreshCell()
        {
        }

        public void PostMsg(int message)
        {
            messageQueue.Add(message);
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public int RowTop(int Row)
        {
            return 0;
        }

        /// <summary>
        /// The Delete method deletes the current record, then automatically moves to the next available record. 
        /// If the last record is deleted, then EOF becomes the current position.
        /// </summary>
        public void Delete()
        {
            if (CurrentRow != null)
            {
                this.CurrentRow.Selected = true;
                //CHE: in some cases it throws NullReferenceException but deletion is completed though
                try
                {
                    this.ProcessDeleteKey(Keys.None);
                }
                catch (NullReferenceException)
                {
                }

            }
        }

        /// <summary>
        /// change current row position as first 
        /// </summary> 
        public void MoveFirst()
        {
            if (this.RowCount > 0 && this.ColumnCount > 0)
            {
                if (this.CurrentCell != null && this.CurrentCell.ColumnIndex >= 0)
                {
                    this.CurrentCell = this.Rows[0].Cells[this.CurrentCell.ColumnIndex];
                }
                else
                {
                    this.CurrentCell = this.Rows[0].Cells[FirstVisibleColumn()];
                }

                bof = eof = false;
            }
        }

        /// <summary>
        /// change current row position as last
        /// </summary>
        public void MoveLast()
        {
            if (this.RowCount > 0 && this.ColumnCount > 0)
            {
                if (this.CurrentCell != null && this.CurrentCell.ColumnIndex >= 0)
                {
                    this.CurrentCell = this.Rows[this.RowCount - 1].Cells[this.CurrentCell.ColumnIndex];
                }
                else
                {
                    this.CurrentCell = this.Rows[this.RowCount - 1].Cells[FirstVisibleColumn()];
                }

                bof = eof = false;
            }
        }

        /// <summary>
        /// change current row position as next
        /// </summary>
        public void MoveNext()
        {
            if (this.RowCount > 0 && this.ColumnCount > 0 && this.CurrentRow != null)
            {
                //CNA: if AllowUserToAddRows is True last row should not be considered in RowCount
                int realRowCount = this.RowCount;
                if (this.AllowUserToAddRows)
                {
                    --realRowCount;
                }
                if (this.CurrentRow.Index == realRowCount - 1)
                {
                    eof = true;
                    return;
                }

                if (this.CurrentCell != null && this.CurrentCell.ColumnIndex >= 0)
                {
                    this.CurrentCell = this.Rows[this.CurrentRow.Index + 1].Cells[this.CurrentCell.ColumnIndex];
                }
                else
                {
                    this.CurrentCell = this.Rows[this.CurrentRow.Index + 1].Cells[FirstVisibleColumn()];
                }
            }
        }

        /// <summary>
        /// change current row position as previous
        /// </summary>
        public void MovePrevious()
        {
            if (this.RowCount > 0 && this.ColumnCount > 0 && this.CurrentRow != null)
            {
                if (this.CurrentRow.Index == 0)
                {
                    bof = true;
                    return;
                }

                if (this.CurrentCell != null && this.CurrentCell.ColumnIndex >= 0)
                {
                    this.CurrentCell = this.Rows[this.CurrentRow.Index - 1].Cells[this.CurrentCell.ColumnIndex];
                }
                else
                {
                    this.CurrentCell = this.Rows[this.CurrentRow.Index - 1].Cells[FirstVisibleColumn()];
                }
            }
        }

        /// <summary>
        /// The MoveRelative method operates like the Move method of the Recordset object; it moves the current record offset rows relative to the specified bookmark.
        /// </summary>
        /// <param name="position"></param>
        public void MoveRelative(int position)
        {
            if (position > 0)
            {
                while (position-- > 0)
                {
                    MoveNext();
                }
            }
            else if (position < 0)
            {
                while (position++ < 0)
                {
                    MovePrevious();
                }
            }
        }

        public void ReBind()
        {
            int crtRow = this.Row;
            int crtCol = this.Col;

            if (this.DataMode != DataMode.Storage)
                return;

            if (Array == null)
                return;

            if (Array.Value == null)
            {
                //if now Value is available, the existing Rows are not valid anymore
                this.Rows.Clear();
                return;
            }

            int rowCount = Array.Value.GetLength(0);
            int colCount = Array.Value.GetLength(1);

            object ds = null;
            disableEvents = true;
            inReBind = true;
            //if datasource is set the dataview is considered bounded although our datamode is set to dbgUnboundSt
            //in bounded mode you cannot change rowcount and columncount
            if (this.DataSource != null)
            {
                ds = this.DataSource;
                this.DataSource = null;
            }

            if (rowCount == 0)
            {
                //if AllowUserToAddRows is true, you cannot set RowCount to 0. 
                //In this case, call the DataGridViewRowCollection.Clear method to remove all rows except the row for new records. 
                //Calling Clear has the same result as setting RowCount to 1 in this case, but is much faster. 
                this.Rows.Clear();
            }
            else
            {
                this.RowCount = rowCount;
            }
            if (this.AutoGenerateColumns)
            {
                this.ColumnCount = Math.Max(this.ColumnCount, colCount);
            }
            //reassign datasource, possible it is use later; VB6 allows to have both array and datasource set
            if (ds != null)
            {
                this.DataSource = ds;
            }
            else
            {
                //clear rows
                if (this.Rows.Count != 0)
                {
                    this.Rows.Clear();
                }

                for (int i = 0; i < rowCount; i++)
                {
                    //add new row
                    this.Rows.Add();

                    for (int j = 0; j < Math.Min(this.ColumnCount, colCount); j++)
                    {
                        //CNA - Readonly CustomCombobox columns are made by using DropdownList style, which does not allow to display values which are not in ValuItems, so values to display are add to ValuItems too.
                        if (GetColumnType(Columns[j]) == typeof(DataGridViewCustomComboBoxColumn) && Columns[j].DropDownList() && this.Columns[j].ReadOnly)
                        {
                            if (!this.Columns[j].ValueItems().ContainsValue(Array.Value[i, j]))
                            {
                                this.Columns[j].ValueItems().Add(Array.Value[i, j], Array.Value[i, j]);
                            }
                        }
                        //number format is ignored when passing string e.g. #,##0.00 will work with 0 value, but will not work for "0"
                        if (this.Columns[j].NumberFormat().Contains('#') && Array.Value != null && Array.Value[i, j].GetType() == typeof(string))
                        {
                            this[j, i].Value = Convert.ToDouble(Array.Value[i, j]);
                        }
                        else
                        {
                            this[j, i].Value = Array.Value[i, j];
                        }
                    }
                }
            }

            this.RedrawInverted();
            this.disableEvents = false;
            inReBind = false;

            //reset position
            if (IsValidItem(crtRow, crtCol) && crtRow <= this.Rows.Count && crtCol <= this.Columns.Count)
            {
                this.Row = crtRow;
                this.Col = crtCol;
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        public void RefreshCol(int col)
        {
        }

        //BAN
        public PointAt PointAt(float x, float y)
        {
            #region Replaced by CellMouseClick and MouseClick implementation
            ////Create coodinates point
            //Point coordinatesAt = new Point((int)x, (int)y);
            ////Create zero point
            //Point zeroPoint = new Point(0,0);
            ////Create the grid`s rectangle
            //Rectangle grid = new Rectangle(zeroPoint, this.Size);

            ////Check if the event was fired from the caption or from the grid
            //bool isfromCaption = false;
            //foreach (System.Diagnostics.StackFrame frame in new System.Diagnostics.StackTrace().GetFrames())
            //{
            //    if (frame.GetMethod().Name == "FireEvent")
            //    {
            //        if (frame.GetMethod().DeclaringType.Name == "Control")
            //        {
            //            isfromCaption = true;
            //            break;
            //        }
            //    }
            //}

            ////If the grid does not contain the coodtinates point, the point is not inside the control
            //if (!grid.Contains(coordinatesAt))
            //{
            //    return fecherFoundation.PointAt.dbgNotInGrid;
            //}
            //else
            //{
            //    if (isfromCaption)
            //    {
            //        return fecherFoundation.PointAt.dbgAtCaption;
            //    }
            //    else
            //    {
            //        Rectangle colHeaders = new Rectangle(zeroPoint, new Size(this.Width, this.ColumnHeadersHeight));
            //        Rectangle rowHeaders = new Rectangle(zeroPoint, new Size(this.RowHeadersWidth, this.Height));
            //        if (colHeaders.Contains(coordinatesAt))
            //        {
            //            return fecherFoundation.PointAt.dbgAtColumnHeader;
            //        }
            //        else if (rowHeaders.Contains(coordinatesAt))
            //        {
            //            return fecherFoundation.PointAt.dbgAtRowSelect;
            //        }
            //        else
            //        {
            //            return fecherFoundation.PointAt.dbgAtDataArea;
            //        }
            //    }
            //}

            //return fecherFoundation.PointAt.dbgNotInGrid;
            #endregion

            return pointAt;
        }

        /// <summary>
        /// HoldFields Method
        ///The HoldFields method sets the current column/field layout as the customized layout so that subsequent ReBind operations will use the current layout for display.
        ///Syntax
        ///object.HoldFields
        ///Arguments
        ///None
        ///Return Value
        ///None
        ///Remarks
        ///You can resume the grid's automatic layout behavior by invoking the ClearFields method.
        ///The HoldFields method is especially useful in the unbound modes when you have specified the column layout in code and would like to keep it intact after a ReBind operation.
        /// </summary>
        public void HoldFields()
        {
            disableEvents = true;
            if (this.ColumnCount > 0 && originalColumns == null)
            {
                originalColumns = new DataGridViewColumn[this.ColumnCount];
                for (int i = 0; i < this.ColumnCount; i++)
                {
                    originalColumns[i] = this.Columns[i];
                }
                //Displayindex will be reset on Databind, sort by DisplayIndex is required
                originalColumns = originalColumns.OrderBy(col => col.DisplayIndex).ToArray();
            }
            if (this.ColumnCount == 0)
            {
                originalColumns = null;
            }
            disableEvents = false;
        }

        /// <summary>
        /// retrieve the Bookmark for the FormatText event
        /// Bookmark is a variant that uniquely identifies the row from which the underlying data value was obtained.
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns></returns>
        public int GetDataBoundIndex(int rowIndex)
        {
            if (this.DataSource == null)
            {
                return 0;
            }

            if (this.dataTable == null)
            {
                return 0;
            }

            if (rowIndex < 0 || rowIndex >= this.RowCount)
            {
                return 0;
            }

            DataRowView dataRow = this.Rows[rowIndex].DataBoundItem as DataRowView;
            if (dataRow == null)
            {
                if (this.LastRow == null || this.Rows.Count <= this.LastRow)
                {
                    return 0;
                }
                dataRow = this.Rows[(int)this.LastRow].DataBoundItem as DataRowView;

                if (dataRow == null)
                {
                    return 0;
                }
            }

            //SBE: use dataRow.DataView.Table to search for Row index; dataRow.DataView contains only filtered and sorted data
            DataTable dataTable = dataRow.DataView.Table;
            int i = 0;

            for (i = 0; i < dataTable.Rows.Count; i++)
            {
                if (dataTable.Rows[i] == dataRow.Row)
                {
                    return i + 1;
                }
            }

            if (dataTable.DefaultView == null)
            {
                return 0;
            }

            //IPI: if user adds a new row in the table, the new row appears only in DefaultView until the row gets validated
            for (i = 0; i < dataTable.DefaultView.Count; i++)
            {
                if (dataTable.DefaultView[i] == dataRow)
                {
                    return i + 1;
                }
            }

            return 0;
        }

        //public void ShowExtendedHeaders()
        //{
        //    if (this.IsInvertedOrFormView())
        //    {
        //        return;
        //    }
        //    else
        //    {
        //        showExtendedColumnHeader = false;
        //        if (this.Splits.Count <= 1)
        //        {
        //            this.ExtendedColumnHeaders.ShowExtendedColumnHeader = false;
        //            return;
        //        }
        //        else
        //        {
        //            for (int i = 0; i < this.Splits.Count; i++)
        //            {
        //                if (!String.IsNullOrEmpty(this.Splits[i].Text))
        //                {
        //                    showExtendedColumnHeader = true;
        //                    break;
        //                }
        //            }
        //            this.ExtendedColumnHeaders.ShowExtendedColumnHeader = showExtendedColumnHeader;
        //            if (showExtendedColumnHeader)
        //            {
        //                this.ExtendedColumnHeaders.Rows.Clear();
        //                this.ExtendedColumnHeaders.HeaderControls.Clear();
        //                this.ExtendedColumnHeaders.Rows.Add(new System.Windows.Forms.ExtendedHeaderRowData());
        //                List<int> splitMaxColIndex = GetSplitMaxColIndex();
        //                for (int i = 0; i < this.Splits.Count; i++)
        //                {
        //                    ExtendedHeaderLabel headerLabel = new ExtendedHeaderLabel();
        //                    headerLabel.ColIndex = i > 0 ? splitMaxColIndex[i - 1] + 1 : 0;
        //                    headerLabel.ColSpan = (i > 0) ? (splitMaxColIndex[i] - splitMaxColIndex[i - 1]) : splitMaxColIndex[i] + 1;
        //                    headerLabel.Text = string.IsNullOrEmpty(this.Splits[i].Text) ? string.Empty : this.Splits[i].Text;
        //                    headerLabel.Dock = System.Windows.Forms.DockStyle.Fill;
        //                    headerLabel.Location = new System.Drawing.Point(0, 0);
        //                    headerLabel.ExtendedHeaderCellType = ExtendedHeaderCellType.Headers;
        //                    this.ExtendedColumnHeaders.HeaderControls.Add(headerLabel);
        //                }
        //            }
        //        }
        //    }
        //}

        public int GetCaptionHeight()
        {
            if (this.IsCaptionVisible)
            {
                int intCaptionHeight = this.ColumnHeadersHeight;
                return intCaptionHeight;
            }
            else
            {
                return 0;
            }



        }

        public void UpdateCurrentRow()
        {
            //Updates any changes on the current row to the data source
            if (ChangePositionOnDataTable(true))
            {
                Information.Err().Number = 6160;//ERR_DATA_ACCESS;
                throw new Exception();
            }
        }

        public new bool Focus()
        {
            if (this.IsInvertedOrFormViewGenerated() && !isInvertedCtrlFocused)
            {
                foreach (Control ctrl in this.InvertedView.Controls)
                {
                    InvertedData data = ctrl.Tag as InvertedData;
                    if (ctrl.GetType() != typeof(Label) && ctrl.CanFocus && data != null && data.Row == this.Row && data.Col.Index == this.Col)
                    {
                        //CNA: container panel should also be focused
                        this.InvertedView.Focus();
                        return ctrl.Focus();
                    }
                }
                return this.InvertedView.Focus();
            }

            return base.Focus();
        }

        public void RedrawInverted(bool checkDisableEvents = false)
        {
            if (checkDisableEvents && disableEvents)
            {
                return;
            }

            this.GenerateInvertedGrid();
            this.RefreshInvertedData();
        }


        /// <summary>
        /// Displays the Whats This Help topic specified for an object with the WhatsThisHelpID property.
        /// </summary>
        /// <example>
        /// Object.ShowWhatsThis
        /// Return Value
        /// Object
        /// Specifies the object for which the Whats This Help topic is displayed.
        /// </example>
        /// <remarks>
        /// Applies To: CheckBox | ComboBox | CommandButton | CommandGroup | Container Object | Control Object | EditBox | Form | Grid | Image | Label | Line | ListBox | OLE Bound Control | OLE Container Control | OptionButton | OptionGroup | Shape | Spinner | TextBox | Timer | ToolBar
        /// The ShowWhatsThis method is automatically called when the F1 key is pressed.If the WhatsThisHelp property is set to true (.T.), the Whats This Help topic for the object, specified with the WhatsThisHelpID property, is displayed.If the WhatsThisHelp property is set to false (.F.), the Help topic for the object, specified with the HelpContextID property, is displayed.
        /// </remarks>
        public void ShowWhatsThis()
        {
            Form form = this.FindForm();
            bool whatsThisHelp = (bool)form.GetType().GetProperty("WhatsThisHelp").GetValue(form);

            if (form != null && whatsThisHelp == true)
            {
                //BB:TODO display the Whats This Help Topic for the object, specified with WhatsThisHelpID
                if (this.WhatsThisHelpID > 0)
                {
                    //Displays the What's This Help topic or HTML Help topic matching the specified nContextID.
                    //If no Help topic matches the specified nContextID, Visual FoxPro displays the What's This Help popup window with the text, "No Help topic is associated with this item" or main Help page of an HTML Help file, if available.
                    ToolTipText = "Control " + this.Name + "\nWhatsThisHelpID = " + this.WhatsThisHelpID;
                }
                else if (this.WhatsThisHelpID == 0)
                {
                    //Searches the object hierarchy for the first object with a positive WhatsThisHelpID value. If found, Visual FoxPro displays the corresponding What's This Help topic or HTML Help topic, if available.
                    //If not found, Visual FoxPro does not display a What's This Help topic or an HTML Help topic.
                    ToolTipText = "Control " + this.Name + "\nWhatsThisHelpID = " + this.WhatsThisHelpID;
                }
                else
                {
                    // display this
                    ToolTipText = "No Help topic is associated with this item";
                    // or display the main Help page of an HTML Help file, if available
                    // or display nothing
                }
            }
            else
            {
                //BB:TODO display the Whats This Help Topic for the object, specified with HelpContextID

            }
        }
        #endregion

        #region Internal Methods

        internal void SetDataCellValue(DataGridViewColumn col, object cellValue)
        {
            SetDataCellValue(this.dataTable.CurrentRow(), col, cellValue);
        }

        internal void SetDataCellValue(DataRow row, DataGridViewColumn col, object cellValue)
        {
            if (this.dataTable == null)
            {
                return;
            }
            DataRow dr = row;
            if (!string.IsNullOrEmpty(col.DataPropertyName) && this.dataTable.Columns[col.DataPropertyName] != null)
            {
                if(!disableEvents && col.NumberFormat() == "FormatText Event")
                {
                    var e = new DataGridViewCellValueEventArgs(col.Index, this.CurrentRow.Index);
                    e.Value = cellValue;
                    RaiseFormatText(e);
                    cellValue = e.Value;
                }
                if (this.dataTable.Columns[col.DataPropertyName].DataType == typeof(int) && cellValue.GetType() == typeof(string) && string.IsNullOrEmpty(cellValue.ToString()))
                {
                    cellValue = "0";
                }
                //SBE - check ConvertEmptyCell property
                DataGridViewColumnDefinitions objColDefinitions = DataGridViewColumnDefinitions.GetDataTableDefinitions(col);
                if (objColDefinitions.ConvertEmptyCell == ConvertEmptyCell.AsNull && Convert.ToString(cellValue) == string.Empty)
                {
                    dr[col.DataPropertyName] = DBNull.Value;
                }
                else
                {
                    dr[col.DataPropertyName] = cellValue;
                }
                if (!this.DisableEvents)
                {
                    DataGridViewCellEventArgs cellEventArgs = new DataGridViewCellEventArgs(col.Index, this.CurrentRow.Index);
                    //this.OnCellValueChanged(cellEventArgs, false);
                }
            }
        }

        //CHE: set max length
        internal void SetMaxLength(DataGridViewTextBoxColumn tbCol, TextBox tb = null)
        {
            if ((this.dataTable != null) && (this.dataTable.Columns[tbCol.DataPropertyName] != null))
            {
                //SBE: set max length if value > 0
                int maxLength = this.dataTable.Columns[tbCol.DataPropertyName].MaxLength;
                if (maxLength > 0)
                {
                    if (tb != null)
                    {
                        tb.MaxLength = maxLength;
                    }
                    else
                    {
                        SetMaxLengthAll(tbCol, maxLength);
                    }
                }
            }
            else
            {
                //CHE: for unbounded grid set MaxInputLength on grid column
                if (tbCol.MaxInputLength > 0)
                {
                    if (tb != null)
                    {
                        tb.MaxLength = tbCol.MaxInputLength;
                    }
                    else
                    {
                        SetMaxLengthAll(tbCol, tbCol.MaxInputLength);
                    }
                }
            }
        }

        internal HorizontalAlignment ColAlignToTextAlign(Type colValueType, Alignment colAlign)
        {
            switch (colAlign)
            {
                case fecherFoundation.Alignment.Center:
                    {
                        return HorizontalAlignment.Center;
                    }

                case fecherFoundation.Alignment.Left:
                    {
                        return HorizontalAlignment.Left;
                    }

                case fecherFoundation.Alignment.Right:
                    {
                        return HorizontalAlignment.Right;
                    }

                case fecherFoundation.Alignment.General:
                    {
                        TypeCode typeCode = Type.GetTypeCode(colValueType);
                        if (typeCode == TypeCode.Byte || typeCode == TypeCode.SByte ||
                            typeCode == TypeCode.UInt16 || typeCode == TypeCode.UInt32 || typeCode == TypeCode.UInt64 ||
                            typeCode == TypeCode.Int16 || typeCode == TypeCode.Int32 || typeCode == TypeCode.Int64 ||
                            typeCode == TypeCode.Decimal ||
                            typeCode == TypeCode.Double ||
                            typeCode == TypeCode.Single)
                        {
                            return HorizontalAlignment.Right;
                        }
                        else
                        {
                            return HorizontalAlignment.Left;
                        }
                    }

                default:
                    return HorizontalAlignment.Left;
            }
        }

        internal void RefreshInvertedData(DataGridViewColumn column = null)
        {
            if (DesignMode || SuppressRedraw)
            {
                return;
            }
            if (this.InvertedView == null)
            {
                return;
            }
            bool disableEventsPreviouslySet = DisableEvents == true;
            disableEvents = true;
            TranslateValues();
            List<Control> controls = null;
            if (column != null)
            {
                KeyValuePair<Label, List<Control>> colControls = column.ColumnControls();
                if (colControls.Value != null)
                {
                    controls = colControls.Value;
                }
            }
            else
            {
                controls = this.InvertedView.Controls.OfType<Control>().ToList();
            }
            foreach (Control ctrl in controls)
            {
                DataGridViewColumn col = null;
                int row = 0;
                InvertedData data = ctrl.Tag as InvertedData;
                Type controlType = ctrl.GetType();
                if (data != null)
                {
                    col = data.Col as DataGridViewColumn;
                    row = data.Row;
                }
                object value = null;
                if (col != null)
                {
                    if (row >= 0 && row < this.Rows.Count)
                    {
                        value = this.Rows[row].Cells[col.Index].Value;
                    }
                }

                if (controlType == typeof(TextBox))
                {
                    TextBox tb = ctrl as TextBox;
                    if (value != null)
                    {
                        tb.Text = value.ToString();
                        //CNA, SBE: Use column alignment, and check value type for textbox
                        tb.TextAlign = ColAlignToTextAlign(col.ValueType, col.Alignment());
                    }
                }
                if (controlType == typeof(FCTextBox))
                {
                    FCTextBox tb = ctrl as FCTextBox;
                    if (value != null)
                    {
                        tb.Text = value.ToString();
                        //CNA,SBE: Use column alignment, and check value type for textbox
                        tb.TextAlign = ColAlignToTextAlign(col.ValueType, col.Alignment());
                    }
                }
                if (controlType == typeof(FCComboBox) || controlType == typeof(FCCustomComboBox))
                {
                    ComboBox cmb = ctrl as ComboBox;
                    if (value != null)
                    {
                        if (cmb.Items.Count > 0)
                        {
                            cmb.SelectedItem = GetComboBoxItemByValue(cmb, value.ToString());
                            //CHE: for case when value has been modified in BeforeColUpdate and it doesn't match any item from list, leave it as it is
                            if (cmb.SelectedItem == null)
                            {
                                cmb.Text = value.ToString();
                            }
                        }
                        else
                        {
                            cmb.Text = value.ToString();
                        }
                    }
                }
                if (controlType == typeof(Panel))
                {
                    foreach (Control c in ctrl.Controls)
                    {
                        if (c.GetType() == typeof(RadioButton))
                        {
                            RadioButton rd = c as RadioButton;
                            InvertedData rdInvertedData = rd.Tag as InvertedData;
                            if (value != null)
                            {
                                //CHE: add Trim for case when value is " "
                                rd.Checked = (rd.Text == value.ToString() || Convert.ToString(rdInvertedData.Value).Trim() == value.ToString().Trim());
                            }
                        }
                    }
                }
                if (controlType == typeof(CheckBox))
                {
                    CheckBox chk = ctrl as CheckBox;
                    DataGridViewCheckBoxColumn chkCol = null;
                    if (data != null)
                    {
                        chkCol = data.Col as DataGridViewCheckBoxColumn;
                    }
                    if (value != null)
                    {
                        if (value.GetType() == typeof(string))
                        {
                            if (String.IsNullOrEmpty(Convert.ToString(value)))
                            {
                                chk.Checked = false;
                                continue;
                            }
                            else
                            {
                                chk.Checked = true;
                            }
                        }
                        else if (value.GetType() == typeof(bool))
                        {
                            chk.Checked = (bool)value;
                        }
                        else if (chkCol != null)
                        {
                            chk.Checked = value == chkCol.TrueValue ? true : false;
                        }
                    }
                }
            }
            if (!disableEventsPreviouslySet)
            {
                disableEvents = false;
            }
        }

        internal bool IsInvertedOrFormView()
        {
            return (this.DataView == DataViewType.dbgInvertedView || this.dataView == DataViewType.dbgFormView);
        }

        internal string RightStr(string value, int length)
        {
            return value.Substring(value.Length - length);
        }

        internal void TranslateValues()
        {
            //CHE: do not execute for unbound mode
            if (IsInvertedOrFormViewGenerated() || this.GetType() == typeof(TDBDropDown) || this.DataMode == fecherFoundation.DataMode.Unbound)
            {
                return;
            }

            object cellvalue = null;
            object displayValue = null;
            foreach (DataGridViewColumn col in this.Columns)
            {
                //BAN: display value may be the default value (such as 0 for a databound Int32 column) and may not be present
                //in the ValueItems list; if so, add it to the list otherwise DataError may occur
                if (col is DataGridViewComboBoxColumn && col.ValueItems().Count > 0 && col.ValueItems().Translate)
                {
                    foreach (DataGridViewRow row in this.Rows)
                    {
                        if (this[col.Index, row.Index].Value == null)
                            continue;

                        //SBE: add an empty row in ValueItems, if value is not null
                        cellvalue = this[col.Index, row.Index].Value;
                        displayValue = col.ValueItems()[cellvalue];
                        if (displayValue == null && !this.DisableEvents && !(cellvalue == null || Information.IsDBNull(cellvalue)))
                        {
                            this.disableEvents = true;
                            col.ValueItems().Add(cellvalue, cellvalue);
                            this.disableEvents = false;
                        }
                    }
                }
            }
        }

        internal void PostEvents()
        {
            int msg;

            while (this.messageQueue.Count > 0)
            {
                msg = this.messageQueue[0];
                this.messageQueue.RemoveAt(0);
                if (PostEvent != null)
                {
                    PostEvent(this, msg);
                }
            }
        }

        //IPI: OnCellFormatting is not raised for CustomComboBoxColumn which will not raise FormatText Event => must allow CustomComboBox cell to explicitly raise CellFormatting
        internal void RaiseCellFormatting(DataGridViewCellFormattingEventArgs e)
        {
            //CHE: avoid reentrant call, do not raise again CellFormatting if is already in progress 
            //(e.g. setting row ReadOnly in FetchRowStyle will enter in GetFormattedValue for DataGridViewCustomComboBoxCell and that should not call again grid.RaiseCellFormatting)
            if (!cellFormattingInProgress)
            {
                this.OnCellFormatting(e);
            }
        }

        internal void RaiseCellValidating(object sender, FCDataGridViewCellValidatingEventArgs e)
        {
            //CHE: when clicking another cell should consider validating cancel and remaining on previous cell if the case
            this.validatingCanceled = false;
            if (this.DisableEvents)
            {
                return;
            }

            //DataGridViewCellValidatingEventHandler cellValidatingEventHandler = (DataGridViewCellValidatingEventHandler)this.GetHandler(OnCellValidating);
            DataGridViewCellValidatingEventHandler cellValidatingEventHandler = null;
            if (cellValidatingEventHandler != null)
            {
                if (this.IsInvertedOrFormView())
                {
                    FCDataGridViewCellValidatingEventArgs eventArgs = new FCDataGridViewCellValidatingEventArgs(this.Col, this.Row, this[this.Col, this.Row].FormattedValue, this.currentCellOldValue);
                    cellValidatingEventHandler(sender, eventArgs);
                    if (eventArgs.Cancel)
                    {
                        this[this.Col, this.Row].Value = eventArgs.OldValue;
                    }
                    else
                    {
                        this.currentCellOldValue = this.CurrentCell.Value;
                        //CHE: avoid AfterColUpdate being called twice, use flag for inverted too
                        if (shouldRaiseCellValidated)
                        {
                            RaiseCellValidated(this, new DataGridViewCellEventArgs(this.Col, this.Row));
                        }
                    }
                }
                else
                {
                    // CellValidating/CellValidated is sent in .NET also when changing DataSource 
                    // or when adding new columns; when current cell is not in edit mode => setting e.Cancel generates exception
                    //The CustomComboBoxCell will not always return IsCurrentCellInEditMode true - check if the dropdown is closing
                    //CNA: Also check cellValidatingCellInEditMode for cases when CellValidating is called twice and only the second call proceed
                    if (this.IsCurrentCellInEditMode || this.IsDropDownClosing || cellValidatingCellInEditMode)
                    {
                        //IPI: if MoveFirst is called in BeforeColUpdate for a DropDownGrid, setting CurrentCell will generate SelectionChanged causing an additional CellValidating
                        this.CellValidatingInProgress = true;
                        //IPI: must save CurrentCell in case in BeforeColUpdate the current row is deleted (with DataChanged = false)
                        DataGridViewCell currentCellBeforeValidating = this.CurrentCell;
                        cellValidatingEventHandler(sender, e);
                        this.CellValidatingInProgress = false;
                        if (e.Cancel)
                        {
                            //IPI: old value must be restored only if the CurrentCell has not been modified in BeforeColUpdate
                            if (this.CurrentCell == currentCellBeforeValidating)
                            {
                                this.CurrentCell.Value = e.OldValue;
                                //IPI: even though CellValidating returns false, the next cell will be selected; in this case, must clear the SelectedCells and get in edit mode for the current cell
                                this.ClearSelection();
                                this.BeginEdit(true);
                                //CHE: restore selection
                                this.CurrentCell.Selected = true;
                                //CHE: when clicking another cell should consider validating cancel and remaining on previous cell if the case
                                this.validatingCanceled = true;
                            }
                        }
                        else
                        {
                            this.currentCellOldValue = this.CurrentCell.Value;
                            if (shouldRaiseCellValidated)
                            {
                                RaiseCellValidated(this, new DataGridViewCellEventArgs(this.CurrentCell.ColumnIndex, this.CurrentCell.RowIndex));
                            }
                        }
                    }
                }

            }
            else
            {
                if (shouldRaiseCellValidated && this.CurrentCell != null)
                {
                    this.currentCellOldValue = this.CurrentCell.Value;
                    RaiseCellValidated(this, new DataGridViewCellEventArgs(this.CurrentCell.ColumnIndex, this.CurrentCell.RowIndex));
                }
            }
        }

        internal void RaiseRowColChange()
        {
            if(RowColChange != null)
            {
                var rowColChangeEventArgs = new EventArgs();
                RowColChange(this, rowColChangeEventArgs);
            }
        }

        internal void RaiseHeadClick(DataGridViewCellMouseEventArgs args)
        {
            if(HeadClick != null)
            {
                var e = new ColEventArgs(this.Columns[args.ColumnIndex]);
                HeadClick(this, e);
            }
        }

        internal void RaiseFormatText(DataGridViewCellValueEventArgs args)
        {
            if(FormatText != null)
            {
                FormatText(this, args);
            }
        }


        internal void RaiseBeforeRowColChange()
        {
            if (beforeRowColChange)
            {
                if (BeforeRowColChange != null)
                {
                    isFromBeforeRowColChange = true;
                    CancelEventArgs beforeRowColChangeEventArgs = new CancelEventArgs();
                    BeforeRowColChange(this, beforeRowColChangeEventArgs);
                    beforeRowColChangeCancel = beforeRowColChangeEventArgs.Cancel;
                    isFromBeforeRowColChange = false;
                    //If the cell click event was raised before the RowColChange event and was not processed, raise it again
                    //if the cancel parameter is not set to true
                    if (requiresCellClick && !beforeRowColChangeCancel)
                    {
                        this.disableEvents = true;
                        //Set the current cell to the cell that was clicked
                        if (IsValidItem(cellClickEventArgs.RowIndex, cellClickEventArgs.ColumnIndex))
                        {
                            this.CurrentCell = this[cellClickEventArgs.ColumnIndex, cellClickEventArgs.RowIndex];
                        }
                        this.disableEvents = false;
                        OnCellClick(cellClickEventArgs);
                        requiresCellClick = false;
                    }
                }
            }
        }

        internal void RedrawInvertedColumn(DataGridViewColumn col)
        {
            KeyValuePair<Label, List<Control>> colControls = col.ColumnControls();

            if (colControls.Value == null || colControls.Value.Count == 0)
            {
                RedrawInverted();
                return;
            }

            int labelX = colControls.Key.Left;
            int y = colControls.Key.Top;
            int headerWidth = colControls.Key.Width;
            Font ctrlFont = colControls.Key.Font.Clone() as Font;
            Font ctrlFontBold = colControls.Key.Font.Clone() as Font;

            int controlX = colControls.Value[0].Left;
            int ctrlWidth = colControls.Value[0].Width;
            int ctrlTabIndex = colControls.Value[0].TabIndex;
            int rows = GetInvertedRows();
            Panel invertedPanel = this.InvertedView;
            bool wasFocused = colControls.Value[0].Focused;


            disableEvents = true;
            DisposeInvertedControl(colControls.Key);
            for (int i = 0; i < colControls.Value.Count; i++)
            {
                DisposeInvertedControl(colControls.Value[i]);
            }
            colControls.Value.Clear();
            disableEvents = false;

            //generate controls
            GenerateInvertedGridColumn(col, ref y, ref controlX, ref ctrlWidth, ref ctrlTabIndex, labelX, headerWidth, ctrlFont, ctrlFontBold, rows, invertedPanel, wasFocused);

            //fill value
            this.RefreshInvertedData(col);
        }

        //CHE: remove all controls for inverted
        internal void ClearInvertedPanel()
        {
            if (this.InvertedView != null)
            {
                //APE: prevent ctrl_Leave to be raised when controls are added
                disableEvents = true;
                this.Parent.Controls.Remove(InvertedView);

                //CHE: dispose generated objects for inverted
                DisposeInvertedControl(InvertedView);

                this.InvertedView.Controls.Clear();

                this.InvertedView.Dispose();
                disableEvents = false;
                this.InvertedView = null;
            }
        }

        internal void ChangeColAllowFocus(DataGridViewColumn col)
        {
            if (!IsInvertedOrFormViewGenerated())
            {
                return;
            }

            KeyValuePair<Label, List<Control>> colControls = col.ColumnControls();

            if (colControls.Value != null)
            {
                foreach (Control ctrl in colControls.Value)
                {
                    ctrl.Enabled = !col.ReadOnly;
                }
            }
        }

        internal void ChangeColHeaderBackColor(DataGridViewColumn col, Color backColor)
        {
            if (!IsInvertedOrFormViewGenerated())
            {
                return;
            }

            KeyValuePair<Label, List<Control>> colControls = col.ColumnControls();
            Label lbl = colControls.Key as Label;
            if (lbl != null)
            {
                lbl.BackColor = backColor;
            }
        }

        internal void ChangeColHeaderForeColor(DataGridViewColumn col, Color foreColor)
        {
            if (!IsInvertedOrFormViewGenerated())
            {
                return;
            }

            KeyValuePair<Label, List<Control>> colControls = col.ColumnControls();
            Label lbl = colControls.Key as Label;
            if (lbl != null)
            {
                lbl.ForeColor = foreColor;
            }
        }

        internal void SetModified(DataGridViewColumn col, bool on)
        {
            if (col != null)
            {
                DataGridViewRow gridRow = null;

                gridRow = this.CurrentRow;

                if (gridRow != null)
                {
                    gridRow.SetModified(col, on);
                }
            }

            if (on)
            {
                dataChanged = true;
            }
        }

        internal void RaiseButtonClick(object sender, EventArgs e)
        {
            if (ButtonClick != null)
            {
                ButtonClick(sender, e);
                PostEvents();
            }
        }

        // OnRowValidated is protected, must add wrapper to be able to raise from fecherFoundation (e.g. DGVCustomComboBoxCell)
        internal void RaiseRowValidated(object sender, DataGridViewCellEventArgs e)
        {
            this.OnRowValidated(e);
        }

        // OnRowValidating is protected, must add wrapper to be able to raise from fecherFoundation (e.g. DGVCustomComboBoxCell)
        internal void RaiseRowValidating(object sender, DataGridViewCellCancelEventArgs e)
        {
            this.OnRowValidating(e);
        }

        internal void UpdateInvertedComboBox(DataGridViewColumn col)
        {
            if (!IsInvertedOrFormViewGenerated() || col == null)
            {
                return;
            }

            foreach (Control ctrl in this.InvertedView.Controls)
            {
                InvertedData data = ctrl.Tag as InvertedData;
                if (data != null && data.Col.Index == col.Index)
                {
                    FillInvertedComboBox(col, ctrl as ComboBox);
                }
            }
        }

        #endregion

        #region Protected Methods

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            // stop if grid resize was raised due to invertedView size change => avoid infinite loop 
            if (DisableEvents)
            {
                return;
            }

            if (IsInvertedOrFormViewGenerated())
            {
                int widthDifference = this.Width - this.InvertedView.Width;
                int ctrlWidthDifference = widthDifference;
                // setting the size of the invertedView (contained control), will generate grid resize
                // must use disable Events to prevent infinite loop
                this.disableEvents = true;
                this.InvertedView.Size = this.Size;
                this.disableEvents = false;
                foreach (Control ctrl in this.InvertedView.Controls)
                {
                    if (ctrl.GetType() != typeof(Label))
                    {
                        InvertedData data = (InvertedData)ctrl.Tag;
                        if (data != null)
                        {
                            ctrlWidthDifference = widthDifference / data.RowCount;
                            ctrl.Location = new Point(ctrl.Location.X + (data.Row * ctrlWidthDifference), ctrl.Location.Y);
                            ctrl.Width += ctrlWidthDifference;
                        }
                    }
                }
            }
        }

        //protected override void OnLocationChanged(LayoutEventArgs e)
        //{
        //    base.OnLocationChanged(e);
        //    if (IsInvertedOrFormViewGenerated())
        //    {
        //        //CNA: Prevent looped trigger of OnLocationChange
        //        if (!isResizing)
        //        {
        //            isResizing = true;
        //            this.InvertedView.Location = this.Location;
        //            isResizing = false;
        //        }
        //    }
        //}

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            if (IsInvertedOrFormViewGenerated())
            {
                this.InvertedView.Visible = this.Visible;
            }
        }

        protected override void OnSelectionChanged(EventArgs e)
        {
            //CHE: do not execute when changing MarqueeStyle
            if (inMarqueeStyle)
            {
                return;
            }

            //@CBA if disableevents = true don't call any event
            if (DisableEvents)
            {
                //SBE: Call PostEvents() even events are disabled. SelectionChanged is invoked also on selecting cell from other row. If PostEvents is called later, selected row is different
                PostEvents();
                //IPI: the base event must be raised otherwise the grid.Rows and grid.DefaultView row will no longer be synchronized
                base.OnSelectionChanged(e);
                return;
            }

            if (!beforeRowColChangeCancel && !isFromBeforeRowColChange)
            {
                base.OnSelectionChanged(e);
            }

            //APE: don't call PostEvents() if the current row it's a new row so the OnAddNew event will be raised
            if (this.CurrentRow != null && this.CurrentRow.IsNewRow)
            {
            }
            else
            {
                PostEvents();
            }
            //CHE: call FetchRowStyle when changing row
            if (this.CurrentCell != null && this.CurrentCell.RowIndex != LastRow)
            {
                int rowIndex = this.CurrentCell.RowIndex;
                int columnIndex = this.CurrentCell.ColumnIndex;

				//Type currentCellType = typeof(string);
				//if (this.CurrentCell.Value != null && this.CurrentCell.Value != DBNull.Value)
				//{
				//    currentCellType = this.CurrentCell.Value.GetType();
				//}

				//CHE: copy font from DefaultCellStyle to avoid null exception in FetchRowStyle
				//DataGridViewCellFormattingEventArgs args = null;
				//if (this.CurrentCell.Style != null && this.CurrentCell.Style.Font == null && this.DefaultCellStyle != null)
				//{
				//    //CHE: VWG bug, cannot set Font in SelectionChange, navigation with arrows is no longer possible after it
				//    //this.CurrentCell.Style.Font = this.DefaultCellStyle.Font;
				//    args = new DataGridViewCellFormattingEventArgs(columnIndex, rowIndex, this.CurrentCell.Value, currentCellType, this.DefaultCellStyle.Clone());
				//}
				//else
				//{
				//    args = new DataGridViewCellFormattingEventArgs(columnIndex, rowIndex, this.CurrentCell.Value, currentCellType, this.CurrentCell.Style);
				//}

                if (!IsValidItem(rowIndex, columnIndex) || !this.Visible)
                {
                    return;
                }
                DataGridViewColumn col = this.Columns[columnIndex];

                FetchRowStyleFetchCellStyle(rowIndex);
				lastRow = rowIndex;
				lastColumn = columnIndex;
			}
        }

        //HACK:IPI: VWG-12354: columns order is changed when table is transfered to another container
        protected override void OnBindingContextChanged(EventArgs e)
        {
            //base.OnBindingContextChanged(e);
        }

        //protected override void PreRenderControl(Gizmox.WebGUI.Common.Interfaces.IContext objContext, long lngRequestID)
        //{
        //    #region Adjust column headers height
        //    int maxRows = 1;

        //    Graphics g = null;
        //    Bitmap tmpBitmap = null;
        //    try
        //    {
        //        tmpBitmap = new Bitmap(10, 10);
        //        g = Graphics.FromImage(tmpBitmap);

        //        Font Tempfont = new System.Drawing.Font(this.ColumnHeadersDefaultCellStyle.Font.FontFamily, this.ColumnHeadersDefaultCellStyle.Font.Size);
        //        for (int counter = 0; counter < this.Columns.Count; counter++)
        //        {
        //            int rows = 1;
        //            if (this.Columns[counter].Visible)
        //            {
        //                int colWidth = this.Columns[counter].Width + 3;
        //                int width = (int)g.MeasureString(this.Columns[counter].HeaderText, Tempfont).Width;
        //                if (width > colWidth)
        //                {
        //                    string[] words = this.Columns[counter].HeaderText.Split(' ');
        //                    if (words.Length > 1)
        //                    {
        //                        int wordWidth = 0;
        //                        for (int i = 0; i < words.Length; i++)
        //                        {
        //                            string word = words[i];
        //                            int currentWordWidth = (int)g.MeasureString(word, Tempfont).Width;
        //                            if (wordWidth + currentWordWidth <= colWidth)
        //                            {
        //                                wordWidth += currentWordWidth;
        //                            }
        //                            else
        //                            {
        //                                rows++;
        //                                wordWidth = 0;
        //                            }
        //                        }
        //                    }
        //                }

        //                if (maxRows < rows)
        //                {
        //                    maxRows = rows;
        //                }
        //            }
        //        }
        //        if (maxRows > 1)
        //        {
        //            this.ColumnHeadersHeight = columnHeaderHeight * maxRows;
        //        }

        //        Tempfont.Dispose();
        //    }
        //    finally
        //    {
        //        g.Dispose();
        //        g = null;
        //        tmpBitmap.Dispose();
        //        tmpBitmap = null;
        //    }
        //    #endregion

        //    #region Set Columns Alignment
        //    foreach (DataGridViewColumn col in this.Columns)
        //    {
        //        this.SetCellAlignment(col);
        //    }
        //    #endregion

        //    TranslateValues();

        //    //CHE: do not execute merging unless the form is active (when executing while this.Context.ActiveForm is frmMDIMain the close button or any other click events only work second time)
        //    if (this.Context.ActiveForm == this.Form)
        //    {
        //        MergeCellsInColumn();
        //    }

        //    //SBE: Extended headers is used in one form (SdKarteikasten)
        //    //ShowExtendedHeaders();

        //    base.PreRenderControl(objContext, lngRequestID);
        //}

        protected override void OnMouseDown(MouseEventArgs e)
        {
            //do not raise for paging bar
            if (this.RowContaining(e.Y) >= this.VisibleRows)
            {
                return;
            }

            SetPointAt(e.Y);

            base.OnMouseDown(e);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return && EnterPressed != null)
            {
                EnterPressed(this, e);
            }
            else
            {
                base.OnKeyDown(e);
            }
        }

        /// <summary>
        /// Occurs when the user requests help for a control.
        /// The HelpRequested event is commonly raised when the user presses the F1 key or an associated context-sensitive help button is clicked.
        /// </summary>
        /// <param name="hevent"></param>
        protected override void OnHelpRequested(HelpEventArgs hevent)
        {
            //BB: The ShowWhatsThis method is automatically called when the F1 key is pressed.
            this.ShowWhatsThis();
        }

        protected override void OnCellMouseDown(DataGridViewCellMouseEventArgs e)
        {
            SetPointAt(e.ColumnIndex, e.RowIndex);

            //CHE: on user interaction SelBookmarks must be updated; if current cell is changed programmatically the SelBookmarks collection must be preserved
            SynchronizeSelBookmarks();

            base.OnCellMouseDown(e);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            //SBE: pointAt is calculated in OnMouseDown
            //SetPointAt(e.Y);

            base.OnMouseClick(e);
        }

        protected override void OnRowEnter(DataGridViewCellEventArgs e)
        {
            if (this.lastRow != e.RowIndex + 1 && this.Columns.Count > this.lastColumn)
            {
                this.SetModified(this.Columns[this.lastColumn], false);
            }
            if (!disableEvents)
            {
                base.OnRowEnter(e);
            }
        }

        protected override void OnRowValidated(DataGridViewCellEventArgs e)
        {
            //IPI: "Sometimes" RowValidated is generated in VWG immediately after "DropDownWindow" event
            // prevent closing the DropDownForm
            if (this.isDropDownOpened)
            {
                return;
            }
            bool cancel = false;
            //TODO:PJ:Why will DataChanged be set to true in CustomComboBoxCell, only?????
            if (!disableEvents && DataChanged)
                {
                DataGridViewRow row = this.Rows[e.RowIndex];
                //check if the row is the last row, used for the entry of a new row of data
                if (!row.IsNewRow)
                {
                    bool isAddedRow = false;

                    //SBE: check row state from DataSource
                    //DataRowView drView = row.DataBoundItem as DataRowView;
                    //if (row.IsNewRow || (drView != null && (drView.IsNew || drView.Row.RowState == DataRowState.Added)))
                    if (this.dataTable != null)
                    {
                        DataRowView dRow = null;
                        try
                        {
                            dRow = this.Rows[e.RowIndex].DataBoundItem as DataRowView;
                        }
                        catch (Exception ex)
                        {
                            //IPI: when RowValidated occurs due to a RowHeaderClick on the new * row, 
                            // accessing DataBoundItem throws exception
                        }
                        //IPI: must use index because setting the bookmark will change the grid's CurrentCell
                        int index = 0;
                        if (!this.dataTable.EOF())
                        {
                            // when user clicks on the new row, the new row is automatically added to DataTable.DefaultView
                            // and the current index references the new empty row
                            index = this.dataTable.GetIndex();
                        }

                        if (index > 0)
                        {
                            // in VB6, the current index stays on the row which is currently validated
                            int dataSourceIndex = this.dataTable.Rows.IndexOf(dRow.Row) + 1;
                            if (dataSourceIndex == 0)
                            {
                                //if the row is not in the Rows collection, it is a new row in DefaultView
                                if (this.dataTable.DefaultView.Count > 0)
                                {
                                    if (this.dataTable.DefaultView[this.dataTable.DefaultView.Count - 1] == dRow)
                                    {
                                        dataSourceIndex = this.dataTable.Rows.Count + 1;
                                    }
                                }
                            }
                            this.dataTable.SetIndex(dataSourceIndex);
                        }

                        if (dRow != null && dRow.Row.RowState == DataRowState.Added)
                        {
                            //IPI: in VB6, AfterInsert is generated when the user clicks on a new row in the grid and the previous editted row is validated
                            if (this.Rows.Count > 0 && this.Rows[this.Rows.Count - 1].IsNewRow && e.RowIndex != this.Rows.Count - 1)
                            {
                                isAddedRow = true;
                            }
                        }
                        if (!cancel)
                        {
                            cancel = RaiseBeforeUpdate(row);
                            if (!cancel)
                            {
                                base.OnRowValidated(e);
                                //IPI: when RowValidated is generated from DropDownForm.OnClose
                                // must commit the row in DataSource from DefaultView to Rows Collection
                                if (this.IsDropDownClosing && dRow != null && dRow.Row.RowState == DataRowState.Detached)
                                {
                                    //IPI: need to disable events because EndEdit will raise DGV RowValidated event
                                    this.disableEvents = true;
                                    //CNA: need to set empty values for CurrentRow's cells if DataTable column does not allow DBNull
                                    this.dataTable.SetRowEmptyValues(dRow.Row);
                                    dRow.EndEdit();
                                    this.disableEvents = false;
                                    //IPI: when the RowState changes from Detached to Added, AfterInsert event must be raised
                                    isAddedRow = true;
                                }
                                RaiseAfterUpdate();

                                if (isAddedRow)
                                {
                                    RaiseAfterInsert();
                                }
                            }
                            else
                            {
                                //IPI: when cancelled in BeforeUpdate, the row must be removed from grid
                                this.disableEvents = true;
                                this.Rows.Remove(row);
                                this.disableEvents = false;
                            }
                        }

                        //IPI: must use index because setting the bookmark will change the grid's CurrentCell
                        if (index > 0)
                        {
                            this.dataTable.SetIndex(index);
                        }
                    }
                }
                else
                {
                    base.OnRowValidated(e);
                }
                (base.DataSource as FCRecordset)?.Update();
            }
            else
            {
                base.OnRowValidated(e);
                (base.DataSource as FCRecordset)?.Update();
            }
        }

        protected override void OnRowHeaderMouseClick(DataGridViewCellMouseEventArgs e)
        {
            if (IsValidItem(e.RowIndex, 0))
            {
                // IPI: on right mouse click, context menu should be displayed and selected rows should not change 
                if (e.Button != MouseButtons.Right)
                {
                    // IPI: synchronize selBookmarks with SelectedRows on left mouse Click/CTRL+Click/ Shift+Click
                    SynchronizeSelBookmarks();
                }

                int firstCol = FirstVisibleColumn();
                if (firstCol != -1)
                {
                    //CHE: when setting CurrentCell the collection SelectedRows is changed, save it and reload it after
                    DataGridViewRow[] selRows = new DataGridViewRow[this.SelectedRows.Count];
                    this.SelectedRows.CopyTo(selRows, 0);

                    this.CurrentCell = this.Rows[e.RowIndex].Cells[firstCol];


                    //CHE: when setting CurrentCell the collection SelectedRows is changed, save it and reload it after
                    this.ClearSelection();
                    for (int i = 0; i < selRows.Count(); i++)
                    {
                        selRows[i].Selected = true;
                    }
                }

            }

            ChangePositionOnDataTable();
            base.OnRowHeaderMouseClick(e);
        }

        protected override void OnValidating(CancelEventArgs e)
        {
            if (disableEvents)
            {
                return;
            }

            base.OnValidating(e);
        }

        protected override void OnCurrentCellChanged(EventArgs e)
        {
            //CHE: do not execute when changing MarqueeStyle
            //CNA: Execute when DataSource is set
            if (!dataSourceIsSetting && (disableEvents || inMarqueeStyle))
            {
                return;
            }
            if (this.CurrentCell != null)
            {
                this.Col = this.Columns[this.CurrentCell.ColumnIndex].DisplayIndex;
                ChangePositionOnDataTable();

                //CHE: do not execute RowColChange if current row on datatable is on some row marked for deletion
                if (this.dataTable != null && !this.dataTable.EOF() && !this.dataTable.BOF())
                {
                    DataRow currentRow = this.dataTable.CurrentRow(true);
                    if (currentRow != null && currentRow.RowState == DataRowState.Deleted)
                    {
                        return;
                    }
                }
                currentCellOldValue = this.CurrentCell.Value;
                //CNA: Still do not fire if events are disabled, even if DataSource is set
                if (!disableEvents)
                {
                    base.OnCurrentCellChanged(e);
                    //IFCForm ifrm = this.Form as IFCForm;
                    //if (ifrm != null)
                    //{
                    //    //CNA: Set the next and previous navigable cell's relative coords to the current cell, used when navigating with TAB
                    //    if (!this.IsInvertedOrFormView())
                    //    {
                    //        Point NextNavigation = GetNextNavigableCellRelativeCoords();
                    //        Point PreviousNavigation = GetPreviousNavigableCellRelativeCoords();
                    //        ifrm.InvokeMethodRegisteredComponent("DataGridView_SetCellNavigation", NextNavigation.X, NextNavigation.Y, PreviousNavigation.X, PreviousNavigation.Y);
                    //    }
                    //}

                    RaiseRowColChange();
                }
            }
        }

        //protected override void FireEvent(IEvent objEvent)
        //{
        //    switch (objEvent.Type)
        //    {
        //        case "SelectionChanged":
        //            {
        //                if (this.CurrentCell != null)
        //                {
        //                    if (this.CurrentCell.ColumnIndex >= 0)
        //                    {
        //                        this.lastColumn = this.CurrentCell.ColumnIndex;
        //                    }
        //                    if (this.CurrentCell.RowIndex >= 0)
        //                    {
        //                        if (this.Rows[this.CurrentCell.RowIndex].IsNewRow)
        //                        {
        //                            this.lastRow = null;
        //                        }
        //                        else
        //                        {
        //                            this.lastRow = this.CurrentCell.RowIndex + 1;
        //                        }
        //                    }
        //                }

        //                if (!this.isDropDownOpened)
        //                {
        //                    RaiseBeforeRowColChange();
        //                }

        //                break;
        //            }
        //    }
        //    if (!this.disableEvents)
        //    {
        //        //IPI: if focus is on cell (0,0) and user clicks on a drop down form in cell e.g.(0,3), LostFocus/GotFocus/Click is generated
        //        // which will make the CurrentCell to be changed to (0,3) -> changing CurrentCell should not be possible until DropDownClosed
        //        if (!this.isDropDownOpened)
        //        {
        //            switch (objEvent.Type)
        //            {
        //                case "ValueChange":
        //                case "TextChange":
        //                    if (IsCurrentCellDirty)
        //                    {
        //                        NotifyCurrentCellDirty(false);
        //                    }
        //                    //IPI: if the current column is CustomComboBoxColumn, when deleting the combobox content, the new value is not set in CurrentCell
        //                    if (this.CurrentCell != null && this.Columns[this.CurrentCell.ColumnIndex] is DataGridViewCustomComboBoxColumn &&
        //                        (Information.IsDBNull(this.CurrentCell.Value) || Convert.ToString(this.CurrentCell.Value) != objEvent.Value))
        //                    {
        //                        this.CurrentCell.Value = objEvent.Value;
        //                    }
        //                    break;
        //            }
        //            base.FireEvent(objEvent);
        //        }
        //        else
        //        {
        //            //IPI: if DropDownForm is already opened, allow user to click the combobox drop-down arrow to close the DropDownForm
        //            if (objEvent.Type == "DropDownWindow")
        //            {
        //                base.FireEvent(objEvent);
        //            }
        //        }
        //    }
        //}

        protected override void OnCellClick(DataGridViewCellEventArgs e)
        {
            //CHE: when clicking another cell should consider validating cancel and remaining on previous cell if the case
            if (validatingCanceled)
            {
                validatingCanceled = false;
                return;
            }

            if (!IsValidItem(e.RowIndex, e.ColumnIndex))
            {
                base.OnCellClick(e);
                return;
            }

            //HACK:IPI: with VWG QA 42 for "some" reason, the CurrentCell is not correctly set in OnCellBeginEdit and in OnCellClick events
            SynchronizeCurrentCell(e.RowIndex, e.ColumnIndex);

            //exit for combo
            DataGridViewComboBoxColumn comboCol = this.Columns[e.ColumnIndex] as DataGridViewComboBoxColumn;
            DataGridViewCustomComboBoxColumn customCol = this.Columns[e.ColumnIndex] as DataGridViewCustomComboBoxColumn;
            if (comboCol != null && customCol == null)
            {
                return;
            }

            //BAN: if the column index is less than 0 then the row header was clicked. In this case the grid should not enter edit mode
            //as it will begin the edit on the current cell
            bool rowHeaderClicked = false;
            if (e.ColumnIndex < 0)
            {
                rowHeaderClicked = true;
            }

            //If the cancel parameter from BeforeRowColChange is false, do not process cell click and set the current cell back to the 
            //previously selected cell
            if (!beforeRowColChangeCancel)
            {
                if (!this.IsCurrentCellInEditMode)
                {
                    //BAN: if the current cell is the cell that was clicked, start the edit and select the text
                    if (this.CurrentCell != null && this.CurrentCell == this[e.ColumnIndex, e.RowIndex])
                    {
                        //If the row header was clicked, the current cell should not enter edit mode
                        if (!rowHeaderClicked)
                        {
                            if (this.IsCurrentCellInEditMode)
                            {
                                this.EndEdit();
                            }
                            //CHE: no need to call BeginEdit for CheckBoxColumn, CellBeginEdit already executed, otherwise OldValue wrongly set
                            if (this.Columns[e.ColumnIndex].GetType() != typeof(DataGridViewCheckBoxColumn))
                            {
                                this.BeginEdit(true);
                            }
                        }

                        base.OnCellClick(e);
                    }
                    //If the clicked cell is not the current cell, do not start the edit mode because the cancel parameter for BeforeRowColChange
                    //might be set to true. Preserve the cell click event args for further processing
                    else
                    {
                        cellClickEventArgs = e;
                        requiresCellClick = true;
                    }
                }
                else
                {
                    base.OnCellClick(e);
                }
            }
            else
            {
                if (this.lastRow != null && IsValidItem((int)this.lastRow - 1, this.lastColumn))
                {
                    this.disableEvents = true;
                    this.CurrentCell = this[this.lastColumn, (int)this.lastRow - 1];
                    this.disableEvents = false;
                }
            }
            //Reset the cancel parameter of the BeforeRowColChange
            beforeRowColChangeCancel = false;
        }

        protected override void OnGotFocus(EventArgs e)
        {
            if (!IsDropDownClosing && !isDropDownOpened)
            {
                base.OnGotFocus(e);
                SetAlignment();
                PostEvents();
            }
            else
            {
                IsDropDownClosing = false;
            }
        }

        protected override void OnAutoGenerateColumnsChanged(EventArgs e)
        {
            base.OnAutoGenerateColumnsChanged(e);

            //set ValueMember and DisplayMember for DataGridViewComboBoxColumn
            if (!this.AutoGenerateColumns)
            {
                foreach (DataGridViewColumn col in this.Columns)
                {
                    DataGridViewComboBoxColumn comboCol = col as DataGridViewComboBoxColumn;
                    if (comboCol != null && comboCol.Items.Count > 0)
                    {
                        comboCol.ValueMember = "Value";
                        comboCol.DisplayMember = "DisplayValue";
                    }
                }

            }
        }

        protected override void OnCellDoubleClick(DataGridViewCellEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                RowAreaDblClick = true;
                if (e.ColumnIndex == -1)
                {
                    RowHeaderDblClick = true;
                }
            }

            base.OnCellDoubleClick(e);
        }

        protected override void OnDoubleClick(EventArgs e)
        {
            ChangePositionOnDataTable();
            base.OnDoubleClick(e);
            RowAreaDblClick = false;
            RowHeaderDblClick = false;
        }

        //protected override void OnCellValueChanged(DataGridViewCellEventArgs e, bool blnClientSource)
        //{
        //    if (!IsValidItem(e.RowIndex, e.ColumnIndex))
        //    {
        //        if (e.RowIndex == -1)
        //        {
        //            //HACK:CHE: use " " instead of "" to clear HeaderText (grid.Columns[0].HeaderText = "" is automatically transformed into grid.Columns[0].HeaderText = "grid_0")
        //            if (e.ColumnIndex >= 0)
        //            {
        //                if (this.Columns[e.ColumnIndex].HeaderText == this.Name + "_" + e.ColumnIndex.ToString())
        //                {
        //                    this.Columns[e.ColumnIndex].HeaderText = " ";
        //                }
        //            }

        //            //in VB6 when ViewColumnCaptionWidth = 0, the header width is calculated as Max width of column headers text width
        //            if (this.ViewColumnCaptionWidth == 0 && this.ColumnHeadersVisible)
        //            {
        //                RedrawInverted();
        //            }
        //        }
        //        return;
        //    }

        //    //SBE: ComboSelect is not triggered for DataGridViewCustomComboBoxColumn, check if type is DataGridViewComboBoxColumn
        //    DataGridViewComboBoxColumn comboBoxColumn = this.Columns[e.ColumnIndex] as DataGridViewComboBoxColumn;
        //    if (comboBoxColumn != null)
        //    {
        //        //BAN: if the column does not contain any items or value items, and the value has been changed, the ComboSelect event should not be raised
        //        if (ComboSelect != null && !disableEvents && ((comboBoxColumn.ValueItems() != null && comboBoxColumn.ValueItems().Count != 0) || comboBoxColumn.Items.Count != 0))
        //        {
        //            ComboSelect(this, EventArgs.Empty);
        //        }
        //    }

        //    if (this.IsInvertedOrFormViewGenerated() && !disableEvents)
        //    {
        //        RefreshInvertedData();
        //    }

        //    if (this.CurrentCell != null)
        //    {
        //        SetModified(this.Columns[this.CurrentCell.ColumnIndex], true);

        //        if (Array != null && Array.Value != null && !inReBind && Array.Value.GetLength(0) > e.RowIndex && Array.Value.GetLength(1) > e.ColumnIndex)
        //        {
        //            Array.Value[e.RowIndex, e.ColumnIndex] = this.Columns[e.ColumnIndex].Value();
        //        }
        //    }
        //    if (this.disableEvents)
        //    {
        //        return;
        //    }
        //    base.OnCellValueChanged(e, blnClientSource);
        //}

        //protected override void OnCellFormatting(DataGridViewCellFormattingEventArgs e)
        //{
        //    //CHE: exit OnCellFormatting when disableEvents e.g. fix for these cases:
        //    //  1.  when calling Columns.Clear the Cell Formatting is triggered with RowIndex 0 and ColIndex 0 and base.Columns.Count is still > 0
        //    //  2.  CNA: CellFormatting is triggered with RowIndex 0 and ColIndex 0 when DataSource is setting, but at this point DataSource is not yet set, and using the old one causes conflicts.
        //    if (!IsValidItem(e.RowIndex, e.ColumnIndex) || this.Visible == false || disableEvents || disableCellFormatting)
        //    {
        //        return;
        //    }

        //    //CHE: avoid reentrant call, do not raise again CellFormatting if is already in progress 
        //    //(e.g. setting row ReadOnly in FetchRowStyle will enter in GetFormattedValue for DataGridViewCustomComboBoxCell and that should not call again grid.RaiseCellFormatting)
        //    cellFormattingInProgress = true;

        //    DataGridViewColumn col = this.Columns[e.ColumnIndex];

        //    FetchRowStyleFetchCellStyle(this.dataTable, col, e);

        //    //CHE: do not call for last row with DataBoundItem null
        //    if (UnboundColumnFetch != null && this.DataMode == DataModeType.dbgBound && !col.IsDataBound && this.Rows[e.RowIndex].DataBoundItem != null)
        //    {
        //        UnboundColumnFetch(this, e);
        //    }

        //    if (this.Columns[e.ColumnIndex].NumberFormat() == "FormatText Event")
        //    {
        //        base.OnCellFormatting(e);
        //    }

        //    //added here from TranslateValues
        //    DataGridViewRow row = this.Rows[e.RowIndex];
        //    FCValueItems valueItemsList = col.ValueItems();
        //    if (valueItemsList.Count > 0 && valueItemsList.Translate && e.Value != null)
        //    {
        //        object displayValue = col.ValueItems()[this[col.Index, row.Index].Value];
        //        //BAN: if the value is null and the column is an int column, get the display value for the 0 value item
        //        if (displayValue == null && this.dataTable != null)
        //        {
        //            if (this.dataTable.Columns[col.DataPropertyName].DataType == typeof(int))
        //            {
        //                displayValue = valueItemsList[0].DisplayMember;
        //            }
        //        }
        //        if (displayValue != null)
        //        {
        //            if (this.dataTable != null)
        //            {
        //                DataColumn dataColumn = this.dataTable.Columns[col.DataPropertyName];
        //                if (dataColumn != null && dataColumn.DataType == typeof(System.String) && dataColumn.MaxLength < displayValue.ToString().Length)
        //                {
        //                    dataColumn.MaxLength = displayValue.ToString().Length;
        //                }
        //            }
        //            if (displayValue.GetType() == typeof(Gizmox.WebGUI.Common.Resources.ImageResourceHandle))
        //            {
        //                DataGridViewCell currentCell = this[col.Index, row.Index];
        //                if (currentCell.Panel.Controls.Count == 0)
        //                {
        //                    Label imageLabel = new Label();
        //                    imageLabel.Image = displayValue as Gizmox.WebGUI.Common.Resources.ImageResourceHandle;
        //                    imageLabel.Dock = DockStyle.Fill;
        //                    currentCell.Panel.Controls.Add(imageLabel);
        //                    currentCell.Colspan = 1;
        //                    currentCell.Rowspan = 1;
        //                }
        //            }
        //            else
        //            {
        //                e.Value = displayValue;
        //            }
        //        }
        //    }
        //    //CHE: avoid reentrant call, do not raise again CellFormatting if is already in progress 
        //    //(e.g. setting row ReadOnly in FetchRowStyle will enter in GetFormattedValue for DataGridViewCustomComboBoxCell and that should not call again grid.RaiseCellFormatting)
        //    cellFormattingInProgress = false;
        //}

        //CHE: create function for FetchRowStyle/FetchCellStyle to be called in CellFormatting and SelectionChange
        private void FetchRowStyleFetchCellStyle(int rowIndex)
        {
            //SBE: bookmark value depends on DataMode. If grid source is an Array(DataMode == dbgUnboundSt) then bookmark is 0 based, otherwise is 1 based
            int bookmark;
            if (this.DataMode == DataMode.Storage || this.DataMode == DataMode.Unbound)
            {
                bookmark = rowIndex;
            }
            else
            {
                bookmark = rowIndex + 1;
            }
            //SBE: Call FetchRowStyle with first cell of the row, get the style applied, and apply to other cells from row
            if (FetchRowStyleEvent != null && this.FetchRowStyle)
            {               
                int firstVisibleColumn = -1;
                for (int i = 0; i < this.Columns.Count; i++)
                {
                    if (this.Columns[i].Visible)
                    {
                        firstVisibleColumn = i;
                        break;
                    }
                }
				int splitIndex = -1;
				foreach (FCSplit split in Splits)
				{
					++splitIndex;
					FetchRowStyleEventArgs args = new FetchRowStyleEventArgs(splitIndex, bookmark, Rows[rowIndex].DefaultCellStyle);
					//args.Bookmark = bookmark;
					//if (e.ColumnIndex == firstVisibleColumn)
					//{
					//    FetchRowStyleEvent(this, args);
					//    appliedRowStyle = e.CellStyle;
					//    isRowStyleApplied = false;
					//    if (this.Columns.Count == 1)
					//    {
					//        isRowStyleApplied = true;
					//    }
					//    for (int i = 0; i < this.Columns.Count; i++)
					//    {
					//        if (i != e.ColumnIndex)
					//        {
					//            CopyFromStyle(appliedRowStyle, this[i, e.RowIndex].Style);
					//        }
					//    }
					//}
					//else if (!isRowStyleApplied)
					//{
					//    CopyFromStyle(appliedRowStyle, e.CellStyle);

					//    if (e.ColumnIndex == this.Columns.Count - 1)
					//    {
					//        isRowStyleApplied = true;
					//    }
					//}
					//else
					//{
					//    FetchRowStyleEvent(this, args);
					//}
					if (FetchRowStyleEvent != null)
					{
						FetchRowStyleEvent(this, args);
					}
				}
            }

			if (FetchCellStyle != null)
            {
				foreach (DataGridViewColumn column in Columns)
				{
					if (column.FetchStyle() != FetchCellType.dbgFetchCellStyleNone)
					{
						int splitIndex = -1;
						foreach (FCSplit split in Splits)
						{
							++splitIndex;
							FetchCellStyleEventArgs args = new FetchCellStyleEventArgs(CellStyleFlag.AllCells, splitIndex, bookmark, column.Index + 1, this[column.Index, bookmark].Style);
							FetchCellStyle(this, args);
							if (lastRow != null && lastRow > -1 && lastRow != bookmark)
							{
								args = new FetchCellStyleEventArgs(CellStyleFlag.AllCells, splitIndex, (int)lastRow, column.Index + 1, this[column.Index, (int)lastRow].Style);
								FetchCellStyle(this, args);
							}
						}
					}
				}
            }
        }

        //CHE: copy style properties that were set in FetchRowStyle event
        //(cannot assign entire style object because it generates problems with format)
        private void CopyFromStyle(DataGridViewCellStyle sourceStyle, DataGridViewCellStyle targetStyle)
        {
            if (targetStyle != null && sourceStyle != null)
            {
                targetStyle.BackColor = sourceStyle.BackColor;
                targetStyle.ForeColor = sourceStyle.ForeColor;
                targetStyle.Font = sourceStyle.Font;
            }
        }

        protected override void OnUserDeletingRow(DataGridViewRowCancelEventArgs e)
        {
            ChangePositionOnDataTable();

            base.OnUserDeletingRow(e);
        }

        //CNA
        /// <summary>
        /// Returns the navigation point to the next Navigable cell (not in a read only column) relative to the current cell.
        /// Skips read only columns
        /// </summary>
        /// <returns></returns>
        protected Point GetNextNavigableCellRelativeCoords()
        {
            Point cellNavigation = default(Point);

            if (this.CurrentCell != null)
            {
                DataGridViewColumn[] sortedCols = GetColumnsByDisplayIndex();
                int initialColIdx = this.Columns[this.CurrentCell.ColumnIndex].DisplayIndex;
                int colIdx = initialColIdx;
                int rowIdx = this.CurrentCell.RowIndex;

                ++colIdx;
                ++cellNavigation.X;
                while (rowIdx < this.Rows.Count && colIdx < sortedCols.Count() && this.Columns[GetColumnIndexByDisplayIndex(colIdx)].ReadOnly)
                {
                    while (colIdx < sortedCols.Count())
                    {
                        DataGridViewColumn column = this.Columns[GetColumnIndexByDisplayIndex(colIdx)];
                        if (column.Visible && !column.ReadOnly)
                        {
                            return ValidNextNavigableCell(cellNavigation);
                        }
                        else
                        {
                            ++colIdx;
                            ++cellNavigation.X;
                        }
                    }
                    ++rowIdx;
                    ++cellNavigation.Y;
                    colIdx = 0;
                    cellNavigation.X = -initialColIdx;
                }
            }

            return ValidNextNavigableCell(cellNavigation);
        }

        private Point ValidNextNavigableCell(Point cellNavigation)
        {
            //CHE: check for last row, go with default values if there is no other right cells to jump on
            if (cellNavigation.Y == 1 && (this.CurrentCell.RowIndex == this.Rows.Count - 1))
            {
                //reset to defaults
                cellNavigation.X = 1;
                cellNavigation.Y = 0;
            }
            return cellNavigation;
        }

        //CNA
        /// <summary>
        /// Returns the navigation point to the previous Navigable cell (not in a read only column) relative to the current cell.
        /// Skips read only columns
        /// </summary>
        /// <returns></returns>
        protected Point GetPreviousNavigableCellRelativeCoords()
        {
            Point cellNavigation = default(Point);

            if (this.CurrentCell != null)
            {
                DataGridViewColumn[] sortedCols = GetColumnsByDisplayIndex();
                int initialColIdx = this.Columns[this.CurrentCell.ColumnIndex].DisplayIndex;
                int colIdx = initialColIdx;
                int rowIdx = this.CurrentCell.RowIndex;

                --colIdx;
                --cellNavigation.X;
                while (rowIdx >= 0 && colIdx >= 0 && this.Columns[GetColumnIndexByDisplayIndex(colIdx)].ReadOnly)
                {
                    while (colIdx >= 0)
                    {
                        DataGridViewColumn column = this.Columns[GetColumnIndexByDisplayIndex(colIdx)];
                        if (column.Visible && !column.ReadOnly)
                        {
                            return ValidPreviousNavigableCell(cellNavigation);
                        }
                        else
                        {
                            --colIdx;
                            --cellNavigation.X;
                        }
                    }
                    --rowIdx;
                    --cellNavigation.Y;
                    colIdx = this.Columns.Count - 1;
                    cellNavigation.X = this.Columns.Count - 1 - initialColIdx;
                }
            }

            return ValidPreviousNavigableCell(cellNavigation);
        }

        private Point ValidPreviousNavigableCell(Point cellNavigation)
        {
            //CHE: check for first row, go with default values if there is no other right cells to jump on
            if (cellNavigation.Y == -1 && (this.CurrentCell.RowIndex == 0))
            {
                //reset to defaults
                cellNavigation.X = -1;
                cellNavigation.Y = 0;
            }
            return cellNavigation;
        }

        protected override void OnCellBeginEdit(System.Windows.Forms.DataGridViewCellCancelEventArgs e)
        {
            //HACK:IPI: with VWG QA 42 for "some" reason, the CurrentCell is not correctly set in OnCellBeginEdit and in OnCellClick events
            SynchronizeCurrentCell(e.RowIndex, e.ColumnIndex);

            //IPI: currentCellOldValue is set in OnDropDownClosed
            if (!this.IsDropDownClosing)
            {
                if (this.CurrentCell != null)
                {
                    currentCellOldValue = this.CurrentCell.Value;
                }
            }

            if (!this.disableEvents)
            {
                isCancelEdit = false;
                //CHE: if value is retrieved from CellBeginEdit do not move to penultimate row, should stay on current new added
                IsCellBeginEdit = true;
                //IPI: preserve cell value before raising event
                object oldCellValue = currentCellOldValue;
                //CHE: set MaskInputLength on javascript
                //IFCForm ifrm = this.Form as IFCForm;
                //if (ifrm != null)
                //{
                //    DataGridViewTextBoxColumn textBoxColumn = this.Columns[e.ColumnIndex] as DataGridViewTextBoxColumn;
                //    if (textBoxColumn != null && textBoxColumn.MaxInputLength > 0)
                //    {
                //        ifrm.InvokeMethodRegisteredComponent("SetProperty", "MaxInputLength", textBoxColumn.MaxInputLength, this.ID, (e.RowIndex * this.Columns.Count) + e.ColumnIndex);
                //    }
                //    int dataWidth = this.Columns[e.ColumnIndex].DataWidth();
                //    if (dataWidth > 0)
                //    {
                //        ifrm.InvokeMethodRegisteredComponent("SetProperty", "DataWidth", dataWidth, this.ID, (e.RowIndex * this.Columns.Count) + e.ColumnIndex);
                //        FCDataGridViewTextBoxColumn fcTextBoxColumn = this.Columns[e.ColumnIndex] as FCDataGridViewTextBoxColumn;
                //        if (fcTextBoxColumn != null)
                //        {
                //            fcTextBoxColumn.ClientKeyPressParams = new string[] { fcTextBoxColumn.DataWidth().ToString() };
                //        }
                //    }
                //}

                base.OnCellBeginEdit(e);

                //CHE: set EditMask on javascript
                //(after calling base because there was a case when EditMask was set in VB6 in FetchRowStyle and it was moved as workaround to CellBeginEdit to have the correct value)
                //if (ifrm != null)
                //{
                //    string editMask = this.Columns[e.ColumnIndex].EditMask();
                //    if (!string.IsNullOrEmpty(editMask))
                //    {
                //        ifrm.InvokeMethodRegisteredComponent("SetProperty", "EditMask", editMask, this.ID, (e.RowIndex * this.Columns.Count) + e.ColumnIndex);
                //    }
                //}

                IsCellBeginEdit = false;
                ColEditEvent(e);
                isCancelEdit = e.Cancel;
                if (!isCancelEdit)
                {
                    //IPI: BeforeInsert is raised when user starts editing a new row in the grid
                    if (this.Rows[e.RowIndex].IsNewRow)
                    {
                        RaiseBeforeInsert(this.Rows[e.RowIndex]);
                    }
                }
                //IPI: when BeforeColEdit is cancelled, need to set focus on previous checked RadioButton (InvertedGrid)
                else
                {
                    this.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = oldCellValue;

                    string sOldCellValue = Convert.ToString(oldCellValue);

                    if (this.IsInvertedOrFormViewGenerated())
                    {
                        foreach (Control ctrl in this.InvertedView.Controls)
                        {
                            InvertedData data = ctrl.Tag as InvertedData;
                            if (data != null && data.Col.Index == e.ColumnIndex && data.Row == e.RowIndex)
                            {
                                if (ctrl.GetType() == typeof(Panel))
                                {
                                    foreach (Control c in ctrl.Controls)
                                    {
                                        if (c.GetType() == typeof(RadioButton))
                                        {
                                            RadioButton rd = c as RadioButton;
                                            InvertedData rdInvertedData = rd.Tag as InvertedData;
                                            if (rd.Text == sOldCellValue || Convert.ToString(rdInvertedData.Value) == sOldCellValue)
                                            {
                                                rd.Focus();
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }

        protected override void OnLostFocus(EventArgs e)
        {
            base.OnLostFocus(e);
            SetAlignment();
            PostEvents();
        }

        protected override void OnColumnHeaderMouseClick(DataGridViewCellMouseEventArgs e)
        {
            disableCellFormatting = true;
            base.OnColumnHeaderMouseClick(e);

            RaiseHeadClick(e);

            disableCellFormatting = false;
        }

        protected override void OnColumnHeaderMouseDoubleClick(DataGridViewCellMouseEventArgs e)
        {
            disableCellFormatting = true;
            base.OnColumnHeaderMouseDoubleClick(e);
            disableCellFormatting = false;
        }

        #endregion

        #region Private Methods
		private void SetDataSource(object value)
		{
			this.dataTable = value as DataTable;
			FCData data = value as FCData;
			if (data != null)
			{
				this.dataTable = data.Recordset;
			}
			disableEvents = true;
			//CNA
			dataSourceIsSetting = true;
			if (this.IsCurrentCellInEditMode)
			{
				try
				{
					this.EndEdit();
					//CHE: modify also CurrentCellDirty state otherwise when setting DataSource to null this error occurs:
					//Operation did not succeed because the program cannot commit or quit a cell value change.
					if (IsCurrentCellDirty)
					{
						NotifyCurrentCellDirty(false);
					}
				}
				catch (Exception ex) { }
			}

			//SBE: in VB6, when DataSource is set, DataTable fields are initialized with default value
			if (this.dataTable != null)
			{
				bool isValueChanged = false;
				isValueChanged = this.dataTable.SetDataTableEmptyValues();
				if (isValueChanged)
				{
					this.dataTable.Update();
				}
			}

			//CHE: reset selBookmarks
			if (this.selBookmarks != null)
			{
				this.selBookmarks.Clear();
			}
			//CHE: when AutoGenerateColumns is set and HoldFields was not called then all columns must be removed to be recreated according to new DataSource
			if (value != null && originalColumns == null && this.AutoGenerateColumns)
			{
				base.DataSource = null;
				this.ClearFields();
			}

			if (data != null)
			{
				base.DataSource = data.Recordset;
                data.ParentGrid = this;
			}
			else
			{
				base.DataSource = value;
			}

			//if (data != null && data.Recordset != null)
			//{
			//	data.Recordset.ParentGrid = this;
			//}

			if (this.dataTable != null)
			{
				//CHE: set bookmark to first row if it was on eof or bof before setting datasource of grid
				if (this.dataTable != null && this.dataTable.RecordCount() > 0 && (this.dataTable.BOF() || this.dataTable.EOF()))
				{
					this.dataTable.MoveFirst();
				}

				if (IsInvertedOrFormView())
				{
					//when Filter is set in DataTableExtension the DefaultView is changed and values for controls must be refilled
					DataView dv = this.dataTable.DefaultView;
					if (dv != null)
					{
						dv.ListChanged -= new ListChangedEventHandler(DefaultView_ListChanged);
						dv.ListChanged += new ListChangedEventHandler(DefaultView_ListChanged);
					}

					this.RedrawInverted();
				}
			}
			//CNA
			dataSourceIsSetting = false;
			disableEvents = false;
		}

		private void Data_Refreshed(object sender, EventArgs e)
		{
			if (initialized)
			{
				SetDataSource(sender);
			}
		}

        private bool IsInvertedOrFormViewGenerated()
        {
            return (IsInvertedOrFormView() && this.InvertedView != null);
        }

        /// <summary>
        /// check for valid item at row index and col index
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <returns></returns>
        private bool IsValidItem(int rowIndex, int columnIndex)
        {
            if (rowIndex < 0 || columnIndex < 0)
            {
                return false;
            }
            return true;
        }

        private void DisposeInvertedControl(Control control)
        {
            if (control == null)
            {
                return;
            }

            //CHE: dispose control with all subcontrols
            List<Control> originalControlList = control.GetAllControls();
            Control ctrl = null;
            for (int i = 0; i < originalControlList.Count; i++)
            {
                ctrl = originalControlList[i];
                ctrl.Dispose();
                ctrl = null;
            }

            originalControlList.Clear();
            originalControlList = null;
        }

        private int GetInvertedRows()
        {
            int rows = 1;
            if (this.DataView == DataViewType.dbgInvertedView)
            {
                rows = GetVisibleRows();
            }
            return rows;
        }

        private void GenerateInvertedGridColumn(DataGridViewColumn col, ref int y, ref int controlX, ref int ctrlWidth, ref int ctrlTabIndex, int labelX, int headerWidth, Font ctrlFont, Font ctrlFontBold, int rows, Panel invertedPanel, bool wasFocused)
        {
            if (!col.Visible)
            {
                return;
            }

            Control ctrl = null;
            ComboBox cmb = null;
            CheckBox chk = null;
            Label lbl = null;
            TextBox tb = null;

            // even if header is not visible, it must be generated in order to be added to ColumnControls collection
            lbl = new Label();
            lbl.Location = new Point(labelX, y);
            lbl.Width = headerWidth;
            //add : as in VB6
            lbl.Text = col.HeaderText + ((col.HeaderText.Length > 0) && (RightStr(col.HeaderText, 1) != ":") ? ":" : "");
            lbl.BackColor = col.HeadBackColor();
            lbl.Height = Convert.ToInt32(RowHeight);
            lbl.Font = col.HeadFont().Bold ? ctrlFontBold : ctrlFont;
            invertedPanel.Controls.Add(lbl);

            Type colType = GetColumnType(col);

            int rowX = controlX;

            // if ViewColumnWidth is 0, must use the width of the column
            if (this.ViewColumnWidth == 0)
            {
                ctrlWidth = col.Width;
            }
            // if the table has only 1 row, the column width cannot extend the table's width
            if (rows == 1)
            {
                if (ctrlWidth + headerWidth > this.Width)
                {
                    ctrlWidth = this.Width - headerWidth;
                }
            }

            List<Control> controls = new List<Control>();

            for (int counter = 0; counter < rows; counter++)
            {
                #region Generate RadioButton Column
                //draw with radio buttons
                if (colType == typeof(DataGridViewRadioColumn))
                {
                    DataGridViewRadioColumn radioCol = col as DataGridViewRadioColumn;
                    int posx = 0, posy = 0;

                    Panel rdPanel = new Panel();
                    rdPanel.BackColor = this.DeadAreaBackColor;

                    foreach (FCValueItem item in col.ValueItems())
                    {
                        RadioButton rd = new RadioButton();
                        // align radio button with the rest of controls in the grid
                        //posx += 1;
                        rd.Location = new Point(posx, posy);
                        rd.AutoSize = true;
                        rd.Text = item.DisplayValue.ToString();
                        //posx += (int)rd.TextWidth(rd.Text) + 40;
                        //rd.Height = RowHeight - 1;
                        rd.Tag = new InvertedData(col, counter, rows, item.Value.ToString().Trim());
                        rdPanel.Controls.Add(rd);
                        controls.Add(rd);

                        rd.CheckedChanged += new EventHandler(ctrl_CellValueChanged);
                        rd.Leave += new EventHandler(ctrl_Leave);
                        rd.Enter += new EventHandler(ctrl_Enter);
                        rd.Click += new EventHandler(rd_Click);

                        //SBE: set Controls Enabled property
                        rd.Enabled = this.Enabled;
                    }

                    rdPanel.Size = new Size(posx + 10 * col.ValueItems().Count, Convert.ToInt32(RowHeight));
                    ctrl = rdPanel;
                }
                #endregion

                #region Generate ComboBox Column
                if (colType == typeof(DataGridViewComboBoxColumn))
                {
                    //cmb = new FCComboBox(col);
                    FillInvertedComboBox(col, cmb);

                    ctrl = cmb;

                    //CHE: do not attach TextChanged event unless combo is editable, otherwise use SelectedIndexChanged
                    //(TextChanged is raised from javascript and it will not let you select from items)
                    if (!col.DropDownList())
                    {
                        cmb.TextChanged += new EventHandler(ctrl_CellValueChanged);
                    }
                    //CNA: Do not attach ctrl_CellValueChanged to both events (TextChanged and SelectedIndexChanged) 
                    //to prevent handling it twice
                    else
                    {
                        cmb.SelectedIndexChanged += new EventHandler(ctrl_CellValueChanged);
                    }
                    //IPI: when value is selected from the DropDownList, TextChanged and SelectedIndexChanged are not raised, only SelectedValueChanged is raised
                    cmb.SelectedValueChanged += new EventHandler(ctrl_CellValueChanged);

                    cmb.Leave += new EventHandler(ctrl_Leave);
                    cmb.Enter += new EventHandler(ctrl_Enter);
                    cmb.KeyPress += new KeyPressEventHandler(cmb_KeyPress);

                    //SBE: set Controls Enabled property
                    cmb.Enabled = this.Enabled;
                }
                #endregion

                #region Generate TextBox Column
                if (colType == typeof(DataGridViewTextBoxColumn))
                {
                    DataGridViewTextBoxColumn tbCol = col as DataGridViewTextBoxColumn;
                    if (string.IsNullOrEmpty(tbCol.NumberFormat()))
                    {
                        tb = new TextBox();
                    }
                    else
                    {
                        FCTextBox numericTextbox = new FCTextBox();
                        //numericTextbox.NumberFormat = tbCol.NumberFormat();
                        ////APE: Set data type to EqualOnly to be able to do custom validation
                        //if (numericTextbox.NumberFormat == "EqualOnly")
                        //{
                        //    numericTextbox.DataType = DataType.EqualOnly;
                        //}
                        //else
                        //{
                        //    numericTextbox.DataType = DataType.Decimal;
                        //}
                        tb = numericTextbox;
                    }

                    //SBE: if color name is Black, when control is Readonly, color is displayed as gray. Updated color to ff000000 (black)
                    if (tb.ForeColor == System.Drawing.Color.Black)
                    {
                        tb.ForeColor = System.Drawing.Color.FromArgb(System.Drawing.Color.Black.ToArgb());
                    }
                    tb.ReadOnly = col.ReadOnly;
                    tb.TabStop = !tb.ReadOnly;
                    //CHE: set max length
                    SetMaxLength(tbCol, tb);
                    ctrl = tb;

                    tb.KeyDown += new KeyEventHandler(tb_KeyDown);
                    tb.KeyUp += new KeyEventHandler(tb_KeyUp);
                    tb.LostFocus += new EventHandler(tb_LostFocus);
                    tb.TextChanged += new EventHandler(ctrl_CellValueChanged);
                    tb.GotFocus += new EventHandler(tb_GotFocus);
                    tb.Enter += new EventHandler(ctrl_Enter);
                    tb.Leave += new EventHandler(ctrl_Leave);
                    tb.KeyPress += new KeyPressEventHandler(tb_KeyPress);

                    //SBE: set Controls Enabled property
                    tb.Enabled = this.Enabled;
                }
                #endregion

                #region Generate CheckBox Column
                if (colType == typeof(DataGridViewCheckBoxColumn))
                {
                    DataGridViewCheckBoxColumn chkCol = col as DataGridViewCheckBoxColumn;
                    chk = new CheckBox();
                    chk.CheckAlign = ContentAlignment.MiddleLeft;
                    ctrl = chk;

                    chk.CheckedChanged += new EventHandler(ctrl_CellValueChanged);
                    chk.Leave += new EventHandler(ctrl_Leave);
                    chk.Enter += new EventHandler(ctrl_Enter);
                    chk.Click += new EventHandler(chk_Click);

                    //SBE: set Controls Enabled property
                    chk.Enabled = this.Enabled;
                }
                #endregion

                #region Generate Custom ComboBox Column
                if (colType == typeof(DataGridViewCustomComboBoxColumn))
                {
                    DataGridViewCustomComboBoxColumn cmbCol = col as DataGridViewCustomComboBoxColumn;
                    FCCustomComboBox cCmb = new FCCustomComboBox(cmbCol);
                    FillInvertedComboBox(col, cCmb);

                    cCmb.TDBDropDown = cmbCol.TDBDropDown;

                    ctrl = cCmb;

                    //CNA: attach textchanged event to editable custom combobox
                    //CHE: do not attach TextChanged event unless combo is editable, otherwise use SelectedIndexChanged
                    //(TextChanged is raised from javascript and it will not let you select from items)
                    if (!col.DropDownList())
                    {
                        cCmb.TextChanged += new EventHandler(ctrl_CellValueChanged);
                    }
                    //CNA: Do not attach ctrl_CellValueChanged to both events (TextChanged and SelectedIndexChanged) 
                    //to prevent handling it twice
                    else
                    {
                        cCmb.SelectedIndexChanged += new EventHandler(ctrl_CellValueChanged);
                    }

                    cCmb.KeyPress += new KeyPressEventHandler(cCmb_KeyPress);
                    cCmb.Leave += new EventHandler(ctrl_Leave);
                    cCmb.Enter += new EventHandler(ctrl_Enter);
                    //CHE: attach KeyDown event for custom combo when inverted
                    cCmb.KeyDown += new KeyEventHandler(tb_KeyDown);
                    //SBE: set Controls Enabled property
                    cCmb.Enabled = this.Enabled;
                }
                #endregion

                if (ctrl != null)
                {
                    ctrl.Location = new Point(controlX, y);
                    ctrl.Tag = new InvertedData(col, counter, rows);
                    ctrl.Width = ctrlWidth;
                    ctrl.BackColor = col.DefaultCellStyle.BackColor;
                    ctrl.Height = Convert.ToInt32(RowHeight) - 1;
                    ctrl.Font = ctrlFont;
                    //CHE: for textbox readonly property was already set, setting enabled to false will modify backcolor and textbox is "hidden"
                    if (!(ctrl is TextBox || ctrl is FCTextBox))
                    {
                        //SBE: set Controls Enabled property
                        if (this.Enabled)
                        {
                            ctrl.Enabled = !col.ReadOnly;
                            ctrl.TabStop = ctrl.Enabled;
                        }
                    }
                    ctrl.TabIndex = ctrlTabIndex;
                    ++ctrlTabIndex;

                    //CHE : set DropDownStyle
                    ComboBox comboBox = ctrl as ComboBox;
                    DataGridViewComboBoxColumn dataGridViewComboBoxColumn = col as DataGridViewComboBoxColumn;
                    if (comboBox != null && dataGridViewComboBoxColumn != null)
                    {
                        //comboBox.DropDownStyle = col.DropDownList() ? ComboBoxStyle.DropDownList : dataGridViewComboBoxColumn.DropDownStyle;
                    }
                    //SBE: prevent ctrl_CellValueChanged to be raised when controls are added
                    this.disableEvents = true;
                    invertedPanel.Controls.Add(ctrl);
                    this.disableEvents = false;
                    controls.Add(ctrl);
                    //APE: keep focus on current cell; CHE: check if it was focused before
                    if (this.CurrentCell != null && col == this.Columns[this.CurrentCell.ColumnIndex] && counter == this.CurrentCell.RowIndex && wasFocused)
                    {
                        generateInvertedGridInProgress = true;
                        ctrl.Focus();
                        generateInvertedGridInProgress = false;
                    }
                }

                if (rows > 1)
                {
                    controlX += ctrlWidth + 1;
                }
            }

            y += Convert.ToInt32(RowHeight);
            KeyValuePair<Label, List<Control>> colControls = new KeyValuePair<Label, List<Control>>(lbl, controls);
            col.ColumnControls(colControls);
            controlX = rowX;
        }

        private void FillInvertedComboBox(DataGridViewColumn col, ComboBox cmb)
        {
            if (col == null || cmb == null)
            {
                return;
            }

            DataGridViewComboBoxColumn cmbCol = col as DataGridViewComboBoxColumn;
            cmb.DataSource = null;
            cmb.Items.Clear();
            if (cmbCol != null)
            {
                //cmb.Items.AddRange(cmbCol.Items);
            }
            else if (col.ValueItems().Count > 0)
            {
                cmb.DataSource = col.ValueItems().ToList();
                cmb.ValueMember = "Value";
                cmb.DisplayMember = "DisplayValue";
            }
        }

        private Type GetColumnType(DataGridViewColumn col)
        {
            Type colType = col.GetType();

            #region determine col type based on presentation
            FCValueItems vi = this.Columns[col.DataPropertyName].ValueItems();
            //CNA: CustomCombobox Columns should not use Presentation
            if (colType != typeof(DataGridViewCustomComboBoxColumn))
            {
                if (vi != null)
                {
                    switch (vi.Presentation())
                    {
                        case Presentation.dbgCheckBox:
                            {
                                colType = typeof(DataGridViewCheckBoxColumn);
                                break;
                            }
                        case Presentation.dbgComboBox:
                            {
                                colType = typeof(DataGridViewComboBoxColumn);
                                break;
                            }
                        case Presentation.dbgNormal:
                            {
                                //colType = typeof(DataGridViewTextBoxColumn);
                                break;
                            }
                        case Presentation.dbgRadioButton:
                            {
                                colType = typeof(DataGridViewRadioColumn);
                                break;
                            }
                        case Presentation.dbgSortedComboBox:
                            {
                                colType = typeof(DataGridViewComboBoxColumn);
                                break;
                            }
                    }
                }
            }
            #endregion

            #region determine col type based on data type
            if (this.dataTable != null && this.dataTable.Columns[col.DataPropertyName] != null && this.dataTable.Columns[col.DataPropertyName].DataType == typeof(bool))
            {
                colType = typeof(DataGridViewCheckBoxColumn);
            }

            if (colType == typeof(DataGridViewTextBoxColumn))
            {
                if (col.ValueItems().Count > 0)
                {
                    colType = typeof(DataGridViewComboBoxColumn);
                }
            }
            #endregion

            return colType;
        }

        //HACK:IPI: with VWG QA 42 for "some" reason, the CurrentCell is not correctly set in OnCellBeginEdit and in OnCellClick events
        private void SynchronizeCurrentCell(int rowIndex, int columnIndex)
        {
            if (this.CurrentCell != null && IsValidItem(rowIndex, columnIndex))
            {
                if (this.CurrentCell.RowIndex != rowIndex || this.CurrentCell.ColumnIndex != columnIndex)
                {
                    bool initialDisableEvents = disableEvents;
                    disableEvents = true;
                    this.Row = rowIndex;
                    this.Col = columnIndex;
                    //CHE: reset disableEvents to let OnCurrentCellChanged to be executed, for cell click on * row when multipaging the OnCurrentCellChanged is not coming
                    disableEvents = initialDisableEvents;
                    this.CurrentCell = this[columnIndex, rowIndex];
                    currentCellOldValue = this.CurrentCell.Value;
                }
            }
        }

        //CHE: on user interaction SelBookmarks must be updated; if current cell is changed programmatically the SelBookmarks collection must be preserved
        private void SynchronizeSelBookmarks()
        {
            this.disableEvents = true;
            this.selBookmarks.Clear(false);
            foreach (DataGridViewRow selectedRow in this.SelectedRows)
            {
                this.selBookmarks.Add(selectedRow.Index + 1, true);
            }
            this.disableEvents = false;
        }

        //CHE: silent error for editable ComboBoxColumn when entered value is not in drop down list (System.ArgumentException - DataGridViewComboBoxCell value is not valid)
        private void FCTrueDBGrid_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            DataGridViewComboBoxColumn comboColumn = this.Columns[e.ColumnIndex] as DataGridViewComboBoxColumn;
            if (comboColumn != null && !comboColumn.DropDownList())
            {
                bool inDropDownList = false;
                foreach (FCValueItem valueItem in this.Columns[e.ColumnIndex].ValueItems())
                {
                    if (Convert.ToString(this.Columns[e.ColumnIndex].Value()) == Convert.ToString(valueItem.Value))
                    {
                        inDropDownList = true;
                    }
                }
                if (!inDropDownList)
                {
                    e.ThrowException = false;
                    return;
                }
            }

            e.ThrowException = true;
        }

        private int FirstVisibleColumn()
        {
            int i = 0;
            while (i < this.Columns.Count && !this.Columns[i].Visible)
            {
                i++;
            }
            if (i < this.Columns.Count)
            {
                return i;
            }
            return -1;
        }

        private void SetPointAt(int colIndex, int rowIndex)
        {
            objClickedCoordinates = new Coordinates(colIndex, rowIndex);

            if (objClickedCoordinates.RowIndex < 0)
            {
                pointAt = fecherFoundation.PointAt.dbgAtColumnHeader;
            }
            else if (objClickedCoordinates.ColIndex < 0)
            {
                pointAt = fecherFoundation.PointAt.dbgAtRowSelect;
            }
            else
            {
                pointAt = fecherFoundation.PointAt.dbgAtDataArea;
            }
        }

        private void SetPointAt(int y)
        {
            if (objClickedCoordinates == null)
            {
                if (y <= this.GetCaptionHeight())
                {
                    pointAt = fecherFoundation.PointAt.dbgAtCaption;
                }
                else
                {
                    pointAt = fecherFoundation.PointAt.dbgAtDataArea;
                }
            }

            objClickedCoordinates = null;
        }

        private int GetColumnIndexByDisplayIndex(int displayIndex)
        {
            foreach (DataGridViewColumn column in this.Columns)
            {
                if (column.DisplayIndex == displayIndex)
                {
                    return column.Index;
                }
            }
            return -1;
        }

        //Added the CellClick event handler because otherwise the override of the OnCellClick will not be triggered on every cell click
        private void FCTrueDBGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void FCTrueDBGrid_ColumnDividerDoubleClick(object sender, DataGridViewColumnDividerDoubleClickEventArgs e)
        {
            disableCellFormatting = true;
            if (e.ColumnIndex != -1)
            {
                this.Columns[e.ColumnIndex].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                this.Columns[e.ColumnIndex].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                this.Update();
            }
            disableCellFormatting = false;
        }

		private void FCTrueDBGrid_ColumnRemoved(object sender, DataGridViewColumnEventArgs e)
		{
			// IPI - mandatory to explicitly remove column from "DataGridViewColumnExtension.mobjDefinitions"
			// DGVColumn.Dispose() is not raising "Disposed" event for the DGVColumn
			if (preventReleasingExtension && originalColumns.Contains(e.Column))
			{
				return;
			}
			e.Column.ReleaseColumn();
		}

		private void FCTDBGrid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
		{
			FetchRowStyleFetchCellStyle(e.RowIndex);
			lastRow = e.RowIndex;
		}

		private void FCTDBGrid_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
			lastRow = null;
            (base.DataSource as FCRecordset)?.Update();
        }

        private void DefaultView_ListChanged(object sender, ListChangedEventArgs e)
        {
            //BAN: if the data is being refreshed do not call again RefreshInvertedData from ListChanged 
            //because it will enter in an infinite loop
            if (IsInvertedOrFormViewGenerated() && !this.DisableEvents)
            {
                this.RefreshInvertedData();
            }
        }

        private void RaiseAfterUpdate()
        {
            //IPI: after the row has been validated, need to reset the dataChanged property for the next CurrentRow
            dataChanged = false;
            if (this.AfterUpdate != null)
            {
                this.AfterUpdate(this, EventArgs.Empty);
            }
        }

        private void RaiseAfterInsert()
        {
            if (this.AfterInsert != null)
            {
                this.AfterInsert(this, EventArgs.Empty);
            }
        }

        private bool RaiseBeforeUpdate(DataGridViewRow row)
        {
            if (this.BeforeUpdate != null)
            {
                DataGridViewRowCancelEventArgs ea = new DataGridViewRowCancelEventArgs(row);
                this.BeforeUpdate(this, ea);
                return ea.Cancel;
            }

            return false;
        }

        private bool RaiseBeforeInsert(DataGridViewRow row)
        {
            if (this.BeforeInsert != null)
            {
                DataGridViewRowCancelEventArgs ea = new DataGridViewRowCancelEventArgs(row);
                this.BeforeInsert(this, ea);
                return ea.Cancel;
            }

            return false;
        }

        private void RaiseCellValidated(object sender, DataGridViewCellEventArgs e)
        {
            //DataGridViewCellEventHandler cellValidatedEventHandler = (DataGridViewCellEventHandler)this.GetHandler(OnCellValidated);
            //if (cellValidatedEventHandler != null)
            //{
            //    if (!this.DisableEvents)
            //    {
            //        //IPI: after a cell is validated, in AfterColUpdate all DBNull (where !col.AllowDBNull) values have to be already changed to empty values
            //        if (this.dataTable != null)
            //        {
            //            DataRowView dr = this.CurrentRow.DataBoundItem as DataRowView;
            //            if (dr != null)
            //            {
            //                this.dataTable.SetRowEmptyValues(dr.Row);
            //            }
            //        }
            //        if (this.IsInvertedOrFormView())
            //        {
            //            cellValidatedEventHandler(sender, new DataGridViewCellEventArgs(this.Col, this.Row));
            //        }
            //        else
            //        {
            //            //CHE: added condition, if you sort a column twice (e.g. first ascending and then descending) by column header click without changing anything in grid the AfterColUpdate should not be raised;
            //            //OnCellValidated is specifically called in DataGridView in CommitEdit function when (!this.IsCurrentCellInEditMode || !this.IsCurrentCellDirty)
            //            //IPI: when CustomComboBoxCell is closing, validation must also be raised
            //            if ((cellValidatingCellInEditMode && cellValidatingCellIsDirty) || this.IsDropDownClosing)
            //            {
            //                cellValidatedEventHandler(sender, e);
            //            }
            //        }
            //    }
            //}
        }

        private void FCTrueDBGrid_ColumnStateChanged(object sender, DataGridViewColumnStateChangedEventArgs e)
        {
            DataGridViewColumn col = e.Column;
            if (!IsInvertedOrFormViewGenerated())
            {
                return;
            }

            KeyValuePair<Label, List<Control>> colControls = col.ColumnControls();
            Label lbl = colControls.Key;
            if (colControls.Value != null)
            {
                foreach (Control ctrl in colControls.Value)
                {
                    if (e.StateChanged == DataGridViewElementStates.Visible)
                    {
                        ctrl.Visible = col.Visible;
                    }
                    else if (e.StateChanged == DataGridViewElementStates.ReadOnly)
                    {
                        ctrl.Enabled = !col.ReadOnly;
                    }
                }
            }
            //BAN: the table may not have any rows, in this case the label for the header should also change allong with the columns visibility
            if (e.StateChanged == DataGridViewElementStates.Visible)
            {
                lbl.Visible = col.Visible;
            }
        }

		private void FCTDBGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
		{
			if (disableEvents) return;

			var col = this.Columns[e.ColumnIndex];
			if (col != null)
			{
				if (col.NumberFormat() == "FormatText Event")
				{
					var cvea = new DataGridViewCellValueEventArgs(col.Index, this.CurrentRow.Index);
					cvea.Value = e.Value;
					RaiseFormatText(cvea);
					e.Value = cvea.Value;
					e.FormattingApplied = true;
				}

				FCValueItems valueItems = col.ValueItems();
				//If Translate is set, any column has to contain DisplayValue instead of Value
				if (valueItems != null && valueItems.Translate)
				{
					double cellValue = 0, itemValue = 0;
					bool isDouble = double.TryParse(e.Value?.ToString(), out cellValue); 
					foreach (FCValueItem item in valueItems)
					{
						if (isDouble)
						{
							if (double.TryParse(item.Value?.ToString(), out itemValue) && itemValue == cellValue)
							{
								e.Value = item.DisplayValue;
							}
						}
						else if ((e.Value != null && e.Value.Equals(item.Value)) || (e.Value == null && item.Value == null))
						{
							e.Value = item.DisplayValue;
							break;
						}
					}
				}
			}
		}

        //BAN;CNA: The CellValidating/CellValidated event is raised twice for each cell. Do not execute the first call because the 
        //formatted value is incorrect; save IsCurrentCellInEditMode state from first call used further in RaiseCellValidating
        private void FCTrueDBGrid_CellValidating(object sender, System.Windows.Forms.DataGridViewCellValidatingEventArgs e)
        {
            if (disableEvents)
            {
                return;
            }
            if (!this.IsInvertedOrFormView())
            {
                if (e.ColumnIndex == cellValidatingColIndex && e.RowIndex == cellValidatingRowIndex)
                {
                    if (needCellValidating)
                    {
                        needCellValidating = false;
                        //IPI: in case of FCDataGridViewTextBoxColumn, IsCurrentCellDirty will be set to 'true' only on second CellValidating event
                        // but for DataGridViewTextBoxColumn, cellValidatingCellIsDirty was already set to 'true' on first CellValidating
                        // and on second CellValidating, IsCurrentCellDirty will return 'false', so cellValidatingCellIsDirty should not be set
                        if (!cellValidatingCellIsDirty)
                        {
                            cellValidatingCellIsDirty = this.IsCurrentCellDirty;
                        }
                    }
                    else
                    {
                        needCellValidating = true;
                        //IPI: return when the same cell is getting validated the second time (the CurrentCell's value is not updated yet, only on the second CellValidating)
                        return;
                    }
                }
                else
                {
                    cellValidatingColIndex = e.ColumnIndex;
                    cellValidatingRowIndex = e.RowIndex;
                    needCellValidating = true;
                    cellValidatingCellInEditMode = this.IsCurrentCellInEditMode;
                    cellValidatingCellIsDirty = this.IsCurrentCellDirty;
                    return;
                }
            }


            if (!IsValidItem(e.RowIndex, e.ColumnIndex))
            {
                return;
            }

            object originalValue = e.FormattedValue;

            //for comboboxcolumn get corresponding valuemember
            DataGridViewComboBoxColumn col = this.Columns[e.ColumnIndex] as DataGridViewComboBoxColumn;
            if (col != null)
            {
                bool foundItem = false;
                string formattedValue = Convert.ToString(e.FormattedValue);
                foreach (FCValueItem item in col.Items)
                {
                    if (Convert.ToString(item.DisplayValue) == formattedValue)
                    {
                        originalValue = item.Value;
                        //APE;CNA: setting the cell value will trigger the CellValueChange event. Do not set the cell value if the 
                        //new value is the same as the old one; cell value was not set if there was a previous one, so do not check if cell value is empty
                        if (originalValue != null && Convert.ToString(this[e.ColumnIndex, e.RowIndex].Value) != Convert.ToString(originalValue))
                        {
                            this[e.ColumnIndex, e.RowIndex].Value = originalValue;
                        }
                        foundItem = true;
                        break;
                    }
                }
                if (!foundItem)
                {
                    this[e.ColumnIndex, e.RowIndex].Value = originalValue;
                }
            }
            else
            {
                //CNA: for radiocolumn get corresponding Value instead of DisplayValue
                FCValueItems vi = this.Columns[e.ColumnIndex].ValueItems();
                if (vi != null && vi.Presentation() == Presentation.dbgRadioButton)
                {
                    originalValue = this.Columns[e.ColumnIndex].ValueItems()[e.RowIndex].Value;
                }
            }

            shouldRaiseCellValidated = false;
            FCDataGridViewCellValidatingEventArgs eventArgs = new FCDataGridViewCellValidatingEventArgs(e.ColumnIndex, e.RowIndex, e.FormattedValue, currentCellOldValue);
            RaiseCellValidating(sender, eventArgs);
            shouldRaiseCellValidated = true;
            e.Cancel = eventArgs.Cancel;
        }

        private void FCTrueDBGrid_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            //CHE: return if disableEvents otherwise it throws exception in base.PreRenderControl when clearing columns
            if (disableEvents)
            {
                return;
            }

            //BAN: do not fire the CellValidated event the first time
            if (needCellValidating)
            {
                return;
            }
            RaiseCellValidated(sender, e);
        }

        private int columnHeaderHeight = 0;
        private ToolTip toolTip = new ToolTip();

        private List<int> GetSplitMaxColIndex()
        {
            List<int> splitMaxColIndex = new List<int>(this.Splits.Count);
            for (int i = 0; i < this.Splits.Count; i++)
            {
                splitMaxColIndex.Add(0);
            }
            int splitNr = -1;
            foreach (DataGridViewColumn tmp in this.Columns)
            {
                splitNr = tmp.GetAttachedSplit();
                if (splitNr >= 0)
                {
                    if (splitMaxColIndex[splitNr] < tmp.Index)
                    {
                        splitMaxColIndex[splitNr] = tmp.Index;
                    }
                }
            }

            return splitMaxColIndex;
        }

        //CHE: merge cells in column
        //private void MergeCellsInColumn()
        //{
        //    if (!this.AllowMerging)
        //    {
        //        return;
        //    }
        //    if (this.Rows.Count == 0)
        //    {
        //        return;
        //    }

        //    for (int i = 0; i < this.Columns.Count; i++)
        //    {
        //        DataGridViewColumn col = this.Columns[i];

        //        //clear previous merge
        //        if (!this.UseInternalPaging)
        //        {
        //            foreach (DataGridViewRow row in this.Rows)
        //            {
        //                //If the cell tag has the value 1, it means that the cell was merged
        //                //(tag was used because when accessing rowspan,colspan or panel.controls then the editing control will dissapear - cell cannot be edit)
        //                if (row.Cells[col.Index].Tag != null && row.Cells[col.Index].Tag.GetType() == typeof(int) && Convert.ToInt32(row.Cells[col.Index].Tag) == 1)
        //                {
        //                    for (int count = 0; count < row.Cells[col.Index].Panel.Controls.Count; count++)
        //                    {
        //                        Control ctrl = row.Cells[col.Index].Panel.Controls[count];
        //                        row.Cells[col.Index].Panel.Controls.Remove(ctrl);
        //                    }
        //                }
        //            }
        //        }

        //        if (!col.Merge())
        //        {
        //            continue;
        //        }

        //        //set merge
        //        this.UseInternalPaging = false;
        //        int rowSpan = 1;
        //        int firstRow = 0;
        //        object value = base.Rows[0].Cells[i].FormattedValue;
        //        for (int j = 1; j < base.Rows.Count; j++)
        //        {
        //            while (j < base.Rows.Count && base.Rows[j].Cells[i].FormattedValue != null && base.Rows[j].Cells[i].FormattedValue != DBNull.Value && value.ToString() == base.Rows[j].Cells[i].FormattedValue.ToString())
        //            {
        //                rowSpan++;
        //                j++;
        //            }
        //            if (rowSpan > 1)
        //            {
        //                base.Rows[firstRow].Cells[i].Rowspan = rowSpan;
        //                base.Rows[firstRow].Cells[i].Colspan = 1;
        //                base.Rows[firstRow].Cells[i].Tag = 1;
        //                DataGridViewCellPanel panel = base.Rows[firstRow].Cells[i].Panel;
        //                Label lbl = new Label();
        //                lbl.Text = value.ToString();
        //                lbl.Dock = DockStyle.Fill;
        //                lbl.Padding = new Padding(4);
        //                //CHE: attach grid events for merge
        //                lbl.MouseDown += new MouseEventHandler(lbl_MouseDown);
        //                lbl.DoubleClick += new EventHandler(lbl_DoubleClick);
        //                panel.Controls.Add(lbl);
        //                panel.BorderStyle = System.Windows.Forms.BorderStyle.None;
        //                panel.BackColor = Color.White;
        //            }

        //            if (j < base.Rows.Count)
        //            {
        //                value = base.Rows[j].Cells[i].FormattedValue;
        //                firstRow = base.Rows[j].Index;
        //                rowSpan = 1;
        //            }
        //        }
        //    }
        //}

        private void lbl_DoubleClick(object sender, EventArgs e)
        {
            this.OnDoubleClick(new EventArgs());
        }

        private void lbl_MouseDown(object sender, MouseEventArgs e)
        {
            //int topRelative = Convert.ToInt32(GetRowVisibleIndex(((DataGridViewCellPanel)((Label)sender).Parent).OwnerCell.RowIndex) * this.RowHeight) + this.ColumnHeadersHeight;
            //this.OnMouseDown(new MouseEventArgs(e.Button, e.Clicks, e.X, e.Y + topRelative, e.Delta));
        }

        private void SetAlignment()
        {
            if (this.IsInvertedOrFormViewGenerated())
            {
                foreach (Control ctrl in this.InvertedView.Controls)
                {
                    DataGridViewColumn col = null;
                    int row = 0;
                    InvertedData data = ctrl.Tag as InvertedData;
                    if (data != null)
                    {
                        col = data.Col as DataGridViewColumn;
                        row = data.Row;
                    }
                    ComboBox cmb = ctrl as ComboBox;

                    if (cmb != null)
                    {
                        if (col.Alignment() == fecherFoundation.Alignment.Right)
                        {
                            cmb.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
                        }
                        else
                        {
                            cmb.RightToLeft = System.Windows.Forms.RightToLeft.No;
                        }
                    }
                }
            }
        }

        private void ColEditEvent(System.Windows.Forms.DataGridViewCellCancelEventArgs e)
        {
            if (ColEdit != null)
            {
                ColEdit(this, new DataGridViewColumnEventArgs(this.Columns[e.ColumnIndex]));
            }
        }

        private bool ChangePositionOnDataTable(bool update = false)
        {
            bool cancel = false;
            //change position on datatable
            if (this.dataTable != null && this.CurrentRow != null)
            {
                if (this.CurrentRow.DataBoundItem != null)
                {
                    this.dataTable.SetIndex(GetDataBoundIndex(this.CurrentRow.Index));
                }
                else if (this.CurrentRow.IsNewRow)
                {
                    this.dataTable.SetIndex(this.CurrentRow.Index + 1);
                }
                if (update)
                {
                    // If the last row is empty and not modified, remove it from the default view, otherwise it may appear again in the grid
                    // as an empty row
                    bool empty = true;
                    foreach (DataGridViewCell cell in this.CurrentRow.Cells)
                    {
                        if (cell.Value != null && !Microsoft.VisualBasic.Information.IsDBNull(cell.Value))
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(cell.Value)))
                            {
                                empty = false;
                                break;
                            }
                        }
                    }
                    //CHE: if DataTable is empty do not remove row from DefaultView and do not raise BeforeColUpdate and AfterColUpdate
                    if (empty && this.dataTable.DefaultView.Count == 1)
                    {
                        return false;
                    }

                    if (this.CurrentRow.Index == this.Rows.Count - 1 && !this.CurrentRow.GetModified() && empty)
                    {
                        this.dataTable.DefaultView[this.dataTable.DefaultView.Count - 1].Delete();
                    }

                    //IPI,CBA: UpdateCurrentRow must raise BeforeColUpdate & AfterColUpdate, only if the current cell is dirty because, once the cell is already validated
                    //the cell validation should not be raised again
                    //IPI: in case of CustomComboBoxColumn, IsCurrentCellDirty does not return the expected result
                    //if (this.CurrentCell != null && this.IsCurrentCellDirty)
                    if (this.CurrentCell != null && (this.IsCurrentCellDirty ||
                        (this.CurrentCell is DataGridViewCustomComboBoxCell && this.Columns[this.CurrentCell.ColumnIndex].Button() && Convert.ToString(currentCellOldValue) != Convert.ToString(this.CurrentCell.Value))))
                    {
                        object oldValue = currentCellOldValue;
                        //IPI: if BeforeColUpdate generates another BeforeColUpdate, must set currentCellOldValue to avoid inifite loop
                        currentCellOldValue = this.CurrentCell.Value;
                        FCDataGridViewCellValidatingEventArgs eventArgs = new FCDataGridViewCellValidatingEventArgs(this.CurrentCell.ColumnIndex, this.CurrentCell.RowIndex, this.CurrentCell.FormattedValue, oldValue);

                        RaiseCellValidating(this, eventArgs);
                    }

                    cancel = RaiseBeforeUpdate(this.CurrentRow);
                    if (!cancel)
                    {
                        //IPI: if the current row is still in DefaultView, the new row must be submitted to DataSource Rows Collection
                        // this will generate the RowChanged (in VB6 - RecordChangeComplete) event for the DataSource
                        DataRowView dr = this.CurrentRow.DataBoundItem as DataRowView;
                        bool isAddedRow = false;
                        if (dr != null && dr.Row.RowState == DataRowState.Detached)
                        {
                            //IPI: need to disable events because EndEdit will raise DGV RowValidated event
                            this.disableEvents = true;
                            //CNA: need to set empty values for CurrentRow's cells if DataTable column does not allow DBNull
                            this.dataTable.SetRowEmptyValues(dr.Row);
                            dr.EndEdit();
                            this.disableEvents = false;
                            isAddedRow = true;
                        }
                        //SBE: update only if not canceled in BeforeUpdate
                        RaiseAfterUpdate();
                        if (isAddedRow)
                        {
                            RaiseAfterInsert();
                        }
                    }
                }
            }
            return cancel;
        }

		private void SetCellAlignment(DataGridViewColumn column)
        {
            if ((IsInvertedOrFormViewGenerated()) || this.GetType() == typeof(TDBDropDown))
            {
                return;
            }
            if (column.DefaultCellStyle.Alignment == DataGridViewContentAlignment.NotSet &&
                column.Alignment() == fecherFoundation.Alignment.General)
            {
                //For data cells, the setting 3 - General means that text will be left-aligned and 
                //numbers will be right-aligned. This setting is only useful in bound mode, 
                //where the grid can query the data source to determine the data types of individual columns.
                if (this.dataTable == null)
                    return;

                if (column.Index >= this.dataTable.Columns.Count)
                    return;

                DataGridViewContentAlignment alignment = DataGridViewContentAlignment.NotSet;
                DataColumn dataColumn = null;
                if (!String.IsNullOrEmpty(column.DataPropertyName))
                {
                    dataColumn = this.dataTable.Columns[column.DataPropertyName];
                }
                if (dataColumn == null)
                {
                    dataColumn = this.dataTable.Columns[column.Index];
                }
                switch (Type.GetTypeCode(dataColumn.DataType))
                {
                    case TypeCode.Char:
                    case TypeCode.String:
                        alignment = DataGridViewContentAlignment.MiddleLeft;
                        break;
                    case TypeCode.Boolean:
                        alignment = DataGridViewContentAlignment.MiddleCenter;
                        break;

                    default:
                        alignment = DataGridViewContentAlignment.MiddleRight;
                        break;
                }

                column.DefaultCellStyle.Alignment = alignment;
            }
        }

        private void FCTrueDBGrid_BindingContextChanged(object sender, EventArgs e)
        {
            RefreshDataSource();
        }

        private void FCTrueDBGrid_DataSourceChanged(object sender, EventArgs e)
        {
            //check if HoldFields() method was called
            if (this.dataTable != null && originalColumns != null)
            {
				//Columns will be added again, so prevent releasing extension in order to not lost extension properties
				preventReleasingExtension = true;
				this.Columns.Clear();
				int frozenColumnsCount = 0;
                for (int i = 0; i < originalColumns.Length; i++)
                {
					if (originalColumns[i].Frozen)
					{
						++frozenColumnsCount;
						originalColumns[i].Frozen = false;
					}
                    this.Columns.Add(originalColumns[i]);
                    this.Columns[i].DisplayIndex = i;
                }
				for (int i = 0; i < frozenColumnsCount; i++)
				{
					Columns[i].Frozen = true;
				}
				preventReleasingExtension = false;
            }

            RefreshDataSource();
            if (this.dataTable != null)
            {
                this.dataTable.SetParentGrid(this);

                if (this.dataTable != null && this.dataTable.LockType() == LockTypeEnum.adLockReadOnly)
                {
                    this.ReadOnly = true;
                }

                this.RedrawInverted();
            }

            DataGridViewComboBoxColumn comboCol = null;
            DataGridViewCustomComboBoxColumn customComboCol = null;
            DataTable dropDownGridDataSource = null;
            FCValueItems cmbColValueItems = null;
            FCValueItem currentColItems = null;
            TDBDropDown dropDownGrid = null;
            Type colType = null;

            foreach (DataGridViewColumn col in this.Columns)
            {
                colType = col.GetType();

				if (colType == typeof(DataGridViewCustomComboBoxColumn))
				{
					#region Add ValueItems for DropDown

					customComboCol = col as DataGridViewCustomComboBoxColumn;
					dropDownGrid = customComboCol.TDBDropDown;

					if (dropDownGrid != null)
					{
						dropDownGridDataSource = dropDownGrid.DataSource as DataTable;
						if (dropDownGridDataSource != null)
						{
							cmbColValueItems = customComboCol.ValueItems();
							cmbColValueItems.Translate = dropDownGrid.ValueTranslate;

							object valueMember = null;
							object displayMember = null;
							foreach (DataRow dr in dropDownGridDataSource.Rows)
							{
								if (string.IsNullOrEmpty(dropDownGrid.DataField) || string.IsNullOrEmpty(dropDownGrid.ListField))
								{
									displayMember = valueMember = dr[0];
								}
								else
								{
									displayMember = dr[dropDownGrid.ListField];
									valueMember = dr[dropDownGrid.DataField];
								}

								cmbColValueItems.Add(new FCValueItem(displayMember, valueMember));
							}
						}
					}

					#endregion
				}
				//CNA: compare to original, ValueItems are created only for ComboBoxColumns and not for CustomComboBoxColumns too
				else if (colType == typeof(DataGridViewComboBoxColumn))
				{
					#region Add ValueItems
					comboCol = col as DataGridViewComboBoxColumn;
					cmbColValueItems = col.ValueItems();

					foreach (DataGridViewRow row in this.Rows)
					{
						if (IsValidItem(col.Index, row.Index))
						{
							object cellValue = this[col.Index, row.Index].Value;
							int valueItemIndex = -1;
							//BAN: add the value item to the ValueItems list with empty display member, because, othewise,
							//when getting the formated value, if the value is not found amongst the column items it will throw a DataError
							if (cellValue != null && cmbColValueItems[cellValue] == null)
							{
								valueItemIndex = cmbColValueItems.Add(new FCValueItem("", cellValue));
							}
							//After adding the value to the column items, get the formated value.
							object cellDisplayValue = this[col.Index, row.Index].FormattedValue;
							if (cellValue != null && cmbColValueItems[cellValue] != null && valueItemIndex >= 0)
							{
								//If the display value is empty, then the value is the same as the display value - this is the case when
								//the combo box column is populated directly via the DataSource, and the value items where not added via 
								//the ValueItems collection Add method
								if (cellDisplayValue != null)
								{
									if (string.IsNullOrEmpty(cellDisplayValue.ToString()))
									{
										cellDisplayValue = cellValue;
									}
								}
								//After setting the display value, the item in the column Items collection needs to be set
								//because the display value was set before as empty
								cmbColValueItems[valueItemIndex].DisplayValue = cellDisplayValue;

								if (valueItemIndex < comboCol.Items.Count)
								{
									currentColItems = comboCol.Items[valueItemIndex] as FCValueItem;
								}

								if (currentColItems != null)
								{
									currentColItems.DisplayValue = cellDisplayValue;
								}

								cmbColValueItems.Translate = true;
							}
						}
					}
					#endregion
				}
            }
        }

        private object GetComboBoxItemByValue(ComboBox cmb, string value)
        {
            object selectedItem = null;

            foreach (object item in cmb.Items)
            {
                string val;
                Type itemType = item.GetType();
                if (itemType == typeof(FCValueItem))
                {
                    val = ((FCValueItem)item).DisplayValue as string;
                }
                else
                {
                    val = item as string;
                }
                if (val != null)
                {
                    if (itemType == typeof(FCValueItem))
                    {
                        if (((FCValueItem)item).Value.ToString() == value)
                        {
                            selectedItem = item;
                            break;
                        }
                    }
                }
            }

            return selectedItem;
        }

        //CHE: set max length for all textboxes
        private void SetMaxLengthAll(DataGridViewTextBoxColumn tbCol, int maxLength)
        {
            KeyValuePair<Label, List<Control>> colControls = tbCol.ColumnControls();
            if (colControls.Value != null)
            {
                foreach (Control ctrl in colControls.Value)
                {
                    TextBox tb = ctrl as TextBox;
                    if (tb != null)
                    {
                        tb.MaxLength = maxLength;
                    }
                }
            }
        }

        private void tb_KeyUp(object objSender, KeyEventArgs objArgs)
        {
            base.OnKeyUp(objArgs);
        }

        private void cmb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ControlRaiseCellBeginEdit(sender);
        }

        private void rd_Click(object sender, EventArgs e)
        {
            ControlRaiseCellBeginEdit(sender);
        }

        private void chk_Click(object sender, EventArgs e)
        {
            //IPI: if a MessageBox is shown in BeforeColEdit, will generate CheckBox click again
            if (this.IsCellBeginEdit)
            {
                return;
            }
            ControlRaiseCellBeginEdit(sender);
        }

        private void cCmb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ControlRaiseCellBeginEdit(sender);
        }

        private void tb_KeyPress(object sender, KeyPressEventArgs e)
        {
            ControlRaiseCellBeginEdit(sender);
            //CNA: also fire grid's KeyPress event
            base.OnKeyPress(e);
        }

        private void CheckBoxRestoreOriginalValue(CheckBox chkCtrl)
        {
            bool prevDisableEvents = this.disableEvents;
            //prevent CheckedChanged event
            disableEvents = true;
            chkCtrl.Checked = !Convert.ToBoolean(this.Rows[this.Row].Cells[this.Col].Value == DBNull.Value ? false : this.Rows[this.Row].Cells[this.Col].Value);
            this.Rows[this.Row].Cells[this.Col].Value = chkCtrl.Checked;
            disableEvents = prevDisableEvents;
        }

        private void ControlRaiseCellBeginEdit(object sender)
        {
            Control objSender = sender as Control;
            InvertedData ctrlData = objSender.Tag as InvertedData;
            if (ctrlData != null && !ctrlData.Modified)
            {
                //CNA: set CurrentCell before raising CellBeginEdit event
                DataGridViewColumn col = ctrlData.Col as DataGridViewColumn;
                int row = ctrlData.Row;

                if (col != null)
                {
                    this.Col = col.DisplayIndex;
                }
                if (this.Row != row)
                {
                    this.Row = row;
                }

                if (this.CurrentCell != null)
                {
                    DataGridViewCellCancelEventArgs e = new DataGridViewCellCancelEventArgs(this.CurrentCell.ColumnIndex, this.CurrentCell.RowIndex);
                    //IPI: when CheckedChanged is raised before CheckBox Click, CheckBox value is already modified
                    // need to set back the previous value to correctly get oldCellValue
                    CheckBox chkCtrl = objSender as CheckBox;
                    if (chkCtrl != null)
                    {
                        if (isCheckBoxValueChanged)
                        {
                            CheckBoxRestoreOriginalValue(chkCtrl);

                            isCheckBoxValueChanged = false;
                        }
                    }
                    this.OnCellBeginEdit(e);
                    if (!e.Cancel)
                    {
                        ctrlData.Modified = true;
                        if (chkCtrl != null)
                        {
                            CheckBoxRestoreOriginalValue(chkCtrl);
                        }
                    }
                    else
                    {
                        //IPI: in case of CheckBox, the value is already modified in Click and CheckChanged events
                        // for RadioButtons, the value is not yet changed in Click event
                        if (chkCtrl != null)
                        {
                            chkCtrl.Checked = Convert.ToBoolean(this.Rows[row].Cells[col.DisplayIndex].Value);
                            chkCtrl.Invalidate();
                        }
                    }
                }
            }
        }

        private void ctrl_Enter(object sender, EventArgs e)
        {
            Control objSender = sender as Control;

            // save the original value of the control before user starts editing
            if (!generateInvertedGridInProgress)
            {
                currentCellOldValue = objSender.Text;
            }
            //IPI: set DataWidth & EditMask on generated controls
            //IFCForm ifrm = this.Form as IFCForm;
            //InvertedData data = objSender.Tag as InvertedData;
            //if (data != null)
            //{
            //    int dataWidth = data.Col.DataWidth();
            //    if (dataWidth > 0)
            //    {
            //        ifrm.InvokeMethodRegisteredComponent("SetProperty", "DataWidth", dataWidth, objSender.ID);
            //    }
            //    string editMask = data.Col.EditMask();
            //    if (!string.IsNullOrEmpty(editMask))
            //    {
            //        ifrm.InvokeMethodRegisteredComponent("SetProperty", "EditMask", editMask, objSender.ID);
            //    }
            //}

            //set current cell
            SetCurrentCell(objSender);
            //execute gotfocus
            if (this.IsInvertedOrFormViewGenerated() && !isInvertedCtrlFocused)
            {
                isInvertedCtrlFocused = true;
                //CNA: container panel should also be focused
                this.InvertedView.Focus();
                this.OnGotFocus(EventArgs.Empty);
            }
            //IPI: when switching between inverted grid's controls with TAB key, the focus is not always set
            if (!objSender.Focused)
            {
                objSender.Focus();
            }
        }

        private void ctrl_Leave(object sender, EventArgs e)
        {
            if (disableEvents)
            {
                return;
            }
            Control objSender = sender as Control;

            InvertedData ctrlData = objSender.Tag as InvertedData;
            if (ctrlData != null)
            {
                ctrlData.Modified = false;
            }

            //CNA: BeforeRowColChange should be fired after leaving a control on inverted grid; CHE - only if events are not disabled
            if (!disableEvents)
            {
                RaiseBeforeRowColChange();
            }

            if (this.IsInvertedOrFormViewGenerated() &&
                !this.InvertedView.ContainsFocus && this.Form != null && !this.InvertedView.Controls.Contains(this.Form.ActiveControl))
            {
                this.OnLostFocus(EventArgs.Empty);
                isInvertedCtrlFocused = false;
            }
        }

        private void SetCurrentCell(Control sender)
        {
            InvertedData data = sender.Tag as InvertedData;

            if (data != null)
            {
                this.disableEvents = true;
                this.Col = data.Col.DisplayIndex;
                this.Row = data.Row;
                //IPI: because the events are disabled, must also set CurrentCell (it will not get set in Row/Col setter)
                this.CurrentCell = this[data.Col.DisplayIndex, data.Row];
                this.disableEvents = false;
            }
        }

        private int GetVisibleRows()
        {
            int visibleRows = 0;
            foreach (DataGridViewRow dataGridViewRow in this.Rows)
            {
                if (dataGridViewRow.Visible)
                {
                    ++visibleRows;
                }
            }
            return visibleRows;
        }

        private void tb_GotFocus(object sender, EventArgs e)
        {
            //set current cell
            SetCurrentCell(sender as Control);
            DisablePreRenderRegenerate = true;
        }

        private void tb_LostFocus(object sender, EventArgs e)
        {
            DisablePreRenderRegenerate = false;
        }

        private void tb_KeyDown(object objSender, KeyEventArgs objArgs)
        {
            DisablePreRenderRegenerate = true;

            this.OnKeyDown(objArgs);
        }

        //Trigger CellValueChanged for inverted view
        private void ctrl_CellValueChanged(object sender, EventArgs e)
        {
            if (this.DisableEvents)
            {
                return;
            }
            disableEvents = true;
            Control objSender = sender as Control;
            DataGridViewColumn col = null;
            int row = 0;
            InvertedData data = objSender.Tag as InvertedData;
            Type controlType = objSender.GetType();
            if (data != null)
            {
                col = data.Col as DataGridViewColumn;
                row = data.Row;
            }

            object cellValue = null;
            if (controlType == typeof(CheckBox))
            {
                CheckBox cb = objSender as CheckBox;

                if (col.Value().ToString() != cb.Checked.ToString())
                {
                    cellValue = cb.Checked;
                    //IPI: if a Click event will be followed after CheckedChanged, the flag will be used in ControlRaiseCellBeginEdit
                    isCheckBoxValueChanged = true;
                }
            }
            else if (controlType == typeof(FCCustomComboBox) || controlType == typeof(FCComboBox))
            {
                ComboBox cCmb = sender as ComboBox;
                //SBE: if text was typed in ComboBox, Text value is different than SelectedItem string value
                if (cCmb.SelectedItem != null && (cCmb.Text != null && cCmb.Text == cCmb.SelectedItem.ToString()))
                {
                    if (cCmb.SelectedItem.GetType() == typeof(FCValueItem))
                    {
                        cellValue = ((FCValueItem)cCmb.SelectedItem).Value;
                    }
                }
                else
                {
                    cellValue = objSender.Text;
                }
            }
            //SBE: get RadioButton value instead of display text
            else if (controlType == typeof(RadioButton))
            {
                RadioButton rd = sender as RadioButton;
                cellValue = data.Value;
            }
            else
            {
                cellValue = objSender.Text;
            }

            if (cellValue == null)
            {
                cellValue = DBNull.Value;
            }

            if (this.dataTable == null)
            {
                this.Col = col.DisplayIndex;
                if (this.Row != row)
                {
                    this.Row = row;
                }
                //IPI: when the grid has no rows and ValueItems are added in the ComboBox, Row = -1 and grid value should not be set
                if (this.Col >= 0 && this.Row >= 0)
                {
                    this[this.Col, this.Row].Value = cellValue;
                }
            }
            else
            {
                //CHE: do not call SetDataCellValue if no change (avoid entering in grid_Change)
                if (!string.IsNullOrEmpty(col.DataPropertyName) && this.Columns[col.Index].Value() != cellValue)
                {
                    disableEvents = false;
                    SetDataCellValue(col, cellValue);
                    disableEvents = true;
                }
                else
                {
                    if (this.Row != row)
                    {
                        this.Row = row;
                    }
                    //IPI: when the grid has no rows and ValueItems are added in the ComboBox, Row = -1 and grid value should not be set
                    if (this.Col >= 0 && this.Row >= 0)
                    {
                        this[this.Col, this.Row].Value = cellValue;
                        //SBE: raise CellValueChanged if DataProperty is not set for column
                        disableEvents = false;
                        DataGridViewCellEventArgs cellEventArgs = new DataGridViewCellEventArgs(col.Index, this.CurrentRow.Index);
                        //this.OnCellValueChanged(cellEventArgs, false);
                        disableEvents = true;
                    }
                }
            }

            //IPI: when the grid has no rows and ValueItems are added in the ComboBox, Row = -1 and CurrentCell should not be set
            if (this.Col < 0 || this.Row < 0)
            {
                disableEvents = false;
                return;
            }
            DataGridViewCell currentCell = this[this.Col, this.Row];
            this.EndEdit();
            //CHE: flag for skipping Validating when TextChange was raised by javascript on DropDownOpen for FCCustomComboBox
            //FCCustomComboBox customCmb = sender as FCCustomComboBox;
            //if (customCmb != null && customCmb.TextChangeRaised)
            //{
            //    customCmb.TextChangeRaised = false;
            //}
            //else
            {
                disableEvents = false;
                this.CurrentCell = null;
                disableEvents = true;
                this.CurrentCell = currentCell;
                //CNA: Refresh the grid in addition because changes may have been occured in eventhandlers
                RefreshInvertedData();
            }
            disableEvents = false;
        }

        private void RefreshDataSource()
        {
            if (this.DataSource == null || this.dataTable == null)
            {
                return;
            }

            //update pending new rows
            if (this.dataTable.HasNewRows())
            {
                this.dataTable.Update();
            }

            if (this.DataMode != DataMode.Bound)
            {
                return;
            }

            if ((this.dataTable.EOF() || this.dataTable.BOF()) && this.dataTable.Rows.Count > 0)
            {
                this.dataTable.MoveFirst();
            }

            DataGridViewTextBoxColumn textColumn = null;
            foreach (DataGridViewColumn col in this.Columns)
            {
                // by default, the columns have General Alignment style
                //SBE: set general Alignment style only if AuotoGenerateColumns is true
                if (AutoGenerateColumns)
                {
                    col.Alignment(fecherFoundation.Alignment.General);
                }

				//set input max length
				if (textColumn != null)
                {
                    if (this.dataTable.Columns[col.DataPropertyName] != null)
                    {
                        if (this.dataTable.Columns[col.DataPropertyName].MaxLength > 0)
                        {
                            textColumn.MaxInputLength = this.dataTable.Columns[col.DataPropertyName].MaxLength;
                        }
                        //CHE: for TextBoxColumn in CellBeginEdit the javascript SetDataWith does not find textarea TRG_ which is created later when key is pressed 
                        // (same works fine in SetEditMask for ComboBoxColumn); 
                        // as solution for TextBoxColumn set MaxInputLength based on associated DataColumn type and size
                        if (this.dataTable.Fields(col.DataPropertyName).DataWidth > 0)
                        {
                            textColumn.MaxInputLength = this.dataTable.Fields(col.DataPropertyName).DataWidth;
                        }
                    }
                }
            }
        }

        private bool ShouldChangeTypeByUsingPropertyName(System.ComponentModel.PropertyDescriptor objPropertyDescriptor)
        {
            return objPropertyDescriptor.Name == "Col: 0"; // First column
        }

        /// <summary>
        /// when rows are scrolled up/ down in the table visible area, this functionswill transform the row index to visible rows index
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <returns> row index in the visible area of the table</returns>
        private int GetRowVisibleIndex(int rowIndex)
        {
            int visibleRowIndex = rowIndex;

            visibleRowIndex -= this.FirstDisplayedScrollingRowIndex;
            //if (this.ScrollTop != 0)
            //{
            //    ++visibleRowIndex;
            //}
            return visibleRowIndex;
        }

        /// <summary>
        /// when rows are scrolled up/ down in the table visible area, 
        /// this function will transform the visible rows index to actual row index in the table's rows collection
        /// </summary>
        /// <param name="visibleRowIndex"></param>
        /// <returns></returns>
        private int GetRowIndex(int visibleRowIndex)
        {
            int rowIndex = visibleRowIndex + this.FirstDisplayedScrollingRowIndex;
            //if (this.ScrollTop != 0)
            //{
            //    --rowIndex;
            //}
            return rowIndex;
        }

        //CHE: get columns in displayed order
        private DataGridViewColumn[] GetColumnsByDisplayIndex()
        {
            DataGridViewColumn[] columns = new DataGridViewColumn[this.Columns.Count];

            foreach (DataGridViewColumn col in this.Columns)
            {
                if (col.DisplayIndex >= 0)
                {
                    columns[col.DisplayIndex] = col;
                }
            }

            return columns;
        }

        #endregion

        //void IFCGrid.SetDisableEvents(bool disableEvents)
        //{
        //    this.disableEvents = disableEvents;
        //}

        private class Coordinates
        {
            public Coordinates(int intColIndex, int intRowIndex)
            {
                this.RowIndex = intRowIndex;
                this.ColIndex = intColIndex;
            }

            public int RowIndex { get; set; }
            public int ColIndex { get; set; }
        }
    }

    public class ColEventArgs : DataGridViewColumnEventArgs
    {
        private int colIndex;

        public int ColIndex
        {
            get
            {
                return colIndex;
            }
        }

        public ColEventArgs(DataGridViewColumn column) : base(column)
        {
            colIndex = column.Index;
        }
    }

	public class FetchCellStyleEventArgs
	{
		public CellStyleFlag Condition;
		public int Split;
		public int Bookmark;
		public int ColumnIndex;
		public DataGridViewCellStyle CellStyle;

		public FetchCellStyleEventArgs(CellStyleFlag condition, int split, int bookmark, int columnIndex, DataGridViewCellStyle cellStyle)
		{
			Condition = condition;
			Split = split;
			Bookmark = bookmark;
			ColumnIndex = columnIndex;
			CellStyle = cellStyle;
		}
	}

    public class FetchRowStyleEventArgs
    {
		public int Split;
        public int Bookmark;
        public DataGridViewCellStyle RowStyle;

        public FetchRowStyleEventArgs(int split, int bookmark, DataGridViewCellStyle rowStyle) 
        {
			Split = split;
			Bookmark = bookmark;
			RowStyle = rowStyle;
        }
    }

    public class UnboundReadDataEventArgs : EventArgs
    {
        public RowBuffer RowBuf;
        public int? StartLocation;
        public bool ReadPriorRows;
    }

}