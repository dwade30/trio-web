﻿using System.Windows.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    /// <summary>
    /// This class represents custom column type that is inherited from the DataGridViewComboBoxColumn column type.
    /// </summary>
    public class DataGridViewCustomComboBoxColumn : DataGridViewComboBoxColumn
    {
        private TDBDropDown dropDownGrid;

        public TDBDropDown TDBDropDown
        {
            get { return dropDownGrid; }
            set
            {
                DataGridViewCustomComboBoxCell template = this.CellTemplate as DataGridViewCustomComboBoxCell;
                if (template != null)
                {
                    dropDownGrid = value;
                }
            }
        }

        public DataGridViewCustomComboBoxColumn()
        {
            // Set a custom template for a cell of DataGridView.
            this.CellTemplate = new DataGridViewCustomComboBoxCell();

            // Set default column property values
            this.AutoComplete = false;
            this.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.NotSet;
            this.DataPropertyName = "";
            this.DefaultCellStyle = new System.Windows.Forms.DataGridViewCellStyle();
            this.DefaultHeaderCellType = typeof(System.Windows.Forms.DataGridViewColumnHeaderCell);
            this.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.DropDownButton;
            //this.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDown;
            this.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.HeaderText = "Custom ComboBox";
            this.Name = "customComboBox";
            this.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Width = 100;
        }
    }
}
