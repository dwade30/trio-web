﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    public partial class FCMonthCalendar : MonthCalendar, ISupportInitialize
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private AppearanceConstants appearance = AppearanceConstants.cc23D;
        private BorderStyleConstants borderStyle = BorderStyleConstants.cc2None;
        private DayConstants startOfWeek = DayConstants.mvwMonday;
        private DateTime originDate = new DateTime(1899, 12, 30);
        private int currentDate;

        #endregion

        #region Constructors

        public FCMonthCalendar()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events

        public event EventHandler DateClick;

        #endregion

        #region Private Events
        #endregion

        #region Enums

        public enum AppearanceConstants
        {
            cc2Flat = 0,
            cc23D = 1
        }

        public enum BorderStyleConstants
        {
            cc2None = 0,
            cc2FixedSingle = 1
        }

        public enum DayConstants
        {
            mvwSunday = 1,
            mvwMonday = 2,
            mvwTuesday = 3,
            mvwWednesday = 4,
            mvwThursday = 5,
            mvwFriday = 6,
            mvwSaturday = 7
        }

        #endregion

        #region Properties

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        public DateTime Value
        {
            get
            {
                return this.SelectionRange.Start;
            }
            set
            {
                this.SetSelectionRange(value, value);
            }                
        }

        /// <summary>
        /// Gets/sets the Appearance of the control
        /// </summary>
        [Obsolete(".NET does not support MonthCalendar Appearance")]
        [DefaultValue(AppearanceConstants.cc23D)]
        public AppearanceConstants Appearance
        {
            get
            {
                return this.appearance;
            }
            set
            {
                this.appearance = value;
            }
        }

        /// <summary>
        /// Gets/sets the BorderStyle of the control
        /// </summary>
        [Obsolete(".NET does not support MonthCalendar BorderStyle")]
        [DefaultValue(BorderStyleConstants.cc2None)]
        public BorderStyleConstants BorderStyle
        {
            get
            {
                return this.borderStyle;
            }
            set
            {
                this.borderStyle = value;
            }
        }
        /// <summary>
        /// StartOfWeek is the direct correspondant of FirstDayOfWeek from the .NET MontCalendar control
        /// </summary>
        [DefaultValue(DayConstants.mvwMonday)]
        public DayConstants StartOfWeek
        {
            get
            {
                return this.startOfWeek;
            }
            set
            {
                this.startOfWeek = value;
                base.FirstDayOfWeek = DayFromDayConstants(this.startOfWeek);
            }
        }


        /// <summary>
        /// Gets and sets the number of days elapsed since 30.12.1899 
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int CurrentDate
        {
            get
            {
                return currentDate;
            }
            set
            {
                currentDate = value;
                DateTime date = originDate.AddDays(currentDate);
                this.SelectionRange = new SelectionRange(date, date);
            }

        }

        public object OcxState { get; set; }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        public int VisibleDays(int value)
        {
            // TODO:BAN
            return 0;
        }

        public void BeginInit()
        {

        }

        public void EndInit()
        {

        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            //CHE: do not raise when changing month
            if (DateClick != null && e.Y > 50)
            {
                DateClick(this, EventArgs.Empty);
            }
        }

        #endregion

        #region Private Methods

        private Day DayFromDayConstants(DayConstants dc)
        {
            Day day = Day.Default;
            switch (dc)
            {
                case DayConstants.mvwMonday:
                    day = Day.Monday;
                    break;
                case DayConstants.mvwTuesday:
                    day = Day.Tuesday;
                    break;
                case DayConstants.mvwWednesday:
                    day = Day.Wednesday;
                    break;
                case DayConstants.mvwThursday:
                    day = Day.Thursday;
                    break;
                case DayConstants.mvwFriday:
                    day = Day.Friday;
                    break;
                case DayConstants.mvwSaturday:
                    day = Day.Saturday;
                    break;
                case DayConstants.mvwSunday:
                    day = Day.Sunday;
                    break;
            }
            return day;
        }

        #endregion
    }
}
