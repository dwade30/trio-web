﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using System.Collections;
using fecherFoundation.Extensions;
using System.Threading;

namespace fecherFoundation
{
    /// <summary>
    /// VB.Menu
    /// </summary>
    public partial class FCToolStripMenuItem : MenuItem
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        #endregion

        #region Constructors

        public FCToolStripMenuItem()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        // TODO
        //public event ToolStripDropDownClosedEventHandler FCDropDownClosedEventHandler;
        #endregion

        #region Private Events
       

        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// In .NET use the Availabe-Property instead of Visible
        /// </summary>
        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public new bool Visible
        //{
        //    get { return Available; }
        //    set
        //    {
        //        base.Visible = value;
        //        Available = value;
        //    }
        //}

        /// <summary>
        /// Get / Set Shortcut-Keys (default for ShortcutKeys is true) 
        /// </summary>
        // TODO
        //public object Shortcut
        //{
        //    get
        //    {
        //        return base.ShortcutKeys;
        //    }
        //    set
        //    {
        //        base.ShortcutKeys = (Wisej.Web.Keys)value;
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        public new string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
                //CHE: in VB6 if menu's text is "-" then is interpreted as menu separator
                // TODO
                //if (value == "-")
                //{
                //    FCToolStripMenuItem parent = this.OwnerItem as FCToolStripMenuItem;
                //    if (parent != null)
                //    {
                //        FCToolStripSeparator toolStripSeparator = new FCToolStripSeparator();
                //        //save the menuitem
                //        toolStripSeparator.Tag = this;
                //        //replace in parent dropdownitems list
                //        int index = parent.DropDownItems.IndexOf(this);
                //        parent.DropDownItems.Insert(index, toolStripSeparator);
                //        parent.DropDownItems.Remove(this);
                //        //replace in control array list
                //        List<object> list = this.GetControlArray() as List<object>;
                //        if (list != null)
                //        {
                //            index = list.IndexOf(this);
                //            list.Insert(index, toolStripSeparator);
                //            toolStripSeparator.SetControlArray(list);
                //            list.Remove(this);
                //        }
                //    }
                //}
            }

        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Caption
        {
            get
            {
                return this.Text;
            }
            set
            {
                this.Text = value;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// Show drop down of a ToolStripMenuItem as a context menu using the same objects as present in the tool strip.
        /// Displays a pop-up menu on an MDIForm or Form object at the current mouse location or at specified coordinates. 
        /// </summary>
        /// <param name="form"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="boldCommand"></param>
        public void ShowDropDown(dynamic form, int x, int y, Object boldCommand = null)
        {
            using (Graphics g = form.CreateGraphics())
            {
                GraphicsFactory graphicsFactory = new GraphicsFactory();
                FCGraphics fcGraphics = new FCGraphics(g, graphicsFactory);
                fcGraphics.InitializeCoordinateSpace(g, form.Bounds, 0, 0);
                fcGraphics.ScaleMode = form.ScaleMode;
                fcGraphics.ScaleWidth = form.ScaleWidth;
                fcGraphics.ScaleHeight = form.ScaleHeight;
                if (form.ScaleMode != ScaleModeConstants.vbUser)
                {
                    x = Convert.ToInt32(fcGraphics.ScaleX(x, form.ScaleMode, ScaleModeConstants.vbPixels));
                    y = Convert.ToInt32(fcGraphics.ScaleY(y, form.ScaleMode, ScaleModeConstants.vbPixels));
                }
                else
                {
                    //CHE: consider ScaleLeft/ScaleTop for ScaleMode vbUser
                    x = Convert.ToInt32(fcGraphics.ScaleX(x - form.ScaleLeft, form.ScaleMode, ScaleModeConstants.vbPixels));
                    y = Convert.ToInt32(fcGraphics.ScaleY(y - form.ScaleTop, form.ScaleMode, ScaleModeConstants.vbPixels));
                }
            }
            ShowDropDown(form, x, y, boldCommand as MenuItem);
        }

        /// <summary>
        /// Show drop down of a ToolStripMenuItem as a context menu using the same objects as present in the tool strip.
        /// Displays a pop-up menu on an MDIForm or Form object at the current mouse location or at specified coordinates. 
        /// </summary>
        /// <param name="form"></param>
        public void ShowDropDown(FCForm form)
        {
            var relativePoint = form.PointToClient(Cursor.Position);
            ShowDropDown(form, relativePoint.X, relativePoint.Y, null);
        }

        /// <summary>
        /// Show drop down of a ToolStripMenuItem as a context menu using the same objects as present in the tool strip.
        /// Displays a pop-up menu at the current mouse location or at specified coordinates. 
        /// </summary>
        public void ShowDropDown()
        {
            Point pt = new Point();
            Externals.USER32.GetCursorPos(ref pt);
            ShowDropDown(null, pt.X, pt.Y, new MenuItem());
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        // TODO
        //protected internal virtual void FCToolStripDropDownClosed(object sender, ToolStripDropDownClosedEventArgs e)
        //{
        //}

        /// <summary>
        /// Raises the TextChanged event.
        /// </summary>
        /// <param name="e"></param>
        // TODO
        //protected override void OnTextChanged(EventArgs e)
        //{
        //    base.OnTextChanged(e);
        //    //FC:FINAL:DSE In VB6, the Text = "-" will change the menu item into a separator
        //    if (this.Text == "-")
        //    {
        //        ToolStripDropDownMenu parentMenu = (this.GetCurrentParent() as ToolStripDropDownMenu);
        //        if (parentMenu != null)
        //        {
        //            int itemIndex = parentMenu.Items.IndexOf(this);
        //            parentMenu.Items.Remove(this);
        //            parentMenu.Items.Insert(itemIndex, new FCToolStripSeparator());
        //        }
        //    }
        //    //FC:FINAL:DSE In VB6 text followed by text is converted to shortcut key
        //    if (this.Text.Contains("\t"))
        //    {
        //        int tabLocation = this.Text.IndexOf("\t");
        //        KeysConverter converter = new KeysConverter();
        //        try
        //        {
        //            this.ShortcutKeys = (Keys)converter.ConvertFromString(this.Text.Substring(tabLocation));
        //            this.Text = this.Text.Substring(0, tabLocation);
        //        }
        //        //if text following is not a key shortcut, do nothing
        //        catch
        //        {
        //        }
        //    }
        //}
        #endregion

        #region Private Methods

        protected override void OnClick(EventArgs e)
        {
            //JEI:HARRIS:IT230: in VB6 when a parent of a MenuItem is set to invisible, the Shortcuts aren't executed from the 
            // child MenuItems. That is different from WinForms and also Wisej.
            if (this.Shortcut != Shortcut.None)
            {
                MenuItem parent = this.Parent as MenuItem;
                while (parent != null)
                {
                    if (!parent.Visible)
                    {
                        return;
                    }
                    parent = parent.Parent as MenuItem;
                }
            }
            base.OnClick(e);
        }

        // TODO
        //private void ShowDropDown(Control form, int x, int y, MenuItem boldMenuItem)
        //{
        //    ContextMenuStrip menuStrip = new ContextMenuStrip();

        //    menuStrip.Tag = this;
        //    foreach (MenuItem mnuItem in this.MenuItems)
        //    {
        //        if (mnuItem is MenuItem)
        //        {
        //            if (mnuItem.Available == true)
        //            {
        //                ToolStripMenuItem tsmi = new ToolStripMenuItem() { Text = mnuItem.Text, Tag = mnuItem, Enabled = mnuItem.Enabled, Checked = ((ToolStripMenuItem)mnuItem).Checked };
        //                if (boldMenuItem != null && mnuItem.Equals(boldMenuItem))
        //                {
        //                    tsmi.Font = new Font(tsmi.Font, FontStyle.Bold);
        //                }
        //                int itemIndex = menuStrip.Items.Add(tsmi);
        //                if (((ToolStripMenuItem)mnuItem).DropDownItems.Count > 0)
        //                {
        //                    ToolStripItem[] dropDownItems = new ToolStripItem[((ToolStripMenuItem)mnuItem).DropDownItems.Count];
        //                    GetItemsFromToolStripItem(((ToolStripMenuItem)mnuItem), ref dropDownItems);
        //                    ((ToolStripMenuItem)menuStrip.Items[itemIndex]).DropDownItems.AddRange(dropDownItems);
        //                    ((ToolStripMenuItem)menuStrip.Items[itemIndex]).DropDownItemClicked += new ToolStripItemClickedEventHandler(menuStrip_ItemClicked);
        //                }
        //            }
        //        }
        //        else if (mnuItem is ToolStripSeparator)
        //        {
        //            menuStrip.Items.Add(new ToolStripSeparator());
        //        }
        //    }
        //    menuStrip.ItemClicked += new ToolStripItemClickedEventHandler(menuStrip_ItemClicked);
        //    menuStrip.Closed += menuStrip_Closed;
        //    if (form != null)
        //    {
        //        menuStrip.Show(form, x, y);
        //    }
        //    else
        //    {
        //        menuStrip.Show(x, y);
        //    }
        //    menuStrip.Focus();
        //    menuStrip.Capture = true;
        //    //CHE: modal context menu
        //    while (menuStrip.Visible)
        //    {
        //        App.DoEvents();
        //    }
        //}

        private void GetItemsFromToolStripItem(MenuItem mnuItem, ref MenuItem[] dropDownItems)
        {
            int counter = 0;
            foreach (MenuItem itm in mnuItem.MenuItems)
            {
                counter = mnuItem.MenuItems.IndexOf(itm);
                if (itm is MenuItem)
                {
                    if (itm.Visible == true)
                    {
                        dropDownItems[counter] = new MenuItem() { Text = itm.Text, Tag = itm, Enabled = itm.Enabled, Checked = itm.Checked };
                    }

                    if (itm.MenuItems.Count > 0)
                    {
                        MenuItem[] itmDropDownItems = new MenuItem[itm.MenuItems.Count];
                        GetItemsFromToolStripItem(itm, ref itmDropDownItems);
                        dropDownItems[counter].MenuItems.AddRange(itmDropDownItems);
                    }
                }
                //else if (itm is ToolStripSeparator)
                //{
                //    dropDownItems[counter] = new ToolStripSeparator();
                //}
            }
        }

        /// <summary>
        ///  Activates the Wisej.Web.ToolStripItem when it is clicked with the mouse.
        /// </summary>
        //private void menuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        //{
        //    ((ToolStripMenuItem)e.ClickedItem.Tag).PerformClick();
        //}

        /// <summary>
        /// Closing-Event-Handler for MenuStrip
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void menuStrip_Closed(object sender, ToolStripDropDownClosedEventArgs e)
        //{
        //    if (FCDropDownClosedEventHandler != null)
        //    {
        //        FCDropDownClosedEventHandler(this, e);
        //    }
        //}

        /// <summary>
        /// Send the Click event if the Menu is in opening-state after clicking on one Menu and moving the mouse to another one, without clicking
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FCToolStripMenuItem_DropDownOpening(object sender, System.EventArgs e)
        {
            //CHE: avoid twice click event - on winform menu is not automatically opened on moving cursor
            //if (!this.IsOnDropDown)
            //{
            //    ((ToolStripMenuItem)sender).PerformClick();
            //}
        }
        #endregion
    }
}
