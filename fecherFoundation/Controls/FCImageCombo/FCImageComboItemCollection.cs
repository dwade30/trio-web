﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    public class FCImageComboItemCollection
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        List<FCImageComboItem> items;
        private FCImageCombo imageCombo;

        #endregion

        #region Constructors

        public FCImageComboItemCollection(FCImageCombo imageCombo)
        {
            this.items = new List<FCImageComboItem>();
            this.imageCombo = imageCombo;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public FCImageComboItem this[int index]
        {
            get
            {
                return items[index];
            }
        }

        public FCImageComboItem this[string key]
        {
            get
            {
                return items.Find(delegate(FCImageComboItem target)
                {
                    return target.Key == key;
                });
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="key"></param>
        /// <param name="text"></param>
        /// <param name="image"></param>
        /// <param name="selImage"></param>
        /// <param name="indentation"></param>
        /// <returns></returns>
        public FCImageComboItem Add(int index, string key, string text, string image, object selImage = null, object indentation = null)
        {
            FCImageComboItem item = new FCImageComboItem();
            item.Text = text;
            item.Key = key;
            item.ImageIndex = this.imageCombo.ImageList.Images.IndexOfKey(image);
            this.items.Insert(index, item);
			this.imageCombo.Items.Insert(index, item);
			item.ImageCombo = imageCombo;
            return item;
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
