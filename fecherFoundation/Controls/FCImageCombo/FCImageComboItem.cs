using System;
using System.Drawing;

namespace fecherFoundation
{

	public class FCImageComboItem : object
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        // forecolor: transparent = inherit
        private Color forecolor = Color.FromKnownColor(KnownColor.Transparent);
        private bool mark = false;
        private int imageindex = -1;
        private object tag = null;
        private string text = null;
        private string key = null;
        private bool selected = false;
        
        #endregion

        #region Constructors

        // constructors
        public FCImageComboItem()
        {
        }

        public FCImageComboItem(string Text)
        {
            text = Text;
        }

        public FCImageComboItem(string Text, int ImageIndex)
        {
            text = Text;
            imageindex = ImageIndex;
        }

        public FCImageComboItem(string Text, int ImageIndex, bool Mark)
        {
            text = Text;
            imageindex = ImageIndex;
            mark = Mark;
        }

        public FCImageComboItem(string Text, int ImageIndex, bool Mark, Color ForeColor)
        {
            text = Text;
            imageindex = ImageIndex;
            mark = Mark;
            forecolor = ForeColor;
        }

        public FCImageComboItem(string Text, int ImageIndex, bool Mark, Color ForeColor, object Tag)
        {
            text = Text;
            imageindex = ImageIndex;
            mark = Mark;
            forecolor = ForeColor;
            tag = Tag;
        }
        
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties
        
        // forecolor
		public Color ForeColor 
		{
			get 
			{
				return forecolor;
			}
			set
			{
				forecolor = value;
			}
		}

		// image index
		public int ImageIndex 
		{
			get 
			{
				return imageindex;
			}
			set 
			{
				imageindex = value;
			}
		}

		// mark (bold)
		public bool Mark
		{
			get
			{
				return mark;
			}
			set
			{
				mark = value;
			}
		}

		// tag
		public object Tag
		{
			get
			{
				return tag;
			}
			set
			{
				tag = value;
			}
		}

		// item text
		public string Text 
		{
			get
			{
				return text;
			}
			set
			{
				text = value;
			}
		}

        // item key
        public string Key
        {
            get
            {
                return key;
            }
            set
            {
                key = value;
            }
        }

        public bool Selected
        {
            get
            {
                return selected;
            }
            set
            {
                selected = value;
            }
        }

		public int Index
		{
			get
			{
				if (ImageCombo.Items.Contains(this))
				{
					return ImageCombo.Items.IndexOf(this);
				}
				return -1;
			}
		}
        #endregion

        #region Internal Properties
		internal FCImageCombo ImageCombo { get; set; }
        #endregion

        #region Public Methods

        // ToString() should return item text
        public override string ToString()
        {
            return text;
        }
        
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion

	}

}
