﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;

namespace fecherFoundation
{
    [Editor("System.Windows.Forms.Design.StringCollectionEditor, System.Design, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", typeof(UITypeEditor)), ListBindable(false)]
    public class FCListViewComboBoxItems : CollectionBase
    {
        #region Members
        private ListViewComboBox comboBox = null;
        private FCListViewComboBox myComboBox = null;
        #endregion

        #region Constructor
        public FCListViewComboBoxItems(ListViewComboBox comboBox)
        {
            this.comboBox = comboBox;
            this.myComboBox = comboBox as FCListViewComboBox;
        }
        #endregion

        #region Indexes
        public object this[int index]
        {
            get
            {
                return base.List[index];
            }

            set
            {
                base.List[index] = value;
            }
        }
        #endregion

        #region Properties
        #endregion

        #region Methods
        private ListViewItem GetNewItem(object item)
        {
            ListViewItem viewItem;
            string token = Convert.ToString(item);
            string value = FCListViewComboBox.ExtractValue(token);
            string text = FCListViewComboBox.ExtractText(token);
            string displayColumn = FCListViewComboBox.ExtractDisplayColumn(token);
            int nDisplayColumn = 0;

            Int32.TryParse(displayColumn, out nDisplayColumn);
            if(nDisplayColumn != 0)
            {
                this.myComboBox.DisplayColumn = nDisplayColumn;
            }
            
            
            // create substrings
            string[] subItems = text.Split('\t');
            if (this.comboBox.Columns.Count != subItems.Length)
            {
                for (int i = this.comboBox.Columns.Count; i < subItems.Length; i++)
                {
                    this.comboBox.Columns.Add("Column" + i.ToString());
                }
            }
            viewItem = new ListViewItem(subItems[0]);
            for (int i = 0; i < subItems.Length; i++)
            {
                //adjust column width to fit the subitem text
                int itemWidth = System.Windows.Forms.TextRenderer.MeasureText(subItems[i], comboBox.Font).Width;
                if (this.comboBox.Columns[i].Width < itemWidth)
                {
                    this.comboBox.Columns[i].Width = itemWidth;
                }

                //FC:FINAL:MSH - issue #1677: height calculating moved here for calculating and applying max height value(height will be 0 if first item is an empty string)
                //int rowHeight = 0;
                //if (!string.IsNullOrEmpty(subItems[i]))
                //{
                //    rowHeight = System.Windows.Forms.TextRenderer.MeasureText(subItems[i], comboBox.Font).Height + 3;
                //} else
                //{
                //    //FC:FINAL:DSE:#i1719 Row should be visible even if item is empty
                //    rowHeight = System.Windows.Forms.TextRenderer.MeasureText("A", comboBox.Font).Height + 3;
                //}
                if (i != 0)
                {
                    viewItem.SubItems.Add(subItems[i]);
                    //if (viewItem.RowHeight < rowHeight)
                    //{
                    //    viewItem.RowHeight = rowHeight;
                    //}
                }
                //else
                //{
                //    viewItem.RowHeight = rowHeight;
                //}
            }
            viewItem.Name = value;
            //PPJ:FINAL:BCU #i1049 - calculate row height based on font
            //viewItem.RowHeight = System.Windows.Forms.TextRenderer.MeasureText(viewItem.Text, comboBox.Font).Height + 3;
            //AM:HARRIS:#1907 - set fixed row height
            viewItem.RowHeight = 26;
            return viewItem;
        }

        public bool Contains(object item)
        {
            return this.InnerList.OfType<object>().Where(origItem => Convert.ToString(origItem) == Convert.ToString(item)).FirstOrDefault() != null;
        }


        public void Remove(object item)
        {
            this.comboBox.Items.RemoveByKey(Convert.ToString(item));
        }

        public int Add(object item)
        {
            this.comboBox.Items.Add(GetNewItem(item));
            this.myComboBox.UpdateItemsOnClient = true;
            return InnerList.Add(item);
        }

        public void AddRange(object[] items)
        {
            foreach (object item in items)
            {
                this.Add(item);
            }
        }

        public int IndexOf(object item)
        {
            return this.InnerList.IndexOf(this.InnerList.OfType<object>().Where(origItem => Convert.ToString(origItem) == Convert.ToString(item)).FirstOrDefault());
        }

        protected override void OnInsertComplete(int index, object value)
        {
            base.OnInsertComplete(index, value);
            this.comboBox.Items.Insert(index, GetNewItem(value));
            this.myComboBox.UpdateItemsOnClient = true;
        }

        protected override void OnRemoveComplete(int index, object value)
        {
            base.OnRemoveComplete(index, value);
            this.comboBox.Items.RemoveAt(index);
            this.myComboBox.UpdateItemsOnClient = true;
        }

        protected override void OnClearComplete()
        {
            base.OnClearComplete();
            this.comboBox.Items.Clear();
            this.myComboBox.UpdateItemsOnClient = true;
        }

        protected override void OnSetComplete(int index, object oldValue, object newValue)
        {
            base.OnSetComplete(index, oldValue, newValue);
            this.comboBox.Items[index].Text = Convert.ToString(newValue);
            this.myComboBox.UpdateItemsOnClient = true;
        }

        public void Insert(int index, object item)
        {
            InnerList.Insert(index, Convert.ToString(item));
            this.comboBox.Items.Insert(index, GetNewItem(item));
            this.myComboBox.UpdateItemsOnClient = true;
        }
        #endregion
    }
}
