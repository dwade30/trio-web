﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;
using fecherFoundation.Extensions;
using System.Collections;
using System.Drawing.Design;

namespace fecherFoundation
{
    /// <summary>
    /// VB.ComboBox
    /// </summary>
    public partial class FCListViewComboBox : ListViewComboBox 
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private bool locked = false;
        private bool localDisabled = false;

        private bool visible = true;

        private dynamic dataSourceOriginal;

        private bool inLeave = false;

        private Dictionary<object, int> itemData = new Dictionary<object, int>();

        private ToolTip toolTip = new ToolTip();
        private Color _borderColor = Color.Black;
        //private ButtonBorderStyle _borderStyle = ButtonBorderStyle.Solid;
        private static int WM_PAINT = 0x000F; 
        private static int CB_ADDSTRING = 0x0143;
        private static int CB_INSERTSTRING = 0x014A;
        private bool keydowncombo = false;
        private bool textChangedRaised = false;

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        /// <summary>
        /// During the execution of the code, by setting properties or by calling methods, events may be raised that in different contexts must not be raised
        /// Use the disableEvents flag to disable events in the particular contexts
        /// </summary>
        private bool disableEvents = false;
        private string dataField = "";
        private string dataMember = "";
        private DataTable dataTable { get; set; }
        private AppearanceConstants appearance = AppearanceConstants.dbl3D;
        private int newIndex = -1;

        //FC:FINAL:DSE SelctedValue can be set even if ValueMember is null
        private object selectedValue = 0;
        private MatchEntryConstants matchEntry = MatchEntryConstants.dblBasicMatching;
        private FCListViewComboBoxItems items = null;
        private int displayColumn = 0;
        private DataGridViewCell owningCell = null;
        private object dataSource = null;

        //private string keyPressSearchString = "";
        //private long lastKeyPressTicks = DateTime.Now.Ticks;
        ////max difference between two keypress in miliseconds
        //private const long keyPressMaxMillisecondsDifference = 1500;
        #endregion

        #region Constructors

        public FCListViewComboBox()
            : base()
        {
            InitializeComponent();
            this.AutoSize = false;
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            //CHE: set default value
            this.WhatsThisHelpID = 0;
            this.Appearance = AppearanceConstants.dbl3D;
            this.ToolTipText = "";
            this.DataField = "";
            this.DataMember = "";
            this.Text = "";
            this.DisplayColumn = 0;
            //CHE: in Winform if you drag a ComboBox on a form the property FormattingEnabled is true, but same property is false if you instantiate a ComboBox from code;
            //if false TextChanged is not triggered when you select an item from list in case of a combo with DataSource as DataTable and ValueMember/DisplayMember set
            this.FormattingEnabled = true;
            this.DropDownStyle = ComboBoxStyle.DropDownList;
            this.items = new fecherFoundation.FCListViewComboBoxItems(this);
            this.ListView.Columns.Add("Column");
            this.ListView.Columns[0].Width = this.Width - 10;
            this.ListView.Columns[0].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
            this.ListView.HeaderStyle = ColumnHeaderStyle.None;
            this.ListView.Columns[0].Resizable = false;

            this.ListView.ItemSelectionChanged -= ListView_ItemSelectionChanged;
            this.ListView.ItemSelectionChanged += ListView_ItemSelectionChanged;
            this.ListView.ItemClick += ListView_ItemClick;
            //FC:FINAL:SBE - Harris #i2364 - remove resize of columns because it will generate a stack overflow on client side
            //size of the dropdown is calculated also in FCGrid, OnCellBeginEdit, when the editor is a FCListViewComboBox
            //this.ListView.SizeChanged += ListView_SizeChanged;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums

        public enum AppearanceConstants : int
        {
            dblFlat = 0,
            dbl3D = 1
        }

        public enum MatchEntryConstants : int
        {
            dblBasicMatching = 0,
            dblExtendedMatching = 1
        }

        #endregion

        #region Properties

        //public new string SelectedItem
        //{
        //    get
        //    {
        //        if(base.SelectedItem == null)
        //        {
        //            return String.Empty;
        //        } else
        //        {
        //            return base.SelectedItem.Text;
        //        }
        //    }
        //}
        public new object SelectedItem
        {
            get { return base.SelectedItem; }
            set { base.SelectedItem = base.Items.Where(item => item.Text == Convert.ToString(item)).FirstOrDefault(); }
        }



        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content), MergableProperty(false)]
        public new FCListViewComboBoxItems Items
        {
            get
            {
                return items;
            }
        }


        /// <summary>
        /// Returns/sets a value indicating that data in a control has changed by some process other than by retrieving data from the current record.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool DataChanged { get; set; }

        /// <summary>
        /// use internal flag to set Visible regardless the control is shown currently or not
        /// </summary>
        [DefaultValue(true)]
        public new bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
                base.Visible = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(false)]
        public bool Locked
        {
            get
            {
                return locked;
            }
            set
            {
                locked = value;
                if (locked)
                {
                    //CHE: cannot set ComboBox readonly, made disabled instead
                    this.Enabled = false;
                    this.localDisabled = true;
                }
                else if (localDisabled)
                {
                    this.Enabled = true;
                    this.localDisabled = false;
                }
            }
        }

        /// <summary>
        /// Returns/sets an associated context number for an object.
        /// </summary>
        [DefaultValue(0)]
        public int WhatsThisHelpID
        {
            // TODO:CHE
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ListCount
        {
            get
            {
                return this.Items.Count;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ListIndex
        {
            get
            {
                return this.SelectedIndex;
            }
            set 
            {
                this.SelectedIndex = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public ObjectCollection List
        {
            get
            {
                return null;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
        /// Returns or sets a ToolTip.
        /// Syntax
        /// object.ToolTipText [= string]
        /// The ToolTipText property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// string	A string associated with an object in the Applies To list. that appears in a small rectangle below the object when the user's cursor hovers over the object at run time for about one second.
        /// Remarks
        /// If you use only an image to label an object, you can use this property to explain each object with a few words.
        /// At design time you can set the ToolTipText property string in the control's properties dialog box.
        /// For the Toolbar and TabStrip controls, you must set the ShowTips property to True to display ToolTips.
        /// </summary>
        [DefaultValue("")]
        public string ToolTipText
        {
            get
            {
                return toolTip.GetToolTip(this);
            }
            set
            {
                toolTip.SetToolTip(this, value);
            }
        }

        /// <summary>
        /// SelLength returns or sets the number of characters selected.
        /// SelStart returns or sets the starting point of text selected; indicates the position of the insertion point if no text is selected.
        /// SelText returns or sets the string containing the currently selected text; consists of a zero-length string ("") if no characters are selected.
        /// These properties aren't available at design time.
        /// Syntax
        /// object.SelLength [= number]
        /// object.SelStart [= index]
        /// object.SelText [= value]
        /// The SelLength, SelStart, and SelText property syntaxes have these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// number	A numeric expression specifying the number of characters selected. For SelLength and SelStart, the valid range of settings is 0 to text length the total number of characters 
        /// in the edit area of a ComboBox or TextBox control.
        /// index	A numeric expression specifying the starting point of the selected text, as described in Settings.
        /// value	A string expression containing the selected text.
        /// Remarks
        /// Use these properties for tasks such as setting the insertion point, establishing an insertion range, selecting substrings in a control, or clearing text. Used in conjunction with 
        /// the Clipboard object, these properties are useful for copy, cut, and paste operations.
        /// When working with these properties:
        /// Setting SelLength less than 0 causes a run-time error.
        /// Setting SelStart greater than the text length sets the property to the existing text length; changing SelStart changes the selection to an insertion point and sets SelLength to 0.
        /// Setting SelText to a new value sets SelLength to 0 and replaces the selected text with the new string.
        /// </summary>
        /// <returns></returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelLength
        {
            get
            {
                return base.SelectionLength;
            }
            set
            {
                base.SelectionLength = value;
            }
        }

        /// <summary>
        /// SelLength returns or sets the number of characters selected.
        /// SelStart returns or sets the starting point of text selected; indicates the position of the insertion point if no text is selected.
        /// SelText returns or sets the string containing the currently selected text; consists of a zero-length string ("") if no characters are selected.
        /// These properties aren't available at design time.
        /// Syntax
        /// object.SelLength [= number]
        /// object.SelStart [= index]
        /// object.SelText [= value]
        /// The SelLength, SelStart, and SelText property syntaxes have these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// number	A numeric expression specifying the number of characters selected. For SelLength and SelStart, the valid range of settings is 0 to text length the total number of characters 
        /// in the edit area of a ComboBox or TextBox control.
        /// index	A numeric expression specifying the starting point of the selected text, as described in Settings.
        /// value	A string expression containing the selected text.
        /// Remarks
        /// Use these properties for tasks such as setting the insertion point, establishing an insertion range, selecting substrings in a control, or clearing text. Used in conjunction with 
        /// the Clipboard object, these properties are useful for copy, cut, and paste operations.
        /// When working with these properties:
        /// Setting SelLength less than 0 causes a run-time error.
        /// Setting SelStart greater than the text length sets the property to the existing text length; changing SelStart changes the selection to an insertion point and sets SelLength to 0.
        /// Setting SelText to a new value sets SelLength to 0 and replaces the selected text with the new string.
        /// </summary>
        /// <returns></returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelStart
        {
            get
            {
                return base.SelectionStart;
            }
            set
            {
                base.SelectionStart = value;
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the font used to display text in a control or in a run-time drawing or printing operation.
        /// Note   The FontName property is included for use with the CommonDialog control and for compatibility with earlier versions of Visual Basic. For additional functionality, 
        /// use the new Font object properties (not available for the CommonDialog control).
        /// Syntax
        /// object.FontName [= font]
        /// The FontName property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// font	A string expression specifying the font name to use.
        /// Remarks
        /// The default for this property is determined by the system. Fonts available with Visual Basic vary depending on your system configuration, display devices, and printing 
        /// devices. Font-related properties can be set only to values for which fonts exist.
        /// In general, you should change FontName before setting size and style attributes with the FontSize, FontBold, FontItalic, FontStrikethru, and FontUnderline properties.
        /// Note   At run time, you can get information on fonts available to the system through the FontCount and Fonts properties.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string FontName
        {
            get
            {
                return this.Font.Name;
            }
            set
            {
                this.SetFontName(value);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontBold
        {
            get
            {
                return this.Font.Bold;
            }
            set
            {
                this.SetFontBold(value);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontItalic
        {
            get
            {
                return this.Font.Italic;
            }
            set
            {
                this.SetFontItalic(value);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontUnderline
        {
            get
            {
                return this.Font.Underline;
            }
            set
            {
                this.SetFontUnderline(value);
            }
        }

        /// <summary>
        /// Gets or sets the size of the font that is used for text in a run-time printing operation.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float FontSize
        {
            get
            {
                return this.graphicsFactory.FontSize;
            }
            set
            {
                this.SetFontSize(value);
                this.graphicsFactory.FontSize = value;
            }
        }

        //BAN - #i170 - in VB6, the returned value for BoundText is -1 if the ComboBox has nothing selected and the text is empty. In .NET the SelectedValue returns null in this case. Return -1 if nothing is selected
        /// <summary>
        /// Gets/sets the value of the member property specified by the <see cref="P:Wisej.Web.ListControl.ValueMember"/>
        /// property.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new object SelectedValue
        {
            get
            {
                if (base.SelectedValue == DBNull.Value && string.IsNullOrEmpty(this.Text))
                {
                    return -1;
                }
                else if (base.SelectedValue == null && string.IsNullOrEmpty(this.Text))
                {
                    //FC:FINAL:DSE SelctedValue can be set even if ValueMember is null
                    return selectedValue;
                }

                return base.SelectedValue;
            }
            set
            {
                // PJ:FINAL:JSP   Test if Value Member is null to prevent exception
                if (!string.IsNullOrEmpty(base.ValueMember))
                {
                    base.SelectedValue = value;
                    if (Convert.ToInt32(value) == -2 && base.SelectedValue != null)
                    {
                        base.SelectedValue = value;
                    }
                }
                //FC:FINAL:DSE SelctedValue can be set even if ValueMember is null
                else
                {
                    selectedValue = value;
                }
            }
        }


        /// <summary>
        /// Gets/sets the Datafield property of the control
        /// </summary>
        [DefaultValue("")]
        public string DataField
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
                if (this.DataBindings.Count > 0)
                {
                    return;
                }
                if (!string.IsNullOrEmpty(value))
                {
                    BindingSource bs = new BindingSource();
                    bs.DataSource = this.dataTable;
                    if (this.dataTable != null)
                    {
                        this.DataBindings.Add(BindingPropertyName, bs, value);
                    }
                    this.DataSourceOriginal.AddBindingSource(this.DataMember, bs, this);
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string BindingPropertyName
        {
            get;
            set;
        }


        /// <summary>
        /// Gets/sets a value that describes the DataMember property of a data connection.
        /// </summary>
        [DefaultValue("")]
        public string DataMember
        {
            get
            {
                return dataMember;
            }
            set
            {
                dataMember = value;
            }
        }

        /// <summary>
        /// DataSource property of VB6 control
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public dynamic DataSourceOriginal
        {
            set
            {
                if (value == null)
                {
                    return;
                }
                DataTable data = new DataTable();
                string dataMember = DataMember;
                value.Class_GetDataMember(ref dataMember, ref data);
                this.dataTable = data;
                this.dataSourceOriginal = value;
            }
            get
            {
                return this.dataSourceOriginal;
            }
        }

        /// <summary>
        /// Gets/sets the text associated with this control.
        /// </summary>
        [DefaultValue("")]
        public new string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                DataTable source = this.DataSource as DataTable;
                string val = value;
                // Try to set text. if val is not in the DataSource new record must be added to DataSource 
                base.Text = val;
                if (base.Text != val && !string.IsNullOrEmpty(val) && this.DataSource != null && !string.IsNullOrEmpty(this.DisplayMember))
                {
                    DataRow dr = source.NewRow();
                    dr[this.DisplayMember] = val;
                    source.Rows.Add(dr);
                    base.Text = val;
                }

                //CHE: in VB6 if you have DataCombo with recordset containing (displaymember = null, valueMember = something) it will not be bound to that row when combo.Text is set to ""  
                //CHE: if Text was not found then leave it without any selection
                if (this is FCDataCombo)
                {
                    //DSE Change implementation to ListViewComboBox to enable columns
                    //if ((this.SelectedItem != null && this.SelectedItem is DataRowView && ((DataRowView)this.SelectedItem)[this.DisplayMember] == DBNull.Value && base.Text == "") ||
                    //    (base.Text != val))
                    //{
                    //    this.SelectedIndex = -1;
                    //}
                    if ((this.SelectedItem != null && base.SelectedItem.SubItems.ContainsKey(this.DisplayMember) && base.SelectedItem.SubItems[this.DisplayMember].Text == String.Empty && base.Text == "") ||
                        (base.Text != val))
                    {
                        this.SelectedIndex = -1;
                    }
                }
                //CHE: simulate VB6 behaviour, exception is received when trying to set a ComboBox Text property to some value that is 
                //not in the list (in case of DropDownList)
                if (this.DropDownStyle == ComboBoxStyle.DropDownList && base.Text != val && this.SelectedIndex >= 0)
                {
                    throw new System.ComponentModel.Win32Exception(383, "'Text' property is read-only");
                }
            }
        }

        /// <summary>
        /// Gets/sets the DataSource associated with this control.
        /// </summary>
        public new object DataSource
        {
            get
            {
                return this.dataSource;
            }
            set
            {
                //BAN - in VB6, when setting the DataSource, the Change event is not raised. Disable the SelectedIndexChanged event while setting the DataSource
                disableEvents = true;
                //BAN - clear value member and data member before setting the DataSource
                base.ValueMember = "";
                base.DisplayMember = "";
                //BAN - the data source for the combo box must be a clone, otherwise, the actions on the DataTable will influence the values in the ComboBox
                IFCDataTable source = value as IFCDataTable;
                if (source != null)
                {
                    this.dataSource = source.CloneRows();
                }
                else
                {
                    this.dataSource= value;
                }
                if(value is IList)
                {
                    this.Items.Clear();
                    foreach(object item in value as IList)
                    {
                        this.Items.Add(item);
                    }
                }
                ResetSelectedValue();
                disableEvents = false;
            }
        }

        /// <summary>
        /// Gets/sets the property to use as the actual value for the items in the <see cref="T:Wisej.Web.ListControl"/>.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new string ValueMember
        {
            get
            {
                return base.ValueMember;
            }
            set
            {
                //BAN - in VB6, when setting the value member, the Change event is not raised. Disable the SelectedIndexChanged event while setting the value member
                disableEvents = true;
                base.ValueMember = value;
                ResetSelectedValue();
                disableEvents = false;
            }
        }

        /// <summary>
        /// Gets/sets the property to display for this <see cref="T:Wisej.Web.ListControl"/>.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new string DisplayMember
        {
            get
            {
                return base.DisplayMember;
            }
            set
            {
                //BAN - in VB6, when setting the display member, the Change event is not raised. Disable the SelectedIndexChanged event while setting the display member
                disableEvents = true;
                base.DisplayMember = value;
                ResetSelectedValue();
                disableEvents = false;
            }
        }

        /// <summary>
        /// Gets/sets the appearance of the control
        /// </summary>
        [DefaultValue(AppearanceConstants.dbl3D)]
        public AppearanceConstants Appearance
        {
            get
            {
                return this.appearance;
            }
            set
            {
                this.appearance = value;
                // TODO
                //if (this.appearance == AppearanceConstants.dbl3D)
                //{
                //    this.FlatStyle = Wisej.Web.FlatStyle.Standard;
                //}
                //else
                //{
                //    this.FlatStyle = Wisej.Web.FlatStyle.Flat;
                //}
            }
        }

        [Obsolete("The behaviour is by default in .NET")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool MatchedWithList { get; set; }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public object Bindings
        {
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Length
        {
            get
            {
                return itemData.Count;
            }
        }

        /// <summary>
        /// Returns the index of the most recently added item
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int NewIndex
        {
            get
            {
                return newIndex;
            }
        }

        #endregion

        #region Internal Properties

        public bool DisableEvents
        {
            get
            {
                return disableEvents;
            }
        }

        internal bool UpdateItemsOnClient { get; set; }

        internal int DisplayColumn
        {
            get
            {
                return displayColumn;
            }
            set
            {
                displayColumn = value;
            }
        }

        internal DataGridViewCell OwningCell
        {
            set
            {
                this.owningCell = value;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
        /// User-defined conversion from FCComboBox to string
        /// </summary>
        /// <param name="combo"></param>
        /// <returns></returns>
        public static implicit operator string(FCListViewComboBox combo)
        {
            return combo.Text;
        }

        /// <summary>
        /// Clears the contents of a ListBox, ComboBox, or the system Clipboard.
        /// Syntax
        /// object.Clear
        /// The object placeholder represents an object expression that evaluates to an object in the Applies To list.
        /// Remarks
        /// A ListBox or ComboBox control bound to a Data control doesn't support the Clear method.
        /// </summary>
        public void Clear()
        {
            //FC:FINAL:SBE - Harris #207 - SelectedIndexChanged is not triggered on items clear
            bool currentDisableEvents = this.disableEvents;
            this.disableEvents = true;
            this.Items.Clear();
            this.disableEvents = currentDisableEvents;
            //chear text too, as in VB6
            if (this.DropDownStyle != ComboBoxStyle.DropDownList)
                this.Text = "";
        }

        /// <summary>
        /// Adds an item to a ListBox or ComboBox control or adds a row to a MS Flex Grid control. Doesn't support named arguments.
        /// Syntax
        /// object.AddItem item, index
        /// The AddItem method syntax has these parts:
        /// Part	Description
        /// object	Required. An object expression that evaluates to an object in the Applies To list.
        /// item	Required. string expression specifying the item to add to the object. For the MS Flex Grid control only, use the tab character (character code 09) to separate multiple strings you 
        /// want to insert into each column of a newly added row.
        /// index	Optional. Integer specifying the position within the object where the new item or row is placed. For the first item in a ListBox or ComboBox control or for the first row in a 
        /// MS Flex Grid control, index is 0.
        /// Remarks
        /// If you supply a valid value for index, item is placed at that position within the object. If index is omitted, item is added at the proper sorted position (if the Sorted property is set to True)
        /// or to the end of the list (if Sorted is set to False).
        /// A ListBox or ComboBox control that is bound to a Data control doesn't support the AddItem method.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"></param>
        public void AddItem(string item, int index = -1)
        {
            int newIndex = -1;
            
            if (index == -1)
            {
                newIndex = this.Items.Add(item);
                if(this.Items.Count == 1)
                {
                    // was the first item selected...
                    this.OnSelectedIndexChanged(EventArgs.Empty);
                    this.UpdateText();
                }
            }
            else
            {
                this.Items.Insert(index, item);
                newIndex = index;
            }
            this.newIndex = newIndex;
        }

        /// <summary>
        /// Gets the ItemData stored for the ComboBox at index
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>The value.</returns>
        public int ItemData(int index)
        {
            if (index >= this.Items.Count)
            {
                return 0;
            }

            //CHE - for itemdata the key is the index not the item text
            object item = index; // this.Items[index];
            if (!this.itemData.ContainsKey(item))
            {
                return 0;
            }

            return this.itemData[item];
        }

        /// <summary>
        /// Sets the ItemData stored for the ComboBox at index
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="value">The value.</param>
        public void ItemData(int index, int value)
        {
            if (index >= this.Items.Count)
            {
                return;
            }

            //CHE - for itemdata the key is the index not the item text
            object item = index; //this.Items[index];

            if (this.itemData.ContainsKey(item))
            {
                this.itemData[item] = value;
            }
            else
            {
                this.itemData.Add(item, value);
            }
        }

        /// <summary>
        /// Items data.
        /// </summary>
        /// <param name="objComboBox">The combo box.</param>
        public void ItemData(int[] arrValues)
        {
            //set the item data in the indexer
            this.itemData.Clear();

            // Add items data
            if (arrValues != null)
            {
                for (int intCounter = 0; intCounter < arrValues.Length; intCounter++)
                {
                    this.itemData.Add(intCounter, arrValues[intCounter]);
                }
            }
        }

        /// <summary>
        /// Sets the add value.
        /// </summary>
        /// <param name="objComboBox">The obj combo box.</param>
        /// <param name="intIndex">Index of the int.</param>
        /// <param name="objValue">The obj value.</param>
        public void SetAddValue(int intIndex, object objValue)
        {
            //If there is an item in the that index
            if (this.Items.Count <= intIndex)
            {
                //Add an item
                this.Items.Add(objValue);
            }
            else
            {
                //Set the value to the index
                this.Items[intIndex] = new ListViewItem(objValue == null ? String.Empty : objValue.ToString());
            }
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods

        protected override void OnLeave(EventArgs e)
        {
            //CHE: do not call lost focus when form is closing
            FCForm form = this.FindForm() as FCForm;
            if (inLeave || form == null || !form.IsLoaded)
            {
                return;
            }

            inLeave = true;
            base.OnLeave(e);
            inLeave = false;
        }

        protected override void OnTextChanged(EventArgs e)
        {
            //CHE: do not raise while in InitializeComponent
            Form f = this.FindForm();
            if (f == null || (!f.Created && !((FCForm)f).IsLoaded))
            {
                return;
            }
            base.OnTextChanged(e);
            textChangedRaised = true;
            this.DataChanged = true;
        }

        protected override void OnValidated(EventArgs e)
        {
            base.OnValidated(e);
            this.DataChanged = false;
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            if (!disableEvents)
            {
                //CHE: ComboBox does not clear when you set SelectedIndex to - 1
                //https://support.microsoft.com/en-us/kb/327244
                if (this.DataSource != null && this.SelectedIndex == -1)
                {
                    base.OnSelectedIndexChanged(e);
                    if (this.SelectedIndex != -1)
                    {
                        this.SelectedIndex = -1;
                    }
                }
                else
                {
                    base.OnSelectedIndexChanged(e);
                }


                //for ComboBoxStyle.DropDownList TextChanged is not raised in .NET when key is pressed
                //as it is in VB, force event
                if (keydowncombo)
                {
                    keydowncombo = false;
                    this.OnTextChanged(e);
                }

            }
        }

        protected override void OnSelectedValueChanged(EventArgs e)
        {
            if (!disableEvents)
            {
                base.OnSelectedValueChanged(e);

                //CHE: force TextChanged needed when control becomes visible and Binding gets active
                if (!textChangedRaised)
                {
                    this.OnTextChanged(e);
                    textChangedRaised = false;
                }
            }
        }

        // TODO
        //protected override void WndProc(ref Message m)
        //{
        //    base.WndProc(ref m);

        //    //CHE: draw border for ComboBox with Appearance 2D
        //    if (m.Msg == WM_PAINT && this.Appearance == AppearanceConstants.dblFlat)
        //    {
        //        using (Graphics g = Graphics.FromHwnd(Handle))
        //        {
        //            Rectangle rect = new Rectangle(0, 0, Width, Height);
        //            ControlPaint.DrawBorder(g, rect, _borderColor, _borderStyle);
        //        }
        //    }
        //    //AM: set the newIndex when an item is added to the combo
        //    else if (m.Msg == CB_ADDSTRING || m.Msg == CB_INSERTSTRING)
        //    {
        //        newIndex = m.Result.ToInt32();
        //    }
        //}

        ////FC:FINAL:SBE - #902 - select item on keypress
        //protected override void OnKeyPress(KeyPressEventArgs e)
        //{
        //    base.OnKeyPress(e);
        //    //if the user can type in the combobx, don't search for item
        //    if (this.DropDownStyle != ComboBoxStyle.DropDownList)
        //    {
        //        return;
        //    }

        //    //if the user type fast, combine the new key with the previous search key
        //    long currentTicks = DateTime.Now.Ticks;
        //    string currentKey = e.KeyChar.ToString().ToLower();
        //    TimeSpan diff = new TimeSpan(currentTicks - lastKeyPressTicks);
        //    if (diff.TotalMilliseconds <= keyPressMaxMillisecondsDifference)
        //    {
        //        keyPressSearchString = keyPressSearchString + e.KeyChar.ToString().ToLower();
        //    }
        //    else
        //    {
        //        keyPressSearchString = currentKey;
        //    }

        //    int startIndex = -1;
        //    if (this.SelectedIndex >= 0)
        //    {
        //        startIndex = this.SelectedIndex;
        //    }
        //    bool itemFound = false;
        //    this.SelectItemOnKeyPress(keyPressSearchString, startIndex, ref itemFound);
        //    //if item was not found with the combinet search key, then search only by last key pressed
        //    if (!itemFound && keyPressSearchString.Length > 1)
        //    {
        //        this.SelectItemOnKeyPress(currentKey, startIndex, ref itemFound);
        //        keyPressSearchString = currentKey;
        //    }
        //    this.lastKeyPressTicks = currentTicks;
        //}

        //private void SelectItemOnKeyPress(string searchKey, int startIndex, ref bool itemFound)
        //{
        //    for (int i = startIndex + 1; i < base.Items.Count; i++)
        //    {
        //        ListViewItem item = base.Items[i] as ListViewItem;
        //        if (item != null && CheckItemOnKeyPress(searchKey, item))
        //        {
        //            itemFound = true;
        //            this.SelectedIndex = i;
        //            break;
        //        }
        //    }
        //    if (!itemFound)
        //    {
        //        for (int i = 0; i < startIndex; i++)
        //        {
        //            ListViewItem item = base.Items[i] as ListViewItem;
        //            if (item != null && CheckItemOnKeyPress(searchKey, item))
        //            {
        //                itemFound = true;
        //                this.SelectedIndex = i;
        //                break;
        //            }
        //        }
        //    }
        //}

        //private bool CheckItemOnKeyPress(string searchKey, ListViewItem item)
        //{
        //    if (item != null && item.Text.ToLower().StartsWith(searchKey))
        //    {
        //        return true;
        //    }
        //    return false;
        //}

        protected override void OnKeyDown(KeyEventArgs e)
        {
            keydowncombo = false;
            base.OnKeyDown(e);
            if (this.DropDownStyle == ComboBoxStyle.DropDownList)
            {
                keydowncombo = true;
            }
        }

        #endregion

        #region Private Methods

        private void ListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.DroppedDown = false;
            ////PPJ:FINAL:DSE:IN-4052 SAM_AnyEdit and SAM_Validate events should come when clicking on an item
            //if (m_modified && this.TableColumn != null)
            //{
            //    this.TableColumn.SendMessage(Sys.SAM_AnyEdit, 0, this.TableColumn.ParentTable.CurrentRow);
            //    SalWindow.FireWindowActions(this.TableColumn, Sys.SAM_Validate, 0, 0);
            //    m_validated = true;
            //}
            this.SelectAll();
            //AM:HARRIS:#i1692 - move code from ItemSelectionChanged because on ComboCloseUp event the new value is not set yet when calling TextMatrix
            //DSE save value on the owning cell
            if (this.owningCell != null)
            {
                if (ListView.SelectedIndex == -1)
                {
                    this.owningCell.Tag = null;
                }
                else
                {
                    if (this.DataSource is IList)
                    {
                        string val = Convert.ToString((this.DataSource as IList)[ListView.SelectedIndex]);
                        owningCell.Tag = FCListViewComboBox.ExtractValue(val);
                    }
                }
            }
        }

        private void ListView_ItemSelectionChanged(object sender, ItemSelectionChangedEventArgs e)
        {
            if (e.IsSelected)
            {
                UpdateText();
                OnSelectedItemChanged(EventArgs.Empty);
                OnSelectedIndexChanged(EventArgs.Empty);

                if (this.DroppedDown)
                {
                   // m_modified = true;
                   // m_validated = false;
                    //this.SendMessage(Sys.SAM_Click, 0, 0);
                    this.OnClick(EventArgs.Empty);
                }
            }
        }

        private void ListView_SizeChanged(object sender, EventArgs e)
        {
            if (this.Columns.Count == 1)
            {
                this.Columns[0].Width = this.Width - 20;
            }
        }

        private void ResizeDropDown()
        {
            if (this.UpdateItemsOnClient)
            {
                if (this.ListView.Columns.Count == 1)
                {
                    int width = this.Width;

                    foreach (var item in this.ListView.Items)
                    {
                        int itemWidth = System.Windows.Forms.TextRenderer.MeasureText(item.Text, this.Font).Width;
                        if (width < itemWidth)
                        {
                            width = itemWidth;
                        }
                    }
                    this.DropDownWidth = width + 20;
                    this.Columns[0].Width = width;
                }
                else
                {
                    int width = 0;
                    for(int i = 0; i < this.Columns.Count; i++)
                    {
                        width += this.Columns[i].Width;
                    }
                    //FC:FINAL:MSH - issue #1694: increase width of the last column if width of all columns less then DropDown width to avoid showing empty area (extra column)
                    if (width < this.DropDownWidth)
                    {
                        width -= this.Columns[this.Columns.Count - 1].Width;
                        this.Columns[this.Columns.Count - 1].Width = this.DropDownWidth - width - 10;
                        width += this.Columns[this.Columns.Count - 1].Width;
                    }
                    width += 10;
                    this.DropDownWidth = width;
                }
                this.UpdateItemsOnClient = false;
            }
        }

        //FC:FINAL:SBE - Harris #i2364 - remove resize of columns done OnDropDownOpen, because it will generate a stack overflow on client side
        //size of the dropdown is calculated also in FCGrid, OnCellBeginEdit, when the editor is a FCListViewComboBox
        //protected override void OnDropDown(EventArgs e)
        //{
        //    base.OnDropDown(e);
        //    this.ResizeDropDown();
        //}

        private void UpdateText()
        {
            //if (updateTextOnselection)
            //{
            //    //PPJ:FINAL:DSE:IN-3906 MaxLength property should not be considered when selecting items from dropdown
            //    this.disableMaxLength = true;
            //    if (this.ListView.SelectedIndex >= 0)
            //    {
            //        this.Text = this.ListView.FocusedItem.Text ?? string.Empty;
            //    }
            //    else
            //    {
            //        this.Text = "";
            //    }
            //    this.disableMaxLength = false;
            //}
            if (this.ListView.SelectedIndex >= 0)
            {
                if (this.displayColumn == 0)
                {
                    this.Text = this.ListView.FocusedItem.Text ?? string.Empty;
                } else
                {
                    if (this.ListView.FocusedItem != null)
                    {
                        this.Text = this.ListView.FocusedItem.SubItems[this.displayColumn].Text;
                    }
                }
            }
            else
            {
                this.Text = "";
            }
        }


        /// <summary>
        /// When changing the DataSource, ValueMember or DisplayMember, the SelectedValue will change to an existing value, thus the ComboBox will display an item
        /// In VB6 the ComboBox does not display an item unless speciffically set to do so
        /// Reset the SelectedValue by setting it to -2
        /// </summary>
        private void ResetSelectedValue()
        {
            if (!string.IsNullOrEmpty(this.ValueMember) && !string.IsNullOrEmpty(this.DisplayMember))
            {
                this.SelectedValue = -2;
            }
        }

        #endregion

        #region Internal Static Methods
        internal static string ExtractValue(string token)
        {
            string item = token.TrimStart('#');
            string id = item;

            if (item.IndexOf(";") != -1)
            {
                id = item.Substring(0, item.IndexOf(";"));
                //FC:FINAL:DSE Use string with the format "*nnn;" to the first item (where nnn is the zero-based index of the column to be displayed) to display a different column
                if (id.Contains("*"))
                {
                    id = id.Substring(0, id.IndexOf("*"));
                }
            } else
			{
				//FC:FINAL:DSE:#i2249 If token contains more than one  column, return first column as value
				id = id.Split('\t')[0];
			}
			return id;
        }

        internal static string ExtractText(string token)
        {
            string item = token.TrimStart('#');
            string value = item;

            if (item.IndexOf(";") != -1)
            {
                //FC:FINAL:MSH - issue #1677: text value will be wrong if nothing exists between ';' and '\t'. Number of items in ListViewItem will be wrong
                //value = item.Substring(item.IndexOf(";") + 1).Trim();
                value = item.Substring(item.IndexOf(";") + 1);
            }
            return value;
        }

        internal static string ExtractDisplayColumn(string token)
        {
            string displayColumn = "0";

            if (token.IndexOf("#") != -1
                //FC:FINAL:VGE - #567 There is possibility that Item 'value' will have # in it triggering first condition.
                && token.IndexOf(";") > token.IndexOf("#"))
            {
                string id = token.Substring(token.IndexOf("#") + 1, token.IndexOf(";") - token.IndexOf("#") - 1);
                string value = token.Substring(token.IndexOf(";") + 1).Trim();
                //FC:FINAL:DSE Use string with the format "*nnn;" to the first item (where nnn is the zero-based index of the column to be displayed) to display a different column
                if (id.Contains("*"))
                {
                    string valueCol = id.Substring(id.IndexOf("*") + 1);
                    id = id.Substring(0, id.IndexOf("*"));
                    if (!String.IsNullOrEmpty(id) && Information.IsNumeric(id))
                    {
                        displayColumn = valueCol;
                    }
                }
            }

            return displayColumn;
        }

        public string ComboItem(int i)
        {
            if (this.SelectedIndex != -1)
            {
                List<string> items = (List<string>)this.DataSource;
                return FCListViewComboBox.ExtractValue(items[i]);
            }
            return string.Empty;
        }

        #endregion
    }
}
