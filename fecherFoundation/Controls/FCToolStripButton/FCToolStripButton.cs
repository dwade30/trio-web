﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation
{
    public partial class FCToolStripButton : ToolStripButton
    {
        #region Constructor
        public FCToolStripButton()
        {
            InitializeComponent();
        }
        #endregion

        #region Methods

        /// <summary>
        /// Sets the index of the image.
        /// </summary>
        public void SetImageIndex(object index)
        {
            if (index != null)
            {
                string strKey = index.ToString();
                int intIndex = 0;

                // Try to treat key as numeric index
                if (Int32.TryParse(strKey, out intIndex))
                {
                    this.ImageIndex = intIndex;
                }
                else
                {
                    // Use string key
                    this.ImageKey = strKey;
                }
            }
        }

        #endregion
    }
}
