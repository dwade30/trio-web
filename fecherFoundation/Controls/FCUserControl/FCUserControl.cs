﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    public partial class FCUserControl : UserControl
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private const int WM_KEYDOWN = 0x100;
        private const int WM_CHAR = 0x102;
        private const int WM_SYSCHAR = 0x106;
        private const int WM_SYSKEYDOWN = 0x104;
        private const int WM_IME_CHAR = 0x286;

        private GraphicsFactory graphicsFactory;
        private FCGraphics fcGraphics;

        private bool resetBackgroundImage = false;
        private Image oldBackgroundImage = null;

        private DrawModeConstants drawMode = DrawModeConstants.VbCopyPen;

        #endregion

        #region Constructors
        
        public FCUserControl()
        {
            InitializeComponent();

            using (Graphics g = GetGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }

            //CHE: set default value
            this.UserControlScaleMode = ScaleModeConstants.vbTwips;
            this.UserControlDrawMode = DrawModeConstants.VbCopyPen;
            this.UserControlDrawStyle = 0;
            this.UserControlDrawWidth = 1;
            this.UserControlFillColor = 0;
            this.UserControlFillStyle = 1;
        }
        
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties


        /// <summary>
        /// Gets/sets whether keyboard events are raised for the user control before keyboard events for controls in this user control
        /// </summary>
        public bool KeyPreview { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Control Extender
        {
            get
            {
                return this;
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                return Convert.ToInt32(base.Width * fcGraphics.TwipsPerPixelX);
            }
            set
            {
                base.Width = Convert.ToInt32(value / fcGraphics.TwipsPerPixelX);
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                return Convert.ToInt32(base.Height * fcGraphics.TwipsPerPixelY);
            }
            set
            {
                base.Height = Convert.ToInt32(value / fcGraphics.TwipsPerPixelY);
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                return Convert.ToInt32(base.Top * fcGraphics.TwipsPerPixelY) - FCUtils.GetMainMenuHeightOriginal(this);
            }
            set
            {
                base.Top = Convert.ToInt32((value + FCUtils.GetMainMenuHeightOriginal(this)) / fcGraphics.TwipsPerPixelY);
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                return Convert.ToInt32(base.Left * fcGraphics.TwipsPerPixelX);
            }
            set
            {
                base.Left = Convert.ToInt32(value / fcGraphics.TwipsPerPixelX);
            }
        }

        public int UserControlWidth
        {
            get
            {
                return this.WidthOriginal;
            }
            set
            {
                this.WidthOriginal = value;
            }
        }

        public int UserControlHeight
        {
            get
            {
                return this.HeightOriginal;
            }
            set
            {
                this.HeightOriginal = value;
            }
        }

        /// <summary>
        /// Returns a handle provided by the Microsoft Windows operating environment to the device context of an object.
        /// Remarks
        /// This property is a Windows operating environment device context handle. The Windows operating environment manages the system display by assigning a device context for the Printer object and 
        /// for each form and PictureBox control in your application. You can use the hDC property to refer to the handle for an object's device context. This provides a value to pass to Windows API calls.
        /// With a CommonDialog control, this property returns a device context for the printer selected in the Print dialog box when the cdlReturnDC flag is set or an information context when the cdlReturnIC 
        /// flag is set.
        /// Note   The value of the hDC property can change while a program is running, so don't store the value in a variable; instead, use the hDC property each time you need it.
        /// The AutoRedraw property can cause the hDC property setting to change. If AutoRedraw is set to True for a form or PictureBox container, hDC acts as a handle to the device context of the persistent 
        /// graphic (equivalent to the Image property). When AutoRedraw is False, hDC is the actual hDC value of the Form window or the PictureBox container. The hDC property setting may change while the 
        /// program is running regardless of the AutoRedraw setting.
        /// If the HasDC property is set to False, a new device context will be created by the system and the value of the hDC property will change each time it is called.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Obsolete("Not supported.")]
        public int UserControlhwnd
        {
            get
            {
                return this.Handle.ToInt32();
            }
        }

        /// <summary>
        /// Return or set the number of units for the horizontal (ScaleWidth) and vertical (ScaleHeight) measurement of the interior of an object when using graphics methods or when positioning controls. 
        /// For MDIForm objects, not available at design time and read-only at run time.
        /// Syntax
        /// object.ScaleHeight [= value]
        /// object.ScaleWidth [= value]
        /// The ScaleHeight and ScaleWidth property syntaxes have these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Value	A numeric expression specifying the horizontal or vertical measurement.
        /// Remarks
        /// You can use these properties to create a custom coordinate scale for drawing or printing. For example, the statement ScaleHeight = 100 changes the units of measure of the actual interior height 
        /// of the form. Instead of the height being n current units (twips, pixels, ...), the height will be 100 user-defined units. Therefore, a distance of 50 units is half the height/width of the object, 
        /// and a distance of 101 units will be off the object by 1 unit.
        /// Use the ScaleMode property to define a scale based on a standard unit of measurement, such as twips, points, pixels, characters, inches, millimeters, or centimeters.
        /// Setting these properties to positive values makes coordinates increase from top to bottom and left to right. Setting them to negative values makes coordinates increase from bottom to top and 
        /// right to left.
        /// Using these properties and the related ScaleLeft and ScaleTop properties, you can set up a full coordinate system with both positive and negative coordinates. All four of these Scale properties 
        /// interact with the ScaleMode property in the following ways:
        /// Setting any other Scale property to any value automatically sets ScaleMode to 0. A ScaleMode of 0 is user-defined.
        /// Setting ScaleMode to a number greater than 0 changes ScaleHeight and ScaleWidth to the new unit of measurement and sets ScaleLeft and ScaleTop to 0. In addition, the CurrentX and CurrentY 
        /// settings change to reflect the new coordinates of the current point.
        /// You can also use the Scale method to set the ScaleHeight, ScaleWidth, ScaleLeft, and ScaleTop properties in one statement.
        /// Note   The ScaleHeight and ScaleWidth properties aren't the same as the Height and Width properties.
        /// For MDIForm objects, ScaleHeight and ScaleWidth refer only to the area not covered by PictureBox controls in the form. Avoid using these properties to size a PictureBox in the Resize event of 
        /// an MDIForm.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float UserControlScaleWidth
        {
            get
            {
                using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.ClientRectangle, 0, 0);
                    return fcGraphics.ScaleWidth;
                }
            }
            set
            {
                using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.ClientRectangle, 0, 0);
                    fcGraphics.ScaleWidth = value;
                }
            }
        }

        /// <summary>
        /// Return or set the number of units for the horizontal (ScaleWidth) and vertical (ScaleHeight) measurement of the interior of an object when using graphics methods or when positioning controls. 
        /// For MDIForm objects, not available at design time and read-only at run time.
        /// Syntax
        /// object.ScaleHeight [= value]
        /// object.ScaleWidth [= value]
        /// The ScaleHeight and ScaleWidth property syntaxes have these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Value	A numeric expression specifying the horizontal or vertical measurement.
        /// Remarks
        /// You can use these properties to create a custom coordinate scale for drawing or printing. For example, the statement ScaleHeight = 100 changes the units of measure of the actual interior height 
        /// of the form. Instead of the height being n current units (twips, pixels, ...), the height will be 100 user-defined units. Therefore, a distance of 50 units is half the height/width of the object, 
        /// and a distance of 101 units will be off the object by 1 unit.
        /// Use the ScaleMode property to define a scale based on a standard unit of measurement, such as twips, points, pixels, characters, inches, millimeters, or centimeters.
        /// Setting these properties to positive values makes coordinates increase from top to bottom and left to right. Setting them to negative values makes coordinates increase from bottom to top and 
        /// right to left.
        /// Using these properties and the related ScaleLeft and ScaleTop properties, you can set up a full coordinate system with both positive and negative coordinates. All four of these Scale properties 
        /// interact with the ScaleMode property in the following ways:
        /// Setting any other Scale property to any value automatically sets ScaleMode to 0. A ScaleMode of 0 is user-defined.
        /// Setting ScaleMode to a number greater than 0 changes ScaleHeight and ScaleWidth to the new unit of measurement and sets ScaleLeft and ScaleTop to 0. In addition, the CurrentX and CurrentY 
        /// settings change to reflect the new coordinates of the current point.
        /// You can also use the Scale method to set the ScaleHeight, ScaleWidth, ScaleLeft, and ScaleTop properties in one statement.
        /// Note   The ScaleHeight and ScaleWidth properties aren't the same as the Height and Width properties.
        /// For MDIForm objects, ScaleHeight and ScaleWidth refer only to the area not covered by PictureBox controls in the form. Avoid using these properties to size a PictureBox in the Resize event of 
        /// an MDIForm.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float UserControlScaleHeight
        {
            get
            {
                using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.ClientRectangle, 0, 0);
                    return fcGraphics.ScaleHeight;
                }
            }
            set
            {
                using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.ClientRectangle, 0, 0);
                    fcGraphics.ScaleHeight = value;
                }
            }
        }

        /// <summary>
        /// Return or set the horizontal (ScaleLeft) and vertical (ScaleTop) coordinates for the left and top edges of an object when using graphics methods or when positioning controls.
        /// Syntax
        /// object.ScaleLeft [= value]
        /// object.ScaleTop [= value]
        /// The ScaleLeft and ScaleTop property syntaxes have these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Value	A numeric expression specifying the horizontal or vertical coordinate. The default is 0.
        /// Remarks
        /// Using these properties and the related ScaleHeight and ScaleWidth properties, you can set up a full coordinate system with both positive and negative coordinates. These four 
        /// Scale properties interact with the ScaleMode property in the following ways:
        /// Setting any other Scale property to any value automatically sets ScaleMode to 0. A ScaleMode of 0 is user-defined.
        /// Setting the ScaleMode property to a number greater than 0 changes ScaleHeight and ScaleWidth to the new unit of measurement and sets ScaleLeft and ScaleTop to 0. The CurrentX 
        /// and CurrentY property settings change to reflect the new coordinates of the current point.
        /// You can also use the Scale method to set the ScaleHeight, ScaleWidth, ScaleLeft, and ScaleTop properties in one statement.
        /// Note   The ScaleLeft and ScaleTop properties aren't the same as the Left and Top properties.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float UserControlScaleLeft
        {
            get
            {
                using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.ClientRectangle, 0, 0);
                    return fcGraphics.ScaleLeft;
                }
            }
            set
            {
                using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.ClientRectangle, 0, 0);
                    fcGraphics.ScaleLeft = value;
                }
            }
        }

        /// <summary>
        /// Return or set the horizontal (ScaleLeft) and vertical (ScaleTop) coordinates for the left and top edges of an object when using graphics methods or when positioning controls.
        /// Syntax
        /// object.ScaleLeft [= value]
        /// object.ScaleTop [= value]
        /// The ScaleLeft and ScaleTop property syntaxes have these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Value	A numeric expression specifying the horizontal or vertical coordinate. The default is 0.
        /// Remarks
        /// Using these properties and the related ScaleHeight and ScaleWidth properties, you can set up a full coordinate system with both positive and negative coordinates. These four 
        /// Scale properties interact with the ScaleMode property in the following ways:
        /// Setting any other Scale property to any value automatically sets ScaleMode to 0. A ScaleMode of 0 is user-defined.
        /// Setting the ScaleMode property to a number greater than 0 changes ScaleHeight and ScaleWidth to the new unit of measurement and sets ScaleLeft and ScaleTop to 0. The CurrentX 
        /// and CurrentY property settings change to reflect the new coordinates of the current point.
        /// You can also use the Scale method to set the ScaleHeight, ScaleWidth, ScaleLeft, and ScaleTop properties in one statement.
        /// Note   The ScaleLeft and ScaleTop properties aren't the same as the Left and Top properties.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float UserControlScaleTop
        {
            get
            {
                using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.ClientRectangle, 0, 0);
                    return fcGraphics.ScaleTop;
                }
            }
            set
            {
                using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.ClientRectangle, 0, 0);
                    fcGraphics.ScaleTop = value;
                }
            }
        }

        /// <summary>
        /// Gets/sets a value that determines the units of measure for object coordinates when using graphics methods or controls are positioned.
        /// Obsolete <see cref="http://msdn.microsoft.com/en-us/library/fhk6kcce(v=vs.71).aspx"/>
        /// </summary>
        [DefaultValue(ScaleModeConstants.vbTwips)]
        public ScaleModeConstants UserControlScaleMode
        {
            get
            {
                return fcGraphics.ScaleMode;
            }
            set
            {
                fcGraphics.ScaleMode = value;
            }
        }

        /// <summary>
        /// Returns or sets the color used to fill in shapes; FillColor is also used to fill in circles and boxes created with the Circle and Line graphics methods.
        /// Syntax
        /// object.FillColor [ = value]
        /// The FillColor property syntax has these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Value	A value or constant that determines the fill color, as described in Settings.
        /// Settings
        /// The settings for value are:
        /// Setting	Description
        /// Normal RGB colors	Colors set with the RGB or QBColor functions in code.
        /// System default colors	Colors specified with the system color constants in the Visual Basic (VB) object library in the Object Browser. The Microsoft Windows operating environment substitutes 
        /// the user's choices, as specified by the user's Control Panel settings.
        /// By default, FillColor is set to 0 (Black).
        /// Remarks
        /// Except for the Form object, when the FillStyle property is set to its default, 1 (Transparent), the FillColor setting is ignored.
        /// </summary>
        [DefaultValue(0)]
        public int UserControlFillColor
        {
            get
            {
                return this.graphicsFactory.FillColor;
            }
            set
            {
                this.graphicsFactory.FillColor = value;
            }
        }
        /// <summary>
        /// Gets or sets the pattern used to fill shapes created by using the <see cref="M:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.Circle(System.Boolean,System.Single,System.Single,System.Single,System.Int32,System.Single,System.Single,System.Single)"/> and <see cref="M:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.Line(System.Boolean,System.Single,System.Single,System.Boolean,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean)"/> graphics methods.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short. The default is 1.
        /// </returns>
        [DefaultValue(1)]
        public short UserControlFillStyle
        {
            get
            {
                return this.graphicsFactory.FillStyle;
            }
            set
            {
                this.graphicsFactory.FillStyle = value;
            }
        }

        /// <summary>
        /// Returns or sets the line width for output from graphics methods.
        /// object.DrawWidth [= size]
        /// size	A numeric expression from 1 through 32,767. This value represents the width of the line in pixels. The default is 1; that is, 1 pixel wide.
        /// Remarks
        /// Increase the value of this property to increase the width of the line. If the DrawWidth property setting is greater than 1, DrawStyle property settings 1 through 4 produce a solid line 
        /// (the DrawStyle property value isn't changed). Setting DrawWidth to 1 allows DrawStyle to produce the results shown in the DrawStyle property table.
        /// </summary>
        [DefaultValue(1)]
        public short UserControlDrawWidth
        {
            get
            {
                return graphicsFactory.DrawWidth;
            }
            set
            {
                graphicsFactory.DrawWidth = value;
            }
        }

        /// <summary>
        /// Returns or sets a value that determines the line style for output from graphics methods.
        /// Syntax
        /// object.DrawStyle [= number]
        /// The DrawStyle property syntax has these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Number	An integer that specifies line style, as described in Settings.
        /// Settings
        /// The settings for number are:
        /// Constant	Setting	Description
        /// VbSolid	0	(Default) Solid
        /// VbDash	1	Dash
        /// VbDot	2	Dot
        /// VbDashDot	3	Dash-Dot
        /// VbDashDotDot	4	Dash-Dot-Dot
        /// VbInvisible	5	Transparent
        /// VbInsideSolid	6	Inside Solid
        /// Remarks
        /// If DrawWidth is set to a value greater than 1, DrawStyle settings 1 through 4 produce a solid line (the DrawStyle property value isn't changed). If DrawWidth is set to 1, DrawStyle produces 
        /// the effect described in the preceding table for each setting.
        /// </summary>
        [DefaultValue(0)]
        public short UserControlDrawStyle
        {
            get
            {
                return graphicsFactory.DrawStyle;
            }
            set
            {
                graphicsFactory.DrawStyle = value;
            }
        }

        /// <summary>
        /// Returns or sets the font used to display text in a control or in a run-time drawing or printing operation.
        /// Note   The FontName property is included for use with the CommonDialog control and for compatibility with earlier versions of Visual Basic. For additional functionality, 
        /// use the new Font object properties (not available for the CommonDialog control).
        /// Syntax
        /// object.FontName [= font]
        /// The FontName property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// font	A string expression specifying the font name to use.
        /// Remarks
        /// The default for this property is determined by the system. Fonts available with Visual Basic vary depending on your system configuration, display devices, and printing 
        /// devices. Font-related properties can be set only to values for which fonts exist.
        /// In general, you should change FontName before setting size and style attributes with the FontSize, FontBold, FontItalic, FontStrikethru, and FontUnderline properties.
        /// Note   At run time, you can get information on fonts available to the system through the FontCount and Fonts properties.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string UserControlFontName
        {
            get
            {
                return this.Font.Name;
            }
            set
            {
                this.SetFontName(value);
                this.graphicsFactory.FontName = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the font that is used for text in a run-time printing operation.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float UserControlFontSize
        {
            get
            {
                return this.graphicsFactory.FontSize;
            }
            set
            {
                this.SetFontSize(value);
                this.graphicsFactory.FontSize = value;
            }
        }

        public new Font Font
        {
            get
            {
                return base.Font;
            }
            set
            {
                base.Font = value;
                graphicsFactory.Font = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool UserControlFontBold
        {
            get
            {
                return this.Font.Bold;
            }
            set
            {
                this.SetFontBold(value);
                this.graphicsFactory.FontBold = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool UserControlFontItalic
        {
            get
            {
                return this.Font.Italic;
            }
            set
            {
                this.SetFontItalic(value);
                this.graphicsFactory.FontItalic = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool UserControlFontUnderline
        {
            get
            {
                return this.Font.Underline;
            }
            set
            {
                this.SetFontUnderline(value);
                this.graphicsFactory.FontUnderline = value;
            }
        }

        /// <summary>
        /// Gets or sets the strikethrough font style.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Boolean.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool UserControlFontStrikethru
        {
            get
            {
                return this.graphicsFactory.FontStrikethru;
            }
            set
            {
                this.graphicsFactory.FontStrikethru = value;
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether background text and graphics on a Form or Printer object or a PictureBox control are displayed in the spaces around characters.
        /// Syntax
        /// object.FontTransparent [= boolean]
        /// The FontTransparent property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// boolean	A Boolean expression specifying the state of background text and graphics, as described in Settings.
        /// Settings
        /// The settings for boolean are:
        /// Setting	Description
        /// True	(Default) Permits background graphics and text to show around the spaces of the characters in a font.
        /// False	Masks existing background graphics and text around the characters of a font.
        /// Remarks
        /// Set FontTransparent at design time using the Properties window or at run time using code. Changing FontTransparent at run time doesn't affect graphics and text already drawn to Form, 
        /// Printer, or PictureBox.
        /// </summary>
        [DefaultValue(true)]
        public bool UserControlFontTransparent
        {
            get
            {
                return this.graphicsFactory.FontTransparent;
            }
            set
            {
                // TODO:CHE
                this.graphicsFactory.FontTransparent = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Color UserControlBackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Color UserControlForeColor
        {
            get
            {
                return base.ForeColor;
            }
            set
            {
                base.ForeColor = value;
            }
        }
                
        /// <summary>
        /// Returns or sets a value that determines the appearance of output from graphics method or the appearance of a Shape or Line control.
        /// object.DrawMode [= number]
        /// Number	An integer that specifies appearance, as described in Settings.
        /// Settings
        /// The settings for number are:
        /// Constant	Setting	Description
        /// VbBlackness	1	Blackness.
        /// VbNotMergePen	2	Not Merge Pen Inverse of setting 15 (Merge Pen).
        /// VbMaskNotPen	3	Mask Not Pen Combination of the colors common to the background color and the inverse of the pen.
        /// VbNotCopyPen	4	Not Copy Pen Inverse of setting 13 (Copy Pen).
        /// VbMaskPenNot	5	Mask Pen Not Combination of the colors common to both the pen and the inverse of the display.
        /// VbInvert	6	Invert Inverse of the display color.
        /// VbXorPen	7	Xor Pen Combination of the colors in the pen and in the display color, but not in both.
        /// VbNotMaskPen	8	Not Mask Pen Inverse of setting 9 (Mask Pen).
        /// VbMaskPen	9	Mask Pen Combination of the colors common to both the pen and the display.
        /// VbNotXorPen	10	Not Xor Pen Inverse of setting 7 (Xor Pen).
        /// VbNop	11	Nop No operation output remains unchanged. In effect, this setting turns drawing off.
        /// VbMergeNotPen	12	Merge Not Pen Combination of the display color and the inverse of the pen color.
        /// VbCopyPen	13	Copy Pen (Default) Color specified by the ForeColor property.
        /// VbMergePenNot	14	Merge Pen Not Combination of the pen color and the inverse of the display color.
        /// VbMergePen	15	Merge Pen Combination of the pen color and the display color.
        /// VbWhiteness	16	Whiteness.
        /// Remarks
        /// Use this property to produce visual effects with Shape or Line controls or when drawing with the graphics methods. Visual Basic compares each pixel in the draw pattern to the corresponding 
        /// pixel in the existing background and then applies bit-wise operations. For example, setting 7 (Xor Pen) uses the Xor operator to combine a draw pattern pixel with a background pixel.
        /// The exact effect of a DrawMode setting depends on the way the color of a line drawn at run time combines with colors already on the screen. Settings 1, 6, 7, 11, 13, and 16 yield the most 
        /// predictable results.
        /// </summary>
        [DefaultValue(DrawModeConstants.VbCopyPen)]
        public DrawModeConstants UserControlDrawMode
        {
            // TODO:CHE
            get
            {
                return this.drawMode;
            }
            set
            {
                this.drawMode = value;
            }
        }
        
        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        
        /// <summary>
        /// get Graphics
        /// </summary>
        /// <returns></returns>
        public Graphics GetGraphics()
        {
            if (this.BackgroundImage == null || resetBackgroundImage)
            {
                resetBackgroundImage = false;
                oldBackgroundImage = this.BackgroundImage;
                if (this.Width != 0 || this.Height != 0)
                {
                    this.BackgroundImage = new Bitmap(this.Width, this.Height);
                }
            }
            Graphics graphic = null;

            if (this.BackgroundImage.PixelFormat == System.Drawing.Imaging.PixelFormat.Format32bppArgb || this.BackgroundImage.PixelFormat == System.Drawing.Imaging.PixelFormat.Format24bppRgb)
            {
                graphic = Graphics.FromImage(this.BackgroundImage);
            }
            else
            {
                if (this.Width != 0 || this.Height != 0)
                {
                    Bitmap newBitmap = new Bitmap(this.Width, this.Height);
                    graphic = Graphics.FromImage(newBitmap);
                    graphic.DrawImage(this.BackgroundImage, 0, 0);
                }
            }

            //CHE: draw previous image, otherwise any previous Print will be lost
            if (oldBackgroundImage != null)
            {
                graphic.DrawImage(oldBackgroundImage, 0, 0);
                oldBackgroundImage = null;
            }

            graphic.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;

            //try
            //{
            //    graphic = Graphics.FromImage(this.BackgroundImage);
            //}
            //catch (Exception ex)
            //{
            //    //ex.Message = "A Graphics object cannot be created from an image that has an indexed pixel format."
            //    Bitmap newBitmap = new Bitmap(this.Width, this.Height);
            //    graphic = Graphics.FromImage(newBitmap);
            //    graphic.DrawImage(this.BackgroundImage, 0, 0);
            //}
            //finally
            //{
            //    graphic.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;
            //}
            return graphic;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="fromScale"></param>
        /// <param name="toScale"></param>
        /// <returns></returns>
        public float UserControlScaleX(float width, ScaleModeConstants fromScale, ScaleModeConstants toScale)
        {
            return this.fcGraphics.ScaleX(width, fromScale, toScale);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="height"></param>
        /// <param name="fromScale"></param>
        /// <param name="toScale"></param>
        /// <returns></returns>
        public float UserControlScaleY(float height, ScaleModeConstants fromScale, ScaleModeConstants toScale)
        {
            return this.fcGraphics.ScaleY(height, fromScale, toScale);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        public void UserControlScale(int x1, int y1, int x2, int y2)
        {
            this.fcGraphics.ScaleLeft = x1;
            this.fcGraphics.ScaleTop = y1;
            this.fcGraphics.ScaleWidth = x2;
            this.fcGraphics.ScaleHeight = y2;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public float UserControlTextWidth(string text)
        {
            using (Graphics g = GetGraphics())
            {
                return fcGraphics.TextWidth(g, text, this.ClientRectangle);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public float UserControlTextHeight(string text)
        {
            using (Graphics g = GetGraphics())
            {
                return fcGraphics.TextHeight(g, text, this.ClientRectangle);
            }
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods

        /// <summary> 
        /// If the KeyPreview is set to true, the user control will receive the KeyPress event before the control from the user control
        /// in which the key press originages
        /// </summary>
        /// <param name="m">Windows message</param>
        /// <returns></returns>
        // TODO
        //protected override bool ProcessKeyPreview(ref Message m)
        //{
        //    if (KeyPreview)
        //    {
        //        if ((m.Msg == WM_KEYDOWN) || (m.Msg == WM_SYSKEYDOWN))
        //        {
        //            OnKeyPress(new KeyPressEventArgs((char)m.WParam));
        //        }
        //    }

        //    return base.ProcessKeyPreview(ref m);
        //}
        
        #endregion

        #region Private Methods
        #endregion
    }
}
