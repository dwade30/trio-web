﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    /// <summary>
    /// VB.TextBox
    /// </summary>
    public partial class FCTextBox : TextBox
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private bool visible = true;

        private bool setSelection = false;
        private int selectionLength = 0;
        private int selectionStart = 0;

        private dynamic dataSourceOriginal;
        private string dataMember = "";
        private string dataField = "";
        private DataTable dataTable { get; set; }

        private bool inDataBinding = false;
        private bool inLeave = false;

        private bool inApplyDataFormat = false;

        private ToolTip toolTip = new ToolTip();
        //CHE: Remove the default behavior of a TextBox in Windows Forms is to highlight all of the text if it gets focused for the first time 
        // by tabbing into it, but not if it is clicked into.
        // Base class has a selectionSet property, but its private.
        // We need to shadow with our own variable. If true, this means
        // "don't mess with the selection, the user did it."
        private bool selectionSet;
        private bool useDefaultSelectionMode = false;
        private Color backColor = Color.White;

        private bool multilineInternal = false;

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private DataFormat m_DataFormat = null;

        private MousePointerConstants mousePointer = MousePointerConstants.vbDefault;

		//FC:FINAL:DSE:#i2219 SelectionStart not working if set during OnTextChanged call
		/// <summary>
		/// OnTextChanged method is running
		/// </summary>
		private bool textChangedRunning = false;

        #endregion

        #region Constructors

        public FCTextBox()
        {
            InitializeComponent();
            this.AutoSize = false;
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            base.Validating += FCTextBox_Validating;
            this.KeyDown += FCTextBox_KeyDown;
            this.MultilineChanged += FCTextBox_MultilineChanged;
            //CHE: set default value
            this.WhatsThisHelpID = 0;
            this.LinkTimeout = 50;
            this.ToolTipText = "";
            this.Appearance = 1;
            this.DataField = "";
            this.DataMember = "";
            this.m_DataFormat = new DataFormat();

            this.DataBindings.CollectionChanged += DataBindings_CollectionChanged;
            this.DataBindings.CollectionChanging += DataBindings_CollectionChanging;
            this.AutoComplete = AutoComplete.Off;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties
        /// <summary>
        ///  Returns/sets the source application and topic for a destination control.
        /// </summary>
        public string LinkTopic
        {
            //TODO:
            get;
            set;
        }

        /// <summary>
        /// Returns/sets the data passed to a destination control in a DDE conversation with another application.
        /// </summary>
        public string LinkItem
        {
            //TODO:
            get;
            set;
        }

        /// <summary>
        /// Returns/sets the type of link used for a DDE conversation and activates the connection.
        /// </summary>
        public FCForm.LinkModeConstants LinkMode
        {
            //TODO:
            get;
            set;
        }
        /// <summary>
        /// use internal flag to set Visible regardless the control is shown currently or not
        /// </summary>
        [DefaultValue(true)]
        public new bool Visible
        {
            get
            {
                return visible;
            }
            set
            {
                visible = value;
                base.Visible = value;
            }
        }

        /// <summary>
        /// Returns/sets an associated context number for an object.
        /// </summary>
        [DefaultValue(0)]
        public int WhatsThisHelpID
        {
            // TODO:CHE
            get;
            set;
        }

        /// Set SelectionLength-Value
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override int SelectionLength
        {
            get
            {
                return base.SelectionLength;
            }
            set
            {
                base.SelectionLength = value;
                if (value > 0)
                {
                    this.selectionLength = value;
                    setSelection = true;
                }
            }
        }

        /// <summary>
        /// Set SelectionStart-Value
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new int SelectionStart
        {
            get
            {
                return base.SelectionStart;
            }
            set
            {
				//FC:FINAL:DSE:#i2219 SelectionStart not working if set during OnTextChanged
				if (textChangedRunning)
				{
					Application.Post(() => { base.SelectionStart = value; });
				}
				else
				{
					base.SelectionStart = value;
				}
                this.selectionStart = value;
                setSelection = true;
            }
        }

        /// <summary>
        /// if false, then the Text of the Control will not be selected by default 
        /// </summary>
        [Browsable(true)]
        [DefaultValue(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public bool UseDefaultSelectionMode
        {
            get
            {
                return useDefaultSelectionMode;
            }
            set
            {
                useDefaultSelectionMode = value;
            }
        }
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontBold
        {
            get
            {
                return this.Font.Bold;
            }
            set
            {
                this.SetFontBold(value);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(HorizontalAlignment.Left)]
        public HorizontalAlignment Alignment
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(false)]
        public bool LockedOriginal
        {
            get
            {
                return this.ReadOnly;
            }
            set
            {
                this.ReadOnly = value;

            }
        }

        [DefaultValue(MousePointerConstants.vbDefault)]
        public MousePointerConstants MousePointer
        {
            get
            {
                return mousePointer;
            }
            set
            {
                mousePointer = value;
                this.Cursor = FCUtils.GetTranslatedCursor(mousePointer);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        //CHE: Remove the default behavior of a TextBox in Windows Forms is to highlight all of the text if it gets focused for the first time 
        // by tabbing into it, but not if it is clicked into.
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                //FC:FINAL:CHN - issue #1242: Incorrect showing symbol '\0'.
                if (value != null && value.Length > 0 && !value.Any(x=>x != '\0'))
                {
                    value = value.Replace("\0", "");
                }

                base.Text = value;

                ApplyDataFormat();

                // Update our copy of the variable since the
                // base implementation will have flipped its back.
                this.selectionSet = false;
            }
        }

        public override Color BackColor
        {
            get
            {
                //FC:FINAL:SBE - Harris #3977 - return correct back color when input is disabled
                //return base.BackColor;
                if (this.Enabled)
                {
                    return base.BackColor;
                }
                else
                {
                    return this.backColor;
                }
            }

            set
            {
                base.BackColor = value;
                this.backColor = value;
            }
        }

        /// <summary>
        /// Returns or sets a ToolTip.
        /// Syntax
        /// object.ToolTipText [= string]
        /// The ToolTipText property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// string	A string associated with an object in the Applies To list. that appears in a small rectangle below the object when the user's cursor hovers over the object at run time for about one second.
        /// Remarks
        /// If you use only an image to label an object, you can use this property to explain each object with a few words.
        /// At design time you can set the ToolTipText property string in the control's properties dialog box.
        /// For the Toolbar and TabStrip controls, you must set the ShowTips property to True to display ToolTips.
        /// </summary>
        [DefaultValue("")]
        public string ToolTipText
        {
            get
            {
                return toolTip.GetToolTip(this);
            }
            set
            {
                toolTip.SetToolTip(this, value);
            }
        }
        
        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    //CHE: cannot set Height unless is multine, otherwise size is fixed determined by font
                    if (!this.Multiline)
                    {
                        this.Multiline = true;
                        multilineInternal = true;
                    }
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                    //CHE: "Parameter is not valid" exception received when calling this setter through FCUtils.CloneControlProperties
                    //if (!FCUtils.inCloneControlProperties)
                    //{
                    //    //CHE: increase value if it is lower than the minimum for the current font, to have same values as in VB6
                    //    SizeF size = g.MeasureString("TEST", this.Font);
                    //    int minHeight = Convert.ToInt32(size.Height + 6);
                    //    if (base.Height < minHeight)
                    //    {
                    //        base.Height = minHeight;
                    //    }
                    //}
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// In .Net there is no longer an property Appearance, it is combined with the Border Style 
        /// VB6                             .NET
        /// 
        /// BorderStyle = 0 – None          None
        ///
        /// Appearance = 0 – Flat           FixedSingle
        /// BorderStyle = 1 – Fixed Single   
        /// 
        /// Appearance = 1 – 3D             Fixed3D
        /// BorderStyle = 1 – Fixed Single
        /// 
        /// </summary>
        [DefaultValue(1)]
        public int Appearance
        {
            get
            {
                if (base.BorderStyle == Wisej.Web.BorderStyle.Solid)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                if (value == 1)
                {
                    base.BorderStyle = Wisej.Web.BorderStyle.Solid;
                }
                else
                {
                    base.BorderStyle = Wisej.Web.BorderStyle.None;
                }
            }
        }

        /// <summary>
        /// Returns or sets the amount of time a control waits for a response to a DDE message.
        /// Syntax
        /// object.LinkTimeout [= number]
        /// The LinkTimeout property syntax has these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Number	A numeric expression that specifies the wait time.
        /// Remarks
        /// By default, the LinkTimeout property is set to 50 (equivalent to 5 seconds). You can specify other settings in tenths of a second.
        /// DDE response time from source applications varies. Use this property to adjust the time a destination control waits for a response from a source application. If you use LinkTimeout, 
        /// you can avoid generating a Visual Basic error if a given source application takes too long to respond.
        /// Note   The maximum length of time that a control can wait is 65,535 tenths of a second, or about 1 hour 49 minutes. Setting LinkTimeout to 1 tells the control to wait the maximum length 
        /// of time for a response in a DDE conversation. The user can force the control to stop waiting by pressing the ESC key.
        /// </summary>
        [DefaultValue(50)]
        public int LinkTimeout
        {
            // TODO:CHE
            get;
            set;
        }

        [Obsolete("Found no Information about what _ExtentX and _ExtentY in VB means")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int _ExtentX { get; set; }

        [Obsolete("Found no Information about what _ExtentX and _ExtentY in VB means")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int _ExtentY { get; set; }

        /// <summary>
        /// DataFormat is a Struct with some internal Information about the TextField
        /// But only Format can be used for something in c#
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [DefaultValue(null)]
        public DataFormat DataFormat
        {
            get { return m_DataFormat; }
            set { m_DataFormat = value; }
        }

        [DefaultValue("")]
        public string DataField
        {
            get
            {
                return dataField;
            }
            set
            {
                if (this.DataBindings.Count > 0)
                {
                	return;
                }
                dataField = value;
                if (!string.IsNullOrEmpty(value))
                {
                    BindingSource bs = new BindingSource();
                    bs.DataSource = this.dataTable;
                    if (this.dataTable != null)
                    {
                        this.DataBindings.Add(BindingPropertyName, bs, value);
                    }
                    this.DataSourceOriginal.AddBindingSource(this.DataMember, bs, this);
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string BindingPropertyName
        {
            get;
            set;
        }
        
        /// <summary>
        /// DataSource property of VB6 control
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public dynamic DataSourceOriginal
        {
            set
            {
                if (value == null)
                {
                    return;
                }
                DataTable data = new DataTable();
                string dataMember = DataMember;
                value.Class_GetDataMember(ref dataMember, ref data);
                this.dataTable = data;
                this.dataSourceOriginal = value;
            }
            get
            {
                return this.dataSourceOriginal;
            }
        }

        [DefaultValue("")]
        public string DataMember
        {
            get
            {
                return dataMember;
            }
            set
            {
                dataMember = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string FontName
        {
            get
            {
                return this.Font.Name;
            }
            set
            {
                this.SetFontName(value);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float FontSize
        {
            get
            {
                return base.Font.Size;
            }
            set
            {
                this.SetFontSize(value);
            }
        }

        /// <summary>
        /// Returns/sets a value indicating that data in a control has changed by some process other than by retrieving data from the current record.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool DataChanged
        {
            get
            {
                return this.Modified;
            }
            set
            {
                this.Modified = value;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        /// <summary>
        /// Sends a command string to the source application in a DDE conversation.
        /// </summary>
        public void  LinkExecute(string Command)
        {
            //FC:FINAL:DSE
            string typeName = this.LinkTopic.Replace("|", ".");
            string assemblyName = typeName.Substring(0, typeName.IndexOf("."));
            Type linkType = Type.GetType(typeName + ", " + assemblyName, false, true);
            // start corresponding module
            try
            {
                Type modMain = Type.GetType(assemblyName + ".modMain, " + assemblyName, false, true);
                if (modMain == null)
                {
                    modMain = Type.GetType(assemblyName + ".modPPGN, " + assemblyName, false, true);
                }
                if (modMain == null)
                {
                    modMain = Type.GetType(assemblyName + ".modREMain, " + assemblyName, false, true);
                }
                if (modMain == null)
                {
                    modMain = Type.GetType(assemblyName + ".modGlobal, " + assemblyName, false, true);
                }
                if(modMain != null)
                {
                    System.Reflection.MethodInfo method = modMain.GetMethod("Main");
                    if(method != null)
                    {
                        method.Invoke(null, null);
                    }
                }
                System.Reflection.PropertyInfo instanceProperty = linkType.GetProperty("InstancePtr", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
                if (instanceProperty != null)
                {
                    FCForm frm = (FCForm)instanceProperty.GetValue(null, null);
                    frm.FireLinkExecute(Command, 0);
                }

            }
            catch
            {

            }


        }
        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
        /// User-defined conversion from FCTextBox to string
        /// </summary>
        /// <param name="textBox"></param>
        /// <returns></returns>
        public static implicit operator string(FCTextBox textBox)
        {
            return textBox.Text;
        }

        /// <summary>
        /// User-defined conversion from FCTextBox to int
        /// </summary>
        /// <param name="textBox"></param>
        /// <returns></returns>
        public static implicit operator int(FCTextBox textBox)
        {
            return Convert.ToInt32(textBox.Text);
        }

        /// <summary>
        /// User-defined conversion from FCTextBox to decimal
        /// </summary>
        /// <param name="textBox"></param>
        /// <returns></returns>
        public static implicit operator decimal(FCTextBox textBox)
        {
            return Convert.ToDecimal(textBox.Text);
        }

        /// <summary>
        /// overload operator < 
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool operator <(FCTextBox textBox, int value)
        {
            return GetFieldValueAsDouble(textBox) < value;
        }

        /// <summary>
        /// overload operator >
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="value"></param>
        /// <returns></returns> 
        public static bool operator >(FCTextBox textBox, int value)
        {
            return GetFieldValueAsDouble(textBox) > value;
        }

        /// <summary>
        /// overload operator <=
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="value"></param>
        /// <returns></returns> 
        public static bool operator <=(FCTextBox textBox, int value)
        {
            return GetFieldValueAsDouble(textBox) <= value;
        }

        /// <summary>
        /// overload operator >=
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="value"></param>
        /// <returns></returns> 
        public static bool operator >=(FCTextBox textBox, int value)
        {
            return GetFieldValueAsDouble(textBox) >= value;
        }

        /// <summary>
        /// overload operator == 
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool operator ==(FCTextBox textBox, int value)
        {
            return GetFieldValueAsDouble(textBox) == value;
        }

        /// <summary>
        /// overload operator != 
        /// </summary>
        /// <param name="textBox"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool operator !=(FCTextBox textBox, int value)
        {
            return GetFieldValueAsDouble(textBox) != value;
        }

        /// <summary>
        /// overload operator == 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="textBox"></param>
        /// <returns></returns>
        public static bool operator ==(int value, FCTextBox textBox)
        {
            return GetFieldValueAsDouble(textBox) == value;
        }

        /// <summary>
        /// overload operator != 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="textBox"></param>
        /// <returns></returns>
        public static bool operator !=(int value, FCTextBox textBox)
        {
            return GetFieldValueAsDouble(textBox) != value;
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        protected override void OnLeave(EventArgs e)
        {
            FCForm form = this.FindForm() as FCForm;
            if(inLeave || form == null || !form.IsLoaded)
            {
                return;
            }
            ResetSelection();
            inLeave = true;
            base.OnLeave(e);
            inLeave = false;
        }


        //CHE: Remove the default behavior of a TextBox in Windows Forms is to highlight all of the text if it gets focused for the first time 
        // by tabbing into it, but not if it is clicked into.
        protected override void OnGotFocus(EventArgs e)
        {
            bool needToDeselect = false;

            // We don't want to avoid calling the base implementation
            // completely. We mirror the logic that we are trying to avoid;
            // if the base implementation will select all of the text, we
            // set a boolean.
            if (!this.selectionSet && !useDefaultSelectionMode)
            {
                this.selectionSet = true;

                if ((this.SelectionLength == 0) &&
                    (Control.MouseButtons == MouseButtons.None))
                {
                    needToDeselect = true;
                }
            }

            // Call the base implementation
            base.OnGotFocus(e);

            // Did we notice that the text was selected automatically? Let's
            // de-select it
            if (needToDeselect)
            {
                this.DeselectAll();
            }
        }


        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            //FC:FINAL:SBE - Harris #3839 - in VB6 KeyPressEdit is not fired for the TAB key
            if (e.KeyChar == 9)
            {
                return;
            }
            base.OnKeyPress(e);
        }

        protected override void OnTextChanged(EventArgs e)
        {
            //CHE: do not raise TextChanged when binding is made to Text property
            if (inDataBinding)
            {
                return;
            }

            //FC:FINAL:MHO - do not raise while in InitializeComponent
            Form f = this.FindForm();
            if (f == null || (!f.Created && !((FCForm)f).IsLoaded))
            {
                return;
            }
			//FC:FINAL:DSE:#i2219 SelectionStart not working if set during OnTextChanged method call
			// save text changed status (if .Text property is set during TextChanged event handler, the TextChanged event will be fired again)
			bool oldTextChangeRunning = textChangedRunning;
			textChangedRunning = true;
            base.OnTextChanged(e);
			//FC:FINAL:DSE:#i2219 
			textChangedRunning = oldTextChangeRunning;
        }

        //protected override void OnSizeChanged(EventArgs e)
        //{
        //    //CHE: cannot set Height unless is multine, otherwise size is fixed determined by font
        //    if (!this.Multiline)
        //    {
        //        this.Multiline = true;
        //        multilineInternal = true;
        //    }
        //    base.OnSizeChanged(e);
        //}

        /// <summary>
        /// MouseUp-Event-Handler 
        /// Select Text if Selection is set. This is not working by default (e.g. Events Click-GotFocus-_MouseUp prevents from selectiong Text if it's set in GotFocus or Enter Event )
        /// </summary>
        /// <param name="mevent"></param>
        protected override void OnMouseUp(MouseEventArgs mevent)
        {
            base.OnMouseUp(mevent);
            if (setSelection)
            {
                this.SelectionStart = this.selectionStart;
                this.SelectionLength = this.selectionLength;
            }
            ResetSelection();
        }

        protected override void OnValidating(CancelEventArgs e)
        {
            //In VB6 the Validate event is not fired when the form is closed
            //SBE - Harris - #2327 - in VB6 Validating event is triggered only when form is Active
            //MSH - Harris - i.issue #1663: in VB6 Validating will be fired even if data wasn't changed
            var form = this.FindForm();
            if (form != null)
            {
	            if (form.CloseReason != CloseReason.UserClosing && form.Active)
	            {
		            base.OnValidating(e);
		            if (!e.Cancel)
		            {
			            this.Modified = false;
		            }
	            }
            }
        }

        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);
            if(this.Enabled)
            {
                base.BackColor = backColor;
            }
            else
            {
                base.BackColor = Color.FromName("@disabledTextBox");
            }
        }

        #endregion

        #region Private Methods

        private void DataBindings_CollectionChanging(object sender, CollectionChangeEventArgs e)
        {
            inDataBinding = true;
        }

        private void DataBindings_CollectionChanged(object sender, CollectionChangeEventArgs e)
        {
            inDataBinding = false;
        }

        private void FCTextBox_MultilineChanged(object sender, EventArgs e)
        {
            if (this.Multiline)
            {
                multilineInternal = true;
                this.AcceptsReturn = true;
            }
            else
            {
                multilineInternal = false;
                this.AcceptsReturn = false;
            }
        }

        private void FCTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            //CHE: Enter key is handled in client code on KeyPress, do not suppress
            ////CHE: prevent beep when pressing Enter key on TextBox (default .NET behavior)
            //if (!this.Multiline && e.KeyCode == Keys.Enter)
            //{
            //    e.SuppressKeyPress = true;
            //}
            //CHE: if Multiline was set only to allow Height to be changed then do not process Down and Up keys, act as single line TextBox
            //CHE: for this project Enter key is not handled in client code, we can suppress it
            // TODO
            //if (multilineInternal && (e.KeyCode == Keys.Down || e.KeyCode == Keys.Up || e.KeyCode == Keys.Enter))
            //{
            //    e.SuppressKeyPress = true;
            //}
        }

        private void FCTextBox_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ApplyDataFormat();
        }

        private void ApplyDataFormat()
        {
            if (inApplyDataFormat)
            {
                return;
            }

            inApplyDataFormat = true;

            DateTime inputDate;

            string format = m_DataFormat.Format;

            if (m_DataFormat != null && !string.IsNullOrEmpty(format))
            {
                if (format.Contains("d") ||
                    format.Contains("M") ||
                    format.Contains("y") ||
                    format.Contains("h") ||
                    format.Contains("m") ||
                    format.Contains("s")
                    )
                {
                    // Format String contains picture for date or time Try to Format as given in Format String

                    if (DateTime.TryParse(this.Text, out inputDate))
                    {
                        this.Text = inputDate.ToString(format);
                    }
                    else
                    {
                        //CHE: do not throw exception e.g. DataFormat is date format and Text is "None"
                        //throw new System.NotImplementedException();
                    }

                }
                else
                {
                    throw new System.NotImplementedException();
                }
            }

            inApplyDataFormat = false;
        }

        private static double GetFieldValueAsDouble(FCTextBox field)
        {
            return Convert.IsDBNull(field.Text) ? 0 : Convert.ToDouble(field.Text);
        }


        /// <summary>
        /// Reset Selection-Mode
        /// </summary>
        private void ResetSelection()
        {
            this.setSelection = false;
            //this.SelectionStart = 0;
            //this.selectionLength = 0;
        }

        #endregion
    }
}
