﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    public class DataFormat
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        #endregion

        #region Constructors
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        [Obsolete("The property is a VB6 designer property to set the type of the textbox")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Type { get; set; }

        [Obsolete("The property is a VB6 designer property ")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Format { get; set; }

        [Obsolete("The property is a VB6 designer property ")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HaveTrueFalseNull { get; set; }

        [Obsolete("The property is a VB6 designer property ")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int FirstDayOfWeek { get; set; }

        [Obsolete("The property is a VB6 designer property ")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int FirstWeekOfYear { get; set; }

        [Obsolete("The property is a VB6 designer property ")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LCID { get; set; }

        [Obsolete("The property is a VB6 designer property ")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SubFormatType { get; set; }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
