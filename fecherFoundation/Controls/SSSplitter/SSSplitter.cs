using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using fecherFoundation.Extensions;
using Encoder = System.Text.Encoder;

namespace fecherFoundation
{
    #region Splitter enums

    public enum Constants_Alignment
    {

        /// <summary>
        /// 
        /// </summary>
        ssBottomOfCaption,

        /// <summary>
        /// 
        /// </summary>
        ssCenterBottom,

        /// <summary>
        /// 
        /// </summary>
        ssCenterMiddle,

        /// <summary>
        /// 
        /// </summary>
        ssCenterTop,

        /// <summary>
        /// 
        /// </summary>
        ssLeftBottom,

        /// <summary>
        /// 
        /// </summary>
        ssLeftMiddle,

        /// <summary>
        /// 
        /// </summary>
        ssLeftOfCaption,

        /// <summary>
        /// 
        /// </summary>
        ssLeftTop,

        /// <summary>
        /// 
        /// </summary>
        ssRightBottom,

        /// <summary>
        /// 
        /// </summary>
        ssRightMiddle,

        /// <summary>
        /// 
        /// </summary>
        ssRightOfCaption,

        /// <summary>
        /// 
        /// </summary>
        ssRightTop,

        /// <summary>
        /// 
        /// </summary>
        ssTopOfCaption
    }

    public enum Constants_Bevel
    {

        /// <summary>
        /// 
        /// </summary>
        ssInsetBevel,

        /// <summary>
        /// 
        /// </summary>
        ssNoneBevel,

        /// <summary>
        /// 
        /// </summary>
        ssRaisedBevel
    }


    public enum SplitterOrientation
    {
        Horizontal = 1,
        Vertical = 3
    }

    public enum Constants_AutoSize
    {

        /// <summary>
        /// 
        /// </summary>
        ssAutoSizeFillContainer,

        /// <summary>
        /// 
        /// </summary>
        ssAutoSizeNone
    }

    public enum Constants_BorderStyle
    {

        /// <summary>
        /// 
        /// </summary>
        ssBorderStyleFixedSingle,

        /// <summary>
        /// 
        /// </summary>
        ssBorderStyleInset,

        /// <summary>
        /// 
        /// </summary>
        ssBorderStyleNone,

        /// <summary>
        /// 
        /// </summary>
        ssBorderStyleRaised
    }

    public enum Constants_SplitterBarAppearance
    {

        /// <summary>
        /// 
        /// </summary>
        ssSplitterBar3D,

        /// <summary>
        /// 
        /// </summary>
        ssSplitterBarBorderless,

        /// <summary>
        /// 
        /// </summary>
        ssSplitterBarFlat
    }

    public enum Constants_SplitterBarJoinStyle
    {

        /// <summary>
        /// 
        /// </summary>
        ssJoinContinuous,

        /// <summary>
        /// 
        /// </summary>
        ssJoinSegmented
    }

    public enum Constants_SplitterResizeStyle
    {

        /// <summary>
        /// 
        /// </summary>
        ssResizeNonProportional,

        /// <summary>
        /// 
        /// </summary>
        ssResizeProportional
    }

    public enum SplitterBarStyle
    {
        Horizontal = 0,
        Vertical = 1
    }

    #endregion

    public partial class SSSplitter : Panel
    {
        #region Members
        private Panes panes;
        #endregion

        #region Properties

        [Browsable(false),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Panes Panes
        {
            get
            {
                return panes;
            }
        }

        public int PanesCount
        {
            get
            {
                return this.panes.Count;
            }
        }

        public Constants_SplitterBarJoinStyle SplitterBarJoinStyle { get; set; }

        public Constants_SplitterBarAppearance SplitterBarAppearance { get; set; }

        public Constants_SplitterResizeStyle SplitterResizeStyle { get; set; }

        public new Constants_BorderStyle BorderStyle { get; set; }

        public int SplitterBarWidth { get; set; }

        #endregion

        #region Constructor
        public SSSplitter() : base()
        {
            InitializeComponent();
            base.AutoSize = false;
            panes = new Panes(this);
        }
        #endregion
    }

    /// <summary>
    /// 
    /// </summary>

    //[Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    public class SSPanel : Panel
    {
        #region Members
        private SSSplitter parentSplitter;
        private FCControlCollection controlCollection;
        private object containedControl;
        private Image picture = null;
        private int pictureFrames = 0;
        private string identifier = "";
        #endregion

        #region Constructor
        public SSPanel()
        {
            this.controlCollection = new FCControlCollection(this);

            this.ParentChanged += new EventHandler(SSPanel_ParentChanged);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public uint PictureMaskColor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public int PictureAnimationDelay
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public int PictureFrames
        {
            get
            {
                return pictureFrames;
            }
            set
            {
                pictureFrames = value;
                SetPicture();
            }
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public bool PictureUseMask
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public Image Picture
        {
            get
            {
                return picture; 
            }
            set
            {
                picture = value;
                SetPicture();
            }
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public Constants_Bevel BevelOuter
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public Constants_Alignment PictureAlignment
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public bool RoundedCorners
        {
            get;
            set;
        }

        [Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        internal SSSplitter ParentSplitter
        {
            get { return parentSplitter; }
            set { parentSplitter = value; }
        }

        /// <summary>
        /// When setting the control to the pane, if the object is a string, search for it in the parent form controls list and add it to the pane controls collection
        /// If the object is a control, then add it to the panes control collection
        /// After adding the control to the panes control collection, set the docking to fill
        /// </summary>
        public object ContainedControl
        {
            get { return containedControl; }

            set
            {
                containedControl = value;
                if (containedControl == null)
                {
                    return;
                }
                Control control = null;
                if (containedControl.GetType() == typeof(string))
                {
                    control = this.parentSplitter.FindForm().Controls[containedControl.ToString()];
                }
                else if (containedControl is Control)
                {
                    control = containedControl as Control;
                }

                base.Controls.Add(control);
                this.Size = control.Size;
                control.Dock = DockStyle.Fill;
            }
        }

        public bool LockHeight { get; set; }

        public bool LockWidth { get; set; }

        [Browsable(false),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Splitter Splitter { get; set; }

        [DefaultValue(-1)]
        public int Index
        {
            get;
            set;
        }

        public new FCControlCollection Controls
        {
            get
            {
                return controlCollection;
            }
        }

        public new int Height
        {
            get
            {
                return base.Height;
            }
            set
            {
                Panel parent = null;
                if (this.Parent.GetType() == typeof(Panel))
                {
                    parent = this.Parent as Panel;
                    while (parent.Parent.GetType() == typeof(Panel))
                    {
                        parent = this.Parent as Panel;
                    }
                }
                if (parent != null)
                {
                    parent.Height = value;
                }
                base.Height = value;
            }
        }

        public new int Top
        {
            get
            {
                Panel parent = null;
                if (this.Parent.GetType() == typeof(Panel))
                {
                    parent = this.Parent as Panel;
                    while (parent.Parent.GetType() == typeof(Panel))
                    {
                        parent = this.Parent as Panel;
                    }
                }
                if (parent != null)
                {
                    return parent.Top;
                }

                return base.Top;
            }
            set
            {
                Panel parent = null;
                if (this.Parent.GetType() == typeof(Panel))
                {
                    parent = this.Parent as Panel;
                    while (parent.Parent.GetType() == typeof(Panel))
                    {
                        parent = this.Parent as Panel;
                    }
                }
                if (parent != null)
                {
                    parent.Top = value;
                }
            }
        }

        //In vb6 a SSPanel is identified in the spliters Pane collection by its name. 
        //In .NET the name of the panel is the same as the object name (ex SSPanel pane1 = new SSPanel();, pane1.Name will be pane1
        //Use a different property to store the identifier
        public string Identifier
        {
            get
            {
                return this.identifier;
            }
            set
            {
                this.identifier = value; 
            }
        }
        #endregion

        #region Private methods
        private void SetPicture()
        {
            if (this.picture != null && this.pictureFrames > 1)
            {
                PictureBox picBox = new PictureBox();
                picBox.Image = this.picture;
                this.Controls.Add(picBox);
                picBox.Dock = DockStyle.Fill;
            }
            else if (this.picture != null)
            {
                this.Controls.Clear();
                this.BackgroundImage = this.picture;
            }
        }
        #endregion

        #region Private events
        private void SSPanel_ParentChanged(object sender, EventArgs e)
        {
            if (this.Parent != null)
            {
                Control parentControl = this.Parent;
                while (parentControl != null && parentControl.GetType() != typeof(SSSplitter))
                {
                    parentControl = parentControl.Parent;
                }

                if (parentControl != null && parentControl.GetType() == typeof(SSSplitter))
                {
                    this.ParentSplitter = ((SSSplitter)parentControl);
                    if (!this.parentSplitter.Panes.Contains((this)))
                    {
                        this.parentSplitter.Panes.Add(this);
                    }
                }
            }
        }
        #endregion

    }

    public class FCControlCollection : List<Control>
    {
        #region Members
        private Control owner;
        #endregion

        #region Constructor
        public FCControlCollection(Control owner)
        {
            this.owner = owner;
        }
        #endregion

        #region Public methods
        public new void Add(Control value)
        {
            base.Add(value);
            SSPanel panelOwner = this.owner as SSPanel;
            if (panelOwner != null)
            {
                panelOwner.ContainedControl = value;
            }
        }
        #endregion
    }

    public class Panes : List<SSPanel>
    {
        #region Members
        private SSSplitter parent;
        #endregion

        #region Constructor
        public Panes(SSSplitter splitter)
        {
            this.parent = splitter;
        }
        #endregion

        #region Properties

        public SSPanel this[string name]
        {
            get
            {
                foreach (SSPanel pane in this)
                {
                    if (pane.Identifier == name || pane.Name == name)
                    {
                        return pane;
                    }
                }

                return null;
            }
        }

        public SSPanel this[int index]
        {
            get
            {
                foreach (SSPanel pane in this)
                {
                    if (pane.Index == index)
                    {
                        return pane;
                    }
                }

                return base[index];
            }
        }
        #endregion

        #region Public methods
        public new void Clear()
        {
            base.Clear();
        }
        #endregion

        #region Private methods
        private bool CheckIndex(int index)
        {
            if (index >= base.Count || index < 0)
            {
                return false;
            }
            return true;
        }
        #endregion
    }
    
}
