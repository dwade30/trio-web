﻿ using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fecherFoundation
{
    /// <summary>
    /// VertMenu.VerticalMenu
    /// </summary>
    public class FCVerticalMenu : ToolStrip
    {
        #region Private Members

        private string menuCaption;
        private int align = 1;
        private int menusMax = 1;
        private int menuItemsMax = 1;

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private List<List<ToolStripButton>> tbMatrix = new List<List<ToolStripButton>>();

        #endregion

        #region Constructors

        public FCVerticalMenu()
        {
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            this.MenuCur = 1;
            this.MenuItemCur = 1;
            this.MenusMax = 1;
            this.MenuItemsMax = 1;
            this.AutoSize = false;
        }

        #endregion

        #region Public Delegates

        public delegate void OnMenuItemClick(object sender, MenuItemClickEventArgs e);

        #endregion

        #region Public Events

        public event EventHandler<MenuItemClickEventArgs> MenuItemClick;

        #endregion

        #region Properties

        [DefaultValue(1)]
        public int Align
        {
            get
            {
                return align;
            }
            set
            {
                align = value;
                base.Dock = (DockStyle)value;
            }
        }

        public string MenuCaption
        {
            get
            {
                return this.tbMatrix[this.MenuCur - 1][0].Text;
            }
            set
            {
                this.tbMatrix[this.MenuCur - 1][0].Text = value;
            }
        }

        public string MenuItemCaption
        {
            get
            {
                return this.tbMatrix[this.MenuCur - 1][this.MenuItemCur].Text;
            }
            set
            {
                this.tbMatrix[this.MenuCur - 1][this.MenuItemCur].Text = value;
            }
        }

        [DefaultValue(1)]
        public int MenuCur
        {
            get;
            set;
        }

        [DefaultValue(1)]
        public int MenuItemCur
        {
            get;
            set;
        }


        public Image MenuItemIcon
        {
            get
            {
                return this.tbMatrix[this.MenuCur - 1][this.MenuItemCur].Image;
            }
            set
            {
                this.tbMatrix[this.MenuCur - 1][this.MenuItemCur].Image = value;
            }
        }

        public int MenusMax
        {
            get
            {
                return menusMax;
            }
            set
            {
                menusMax = value;
                for (int i = 0; i < menusMax; i++)
                {
                    List<ToolStripButton> tbList = new List<ToolStripButton>();
                    tbMatrix.Add(tbList);
                }
            }
        }

        [DefaultValue(1)]
        public int MenuItemsMax
        {
            get
            {
                return menuItemsMax;
            }
            set
            {
                this.Items.Clear();

                menuItemsMax = value;
                foreach (List<ToolStripButton> tbList in tbMatrix)
                {
                    tbList.Clear();
                    ToolStripButton tb = new ToolStripButton("");
                    tb.DisplayStyle = ToolStripItemDisplayStyle.Text;
                    this.Items.Add(tb);
                    tbList.Add(tb);
                    for (int i = 0; i < menuItemsMax; i++)
                    {
                        ToolStripButton tb1 = new ToolStripButton("");
                        tb1.TextImageRelation = TextImageRelation.ImageAboveText;
                        tb1.MouseDown += Tb1_MouseDown;
                        tb1.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
                        this.Items.Add(tb1);

                        this.BackColor = System.Drawing.SystemColors.ControlDark;
                        tb.BackColor = System.Drawing.SystemColors.Control;

                        tbList.Add(tb1);
                    }
                }
            }
        }

        #endregion

        #region Private Methods

        private void Tb1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                return;
            }

            if (MenuItemClick != null)
            {
                MenuItemClickEventArgs args = new MenuItemClickEventArgs();
                //detect position
                int i = 0;
                foreach (List<ToolStripButton> tbList in tbMatrix)
                {
                    i++;
                    int j = tbList.IndexOf(sender as ToolStripButton);
                    if (j > 0)
                    {
                        args.MenuNumber = i;
                        args.MenuItem = j;
                        break;
                    }
                }
                Point transformedPoint = TransformCoordinate(e.X, e.Y);
                args.X = transformedPoint.X;
                args.Y = transformedPoint.Y;
                MenuItemClick(sender, args);
            }
        }

        private Point TransformCoordinate(int X, int Y)
        {
            Point result = new Point();

            using (Graphics g = this.CreateGraphics())
            {
                fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                result.X = Convert.ToInt32(fcGraphics.ScaleX(X, ScaleModeConstants.vbPixels, ScaleModeConstants.vbTwips));
                result.Y = Convert.ToInt32(fcGraphics.ScaleY(Y, ScaleModeConstants.vbPixels, ScaleModeConstants.vbTwips));
            }

            return result;
        }

        #endregion
    }
}
