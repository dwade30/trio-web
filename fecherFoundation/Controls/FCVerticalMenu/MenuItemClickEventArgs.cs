﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation
{
    public class MenuItemClickEventArgs : EventArgs
    {        
        #region Constructors

        public MenuItemClickEventArgs()
        {
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public int MenuNumber
        {
            // TODO:CHE
            get;
            set;
        }

        public int MenuItem
        {
            // TODO:CHE
            get;
            set;
        }

        public int X
        {
            // TODO:CHE
            get;
            set;
        }

        public int Y
        {
            // TODO:CHE
            get;
            set;
        }

        #endregion
    }
}
