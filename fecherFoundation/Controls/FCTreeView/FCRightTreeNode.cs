﻿using Wisej.Web;

namespace fecherFoundation
{
	public class FCRigthTreeNode : TreeNode
	{
		protected override void OnWebRender(dynamic config)
		{
			base.OnWebRender((object)config);

			config.className = "app.RightTreeNode";
		}
	}
}