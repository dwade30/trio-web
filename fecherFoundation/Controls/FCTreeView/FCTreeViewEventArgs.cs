﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation
{
    public class FCTreeViewEventArgs : TreeViewEventArgs
    {
        #region Private Members

        private FCTreeNode fcnode;

        #endregion

        #region Constructors

        public FCTreeViewEventArgs(TreeViewEventArgs e) : base(e.Node)
        {
            fcnode = e.Node as FCTreeNode;
        }

        #endregion

        #region Properties

        public new FCTreeNode Node
        {
            get
            {
                return this.fcnode;
            }
        }

        #endregion
    }
}
