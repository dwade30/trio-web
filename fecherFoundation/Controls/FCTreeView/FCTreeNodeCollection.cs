﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    /// <summary>
    /// TreeLineStyleConstants enum
    /// </summary>
    //public enum TreeLineStyleConstants
    //{
    //    tvwTreeLines = 0,
    //    tvwRootLines = 1
    //}

    //public enum TreeStyleConstants : int
    //{
    //    tvwPictureText = 1,
    //    tvwPlusMinusText = 2,
    //    tvwPlusPictureText = 3,
    //    tvwTextOnly = 0,
    //    tvwTreelinesPictureText = 5,
    //    tvwTreelinesPlusMinusPictureText = 7,
    //    tvwTreelinesPlusMinusText = 6,
    //    tvwTreelinesText = 4
    //}
    //public enum TreeRelationshipConstants
    //{
    //    tvwChild = 4,
    //    tvwFirst = 0,
    //    tvwLast = 1,
    //    tvwNext = 2,
    //    tvwPrevious = 3
    //}
    
    public class FCTreeNodeCollection : IList, ICollection, IEnumerable
    {
        public TreeNodeCollection BaseNodes;
        public FCTreeNode Parent;
        private bool isDirty;
        public Dictionary<string, FCTreeNode> Nodelist;

        //DSE Internaly, nodes should be kept in the original adding order
        private List<FCTreeNode> OrderedNodeList;
        
        public Dictionary<string, FCTreeNode> GetAllNodes()
        {
            Dictionary<string, FCTreeNode> list = new Dictionary<string, FCTreeNode>();
            if (isDirty)
            {
                // recreate Nodelist
                Nodelist = GetAllNodes(this.BaseNodes, list);
                isDirty = false;
            }

            return Nodelist;
        }

        public Dictionary<string,FCTreeNode> GetAllNodes(TreeNodeCollection collection, Dictionary<string, FCTreeNode> list)
        {           
            foreach (FCTreeNode item in collection)
            {
                if (string.IsNullOrEmpty(item.Name) || list.ContainsKey(item.Name))
                {
                    list.Add(Guid.NewGuid().ToString(), item);
                }
                else
                {
                    list.Add(item.Name, item);
                }

                GetAllNodes(item.Nodes.BaseNodes, list);
            }

            return list;
        }

        public FCTreeNodeCollection(TreeNodeCollection treeNodes,FCTreeNode parent):this(treeNodes)
        {
            Parent = parent;
            Nodelist = new Dictionary<string, FCTreeNode>();
            isDirty = false;
			OrderedNodeList = new List<FCTreeNode>();
        }

        public FCTreeNodeCollection(TreeNodeCollection treeNodes)
        {
            BaseNodes = treeNodes;
            Nodelist = new Dictionary<string, FCTreeNode>();
            isDirty = false;
            OrderedNodeList = new List<FCTreeNode>();
        }

        public int Add(FCTreeNode node)
        {
            node.Parent = this.Parent;
            //TODO:PJU:Why changing the Tag (for Anadarko it was set to the Index)????
            //node.Tag = node.Name;
            if(node.Tag==null)
            {
                node.Tag = node.Name;
            }
            isDirty = true;

			//FC:FINAL:DSE - Set StandardDefaultPicture for the new node
			int newNodeIndex = BaseNodes.Add(node);

            AddInternalNode(node);
            if (!Nodelist.ContainsKey(node.Name))
            {
                Nodelist.Add(node.Name, node);
            }
            SetStandardPicture(node);
			return newNodeIndex;
        }

        public FCTreeNode Add(string value)
        {
            return _Add(string.Empty, value, - 1, -1, string.Empty, string.Empty);
        }

        public FCTreeNode Add(string key, string text)
        {
            return _Add(key, text, -1, -1, string.Empty, string.Empty);
        }

        public FCTreeNode Add(string key, string text, int ImageIndex)
        {
            return _Add(key, text, ImageIndex, -1, string.Empty, string.Empty);
        }

        public FCTreeNode Add(string key, string text, int ImageIndex, int selectedImageIndex)
        {
            return _Add(key, text, ImageIndex, selectedImageIndex, string.Empty, string.Empty);
        }

        public FCTreeNode Add(string key, string text, string imageKey)
        {
            return _Add(key, text, -1, -1, imageKey, string.Empty);
        }

        public FCTreeNode Add(string key, string text, string imageKey, string SelectedImageKey)
        {
            return _Add(key, text, -1, -1, imageKey, SelectedImageKey);
        }

        public int AddNode(FCTreeNode node)
        {
            node.Parent = this.Parent;
            //TODO:PJU:Why changing the Tag (for Anadarko it was set to the Index)????
            if(node.Tag==null)
            {
                node.Tag = node.Name;
            }
            isDirty = true;

			//FC:FINAL:DSE - Set StandardPicture for the new node
			int newNodeIndex = BaseNodes.Add(node);

            AddInternalNode(node);
            if (!Nodelist.ContainsKey(node.Name))
            {
                Nodelist.Add(node.Name, node);
            }
            SetStandardPicture(node);
			return newNodeIndex;
        }

        public FCTreeNode AddNode(string value)
        {
            return _Add(string.Empty, value, -1, -1, string.Empty, string.Empty);
        }

        public FCTreeNode AddNode(string key, string text)
        {
            return _Add(key, text, -1, -1, string.Empty, string.Empty);
        }

        public FCTreeNode AddNode(string key, string text, int ImageIndex)
        {
            return _Add(key, text, ImageIndex, -1, string.Empty, string.Empty);
        }

        public FCTreeNode AddNode(string key, string text, int ImageIndex, int selectedImageIndex)
        {
            return _Add(key, text, ImageIndex, selectedImageIndex, string.Empty, string.Empty);
        }

        public FCTreeNode AddNode(string key, string text, string imageKey)
        {
            return _Add(key, text, -1, -1, imageKey, string.Empty);
        }

        public FCTreeNode AddNode(string key, string text, string imageKey, string SelectedImageKey)
        {
            return _Add(key, text, -1, -1, imageKey, SelectedImageKey);
        }


        /// <summary>
        ///  Zentraler Aufruf der ADD Methode mit allen möglichen Parameter
        ///  Alle anderen Methoden rufen diese auf (bis auf Add( node) )
        /// </summary>
        /// <param name="value"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private FCTreeNode _Add(string key, string text, int imageIndex, int selectedImageIndex, string imageKey, string SelectedImageKey)
        {
            // Set FCTreeNode
            FCTreeNode newnode = new FCTreeNode(text);
            isDirty = true;

            // possible calls of original node
            // BaseNodes.Add(node);  this cannot be handled here
            // BaseNodes.Add(text);
            // BaseNodes.Add(key, text);
            // BaseNodes.Add(key, text, imageIndex)
            // BaseNodes.Add(key, text, imageIndex, selectedImageIndex)
            // BaseNodes.Add(key, text, imageKey)
            // BaseNodes.Add(key, text, imageKey, selectedImageKey)

            if (String.IsNullOrEmpty(text))
            {
                // Exception, Text has always to be set
                //Set at least the name
                if (!String.IsNullOrEmpty(key)) { newnode.Name = key; }
            }
            else 
            {
                //  now set the rest of the given Values
                if (!String.IsNullOrEmpty(key))  {newnode.Name = key; }

                if (imageIndex != -1)  {newnode.ImageIndex = imageIndex;}

                if (selectedImageIndex != -1) { newnode.SelectedImageIndex = selectedImageIndex; }
                else
                {
                    newnode.SelectedImageIndex = imageIndex;
                }

                if (!String.IsNullOrEmpty( imageKey))  {newnode.ImageKey = imageKey;}

                if (!String.IsNullOrEmpty(SelectedImageKey))  {newnode.SelectedImageKey = SelectedImageKey;}
            }

            BaseNodes.Add(newnode);
            newnode.Parent = this.Parent;

			//FC:FINAL:DSE - Set StandardPicture for the new node
			SetStandardPicture(newnode);
			//DSE Internaly, nodes should be kept in the original adding order
			AddInternalNode(newnode);
            if (!Nodelist.ContainsKey(key))
            {
                Nodelist.Add(key, newnode);
            }
            return newnode;
        }


        /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <param name="intImageIndex">Index of the int image.</param>
        /// <param name="intSelectedImageIndex">Index of the int selected image.</param>
        /// <returns></returns>
        public FCTreeNode Add(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, int intSelectedImageIndex = -1)
        {
            //the variable that will hold the returning result
            FCTreeNode objNode = null;
            isDirty = true;

            //add the node without the image details
            objNode = Add(objRelative, enumRelationship, strKey, strText);

            if (!Nodelist.ContainsKey(strKey))
            {
                this.Nodelist.Add(strKey, objNode);
            }

            //assign ImageIndex
            if (intImageIndex != -1)
                objNode.ImageIndex = intImageIndex;

            //assign SelectedImageIndex
            if (intSelectedImageIndex != -1)
            {
                objNode.SelectedImageIndex = intSelectedImageIndex;
            }
            else
            {
                objNode.SelectedImageIndex = intImageIndex;
            }

			//FC:FINAL:DSE - Set StandardPicture for the new node
			SetStandardPicture(objNode);
            //return the node
            return objNode;
        }

        public FCTreeNode AddNode(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, int intSelectedImageIndex = -1)
        {
            return Add(objRelative, enumRelationship, strKey, strText, intImageIndex, intSelectedImageIndex);
        }

        public FCTreeNode Add(string objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText)
        {
            return Add(objRelative, enumRelationship, strKey, strText, -1);
        }

        public FCTreeNode Add(string objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, int intSelectedImageIndex = -1)
        {
            FCTreeNode relative = null;

            if (!string.IsNullOrEmpty(objRelative) && this.Nodelist != null && this.Nodelist.Count > 0)
            {
                relative = this.Nodelist[objRelative];
            }

            return Add(relative, enumRelationship, strKey, strText, intImageIndex, intSelectedImageIndex);
        }

        public FCTreeNode Add(string objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageIndex, string strSelectedImageIndex)
        {
            FCTreeNode relative = null;

            if (!string.IsNullOrEmpty(objRelative) && this.Nodelist != null && this.Nodelist.Count > 0)
            {
                //FC:HACK:AM
                //CHE: search in all nodes, not just this.Nodelist
                Dictionary<string, FCTreeNode> dict = GetAllNodes();
                if (dict.ContainsKey(objRelative))
                {
                    relative = dict[objRelative];
                }
                else
                {
                    string[] keys = objRelative.Split('\\');
                    if (keys.Length == 2)
                    {
                        relative = dict[keys[0]];
                        relative = relative.Nodes.GetAllNodes()[objRelative];
                    }
                }
            }

            return Add(relative, enumRelationship, strKey, strText, strImageIndex, strSelectedImageIndex);
        }

        /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <param name="strImageKey">The STR image key.</param>
        /// <param name="intSelectedImageIndex">Index of the int selected image.</param>
        /// <returns></returns>
        public FCTreeNode Add(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, int intSelectedImageIndex)
        {
            //the variable that will hold the returning result
            FCTreeNode objNode = null;

            //add the node without the image details
            objNode = Add(objRelative, enumRelationship, strKey, strText);

            //assign ImageIndex
            if (strImageKey != null)
                objNode.ImageKey = strImageKey;

            //assign SelectedImageIndex
            if (intSelectedImageIndex != -1)
            {
                objNode.SelectedImageIndex = intSelectedImageIndex;
            }
            else
            {
                objNode.SelectedImageKey = strImageKey;
            }
			//FC:FINAL:DSE - Set StandardPicture for the new node
			SetStandardPicture(objNode);

			//return the node
            return objNode;
        }
        public FCTreeNode AddNode(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, int intSelectedImageIndex)
        {
            return Add(objRelative, enumRelationship, strKey, strText, strImageKey, intSelectedImageIndex);
        }

        /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <param name="intImageIndex">Index of the int image.</param>
        /// <param name="strSelectedImageKey">The STR selected image key.</param>
        /// <returns></returns>
        public FCTreeNode Add(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, string strSelectedImageKey)
        {
            //the variable that will hold the returning result
            FCTreeNode objNode = null;

            //add the node without the image details
            objNode = Add(objRelative, enumRelationship, strKey, strText);

            //assign ImageIndex
            if (intImageIndex != -1)
                objNode.ImageIndex = intImageIndex;

            //assign SelectedImageIndex
            if (strSelectedImageKey != null)
                objNode.SelectedImageKey = strSelectedImageKey;

			//FC:FINAL:DSE - Set StandardPicture for the new node
			SetStandardPicture(objNode);

			//return the node
            return objNode;
        }

        public FCTreeNode AddNode(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, string strSelectedImageKey)
        {
              return Add(objRelative, enumRelationship, strKey, strText, intImageIndex, strSelectedImageKey);
        }
        /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <param name="strImageKey">The STR image key.</param>
        /// <param name="strSelectedImageKey">The STR selected image key.</param>
        /// <returns></returns>
        public FCTreeNode Add(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, string strSelectedImageKey)
        {
            //the variable that will hold the returning result
            FCTreeNode objNode = null;

            //add the node without the image details
            objNode = Add(objRelative, enumRelationship, strKey, strText);

            //check if we assign ImageKey
            if (strImageKey != null)
                objNode.ImageKey = strImageKey;

            //check if we assign SelectedImageKey
            if (strSelectedImageKey != null)
                objNode.SelectedImageKey = strSelectedImageKey;

			//FC:FINAL:DSE - Set StandardPicture for the new node
			SetStandardPicture(objNode);

			//return the node
            return objNode;
        }

        public FCTreeNode Add(int objRelativeIndex, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, string strSelectedImageKey)
        {
            FCTreeNode objRelative = this[objRelativeIndex];
            return Add(objRelative, enumRelationship, strKey, strText, strImageKey, strSelectedImageKey);
        }


        public FCTreeNode AddNode(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, string strSelectedImageKey)
        {
            return Add(objRelative, enumRelationship, strKey, strText, strImageKey, strSelectedImageKey);
        }

        /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <returns></returns>
        public FCTreeNode Add(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText)
        {
            //the variable that will hold the returning result
            FCTreeNode objNode = null;
            isDirty = true;
			//DSE check not to add same node twice
			bool nodeAdded = false;

            //check if there is a relative object specified
            if (objRelative == null)
            {
                //if no relative found, add the TreeNode at the end of the TreeView
                objNode = Add(strKey, strText);
				//DSE node already added
				nodeAdded = true;
            }
            else
            {
                //check for the relationship
                switch (enumRelationship)
                {
                    //case child
                    case TreeRelationshipConstants.tvwChild:

                        //add the Node to the Nodes collection of the specified relative Node
                        objNode = objRelative.Nodes.Add(strKey, strText);
                        break;

                    //case first
                    case TreeRelationshipConstants.tvwFirst:

                        //check if the relative Node has parent
                        if (objRelative.Parent == null)
                        {
                            //if it doesn't have parent then add it at the begining of the tree
                            Insert(0, new FCTreeNode(strText));
                            //assign the newly created TreeNode to the returning variable
                            objNode = this[0];
                        }
                        else
                        {
                            //if the relative node has parent, add the TreeNode to the begining of its parents Nodes collection
                            objRelative.Parent.Nodes.Insert(0, new FCTreeNode(strText));

                            //assign the newly created TreeNode to the returning variable
                            objNode = objRelative.Parent.Nodes[0];
                        }
                        //assign the Name property with the value passed from the Key paramether
                        objNode.Name = strKey;
                        break;

                    //case Last
                    case TreeRelationshipConstants.tvwLast:

                        //check if the relative Node has parent
                        if (objRelative.Parent == null)
                        {
                            //if it doesn't have a parent add it at the end of the TreeView
                            objNode = Add(strKey, strText);
							//DSE node already added
							nodeAdded = true;
                        }
                        else
                        {
                            //it it has parent add it at the end of the child nodes collection
                            objNode = objRelative.Parent.Nodes.Add(strKey, strText);
                        }
                        break;

                    //case Next
                    case TreeRelationshipConstants.tvwNext:

                        //check if the relative node has parent
                        if (objRelative.Parent == null)
                        {
                            //if it doesn't have, check if the selected node is the last
                            if (objRelative.Index + 1 == this.Count)
                            {
                                //if it's the last add it at the end
                                objNode = Add(strKey, strText);
								//DSE node already added
								nodeAdded = true;
                            }
                            else
                            {
                                //if the selected node is not the last add it on the next index
                                Insert(objRelative.Index + 1, new FCTreeNode(strText));

                                //asign the newly created TreeNode to the return variable
                                objNode = this[objRelative.Index + 1];

                                //assign the Name property with the value passed from the Key paramether
                                objNode.Name = strKey;
                            }
                        }
                        else
                        {
                            //if it has parent check if it is the last node in the collection
                            if (objRelative.Index + 1 == this.Count)
                            {
                                //if it's the last add the new node at the end
                                objNode = objRelative.Parent.Nodes.Add(strKey, strText);
                            }
                            else
                            {
                                //if it's not the last, insert it after the selected index
                                //asign the newly created TreeNode to the return variable
                                objNode = new FCTreeNode(strText);
                                //assign the Name property with the value passed from the Key paramether
                                objNode.Name = strKey;
                                objNode.Parent = objRelative.Parent;
                                objRelative.Parent.Nodes.Insert(objRelative, objNode, 1);
                                //objNode = objRelative.Parent.Nodes[objRelative.Index + 1];

                            }
                        }

                        break;

                    //case Previous
                    case TreeRelationshipConstants.tvwPrevious:

                        //check if the realtive object has parent
                        if (objRelative.Parent == null)
                        {
                            //if it doesn't have a parent add new node at the root of the tree with same index as the selected node
                            Insert(objRelative.Index, new FCTreeNode(strText));

                            //asign the newly created TreeNode to the return variable
                            objNode = this[objRelative.Index];
                        }
                        else
                        {
                            //if it has a parent add the node to its parents node collection
                            //objRelative.Parent.Nodes.Insert(objRelative.Index, new FCTreeNode(strText));
                            //asign the newly created TreeNode to the return variable
                            //objNode = objRelative.Parent.Nodes[objRelative.Index];
                            objNode = new FCTreeNode(strText);
                            objNode.Parent = objRelative.Parent;
                            objRelative.Parent.Nodes.Insert(objRelative, objNode, 0);
                        }
                        //assign the Name property with the value passed from the Key paramether
                        objNode.Name = strKey;
                        break;

                    default:
                        break;
                }
            }

            if (objNode.Parent != null && objNode.Parent.isExpanded)
            {
                objNode.Parent.Expand();
                objNode.TreeView.Nodes[0].EnsureVisible();
            }

            //FC:FINAL:DSE Due to WinForms bug, TreeView font should be set to BOLD and particular nodes to Regular
            //if (objNode.Parent != null && objNode.Parent.isBold)
            //{
            //    objNode.Parent.SetNodeFontBold(objNode.Parent.isBold);
            //}
            if (objNode.TreeView.Font.Bold)
            {
                objNode.Bold = false;
            }

			//DSE check not to add same node twice
			if (!nodeAdded)
			{
				AddInternalNode(objNode);
			}

			//return the new node
            return objNode;
        }
        public FCTreeNode AddNode(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText)
        {
            return Add(objRelative, enumRelationship, strKey, strText);
        }


        public void Clear()
        {
            Nodelist.Clear();
            BaseNodes.Clear();
			OrderedNodeList.Clear();
            isDirty = true;
        }

        public bool Contains(FCTreeNode value)
        {
            return OrderedNodeList.Contains(value);
        }

        public int IndexOf(FCTreeNode value)
        {
            return OrderedNodeList.IndexOf(value);
        }

        public void Insert(int index, FCTreeNode value)
        {
            OrderedNodeList.Insert(index, value);
            BaseNodes.Insert(index, value);
            GetAllNodes();
			//FC:FINAL:DSE - Set StandardPicture for the new node
			SetStandardPicture(value);
            isDirty = true;
        }

        public void Insert(FCTreeNode objRelativ, FCTreeNode newNode, int addValue)
        {
            OrderedNodeList.Insert(OrderedNodeList.IndexOf(objRelativ) + addValue, newNode);
            BaseNodes.Insert(BaseNodes.IndexOf(objRelativ) + addValue, newNode);
          //  BaseNodes.Insert(index, newNode);
            GetAllNodes();
            //FC:FINAL:DSE - Set StandardPicture for the new node
            SetStandardPicture(newNode);
            isDirty = true;
        }


        public bool IsFixedSize
        {
            get { return false; }
        }

        public bool IsReadOnly
        {
            get { return BaseNodes.IsReadOnly;}
        }

        public void Remove(FCTreeNode value)
        {
            Remove(value.Name);
            isDirty = true;
        }

        // Use this method to remove nodes
        public void Remove(string strNodeName)
        {
            FCTreeNode parent = Parent;
            //If the node is removed directly from the treeView, using treeView.Nodes.Remove function
            if (Parent == null)
            {
                //parent = Nodelist[strNodeName].Parent;
                parent = this[strNodeName].Parent;
                RemoveInternal(strNodeName);
            }
            RemoveFromParent(parent, strNodeName);
        }

        internal void RemoveFromParent(FCTreeNode parent, string nodeName)
        {
            if (parent == null)
            {
                return;
            }
            parent.Nodes.RemoveInternal(nodeName);
            parent = parent.Parent;
            RemoveFromParent(parent, nodeName);
        }

        internal void RemoveInternal(string strNodeName)
        {
            if (BaseNodes.ContainsKey(strNodeName))
            {
                BaseNodes.Remove(strNodeName);
            }
            Nodelist.Remove(strNodeName);
			//DSE remove node from internal list too
			OrderedNodeList.RemoveAll(delegate(FCTreeNode target)
			{
				return target.Name.ToUpper() == strNodeName.ToUpper();
			});
			isDirty = true;
        }

        public void RemoveAt(int index)
        {
            RemoveAt(index, true);
        }

        public void RemoveAt(int index, bool withSubNodes)
        { 
			FCTreeNode node = OrderedNodeList[index];
            if (withSubNodes && node.Nodes.Count > 0)
            {
                ClearNodes(node.Nodes);
                node.Nodes.Clear();
            }
            Remove(node.Name);
			isDirty = true;
        }

        public void Remove(int index)
        {
            RemoveAt(index);
        }

        public FCTreeNode this[int index]
        {
            get
            {
				//DSE Internaly, nodes should be kept in the original adding order
	            //KPF: QUICK Workaround
	            //TODO:KPF OrderedNodeList should contain all Nodes
                //PJU: adjust to OrderdNodeList; For Anadarko the previous Solution was not working
                //return GetAllNodes().Values.ToArray()[index];
                return OrderedNodeList[index];
            }
            set
            {
                isDirty = true;
				//DSE Internaly, nodes should be kept in the original adding order
				OrderedNodeList[index] = value;
            }
        }
        public FCTreeNode this[string key]
        {
            get
            {
				//DSE Internaly, nodes should be kept in the original adding order
				return OrderedNodeList.Find(delegate(FCTreeNode target)
				{
					return target.Name == key;
				});
            }
        }

        public void CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }
        
        /// <summary>
        /// Gets the number of sub nodes.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns></returns>
        public static int GetNodes(FCTreeNode node)
        {
            return node.Nodes.Count;
        }

        public int Count
        {
            get {
				//DSE Internaly, nodes should be kept in the original adding order
				return OrderedNodeList.Count;
			}
        }

        public int GetNumberOfAllNodes
        {
            get {
				//DSE Internaly, nodes should be kept in the original adding order
				return OrderedNodeList.Count;
			}
        }

        public int AllNodeCount(TreeNodeCollection value)
        {
            int retVal = value.Count;

            foreach (TreeNode item in value)
            {
                retVal += AllNodeCount(item.Nodes);
            }
            return retVal;
        }

        public bool IsSynchronized
        {
            get { return ((ICollection)BaseNodes).IsSynchronized; }
        }

        public object SyncRoot
        {
            get { return ((ICollection)BaseNodes).SyncRoot; }
        }

        public IEnumerator GetEnumerator()
        {
            return GetAllNodes().Values.GetEnumerator();
        }

        void ICollection.CopyTo(Array array, int index)
        {
            throw new NotImplementedException();
        }

        int ICollection.Count
        {
            get { return BaseNodes.Count; }
        }

        bool ICollection.IsSynchronized
        {
            get { return ((ICollection)BaseNodes).IsSynchronized; }
        }

        object ICollection.SyncRoot
        {
            get { return ((ICollection)BaseNodes).SyncRoot; }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetAllNodes().Values.GetEnumerator();
        }

        public int Add(object value)
        {
            throw new NotImplementedException();
        }

        public bool Contains(object value)
        {
            throw new NotImplementedException();
        }

        public int IndexOf(object value)
        {
            throw new NotImplementedException();
        }

        public void Insert(int index, object value)
        {
            throw new NotImplementedException();
        }

        public void Remove(object value)
        {
            throw new NotImplementedException();
        }

        object IList.this[int index]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public FCTreeNode[] Find(string RelativeKey, bool p)
        {
            throw new NotImplementedException();
        }

        public FCTreeNode Insert(int index, string Key, string Text, int imageIndex, int selectedImageIndex)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Finds a node in the collection
        /// </summary>
        /// <param name="objTreeNode">TreeNode object</param>
        /// <param name="strNodeName">Node name</param>
        /// <returns>TreeNode object</returns>
        public FCTreeNode FindNode(string strNodeName)
        {
            //Search the node collection and take the first node or default
            FCTreeNode objFindNode = Nodelist[strNodeName];

            //Return tree node object
            return objFindNode;
        }

		//FC:FINAL:DSE - Set StandardPicture for the new node
		private void SetStandardPicture(FCTreeNode newnode)
		{
			if (newnode == null)
			{
				return;
			}
			FCTreeView parentTree = newnode.TreeView as FCTreeView;
			if (parentTree != null)
			{
				if (newnode.ImageIndex == -1 && parentTree.StandardDefaultPicture > 0)
				{
					newnode.ImageKey = "PVTreeStandardDefaultPicture";
				}
			}
		}

		//DSE Internaly, nodes should be kept in the original adding order
		private void AddInternalNode(FCTreeNode node)
		{
			OrderedNodeList.Add(node);
		}

        /// <summary>
        /// BAN: recursively clear the child nodes from the OrderedNodeList, that way, when removing a parent node, 
        /// the child nodes will also be removed from the list
        /// </summary>
        /// <param name="nodes"></param>
        private void ClearNodes(FCTreeNodeCollection nodes)
        {
            foreach (FCTreeNode node in nodes)
            {
                if (node.Nodes.Count > 0)
                {
                    ClearNodes(node.Nodes);
                }
                //TB: remove the node from NodeList collection
                this.Nodelist.Remove(node.Name);
                OrderedNodeList.RemoveAll(delegate(FCTreeNode target)
                {
                    return target.Name.ToUpper() == node.Name.ToUpper();
                });
            }
        }
    }
}
