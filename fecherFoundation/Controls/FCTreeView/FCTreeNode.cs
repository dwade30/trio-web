﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;
using fecherFoundation.Extensions;
using System.Drawing;

namespace fecherFoundation
{
    public enum CheckBoxConstant
    {
        pvtChkBoxRegular = 0
    }

    public  class FCTreeNode : TreeNode
    {
        internal bool isExpanded = false;
        internal bool isBold = false;

        public new FCTreeNodeCollection Nodes;
        private FCTreeNode parent;
        private bool inSetParent = false;
        //FC:FINAL:ZSA - 246 - wrong icons displayed on node , fixed to simulate vb6
        public string Image
        {
            get
            {
                return this.ImageKey;
            }
            set
            {
                this.ImageKey = value;
            }
        }
        //FC:FINAL:ZSA - 246 - wrong icons displayed on node , fixed to simulate vb6
        public string SelectedImage
        {
            get
            {
                return this.SelectedImageKey;
            }
            set
            {
                this.SelectedImageKey = value;
            }
        }

        public new FCTreeNode PrevNode
        {
            get
            {
                return (FCTreeNode)base.PrevNode;
            }
        }

        public bool Bold
        {
            get
            {
                return isBold;
            }
            set
            {
                isBold = value;
                SetNodeFontBold(isBold);
            }
        }

        public new int ImageIndex
        {
            get
            {
                return base.ImageIndex;
            }
            set
            {
                base.ImageIndex = value;
                if (base.SelectedImageIndex == -1 && base.ImageIndex >= 0)
                {
                    base.SelectedImageIndex = base.ImageIndex;
                }
            }
        }

        public CheckBoxConstant CheckBoxType { get; set; }

        public FCTreeNode(string text) : base(text) 
        { 
            this.Nodes = new FCTreeNodeCollection(base.Nodes,this);
        }

        public FCTreeNode()
        {
            this.Nodes = new FCTreeNodeCollection(base.Nodes, this);
        }
        

        /// <summary>
        /// Set the selected state of TreeNode
        /// </summary>
        /// <param name="objTreeNode"></param>
        /// <param name="boolValue"></param>
        public void Selected( bool boolValue)
        {
            //check the value we recieve
            if (boolValue)
            {
                //if the value is true, select the treenode
                TreeView.SelectedNode = this;
            }
            else
            {
                //if the value is false, set selected node to null
                TreeView.SelectedNode = null;
            }
        }

        /// <summary>
        /// Sets the selected node.
        /// </summary>
        /// <param name="objTreeNode">The  tree node object.</param>
        /// <param name="intIndex">Index of the int.</param>
        /// <exception cref="System.NullReferenceException"></exception>
        public void SetSelected(bool blnValue)
        {
                // Get tree view control
            TreeView objTreeView = this.TreeView;

                // Set selected node 
                objTreeView.SelectedNode = this;

        }

        /// <summary>
        /// Get the selected state of TreeNode
        /// </summary>
        /// <param name="objTreeNode"></param>
        /// <returns></returns>
        public bool Selected()
        {
            //return the selected state of the node
            return IsSelected;
        }

        public new FCTreeNode NextNode
        {
            get 
            { 
                return (FCTreeNode)base.NextNode; 
            }
        }

        public new FCTreeNode FirstNode
        {
            get 
            { 
                return (FCTreeNode)base.FirstNode; 
            }
        }

        public new FCTreeNode LastNode
        {
            get 
            { 
                return (FCTreeNode)base.LastNode; 
            }
        }

        public FCTreeNode Child
        {
            get
            {
                if (this.Nodes.Count == 0)
                {
                    return null;
                }
                return this.Nodes[0];
            }
        }

        /// <summary>
        /// get root node - first level in tree
        /// </summary>
        public FCTreeNode Root
        {
            get
            {
                FCTreeNode node = this;
                while (node.Parent != null)
                {
                    node = node.Parent;
                }                
                return node;
            }
        }

        /// <summary>
        /// Returns a reference to the first sibling of a Node object in a TreeView control.
        /// The first sibling is the Node that appears in the first position in one level of a hierarchy of nodes. Which Node actually appears in the first position 
        /// depends on whether or not the Node objects at that level are sorted, which is determined by the Sorted property.
        /// </summary>
        public FCTreeNode FirstSibling
        {
            get
            {
                if(this.Parent ==null)
                {
                    if (this.TreeView == null || this.Nodes.Count == 0)
                    {
                        return null;
                    }
                    return this.TreeView.Nodes[0] as FCTreeNode;
                }
                else
                { 
                    return this.Parent.FirstNode;
                }
            }
        
        }

        /// <summary>
        /// Returns a reference to the last sibling of a Node object in a TreeView control. 
        /// The last sibling is the Node that appears in the last position in one level of a hierarchy of nodes. Which Node actually appears in the last position 
        /// depends on whether or not the Node objects at that level are sorted, which is determined by the Sorted property. To sort the Node objects at one level, 
        /// set the Sorted property of the Parent node to True.
        /// </summary>
        public FCTreeNode LastSibling
        {
            get
            {
                if (this.Parent == null)
                {
                    if (this.TreeView == null || this.Nodes.Count == 0)
                    {
                        return null;
                    }
                    return this.TreeView.Nodes[this.TreeView.Nodes.Count - 1] as FCTreeNode;
                }
                else
                { 
                    return this.Parent.LastNode; 
                }
            }
      
        }

        /// <summary>
        /// Returns a reference to the next sibling Node of a TreeView control's Node object. 
        /// </summary>
        public FCTreeNode Next
        {
            get
            {
                return this.NextNode;
            }
        }

        public new bool Checked
        {
            get
            {
                return base.Checked;
            }
            set
            {
                ((FCTreeView)this.TreeView).isInChecked = true;
                base.Checked = value;
                ((FCTreeView)this.TreeView).isInChecked = false;
            }
        }

        /// <summary>
        /// in VB6 Node.Index is 1 based but our implementation is considering 0 based
        /// </summary>
        public new int Index
        {
            get
            {
                int index = -1;
                FCTreeView treeView = this.TreeView as FCTreeView;
                if (treeView == null)
                {
                    return index;
                }

                index = treeView.Nodes.IndexOf(this);

                if (index != -1)
                {
                    return index;
                }

                foreach (FCTreeNode n in treeView.Nodes)
                {
                    index++;
                    if (n.Name == this.Name)
                    {
                        return index;
                    }
                }

                return index;
            }
        }

        public new FCTreeNode Parent
        {
            get
            {
                return parent;
            }

            set
            {
                if (!inSetParent)
                {
                    inSetParent = true;
                    if (parent != null)
                    {
                        this.parent.Nodes.RemoveAt(this.parent.Nodes.IndexOf(this), false);
                        value.Nodes.Add(this);

                    }
                    parent = value;
                    inSetParent = false;

                }
            }
        }

        /// <summary>
        /// Set the expanded state of TreeNode
        /// </summary>
        /// <param name="objTreeNode"></param>
        /// <param name="boolValue"></param>
        public void Expanded(bool boolValue)
        {
            this.isExpanded = boolValue;

            //check the value we recieve
            if (boolValue)
            {
                //if the value is true, expand the treenode
                base.Expand();
            }
            else
            {
                //if the value is false, colapse the treenode
                base.Collapse();
            }
        }

        /// <summary>
        /// Get the expanded state of TreeNode
        /// </summary>
        /// <param name="objTreeNode"></param>
        /// <returns></returns>
        public bool Expanded()
        {
            //return the expanded state of the node
            return base.IsExpanded;
        }

        /// <summary>
        /// Sorts the nodes of a node
        /// </summary>
        /// <param name="objTreeNode">TreeNode object</param>
        /// <param name="blnValue">bool value</param>
        public void SetSorted(bool blnValue)
        {
            // Null reference checking
            if (TreeView != null)
            {
                // Get TreeView control 
                TreeView objTreeView = TreeView;

                // Get current node index
                int intIndex = Index;

                // If true
                if (blnValue)
                {

                    // Begin update on tree view
                    objTreeView.BeginUpdate();

                    // Get the nodes of the node
                    FCTreeNodeCollection objNodeCollection = Nodes;

                    // Check if collection has items
                    if (objNodeCollection.Count > 0)
                    {

                        // Declare new list
                        List<FCTreeNode> objListNodes = new List<FCTreeNode>();

                        // Add all tree nodes to our list
                        foreach (FCTreeNode objNode in objNodeCollection)
                        {
                            objListNodes.Add(objNode);
                        }

                        // Sort it
                        var sortedList = objListNodes.OrderBy(t => t.Text).ToList();

                        // Clear the tree node collection
                        Nodes.Clear();

                        // Add the sorted nodes to the nodes collection
                        foreach (FCTreeNode objNode in sortedList)
                        {
                            objNodeCollection.Add(objNode);
                        }

                    }

                    // End update
                    TreeView.EndUpdate();

                }

                // Set bool value for Sorted
                GenericExtender.SetValue(objTreeView, this, intIndex, blnValue);
            }
            else
            {
                // Throw an exception
                throw new NullReferenceException();
            }
        }

        private List<FCTreeNode> GetNodes()
        {
            Dictionary<string, FCTreeNode> dic = GetAllNodes();

            List<FCTreeNode> list = new List<FCTreeNode>();

            foreach (FCTreeNode node in dic.Values)
            {
                list.Add(node);
            }
            
            return list;
        }

        public Dictionary<string, FCTreeNode> GetAllNodes()
        {
            Dictionary<string, FCTreeNode> list = new Dictionary<string, FCTreeNode>();

            list = GetAllNodes(Nodes, list);

            return list;
        }

        public Dictionary<string, FCTreeNode> GetAllNodes(FCTreeNodeCollection collection, Dictionary<string, FCTreeNode> list)
        {
            foreach (FCTreeNode item in collection)
            {
                if (string.IsNullOrEmpty(item.Name))
                {
                    list.Add(Guid.NewGuid().ToString(), item);
                }
                else
                {
                    list.Add(item.Name, item);
                }

                GetAllNodes(item.Nodes, list);
            }

            return list;
        }



        /// <summary>
        /// Sets the node font bold.
        /// </summary>
        /// <param name="objNode">The node object.</param>
        /// <param name="blnValue">if set to <c>true</c> [BLN value].</param>
        /// <exception cref="System.NullReferenceException"></exception>
        public void SetNodeFontBold(bool blnValue)
        {
            isBold = blnValue;

            // Get node font
            Font objFont = NodeFont;

            // If the objFont is null then node font is equal to TreeView control font
            if (objFont == null)
            {
                // Get TreeView control that this node is attached to.
                TreeView objTreeView = TreeView;

                // Get TreeView control font
                objFont = objTreeView.Font;
            }

            // This will be false if both values are false
            if (objFont.Bold | blnValue)
            {
                // If blnValue is true set bold otherwise set bold to false
                FontStyle objFontStyle = (blnValue) ? objFont.Style | FontStyle.Bold : objFont.Style ^ FontStyle.Bold;

                //BAN - if we set the font to bold, the text will not be displayed entirely. We need to reset the text after setting the font

                string objNodeText = Text;

                // Set new font
                Font font = new Font(objFont, objFontStyle);
                FCUtils.AddFont(font);
                NodeFont = font;
                FCUtils.DisposeFont(TreeView, objFont);

                Text = objNodeText;

                //FC:FINAL:DSE Due to WinForms bug, TreeView font should be set to BOLD and particular nodes to Regular
                if (blnValue && !this.TreeView.Font.Bold)
                {
                    Font font1 = new Font(this.TreeView.Font, this.TreeView.Font.Style | FontStyle.Bold);
                    FCUtils.AddFont(font1);
                    this.TreeView.Font = font1;
                }
            }
        }
    }
}
