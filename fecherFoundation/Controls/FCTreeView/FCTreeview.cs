﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;
using fecherFoundation.Extensions;
using System.ComponentModel;

namespace fecherFoundation
{
    #region Enums

    /// <summary>
    /// TreeLineStyleConstants enum
    /// </summary>
    public enum TreeLineStyleConstants
    {
        tvwTreeLines = 0,
        tvwRootLines = 1
    }

    public enum TreeStyleConstants : int
    {
        tvwPictureText = 1,
        tvwPlusMinusText = 2,
        tvwPlusPictureText = 3,
        tvwTextOnly = 0,
        tvwTreelinesPictureText = 5,
        tvwTreelinesPlusMinusPictureText = 7,
        tvwTreelinesPlusMinusText = 6,
        tvwTreelinesText = 4
    }

    public enum TreeRelationshipConstants
    {
        tvwChild = 4,
        tvwFirst = 0,
        tvwLast = 1,
        tvwNext = 2,
        tvwPrevious = 3
    }
    #endregion


    public partial class FCTreeView : TreeView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;


        #region members for fix for indentation issue on Vista and above
        private const int WM_MOUSEMOVE = 0x0200;
        private const int WM_LBUTTONDOWN = 0x0201;
        private const int WM_LBUTTONUP = 0x0202;
        private const int SB_HORZ = 0;
        private const int SIF_POS = 0x4;
        private const int TV_FIRST = 0x1100;
        private const int TVM_SETEXTENDEDSTYLE = (TV_FIRST + 44);
        private const int TVS_EX_DOUBLEBUFFER = 0x0004;
        private readonly bool isWinVista;
        #endregion

        public FCTreeViewDefinitions objTreeViewDefinitions = new FCTreeViewDefinitions();

        /// <summary>
        /// in VB6 Nodes collection is 1 based but our implementation is considering 0 based
        /// </summary>
        public new FCTreeNodeCollection Nodes;
        public TreeNodeCollection BaseNodes { get { return Nodes.BaseNodes; } }

        internal bool isInChecked = false;
        private TreeLineStyleConstants lineStyle = TreeLineStyleConstants.tvwTreeLines;

		//FC:FINAL:DSE - Property used only for PVTree, -2 meaning no image
		private int standardDefaultPicture = -2;

        /// <summary> 
        ///  the SingleSel property of a TreeView control determines whether a node expands to show its child nodes when selected. 
        ///  Setting this property to True causes the node to expand when selected and causes the previously selected node to contract.
        /// </summary>
        public bool SingleSel { get; set;}

        public void SetSingleSel(bool blnValue)
        {
            SingleSel = blnValue;
        }

        // TODO
        //public FlatStyle Appearance
        //{
        //    get
        //    {
        //        return (base.BorderStyle == Wisej.Web.BorderStyle.Solid) ? FlatStyle.Flat : FlatStyle.Standard;
        //    }
        //    set
        //    {
        //        switch (value)
        //        {
        //            case FlatStyle.Flat:
        //                base.BorderStyle = Wisej.Web.BorderStyle.Solid;
        //                break;
        //            default:
        //                base.BorderStyle = Wisej.Web.BorderStyle.Solid;
        //                break;
        //        }
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        public float Indentation
        {
            // TODO:CHE
            get;
            set;
        }
        /// <summary>
        /// Gets or sets a value indicating whether the label text of the tree nodes can be edited.
        /// </summary>
        public bool AllowInPlaceEditing {
            get
            {
              return base.LabelEdit;
            }
            set
            {
              base.LabelEdit = value  ;
            }
        }

        public TreeLineStyleConstants LineStyle
        {
            get
            {
                return lineStyle;
            }
            set
            {
                lineStyle = value;

                switch (this.Style)
                {
                    case TreeStyleConstants.tvwTreelinesPictureText:
                    case TreeStyleConstants.tvwTreelinesPlusMinusPictureText:
                    case TreeStyleConstants.tvwTreelinesPlusMinusText:
                    case TreeStyleConstants.tvwTreelinesText:
                        HandleRootLines();
                        break;
                }
            }
        }

        /// <summary>
        /// If set to true, the node is opend when the user clicks on it
        /// </summary>
        public bool AutoOpen { get; set; }

        [Obsolete("In Visual Basic 6.0, there is no property Data Member in the Treeview, comes from PVTreeViewX  infragistics")]
        public string DataMember { get; set;}

        [Obsolete("In Visual Basic 6.0, there is no property Data Member in the Treeview, comes from PVTreeViewX  infragistics")]
		public int StandardDefaultPicture
		{
			get
			{
				return standardDefaultPicture;
			}
			set
			{
				standardDefaultPicture = value;
				if (value > 0)
				{
					//DSE use saved ImageList
					//if (this.ImageList == null)
					//{
					//    this.ImageList = new ImageList();
					//}
					//if (!this.ImageList.Images.ContainsKey("PVTreeStandardDefaultPicture"))
					//{
					//    this.ImageList.Images.Add("PVTreeStandardDefaultPicture", global::fecherFoundation.Properties.Resources.PVTreeStandardDefaultPicture);
					//}
					if (this.objTreeViewDefinitions.ImageList == null)
					{
						this.objTreeViewDefinitions.ImageList = new ImageList();
					}
					if (!objTreeViewDefinitions.ImageList.Images.ContainsKey("PVTreeStandardDefaultPicture"))
					{
						objTreeViewDefinitions.ImageList.Images.Add("PVTreeStandardDefaultPicture", global::fecherFoundation.Properties.Resources.PVTreeStandardDefaultPicture);
					}
					if (this.Style != TreeStyleConstants.tvwTextOnly)
					{
						base.ImageList = this.objTreeViewDefinitions.ImageList;
					}
				}
			}
		}

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        public FCTreeNode DropHighlight { get; set; }

        public FCTreeView()
        {
            InitializeComponent();
            this.Nodes = new FCTreeNodeCollection(base.Nodes);

            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
        }

        #region Public Events

        public event EventHandler<FCTreeViewEventArgs> NodeCheck;
        public event EventHandler<FCTreeViewEventArgs> NodeClick;
        public event EventHandler<FCTreeViewEventArgs> Collapse;

        #endregion

        protected override void OnNodeMouseClick(TreeNodeMouseClickEventArgs e)
        {
            //DSE when NodeClick event arrives, SelectedNode should be matching the e.Node
            base.SelectedNode = e.Node;
            base.OnNodeMouseClick(e);
        }

        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            this.LabelEdit = true;

            this.Style = TreeStyleConstants.tvwTextOnly;
            this.LineStyle = TreeLineStyleConstants.tvwTreeLines;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public new FCTreeNode SelectedNode
        {
            get { return (FCTreeNode)base.SelectedNode; }

            set { base.SelectedNode = value; }   
        }

        /// <summary>
        /// Get a Treenode by PixelX, PixelY 
        /// Note: DragDrop-Events need to be calculated by PointToClient but not MouseEventArgs)
        /// </summary>
        /// <param name="PixelsX"></param>
        /// <param name="PixelsY"></param>
        /// <param name="clientCoordinates"></param>
        /// <returns></returns>
        public new FCTreeNode HitTest(int PixelsX, int PixelsY, bool clientCoordinates=true)
        {
            Point Position;
            Position = (clientCoordinates == true) ? Position = ((TreeView)this).PointToClient(new Point(PixelsX, PixelsY)) : new Point(PixelsX, PixelsY); 
            FCTreeNode treeNode = GetNodeAt(Position) as FCTreeNode;
            return treeNode;
        }

		/// <summary>
		/// HitTest using Client coordinates
		/// To be called from MouseUp, MouseDown, MouseClick events
		/// </summary>
		/// <param name="PixelsX"></param>
		/// <param name="PixelsY"></param>
		/// <returns></returns>
		public new FCTreeNode HitClientTest(int PixelsX, int PixelsY)
		{
			return GetNodeAt(PixelsX, PixelsY) as FCTreeNode;
		}

		//DSE Setting ImageList after setting the Style to TextOnly should let the base ImageList null
		public new ImageList ImageList
		{
			get
			{
				return base.ImageList;
			}
			set
			{
				if(value != null) {
					objTreeViewDefinitions.ImageList = value;
				}
				if (this.Style == TreeStyleConstants.tvwTextOnly)
				{
					base.ImageList = null;
				}
				else
				{
					base.ImageList = value;
				}
			}
		}

        protected override void OnAfterCheck(TreeViewEventArgs e)
        {
            if (SingleSel == true)
            {
                // select onle one child at a time
                // delselect all
                base.SelectedNode = null;
                base.SelectedNode = e.Node;
            }
            else
            {
                if (!isInChecked)
                {
                    if(NodeCheck != null)
                    {
                        FCTreeViewEventArgs fce = new FCTreeViewEventArgs(e);
                        NodeCheck(this, fce);
                    }
                    base.OnAfterCheck(e);
                }
            }
            if (AutoOpen == true)
            {
                e.Node.Expand();
            }
        }

        protected override void OnAfterSelect(TreeViewEventArgs e)
        {
            if (NodeClick != null)
            {
                FCTreeViewEventArgs fce = new FCTreeViewEventArgs(e);
                NodeClick(this, fce);
            }
            base.OnAfterSelect(e);
        }

        protected override void OnAfterCollapse(TreeViewEventArgs e)
        {
            if (Collapse != null)
            {
                FCTreeViewEventArgs fce = new FCTreeViewEventArgs(e);
                Collapse(this, fce);
            }
            base.OnAfterCollapse(e);
        }

        protected override void OnDragOver(DragEventArgs drgevent)
        {
            base.OnDragOver(drgevent);

            //Set an autoscroll region constant
            const Single scrollRegion = 20;

            // Get the cursor location
            Point pt = this.PointToClient(Cursor.Position);

            // Check if scroll up or down is needed
            if ((pt.Y + scrollRegion) > this.Height)
            {
                // Scroll down
                Externals.USER32.SendMessage(this.Handle, (uint)277, (IntPtr)1, (IntPtr)0);
            }
            else if (pt.Y < (this.Top + scrollRegion))
            {
                // Scroll up
                Externals.USER32.SendMessage(this.Handle, (uint)277, (IntPtr)0, (IntPtr)0);
            }
        }

        protected override void OnDragEnter(DragEventArgs drgevent)
        {
            drgevent.Effect = drgevent.AllowedEffect;
            base.OnDragEnter(drgevent);

        }

        protected override void OnDragDrop(DragEventArgs drgevent)
        {
            //CHE: get position relative to sender control not to screen
            Point senderRelativePoint = this.PointToClient(new Point(drgevent.X, drgevent.Y));
            // TODO
            //DragEventArgs dragEvent = new DragEventArgs(drgevent.Data, drgevent.KeyState, senderRelativePoint.X, senderRelativePoint.Y, drgevent.AllowedEffect, drgevent.Effect);
            //base.OnDragDrop(dragEvent);
        }
        
        // Function from extension
        /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <param name="intImageIndex">Index of the int image.</param>
        /// <param name="intSelectedImageIndex">Index of the int selected image.</param>
        /// <returns></returns>
        public FCTreeNode Add(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, int intSelectedImageIndex = -1)
        {
            //the variable that will hold the returning result
            FCTreeNode objNode = null;

            //add the node without the image details
            objNode = Nodes.Add(objRelative, enumRelationship, strKey, strText);

            //assign ImageIndex
            if (intImageIndex != -1)
                objNode.ImageIndex = intImageIndex;

            //assign SelectedImageIndex
            if (intSelectedImageIndex != -1)
            {
                objNode.SelectedImageIndex = intSelectedImageIndex;
            }
            else
            {
                objNode.SelectedImageIndex = intImageIndex;
            }

            //return the node
            return objNode;
        }

        /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <param name="strImageKey">The STR image key.</param>
        /// <param name="intSelectedImageIndex">Index of the int selected image.</param>
        /// <returns></returns>
        public FCTreeNode Add(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, int intSelectedImageIndex)
        {
            //the variable that will hold the returning result
            FCTreeNode objNode = null;

            //add the node without the image details
            objNode = Nodes.Add(objRelative, enumRelationship, strKey, strText);

            //assign ImageIndex
            if (strImageKey != null)
                objNode.ImageKey = strImageKey;

            //assign SelectedImageIndex
            if (intSelectedImageIndex != -1)
            {
                objNode.SelectedImageIndex = intSelectedImageIndex;
            }
            else
            {
                objNode.SelectedImageKey = strImageKey;
            }
            //return the node
            return objNode;
        }
        /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <param name="intImageIndex">Index of the int image.</param>
        /// <param name="strSelectedImageKey">The STR selected image key.</param>
        /// <returns></returns>
        public FCTreeNode Add(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, int intImageIndex, string strSelectedImageKey)
        {
            //the variable that will hold the returning result
            FCTreeNode objNode = null;

            //add the node without the image details
            objNode = Nodes.Add(objRelative, enumRelationship, strKey, strText);

            //assign ImageIndex
            if (intImageIndex != -1)
                objNode.ImageIndex = intImageIndex;

            //assign SelectedImageIndex
            if (strSelectedImageKey != null)
                objNode.SelectedImageKey = strSelectedImageKey;

            //return the node
            return objNode;
        }

        /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <param name="strImageKey">The STR image key.</param>
        /// <param name="strSelectedImageKey">The STR selected image key.</param>
        /// <returns></returns>
        public FCTreeNode Add(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText, string strImageKey, string strSelectedImageKey)
        {
            //the variable that will hold the returning result
            FCTreeNode objNode = null;

            //add the node without the image details
            objNode = Nodes.Add(objRelative, enumRelationship, strKey, strText);

            //check if we assign ImageKey
            if (strImageKey != null)
                objNode.ImageKey = strImageKey;

            //check if we assign SelectedImageKey
            if (strSelectedImageKey != null)
                objNode.SelectedImageKey = strSelectedImageKey;

            //return the node
            return objNode;
        }

                /// <summary>
        /// Adds the specified obj tree node.
        /// </summary>
        /// <param name="objTreeNode">The obj tree node.</param>
        /// <param name="objRelative">The obj relative.</param>
        /// <param name="enumRelationship">The enum relationship.</param>
        /// <param name="strKey">The STR key.</param>
        /// <param name="strText">The STR text.</param>
        /// <returns></returns>
        public FCTreeNode Add(FCTreeNode objRelative, TreeRelationshipConstants enumRelationship, string strKey, string strText)
        {
            //the variable that will hold the returning result
            FCTreeNode objNode = null;

            //add the node without the image details
            objNode = Nodes.Add(objRelative, enumRelationship, strKey, strText);

            //return the new node
            return objNode;
        }

        /// <summary>
        /// public method used recursively to pass through the nodes in the treeview
        /// </summary>
        /// <param name="objNode">Parent TreeNode object</param>
        /// <param name="objNodesCollection">TreeNode collection to store all treenodes</param>
        internal void BuildChildNodesList(FCTreeNode objNode, List<FCTreeNode> objNodesCollection)
        {
            // Add TreeNode to the collection
            Nodes.Add(objNode);

            // Loop through the nodes
            foreach (FCTreeNode objChild in objNode.Nodes)
            {
                // If this node object has child call the method again
                BuildChildNodesList(objChild, objNodesCollection);
            }
        }

        /// <summary>
        /// Sets the style of the TreeView
        /// </summary>
        /// <param name="objTreeView"></param>
        /// <param name="enuTreeStyle"></param>
        public TreeStyleConstants Style
        {
            set
            {
                //save the curent ImageList to the definition
                objTreeViewDefinitions.ImageList = ImageList;

                //check what style is being applied
                switch (value)
                {
                    case TreeStyleConstants.tvwTextOnly:
                        //hide plusminus
                        ShowPlusMinus = false;

                        ShowRootLines = false;

						ShowLines = false;

                        //remove the ImageList from the TreeView
						base.ImageList = null;
                        break;
                    case TreeStyleConstants.tvwPictureText:
                        //hide lines
                        ShowLines = false;

                        //hide plusminus
                        ShowPlusMinus = false;

                        //restore the previosly saved ImageList
                        ImageList = objTreeViewDefinitions.ImageList;
                        break;
                    case TreeStyleConstants.tvwPlusMinusText:
                        //hide lines
                        ShowLines = false;

                        //show plusminus
                        ShowPlusMinus = true;

                        //remove the ImageList from the TreeView
                        ImageList = null;
                        break;
                    case TreeStyleConstants.tvwPlusPictureText:
                        //hide lines
                        ShowLines = false;

                        //show plusminus
                        ShowPlusMinus = true;

                        //restore the previosly saved ImageList
                        ImageList = objTreeViewDefinitions.ImageList;
                        break;
                    case TreeStyleConstants.tvwTreelinesText:
                        //show lines
                        ShowLines = true;

                        //hide plusminus
                        ShowPlusMinus = false;

                        //remove the ImageList from the TreeView
                        ImageList = null;

                        HandleRootLines();
                        break;
                    case TreeStyleConstants.tvwTreelinesPictureText:
                        //show lines
                        ShowLines = true;

                        //hide plusminus
                        ShowPlusMinus = false;

                        //restore the previosly saved ImageList
                        ImageList = objTreeViewDefinitions.ImageList;

                        HandleRootLines();
                        break;
                    case TreeStyleConstants.tvwTreelinesPlusMinusText:
                        //show lines
                        ShowLines = true;

                        //show plusminus
                        ShowPlusMinus = true;

                        //remove the ImageList from the TreeView
                        ImageList = null;

                        HandleRootLines();
                        break;
                    case TreeStyleConstants.tvwTreelinesPlusMinusPictureText:
                        //show lines
                        ShowLines = true;

                        //show plusminus
                        ShowPlusMinus = true;

                        //restore the previosly saved ImageList
                        ImageList = objTreeViewDefinitions.ImageList;

                        HandleRootLines();
                        break;
                }

                //save the Style value in the definition
                objTreeViewDefinitions.Style = value;
            }
            get
            {
                return objTreeViewDefinitions.Style;
            }
        }

        public FCTreeNode GetNode(TreeNode node)
        {
            return this.Nodes.Nodelist[node.Name];
        }

        private void HandleRootLines()
        {
            if (this.lineStyle == TreeLineStyleConstants.tvwRootLines)
            {
                this.ShowRootLines = true;
            }
            else
            {
                this.ShowRootLines = false;
            }
        }

        #region Fix for indent issue on Vista and above

        /// <summary>
        /// The native TreeView (COMCTL 6.1) deployed on all current Vista and Windows7 installations introduced a bug, 
        /// which becomes apparent when using a non-default Indent style (> 19)
        /// 
        /// http://www.codeproject.com/Articles/122816/Bugfix-TreeView-indentation-on-Vista-Win
        /// 
        /// </summary>

        private static bool isWinVistaOrAbove
        {
            get
            {
                OperatingSystem OS = Environment.OSVersion;
                return (OS.Platform == PlatformID.Win32NT) && (OS.Version.Major >= 6);
            }
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);

            if (isWinVista)
            {
                Externals.USER32.SendMessage(Handle, TVM_SETEXTENDEDSTYLE, (IntPtr)TVS_EX_DOUBLEBUFFER, (IntPtr)TVS_EX_DOUBLEBUFFER);

                Externals.UXTHEME.SetWindowTheme(Handle, "explorer", null);
            }
        }

        // TODO
        //protected override void WndProc(ref Message m)
        //{
        //    switch (m.Msg)
        //    {
        //        case WM_MOUSEMOVE:
        //        case WM_LBUTTONDOWN:
        //        case WM_LBUTTONUP:
        //            //case WM_MOUSEHOVER:
        //            if (isWinVistaOrAbove && ShowPlusMinus && Indent > 19 && ImageList != null)
        //            {
        //                // lParam: cursor in client coords
        //                Point pt = new Point(Convert.ToInt32(m.LParam);

        //                if (translateMouseLocationCore(ref pt))
        //                {
        //                    m.LParam = MAKELPARAM(pt.X, pt.Y);
        //                }
        //            }
        //            break;
        //    }
        //    base.WndProc(ref m);
        //}

        private static IntPtr MAKELPARAM(int loWord, int hiWord)
        {
            return new IntPtr((loWord & 0xffff) | (hiWord << 16));
        }

        //private bool translateMouseLocationCore(ref Point pt)
        //{
        //    TreeViewHitTestInfo info = HitTest(pt);
        //    return translateMouseLocationCore(info, ref pt);
        //}

        //private bool translateMouseLocationCore(TreeViewHitTestInfo info, ref Point pt)
        //{
        //    Debug.Assert((isWinVistaOrAbove && ShowPlusMinus &&
        //                  Indent > 19 && ImageList != null));

        //    if (info.Node == null || info.Node.Nodes.Count == 0)
        //    {
        //        return false;
        //    }

        //    if ((info.Location & TreeViewHitTestLocations.Indent) != 0)
        //    {
        //        Externals.USER32.SCROLLINFO si = new Externals.USER32.SCROLLINFO
        //        {
        //            cbSize = Marshal.SizeOf(typeof(Externals.USER32.SCROLLINFO)),
        //            fMask = SIF_POS
        //        };
        //        Externals.USER32.GetScrollInfo(Handle, SB_HORZ, ref si);

        //        const int btnWidth2 = 8;            // half of button width
        //        int newX = pt.X + si.nPos;          // mouse in dc coords
        //        int level = info.Node.Level;

        //        if (ShowRootLines)
        //        {
        //            if (newX >= 3 + level * Indent)
        //            {
        //                // center of button in dc coords, that Vista expects
        //                newX = 3 + (level + 1) * Indent - btnWidth2;
        //                pt.X = newX - si.nPos;
        //                return true;
        //            }
        //        }
        //        else if (level > 0)
        //        {
        //            if (newX >= 3 + (level - 1) * Indent)
        //            {
        //                newX = 3 + level * Indent - btnWidth2;
        //                pt.X = newX - si.nPos;
        //                return true;
        //            }
        //        }
        //    }

        //    return false;
        //}

        #endregion

        #region Wisej missing methods
        public bool ShowLines { get; set; }

        private FCTreeNode GetNodeAt(int PixelsX, int PixelsY)
        {
            throw new NotImplementedException();
        }

        private FCTreeNode GetNodeAt(Point Position)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public class FCTreeViewDefinitions : ControlDefinitions
    {
        public  FCTreeViewDefinitions GetTreeViewDefinitions(FCTreeView objTreeView)
        {
            FCTreeViewDefinitions objTreeViewDefinitions;
            objTreeViewDefinitions = new FCTreeViewDefinitions();
            return objTreeViewDefinitions;
        }

        public ImageList mobjImageList = null;
        public TreeStyleConstants mobjStyle = TreeStyleConstants.tvwTreelinesPlusMinusPictureText;

        public ImageList ImageList
        {
            get
            {
                return mobjImageList;
            }
            set
            {
                if(value != null)
                    mobjImageList = value;
            }
        }

        public TreeStyleConstants Style
        {
            get
            {
                return mobjStyle;
            }
            set
            {
                mobjStyle = value;
            }
        }

    }
}


