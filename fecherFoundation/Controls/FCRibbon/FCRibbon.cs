﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    public class FCRibbon : FCButton, ISupportInitialize
    {
        #region Private Members

        private bool val;
        private Image pictureUp = null;
        private Image pictureDn = null;
        private Image pictureDisabled = null;

        #endregion

        #region Public Events

        public event EventHandler<FCRibbonEventArgs> ClickEvent;

        #endregion

        #region Properties

        //TODO: @@TB add logic here
        public bool Value
        {
            get
            {
                return val;
            }
            set
            {
                //CHE: raise click event only when value is changing
                bool valueChanged = (val != value);
                val = value;
                SetImages();
                if (ClickEvent != null && valueChanged)
                {
                    FCRibbonEventArgs e = new FCRibbonEventArgs(val);
                    ClickEvent(this, e);
                }
            }
        }

        [DefaultValue(null)]
        public Image PictureUp
        {
            get
            {
                return pictureUp;
            }

            set
            {
                pictureUp = value;
                SetImages();
            }
        }

        [DefaultValue(null)]
        public Image PictureDn
        {
            get
            {
                return pictureDn;
            }

            set
            {
                pictureDn = value;
                SetImages();
            }
        }

        [DefaultValue(null)]
        public Image PictureDisabled
        {
            get
            {
                return pictureDisabled;
            }

            set
            {
                pictureDisabled = value;
                SetImages();
            }
        }

        #endregion

        #region Public Methods

        public void BeginInit()
        {

        }

        public void EndInit()
        {

        }
        #endregion

        #region Protected Methods

        protected override void OnClick(EventArgs e)
        {
            base.OnClick(e);
            this.Value = !this.Value;
        }

        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);
            SetImages();
        }

        #endregion

        #region Private Methods

        private void SetImages()
        {
            if (this.Enabled)
            {
                if (this.Value)
                {
                    this.BackgroundImage = this.pictureDn;
                }
                else
                {
                    this.BackgroundImage = this.pictureUp;
                }
            }
            else
            {
                this.BackgroundImage = this.pictureDisabled;
            }
        }

        #endregion
    }

    public class FCRibbonEventArgs : EventArgs
    {
        private bool value;

        public FCRibbonEventArgs(bool value)
        {
            this.value = value;
        }

        public bool Value
        {
            get
            {
                return value;
            }
        }

    }
}
