﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace fecherFoundation
{
    /// <summary>
    /// VB.Line
    /// </summary>
    public class FCThreeDLine : Panel
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private DisplayMode displayMode;
        private int thickness = 1;

        #endregion

        #region Constructors

        public FCThreeDLine()
        {
            this.BorderStyle = BorderStyle.FixedSingle;
            this.Color = Color.Black;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        /// <summary>
        /// The BorderStyle must be FixedSingle in order for the line to be displayed correctly
        /// </summary>
        [DefaultValue(BorderStyle.FixedSingle)]
        public new BorderStyle BorderStyle
        {
            get { return base.BorderStyle; }
            set { base.BorderStyle = value; }
        }

        /// <summary>
        /// When setting the Size of the FCThreeDLine, based on the DisplaySize, the width or height cannot be larger than 2 pixels.
        /// If the DisplaySize is horizontal, the height of the line cannot be larger than 2 pixels
        /// If the DisplaySize is vertical, the width of the line cannot be larger than 2 pixels
        /// </summary>
        public new Size Size
        {
            get { return base.Size; }
            set
            {
                base.Size = CalculateSize(value);

            }
        }

        /// <summary>
        /// Gets/sets the thickness of the line
        /// </summary>
        [DefaultValue(1)]
        public int Thickness
        {
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                this.thickness = value;
                base.Size = CalculateSize(this.Size);
            }
            get
            {
                return this.thickness;
            }
        }

        /// <summary>
        /// Gets/sets the color of the line
        /// </summary>
        public Color Color
        {
            set
            {
                base.BackColor = value;
            }
            get
            {
                return base.BackColor;
            }
        }

        /// <summary>
        /// The FCThreeDLine can be displayed vertically or horizontally. Based on the setting, keep the location of the control
        /// but inverse the width/height in order to display the line accordingly
        /// </summary>
        [DefaultValue(DisplayMode.Horizontal)]
        public DisplayMode DisplayMode
        {
            set
            {
                if (this.displayMode != value)
                {
                    this.displayMode = value;
                    // interchange width and height
                    base.Size = CalculateSize(new Size(this.Height, this.Width));
                }
            }
            get
            {
                return this.displayMode;
            }
        }

        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _ExtentX { get; set; }

        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _ExtentY { get; set; }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        /// <summary>
        /// Calculates the size of the line when changing its display mode
        /// When the display mode changes, the location of the line remains constant but the width/height are inversed
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        private Size CalculateSize(Size size)
        {
            Size newSize = new Size();
            if (this.DisplayMode == DisplayMode.Horizontal)
            {
                newSize.Width = size.Width;
                newSize.Height = thickness;
            }
            else
            {
                newSize.Width = thickness;
                newSize.Height = size.Height;
            }
            return newSize;
        }

        #endregion
    }
}
