﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    /// <summary>
    /// Threed.SSPanel
    ///is derived from Button and not Panel, otherwise it cannot have text in the middle of it
    /// </summary>
    public class FCPanel : Panel, ISupportInitialize
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private BevelConstants bevelOuter;
        private AlignPanelTextConstants alignment;

        private AutoSizePanelConstants autoSizePanel = AutoSizePanelConstants._None;
        private bool outline = false;

        #endregion

        #region Constructors

        public FCPanel()
        {
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            //CHE: set default value
            //this.Alignment = AlignPanelTextConstants._CenterMiddle;
            this.BevelOuter = BevelConstants._Raised;
            this.AutoSizePanel = AutoSizePanelConstants._None;
            this.Font3D = Font3DConstants._None;
            this.Outline = false;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums

        public enum AutoSizePanelConstants
        {
            _None = 0,
            _WidthToCaption = 1,
            _HeightToCaption = 2,
            _ChildToPanel = 3
        }

        #endregion

        #region Properties

        //[DefaultValue(true)]
        //public bool ClipControls
        //{
        //    // TODO:CHE
        //    get;
        //    set;
        //}

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        public new string Text
        {
            get
            {
                return base.Text;
            }
            set
            {
                base.Text = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(BevelConstants._Raised)]
        public BevelConstants BevelOuter
        {
            get
            {
                return bevelOuter;
            }
            set
            {
                bevelOuter = value;
                switch (bevelOuter)
                {
                    case BevelConstants._None:
                        {
                            if (!outline)
                            {
                                //this.FlatStyle = Wisej.Web.FlatStyle.Flat;
                                //this.FlatAppearance.BorderSize = 0;
                            }
                            break;
                        }
                    case BevelConstants._Raised:
                        {
                            //this.FlatStyle = Wisej.Web.FlatStyle.Standard;
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        //[DefaultValue(AlignPanelTextConstants._CenterMiddle)]
        //public AlignPanelTextConstants Alignment
        //{
        //    get
        //    {
        //        return alignment;
        //    }
        //    set
        //    {
        //        alignment = value;
        //        switch (alignment)
        //        {
        //            case AlignPanelTextConstants._CenterBottom:
        //                {
        //                    this.TextAlign = ContentAlignment.BottomCenter; 
        //                    break;
        //                }
        //            case AlignPanelTextConstants._CenterMiddle:
        //                {
        //                    this.TextAlign = ContentAlignment.MiddleCenter;
        //                    break;
        //                }
        //            case AlignPanelTextConstants._CenterTop:
        //                {
        //                    this.TextAlign = ContentAlignment.TopCenter;
        //                    break;
        //                }
        //            case AlignPanelTextConstants._LeftBottom:
        //                {
        //                    this.TextAlign = ContentAlignment.BottomLeft;
        //                    break;
        //                }
        //            case AlignPanelTextConstants._LeftMiddle:
        //                {
        //                    this.TextAlign = ContentAlignment.MiddleLeft;
        //                    break;
        //                }
        //            case AlignPanelTextConstants._LeftTop:
        //                {
        //                    this.TextAlign = ContentAlignment.TopLeft;
        //                    break;
        //                }
        //            case AlignPanelTextConstants._RightBottom:
        //                {
        //                    this.TextAlign = ContentAlignment.BottomRight;
        //                    break;
        //                }
        //            case AlignPanelTextConstants._RightMiddle:
        //                {
        //                    break;
        //                }
        //            case AlignPanelTextConstants._RightTop:
        //                {
        //                    this.TextAlign = ContentAlignment.TopRight;
        //                    break;
        //                }
        //        }
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        //[DefaultValue(1)]
        //public int BevelWith
        //{
        //    get
        //    {
        //        return this.FlatAppearance.BorderSize;
        //    }
        //    set
        //    {
        //        this.FlatAppearance.BorderSize = value;
        //    }
        //}

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(AutoSizePanelConstants._None)]
        public AutoSizePanelConstants AutoSizePanel
        {
            get
            {
                return autoSizePanel;
            }
            set
            {
                autoSizePanel = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(Font3DConstants._None)]
        public Font3DConstants Font3D
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the size of the font that is used for text in a run-time printing operation.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float FontSize
        {
            get
            {
                return this.graphicsFactory.FontSize;
            }
            set
            {
                this.SetFontSize(value);
                this.graphicsFactory.FontSize = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(false)]
        public bool Outline
        {
            get
            {
                return outline;
            }
            set
            {
                outline = value;
                if (outline)
                {
                    //this.FlatStyle = Wisej.Web.FlatStyle.Standard;
                    //this.FlatAppearance.BorderSize = 1;
                }
            }
        }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int _ExtentX { get; set; }

        [Obsolete("Not implemented. Added for migration compatibility")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int _ExtentY { get; set; }
        //public BorderStyle BorderStyle { get; set; }

        //public int FloodColor
        //{
        //    get;
        //    set;
        //}
        #endregion

        #region Public Methods

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }
        
        /// <summary>
        /// Begins a drag-and-drop operation.
        /// </summary>
        /// <param name="action"></param>
        public void Drag(DragConstants action)
        {
            throw new NotImplementedException();
        }

        public void BeginInit()
        {
        }

        public void EndInit()
        {
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
