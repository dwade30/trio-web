﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;

namespace fecherFoundation
{
    /// <summary>
    /// ListBox with items that can be ordered by drag & drop
    /// </summary>
    public class FCDraggableListBox : FCListBox
    {
        public FCDraggableListBox()
        {
            this.AllowDrag = true;
            this.AllowDrop = true;
            this.View = View.SmallIcon;
            this.ItemSize = new Size(this.Width - 20,32);
            this.DragStart += FCDraggableListBox_DragStart;
            this.DragDrop += FCDraggableListBox_DragDrop;
            this.Resize += FCDraggableListBox_Resize;
        }

        private void FCDraggableListBox_DragStart(object sender, EventArgs e)
        {
            var item = this.FocusedItem;
            if (item == null)
            {
                return;
            }

            this.DoDragDrop(this.FocusedItem, DragDropEffects.Move);
        }

        private void FCDraggableListBox_Resize(object sender, EventArgs e)
        {
            this.ItemSize = new Size(this.Width - 20,32);
        }

        
      

        private void FCDraggableListBox_DragDrop(object sender, Wisej.Web.DragEventArgs e)
        {
            //int index = (e.DropTarget as ListViewItem).Index;
            //ListViewItem data = (ListViewItem)e.Data.GetData(typeof(ListViewItem));
            //if (data != null)
            //{
            //    var sourceIndex = data.Index;
            //    this.Items.Remove(data);
            //    this.Items.Insert(index, data);
            //}
            //this.Items[index].Focused = true;
            //this.Items[index].Selected = true;


            var item = e.Data.GetData(typeof(ListViewItem)) as ListViewItem;
            if (item != null)
            {
                var listView = (ListView)sender;
                var target = e.DropTarget as ListViewItem;
                if (target == null)
                {
                    // listView.Items.Add(item);
                }
                else
                {
                    listView.Items.Insert(target.Index, item);
                }
                listView.FocusedItem = item;
                item.Selected = true;
            }
        }

    }
}
