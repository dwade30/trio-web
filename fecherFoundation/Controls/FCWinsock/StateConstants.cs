﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation
{
    public enum StateConstants
    {
        sckClosed, // Default
        sckClosing,
        sckConnected,
        sckConnecting,
        sckConnectionPending,
        sckError,
        sckHostResolved,
        sckListening,
        sckOpen,
        sckResolvingHost
    }
}
