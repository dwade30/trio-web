﻿using System.Net.Sockets;

namespace fecherFoundation
{
    public class ConnectionRequestEventArgs
    {
        public Socket requestID;

        public ConnectionRequestEventArgs(Socket s)
        {
            requestID = s;
        }
    }
}
