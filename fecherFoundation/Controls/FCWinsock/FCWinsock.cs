﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Threading;
using System.Net.Sockets;

namespace fecherFoundation
{
    public class StateObject
    {
        // Client socket.
        public Socket workSocket = null;
        // Size of receive buffer.
        public const int BufferSize = 256;
        // Receive buffer.
        public byte[] buffer = new byte[BufferSize];
        // Received data string.
        public StringBuilder sb = new StringBuilder();
    }

    public partial class FCWinsock : Component
    {
        #region Public Members

        public string RemoteHost = "192.168.5.1";
        public int RemotePort = 5150;

        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        // ManualResetEvent instances signal completion.
        private ManualResetEvent connectDone =
            new ManualResetEvent(false);
        private ManualResetEvent sendDone =
            new ManualResetEvent(false);
        private ManualResetEvent receiveDone =
            new ManualResetEvent(false);

        // The response from the remote device.
        private String response = String.Empty;
        private byte[] responseByteArray = null;

        private Socket client = null;

        #endregion

        #region Constructors

        public FCWinsock()
        {
            InitializeComponent();
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events

        public event EventHandler DataArrival;

        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public int State
        {
            get
            {
                return Convert.ToInt32(this.client.Connected);
            }
        }
        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        public void Close()
        {
            if (this.client != null)
            {
                this.client.Close();
            }
        }

        public void SendData(string data)
        {
            // Send test data to the remote device.
            Send(client, data);
            WaitResponse();
        }

        public void SendData(byte[] data)
        {
            // Send test data to the remote device.
            SendBytes(client, data);
            WaitResponse();
        }

        public void GetData(ref string data)
        {
            data = response;
        }

        public void GetData(ref byte[] data)
        {
            data = responseByteArray;
        }

        public void Connect(string RemoteHost, int RemotePort)
        {
            this.RemoteHost = RemoteHost;
            this.RemotePort = RemotePort;

            // Establish the remote endpoint for the socket.
            // The name of the 
            // remote device is "host.contoso.com".
            IPHostEntry ipHostInfo = Dns.Resolve(RemoteHost);
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, RemotePort);

            // Create a TCP/IP socket.
            client = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream, ProtocolType.Tcp);

            // Connect to the remote endpoint.
            client.BeginConnect(remoteEP,
                new AsyncCallback(ConnectCallback), client);
            connectDone.WaitOne();
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;

                // Complete the connection.
                client.EndConnect(ar);

                //Console.WriteLine("Socket connected to {0}", client.RemoteEndPoint.ToString());

                // Signal that the connection has been made.
                connectDone.Set();
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.ToString());

                //CHE: set flag when error occurs otherwise execution is hang
                connectDone.Set();
            }
        }

        private void Receive(Socket client)
        {
            try
            {
                // Create the state object.
                StateObject state = new StateObject();
                state.workSocket = client;

                // Begin receiving the data from the remote device.
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReceiveCallback), state);
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.ToString());
            }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the client socket 
                // from the asynchronous state object.
                StateObject state = (StateObject)ar.AsyncState;
                Socket client = state.workSocket;

                // Read data from the remote device.
                int bytesRead = client.EndReceive(ar);

                if (bytesRead > 0)
                {
                    // There might be more data, so store the data received so far.
                    state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                    //// Get the rest of the data.
                    //client.BeginReceive(state.buffer, 0, StateObject1.BufferSize, 0,
                    //    new AsyncCallback(ReceiveCallback), state);
                }
                //CHE: it seems data was read once, no need to call again, set receiveDone instead
                //else
                {
                    // All the data has arrived; put it in response.
                    if (state.sb.Length > 1)
                    {
                        response = state.sb.ToString();
                        responseByteArray = Encoding.ASCII.GetBytes(response);

                        if (DataArrival != null)
                        {
                            DataArrival(this, EventArgs.Empty);
                        }
                    }

                    // Signal that all bytes have been received.
                    receiveDone.Set();
                }
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.ToString());

                //CHE: set flag when error occurs otherwise execution is hang
                receiveDone.Set();
            }
        }

        private void Send(Socket client, String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            SendBytes(client, byteData);
        }
        
        private void SendBytes(Socket client, byte[] byteData)
        {
            // Begin sending the data to the remote device.
            client.BeginSend(byteData, 0, byteData.Length, 0,
                new AsyncCallback(SendCallback), client);
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;

                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);
                //Console.WriteLine("Sent {0} bytes to server.", bytesSent);

                // Signal that all bytes have been sent.
                sendDone.Set();
            }
            catch (Exception e)
            {
                //Console.WriteLine(e.ToString());

                //CHE: set flag when error occurs otherwise execution is hang
                sendDone.Set();
            }
        }

        private void WaitResponse()
        {
            sendDone.WaitOne();

            // Receive the response from the remote device.
            Receive(client);
            receiveDone.WaitOne();
        }

        #endregion
    }
}
