﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    /// <summary>
    /// TabDlg.SSTab
    /// ComctlLib.TabStrip
    /// </summary>
    public partial class FCTabControl : TabControl
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private Dictionary<int, bool> tabPageIsEnabled;
        private Dictionary<int, bool> tabPageIsVisible;
        private FCTabPageCollection tabPages;
        private int tabsPerRow;

        #endregion

        #region Constructors

        public FCTabControl()
        {
            InitializeComponent();
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            tabPageIsEnabled = new Dictionary<int, bool>();
            tabPageIsVisible = new Dictionary<int, bool>();
            tabPages = new FCTabPageCollection(this);

            //CHE: disable owner drawing and default values, Tabs, TabHeight and TabsPerRow are not used in DeutscheBahn project
            //this.DrawMode = TabDrawMode.OwnerDrawFixed;
            //this.DrawItem += FCTabControl_DrawItem;
            //CHE: set default value
            //this.TabsPerRow = 3;
            //this.Tabs = 3;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Tab
        {
            get
            {
                return this.TabPages.IndexOf(this.SelectedTab as FCTabPage);
            }
            set
            {
                this.SelectedTab = this.TabPages[value];
            }
        }

        [DefaultValue(0)]
        public int TabHeight
        {
            get
            {
                if (this.tabPages.Count == 0)
                {
                    return 0;
                }
                return this.HeightOriginal - this.tabPages[0].HeightOriginal;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        //CHE: disable owner drawing and default values, Tabs, TabHeight and TabsPerRow are not used in DeutscheBahn project
        ///// <summary>
        ///// Returns or sets the number of tabs for each row of an SSTab control.
        ///// Remarks
        ///// Use this property at design time in conjunction with the Tabs property to determine the number of rows 
        ///// displayed by the control. At run time, use the Rows property.
        ///// </summary>
        //[DefaultValue(3)]
        //public int TabsPerRow
        //{
        //    get
        //    {
        //        return tabsPerRow;
        //    }
        //    set
        //    {
        //        tabsPerRow = value;
        //        SetMultiline();
        //    }
        //}

        //CHE: disable owner drawing and default values, Tabs, TabHeight and TabsPerRow are not used in DeutscheBahn project
        ///// <summary>
        ///// Returns or sets the height of all tabs on an SSTab control.
        ///// </summary>
        //public float TabHeight
        //{
        //    // TODO:CHE
        //    get;
        //    set;
        //}

        //CHE: disable owner drawing and default values, Tabs, TabHeight and TabsPerRow are not used in DeutscheBahn project
        ///// <summary>
        ///// Returns a reference to the collection of Tab objects in a TabStrip control.
        ///// Syntax
        ///// object.Tabs(index)
        ///// The Tabs property syntax has these parts:
        ///// Part	Description
        ///// object	An object expression that evaluates to a TabStrip control.
        ///// index	A value that identifies a Tab object in the Tabs collection. This may either be the Index property or the Key property of the desired Tab object.
        ///// Remarks
        ///// The Tabs collection can be accessed by using the standard collection methods, such as the Item method.
        ///// </summary>
        //[DefaultValue(3)]
        //public int Tabs
        //{
        //    get
        //    {
        //        return this.TabPages.Count;
        //    }
        //    set
        //    {
        //        try
        //        {
        //            this.TabPages.Clear();
        //            for (int counter = 0; counter < value; counter++)
        //            {
        //                FCTabPage tabPage = new FCTabPage();
        //                tabPage.Text = "Tab " + counter;
        //                this.TabPages.Add(tabPage);
        //            }
        //        }
        //        catch
        //        {
        //        }
        //        SetMultiline();
        //    }
        //}

        //in VB6 SSTab.Caption refers to caption of current tab page
        public new string Text
        {
            get
            {
                if (this.TabPages.Count > 0 && this.SelectedIndex >= 0)
                {
                    return this.TabPages[this.SelectedIndex].Text;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (this.TabPages.Count > 0 && this.SelectedIndex >= 0)
                {
                    this.TabPages[this.SelectedIndex].Text = value;
                }
            }
        }

        public bool WordWrap
        {
            // TODO:LSO
            get;
            set;
         }

        public int TabsPerRow
        {
            // TODO:LSO
            get;
            set;
        }

        public bool ShowFocusRect
        {
            // TODO:LSO
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ClientWidth
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(this.ClientWidthPixels, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    this.ClientWidthPixels = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ClientHeight
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(this.ClientHeightPixels, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    this.ClientHeightPixels = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ClientLeft
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(this.ClientLeftPixels, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    this.ClientLeftPixels = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ClientTop
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(this.ClientTopPixels, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    this.ClientTopPixels = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        //
        // Summary:
        //     Returns or sets the index of the currently selected tab page.
        //
        // Returns:
        //     The zero-based index of the currently selected tab page. The default is -1, which
        //     is also the value if no tab page is selected.
        //
        // Exceptions:
        //   T:System.ArgumentOutOfRangeException:
        //     The value is less than -1.
        [DefaultValue(-1)]
        //FC:FINAL:SBE - Harris #i2328 - implement SelectedIndex property. When some tab pages are not visible, we remove them form the base TabPage collection, and base SelectedIndex does not correspond with our TabPage collection
        public int SelectedIndex
        {
            get
            {
                var selectedTab = SelectedTab as FCTabPage;
                if (selectedTab != null)
                {
                    return TabPages.IndexOf(selectedTab);
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                if (value >= 0 && value < TabPages.Count)
                {
                    SelectedTab = TabPages[value];
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public FCTabPage SelectedItem
        {
            get 
            {
                return this.TabPages[this.SelectedIndex];
            }
        }

        public new FCTabPageCollection TabPages
        {
            get
            {
                return this.tabPages;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool DisableEvents
        {
            get;
            set;
        }

        /// <summary>
        /// Returns the previously selected tab index
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int PreviousTab
        {
            get;
            protected set;
        }

        #endregion

        #region Internal Properties

        /// <summary>
        /// Gets/sets the top coordinate of the internal area of the control
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        internal int ClientTopPixels
        {
            get
            {
                // Check if there is tab pages and check aligment
                if (this.TabCount > 0 && this.Alignment == TabAlignment.Top)
                {
                    // Create tab page object
                    TabPage tabPage = this.TabPages[0];

                    // Create rectangle object
                    Rectangle rectangle = tabPage.Bounds;

                    // Get button height
                    int height = rectangle.Y;

                    // Get control location
                    Point point = this.Location;

                    // Calculate top
                    return point.Y + height;
                }

                else
                {
                    // If tab control tabs count is 0 then return default tab control top
                    return this.Top + 2;
                }
            }
            set
            {
                // Check alignment and check if there are some tab pages
                if (this.Alignment == TabAlignment.Top && this.TabCount > 0)
                {
                    // Create tab page object
                    TabPage tabPage = this.TabPages[0];

                    // Create rectangle object
                    Rectangle rectangle = tabPage.Bounds;

                    // Get button height
                    int height = rectangle.Y;

                    //calculate client top
                    this.Top = value - height;
                }
                else
                {
                    // If TabControl alignment is not top or tab pages count is 0 then client.top is equal tab control top - 2 because of bounds
                    this.Top = value - 2;
                }
            }
        }

        /// <summary>
        /// Gets/sets the top coordinate of the internal area of the control
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        internal int ClientLeftPixels
        {
            get
            {
                // Check number of tabs
                if (this.TabCount > 0)
                {
                    // Check alignment
                    if (this.Alignment == TabAlignment.Left)
                    {
                        // Create tab page object
                        TabPage tabPage = this.TabPages[0];

                        // Create rectangle object
                        Rectangle rectangle = tabPage.Bounds;

                        // Get button height
                        int width = rectangle.X;

                        // Get control location
                        Point point = this.Location;

                        // Calculate client left
                        return width + point.X;
                    }
                    else
                    {
                        // If alignment is top,right or bottom return default tab control left
                        return this.Left + 2;
                    }
                }

                return -1;
            }
            set
            {
                // Check if there are some tab pages and check aligment 
                if (this.TabCount > 0 && this.Alignment == TabAlignment.Left)
                {
                    // Create tab page object
                    TabPage tabPage = this.TabPages[0];

                    // Create rectangle object
                    Rectangle rectangle = tabPage.Bounds;

                    // Get button height
                    int width = rectangle.X;

                    // Set client left
                    this.Left = value - width;
                }
                else
                {
                    // If alignment is not left or there aren't tab pages then client left is equal to tab control left -2 because of bounds
                    this.Left = value - 2;
                }
            }
        }

        /// <summary>
        /// Gets/sets the height of the internal area of the control
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        internal int ClientHeightPixels
        {
            get
            {
                if (this.TabCount > 0)
                {
                    // Create tab page object
                    TabPage tabPage = this.TabPages[0];

                    // Create rectangle object
                    Rectangle rectangle = tabPage.Bounds;

                    // Get button height
                    return rectangle.Height + 4;
                }
                else
                {
                    return this.Height;
                }
            }
            set
            {
                if (this.TabCount > 0)
                {
                    // Create tab page object
                    TabPage tabPage = this.TabPages[0];

                    // Check aligment and tab count 
                    if (this.TabCount > 0 && this.Alignment == TabAlignment.Top)
                    {
                        // Create rectangle object
                        Rectangle rectangle = tabPage.Bounds;

                        // Get button height
                        this.Height = value + rectangle.Y;
                    }
                    else if (this.TabCount > 0 && this.Alignment == TabAlignment.Bottom)
                    {
                        // Get tab control height
                        int tabControlHeight = this.Height;

                        // Get tab page height
                        int tabPageHeight = tabPage.Height;

                        // Get button height
                        int tabButtonHeight = tabControlHeight - tabPageHeight - 4;

                        // Set tab control height
                        this.Height = value + tabButtonHeight;
                    }
                    else
                    {
                        // If alignment is not top and tab count is 0 then client height is equal to tab control height 
                        this.Height = value + 4;
                    }
                }
            }
        }

        /// <summary>
        /// Gets/sets the height of the internal area of the control
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        internal int ClientWidthPixels
        {
            get
            {
                if (this.TabCount > 0)
                {
                    // Create tab page object
                    TabPage tabPage = this.TabPages[0];

                    // Create rectangle object
                    Rectangle rectangle = tabPage.Bounds;

                    if (this.Alignment == TabAlignment.Right || this.Alignment == TabAlignment.Left)
                    {
                        // Get button height
                        return rectangle.Width;
                    }
                    else
                    {
                        return rectangle.Width + 4;
                    }
                }
                else
                {
                    return this.Height;
                }
            }
            set
            {
                // Check tab count and alignment
                if (this.TabCount > 0 && (this.Alignment == TabAlignment.Left || this.Alignment == TabAlignment.Right))
                {
                    // Create tab page object
                    TabPage tabPage = this.TabPages[0];

                    // Get tab page width
                    int tabPageWidth = tabPage.Width;

                    // Get tab control width
                    int tabControlWidth = this.Width;

                    // Calculate button width
                    int tabButtonsWidth = tabControlWidth - tabPageWidth;

                    // Set tab control width
                    this.Width = value + tabButtonsWidth;
                }
                else
                {
                    // If alignment is top or bottom or there aren't tab pages client width is equal to tab control width + 4 because of bounds
                    this.Width = value + 4;
                }
            }
        }

        internal TabPageCollection BaseTabPages
        {
            get
            {
                return base.TabPages;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
        /// Sets the color of the TabControl TabPages.
        /// </summary>
        /// <param name="color">Color to set to the TabPages of the TabControl</param>
        public void SetForeColor(Color color)
        {
            // Get the collection of TabPages from the TabControl
            FCTabPageCollection tabPageCollection = this.TabPages;

            // If the TabCollection is not NULL
            if (tabPageCollection != null)
            {
                // Loop through all the TabPages
                foreach (TabPage tabPage in tabPageCollection)
                {
                    // Set the fore color to the TabPage
                    tabPage.ForeColor = color;
                }
            }
        }

        /// <summary>
        /// Sets the TabEnabled property.
        /// </summary>
        /// <param name="index">The Index.</param>
        /// <param name="isTabEnabled">if set to <c>true</c>the tab will be enabled.</param>
        public void SetTabEnabled(int index, bool isTabEnabled)
        {
            // If TabPage should be enabled
            if (isTabEnabled)
            {
                // If tab is set to "Enabled", then it should be removed from the extender
                if (tabPageIsEnabled.ContainsKey(index))
                {
                    tabPageIsEnabled.Remove(index);
                }

                // If no values exist in disabled tab pages Extender
                if (tabPageIsEnabled.Count == 0)
                {
                    // Sign out from the "Selecting" event
                    this.Selecting -= objTabControl_Selecting;
                }
            }
            else
            {
                // Set the value in to the cache
                if (tabPageIsEnabled.ContainsKey(index))
                {
                    tabPageIsEnabled[index] = isTabEnabled;
                }
                else
                {
                    tabPageIsEnabled.Add(index, isTabEnabled);
                }

                // Sign up for the "Selecting" event
                this.Selecting += objTabControl_Selecting;
            }
        }

        /// <summary>
        /// Sets tab key
        /// </summary>
        /// <param name="tabIndex"></param>
        /// <param name="key"></param>
        public void SetTabKey(int tabIndex, string key)
        {
            // Check for valid index
            if (tabIndex >= 0 && tabIndex < this.TabCount)
            {
                // Create tab object with that index
                TabPage tabPage = this.TabPages[tabIndex];

                // Set key to tab with that index
                tabPage.Name = key;
            }
            else
            {
                // If index is out of range throw exception
                throw new IndexOutOfRangeException();
            }
        }

        /// <summary>
        /// Gets tab page key
        /// </summary>
        /// <param name="tabIndex">tab page index</param>
        /// <returns>tab page key</returns>
        public string GetTabKey(int tabIndex)
        {
            // Check for valid index
            if (tabIndex >= 0 && tabIndex < this.TabCount)
            {
                // Create tab page object
                TabPage tabPage = this.TabPages[tabIndex];

                // Return tab page key
                return tabPage.Name;
            }
            else
            {
                // If index is not valid throw exception
                throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Sets Visible property.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="state"></param>
        public void TabVisible(int index, bool state)
        {
            //BAN - TabPage has a Visible property but it is set to non browsable
            this.TabPages[index].Visible = state;
        }

        /// <summary>
        /// Gets Visible Property.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool TabVisible(int index)
        {
            //BAN - TabPage has a Visible property but it is set to non browsable
            return this.TabPages[index].Visible;
        }

        /// <summary>
        /// //BAN - dynamically create tab pages
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public int CreatePages(int count)
        {
            try
            {
                for (int counter = 0; counter < count; counter++)
                {
                    this.TabPages.Add(new FCTabPage());
                }

                return this.TabPages.Count;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Sets the tab visible.
        /// </summary>
        /// <param name="isVisible">if set to <c>true</c> visible tab.</param>
        public void SetTabVisible(int index, bool isVisible)
        {

        }
        /// <summary>
        /// Gets the tab visibility.
        /// </summary>
        /// <param name="index">Index of the tab page.</param>
        /// <returns></returns>
        public bool GetTabVisible(int index)
        {
            // Initialize a value for tab visibility
            bool isTabVisible = false;

            TabPage tabPage;

            // Try get the TabPage from the extender cache
            if (tabPageIsVisible.ContainsKey(index))
            {
                isTabVisible = tabPageIsVisible[index];
            }

            return isTabVisible;
        }

        /// <summary>
        /// Sets the tabs per row.
        /// </summary>
        /// <param name="tabsPerRow">The tabs per row.</param>
        public void SetTabsPerRow(int tabsPerRow)
        {
            // If has more tab pages than single row can contain
            if (tabsPerRow < this.TabCount)
            {
                // Set multiline properties
                // TODO
                //this.Multiline = true;
                //this.SizeMode = TabSizeMode.Fixed;
                //this.ItemSize = new Size(this.DisplayRectangle.Width / tabsPerRow, this.ItemSize.Height);
            }
            else
            {
                // Set single-line properties
                //this.Multiline = false;
            }

        }

        #endregion

        #region Internal Methods

        internal void Insert(int index, FCTabPage tabPage)
        {
            if (index < this.tabPages.Count)
                Array.Copy(this.tabPages.ToArray(), index, this.tabPages.ToArray(), index + 1, this.tabPages.Count - index);
            this.tabPages[index] = tabPage;

            //if (this.Appearance != TabAppearance.FlatButtons)
            //    return;
            this.Invalidate();
        }

        #endregion

        #region Protected Methods

        protected override void OnControlAdded(ControlEventArgs e)
        {
            base.OnControlAdded(e);
            //CHE: populate also internal collection tabPages if needed
            FCTabPage tabPage = e.Control as FCTabPage;
            if (!this.tabPages.Contains(tabPage))
            {
                this.tabPages.Add(tabPage);
            }
        }

        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            if (this.DisableEvents)
            {
                return;
            }
            base.OnSelectedIndexChanged(e);
        }

        /// <summary>
        /// Processes the & mnemonic keys
        /// </summary>
        /// <param name="charCode"></param>
        /// <returns></returns>
        protected override bool ProcessMnemonic(char charCode)
        {
            if (!this.Visible)
                return false;

            for (int tab = 0; tab < this.TabPages.Count; tab++)
            {
                FCTabPage p = (FCTabPage)this.TabPages[tab];
                if (!p.Enabled)
                    continue;

                if (IsMnemonic(charCode, p.Label))
                {
                    if (this.SelectedIndex != tab)
                    {
                            if (Focus() && this.Focused)
                            this.SelectedIndex = tab;

                        return true;
                    }
                }
            }

            return false;
        }

        #endregion

        #region Private Methods

        //CHE: disable owner drawing and default values, Tabs, TabHeight and TabsPerRow are not used in DeutscheBahn project
        //private void SetMultiline()
        //{
        //    // If has more tab pages than single row can contain
        //    if (tabsPerRow < this.TabCount)
        //    {
        //        // Set multiline properties
        //        this.Multiline = true;
        //        this.SizeMode = TabSizeMode.Fixed;
        //        this.ItemSize = new Size(this.DisplayRectangle.Width / tabsPerRow, this.ItemSize.Height);
        //    }
        //    else
        //    {
        //        // Set single-line properties
        //        this.Multiline = false;
        //    }
        //}

        //CHE: disable owner drawing and default values, Tabs, TabHeight and TabsPerRow are not used in DeutscheBahn project
        //private void FCTabControl_DrawItem(object sender, DrawItemEventArgs e)
        //{
        //    Graphics g = e.Graphics;
        //    Brush textBrush;
        //    Brush backgroundBrush;
        //    Font tabFont;

        //    TabPage tabPage = this.TabPages[e.Index];
        //    Rectangle tabBounds = this.GetTabRect(e.Index);

        //    if (e.State == DrawItemState.Selected)
        //    {
        //        backgroundBrush = new SolidBrush(this.BackColor);
        //        g.FillRectangle(backgroundBrush, e.Bounds);

        //        textBrush = new SolidBrush(this.ForeColor);
        //        tabFont = new Font(e.Font.FontFamily, e.Font.Size, FontStyle.Bold, e.Font.Unit);
        //    }
        //    else
        //    {
        //        textBrush = new System.Drawing.SolidBrush(e.ForeColor);
        //        tabFont = e.Font;
        //    }

        //    StringFormat stringFlags = new StringFormat();
        //    stringFlags.Alignment = StringAlignment.Center;
        //    stringFlags.LineAlignment = StringAlignment.Center;
        //    if (this.TabPages[e.Index] != null)
        //    {
        //        g.DrawString(this.TabPages[e.Index].Text, tabFont, textBrush, tabBounds, new StringFormat(stringFlags));
        //    }
        //}

        /// <summary>
        /// Handles the Selecting event of the objTabControl control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="TabControlCancelEventArgs"/> instance containing the event data.</param>
        private void objTabControl_Selecting(object sender, TabControlCancelEventArgs e)
        {
            // Cast sender as Component
            System.ComponentModel.Component component = sender as System.ComponentModel.Component;

            // If Component is NULL
            if (component == null)
            {
                return;
            }

            // Check if such index exists in the Component Indexer
            bool valueExistInExtender = tabPageIsEnabled.ContainsKey(e.TabPageIndex);

            // if value already exists in extender
            if (valueExistInExtender)
            {
                // Check if the TabPage should be Enabled
                bool isTabEnabled = tabPageIsEnabled[e.TabPageIndex];

                // If tab should be disabled
                if (!isTabEnabled)
                {
                    // Don't activate the clicked TabPage
                    e.Cancel = true;
                }
            }
        }

        protected override void OnSelecting(TabControlCancelEventArgs e)
        {
            //CHE: cannot hide a TabPage, do not allow selection if Visible flag is false
            if (!((FCTabPage)e.TabPage).Visible || !e.TabPage.Enabled)
            {
                e.Cancel = true;
                return;
            }
            base.OnSelecting(e);
        }

        /// <summary>
        /// Raises the Deselected event.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDeselected(TabControlEventArgs e)
        {
            base.OnDeselected(e);
            PreviousTab = e.TabPageIndex;
        }

        protected override void OnParentChanged(EventArgs e)
        {
            base.OnParentChanged(e);
            if (base.Parent != null)
            {
                //CHE: if ImageList associated with TabControl is populated (set key for images) after setting buttons ImageKey, 
                // the images will be visible in designer but they will not be visible at runtime - reassign property to force relink
                if (this.ImageList != null)
                {
                    foreach (FCTabPage tabPage in this.TabPages)
                    {
                        string oldImageKey = tabPage.ImageKey;
                        if (!string.IsNullOrWhiteSpace(oldImageKey))
                        {
                            tabPage.ImageKey = null;
                            tabPage.ImageKey = oldImageKey;
                        }
                    }
                }
            }
        }

        #endregion
    }
}
