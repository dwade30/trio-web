﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation
{
    public class FCTabPageCollection : List<FCTabPage>
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private int lastAccessedIndex = -1;
        private FCTabControl owner;

        #endregion

        #region Constructors

        public FCTabPageCollection(FCTabControl owner)
        {
            this.owner = owner;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public FCTabPage this[string key]
        {
            get
            {
                if (string.IsNullOrEmpty(key))
                {
                    return (FCTabPage)null;
                }
                int index = this.IndexOfKey(key);
                if (this.IsValidIndex(index))
                {
                    return this[index];
                }
                else
                {
                    return (FCTabPage)null;
                }
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// Removes the tab by index.
        /// </summary>
        /// <param name="index">Index of the int.</param>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        /// <exception cref="System.NullReferenceException"></exception>
        public void RemoveTab(int index)
        {
            // Check if index is valid 
            if (this.Count > index && index >= 0)
            {
                // Remove the tab with given index
                this.Remove(this[index]);
            }
            else
            {
                // If index is not valid throw exception
                throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Removes the tab.
        /// </summary>
        /// <param name="key">The STR key.</param>
        /// <exception cref="System.ArgumentException"></exception>
        /// <exception cref="System.NullReferenceException"></exception>
        public void RemoveTab(string key)
        {
            // Check for empty string
            if (key != string.Empty)
            {
                // Remove tab with that key
                this.RemoveByKey(key);
            }
        }

        /// <summary>
        /// Adds the tab.
        /// </summary>
        /// <param name="index">The index of the tab.</param>
        /// <param name="key">The key string.</param>
        /// <param name="caption">The caption string.</param>
        /// <param name="image">The image index number.</param>
        public void AddTab(int index = -1, string key = "", string caption = "", int image = -1)
        {
            // If there is an index, we will use the Insert method
            if (index > 0)
            {
                // If there is a key
                if (string.IsNullOrEmpty(key))
                {
                    // If there isn't an image index number
                    if (image == -1)
                    {
                        // Using the Insert(int index, string text) overload
                        this.Insert(index, caption);
                    }
                    // If there is an image index number
                    else
                    {
                        // Using the Insert(int index, string key, string text, int imageIndex) overload
                        this.Insert(index, string.Empty, caption, image);
                    }
                }
                // If we have a key
                else
                {
                    // If there isn't an image index number
                    if (image == -1)
                    {
                        // Using the Insert(int index, string key, string text) overload
                        this.Insert(index, key, caption);
                    }
                    // If there is an image index number
                    else
                    {
                        // Using the Insert(int index, string key, string text, int imageIndex) overload
                        this.Insert(index, key, caption, image);
                    }
                }
            }
            // If there isn't an index, we will use the Add method
            else
            {
                // If there isn't have a key
                if (string.IsNullOrEmpty(key))
                {
                    // If there isn't an image index number
                    if (image == -1)
                    {
                        // Using the Add(string text) overload
                        this.Add(caption);
                    }
                    // If there is an image index number
                    else
                    {
                        // Using the Add(string key, string text, int imageIndex) overload
                        this.Add(string.Empty, caption, image);
                    }
                }
                // If there is a key
                else
                {
                    // If there isn't have an image index number
                    if (image == -1)
                    {
                        // Using the Add(string key, string text) overload
                        this.Add(key, caption);
                    }
                    // If there is an image index number
                    else
                    {
                        // Using the Add(string key, string text, string imageIndex overload
                        this.Add(key, caption, image);
                    }
                }
            }
        }

        /// <summary>
        /// Adds the tab.
        /// </summary>
        /// <param name="objTabPage">The tab page object.</param>
        public void AddTab(FCTabPage objTabPage)
        {
            // Add the tab page object to the tab page collection
            this.Add(objTabPage);
        }

        /// <summary>
        /// Removes the tab page with the specified key from the collection
        /// </summary>
        /// <param name="key"></param>
        public void RemoveByKey(string key)
        {
            int index = this.IndexOfKey(key);
            if (!this.IsValidIndex(index))
            {
                return;
            }
            this.RemoveAt(index);
        }

        /// <summary>
        /// Returns the index of the first occurrence of the FCTabPage with the specified key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual int IndexOfKey(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return -1;
            }
            if (this.IsValidIndex(this.lastAccessedIndex) && this[this.lastAccessedIndex].Key == key)
            {
                return this.lastAccessedIndex;
            }
            for (int index = 0; index < this.Count; ++index)
            {
                if (this[index].Key == key)
                {
                    this.lastAccessedIndex = index;
                    return index;
                }
            }
            this.lastAccessedIndex = -1;
            return -1;
        }

        /// <summary>
        /// add
        /// </summary>
        /// <param name="key"></param>
        /// <param name="caption"></param>
        /// <param name="image"></param>
        public void Add(string key = "", string caption = "", int image = -1)
        {
            FCTabPage tabPage = new FCTabPage();
            if (!string.IsNullOrEmpty(key))
            {
                tabPage.Key = key;
            }
            if (!string.IsNullOrEmpty(caption))
            {
                tabPage.Text = caption;
            }
            if (image >= 0)
            {
                tabPage.ImageIndex = image;
            }
            this.Add(tabPage);
        }

        /// <summary>
        /// add
        /// </summary>
        /// <param name="tabPage"></param>
        public new void Add(FCTabPage tabPage)
        {
            base.Add(tabPage);
            //CHE: cannot hide a tabpage, add in base.TabPages collection only if Visible
            tabPage.ParentTabControl = this.owner;
            if (tabPage.Visible)
            {
                if (!this.owner.Controls.Contains(tabPage))
                {
                    this.owner.Controls.Add(tabPage);
                }
            }
        }

        /// <summary>
        /// insert
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        public new void Insert(int index, FCTabPage item)
        {
            base.Insert(index, item);
            //CHE: cannot hide a tabpage, add in base.TabPages collection only if Visible
            item.ParentTabControl = this.owner;
            if (item.Visible)
            {
                if (!this.owner.Controls.Contains(item))
                {
                    this.owner.Controls.Add(item);
                    this.owner.Controls.SetChildIndex(item, index);
                }
            }
        }

        /// <summary>
        /// insert
        /// </summary>
        /// <param name="index"></param>
        /// <param name="caption"></param>
        public void Insert(int index, string caption)
        {
            FCTabPage tabPage = new FCTabPage();
            tabPage.Text = caption;
            base.Insert(index, tabPage);
            //CHE: cannot hide a tabpage, add in base.TabPages collection only if Visible
            tabPage.ParentTabControl = this.owner;
            if (tabPage.Visible)
            {
                if (!this.owner.Controls.Contains(tabPage))
                {
                    this.owner.Controls.Add(tabPage);
                    this.owner.Controls.SetChildIndex(tabPage, index);
                }
            }
        }

        /// <summary>
        /// insert
        /// </summary>
        /// <param name="index"></param>
        /// <param name="key"></param>
        /// <param name="caption"></param>
        /// <param name="image"></param>
        public void Insert(int index, string key, string caption, int image = -1)
        {
            FCTabPage tabPage = new FCTabPage();
            tabPage.Key = key;
            tabPage.Text = caption;
            if (image >= 0)
            {
                tabPage.ImageIndex = image;
            }
            this.Insert(index, tabPage);
        }

        /// <summary>
        /// clear
        /// </summary>
        public new void Clear()
        {
            base.Clear();
            FCTabControl tabControl = this.owner as FCTabControl;
            bool prevDisableEvents = false;
            if (tabControl != null)
            {
                prevDisableEvents = tabControl.DisableEvents;
                tabControl.DisableEvents = true;
            }
            this.owner.TabPages.Clear();
            if (tabControl != null)
            {
                tabControl.DisableEvents = prevDisableEvents;
            }
        }

        /// <summary>
        /// remove
        /// </summary>
        /// <param name="item"></param>
        public new void Remove(FCTabPage item)
        {
            base.Remove(item);
            this.owner.TabPages.Remove(item);
        }

        /// <summary>
        /// remove at
        /// </summary>
        /// <param name="index"></param>
        public new void RemoveAt(int index)
        {
            base.RemoveAt(index);
            this.owner.TabPages.RemoveAt(index);
        }

        /// <summary>
        /// remove range
        /// </summary>
        /// <param name="index"></param>
        /// <param name="count"></param>
        public new void RemoveRange(int index, int count)
        {
            base.RemoveRange(index, count);
            for (int i = 1; i <= count; i++)
            {
                this.owner.TabPages.RemoveAt(index);
            }
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        /// <summary>
        /// Validates the index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private bool IsValidIndex(int index)
        {
            if (index >= 0)
            {
                return index < this.Count;
            }

            return false;
        }

        #endregion
    }
}
