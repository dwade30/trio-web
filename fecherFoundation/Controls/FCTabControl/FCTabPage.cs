﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation
{
    public class FCTabPage : TabPage
    {
        #region Public Members
        #endregion

        #region Internal Members

        internal FCTabControl ParentTabControl = null;

        #endregion

        #region Private Members

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private string text;
        private string key;
        private string label;

        private bool visible = true;

        private Color tabForeColor;

        #endregion

        #region Constructors

        public FCTabPage()
            : base()
        {
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }

            //FC:ZSA: set default value
            this.Key = "";
            this.tabForeColor = this.TabForeColor;
        }

        public FCTabPage(string text)
            : base(text)
        {
            //FC:ZSA: set default value
            this.Key = "";
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        //CHE: cannot hide a TabPage, do not allow selection if Visible flag is false
        public new bool Visible
        {
            get
            {
                return visible;
            }

            set
            {
                visible = value;

                //CHE: cannot hide a tabpage, remove/add from base.TabPages collection
                if (ParentTabControl != null)
                {
                    FCTabPageCollection tabCollection = ParentTabControl.TabPages;

                    if (visible && !ParentTabControl.BaseTabPages.Contains(this))
                    {
                        int index = ParentTabControl.TabPages.IndexOf(this);
                        //CHE: fix exception e.g. when only one tab is visible and you set Visible true for forth tab in original collection
                        if (index > ParentTabControl.BaseTabPages.Count)
                        {
                            index = ParentTabControl.BaseTabPages.Count;
                        }
                        ParentTabControl.BaseTabPages.Insert(index, this);
                    }
                    else if (!visible)
                    {
                        ParentTabControl.BaseTabPages.Remove(this);
                    }
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        public new string Text
        {
            get
            {
                return text;
            }
            set
            {
                //CHE: selected tab is made bold in FCTabControl, add spaces to have enough space on tab label
                base.Text = String.Format("  {0}  ", RemoveMnemonic(value));
                text = RemoveMnemonic(value);
                label = value;
            }
        }

        /// <summary>
        /// Returns the orignal label text with the mnemonics
        /// </summary>
        public string Label
        {
            get { return label; }
        }

        /// <summary>
        /// Gets/sets the key for the specified tab page object
        /// </summary>
        /// 
        [DefaultValue("")]
        public string Key
        {
            get
            {
                return this.key;
            }
            set
            {
                this.key = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool Selected
        {
            get
            {
                // Cast to tab control
                TabControl objTabControl = this.Parent as TabControl;

                // Check for null reference
                if (objTabControl != null)
                {
                    // If objTabPage is selected tab return true otherwise return false 
                    if (objTabControl.SelectedTab.Equals(this))
                    {
                        return true;
                    }
                    return false;
                }
                return false;
            }
            set
            {
                // Cast to tab control
                TabControl objTabControl = this.Parent as TabControl;

                // Check for null reference
                if (objTabControl != null)
                {
                    // Set objTabPage to be the selected tab in the tab control
                    objTabControl.SelectedTab = this;
                }
                else
                {
                    // If object is null throw exception
                    throw new NullReferenceException();
                }
            }
        }

        /// <summary>
        /// Gets the tab index
        /// </summary>
        public int Index
        {
            get
            {
                // Get the TabControl
                TabControl tabControl = this.Parent as TabControl;

                // Check for null reference
                if (tabControl != null)
                {
                    // Get collection of tab pages
                    TabControl.TabPageCollection tabCollection = tabControl.TabPages;

                    // Return tab page index 
                    return tabCollection.IndexOf(this);
                }
                if (!DesignMode)
                {
                    // If object is null throw exception
                    throw new NullReferenceException();
                }
                else
                {
                    return -1;
                }
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        protected override void OnEnabledChanged(EventArgs e)
        {
            base.OnEnabledChanged(e);

            if (this.Enabled)
            {
                this.TabForeColor = this.tabForeColor;
            }
            else
            {
                this.tabForeColor = this.TabForeColor;
                this.TabForeColor = Color.LightGray;
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Removes the mnemonic symbol (&) from the text of the tab page.
        /// </summary>
        /// <param name="label"></param>
        /// <returns></returns>
        private string RemoveMnemonic(string label)
        {
            int pos = label.IndexOf('&');
            if (pos > -1)
                label = label.Remove(pos, 1);

            return label;
        }
        #endregion
    }
}
