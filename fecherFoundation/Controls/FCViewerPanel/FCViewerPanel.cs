﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Wisej.Web;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace fecherFoundation
{
    public class FCViewerPanel : Panel
    {
        private PdfViewer pdfViewer = new PdfViewer();
        private PictureBox pictureBox = new PictureBox();

        /// <summary>
        /// Name of the file to display.
        /// File could be PDF or Image.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public string FileName { get; internal set; }

        /// <summary>
        /// Set FileName first!
        /// File could be PDF or Image.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public byte[] ItemData { get; internal set; }

        private string MediaType = string.Empty;

        public FCViewerPanel()
        {
            this.DragEnter += FCViewerPanel_DragEnter;
            this.DragDrop += FCViewerPanel_DragDrop;

        }

        private void FCViewerPanel_DragDrop(object sender, DragEventArgs e)
        {
            //make it critical
        }

        private void FCViewerPanel_DragEnter(object sender, DragEventArgs e)
        {
            //make it critical
        }

        protected override void OnDragEnter(DragEventArgs e)
        {
            base.OnDragEnter(e);
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.AllowedFileTypes = "pdf|image.*";

                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (files != null)
                {
                    foreach (string type in files)
                    {
                        if (type.IndexOf("/png") > -1
                            || type.IndexOf("/jpg") > -1
                            || type.IndexOf("/jpeg") > -1
                            || type.IndexOf("/gif") > -1
                            || type.IndexOf("pdf") > -1)
                        {
                            e.Effect = DragDropEffects.Copy;
                            return;
                        }
                    }
                }
            }

            e.Effect = DragDropEffects.None;
        }

        protected override void OnDragDrop(DragEventArgs e)
        {
            LoadFile((HttpFileCollection)e.Data.GetData(DataFormats.Files));

            base.OnDragDrop(e);
        }


        /// <summary>
        /// Load and display a file, which was uploaded from the client
        /// </summary>
        /// <param name="files"></param>
        public void LoadFile(HttpFileCollection files)
        {
            if (files == null || files.Count == 0)
                return;

            // only allow pdf and images
            if (files[0].ContentType == "application/pdf" || files[0].ContentType.StartsWith("image/"))
            {
                FileName = files.Get(0).FileName;
                using (var memoryStream = new MemoryStream())
                {
                    files.Get(0).InputStream.CopyTo(memoryStream);
                    ItemData = memoryStream.ToArray();
                }

                LoadFileIntoControl();
            }
            else
            {
                Controls.Clear();
                Label errorPanel = new Label();
                errorPanel.Dock = DockStyle.Top;
                errorPanel.Text = "Invalid file. Only PDF and images are allowed.";
                errorPanel.Name = "errorPanel";
                Controls.Add(errorPanel);
            }
        }

        /// <summary>
        /// Loads a PDF or Image into the FCViewerPanel.
        /// If itemData is omitted, then fileName is handeled as a LinkedDocument,
        /// which will be loaded with File.ReadAllBytes from the file system
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="itemData"></param>
        public void LoadFileFromApplication(string fileName, byte[] itemData = null, string mediaType = "")
        {
            // we have a LinkedDocument
            if (itemData == null && File.Exists(fileName))
            {
                try
                {
                    itemData = File.ReadAllBytes(fileName);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                    return;
                }
            }
            if (!string.IsNullOrEmpty(mediaType))
            {
                MediaType = mediaType;
            }
            FileName = fileName;
            ItemData = itemData;
            LoadFileIntoControl();
        }

        private void LoadFileIntoControl()
        {
            if (string.IsNullOrEmpty(FileName) || ItemData == null)
            {
				//FC:FINAL:MSH - i.issue #1746: exception in original won't be threw if file doesn't exist (just nothing will be added to ImageViewer)
				//throw new InvalidOperationException("FileName and ItemData has to be set");
				return;
            }

            Controls.Clear();
            using (var memoryStream = new MemoryStream(ItemData))
            {
                if (FileName.ToLower().EndsWith("pdf") || MediaType.ToLower().EndsWith("pdf"))
                {
                    //PdfViewer pdfViewer = new PdfViewer();
                    pdfViewer.Dock = DockStyle.Fill;
                    pdfViewer.Name = "pdfViewer1";
                    pdfViewer.PdfStream = new MemoryStream();
                    memoryStream.CopyTo(pdfViewer.PdfStream);
                    Controls.Add(pdfViewer);
                }
                else
                {
                    //PictureBox pictureBox = new PictureBox();
                    pictureBox.Dock = DockStyle.Fill;
                    pictureBox.Name = "pictureBox1";
                    pictureBox.SizeMode = PictureBoxSizeMode.Zoom;
					//FC:FINAL:DSE:#1913 IN VB6 if wrong file type is set to a picture box, no exception is thrown, only an empty picture box is displayed
					try
					{
						using (Image image = Image.FromStream(memoryStream))
						{
							pictureBox.Image = new Bitmap(image);
						}
					}
					catch { }
                    Controls.Add(pictureBox);
                }
            }

            FCUtils.ApplicationUpdate(this);
        }
    }
}
