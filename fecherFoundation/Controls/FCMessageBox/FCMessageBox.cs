using System;
using Wisej.Web;

namespace fecherFoundation
{
    public static class FCMessageBox
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        #endregion

        #region Constructors
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties
        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// show
        /// </summary>
        /// <param name="prompt"></param>
        /// <returns></returns>
        public static DialogResult Show(string prompt)
        {
            return MessageBox.Show(prompt);
        }

        /// <summary>
        /// show
        /// </summary>
        /// <param name="prompt"></param>
        /// <param name="msgBoxStyle"></param>
        /// <returns></returns>
        public static DialogResult Show(string prompt, MsgBoxStyle msgBoxStyle)
        {
            return Show(prompt, msgBoxStyle, string.Empty, string.Empty, -1);
        }

        /// <summary>
        /// show
        /// </summary>
        /// <param name="prompt"></param>
        /// <param name="msgBoxStyle"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public static DialogResult Show(string prompt, MsgBoxStyle msgBoxStyle, string title)
        {
            return Show(prompt, msgBoxStyle, title, string.Empty, -1);
        }

        /// <summary>
        /// show
        /// </summary>
        /// <param name="prompt"></param>
        /// <param name="msgBoxStyle"></param>
        /// <param name="title"></param>
        /// <param name="helpFile"></param>
        /// <returns></returns>
        public static DialogResult Show(string prompt, MsgBoxStyle msgBoxStyle, string title, string helpFile)
        {
            return Show(prompt, msgBoxStyle, title, helpFile, -1);
        }

        /// <summary>
        /// show
        /// </summary>
        /// <param name="prompt"></param>
        /// <param name="msgBoxStyle"></param>
        /// <param name="title"></param>
        /// <param name="helpFile"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static DialogResult Show(string prompt, MsgBoxStyle msgBoxStyle, string title, string helpFile, int context)
        {
            var activeForm = Form.ActiveForm;
            int result = -1;

            int intMessageBoxDefaultButton = MsgBoxStyleToMessageBoxDefaultButton(msgBoxStyle);

            int intMessageBoxOptions = MsgBoxStyleToMessageBoxOptions(msgBoxStyle);

            if (intMessageBoxDefaultButton > 0 && intMessageBoxOptions > 0)
            {
                result = Convert.ToInt32(MessageBox.Show(prompt, title, MsgBoxStyleToMessageBoxButtons(msgBoxStyle), MsgBoxStyleToMessageBoxIcon(msgBoxStyle), (MessageBoxDefaultButton)intMessageBoxDefaultButton));
            }
            else if (intMessageBoxDefaultButton > 0)
            {
                result = Convert.ToInt32(MessageBox.Show(prompt, title, MsgBoxStyleToMessageBoxButtons(msgBoxStyle), MsgBoxStyleToMessageBoxIcon(msgBoxStyle), (MessageBoxDefaultButton)intMessageBoxDefaultButton));
            }            

            if (result == -1)
            {
                result = Convert.ToInt32(MessageBox.Show(prompt, title, MsgBoxStyleToMessageBoxButtons(msgBoxStyle), MsgBoxStyleToMessageBoxIcon(msgBoxStyle)));
            }

            //FC:FINAL:SBE - Harris #162 - in some cases after message box is shown, the ActiveForm is lost
            if (activeForm != null && Form.ActiveForm != activeForm)
            {
                activeForm.Activate();
            }
            var dialogResult = (DialogResult)result;
            //FC:FINAL:SBE - Harris #i2128 - in Wisej when you close the MessageBox from Close Button ("X") Dialog result is Abort, and not Cancel as in VB6 and WinForms
            if (dialogResult == DialogResult.Abort && ((msgBoxStyle | MsgBoxStyle.AbortRetryIgnore) != MsgBoxStyle.AbortRetryIgnore))
            {
                dialogResult = DialogResult.Cancel;
            }
            return dialogResult;
        }

        /// <summary>
        /// MSGs the box style to message box buttons.
        /// </summary>
        /// <param name="msgBoxStyle">The MSG box style.</param>
        /// <returns></returns>
        public static MessageBoxButtons MsgBoxStyleToMessageBoxButtons(MsgBoxStyle msgBoxStyle)
        {
            if ((msgBoxStyle & MsgBoxStyle.YesNoCancel) == MsgBoxStyle.YesNoCancel)
            {
                return MessageBoxButtons.YesNoCancel;
            }
            else if ((msgBoxStyle & MsgBoxStyle.YesNo) == MsgBoxStyle.YesNo)
            {
                return MessageBoxButtons.YesNo;
            }
            else if ((msgBoxStyle & MsgBoxStyle.OkCancel) == MsgBoxStyle.OkCancel)
            {
                return MessageBoxButtons.OKCancel;
            }
            else if ((msgBoxStyle & MsgBoxStyle.AbortRetryIgnore) == MsgBoxStyle.AbortRetryIgnore)
            {
                return MessageBoxButtons.AbortRetryIgnore;
            }
            else if ((msgBoxStyle & MsgBoxStyle.RetryCancel) == MsgBoxStyle.RetryCancel)
            {
                return MessageBoxButtons.RetryCancel;
            }

            return MessageBoxButtons.OK;
        }

        /// <summary>
        /// MsgBoxStyle to MessageBoxIcon
        /// </summary>
        /// <param name="msgBoxStyle"></param>
        /// <returns></returns>
        public static MessageBoxIcon MsgBoxStyleToMessageBoxIcon(MsgBoxStyle msgBoxStyle)
        {

            if ((msgBoxStyle & MsgBoxStyle.Exclamation) == MsgBoxStyle.Exclamation)
            {
                return MessageBoxIcon.Warning;
            }
            else if ((msgBoxStyle & MsgBoxStyle.Information) == MsgBoxStyle.Information)
            {
                return MessageBoxIcon.Information;
            }
            else if ((msgBoxStyle & MsgBoxStyle.Question) == MsgBoxStyle.Question)
            {
                return MessageBoxIcon.Question;
            }
            else if ((msgBoxStyle & MsgBoxStyle.Critical) == MsgBoxStyle.Critical)
            {
                return MessageBoxIcon.Error;
            }
            return MessageBoxIcon.Question;
        }

        /// <summary>
        /// MsgBoxStyle to MessageBoxDefaultButton
        /// </summary>
        /// <param name="msgBoxStyle"></param>
        /// <returns></returns>
        public static int MsgBoxStyleToMessageBoxDefaultButton(MsgBoxStyle msgBoxStyle)
        {
            if ((msgBoxStyle & MsgBoxStyle.DefaultButton1) == MsgBoxStyle.DefaultButton1)
            {
                return Convert.ToInt32(MessageBoxDefaultButton.Button1);
            }
            else if ((msgBoxStyle & MsgBoxStyle.DefaultButton2) == MsgBoxStyle.DefaultButton2)
            {
                return Convert.ToInt32(MessageBoxDefaultButton.Button2);
            }
            else if ((msgBoxStyle & MsgBoxStyle.DefaultButton3) == MsgBoxStyle.DefaultButton3)
            {
                return Convert.ToInt32(MessageBoxDefaultButton.Button3);
            }

            return -1;
        }

        /// <summary>
        /// MsgBoxStyle to MessageBoxOptions
        /// </summary>
        /// <param name="msgBoxStyle"></param>
        /// <returns></returns>
        public static int MsgBoxStyleToMessageBoxOptions(MsgBoxStyle msgBoxStyle)
        {
            // TODO
            //if ((msgBoxStyle & MsgBoxStyle.MsgBoxRight) == MsgBoxStyle.MsgBoxRight)
            //{
            //    return Convert.ToInt32(MessageBoxOptions.RightAlign;
            //}
            //else if ((msgBoxStyle & MsgBoxStyle.MsgBoxRtlReading) == MsgBoxStyle.MsgBoxRtlReading)
            //{
            //    return Convert.ToInt32(MessageBoxOptions.RtlReading;
            //}

            return -1;
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }   
}
