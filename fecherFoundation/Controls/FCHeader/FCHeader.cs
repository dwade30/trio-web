﻿using System;
using System.Collections.Generic;
using Wisej.Web;

namespace fecherFoundation
{
    public class FCHeader : Label
    {
        public FCHeader()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FCHeader
            // 
            this.AppearanceKey = "Header";
            this.Font = new System.Drawing.Font("@header", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.Size = new System.Drawing.Size(100, 16);
            this.ResumeLayout(false);

        }
    }
}
