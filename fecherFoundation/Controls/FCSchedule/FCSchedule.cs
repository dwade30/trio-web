﻿using Gravitybox.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation
{
    public partial class FCSchedule : Gravitybox.Controls.Schedule
    {
        #region Events
        public delegate void AppointmentDoubleClickDelegate(object sender, AppointmentMouseEventArgs e);
        public event AppointmentDoubleClickDelegate AppointmentDoubleClick;
        #endregion

        #region Members
        private Color dayRoomForeColor = Color.Black;
        private Color monthHeaderBackColor = SystemColors.Control;
        private Color columnHeaderBackColor;
        private bool autoHilite = true;
        #endregion

        #region Properties
        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _ExtentX { get; set; }
        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _ExtentY { get; set; }

        [Obsolete("The property is not supported in current version")]
        public int SelTab { get; set; }

        public Color MonthOddBackColor
        {
            get
            {
                return base.Appearance.BackColor;
            }
            set
            {
                base.Appearance.BackColor = value;
            }
        }

        public Color MonthEvenBackColor
        {
            get
            {
                return base.Appearance.BackColor;
            }
            set
            {
                base.Appearance.BackColor = value;
            }
        }

        public int ColumnWidth
        {
            get
            {
                return base.ColumnHeader.Size;
            }
            set
            {
                base.ColumnHeader.Size = value;
            }
        }

        public int RowHeight
        {
            get
            {
                return base.RowHeader.Size;
            }
            set
            {
                base.RowHeader.Size = value;
            }
        }

        public bool AllowColumnResizing
        {
            get
            {
                return base.ColumnHeader.AllowResize;
            }
            set
            {
                base.ColumnHeader.AllowResize = value;
            }
        }

        public bool AllowRowResizing
        {
            get
            {
                return base.RowHeader.AllowResize;
            }
            set
            {
                base.RowHeader.AllowResize = value;
            }
        }

        public bool AutoColumnFit
        {
            get
            {
                return base.ColumnHeader.AutoFit;
            }
            set
            {
                base.ColumnHeader.AutoFit = value;
            }
        }

        public bool AutoRowFit
        {
            get
            {
                return base.RowHeader.AutoFit;
            }
            set
            {
                base.RowHeader.AutoFit = value;
            }
        }

        public int AppointmentBorderWidth
        {
            get
            {
                return base.DefaultAppointmentAppearance.BorderWidth;
            }
            set
            {
                base.DefaultAppointmentAppearance.BorderWidth = value;
            }
        }

        /// <summary>
        /// Gets/sets the Column header text color of the control
        /// </summary>
        [DefaultValue(KnownColor.Black)]
        public Color DayRoomForeColor
        {
            get
            {
                return this.dayRoomForeColor;
            }
            set
            {
                this.dayRoomForeColor = value;
                base.ColumnHeader.Appearance.ForeColor = this.dayRoomForeColor;
            }
        }
        /// <summary>
        /// Gets/sets the Column header backgroung color of the control when ViewMode is Month or MonthFull
        /// </summary>
        [DefaultValue(KnownColor.Control)]
        public Color MonthHeaderBackColor
        {
            get
            {
                return this.monthHeaderBackColor;
            }
            set
            {
                this.monthHeaderBackColor = value;
                if (base.ViewMode == ViewModeConstants.Month || base.ViewMode == ViewModeConstants.MonthFull)
                {
                    columnHeaderBackColor = base.ColumnHeader.Appearance.BackColor;
                    base.ColumnHeader.Appearance.BackColor = this.monthHeaderBackColor;
                }
            }
        }
        /// <summary>
        /// Determines the way a schedule is displayed. There are numerous options such As DateTimes on top and times on left. This property provides the schedule with much functionality in that it allows you to display the same data in various ways.
        /// </summary>
        public new ViewModeConstants ViewMode
        {
            get
            {
                return base.ViewMode;
            }
            set
            {
                base.ViewMode = value;
                if (base.ViewMode == ViewModeConstants.MonthFull || base.ViewMode == ViewModeConstants.Month)
                {
                    base.ColumnHeader.Appearance.BackColor = this.monthHeaderBackColor;
                }
                else
                {
                    base.ColumnHeader.Appearance.BackColor = this.columnHeaderBackColor;
                }
            }
        }
        [Obsolete("The property is not supported in current version")]
        public bool CategoryBar { get; set; }

        /// <summary>
        /// Display or not the Event header of the control
        /// </summary>
        public bool AllowEventHeader 
        {
            get
            {
                return base.EventHeader.AllowHeader;
            }
            set
            {
                base.EventHeader.AllowHeader = value;
            }
        }
        /// <summary>
        /// The AllowAdd property determines if the user can double-click on the background and create an appointment.
        /// </summary>
        public bool AllowDblClickAdd
        {
            get
            {
                return base.AllowAdd;
            }
            set
            {
                base.AllowAdd = value;
            }
        }
        /// <summary>
        /// The AutoHilite property determines if the appointment is highlighted when the mouse cursor is above it.
        /// </summary>
        [DefaultValue(true)]
        public bool AutoHilite
        {
            get
            {
                return this.autoHilite;
            }
            set
            {
                this.autoHilite = value;
            }
        }
        [Obsolete("The property is not supported in current version")]
        public bool AllowWeekMargin { get; set; }
        [Obsolete("The property is not supported in current version")]
        public bool AllowEventDrag { get; set; }
        [Obsolete("The property is not supported in current version")]
        public string DragFormatDate { get; set; }
        [Obsolete("The property is not supported in current version")]
        public string CategoryName { get; set; }

        #endregion
        #region Constructor
        public FCSchedule()
        {
            InitializeComponent();
        }
        #endregion

        #region Overrides
        protected override void OnAppointmentDoubleClick(Gravitybox.Objects.EventArgs.AppointmentMouseEventArgs e)
        {
            base.OnAppointmentDoubleClick(e);
            if (AppointmentDoubleClick != null)
            {
                AppointmentDoubleClick(this, new AppointmentMouseEventArgs(e.Appointment, new MouseEventArgs(e.Button, e.Clicks, e.X, e.Y, e.Delta)));
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            if (this.AutoHilite)
            {
                if (this.AppointmentCollection.Count > 0)
                {
                    Appointment appointment = this.Visibility.GetAppointmentFromCor(e.Location);
                    if (appointment != null)
                    {
                        this.SelectedItems.Add(appointment);
                        this.SelectedItem = appointment;
                    }
                }
            }
        }

        #endregion
    }

    #region EventArgs
    public class AppointmentMouseEventArgs : Gravitybox.Objects.EventArgs.AppointmentMouseEventArgs
    {
        public AppointmentMouseEventArgs(Gravitybox.Objects.Appointment appointment, MouseEventArgs e):base(appointment, e)
        {
        }
    }
    #endregion
}