﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation
{
    public class FCListViewItemCollection : ListView.ListViewItemCollection
    {
        #region Members
        private ListView ownerListView;
        #endregion

        #region Events
        public event OnItemAdd ItemAdd;
        public delegate void OnItemAdd(object sender, EventArgs e);
        #endregion

        #region Constructor

        //public FCListViewItemCollection(ListView owner)
        //{
        //    ownerListView = owner;
        //}
        #endregion

        #region Public methods
        
        /// <summary>
        /// remove
        /// </summary>
        /// <param name="itemName"></param>
        public void Remove(string itemName)
        {
            int index = -1;

            foreach (ListViewItem item in this)
            {
                if (item.Name == itemName)
                {
                    index = this.IndexOf(item);
                    break;
                }
            }

            if (index >= 0)
            {
                this.RemoveAt(index);
            }
        }

        /// <summary>
        /// add item
        /// </summary>
        /// <param name="itemKey"></param>
        /// <param name="itemText"></param>
        /// <returns></returns>
        public ListViewItem AddItem(string itemKey, string itemText)
        {
            ListViewItem li = new ListViewItem(itemText);
            li.Name = itemKey;

            return this.Add(li);
        }

        /// <summary>
        /// add
        /// </summary>
        /// <param name="index"></param>
        /// <param name="itemName"></param>
        /// <param name="itemText"></param>
        /// <returns></returns>
        public ListViewItem Add(int index, string itemName, string itemText)
        {
            ListViewItem li = new ListViewItem(itemText);
            li.Name = itemName;

            if (index <= this.Count)
            {
                this.Insert(index, li);
                return li;
            }
            else
            {
                return this.Add(li);
            }
        }

        #endregion

        #region Overrides

        #region Select first item overrides
        //START: The overrides are needed in order to select the first item in the list, if the list displays CheckBoxes
        public ListViewItem Add(ListViewItem value)
        {
            int index = base.Add(value);
            SelectFirstItem();
            NewIndex = index;
            return value;
        }

        public override ListViewItem Add(string key, string text, int imageIndex)
        {
            ListViewItem addedItem = base.Add(key, text, imageIndex);
            SelectFirstItem();
            NewIndex = addedItem.Index;
            return addedItem;
        }

        public override ListViewItem Add(string key, string text, string imageKey)
        {
            ListViewItem addedItem = base.Add(key, text, imageKey);
            SelectFirstItem();
            NewIndex = addedItem.Index;
            return addedItem;
        }

        public override ListViewItem Add(string text)
        {
            ListViewItem addedItem = base.Add(text);
            SelectFirstItem();
            NewIndex = addedItem.Index;
            return addedItem;
        }

        public ListViewItem Add(string text, int imageIndex)
        {
            ListViewItem addedItem = base.Add(text, text, imageIndex);
            SelectFirstItem();
            NewIndex = addedItem.Index;
            return addedItem;
        }

        public override ListViewItem Add(string text, string imageKey)
        {
            ListViewItem addedItem = base.Add(text, imageKey);
            SelectFirstItem();
            NewIndex = addedItem.Index;
            return addedItem;
        }

        public override ListViewItem Insert(int index, string key, string text, int imageIndex)
        {
            ListViewItem insertedItem = base.Insert(index, key, text, imageIndex);
            SelectFirstItem();
            NewIndex = insertedItem.Index;
            return insertedItem;
        }

        public override ListViewItem Insert(int index, string key, string text, string imageKey)
        {
            ListViewItem insertedItem = base.Insert(index, key, text, imageKey);
            SelectFirstItem();
            NewIndex = insertedItem.Index;
            return insertedItem;
        }
        //END: The overrides are needed in order to select the first item in the list, if the list displays CheckBoxes
        #endregion

        public override ListViewItem this[int index]
        {
            get
            {
                return base[index];
            }
        }

        public override ListViewItem this[string key]
        {
            get
            {
                return base[key];
            }
        }

        #endregion

        #region Internal Methods
        /// <summary>
        /// In VB6 the first item added will become selected if the list displays CheckBoxes
        /// </summary>
        internal void SelectFirstItem()
        {
            //CHE: select item when adding first item for simple listview too
            //if (ownerListView.CheckBoxes && ownerListView.Items.Count >= 1 && ownerListView.SelectedItems.Count == 0)
            if (ownerListView.Items.Count >= 1 && ownerListView.SelectedItems.Count == 0)
            {
                ownerListView.Items[0].Selected = true;
            }
        }
        #endregion

        #region public Properties 
            /// <summary>
            /// NewIndex Property implementation for vb6 Listbox compatibility
            /// </summary>
            public int NewIndex { get; set; }
        #endregion

    }
}
