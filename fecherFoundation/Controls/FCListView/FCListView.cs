﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    public partial class FCListView : ListView
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private AppearanceConstants appearance = AppearanceConstants.cc3D;
        private int sortKey = 0;
        private SortOrder sortOrder;
        private bool sorted = false;

        #endregion

        #region Constructors

        public FCListView()
        {
            //this.BackColor = SystemColors.Control;
            InitializeComponent();
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            //CHE: set default value
            this.Appearance = AppearanceConstants.cc3D;
            //CHE: set default value for MultiSelect as in VB6
            this.MultiSelect = false;
            this.SortKey = 0;
            this.Sorted = false;

            this.ColumnClick += FCListView_ColumnClick;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums

		public enum ListColumnAlignmentConstants
		{
			lvwColumnLeft = 0,
			lvwColumnRight = 1,
			lvwColumnCenter = 2
		}

		public enum ListLabelEditConstants
		{
			lvwAutomatic = 0,
			lvwManual = 1
		}


		public enum OLEDragConstants
        {
            ccOLEDragManual = 0,
            ccOLEDragAutomatic = 1
        }

        public enum AppearanceConstants
        {
            ccFlat = 0,
            cc3D = 1
        }   

        #endregion

        #region Properties

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        [Obsolete(".NET does not support the property OLEDragMode")]
        public OLEDragConstants OLEDragMode { get; set; }
        [Obsolete(".NET does not support the property OLEDropMode")]
        public OLEDragConstants OLEDropMode { get; set; }

        /// <summary>
        /// Gets/sets the Appearance of the control
        /// </summary>
        [DefaultValue(AppearanceConstants.cc3D)]
        public AppearanceConstants Appearance
        {
            get
            {
                return this.appearance;
            }
            set
            {
                this.appearance = value;
                //changing appearance in InitializeComponent should not change the BackColor to white for 2D
                Form f = this.FindForm();
                if (f != null && f.Created)
                {
                    if (this.appearance == AppearanceConstants.cc3D)
                    {
                        base.BorderStyle = Wisej.Web.BorderStyle.Solid;
                    }
                    else
                    {
                        base.BorderStyle = Wisej.Web.BorderStyle.Solid;
                    }
                }
            }
        }

        /// <summary>
        /// Gets/sets if the list is sorted
        /// </summary>
        [DefaultValue(false)]
        public bool Sorted
        {
            get
            {
                return sorted;
            }
            set
            {
                sorted = value;
                UpdateSorting();
            }
        }

        /// <summary>
        /// Gets/sets the sort order for items in the control
        /// </summary>
        public SortOrder SortOrder
        {
            get
            {
                return base.Sorting;
            }
            set
            {
                base.Sorting = value;
                UpdateSorting();
            }
        }

        /// <summary>
        /// Gets/sets the key for sorting
        /// </summary>
        [DefaultValue(0)]
        public int SortKey
        {
            get
            {
                return sortKey;
            }
            set
            {
                sortKey = value;
                UpdateSorting();
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
		/// <summary>
		/// SubItemsSetText
		/// </summary>
		/// <param name="itm"></param>
		/// <param name="SubItemIndex"></param>
		/// <param name="SubItemText"></param>
		public static void SubItemsSetText(ListViewItem itm, int SubItemIndex, string SubItemText)
		{
			int nAdd = SubItemIndex - itm.SubItems.Count;
			if (nAdd >= 0)
			{
				while (nAdd-- > 0) itm.SubItems.Add("");
				itm.SubItems.Add(SubItemText);
			}
			else
				itm.SubItems[SubItemIndex].Text = SubItemText;
		}

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
        /// Updates the sorting in the list view
        /// </summary>
        public void UpdateSorting()
        {
            //if the list is sorted
            if (this.Sorted)
            {
                //create new sorter
                ListViewColumnSorter columnSorter = new ListViewColumnSorter();

                //assign the sorter to the ListView
                //this.ListViewItemSorter = columnSorter;

                //set the sort column
                columnSorter.SortColumn = this.SortKey;

                //set the ordering
                columnSorter.Order = this.SortOrder;

                //re sort the ListView
                this.Sort();
            }
        }

        /// <summary>
        /// Removes the item from the list
        /// </summary>
        /// <param name="itemName"></param>
        public void Remove(string itemName)
        {
            int index = -1;

            foreach (ListViewItem item in base.Items)
            {
                if (item.Name == itemName)
                {
                    index = base.Items.IndexOf(item);
                    break;
                }
            }

            if (index >= 0)
            {
                base.Items.RemoveAt(index);
            }
        }

        /// <summary>
        /// ensure visible for item
        /// </summary>
        /// <param name="item"></param>
        public void EnsureVisibleWithRegister(ListViewItem item)
        {
            System.Reflection.MethodInfo info = this.GetType().GetMethod("RegisterSelf", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            info.Invoke(item, new object[] { });
            base.EnsureVisible(item.Index);
        }

        /// <summary>
        /// ensure visible for items
        /// </summary>
        public void EnsureVisibleSelectedItems()
        {
            foreach (ListViewItem item in base.SelectedItems)
            {
                EnsureVisibleWithRegister(item);
            }
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private void FCListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (this.SortKey == e.Column)
            {
                this.Sorted = true;
                if (this.SortOrder == SortOrder.Ascending)
                {
                    this.SortOrder = SortOrder.Descending;
                }
                else
                {
                    this.SortOrder = SortOrder.Ascending;
                }
            }
            else
            {
                this.SortKey = e.Column;
                this.SortOrder = SortOrder.Ascending;
                this.Sorted = true;
            }
        }

        #endregion

        #region Constructor
        #endregion
    }
}
