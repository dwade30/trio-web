﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;

namespace fecherFoundation
{
	public class FCListViewItemComparer : IComparer
	{
		#region Fields
		private int column;
		#endregion

		#region Constructors
		public FCListViewItemComparer()
		{
			column = 0;
		}

		public FCListViewItemComparer(int column)
		{
			this.column = column;
		}
		#endregion

		#region methods
		public int Compare(object x, object y)
		{
			ListViewItem liX = (ListViewItem)x;
			ListViewItem liY = (ListViewItem)y;
			int iRet = string.Compare(liX.SubItems[column].Text, liY.SubItems[column].Text);
			if (liX.ListView.Sorting == SortOrder.Descending)
			{
				iRet = -iRet;
			}
			return iRet;
		}
		#endregion
	}
}
