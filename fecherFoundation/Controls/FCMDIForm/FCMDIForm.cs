﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    /// <summary>
    /// VB.MDIForm
    /// </summary>
    public class FCMDIForm : FCForm
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        #endregion

        #region Constructors

        public FCMDIForm()
            : base()
        {
            this.IsMdiContainer = true;
            //CHE: set default value
            this.ScrollBars = true;
            this.AutoShowChildren = true;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Properties

        /// <summary>
        /// For an MDIForm object, the ScrollBars property settings are:
        /// True	(Default) The form has a horizontal or vertical scroll bar, or both.
        /// False	The form has no scroll bars.
        /// </summary>
        [DefaultValue(true)]
        public bool ScrollBars
        {
            // TODO:CHE
            get;
            set;
        }

        [DefaultValue(true)]
        public bool AutoShowChildren
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// In VB6 ActiveForm has a different meaning as in VB.NET
        /// If a Form is created in VB6 it gets the ActiveForm status,
        /// so we cannot have the same by default in VB.NET because replace
        /// ActiveForm with ActiveMDIChild could run into an exception when
        /// ActiveMDIChild is null or replace it with MDI.GetActiveForm() does also not working
        /// when we click on a command button (which lie son the a MDI Toolbar) which should do something on ActiveMDIChild but now the MDI
        /// is the ActiveForm.
        /// </summary>
        public new FCForm ActiveForm 
        {
            get
            {
                return this.ActiveMdiChild != null ? (FCForm)this.ActiveMdiChild : this;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
