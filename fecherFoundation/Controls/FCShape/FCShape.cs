using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using Wisej.Web;
using System.Drawing.Drawing2D;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
	//All the defined shape type
	public enum ShapeType{	Rectangle,
							RoundedRectangle,
		                    Diamond,
							Ellipse, 
							TriangleUp,
							TriangleDown,
							TriangleLeft,
							TriangleRight,
		                    BallonNE,
		                    BallonNW,
							BallonSW,
							BallonSE,
		                    CustomPolygon,
		                    CustomPie
		                    
						}

    /// <summary>
    /// http://www.codeproject.com/Articles/10558/Shape-Control-for-NET
    /// </summary>
    public class FCShape : Control
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private GraphicsPath _custompath = new GraphicsPath();
        private System.ComponentModel.Container components = null;
        private bool _istextset = false;
        private ShapeType _shape = ShapeType.Rectangle;
        private DashStyle _borderstyle = DashStyle.Solid;
        private Color _bordercolor = Color.FromArgb(255, 255, 0, 0);
        private int _borderwidth = 1;
        private GraphicsPath _outline = new GraphicsPath();
        private bool _usegradient = false;
        private Color _centercolor = Color.FromArgb(100, 255, 0, 0);
        private Color _surroundcolor = Color.FromArgb(100, 0, 255, 255);
        private Bitmap _bm;

        #endregion

        #region Constructors

        public FCShape()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }

            //Using of Double Buffer allow for smooth rendering 
            //minizing flickering
            //this.SetStyle(ControlStyles.SupportsTransparentBackColor |
            //              ControlStyles.DoubleBuffer |
            //              ControlStyles.AllPaintingInWmPaint |
            //              ControlStyles.UserPaint, true);

            //set the default backcolor and font
            this.BackColor = Color.FromArgb(0, 255, 255, 255);
            this.Font = new Font("Arial", 12, FontStyle.Bold);
        }
        
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        [Category("Shape"), Description("Background Image to define outline")]
        public Image ShapeImage
        {
            get
            {
                return _bm;
            }
            set
            {


                if (value != null)
                {
                    _bm = (Bitmap)value.Clone();
                    Width = 150;
                    Height = 150;
                    OnResize(null);
                }
                else
                {
                    if (_bm != null)
                        _bm = null;

                    OnResize(null);
                }
            }
        }

        [Category("Shape"), Description("Text to display")]
        public override string Text
        {
            get
            {
                return base.Text;
            }
            set
            {

                base.Text = value;

                //When Visual Studio first create a new control, text=name
                //we do not want any default text, thus we override it with blank
                if ((!_istextset) && (base.Text.Equals(base.Name)))
                    base.Text = "";
                _istextset = true;
            }
        }

        //Overide the BackColor Property to be associated with our custom editor
        [Category("Shape"), Description("Back Color")]
        [BrowsableAttribute(true)]
        [EditorAttribute(typeof(ColorEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }
            set
            {
                base.BackColor = value; this.Refresh();

            }
        }

        [Category("Shape"), Description("Using Gradient to fill Shape")]
        public bool UseGradient
        {
            get { return _usegradient; }
            set { _usegradient = value; this.Refresh(); }
        }

        //For Gradient Rendering, this is the color at the center of the shape
        [Category("Shape"), Description("Color at center")]
        [BrowsableAttribute(true)]
        [EditorAttribute(typeof(ColorEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public Color CenterColor
        {
            get { return _centercolor; }
            set { _centercolor = value; this.Refresh(); }
        }

        //For Gradient Rendering, this is the color at the edges of the shape
        [Category("Shape"), Description("Color at the edges of the Shape")]
        [BrowsableAttribute(true)]
        [EditorAttribute(typeof(ColorEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public Color SurroundColor
        {
            get { return _surroundcolor; }
            set { _surroundcolor = value; this.Refresh(); }
        }

        [Category("Shape"), Description("Border Width")]
        public int BorderWidth
        {
            get { return _borderwidth; }
            set
            {
                _borderwidth = value;
                if (_borderwidth < 0) _borderwidth = 0;

                this.Refresh();
            }
        }

        [Category("Shape"), Description("Border Color")]
        [BrowsableAttribute(true)]
        [EditorAttribute(typeof(ColorEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public Color BorderColor
        {
            get { return _bordercolor; }
            set { _bordercolor = value; this.Refresh(); }
        }

        [Category("Shape"), Description("Border Style")]
        public DashStyle BorderStyle
        {
            get { return _borderstyle; }
            set { _borderstyle = value; this.Refresh(); }
        }

        [Category("Shape"), Description("Select Shape")]
        [BrowsableAttribute(true)]
        [EditorAttribute(typeof(ShapeTypeEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public ShapeType Shape
        {
            get { return _shape; }
            set
            {
                _shape = value;
                OnResize(null);
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        #endregion

        #region Internal Methods

        //This function creates the path for each shape
        //It is also being used by the ShapeTypeEditor to create the various shapes
        //for the Shape property editor UI
        internal static void updateOutline(ref GraphicsPath outline, ShapeType shape, int width, int height)
        {
            switch (shape)
            {
                case ShapeType.CustomPie:
                    outline.AddPie(0, 0, width, height, 180, 270);
                    break;
                case ShapeType.CustomPolygon:
                    outline.AddPolygon(new Point[]{
								  new Point(0,0),
						          new Point(width/2,height/4),
						          new Point(width,0),
						          new Point((width*3)/4,height/2),
						          new Point(width,height),
						          new Point(width/2,(height*3)/4),
						          new Point(0,height),
						          new Point(width/4,height/2)
												  }
                        );
                    break;
                case ShapeType.Diamond:
                    outline.AddPolygon(new Point[]{ 
								new Point(0,height/2),
						        new Point(width/2,0),
						        new Point(width,height/2),
						        new Point(width/2,height)
												  });
                    break;

                case ShapeType.Rectangle:
                    outline.AddRectangle(new Rectangle(0, 0, width, height));
                    break;

                case ShapeType.Ellipse:
                    outline.AddEllipse(0, 0, width, height);
                    break;

                case ShapeType.TriangleUp:
                    outline.AddPolygon(new Point[] { new Point(0, height), new Point(width, height), new Point(width / 2, 0) });
                    break;

                case ShapeType.TriangleDown:
                    outline.AddPolygon(new Point[] { new Point(0, 0), new Point(width, 0), new Point(width / 2, height) });
                    break;

                case ShapeType.TriangleLeft:
                    outline.AddPolygon(new Point[] { new Point(width, 0), new Point(0, height / 2), new Point(width, height) });
                    break;

                case ShapeType.TriangleRight:
                    outline.AddPolygon(new Point[] { new Point(0, 0), new Point(width, height / 2), new Point(0, height) });
                    break;

                case ShapeType.RoundedRectangle:
                    outline.AddArc(0, 0, width / 4, width / 4, 180, 90);
                    outline.AddLine(width / 8, 0, width - width / 8, 0);
                    outline.AddArc(width - width / 4, 0, width / 4, width / 4, 270, 90);
                    outline.AddLine(width, width / 8, width, height - width / 8);
                    outline.AddArc(width - width / 4, height - width / 4, width / 4, width / 4, 0, 90);
                    outline.AddLine(width - width / 8, height, width / 8, height);
                    outline.AddArc(0, height - width / 4, width / 4, width / 4, 90, 90);
                    outline.AddLine(0, height - width / 8, 0, width / 8);
                    break;

                case ShapeType.BallonSW:
                    outline.AddArc(0, 0, width / 4, width / 4, 180, 90);
                    outline.AddLine(width / 8, 0, width - width / 8, 0);
                    outline.AddArc(width - width / 4, 0, width / 4, width / 4, 270, 90);
                    outline.AddLine(width, width / 8, width, (height * 0.75f) - width / 8);
                    outline.AddArc(width - width / 4, (height * 0.75f) - width / 4, width / 4, width / 4, 0, 90);
                    outline.AddLine(width - width / 8, (height * 0.75f), width / 8 + (width / 4), (height * 0.75f));
                    outline.AddLine(width / 8 + (width / 4), height * 0.75f, width / 8 + (width / 8), height);
                    outline.AddLine(width / 8 + (width / 8), height, width / 8 + (width / 8), (height * 0.75f));
                    outline.AddLine(width / 8 + (width / 8), (height * 0.75f), width / 8, (height * 0.75f));
                    outline.AddArc(0, (height * 0.75f) - width / 4, width / 4, width / 4, 90, 90);
                    outline.AddLine(0, (height * 0.75f) - width / 8, 0, width / 8);
                    break;

                case ShapeType.BallonSE:
                    outline.AddArc(0, 0, width / 4, width / 4, 180, 90);
                    outline.AddLine(width / 8, 0, width - width / 8, 0);
                    outline.AddArc(width - width / 4, 0, width / 4, width / 4, 270, 90);
                    outline.AddLine(width, width / 8, width, (height * 0.75f) - width / 8);
                    outline.AddArc(width - width / 4, (height * 0.75f) - width / 4, width / 4, width / 4, 0, 90);
                    outline.AddLine(width - width / 8, (height * 0.75f), width - (width / 4), (height * 0.75f));
                    outline.AddLine(width - (width / 4), height * 0.75f, width - (width / 4), height);
                    outline.AddLine(width - (width / 4), height, width - (3 * width / 8), (height * 0.75f));
                    outline.AddLine(width - (3 * width / 8), (height * 0.75f), width / 8, (height * 0.75f));
                    outline.AddArc(0, (height * 0.75f) - width / 4, width / 4, width / 4, 90, 90);
                    outline.AddLine(0, (height * 0.75f) - width / 8, 0, width / 8);
                    break;

                case ShapeType.BallonNW:
                    outline.AddArc(width - width / 4, (height) - width / 4, width / 4, width / 4, 0, 90);
                    outline.AddLine(width - width / 8, (height), width - (width / 4), (height));
                    outline.AddArc(0, (height) - width / 4, width / 4, width / 4, 90, 90);
                    outline.AddLine(0, (height) - width / 8, 0, height * 0.25f + width / 8);
                    outline.AddArc(0, height * 0.25f, width / 4, width / 4, 180, 90);
                    outline.AddLine(width / 8, height * 0.25f, width / 4, height * 0.25f);
                    outline.AddLine(width / 4, height * 0.25f, width / 4, 0);
                    outline.AddLine(width / 4, 0, 3 * width / 8, height * 0.25f);
                    outline.AddLine(3 * width / 8, height * 0.25f, width - width / 8, height * 0.25f);
                    outline.AddArc(width - width / 4, height * 0.25f, width / 4, width / 4, 270, 90);
                    outline.AddLine(width, width / 8 + height * 0.25f, width, (height) - width / 8);
                    break;

                case ShapeType.BallonNE:
                    outline.AddArc(width - width / 4, (height) - width / 4, width / 4, width / 4, 0, 90);
                    outline.AddLine(width - width / 8, (height), width - (width / 4), (height));
                    outline.AddArc(0, (height) - width / 4, width / 4, width / 4, 90, 90);
                    outline.AddLine(0, (height) - width / 8, 0, height * 0.25f + width / 8);
                    outline.AddArc(0, height * 0.25f, width / 4, width / 4, 180, 90);
                    outline.AddLine(width / 8, height * 0.25f, 5 * width / 8, height * 0.25f);
                    outline.AddLine(5 * width / 8, height * 0.25f, 3 * width / 4, 0);
                    outline.AddLine(3 * width / 4, 0, 3 * width / 4, height * 0.25f);
                    outline.AddLine(3 * width / 4, height * 0.25f, width - width / 8, height * 0.25f);
                    outline.AddArc(width - width / 4, height * 0.25f, width / 4, width / 4, 270, 90);
                    outline.AddLine(width, width / 8 + height * 0.25f, width, (height) - width / 8);
                    break;

                default: break;
            }
        }

        #endregion

        #region Protected Methods

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                    components.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnResize(EventArgs e)
        {
            if ((this.Width < 0) || (this.Height <= 0)) return;

            if (_bm == null)
            {

                _outline = new GraphicsPath();

                updateOutline(ref _outline, _shape, this.Width, this.Height);
            }
            else
            {
                Bitmap bm = (Bitmap)_bm.Clone();
                Bitmap bm2 = new Bitmap(Width, Height);
                System.Diagnostics.Debug.WriteLine(bm2.Width + "," + bm2.Height);
                Graphics.FromImage(bm2).DrawImage(bm, new RectangleF(0, 0, bm2.Width, bm2.Height), new RectangleF(0, 0, bm.Width, bm.Height), GraphicsUnit.Pixel);
                TraceOutline.CTraceOuline trace = new TraceOutline.CTraceOuline();
                string s = trace.TraceOutlineN(bm2, 0, bm2.Height / 2, bm2.Width / 2, Color.Black, Color.White, true, 1);
                Point[] p = trace.StringOutline2Polygon(s);
                _outline = new GraphicsPath();
                _outline.AddPolygon(p);
            }
            if (_outline != null)
                this.Region = new Region(_outline);

            this.Refresh();
            base.OnResize(e);
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            //Rendering with Gradient
            if (_usegradient)
            {
                PathGradientBrush br = new PathGradientBrush(this._outline);
                br.CenterColor = this._centercolor;
                br.SurroundColors = new Color[] { this._surroundcolor };
                pe.Graphics.FillPath(br, this._outline);
            }

            //Rendering with Border
            if (_borderwidth > 0)
            {
                Pen p = new Pen(_bordercolor, _borderwidth * 2);
                p.DashStyle = _borderstyle;
                pe.Graphics.SmoothingMode = SmoothingMode.HighQuality;
                pe.Graphics.DrawPath(p, this._outline);
                p.Dispose();
            }

            //Rendering the text to be at the center of the shape
            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;
            sf.LineAlignment = StringAlignment.Center;
            switch (_shape)
            {
                case ShapeType.BallonNE:
                case ShapeType.BallonNW:
                    pe.Graphics.DrawString(this.Text, this.Font, new SolidBrush(this.ForeColor), new RectangleF(0, this.Height * 0.25f, this.Width, this.Height * 0.75f), sf);
                    break;

                case ShapeType.BallonSE:
                case ShapeType.BallonSW:
                    pe.Graphics.DrawString(this.Text, this.Font, new SolidBrush(this.ForeColor), new RectangleF(0, 0, this.Width, this.Height * 0.75f), sf);
                    break;

                default:
                    pe.Graphics.DrawString(this.Text, this.Font, new SolidBrush(this.ForeColor), new Rectangle(0, 0, this.Width, this.Height), sf);
                    break;
            }

            // Calling the base class OnPaint

            base.OnPaint(pe);
        }

        #endregion

        #region Private Methods

        private void FCShape_TextChanged(object sender, System.EventArgs e)
        {
            this.Refresh();
        }

        #endregion

        #region Component Designer generated code

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FCShape1
            // 
            this.Name = "FCShape1";
            this.TextChanged += new System.EventHandler(this.FCShape_TextChanged);
            this.ResumeLayout(false);

        }

        #endregion
	}
}
