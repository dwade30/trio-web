﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation
{
    /// <summary>
    /// VB.Timer
    /// </summary>
    public class FCTimer : Timer
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCForm form;
        private bool enabled = false;
        private int interval = 0;

        #endregion

        #region Constructors

        public FCTimer()
            : base()
        {
            this.Disposed += FCTimer_Disposed;
        }

        public FCTimer(IContainer container)
            : base(container)
        {
        }
        
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public FCForm Form
        {
            get
            {
                return form;
            }
            set
            {
                form = value;
                if (form != null)
                {
                    form.Timers.Add(this);
                }
            }
        }

        /// <summary>
        /// timer name
        /// </summary>
        public string Name
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// timer location
        /// </summary>
        public Point Location
        {
            // TODO:CHE
            get;
            set;
        }

        [DefaultValue(true)]
        public new bool Enabled
        {
            get
            {
                return enabled;
            }
            set
            {
                enabled = value;
                //CHE: in VB6 a timer is not started until the form is loaded even if its Enabled property is set True in designer
                if (!enabled)
                {
                    this.Stop();
                }
                else
                {
                    //PJ: After Initialize-Component the Form should be set. Then the Timer can started
                    //if(this.Form != null)
                    {
                        BaseEnabled = value;
                        //CHE: start the timer
                        if (value)
                        {
                            this.Start();
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(true)]
        public bool BaseEnabled
        {
            get
            {
                return base.Enabled;
            }
            set
            {
                base.Enabled = value;
            }
        }


        /// <summary>
        /// Returns or sets the number of milliseconds between calls to a Timer control's Timer event.
        /// object.Interval [= milliseconds]
        /// object An object expression that evaluates to an object in the Applies To list. 
        /// milliseconds A numeric expression specifying the number of milliseconds, as described in Settings. 
        /// Settings
        /// The settings for milliseconds are:
        /// Setting Description 
        /// 0 (Default) Disables a Timer control. 
        /// 1 to 65,535  Sets an interval (in milliseconds) that takes effect when a Timer control's Enabled property is set to True. 
        /// For example, a value of 10,000 milliseconds equals 10 seconds. The maximum, 65,535 milliseconds, is equivalent to just over 1 minute. 
        /// Remarks
        /// You can set a Timer control's Interval property at design time or run time. When using the Interval property, remember: 
        /// The Timer control's Enabled property determines whether the control responds to the passage of time. Set Enabled to False to turn a Timer control off, and to True 
        /// to turn it on. When a Timer control is enabled, its countdown always starts from the value of its Interval property setting.
        /// Create a Timer event procedure to tell Visual Basic what to do each time the Interval has passed. 
        /// </summary>
        [DefaultValue(0)]
        public new int Interval
        {
            get
            {
                return interval;
            }
            set
            {
                interval = value;
                //CHE: In Visual Basic 6.0, you can disable a Timer control by setting the Interval property to 0. 
                //https://msdn.microsoft.com/en-us/library/zffx82xt(v=vs.90).aspx
                if (value > 0)
                {
                    base.Interval = value;
                }
                else
                {
                    this.BaseEnabled = false;
                }
            }
        }


        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private void FCTimer_Disposed(object sender, EventArgs e)
        {
            if (this.form != null)
            {
                this.form.Timers.Remove(this);
            }
        }

        #endregion
    }
}
