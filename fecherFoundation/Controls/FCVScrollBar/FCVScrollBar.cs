﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;

namespace fecherFoundation
{
    public partial class FCVScrollBar : VScrollBar
    {
        #region Private Members

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        #endregion

        #region Constructors

        public FCVScrollBar()
        {
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns/sets a scroll bar position's maximum Value property setting.
        /// </summary>
        public int Max
        {
            get
            {
                return this.Maximum - this.LargeChange + this.SmallChange;
            }
            set
            {
                this.Maximum = value + this.LargeChange - this.SmallChange;
            }
        }

        /// <summary>
        /// Returns/sets a scroll bar position's maximum Value property setting.
        /// </summary>
        public int Min
        {
            get
            {
                return this.Minimum;
            }
            set
            {
                this.Minimum = value;
            }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        #endregion
    }
}