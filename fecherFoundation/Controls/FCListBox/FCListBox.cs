﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    /// <summary>
    /// VB.ListBox
    /// needs to be derived from ListView because ListBox does not have checkboxes as in VB6
    /// </summary>
    public partial class FCListBox : ListView
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private bool sorted;

        private ToolTip toolTip = new ToolTip();
        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private Dictionary<object, int> itemData = new Dictionary<object, int>();

        //CHE: when a listview is disabled it goes grey and we can't apply any color to it, use a bitmap instead
        private bool tempBackgroundImageSet = false;

		private int newIndex = -1;

        private ListViewItem lastSelectedItem = null;

        //FC:FINAL:RPU: #i1315 - Add disableEvents flag
        /// <summary>
        /// During the execution of the code, by setting properties or by calling methods, events may be raised that in different contexts must not be raised
        /// Use the disableEvents flag to disable events in the particular contexts
        /// </summary>
        private bool disableEvents = false;

        #endregion

        #region Constructors

        public FCListBox()
        {
            InitializeComponent();
            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            this.Style = 0;
            this.ToolTipText = "";
           // this.ItemPadding = new Padding(10,3,3,3);
            //CHE: draw correctly \t in ListView used as base class for FCListBox
            //this.OwnerDraw = true;
            //this.DrawItem += FCListBox_DrawItem;
            this.EnabledChanged += FCListBox_EnabledChanged;

            //CHE: set default value for MultiSelect as in VB6
            this.MultiSelect = 0;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events

        public event ScrollEventHandler Scroll;

        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties
        // TODO
        public int Appearance { set; get; }

        /// <summary>
        /// introduced to fix compiler errors because tool consider base class a ListBox and not a ListView
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelectedIndex
        {
            get
            {
                if (SelectedIndices.Any())
                {
                    return SelectedIndices[0];
                }

                return -1;
            }
            set
            {
                if (this.Items.Count > value && value >= 0)
                {
                    this.SetSelected(value, true);
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

		/// <summary>
		/// Returns the index of the most recently added item
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int NewIndex
		{
			get
			{
				return this.newIndex;
			}
		}

		/// <summary>
		/// Returns or sets a ToolTip.
		/// Syntax
		/// object.ToolTipText [= string]
		/// The ToolTipText property syntax has these parts:
		/// Part	Description
		/// object	An object expression that evaluates to an object in the Applies To list.
		/// string	A string associated with an object in the Applies To list. that appears in a small rectangle below the object when the user's cursor hovers over the object at run time for about one second.
		/// Remarks
		/// If you use only an image to label an object, you can use this property to explain each object with a few words.
		/// At design time you can set the ToolTipText property string in the control's properties dialog box.
		/// For the Toolbar and TabStrip controls, you must set the ShowTips property to True to display ToolTips.
		/// </summary>
		[DefaultValue("")]
        public string ToolTipText
        {
            get
            {
                return toolTip.GetToolTip(this);
            }
            set
            {
                toolTip.SetToolTip(this, value);
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Gets/sets whether or not check boxes are displayed in the control
        /// </summary>
        [DefaultValue(0)]
        public int Style
        {
            get
            {
                return this.CheckBoxes ? 1 : 0;
            }
            set
            {
                this.CheckBoxes = value == 1;
            }
        }

        /// <summary>
        /// Gets/sets whether or not check boxes are displayed in the control
        /// </summary>
        public new bool CheckBoxes
        {
            get
            {
                return base.CheckBoxes;
            }
            set
            {
                base.CheckBoxes = value;
                // If the CheckBoxes property is set to true, the list can be only single select
                if (base.CheckBoxes)
                {
                    base.MultiSelect = false;
                    // TODO
                    //this.Items.SelectFirstItem();
                }
            }
        }

        /// <summary>
        /// Returns or sets a value indicating whether a user can make multiple selections in a FileListBox or ListBox control and how the multiple selections can be made. 
        /// Read only at run time.
        /// 0 (Default) Multiple selection isn't allowed. 
        /// 1 Simple multiple selection. A mouse click or pressing the SPACEBAR selects or deselects an item in the list. (Arrow keys move the focus.) 
        /// 2 Extended multiple selection. Pressing SHIFT and clicking the mouse or pressing SHIFT and one of the arrow keys (UP ARROW, DOWN ARROW, LEFT ARROW, and RIGHT ARROW) 
        /// extends the selection from the previously selected item to the current item. Pressing CTRL and clicking the mouse selects or deselects an item in the list. 
        /// </summary>
        public new int MultiSelect
        {
            get
            {
                return base.MultiSelect ? 1 : 0;
            }
            set
            {
                // If the CheckBoxes property is set to true, the list can be only single select
                if (base.CheckBoxes)
                {
                    base.MultiSelect = false;
                }
                else
                {
                    base.MultiSelect = value > 0;
                }
            }
        }

        /// <summary>
        /// Gets/sets the index of the currently selected item
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ListIndex
        {
            get
            {
                return SelectedIndex;
            }
            set
            {
                if (ValidateIndex(value))
                {
                    base.Items[value].Selected = true;
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ListCount
        {
            get
            {
                return this.Items.Count;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        // TODO
        public int TopIndex
        {
            get;
            set;
        }

        /// <summary>
        /// Gets/sets if the list is sorted
        /// </summary>
        public bool Sorted
        {
            get
            {
                return sorted;
            }
            set
            {
                sorted = value;
                
                //CHE: update sorting - based on implementation done in FCListView

                //if the list is sorted
                if (this.Sorted)
                {
                    this.Sorting = SortOrder.Ascending;
                    //create new sorter
                    ListViewColumnSorter columnSorter = new ListViewColumnSorter();

                    //assign the sorter to the ListView
                    // TODO
                    //this.ListViewItemSorter = columnSorter;

                    //set the sort column
                    columnSorter.SortColumn = 0;

                    //set the ordering
                    columnSorter.Order = SortOrder.Ascending;

                    //re sort the ListView
                    this.Sort();
                }
                else
                {
                    // TODO
                    //this.ListViewItemSorter = null;

                    this.Sorting = SortOrder.None;
                }
            }
        }

        /// <summary>
        /// Gets/sets if the items in the listbox should be displayed vertically or on multiple columns so that the vertical scroll is not shown
        /// </summary>
        //FC:FINAL:BBE:#584 - Remove, otherwise columns cannot be added to the view
        //public new int Columns { get; set; }

        // These properties were hidden on purpoise in order to replicate the property list of the ListBox, 
        // because ListBox does not contain View, Groups or other properties hidden below

        /// <summary>
        /// Gets/sets how items are displayed in the control. In the case of FCListBox, the view should be set to Details
        /// This way, the control will display a list of items with full row select
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(View.Details)]
        public new View View
        {
            get
            {
                return base.View;
            }
            set
            {
                base.View = value;
            }
        }

        /// <summary>
        /// Gets the collection of Wisej.Web.ListViewGroup objects assigned to the control
        /// </summary>
        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public new ListViewGroupCollection Groups
        //{
        //    get
        //    {
        //        return base.Groups;
        //    }
        //}

        /// <summary>
        /// Gets or sets a value indicating whether clicking an item selects all its subitems.
        /// In the case of the FCListBox, the row should be selected entirely
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(true)]
        public new bool FullRowSelect
        {
            get;
            set;
            //get
            //{
            //    return base.FullRowSelect;
            //}
            //set
            //{
            //    base.FullRowSelect = value;
            //}
        }

        /// <summary>
        /// Gets or sets the column header style. In the FCListBox case, the header style should not be displayed
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(ColumnHeaderStyle.None)]
        public new ColumnHeaderStyle HeaderStyle
        {
            get
            {
                return base.HeaderStyle;
            }
            set
            {
                base.HeaderStyle = value;
            }
        }

        /// <summary>
        /// Gets/sets a value indicating whether items are displayed in groups
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new bool ShowGroups
        {
            get;
            set;
            //get
            //{
            //    return base.ShowGroups;
            //}
            //set
            //{
            //    base.ShowGroups = value;
            //}
        }

        /// <summary>
        /// Gets/sets the Wisej.Web.ImageList to use when displaying items as small icons in the control
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public ImageList SmallImageLsit
        {
            get
            {
                return base.SmallImageList;
            }
            set
            {
                base.SmallImageList = value;
            }
        }

        /// <summary>
        /// Gets/sets the Wisej.Web.ImageList associated with application-defined states in the control
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new ImageList StateImageList
        {
            get
            {
                return base.StateImageList;
            }
            set
            {
                base.StateImageList = value;
            }
        }

        public new string Text
        {
            get
            {
                string text = "";
                var lastItem = this.SelectedItems?.LastOrDefault()??null;
                if (lastItem != null)
                {
                    
                    foreach (ListViewItem.ListViewSubItem item in lastItem.SubItems)
                    {
                        text += item.Text + " ";
                    }
                    text = text.TrimEnd();
                }
                return text;
            }
        }

       
        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
        /// Returns the selected state of the item found at the specified index
        /// </summary>
        /// <param name="index">The index of the item for which the selected state should be returned</param>
        /// <returns>true if the item is selected; false if the item is not selected</returns>
        public bool Selected(int index)
        {
            if (ValidateIndex(index))
            {
                //FC:FINAL:SBE - Harris #i265 - Selectd property returns if item is checked or not
                //return base.Items[index].Selected;
                return base.Items[index].Checked;
            }

            return false;
        }

        /// <summary>
        /// Sets the selected state of the item found at the specified index
        /// </summary>
        /// <param name="index">The index of the item for which the selected state should be returned</param>
        /// <param name="selected">The selected state of the item at the specified index</param>
        public void Selected(int index, bool selected)
        {
            if (ValidateIndex(index))
            {
                base.Items[index].Selected = selected;
            }
        }

        /// <summary>
        /// Gets the ItemData on the ListBox for specified index.
        /// </summary>
        public int ItemData(int index)
        {
            if (index >= this.Items.Count)
            {
                return 0;
            }

            object item = this.Items[index];

            //in VB6 when adding a new item the corresponding ItemData is set to 0
            //if item exists but corresponding ItemData was not set then return 0 as in VB6
            if (!this.itemData.ContainsKey(item))
            {
                return 0;
            }

            return this.itemData[item];
        }

        /// <summary>
        /// Sets the ItemData on the ListBox for specified index.
        /// </summary>
        public void ItemData(int index, int value)
        {
            if (index >= this.Items.Count)
            {
                return;
            }

            object item = this.Items[index];

            if (this.itemData.ContainsKey(item))
            {
                this.itemData[item] = value;
            }
            else
            {
                this.itemData.Add(item, value);
            }
        }

        /// <summary>
        /// Items data.
        /// </summary>
        public void ItemData(int[] arrValues)
        {
            itemData.Clear();

            this.AddRange(arrValues);
            this.AddToListCollection(itemData);
        }

        /// <summary>
        /// Items data.
        /// </summary>
        /// <param name="arrValues"></param>
        public void List(string[] arrValues)
        {
            itemData.Clear();

            this.AddRange(arrValues);
            this.AddToListCollection(itemData);
        }

        public string List(int index)
        {
            if (index >= this.Items.Count)
            {
                return "";
            }

            ListViewItem item = this.Items[index];

            return item.Text;
        }

        /// <summary>
        /// Adds an item to a ListBox or ComboBox control or adds a row to a MS Flex Grid control. Doesn't support named arguments.
        /// Syntax
        /// object.AddItem item, index
        /// The AddItem method syntax has these parts:
        /// Part	Description
        /// object	Required. An object expression that evaluates to an object in the Applies To list.
        /// item	Required. string expression specifying the item to add to the object. For the MS Flex Grid control only, use the tab character (character code 09) to separate multiple strings you 
        /// want to insert into each column of a newly added row.
        /// index	Optional. Integer specifying the position within the object where the new item or row is placed. For the first item in a ListBox or ComboBox control or for the first row in a 
        /// MS Flex Grid control, index is 0.
        /// Remarks
        /// If you supply a valid value for index, item is placed at that position within the object. If index is omitted, item is added at the proper sorted position (if the Sorted property is set to True)
        /// or to the end of the list (if Sorted is set to False).
        /// A ListBox or ComboBox control that is bound to a Data control doesn't support the AddItem method.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"></param>
        public void AddItem(string item, int index = -1)
        {
            ListViewItem lvItem;

            if (item.Contains("\t"))
            {
                string[] items = item.Split(new string[] { "\t" }, StringSplitOptions.None);
                lvItem = new ListViewItem(items);
                if (index == -1)
                {
                    this.Items.Add(lvItem);
                }
                else
                {
                    this.Items.Insert(index, lvItem);
                }
            }
            else
            {
                if (index == -1)
                {
                    lvItem = this.Items.Add(item);
                }
                else
                {
                    lvItem = this.Items.Insert(index, item);
                }
            }

            if (Sorted)
            {
                base.Sort();
            }
            //AM:HARRIS:#3515 - return the  new index after sorting
            newIndex = lvItem.Index;
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        /// <summary>
        /// Returns a value indicating whether the specified item is checked
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public bool GetItemChecked(int index)
        {
            return this.Items[index].Checked;
        }

        /// <summary>
        /// Sets Wisej.Web.CheckState for the item at the specified index to Checked
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        public void SetItemChecked(int index, bool value)
        {
            this.Items[index].Checked = true;
        }
              
        #endregion

        #region Internal Methods

        /// <summary>
        /// Adds range of item data.
        /// </summary>
        internal void AddRange(int[] values)
        {
            if (values != null)
            {
                for (int intCounter = 0; intCounter < values.Length; intCounter++)
                {
                    itemData.Add(intCounter, values[intCounter]);
                }
            }
        }

        /// <summary>
        /// Adds range of item data.
        /// </summary>
        /// <param name="values"></param>
        internal void AddRange(string[] values)
        {
            if (values != null)
            {
                for (int intCounter = 0; intCounter < values.Count(); intCounter++)
                {
                    itemData.Add(values[intCounter], intCounter);
                }
            }
        }

        #endregion

        #region Protected Methods
        //FC:FINAL:RPU: #i1302 - The SelectedIndexChanged must be fired only if the object was not disposed
        protected override void OnSelectedIndexChanged(EventArgs e)
        {
            if (!this.IsDisposed && !disableEvents)
            {
                base.OnSelectedIndexChanged(e);
            }
        }
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            // The only column displayed should be resized along with the control, this way the full row select will be displayed correctly
            if (this.Columns.Count == 1)
            {
                originalColumn.Width = this.ClientSize.Width - 18; //Need to subtract 18 in case there is a vertical scrollbar
            }
        }

        protected override void OnItemCheck(ItemCheckEventArgs ice)
        {
			//FC:FINAL:MSH - i.issue #1770: check if listbox was disposed, because in some cases owner form can be disposed before finishing work of all events and will be threw an exception
			if (!this.IsDisposed)
			{
				// When checking an item, the checked item should also become selected
				if (ValidateIndex(ice.Index) && ice.NewValue == CheckState.Checked)
				{
					ListViewItem checkedItem = base.Items[ice.Index];
					if (!this.SelectedItems.Contains(checkedItem))
					{
						checkedItem.Selected = true;
						//return;
					}
				}
				//FC:FINAL:MSH - i.issue #1770: check if listbox was disposed, because in some cases owner form can be disposed before finishing work of all events and will be threw an exception
				if (!this.IsDisposed)
				{
					base.OnItemCheck(ice);
				}
			}
        }

        protected override void OnItemClick(ItemClickEventArgs e)
        {
			//FC:FINAL:MSH - i.issue #1770: check if listbox was disposed, because in some cases owner form can be disposed before finishing work of all events and will be threw an exception
			if (!this.IsDisposed)
			{
				if (e.Item == this.lastSelectedItem)
				{
					this.lastSelectedItem = null;
				}
				else
				{
					if (e.Role == "content")
					{
						//FC:FINAL:MSH - i.issue #1772: check if listbox was disposed, because in some cases owner form can be disposed before finishing work of all events and will be threw an exception
						//DDU:HARRIS:#i2365 - check also if item was selected by user. Problem at item not entirely visible and visual moves lower to be entirely visible
						if (!this.IsDisposed && e.Item.ListView != null && e.Item.Selected)
						{
							e.Item.Checked = !e.Item.Checked;
						}
					}
				}
				//FC:FINAL:MSH - i.issue #1770: check if listbox was disposed, because in some cases owner form can be disposed before finishing work of all events and will be threw an exception
				if (!this.IsDisposed)
				{
					base.OnItemClick(e);
				}
			}
        }

        protected override void OnItemSelectionChanged(ItemSelectionChangedEventArgs e)
        {
            base.OnItemSelectionChanged(e);

            if (e.IsSelected)
            {
                this.lastSelectedItem = e.Item;
            }
        }

        /// <summary>
        /// Scroll event
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnScroll(ScrollEventArgs e)
        {
            if (this.Scroll != null)
            {
                this.Scroll(this, e);
            }
        }

        //AM:HARRIS:#4014 - click/double click shouldn't be fired when there are no items in the list
        protected override void OnDoubleClick(EventArgs e)
        {
            if (this.Items.Count > 0)
            {
                base.OnDoubleClick(e);
            }
        }

        protected override void OnClick(EventArgs e)
        {
            //if (this.Items.Count > 0)
            //{
                base.OnClick(e);
            //}
        }

        #endregion

        #region Private Methods

        private void FCListBox_EnabledChanged(object sender, EventArgs e)
        {
            //CHE: when a listview is disabled it goes grey and we can't apply any color to it, use a bitmap instead
            if (!this.Enabled && this.BackgroundImage == null)
            {
                Bitmap bm = new Bitmap(this.ClientSize.Width, this.ClientSize.Height);
                Graphics.FromImage(bm).Clear(this.BackColor);
                this.BackgroundImage = bm;
                tempBackgroundImageSet = true;
            }
            else if (this.Enabled && tempBackgroundImageSet)
            {
                this.BackgroundImage = null;
                tempBackgroundImageSet = false;
            }
        }

        /// <summary>
        /// DBU : For each keyvalue pair found in itemData dictionary, adds an item to ListViewCollection.
        /// </summary>
        /// <param name="itemData">Dictionary <object, int > </param>
        private void AddToListCollection(Dictionary<object, int> itemData)
        {
            foreach (KeyValuePair<object, int> kvp in itemData)
            {
                this.AddItem(kvp.Key.ToString(), kvp.Value);
            }
        }

        // TODO
        //private void FCListBox_DrawItem(object sender, DrawListViewItemEventArgs e)
        //{
        //    //CHE: draw correctly \t in ListView used as base class for FCListBox
        //    if (!this.CheckBoxes)
        //    {
        //        e.DrawBackground();
        //        if (e.Item != null && e.Item.Selected)
        //        {
        //            e.Graphics.FillRectangle(SystemBrushes.Highlight, e.Bounds);
        //            TextRenderer.DrawText(e.Graphics, e.Item.Text, this.Font,
        //                e.Bounds, SystemColors.HighlightText, TextFormatFlags.ExpandTabs);
        //            e.DrawFocusRectangle();
        //        }
        //        else
        //        {
        //            e.DrawText(TextFormatFlags.ExpandTabs);
        //        }
        //    }
        //    else
        //    {
        //        e.DrawDefault = true;
        //    }
        //}

        /// <summary>
        /// Validate the index
        /// </summary>
        /// <param name="index">Index to validate</param>
        /// <returns>true if the index is within bounds, false otherwise</returns>
        private bool ValidateIndex(int index)
        {
            return index >= 0 && index < this.Items.Count;
        }

        public void SetSelected(int index, bool value)
        {
            if (ValidateIndex(index))
            {
                this.Items[index].Selected = value;
                if (this.CheckBoxes)
                {
                    this.Items[index].Checked = value;
                }
            }
        }
        
        public new void Clear()
        {
            //FC:FINAL:RPU:#i1315 - SelectedIndexChanged is not triggered on clear
            bool currentDisableEvents = this.disableEvents;
            this.disableEvents = true;
            base.Clear();
            this.disableEvents = currentDisableEvents;
            //Add back the column
            base.Columns.AddRange(new Wisej.Web.ColumnHeader[] {
            this.originalColumn});
        }

        #endregion
    }
}
