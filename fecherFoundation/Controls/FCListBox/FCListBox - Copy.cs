﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation
{
    public partial class FCListBox : ListView
    {
        #region Properties

        /// <summary>
        /// Gets/sets whether or not check boxes are displayed in the control
        /// </summary>
        public int Style
        {
            get
            {
                return this.CheckBoxes ? 1 : 0;
            }
            set
            {
                this.CheckBoxes = ( value == 1 );
            }
        }

        /// <summary>
        /// Gets/sets whether or not check boxes are displayed in the control
        /// </summary>
        public new bool CheckBoxes
        {
            get
            {
                return base.CheckBoxes;
            }
            set
            {
                base.CheckBoxes = value;
                // If the CheckBoxes property is set to true, the list can be only single select
                if (base.CheckBoxes)
                {
                    base.MultiSelect = false;
                }
            }
        }

        /// <summary>
        /// Gets/sets a value indicating whether multiple items can be selected
        /// </summary>
        public new bool MultiSelect
        {
            get
            {
                return base.MultiSelect;
            }
            set
            {
                // If the CheckBoxes property is set to true, the list can be only single select
                if (base.CheckBoxes)
                {
                    base.MultiSelect = false;
                }
                else
                {
                    base.MultiSelect = value;
                }
            }
        }

        /// <summary>
        /// Gets/sets how items are displayed in the control. In the case of FCListBox, the view should be set to Details
        /// This way, the control will display a list of items with full row select
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(View.Details)]
        public new View View
        {
            get
            {
                return base.View;
            }
            set
            {
                base.View = value;
            }
        }

        /// <summary>
        /// Gets the collection of all column headers that appear in the control
        /// In the case of FCListBox, a single column will be used in order to replicate the ListBox behavior
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new ColumnHeaderCollection Columns
        {
            get
            {
                return base.Columns;
            }
        }

        /// <summary>
        /// Gets the collection of System.Windows.Forms.ListViewGroup objects assigned to the control
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new ListViewGroupCollection Groups
        {
            get
            {
                return base.Groups;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether clicking an item selects all its subitems.
        /// In the case of the FCListBox, the row should be selected entirely
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(true)]
        public new bool FullRowSelect
        {
            get
            {
                return base.FullRowSelect;
            }
            set
            {
                base.FullRowSelect = value;
            }
        }

        /// <summary>
        /// Gets or sets the column header style. In the FCListBox case, the header style should not be displayed
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(ColumnHeaderStyle.None)]
        public new ColumnHeaderStyle HeaderStyle
        {
            get
            {
                return base.HeaderStyle;
            }
            set
            {
                base.HeaderStyle = value;
            }
        }

        /// <summary>
        /// Gets/sets a value indicating whether items are displayed in groups
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new bool ShowGroups
        {
            get
            {
                return base.ShowGroups;
            }
            set
            {
                base.ShowGroups = value;
            }
        }

        /// <summary>
        /// Gets/sets the System.Windows.Forms.ImageList to use when displaying items as small icons in the control
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new ImageList SmallImageLsit
        {
            get
            {
                return base.SmallImageList;
            }
            set
            {
                base.SmallImageList = value;
            }
        }

        /// <summary>
        /// Gets/sets the System.Windows.Forms.ImageList associated with application-defined states in the control
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new ImageList StateImageLsit
        {
            get
            {
                return base.StateImageList;
            }
            set
            {
                base.StateImageList = value;
            }
        }

        #endregion

        #region Constructor
        public FCListBox()
        {
            InitializeComponent();
        }
        #endregion

        #region Public Methods
        public bool Selected(int index)
        {
            if (index >= 0 && index < this.Items.Count)
            {
                return this.Items[index].Selected;
            }

            return false;
        }
        #endregion


        #region Members
        private ListViewItem prevSelected = null;
        #endregion

        #region Overrides
        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            // The only column displayed should be resized along with the control, this way the full row select will be displayed correctly
            this.originalColumn.Width = this.Width - 5;
        }

        protected override void OnColumnClick(ColumnClickEventArgs e)
        {
            base.OnColumnClick(e);
        }
        protected override void OnItemSelectionChanged(ListViewItemSelectionChangedEventArgs e)
        {
            if (this.CheckBoxes && !e.IsSelected)
            {
                prevSelected = e.Item;
            }
            base.OnItemSelectionChanged(e);
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            return;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Clicks > 1)
            {
                return;
            }

            ListViewHitTestInfo hti = base.HitTest(e.Location);
            if (this.CheckBoxes && hti != null)
            {
                if (hti.Location == ListViewHitTestLocations.StateImage)
                {
                    //when clicking on checkbox image select the item
                    hti.Item.Selected = true;
                }
                else
                {
                    if (prevSelected != null && prevSelected == hti.Item)
                    {
                        // toggle on single click on an already seleted row
                        hti.Item.Checked = !hti.Item.Checked;
                    }
                    else
                    {
                        hti.Item.Selected = true;
                    }
                }

                prevSelected = hti.Item;
            }
            base.OnMouseDown(e);
        }

        protected override void OnMouseClick(MouseEventArgs e)
        {
            base.OnMouseClick(e);
            return;

            ListViewHitTestInfo hti = base.HitTest(e.Location);
            if (this.CheckBoxes && hti != null)
            {
                if (hti.Location == ListViewHitTestLocations.StateImage)
                {
                    //when clicking on checkbox image select the item
                    hti.Item.Selected = true;
                }
                else
                {
                    if (prevSelected != null && prevSelected == hti.Item)
                    {
                        // toggle on single click on an already seleted row
                        hti.Item.Checked = !hti.Item.Checked;
                    }
                    else
                    {
                        hti.Item.Selected = true;
                    }
                }

                prevSelected = hti.Item;
            }
            base.OnMouseClick(e);
        }

        #endregion
    }
}
