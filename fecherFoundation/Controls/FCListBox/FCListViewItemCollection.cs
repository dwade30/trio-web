﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation
{
    public class FCListViewItemCollection : ListView.ListViewItemCollection
    {
        #region Members
        private FCListBox ownerListView;
        #endregion

        #region Events
        public event OnItemAdd ItemAdd;
        public delegate void OnItemAdd(object sender, EventArgs e);
        #endregion

        #region Constructor
        public FCListViewItemCollection(FCListBox owner)
            : base(owner)
        {
            ownerListView = owner;
        }
        #endregion

        #region Private methods

        #endregion

        #region Overrides

        #region Select first item overrides
        //START: The overrides are needed in order to select the first item in the list, if the list displays CheckBoxes
        public override ListViewItem Add(ListViewItem value)
        {
            ListViewItem addedItem = base.Add(value);
            ownerListView.SelectFirstItem();
            return addedItem;
        }

        public override ListViewItem Add(string key, string text, int imageIndex)
        {
            ListViewItem addedItem = base.Add(key, text, imageIndex);
            ownerListView.SelectFirstItem();
            return addedItem;
        }

        public override ListViewItem Add(string key, string text, string imageKey)
        {
            ListViewItem addedItem = base.Add(key, text, imageKey);
            ownerListView.SelectFirstItem();
            return addedItem;
        }

        public override ListViewItem Add(string text)
        {
            ListViewItem addedItem = base.Add(text);
            ownerListView.SelectFirstItem();
            return addedItem;
        }

        public override ListViewItem Add(string text, int imageIndex)
        {
            ListViewItem addedItem = base.Add(text, imageIndex);
            ownerListView.SelectFirstItem();
            return addedItem;
        }

        public override ListViewItem Add(string text, string imageKey)
        {
            ListViewItem addedItem = base.Add(text, imageKey);
            ownerListView.SelectFirstItem();
            return addedItem;
        }

        public override ListViewItem Insert(int index, string key, string text, int imageIndex)
        {
            ListViewItem insertedItem = base.Insert(index, key, text, imageIndex);
            ownerListView.SelectFirstItem();
            return insertedItem;
        }

        public override ListViewItem Insert(int index, string key, string text, string imageKey)
        {
            ListViewItem insertedItem = base.Insert(index, key, text, imageKey);
            ownerListView.SelectFirstItem();
            return insertedItem;
        }
        //END: The overrides are needed in order to select the first item in the list, if the list displays CheckBoxes
        #endregion

        public string this[int index]
        {
            get
            {
                return base[index].Text;
            }
            set
            {
                base[index].Text = value;
            }
        }

        public override ListViewItem this[string key]
        {
            get
            {
                return base[key];
            }
        }
        #endregion
    }
}
