﻿namespace fecherFoundation
{
    partial class FCListBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.originalColumn = ((Wisej.Web.ColumnHeader)(new Wisej.Web.ColumnHeader()));
            this.SuspendLayout();
            // 
            // originalColumn
            // 
            this.originalColumn.Width = 145;
            this.originalColumn.Resizable = false;
            // 
            // FCListBox
            // 
            base.Columns.AddRange(new Wisej.Web.ColumnHeader[] {
            this.originalColumn});
            this.HeaderStyle = Wisej.Web.ColumnHeaderStyle.None;
            this.Size = new System.Drawing.Size(100, 150);
            this.View = Wisej.Web.View.Details;
            this.FullRowSelect = true;
            this.GridLineStyle = Wisej.Web.GridLineStyle.None;
            this.ResumeLayout(false);
        }

        private Wisej.Web.ColumnHeader originalColumn;
        #endregion
    }
}
