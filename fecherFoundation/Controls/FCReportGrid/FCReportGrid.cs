﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static fecherFoundation.FCGrid;

namespace fecherFoundation
{
	public class FCReportGrid : DataGridView
	{
		#region Private Members

		private float height = 0;
		private int m_currentRow = 0;
		private int m_currentColumn = 0;
		private int m_cols = 0;
		private int m_rows = 0;
		private int m_rowSel = -1;
		private int m_colSel = -1;

		#endregion

		#region Constructor

		public FCReportGrid()
		{
			this.Enabled = false;
			this.ColumnHeadersVisible = false;
			this.RowHeadersVisible = false;
			this.AllowUserToAddRows = false;
			this.BorderStyle = BorderStyle.None;
			this.GridColor = System.Drawing.Color.White;
			this.BackgroundColor = System.Drawing.Color.White;
			this.RowsDefaultCellStyle.SelectionBackColor = System.Drawing.Color.White;
			this.RowsDefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
		}

		#endregion

		#region Public Methods

		public void AddItem(string rowData)
        {
            object[] args = rowData.Split('\t');
            while (args.Length > this.ColumnCount)
            {
                this.Columns.Add(new DataGridViewTextBoxColumn());
            }
            base.Rows.Add(args);

        }

        public void RemoveItem(int index)
        {
            base.Rows.RemoveAt(index);
        }

        /// <summary>
        /// Returns the height of the specified row. This property is not available at design time.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public float RowHeight(int row)
		{

			if (this.RowCount <= 0)
			{
				return base.ColumnHeadersHeight / 144F;
			}
			else
			{
				return base.Rows[row].Height / 144F;
			}
		}

		/// <summary>
		/// Sets the text contents of an arbitrary cell.
		/// </summary>
		/// <param name="row"></param>
		/// <param name="col"></param>
		/// <param name="contents"></param>
		public void TextMatrix(int row, int col, object contents)
		{
			if (row >= 0 && row < this.RowCount && col >= 0 && col < this.ColumnCount)
			{
				this[col, row].Value = contents;
			}
			else
			{
				while (this.ColumnCount <= col)
				{
					base.Columns.Add(new DataGridViewTextBoxColumn());
				}
				if (this.Rows < row + 1)
				{
					this.Rows = row + 1;
				}
				this[col, row].Value = contents;
			}
		}

		public string TextMatrix(int row, int col)
		{
			if (row >= 0 && row < this.RowCount && col >= 0 && col < this.ColumnCount)
			{
				return Convert.ToString(this[col, row].Value);
			}
			return null;
		}

		public void ColWidth(int column, int width)
		{
			while (this.ColumnCount <= column)
			{
				base.Columns.Add(new DataGridViewTextBoxColumn());
			}

			base.Columns[column].Width = width;
		}

		/// <summary>
        /// Returns the index of a row that contains a specified string or RowData value.
        /// </summary>
        /// <param name="item">This parameter contains the data being searched.</param>
        /// <param name="row">This parameter contains the rows where the search should start. The default value is FixedRows.</param>
        /// <param name="col">This parameter tells the control which column should be searched. By default, this value is set to -1, which means the control will look for matches against RowData. If Col is set to a value greater than -1, then the control will look for matches against the cell's contents for the given column.</param>
        /// <param name="caseSensitive">This parameter is True by default, which means the search is case-sensitive. Set it to False if you want a case-insensitive search. (for example, when looking for "CELL" you may find "cell" ). </param>
        /// <param name="fullMatch">This parameter is True by default, which means the search is for a full match. Set it to False if you want to allow partial matches (for example, when looking for "MY" you may find "MYCELL"). This parameter is only relevant when you are looking for a string. </param>
        /// <returns></returns>
        public int FindRow(dynamic item, int row = -1, int col = -1, bool caseSensitive = true, bool fullMatch = true)
        {
            if (row > -1)
            {
                if (col != -1)
                {
                    dynamic cellValue;
                    for (int i = row; i < this.Rows; i++)
                    {
                        if (col != -1)
                        {
                            cellValue = this[col, i].Value;
                        }
                        else
                        {
                            cellValue = base.Rows[i].HeaderCell.Value;
                        }
                        if (caseSensitive)
                        {
                            if (fullMatch)
                            {
                                if (Convert.ToString(cellValue) == item.ToString())
                                {
                                    return i;
                                }
                            }
                            else
                            {
                                if (cellValue != null && cellValue.ToString().Contains(item))
                                {
                                    return i;
                                }
                            }
                        }
                        else
                        {
                            if (fullMatch)
                            {
                                if (cellValue.ToUpper() == item.ToUpper())
                                {
                                    return i;
                                }
                            }
                            else
                            {
                                if (cellValue.ToUpper().Contains(item.ToUpper()))
                                {
                                    return i;
                                }
                            }
                        }
                    }
                }
            }
        return -1;
		}

		public void Cell(CellPropertySettings setting, int row1, int col1, int row2, int col2, dynamic value)
		{
			switch (setting)
			{
				case CellPropertySettings.flexcpAlignment:
					FCGrid.AlignmentSettings alignment = value is FCGrid.AlignmentSettings ? value : FCGrid.AlignmentSettings.flexAlignLeftCenter;
					for (int i = row1; i <= row2; i++)
					{
						for (int j = col1; j <= col2; j++)
						{
							int col = j;
							int row = i;
							if (row < -1)
							{
								return;
							}
							DataGridViewCell cell = GetDataGridViewCell(row, col);

							switch (alignment)
							{
								case AlignmentSettings.flexAlignCenterBottom:
									cell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;
									break;
								case AlignmentSettings.flexAlignCenterCenter:
									cell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
									break;
								case AlignmentSettings.flexAlignCenterTop:
									cell.Style.Alignment = DataGridViewContentAlignment.TopCenter;
									break;
								case AlignmentSettings.flexAlignLeftBottom:
									cell.Style.Alignment = DataGridViewContentAlignment.BottomLeft;
									break;
								case AlignmentSettings.flexAlignLeftCenter:
									cell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
									break;
								case AlignmentSettings.flexAlignLeftTop:
									cell.Style.Alignment = DataGridViewContentAlignment.TopLeft;
									break;
								case AlignmentSettings.flexAlignRightBottom:
									cell.Style.Alignment = DataGridViewContentAlignment.BottomRight;
									break;
								case AlignmentSettings.flexAlignRightCenter:
									cell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
									break;
								case AlignmentSettings.flexAlignRightTop:
									cell.Style.Alignment = DataGridViewContentAlignment.TopRight;
									break;
							}
							if (row == -1)
							{
								if (col >= 0 && col < this.Columns.Count)
								{
									this.Columns[col].HeaderCell.Style.Alignment = cell.Style.Alignment;
								}
							}
						}
					}
					break;
				case CellPropertySettings.flexcpBackColor:
					Color backColor = value is Color ? value : ColorTranslator.FromOle(Convert.ToInt32(value));
					for (int i = row1; i <= row2; i++)
					{
						if (col2 - col1 + 1 == base.ColumnCount)
						{
							base.Rows[i].DefaultCellStyle.BackColor = backColor;
							base.Rows[i].HeaderCell.Style.BackColor = backColor;
						}
						else
						{
							for (int j = col1; j <= col2; j++)
							{
								GetDataGridViewCell(i, j).Style.BackColor = backColor;
							}
						}
					}
					break;
				case CellPropertySettings.flexcpForeColor:
					Color foreColor = value is Color ? value : ColorTranslator.FromOle(Convert.ToInt32(value));
					for (int i = row1; i <= row2; i++)
						for (int j = col1; j <= col2; j++)
						{
							GetDataGridViewCell(i, j).Style.ForeColor = foreColor;
						}
					break;
				case CellPropertySettings.flexcpFontBold:
					for (int i = row1; i <= row2; i++)
						for (int j = col1; j <= col2; j++)
						{
							DataGridViewCell cell = GetDataGridViewCell(i, j);
							if (cell.Style.Font != null)
							{
								cell.Style.Font = new Font(cell.Style.Font, value ? FontStyle.Bold : FontStyle.Regular);
							}
							else
							{
								cell.Style.Font = new Font("default", 16F, value ? FontStyle.Bold : FontStyle.Regular, GraphicsUnit.Pixel);
							}
						}
					break;
				case CellPropertySettings.flexcpFontSize:
					for (int i = row1; i <= row2; i++)
						for (int j = col1; j <= col2; j++)
						{
							Single fontSize = (Single)value;
							DataGridViewCell cell = GetDataGridViewCell(i, j);
							if (cell.Style.Font != null)
							{
								cell.Style.Font = new Font("default", fontSize, cell.Style.Font.Style, cell.Style.Font.Unit);
							}
							else
							{
								cell.Style.Font = new Font("default", fontSize, FontStyle.Regular, GraphicsUnit.Pixel);
							}
						}
					break;
			}
		}

		#endregion

		#region Internal Methods

		/// <summary>
		/// GetDataGridViewCell
		/// </summary>
		/// <param name="rowIndex"></param>
		/// <param name="columnIndex"></param>
		/// <returns></returns>
		internal DataGridViewCell GetDataGridViewCell(int rowIndex, int columnIndex)
		{
			DataGridViewCell cell = null;
			if (columnIndex == -1 && rowIndex < 0)
			{
				cell = this.TopLeftHeaderCell;
			}
			else if (rowIndex < 0 && columnIndex > -1)
			{
				cell = base.Columns[columnIndex].HeaderCell;
			}
			else if (columnIndex == -1 && rowIndex > -1)
			{
				cell = base.Rows[rowIndex].HeaderCell;
			}
			else
			{
				cell = base[columnIndex, rowIndex];
			}
			return cell;
		}

		#endregion

		#region Properties

		public int WidthOriginal
		{
			get
			{
				return this.Width;
			}
			set
			{
				base.Width = value;
			}
		}

		public float Height
		{
			get
			{
				return height;
			}
			set
			{
				height = value;
			}
		}

		//:RPU
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public SortSettings Sort
		{
			set
			{
				switch (value)
				{
					case SortSettings.flexSortGenericAscending:
						base.Sort(this.Columns[this.Col], ListSortDirection.Ascending);
						break;
				}

			}
		}

		/// <summary>
		/// This property allows you to specify different sorting orders for each column on the grid.
		/// The most common settings for this property are flexSortGenericAscending and flexSortGenericDescending. 
		/// For a complete list of possible settings, see the Sort property.
		/// </summary>
		/// <param name="Col"></param>
		/// <param name="flexSortGenericAscending"></param>
		public void ColSort(int Col, SortSettings flexSortGenericAscending)
		{
			//TODO
		}

		/// <summary>
		/// Returns or sets the coordinates of the active cell, not available at design time.
		/// Use these properties to specify a cell in an MSFlexGrid or to determine which row or column contains the current cell. 
		/// Columns and rows are numbered from zero rows are numbered from top to bottom, and columns are numbered from left to right.
		/// Setting these properties automatically resets RowSel and ColSel, the selection becoming the current cell. Therefore, to specify a block selection, 
		/// you must set Row and Col first, and then set RowSel and ColSel.
		/// The value of the current cell, defined by the Col and Row settings, is the text contained in that cell. To modify a cells value without changing 
		/// the selected Row and Col properties, use the TextMatrix property.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int Row
		{
			get
			{
				return this.m_currentRow;
			}
			set
			{
				//exit if no changes
				if (this.m_currentRow == value)
				{
					return;
				}

				this.m_currentRow = value;

			}
		}

		/// <summary>
		/// Gets or sets the current number of rows
		/// </summary>
		public new int Rows
		{
			get
			{
				return base.Rows.Count;
			}
			set
			{
				base.RowCount = value;
			}
		}

		/// <summary>
		/// RowSel Returns or sets the start or end row for a range of cells.
		/// These properties are not available at design time.
		/// object.RowSel [= value]
		/// object	An object expression that evaluates to an object in the Applies To list.
		/// value	A Long value that specifies the start or end row, or column, for a range of cells.
		/// Remarks
		/// You can use these properties to select a specific region of the MSHFlexGrid programmatically, or to read the dimensions of an area that the user selects into code.
		/// The MSHFlexGrid cursor is in the cell at Row, Col. The MSHFlexGrid selection is the region between rows Row and RowSel and columns Col and ColSel. Note that RowSel 
		/// may be above or below Row, and ColSel may be to the left or to the right of Col.
		/// Whenever you set the Row and Col properties, RowSel and ColSel are automatically reset, so the cursor becomes the current selection. 
		/// To select a block of cells from code, you must first set the Row and Col properties, and then set RowSel and ColSel.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int RowSel
		{
			get
			{
				return m_rowSel;
			}
			set
			{
				if (value < 0 || value > this.Rows)
				{
					return;
				}
				m_rowSel = value;
			}
		}

		/// <summary>
		/// Returns or sets the coordinates of the active cell, not available at design time.
		/// Use these properties to specify a cell in an MSFlexGrid or to determine which row or column contains the current cell. 
		/// Columns and rows are numbered from zero rows are numbered from top to bottom, and columns are numbered from left to right.
		/// Setting these properties automatically resets RowSel and ColSel, the selection becoming the current cell. Therefore, to specify a block selection, 
		/// you must set Row and Col first, and then set RowSel and ColSel.
		/// The value of the current cell, defined by the Col and Row settings, is the text contained in that cell. To modify a cells value without changing 
		/// the selected Row and Col properties, use the TextMatrix property.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int Col
		{
			get
			{
				return this.m_currentColumn;
			}
			set
			{
				//exit if no changes
				if (this.m_currentColumn == value)
				{
					return;
				}

				this.m_currentColumn = value;
			}
		}

		public int Cols
		{
			get
			{
				return m_cols;
			}
			set
			{
				m_cols = value;
				if (this.Cols < 1)
				{
					base.ColumnCount = 0;
				}
				else
				{
					bool wasColumnHeaderSelect = false;
					if (base.SelectionMode == DataGridViewSelectionMode.ColumnHeaderSelect)
					{
						wasColumnHeaderSelect = true;
						base.SelectionMode = DataGridViewSelectionMode.CellSelect;
					}
					base.ColumnCount = Cols - 1;
				}
			}
		}

		/// <summary>
		/// ColSel Returns or sets the start or end column for a range of cells.
		/// These properties are not available at design time.
		/// object.ColSel [= value]
		/// object	An object expression that evaluates to an object in the Applies To list.
		/// value	A Long value that specifies the start or end row, or column, for a range of cells.
		/// Remarks
		/// You can use these properties to select a specific region of the MSHFlexGrid programmatically, or to read the dimensions of an area that the user selects into code.
		/// The MSHFlexGrid cursor is in the cell at Row, Col. The MSHFlexGrid selection is the region between rows Row and RowSel and columns Col and ColSel. Note that RowSel 
		/// may be above or below Row, and ColSel may be to the left or to the right of Col.
		/// Whenever you set the Row and Col properties, RowSel and ColSel are automatically reset, so the cursor becomes the current selection. 
		/// To select a block of cells from code, you must first set the Row and Col properties, and then set RowSel and ColSel.
		/// </summary>
		[Browsable(false)]
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
		public int ColSel
		{
			get
			{
				return m_colSel;
			}
			set
			{
				if (value < 0 || value > this.Cols)
				{
					return;
				}
				m_colSel = value;
			}
		}

		#endregion
	}
}
