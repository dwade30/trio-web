﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using System.IO;

namespace fecherFoundation
{
    public partial class FCDriveListBox : ListBox
    {
        #region Constructors
        public FCDriveListBox()
        {
            InitializeComponent();
            UpdateList();
			Drives();
		}
        #endregion

        #region Properties
        /// <summary>
        /// Gets/sets the Path for Files to List
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Drive
        {
            get
            {
                return this.SelectedItem.ToString();
            }

            set
            {
                this.SelectedItem = value;
            }
        }
        #endregion

        #region Private Methods
        private void UpdateList()
        {
            this.BeginUpdate();
            this.Items.Clear();
            //FC:TODO:BBE - correct application path
            //foreach (string drive in Directory.GetLogicalDrives())
            //{
            //    this.Items.Add(drive);
            //}
            this.EndUpdate();
        }
		private void Drives()
		{
			foreach (DriveInfo drive in DriveInfo.GetDrives())
			{
				this.Items.Add(drive);
			}
			this.Update();
		}
		#endregion
	}
}
