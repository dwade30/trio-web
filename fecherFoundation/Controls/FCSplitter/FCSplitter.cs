using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using fecherFoundation.Extensions;
using Encoder = System.Text.Encoder;
using System;

namespace fecherFoundation
{
    #region Splitter enums

    public enum Constants_Alignment
    {

        /// <summary>
        /// 
        /// </summary>
        ssBottomOfCaption,

        /// <summary>
        /// 
        /// </summary>
        ssCenterBottom,

        /// <summary>
        /// 
        /// </summary>
        ssCenterMiddle,

        /// <summary>
        /// 
        /// </summary>
        ssCenterTop,

        /// <summary>
        /// 
        /// </summary>
        ssLeftBottom,

        /// <summary>
        /// 
        /// </summary>
        ssLeftMiddle,

        /// <summary>
        /// 
        /// </summary>
        ssLeftOfCaption,

        /// <summary>
        /// 
        /// </summary>
        ssLeftTop,

        /// <summary>
        /// 
        /// </summary>
        ssRightBottom,

        /// <summary>
        /// 
        /// </summary>
        ssRightMiddle,

        /// <summary>
        /// 
        /// </summary>
        ssRightOfCaption,

        /// <summary>
        /// 
        /// </summary>
        ssRightTop,

        /// <summary>
        /// 
        /// </summary>
        ssTopOfCaption
    }

    public enum Constants_Bevel
    {

        /// <summary>
        /// 
        /// </summary>
        ssInsetBevel,

        /// <summary>
        /// 
        /// </summary>
        ssNoneBevel,

        /// <summary>
        /// 
        /// </summary>
        ssRaisedBevel
    }


    public enum SplitterOrientation
    {
        Horizontal = 1,
        Vertical = 3
    }

    public enum Constants_AutoSize
    {

        /// <summary>
        /// 
        /// </summary>
        ssAutoSizeFillContainer,

        /// <summary>
        /// 
        /// </summary>
        ssAutoSizeNone
    }

    public enum Constants_BorderStyle
    {

        /// <summary>
        /// 
        /// </summary>
        ssBorderStyleFixedSingle,

        /// <summary>
        /// 
        /// </summary>
        ssBorderStyleInset,

        /// <summary>
        /// 
        /// </summary>
        ssBorderStyleNone,

        /// <summary>
        /// 
        /// </summary>
        ssBorderStyleRaised
    }

    public enum Constants_SplitterBarAppearance
    {

        /// <summary>
        /// 
        /// </summary>
        ssSplitterBar3D,

        /// <summary>
        /// 
        /// </summary>
        ssSplitterBarBorderless,

        /// <summary>
        /// 
        /// </summary>
        ssSplitterBarFlat
    }

    public enum Constants_SplitterBarJoinStyle
    {

        /// <summary>
        /// 
        /// </summary>
        ssJoinContinuous,

        /// <summary>
        /// 
        /// </summary>
        ssJoinSegmented
    }

    public enum Constants_SplitterResizeStyle
    {

        /// <summary>
        /// 
        /// </summary>
        ssResizeNonProportional,

        /// <summary>
        /// 
        /// </summary>
        ssResizeProportional
    }

    public enum SplitterBarStyle
    {
        Horizontal = 0,
        Vertical = 1
    }

    #endregion

    public partial class FCSplitter : Panel
    {
        #region Members
        private Panes panes;
        #endregion

        #region Properties

        [Browsable(false),
            DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Panes Panes
        {
            get
            {
                return panes;
            }
        }

        public int PanesCount
        {
            get
            {
                return this.panes.Count;
            }
        }

        public Constants_SplitterBarJoinStyle SplitterBarJoinStyle { get; set; }

        public Constants_SplitterBarAppearance SplitterBarAppearance { get; set; }

        public Constants_SplitterResizeStyle SplitterResizeStyle { get; set; }

        public new Constants_BorderStyle BorderStyle { get; set; }

        public int SplitterBarWidth { get; set; }
 
        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _ExtentX { get; set; }

        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _ExtentY { get; set; }

        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _Version { get; set; }

        public object PaneTree { get; set; }

        #endregion

        #region Constructor
        public FCSplitter() : base()
        {
            InitializeComponent();
            base.AutoSize = false;
            panes = new Panes(this);
        }
        #endregion
    }
}
