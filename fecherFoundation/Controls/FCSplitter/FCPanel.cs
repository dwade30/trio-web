using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace fecherFoundation
{
    /// <summary>
    /// 
    /// </summary>

    //[Designer("System.Windows.Forms.Design.ParentControlDesigner, System.Design", typeof(IDesigner))]
    public class FCPanel : Panel
    {
        #region Members
        private FCSplitter parentSplitter;
        private FCControlCollection controlCollection;
        private object containedControl;
        private Image picture = null;
        private int pictureFrames = 0;
        private string identifier = "";
        #endregion

        #region Constructor
        public FCPanel()
        {
            this.controlCollection = new FCControlCollection(this);

            this.ParentChanged += new EventHandler(SSPanel_ParentChanged);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public uint PictureMaskColor
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public int PictureAnimationDelay
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public int PictureFrames
        {
            get
            {
                return pictureFrames;
            }
            set
            {
                pictureFrames = value;
                SetPicture();
            }
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public bool PictureUseMask
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public Image Picture
        {
            get
            {
                return picture; 
            }
            set
            {
                picture = value;
                SetPicture();
            }
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public Constants_Bevel BevelOuter
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public Constants_Alignment PictureAlignment
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the .
        /// </summary>
        /// <value>The .</value>
        public bool RoundedCorners
        {
            get;
            set;
        }

        [Browsable(false),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        internal FCSplitter ParentSplitter
        {
            get { return parentSplitter; }
            set { parentSplitter = value; }
        }

        /// <summary>
        /// When setting the control to the pane, if the object is a string, search for it in the parent form controls list and add it to the pane controls collection
        /// If the object is a control, then add it to the panes control collection
        /// After adding the control to the panes control collection, set the docking to fill
        /// </summary>
        public object ContainedControl
        {
            get { return containedControl; }

            set
            {
                containedControl = value;
                if (containedControl == null)
                {
                    return;
                }
                Control control = null;
                if (containedControl is string)
                {
                    control = this.parentSplitter.FindForm().Controls[containedControl.ToString()];
                }
                else if (containedControl is Control)
                {
                    control = containedControl as Control;
                }

                base.Controls.Add(control);
                this.Size = control.Size;
                control.Dock = DockStyle.Fill;
            }
        }

        public bool LockHeight { get; set; }

        public bool LockWidth { get; set; }

        [Browsable(false),
         DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Splitter Splitter { get; set; }

        [DefaultValue(-1)]
        public int Index
        {
            get;
            set;
        }

        public new FCControlCollection Controls
        {
            get
            {
                return controlCollection;
            }
        }

        /// <summary>
        /// Set Height of Panel parent to the same Top value as FCPanel
        /// In VB6 a splitter contains panels grouped by the splitter bar, when setting the height of one panel
        /// the entire group of panels changes height. In .NET the FCPanels are grouped by a parent Panel
        /// We need to set the height of the parent Panel, if existing, to the height set to one of the FCPanels
        /// Because the FCPanels are all docked, the height changing of the parent Panel will affect all of the child FCPanels
        /// </summary>
        public new int Height
        {
            get
            {
                return base.Height;
            }
            set
            {
                //Set Height of Panel parent to the same height as FCPanel
                Panel parent = GetOutmostParent();
                if (parent != null)
                {
                    parent.Height = value;
                }
                base.Height = value;
            }
        }

        /// <summary>
        /// Set Top of Panel parent to the same Top value as FCPanel
        /// In VB6 a splitter contains panels grouped by the splitter bar, when setting the Top of one panel
        /// the entire group of panels changes the top position. In .NET the FCanels are grouped by a parent Panel
        /// We need to set the Top of the parent Panel, if existing, to the top set to one of the FCPanels
        /// Because the FCPanels are all docked, the top changing of the parent Panel will affect all of the child FCPanels
        /// </summary>
        public new int Top
        {
            get
            {
                Panel parent = GetOutmostParent();
                if (parent != null)
                {
                    return parent.Top;
                }

                return base.Top;
            }
            set
            {
                Panel parent = GetOutmostParent();
                if (parent != null)
                {
                    parent.Top = value;
                }
            }
        }

        //In vb6 a FCPanel is identified in the spliters Pane collection by its name. 
        //In .NET the name of the panel is the same as the object name (ex FCPanel pane1 = new FCPanel();, pane1.Name will be pane1
        //Use a different property to store the identifier
        public string Identifier
        {
            get
            {
                return this.identifier;
            }
            set
            {
                this.identifier = value; 
            }
        }
        #endregion

        #region Private methods
        private void SetPicture()
        {
            if (this.picture != null && this.pictureFrames > 1)
            {
                PictureBox picBox = new PictureBox();
                picBox.Image = this.picture;
                this.Controls.Add(picBox);
                picBox.Dock = DockStyle.Fill;
            }
            else if (this.picture != null)
            {
                this.Controls.Clear();
                this.BackgroundImage = this.picture;
            }
        }

        /// <summary>
        /// General method of getting the outmost parent of type Panel of the current FCPanel
        /// 
        /// In VB6 a splitter contains panels grouped by the splitter bar, when setting the Height or Top of one panel
        /// the entire group of panels changes the top or height properties. In .NET the panels are grouped in a parent Panel
        /// We need to set the Top or Height of the parent Panel, if existing, to the top or height set to one of the panels
        /// Because the panels are all docked, the top or height changing of the parent Panel will affect all of the child panels
        /// </summary>
        /// <returns>Parent Panel, if existing, of the current FCPanel</returns>
        private Panel GetOutmostParent()
        {
            Panel parent = this.Parent as Panel;
            while (parent != null && parent.Parent is Panel)
            {
                parent = parent.Parent as Panel;
            }

            return parent;
        }

        #endregion

        #region Private events
        private void SSPanel_ParentChanged(object sender, EventArgs e)
        {
            if (this.Parent != null)
            {
                Control parent = this.Parent;
                while (parent != null && !(parent is FCSplitter))
                {
                    parent = parent.Parent;
                }


                if (parent != null)
                {
                    //parent is FCSplitter
                    this.ParentSplitter = ((FCSplitter)parent);
                    if (!this.parentSplitter.Panes.Contains((this)))
                    {
                        this.parentSplitter.Panes.Add(this);
                    }
                }
            }
        }
        #endregion

    }
}