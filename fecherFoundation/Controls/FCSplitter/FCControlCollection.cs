using System.Collections.Generic;
using System.Windows.Forms;

namespace fecherFoundation
{
    public class FCControlCollection : List<Control>
    {
        #region Members
        private Control owner;
        #endregion

        #region Constructor
        public FCControlCollection(Control owner)
        {
            this.owner = owner;
        }
        #endregion

        #region Public methods
        public new void Add(Control value)
        {
            base.Add(value);
            FCPanel panelOwner = this.owner as FCPanel;
            if (panelOwner != null)
            {
                panelOwner.ContainedControl = value;
            }
        }
        #endregion
    }
}