using System.Collections.Generic;

namespace fecherFoundation
{
    public class Panes : List<FCPanel>
    {
        #region Members
        private FCSplitter parent;
        #endregion

        #region Constructor
        public Panes(FCSplitter splitter)
        {
            this.parent = splitter;
        }
        #endregion

        #region Properties

        public FCPanel this[string name]
        {
            get
            {
                foreach (FCPanel pane in this)
                {
                    if (pane.Identifier == name || pane.Name == name)
                    {
                        return pane;
                    }
                }

                return null;
            }
        }

        public new FCPanel this[int index]
        {
            get
            {
                foreach (FCPanel pane in this)
                {
                    if (pane.Index == index)
                    {
                        return pane;
                    }
                }

                return base[index];
            }
        }
        #endregion

        #region Public methods
        public new void Clear()
        {
            base.Clear();
        }
        #endregion

    }
}