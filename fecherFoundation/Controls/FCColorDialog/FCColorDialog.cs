﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;

namespace fecherFoundation
{
    public class FCColorDialog : Wisej.Web.ColorDialog
    {
        protected override Wisej.Web.Form CreateUI()
        {
            Form colorDialogUI = base.CreateUI();

            colorDialogUI.Controls["colorGroupBox"].Controls["hexField"].AppearanceKey = "standardTextBox";
            colorDialogUI.Controls["colorGroupBox"].Controls["blueField"].AppearanceKey = "standardTextBox";
            colorDialogUI.Controls["colorGroupBox"].Controls["greenField"].AppearanceKey = "standardTextBox";
            colorDialogUI.Controls["colorGroupBox"].Controls["redField"].AppearanceKey = "standardTextBox";
            colorDialogUI.Controls["colorGroupBox"].Controls["brightnessField"].AppearanceKey = "standardTextBox";
            colorDialogUI.Controls["colorGroupBox"].Controls["saturationField"].AppearanceKey = "standardTextBox";
            colorDialogUI.Controls["colorGroupBox"].Controls["hueField"].AppearanceKey = "standardTextBox";

            colorDialogUI.Controls["colorGroupBox"].Controls["hexField"].Height = 22;
            colorDialogUI.Controls["colorGroupBox"].Controls["blueField"].Height = 22;
            colorDialogUI.Controls["colorGroupBox"].Controls["greenField"].Height = 22;
            colorDialogUI.Controls["colorGroupBox"].Controls["redField"].Height = 22;
            colorDialogUI.Controls["colorGroupBox"].Controls["brightnessField"].Height = 22;
            colorDialogUI.Controls["colorGroupBox"].Controls["saturationField"].Height = 22;
            colorDialogUI.Controls["colorGroupBox"].Controls["hueField"].Height = 22;

            return colorDialogUI;
        }
    }
}
