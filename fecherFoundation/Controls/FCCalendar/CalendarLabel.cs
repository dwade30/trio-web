﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation
{
    public class CalendarLabel
    {
        private int labelId;
        private Color color;
        private string name;

        public CalendarLabel()
        {
        }

        public int LabelID
        {
            get
            {
                return this.labelId;
            }
            set
            {
                this.labelId = value;
            }
        }

        public Color Color
        {
            get
            {
                return this.color;
            }
            set
            {
                this.color = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
        
    }
}
