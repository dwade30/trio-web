﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;
using Wisej.Web.Ext.FullCalendar;

namespace fecherFoundation
{
    public class FCCalendarControl : UserControl
    {
        private Wisej.Web.Ext.FullCalendar.FullCalendar fullCalendar;
        private Button prevButton;
        private Button nextButton;
        private Button switchDay;
        private Button switchWeek;
        private Button switchMonth;
        private Label currentPeriodLabel;
        private List<CalendarLabel> labelList = new List<CalendarLabel>();
		private DateTime beginSelection, endSelection;
		private bool allDay = false;

        //
        // Summary:
        //     Triggered when the user clicks an event.
        [Description("Triggered when the user clicks an event.")]
        public event EventClickEventHandler EventClick;

        //
        // Summary:
        //     Triggered when the user double clicks an event.
        [Description("Triggered when the user double clicks an event.")]
        public event EventClickEventHandler EventDoubleClick;

        //
        // Summary:
        //     Triggered when the user double clicks a day in the calendar.
        [Description("Triggered when the user double clicks a day in the calendar.")]
        public event DayClickEventHandler DayDoubleClick;

        private void InitializeComponent()
        {
            this.fullCalendar = new Wisej.Web.Ext.FullCalendar.FullCalendar();
            this.prevButton = new Wisej.Web.Button();
            this.nextButton = new Wisej.Web.Button();
            this.currentPeriodLabel = new Wisej.Web.Label();
            this.switchDay = new Wisej.Web.Button();
            this.switchWeek = new Wisej.Web.Button();
            this.switchMonth = new Wisej.Web.Button();
            this.SuspendLayout();
            // 
            // fullCalendar
            // 
            this.fullCalendar.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fullCalendar.Location = new System.Drawing.Point(3, 80);
            this.fullCalendar.Name = "fullCalendar";
            this.fullCalendar.Size = new System.Drawing.Size(780, 431);
            this.fullCalendar.TabIndex = 0;
            this.fullCalendar.Text = "fullCalendar";
            this.fullCalendar.DayDoubleClick += new Wisej.Web.Ext.FullCalendar.DayClickEventHandler(this.fullCalendar_DayDoubleClick);
            this.fullCalendar.EventDoubleClick += new Wisej.Web.Ext.FullCalendar.EventClickEventHandler(this.fullCalendar_EventDoubleClick);
            this.fullCalendar.EventClick += new Wisej.Web.Ext.FullCalendar.EventClickEventHandler(this.fullCalendar_EventClick);
            this.fullCalendar.CurrentDateChanged += new System.EventHandler(this.fullCalendar1_CurrentDateChanged);
			this.fullCalendar.DayClick += fullCalendar_DayClick;
            // 
            // prevButton
            // 
            this.prevButton.ImageSource = "scrollbar-arrow-left";
            this.prevButton.Location = new System.Drawing.Point(5, 45);
            this.prevButton.Name = "prevButton";
            this.prevButton.Size = new System.Drawing.Size(30, 30);
            this.prevButton.TabIndex = 1;
            this.prevButton.TextImageRelation = Wisej.Web.TextImageRelation.Overlay;
            this.prevButton.Click += new System.EventHandler(this.prevButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.ImageSource = "scrollbar-arrow-right";
            this.nextButton.Location = new System.Drawing.Point(41, 45);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(30, 30);
            this.nextButton.TabIndex = 2;
            this.nextButton.TextImageRelation = Wisej.Web.TextImageRelation.Overlay;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // currentPeriodLabel
            // 
            this.currentPeriodLabel.AutoSize = true;
            this.currentPeriodLabel.Location = new System.Drawing.Point(77, 52);
            this.currentPeriodLabel.Name = "currentPeriodLabel";
            this.currentPeriodLabel.Size = new System.Drawing.Size(4, 14);
            this.currentPeriodLabel.TabIndex = 3;
            // 
            // switchDay
            // 
            this.switchDay.Location = new System.Drawing.Point(5, 12);
            this.switchDay.Name = "switchDay";
            this.switchDay.Size = new System.Drawing.Size(76, 27);
            this.switchDay.TabIndex = 4;
            this.switchDay.Text = "Day";
            this.switchDay.Click += new System.EventHandler(this.switchDay_Click);
            // 
            // switchWeek
            // 
            this.switchWeek.Location = new System.Drawing.Point(87, 12);
            this.switchWeek.Name = "switchWeek";
            this.switchWeek.Size = new System.Drawing.Size(76, 27);
            this.switchWeek.TabIndex = 5;
            this.switchWeek.Text = "Week";
            this.switchWeek.Click += new System.EventHandler(this.switchWeek_Click);
            // 
            // switchMonth
            // 
            this.switchMonth.Location = new System.Drawing.Point(169, 12);
            this.switchMonth.Name = "switchMonth";
            this.switchMonth.Size = new System.Drawing.Size(76, 27);
            this.switchMonth.TabIndex = 6;
            this.switchMonth.Text = "Month";
            this.switchMonth.Click += new System.EventHandler(this.switchMonth_Click);
            // 
            // FCCalendarControl
            // 
            this.Controls.Add(this.switchMonth);
            this.Controls.Add(this.switchWeek);
            this.Controls.Add(this.switchDay);
            this.Controls.Add(this.currentPeriodLabel);
            this.Controls.Add(this.nextButton);
            this.Controls.Add(this.prevButton);
            this.Controls.Add(this.fullCalendar);
            this.Name = "FCCalendarControl";
            this.Size = new System.Drawing.Size(786, 514);
            this.Load += new System.EventHandler(this.FCCalendarControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        /// <summary>
        /// Prints the control on client side
        /// </summary>
        public void PrintCalendar()
        {
            var controlId = "id_" + this.Handle.ToString();
            Eval("PrintDiv('" + controlId + "')");
        }

        private void fullCalendar_DayClick(object sender, DayClickEventArgs e)
		{
			//FC:FINAL:DSE #1904  Set current date on day-click
			this.fullCalendar.CurrentDate = e.Day;
		}

		public FCCalendarControl()
        {
            InitializeComponent();

            labelList.Add(new CalendarLabel() { LabelID = 0, Color = ColorTranslator.FromOle(16777215), Name = "None" });
            labelList.Add(new CalendarLabel() { LabelID = 1, Color = ColorTranslator.FromOle(8688895), Name = "Important" });
            labelList.Add(new CalendarLabel() { LabelID = 2, Color = ColorTranslator.FromOle(15178884), Name = "Business" });
            labelList.Add(new CalendarLabel() { LabelID = 3, Color = ColorTranslator.FromOle(6545061), Name = "Personal" });
            labelList.Add(new CalendarLabel() { LabelID = 4, Color = ColorTranslator.FromOle(14084071), Name = "Vacation" });
            labelList.Add(new CalendarLabel() { LabelID = 5, Color = ColorTranslator.FromOle(7583231), Name = "Must Attend" });
            labelList.Add(new CalendarLabel() { LabelID = 6, Color = ColorTranslator.FromOle(16248708), Name = "Travel Required" });
            labelList.Add(new CalendarLabel() { LabelID = 7, Color = ColorTranslator.FromOle(8703702), Name = "Needs Preparation" });
            labelList.Add(new CalendarLabel() { LabelID = 8, Color = ColorTranslator.FromOle(16229830), Name = "Birthday" });
            labelList.Add(new CalendarLabel() { LabelID = 9, Color = ColorTranslator.FromOle(13029029), Name = "Anniversary" });
            labelList.Add(new CalendarLabel() { LabelID = 10, Color = ColorTranslator.FromOle(7596031), Name = "Phone Call" });

			beginSelection = DateTime.FromOADate(0);
			endSelection = DateTime.FromOADate(0);
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public EventCollection Events
        {
            get
            {
                return fullCalendar.Events;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public List<CalendarLabel> LabelList
        {
            get
            {
                return this.labelList;
            }
        }

        private void switchDay_Click(object sender, EventArgs e)
        {
            fullCalendar.View = Wisej.Web.Ext.FullCalendar.ViewType.AgendaDay;
        }

        private void switchWeek_Click(object sender, EventArgs e)
        {
            fullCalendar.View = Wisej.Web.Ext.FullCalendar.ViewType.AgendaWeek;
        }

        private void switchMonth_Click(object sender, EventArgs e)
        {
            fullCalendar.View = Wisej.Web.Ext.FullCalendar.ViewType.Month;
        }

        private void prevButton_Click(object sender, EventArgs e)
        {
            fullCalendar.Previous();
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            fullCalendar.Next();
        }

        private void fullCalendar1_CurrentDateChanged(object sender, EventArgs e)
        {
            FormatHeader();
        }

        private void FormatHeader()
        {   
            switch (fullCalendar.View)
            {
                case Wisej.Web.Ext.FullCalendar.ViewType.AgendaDay:
                    currentPeriodLabel.Text = fullCalendar.CurrentDate.ToString("MMMM dd, yyyy");
                    break;
                case Wisej.Web.Ext.FullCalendar.ViewType.AgendaWeek:
                    DateTime startOfWeek = FirstDayOfWeek(fullCalendar.CurrentDate);
                    DateTime endOfWeek = LastDayOfWeek(fullCalendar.CurrentDate);
                    if (startOfWeek.Year == endOfWeek.Year)
                    {
                        if (startOfWeek.Month == endOfWeek.Month)
                        {
                            currentPeriodLabel.Text = startOfWeek.ToString("MMMM dd") + " - " + endOfWeek.ToString("dd, yyyy");
                        }
                        else
                        {
                            currentPeriodLabel.Text = startOfWeek.ToString("MMMM dd") + " - " + endOfWeek.ToString("MMMM dd, yyyy");
                        }
                    }
                    else
                    {
                        currentPeriodLabel.Text = startOfWeek.ToString("MMMM dd, yyyy") + " - " + endOfWeek.ToString("MMMM dd, yyyy");
                    }
                    break;
                case Wisej.Web.Ext.FullCalendar.ViewType.Month:
                    currentPeriodLabel.Text = fullCalendar.CurrentDate.ToString("MMMM, yyyy");
                    break;
            }
        }

		public void GetSelection( ref DateTime BeginSelection, ref DateTime EndSelection, ref bool AllDay)
		{
			BeginSelection = beginSelection;
			EndSelection = endSelection;
			AllDay = allDay;
		}

        public static DateTime FirstDayOfWeek(DateTime dt)
        {
            var culture = System.Threading.Thread.CurrentThread.CurrentCulture;
            var diff = dt.DayOfWeek - culture.DateTimeFormat.FirstDayOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-diff).Date;
        }

        public static DateTime LastDayOfWeek(DateTime dt)
        {
            return FirstDayOfWeek(dt).AddDays(6);
        }

        private void FCCalendarControl_Load(object sender, EventArgs e)
        {
            FormatHeader();
        }

        private void fullCalendar_EventDoubleClick(object sender, Wisej.Web.Ext.FullCalendar.EventClickEventArgs e)
        {
            if (EventDoubleClick != null)
            {
                EventDoubleClick(sender, e);
            }
        }


        private void fullCalendar_EventClick(object sender, EventClickEventArgs e)
        {
            if (EventClick != null)
            {
                EventClick(sender, e);
            }
        }

        private void fullCalendar_DayDoubleClick(object sender, DayClickEventArgs e)
        {
			beginSelection = e.Day;
			//FC:FINAL:DSE When double click on a date, VB6 set end selection as next day
			endSelection = e.Day.AddDays(1);
			allDay = true;
            if (DayDoubleClick != null)
            {
                DayDoubleClick(sender, e);
            }
        }
    }
}
