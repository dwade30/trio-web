﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace fecherFoundation
{
    public class CalendarEventCustomProperties
    {
        XmlDocument doc = new XmlDocument();

        public CalendarEventCustomProperties()
        {
            this.doc = new XmlDocument();
            this.LoadDefaultDoc();
        }

        public string this[string key]
        {
            get
            {
                XmlNode node = this.doc.SelectSingleNode("Calendar/CustomProperties/CustomProperty[@Name='" + key.ToLower() + "']");
                if (node != null && node.Attributes["Value"] != null)
                {
                    return node.Attributes["Value"].Value;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                XmlNode node = this.doc.SelectSingleNode("Calendar/CustomProperties/CustomProperty[@Name='" + key + "']");
                if (node != null && node.Attributes["Value"] != null)
                {
                    node.Attributes["Value"].Value = Convert.ToString(value);
                }
                else
                {
                    XmlNode newNode = this.doc.CreateElement("CustomProperty");

                    XmlAttribute nameAttribute = this.doc.CreateAttribute("Name");
                    nameAttribute.Value = key.ToLower();
                    XmlAttribute valueAttribute = this.doc.CreateAttribute("Value");
                    valueAttribute.Value = Convert.ToString(value);
                    XmlAttribute varianTypeAttribute = this.doc.CreateAttribute("VarianType");
                    varianTypeAttribute.Value = "8";
                    newNode.Attributes.Append(nameAttribute);
                    newNode.Attributes.Append(valueAttribute);
                    newNode.Attributes.Append(varianTypeAttribute);

                    XmlNode parentNode = this.doc.SelectSingleNode("Calendar/CustomProperties");
                    parentNode.AppendChild(newNode);
                }
            }
        }

        public void LoadFromString(string strData)
        {
            try
            {
                this.doc.LoadXml(strData);
                XmlNode parentNode = this.doc.SelectSingleNode("Calendar/CustomProperties");
                if (parentNode == null)
                {
                    this.LoadDefaultDoc();
                }
            }
            catch (Exception ex)
            {
                this.LoadDefaultDoc();
            }
        }

        public string SaveToString()
        {
            return doc.InnerXml;
        }

        private void LoadDefaultDoc()
        {
            doc.LoadXml("<Calendar CompactMode=\"1\"><CustomProperties></CustomProperties></Calendar>");
        }
    }
}
