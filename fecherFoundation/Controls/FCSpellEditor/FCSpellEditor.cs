﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;

namespace fecherFoundation
{
    #region SpellEditor enums
    public enum eBorderStyles
    {

        /// <summary>
        /// 
        /// </summary>
        se3DBorder,

        /// <summary>
        /// 
        /// </summary>
        seNoBorder
    }

    public enum eDoneAction
    {

        /// <summary>
        /// 
        /// </summary>
        seDoNothing,

        /// <summary>
        /// 
        /// </summary>
        seFireDoneEvent,

        /// <summary>
        /// 
        /// </summary>
        seShowMsgBox
    }

    public enum eDragDropBehaviorTypes
    {

        /// <summary>
        /// 
        /// </summary>
        seFireDropEvent,

        /// <summary>
        /// 
        /// </summary>
        seInsertFileAsIcon,

        /// <summary>
        /// 
        /// </summary>
        seInsertFileContents,

        /// <summary>
        /// 
        /// </summary>
        seNoDragDropAllowed,

        /// <summary>
        /// 
        /// </summary>
        seOpenFileContents
    }

    public enum RicherTextBoxToolStripGroups
    {
        SaveAndLoad = 0x1,
        FontNameAndSize = 0x2,
        BoldUnderlineItalic = 0x4,
        Alignment = 0x8,
        FontColor = 0x10,
        IndentationAndBullets = 0x20,
        Insert = 0x40,
        Zoom = 0x80
    }

    #endregion

    public partial class FCSpellEditor : UserControl
    {
        #region Events
        public delegate void OnToolbuttonClick(object sender, ToolStripItemClickedEventArgs e);
        public event OnToolbuttonClick ToolbuttonClick;
        public delegate void OnLinkClicked(object sender, LinkClickedEventArgs e);
        public event OnLinkClicked LinkClicked;
        #endregion

        #region Members
        private Dictionary<string, string> autoTypePair = new Dictionary<string, string>();

        // List of separators
        private List<char> separators = new List<char>() { ' ', '\t', '\n', '-', '_', ';', ':', '\'', '"', ',', '.', '<', '>', '/', '?' };

        // Layout dictionary
        private Dictionary<string, ToolStripButton> layout = new Dictionary<string, ToolStripButton>();

        private string toolbarLayout = "";
        private bool changes = false;
        #endregion

        #region Construction and initial loading
        public FCSpellEditor()
        {
            InitializeComponent();
            this.toolStripFindReplace.Visible = false;
            this.rtbDocument.DragDrop += new DragEventHandler(DragDropRichTextBox_DragDrop);

            // Initialize the layout dictionary
            layout.Add("new", this.tsbtnNew);
            layout.Add("print", this.tsbtnPrint);
            layout.Add("cut", this.tsbtnCut);
            layout.Add("copy", this.tsbtnCopy);
            layout.Add("paste", this.tsbtnPaste);
            layout.Add("undo", this.tsbtnUndo);
            layout.Add("redo", this.tsbtnRedo);
            layout.Add("indentLeft", this.tsbtnIndent);
            layout.Add("indentRight", this.tsbtnOutdent);
            layout.Add("find", this.tsbtnFind);
        }
        #endregion

        #region Settings
        private int indent = 10;
        [Category("Settings")]
        [Description("Value indicating the number of characters used for indentation")]
        public int INDENT
        {
            get { return indent; }
            set { indent = value; }
        }
        #endregion

        #region Properties
        [Obsolete(".NET does not support the PassiveChecking property. Added for compatibility")]
        public bool PassiveChecking { get; set; }

        /// <summary>
        /// Gets/sets the selection font name
        /// </summary>
        public string SelFontName
        {
            get
            {
                if (this.tscmbFont.SelectedItem != null)
                {
                    return this.tscmbFont.SelectedItem.ToString();
                }
                return "";
            }
            set
            {
                this.tscmbFont.SelectedItem = value;
            }
        }

        /// <summary>
        /// Gets/sets the selection font size
        /// </summary>
        public float SelFontSize
        {
            get
            {
                if (this.tscmbFontSize.SelectedItem != null)
                {
                    return Convert.ToSingle(this.tscmbFontSize.SelectedItem.ToString());
                }
                return 0;
            }
            set
            {
                this.tscmbFontSize.SelectedItem = value.ToString();
            }
        }

        /// <summary>
        /// Gets/sets if the selection will be displayed with bold font
        /// </summary>
        public bool SelBold
        {
            get
            {
                return this.tsbtnBold.Checked;
            }
            set
            {
                this.tsbtnBold.Checked = value;
            }
        }

        /// <summary>
        /// Gets/sets if the selection is displayed with underline
        /// </summary>
        public bool SelUnderline
        {
            get
            {
                return this.tsbtnUnderline.Checked;
            }
            set
            {
                this.tsbtnUnderline.Checked = value;
            }
        }

        /// <summary>
        /// Gets/sets if the selection is displayed italic 
        /// </summary>
        public bool SelItalic
        {
            get
            {
                return this.tsbtnItalic.Checked;
            }
            set
            {
                this.tsbtnItalic.Checked = value;
            }
        }

        /// <summary>
        /// Gets/sets if the selection will be displayed with bullets
        /// </summary>
        public bool SelBullet
        {
            get
            {
                return this.tsbtnBullets.Checked;
            }
            set
            {
                this.tsbtnBullets.Checked = value;
            }
        }

        /// <summary>
        /// Gets or sets the selection color
        /// </summary>
        /// <value>The selection color</value>
        public Color SelColor
        {
            get
            {
                return this.rtbDocument.SelectionColor;
            }
            set
            {
                this.rtbDocument.SelectionColor = value;
            }
        }

       

        /// <summary>
        /// Gets/sets the selected text and converts it to/from RTF
        /// </summary>
        /// <value>The .</value>
        public string SelTextRTF
        {
            get
            {
                return this.SelRTF;
            }
            set
            {
                this.SelRTF = value;
            }
        }

        /// <summary>
        /// Gets/sets the selected RTF and converts it to text
        /// </summary>
        /// <value>The .</value>
        [Category("Document data")]
        [Description("FCRicherTextBox content in rich-text format")]
        public string TextRTF
        {
            get { return rtbDocument.Rtf; }
            set { try { rtbDocument.Rtf = value; } catch (ArgumentException) { rtbDocument.Text = value; } }
        }

        [Obsolete(".NET does not support this property. Added for compatibility")]
        public bool CheckOnFocusLoss
        {
            get;
            set;
        }

        [Obsolete(".NET does not support this property. Added for compatibility")]
        public eDoneAction DoneAction
        {
            get;
            set;
        }

        [Obsolete(".NET does not support this property. Added for compatibility")]
        public bool AllowAddToCustom
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the drag/drop behavior of the control
        /// </summary>
        public eDragDropBehaviorTypes DragDropBehavior
        {
            get;
            set;
        }

        /// <summary>
        /// This specifies the output folder for any images when converting from RTF to HTML
        /// </summary>
        [Obsolete(".NET does not support this property. Added for compatibility")]
        public string HTMLImgDir
        {
            get;
            set;
        }

        /// <summary>
        /// Gets/sets the selection font
        /// </summary>
        public Font Font
        {
            get { return rtbDocument.Font; }
            set { rtbDocument.Font = value; }
        }

        /// <summary>
        /// Gets/sets if the selection will be highlighted
        /// </summary>
        public bool HideSelection
        {
            get
            {
                return this.rtbDocument.HideSelection;
            }
            set
            {
                this.rtbDocument.HideSelection = value;
            }
        }

        /// <summary>
        /// Gets/sets the visibility of the tool bar
        /// </summary>
        public bool ShowToolBar
        {
            get
            {
                return this.toolStripActions.Visible;
            }
            set
            {
                this.toolStripActions.Visible = value;
            }
        }

        /// <summary>
        /// Gets/sets the visibility of the font bar
        /// </summary>
        public bool ShowFontBar
        {
            get
            {
                return this.toolStrip1.Visible;
            }
            set
            {
                this.toolStrip1.Visible = value;
            }
        }

        /// <summary>
        /// Gets/sets the visibility of the ruler
        /// </summary>
        public bool ShowRuler
        {
            get;
            set;
        }

        /// <summary>
        /// Gets/sets the visibility of the status bar
        /// </summary>
        public bool ShowStatusBar
        {
            get;
            set;
        }

        /// <summary>
        /// Gets/sets the border style of the control
        /// </summary>
        public eBorderStyles BorderStyle
        {
            get;
            set;
        }

        /// <summary>
        /// Gets/sets if the current selection is protected
        /// </summary>
        public bool SelProtected
        {
            get
            {
                return this.rtbDocument.SelectionProtected;
            }
            set
            {
                this.rtbDocument.SelectionProtected = value;
            }
        }

        /// <summary>
        /// Gets/sets the selected RTF
        /// </summary>
        public string SelRTF
        {
            get 
            { 
                return rtbDocument.SelectedRtf; 
            }
            set
            {
                try
                {
                    rtbDocument.SelectedRtf = value;
                }
                catch (ArgumentException)
                {
                    rtbDocument.SelectedText = value;
                }
            }
        }

        /// <summary>
        /// Gets/sets if corrections should be suggested
        /// </summary>
        /// <value>The .</value>
        public bool SuggestCorrections
        {
            get;
            set;
        }

        /// <summary>
        /// Allows you to control whether the user can cancel out of the spell checking corrections dialog.
        /// </summary>
        /// <value>The .</value>
        public bool AllowCancel
        {
            get;
            set;
        }

        /// <summary>
        /// Indicates whether the control ignores the case of words when checking spelling.
        /// </summary>
        /// <value>The .</value>
        public bool IgnoreCase
        {
            get;
            set;
        }

        /// <summary>
        /// Gets/sets if the control should auto handle URLs or fire the LinkClicked event
        /// </summary>
        /// <value>The .</value>
        public bool AutoHandleURLs { get; set; }

        /// <summary>
        /// Gets/sets the height and width of the client area of the control.
        /// </summary>
        public new Size ClientSize
        {
            get
            {
                return base.ClientSize;
            }
            set
            {
                base.ClientSize = value;
            }
        }

        /// <summary>
        /// Gets/sets the coordinates of the upper-left corner of the control relative to the upper-left corner of its container
        /// </summary>
        public new Point Location
        {
            get { return base.Location; }
            set { base.Location = value; }
        }

        /// <summary>
        /// Gets/sets the name of the control
        /// </summary>
        public new string Name
        {
            get { return base.Name; }
            set { base.Name = value; }
        }

        /// <summary>
        /// Gets/sets the tab order of the control within its container
        /// </summary>
        public new int TabIndex
        {
            get { return base.TabIndex; }
            set { base.TabIndex = value; }
        }

        /// <summary>
        /// Gets/sets the foreground color of the control
        /// </summary>
        public new Color ForeColor
        {
            get { return base.ForeColor; }
            set { base.ForeColor = value; }
        }

        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _ExtentX { get; set; }
        [Obsolete("The property is a VB6 designer property used by the IDE to position/size the control in the designer view")]
        public int _ExtentY { get; set; }

        /// <summary>
        /// Gets/sets whether the control should handle spelling errors
        /// </summary>
        public int HandleSpellingErrors { get; set; }

        /// <summary>
        /// Gets/sets the color of the squiggle underlining the erroneous words
        /// </summary>
        public int SquiggleColor { get; set; }

        /// <summary>
        /// Gets/sets the location of the custom dictionary
        /// </summary>
        public string CustomDictionaryFName { get; set; }

        /// <summary>
        /// Gets/sets the caption displayed in the status bar
        /// </summary>
        public string StatusCaption { get; set; }

        /// <summary>
        /// Gets/sets the toolbar layout
        /// </summary>
        public string Toolbarlayout
        {
            get
            {
                return this.toolbarLayout;
            }
            set
            {
                this.toolbarLayout = value;
                // Create the layout only if the layout string is not empty. If it is empty, the layout should be the full layout
                if (!string.IsNullOrEmpty(this.toolbarLayout))
                {
                    this.toolStripActions.Items.Clear();
                    // Split the layout string into individual items
                    string[] layoutString = this.toolbarLayout.Split(' ');
                    ToolStripButton layoutItem;
                    foreach (string layoutObject in layoutString)
                    {
                        // Normalize the layout object
                        string normalizedLayoutObject = layoutObject.ToLower();
                        // Check to see if the layout dictionary contains the object
                        if (layout.ContainsKey(normalizedLayoutObject))
                        {
                            // If the layout dictionary contains the object, remove it from previous occurences in other strip
                            layoutItem = layout[normalizedLayoutObject];
                            if (this.toolStrip1.Items.Contains(layoutItem))
                            {
                                this.toolStrip1.Items.Remove(layoutItem);
                            }
                            
                            // Add it to the strip only if it is not already present
                            if(!this.toolStripActions.Items.Contains(layoutItem))
                            {
                                this.toolStripActions.Items.Add(layoutItem);
                            }
                        }
                        else if (normalizedLayoutObject == "bar")
                        {
                            // The separator needs to be created on the fly because more than one instance can be present
                            this.toolStripActions.Items.Add(new ToolStripSeparator());
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Gets/sets the fontbar layout
        /// </summary>
        public string Fontbarlayout { get; set; }

        [Obsolete(".NET does not support this property. Added for compatibility")]
        public bool ShowFileToolbuttons { get; set; }
        [Obsolete(".NET does not support this property. Added for compatibility")]
        public bool StatusDisplayItems { get; set; }
        #endregion

        #region Properties for toolstrip items visibility
        [Category("Toolstip items visibility")]
        public bool GroupSaveAndLoadVisible
        {
            get { return IsGroupVisible(RicherTextBoxToolStripGroups.SaveAndLoad); }
            set { HideToolstripItemsByGroup(RicherTextBoxToolStripGroups.SaveAndLoad, value); }
        }
        [Category("Toolstip items visibility")]
        public bool GroupFontNameAndSizeVisible
        {
            get { return IsGroupVisible(RicherTextBoxToolStripGroups.FontNameAndSize); }
            set { HideToolstripItemsByGroup(RicherTextBoxToolStripGroups.FontNameAndSize, value); }
        }
        [Category("Toolstip items visibility")]
        public bool GroupBoldUnderlineItalicVisible
        {
            get { return IsGroupVisible(RicherTextBoxToolStripGroups.BoldUnderlineItalic); }
            set { HideToolstripItemsByGroup(RicherTextBoxToolStripGroups.BoldUnderlineItalic, value); }
        }
        [Category("Toolstip items visibility")]
        public bool GroupAlignmentVisible
        {
            get { return IsGroupVisible(RicherTextBoxToolStripGroups.Alignment); }
            set { HideToolstripItemsByGroup(RicherTextBoxToolStripGroups.Alignment, value); }
        }
        [Category("Toolstip items visibility")]
        public bool GroupFontColorVisible
        {
            get { return IsGroupVisible(RicherTextBoxToolStripGroups.FontColor); }
            set { HideToolstripItemsByGroup(RicherTextBoxToolStripGroups.FontColor, value); }
        }
        [Category("Toolstip items visibility")]
        public bool GroupIndentationAndBulletsVisible
        {
            get { return IsGroupVisible(RicherTextBoxToolStripGroups.IndentationAndBullets); }
            set { HideToolstripItemsByGroup(RicherTextBoxToolStripGroups.IndentationAndBullets, value); }
        }
        [Category("Toolstip items visibility")]
        public bool GroupInsertVisible
        {
            get { return IsGroupVisible(RicherTextBoxToolStripGroups.Insert); }
            set { HideToolstripItemsByGroup(RicherTextBoxToolStripGroups.Insert, value); }
        }
        [Category("Toolstip items visibility")]
        public bool ToolStripVisible
        {
            get { return toolStrip1.Visible; }
            set { toolStrip1.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool SaveVisible
        {
            get { return tsbtnSave.Visible; }
            set { tsbtnSave.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool LoadVisible
        {
            get { return tsbtnOpen.Visible; }
            set { tsbtnOpen.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool SeparatorSaveLoadVisible
        {
            get { return toolStripSeparator6.Visible; }
            set { toolStripSeparator6.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool FontFamilyVisible
        {
            get { return tscmbFont.Visible; }
            set { tscmbFont.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool FontSizeVisible
        {
            get { return tscmbFontSize.Visible; }
            set { tscmbFontSize.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool ChooseFontVisible
        {
            get { return tsbtnChooseFont.Visible; }
            set { tsbtnChooseFont.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool SeparatorFontVisible
        {
            get { return toolStripSeparator1.Visible; }
            set { toolStripSeparator1.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool BoldVisible
        {
            get { return tsbtnBold.Visible; }
            set { tsbtnBold.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool ItalicVisible
        {
            get { return tsbtnItalic.Visible; }
            set { tsbtnItalic.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool UnderlineVisible
        {
            get { return tsbtnUnderline.Visible; }
            set { tsbtnUnderline.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool SeparatorBoldUnderlineItalicVisible
        {
            get { return toolStripSeparator2.Visible; }
            set { toolStripSeparator2.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool AlignLeftVisible
        {
            get { return tsbtnAlignLeft.Visible; }
            set { tsbtnAlignLeft.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool AlignRightVisible
        {
            get { return tsbtnAlignRight.Visible; }
            set { tsbtnAlignRight.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool AlignCenterVisible
        {
            get { return tsbtnAlignCenter.Visible; }
            set { tsbtnAlignCenter.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool SeparatorAlignVisible
        {
            get { return toolStripSeparator3.Visible; }
            set { toolStripSeparator3.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool FontColorVisible
        {
            get { return tsbtnFontColor.Visible; }
            set { tsbtnFontColor.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool WordWrapVisible
        {
            get { return tsbtnWordWrap.Visible; }
            set { tsbtnWordWrap.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool SeparatorFontColorVisible
        {
            get { return toolStripSeparator4.Visible; }
            set { toolStripSeparator4.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool IndentVisible
        {
            get { return tsbtnIndent.Visible; }
            set { tsbtnIndent.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool OutdentVisible
        {
            get { return tsbtnOutdent.Visible; }
            set { tsbtnOutdent.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool BulletsVisible
        {
            get { return tsbtnBullets.Visible; }
            set { tsbtnBullets.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool InsertPictureVisible
        {
            get { return tsbtnInsertPicture.Visible; }
            set { tsbtnInsertPicture.Visible = value; }
        }
        [Category("Toolstrip single items visibility")]
        public bool SeparatorInsertVisible
        {
            get { return toolStripSeparator7.Visible; }
            set { toolStripSeparator7.Visible = value; }
        }
        
        #endregion

        #region Data Properties
        /// <summary>
        /// Gets/sets the document data
        /// </summary>
        [Category("Document data")]
        [Description("FCRicherTextBox content in plain text")]
        [Browsable(true)]
        public override string Text
        {
            get { return rtbDocument.Text; }
            set { rtbDocument.Text = value; }
        }

        /// <summary>
        /// Gets/sets the selected data
        /// </summary>
        public string SelText
        {
            get { return rtbDocument.SelectedText; }
            set { rtbDocument.SelectedText = value; }
        }

        /// <summary>
        /// Gets/sets if the document is readonly
        /// </summary>
        public bool ReadOnly
        {
            get
            {
                return this.rtbDocument.ReadOnly;
            }
            set
            {
                this.rtbDocument.ReadOnly = value;
            }
        }

        /// <summary>
        /// Gets/sets the selection start location
        /// </summary>
        public int SelStart
        {
            get { return this.rtbDocument.SelectionStart; }
            set { this.rtbDocument.SelectionStart = value; }
        }

        /// <summary>
        /// Gets/sets the selection length
        /// </summary>
        public int SelLength
        {
            get { return this.rtbDocument.SelectionLength; }
            set { this.rtbDocument.SelectionLength = value; }
        }

        #endregion

        #region Event Handlers

        private void DragDropRichTextBox_DragDrop(object sender, DragEventArgs e)
        {
            if (DragDropBehavior == eDragDropBehaviorTypes.seInsertFileContents)
            {
                string[] fileName = e.Data.GetData("FileDrop") as string[];
                if (fileName != null)
                {
                    if (fileName != null && !string.IsNullOrWhiteSpace(fileName[0]))
                    {
                        rtbDocument.Clear();
                        rtbDocument.LoadFile(fileName[0], RichTextBoxStreamType.UnicodePlainText);
                    }
                }
            }
        }

        private void rtbDocument_KeyPress(object sender, KeyPressEventArgs e)
        {
            changes = true;
            if ( separators.Contains(e.KeyChar) )
            {
                ReplaceAutoTypePair();
            }
        }

        private void rtbDocument_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            if (!AutoHandleURLs && LinkClicked != null)
            {
                LinkClicked(this, e);
            }
        }

        private void RicherTextBox_Load(object sender, EventArgs e)
        {
            // load system fonts
            foreach (FontFamily family in FontFamily.Families)
            {
                tscmbFont.Items.Add(family.Name);
            }
            tscmbFont.SelectedItem = "Microsoft Sans Serif";

            tscmbFontSize.SelectedItem = "9";

            tstxtZoomFactor.Text = FCConvert.ToString(rtbDocument.ZoomFactor * 100);
            tsbtnWordWrap.Checked = rtbDocument.WordWrap;
        }

        private void tsbtnBIU_Click(object sender, EventArgs e)
        {
            // bold, italic, underline
            try
            {
                if (!(rtbDocument.SelectionFont == null))
                {
                    Font currentFont = rtbDocument.SelectionFont;
                    FontStyle newFontStyle = rtbDocument.SelectionFont.Style;
                    string txt = (sender as ToolStripButton).Name;
                    if (txt.IndexOf("Bold") >= 0)
                        newFontStyle = rtbDocument.SelectionFont.Style ^ FontStyle.Bold;
                    else if (txt.IndexOf("Italic") >= 0)
                        newFontStyle = rtbDocument.SelectionFont.Style ^ FontStyle.Italic;
                    else if (txt.IndexOf("Underline") >= 0)
                        newFontStyle = rtbDocument.SelectionFont.Style ^ FontStyle.Underline;
                    else if (txt.IndexOf("Strikeout") >= 0)
                        newFontStyle = rtbDocument.SelectionFont.Style ^ FontStyle.Strikeout;

                    rtbDocument.SelectionFont = new Font(currentFont.FontFamily, currentFont.Size, newFontStyle);
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }

        private void rtbDocument_SelectionChanged(object sender, EventArgs e)
        {
            if (rtbDocument.SelectionFont != null)
            {
                tsbtnBold.Checked = rtbDocument.SelectionFont.Bold;
                tsbtnItalic.Checked = rtbDocument.SelectionFont.Italic;
                tsbtnUnderline.Checked = rtbDocument.SelectionFont.Underline;

                boldToolStripMenuItem.Checked = rtbDocument.SelectionFont.Bold;
                italicToolStripMenuItem.Checked = rtbDocument.SelectionFont.Italic;
                underlineToolStripMenuItem.Checked = rtbDocument.SelectionFont.Underline;

                switch (rtbDocument.SelectionAlignment)
                {
                    case HorizontalAlignment.Left:
                        tsbtnAlignLeft.Checked = true;
                        tsbtnAlignCenter.Checked = false;
                        tsbtnAlignRight.Checked = false;

                        leftToolStripMenuItem.Checked = true;
                        centerToolStripMenuItem.Checked = false;
                        rightToolStripMenuItem.Checked = false;
                        break;

                    case HorizontalAlignment.Center:
                        tsbtnAlignLeft.Checked = false;
                        tsbtnAlignCenter.Checked = true;
                        tsbtnAlignRight.Checked = false;

                        leftToolStripMenuItem.Checked = false;
                        centerToolStripMenuItem.Checked = true;
                        rightToolStripMenuItem.Checked = false;
                        break;

                    case HorizontalAlignment.Right:
                        tsbtnAlignLeft.Checked = false;
                        tsbtnAlignCenter.Checked = false;
                        tsbtnAlignRight.Checked = true;

                        leftToolStripMenuItem.Checked = false;
                        centerToolStripMenuItem.Checked = false;
                        rightToolStripMenuItem.Checked = true;
                        break;
                }

                tsbtnBullets.Checked = rtbDocument.SelectionBullet;
                bulletsToolStripMenuItem.Checked = rtbDocument.SelectionBullet;

                tscmbFont.SelectedItem = rtbDocument.SelectionFont.FontFamily.Name;
                tscmbFontSize.SelectedItem = rtbDocument.SelectionFont.Size.ToString();
            }
        }

        private void tsbtnAlignment_Click(object sender, EventArgs e)
        {
            // alignment: left, center, right
            try
            {
                string txt = (sender as ToolStripButton).Name;
                if (txt.IndexOf("Left") >= 0)
                {
                    rtbDocument.SelectionAlignment = HorizontalAlignment.Left;
                    tsbtnAlignLeft.Checked = true;
                    tsbtnAlignCenter.Checked = false;
                    tsbtnAlignRight.Checked = false;
                }
                else if (txt.IndexOf("Center") >= 0)
                {
                    rtbDocument.SelectionAlignment = HorizontalAlignment.Center;
                    tsbtnAlignLeft.Checked = false;
                    tsbtnAlignCenter.Checked = true;
                    tsbtnAlignRight.Checked = false;
                }
                else if (txt.IndexOf("Right") >= 0)
                {
                    rtbDocument.SelectionAlignment = HorizontalAlignment.Right;
                    tsbtnAlignLeft.Checked = false;
                    tsbtnAlignCenter.Checked = false;
                    tsbtnAlignRight.Checked = true;
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }

        private void tsbtnFontColor_Click(object sender, EventArgs e)
        {
            // font color
            try
            {
                using (ColorDialog dlg = new ColorDialog())
                {
                    dlg.Color = rtbDocument.SelectionColor;
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        rtbDocument.SelectionColor = dlg.Color;
                    }
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }

        private void tsbtnBulletsAndNumbering_Click(object sender, EventArgs e)
        {
            // bullets, indentation
            try
            {
                string name = (sender as ToolStripButton).Name;
                if (name.IndexOf("Bullets") >= 0)
                    rtbDocument.SelectionBullet = tsbtnBullets.Checked;
                else if (name.IndexOf("Indent") >= 0)
                    rtbDocument.SelectionIndent += INDENT;
                else if (name.IndexOf("Outdent") >= 0)
                    rtbDocument.SelectionIndent -= INDENT;
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }

        private void tscmbFontSize_Click(object sender, EventArgs e)
        {
            // font size
            try
            {
                if (!(rtbDocument.SelectionFont == null))
                {
                    Font currentFont = rtbDocument.SelectionFont;
                    float newSize = Convert.ToSingle(tscmbFontSize.SelectedItem.ToString());
                    rtbDocument.SelectionFont = new Font(currentFont.FontFamily, newSize, currentFont.Style);
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }

        private void tscmbFontSize_TextChanged(object sender, EventArgs e)
        {
            // font size custom
            try
            {
                if (!(rtbDocument.SelectionFont == null))
                {
                    Font currentFont = rtbDocument.SelectionFont;
                    float newSize = Convert.ToSingle(tscmbFontSize.Text);
                    rtbDocument.SelectionFont = new Font(currentFont.FontFamily, newSize, currentFont.Style);
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }

        private void tscmbFont_Click(object sender, EventArgs e)
        {
            // font
            try
            {
                if (!(rtbDocument.SelectionFont == null))
                {
                    Font currentFont = rtbDocument.SelectionFont;
                    FontFamily newFamily = new FontFamily(tscmbFont.SelectedItem.ToString());
                    rtbDocument.SelectionFont = new Font(newFamily, currentFont.Size, currentFont.Style);
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }

        private void btnChooseFont_Click(object sender, EventArgs e)
        {
            using (FontDialog dlg = new FontDialog())
            {
                if (rtbDocument.SelectionFont != null) dlg.Font = rtbDocument.SelectionFont;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    rtbDocument.SelectionFont = dlg.Font;
                }
            }
        }

        private void tsbtnInsertPicture_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Insert picture";
                dlg.DefaultExt = "jpg";
                dlg.Filter = "Bitmap Files|*.bmp|JPEG Files|*.jpg|GIF Files|*.gif|All files|*.*";
                dlg.FilterIndex = 1;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        string strImagePath = dlg.FileName;
                        Image img = Image.FromFile(strImagePath);
                        Clipboard.SetDataObject(img);
                        DataFormats.Format df;
                        df = DataFormats.GetFormat(DataFormats.Bitmap);
                        if (this.rtbDocument.CanPaste(df))
                        {
                            this.rtbDocument.Paste(df);
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Unable to insert image.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void tsbtnSave_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog dlg = new SaveFileDialog())
            {
                dlg.Filter = "Rich text format|*.rtf";
                dlg.FilterIndex = 0;
                dlg.OverwritePrompt = true;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        rtbDocument.SaveFile(dlg.FileName, RichTextBoxStreamType.RichText);
                    }
                    catch (IOException exc)
                    {
                        MessageBox.Show("Error writing file: \n" + exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (ArgumentException exc_a)
                    {
                        MessageBox.Show("Error writing file: \n" + exc_a.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void tsbtnOpen_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Filter = "Rich text format|*.rtf";
                dlg.FilterIndex = 0;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        rtbDocument.LoadFile(dlg.FileName, RichTextBoxStreamType.RichText);
                    }
                    catch (IOException exc)
                    {
                        MessageBox.Show("Error reading file: \n" + exc.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (ArgumentException exc_a)
                    {
                        MessageBox.Show("Error reading file: \n" + exc_a.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (rtbDocument.ZoomFactor < 64.0f - 0.20f)
            {
                rtbDocument.ZoomFactor += 0.20f;
                tstxtZoomFactor.Text = String.Format("{0:F0}", rtbDocument.ZoomFactor * 100);
            }
        }

        private void tsbtnZoomOut_Click(object sender, EventArgs e)
        {
            if (rtbDocument.ZoomFactor > 0.16f + 0.20f)
            {
                rtbDocument.ZoomFactor -= 0.20f;
                tstxtZoomFactor.Text = String.Format("{0:F0}", rtbDocument.ZoomFactor * 100);
            }
        }

        private void tstxtZoomFactor_Leave(object sender, EventArgs e)
        {
            try
            {
                rtbDocument.ZoomFactor = Convert.ToSingle(tstxtZoomFactor.Text) / 100;
            }
            catch (FormatException)
            {
                MessageBox.Show("Enter valid number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tstxtZoomFactor.Focus();
                tstxtZoomFactor.SelectAll();
            }
            catch (OverflowException)
            {
                MessageBox.Show("Enter valid number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tstxtZoomFactor.Focus();
                tstxtZoomFactor.SelectAll();
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Zoom factor should be between 20% and 6400%", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                tstxtZoomFactor.Focus();
                tstxtZoomFactor.SelectAll();
            }
        }

        private void tsbtnWordWrap_Click(object sender, EventArgs e)
        {
            rtbDocument.WordWrap = tsbtnWordWrap.Checked;
        }

        private void tsbtnCut_Click(object sender, EventArgs e)
        {
            this.rtbDocument.Cut();
        }

        private void tsbtnCopy_Click(object sender, EventArgs e)
        {
            this.rtbDocument.Copy();
        }

        private void tsbtnPaste_Click(object sender, EventArgs e)
        {
            this.rtbDocument.Paste();
        }

        private void tsbtnUndo_Click(object sender, EventArgs e)
        {
            if (this.rtbDocument.CanUndo)
            {
                this.rtbDocument.Undo();
            }
        }

        private void tsbtnRedo_Click(object sender, EventArgs e)
        {
            if (this.rtbDocument.CanRedo)
            {
                this.rtbDocument.Redo();
            }
        }
        #endregion

        #region Public methods

        /// <summary>
        /// Sets if a custom tool button will be visible
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void ToolButtonVisible(string name, bool value)
        {
            toolStripActions.Items[name].Visible = value;
        }

        /// <summary>
        /// Gets if a custom tool button is visible
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool ToolButtonVisible(string name)
        {
            return toolStripActions.Items[name].Visible;
        }

        /// <summary>
        /// Selects all of the text within the control
        /// </summary>
        public void SelectAll()
        {
            this.rtbDocument.SelectAll();
        }

        /// <summary>
        /// Clears the undo buffer.
        /// </summary>
        public void ClearUndoBuffer()
        {
            this.rtbDocument.ClearUndo();
        }

        /// <summary>
        /// Clears the auto type pairs.
        /// </summary>
        /// <returns></returns>
        public void ClearAutoTypePairs()
        {
            autoTypePair.Clear();
        }

        /// <summary>
        /// Adds the auto type pair.
        /// </summary>
        /// <param name="sKeyPhrase"></param>
        /// <param name="sReplacement"></param>
        /// <returns></returns>
        public void AddAutoTypePair(string keyPhrase, string replacement)
        {
            // key must be at least 2 chars long and replacement cannot be null
            if (!String.IsNullOrEmpty(keyPhrase) && keyPhrase.Length > 1 && replacement != null)
            {
                if (autoTypePair.ContainsKey(keyPhrase))
                {
                    autoTypePair.Remove(keyPhrase);
                }
                autoTypePair.Add(keyPhrase, replacement);
            }
        }

        /// <summary>
        /// Saves the AutoTypePairs to the registry CurrentUser\Software\VB and VBA Program Settings\Chado SpellEditor\Settings
        /// </summary>
        /// <param name="sAppName"></param>
        public void SaveAutoTypeToReg(string sAppName)
        {
            // Remove previous settings for sAppName
            string[] subKeys = Interaction.GetSubKeys("Chado SpellEditor", "Settings");
            foreach (string subKey in subKeys)
            {
                int keyCount = 0;
                if (Int32.TryParse(subKey.Replace("K_" + sAppName, ""), out keyCount))
                {
                    Interaction.DeleteValue("Chado SpellEditor", "Settings", subKey);
                }
                else if (Int32.TryParse(subKey.Replace("R_" + sAppName, ""), out keyCount))
                {
                    Interaction.DeleteValue("Chado SpellEditor", "Settings", subKey);
                }
            }

            // Add the new settings for sAppName
            int count = 0;
            foreach (KeyValuePair<string, string> typePair in autoTypePair)
            {
                count++;
                Interaction.SaveSettingWithCreate("Chado SpellEditor", "Settings", "K_" + sAppName + count, typePair.Key);
                Interaction.SaveSettingWithCreate("Chado SpellEditor", "Settings", "R_" + sAppName + count, typePair.Value);
            }

            // Save the count of the settings for sAppName
            Interaction.DeleteValue("Chado SpellEditor", "Settings", "NATPairs_" + sAppName);
            Interaction.SaveSettingWithCreate("Chado SpellEditor", "Settings", "NATPairs_" + sAppName, count.ToString());
        }

        /// <summary>
        /// Clears the selection
        /// </summary>
        public void SelectNone()
        {
            this.rtbDocument.SelectionLength = 0;
        }

        /// <summary>
        /// Forces the control to refresh
        /// </summary>
        public void Refresh()
        {
            this.rtbDocument.Refresh();
        }

        public void SetFontFamily(FontFamily family)
        {
            if (family != null)
            {
                tscmbFont.SelectedItem = family.Name;
            }
        }

        public void SetFontSize(float newSize)
        {
            tscmbFontSize.Text = newSize.ToString();
        }

        public void ToggleBold()
        {
            tsbtnBold.PerformClick();
        }

        public void ToggleItalic()
        {
            tsbtnItalic.PerformClick();
        }

        public void ToggleUnderline()
        {
            tsbtnUnderline.PerformClick();
        }

        public void SetAlign(HorizontalAlignment alignment)
        {
            switch (alignment)
            {
                case HorizontalAlignment.Center:
                    tsbtnAlignCenter.PerformClick();
                    break;

                case HorizontalAlignment.Left:
                    tsbtnAlignLeft.PerformClick();
                    break;

                case HorizontalAlignment.Right:
                    tsbtnAlignRight.PerformClick();
                    break;
            }
        }

        public void Indent()
        {
            tsbtnIndent.PerformClick();
        }

        public void Outdent()
        {
            tsbtnOutdent.PerformClick();
        }

        public void ToggleBullets()
        {
            tsbtnBullets.PerformClick();
        }

        public void ZoomIn()
        {
            tsbtnZoomIn.PerformClick();
        }

        public void ZoomOut()
        {
            tsbtnZoomOut.PerformClick();
        }

        public void ZoomTo(float factor)
        {
            rtbDocument.ZoomFactor = factor;
        }

        public void SetWordWrap(bool activated)
        {
            rtbDocument.WordWrap = activated;
        }
        
        private int customToolbutton = 0;
        public void AddCustomToolbutton(string buttonName, string buttonText, Image buttonImage)
        {
            ToolStripButton button = new ToolStripButton();
            button.Name = buttonName;
            button.Text = buttonText;
            button.ToolTipText = buttonText;
            button.Image = buttonImage;
            button.DisplayStyle = ToolStripItemDisplayStyle.Image;
            button.Click += new EventHandler(button_Click);
            toolStripActions.Items.Insert(customToolbutton, button);
            customToolbutton++;
        }

        public void MoveCaretToStart()
        {
            this.rtbDocument.Select(0, 0);
        }

        public void MoveCaretToEnd()
        {
            this.rtbDocument.Select(this.rtbDocument.TextLength, 0);
        }

        #region Changing visibility of toolstrip items

        public void HideToolstripItemsByGroup(RicherTextBoxToolStripGroups group, bool visible)
        {
            if ((group & RicherTextBoxToolStripGroups.SaveAndLoad) != 0)
            {
                tsbtnSave.Visible = visible;
                tsbtnOpen.Visible = visible;
                toolStripSeparator6.Visible = visible;
            }
            if ((group & RicherTextBoxToolStripGroups.FontNameAndSize) != 0)
            {
                tscmbFont.Visible = visible;
                tscmbFontSize.Visible = visible;
                tsbtnChooseFont.Visible = visible;
                toolStripSeparator1.Visible = visible;
            }
            if ((group & RicherTextBoxToolStripGroups.BoldUnderlineItalic) != 0)
            {
                tsbtnBold.Visible = visible;
                tsbtnItalic.Visible = visible;
                tsbtnUnderline.Visible = visible;
                toolStripSeparator2.Visible = visible;
            }
            if ((group & RicherTextBoxToolStripGroups.Alignment) != 0)
            {
                tsbtnAlignLeft.Visible = visible;
                tsbtnAlignRight.Visible = visible;
                tsbtnAlignCenter.Visible = visible;
                toolStripSeparator3.Visible = visible;
            }
            if ((group & RicherTextBoxToolStripGroups.FontColor) != 0)
            {
                tsbtnFontColor.Visible = visible;
                tsbtnWordWrap.Visible = visible;
                toolStripSeparator4.Visible = visible;
            }
            if ((group & RicherTextBoxToolStripGroups.IndentationAndBullets) != 0)
            {
                tsbtnIndent.Visible = visible;
                tsbtnOutdent.Visible = visible;
                tsbtnBullets.Visible = visible;
            }
            if ((group & RicherTextBoxToolStripGroups.Insert) != 0)
            {
                tsbtnInsertPicture.Visible = visible;
                toolStripSeparator7.Visible = visible;
            }
            if ((group & RicherTextBoxToolStripGroups.Zoom) != 0)
            {
                tsbtnZoomOut.Visible = visible;
                tsbtnZoomIn.Visible = visible;
                tstxtZoomFactor.Visible = visible;
            }
        }

        public bool IsGroupVisible(RicherTextBoxToolStripGroups group)
        {
            switch (group)
            {
                case RicherTextBoxToolStripGroups.SaveAndLoad:
                    return tsbtnSave.Visible && tsbtnOpen.Visible && toolStripSeparator6.Visible;

                case RicherTextBoxToolStripGroups.FontNameAndSize:
                    return tscmbFont.Visible && tscmbFontSize.Visible && tsbtnChooseFont.Visible && toolStripSeparator1.Visible;

                case RicherTextBoxToolStripGroups.BoldUnderlineItalic:
                    return tsbtnBold.Visible && tsbtnItalic.Visible && tsbtnUnderline.Visible && toolStripSeparator2.Visible;

                case RicherTextBoxToolStripGroups.Alignment:
                    return tsbtnAlignLeft.Visible && tsbtnAlignRight.Visible && tsbtnAlignCenter.Visible && toolStripSeparator3.Visible;

                case RicherTextBoxToolStripGroups.FontColor:
                    return tsbtnFontColor.Visible && tsbtnWordWrap.Visible && toolStripSeparator4.Visible;

                case RicherTextBoxToolStripGroups.IndentationAndBullets:
                    return tsbtnIndent.Visible && tsbtnOutdent.Visible && tsbtnBullets.Visible;

                case RicherTextBoxToolStripGroups.Insert:
                    return tsbtnInsertPicture.Visible && toolStripSeparator7.Visible;

                case RicherTextBoxToolStripGroups.Zoom:
                    return tsbtnZoomOut.Visible && tsbtnZoomIn.Visible && tstxtZoomFactor.Visible;

                default:
                    return false;
            }
        }
        #endregion

        #endregion

        #region Event handlers
        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbDocument.Cut();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbDocument.Copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbDocument.Paste();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbDocument.Clear();
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbDocument.SelectAll();
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbDocument.Undo();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            rtbDocument.Redo();
        }

        private void leftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tsbtnAlignLeft.PerformClick();

            leftToolStripMenuItem.Checked = true;
            centerToolStripMenuItem.Checked = false;
            rightToolStripMenuItem.Checked = false;


        }

        private void centerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tsbtnAlignCenter.PerformClick();

            leftToolStripMenuItem.Checked = false;
            centerToolStripMenuItem.Checked = true;
            rightToolStripMenuItem.Checked = false;
        }

        private void rightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tsbtnAlignRight.PerformClick();

            leftToolStripMenuItem.Checked = false;
            centerToolStripMenuItem.Checked = false;
            rightToolStripMenuItem.Checked = true;
        }

        private void boldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tsbtnBold.PerformClick();
        }

        private void italicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tsbtnItalic.PerformClick();
        }

        private void underlineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tsbtnUnderline.PerformClick();
        }

        private void increaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tsbtnIndent.PerformClick();
        }

        private void decreaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tsbtnOutdent.PerformClick();
        }

        private void bulletsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tsbtnBullets.PerformClick();
        }

        private void zoomInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tsbtnZoomIn.PerformClick();
        }

        private void zoomOuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tsbtnZoomOut.PerformClick();
        }

        private void insertPictureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tsbtnInsertPicture.PerformClick();
        }

        private void button_Click(object sender, EventArgs e)
        {
            if (ToolbuttonClick != null)
            {
                ToolbuttonClick(this, new ToolStripItemClickedEventArgs(sender as ToolStripButton));
            }
        }

        private void tsbtnPrint_Click(object sender, EventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            PrintDocument documentToPrint = new PrintDocument();
            printDialog.Document = documentToPrint;

            if (printDialog.ShowDialog() == DialogResult.OK)
            {
                StringReader reader = new StringReader(this.rtbDocument.Text);
                documentToPrint.PrintPage += new PrintPageEventHandler(DocumentToPrint_PrintPage);
                documentToPrint.Print();
            }
        }

        private void DocumentToPrint_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            StringReader reader = new StringReader(this.rtbDocument.Text);
            float LinesPerPage = 0;
            float YPosition = 0;
            int Count = 0;
            float LeftMargin = e.MarginBounds.Left;
            float TopMargin = e.MarginBounds.Top;
            string Line = null;
            Font PrintFont = this.rtbDocument.Font;
            SolidBrush PrintBrush = new SolidBrush(Color.Black);

            LinesPerPage = e.MarginBounds.Height / PrintFont.GetHeight(e.Graphics);

            while (Count < LinesPerPage && ((Line = reader.ReadLine()) != null))
            {
                YPosition = TopMargin + (Count * PrintFont.GetHeight(e.Graphics));
                e.Graphics.DrawString(Line, PrintFont, PrintBrush, LeftMargin, YPosition, new StringFormat());
                Count++;
            }

            if (Line != null)
            {
                e.HasMorePages = true;
            }
            else
            {
                e.HasMorePages = false;
            }
            PrintBrush.Dispose();
        }

        private void tsbtnNew_Click(object sender, EventArgs e)
        {
            if (this.changes)
            {
                DialogResult result = MessageBox.Show("Your document has changed. Would you like to save before you close the document?", "Closing document", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                {
                    SaveFileDialog saveFile = new SaveFileDialog();
                    saveFile.Filter = "Text Files (*.txt)|*.txt|Rich Text Files (*.rtf)|*.rtf|HTML Files (*.html)|*.html|All files (*.*)|*.*";
                    saveFile.FilterIndex = 2;
                    DialogResult saveResult = saveFile.ShowDialog();
                    if (saveResult == DialogResult.OK)
                    {
                        string extension = Path.GetExtension(saveFile.FileName).ToLower();
                        RichTextBoxStreamType saveType;
                        if (extension == ".rtf")
                        {
                            saveType = RichTextBoxStreamType.RichText;
                        }
                        else
                        {
                            saveType = RichTextBoxStreamType.PlainText;
                        }
                        this.rtbDocument.SaveFile(saveFile.FileName, saveType);

                        ResetSpellEditor();
                    }
                }
                else if (result == DialogResult.No)
                {
                    ResetSpellEditor();
                }
                else if (result == DialogResult.Cancel)
                {
                    return;
                }
            }
        }

        private void tsbtnSuperSubScript_Click(object sender, EventArgs e)
        {
            ToolStripButton button = sender as ToolStripButton;
            string buttonName = button.Name;
            if (!button.Checked)
            {
                this.rtbDocument.SelectionCharOffset = 0;
            }
            else
            {
                if (buttonName.Contains("Super"))
                {
                    this.rtbDocument.SelectionCharOffset = 10;
                    this.tsbtnSubscript.Checked = false;
                }
                else if (buttonName.Contains("Sub"))
                {
                    this.rtbDocument.SelectionCharOffset = -10;
                    this.tsbtnSuperScript.Checked = false;
                }
            }
        }
        #endregion

        #region Find and Replace
        private void tsbtnFind_Click(object sender, EventArgs e)
        {
            fecherFoundation.FCFindForm findForm = new fecherFoundation.FCFindForm();
            findForm.RtbInstance = this.rtbDocument;
            findForm.InitialText = this.tstxtSearchText.Text;
            findForm.Show();
        }

        private void tsbtnReplace_Click(object sender, EventArgs e)
        {
            fecherFoundation.FCReplaceForm replaceForm = new fecherFoundation.FCReplaceForm();
            replaceForm.RtbInstance = this.rtbDocument;
            replaceForm.InitialText = this.tstxtSearchText.Text;
            replaceForm.Show();
        }
        #endregion

        #region Private Methods
        private void ReplaceAutoTypePair()
        {
            int pos = this.rtbDocument.SelectionStart;
            string text = this.rtbDocument.Text.Substring(0, pos);
            int len = text.Length;
            foreach (KeyValuePair<string, string> rule in autoTypePair)
            {
                if (rule.Key.Length <= len)
                {
                    if (text.Substring(len - rule.Key.Length) == rule.Key)
                    {
                        //found key
                        //remove key from text
                        text = rtbDocument.Text.Remove(pos - rule.Key.Length, rule.Key.Length);
                        //insert pair value
                        text = text.Insert(pos - rule.Key.Length, rule.Value);
                        //set back the text
                        rtbDocument.Text = text;
                        //set caret after the inserted text
                        rtbDocument.SelectionStart = pos - rule.Key.Length + rule.Value.Length;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Resets the spell editor to the default values and clears the text
        /// </summary>
        private void ResetSpellEditor()
        {
            this.rtbDocument.Text = "";

            // Reset values
            tscmbFont.SelectedItem = "Microsoft Sans Serif";
            tscmbFontSize.SelectedItem = "9";

            // Uncheck all items
            UncheckItems(toolStrip1);
            UncheckItems(toolStripActions);

            this.rtbDocument.SelectionColor = Color.Black;

            changes = false;
        }

        private void UncheckItems(ToolStrip strip)
        {
            foreach (ToolStripItem item in strip.Items)
            {
                ToolStripButton btn = item as ToolStripButton;
                if (btn != null)
                {
                    btn.Checked = false;
                }
            }
        }
        #endregion
    }
}
