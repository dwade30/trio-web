﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
using fecherFoundation.Extensions;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Wisej.Base;

namespace fecherFoundation
{
    /// <summary>
    /// VB.PictureBox
    /// </summary>
    public partial class FCPictureBox : PictureBox
    {
        #region Public Members
        /// <summary>
        /// if false, then the assigned Image will not be cleard in Cls and BackColorChanged 
        /// </summary>
        [DefaultValue(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        public bool AllowImageNull
        {
            get
            {
                return allowImageNull;
            }
            set
            {
                allowImageNull = value;
            }
        }
        #endregion

        #region Internal Members
		/// <summary>
		/// Used to save list of points used by the SetClipRegion function
		/// </summary>
		internal GraphicsPath ClipPath { get; set; }
        #endregion

        #region Private Members

        private Image dragIcon;
        private ToolTip toolTip = new ToolTip();
        private MouseEventArgs prevMouseEventArgs = null;
        private AppearanceConstants appearance = AppearanceConstants.a3D;
        private DrawModeConstants drawMode = DrawModeConstants.VbCopyPen;
        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;
        private int scaleHeight = -1;
        private int scaleWidth = -1;
        private int borderStyle = 1;
        //PJU:TODO: avoid executing double Eventhandlers while in Event-Handler (case if ContextMenu is shown and Value will be selected first time s. IIT #120)
        private bool mouseDownActive = false;
        private bool mouseUpActive = false;
        private bool batchDrawModeActive = false;
        private bool vbNotXorPenModeActive = false;
        private bool hDCActive = false;

        private MousePointerConstants mousePointer = MousePointerConstants.vbDefault;

        //Cached graphics Object
        private Graphics currentGraphics;

        private IntPtr localHdc;

		//When image is set to null, in VB6 the PictureBox maintains its size even AutoSize is set to true
		private bool autoSizeInternal = false;

        //if false, then the assigned Image will not be cleard in Cls and BackColorChanged 
        private bool allowImageNull = true;

        #endregion

        #region Constructors

        public FCPictureBox()
        {
            graphicsFactory = new GraphicsFactory();
            //this.BackColor = SystemColors.Control;
            InitializeComponent();
			this.autoSizeInternal = AutoSize;
            CurrentGraphics = this.GetGraphics();
            fcGraphics = new FCGraphics(currentGraphics, graphicsFactory);
            this.AllowDrop = true;
            //CHE: set default vbTwips
            this.ScaleMode = ScaleModeConstants.vbTwips;

            this.BorderStyle = Wisej.Web.BorderStyle.Solid;
            //CHE: set default value
            this.WhatsThisHelpID = 0;
            this.DragMode = DragModeConstants.vbManual;
            this.AutoRedraw = false;
            this.DrawMode = DrawModeConstants.VbCopyPen;
            this.ToolTipText = "";
            this.DrawStyle = 0;
            this.DrawWidth = 1;
            this.FillColor = 0;
            this.FillStyle = 1;
            this.AllowImageNull = false;
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
       
        #endregion

        #region Enums

        public enum AppearanceConstants : int
        {
            a2D = 0,
            a3D = 1
        }

        public enum FillStyleConstants : int
        {
            Ausgefullt = 0,
            Transparent = 1,
            HorizontalLine = 2,
            VerticalLine = 3,
            AufwartsDiagonal = 4,
            AbwartsDiagonal = 5,
            Kreuz = 6,
            DiagonalKreuz = 7
        }

        #endregion

        #region Properties

        /// <summary>
        /// Returns/sets an associated context number for an object.
        /// </summary>
        [DefaultValue(0)]
        public int WhatsThisHelpID
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Set BatchDrowMode to true when prevent from releasing and creating Graphics.DC because of Performance-Improvment
        /// Don't forget to reset the BatchDrawMode
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool BatchDrowMode
        {
            get
            {
                return batchDrawModeActive;
            }
            set
            {
                batchDrawModeActive = value;
                if (batchDrawModeActive == false)
                {
                    this.ReleaseCurrentGraphicshDc();
                }
            }
        }

        [DefaultValue(MousePointerConstants.vbDefault)]
        public MousePointerConstants MousePointer
        {
            get
            {
                return mousePointer;
            }
            set
            {
                mousePointer = value;
                this.Cursor = FCUtils.GetTranslatedCursor(mousePointer);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
		/// Returns or sets the image that is displayed by <see cref="T:Wisej.Web.PictureBox" />.
		/// </summary>
		/// <returns>The <see cref="T:System.Drawing.Image" /> to display.</returns>
		[Bindable(true)]
        [Localizable(true)]
        [PostbackProperty]
        public new Image Image
		{
			get
			{
				return base.Image;
			}
			set
			{
                //when setting a new Image, then release the Graphics-Object first to be sure that it will be newly generated
                ReleaseGraphics();

                if (AutoSize && base.Image != null && value == null)
				{
                    this.Image.Dispose();
                    //Save Size because it is reset when AutoSize will be set to false
                    Size sizeBeforeImageNull = this.Size; 
					autoSizeInternal = true;
					AutoSize = false;
					//Set back Size value
					this.Size = sizeBeforeImageNull;
				}
				else if (value != null && base.Image == null)
				{
					AutoSize = autoSizeInternal;
				}
                if (!object.Equals(base.Image, value))
                {
                    base.Image = value;
                }
			}
		}

		public override bool AutoSize
		{
			get
			{
				return base.AutoSize;
			}
			set
			{
				autoSizeInternal = value;
				base.AutoSize = value;
			}
		}

        /// <summary>
        /// Returns or sets the border style for an object. For the Form object and the TextBox control, read-only at run time.
        /// Syntax
        /// object.BorderStyle = [value]
        /// The BorderStyle property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// value	A value or constant that determines the border style, as described in Settings.
        /// Settings
        /// The BorderStyle property settings for a Form object are:
        /// Constant	Setting	Description
        /// vbBSNone	0	None (no border or border-related elements).
        /// vbFixedSingle	1	Fixed Single. Can include Control-menu box, title bar, Maximize button, and Minimize button. Resizable only using Maximize and Minimize buttons.
        /// vbSizable	2	(Default) Sizable. Resizable using any of the optional border elements listed for setting 1.
        /// vbFixedDouble	3	Fixed Dialog. Can include Control-menu box and title bar; can't include Maximize or Minimize buttons. Not resizable.
        /// vbFixedToolWindow	4	Fixed ToolWindow. Displays a non-sizable window with a Close button and title bar text in a reduced font size. The form does not appear in the Windows taskbar.
        /// vbSizableToolWindow	5	Sizable ToolWindow. Displays a sizable window with a Close button and title bar text in a reduced font size. The form does not appear in the Windows taskbar.
        /// 
        ///The BorderStyle property settings for MS Flex Grid, Image, Label, OLE container, PictureBox, Frame, and TextBox controls are:
        ///Setting	Description
        ///0	(Default for Image and Label controls) None.
        ///1	(Default for MS Flex Grid, PictureBox, TextBox, and OLE container controls) Fixed Single.
        ///
        ///The BorderStyle property settings for Line and Shape controls are:
        ///Constant	Setting	Description
        ///vbTransparent	0	Transparent
        ///vbBSSolid	1	(Default) Solid. The border is centered on the edge of the shape.
        ///vbBSDash	2	Dash
        ///vbBSDot	3	Dot
        ///vbBSDashDot	4	Dash-dot
        ///vbBSDashDotDot	5	Dash-dot-dot
        ///vbBSInsideSolid	6	Inside solid. The outer edge of the border is the outer edge of the shape.
        ///
        /// Remarks
        /// For a form, the BorderStyle property determines key characteristics that visually identify a form as either a general-purpose window or a dialog box. Setting 3 (Fixed Dialog) is useful for 
        /// standard dialog boxes. Settings 4 (Fixed ToolWindow) and 5 (Sizable ToolWindow) are useful for creating toolbox-style windows.
        /// MDI child forms set to 2 (Sizable) are displayed within the MDI form in a default size defined by the Windows operating environment at run time. For any other setting, the form is displayed 
        /// in the size specified at design time.
        /// Changing the setting of the BorderStyle property of a Form object may change the settings of the MinButton, MaxButton, and ShowInTaskbar properties. When BorderStyle is set to 1 (Fixed Single) 
        /// or 2 (Sizable), the MinButton, MaxButton, and ShowInTaskbar properties are automatically set to True. When BorderStyle is set to 0 (None), 3 (Fixed Dialog), 4 (Fixed ToolWindow), or 5 (Sizable 
        /// ToolWindow), the MinButton, MaxButton, and ShowInTaskbar properties are automatically set to False.
        /// Note   If a form with a menu is set to 3 (Fixed Dialog), it is displayed with a setting 1 (Fixed Single) border instead.
        /// At run time, a form is either modal or modeless, which you specify using the Show method.
        /// </summary>
        [DefaultValue(BorderStyle.Solid)]
        public new BorderStyle BorderStyle
        {
            get 
            { 
                return base.BorderStyle; 
            }
            set 
            { 
                base.BorderStyle = value; 
            }
        }

        /// <summary>
        /// Returns or sets a ToolTip.
        /// Syntax
        /// object.ToolTipText [= string]
        /// The ToolTipText property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// string	A string associated with an object in the Applies To list. that appears in a small rectangle below the object when the user's cursor hovers over the object at run time for about one second.
        /// Remarks
        /// If you use only an image to label an object, you can use this property to explain each object with a few words.
        /// At design time you can set the ToolTipText property string in the control's properties dialog box.
        /// For the Toolbar and TabStrip controls, you must set the ShowTips property to True to display ToolTips.
        /// </summary>
        [DefaultValue("")]
        public string ToolTipText
        {
            get
            {
                return toolTip.GetToolTip(this);
            }
            set
            {
                toolTip.SetToolTip(this, value);
            }
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                InitializeCoordinateSpace();
                return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));

                //using (Graphics g = GetGraphics())
                //{
                //    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                //    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                //}
            }
            set
            {
                InitializeCoordinateSpace();
                this.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));

                //using (Graphics g = GetGraphics())
                //{
                //    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                //    // must use this.Width (which is overriden) instead of base.Width to allow setting of WidthOriginal value when SizeMode is AutoSize
                //    this.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                //}
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                InitializeCoordinateSpace();
                return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                //using (Graphics g = GetGraphics())
                //{
                //    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                //    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                //}
            }
            set
            {
                InitializeCoordinateSpace();
                this.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                //using (Graphics g = GetGraphics())
                //{
                //    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                //    // must use this.Height (which is overriden) instead of base.Height to allow setting of HeightOriginal value when SizeMode is AutoSize
                //    this.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                //}
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                InitializeCoordinateSpace();
                return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);

                //using (Graphics g = GetGraphics())
                //{
                //    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                //    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                //}
            }
            set
            {
                InitializeCoordinateSpace();
                base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));

                //using (Graphics g = GetGraphics())
                //{
                //    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                //    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                //}
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                InitializeCoordinateSpace();
                return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));

                //using (Graphics g = GetGraphics())
                //{
                //    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                //    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                //}
            }
            set
            {
                InitializeCoordinateSpace();
                base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));

                //using (Graphics g = GetGraphics())
                //{
                //    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                //    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                //}
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float ScaleHeight
        {
            get
            {
                //using (Graphics g = GetGraphics())
                //{
                    fcGraphics.InitializeCoordinateSpace(CurrentGraphics, this.ClientRectangle, 0, 0);
                    return fcGraphics.ScaleHeight;
                //}
            }
            set
            {
                //using (Graphics g = GetGraphics())
                //{
                    fcGraphics.InitializeCoordinateSpace(CurrentGraphics, this.ClientRectangle, 0, 0);
                    fcGraphics.ScaleHeight = value;
                //}
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float ScaleWidth
        {
            get
            {
                //using (Graphics g = GetGraphics())
                //{
                    fcGraphics.InitializeCoordinateSpace(CurrentGraphics, this.ClientRectangle, 0, 0);
                    return fcGraphics.ScaleWidth;
                //}
            }
            set
            {
                //using (Graphics g = GetGraphics())
                //{
                    fcGraphics.InitializeCoordinateSpace(CurrentGraphics, this.ClientRectangle, 0, 0);
                    fcGraphics.ScaleWidth = value;
                //}
            }
        }

        /// <summary>
        /// Return or set the horizontal (ScaleLeft) and vertical (ScaleTop) coordinates for the left and top edges of an object when using graphics methods or when positioning controls.
        /// Syntax
        /// object.ScaleLeft [= value]
        /// object.ScaleTop [= value]
        /// The ScaleLeft and ScaleTop property syntaxes have these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Value	A numeric expression specifying the horizontal or vertical coordinate. The default is 0.
        /// Remarks
        /// Using these properties and the related ScaleHeight and ScaleWidth properties, you can set up a full coordinate system with both positive and negative coordinates. These four 
        /// Scale properties interact with the ScaleMode property in the following ways:
        /// Setting any other Scale property to any value automatically sets ScaleMode to 0. A ScaleMode of 0 is user-defined.
        /// Setting the ScaleMode property to a number greater than 0 changes ScaleHeight and ScaleWidth to the new unit of measurement and sets ScaleLeft and ScaleTop to 0. The CurrentX 
        /// and CurrentY property settings change to reflect the new coordinates of the current point.
        /// You can also use the Scale method to set the ScaleHeight, ScaleWidth, ScaleLeft, and ScaleTop properties in one statement.
        /// Note   The ScaleLeft and ScaleTop properties aren't the same as the Left and Top properties.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float ScaleLeft
        {
            get
            {
             //   using (Graphics g = GetGraphics())
               // {
                    fcGraphics.InitializeCoordinateSpace(CurrentGraphics, this.ClientRectangle, 0, 0);
                    return fcGraphics.ScaleLeft;
                //}
            }
            set
            {
                //using (Graphics g = GetGraphics())
                //{
                    fcGraphics.InitializeCoordinateSpace(CurrentGraphics, this.ClientRectangle, 0, 0);
                    fcGraphics.ScaleLeft = value;
                //}
            }
        }

        /// <summary>
        /// Return or set the horizontal (ScaleLeft) and vertical (ScaleTop) coordinates for the left and top edges of an object when using graphics methods or when positioning controls.
        /// Syntax
        /// object.ScaleLeft [= value]
        /// object.ScaleTop [= value]
        /// The ScaleLeft and ScaleTop property syntaxes have these parts:
        /// Part	Description
        /// Object	An object expression that evaluates to an object in the Applies To list.
        /// Value	A numeric expression specifying the horizontal or vertical coordinate. The default is 0.
        /// Remarks
        /// Using these properties and the related ScaleHeight and ScaleWidth properties, you can set up a full coordinate system with both positive and negative coordinates. These four 
        /// Scale properties interact with the ScaleMode property in the following ways:
        /// Setting any other Scale property to any value automatically sets ScaleMode to 0. A ScaleMode of 0 is user-defined.
        /// Setting the ScaleMode property to a number greater than 0 changes ScaleHeight and ScaleWidth to the new unit of measurement and sets ScaleLeft and ScaleTop to 0. The CurrentX 
        /// and CurrentY property settings change to reflect the new coordinates of the current point.
        /// You can also use the Scale method to set the ScaleHeight, ScaleWidth, ScaleLeft, and ScaleTop properties in one statement.
        /// Note   The ScaleLeft and ScaleTop properties aren't the same as the Left and Top properties.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float ScaleTop
        {
            get
            {
                //using (Graphics g = GetGraphics())
                //{
                    fcGraphics.InitializeCoordinateSpace(CurrentGraphics, this.ClientRectangle, 0, 0);
                    return fcGraphics.ScaleTop;
                //}
            }
            set
            {
                //using (Graphics g = GetGraphics())
                //{
                    fcGraphics.InitializeCoordinateSpace(CurrentGraphics, this.ClientRectangle, 0, 0);
                    fcGraphics.ScaleTop = value;
                //}
            }
        }

        /// <summary>
        /// Returns or sets a value indicating the unit of measurement for coordinates of an object when using graphics methods or when positioning controls.
        /// object.ScaleMode [= value]
        /// The ScaleMode property syntax has these parts:
        /// Value	An integer specifying the unit of measurement, as described in Settings.
        /// Settings
        /// The settings for value are:
        /// Constant	Setting	Description
        /// vbUser	0	Indicates that one or more of the ScaleHeight, ScaleWidth, ScaleLeft, and ScaleTop properties are set to custom values.
        /// vbTwips	1	(Default) Twip (1440 twips per logical inch; 567 twips per logical centimeter).
        /// vbPoints	2	Point (72 points per logical inch).
        /// vbPixels	3	Pixel (smallest unit of monitor or printer resolution).
        /// vbCharacters	4	Character (horizontal = 120 twips per unit; vertical = 240 twips per unit).
        /// vbInches	5	Inch.
        /// vbMillimeters	6	Millimeter.
        /// vbCentimeters	7	Centimeter.
        /// vbHimetric	8	HiMetric
        /// vbContainerPosition	9	Units used by the control's container to determine the control's position.
        /// vbContainerSize	10	Units used by the control's container to determine the control's size.
        /// Remarks
        /// Using the related ScaleHeight, ScaleWidth, ScaleLeft, and ScaleTop properties, you can create a custom coordinate system with both positive and negative coordinates. These four Scale properties 
        /// interact with the ScaleMode property in the following ways:
        /// Setting the value of any other Scale property to any value automatically sets ScaleMode to 0. A ScaleMode of 0 is user-defined.
        /// Setting the ScaleMode property to a number greater than 0 changes ScaleHeight and ScaleWidth to the new unit of measurement and sets ScaleLeft and ScaleTop to 0. The CurrentX and CurrentY 
        /// property settings change to reflect the new coordinates of the current point.
        /// </summary>
        [DefaultValue(ScaleModeConstants.vbTwips)]
        public ScaleModeConstants ScaleMode
        {
            get
            {
                return fcGraphics.ScaleMode;
            }
            set
            {
                fcGraphics.ScaleMode = value;
            }
        }

        /// <summary>
        /// Returns a handle provided by the Microsoft Windows operating environment to the device context of an object.
        /// Remarks
        /// This property is a Windows operating environment device context handle. The Windows operating environment manages the system display by assigning a device context for the Printer object and 
        /// for each form and PictureBox control in your application. You can use the hDC property to refer to the handle for an object's device context. This provides a value to pass to Windows API calls.
        /// With a CommonDialog control, this property returns a device context for the printer selected in the Print dialog box when the cdlReturnDC flag is set or an information context when the cdlReturnIC 
        /// flag is set.
        /// Note   The value of the hDC property can change while a program is running, so don't store the value in a variable; instead, use the hDC property each time you need it.
        /// The AutoRedraw property can cause the hDC property setting to change. If AutoRedraw is set to True for a form or PictureBox container, hDC acts as a handle to the device context of the persistent 
        /// graphic (equivalent to the Image property). When AutoRedraw is False, hDC is the actual hDC value of the Form window or the PictureBox container. The hDC property setting may change while the 
        /// program is running regardless of the AutoRedraw setting.
        /// If the HasDC property is set to False, a new device context will be created by the system and the value of the hDC property will change each time it is called.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public IntPtr hDC
        {
            get
            {
                //CHE: GDI32
                if (FCUtils.inCloneControlProperties)
                {
                    return IntPtr.Zero;
                }

                if (currentGraphics == null)
                {
                    CurrentGraphics = this.GetGraphics();
                    hDCActive = false;
                }
                if(!hDCActive)
                {
                    localHdc = GetHdc_CurrentGraphics();
                }
               
                return localHdc;

            }
        }

        /// <summary>
        /// Gets/sets the Appearance of the control
        /// </summary>
        [DefaultValue(AppearanceConstants.a3D)]
        public AppearanceConstants Appearance
        {
            get
            {
                return this.appearance;
            }
            set
            {
                this.appearance = value;
                //changing appearance in InitializeComponent should not change the BackColor to white for 2D
                Form f = this.FindForm();
                if (f != null && f.Created)
                {
                    if (this.appearance == AppearanceConstants.a3D)
                    {
                        base.BorderStyle = Wisej.Web.BorderStyle.Solid;
                        base.BackColor = System.Drawing.SystemColors.Control;
                    }
                    else
                    {
                        base.BackColor = System.Drawing.Color.White;
                        base.BorderStyle = Wisej.Web.BorderStyle.Solid;
                    }
                }
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether manual or automatic drag mode is used for a drag-and-drop operation.
        /// object.DragMode [= number]
        /// Number	An integer that specifies the drag mode, as described in Settings.
        /// Settings
        /// The settings for number are:
        /// Constant	Setting	Description
        /// VbManual	0	(Default) Manual requires using the Drag method to initiate a drag-and-drop operation on the source control.
        /// VbAutomatic	1	Automatic clicking the source control automatically initiates a drag-and-drop operation. OLE container controls are automatically dragged only when they don't have the focus.
        /// Remarks
        /// When DragMode is set to 1 (Automatic), the control doesn't respond as usual to mouse events. Use the 0 (Manual) setting to determine when a drag-and-drop operation begins or ends; you can use 
        /// this setting to initiate a drag-and-drop operation in response to a keyboard or menu command or to enable a source control to recognize a MouseDown event prior to a drag-and-drop operation.
        /// Clicking while the mouse pointer is over a target object or form during a drag-and-drop operation generates a DragDrop event for the target object. This ends the drag-and-drop operation. 
        /// A drag-and-drop operation may also generate a DragOver event.
        /// Note   While a control is being dragged, it can't recognize other user-initiated mouse or keyboard events (KeyDown, KeyPress or KeyUp, MouseDown, MouseMove, or MouseUp). However, the control 
        /// can receive events initiated by code or by a DDE link.
        /// </summary>
        [DefaultValue(DragModeConstants.vbManual)]
        public DragModeConstants DragMode
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the output from a graphics method to a persistent graphic.
        /// object.AutoRedraw [= boolean]
        /// Boolean	A Boolean expression that specifies how the object is repainted, as described in Settings.
        /// True	Enables automatic repainting of a Form object or PictureBox control. Graphics and text are written 
        /// to the screen and to an image stored in memory. The object doesn't receive Paint events; it's repainted when 
        /// necessary, using the image stored in memory.
        /// False	(Default) Disables automatic repainting of an object and writes graphics or text only to the screen. 
        /// Visual Basic invokes the object's Paint event when necessary to repaint the object.
        /// Remarks
        /// This property is central to working with the following graphics methods: Circle, Cls, Line, Point, Print, and PSet. 
        /// Setting AutoRedraw to True automatically redraws the output from these methods in a Form object or PictureBox control
        /// when, for example, the object is resized or redisplayed after being hidden by another object.
        /// You can set AutoRedraw in code at run time to alternate between drawing persistent graphics (such as a background or 
        /// grid) and temporary graphics. If you set AutoRedraw to False, previous output becomes part of the background screen. 
        /// When AutoRedraw is set to False, background graphics aren't deleted if you clear the drawing area with the Cls method.
        /// Setting AutoRedraw back to True and then using Cls clears the background graphics.
        /// Note   If you set the BackColor property, all graphics and text, including the persistent graphic, are erased. 
        /// In general, all graphics should be displayed using the Paint event unless AutoRedraw is set to True.
        /// To retrieve the persistent graphic created when AutoRedraw is set to True, use the Image property. To pass the 
        /// persistent graphic to a Windows API when AutoRedraw is set to True, use the object's hDC property.
        /// If you set a form's AutoRedraw property to False and then minimize the form, the ScaleHeight and ScaleWidth properties
        /// are set to icon size. When AutoRedraw is set to True, ScaleHeight and ScaleWidth remain the size of the restored window.
        /// If AutoRedraw is set to False, the Print method will print on top of graphical controls such as the Image and Shape controls.
        /// </summary>
        [DefaultValue(false)]
        public bool AutoRedraw
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the horizontal coordinates for the next printing or drawing method.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float CurrentX { get; set; }


        /// <summary>
        /// Gets or sets the vertical coordinates for the next printing or drawing method.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float CurrentY { get; set; }
        
        /// <summary>
        /// Gets or sets a value that determines the line style for output from graphics methods.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short.
        /// </returns>
        [DefaultValue(0)]
        public short DrawStyle
        {
            get
            {
                return this.graphicsFactory.DrawStyle;
            }
            set
            {
                this.graphicsFactory.DrawStyle = value;
            }
        }

        /// <summary>
        /// Gets or sets the line width for output from graphics methods.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short.
        /// </returns>
        [DefaultValue(1)]
        public short DrawWidth
        {
            get
            {
                return this.graphicsFactory.DrawWidth;
            }
            set
            {
                this.graphicsFactory.DrawWidth = value;
            }
        }

        /// <summary>
        /// Gets or sets the color that is used to fill in shapes created by using the <see cref="Overload:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.Circle"/> and <see cref="Overload:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.Line"/> graphics methods.
        /// </summary>
        /// 
        /// <returns>
        /// Returns an Integer.
        /// </returns>
        [DefaultValue(0)]
        public int FillColor
        {
            get
            {
                return this.graphicsFactory.FillColor;
            }
            set
            {
                this.graphicsFactory.FillColor = value;
            }
        }

        /// <summary>
        /// Gets or sets the pattern used to fill shapes created by using the <see cref="M:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.Circle(System.Boolean,System.Single,System.Single,System.Single,System.Int32,System.Single,System.Single,System.Single)"/> and <see cref="M:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.Line(System.Boolean,System.Single,System.Single,System.Boolean,System.Single,System.Single,System.Int32,System.Boolean,System.Boolean)"/> graphics methods.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short. The default is 1.
        /// </returns>
        [DefaultValue(1)]
        public short FillStyle
        {
            get
            {
                return this.graphicsFactory.FillStyle;
            }
            set
            {
                this.graphicsFactory.FillStyle = value;
            }
        }

        /// <summary>
        /// Gets or sets a FontFamily by name.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a <see cref="T:System.Drawing.Font"/>.
        /// </returns>
        public new Font Font
        {
            get
            {
                return base.Font;
            }
            set
            {
                base.Font = value;
                graphicsFactory.Font = value;
            }
        }

        /// <summary>
        /// Gets or sets the bold font style.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Boolean.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontBold
        {
            get
            {
                return this.Font.Bold;
            }
            set
            {
                this.SetFontBold(value);
                this.graphicsFactory.FontBold = value;
            }
        }

        /// <summary>
        /// Returns the number of fonts available for the current display device or active printer.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Short.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public short FontCount
        {
            get
            {
                return this.graphicsFactory.FontCount;
            }
        }

        /// <summary>
        /// Gets or sets the italic font style.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Boolean.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontItalic
        {
            get
            {
                return this.Font.Italic;
            }
            set
            {
                this.SetFontItalic(value);
                this.graphicsFactory.FontItalic = value;
            }
        }

        /// <summary>
        /// Gets or sets the name of the font in which text is displayed for a printing operation.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a String.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string FontName
        {
            get
            {
                return this.Font.Name;
            }
            set
            {
                this.SetFontName(value);
                this.graphicsFactory.FontName = value;
            }
        }

        /// <summary>
        /// Gets or sets the size of the font that is used for text in a run-time printing operation.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Single.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float FontSize
        {
            get
            {
                return this.graphicsFactory.FontSize;
            }
            set
            {
                this.SetFontSize(value);
                this.graphicsFactory.FontSize = value;
            }
        }

        /// <summary>
        /// Gets or sets the strikethrough font style.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Boolean.
        /// </returns>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontStrikethru
        {
            get
            {
                return this.graphicsFactory.FontStrikethru;
            }
            set
            {
                this.graphicsFactory.FontStrikethru = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(null)]
        public Image DragIcon
        {
            get
            {
                return dragIcon;
            }
            set
            {
                dragIcon = value;
            }

        }


        /// <summary>
        /// Gets or sets a value that determines whether background graphics on a Printer object are printed behind text characters.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Boolean.
        /// </returns>
        public bool FontTransparent
        {
            get
            {
                return this.graphicsFactory.FontTransparent;
            }
            set
            {
                this.graphicsFactory.FontTransparent = value;
            }
        }

        /// <summary>
        /// Gets or sets the underlined font style.
        /// </summary>
        /// 
        /// <returns>
        /// Returns a Boolean.
        /// </returns>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontUnderline
        {
            get
            {
                return this.Font.Underline;
            }
            set
            {
                this.SetFontUnderline(value);
                this.graphicsFactory.FontUnderline = value;
            }
        }

        /// <summary>
        /// Gets/sets the Picture of the FCPictureBox
        /// VB.PictureBox has both properties: Image and Picture
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public Image Picture
        {
            get
            {
                return this.Image;
            }
            set
            {
                if (!object.Equals(this.Image, value))
                {
                    this.Image = value;
                    SetSizeFromImage(value);
                }
            }

        }

        /// <summary>
        /// Returns or sets a value that determines the appearance of output from graphics method or the appearance of a Shape or Line control.
        /// object.DrawMode [= number]
        /// Number	An integer that specifies appearance, as described in Settings.
        /// Settings
        /// The settings for number are:
        /// Constant	Setting	Description
        /// VbBlackness	1	Blackness.
        /// VbNotMergePen	2	Not Merge Pen Inverse of setting 15 (Merge Pen).
        /// VbMaskNotPen	3	Mask Not Pen Combination of the colors common to the background color and the inverse of the pen.
        /// VbNotCopyPen	4	Not Copy Pen Inverse of setting 13 (Copy Pen).
        /// VbMaskPenNot	5	Mask Pen Not Combination of the colors common to both the pen and the inverse of the display.
        /// VbInvert	6	Invert Inverse of the display color.
        /// VbXorPen	7	Xor Pen Combination of the colors in the pen and in the display color, but not in both.
        /// VbNotMaskPen	8	Not Mask Pen Inverse of setting 9 (Mask Pen).
        /// VbMaskPen	9	Mask Pen Combination of the colors common to both the pen and the display.
        /// VbNotXorPen	10	Not Xor Pen Inverse of setting 7 (Xor Pen).
        /// VbNop	11	Nop No operation output remains unchanged. In effect, this setting turns drawing off.
        /// VbMergeNotPen	12	Merge Not Pen Combination of the display color and the inverse of the pen color.
        /// VbCopyPen	13	Copy Pen (Default) Color specified by the ForeColor property.
        /// VbMergePenNot	14	Merge Pen Not Combination of the pen color and the inverse of the display color.
        /// VbMergePen	15	Merge Pen Combination of the pen color and the display color.
        /// VbWhiteness	16	Whiteness.
        /// Remarks
        /// Use this property to produce visual effects with Shape or Line controls or when drawing with the graphics methods. Visual Basic compares each pixel in the draw pattern to the corresponding 
        /// pixel in the existing background and then applies bit-wise operations. For example, setting 7 (Xor Pen) uses the Xor operator to combine a draw pattern pixel with a background pixel.
        /// The exact effect of a DrawMode setting depends on the way the color of a line drawn at run time combines with colors already on the screen. Settings 1, 6, 7, 11, 13, and 16 yield the most 
        /// predictable results.
        /// </summary>
        [DefaultValue(DrawModeConstants.VbCopyPen)]
        public DrawModeConstants DrawMode
        {
            // TODO:CHE
            get
            {
                return this.drawMode;
            }
            set
            {
                vbNotXorPenModeActive = false;
                this.drawMode = value;
            }
        }

        /// <summary>
        /// Returns or sets the Width of a PictureBox
        /// </summary>
        public new int Width
        {
            get
            {
                return base.Width;
            }
            set
            {
                if (SizeMode == PictureBoxSizeMode.AutoSize)
                {
                    SizeMode = PictureBoxSizeMode.Normal;
                }
                base.Width = value;
            }
        }

        /// <summary>
        /// Returns or sets the Height of a PictureBox
        /// </summary>
        public new int Height
        {
            get
            {
                return base.Height;
            }
            set
            {
                if (SizeMode == PictureBoxSizeMode.AutoSize)
                {
                    SizeMode = PictureBoxSizeMode.Normal;
                }
                base.Height = value;
            }
        }

        public Graphics CurrentGraphics
        {
            get
            {
                if(currentGraphics == null)
                {
                    CurrentGraphics = GetGraphics();
                }
                return currentGraphics;
            }

            set
            {
                currentGraphics = value;
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
        /// Returns the width of a text string as it would be printed in the current font of a Form, PictureBox, or Printer. Doesn't support named arguments.
        /// Syntax
        /// object.TextWidth(string)
        /// The TextWidth method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// String	Required. A string expression that evaluates to a string for which the text width is determined. Parentheses must surround the string expression.
        /// Remarks
        /// The width is expressed in terms of the ScaleMode property setting or Scale method coordinate system in effect for object. Use TextWidth to determine the amount 
        /// of horizontal space required to display the text. If string contains embedded carriage returns, TextWidth returns the width of the longest line.
        /// </summary>
        /// <param name="text"></param>
        /// <returns>The width of the text in twips</returns>
        public float TextWidth(string text)
        {
            //using (Graphics g = GetGraphics())
            //{
                return fcGraphics.TextWidth(CurrentGraphics, text, this.ClientRectangle);
            //}
        }

        /// <summary>
        /// Returns the height of a text string as it would be printed in the current font of a Form, PictureBox, or Printer. Doesn't support named arguments.
        /// Syntax
        /// object.TextHeight(string)
        /// The TextHeight method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the Form object with the focus is assumed to be object.
        /// String	Required. A string expression that evaluates to a string for which the text height is determined. Parentheses must enclose the string expression.
        /// Remarks
        /// The height is expressed in terms of the ScaleMode property setting or Scale method coordinate system in effect for object. Use TextHeight to determine the amount of 
        /// vertical space required to display the text. The height returned includes the normal leading space above and below the text, so you can use the height to calculate 
        /// and position multiple lines of text within object.
        /// If string contains embedded carriage returns, TextHeight returns the cumulative height of the lines, including the leading space above and below each line.
        /// </summary>
        /// <param name="text"></param>
        /// <returns>The height of the text in twips</returns>
        public float TextHeight(string text)
        {
            //using (Graphics g = GetGraphics())
            //{
                return fcGraphics.TextHeight(CurrentGraphics, text, this.ClientRectangle);
            //}
        }

        /// <summary>
        /// Prints text to a page.
        /// </summary>
        /// <param name="args">A parameter array containing optional printing parameters.</param>
        public void Print(params object[] args)
        {
            float currentX = CurrentX, currentY = CurrentY;
            fcGraphics.Output(CurrentGraphics, this.Bounds, ref currentX, ref currentY, this, true, args);
            CurrentX = currentX;
            CurrentY = currentY;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        public void Scale(int x1, int y1, int x2, int y2)
        {
            this.ScaleLeft = x1;
            this.ScaleTop = y1;
            this.ScaleWidth = x2 - x1;
            this.ScaleHeight = y2 - y1;
        }

		public void SetClipRegion(Point[] points, int length, int fillMode)
		{
			//Only first values are used, the others are set to 0
			Point[] usedPoints = new Point[length];
			for (int i = 0; i < length; i++)
			{
				usedPoints[i] = points[i];
			}
			ClipPath = new System.Drawing.Drawing2D.GraphicsPath();
			ClipPath.AddPolygon(usedPoints); 
		}

        // MW general Scale function needed 
        public double ScaleX(double val, ScaleModeConstants scaleModeFrom, ScaleModeConstants scaleModeTo)
        {
            //using (Graphics g = GetGraphics())
            //{
                fcGraphics.InitializeCoordinateSpace(CurrentGraphics, this.Bounds, 0, 0);
                return (float)fcGraphics.ScaleX((float)val, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this));
            //}
        }

        /// <summary>
        /// Prints lines on a page.
        /// </summary>
        /// <param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. The starting point for the line is determined by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> property values.</param><param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. The starting point for the line is determined by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> property values.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Line(double x2, double y2)
        {
            this.Line(true, 0.0f, 0.0f, false, x2, y2, -1, false, false);
        }

        /// <summary>
        /// Prints lines, squares, or rectangles on a page.
        /// </summary>
        /// <param name="x1">Single value indicating the horizontal coordinate of the starting point for the line being printed. </param><param name="y1">Single value indicating the vertical coordinate of the starting point for the line being printed. </param><param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. </param><param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. </param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the line. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="box">Optional. Boolean. If this parameter is set to true, a rectangle is printed. The <paramref name="x1"/>, <paramref name="y1"/>, <paramref name="x2"/>, and <paramref name="y2"/> coordinates specify opposite corners of the rectangle.</param><param name="fill">Optional. Boolean. If the <paramref name="box"/> parameter is used and the <paramref name="fill"/> parameter is set to true, the rectangle is filled with the same color used to print the rectangle. You cannot use <paramref name="fill"/> without <paramref name="box"/>. If <paramref name="box"/> is used without <paramref name="fill"/>, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillColor"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> values are used to fill the rectangle. The default value for <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> is transparent.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Line(double x1, double y1, double x2, double y2, int color = -1, bool box = false, bool fill = false)
        {
            this.Line(false, x1, y1, false, x2, y2, color, box, fill);
        }

		/// <summary>
		/// Prints lines, squares, or rectangles on a page.
		/// </summary>
		/// <param name="x1">Single value indicating the horizontal coordinate of the starting point for the line being printed. </param><param name="y1">Single value indicating the vertical coordinate of the starting point for the line being printed. </param><param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. </param><param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. </param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the line. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="box">Optional. Boolean. If this parameter is set to true, a rectangle is printed. The <paramref name="x1"/>, <paramref name="y1"/>, <paramref name="x2"/>, and <paramref name="y2"/> coordinates specify opposite corners of the rectangle.</param><param name="fill">Optional. Boolean. If the <paramref name="box"/> parameter is used and the <paramref name="fill"/> parameter is set to true, the rectangle is filled with the same color used to print the rectangle. You cannot use <paramref name="fill"/> without <paramref name="box"/>. If <paramref name="box"/> is used without <paramref name="fill"/>, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillColor"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> values are used to fill the rectangle. The default value for <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> is transparent.</param>
		[SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
		[SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
		public void LineWithStep(double x1, double y1, double x2, double y2, int color = -1, bool box = false, bool fill = false)
		{
			//If in the original code the Line is called with Step(x2, y2), they are relative to the first parameters
			x2 += x1;
			y2 += y1;
			this.Line(false, x1, y1, false, x2, y2, color, box, fill);
		}

        /// <summary>
        /// get Graphics
        /// </summary>
        /// <returns></returns>
        public Graphics GetGraphics()
        {
            Image image = null;
            if (this.Image == null)
            {
                try
                {
                    image = new Bitmap(this.Width, this.Height);
                }
                catch
                {
                    //if an Image is too large a lot of Memory is allocated, if the Program can do it. Otherwise an Exception is thrown
                    image = new Bitmap(640, 480, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
                }
            }
            else
            {
                image = this.Image;
            }

            Graphics graphic = null;

            if (image.PixelFormat == System.Drawing.Imaging.PixelFormat.Format32bppArgb || image.PixelFormat == System.Drawing.Imaging.PixelFormat.Format24bppRgb)
            {
                graphic = Graphics.FromImage(image);
            }
            else
            {
                Bitmap newBitmap = new Bitmap(this.Width, this.Height);
                graphic = Graphics.FromImage(newBitmap);
                graphic.DrawImage(image, 0, 0);
                FCUtils.DisposeBitmap(ref newBitmap);
            }

            graphic.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;

            //try
            //{
            //    graphic = Graphics.FromImage(this.Image);
            //}
            //catch(Exception ex)
            //{
            //    //ex.Message = "A Graphics object cannot be created from an image that has an indexed pixel format."
            //    Bitmap newBitmap = new Bitmap(this.Width, this.Height);
            //    graphic = Graphics.FromImage(newBitmap);
            //    graphic.DrawImage(this.Image, 0, 0);
            //}
            //finally
            //{
            //    graphic.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixelGridFit;
            //}
            return graphic;
        }

        /// <summary>
        /// Prints lines, squares, or rectangles on a page.
        /// </summary>
        /// <param name="relativeStart">Boolean. If this parameter is set to true, the starting coordinates are relative to the coordinates given by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> properties of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer"/> object.</param><param name="x1">Single value indicating the horizontal coordinate of the starting point for the line being printed.</param><param name="y1">Single value indicating the vertical coordinate of the starting point for the line being printed.</param><param name="relativeEnd">Boolean. If this parameter is set to true, the ending coordinates are relative to the coordinates given by the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> properties of the <see cref="T:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer"/> object.</param><param name="x2">Single value indicating the horizontal coordinate of the endpoint for the line being printed. </param><param name="y2">Single value indicating the vertical coordinate of the endpoint for the line being printed. </param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the line. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="box">Optional. Boolean. If this parameter is set to true, a rectangle is printed. The <paramref name="x1"/>, <paramref name="y1"/>, <paramref name="x2"/>, and <paramref name="y2"/> coordinates specify opposite corners of the rectangle.</param><param name="fill">Optional. Boolean. If the <paramref name="box"/> parameter is used and the <paramref name="fill"/> parameter is set to true, the rectangle is filled with the same color used to print the rectangle. you cannot use <paramref name="fill"/> without <paramref name="box"/>. If <paramref name="box"/> is used without <paramref name="fill"/>, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillColor"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> values are used to fill the rectangle. The default value for <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.FillStyle"/> is transparent.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        public void Line(bool relativeStart, double x1, double y1, bool relativeEnd, double x2, double y2, int color = -1, bool box = false, bool fill = false)
        {
            //using (Graphics g = GetGraphics())
            //{
                float currentX = CurrentX;
                float currentY = CurrentY;
                bool invert = (this.DrawMode == DrawModeConstants.VbInvert) ? true : false;
                fcGraphics.LineInternal(CurrentGraphics, this.Bounds, ref currentX, ref currentY, relativeStart, (float)x1, (float)y1, relativeEnd, (float)x2, (float)y2, color, box, fill, invert, this);
                CurrentX = currentX;
                CurrentY = currentY;
            //}
        }

        /// <summary>
        /// Draws lines and rectangles on an object.
        /// Syntax
        /// object.Line [Step] (x1, y1) [Step] - (x2, y2), [color], [B][F]
        /// The Line method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the starting point coordinates are relative to the current graphics position given by the CurrentX and CurrentY properties.
        /// (x1, y1)	Optional. Single values indicating the coordinates of the starting point for the line or rectangle.  The ScaleMode property determines the unit of measure used.  
        /// If omitted, the line begins at the position indicated by CurrentX and CurrentY.
        /// Step	Optional. Keyword specifying that the end point coordinates are relative to the line starting point.
        /// (x2, y2)	Required. Single values indicating the coordinates of the end point for the line being drawn.
        /// color	Optional. Long integer value indicating the RGB color used to draw the line. If omitted, the ForeColor property setting is used. You can use the RGB function or 
        /// QBColor function to specify the color.
        /// B	Optional. If included, causes a box to be drawn using the coordinates to specify opposite corners of the box.
        /// F	Optional. If the B option is used, the F option specifies that the box is filled with the same color used to draw the box. You cannot use F without B. If B is used 
        /// without F, the box is filled with the current FillColor and FillStyle. The default value for FillStyle is transparent.
        /// Remarks
        /// To draw connected lines, begin a subsequent line at the end point of the previous line.
        /// The width of the line drawn depends on the setting of the DrawWidth property. The way a line or box is drawn on the background depends on the setting of the DrawMode 
        /// and DrawStyle properties.
        /// When Line executes, the CurrentX and CurrentY properties are set to the end point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="StepPos1"></param>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <param name="StepPos2"></param>
        /// <param name="X2"></param>
        /// <param name="Y2"></param>
        /// <param name="Color"></param>
        /// <param name="Rectangle"></param>
        public void Line(LineDrawStep StepPos1, float X1, float Y1, LineDrawStep StepPos2, float X2, float Y2, Color Color, LineDrawConstants Rectangle)
        {
            this.Line(StepPos1 == LineDrawStep.Absolute ? false : true, X1, Y1, StepPos2 == LineDrawStep.Absolute ? false : true, X2, Y2, System.Drawing.ColorTranslator.ToOle(Color), Rectangle == LineDrawConstants.B || Rectangle == LineDrawConstants.BF, Rectangle == LineDrawConstants.BF);
        }

        /// <summary>
        /// Draws lines and rectangles on an object.
        /// Syntax
        /// object.Line [Step] (x1, y1) [Step] - (x2, y2), [color], [B][F]
        /// The Line method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the starting point coordinates are relative to the current graphics position given by the CurrentX and CurrentY properties.
        /// (x1, y1)	Optional. Single values indicating the coordinates of the starting point for the line or rectangle.  The ScaleMode property determines the unit of measure used.  
        /// If omitted, the line begins at the position indicated by CurrentX and CurrentY.
        /// Step	Optional. Keyword specifying that the end point coordinates are relative to the line starting point.
        /// (x2, y2)	Required. Single values indicating the coordinates of the end point for the line being drawn.
        /// color	Optional. Long integer value indicating the RGB color used to draw the line. If omitted, the ForeColor property setting is used. You can use the RGB function or 
        /// QBColor function to specify the color.
        /// B	Optional. If included, causes a box to be drawn using the coordinates to specify opposite corners of the box.
        /// F	Optional. If the B option is used, the F option specifies that the box is filled with the same color used to draw the box. You cannot use F without B. If B is used 
        /// without F, the box is filled with the current FillColor and FillStyle. The default value for FillStyle is transparent.
        /// Remarks
        /// To draw connected lines, begin a subsequent line at the end point of the previous line.
        /// The width of the line drawn depends on the setting of the DrawWidth property. The way a line or box is drawn on the background depends on the setting of the DrawMode 
        /// and DrawStyle properties.
        /// When Line executes, the CurrentX and CurrentY properties are set to the end point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="StepPos1"></param>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <param name="StepPos2"></param>
        /// <param name="X2"></param>
        /// <param name="Y2"></param>
        /// <param name="Color"></param>
        /// <param name="Rectangle"></param>
        public void Line(LineDrawStep StepPos1, float X1, float Y1, LineDrawStep StepPos2, float X2, float Y2, int Color, LineDrawConstants Rectangle)
        {
            this.Line(StepPos1, X1, Y1, StepPos2, X2, Y2, ColorTranslator.FromOle(Color), Rectangle);
        }

        /// <summary>
        /// Draws lines and rectangles on an object.
        /// Syntax
        /// object.Line [Step] (x1, y1) [Step] - (x2, y2), [color], [B][F]
        /// The Line method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the starting point coordinates are relative to the current graphics position given by the CurrentX and CurrentY properties.
        /// (x1, y1)	Optional. Single values indicating the coordinates of the starting point for the line or rectangle.  The ScaleMode property determines the unit of measure used.  
        /// If omitted, the line begins at the position indicated by CurrentX and CurrentY.
        /// Step	Optional. Keyword specifying that the end point coordinates are relative to the line starting point.
        /// (x2, y2)	Required. Single values indicating the coordinates of the end point for the line being drawn.
        /// color	Optional. Long integer value indicating the RGB color used to draw the line. If omitted, the ForeColor property setting is used. You can use the RGB function or 
        /// QBColor function to specify the color.
        /// B	Optional. If included, causes a box to be drawn using the coordinates to specify opposite corners of the box.
        /// F	Optional. If the B option is used, the F option specifies that the box is filled with the same color used to draw the box. You cannot use F without B. If B is used 
        /// without F, the box is filled with the current FillColor and FillStyle. The default value for FillStyle is transparent.
        /// Remarks
        /// To draw connected lines, begin a subsequent line at the end point of the previous line.
        /// The width of the line drawn depends on the setting of the DrawWidth property. The way a line or box is drawn on the background depends on the setting of the DrawMode 
        /// and DrawStyle properties.
        /// When Line executes, the CurrentX and CurrentY properties are set to the end point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="StepPos1"></param>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <param name="StepPos2"></param>
        /// <param name="X2"></param>
        /// <param name="Y2"></param>
        public void Line(LineDrawStep StepPos1, float X1, float Y1, LineDrawStep StepPos2, float X2, float Y2)
        {
            Color Color = default(Color);
            Line(StepPos1, X1, Y1, StepPos2, X2, Y2, Color);
        }

        /// <summary>
        /// Draws lines and rectangles on an object.
        /// Syntax
        /// object.Line [Step] (x1, y1) [Step] - (x2, y2), [color], [B][F]
        /// The Line method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the starting point coordinates are relative to the current graphics position given by the CurrentX and CurrentY properties.
        /// (x1, y1)	Optional. Single values indicating the coordinates of the starting point for the line or rectangle.  The ScaleMode property determines the unit of measure used.  
        /// If omitted, the line begins at the position indicated by CurrentX and CurrentY.
        /// Step	Optional. Keyword specifying that the end point coordinates are relative to the line starting point.
        /// (x2, y2)	Required. Single values indicating the coordinates of the end point for the line being drawn.
        /// color	Optional. Long integer value indicating the RGB color used to draw the line. If omitted, the ForeColor property setting is used. You can use the RGB function or 
        /// QBColor function to specify the color.
        /// B	Optional. If included, causes a box to be drawn using the coordinates to specify opposite corners of the box.
        /// F	Optional. If the B option is used, the F option specifies that the box is filled with the same color used to draw the box. You cannot use F without B. If B is used 
        /// without F, the box is filled with the current FillColor and FillStyle. The default value for FillStyle is transparent.
        /// Remarks
        /// To draw connected lines, begin a subsequent line at the end point of the previous line.
        /// The width of the line drawn depends on the setting of the DrawWidth property. The way a line or box is drawn on the background depends on the setting of the DrawMode 
        /// and DrawStyle properties.
        /// When Line executes, the CurrentX and CurrentY properties are set to the end point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="StepPos1"></param>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <param name="StepPos2"></param>
        /// <param name="X2"></param>
        /// <param name="Y2"></param>
        /// <param name="Color"></param>
        public void Line(LineDrawStep StepPos1, float X1, float Y1, LineDrawStep StepPos2, float X2, float Y2, Color Color)
        {
            LineDrawConstants Rectangle = LineDrawConstants.L;
            Line(StepPos1, X1, Y1, StepPos2, X2, Y2, Color, Rectangle);

        }

        /// <summary>
        /// Draws lines and rectangles on an object.
        /// Syntax
        /// object.Line [Step] (x1, y1) [Step] - (x2, y2), [color], [B][F]
        /// The Line method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the starting point coordinates are relative to the current graphics position given by the CurrentX and CurrentY properties.
        /// (x1, y1)	Optional. Single values indicating the coordinates of the starting point for the line or rectangle.  The ScaleMode property determines the unit of measure used.  
        /// If omitted, the line begins at the position indicated by CurrentX and CurrentY.
        /// Step	Optional. Keyword specifying that the end point coordinates are relative to the line starting point.
        /// (x2, y2)	Required. Single values indicating the coordinates of the end point for the line being drawn.
        /// color	Optional. Long integer value indicating the RGB color used to draw the line. If omitted, the ForeColor property setting is used. You can use the RGB function or 
        /// QBColor function to specify the color.
        /// B	Optional. If included, causes a box to be drawn using the coordinates to specify opposite corners of the box.
        /// F	Optional. If the B option is used, the F option specifies that the box is filled with the same color used to draw the box. You cannot use F without B. If B is used 
        /// without F, the box is filled with the current FillColor and FillStyle. The default value for FillStyle is transparent.
        /// Remarks
        /// To draw connected lines, begin a subsequent line at the end point of the previous line.
        /// The width of the line drawn depends on the setting of the DrawWidth property. The way a line or box is drawn on the background depends on the setting of the DrawMode 
        /// and DrawStyle properties.
        /// When Line executes, the CurrentX and CurrentY properties are set to the end point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="StepPos1"></param>
        /// <param name="X1"></param>
        /// <param name="Y1"></param>
        /// <param name="StepPos2"></param>
        /// <param name="X2"></param>
        /// <param name="Y2"></param>
        /// <param name="Color"></param>
        public void Line(LineDrawStep StepPos1, float X1, float Y1, LineDrawStep StepPos2, float X2, float Y2, int Color)
        {
            this.Line(StepPos1, X1, Y1, StepPos2, X2, Y2, ColorTranslator.FromOle(Color));
        }

        /// <summary>
        /// Prints the contents of an image file on a page.
        /// </summary>
        /// <param name="picture"><see cref="T:System.Drawing.Image"/> value representing the image to be printed.</param><param name="x1">Single value indicating the horizontal destination coordinates where the image will be printed. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measure used.</param><param name="y1">Single value indicating the vertical destination coordinates where the image will be printed. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measure used.</param><param name="width1">Optional. Single value indicating the destination width of the picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If the destination width is larger or smaller than the source width, picture is stretched or compressed to fit. If omitted, the source width is used.</param><param name="height1">Optional. Single value indicating the destination height of the picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If the destination height is larger or smaller than the source height, picture is stretched or compressed to fit. If omitted, the source height is used.</param><param name="x2">Optional. Single values indicating the coordinates (x-axis) of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If omitted, 0 is assumed.</param><param name="y2">Optional. Single values indicating the coordinates (y-axis) of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If omitted, 0 is assumed.</param><param name="width2">Optional. Single value indicating the source width of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If omitted, the entire source width is used.</param><param name="height2">Optional. Single value indicating the source height of a clipping region within picture. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property of object determines the units of measurement used. If omitted, the entire source height is used.</param>
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [MethodImpl(MethodImplOptions.NoInlining | MethodImplOptions.NoOptimization)]
        public void PaintPicture(Image objPicture, double intX1, double intY1, double? intWidth1 = null, double? intHeight1 = null, double? intX2 = null, double? intY2 = null, double? intWidth2 = null, double? intHeight2 = null, long? lngOpcode = null)
        {
            if (intWidth1 == null)
            {
                intWidth1 = float.NaN;
            }
            if (intHeight1 == null)
            {
                intHeight1 = float.NaN;
            }
            if (intX2 == null)
            {
                intX2 = 0;
            }
            if (intY2 == null)
            {
                intY2 = 0;
            }
            if (intWidth2 == null)
            {
                intWidth2 = float.NaN;
            }
            if (intHeight2 == null)
            {
                intHeight2 = float.NaN;
            }
            if (lngOpcode == null)
            {
                lngOpcode = 0;
            }
            //using (Graphics g = GetGraphics())
            //{
                fcGraphics.PaintPicture(CurrentGraphics, this.Bounds, CurrentX, CurrentY, objPicture, (float)intX1, (float)intY1, (float)intWidth1, (float)intHeight1, (float)intX2, (float)intY2, (float)intWidth2, (float)intHeight2);
            //}
        }

        /// <summary>
        /// Draws a circle, ellipse, or arc on an object.
        /// Syntax
        /// object.Circle [Step] (x, y), radius, [color, start, end, aspect]
        /// The Circle method syntax has the following object qualifier and parts:
        /// Part	Description
        /// object	Optional. Object expression that evaluates to an object in the Applies To list. If object is omitted, the Form with the focus is assumed to be object.
        /// Step	Optional. Keyword specifying that the center of the circle, ellipse, or arc is relative to the current coordinates given by the CurrentX and CurrentY 
        /// properties of object.
        /// (x, y)	Required. Single values indicating the coordinates for the center point of the circle, ellipse, or arc. The ScaleMode property of object determines the 
        /// units of measure used.
        /// radius	Required. Single value indicating the radius of the circle, ellipse, or arc. The ScaleMode property of object determines the unit of measure used.
        /// color	Optional. Long integer value indicating the RGB color of the circle's outline. If omitted, the value of the ForeColor property is used. You can use the 
        /// RGB function or QBColor function to specify the color.
        /// start, end	Optional. Single-precision values. When an arc or a partial circle or ellipse is drawn, start and end specify (in radians) the beginning and end positions 
        /// of the arc. The range for both is 2 pi radians to 2 pi radians. The default value for start is 0 radians; the default for end is 2 * pi radians.
        /// aspect	Optional. Single-precision value indicating the aspect ratio of the circle. The default value is 1.0, which yields a perfect (non-elliptical) circle on any screen.
        /// Remarks
        /// To fill a circle, set the FillColor and FillStyle properties of the object on which the circle or ellipse is drawn. Only a closed figure can be filled. Closed figures 
        /// include circles, ellipses, or pie slices (arcs with radius lines drawn at both ends).
        /// When drawing a partial circle or ellipse, if start is negative, Circle draws a radius to start, and treats the angle as positive; if end is negative, Circle draws a radius 
        /// to end and treats the angle as positive. The Circle method always draws in a counter-clockwise (positive) direction.
        /// The width of the line used to draw the circle, ellipse, or arc depends on the setting of the DrawWidth property. The way the circle is drawn on the background depends on 
        /// the setting of the DrawMode and DrawStyle properties.
        /// When drawing pie slices, to draw a radius to angle 0 (giving a horizontal line segment to the right), specify a very small negative value for start, rather than zero.
        /// You can omit an argument in the middle of the syntax, but you must include the argument's comma before including the next argument. If you omit an optional argument, 
        /// omit the comma following the last argument you specify.
        /// When Circle executes, the CurrentX and CurrentY properties are set to the center point specified by the arguments.
        /// This method cannot be used in an WithEnd With block.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="step1"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="radius"></param>
        /// <param name="color"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="aspect"></param>
        public void Circle(LineDrawStep step1, float x, float y, float radius, Color color, float start, float end, float aspect)
        {
            this.Circle(step1 == LineDrawStep.Relative, x, y, radius, ColorTranslator.ToOle(color), start, end, aspect);
        }

        /// <summary>
        /// Prints a circle, an ellipse, or an arc on a page.
        /// </summary>
        /// <param name="x">Single value indicating the horizontal coordinate for the center point of the circle, ellipse, or arc. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="y">Single value indicating the vertical coordinate for the center point of the circle, ellipse, or arc. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="radius">Single value indicating the radius of the circle or ellipse. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the circle's outline. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="startAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startAngle"/> and <paramref name="endAngle"/> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle"/> is 0 radians; the default for <paramref name="endAngle"/> is 2 * pi radians.</param><param name="endAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startAngle"/> and <paramref name="endAngle"/> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle"/> is 0 radians; the default for <paramref name="endAngle"/> is 2 * pi radians.</param><param name="aspect">Optional. Single-precision value indicating the aspect ratio of the circle or ellipse. The default value is 1.0, which yields a perfect (non-elliptical) circle on any screen.</param>
        [SuppressMessage("Microsoft.Design", "CA1026:DefaultParametersShouldNotBeUsed")]
        [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly")]
        public void Circle(double x, double y, double radius, int color = -1, double startAngle = float.NaN, double endAngle = float.NaN, double aspect = 1f)
        {
            this.Circle(false, x, y, radius, color, startAngle, endAngle, aspect);
        }

        /// <summary>
        /// Prints a circle, an ellipse, or an arc on a page, specifying whether the center point is relative to the current location.
        /// </summary>
        /// <param name="relativeStart">Boolean. If this parameter is set to true, the center of the circle, ellipse, or arc is printed relative to the coordinates specified in the <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/> and <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/> properties of the object.</param><param name="x">Single value indicating the vertical coordinate for the center point of the circle, ellipse, or arc. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="y">Single value indicating the radius of the circle or ellipse. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="radius">Single value indicating the radius of the circle or ellipse. The <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ScaleMode"/> property determines the units of measurement used.</param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color of the circle's outline. If this parameter is omitted, the value of <see cref="P:System.Drawing.Color.Black"/> is used.</param><param name="startAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startangle"/> and <paramref name="endAngle"/> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle"/> is 0 radians; the default for <paramref name="endAngle"/> is 2 * pi radians.</param><param name="endAngle">Optional. Single-precision value. When an arc or a partial circle or ellipse is printed, <paramref name="startAngle"/> and <paramref name="endAngle"/> specify (in radians) the start and end positions of the arc. The range for both is 2 * pi radians to 2 * pi radians. The default value for <paramref name="startAngle"/> is 0 radians; the default for <paramref name="endAngle"/> is 2 * pi radians.</param><param name="aspect">Optional. Single-precision value indicating the aspect ratio of the circle or ellipse. The default value is 1.0, which yields a perfect (non-elliptical) circle on any screen.</param>
        public void Circle(bool relativeStart, double x, double y, double radius, int color = -1, double startAngle = float.NaN, double endAngle = float.NaN, double aspect = 1f)
        {
            //using (Graphics g = GetGraphics())
            //{
                fcGraphics.Circle(CurrentGraphics, relativeStart, (float)x, (float)y, (float)radius, color, (float)startAngle, (float)endAngle, (float)aspect);
            //}
        }

        /// <summary>
        /// Clears graphics and text generated at run time from a Form or PictureBox.
        /// Remarks
        /// Cls clears text and graphics generated at run time by graphics and printing statements. Background bitmaps set using the Picture property and controls placed on a Form at design time 
        /// aren't affected by Cls. Graphics and text placed on a Form or PictureBox while the AutoRedraw property is set to True aren't affected if AutoRedraw is set to False before Cls is invoked. 
        /// That is, you can maintain text and graphics on a Form or PictureBox by manipulating the AutoRedraw property of the object you're working with.
        /// After Cls is invoked, the CurrentX and CurrentY properties of object are reset to 0.
        /// </summary>
        /// <param name="control"></param>
        public void Cls()
        {
            //Have to reset the image because the size of the control might have been changed
            if (allowImageNull)
            {
                this.Image = null;

                //using (Graphics g = GetGraphics())
                //{
                CurrentGraphics.Clear(this.BackColor);
                //}
            }
            CurrentX = 0;
            CurrentY = 0;
        }

        /// <summary>
        /// Prints a single point in a specified color on a page, optionally specifying a point relative to the current coordinates.
        /// </summary>
        /// <param name="relativeStart">Boolean value indicating whether the coordinates are relative to the current graphics position (as set by <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentX"/>, <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.CurrentY"/>).</param><param name="x">Single value indicating the horizontal coordinates of the point to print.</param><param name="y">Single value indicating the vertical coordinates of the point to print.</param><param name="color">Optional. Integer value indicating the RGB (red-green-blue) color specified for the point. If this parameter is omitted, the current <see cref="P:Microsoft.VisualBasic.PowerPacks.Printing.Compatibility.VB6.Printer.ForeColor"/> property setting is used.</param>
        public void PSet(bool relativeStart, double x, double y, int color)
        {
            
            //fcGraphics.PSet(GetGraphics(), relativeStart, (float)x, (float)y, color);
            fcGraphics.PSet(CurrentGraphics, relativeStart, (float)x, (float)y, color);
        }

        //CHE: use Color because ColorTranslator.ToOle is testing if the color is a standard system color otherwise it's using ToWin32 conversion which loses the alpha component for transparency
        public void PSet(bool relativeStart, double x, double y, Color? color)
        {
			if (color.Value.A == 0)
			{
				color = this.BackColor;
			}
            //fcGraphics.PSet(GetGraphics(), relativeStart, (float)x, (float)y, color);
            fcGraphics.PSet(CurrentGraphics, relativeStart, (float)x, (float)y, color);
        }

        /// <summary>
        /// Returns, as a long integer, the red-green-blue (RGB) color of the specified point on a Form or PictureBox. Doesn't support named arguments.
        /// object.Point(x, y)
        /// object Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the Form object with the focus is assumed to be object. 
        /// x, y Required. Single-precision values indicating the horizontal (x-axis) and vertical (y-axis) coordinates of the point in the ScaleMode property of 
        /// the Form or PictureBox. Parentheses must enclose the values. 
        /// Remarks
        /// If the point referred to by the x and y coordinates is outside object, the Point method returns -1.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public long Point(Single x, Single y)
        {
            return fcGraphics.Point(this.Image, x, y);
        }

        //CHE: use Color because ColorTranslator.ToOle is testing if the color is a standard system color otherwise it's using ToWin32 conversion which loses the alpha component for transparency
        public Color? PointColor(Single x, Single y)
        {
            return fcGraphics.PointColor(this.Image, x, y);
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        /// <summary>
        /// replacement for TextOut Lib "gdi32"
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="text"></param>
        /// <param name="count"></param>
        public void TextOut(int x, int y, string text, int count)
        {
            float currentX = x, currentY = y;
            if (count < text.Length)
                {
                    text = text.Substring(0, count);
                }
                fcGraphics.Output(currentGraphics, this.Bounds, ref currentX, ref currentY, this, true, new object[] { text });
                CurrentX = currentX;
                CurrentY = currentY;
        }

        /// <summary>
        /// replacement for Polygon Lib "gdi32"
        /// </summary>
        /// <param name="points"></param>
        /// <param name="size"></param>
        public void Polygon(Point[] points, int size)
        {
            Point[] pointsPolygon = GetPoints(points, size);
            //using (Graphics g = GetGraphics())
            //{
                using (Pen drawPen = this.graphicsFactory.CreateDrawPen(this.graphicsFactory.ForeColor))
                {
                CurrentGraphics.DrawPolygon(drawPen, pointsPolygon);
                }
            //}
        }

   /// <summary>
   /// Draw a Line by the ForeColor of the current Object
   /// </summary>
   /// <param name="nXStart"></param>
   /// <param name="nYStart"></param>
   /// <param name="nXEnd"></param>
   /// <param name="nYEnd"></param>
        public void DrawLine(int nXStart, int nYStart, int nXEnd, int nYEnd)
        {
            //using (Graphics g = GetGraphics())
            //{
                using (Brush brush = this.graphicsFactory.CreateFillBrush())
                {
                    using (Pen pen = this.graphicsFactory.CreateDrawPen(ColorTranslator.ToOle(this.ForeColor)))
                    {
                    CurrentGraphics.DrawLine(pen, new Point(nXStart, nYStart), new Point(nXEnd, nYEnd));

                    }
                }
            //}
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="points"></param>
        /// <param name="size"></param>
        public void FillPolygon(Point[] points, int size)
        {
            if (size > 0)
            {
                Point[] pointsPolygon = GetPoints(points, size);
                //using (Graphics g = GetGraphics())
                //{
                    using (Brush brush = this.graphicsFactory.CreateFillBrush())
                    {
                    CurrentGraphics.FillPolygon(brush, pointsPolygon);
                    }
                    if ((DrawStyleConstants)this.DrawStyle != DrawStyleConstants.vbInvisible)
                    {
                        Color color = TransformColorVbNotXorPen();
                        using (Pen pen = this.graphicsFactory.CreateDrawPen(ColorTranslator.ToOle(color)))
                        {
                        currentGraphics.DrawPolygon(pen, pointsPolygon);
                        }
                    }
                //}
            }
        }


        /// <summary>
        /// Release the Hdc of CurrentGraphics
        /// </summary>
        public void ReleaseCurrentGraphicshDc()
        {
            if (hDCActive && currentGraphics != null  && !batchDrawModeActive)
            {
                currentGraphics.ReleaseHdc();
                hDCActive = false;
            }
        }

        /// <summary>
        /// Get hDc of currentGraphics-Object
        /// </summary>
        /// <returns></returns>
        public IntPtr GetHdc_CurrentGraphics()
        {
            IntPtr ptr = CurrentGraphics.GetHdc();
            hDCActive = true;
            return ptr;
        }

        /// <summary>
        /// Release the Graphics-Object
        /// </summary>
        public void ReleaseGraphics()
        {
            if (this.currentGraphics != null)
            {
                this.ReleaseCurrentGraphicshDc();
                this.currentGraphics.Dispose();
                this.currentGraphics = null;
            }
        }
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods

        protected override void OnEnter(EventArgs e)
        {
            //CHE: support for selectable PictureBox
            if (this.TabStop)
            {
                this.Invalidate();
            }
            base.OnEnter(e);
        }

        protected override void OnLeave(EventArgs e)
        {
            //CHE: support for selectable PictureBox
            if (this.TabStop)
            {
                this.Invalidate();
            }
            base.OnLeave(e);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            //CHE: support for selectable PictureBox
            if (this.TabStop)
            {
                this.Invalidate();
            }
            base.OnKeyDown(e);
        }

        protected override void OnTabStopChanged(EventArgs e)
        {
            base.OnTabStopChanged(e);
            //CHE: support for selectable PictureBox
            this.SetStyle(ControlStyles.Selectable, this.TabStop);
        }

        protected override bool IsInputKey(Keys keyData)
        {
            //CHE: support for selectable PictureBox, return true to allow left and right arrow to be caught on KeyDown event handlers
            if (this.TabStop && (keyData == Keys.Left || keyData == Keys.Right)) return true;
            return base.IsInputKey(keyData);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (mouseDownActive ||mouseUpActive)
            {
                return;
            }
            mouseDownActive = true;

            Point transformedPoint = TransformCoordinate(e.X, e.Y);
            MouseEventArgs evArg = new MouseEventArgs(e.Button, e.Clicks, transformedPoint.X, transformedPoint.Y, e.Delta);

            //CHE: support for selectable PictureBox
            if (this.TabStop)
            {
                this.Focus();
            }

            base.OnMouseDown(evArg);
            if (DragMode == DragModeConstants.vbAutomatic && e.Clicks == 1 && e.Button == MouseButtons.Left)
            {
                //Set the Source-Control for Drag&Drop-Operation
                using (Image imageDragDrop = (this.DragIcon != null) ? ((Image)this.DragIcon.Clone()) : ((Image)this.Image.Clone()))
                {
                    // TODO
                    //Utils.FCDragDrop.Init(this, imageDragDrop, Cursor.Current);
                    this.DoDragDrop(this, DragDropEffects.All);
                }
            }
            mouseDownActive = false;
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (mouseUpActive)
            {
                return;
            }
            mouseUpActive = true;

            Point transformedPoint = TransformCoordinate(e.X, e.Y);
            MouseEventArgs evArg = new MouseEventArgs(e.Button, e.Clicks, transformedPoint.X, transformedPoint.Y, e.Delta);

            base.OnMouseUp(evArg);
            mouseUpActive = false;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            //exit if mouse position was not changed
            if (prevMouseEventArgs != null && prevMouseEventArgs.X == e.X && prevMouseEventArgs.Y == e.Y)
            {
                return;
            }
            prevMouseEventArgs = e;
            Point transformedPoint = TransformCoordinate(e.X, e.Y);
            MouseEventArgs evArg = new MouseEventArgs(e.Button, e.Clicks, transformedPoint.X, transformedPoint.Y, e.Delta);
            base.OnMouseMove(evArg);
        }

        //protected override void OnDragDrop(DragEventArgs drgevent)
        //{
        //    //CHE: get position relative to sender control not to screen
        //    Point senderRelativePoint = this.PointToClient(new Point(drgevent.X, drgevent.Y));
        //    Point transformedPoint = TransformCoordinate(senderRelativePoint.X, senderRelativePoint.Y);
        //    // TODO
        //    //DragEventArgs dragEvent = new DragEventArgs(drgevent.Data, drgevent.KeyState, transformedPoint.X, transformedPoint.Y, drgevent.AllowedEffect, drgevent.Effect);
        //    //base.OnDragDrop(dragEvent);
        //    //PJU: Reset Drag&Drop-Source-Control
        //    Utils.FCDragDrop.Reset();
        //}
        //protected override void OnDragOver(DragEventArgs drgevent)
        //{
        //    Point senderRelativePoint = this.PointToClient(new Point(drgevent.X, drgevent.Y));
        //    Point transformedPoint = TransformCoordinate(senderRelativePoint.X, senderRelativePoint.Y);
        //    // TODO
        //    //DragEventArgs dragEvent = new DragEventArgs(drgevent.Data, drgevent.KeyState, transformedPoint.X, transformedPoint.Y, drgevent.AllowedEffect, drgevent.Effect);
        //    //base.OnDragOver(dragEvent);
        //}
        protected override void OnDragEnter(DragEventArgs drgevent)
        {
            drgevent.Effect = DragDropEffects.All;
            Form form = this.FindForm();
            if(Utils.FCDragDrop.DragIcon != null && form != null)
            {

                Bitmap b = new Bitmap(Utils.FCDragDrop.DragIcon);
                Graphics g = Graphics.FromImage(b);
                IntPtr ptr = b.GetHicon();
                //Cursor.Current = new Cursor(ptr);
            }
            base.OnDragEnter(drgevent);
        }

        //protected override void OnGiveFeedback(GiveFeedbackEventArgs gfbevent)
        //{
        //    if (Utils.FCDragDrop.DragIcon != null && Utils.FCDragDrop.OriginalCursor != null)
        //    {
        //        gfbevent.UseDefaultCursors = false;
        //    }
        //    base.OnGiveFeedback(gfbevent);
        //}

        protected override void OnQueryContinueDrag(QueryContinueDragEventArgs qcdevent)
        {
            if(!(qcdevent.Action == DragAction.Continue) && Utils.FCDragDrop.OriginalCursor != null)
            {
                // TODO
                //Cursor.Current = Utils.FCDragDrop.OriginalCursor;
                Utils.FCDragDrop.OriginalCursor = null;
            }
            
            base.OnQueryContinueDrag(qcdevent);
        }
        protected override void OnBackColorChanged(EventArgs e)
        {
            base.OnBackColorChanged(e);
            this.graphicsFactory.FillColor = ColorTranslator.ToOle(this.BackColor);
            if (allowImageNull)
            {
                this.Image = null;
            }
        }

        protected override void OnForeColorChanged(EventArgs e)
        {
            base.OnForeColorChanged(e);
            this.graphicsFactory.ForeColor = ColorTranslator.ToOle(this.ForeColor);
        }

        #endregion

        #region Private Methods

        private Point[] GetPoints(Point[] points, int size)
        {
            Point[] pointsPolygon = new Point[size];

            for (int i = 0; i < size; i++)
            {
                pointsPolygon[i] = points[i];
            }
            return pointsPolygon;


        }

        private Point TransformCoordinate(int X, int Y)
        {
            Point result = new Point();
            
            //using (Graphics g = GetGraphics())
            //{
                fcGraphics.InitializeCoordinateSpace(CurrentGraphics, this.Bounds, 0, 0);
                if (this.ScaleMode != ScaleModeConstants.vbUser)
                {
                    result.X = Convert.ToInt32(fcGraphics.ScaleX(X, ScaleModeConstants.vbPixels, this.ScaleMode));
                    result.Y = Convert.ToInt32(fcGraphics.ScaleY(Y, ScaleModeConstants.vbPixels, this.ScaleMode));
                }
                else
                {
                    //CHE: consider ScaleLeft/ScaleTop for ScaleMode vbUser
                    result.X = Convert.ToInt32(fcGraphics.ScaleX(X, ScaleModeConstants.vbPixels, this.ScaleMode) + this.ScaleLeft);
                    result.Y = Convert.ToInt32(fcGraphics.ScaleY(Y, ScaleModeConstants.vbPixels, this.ScaleMode) + this.ScaleTop);
                }
            //}

            return result;
        }
        /// <summary>
        /// Initializes CoordinateSpace related to the parents Control (DPI)
        /// </summary>
        private void InitializeCoordinateSpace()
        {
            Control ctrl = this.Parent;
            if (ctrl != null)
            {
                using (Graphics g = ctrl.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                }
            }
            else
            {
                using (Graphics g = GetGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                }
            }
        }

        /// <summary>
        /// Transforms the Color related to the VbNotXorPen-Mode (e.g. used for bliking Objects)
        /// </summary>
        /// <returns></returns>
        private Color TransformColorVbNotXorPen()
        {
            Color color = this.ForeColor;
            if (this.drawMode == DrawModeConstants.VbNotXorPen)
            {
                //used to draw invert the Color
                vbNotXorPenModeActive = !vbNotXorPenModeActive;
            }
            if (vbNotXorPenModeActive)
            {
              color = ColorExtensions.InvertColorFromRGB(this.ForeColor);
            }
            return color;
        }

      /// <summary>
      /// Set the Size of the Control to the image-Size 
      /// This is a VB6-bahavior: e.g. the Picture is set but AutoSize is not defined, then the Size will be taken over
      /// </summary>
      /// <param name="img"></param>
      private void SetSizeFromImage(Image img)
      {
          //return;
          if (!AutoSize && img != null && this.SizeMode != PictureBoxSizeMode.StretchImage)
          {
              this.Size = img.Size;
          }
      }
        #endregion
    }
}
