﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fecherFoundation.Extensions;

namespace fecherFoundation
{
    /// <summary>
    ///vsFlexLib.vsFlexArray
    ///http://helpcentral.componentone.com/docs/vsflexgrid8/vsflexgridpropertieseventsandmethods.htm
    ///http://wutils.com/com-dll/constants/constants-vsFlexLib.htm
    /// </summary>
    public class FCGridFlexArray : DataGridView
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private SortedDictionary<int, int> colData = new SortedDictionary<int, int>();
        private SortedDictionary<int, int> rowData = new SortedDictionary<int, int>();

        private MousePointerSettings mousePointer = MousePointerSettings.flexDefault;
        private string toolTipText = "";
        private bool fromCurrentCellChanged = false;
        private bool inRowColSetter = false;

        private AllowUserResizeSettings allowUserResizing = AllowUserResizeSettings.flexResizeNone;

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private int m_fixedColumns = 1;
        private int m_fixedRows = 1;
        //flex grid row/col values starting for 0,0 as first row/column
        private int m_currentRow = 0;
        private int m_currentColumn = 0;
        private SelModeSettings m_selectionMode = SelModeSettings.flexSelectionFree;
        private bool m_allowBigSelection = true;
        private Color m_foreColorFixed;
        private Color m_foreColorFixedBackup;
        private int m_rowSel = -1;
        private int m_colSel = -1;
        private int m_gridLineWidth = 0;
        private FocusRectSettings m_focusRect = FocusRectSettings.flexFocusLight;
        private HighLightSettings m_highLight = HighLightSettings.flexHighlightAlways;
        private ScrollBarsSettings m_scrollBars = ScrollBarsSettings.flexScrollBoth;
        // helper variable for FocusRect-implementation - only draw the rectangle when current cell has changed
        private bool m_currentCellChanged = false;
        private int m_cols = 2;
        private int m_rows = 2;
        private Dictionary<int, bool> columnsVisibility = new Dictionary<int, bool>();

        private bool disableEvents = false;

        private bool keyHandled = false;

        private int dgvColIndex = 0;
        private int dgvRowIndex = 0;

        private bool widthToZeroChanging;

        #endregion

        #region Constructors

        public FCGridFlexArray()
        {
            //CHE: default row height in VB6 is 240 twips
            this.RowTemplate.Height = 16;

            this.MouseDown += new MouseEventHandler(FCGridFlexArray_MouseDown);
            this.MouseUp += new MouseEventHandler(FCGridFlexArray_MouseUp);
            this.CurrentCellChanged += new EventHandler(FCGridFlexArray_CurrentCellChanged);
            this.CellPainting += new DataGridViewCellPaintingEventHandler(FCGridFlexArray_CellPainting);
            this.Scroll += FCGridFlexArray_Scroll;

            this.SelectionMode = SelModeSettings.flexSelectionFree;
            this.FocusRect = FocusRectSettings.flexFocusLight;
            this.HighLight = HighLightSettings.flexHighlightAlways;
            this.ScrollBars = ScrollBarsSettings.flexScrollBoth;
            this.EnableHeadersVisualStyles = false;
            this.Editable = false;
            this.ExtendLastCol = false;
            this.FillStyle = FillStyleSettings.flexFillSingle;
            this.WhatsThisHelpID = 0;
            this.AutoResize = true;
            this.FormatString = "";

            this.AllowUserToAddRows = false;

            //editing in grid is not allowed, on top TextBox/ComboBox is used in customer code 
            this.ReadOnly = true;

            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }

            //CHE: Row/Col property is changing when FixedRows/FixedCols is changed
            this.m_currentRow = this.m_fixedRows;
            this.m_currentColumn = this.m_fixedColumns;

            //CHE: initialize also RowSel/ColSel
            this.m_colSel = this.m_currentColumn;
            this.m_rowSel = this.m_currentRow;

            //CHE: in VB6 MSFlexGrid the tab key causes the focus to jump to other objects on the form, leaving the grid
            this.StandardTab = true;

            this.AllowUserResizing = AllowUserResizeSettings.flexResizeNone;

            //CHE: hide black arrow of RowHeader
            this.RowHeadersDefaultCellStyle.Padding = new Padding(this.RowHeadersWidth);
            this.RowPostPaint += FCGridFlexArray_RowPostPaint;

            //CHE: set default value
            this.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ForeColorFixed = System.Drawing.SystemColors.ControlText;
            this.MergeCells = MergeCellsSettings.flexMergeNever;
            this.GridLineWidth = 1;
            this.Redraw = true;
            this.CellTextStyle = TextStyleSettings.flexTextFlat;
            this.AllowBigSelection = true;
            this.WordWrap = false;
            this.FixedRows = 1;
            this.FixedCols = 1;
            this.ToolTipText = "";
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events

        public event EventHandler<KeyEventArgs> FlexKeyDown;

        #endregion

        #region Private Events
        #endregion

        #region Enums

        public enum AllowUserResizeSettings
        {
            flexResizeNone = 0,
            flexResizeColumns = 1,
            flexResizeRows = 2,
            flexResizeBoth = 3
        }

        public enum SelModeSettings
        {
            flexSelectionFree = 0,
            flexSelectionByRow = 1,
            flexSelectionByColumn = 2,
            flexSelectionListBox = 3
        }

        public enum ClearWhereSettings
        {
            //Everywhere
            flexClearEverywhere = 0, //&H0
            //Scrollable Area
            flexClearScrollable = 1,//&H1
            //Current Selection
            flexClearSelection = 2//&H2
        }

        public enum ClearWhatSettings
        {
            //Everything
            flexClearEverything = 0, //&H0
            //Text
            flexClearText = 1,//&H1
            //Formatting
            flexClearFormatting = 2//&H2
        }

        public enum HighLightSettings
        {
            flexHighlightNever = 0,
            flexHighlightAlways = 1,
            flexHighlightWithFocus = 2
        }

        public enum ScrollBarsSettings
        {
            flexScrollNone = 0,
            flexScrollHorizontal = 1,
            flexScrollVertical = 2,
            flexScrollBoth = 3
        }

        public enum FocusRectSettings
        {
            flexFocusNone = 0,
            flexFocusLight = 1,
            flexFocusHeavy = 2
        }

        public enum TextStyleSettings
        {
            flexTextFlat = 0,
            flexTextRaised = 1,
            flexTextInset = 2,
            flexTextRaisedLight = 3,
            flexTextInsetLight = 4
        }

        public enum FillStyleSettings
        {
            //Single
            flexFillSingle = 0,//&H0
            //Repeat
            flexFillRepeat = 1 //&H1
        }

        public enum AlignmentSettings
        {
            flexAlignLeftTop = 0,
            flexAlignLeftCenter = 1,
            flexAlignLeftBottom = 2,
            flexAlignCenterTop = 3,
            flexAlignCenterCenter = 4,
            flexAlignCenterBottom = 5,
            flexAlignRightTop = 6,
            flexAlignRightCenter = 7,
            flexAlignRightBottom = 8,
            flexAlignGeneral = 9
        }

        public enum MergeCellsSettings
        {
            flexMergeNever = 0,
            flexMergeFree = 1,
            flexMergeRestrictRows = 2,
            flexMergeRestrictColumns = 3,
            flexMergeRestrictAll = 4
        }

        public enum SortSettings
        {
            //None
            flexSortNone = 0,//&H0
            //Generic Ascending
            flexSortGenericAscending = 1,//&H1
            //Generic Descending
            flexSortGenericDescending = 2,//&H2
            //Numeric Ascending
            flexSortNumericAscending = 3,//&H3
            //Numeric Descending
            flexSortNumericDescending = 4,//&H4
            //String Ascending Insensitive
            flexSortStringNoCaseAscending = 5,//&H5
            //String Descending Insensitive
            flexSortStringNoCaseDescending = 6,//&H6
            //String Ascending
            flexSortStringAscending = 7,//&H7
            //String Descending
            flexSortStringDescending = 8,//&H8
            //Custom
            flexSortCustom = 9,//&H9
            //Use ColSort setting
            flexSortUseColSort = 10 //&HA
        }

        public enum MousePointerSettings
        {
            //Default
            flexDefault = 0,  //&H0
            //Arrow
            flexArrow = 1,  //&H1
            //Cross
            flexCross = 2,  //&H2
            //I-Beam
            flexIBeam = 3,  //&H3
            //Icon
            flexIcon = 4,  //&H4
            //Size
            flexSize = 5,  //&H5
            //Size NE SW
            flexSizeNESW = 6,  //&H6
            //Size NS
            flexSizeNS = 7,  //&H7
            //Size NW SE
            flexSizeNWSE = 8,  //&H8
            //Size EW
            flexSizeEW = 9,  //&H9
            //Up Arrow
            flexUpArrow = 10,  //&HA
            //Hourglass
            flexHourglass = 11,  //&HB
            //No Drop
            flexNoDrop = 12,  //&HC
            //Arrow and HourGlass
            flexArrowHourGlass = 13,  //&HD
            //Arrow and Question
            flexArrowQuestion = 14,  //&HE
            //SizeAll
            flexSizeAll = 15,  //&HF
            //Custom
            flexCustom = 99  //&H63
        }

        #endregion

        #region Properties

        /// <summary>
        /// Returns/sets an associated context number for an object.
        /// </summary>
        [DefaultValue(0)]
        public int WhatsThisHelpID
        {
            // TODO:CHE
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool MoveCurrentCell = true;

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
        /// Returns or sets a ToolTip.
        /// Syntax
        /// object.ToolTipText [= string]
        /// The ToolTipText property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// string	A string associated with an object in the Applies To list. that appears in a small rectangle below the object when the user's cursor hovers over the object at run time for about one second.
        /// Remarks
        /// If you use only an image to label an object, you can use this property to explain each object with a few words.
        /// At design time you can set the ToolTipText property string in the control's properties dialog box.
        /// For the Toolbar and TabStrip controls, you must set the ShowTips property to True to display ToolTips.
        /// </summary>
        [DefaultValue("")]
        public string ToolTipText
        {
            get
            {
                return toolTipText;
            }
            set
            {
                toolTipText = value;
            }
        }

        /// <summary>
        /// Sets/returns the font to be used for individual cells or ranges of cells
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string CellFontName
        {
            get
            {
                DataGridViewCell cell = GetCurrentCell();
                if (cell.Style.Font == null)
                {
                    cell.Style.Font = base.DefaultCellStyle.Font.Clone() as Font;
                }
                return cell.Style.Font.Name;
            }
            set
            {
                DataGridViewCell cell = GetCurrentCell();
                if (cell.Style.Font == null)
                {
                    cell.Style.Font = base.DefaultCellStyle.Font.Clone() as Font;
                }
                Font oldFont = cell.Style.Font;
                if (cell.Style.Font.FontFamily.Name == value)
                {
                    return;
                }
                FontFamily fontFamily = new FontFamily(value);
                int dgvColIndex1 = this.GetDGVColIndex(Math.Min(this.Col, this.ColSel));
                int dgvRowIndex1 = this.GetDGVRowIndex(Math.Min(this.Row, this.RowSel));
                int dgvColIndex2 = this.GetDGVColIndex(Math.Max(this.Col, this.ColSel));
                int dgvRowIndex2 = this.GetDGVRowIndex(Math.Max(this.Row, this.RowSel));
                for (int i = dgvRowIndex1; i <= dgvRowIndex2; i++)
                {
                    for (int j = dgvColIndex1; j <= dgvColIndex2; j++)
                    {
                        cell = base.Rows[i].Cells[j];
                        if (cell.Style.Font == null)
                        {
                            cell.Style.Font = base.DefaultCellStyle.Font.Clone() as Font;
                        }
                        cell.Style.Font = FCUtils.CreateFont(fontFamily, cell.Style.Font.Size, cell.Style.Font.Style);
                    }
                }                
                oldFont.Dispose();
            }
        }

        /// <summary>
        /// Sets/returns the font to be used for individual cells or ranges of cells
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public float CellFontSize
        {
            get
            {
                DataGridViewCell cell = GetCurrentCell();
                if (cell.Style.Font == null)
                {
                    cell.Style.Font = base.DefaultCellStyle.Font.Clone() as Font;
                }
                return cell.Style.Font.Size;
            }
            set
            {
                DataGridViewCell cell = GetCurrentCell();
                if (cell.Style.Font == null)
                {
                    cell.Style.Font = base.DefaultCellStyle.Font.Clone() as Font;
                }
                Font oldFont = cell.Style.Font;
                if (cell.Style.Font.Size == value)
                {
                    return;
                }
                FontFamily fontFamily = new FontFamily(cell.Style.Font.Name);
                int dgvColIndex1 = this.GetDGVColIndex(Math.Min(this.Col, this.ColSel));
                int dgvRowIndex1 = this.GetDGVRowIndex(Math.Min(this.Row, this.RowSel));
                int dgvColIndex2 = this.GetDGVColIndex(Math.Max(this.Col, this.ColSel));
                int dgvRowIndex2 = this.GetDGVRowIndex(Math.Max(this.Row, this.RowSel));
                for (int i = dgvRowIndex1; i <= dgvRowIndex2; i++)
                {
                    for (int j = dgvColIndex1; j <= dgvColIndex2; j++)
                    {
                        cell = base.Rows[i].Cells[j];
                        if (cell.Style.Font == null)
                        {
                            cell.Style.Font = base.DefaultCellStyle.Font.Clone() as Font;
                        }
                        cell.Style.Font = FCUtils.CreateFont(fontFamily, value, cell.Style.Font.Style);
                    }
                }
                oldFont.Dispose();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool CellFontItalic
        {
            get
            {
                DataGridViewCell cell = GetCurrentCell();
                return cell.Style.Font.Italic;
            }
            set
            {
                DataGridViewCell cell = GetCurrentCell();
                cell.Style.Font = ControlExtension.SetFontProperty(cell.Style.Font, value, FontStyle.Italic);
            }
        }

        /// <summary>
        /// Sets/returns the font to be used for individual cells or ranges of cells
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool CellFontStrikethru
        {
            get
            {
                DataGridViewCell cell = GetCurrentCell();
                return cell.Style.Font.Strikeout;
            }
            set
            {
                DataGridViewCell cell = GetCurrentCell();
                cell.Style.Font = ControlExtension.SetFontProperty(cell.Style.Font, value, FontStyle.Strikeout);
            }
        }

        /// <summary>
        /// save associated TextBox
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public TextBox EditorTextBox
        {
            get;
            set;
        }

        /// <summary>
        /// save associated TextBox Visible flag
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool EditorTextBoxVisible
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the font used to display text in a control or in a run-time drawing or printing operation.
        /// Note   The FontName property is included for use with the CommonDialog control and for compatibility with earlier versions of Visual Basic. For additional functionality, 
        /// use the new Font object properties (not available for the CommonDialog control).
        /// Syntax
        /// object.FontName [= font]
        /// The FontName property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// font	A string expression specifying the font name to use.
        /// Remarks
        /// The default for this property is determined by the system. Fonts available with Visual Basic vary depending on your system configuration, display devices, and printing 
        /// devices. Font-related properties can be set only to values for which fonts exist.
        /// In general, you should change FontName before setting size and style attributes with the FontSize, FontBold, FontItalic, FontStrikethru, and FontUnderline properties.
        /// Note   At run time, you can get information on fonts available to the system through the FontCount and Fonts properties.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string FontName
        {
            get
            {
                return this.Font.Name;
            }
            set
            {
                this.SetFontName(value);
            }
        }

        /// <summary>
        /// FontBold
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontBold
        {
            get
            {
                return this.Font.Bold;
            }
            set
            {
                this.SetFontBold(value);
            }
        }

        /// <summary>
        /// FontItalic
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontItalic
        {
            get
            {
                return this.Font.Italic;
            }
            set
            {
                this.SetFontItalic(value);
            }
        }

        /// <summary>
        /// FontUnderline
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontUnderline
        {
            get
            {
                return this.Font.Underline;
            }
            set
            {
                this.SetFontUnderline(value);
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether an MSHFlexGrid should allow regular cell selection, selection by rows, or selection by columns.
        /// flexSelectionFree	0	Free. This allows individual cells in the MSHFlexGrid to be selected, spreadsheet style. This is the default.
        /// flexSelectionByRow	1	By Row. This forces selections to span entire rows, as in a multi-column list box or record-based display.
        /// flexSelectionByColumn	2	By Column. This forces selections to span entire columns, as if selecting ranges for a chart or fields for sorting.
        /// </summary>
        [DefaultValue(SelModeSettings.flexSelectionFree)]
        public new SelModeSettings SelectionMode
        {
            get
            {
                return this.m_selectionMode;
            }
            set
            {
                this.m_selectionMode = value;
                switch (this.SelectionMode)
                {
                    case SelModeSettings.flexSelectionFree:
                        {
                            base.SelectionMode = DataGridViewSelectionMode.CellSelect;
                            break;
                        }
                    case SelModeSettings.flexSelectionByRow:
                        {
                            base.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                            break;
                        }
                    case SelModeSettings.flexSelectionByColumn:
                        {
                            base.SelectionMode = DataGridViewSelectionMode.FullColumnSelect;
                            break;
                        }
                    case SelModeSettings.flexSelectionListBox:
                        {
                            base.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                            base.MultiSelect = true;
                            break;
                        }
                }
            }
        }


        /// <summary>
        /// Sets/returns the contents of the cell-editor's combo list
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public string ComboList
        {
            get
            {
                if (this.EditingControl != null)
                {
                    ComboBox combo = this.EditingControl as ComboBox;
                    if (combo != null)
                    {
                        string t = "";
                        foreach (object item in combo.Items)
                        {
                            t += item.ToString();
                        }
                        return t;
                    }
                }
                return "";
            }
            set
            {
                if (this.EditingControl != null)
                {
                    ComboBox combo = this.EditingControl as ComboBox;
                    if (combo != null)
                    {
                        combo.Items.Clear();
                        string[] parts = value.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string part in parts)
                        {
                            combo.Items.Add(part);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sets up the control's column widths, alignments, and fixed row and column text at design time
        /// </summary>
        [DefaultValue("")]
        public string FormatString
        {
            get
            {
                return this.DefaultCellStyle.Format;
            }
            set
            {
                this.DefaultCellStyle.Format = value;
            }
        }

        /// <summary>
        /// When retrieving, the Text property always retrieves the contents of the current cell as defined by the Row and Col properties.
        /// When setting, the Text property sets the contents of the current cell or selection depending on the setting of the FillStyle property.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public new string Text
        {
            get
            {
                DataGridViewCell cell = GetCurrentCell();
                return Convert.ToString(cell.Value);
            }
            set
            {
                DataGridViewCell cell = GetCurrentCell();
                cell.Value = value;
            }
        }

        /// <summary>
        /// Sets a sorting order for the selected rows using the selected columns as keys
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public SortSettings Sort
        {
            set
            {
                // TODO:CHE
                dgvColIndex = this.GetDGVColIndex(this.Col);
                switch(value)
                {
                    case SortSettings.flexSortNone:
                        {
                            base.Columns[dgvColIndex].SortMode = DataGridViewColumnSortMode.Automatic;
                            break;
                        }
                    case SortSettings.flexSortGenericAscending:
                        {
                            base.Columns[dgvColIndex].SortMode = DataGridViewColumnSortMode.Automatic;
                            break;
                        }
                    case SortSettings.flexSortGenericDescending:
                        {
                            base.Columns[dgvColIndex].SortMode = DataGridViewColumnSortMode.Automatic;
                            break;
                        }
                    case SortSettings.flexSortNumericAscending:
                        {
                            base.Columns[dgvColIndex].SortMode = DataGridViewColumnSortMode.Automatic;
                            break;
                        }
                    case SortSettings.flexSortNumericDescending:
                        {
                            base.Columns[dgvColIndex].SortMode = DataGridViewColumnSortMode.Automatic;
                            break;
                        }
                    case SortSettings.flexSortStringNoCaseAscending:
                        {
                            base.Columns[dgvColIndex].SortMode = DataGridViewColumnSortMode.Automatic;
                            break;
                        }
                    case SortSettings.flexSortStringNoCaseDescending:
                        {
                            base.Columns[dgvColIndex].SortMode = DataGridViewColumnSortMode.Automatic;
                            break;
                        }
                    case SortSettings.flexSortStringAscending:
                        {
                            base.Columns[dgvColIndex].SortMode = DataGridViewColumnSortMode.Automatic;
                            break;
                        }
                    case SortSettings.flexSortStringDescending:
                        {
                            base.Columns[dgvColIndex].SortMode = DataGridViewColumnSortMode.Automatic;
                            break;
                        }
                    case SortSettings.flexSortCustom:
                        {
                            base.Columns[dgvColIndex].SortMode = DataGridViewColumnSortMode.Automatic;
                            break;
                        }
                    case SortSettings.flexSortUseColSort:
                        {
                            base.Columns[dgvColIndex].SortMode = DataGridViewColumnSortMode.Automatic;
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Returns or sets the uppermost visible row (other than a fixed row) in the MSHFlexGrid. This property is not available at design time.
        /// Remarks
        /// You can use this property to programmatically read or set the visible top row of the MSHFlexGrid. Use the LeftCol property to determine the leftmost visible column in the MSHFlexGrid.
        /// The largest row number that you can use when setting TopRow is the total number of rows minus the number of rows that are visible in the MSHFlexGrid. If this property is set to a greater 
        /// row number, the MSHFlexGrid will reset it to this largest possible value.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int TopRow
        {
            get
            {
                int fixedRowsCount = this.GetFixedRows().Count;
                if (this.RowCount <= fixedRowsCount)
                {
                    return -1;
                }
                else
                {
                    int topRow = this.FirstDisplayedScrollingRowIndex;
                    if (topRow > -1)
                    {
                        return this.GetFlexRowIndex(topRow);
                    }
                    else
                    {
                        return fixedRowsCount;
                    }
                }
            }
            set
            {
                this.FirstDisplayedScrollingRowIndex = this.GetDGVRowIndex(value);
            }
        }

        /// <summary>
        /// Returns or sets the colors used to draw text on fixed part
        /// </summary>
        [DefaultValue(0)]
        public Color ForeColorFixed
        {
            get
            {
                return this.m_foreColorFixed;
            }
            set
            {
                if (value == null)
                {
                    return;
                }
                if (m_foreColorFixed != value)
                {
                    m_foreColorFixedBackup = this.ColumnHeadersDefaultCellStyle.ForeColor;
                }
                this.m_foreColorFixed = value;
                base.ColumnHeadersDefaultCellStyle.ForeColor = this.m_foreColorFixed;
                base.RowHeadersDefaultCellStyle.ForeColor = this.m_foreColorFixed;

                if (HasColumns)
                {
                    for (int i = 0; i < this.GetDGVColIndex(this.FixedCols); i++)
                    {
                        base.Columns[i].DefaultCellStyle.ForeColor = this.m_foreColorFixed;
                    }
                }

                if (HasRows)
                {
                    for (int j = 0; j < this.GetDGVRowIndex(this.FixedRows); j++)
                    {
                        base.Rows[j].DefaultCellStyle.ForeColor = this.m_foreColorFixed;
                    }
                }

            }
        }

        /// <summary>
        /// RowSel Returns or sets the start or end row for a range of cells.
        /// These properties are not available at design time.
        /// object.RowSel [= value]
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// value	A Long value that specifies the start or end row, or column, for a range of cells.
        /// Remarks
        /// You can use these properties to select a specific region of the MSHFlexGrid programmatically, or to read the dimensions of an area that the user selects into code.
        /// The MSHFlexGrid cursor is in the cell at Row, Col. The MSHFlexGrid selection is the region between rows Row and RowSel and columns Col and ColSel. Note that RowSel 
        /// may be above or below Row, and ColSel may be to the left or to the right of Col.
        /// Whenever you set the Row and Col properties, RowSel and ColSel are automatically reset, so the cursor becomes the current selection. 
        /// To select a block of cells from code, you must first set the Row and Col properties, and then set RowSel and ColSel.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int RowSel
        {
            get
            {
                return m_rowSel;
            }
            set
            {
                if (value < 0 || value > this.Rows)
                {
                    return;
                }
                m_rowSel = value;
            }
        }

        /// <summary>
        /// ColSel Returns or sets the start or end column for a range of cells.
        /// These properties are not available at design time.
        /// object.ColSel [= value]
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// value	A Long value that specifies the start or end row, or column, for a range of cells.
        /// Remarks
        /// You can use these properties to select a specific region of the MSHFlexGrid programmatically, or to read the dimensions of an area that the user selects into code.
        /// The MSHFlexGrid cursor is in the cell at Row, Col. The MSHFlexGrid selection is the region between rows Row and RowSel and columns Col and ColSel. Note that RowSel 
        /// may be above or below Row, and ColSel may be to the left or to the right of Col.
        /// Whenever you set the Row and Col properties, RowSel and ColSel are automatically reset, so the cursor becomes the current selection. 
        /// To select a block of cells from code, you must first set the Row and Col properties, and then set RowSel and ColSel.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ColSel
        {
            get
            {
                return m_colSel;
            }
            set
            {
                if (value < 0 || value > this.Cols)
                {
                    return;
                }
                m_colSel = value;
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether cells with the same contents should be grouped in a single cell spanning multiple rows or columns.
        /// object.MergeCells [=value]
        /// value	An integer or constant that specifies the grouping (merging) of cells, as specified:
        /// flexMergeNever	0	Never. The cells containing identical content are not grouped. This is the default.
        /// flexMergeFree	1	Free. Cells with identical content always merge.
        /// flexMergeRestrictRows	2	Restrict Rows. Only adjacent cells (to the left of the current cell) within the row containing identical content merge.
        /// flexMergeRestrictColumns	3	Restrict Columns. Only adjacent cells (to the top of the current cell) within the column containing identical content merge.
        /// flexMergeRestrictBoth	4	Restrict Both. Only adjacent cells within the row (to the left) or column (to the top) containing identical content merge.
        /// Remarks
        /// The ability to merge cells enables you to present data in a clear, concise manner. You can use cell merging in conjunction with the sorting and column order functions of the MSHFlexGrid.
        /// To use the cell merging capabilities of the MSHFlexGrid:
        /// Set MergeCells to a value other than zero. (The difference between the settings is explained in the example.)
        /// Set the MergeRow and MergeCol array properties to True for the rows and columns to be merged.
        /// When using the cell merging capabilities, the MSHFlexGrid merges cells with identical content. The merging is automatically updated whenever the cell content changes.
        /// When MergeCells is set to a value other than 0 (Never), selection highlighting is automatically turned off. 
        /// This is done to speed up repainting, and because selection of ranges containing merged cells may lead to unexpected results.
        /// </summary>
        [DefaultValue(MergeCellsSettings.flexMergeNever)]
        public MergeCellsSettings MergeCells
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns the position and size of the current cell, in twips. These properties are not available at design time.
        /// Remarks
        /// These properties are useful if you want to emulate in-cell editing. 
        /// By trapping the MSHFlexGrid's KeyPress event, you can place a text box or some other control over the current cell and let the user edit its contents.
        /// The return values are always in twips, regardless of the forms ScaleMode setting.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int CellTop
        {
            get
            {
                if (base.CurrentCell == null)
                {
                    return 0;
                }
                //FCA: GetCellDisplayRectangle return 0 when grid not visible                
                if (this.Visible)
                {
                    using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                        return Convert.ToInt32(fcGraphics.ScaleY(base.GetCellDisplayRectangle(base.CurrentCell.ColumnIndex, base.CurrentCell.RowIndex, false).Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                }
                else
                {
                    this.Visible = true;
                    int cellTop = 0;
                    using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                        cellTop = Convert.ToInt32(fcGraphics.ScaleY(base.GetCellDisplayRectangle(base.CurrentCell.ColumnIndex, base.CurrentCell.RowIndex, false).Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                    if (this.Visible)
                    {
                        this.Visible = false;
                    }
                    return cellTop;
                }
            }
        }

        /// <summary>
        /// Sets/returns the alignment of pictures in a cell or range
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public FCGridFlexArray.AlignmentSettings CellPictureAlignment
        {
            // TODO:CHE
            get;
            set;
        }

        [DefaultValue(FillStyleSettings.flexFillSingle)]
        public FCGridFlexArray.FillStyleSettings FillStyle
        {
            // TODO:CHE
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Image CellPicture
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns the position and size of the current cell, in twips. These properties are not available at design time.
        /// Remarks
        /// These properties are useful if you want to emulate in-cell editing. 
        /// By trapping the MSHFlexGrid's KeyPress event, you can place a text box or some other control over the current cell and let the user edit its contents.
        /// The return values are always in twips, regardless of the forms ScaleMode setting.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int CellLeft
        {
            get
            {
                if (base.CurrentCell == null)
                {
                    return 0;
                }
                //FCA: GetCellDisplayRectangle return 0 when grid not visible                
                if (this.Visible)
                {
                    using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                        return Convert.ToInt32(fcGraphics.ScaleX(base.GetCellDisplayRectangle(base.CurrentCell.ColumnIndex, base.CurrentCell.RowIndex, false).Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                }
                else
                {
                    this.Visible = true;
                    int cellLeft = 0;
                    using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                        cellLeft = Convert.ToInt32(fcGraphics.ScaleX(base.GetCellDisplayRectangle(base.CurrentCell.ColumnIndex, base.CurrentCell.RowIndex, false).Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                    if (this.Visible)
                    {
                        this.Visible = false;
                    }
                    return cellLeft;
                }

            }
        }

        /// <summary>
        /// Returns the position and size of the current cell, in twips. These properties are not available at design time.
        /// Remarks
        /// These properties are useful if you want to emulate in-cell editing. 
        /// By trapping the MSHFlexGrid's KeyPress event, you can place a text box or some other control over the current cell and let the user edit its contents.
        /// The return values are always in twips, regardless of the forms ScaleMode setting.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int CellWidth
        {
            get
            {
                DataGridViewCell cell = base.CurrentCell;
                if (cell == null)
                {
                    cell = GetFirstVisibleCell();
                    if (cell == null)
                    {
                        return 0;
                    }
                }
                //FCA: GetCellDisplayRectangle return 0 when grid not visible                
                if (this.Visible)
                {
                    using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                        return Convert.ToInt32(fcGraphics.ScaleX(base.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false).Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                }
                else
                {
                    this.Visible = true;
                    int cellWidth = 0;
                    using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                        int cellDisplayRectangleWidth = base.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false).Width;
                        //CHE: if form is not Visible then Grid.Visible cannot be set to true and GetCellDisplayRectangle still return 0, for this case use cell.Size property
                        if (cellDisplayRectangleWidth == 0)
                        {
                            cellDisplayRectangleWidth = cell.Size.Width;
                        }
                        cellWidth = Convert.ToInt32(fcGraphics.ScaleX(cellDisplayRectangleWidth, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                    if (this.Visible)
                    {
                        this.Visible = false;
                    }
                    return cellWidth;
                }
            }
        }

        /// <summary>
        /// Returns the position and size of the current cell, in twips. These properties are not available at design time.
        /// Remarks
        /// These properties are useful if you want to emulate in-cell editing. 
        /// By trapping the MSHFlexGrid's KeyPress event, you can place a text box or some other control over the current cell and let the user edit its contents.
        /// The return values are always in twips, regardless of the forms ScaleMode setting.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int CellHeight
        {
            get
            {
                DataGridViewCell cell = base.CurrentCell;
                if (cell == null)
                {
                    cell = GetFirstVisibleCell();
                    if (cell == null)
                    {
                        return 0;
                    }
                }
                //FCA: GetCellDisplayRectangle return 0 when grid not visible                
                if (this.Visible)
                {
                    using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                        return Convert.ToInt32(fcGraphics.ScaleY(base.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false).Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                }
                else
                {
                    this.Visible = true;
                    int cellHeight = 0;
                    using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                        int cellDisplayRectangleHeight = base.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false).Height;
                        //CHE: if form is not Visible then Grid.Visible cannot be set to true and GetCellDisplayRectangle still return 0, for this case use cell.Size property
                        if (cellDisplayRectangleHeight == 0)
                        {
                            cellDisplayRectangleHeight = cell.Size.Height;
                        }
                        cellHeight = Convert.ToInt32(fcGraphics.ScaleY(cellDisplayRectangleHeight, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                    if (this.Visible)
                    {
                        this.Visible = false;
                    }
                    return cellHeight;
                }
            }
        }

        /// <summary>
        /// Returns or sets the width, in pixels, of the lines displayed between cells, bands, headers, indents, or unpopulated areas.
        /// </summary>
        [DefaultValue(1)]
        public int GridLineWidth
        {
            get
            {
                return m_gridLineWidth;
            }
            set
            {
                m_gridLineWidth = value;
                foreach (DataGridViewColumn columnItem in base.Columns)
                {
                    columnItem.DividerWidth = m_gridLineWidth;
                }

                foreach (DataGridViewRow rowItem in base.Rows)
                {
                    rowItem.DividerHeight = m_gridLineWidth;
                }
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether the MSHFlexGrid should be automatically redrawn after each change.
        /// </summary>
        [DefaultValue(true)]
        public bool Redraw
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the three-dimensional style for text in a specific cell or range of cells. This property is not available at design time.
        /// flexTextFlat	0	Default. The text is normal, flat text.
        /// flexTextRaised	1	The text appears raised.
        /// flexTextInset	2	The text appears inset.
        /// flexTextRaisedLight	3	The text appears slightly raised.
        /// flexTextInsetLight	4	The text appears slightly inset.
        /// Remarks
        /// Settings 1 and 2 work best for large and bold fonts. Settings 3 and 4 work best for small regular fonts. 
        /// The cells appearance is also affected by the BackColor settings; some BackColor settings do not show the raised or inset feature.
        /// Changing this property affects the current cell or the current selection, depending on the setting of the FillStyle property.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(TextStyleSettings.flexTextFlat)]
        public TextStyleSettings CellTextStyle
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns or sets a value that determines whether the MSFlexGrid should draw a focus rectangle around the current cell.
        /// flexFocusNone	0	There is no focus rectangle around the current cell.
        /// flexFocusLight	1	There is a light focus rectangle around the current cell. This is the default.
        /// flexFocusHeavy	2	There is a heavy focus rectangle around the current cell.
        /// Remarks
        /// If a focus rectangle is drawn, the current cell is painted in the background color, as in most spreadsheets and grids. 
        /// Otherwise, the current cell is painted in the selection color, hence you can see which cell is selected without the focus rectangle.
        /// </summary>
        [DefaultValue(FocusRectSettings.flexFocusLight)]
        public FocusRectSettings FocusRect
        {
            get
            {
                return m_focusRect;
            }
            set
            {
                m_focusRect = value;
            }
        }

        /// <summary>
        /// Determines whether selected cells appear highlighted
        /// flexHighlightNever	0	There is no highlight on the selected cells.
        /// flexHighlightAlways	1	The selected cells are always highlighted. (Default)
        /// flexHighlightWithFocus	2	The highlight appears only when the control has focus.
        /// Remarks
        /// When this property is set to zero and a range of cells is selected, there is no visual cue or emphasis indicating the selected cells.
        /// </summary>
        [DefaultValue(HighLightSettings.flexHighlightAlways)]
        public HighLightSettings HighLight
        {
            get
            {
                return m_highLight;
            }
            set
            {
                m_highLight = value;
                //CHE: set SelectionBackColor and SelectionForeColor
                SetSelectionColors(base.DefaultCellStyle);
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether an MSFlexGrid has horizontal and/or vertical scroll bars.
        /// flexScrollNone	0	The MSFlexGrid has no scroll bars.
        /// flexScrollHorizontal	1	The MSFlexGrid has a horizontal scroll bar.
        /// flexScrollVertical	2	The MSFlexGrid has a vertical scroll bar.
        /// flexScrollBoth	3	The MSFlexGrid has horizontal and vertical scroll bars. (Default)
        /// Remarks
        /// Scroll bars appear on an MSHFlexGrid only if its contents extend beyond its borders and value specifies scroll bars. 
        /// If the ScrollBars property is set to None, the MSHFlexGrid will not have scroll bars, regardless of its contents.
        /// Note   If the MSHFlexGrid has no scroll bars in either direction, it will not allow any scrolling in that direction, even if the user 
        /// uses the keyboard to select a cell that is beyond the visible area of the control.
        /// </summary>
        [DefaultValue(ScrollBarsSettings.flexScrollBoth)]
        public new ScrollBarsSettings ScrollBars
        {
            get
            {
                return m_scrollBars;
            }
            set
            {
                m_scrollBars = value;
                switch (m_scrollBars)
                {
                    case ScrollBarsSettings.flexScrollNone:
                        base.ScrollBars = System.Windows.Forms.ScrollBars.None;
                        break;
                    case ScrollBarsSettings.flexScrollHorizontal:
                        base.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
                        break;
                    case ScrollBarsSettings.flexScrollVertical:
                        base.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
                        break;
                    case ScrollBarsSettings.flexScrollBoth:
                        base.ScrollBars = System.Windows.Forms.ScrollBars.Both;
                        break;
                }
            }
        }

        /// <summary>
        /// Sets/returns whether the control should provide in-cell editing
        /// </summary>
        [DefaultValue(false)]
        public bool Editable
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        [DefaultValue(false)]
        public bool ExtendLastCol
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns or sets a value that determines whether clicking on a column or row header should cause the entire column or row to be selected.
        /// </summary>
        [DefaultValue(true)]
        public bool AllowBigSelection
        {
            get
            {
                return m_allowBigSelection;
            }
            set
            {
                if (m_allowBigSelection != value)
                {
                    m_allowBigSelection = value;

                    if (m_allowBigSelection && this.SelectionMode == SelModeSettings.flexSelectionFree)
                    {
                        SetSortModeOnColumns();
                    }
                }
            }
        }

        [DefaultValue(true)]
        public bool AutoResize
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns or sets a value that determines whether the user can use the mouse to resize rows and columns in the MSHFlexGrid.
        /// Syntax
        /// object.AllowUserResizing [=value]
        /// The AllowUserResizing property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// value	An integer or constant that specifies whether a user can resize rows and columns, as described in Settings.
        /// Settings
        /// The settings for value are:
        /// Constant	Value	Description
        /// flexResizeNone	0	None. Default. The user cannot resize with the mouse.
        /// flexResizeColumns	1	Columns. The user can resize columns using the mouse.
        /// flexResizeRows	2	Rows. The user can resize rows using the mouse.
        /// flexResizeBoth	3	Both. The user can resize columns and rows using the mouse.
        /// Remarks
        /// To resize rows or columns, the mouse must be over the fixed area of the MSHFlexGrid and close to a border between rows and columns. 
        /// The mouse pointer changes into an appropriate sizing pointer, and the user can drag the row or column to change the row height or column width.
        /// </summary>
        [DefaultValue(AllowUserResizeSettings.flexResizeNone)]
        public AllowUserResizeSettings AllowUserResizing
        {
            get
            {
                return allowUserResizing;
            }
            set
            {
                allowUserResizing = value;
                switch (value)
                {
                    case AllowUserResizeSettings.flexResizeNone:
                        {
                            this.AllowUserToResizeColumns = false;
                            this.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                            this.AllowUserToResizeRows = false;
                            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                            break;
                        }
                    case AllowUserResizeSettings.flexResizeColumns:
                        {
                            this.AllowUserToResizeColumns = true;
                            this.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
                            this.AllowUserToResizeRows = false;
                            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                            break;
                        }
                    case AllowUserResizeSettings.flexResizeRows:
                        {
                            this.AllowUserToResizeColumns = false;
                            this.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                            this.AllowUserToResizeRows = true;
                            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
                            break;
                        }
                    case AllowUserResizeSettings.flexResizeBoth:
                        {
                            this.AllowUserToResizeColumns = true;
                            this.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
                            this.AllowUserToResizeRows = true;
                            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether a cell displays multiple lines of text or one long line of text.
        /// Note   Return characters, such as Chr(13), also force line breaks.
        /// Syntax
        /// object.WordWrap [=Boolean]
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// Boolean	A Boolean expression that specifies whether the text within a cell wraps.
        /// True	The cell text displays with multiple, wrapping lines of text.
        /// False	The cell text displays as one long line of text. This is the default.
        /// Remarks
        /// The MSHFlexGrid displays text slightly faster when WordWrap is set to False.
        /// </summary>
        [DefaultValue(false)]
        public bool WordWrap
        {
            get
            {
                if (base.DefaultCellStyle.WrapMode == DataGridViewTriState.NotSet)
                {
                    base.DefaultCellStyle.WrapMode = DataGridViewTriState.False;
                }

                return base.DefaultCellStyle.WrapMode == DataGridViewTriState.True;
            }
            set
            {
                if (value)
                {
                    base.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                }
                else
                {
                    base.DefaultCellStyle.WrapMode = DataGridViewTriState.False;
                }
            }
        }

        /// <summary>
        /// Returns or sets the total number of fixed rows, by default 1
        /// </summary>
        [DefaultValue(1)]
        public int FixedRows
        {
            get
            {
                return this.m_fixedRows;
            }
            set
            {
                if (this.m_fixedRows == value || value < 0 || value > this.Rows)
                {
                    return;
                }
                this.ColumnHeadersVisible = value > 0;
                if (this.m_fixedRows < value)
                {
                    for (int i = 0; i < this.GetDGVRowIndex(value); i++)
                    {
                        base.Rows[i].Frozen = true;
                        base.Rows[i].DefaultCellStyle.BackColor = base.RowHeadersDefaultCellStyle.BackColor;
                        base.Rows[i].ReadOnly = true;
                    }
                }
                else
                {
                    for (int i = this.GetDGVRowIndex(value); i < this.GetDGVRowIndex(this.m_fixedRows); i++)
                    {
                        base.Rows[i].Frozen = false;
                        base.Rows[i].DefaultCellStyle.BackColor = Color.White;
                        base.Rows[i].ReadOnly = false;
                        foreach (DataGridViewCell item in base.Rows[i].Cells)
                        {
                            if (item.OwningColumn.Frozen)
                            {
                                item.Style.BackColor = base.RowHeadersDefaultCellStyle.BackColor;
                                item.ReadOnly = true;
                            }
                        }
                    }

                    ResetForeColorFixed();
                }
                this.m_fixedRows = value;
                //CHE: Row/Col property is changing when FixedRows/FixedCols is changed
                m_currentRow = m_fixedRows;
            }
        }

        /// <summary>
        /// Returns or sets the total number of fixed columns, by default 1
        /// </summary>
        [DefaultValue(1)]
        public int FixedCols
        {
            get
            {
                return this.m_fixedColumns;
            }
            set
            {
                if (this.m_fixedColumns == value || value < 0 || value > this.Cols)
                {
                    return;
                }
                this.RowHeadersVisible = value > 0;
                if (value > 1)
                {
                    if (this.m_fixedColumns < value)
                    {
                        for (int i = 0; i < this.GetDGVColIndex(value); i++)
                        {
                            base.Columns[i].Frozen = true;
                            base.Columns[i].DefaultCellStyle.BackColor = base.ColumnHeadersDefaultCellStyle.BackColor;
                            base.Columns[i].ReadOnly = true;
                        }
                    }
                    if (this.m_fixedColumns > value)
                    {
                        for (int i = this.GetDGVColIndex(value); i < this.GetDGVColIndex(this.m_fixedColumns); i++)
                        {
                            base.Columns[i].Frozen = false;
                            base.Columns[i].DefaultCellStyle.BackColor = Color.White;
                            base.Columns[i].ReadOnly = false;
                        }

                        ResetForeColorFixed();
                    }
                }
                this.m_fixedColumns = value;
                //CHE: Row/Col property is changing when FixedRows/FixedCols is changed
                this.m_currentColumn = this.m_fixedColumns;
            }
        }

        /// <summary>
        /// Sets/returns the row (or column) over which the mouse pointer is
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int MouseRow
        {
            get;
            set; 
        }

        /// <summary>
        /// Sets/returns the row (or column) over which the mouse pointer is
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int MouseCol
        {
            get;
            set;
        }

        [DefaultValue(MousePointerSettings.flexDefault)]
        public MousePointerSettings MousePointer
        {
            get
            {
                return mousePointer;
            }
            set
            {
                mousePointer = value;
                // TODO:CHE
                //this.Cursor = FCUtils.GetTranslatedCursor(mousePointer);
            }
        }

        /// <summary>
        /// Returns or sets the coordinates of the active cell, not available at design time.
        /// Use these properties to specify a cell in an MSFlexGrid or to determine which row or column contains the current cell. 
        /// Columns and rows are numbered from zero rows are numbered from top to bottom, and columns are numbered from left to right.
        /// Setting these properties automatically resets RowSel and ColSel, the selection becoming the current cell. Therefore, to specify a block selection, 
        /// you must set Row and Col first, and then set RowSel and ColSel.
        /// The value of the current cell, defined by the Col and Row settings, is the text contained in that cell. To modify a cells value without changing 
        /// the selected Row and Col properties, use the TextMatrix property.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Row
        {
            get
            {
                return this.m_currentRow;
            }
            set
            {
                //exit if no changes
                if (this.m_currentRow == value)
                {
                    return;
                }

                this.m_currentRow = value;

                #region set CurrentCell

                if (MoveCurrentCell)
                {
                    int rowIndex = this.GetDGVRowIndex(value);

                    if (fromCurrentCellChanged || !this.IsDGVRowValid(rowIndex))
                    {
                        //if rowIndex is -1 cannot set the current cell to column header, but still the event RowColChange needs to be fired
                        if (rowIndex < 0)
                        {
                            inRowColSetter = true;
                            base.OnCurrentCellChanged(EventArgs.Empty);
                            inRowColSetter = false;
                        }
                        return;
                    }

                    inRowColSetter = true;

                    bool unchanged = false;

                    if (this.CurrentCell != null)
                    {
                        DataGridViewCell cell = this[this.CurrentCell.ColumnIndex, rowIndex];
                        if (this.CurrentCell == cell)
                        {
                            unchanged = true;
                        }
                        this.CurrentCell = cell;
                    }
                    else
                    {
                        //at least one column
                        if (this.ColumnCount > 0)
                        {
                            DataGridViewCell cell = this[FirstVisibleCol, rowIndex];
                            if (cell.Visible)
                            {
                                if (this.CurrentCell == cell)
                                {
                                    unchanged = true;
                                }
                                this.CurrentCell = cell;
                            }
                            else
                            {
                                if (this.CurrentCell == null)
                                {
                                    unchanged = true;
                                }
                                this.CurrentCell = null;
                            }
                        }
                    }

                    //still trigger the event if the CurrentCell was not changed (maybe had remained the same due to the case when current cell could not be set to row header or column header)
                    if (unchanged)
                    {
                        base.OnCurrentCellChanged(EventArgs.Empty);
                    }

                    inRowColSetter = false;
                }

                #endregion
            }
        }

        /// <summary>
        /// Returns or sets the coordinates of the active cell, not available at design time.
        /// Use these properties to specify a cell in an MSFlexGrid or to determine which row or column contains the current cell. 
        /// Columns and rows are numbered from zero rows are numbered from top to bottom, and columns are numbered from left to right.
        /// Setting these properties automatically resets RowSel and ColSel, the selection becoming the current cell. Therefore, to specify a block selection, 
        /// you must set Row and Col first, and then set RowSel and ColSel.
        /// The value of the current cell, defined by the Col and Row settings, is the text contained in that cell. To modify a cells value without changing 
        /// the selected Row and Col properties, use the TextMatrix property.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Col
        {
            get
            {
                return this.m_currentColumn;
            }
            set
            {
                //exit if no changes
                if (this.m_currentColumn == value)
                {
                    return;
                }

                this.m_currentColumn = value;

                #region set CurrentCell

                if (MoveCurrentCell)
                {
                    int colIndex = this.GetDGVColIndex(value);

                    if (fromCurrentCellChanged || !this.IsColValid(value) || colIndex < 0)
                    {
                        //if colIndex is -1 cannot set the current cell to row header, but still the event RowColChange needs to be fired
                        if (colIndex < 0)
                        {
                            inRowColSetter = true;
                            base.OnCurrentCellChanged(EventArgs.Empty);
                            inRowColSetter = false;
                        }
                        return;
                    }

                    inRowColSetter = true;

                    bool unchanged = false;

                    if (this.Columns[colIndex].Visible)
                    {
                        if (CurrentRow != null)
                        {
                            DataGridViewCell cell = GetDataGridViewCell(this.CurrentRow.Index, colIndex);
                            if (this.CurrentCell == cell)
                            {
                                unchanged = true;
                            }
                            this.CurrentCell = cell;
                        }
                    }

                    //still trigger the event if the CurrentCell was not changed (maybe had remained the same due to the case when current cell could not be set to row header or column header)
                    if (unchanged)
                    {
                        base.OnCurrentCellChanged(EventArgs.Empty);
                    }

                    inRowColSetter = false;
                }

                #endregion
            }
        }

        //CHE: remove from designer because there is a bug in Winforms: if you set ColumnCount in base class then when saving each time the designer 
        //will add some new columns beside the ones that were generated on previous saving
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Cols
        {
            get
            {
                return m_cols;
            }
            set
            {
                disableEvents = true;
                m_cols = value;
                if (this.Cols < 1)
                {
                    base.ColumnCount = 0;
                }
                else
                {
                    // CHE: by default, column SortMode is set to Automatic and when SelectionMode is ColumnHeaderSelect then exception is thrown:
                    // "Column's SortMode cannot be set to Automatic while the DataGridView control's SelectionMode is set to ColumnHeaderSelect."
                    bool wasColumnHeaderSelect = false;
                    if (base.SelectionMode == DataGridViewSelectionMode.ColumnHeaderSelect)
                    {
                        wasColumnHeaderSelect = true;
                        base.SelectionMode = DataGridViewSelectionMode.CellSelect;
                    }

                    base.ColumnCount = this.FixedCols > 0 ? Cols - 1 : Cols;

                    if (wasColumnHeaderSelect)
                    {
                        SetSortModeOnColumns();
                        base.SelectionMode = DataGridViewSelectionMode.ColumnHeaderSelect;
                    }
                }
                disableEvents = false;
            }


        }

        /// <summary>
        /// Rows
        /// </summary>
        public new int Rows
        {
            get
            {
                return m_rows;
            }
            set
            {
                disableEvents = true;
                m_rows = value;
                if (this.Rows < 1)
                {
                    base.Rows.Clear();
                }
                else
                {
                    base.RowCount = this.FixedRows > 0 ? Rows - 1 : Rows;
                }
                disableEvents = false;
            }
        }


        /// <summary>
        /// Returns or sets the left-most visible non fixed column in the MSFlexGrid. This property is not available at design time.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftCol
        {
            get
            {
                //FirstDisplayedScrollingColumnIndex doesn't return correctly when there are Frozen columns
                //In this case return the first visible column that is not frozen
                int index = this.FirstDisplayedScrollingColumnIndex;
                if (index < 0)
                {
                    return -1;
                }
                if (base.Columns[index].Frozen)
                {
                    for (int i = index + 1; i < base.Columns.Count; i++)
                    {
                        if (base.Columns[i].Visible && !base.Columns[i].Frozen)
                        {
                            index = i;
                            break;
                        }
                    }
                }
                return this.GetFlexColIndex(index);
            }
            set
            {
                int index = this.GetDGVColIndex(value);
                int columnIndex = index < 0 ? 0 : index;
                if (!base.Columns[columnIndex].Frozen)
                {
                    this.FirstDisplayedScrollingColumnIndex = columnIndex;
                }
            }
        }

        /// <summary>
        /// Returns or sets the background colors of individual cells or cell ranges.
        /// Changing this property affects the current cell or the current selection, depending on the setting of the FillStyle property.
        /// Setting either of these properties to zero causes MSHFlexGrid to paint the cell using the standard background and foreground colors. 
        /// If you want to set either of these properties to black, set them to one instead of zero.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Color CellBackColor
        {
            get
            {
                DataGridViewCell cell = GetCurrentCell();
                return cell.Style.BackColor;
            }
            set
            {
                if (value == null)
                {
                    return;
                }

                DataGridViewCell cell = GetCurrentCell();
                cell.Style.BackColor = value;

                //CHE: set SelectionBackColor and SelectionForeColor
                SetSelectionColors(cell.Style);
            }
        }

        /// <summary>
        /// Returns or sets the foreground colors of individual cells or cell ranges.
        /// Changing this property affects the current cell or the current selection, depending on the setting of the FillStyle property.
        /// Setting either of these properties to zero causes MSHFlexGrid to paint the cell using the standard background and foreground colors. 
        /// If you want to set either of these properties to black, set them to one instead of zero.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Color CellForeColor
        {
            get
            {
                DataGridViewCell cell = GetCurrentCell();
                return cell.Style.ForeColor;
            }
            set
            {
                if (value == null)
                {
                    return;
                }

                DataGridViewCell cell = GetCurrentCell();
                cell.Style.ForeColor = value;

                //CHE: set SelectionBackColor and SelectionForeColor
                SetSelectionColors(cell.Style);
            }
        }

        /// <summary>
        /// Returns or sets the bold style for the current cell text. This property is not available at design time.
        /// Syntax
        /// object.CellFontBold [=Boolean]
        /// The CellFontBold property syntax has these parts:
        /// Part Description 
        /// object An object expression that evaluates to an object in the Applies To list. 
        /// Boolean A Boolean expression that specifies whether the current cell's text is bold. 
        /// Settings
        /// The settings for Boolean are:
        /// Setting Description 
        /// True The current cell text is bold. 
        /// False Default. The current cell text is normal (not bold). 
        /// Remarks
        /// Changing this property affects the current cell or the current selection, depending on the setting of the FillStyle property.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool CellFontBold
        {
            get
            {
                DataGridViewCell cell = GetCurrentCell();
                return cell.Style.Font.Bold;
            }
            set
            {
                DataGridViewCell cell = GetCurrentCell();
                cell.Style.Font = ControlExtension.SetFontProperty(cell.Style.Font, value, FontStyle.Bold);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public AlignmentSettings CellAlignment
        {
            // TODO:CHE
            get;
            set;
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool HasRows
        {
            get
            {
                return base.RowCount > 0;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool HasColumns
        {
            get
            {
                return base.ColumnCount > 0;
            }
        }

        internal int FirstVisibleCol
        {
            get
            {
                foreach (DataGridViewColumn col in base.Columns)
                {
                    if (col.Visible)
                    {
                        return col.Index;
                    }
                }
                return -1;
            }
        }

        #endregion

        #region Public Methods

        public override void Refresh()
        {
            //CHE: do not execute Refresh for grid when Redraw flag is false
            if (Redraw)
            {
                base.Refresh();
            }
        }

        // JSP Fast get/set functions

        /// <summary>
        /// Get Cell Text with vbBase
        /// First Cell is 1/1
        /// </summary>
        /// <param name="intPosition"></param>
        public String GetCellText(int intCol, int intRow)
        {
            if (intCol > 0 & intRow > 0)
            {
                if (base[intCol - 1, intRow - 1].Value != null)
                {
                    return base[intCol - 1, intRow - 1].Value.ToString();
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Set Cell Text with vbBase
        /// First Cell is 1/1
        /// </summary>
        /// <param name="intPosition"></param>
        public void SetCellText(int intCol, int intRow, string CellValue)
        {
            if (intCol > 0 & intRow > 0)
            {
                base[intCol - 1, intRow - 1].Value = CellValue;
            }
        }

        /// <summary>
        /// Set Row Header Text with vbBase
        /// First Cell is 1/1
        /// </summary>
        /// <param name="intPosition"></param>
        public void SetRowHeaderText(int intRow, string CellValue)
        {
            if (intRow > 0)
            {
                base.Rows[intRow - 1].HeaderCell.Value = CellValue;
            }
        }

        /// <summary>
        /// Set Row Header Text with vbBase
        /// First Cell is 1/1
        /// </summary>
        /// <param name="intPosition"></param>
        public void SetColumnHeaderText(int intCol, string CellValue)
        {
            if (intCol > 0)
            {
                base.Columns[intCol - 1].HeaderCell.Value = CellValue;
            }
        }


        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public bool IsSelected(int row)
        {
            int gridRowIndex = this.GetDGVRowIndex(row);
            return base.Rows[gridRowIndex].Selected;
        }

        public void IsSelected(int row, bool value)
        {
            dgvRowIndex = this.GetDGVRowIndex(row);
            base.Rows[dgvRowIndex].Selected = value;
        }

        /// <summary>
        ///  Puts the control in edit mode
        /// </summary>
        public void EditCell()
        {
            base.BeginEdit(false);
        }

        /// <summary>
        /// Sets/returns the contents of an arbitrary cell (see also the TextMatrix property)
        /// This property is provided for backward compatibility with earlier versions of this control. New applications should use the Cell(flexcpText) 
        /// or TextMatrix properties.
        /// The following code places the text "Apple" in the second row and second column of a five column grid:
        /// fg.TextArray(6) = "Apple"
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public string TextArray(int index)
        {
            int row = index / m_cols;
            int col = index % m_cols;
            return TextMatrix(row,col);
        }

        /// <summary>
        /// Sets/returns the contents of an arbitrary cell (see also the TextMatrix property)
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public void TextArray(int index, string value)
        {
            int row = index / m_cols;
            int col = index % m_cols;
            TextMatrix(row, col, value);
        }

        /// <summary>
        /// Sets/returns a long value with user-defined information
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public int ColData(int index)
        {
            return GetData(colData, index);
        }

        public void ColData(int index, int value)
        {
            SetData(colData, index, value);
        }

        /// <summary>
        /// Sets/returns a long value with user-defined information
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public int RowData(int index)
        {
            return GetData(rowData, index);
        }

        public void RowData(int index, int value)
        {
            SetData(rowData, index, value);
        }

        /// <summary>
        /// Returns the text contents of an arbitrary cell.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public string TextMatrix(int row, int col)
        {
            object contents = null;

            dgvColIndex = this.GetDGVColIndex(col);
            dgvRowIndex = this.GetDGVRowIndex(row);

            if (dgvColIndex == -1 || dgvRowIndex == -1)
            {
                if (dgvRowIndex == -1 && dgvColIndex > -1)
                {
                    contents = base.Columns[dgvColIndex].HeaderText;
                }
                else if (dgvColIndex == -1 && dgvRowIndex > -1)
                {
                    contents = base.Rows[dgvRowIndex].HeaderCell.Value;
                }
                else if (dgvColIndex == -1 && dgvRowIndex == -1)
                {
                    contents = base.TopLeftHeaderCell.Value;
                }
            }
            else
            {
                contents = base[dgvColIndex, dgvRowIndex].Value;
            }

            if (contents != null)
            {
                return contents.ToString();
            }
            return "";
        }

        /// <summary>
        /// Sets the text contents of an arbitrary cell.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="contents"></param>
        public void TextMatrix(int row, int col, string contents)
        {
            dgvColIndex = this.GetDGVColIndex(col);
            dgvRowIndex = this.GetDGVRowIndex(row);

            if (dgvColIndex == -1 || dgvRowIndex == -1)
            {
                if (dgvColIndex > -1)
                {
                    base.Columns[dgvColIndex].HeaderText = contents;
                }
                else if (dgvRowIndex > -1)
                {
                    base.Rows[dgvRowIndex].HeaderCell.Value = contents;
                }
                else
                {
                    base.TopLeftHeaderCell.Value = contents;
                }
            }
            else
            {
                base[dgvColIndex, dgvRowIndex].Value = contents;
            }
        }

        /// <summary>
        /// Returns the height of the specified row, in logical twips. This property is not available at design time.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public int RowHeight(int row)
        {
            dgvRowIndex = this.GetDGVRowIndex(row);

            if (dgvRowIndex < 0)
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    //6 px, represent cell padding
                    return Convert.ToInt32(fcGraphics.ScaleY(base.ColumnHeadersHeight - 6, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            else
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Rows[dgvRowIndex].Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
        }

        /// <summary>
        /// Sets the height of the specified row, in logical twips. This property is not available at design time.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public void RowHeight(int row, int height)
        {
            dgvRowIndex = this.GetDGVRowIndex(row);

            if (dgvRowIndex < 0)
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    //6 px, represent cell padding
                    base.ColumnHeadersHeight = Convert.ToInt32(fcGraphics.ScaleY(height, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels)) + 6;
                }
            }
            else
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Rows[dgvRowIndex].Height = Convert.ToInt32(fcGraphics.ScaleY(height, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets a value that determines which rows and columns can have their contents merged. These properties must be True to use the MergeCells property.
        /// True	When adjacent cells display identical content, the rows merge to the left or the columns merge to the top.
        /// False	When adjacent cells display identical content, no cells merge. This is the default for MergeCol and MergeRow.
        /// Remarks
        /// If the MergeCells property is set to a nonzero value, adjacent cells with identical values are merged only if they are in a row with the MergeRow property 
        /// set to True or in a column with the MergeCol property set to True.
        /// For details on the merging functionality of the MSHFlexGrid, see the MergeCells property.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public bool MergeRow(int row)
        {
            // TODO:CHE
            return false;
        }

        /// <summary>
        /// Returns or sets a value that determines which rows and columns can have their contents merged. These properties must be True to use the MergeCells property.
        /// True	When adjacent cells display identical content, the rows merge to the left or the columns merge to the top.
        /// False	When adjacent cells display identical content, no cells merge. This is the default for MergeCol and MergeRow.
        /// Remarks
        /// If the MergeCells property is set to a nonzero value, adjacent cells with identical values are merged only if they are in a row with the MergeRow property 
        /// set to True or in a column with the MergeCol property set to True.
        /// For details on the merging functionality of the MSHFlexGrid, see the MergeCells property.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="merge"></param>
        public void MergeRow(int row, bool merge)
        {
            // TODO:CHE
        }

        /// <summary>
        /// Returns or sets a value that determines which rows and columns can have their contents merged. These properties must be True to use the MergeCells property.
        /// True	When adjacent cells display identical content, the rows merge to the left or the columns merge to the top.
        /// False	When adjacent cells display identical content, no cells merge. This is the default for MergeCol and MergeRow.
        /// Remarks
        /// If the MergeCells property is set to a nonzero value, adjacent cells with identical values are merged only if they are in a row with the MergeRow property 
        /// set to True or in a column with the MergeCol property set to True.
        /// For details on the merging functionality of the MSHFlexGrid, see the MergeCells property.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public bool MergeCol(int column)
        {
            // TODO:CHE
            return false;
        }

        /// <summary>
        /// Returns or sets a value that determines which rows and columns can have their contents merged. These properties must be True to use the MergeCells property.
        /// True	When adjacent cells display identical content, the rows merge to the left or the columns merge to the top.
        /// False	When adjacent cells display identical content, no cells merge. This is the default for MergeCol and MergeRow.
        /// Remarks
        /// If the MergeCells property is set to a nonzero value, adjacent cells with identical values are merged only if they are in a row with the MergeRow property 
        /// set to True or in a column with the MergeCol property set to True.
        /// For details on the merging functionality of the MSHFlexGrid, see the MergeCells property.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="merge"></param>
        public void MergeCol(int column, bool merge)
        {
            // TODO:CHE
        }

        /// <summary>
        /// Returns or sets the alignment of data in a column. This can be a standard column, a column within a band, or a column within a header. 
        /// This property is not available at design time (except indirectly through the FormatString property).
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public AlignmentSettings ColAlignment(int column)
        {
            dgvColIndex = this.GetDGVColIndex(column);

            if (dgvColIndex < 0)
            {
                return this.FixDataGridViewContentAlignment(base.RowHeadersDefaultCellStyle.Alignment);
            }
            //return this.FixDataGridViewContentAlignment(base.Columns[dgvColIndex].CellTemplate.Style.Alignment);
            return this.FixDataGridViewContentAlignment(base.Columns[dgvColIndex].DefaultCellStyle.Alignment);

        }

        /// <summary>
        /// Returns or sets the alignment of data in a column. This can be a standard column, a column within a band, or a column within a header. 
        /// This property is not available at design time (except indirectly through the FormatString property).
        /// object.ColAlignment(Index) [=value]
        /// value	An integer or constant that specifies the alignment of data in a column, as described:
        /// flexAlignLeftTop	0	The column content is aligned left, top.
        /// flexAlignLeftCenter	1	Default for strings. The column content is aligned left, center.
        /// flexAlignLeftBottom	2	The column content is aligned left, bottom.
        /// flexAlignCenterTop	3	The column content is aligned center, top.
        /// flexAlignCenterCenter	4	The column content is aligned center, center.
        /// flexAlignCenterBottom	5	The column content is aligned center, bottom.
        /// flexAlignRightTop	6	The column content is aligned right, top.
        /// flexAlignRightCenter	7	Default for numbers. The column content is aligned right, center.
        /// flexAlignRightBottom	8	The column content is aligned right, bottom.
        /// flexAlignGeneral	9	The column content is of general alignment. This is "left, center" for strings and "right, center" for numbers.
        /// Remarks
        /// Any column can have an alignment that is different from other columns. The ColAlignment property affects all cells in the specified column, including those in fixed rows.
        /// To set individual cell alignments, use the CellAlignment property. To set column alignments at design time, use the FormatString property.
        /// If the MSHFlexGrid is in vertical mode, the setting ColAlignment(3) may affect columns in multiple bands.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="alignment"></param>
        public void ColAlignment(int column, AlignmentSettings alignment)
        {
            dgvColIndex = this.GetDGVColIndex(column);

            if (dgvColIndex < 0)
            {
                base.RowHeadersDefaultCellStyle.Alignment = this.FixAlignmentSettings(alignment);
            }
            else
            {
                base.Columns[dgvColIndex].DefaultCellStyle.Alignment = this.FixAlignmentSettings(alignment);
            }
        }

        /// <summary>
        /// Returns whether a given column is currently within view
        /// The ColIsVisible and RowIsVisible properties are used to determine whether the specified column or row is within the visible area of the 
        /// control or whether it has been scrolled off the visible part of the control.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public bool ColIsVisible(int column)
        {
            // TODO:CHE
            dgvColIndex = this.GetDGVColIndex(this.Col);
            return base.Columns[dgvColIndex].Visible;
        }

        /// <summary>
        /// Returns the left (x) coordinate of a column relative to the edge of the control, in twips.
        /// This property is similar to the CellLeft property, except ColPos applies to an arbitrary column and will not cause the control to scroll. 
        /// The CellLeft property applies to the current selection and reading it will make the current cell visible, scrolling the contents of the 
        /// control if necessary.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public int ColPos(int column)
        {
            if (base.CurrentCell == null)
            {
                return 0;
            }
            //FCA: GetCellDisplayRectangle return 0 when grid not visible                
            if (this.Visible)
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.GetCellDisplayRectangle(column, base.CurrentCell.RowIndex, false).Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            else
            {
                this.Visible = true;
                int cellLeft = 0;
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    cellLeft = Convert.ToInt32(fcGraphics.ScaleX(base.GetCellDisplayRectangle(column, base.CurrentCell.RowIndex, false).Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
                if (this.Visible)
                {
                    this.Visible = false;
                }
                return cellLeft;
            }
        }

        /// <summary>
        /// Returns whether a given row is currently within view.
        /// The ColIsVisible and RowIsVisible properties are used to determine whether the specified column or row is within the visible area of 
        /// the control or whether it has been scrolled off the visible part of the control.
        /// If a row has zero height or is hidden but is within the scrollable area, RowIsVisible will return True.
        /// To ensure a given row is visible, use the ShowCell method.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public bool RowIsVisible(int row)
        {
            // TODO:CHE
            dgvRowIndex = this.GetDGVRowIndex(row);
            if (IsDGVRowValid(dgvRowIndex))
            {
                return base.Rows[dgvRowIndex].Visible;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Returns or sets the width of the column in the specified band, in logical twips. This property is not available at design time.
        /// Note   When using the MSFlexGrid, this property returns or sets the width of the specified column, in twips.
        /// Remarks
        /// You can use this property to set the width of any column at run time. For instructions on setting column widths at design time, see the FormatString property.
        /// You can set ColWidth to zero to create invisible columns, or to 1 to reset the column width to its default value, which depends on the size of the current font.
        /// When number is not specified, it defaults to 0. Hence, when the MSHFlexGrid is not bound to a hierarchical Recordset, using 0 and not specifying number both have 
        /// the same result. Note that number is an optional parameter for backward compatibility with the MSFlexGrid.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public int ColWidth(int column)
        {
            dgvColIndex = this.GetDGVColIndex(column);

            if (dgvColIndex < 0)
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.RowHeadersWidth, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            else
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Columns[dgvColIndex].Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }

        }

        /// <summary>
        /// Returns or sets the width of the column in the specified band, in logical twips. This property is not available at design time.
        /// Note   When using the MSFlexGrid, this property returns or sets the width of the specified column, in twips.
        /// Remarks
        /// You can use this property to set the width of any column at run time. For instructions on setting column widths at design time, see the FormatString property.
        /// You can set ColWidth to zero to create invisible columns, or to 1 to reset the column width to its default value, which depends on the size of the current font.
        /// When number is not specified, it defaults to 0. Hence, when the MSHFlexGrid is not bound to a hierarchical Recordset, using 0 and not specifying number both have 
        /// the same result. Note that number is an optional parameter for backward compatibility with the MSFlexGrid.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="width"></param>
        public void ColWidth(int column, int width)
        {
            dgvColIndex = this.GetDGVColIndex(column);

            if (dgvColIndex < 0)
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.RowHeadersWidth = Convert.ToInt32(fcGraphics.ScaleX(width, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
            else
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Columns[dgvColIndex].Width = Convert.ToInt32(fcGraphics.ScaleX(width, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }

            if (width == 0)
            {
                widthToZeroChanging = true;
                if (columnsVisibility.ContainsKey(dgvColIndex))
                {
                    columnsVisibility[dgvColIndex] = base.Columns[dgvColIndex].Visible;
                }
                else
                {
                    columnsVisibility.Add(dgvColIndex, base.Columns[dgvColIndex].Visible);
                }

                // have to distinguish between RowHeaderColumn and Columns
                if (dgvColIndex < 0)
                {
                    base.RowHeadersVisible = false;
                }
                else
                {
                    base.Columns[dgvColIndex].Visible = false;
                }
                widthToZeroChanging = false;
            }
            else
            {
                widthToZeroChanging = true;
                if (columnsVisibility.ContainsKey(dgvColIndex))
                {
                    // have to distinguish between RowHeaderColumn and Columns
                    if (dgvColIndex < 0)
                    {
                        base.RowHeadersVisible = true;
                    }
                    else
                    {
                        base.Columns[dgvColIndex].Visible = columnsVisibility[dgvColIndex];
                    }
                    columnsVisibility.Remove(dgvColIndex);
                }
                widthToZeroChanging = false;
            }
        }

        /// <summary>
        /// Returns or sets the alignment of data in the fixed cells of a column in an MSHFlexGrid.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public AlignmentSettings FixedAlignment(int column)
        {
            dgvColIndex = this.GetDGVColIndex(column);

            if (dgvColIndex < 0)
            {
                return this.FixDataGridViewContentAlignment(base.RowHeadersDefaultCellStyle.Alignment);
            }
            else
            {
                return this.FixDataGridViewContentAlignment(base.Columns[dgvColIndex].HeaderCell.Style.Alignment);
            }
        }

        /// <summary>
        /// Returns or sets the alignment of data in the fixed cells of a column in an MSHFlexGrid.
        /// object.FixedAlignment(Index) [=value]
        /// value	An integer that determines the alignment of the data in the fixed cells, as described:
        /// flexAlignLeftTop	0	The column content is aligned left, top.
        /// flexAlignLeftCenter	1	Default for strings. The column content is aligned left, center.
        /// flexAlignLeftBottom	2	The column content is aligned left, bottom.
        /// flexAlignCenterTop	3	The column content is aligned center, top.
        /// flexAlignCenterCenter	4	The column content is aligned center, center.
        /// flexAlignCenterBottom	5	The column content is aligned center, bottom.
        /// flexAlignRightTop	6	The column content is aligned right, top.
        /// flexAlignRightCenter	7	Default for numbers. The column content is aligned right, center.
        /// flexAlignRightBottom	8	The column content is aligned right, bottom.
        /// flexAlignGeneral	9	The column content is of general alignment. This is "left, center" for strings and "right, center" for numbers.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="alignment"></param>
        public void FixedAlignment(int column, AlignmentSettings alignment)
        {
            dgvColIndex = this.GetDGVColIndex(column);

            if (dgvColIndex < 0)
            {
                base.TopLeftHeaderCell.Style.Alignment = this.FixAlignmentSettings(alignment);
                base.RowHeadersDefaultCellStyle.Alignment = this.FixAlignmentSettings(alignment);

            }
            else
            {
                base.Columns[dgvColIndex].HeaderCell.Style.Alignment = this.FixAlignmentSettings(alignment);
                if (base.Columns[dgvColIndex].Frozen)
                {
                    base.Columns[dgvColIndex].DefaultCellStyle.Alignment = this.FixAlignmentSettings(alignment);
                }
            }
        }

        /// <summary>
        /// Removes a row from an MSHFlexGrid at run time. This property doesnt support named arguments.
        /// </summary>
        /// <param name="index">An integer representing the row within the MSHFlexGrid to remove. For the first row, index=0.</param>
        public void RemoveItem(int index)
        {
            //remove from dictionary
            SortedDictionary<int, int> rowDataTemp = new SortedDictionary<int, int>();
            foreach(int key in rowData.Keys)
            {
                if (key < index)
                {
                    rowDataTemp.Add(key, rowData[key]);
                }
                else if (key > index)
                {
                    rowDataTemp.Add(key - 1, rowData[key]);
                }
            }
            rowData = rowDataTemp;
            //remove from rows
            dgvRowIndex = this.GetDGVRowIndex(index);
            base.Rows.RemoveAt(dgvRowIndex);
            m_rows--;
        }

        /// <summary>
        /// Adds an item to a ListBox or ComboBox control or adds a row to a MS Flex Grid control. Doesn't support named arguments.
        /// Syntax
        /// object.AddItem item, index
        /// The AddItem method syntax has these parts:
        /// Part	Description
        /// object	Required. An object expression that evaluates to an object in the Applies To list.
        /// item	Required. string expression specifying the item to add to the object. For the MS Flex Grid control only, use the tab character (character code 09) to separate multiple strings you 
        /// want to insert into each column of a newly added row.
        /// index	Optional. Integer specifying the position within the object where the new item or row is placed. For the first item in a ListBox or ComboBox control or for the first row in a 
        /// MS Flex Grid control, index is 0.
        /// Remarks
        /// If you supply a valid value for index, item is placed at that position within the object. If index is omitted, item is added at the proper sorted position (if the Sorted property is set to True)
        /// or to the end of the list (if Sorted is set to False).
        /// A ListBox or ComboBox control that is bound to a Data control doesn't support the AddItem method.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"></param>
        public void AddItem(string item, int index = -1)
        {
            // JSP: Cut the first columne because this is the row header
            // (retval.ToString().IndexOf('?', pos) > 0)

            int pos = item.IndexOf('\t', 0);
            string rowHeader = "";

            if (pos > 0)
            {
                rowHeader = item.Substring(0, pos);
                item = item.Substring(pos + 1);
            }

            string[] rowItems = item.Split(new char[] { '\t' });
            dgvRowIndex = this.GetDGVRowIndex(index);

            // set rows to unfrozen, so that new rows can be added to the DGV 
            for (int i = 0; i < this.GetDGVRowIndex(this.FixedRows); i++)
            {
                base.Rows[i].Frozen = false;
            }

            if (dgvRowIndex < 0)
            {
                base.Rows.Insert(base.Rows.GetLastRow(DataGridViewElementStates.Visible), rowItems);
            }
            else
            {
                base.Rows.Insert(dgvRowIndex, rowItems);
            }

            if (rowHeader != "" & m_rows > 1)
            {
                base.Rows[m_rows - 2].HeaderCell.Value = rowHeader;
            }

            // refroze the unfrozen rows
            for (int i = 0; i < this.GetDGVRowIndex(this.FixedRows); i++)
            {
                base.Rows[i].Frozen = true;
            }
            m_rows++;
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        /// <summary>
        /// Clears the contents of the MSHFlexGrid. This includes all text, pictures, and cell formatting. 
        /// This method does not affect the number of rows and columns within the MSHFlexGrid.
        /// Remarks
        /// To remove cells instead of just clearing them, use the RemoveItem method on each row to be removed.
        /// </summary>
        public void Clear(ClearWhereSettings where = ClearWhereSettings.flexClearEverywhere, ClearWhatSettings what = ClearWhatSettings.flexClearEverything)
        {
            // TODO:CHE params
            int dgvColIndex1 = 0;
            int dgvRowIndex1 = 0;
            int dgvColIndex2 = base.Columns.Count - 1;
            int dgvRowIndex2 = base.Rows.Count - 1;            
            for (int i = dgvRowIndex1; i <= dgvRowIndex2; i++)
            {
                for (int j = dgvColIndex1; j <= dgvColIndex2; j++)
                {
                    if (where == ClearWhereSettings.flexClearEverywhere ||
                        (where == ClearWhereSettings.flexClearSelection && base.Rows[i].Cells[j].Selected == true))
                    {
                        if (what == ClearWhatSettings.flexClearEverything || what == ClearWhatSettings.flexClearText)
                        {
                            base.Rows[i].Cells[j].Value = "";
                        }
                    }
                }
            }
        }

        /// <summary>
        ///  Selects a range with a single command
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="rowSel"></param>
        /// <param name="colSel"></param>
        public void Select(int row, int col, int rowSel = 0, int colSel = 0)
        {
            int dgvColIndex1 = this.GetDGVColIndex(col);
            int dgvRowIndex1 = this.GetDGVRowIndex(row);
            int dgvColIndex2 = this.GetDGVColIndex(colSel);
            int dgvRowIndex2 = this.GetDGVRowIndex(rowSel);
            for (int i = dgvRowIndex1; i <= dgvRowIndex2; i++)
            {
                for (int j = dgvColIndex1; j <= dgvColIndex2; j++)
                {
                    base.Rows[i].Cells[j].Selected = true;
                }
            }
        }


        #endregion

        #region Internal Methods

        /// <summary>
        /// GetDataGridViewCell
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <returns></returns>
        internal DataGridViewCell GetDataGridViewCell(int rowIndex, int columnIndex)
        {
            DataGridViewCell cell = null;
            if (columnIndex == -1 && rowIndex == -1)
            {
                cell = this.TopLeftHeaderCell;
            }
            else if (rowIndex == -1 && columnIndex > -1)
            {
                cell = base.Columns[columnIndex].HeaderCell;
            }
            else if (columnIndex == -1 && rowIndex > -1)
            {
                cell = base.Rows[rowIndex].HeaderCell;
            }
            else
            {
                cell = base[columnIndex, rowIndex];
            }
            return cell;
        }

        /// <summary>
        /// GetCurrentCell
        /// </summary>
        /// <returns></returns>
        internal DataGridViewCell GetCurrentCell()
        {
            dgvColIndex = this.GetDGVColIndex(this.Col);
            dgvRowIndex = this.GetDGVRowIndex(this.Row);

            return GetDataGridViewCell(dgvRowIndex, dgvColIndex);
        }

        #endregion

        #region Protected Methods

        protected override void OnKeyDown(KeyEventArgs e)
        {
            //CHE: skip default DataGridView behavior - do not jump on next row on ENTER and execute accept button if the case
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                e.Handled = true;
                FCForm form = this.FindForm() as FCForm;
                if (form != null && form.AcceptButton != null)
                {
                    form.AcceptButton.PerformClick();
                }
                return;
            }
            //CHE: in VB6 MSFlexGrid control doesn't trap arrow keys http://support.microsoft.com/kb/171733
            if (FlexKeyDown != null && !ArrowKeyPressed(e))
            {
                FlexKeyDown(this, e);
            }
            base.OnKeyDown(e);
        }

        protected override void OnDoubleClick(EventArgs e)
        {
            CheckPosition();
            base.OnDoubleClick(e);
        }

        protected override void OnClick(EventArgs e)
        {
            CheckPosition();
            base.OnClick(e);
        }

        protected override void OnCurrentCellChanged(EventArgs e)
        {
            if (disableEvents || IsFormInitializing())
            {
                return;
            }
            base.OnCurrentCellChanged(e);
        }

        protected override void OnColumnStateChanged(DataGridViewColumnStateChangedEventArgs e)
        {
            base.OnColumnStateChanged(e);
            if (!widthToZeroChanging)
            {
                if (e.StateChanged == DataGridViewElementStates.Visible)
                {
                    if (columnsVisibility.ContainsKey(e.Column.Index))
                    {
                        columnsVisibility[e.Column.Index] = e.Column.Visible;
                    }
                }
            }
        }

        protected override void OnCellMouseMove(DataGridViewCellMouseEventArgs e)
        {
            //CHE: set cell.TollTipText, ToolTip control is not working
            DataGridViewCell cell = GetDataGridViewCell(e.RowIndex, e.ColumnIndex);
            if (!string.IsNullOrEmpty(this.ToolTipText) && cell.ToolTipText != this.ToolTipText)
            {
                cell.ToolTipText = this.ToolTipText;
            }
            this.MouseCol = e.ColumnIndex + 1;
            this.MouseRow = e.RowIndex + 1;
            base.OnCellMouseMove(e);
        }

        protected override void OnColumnRemoved(DataGridViewColumnEventArgs e)
        {
            if (IsFormInitializing())
            {
                return;
            }
            base.OnColumnRemoved(e);
            if (columnsVisibility.ContainsKey(e.Column.Index))
            {
                columnsVisibility.Remove(e.Column.Index);
            }
            RecalculateCols();
        }

        protected override void OnColumnAdded(DataGridViewColumnEventArgs e)
        {
            // CHE: by default, column SortMode is set to Automatic and when SelectionMode is ColumnHeaderSelect then exception is thrown:
            // "Column's SortMode cannot be set to Automatic while the DataGridView control's SelectionMode is set to ColumnHeaderSelect."
            e.Column.SortMode = DataGridViewColumnSortMode.Programmatic;

            base.OnColumnAdded(e);
            RecalculateCols();
        }

        protected override void OnFontChanged(EventArgs e)
        {
            base.OnFontChanged(e);

            //CHE: recalculate RowHeight when changing grid Font
            using (Graphics graphics = Graphics.FromHwnd(this.Handle))
            {
                SizeF baselineSize = graphics.MeasureString("Test line", this.Font);

                int rowHeight = 0;
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    rowHeight = Convert.ToInt32(fcGraphics.ScaleY(baselineSize.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }

                for (int rowIndex = 0; rowIndex < this.Rows; rowIndex++)
                {
                    this.RowHeight(rowIndex, rowHeight);
                }
            }

        }

        #endregion

        #region Private Methods

        private void SetSortModeOnColumns()
        {
            // CHE: by default, column SortMode is set to Automatic and when SelectionMode is ColumnHeaderSelect then exception is thrown:
            // "Column's SortMode cannot be set to Automatic while the DataGridView control's SelectionMode is set to ColumnHeaderSelect."

            foreach (DataGridViewColumn column in base.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.Programmatic;
            }
        }

        private bool ArrowKeyPressed(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right)
            {
                return true;
            }
            return false;
        }

        private void CheckPosition()
        {
            //CHE: if Row or Col was set to 0 then CurrentCell was not changed (could not be set on rowheader or column header) 
            // and when you double click the first valid cell then CurrentCellChanged is not raised like in VB6 (RowColChange), explicitly call it
            if (this.CurrentCell != null)
            {
                int gridRowIndex = this.GetDGVRowIndex(m_currentRow);
                int gridColIndex = this.GetDGVColIndex(m_currentColumn);
                if (this.CurrentCell.RowIndex != gridRowIndex || this.CurrentCell.ColumnIndex != gridColIndex)
                {
                    inRowColSetter = true;
                    m_currentRow = this.GetFlexRowIndex(this.CurrentCell.RowIndex);
                    m_currentColumn = this.GetFlexColIndex(this.CurrentCell.ColumnIndex);
                    base.OnCurrentCellChanged(EventArgs.Empty);
                    inRowColSetter = false;
                }
            }
        }

        private void FCGridFlexArray_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            //set general alignment: left for strings and right for numbers
            if (this.ColAlignment(GetFlexColIndex(e.ColumnIndex)) == AlignmentSettings.flexAlignGeneral)
            {
                if (Information.IsNumeric(e.Value))
                {
                    e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                else
                {
                    e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                }
            }

            if (this.DesignMode || this.CurrentCell == null)
            {
                return;
            }

            if (this.CurrentCell.ColumnIndex == e.ColumnIndex && this.CurrentCell.RowIndex == e.RowIndex)
            {
                //CHE - do not change selection type for FullRowSelect
                if (base.SelectionMode != DataGridViewSelectionMode.FullRowSelect)
                {
                    e.CellStyle.SelectionBackColor = e.CellStyle.BackColor;
                    e.CellStyle.SelectionForeColor = e.CellStyle.ForeColor;
                }

                Rectangle newRect = new Rectangle(e.CellBounds.X + 1, e.CellBounds.Y + 1, e.CellBounds.Width - 4, e.CellBounds.Height - 4);

                switch (this.FocusRect)
                {
                    case FocusRectSettings.flexFocusNone:
                        {
                            // default DGV behaviour
                            break;
                        }
                    case FocusRectSettings.flexFocusLight:
                        {
                            DrawCellDashBorder(e, 1);
                            break;
                        }
                    case FocusRectSettings.flexFocusHeavy:
                        {
                            DrawCellDashBorder(e, 3);
                            break;
                        }
                }
            }

            //CHE: do not color selection on rowheader
            if (e.ColumnIndex == -1)
            {
                e.CellStyle.SelectionBackColor = e.CellStyle.BackColor;
                e.CellStyle.SelectionForeColor = e.CellStyle.ForeColor;
            }
        }

        private void DrawCellDashBorder(DataGridViewCellPaintingEventArgs e, int width)
        {
            //CHE: draw current cell "border" only if grid has focus
            if (!this.ContainsFocus)
            {
                return;
            }
            e.Paint(e.CellBounds, DataGridViewPaintParts.All & ~DataGridViewPaintParts.Border);
            using (Pen p = new Pen(Color.Gray, width))
            {
                p.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
                Rectangle rect = e.CellBounds;
                rect.Width -= 2;
                rect.Height -= 2;
                e.Graphics.DrawRectangle(p, rect);
            }
            e.Handled = true;
        }

        private void FCGridFlexArray_MouseUp(object sender, MouseEventArgs e)
        {
            if (this.CurrentCell != null)
            {
                this.RowSel = this.GetFlexRowIndex(this.CurrentCell.RowIndex);
                this.ColSel = this.GetFlexColIndex(this.CurrentCell.ColumnIndex);
                //CHE: when selecting entire column RowSel is set to last row and Row to first line
                if (this.Columns[this.CurrentCell.ColumnIndex].Selected)
                {
                    this.Row = this.FixedRows;
                    this.RowSel = this.GetFlexRowIndex(this.RowCount - 1);
                }
            }
        }

        private void FCGridFlexArray_CurrentCellChanged(object sender, EventArgs e)
        {
            if (this.CurrentCell != null)
            {
                int colIndex = this.CurrentCell.ColumnIndex;

                //CHE: make current column fully visible
                Rectangle entireColumnRectangle = this.GetColumnDisplayRectangle(colIndex, false);
                Rectangle visibleColumnPart = this.GetColumnDisplayRectangle(colIndex, true);
                if (visibleColumnPart.Width < entireColumnRectangle.Width)
                {
                    this.HorizontalScrollingOffset += entireColumnRectangle.Width - visibleColumnPart.Width;
                    this.Refresh();
                }
                else if (visibleColumnPart.Width < this.Columns[colIndex].Width)
                {
                    int newValue = this.HorizontalScrollingOffset - (this.Columns[colIndex].Width - visibleColumnPart.Width);
                    if (newValue > 0)
                    {
                        this.HorizontalScrollingOffset = newValue;
                        this.Refresh();
                    }
                }

                m_currentCellChanged = true;

                if (!inRowColSetter)
                {
                    fromCurrentCellChanged = true;

                    this.Row = this.GetFlexRowIndex(this.CurrentCell.RowIndex);
                    this.Col = this.GetFlexColIndex(colIndex);

                    fromCurrentCellChanged = false;
                }
            }
        }

        private void SetSelectionColors(DataGridViewCellStyle cellStyle)
        {
            switch (this.HighLight)
            {
                case HighLightSettings.flexHighlightNever:
                    cellStyle.SelectionBackColor = cellStyle.BackColor;
                    cellStyle.SelectionForeColor = InvertAColor(cellStyle.BackColor);
                    break;
                case HighLightSettings.flexHighlightAlways:
                case HighLightSettings.flexHighlightWithFocus:
                    cellStyle.SelectionBackColor = SystemColors.Highlight;
                    cellStyle.SelectionForeColor = SystemColors.HighlightText;
                    break;
            }
        }

        private void FCGridFlexArray_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.AllowBigSelection && this.SelectionMode == SelModeSettings.flexSelectionFree)
            {
                System.Windows.Forms.DataGridView.HitTestInfo hti = this.HitTest(e.X, e.Y);

                if (hti.ColumnIndex == -1 && hti.RowIndex >= 0)
                {
                    // row header click
                    if (base.SelectionMode != DataGridViewSelectionMode.RowHeaderSelect)
                    {
                        base.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
                        //CHE: set SelectionBackColor and SelectionForeColor
                        SetSelectionColors(this.RowHeadersDefaultCellStyle);
                    }
                }
                else if (hti.RowIndex == -1 && hti.ColumnIndex >= 0)
                {
                    // column header click
                    if (base.SelectionMode != DataGridViewSelectionMode.ColumnHeaderSelect)
                    {
                        SetSortModeOnColumns();
                        base.SelectionMode = DataGridViewSelectionMode.ColumnHeaderSelect;
                        //CHE: set SelectionBackColor and SelectionForeColor
                        SetSelectionColors(this.ColumnHeadersDefaultCellStyle);
                    }
                }
            }
        }

        private void RecalculateCols()
        {
            //CHE: for columns added in designer we need to adjust Cols property too
            if (this.Parent == null)
            {
                int realCols = base.ColumnCount + (this.FixedCols > 0 ? 1 : 0);
                if (m_cols < realCols)
                {
                    m_cols = realCols;
                }
            }
        }

        private bool IsFormInitializing()
        {
            FCForm form = this.FindForm() as FCForm;
            if (form != null && form.IsInitializing)
            {
                return true;
            }
            return false;
        }

        private DataGridViewContentAlignment FixAlignmentSettings(AlignmentSettings settings)
        {
            DataGridViewContentAlignment newSettings = DataGridViewContentAlignment.NotSet;
            switch (settings)
            {
                case AlignmentSettings.flexAlignLeftTop:
                    newSettings = DataGridViewContentAlignment.TopLeft;
                    break;
                case AlignmentSettings.flexAlignLeftCenter:
                    newSettings = DataGridViewContentAlignment.MiddleLeft;
                    break;
                case AlignmentSettings.flexAlignLeftBottom:
                    newSettings = DataGridViewContentAlignment.BottomLeft;
                    break;
                case AlignmentSettings.flexAlignCenterTop:
                    newSettings = DataGridViewContentAlignment.TopCenter;
                    break;
                case AlignmentSettings.flexAlignCenterCenter:
                    newSettings = DataGridViewContentAlignment.MiddleCenter;
                    break;
                case AlignmentSettings.flexAlignCenterBottom:
                    newSettings = DataGridViewContentAlignment.BottomCenter;
                    break;
                case AlignmentSettings.flexAlignRightTop:
                    newSettings = DataGridViewContentAlignment.TopRight;
                    break;
                case AlignmentSettings.flexAlignRightCenter:
                    newSettings = DataGridViewContentAlignment.MiddleRight;
                    break;
                case AlignmentSettings.flexAlignRightBottom:
                    newSettings = DataGridViewContentAlignment.BottomRight;
                    break;
                case AlignmentSettings.flexAlignGeneral:
                    newSettings = DataGridViewContentAlignment.NotSet;
                    break;
            }

            return newSettings;
        }

        private AlignmentSettings FixDataGridViewContentAlignment(DataGridViewContentAlignment settings)
        {
            AlignmentSettings newSettings = AlignmentSettings.flexAlignGeneral;
            switch (settings)
            {
                case DataGridViewContentAlignment.BottomCenter:
                    newSettings = AlignmentSettings.flexAlignCenterBottom;
                    break;
                case DataGridViewContentAlignment.BottomLeft:
                    newSettings = AlignmentSettings.flexAlignLeftBottom;
                    break;
                case DataGridViewContentAlignment.BottomRight:
                    newSettings = AlignmentSettings.flexAlignRightBottom;
                    break;
                case DataGridViewContentAlignment.MiddleCenter:
                    newSettings = AlignmentSettings.flexAlignCenterCenter;
                    break;
                case DataGridViewContentAlignment.MiddleLeft:
                    newSettings = AlignmentSettings.flexAlignLeftCenter;
                    break;
                case DataGridViewContentAlignment.MiddleRight:
                    newSettings = AlignmentSettings.flexAlignRightCenter;
                    break;
                case DataGridViewContentAlignment.NotSet:
                    newSettings = AlignmentSettings.flexAlignGeneral;
                    break;
                case DataGridViewContentAlignment.TopCenter:
                    newSettings = AlignmentSettings.flexAlignCenterTop;
                    break;
                case DataGridViewContentAlignment.TopLeft:
                    newSettings = AlignmentSettings.flexAlignLeftTop;
                    break;
                case DataGridViewContentAlignment.TopRight:
                    newSettings = AlignmentSettings.flexAlignRightTop;
                    break;
            }
            return newSettings;
        }

        private DataGridViewRowCollection GetFixedRows()
        {
            DataGridViewRowCollection fixedRows = new DataGridViewRowCollection(this);
            foreach (DataGridViewRow rowItem in base.Rows)
            {
                if (rowItem.Frozen)
                {
                    fixedRows.Add(rowItem);
                }
            }
            return fixedRows;
        }

        private void ResetForeColorFixed()
        {
            for (int i = 0; i < this.GetDGVColIndex(this.FixedCols); i++)
            {
                //if (i >= this.Columns.Count)
                //{
                //    break;
                //}
                this.Columns[i].DefaultCellStyle.ForeColor = m_foreColorFixedBackup;

                //also reset BackColor for FrozenColumns
                //this.Columns[i].DefaultCellStyle.BackColor = this.DefaultCellStyle.BackColor;
            }

            for (int j = 0; j < this.GetDGVRowIndex(this.FixedRows); j++)
            {
                //if (j >= base.Rows.Count)
                //{
                //    break;
                //}
                base.Rows[j].DefaultCellStyle.ForeColor = m_foreColorFixedBackup;

                //also reset BackColor for FrozenRows
                //this.Rows[j].DefaultCellStyle.BackColor = this.DefaultCellStyle.BackColor;
            }
        }

        private Color InvertAColor(Color ColorToInvert)
        {
            return Color.FromArgb((byte)~ColorToInvert.R, (byte)~ColorToInvert.G, (byte)~ColorToInvert.B);
        }

        /// <summary>
        /// Returns DGV Index.
        /// </summary>
        /// <param name="flexIndex"></param>
        /// <returns></returns>
        private int GetDGVColIndex(int flexIndex)
        {
            if (!HasColumns)
            {
                return -1;
            }
            if (FixedCols == 0)
            {
                return flexIndex;
            }
            return --flexIndex;
        }

        /// <summary>
        /// Returns DGV Index.
        /// </summary>
        /// <param name="flexIndex"></param>
        /// <returns></returns>
        private int GetDGVRowIndex(int flexIndex)
        {
            if (!HasRows)
            {
                return -1;
            }
            if (FixedRows == 0)
            {
                return flexIndex;
            }
            return --flexIndex;
        }

        /// <summary>
        /// Returns Flex Index.
        /// </summary>
        /// <param name="dgvIndex"></param>
        /// <returns></returns>
        private int GetFlexColIndex(int dgvIndex)
        {
            if (!HasColumns)
            {
                return -1;
            }
            if (FixedCols == 0)
            {
                return dgvIndex;
            }
            return ++dgvIndex;
        }

        /// <summary>
        /// Returns Flex Index.
        /// </summary>
        /// <param name="dgvIndex"></param>
        /// <returns></returns>
        private int GetFlexRowIndex(int dgvIndex)
        {
            if (!HasRows)
            {
                return -1;
            }
            if (FixedRows == 0)
            {
                return dgvIndex;
            }
            return ++dgvIndex;
        }

        private DataGridViewCell GetFirstVisibleCell()
        {
            //get first visible cell
            for (int i = 0; i < base.Rows.Count; i++)
            {
                for (int j = 0; j < base.Columns.Count; j++)
                {
                    if (base.Rows[i].Cells[j].Visible)
                    {
                        return base.Rows[i].Cells[j];
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Check if the column is valid
        /// The index is for MSFlexGrid
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        private bool IsColValid(int col)
        {
            return col >= 0 && col < this.Columns.Count;
        }

        /// <summary>
        /// Check if the row is valid
        /// The index is for DataGridView
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private bool IsDGVRowValid(int row)
        {
            return row >= 0 && row < base.Rows.Count;
        }

        private void FCGridFlexArray_Scroll(object sender, ScrollEventArgs e)
        {
            //CHE - reload to see correct content of rowheader
            this.Refresh();
        }

        //CHE: hide black arrow of RowHeader
        private void FCGridFlexArray_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            DataGridViewCell headerCell = base.Rows[e.RowIndex].HeaderCell;
            object o = base.Rows[e.RowIndex].HeaderCell.Value;
            string text = o != null ? o.ToString() : "";

            Color color = !headerCell.Style.ForeColor.IsEmpty ? headerCell.Style.ForeColor : this.RowHeadersDefaultCellStyle.ForeColor;

            if (string.IsNullOrEmpty(text))
            {
                return;
            }

            float x = e.RowBounds.Left;
            float y = e.RowBounds.Top;

            FCForm form = this.FindForm() as FCForm;
            if (form != null)
            {
                float textWidth = Convert.ToInt32(fcGraphics.ScaleX(form.TextWidth(text), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                float textHeight = Convert.ToInt32(fcGraphics.ScaleY(form.TextHeight(text), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));

                //CHE - add padding
                textWidth += 5;

                switch (base.RowHeadersDefaultCellStyle.Alignment)
                {
                    case DataGridViewContentAlignment.TopLeft:
                        {
                            x = e.RowBounds.Left;
                            y = e.RowBounds.Top;
                            break;
                        }
                    case DataGridViewContentAlignment.MiddleLeft:
                        {
                            x = e.RowBounds.Left;
                            y = e.RowBounds.Top + e.RowBounds.Height / 2 - textHeight / 2;
                            break;
                        }
                    case DataGridViewContentAlignment.BottomLeft:
                        {
                            x = e.RowBounds.Left;
                            y = e.RowBounds.Top + e.RowBounds.Height - textHeight;
                            break;
                        }
                    case DataGridViewContentAlignment.TopCenter:
                        {
                            // JSP: ContentBounds.Width is always 0
                            // x = this.TopLeftHeaderCell.ContentBounds.Width / 2 - textWidth / 2;
                            x = this.TopLeftHeaderCell.Size.Width / 2 - textWidth / 2;
                            y = e.RowBounds.Top;
                            break;
                        }
                    case DataGridViewContentAlignment.MiddleCenter:
                        {
                            // JSP: ContentBounds.Width is always 0
                            // x = this.TopLeftHeaderCell.ContentBounds.Width / 2 - textWidth / 2;
                            x = this.TopLeftHeaderCell.Size.Width / 2 - textWidth / 2;
                            y = e.RowBounds.Top + e.RowBounds.Height / 2 - textHeight / 2;
                            break;
                        }
                    case DataGridViewContentAlignment.BottomCenter:
                        {
                            // JSP: ContentBounds.Width is always 0
                            // x = this.TopLeftHeaderCell.ContentBounds.Width / 2 - textWidth / 2;
                            x = this.TopLeftHeaderCell.Size.Width / 2 - textWidth / 2;
                            y = e.RowBounds.Top + e.RowBounds.Height - textHeight;
                            break;
                        }
                    case DataGridViewContentAlignment.TopRight:
                        {
                            // JSP: ContentBounds.Width is always 0
                            // x = this.TopLeftHeaderCell.ContentBounds.Width - textWidth;
                            x = this.TopLeftHeaderCell.Size.Width - textWidth;
                            y = e.RowBounds.Top;
                            break;
                        }
                    case DataGridViewContentAlignment.MiddleRight:
                        {
                            // JSP: ContentBounds.Width is always 0
                            // x = this.TopLeftHeaderCell.ContentBounds.Width - textWidth;
                            x = this.TopLeftHeaderCell.Size.Width - textWidth;
                            y = e.RowBounds.Top + e.RowBounds.Height / 2 - textHeight / 2;
                            break;
                        }
                    case DataGridViewContentAlignment.BottomRight:
                        {
                            // JSP: ContentBounds.Width is always 0
                            // x = this.TopLeftHeaderCell.ContentBounds.Width - textWidth;
                            x = this.TopLeftHeaderCell.Size.Width - textWidth;
                            y = e.RowBounds.Top + e.RowBounds.Height - textHeight;
                            break;
                        }
                    case DataGridViewContentAlignment.NotSet:
                        {
                            break;
                        }
                }
            }

            //CHE: write header cell text in available space
            e.Graphics.DrawString(text, this.Font, new SolidBrush(color),
                new RectangleF(new PointF((x > 0 ? x : 0), (y > 0 ? y : 0)), new SizeF(headerCell.Size.Width, e.RowBounds.Height)));

        }

        private int GetData(SortedDictionary<int, int> dict, int index)
        {
            if (dict.Keys.Contains(index))
            {
                return dict[index];
            }
            return 0;
        }

        private void SetData(SortedDictionary<int, int> dict, int index, int value)
        {
            if (!dict.Keys.Contains(index))
            {
                dict.Add(index, value);
            }
            else
            {
                dict[index] = value;
            }
        }


        #endregion
    }
}
