﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using System.IO;

namespace fecherFoundation
{
    public partial class FCFileListBox : ListBox
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        private string path;
        private string pattern;
        #endregion

        #region Constructors

        public FCFileListBox()
        {
            InitializeComponent();
            path = Application.StartupPath;
            pattern = "*.*";
            UpdateList();
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events

        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties
        /// <summary>
        /// Gets/sets the Path for Files to List
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Path
        {
            get
            {
                return path;
            }

            set
            {
                path = value;
                UpdateList();
            }
        }

        /// <summary>
        /// Gets/sets the Filter for Files to List
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Pattern
        {
            get
            {
                return pattern;
            }

            set
            {
                pattern = value;
                UpdateList();
            }
        }

        /// <summary>
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string FileName
        {
            get; set;
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        private void UpdateList()
        {
            this.BeginUpdate();
            this.Items.Clear();
            if (!String.IsNullOrEmpty(path))
            {
                foreach (string file in Directory.GetFiles(path, pattern))
                {
                    this.Items.Add(System.IO.Path.GetFileName(file));
                }
            }
            this.EndUpdate();
        }
        #endregion
    }
}
