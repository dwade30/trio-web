﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fecherFoundation
{
    /// <summary>
    /// The PictureClip control allows you to select an area of a source bitmap and then display the image of that area in a form or picture box. 
    /// PictureClip controls are invisible at run time.
    /// 
    /// The PictureClip control provides an efficient mechanism for storing multiple picture resources. 
    /// Instead of using multiple bitmaps or icons, create a source bitmap that contains all the icon images required by your application. When you need to display an individual icon, use the PictureClip control to select the region in the source bitmap that contains that icon.
    /// 
    /// You can specify the clipping region in the source bitmap in one of two ways:
    /// 
    /// 1. Select any portion of the source bitmap as the clipping region.Specify the upper-left corner of the clipping region using the ClipX and ClipY properties.Specify the area of the clipping region using the ClipHeight and ClipWidth properties.
    /// This method is useful when you want to view a random portion of a bitmap. 
    /// 
    /// 2. Divide the source bitmap into a specified number of rows and columns. The result is a uniform matrix of picture cells numbered 0, 1, 2, and so on.You can display individual cells using the GraphicCell property.
    /// This method is useful when the source bitmap contains a palette of icons that you want to display individually, such as in a toolbar bitmap.
    /// 
    /// https://msdn.microsoft.com/en-us/library/aa228385(v=vs.60).aspx
    /// </summary>
    public class FCPictureClip
    {
        /// <summary>
        /// 
        /// </summary>
        public Bitmap Picture
        {
            get; set;
        }

        public int Cols { get; set; } = 1;

        public int Rows { get; set; } = 1;

        /// <summary>
        /// A one-dimensional array of pictures representing all of the picture cells. 
        /// Remarks:
        /// - Use the Rows and Cols properties to divide a picture into a uniform matrix of graphic cells.
        /// - The cells specified by GraphicCell are indexed, beginning with 0, and increase from left to right and top to bottom.
        /// - When reading this property, an error is generated when there is no picture or when the Rows or Cols property is set to 0.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public Bitmap get_GraphicCell(int index)
        {
            if (Picture == null || Rows == 0 || Cols == 0) throw new ArgumentException();

            int width = Picture.Width / Cols;
            int height = Picture.Height / Rows;
            int x = (index % Cols) * width;
            int y = (index / Cols) * height;


            Rectangle cropRect = new Rectangle(x, y, width, height);

            Bitmap target = new Bitmap(cropRect.Width, cropRect.Height);

            using (Graphics g = Graphics.FromImage(target))
            {
                g.DrawImage(Picture, new Rectangle(0, 0, target.Width, target.Height),
                                 cropRect,
                                 GraphicsUnit.Pixel);
            }

            return target;
        }

    }
}
