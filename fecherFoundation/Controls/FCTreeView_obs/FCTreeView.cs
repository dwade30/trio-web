﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation.Controls
{
    public partial class FCTreeView : TreeView
    {
        private FCTreeNodeCollection nodes;

        public FCTreeView()
        {
            InitializeComponent();
            nodes = new FCTreeNodeCollection(base.Nodes);
        }

        public new FCTreeNodeCollection Nodes
        {
            get { return this.nodes; }
        }

        public FCTreeNode AddNode(FCTreeNode parent, string key, string name)
        {
            FCTreeNode addedNode = null;
            if (parent == null)
            {
                addedNode = this.nodes.Add(null, TreeRelationshipConstants.tvwFirst, key, name);
            }
            else
            {
                addedNode = this.nodes.Add(parent, TreeRelationshipConstants.tvwChild, key, name);
            }
            return addedNode;
        }
    }
}
