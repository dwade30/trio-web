﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fecherFoundation.Extensions;

namespace fecherFoundation.Controls
{
     public enum TreeRelationshipConstants
    {
        tvwChild = 4,
        tvwFirst = 0,
        tvwLast = 1,
        tvwNext = 2,
        tvwPrevious = 3
    }

     public class FCTreeNodeCollection : System.Object, System.Collections.IList, System.Collections.ICollection, System.Collections.IEnumerable
    {
        private TreeNodeCollection treeViewNodes;

        public FCTreeNodeCollection(TreeNodeCollection treeNodes)
        {
            treeViewNodes = treeNodes;
            nodes = new List<FCTreeNode>();
        }

        private List<FCTreeNode> nodes;

        public IEnumerator GetEnumerator()
        {
            return this.nodes.GetEnumerator();
        }

        public int Add(object value)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public bool Contains(object value)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public void Clear()
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public int IndexOf(object value)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public void Insert(int index, object value)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public void Remove(object value)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public bool IsReadOnly
        {
            get
            {
                // TODO: Implement this property getter
                throw new NotImplementedException();
            }
        }

        public bool IsFixedSize
        {
            get
            {
                // TODO: Implement this property getter
                throw new NotImplementedException();
            }
        }

        public FCTreeNode Add(FCTreeNode parent, TreeRelationshipConstants relationship, string key, string text)
        {
            FCTreeNode node = new FCTreeNode();
            node.Name = key;
            node.Text = text;
            node.Parent = parent;
            nodes.Add(node);

            // TODO: Need to take care of the TreeRelationshipConstants!!!

            if (parent == null)
            {
                treeViewNodes.Add(node);
            }
            else
            {
                parent.Nodes.Add(node);
            }

            return node;
        }

        public void CopyTo(Array array, int index)
        {
            // TODO: Implement this method
            throw new NotImplementedException();
        }

        public int Count
        {
            get { return nodes.Count; }
        }

        public object SyncRoot
        {
            get
            {
                // TODO: Implement this property getter
                throw new NotImplementedException();
            }
        }

        public bool IsSynchronized
        {
            get
            {
                // TODO: Implement this property getter
                throw new NotImplementedException();
            }
        }

        public FCTreeNode this[int index]
        {
            get
            {
                return this.nodes[index]; 
            }
            set
            {
                this.nodes[index] = value;
            }
        }

        object IList.this[int index]
        {
            get
            {
                return this.nodes[index];
            }
            set
            {
                this.nodes[index] = value as FCTreeNode;
            }
        }

        // TODO: implement this[string key], this[string name], and other missing properties and methods such as Remove, RemoveAt etc.
    }
}
