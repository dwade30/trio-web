﻿using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation.Controls
{
    public class FCTreeNode : TreeNode
    {
        private FCTreeNode parent = null;

        internal FCTreeNode Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        public FCTreeNode()
            : base()
        {

        }
    }
}
