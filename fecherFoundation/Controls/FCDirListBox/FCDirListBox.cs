﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using Wisej.Web;
using System.IO;

namespace fecherFoundation
{
    public partial class FCDirListBox : ListBox
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        private string path;
        private string pattern;
        #endregion

        #region Constructors

        public FCDirListBox()
        {
            InitializeComponent();
            path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            pattern = "*.*";
            UpdateList();
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events

        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties
        /// <summary>
        /// Gets/sets the Path for Files to List
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Path
        {
            get
            {
                return path;
            }

            set
            {
                path = value;
            }
        }
        #endregion

        #region Private Methods
        private void UpdateList()
        {
            this.BeginUpdate();
            this.Items.Clear();
            //FC:TODO:BBE - correct application path
            //foreach (string dir in Directory.GetDirectories(path))
            //{
            //    this.Items.Add(dir);
            //}
            this.EndUpdate();
        }
        #endregion
    }
}
