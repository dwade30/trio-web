﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;

namespace fecherFoundation
{
    /// <summary>
    /// Base class for checkbox cells
    /// </summary>
    internal class FCDataGridViewCheckBoxCell : DataGridViewCheckBoxCell
    {
        #region Members
        //FC:FINAL:ASZ - CheckValue/UncheckValue replaced with ValueData 
        //private object checkValue = -1;
        //private object uncheckValue = "";
        private object valueData = null;

        private bool validateFireFromClick = false;
        #endregion

        #region Properties

        //FC:FINAL:ASZ - CheckValue/UncheckValue replaced with ValueData 
        ///// <summary>
        ///// Check Value
        ///// </summary>
        //public object CheckValue
        //{
        //    get
        //    {
        //        return checkValue;
        //    }
        //    set
        //    {
        //        checkValue = value;
        //        uncheckValue = 0;
        //    }
        //}

        ///// <summary>
        ///// Uncheck value
        ///// </summary>
        //public object UncheckValue
        //{
        //    get
        //    {
        //        return uncheckValue;
        //    }
        //    set
        //    {
        //        uncheckValue = value;
        //        CheckValue = -1;
        //    }
        //}

        /// <summary>
        /// In VSFlexGrid1 if ColDataType is flexDTBoolean then causes the grid to display check boxes instead of strings in the specified column.
        /// The mapping between strings and check boxes follows the rules for Variant conversion: any non-zero value and the "True" string are displayed as checked boxes; zero values are displayed as unchecked boxes.
        /// For example:
        /// g.ColDataType(1) = flexDTBoolean
        /// g.TextMatrix(1, 1) = 1         ' checked
        /// g.TextMatrix(3, 1) = "4"       ' checked
        /// g.TextMatrix(2, 1) = True      ' checked
        /// g.TextMatrix(3, 1) = "True"    ' checked
        /// g.TextMatrix(3, 1) = "true"    ' checked
        /// g.TextMatrix(4, 1) = 0         ' not checked
        /// g.TextMatrix(5, 1) = "False"   ' not checked
        /// g.TextMatrix(5, 1) = "abc"     ' not checked
        /// </summary>
        public object ValueData
        {
            get
            {
                return valueData;
            }
            set
            {
                valueData = value;
            }
        }

        /// <remarks>
        /// IN VB6 Cell validating event for CheckBoxCell comes only on cell click (and not on cell leave too)
        /// </remarks>
        internal bool ValidateFireFromClick
        {
            get
            {
                return this.validateFireFromClick;
            }
        }
        #endregion

        protected override void OnClick(DataGridViewCellEventArgs e)
        {
            validateFireFromClick = true;
            base.OnClick(e);
            validateFireFromClick = false;
        }
    }
}
