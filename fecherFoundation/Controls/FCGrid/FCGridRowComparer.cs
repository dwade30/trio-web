﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;
using static fecherFoundation.FCGrid;

namespace fecherFoundation
{
    public class FCGridRowComparer : System.Collections.IComparer
    {
        private List<KeyValuePair<DataGridViewColumn, SortSettings>> columnList;

        public FCGridRowComparer(List<KeyValuePair<DataGridViewColumn, SortSettings>> columnList)
        {
            this.columnList = columnList;
        }

        public int Compare(object x, object y)
        {
            DataGridViewRow DataGridViewRow1 = ((DataGridViewRow)(x));
            DataGridViewRow DataGridViewRow2 = ((DataGridViewRow)(y));

            return CompareRows(DataGridViewRow1, DataGridViewRow2, 0);
        }

        public int CompareRows(DataGridViewRow DataGridViewRow1, DataGridViewRow DataGridViewRow2, int i)
        {
            DataGridViewColumn dgvColumn = columnList[i].Key;
            SortSettings sortSettings = columnList[i].Value;

            int sortOrderMultiplier = 0;
            if (sortSettings == SortSettings.flexSortGenericAscending
                || sortSettings == SortSettings.flexSortStringAscending
                || sortSettings == SortSettings.flexSortStringNoCaseAscending
                || sortSettings == SortSettings.flexSortNumericAscending)
            {
                sortOrderMultiplier = 1;
            }
            else
            {
                sortOrderMultiplier = -1;
            }

            int compareValue = 0;

            object value1 = DataGridViewRow1.Cells[dgvColumn.Index].Value;
            object value2 = DataGridViewRow2.Cells[dgvColumn.Index].Value;

            string cellValue1 = Convert.ToString
                (DataGridViewRow1.Cells[dgvColumn.Index].Value);
            string cellValue2 = Convert.ToString
                (DataGridViewRow2.Cells[dgvColumn.Index].Value);

            if ((value1 == null || cellValue1 == string.Empty)
                && (value2 != null || cellValue2 != string.Empty))
            {
                return -1 * sortOrderMultiplier;
            }
            else if ((value1 != null || cellValue1 != string.Empty) 
                && (value2 == null || cellValue2 == string.Empty))
            {
                return 1 * sortOrderMultiplier;
            }
            else if ((value1 == null || cellValue1 == string.Empty) 
                && (value2 == null || cellValue2 == string.Empty))
            {
                return 0;
            }

            if (dgvColumn.ValueType.IsNumeric() 
                || sortSettings == SortSettings.flexSortNumericAscending
                || sortSettings == SortSettings.flexSortNumericDescending)
            {
                double doubleVal1 = Conversion.Val(value1);
                double doubleVal2 = Conversion.Val(value2);

                if (doubleVal1 > doubleVal2)
                {
                    compareValue = 1;
                }
                else if (doubleVal1 < doubleVal2)
                {
                    compareValue = -1;
                }
                else
                {
                    compareValue = 0;
                }
            }
            else if (dgvColumn.ValueType == typeof(DateTime))
            {
                DateTime dateVal1;
                DateTime dateVal2;
                if ((DateTime.TryParse(cellValue1, out dateVal1)) 
                    && (DateTime.TryParse(cellValue2, out dateVal2)))
                {
                    if (dateVal1 > dateVal2)
                    {
                        compareValue = 1;
                    }
                    else if (dateVal1 < dateVal2)
                    {
                        compareValue = -1;
                    }
                    else
                    {
                        compareValue = 0;
                    }
                }
            }
            else
            {
                compareValue = string.Compare(cellValue1, cellValue2);
            }

            compareValue = compareValue * sortOrderMultiplier;

            if (compareValue == 0)
            {
                if (i < columnList.Count - 1)
                {
                    i++;
                    compareValue = CompareRows(DataGridViewRow1, DataGridViewRow2, i);
                }
            }
            return compareValue;
        }
    }
}
