﻿namespace fecherFoundation
{
    partial class FCButtonCell
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {   
            this.label = new Wisej.Web.Label();
            this.button = new Wisej.Web.Button();
            this.SuspendLayout();
            // 
            // label
            // 
            this.label.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.label.AutoEllipsis = true;
            this.label.BackColor = System.Drawing.Color.White;
            this.label.Font = new System.Drawing.Font("@default", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.label.Location = new System.Drawing.Point(0, 0);
            this.label.Name = "label";
            this.label.Padding = new Wisej.Web.Padding(5, 0, 0, 0);
            this.label.Size = new System.Drawing.Size(60, 40);
            this.label.TabIndex = 0;
            this.label.Text = "label1";
            this.label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button
            // 
            this.button.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Right)));
            this.button.Location = new System.Drawing.Point(50, 0);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(40, 40);
            this.button.TabIndex = 1;
            this.button.Text = "...";
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // FCButtonCell
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.Controls.Add(this.button);
            this.Controls.Add(this.label);
            this.Name = "FCButtonCell";
            this.Size = new System.Drawing.Size(90, 40);
            this.ResumeLayout(false);

        }

        #endregion
        private Wisej.Web.Label label;
        private Wisej.Web.Button button;
    }
}
