﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wisej.Web;

namespace fecherFoundation
{
    /// <summary>
    /// Base class for checkbox columns
    /// </summary>
    internal class FCDataGridViewCheckBoxColumn : DataGridViewCheckBoxColumn
    {
        public FCDataGridViewCheckBoxColumn()
        {
            this.CellTemplate = new FCDataGridViewCheckBoxCell();
        }
    }
}
