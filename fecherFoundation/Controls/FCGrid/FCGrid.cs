﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;
using Wisej.Core;

namespace fecherFoundation
{
	/// <summary>
	/// MSFlexGridLib.MSFlexGrid
	/// </summary>
	public class FCGrid : DataGridView
    {
        #region Public Members
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelStartRow
        {
            get
            {
                if (this.SelectedRows.Count > 0)
                {
                    return GetFlexRowIndex(this.SelectedRows[0].Index);
                }
                else
                {
                    return -1;
                }
            }
            set { this.Row = value; }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelEndRow
        {
            get
            {
                if (this.SelectedRows.Count > 0)
                {
                    return GetFlexRowIndex(this.SelectedRows[this.SelectedRows.Count - 1].Index);
                }
                else
                {
                    return -1;
                }
            }
            set { this.RowSel = value; }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelStartCol
        {
            get
            {
                if (this.SelectedColumns.Count > 0)
                {
                    return GetFlexColIndex(this.SelectedColumns[0].Index);
                }
                else
                {
                    return -1;
                }
            }
            set { this.Col = value; }
        }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelEndcol
        {
            get
            {
                if (this.SelectedColumns.Count > 0)
                {
                    return GetFlexColIndex(this.SelectedColumns[this.SelectedColumns.Count - 1].Index);
                }
                else
                {
                    return -1;
                }
            }
            set { this.ColSel = value; }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string CurrentCellText
        {
            //get { if (this.SelectedCells.Count > 0) { return this.SelectedCells[0].Value.ToString(); } else { return null; } } 
            //set { this.SelectedCells[0].Value = value; } }
            get { return this.Text; }
            set { this.Text = value; }
        }


        public void set_TextArray(int cellid, string text)
        {
            if (cellid <= this.ColumnCount & cellid > 0)
            {
                // JSP
                // Row 0 ==> set column header text
                base.Columns[cellid - 1].HeaderText = text;
            }
            else
            {
                // JSP: Calulate Row and cell correct
                int local_row = (cellid / this.ColumnCount) - 1;
                int local_cell = cellid - ((local_row + 1) * this.ColumnCount) - 1;

                base.Rows[local_row].Cells[local_cell].Value = text;
                //base.Rows[cellid / this.ColumnCount].Cells[cellid - (cellid / this.ColumnCount)].Value = text;
            }
        }
        public string get_TextArray(int cellid)
        {
            if (cellid <= this.ColumnCount & cellid > 0)
            {
                // JSP
                // Row 0 ==> get column header text
                return base.Columns[cellid - 1].HeaderText.ToString();
            }
            else
            {
                // JSP: Calulate Row and cell correct
                int local_row = (cellid / this.ColumnCount) - 1;
                int local_cell = cellid - ((local_row + 1) * this.ColumnCount) - 1;

                if (base.Rows[local_row].Cells[local_cell].Value == null)
                {
                    return "";
                }
                else
                {
                    return base.Rows[local_row].Cells[local_cell].Value.ToString();
                }
                //if (base.Rows[cellid / this.ColumnCount].Cells[cellid - (cellid / this.ColumnCount)].Value == null)
                //{
                //    return "";
                //}
                //else
                //{
                //    return base.Rows[cellid / this.ColumnCount].Cells[cellid - (cellid / this.ColumnCount)].Value.ToString();
                //}
            }
        }

        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        // FC:FINAL:VGE - #i1169 Implementing flexcpAlignment
        private static List<KeyValuePair<AlignmentSettings, DataGridViewContentAlignment>> alignments = new List<KeyValuePair<AlignmentSettings, DataGridViewContentAlignment>>() {
            new KeyValuePair<AlignmentSettings, DataGridViewContentAlignment>(AlignmentSettings.flexAlignCenterBottom, DataGridViewContentAlignment.BottomCenter),
            new KeyValuePair<AlignmentSettings, DataGridViewContentAlignment>(AlignmentSettings.flexAlignCenterCenter, DataGridViewContentAlignment.MiddleCenter),
            new KeyValuePair<AlignmentSettings, DataGridViewContentAlignment>(AlignmentSettings.flexAlignCenterTop, DataGridViewContentAlignment.TopCenter),
            new KeyValuePair<AlignmentSettings, DataGridViewContentAlignment>(AlignmentSettings.flexAlignGeneral, DataGridViewContentAlignment.NotSet),
            new KeyValuePair<AlignmentSettings, DataGridViewContentAlignment>(AlignmentSettings.flexAlignLeftBottom, DataGridViewContentAlignment.BottomLeft),
            new KeyValuePair<AlignmentSettings, DataGridViewContentAlignment>(AlignmentSettings.flexAlignLeftCenter, DataGridViewContentAlignment.MiddleLeft),
            new KeyValuePair<AlignmentSettings, DataGridViewContentAlignment>(AlignmentSettings.flexAlignLeftTop, DataGridViewContentAlignment.TopLeft),
            new KeyValuePair<AlignmentSettings, DataGridViewContentAlignment>(AlignmentSettings.flexAlignRightBottom, DataGridViewContentAlignment.BottomRight),
            new KeyValuePair<AlignmentSettings, DataGridViewContentAlignment>(AlignmentSettings.flexAlignRightCenter, DataGridViewContentAlignment.MiddleRight),
            new KeyValuePair<AlignmentSettings, DataGridViewContentAlignment>(AlignmentSettings.flexAlignRightTop, DataGridViewContentAlignment.TopRight)
        };

        private string toolTipText = "";
        private string m_scrollTipText;
        private ToolTip toolTip = new ToolTip();
        private bool fromCurrentCellChanged = false;
        private bool inRowColSetter = false;
        private bool beforeRowColChangeFired = false;

        private AllowUserResizeSettings allowUserResizing = AllowUserResizeSettings.flexResizeNone;

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private int m_fixedColumns = 1;
        private int m_fixedRows = 1;
        private int m_frozenRows = 1;
        //flex grid row/col values starting for 0,0 as first row/column
        private int m_currentRow = 0;
        private int m_currentColumn = 0;
        private SelectionModeSettings m_selectionMode = SelectionModeSettings.flexSelectionFree;
        private bool m_allowBigSelection = true;
        private Color m_foreColorFixed;
        private Color m_foreColorFixedBackup;
        private Color m_gridColorFixed;
        private int m_rowSel = -1;
        private int m_colSel = -1;
        private int m_gridLineWidth = 0;
        private FocusRectSettings m_focusRect = FocusRectSettings.flexFocusLight;
        private HighLightSettings m_highLight = HighLightSettings.flexHighlightAlways;
        private ScrollBarsSettings m_scrollBars = ScrollBarsSettings.flexScrollBoth;
        // helper variable for FocusRect-implementation - only draw the rectangle when current cell has changed
        private bool m_currentCellChanged = false;
        private int m_cols = 2;
        private int m_rows = 2;
        private Dictionary<int, bool> columnsVisibility = new Dictionary<int, bool>();

        private bool disableEvents = false;

        private bool keyHandled = false;

        private bool widthToZeroChanging;

        private AutoSizeSettings autoSizeMode;
        private bool extendLastCol = false;

        private int rowHeightMin = 0;
        private bool editableByEditCell = false;
        internal bool cellValidated = false;
        private ExplorerBarSettings explorerBar = ExplorerBarSettings.flexExNone;
        //FC:FINAL:DSE:#1531 Multiple column combo box implementation
        //private List<DataSourceItem> comboListItems;
        private List<string> comboListItems;
        private int minRowLevel = 1;
        private int maxRowLevel = 1;
        private int maxExpandLevel = 0;
        private int expandCollapseIndex = 0;
        private bool expanding = true;
        private bool inExpandCollapseAll;
        private int editMaxLength = 0;
        private double scaleFactor;
        private bool autoSizeOnRowChanging = false;
        //FC:FINAL:DSE:#i1152 Prevent recursive event firing if .TextMatrix is called from CellValueChanged event handler
        private bool fireCellValueChanged = true;
        private bool cellValueChangedRunning = false;
        private bool inErrorMessage = false;
        private bool inColumnDisplayIndexChanged = false;
        private bool inCellFormatting = false;
        private bool inCellValidating = false;
        private string role = null;
        private int redrawLock = 0;
        private string editMask = "";
        (int columnIndex, int rowIndex, object cellValue) editingValue = (-1, -1, null);

        private int oldCol = -1;
		private int oldRow = -1;
        private Dictionary<int, SortSettings> colSort = new Dictionary<int, SortSettings>();
		#endregion

		#region Constructors

		public FCGrid()
        {
            this.SortCompare += new DataGridViewSortCompareEventHandler(FCGrid_SortCompare);
            this.MouseUp += new MouseEventHandler(FCGrid_MouseUp);
            //JEI:HARRIS:IT273 we need to make it critical, so that we have correct values for MouseRow and MouseCol
            this.CellMouseMove += FCGrid_CellMouseMove;
            this.CurrentCellChanged += new EventHandler(FCGrid_CurrentCellChanged);
            //this.CellPainting += new DataGridViewCellPaintingEventHandler(FCGrid_CellPainting);
            //this.CellPaint += FCGrid_CellPaint;
            base.Rows.CollectionChanged += Rows_CollectionChanged;
            this.ColumnHeadersHeightChanged += FCGrid_ColumnHeadersHeightChanged;

            this.SelectionMode = SelectionModeSettings.flexSelectionFree;
            this.FocusRect = FocusRectSettings.flexFocusLight;
            this.HighLight = HighLightSettings.flexHighlightAlways;
            this.ScrollBars = ScrollBarsSettings.flexScrollBoth;

            this.AllowUserToAddRows = false;

            //editing in grid is not allowed, on top TextBox/ComboBox is used in customer code 
            this.ReadOnly = true;

            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }

            if (this.FixedRows > 0 && this.Rows > this.FixedRows)
            {
                //CHE: Row/Col property is changing when FixedRows/FixedCols is changed
                this.m_currentRow = this.m_fixedRows;
            }

            if (this.FixedCols > 0 && this.Cols > this.FixedCols)
            {
                //CHE: Row/Col property is changing when FixedRows/FixedCols is changed
                this.m_currentColumn = this.m_fixedColumns;
            }

            //CHE: initialize also RowSel/ColSel
            this.m_colSel = this.m_currentColumn;
            this.m_rowSel = this.m_currentRow;

            //CHE: in VB6 MSFlexGrid the tab key causes the focus to jump to other objects on the form, leaving the grid
            this.StandardTab = true;

            this.AllowUserResizing = AllowUserResizeSettings.flexResizeNone;

            //CHE: hide black arrow of RowHeader
            //this.RowHeadersDefaultCellStyle.Padding = new Padding(this.RowHeadersWidth);
            //this.RowPostPaint += FCGrid_RowPostPaint;

            //CHE: set default value
            //this.ForeColor = System.Drawing.SystemColors.WindowText;
            //this.ForeColorFixed = System.Drawing.SystemColors.ControlText;
            this.MergeCells = MergeCellsSettings.flexMergeNever;
            this.GridLineWidth = 1;
            //this.Redraw = true;
            this.CellTextStyle = TextStyleSettings.flexTextFlat;
            this.AllowBigSelection = true;
            this.WordWrap = false;
            //P:2218:SBE:#4585 - Wisej Form designer removes the Rows/Cols properties from designer.cs files, and Rows/Cols setters was not executed anymore.
            this.Rows = m_rows;
            this.Cols = m_cols;
            this.FixedRows = 1;
            this.FixedCols = 1;
            this.ShowColumnVisibilityMenu = false;
            this.EditMode = DataGridViewEditMode.EditOnEnter;
            this.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            this.ColumnHeadersHeight = 30;

            //FC:FINAL:BBE:#595-#596 - correct column size with factor to show it correct on the report
            this.UseScaleFactor = false;
            this.scaleFactor = 1.3;

            this.CellBorderStyle = DataGridViewCellBorderStyle.Horizontal;
        }

		//JEI:HARRIS:IT273 we need to make it critical, so that we have correct values for MouseRow and MouseCol
		private void FCGrid_CellMouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
           //make it critical
        }

        #endregion

        #region Public Delegates
        #endregion

        #region Public Events

        public event EventHandler<KeyEventArgs> FlexKeyDown;

        //TODO - implement on client side
        public event KeyEventHandler KeyDownEdit;
        //TODO - implement on client side
        public event KeyPressEventHandler KeyPressEdit;
        //TODO - implement on client side
        public event KeyEventHandler KeyUpEdit;

        public event EventHandler CellButtonClick;
        public event EventHandler ComboDropDown;
        public event EventHandler ComboCloseUp;
        public event EventHandler<BeforeRowColChangeEventArgs> BeforeRowColChange;
        public event EventHandler<AfterMoveRowEventArgs> AfterMoveRow;
        public event EventHandler<AfterMoveColumnEventArgs> AfterMoveColumn;
        public event DataGridViewCellEventHandler AfterEdit;
        public event EventHandler Collapsed;
        public event EventHandler ChangeEdit;
        #endregion

        #region Private Events
        #endregion

        #region Enums
        public enum CellPropertySettings
        {
            flexcpForeColor,
            flexcpBackColor,
            flexcpAlignment,
            flexcpData,
            flexcpFontBold,
            flexcpPicture,
            flexcpPictureAlignment,
            flexcpText,
            flexcpTextDisplay,
            flexcpFontUnderline,
            flexcpFontItalic,
            flexcpFontStrikethru,
            flexcpFontName,
            flexcpFontSize,
            flexcpLeft,
            flexcpTop,
            flexcpWidth,
            flexcpHeight,
            flexcpCustomFormat,
            flexcpValue,
            flexcpChecked
        }

        public enum CellCheckedSettings
        {
            flexChecked = 1,
            flexUnchecked = 2,
            flexNoCheckbox = 0
        }

        public bool IsSelected(int row)
        {
            int dgvRow = this.GetDGVRowIndex(row);
            return base.Rows[dgvRow].Selected;
        }

        public bool IsSelected(int row, bool value)
        {
            int dgvRow = this.GetDGVRowIndex(row);
            base.Rows[dgvRow].Selected = value;
            return value;
        }

        public enum AllowUserResizeSettings
        {
            flexResizeNone = 0,
            flexResizeColumns = 1,
            flexResizeRows = 2,
            flexResizeBoth = 3
        }

        public enum SelectionModeSettings
        {
            flexSelectionFree = 0,
            flexSelectionByRow = 1,
            flexSelectionByColumn = 2,
            flexSelectionListBox= 3
        }

        public enum HighLightSettings
        {
            flexHighlightNever = 0,
            flexHighlightAlways = 1,
            flexHighlightWithFocus = 2
        }

        public enum ScrollBarsSettings
        {
            flexScrollNone = 0,
            flexScrollHorizontal = 1,
            flexScrollVertical = 2,
            flexScrollBoth = 3
        }

        public enum FocusRectSettings
        {
            flexFocusNone = 0,
            flexFocusLight = 1,
            flexFocusHeavy = 2
        }

        public enum TextStyleSettings
        {
            flexTextFlat = 0,
            flexTextRaised = 1,
            flexTextInset = 2,
            flexTextRaisedLight = 3,
            flexTextInsetLight = 4
        }

        public enum AlignmentSettings
        {
            flexAlignLeftTop = 0,
            flexAlignLeftCenter = 1,
            flexAlignLeftBottom = 2,
            flexAlignCenterTop = 3,
            flexAlignCenterCenter = 4,
            flexAlignCenterBottom = 5,
            flexAlignRightTop = 6,
            flexAlignRightCenter = 7,
            flexAlignRightBottom = 8,
            flexAlignGeneral = 9
        }

        public enum MergeCellsSettings
        {
            flexMergeNever = 0,
            flexMergeFree = 1,
            flexMergeRestrictRows = 2,
            flexMergeRestrictColumns = 3,
            flexMergeRestrictAll = 4,
            flexMergeFixedOnly = 5,
            flexMergeSpill = 6,
            flexMergeOutline = 7
        }

        public enum PictureAlignmentSettings
        {
			flexPicAlignLeftTop = 0,
			flexPicAlignLeftCenter = 1,
			flexPicAlignLeftBottom = 2,
			flexPicAlignCenterTop = 3,
			flexPicAlignCenterCenter = 4,
			flexPicAlignCenterBottom = 5,
			flexPicAlignRightTop = 6,
			flexPicAlignRightCenter = 7,
			flexPicAlignRightBottom = 8,
			flexPicAlignStretch = 9,
			flexPicAlignTile = 10
		}

        public enum RedrawSettings
        {
            flexRDNone,
            flexRDDirect,
            flexRDBuffered

        }

        public enum EditableSettings
        {
            flexEDNone,
            flexEDKbd,
            flexEDKbdMouse
        }

        public enum SortSettings
        {
			flexSortNone,
			flexSortGenericAscending,
            flexSortGenericDescending,
            flexSortNumericAscending,
			flexSortNumericDescending,
            flexSortStringNoCaseAscending,
			flexSortNoCaseDescending,
			flexSortStringAscending,
			flexSortStringDescending,
			flexSortCustom,
            flexSortUseColSort
        }

        public enum ExplorerBarSettings
        {
            flexExNone = 0,
            flexExSort = 1,
            flexExMove = 2,
            flexExSortAndMove = 3,
            flexExSortShow = 5,
            flexExSortShowAndMove = 7,
            flexExMoveRows = 8
        }

        public enum OutlineBarSettings
        {
            flexOutlineBarNone = 0,
            flexOutlineBarComplete = 1,
            flexOutlineBarSimple = 2
        }

        public enum CollapsedSettings
        {
            flexOutlineExpanded,
            flexOutlineSubtotals,
            flexOutlineCollapsed
        }

        public enum DataTypeSettings
        {
            flexDTEmpty,
            flexDTBoolean,
            flexDTDate,
            flexDTCurrency,
            flexDTDouble,
            flexDTLong,
            flexDTString
        }

        public enum TabBehaviorSettings
        {
            flexTabControls,
            flexTabCells
        }

        public enum AutoSizeSettings
        {
            flexAutoSizeColWidth,
            flexAutoSizeRowHeight
        }

        public enum GridStyleSettings
        {
            flexGridNone
        }

        public enum SubtotalPositionSettings
        {
            flexSTBelow = 0,
            flexSTAbove = 1
        }

        public enum SubtotalSettings
        {
            flexSTNone = 0,
            flexSTClear = 1,
            flexSTSum = 2,
            flexSTPercent = 3,
            flexSTCount = 4,
            flexSTAverage = 5,
            flexSTMax = 6,
            flexSTMin = 7,
            flexSTStd = 8,
            flexSTVar = 9,
            flexSTStdPop = 10,
            flexSTVarPop = 11
        }

        public enum ClearWhereSettings
        {
            flexClearEverywhere = 0,
            flexClearScrollable = 1,
            flexClearSelection = 2
        }

        #endregion

        #region Properties

        /// <summary>
        /// Returns or sets the column used to display the outline tree.
        /// </summary>
        public int OutlineCol
        {
            //TODO:
            get;
            set;
        }

        // TODO

        /// <summary>
        /// Returns/sets the icon to be displayed as the pointer in a drag-and-drop operation.
        /// </summary>
        public Image DragIcon
        {
            //TODO:
            get;
            set;
        }

        /// <summary>
        /// Returns or sets whether subtotals should be inserted above or below the totaled data.
        /// </summary>
        public SubtotalPositionSettings SubtotalPosition
        {
            //TODO:
            get;
            set;
        }

        //RPU:
        /// <summary>
        /// Returns or the number of characters selected in the editor. The EditingControl must be a TextBox otherwise the property will return -1;
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int EditSelLength
        {
            get
            {
                TextBox editingControl = (base.EditingControl as TextBox);
                if (editingControl != null)
                {
                    return editingControl.SelectionLength;
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                TextBox editingControl = (base.EditingControl as TextBox);
                if (editingControl != null)
                {
                    editingControl.SelectionLength = value;
                }
            }
        }

        //RPU:
        /// <summary>
        /// Returns or sets the maximum number of characters that can be entered in the editor. The EditingControl must be a TextBox otherwise the property will return -1;
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int EditMaxLength
        {
            get
            {
                    return editMaxLength;
            }
            set
            {
                if(value < 0)
                {
                    throw new Exception("Invalid Property Value");
                }
                TextBox editingControl = (base.EditingControl as TextBox);
                if (editingControl != null)
                {
                    if (value > 0)
                    {
                        editingControl.MaxLength = value;
                    } else
                    {
                        //DSE Setting EditMaxLength to 0 allows editing of strings up to about 32k characters.
                        editingControl.MaxLength = Int16.MaxValue;
                    }
                    
                }
                editMaxLength = value;
            }
        }

		/// <summary>
		/// This property allows you to specify different sorting orders for each column on the grid.
		/// The most common settings for this property are flexSortGenericAscending and flexSortGenericDescending. 
		/// For a complete list of possible settings, see the Sort property.
		/// </summary>
		/// <param name="Col"></param>
		/// <param name="flexSortGenericAscending"></param>
		public void ColSort(int col, SortSettings sortSetting)
		{
            col = GetDGVColIndex(col);
            if (IsValidColumn(col))
            {
                if (!colSort.ContainsKey(col))
                {
                    colSort.Add(col, sortSetting);
                }
                else
                {
                    colSort[col] = sortSetting;
                }
            }
		}

		//RPU:
		/// <summary>
		/// Returns or sets the starting point of text selected in the editor. The EditingControl must be a TextBox otherwise the property will return -1;
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int EditSelStart
        {
            get
            {
                TextBox editingControl = (base.EditingControl as TextBox);
                if (editingControl != null)
                {
                    return editingControl.SelectionStart;
                }
                else
                {
                    return -1;
                }
            }
            set
            {
                TextBox editingControl = (base.EditingControl as TextBox);
                if (editingControl != null)
                {
                    editingControl.SelectionStart = value;
                }
            }
        }

        //RPU:
        /// <summary>
        /// Returns or sets the text in the cell editor.
        /// The EditText property allows you to read and modify the contents of the cell editor while it is active.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string EditText
        {
            get
            {
                if (base.CurrentCell != null && base.CurrentCell.OwningColumn is DataGridViewCheckBoxColumn)
                {   
                    return FCUtils.CBool(FCConvert.ToString(base.CurrentCell.EditedFormattedValue)) ? "1" : "0";
                }
                else if (base.EditingControl != null)
                {
                    if (base.EditingControl is ComboBox)
                    {
                        //FC:FINAL:SBE - Harris #i640 - if editing control is a combobox, it can contains Tab characters. Only the text before the first Tab should be returned in this case, as in VB6
                        string text = base.EditingControl.Text;
                        if (text.Contains('\t'))
                        {
                            return text.Split('\t')[0];
                        }
                        return text;
                    }
                    return base.EditingControl.Text;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (base.CurrentCell != null && base.CurrentCell.OwningColumn is DataGridViewCheckBoxColumn)
                {
                    base.CurrentCell.Value = value != "0";
                }
                else if (base.EditingControl != null)
                {
                    //FC:FINAL:MSH - i.issue #1268: add checking and setting full text to EditingControl for correct work(if value will be not full - EditingControl.Text won't be updated)
                    if (base.EditingControl is FCListViewComboBox)
                    {
                        List<string> items = (List<string>)(base.EditingControl as FCListViewComboBox).DataSource;
                        string[] tab = new string[] { "\t" };
                        bool found = false;
                        foreach (string item in items)
                        {
                            if (FCListViewComboBox.ExtractText(item).Split(tab, StringSplitOptions.None).First() == value)
                            {
                                base.EditingControl.Text = FCListViewComboBox.ExtractText(item);
                                break;
                            }
                        }
                    }
                    else
                    {
                        base.EditingControl.Text = value;
                    }
                }

            }
        }

        //RPU: 
        /// <summary>
        /// Sets whether the last column should be adjusted to fit the control's width.
        /// </summary>
        [DefaultValue(false)]
        public bool ExtendLastCol
        {
            get
            {
                return this.extendLastCol;
            }
            set
            {
                this.extendLastCol = value;
                //FC:FINAL:MSH - issue #1131: change AutoSizeMode only if set 'true'. In another case - disable AutoSize for all columns
                if (value)
                {
                    if (HasColumns)
                    {
                        ExtendLastVisibleColumn();
                    }
                }
                else
                {
                    DisableExtendedModeForColumns();
                }
            }
        }

		/// <summary>
		/// Returns or sets whether the Wisej.Web.TextBox control modifies the case of characters as they are typed (if grid cell contains Wisej.Web.TextBox control)
		/// </summary>
		[DefaultValue(CharacterCasing.Normal)]
		public CharacterCasing CellCharacterCasing
		{
			get;
			set;
		}

		/// <summary>
		/// Returns or sets the list to be used as a drop-down when editing a cell.
		/// The ComboList property controls the type of editor to be used when editing a cell. You may use a text box, drop-down list, drop-down combo, or an edit button to pop up custom editor forms.
		/// To use the ComboList property, set the Editable property to True, and respond to the BeforeEdit event by setting the ComboList property to a string containing the proper options
		/// </summary>
		[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string ComboList { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int MouseCol
        {
            get;
            //JEI:HARRIS:IT273: this.Col holds Col with the CurrentCell, but MouseCol should return the Col under the Mouse
            //{
            //    return this.Col;
            //}
            private set;
        }

        /// <summary>
        /// Returns the current mouse position, in row and column coordinates.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int MouseRow
        {
            get;
            //JEI:HARRIS:IT273: this.Row holds row with the CurrentCell, but MouseRow should return the Row under the Mouse
            //{
            //return this.Row;
            //}
            private set;
        }

        public Color BackColorFixed { get; set; }

        public Color BackColorBkg { get; set; }

        public Color BackColorSel { get; set; }
        public Color BackColorAlternate { get; set; }

        [DefaultValue(true)]
        public bool AllowSelection { get; set; }

        [DefaultValue(EditableSettings.flexEDNone)]
        public EditableSettings Editable
        {
            get
            {
                if (base.ReadOnly)
                {
                    return EditableSettings.flexEDNone;
                }
                else
                {
                    return EditableSettings.flexEDKbdMouse;
                }

            }
            set
            {
                switch (value)
                {
                    case EditableSettings.flexEDNone: base.ReadOnly = true; break;
                    case EditableSettings.flexEDKbdMouse: base.ReadOnly = false; /*TODO: SGA we need to decide here! this.EditMode = DataGridViewEditMode.EditOnKeystroke; */ break;
                    case EditableSettings.flexEDKbd: base.ReadOnly = false; /*TODO: SGA we need to decide here! this.EditMode = DataGridViewEditMode.EditOnKeystrokeOrF2; */ break;
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string EditMask
        {
            get
            {
                return editMask;
            }
            set
            {
                editMask = value;
                if(base.EditingControl != null && base.EditingControl is FCMaskedTextBox)
                {
                    (base.EditingControl as FCMaskedTextBox).Mask = value;
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public object CellButtonPicture { get; set; }

        //RPU:
        /// <summary>
        /// Returns the long value associated with an item in the editor's combo list.
        /// </summary>
        public string ComboData(int index = -1)
        {
            FCListViewComboBox combobox = base.EditingControl as FCListViewComboBox;
            
            if (combobox != null)
            {
                if (index == -1)
                {
                    index = combobox.SelectedIndex;
                }
                if (index != -1)
                {
                    List<string> items = (List<string>)combobox.DataSource;
                    return FCListViewComboBox.ExtractValue(items[index]);
                }
            }
            return "";
        }

        public TabBehaviorSettings TabBehavior
        {
            get
            {
                return base.StandardTab ? TabBehaviorSettings.flexTabControls : TabBehaviorSettings.flexTabCells;
            }
            set
            {
                base.StandardTab = (value == TabBehaviorSettings.flexTabControls);
            }
        }

        public void SortColumns(params KeyValuePair<int, SortSettings>[] columns)
        {
            var columnList = columns.Select(col => new KeyValuePair<DataGridViewColumn, SortSettings>(this.Columns[this.GetDGVColIndex(col.Key)], col.Value)).ToList();
            FCGridRowComparer comparer = new FCGridRowComparer(columnList);
            this.Sort(comparer);
        }

        public void SortColumns(SortSettings sortOrder, params int[] columns)
        {
            SortColumns(columns.Select(col => new KeyValuePair<int, SortSettings>(col, sortOrder)).ToArray());
        }

        //:RPU
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public SortSettings Sort
        {
            set
            {
                //FC:FINAL:MSH - execute sorting for all types (not only for flexSortGenericAscending)
                int col = GetDGVColIndex(this.Col);
                if (base.IsValidColumn(col))
                {
                    ListSortDirection sortDirection;
                    if (value == SortSettings.flexSortCustom || value == SortSettings.flexSortUseColSort)
                    {
                        if (colSort.ContainsKey(col))
                        {
                            value = colSort[col];
                        }
                    }
                    if (value == SortSettings.flexSortGenericAscending || value == SortSettings.flexSortNumericAscending ||
                        value == SortSettings.flexSortStringAscending || value == SortSettings.flexSortStringNoCaseAscending)
                    {
                        sortDirection = ListSortDirection.Ascending;
                    }
                    else
                    {
                        sortDirection = ListSortDirection.Descending;
                    }
                    base.Sort(base.Columns[col], sortDirection);
                    this.Select(1, this.Col);
                }
                //switch (value)
                //{
                //    case SortSettings.flexSortGenericAscending:
                //        int col = GetDGVColIndex(this.Col);
                //        if (base.IsValidColumn(col))
                //        {
                //            base.Sort(this.Columns[col], ListSortDirection.Ascending);
                //        }
                //        break;
                //}

            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool MoveCurrentCell = true;

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Index
        {
            get
            {
                return this.GetIndex();
            }
        }

        /// <summary>
        /// Returns or sets a ToolTip.
        /// Syntax
        /// object.ToolTipText [= string]
        /// The ToolTipText property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// string	A string associated with an object in the Applies To list. that appears in a small rectangle below the object when the user's cursor hovers over the object at run time for about one second.
        /// Remarks
        /// Instead of setting this property on MouseMove set the tooltips for each cell after the grid is populated or use the CellFormating event
        /// </summary>
        [Obsolete]
        [DefaultValue("")]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string ToolTipText
        {
            get; set;
        }

        /// <summary>
        /// save associated TextBox
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public TextBox EditorTextBox
        {
            get;
            set;
        }

        /// <summary>
        /// save associated TextBox Visible flag
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool EditorTextBoxVisible
        {
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the font used to display text in a control or in a run-time drawing or printing operation.
        /// Note   The FontName property is included for use with the CommonDialog control and for compatibility with earlier versions of Visual Basic. For additional functionality, 
        /// use the new Font object properties (not available for the CommonDialog control).
        /// Syntax
        /// object.FontName [= font]
        /// The FontName property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// font	A string expression specifying the font name to use.
        /// Remarks
        /// The default for this property is determined by the system. Fonts available with Visual Basic vary depending on your system configuration, display devices, and printing 
        /// devices. Font-related properties can be set only to values for which fonts exist.
        /// In general, you should change FontName before setting size and style attributes with the FontSize, FontBold, FontItalic, FontStrikethru, and FontUnderline properties.
        /// Note   At run time, you can get information on fonts available to the system through the FontCount and Fonts properties.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string FontName
        {
            get
            {
                return this.Font.Name;
            }
            set
            {
                this.SetFontName(value);
            }
        }

        /// <summary>
        /// FontBold
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontBold
        {
            get
            {
                return this.Font.Bold;
            }
            set
            {
                this.SetFontBold(value);
            }
        }

        /// <summary>
        /// FontItalic
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontItalic
        {
            get
            {
                return this.Font.Italic;
            }
            set
            {
                this.SetFontItalic(value);
            }
        }

        /// <summary>
        /// FontUnderline
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontUnderline
        {
            get
            {
                return this.Font.Underline;
            }
            set
            {
                this.SetFontUnderline(value);
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether an MSHFlexGrid should allow regular cell selection, selection by rows, or selection by columns.
        /// flexSelectionFree	0	Free. This allows individual cells in the MSHFlexGrid to be selected, spreadsheet style. This is the default.
        /// flexSelectionByRow	1	By Row. This forces selections to span entire rows, as in a multi-column list box or record-based display.
        /// flexSelectionByColumn	2	By Column. This forces selections to span entire columns, as if selecting ranges for a chart or fields for sorting.
        /// </summary>
        [DefaultValue(SelectionModeSettings.flexSelectionFree)]
        public new SelectionModeSettings SelectionMode
        {
            get
            {
                return this.m_selectionMode;
            }
            set
            {
                this.m_selectionMode = value;
                switch (this.SelectionMode)
                {
                    case SelectionModeSettings.flexSelectionFree:
                        base.SelectionMode = DataGridViewSelectionMode.CellSelect;
                        break;
                    case SelectionModeSettings.flexSelectionByRow:
					case SelectionModeSettings.flexSelectionListBox:
                        base.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
                        break;
                    case SelectionModeSettings.flexSelectionByColumn:
                        base.SelectionMode = DataGridViewSelectionMode.FullColumnSelect;
                        break;
                }
            }
        }

        /// <summary>
        /// When retrieving, the Text property always retrieves the contents of the current cell as defined by the Row and Col properties.
        /// When setting, the Text property sets the contents of the current cell or selection depending on the setting of the FillStyle property.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public new string Text
        {
            get
            {
                DataGridViewCell cell = GetCurrentCell();
                return Convert.ToString(cell.Value);
            }
            set
            {
                DataGridViewCell cell = GetCurrentCell();
                cell.Value = value;
            }
        }

        /// <summary>
        /// Returns or sets the uppermost visible row (other than a fixed row) in the MSHFlexGrid. This property is not available at design time.
        /// Remarks
        /// You can use this property to programmatically read or set the visible top row of the MSHFlexGrid. Use the LeftCol property to determine the leftmost visible column in the MSHFlexGrid.
        /// The largest row number that you can use when setting TopRow is the total number of rows minus the number of rows that are visible in the MSHFlexGrid. If this property is set to a greater 
        /// row number, the MSHFlexGrid will reset it to this largest possible value.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Browsable(false)]
        public int TopRow
        {
            get
            {
                int fixedRowsCount = this.GetFixedRows().Count;
                if (this.RowCount <= fixedRowsCount)
                {
                    return -1;
                }
                else
                {
                    int topRow = this.FirstDisplayedRowIndex;
                    if (topRow > -1)
                    {
                        return this.GetFlexRowIndex(topRow);
                    }
                    else
                    {
                        return fixedRowsCount;
                    }
                }
            }
            set
            {
                if (this.HasRows)
                {
                    //AM:HARRIS:#284 - ScrollCellIntoView only scrolls the row to the last visible position; adjust the index to have the row on top
                    int row = this.GetDGVRowIndex(value);
                    row += (this.Height - 30) / base.Rows[0].Height - 1;
                    if(row >= base.RowCount)
                    {
                        row = base.RowCount - 1;
                    }
                    //FC:FINAL:RPU: #1798 - The column that owns the cell should be visible in order to scroll cell into view
                    //base.ScrollCellIntoView(0, row);
                    base.ScrollCellIntoView(FirstVisibleCol, row);
                }
            }
        }

        /// <summary>
        ///  Returns or sets the color used to draw the grid lines between the fixed cells.
        ///  Remarks
        /// The GridColorFixed and GridLinesFixed properties determine the appearance of the grid lines displayed in the fixed area of the grid.GridColor and GridLines determine the appearance of the grid lines displayed in the scrollable area of the grid.
        /// The GridColorFixed property is ignored when GridLinesFixed is set to one of the 3D styles.Raised and inset grid lines are always drawn using the system-defined colors for shades and highlights.
        /// </summary>

        [DefaultValue(0)]
        public Color GridColorFixed
        {
            //TODO not implemented
            get
            {
                return this.m_gridColorFixed;
            }
            set
            {
                if (value == null)
                {
                    return;
                }

                this.m_gridColorFixed = value;
            }
        }

        /// <summary>
        /// Returns or sets the ToolTip text shown while the user scrolls vertically.
        /// Syntax
        /// [form!] VSFlexGrid.ScrollTipText[ = value As String ]
        /// Remarks
        /// Set this property in response to the BeforeScrollTip event to display information describing a given row as the user scrolls the contents of the control.
        /// For more details, see the ScrollTips property.
        /// </summary>
        [DefaultValue("")]
        public String ScrollTipText
        {
            //TODO not implemented
            get
            {
                return this.m_scrollTipText;
            }
            set
            {
                this.m_scrollTipText = value;
            }
        }
        
        /// <summary>
        /// Returns or sets the colors used to draw text on fixed part
        /// </summary>
        [DefaultValue(0)]
        public Color ForeColorFixed
        {
            get
            {
                return this.m_foreColorFixed;
            }
            set
            {
                if (value == null)
                {
                    return;
                }
                if (m_foreColorFixed != value)
                {
                    m_foreColorFixedBackup = this.ColumnHeadersDefaultCellStyle.ForeColor;
                }
                this.m_foreColorFixed = value;
                base.ColumnHeadersDefaultCellStyle.ForeColor = this.m_foreColorFixed;
                base.RowHeadersDefaultCellStyle.ForeColor = this.m_foreColorFixed;

                if (HasColumns)
                {
                    for (int i = 0; i < this.GetDGVColIndex(this.FixedCols); i++)
                    {
                        base.Columns[i].DefaultCellStyle.ForeColor = this.m_foreColorFixed;
                    }
                }

                if (HasRows)
                {
                    for (int j = 0; j < this.GetDGVRowIndex(this.FixedRows); j++)
                    {
                        base.Rows[j].DefaultCellStyle.ForeColor = this.m_foreColorFixed;
                    }
                }

            }
        }

        /// <summary>
        /// RowSel Returns or sets the start or end row for a range of cells.
        /// These properties are not available at design time.
        /// object.RowSel [= value]
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// value	A Long value that specifies the start or end row, or column, for a range of cells.
        /// Remarks
        /// You can use these properties to select a specific region of the MSHFlexGrid programmatically, or to read the dimensions of an area that the user selects into code.
        /// The MSHFlexGrid cursor is in the cell at Row, Col. The MSHFlexGrid selection is the region between rows Row and RowSel and columns Col and ColSel. Note that RowSel 
        /// may be above or below Row, and ColSel may be to the left or to the right of Col.
        /// Whenever you set the Row and Col properties, RowSel and ColSel are automatically reset, so the cursor becomes the current selection. 
        /// To select a block of cells from code, you must first set the Row and Col properties, and then set RowSel and ColSel.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int RowSel
        {
            get
            {
                return m_rowSel;
            }
            set
            {
                if (value < 0 || value > this.Rows)
                {
                    return;
                }
                m_rowSel = value;
            }
        }

        /// <summary>
        /// ColSel Returns or sets the start or end column for a range of cells.
        /// These properties are not available at design time.
        /// object.ColSel [= value]
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// value	A Long value that specifies the start or end row, or column, for a range of cells.
        /// Remarks
        /// You can use these properties to select a specific region of the MSHFlexGrid programmatically, or to read the dimensions of an area that the user selects into code.
        /// The MSHFlexGrid cursor is in the cell at Row, Col. The MSHFlexGrid selection is the region between rows Row and RowSel and columns Col and ColSel. Note that RowSel 
        /// may be above or below Row, and ColSel may be to the left or to the right of Col.
        /// Whenever you set the Row and Col properties, RowSel and ColSel are automatically reset, so the cursor becomes the current selection. 
        /// To select a block of cells from code, you must first set the Row and Col properties, and then set RowSel and ColSel.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ColSel
        {
            get
            {
                return m_colSel;
            }
            set
            {
                if (value < 0 || value > this.Cols)
                {
                    return;
                }
                m_colSel = value;
            }
        }

        public string ComboItem(int index)
        {
            FCListViewComboBox combobox = base.EditingControl as FCListViewComboBox;
            if (combobox != null)
            {
                if (combobox.SelectedIndex != -1)
                {
                    List<string> items = (List<string>)combobox.DataSource;
                    return FCListViewComboBox.ExtractValue(items[index]);

                }
            }
            else if(comboListItems != null && index < comboListItems.Count)
            {
                return FCListViewComboBox.ExtractValue(comboListItems[index]);
            }
            return "";
        }

        /// <summary>
        /// Returns or sets a value that determines whether cells with the same contents should be grouped in a single cell spanning multiple rows or columns.
        /// object.MergeCells [=value]
        /// value	An integer or constant that specifies the grouping (merging) of cells, as specified:
        /// flexMergeNever	0	Never. The cells containing identical content are not grouped. This is the default.
        /// flexMergeFree	1	Free. Cells with identical content always merge.
        /// flexMergeRestrictRows	2	Restrict Rows. Only adjacent cells (to the left of the current cell) within the row containing identical content merge.
        /// flexMergeRestrictColumns	3	Restrict Columns. Only adjacent cells (to the top of the current cell) within the column containing identical content merge.
        /// flexMergeRestrictBoth	4	Restrict Both. Only adjacent cells within the row (to the left) or column (to the top) containing identical content merge.
        /// Remarks
        /// The ability to merge cells enables you to present data in a clear, concise manner. You can use cell merging in conjunction with the sorting and column order functions of the MSHFlexGrid.
        /// To use the cell merging capabilities of the MSHFlexGrid:
        /// Set MergeCells to a value other than zero. (The difference between the settings is explained in the example.)
        /// Set the MergeRow and MergeCol array properties to True for the rows and columns to be merged.
        /// When using the cell merging capabilities, the MSHFlexGrid merges cells with identical content. The merging is automatically updated whenever the cell content changes.
        /// When MergeCells is set to a value other than 0 (Never), selection highlighting is automatically turned off. 
        /// This is done to speed up repainting, and because selection of ranges containing merged cells may lead to unexpected results.
        /// </summary>
        [DefaultValue(MergeCellsSettings.flexMergeNever)]
        public MergeCellsSettings MergeCells
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns the position and size of the current cell, in twips. These properties are not available at design time.
        /// Remarks
        /// These properties are useful if you want to emulate in-cell editing. 
        /// By trapping the MSHFlexGrid's KeyPress event, you can place a text box or some other control over the current cell and let the user edit its contents.
        /// The return values are always in twips, regardless of the forms ScaleMode setting.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int CellTop
        {
            get
            {
                if (base.CurrentCell == null)
                {
                    return 0;
                }
                //FCA: GetCellDisplayRectangle return 0 when grid not visible                
                if (this.Visible)
                {
                    //using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                        return Convert.ToInt32(fcGraphics.ScaleY(this.GetCellDisplayRectangle(base.CurrentCell.ColumnIndex, base.CurrentCell.RowIndex, false).Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                }
                else
                {
                    this.Visible = true;
                    int cellTop = 0;
                    //using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                        cellTop = Convert.ToInt32(fcGraphics.ScaleY(this.GetCellDisplayRectangle(base.CurrentCell.ColumnIndex, base.CurrentCell.RowIndex, false).Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                    if (this.Visible)
                    {
                        this.Visible = false;
                    }
                    return cellTop;
                }
            }
        }

        /// <summary>
        /// Returns the position and size of the current cell, in twips. These properties are not available at design time.
        /// Remarks
        /// These properties are useful if you want to emulate in-cell editing. 
        /// By trapping the MSHFlexGrid's KeyPress event, you can place a text box or some other control over the current cell and let the user edit its contents.
        /// The return values are always in twips, regardless of the forms ScaleMode setting.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int CellLeft
        {
            get
            {
                if (base.CurrentCell == null)
                {
                    return 0;
                }
                //FCA: GetCellDisplayRectangle return 0 when grid not visible                
                if (this.Visible)
                {
                    //using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                        return Convert.ToInt32(fcGraphics.ScaleX(this.GetCellDisplayRectangle(base.CurrentCell.ColumnIndex, base.CurrentCell.RowIndex, false).Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                }
                else
                {
                    this.Visible = true;
                    int cellLeft = 0;
                    //using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                        cellLeft = Convert.ToInt32(fcGraphics.ScaleX(this.GetCellDisplayRectangle(base.CurrentCell.ColumnIndex, base.CurrentCell.RowIndex, false).Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                    if (this.Visible)
                    {
                        this.Visible = false;
                    }
                    return cellLeft;
                }

            }
        }

        /// <summary>
        /// Returns the position and size of the current cell, in twips. These properties are not available at design time.
        /// Remarks
        /// These properties are useful if you want to emulate in-cell editing. 
        /// By trapping the MSHFlexGrid's KeyPress event, you can place a text box or some other control over the current cell and let the user edit its contents.
        /// The return values are always in twips, regardless of the forms ScaleMode setting.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int CellWidth
        {
            get
            {
                DataGridViewCell cell = base.CurrentCell;
                if (cell == null)
                {
                    cell = GetFirstVisibleCell();
                    if (cell == null)
                    {
                        return 0;
                    }
                }
                //FCA: GetCellDisplayRectangle return 0 when grid not visible                
                if (this.Visible)
                {
                    //using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                        return Convert.ToInt32(fcGraphics.ScaleX(this.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false).Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                }
                else
                {
                    this.Visible = true;
                    int cellWidth = 0;
                    //using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                        int cellDisplayRectangleWidth = this.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false).Width;
                        //CHE: if form is not Visible then Grid.Visible cannot be set to true and GetCellDisplayRectangle still return 0, for this case use cell.Size property
                        if (cellDisplayRectangleWidth == 0)
                        {
                            cellDisplayRectangleWidth = cell.Size.Width;
                        }
                        cellWidth = Convert.ToInt32(fcGraphics.ScaleX(cellDisplayRectangleWidth, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                    if (this.Visible)
                    {
                        this.Visible = false;
                    }
                    return cellWidth;
                }
            }
        }

        /// <summary>
        /// Returns the position and size of the current cell, in twips. These properties are not available at design time.
        /// Remarks
        /// These properties are useful if you want to emulate in-cell editing. 
        /// By trapping the MSHFlexGrid's KeyPress event, you can place a text box or some other control over the current cell and let the user edit its contents.
        /// The return values are always in twips, regardless of the forms ScaleMode setting.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int CellHeight
        {
            get
            {
                DataGridViewCell cell = base.CurrentCell;
                if (cell == null)
                {
                    cell = GetFirstVisibleCell();
                    if (cell == null)
                    {
                        return 0;
                    }
                }
                //FCA: GetCellDisplayRectangle return 0 when grid not visible                
                if (this.Visible)
                {
                    //using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                        return Convert.ToInt32(fcGraphics.ScaleY(this.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false).Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                }
                else
                {
                    this.Visible = true;
                    int cellHeight = 0;
                    //using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                        int cellDisplayRectangleHeight = this.GetCellDisplayRectangle(cell.ColumnIndex, cell.RowIndex, false).Height;
                        //CHE: if form is not Visible then Grid.Visible cannot be set to true and GetCellDisplayRectangle still return 0, for this case use cell.Size property
                        if (cellDisplayRectangleHeight == 0)
                        {
                            cellDisplayRectangleHeight = cell.Size.Height;
                        }
                        cellHeight = Convert.ToInt32(fcGraphics.ScaleY(cellDisplayRectangleHeight, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                    if (this.Visible)
                    {
                        this.Visible = false;
                    }
                    return cellHeight;
                }
            }
        }

        /// <summary>
        /// Returns or sets the width, in pixels, of the lines displayed between cells, bands, headers, indents, or unpopulated areas.
        /// </summary>
        [DefaultValue(1)]
        public int GridLineWidth
        {
            get
            {
                return m_gridLineWidth;
            }
            set
            {
                m_gridLineWidth = value;
                // TODO
                //foreach (DataGridViewColumn columnItem in base.Columns)
                //{
                //    columnItem.DividerWidth = m_gridLineWidth;
                //}

                //foreach (DataGridViewRow rowItem in base.Rows)
                //{
                //    rowItem.DividerHeight = m_gridLineWidth;
                //}
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether the MSHFlexGrid should be automatically redrawn after each change.
        /// </summary>
        [DefaultValue(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool Redraw
        {
            get
            {
                return redrawLock == 0;
            }
            set
            {
                //BeginUpdate when Redraw is set to false
                if (!value)
                {
                    redrawLock--;
                    this.BeginUpdate();
                }
                else
                {
                    redrawLock++;
                }
                //EndUpdate when Redraw is set to true, and redrawLock is 0 again
                if (redrawLock == 0)
                {
                    this.EndUpdate();
                }
                //redraw property should be used the following way: Redraw = false, do stuff, Redraw = true
                //if Redraw was set to true without being set to false before, then reset the redrawLock
                if (redrawLock > 0)
                {
                    redrawLock = 0;
                }
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether the Grid should be use the ColWidth() with the scaleFactor defined in the constructor.
        /// </summary>
        [DefaultValue(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool UseScaleFactor
        {
            // TODO:BBE
            get;
            set;
        }

        /// <summary>
        /// Returns or sets the three-dimensional style for text in a specific cell or range of cells. This property is not available at design time.
        /// flexTextFlat	0	Default. The text is normal, flat text.
        /// flexTextRaised	1	The text appears raised.
        /// flexTextInset	2	The text appears inset.
        /// flexTextRaisedLight	3	The text appears slightly raised.
        /// flexTextInsetLight	4	The text appears slightly inset.
        /// Remarks
        /// Settings 1 and 2 work best for large and bold fonts. Settings 3 and 4 work best for small regular fonts. 
        /// The cells appearance is also affected by the BackColor settings; some BackColor settings do not show the raised or inset feature.
        /// Changing this property affects the current cell or the current selection, depending on the setting of the FillStyle property.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [DefaultValue(TextStyleSettings.flexTextFlat)]
        public TextStyleSettings CellTextStyle
        {
            // TODO:CHE
            get;
            set;
        }

        /// <summary>
        /// Returns or sets a value that determines whether the MSFlexGrid should draw a focus rectangle around the current cell.
        /// flexFocusNone	0	There is no focus rectangle around the current cell.
        /// flexFocusLight	1	There is a light focus rectangle around the current cell. This is the default.
        /// flexFocusHeavy	2	There is a heavy focus rectangle around the current cell.
        /// Remarks
        /// If a focus rectangle is drawn, the current cell is painted in the background color, as in most spreadsheets and grids. 
        /// Otherwise, the current cell is painted in the selection color, hence you can see which cell is selected without the focus rectangle.
        /// </summary>
        [DefaultValue(FocusRectSettings.flexFocusLight)]
        public FocusRectSettings FocusRect
        {
            get
            {
                return m_focusRect;
            }
            set
            {
                m_focusRect = value;
            }
        }

        /// <summary>
        /// Determines whether selected cells appear highlighted
        /// flexHighlightNever	0	There is no highlight on the selected cells.
        /// flexHighlightAlways	1	The selected cells are always highlighted. (Default)
        /// flexHighlightWithFocus	2	The highlight appears only when the control has focus.
        /// Remarks
        /// When this property is set to zero and a range of cells is selected, there is no visual cue or emphasis indicating the selected cells.
        /// </summary>
        [DefaultValue(HighLightSettings.flexHighlightAlways)]
        public HighLightSettings HighLight
        {
            get
            {
                return m_highLight;
            }
            set
            {
                m_highLight = value;
                //CHE: set SelectionBackColor and SelectionForeColor
                SetSelectionColors(base.DefaultCellStyle);
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether an MSFlexGrid has horizontal and/or vertical scroll bars.
        /// flexScrollNone	0	The MSFlexGrid has no scroll bars.
        /// flexScrollHorizontal	1	The MSFlexGrid has a horizontal scroll bar.
        /// flexScrollVertical	2	The MSFlexGrid has a vertical scroll bar.
        /// flexScrollBoth	3	The MSFlexGrid has horizontal and vertical scroll bars. (Default)
        /// Remarks
        /// Scroll bars appear on an MSHFlexGrid only if its contents extend beyond its borders and value specifies scroll bars. 
        /// If the ScrollBars property is set to None, the MSHFlexGrid will not have scroll bars, regardless of its contents.
        /// Note   If the MSHFlexGrid has no scroll bars in either direction, it will not allow any scrolling in that direction, even if the user 
        /// uses the keyboard to select a cell that is beyond the visible area of the control.
        /// </summary>
        [DefaultValue(ScrollBarsSettings.flexScrollBoth)]
        public new ScrollBarsSettings ScrollBars
        {
            get
            {
                return m_scrollBars;
            }
            set
            {
                m_scrollBars = value;
                switch (m_scrollBars)
                {
                    case ScrollBarsSettings.flexScrollNone:
                        base.ScrollBars = Wisej.Web.ScrollBars.None;
                        break;
                    case ScrollBarsSettings.flexScrollHorizontal:
                        base.ScrollBars = Wisej.Web.ScrollBars.Horizontal;
                        break;
                    case ScrollBarsSettings.flexScrollVertical:
                        base.ScrollBars = Wisej.Web.ScrollBars.Vertical;
                        break;
                    case ScrollBarsSettings.flexScrollBoth:
                        base.ScrollBars = Wisej.Web.ScrollBars.Both;
                        break;
                }
            }
        }

        public void DataRefresh()
        {
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Returns or sets whether clicking on the fixed area will select entire columns and rows.
        /// If AllowBigSelection is set to True, clicking on the top left fixed cell selects the entire grid.
        /// </summary>
        [DefaultValue(true)]
        public bool AllowBigSelection
        {
            get
            {
                return m_allowBigSelection;
            }
            set
            {
                m_allowBigSelection = value;
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether the user can use the mouse to resize rows and columns in the MSHFlexGrid.
        /// Syntax
        /// object.AllowUserResizing [=value]
        /// The AllowUserResizing property syntax has these parts:
        /// Part	Description
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// value	An integer or constant that specifies whether a user can resize rows and columns, as described in Settings.
        /// Settings
        /// The settings for value are:
        /// Constant	Value	Description
        /// flexResizeNone	0	None. Default. The user cannot resize with the mouse.
        /// flexResizeColumns	1	Columns. The user can resize columns using the mouse.
        /// flexResizeRows	2	Rows. The user can resize rows using the mouse.
        /// flexResizeBoth	3	Both. The user can resize columns and rows using the mouse.
        /// Remarks
        /// To resize rows or columns, the mouse must be over the fixed area of the MSHFlexGrid and close to a border between rows and columns. 
        /// The mouse pointer changes into an appropriate sizing pointer, and the user can drag the row or column to change the row height or column width.
        /// </summary>
        [DefaultValue(AllowUserResizeSettings.flexResizeNone)]
        public AllowUserResizeSettings AllowUserResizing
        {
            get
            {
                return allowUserResizing;
            }
            set
            {
                allowUserResizing = value;
                switch (value)
                {
                    case AllowUserResizeSettings.flexResizeNone:
                        {
                            this.AllowUserToResizeColumns = false;
                            this.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                            this.AllowUserToResizeRows = false;
                            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                            break;
                        }
                    case AllowUserResizeSettings.flexResizeColumns:
                        {
                            this.AllowUserToResizeColumns = true;
                            this.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
                            this.AllowUserToResizeRows = false;
                            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
                            break;
                        }
                    case AllowUserResizeSettings.flexResizeRows:
                        {
                            this.AllowUserToResizeColumns = false;
                            this.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.DisableResizing;
                            this.AllowUserToResizeRows = true;
                            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
                            break;
                        }
                    case AllowUserResizeSettings.flexResizeBoth:
                        {
                            this.AllowUserToResizeColumns = true;
                            this.RowHeadersWidthSizeMode = DataGridViewRowHeadersWidthSizeMode.EnableResizing;
                            this.AllowUserToResizeRows = true;
                            this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Returns or sets a value that determines whether a cell displays multiple lines of text or one long line of text.
        /// Note   Return characters, such as Chr(13), also force line breaks.
        /// Syntax
        /// object.WordWrap [=Boolean]
        /// object	An object expression that evaluates to an object in the Applies To list.
        /// Boolean	A Boolean expression that specifies whether the text within a cell wraps.
        /// True	The cell text displays with multiple, wrapping lines of text.
        /// False	The cell text displays as one long line of text. This is the default.
        /// Remarks
        /// The MSHFlexGrid displays text slightly faster when WordWrap is set to False.
        /// </summary>
        [DefaultValue(false)]
        public bool WordWrap
        {
            get
            {
                if (base.DefaultCellStyle.WrapMode == DataGridViewTriState.NotSet)
                {
                    base.DefaultCellStyle.WrapMode = DataGridViewTriState.False;
                }

                return base.DefaultCellStyle.WrapMode == DataGridViewTriState.True;
            }
            set
            {
                if (value)
                {
                    base.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
                }
                else
                {
                    base.DefaultCellStyle.WrapMode = DataGridViewTriState.False;
                }
            }
        }

        /// <summary>
        /// Returns or sets the total number of fixed rows, by default 1
        /// </summary>
        [DefaultValue(1)]
        public int FixedRows
        {
            get
            {
                return this.m_fixedRows;
            }
            set
            {
                if (this.m_fixedRows == value || value < 0 || value > this.Rows)
                {
                    return;
                }
                bool columnHeadersVisible = value > 0;
                if (this.ColumnHeadersVisible != columnHeadersVisible)
                {
                    this.ColumnHeadersVisible = columnHeadersVisible;
                    this.AutoSizeGridHeight();
                }
                //if (this.m_fixedRows < value)
                //{
                //    for (int i = 0; i < this.GetDGVRowIndex(value); i++)
                //    {
                //        base.Rows[i].Frozen = true;
                //        base.Rows[i].DefaultCellStyle.BackColor = base.RowHeadersDefaultCellStyle.BackColor;
                //        base.Rows[i].ReadOnly = true;
                //    }
                //}
                //else
                //{
                //    for (int i = this.GetDGVRowIndex(value); i < this.GetDGVRowIndex(this.m_fixedRows) && i >= 0; i++)
                //    {
                //        base.Rows[i].Frozen = false;
                //        base.Rows[i].DefaultCellStyle.BackColor = Color.White;
                //        base.Rows[i].ReadOnly = false;
                //        foreach (DataGridViewCell item in base.Rows[i].Cells)
                //        {
                //            if (item.OwningColumn.Frozen)
                //            {
                //                item.Style.BackColor = base.RowHeadersDefaultCellStyle.BackColor;
                //                item.ReadOnly = true;
                //            }
                //        }
                //    }

                //    ResetForeColorFixed();
                //}
                if (value > 0)
                {
                    this.ColumnHeadersHeight = 30 * value;
                }
                this.m_fixedRows = value;
                //FC:FINAL:MSH - issue #908: update RowCount after changing FixedRow value
                if(this.Rows > this.FixedRows)
                {
                    base.RowCount = this.FixedRows > 0 ? this.Rows - this.FixedRows : this.Rows;
                }
                else
                {
                    base.RowCount = 0;
                }
                //CHE: Row/Col property is changing when FixedRows/FixedCols is changed
                //BCU: If there are no rows, then current row should be 0
                m_currentRow = this.FixedRows > 0 && this.Rows > this.FixedRows ? m_fixedRows : 0;
            }
        }

        /// <summary>
        /// Returns or sets the total number of fixed columns, by default 1
        /// </summary>
        [DefaultValue(1)]
        public int FixedCols
        {
            get
            {
                return this.m_fixedColumns;
            }
            set
            {
                if (this.m_fixedColumns == value || value < 0 || value > this.Cols)
                {
                    return;
                }
                this.RowHeadersVisible = value > 0;
                if (value > 1)
                {
					//FC:FINAL:DSE:#i2196 Last Grid column should not be frozen 
					this.m_fixedColumns = 1;
                    if (this.m_fixedColumns < value)
                    {
                        for (int i = 0; i < this.GetDGVColIndex(value); i++)
                        {
                            base.Columns[i].Frozen = true;
                            base.Columns[i].DefaultCellStyle.BackColor = base.ColumnHeadersDefaultCellStyle.BackColor;
                            base.Columns[i].ReadOnly = true;
                        }
                    }
                    if (this.m_fixedColumns > value)
                    {
                        for (int i = this.GetDGVColIndex(value); i < this.GetDGVColIndex(this.m_fixedColumns); i++)
                        {
                            base.Columns[i].Frozen = false;
                            base.Columns[i].DefaultCellStyle.BackColor = Color.White;
                            base.Columns[i].ReadOnly = false;
                        }

                        ResetForeColorFixed();
                    }
                }
                this.m_fixedColumns = value;
                //FC:FINAL:MSH - issue #912: update count of columns
                if(this.Cols > this.FixedCols)
                {
                    this.Cols = m_cols;
                }
                if (value == 0)
                {
                    //Update again the column count
                    this.Cols = m_cols;
                }
                //CHE: Row/Col property is changing when FixedRows/FixedCols is changed
                //BCU: if there are no columns, then CurrentColumn should be 0
                this.m_currentColumn = this.FixedCols > 0 && this.Cols > this.FixedCols ? this.m_fixedColumns : 0;
            }
        }


        /// <summary>
        /// Returns or sets the coordinates of the active cell, not available at design time.
        /// Use these properties to specify a cell in an MSFlexGrid or to determine which row or column contains the current cell. 
        /// Columns and rows are numbered from zero rows are numbered from top to bottom, and columns are numbered from left to right.
        /// Setting these properties automatically resets RowSel and ColSel, the selection becoming the current cell. Therefore, to specify a block selection, 
        /// you must set Row and Col first, and then set RowSel and ColSel.
        /// The value of the current cell, defined by the Col and Row settings, is the text contained in that cell. To modify a cells value without changing 
        /// the selected Row and Col properties, use the TextMatrix property.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Row
        {
            get
            {
                return this.m_currentRow;
            }
            set
            {
                //exit if no changes
                if (this.m_currentRow == value)
                {
                    return;
                }

                //AM:HARRIS:#2146 - fire here BeforeRowColChange when the Row property is changed from the code
                if (this.BeforeRowColChange != null && !beforeRowColChangeFired)
                {
                    beforeRowColChangeFired = true;
                    BeforeRowColChangeEventArgs args = new BeforeRowColChangeEventArgs();
                    args.OldCol = this.Col;
                    args.OldRow = this.m_currentRow;
                    args.NewCol = this.Col;
                    args.NewRow = value;
                    this.BeforeRowColChange(this, args);
                    if (args.Cancel)
                    {
                        return;
                    }
                }
                this.m_currentRow = value;

                #region set CurrentCell

                if (MoveCurrentCell)
                {
                    int rowIndex = this.GetDGVRowIndex(value);

                    if (fromCurrentCellChanged || !this.IsRowValid(rowIndex))
                    {
                        //if rowIndex is -1 cannot set the current cell to column header, but still the event RowColChange needs to be fired
                        if (rowIndex < 0)
                        {
                            inRowColSetter = true;
                            base.OnCurrentCellChanged(EventArgs.Empty);
                            inRowColSetter = false;
                        }
                        return;
                    }

                    inRowColSetter = true;

                    bool unchanged = false;

                    if (this.CurrentCell != null)
                    {
                        DataGridViewCell cell = this[this.CurrentCell.ColumnIndex, rowIndex];
                        if (this.CurrentCell == cell || !cell.Visible)
                        {
                            unchanged = true;
                        }
                        if (cell.Visible)
                        {
                            //P2218:SBE:#4477 - switch to another cell only after exiting EditMode
                            if (this.IsCurrentCellInEditMode)
                            {
                                if (this.EndEdit())
                                {
                                    this.CurrentCell = cell;
                                }
                            }
                            else
                            {
                                this.CurrentCell = cell;
                            }
                            
                        }
                    }
                    else
                    {
                        //at least one column
                        if (this.ColumnCount > 0)
                        {
                            DataGridViewCell cell = this[FirstVisibleCol, rowIndex];
                            if (cell.Visible)
                            {
                                if (this.CurrentCell == cell)
                                {
                                    unchanged = true;
                                }
                                this.CurrentCell = cell;
                            }
                            else
                            {
                                if (this.CurrentCell == null)
                                {
                                    unchanged = true;
                                }
                                this.CurrentCell = null;
                            }
                        }
                    }

                    //still trigger the event if the CurrentCell was not changed (maybe had remained the same due to the case when current cell could not be set to row header or column header)
                    if (unchanged)
                    {
                        base.OnCurrentCellChanged(EventArgs.Empty);
                    }

                    inRowColSetter = false;
                    beforeRowColChangeFired = false;
                }

                #endregion
            }
        }

        /// <summary>
        /// Returns or sets the coordinates of the active cell, not available at design time.
        /// Use these properties to specify a cell in an MSFlexGrid or to determine which row or column contains the current cell. 
        /// Columns and rows are numbered from zero rows are numbered from top to bottom, and columns are numbered from left to right.
        /// Setting these properties automatically resets RowSel and ColSel, the selection becoming the current cell. Therefore, to specify a block selection, 
        /// you must set Row and Col first, and then set RowSel and ColSel.
        /// The value of the current cell, defined by the Col and Row settings, is the text contained in that cell. To modify a cells value without changing 
        /// the selected Row and Col properties, use the TextMatrix property.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Col
        {
            get
            {
                return this.m_currentColumn;
            }
            set
            {
                //exit if no changes
                if (this.m_currentColumn == value)
                {
                    return;
                }

                this.m_currentColumn = value;

                #region set CurrentCell

                if (MoveCurrentCell)
                {
                    int colIndex = this.GetDGVColIndex(value);

                    if (fromCurrentCellChanged || !this.IsColValid(colIndex) || colIndex < 0)
                    {
                        //if colIndex is -1 cannot set the current cell to row header, but still the event RowColChange needs to be fired
                        if (colIndex < 0)
                        {
                            inRowColSetter = true;
                            base.OnCurrentCellChanged(EventArgs.Empty);
                            inRowColSetter = false;
                        }
                        return;
                    }

                    inRowColSetter = true;

                    bool unchanged = false;

                    if (this.Columns[colIndex].Visible)
                    {
                        if (CurrentRow != null)
                        {
                            DataGridViewCell cell = GetDataGridViewCell(this.CurrentRow.Index, colIndex);
                            //FC:FINAL:SBE - Harris #i538 - in some cases DataGridView cell is null, and exception is thrown
                            if (this.CurrentCell == cell || cell.DataGridView == null)
                            {
                                unchanged = true;
                            }
                            else
                            {
                                //P2218:SBE:#4477 - switch to another cell only after exiting EditMode
                                if (this.IsCurrentCellInEditMode)
                                {
                                    if (this.EndEdit())
                                    {
                                        this.CurrentCell = cell;
                                    }
                                }
                                else
                                {
                                    this.CurrentCell = cell;
                                }
                            }
                        }
                    }

                    //still trigger the event if the CurrentCell was not changed (maybe had remained the same due to the case when current cell could not be set to row header or column header)
                    if (unchanged)
                    {
                        base.OnCurrentCellChanged(EventArgs.Empty);
                    }

                    inRowColSetter = false;
                }

                #endregion
            }
        }

        //CHE: remove from designer because there is a bug in Winforms: if you set ColumnCount in base class then when saving each time the designer 
        //will add some new columns beside the ones that were generated on previous saving
        //[Browsable(false)]
        //[DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Cols
        {
            get
            {
                return m_cols;
            }
            set
            {
                disableEvents = true;
                m_cols = value;
                if (this.Cols < 1)
                {
					//DDU:HARRIS:#i2457 - if ColumnCount = 0, removes all columns and rows and set HasRows to false, even if Rows != 0
					int rows = this.Rows;
                    base.ColumnCount = 0;
					this.Rows = rows;
                }
                else
                {
                    // CHE: by default, column SortMode is set to Automatic and when SelectionMode is ColumnHeaderSelect then exception is thrown:
                    // "Column's SortMode cannot be set to Automatic while the DataGridView control's SelectionMode is set to ColumnHeaderSelect."
                    bool wasColumnHeaderSelect = false;
                    if (base.SelectionMode == DataGridViewSelectionMode.ColumnHeaderSelect)
                    {
                        wasColumnHeaderSelect = true;
                        base.SelectionMode = DataGridViewSelectionMode.CellSelect;
                    }

                    base.ColumnCount = this.FixedCols > 0 ? Cols - 1 : Cols;

                    if (wasColumnHeaderSelect)
                    {
                        SetSortModeOnColumns();
                        base.SelectionMode = DataGridViewSelectionMode.ColumnHeaderSelect;
                    }
                }

                //BCU - handle fixed rows and current row when changing number of rows
                if (this.m_fixedColumns > this.m_cols)
                {
                    this.FixedCols = this.m_cols;
                }
                else if (this.m_fixedColumns == this.m_cols)
                {
                    this.m_currentColumn = 0;
                }

                disableEvents = false;
            }
        }

        /// <summary>
        /// Rows
        /// </summary>
        public new int Rows
        {
            get
            {
                return m_rows;
            }
            set
            {
                disableEvents = true;
                //JEI:HARRIS:IT267: first call EndEdit, then set new value for Rows
                //FC:FINAL:SBE - when Rows property is changed cell editing is stopped
                if (this.IsCurrentCellInEditMode)
                {
                    this.EndEdit();
                }

                m_rows = value;
               
                if (this.Rows < 1)
                {
                    base.Rows.Clear();
                }
                else
                {
                    //FC:FINAL:DSE Use the right FixedRows number when computing the number of rows
                    //base.RowCount = this.FixedRows > 0 ? Rows - 1 : Rows;
                    base.RowCount = (this.FixedRows > 0 && Rows >= this.FixedRows) ? Rows - this.FixedRows : Rows;
                }
                disableEvents = false;

                //BCU - handle fixed rows and current row when changing number of rows
                if (this.m_fixedRows > this.m_rows)
                {
                    this.FixedRows = this.m_rows;
                }
                else if (this.m_fixedRows == this.m_rows)
                {
                    m_currentRow = 0;
                }
            }
        }


        /// <summary>
        /// Returns or sets the left-most visible non fixed column in the MSFlexGrid. This property is not available at design time.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftCol
        {
            get
            {
                //FirstDisplayedScrollingColumnIndex doesn't return correctly when there are Frozen columns
                //In this case return the first visible column that is not frozen
                int index = base.FirstDisplayedColumnIndex;
                if (index < 0)
                {
                    return -1;
                }
                if (base.Columns[index].Frozen)
                {
                    for (int i = index + 1; i < base.Columns.Count; i++)
                    {
                        if (base.Columns[i].Visible && !base.Columns[i].Frozen)
                        {
                            index = i;
                            break;
                        }
                    }
                }
                return this.GetFlexColIndex(index);
            }
            set
            {
                int index = this.GetDGVColIndex(value);
                int columnIndex = index < 0 ? 0 : index;
                if (!base.Columns[columnIndex].Frozen)
                {
                    base.ScrollColumnIntoView(base.Columns[columnIndex]);
                }
            }
        }

        /// <summary>
        /// Returns or sets the background colors of individual cells or cell ranges.
        /// Changing this property affects the current cell or the current selection, depending on the setting of the FillStyle property.
        /// Setting either of these properties to zero causes MSHFlexGrid to paint the cell using the standard background and foreground colors. 
        /// If you want to set either of these properties to black, set them to one instead of zero.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Color CellBackColor
        {
            get
            {
                DataGridViewCell cell = GetCurrentCell();
                return cell.Style.BackColor;
            }
            set
            {
                if (value == null)
                {
                    return;
                }

                DataGridViewCell cell = GetCurrentCell();
                cell.Style.BackColor = value;

                //CHE: set SelectionBackColor and SelectionForeColor
                SetSelectionColors(cell.Style);
            }
        }

        /// <summary>
        /// Returns or sets the foreground colors of individual cells or cell ranges.
        /// Changing this property affects the current cell or the current selection, depending on the setting of the FillStyle property.
        /// Setting either of these properties to zero causes MSHFlexGrid to paint the cell using the standard background and foreground colors. 
        /// If you want to set either of these properties to black, set them to one instead of zero.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Color CellForeColor
        {
            get
            {
                DataGridViewCell cell = GetCurrentCell();
                return cell.Style.ForeColor;
            }
            set
            {
                if (value == null)
                {
                    return;
                }

                DataGridViewCell cell = GetCurrentCell();
                cell.Style.ForeColor = value;

                //CHE: set SelectionBackColor and SelectionForeColor
                SetSelectionColors(cell.Style);
            }
        }

        /// <summary>
        /// Returns or sets the bold style for the current cell text. This property is not available at design time.
        /// Syntax
        /// object.CellFontBold [=Boolean]
        /// The CellFontBold property syntax has these parts:
        /// Part Description 
        /// object An object expression that evaluates to an object in the Applies To list. 
        /// Boolean A Boolean expression that specifies whether the current cell's text is bold. 
        /// Settings
        /// The settings for Boolean are:
        /// Setting Description 
        /// True The current cell text is bold. 
        /// False Default. The current cell text is normal (not bold). 
        /// Remarks
        /// Changing this property affects the current cell or the current selection, depending on the setting of the FillStyle property.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool CellFontBold
        {
            get
            {
                DataGridViewCell cell = GetCurrentCell();
                return cell.Style.Font.Bold;
            }
            set
            {
                DataGridViewCell cell = GetCurrentCell();
                cell.Style.Font = ControlExtension.SetFontProperty(cell.Style.Font, value, FontStyle.Bold);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool HasRows
        {
            get
            {
                return base.RowCount > 0;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool HasColumns
        {
            get
            {
                return base.ColumnCount > 0;
            }
        }

        [DefaultValue(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool AutoSizeOnRowChanging
        {
            get
            {
                return this.autoSizeOnRowChanging;
            }
            set
            {
                this.autoSizeOnRowChanging = value;
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        internal int FirstVisibleCol
        {
            get
            {
                foreach (DataGridViewColumn col in base.Columns)
                {
                    if (col.Visible)
                    {
                        return col.Index;
                    }
                }
                return -1;
            }
        }

        #endregion

        #region Public Methods

        public bool IsRowDisplayed(int row)
        {
            int index = GetDGVRowIndex(row);
            if (base.IsValidRow(index))
            {
                return base.Rows[index].Displayed;
            }

            return false;
        }

        /// <summary>
        /// Moves a given row to a new position.
        /// </summary>
        /// <param name="row"></param>
        public void RowPosition(int row, int position)
        {
            int index = GetDGVRowIndex(row);
            int newIndex = GetDGVRowIndex(position);
            if (base.IsValidRow(index))
            {
                DataGridViewRow dgvRow = (DataGridViewRow)base.Rows[index].Clone();
                //save cell values
                object[] rowValues = new object[base.Columns.Count];
                object[] rowUserDataValues = new object[base.Columns.Count];
                var rowHeader = base.Rows[index].HeaderCell.Value;
                for (int colIndex = 0; colIndex < base.Columns.Count; colIndex++)
                {
                    rowValues[colIndex] = base[colIndex, index].Value;
                    rowUserDataValues[colIndex] = base[colIndex, index].UserData.Data;
                }

                base.Rows.RemoveAt(index);
                base.Rows.Insert(newIndex, dgvRow);

                //restore cell values
                base.Rows[newIndex].HeaderCell.Value = rowHeader;
                for (int colIndex = 0; colIndex < base.Columns.Count; colIndex++)
                {
                    base[colIndex, newIndex].Value = rowValues[colIndex];
                    base[colIndex, newIndex].UserData.Data = rowUserDataValues[colIndex];
                }
            }
        }

        /// <summary>
        /// Returns the numeric value of a cell identified by its row and column coordinates.
        /// </summary>
        /// <returns></returns>
        public double ValueMatrix(int Row, int Col)
        {
            //FC:FINAL:SGA Harris #i1487 implemented method from original
            double val;

            if (double.TryParse(TextMatrix(Row, Col), out val))
            {
                return val;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Begins, ends, or cancels a drag operation of any object, except Line, Menu, Shape, and Timer.
        /// </summary>
        public void Drag( int Action= 0)
        {
            //TODO:
        }
        /// <summary>
        /// Inserts rows with summary data.
        /// </summary>

        public void Subtotal(SubtotalSettings Function, int GroupOn=0, int TotalOn=0, string Format = "", Color BackColor = default(Color), Color ForeColor = default(Color), bool FontBold= false, string Caption = "", int MatchForm = 0, bool TotalOnly= false )
        {
            //TODO:
        }
        /// <summary>
        /// Activates edit mode for current cell
        /// Note that EditCell will force the control into editing mode even if the Editable property is set to False. You may even use it to allow editing of fixed cells.
        /// </summary>
        public void EditCell()
        {
            if (CurrentCell != null)
            {
                //FC:FINAL:SGA Harris #i2513 - don't enter edit mode for fixed column
                if (this.Col < this.FixedCols)
                {
                    return;
                }
                if (base.ReadOnly)
                {
                    base.ReadOnly = false;
                    editableByEditCell = true;
                }
                base.BeginEdit(true);
            }
        }

        /// <summary>
        /// Returns the index of a row that contains a specified string or RowData value.
        /// </summary>
        /// <param name="item">This parameter contains the data being searched.</param>
        /// <param name="row">This parameter contains the rows where the search should start. The default value is FixedRows.</param>
        /// <param name="col">This parameter tells the control which column should be searched. By default, this value is set to -1, which means the control will look for matches against RowData. If Col is set to a value greater than -1, then the control will look for matches against the cell's contents for the given column.</param>
        /// <param name="caseSensitive">This parameter is True by default, which means the search is case-sensitive. Set it to False if you want a case-insensitive search. (for example, when looking for "CELL" you may find "cell" ). </param>
        /// <param name="fullMatch">This parameter is True by default, which means the search is for a full match. Set it to False if you want to allow partial matches (for example, when looking for "MY" you may find "MYCELL"). This parameter is only relevant when you are looking for a string. </param>
        /// <returns></returns>
        public int FindRow(dynamic item, int row = -1, int col = -1, bool caseSensitive = true, bool fullMatch = true)
        {
            if (row < this.FixedRows)
            {
                row = this.FixedRows;
            }

            if (row >= 0)
            {
                if (col != -1)
                {
                    dynamic cellValue;
                    int dgvCol = GetDGVColIndex(col);
                    if (base.IsValidColumn(dgvCol) || dgvCol == -1)
                    {
                        for (int i = row; i < this.Rows; i++)
                        {
                            if (dgvCol != -1)
                            {
                                cellValue = base[dgvCol, GetDGVRowIndex(i)].Value;
                            }
                            else
                            {
                                cellValue = base.Rows[GetDGVRowIndex(i)].HeaderCell.Value;
                            }
                            if (caseSensitive)
                            {
                                if (fullMatch)
                                {
									//FC:FINAL:AAKV -exception handled
									//if(cellValue == item)
									//FC:FINAL:DDU - check if not null and make them both string
									//if (cellValue == item.ToString())
									if (cellValue != null && cellValue.ToString() == item.ToString())
									{
                                        return i;
                                    }
                                }
                                else
                                {
                                    if (cellValue != null && cellValue.ToString().Contains(item))
                                    {
                                        return i;
                                    }
                                }
                            }
                            else
                            {
                                if (fullMatch)
                                {
                                    if (cellValue.ToString().ToUpper() == (item as object).ToString().ToUpper())
                                    {
                                        return i;
                                    }
                                }
                                else
                                {
                                    if (cellValue.ToString().ToUpper().Contains((item as object).ToString().ToUpper()))
                                    {
                                        return i;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    //FC:FINAL:BCU - check if row is available
                    int dgvRowIndex = GetDGVRowIndex(row);

                    if (dgvRowIndex < 0)
                    {
                        return -1;
                    }

                    var rowData = this.RowData(row);

                    //FC:FINAL:MSH - i.issue #1089: added data won't be saved if first RowData will be equal to null
                    //FC:FINAL:BCU - check if rowData is available
                    //if (rowData == null)
                    //{
                    //    return -1;
                    //}

                    for (int i = row; i < this.Rows; i++)
                    {
                        //FC:FINAL:MSH - i.issue #1077: rowData doesn't change value in cycle
                        rowData = this.RowData(i);
                        if(rowData == null)
                        {
                            continue;
                        }

                        if (caseSensitive)
                        {
                            if (fullMatch)
                            {
                                //FC:FINAL:MSH - i.issue #1077 can't compare object with dynamic bool value.
                                //if (rowData == item)
                                if (rowData.ToString().CompareTo((item as object).ToString()) == 0)
                                {
                                    return i;
                                }
                            }
                            else
                            {
                                //FC:FINAL:MSH - i.issue #1077 can't compare object with dynamic bool value.
                                //if (rowData.ToString().Contains(item))
                                if (rowData.ToString().Contains((item as object).ToString()))
                                {
                                    return i;
                                }
                            }
                        }
                        else
                        {
                            if (fullMatch)
                            {
                                //FC:FINAL:MSH - i.issue #1077 can't compare object with dynamic bool value.
                                //if (rowData.ToString().ToUpper() == item.ToUpper())
                                if (rowData.ToString().ToUpper() == (item as object).ToString().ToUpper())
                                {
                                    return i;
                                }
                            }
                            else
                            {
                                //FC:FINAL:MSH - i.issue #1077 can't compare object with dynamic bool value.
                                //if (rowData.ToString().ToUpper().Contains(item.ToUpper()))
                                if (rowData.ToString().ToUpper().Contains((item as object).ToString().ToUpper()))
                                {
                                    return i;
                                }
                            }
                        }
                    }
                }
            }
            return -1;
        }



        public int RowOutlineLevel(int row)
        {
            int dgvRowIndex = GetDGVRowIndex(row);
            if (IsValidRow(dgvRowIndex))
            {
                return Convert.ToInt32(base.Rows[dgvRowIndex].UserData.Level);
            }
            return 0;
        }

        public void RowOutlineLevel(int row, int level)
        {
            int dgvRowIndex = GetDGVRowIndex(row);
            if (IsValidRow(dgvRowIndex))
            {
                if(level > maxRowLevel)
                {
                    maxRowLevel = level;
                }
                else if(level < minRowLevel)
                {
                    minRowLevel = level;
                }
                base.Rows[dgvRowIndex].UserData.Level = level;
                if (level > 0)
                {
                    for (int i = dgvRowIndex - 1; i >= 0; i--)
                    {
                        if (base.Rows[i].UserData.Level <= level - 1)
                        {
                            base.Rows[dgvRowIndex].ParentRow = base.Rows[i];
                            CalculateMaxExpandLevel(base.Rows[dgvRowIndex]);
                            break;
                        }
                    }
                }
            }
        }
        
        public void AddExpandButton(int dgvColIndex = 0, bool collapsed = true)
        {
            PictureBox expandButton = new PictureBox();
            //expandButton.AppearanceKey = "flatButton";
            //expandButton.AutoSize = false;
            //expandButton.ForeColor = Color.FromArgb(244, 247, 249);// Color.White;
            expandButton.Size = new System.Drawing.Size(18, 18);
            expandButton.Location = new Point(1, 5);
            expandButton.Click += ExpandButton_Click;
            //expandButton.BackgroundImageSource = collapsed ? "table-expand" : "table-collapse";
            expandButton.ImageSource = collapsed ? "table-expand" : "table-collapse";
            //expandButton.BackgroundImageLayout = ImageLayout.OriginalSize;
            expandButton.UserData.Collapsed = collapsed;
            expandButton.UserData.IsExpandButton = true;
            //FC:FINAL:BBE:#i612 - also expand/collapse all other rows based on the boolean collapsed
            this.Outline(collapsed ? 1 : 2);
            this.Columns[dgvColIndex].HeaderCell.Control = expandButton;
            this.Columns[dgvColIndex].HeaderCell.Style.Padding = new Padding(24, 0, 0, 0);
            AlignExpandButton(dgvColIndex);
        }

        private bool AlignExpandButton(int dgvColIndex = 0)
        {
            bool buttonAligned = false;
            DataGridViewColumn column = this.Columns[dgvColIndex];
            Control control = column.HeaderCell.Control;
            if (control != null && control is PictureBox expandButton)
            {
                if (expandButton.UserData.IsExpandButton != null && expandButton.UserData.IsExpandButton)
                {
                    if (ColumnHeadersHeight <= 30)
                    {
                        //add 7px at the bottom of expandButton if Size of the header is 30
                        expandButton.Top = this.ColumnHeadersHeight - (expandButton.Height + 7);
                    }
                    else
                    {
                        //add 9px at the bottom of expandButton if Size of the header is 60
                        expandButton.Top = this.ColumnHeadersHeight - (expandButton.Height + 9);
                    }
                    buttonAligned = true;
                }
            }
            return buttonAligned;
        }

        //P2218:BSE:#3728 clear all header controls 
        public void ClearHeaderControls(int dgvColIndex = 0)
        {
            Control control = this.Columns[dgvColIndex].HeaderCell.Control;
            if (control != null)
            {
                control.Dispose();
                control = null;
            }
        }
        private void ExpandButton_Click(object sender, EventArgs e)
        {
            //P2218:SBE: expand/collapse levels one by one
            /********************************************************
                For Grid with levels, we keep a single button for
                expand / collapse, and when you click the first time on the
                button, first level is expanded, on next click the next level
                is expanded, and so on.When all the levels are
                expanded, and you click on the same button, levels are
                collapsed one by one starting from the last level.
            *********************************************************/
            if (maxExpandLevel > 0)
            {
                inExpandCollapseAll = true;
                PictureBox expandButton = sender as PictureBox;
                if (expanding)
                {
                    foreach (DataGridViewRow row in base.Rows)
                    {
                        if (GetRowLevel(row) == expandCollapseIndex)
                        {
                            row.Expand();
                            ExpandParentRows(row);
                        }
                    }
                    expandCollapseIndex++;
                    if (expandCollapseIndex == maxExpandLevel)
                    {
                        expanding = false;
                        expandButton.ImageSource = "table-collapse";
                    }
                }
                else
                {
                    expandCollapseIndex--;
                    foreach (DataGridViewRow row in base.Rows)
                    {
                        if (GetRowLevel(row) == expandCollapseIndex)
                        {
                            row.Collapse();
                            CollapseChildRows(row);
                        }
                    }
                    if (expandCollapseIndex == 0)
                    {
                        expanding = true;
                        expandButton.ImageSource = "table-expand";
                    }
                }
                //Button expandButton = sender as Button;
                //if (expandButton.UserData.Collapsed)
                //{
                //    this.Outline(maxRowLevel);
                //    expandButton.BackgroundImageSource = "table-collapse";
                //}
                //else
                //{
                //    this.Outline(minRowLevel);
                //    expandButton.BackgroundImageSource = "table-expand";
                //}
                //expandButton.UserData.Collapsed = !expandButton.UserData.Collapsed;
                inExpandCollapseAll = false;
                if (Collapsed != null)
                {
                    Collapsed(this, EventArgs.Empty);
                }
            }
        }

        private void ExpandParentRows(DataGridViewRow row)
        {
            if (row.ParentRow != null && !row.ParentRow.IsExpanded)
            {
                row.ParentRow.Expand();
                ExpandParentRows(row.ParentRow);
            }
        }

        private void CollapseChildRows(DataGridViewRow row)
        {
            if (row.IsParent)
            {
                foreach (DataGridViewRow childRow in row.ChildRows)
                {
                    if (childRow.IsParent)
                    {
                        if (childRow.IsExpanded)
                        {
                            childRow.Collapse();
                        }
                        CollapseChildRows(childRow);
                    }
                }
            }
        }

        private void CalculateMaxExpandLevel(DataGridViewRow row)
        {
            int currentrowLevel = GetRowLevel(row);
            if (currentrowLevel > maxExpandLevel)
            {
                maxExpandLevel = currentrowLevel;
            }
        }

        private int GetRowLevel(DataGridViewRow row)
        {
            if (row.ParentRow == null)
            {
                return 0;
            }
            else
            {
                return GetRowLevel(row.ParentRow) + 1;
            }
        }

        public bool IsSubtotal(int row)
        {
            return Convert.ToBoolean(base.Rows[GetDGVRowIndex(row)].UserData.IsSubtotal);
        }

        public void IsSubtotal(int row, bool value)
        {
            int dgRow = GetDGVRowIndex(row);
            base.Rows[dgRow].UserData.IsSubtotal = value;
            foreach (DataGridViewColumn col in this.Columns)
            {
                if (col is FCDataGridViewCheckBoxColumn)
                {
                    // FC:FINAL:TB+SGA -> when a row has IsSubtotal true -> for each checkbox cell from this row the checkbox image shall not be visible (we move it to the right outside of visible part of cell)                    
                    base.Rows[dgRow].Cells[col].Style.Padding = new Padding(value ? col.Width * 5 : 0);
                }
            }
        }

        // JSP Fast get/set functions

        /// <summary>
        /// Get Cell Text with vbBase
        /// First Cell is 1/1
        /// </summary>
        /// <param name="intPosition"></param>
        public String GetCellText(int intCol, int intRow)
        {
            if (intCol > 0 & intRow > 0)
            {
                if (base[intCol - 1, intRow - 1].Value != null)
                {
                    return base[intCol - 1, intRow - 1].Value.ToString();
                }
                else
                {
                    return "";
                }
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Set Cell Text with vbBase
        /// First Cell is 1/1
        /// </summary>
        /// <param name="intPosition"></param>
        public void SetCellText(int intCol, int intRow, string CellValue)
        {
            if (intCol > 0 & intRow > 0)
            {
                base[intCol - 1, intRow - 1].Value = CellValue;
            }
        }

        /// <summary>
        /// Set Row Header Text with vbBase
        /// First Cell is 1/1
        /// </summary>
        /// <param name="intPosition"></param>
        public void SetRowHeaderText(int intRow, string CellValue)
        {
            if (intRow > 0)
            {
                base.Rows[intRow - 1].HeaderCell.Value = CellValue;
            }
        }

        /// <summary>
        /// Set Row Header Text with vbBase
        /// First Cell is 1/1
        /// </summary>
        /// <param name="intPosition"></param>
        public void SetColumnHeaderText(int intCol, string CellValue)
        {
            if (intCol > 0)
            {
                base.Columns[intCol - 1].HeaderCell.Value = CellValue;
            }
        }


        /// <summary>
        /// modify ZOrder
        /// </summary>
        /// <param name="intPosition"></param>
        public void ZOrder(Extensions.ZOrderConstants position)
        {
            ControlExtension.ZOrder(this, position);
        }

        /// <summary>
        /// Prints the grid on the printer.
        /// </summary>
        public void PrintGrid()
        {
            var controlId = "id_" + this.Handle.ToString();
            Eval("PrintDiv('" + controlId + "')");
        }

        /// <summary>
        /// Returns the text contents of an arbitrary cell.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public string TextMatrix(int row, int col)
        {
            object contents = null;

            int dgvColIndex = this.GetDGVColIndex(col);
            int dgvRowIndex = this.GetDGVRowIndex(row);

            if (dgvColIndex == -1 || dgvRowIndex < 0)
            {
                if (dgvRowIndex < 0 && dgvColIndex > -1)
                {
                    if (this.FixedRows <= 1)
                    {
                        contents = base.Columns[dgvColIndex].HeaderText;
                    }
                    else
                    {
                        string[] lines = base.Columns[dgvColIndex].HeaderText.Split(new string[] { "<br \\><br \\>" }, StringSplitOptions.None);
                        if (lines.Length > row)
                        {
                            contents = lines[row];
                            if (contents != null && contents.Equals("&nbsp;")) contents = string.Empty;
                        }
                        else
                        {
                            contents = string.Empty;
                        }
                    }
                }
                else if (dgvColIndex == -1 && dgvRowIndex > -1)
                {
                    contents = base.Rows[dgvRowIndex].HeaderCell.Value;
                }
                else if (dgvColIndex == -1 && dgvRowIndex < 0)
                {
                    contents = base.TopLeftHeaderCell.Value;
                }
            }
            else
            {   
                FCDataGridViewCheckBoxColumn checkBoxColumn = base.Columns[dgvColIndex] as FCDataGridViewCheckBoxColumn;
                if (checkBoxColumn != null)
                {
                    FCDataGridViewCheckBoxCell checkBoxCell = base[dgvColIndex, dgvRowIndex] as FCDataGridViewCheckBoxCell;
                    contents = checkBoxCell.ValueData;
                    //AM:HARRIS:#2570 - when the checkbox value is set by code the return value is the one set by TextMatrix
                    //When the user checks the checkbox the return value is "-1"
                    //If no content was set at all (code or user) the return value is ""
                    if (contents == null)
                    {
                        bool value = false;
                        bool.TryParse(checkBoxCell.Value?.ToString(), out value);
                        if (checkBoxCell.Value == null || checkBoxCell.Value == "" || !Convert.ToBoolean(checkBoxCell.Value))
                        {
                            contents = "";
                        }
                        else
                        {
                            contents = "1";
                        }
                    }
                    //FC:FINAL:DDU:There is a case when ValueData is -1 and the Value is false
                    //In this case it won't enter in the if to verify the below statement and i moved it here
                    //else if (checkBoxCell.Value == null || checkBoxCell.Value == "" || !Convert.ToBoolean(checkBoxCell.Value))
                    //{
                    //    contents = "0";
                    //}

				}
                //JEI:HARRIS:IT265: if we are in CellValidating/ValidateEdit and we want to compare the new value with the old one
                //like here --> vsAccounts.EditText != vsAccounts.TextMatrix(vsAccounts.Row, vsAccounts.Col)
                // we cannot return the EditText, because EditText will be always the same
                //FC:SBE - get editor text, if current cell is in edit mode
                //else if (this.IsCurrentCellInEditMode && dgvColIndex == this.CurrentCell.ColumnIndex && dgvRowIndex == this.CurrentCell.RowIndex)
                //{
                //    return this.EditText;
                //}
                else
                {
                    //FC:BCU - for combobox columns, return value instead of formatting value, because if cell is in editing state, formatting value will not be updated
                    DataGridViewComboBoxColumn comboBoxColumn = base.Columns[dgvColIndex] as DataGridViewComboBoxColumn;
                    if (comboBoxColumn != null)
                    {
                        List<string> items = (List<string>)comboBoxColumn.DataSource;
                        bool contentsFound = false;
                        int comboValueIndex = this.GetComboValueIndex(comboBoxColumn);
                        if (base[dgvColIndex, dgvRowIndex].UserData.ComboValue != null)
                        {
                            contents = Convert.ToString(base[dgvColIndex, dgvRowIndex].UserData.ComboValue);
                            contentsFound = true;
                        }

                        if (!contentsFound)
                        {
                            //FC:FINAL:DSE:#1531 Implementation for multiple columns in ComboBox
                            //contents = GetComboCellValue(base[dgvColIndex, dgvRowIndex].Value, comboValueIndex);
                            string value = Convert.ToString(base[dgvColIndex, dgvRowIndex].Value);
                            if (items != null)
                            {
                                foreach (string item in items)
                                {
                                    if (value == FCListViewComboBox.ExtractText(item.Split('\t')[comboValueIndex]))
                                    {
                                        contents = FCListViewComboBox.ExtractValue(item);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        FCListViewComboBox comboBox = base.Columns[dgvColIndex].Editor as FCListViewComboBox;
                        if (comboBox != null)
                        {
                            contents = GetComboCellValue(base[dgvColIndex, dgvRowIndex]);
                        }
                        else if (base.Columns[dgvColIndex].UserData.ComboBox != null && base.Columns[dgvColIndex].UserData.ComboBox)
                        {
                            contents = GetComboCellValue(base[dgvColIndex, dgvRowIndex]);
                        }
                        else
                        {
                            contents = base[dgvColIndex, dgvRowIndex].Value;
                        }
                    }
                }
            }
            //JEI:TEST
            //if (contents == null)
            //{
            //    if (base.Columns[dgvColIndex].DefaultCellStyle.Format != null)
            //    {
            //        contents = 0.ToString(base.Columns[dgvColIndex].DefaultCellStyle.Format);
            //    }
            //}

            return Convert.ToString(contents);
        }

        /// <summary>
        /// Sets the text contents of an arbitrary cell.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="contents"></param>
        public void TextMatrix(int row, int col, object contents)
        {
            //FC:FINAL:DSE:#i1152 Prevent recursive event firing if .TextMatrix is called from CellValueChanged event handler
            if (cellValueChangedRunning)
            {
                fireCellValueChanged = false;
            }
            //P2218:AM:#4269 - use FCConvert instead of Convert to convert correctly enum values
            string stringValue = FCConvert.ToString(contents);

            int dgvColIndex = this.GetDGVColIndex(col);
            int dgvRowIndex = this.GetDGVRowIndex(row);

            if (dgvColIndex < 0 || dgvRowIndex < 0)
            {
                if (dgvColIndex > -1)
                {
                    //FC:FINAL:SBE  - Harris #i588 - compare values to avoid unnecessary trigger of CellValueChanged event (avoid StackOverflowException)
                    if (this.FixedRows <= 1)
                    {
                        if (base.Columns[dgvColIndex].HeaderText != stringValue.ToUpper())
                        {
                            base.Columns[dgvColIndex].HeaderText = stringValue.ToUpper();
                        }
                    }
                    else
                    {
                        string currentText = base.Columns[dgvColIndex].HeaderText;
                        string[] currentRows = currentText.Split(new string[] { "<br \\><br \\>" }, StringSplitOptions.None);
                        List<string> newLines = new List<string>();
                        for (int fixedRowIndex = 0; fixedRowIndex < this.FixedRows; fixedRowIndex++)
                        {
                            newLines.Add("&nbsp;");
                            if (currentRows.Length > fixedRowIndex)
                            {
                                newLines[fixedRowIndex] = string.IsNullOrEmpty(currentRows[fixedRowIndex]) ? "&nbsp;" : currentRows[fixedRowIndex];
                            }
                        }
                        newLines[row] = string.IsNullOrEmpty(stringValue.ToUpper()) ? "&nbsp;" : stringValue.ToUpper();
                        base.Columns[dgvColIndex].AllowHtml = true;
                        string headerText = string.Join("<br \\><br \\>", newLines);
                        //FC:FINAL:MSH - issue #923: incorrect comparing. headerText value always has either stringValue or "&nbsp;", so .HeaderText never won't be empty
                        //if (headerText != "<br \\><br \\>")
                        if (!string.IsNullOrEmpty(headerText.Replace("<br \\>", "").Replace("&nbsp;", "").Trim()))
                        {
                            base.Columns[dgvColIndex].HeaderText = headerText;
                        }
                        else
                        {
                            base.Columns[dgvColIndex].HeaderText = string.Empty;
                        }
                    }
                }
                else if (dgvRowIndex > -1)
                {
                    //FC:FINAL:SBE  - Harris #i588 - compare values to avoid unnecessary trigger of CellValueChanged event (avoid StackOverflowException)
                    if (base.Rows[dgvRowIndex].HeaderCell.Value != stringValue)
                    {
                        base.Rows[dgvRowIndex].HeaderCell.Value = stringValue;
                    }
                }
                else
                {
                    //FC:FINAL:SBE  - Harris #i588 - compare values to avoid unnecessary trigger of CellValueChanged event (avoid StackOverflowException)
                    if (base.TopLeftHeaderCell.Value != stringValue.ToUpper())
                    {
                        base.TopLeftHeaderCell.Value = stringValue.ToUpper();
                    }
                }
            }
            else
            {
                FCDataGridViewCheckBoxColumn checkBoxColumn = base.Columns[dgvColIndex] as FCDataGridViewCheckBoxColumn;
                if (checkBoxColumn != null)
                {
                    FCDataGridViewCheckBoxCell checkBoxCell = base[dgvColIndex, dgvRowIndex] as FCDataGridViewCheckBoxCell;
                    bool cellValue = false;

                    //FC:BCU - any numeric value that`s != 0 or "true" string will turn on checkbox checked state
                    if (!String.IsNullOrEmpty(stringValue))
                    {
                        if (stringValue.ToUpper() == "TRUE")
                        {
                            cellValue = true;
                        }
                        else
                        {
                            decimal decimalValue = 0;
                            if(decimal.TryParse(stringValue, out decimalValue) && decimalValue != 0)
                            {
                                cellValue = true;
                            }
                        }
                    }

                    checkBoxCell.Value = cellValue;
                    //FC:FINAL:ASZ - CheckValue/UncheckValue replaced with ValueData 
                    //if (cellValue)
                    //{
                    //    checkBoxCell.CheckValue = contents;
                    //}
                    //else
                    //{
                    //    checkBoxCell.UncheckValue = contents;
                    //}
                    checkBoxCell.ValueData = contents;
                }
                else
                {
                    DataGridViewComboBoxColumn comboboxColumn = base.Columns[dgvColIndex] as DataGridViewComboBoxColumn;
                    if (comboboxColumn != null)
                    {
                        List<string> items = (List<string>)comboboxColumn.DataSource;
                        string[] tab = new string[] { "\t" };
                        bool found = false;

                        // displayed column
                        int comboValueIndex = this.GetComboValueIndex(comboboxColumn);
                        
                        foreach (string item in items)
                        {
                            //FC:FINAL:DSE:#1531 Implementation of multiple columns in ComboBox
                            //if (item.Text.Split(tab, StringSplitOptions.None).First() == stringValue)
                            if (FCListViewComboBox.ExtractValue(item) == stringValue)
                            {
                                string text = FCListViewComboBox.ExtractText(item);
                                //FC:FINAL:SBE  - Harris #i588 - compare values to avoid unnecessary trigger of CellValueChanged event (avoid StackOverflowException)
                                text = text.Split(tab, StringSplitOptions.None)[comboValueIndex];
                                //FC:FINAL:MSH - in original app in cell will be set 'value' if 'text' is an empty string
                                if (string.IsNullOrEmpty(text))
                                {
                                    text = FCListViewComboBox.ExtractValue(item);
                                }
                                if (base[dgvColIndex, dgvRowIndex].Value != text)
                                {
                                    base[dgvColIndex, dgvRowIndex].Value = text;
                                }
                                found = true;
                                break;
                            }
                        }
                        //AM:Harris:#i729 - force the value even if it's not in the combolist
                        if(!found)
                        {
                            base[dgvColIndex, dgvRowIndex].Value = stringValue;
                        }
                        //DSE save value in the cell's tag in case multiple texts are the same but with different values
                        base[dgvColIndex, dgvRowIndex].UserData.ComboValue = stringValue;

                    }
                    else
                    {
                        //FC:FINAL:SBE  - Harris #i588 - compare values to avoid unnecessary trigger of CellValueChanged event (avoid StackOverflowException)
                        if (base[dgvColIndex, dgvRowIndex].Value != contents)
                        {
                            //AM: need to set the value as object for the format to apply properly
                            //FC:FINAL:MSH - issue #1239: in VB6 "\0" symbols aren't added to cell
                            if (contents is string && !String.IsNullOrEmpty(stringValue) && stringValue.Contains("\0"))
                            {
                                base[dgvColIndex, dgvRowIndex].Value = stringValue.Replace("\0", "");
                            }
                            else
                            {
                                base[dgvColIndex, dgvRowIndex].Value = contents;
                            }
                            //FC:FINAL:DSE Enable CellPaint event for cells
                            //base[dgvColIndex, dgvRowIndex].UserPaint = true;
                        }
                    }
                }           
            }
            //FC:FINAL:DSE:#i1152 Prevent recursive event firing if .TextMatrix is called from CellValueChanged event handler
            if (cellValueChangedRunning)
            {
                fireCellValueChanged = true;
            }
        }

        /// <summary>
        /// Returns the height of the specified row, in logical twips. This property is not available at design time.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public int RowHeight(int row)
        {
            int dgvRowIndex = this.GetDGVRowIndex(row);

            if (dgvRowIndex < 0)
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    //6 px, represent cell padding
                    return Convert.ToInt32(fcGraphics.ScaleY(base.ColumnHeadersHeight, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            else
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Rows[dgvRowIndex].Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
        }

        /// <summary>
        /// Sets the height of the specified row, in logical twips. This property is not available at design time.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public void RowHeight(int row, int height)
        {
            int dgvRowIndex = this.GetDGVRowIndex(row);

            //Set the height for all rows
            if (row < 0)
            {
                int newHeight = 0;
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    newHeight = Convert.ToInt32(fcGraphics.ScaleY(height, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
                for (int i = 0; i < base.Rows.Count; i++)
                {
                    base.Rows[i].Height = newHeight;
                }
            }
            //Set the height of the column header
            else if (dgvRowIndex < 0)
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    //6 px, represent cell padding
                    base.ColumnHeadersHeight = Convert.ToInt32(fcGraphics.ScaleY(height, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels)) + 6;
                }
            }
            else
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Rows[dgvRowIndex].Height = Convert.ToInt32(fcGraphics.ScaleY(height, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        //RPU:
        /// <summary>
        /// Returns a user-defined variant associated with the given row.
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public object RowData(int row)
        {
            int index = GetDGVRowIndex(row);
            if (IsValidRow(index))
            {
                return base.Rows[index].UserData.RowData;
            }
            else if (row == 0 && FixedRows > 0)
            {
                return base.ColumnHeadersDefaultCellStyle.Tag;
            }
            else
            {
                return null;
            }
        }

        //RPU:
        /// <summary>
        /// Sets a user-defined variant associated with the given row.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="value"></param>
        public void RowData(int row, dynamic value)
        {
            int index = GetDGVRowIndex(row);
            if (IsValidRow(index))
            {
                base.Rows[index].UserData.RowData = value;
            }
            else if(row == 0 && FixedRows > 0)
            {
                base.ColumnHeadersDefaultCellStyle.Tag = value;
            }
        }

        /// <summary>
        /// Returns or sets a value that determines which rows and columns can have their contents merged. These properties must be True to use the MergeCells property.
        /// True	When adjacent cells display identical content, the rows merge to the left or the columns merge to the top.
        /// False	When adjacent cells display identical content, no cells merge. This is the default for MergeCol and MergeRow.
        /// Remarks
        /// If the MergeCells property is set to a nonzero value, adjacent cells with identical values are merged only if they are in a row with the MergeRow property 
        /// set to True or in a column with the MergeCol property set to True.
        /// For details on the merging functionality of the MSHFlexGrid, see the MergeCells property.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="merge"></param>
        public void MergeRow(int row, bool merge)
        {
            int dgvRow = GetDGVRowIndex(row);
            if (IsValidRow(dgvRow))
            {
                base[0, dgvRow].Style.ColSpan = base.Columns.Count;
            }
            else if(row == 0 && this.FixedRows > 0)
            {
                base.Columns[0].HeaderStyle.ColSpan = base.Columns.Count;
            }
        }

        /// <summary>
        /// Merges the cells in the row specified by the row property from startCol to endCol
        /// </summary>
        /// <param name="row"></param>
        /// <param name="merge"></param>
        /// <param name="startCol"></param>
        /// <param name="endCol"></param>
        public void MergeRow(int row, bool merge, int startCol, int endCol)
        {
            int dgvRow = GetDGVRowIndex(row);
            int dgvCol = GetDGVColIndex(startCol);
            if (IsValidCell(dgvCol, dgvRow))
            {
                base[dgvCol, dgvRow].Style.ColSpan = endCol - startCol + 1;
            }
        }

        /// <summary>
        /// Returns or sets a value that determines which rows and columns can have their contents merged. These properties must be True to use the MergeCells property.
        /// True	When adjacent cells display identical content, the rows merge to the left or the columns merge to the top.
        /// False	When adjacent cells display identical content, no cells merge. This is the default for MergeCol and MergeRow.
        /// Remarks
        /// If the MergeCells property is set to a nonzero value, adjacent cells with identical values are merged only if they are in a row with the MergeRow property 
        /// set to True or in a column with the MergeCol property set to True.
        /// For details on the merging functionality of the MSHFlexGrid, see the MergeCells property.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public bool MergeCol(int column)
        {
            // TODO:CHE
            return false;
        }

        /// <summary>
        /// Returns or sets a value that determines which rows and columns can have their contents merged. These properties must be True to use the MergeCells property.
        /// True	When adjacent cells display identical content, the rows merge to the left or the columns merge to the top.
        /// False	When adjacent cells display identical content, no cells merge. This is the default for MergeCol and MergeRow.
        /// Remarks
        /// If the MergeCells property is set to a nonzero value, adjacent cells with identical values are merged only if they are in a row with the MergeRow property 
        /// set to True or in a column with the MergeCol property set to True.
        /// For details on the merging functionality of the MSHFlexGrid, see the MergeCells property.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="merge"></param>
        public void MergeCol(int column, bool merge)
        {
            // TODO:CHE
        }

        /// <summary>
        /// Returns or sets the alignment of data in a column. This can be a standard column, a column within a band, or a column within a header. 
        /// This property is not available at design time (except indirectly through the FormatString property).
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public AlignmentSettings ColAlignment(int column)
        {
            int dgvColIndex = this.GetDGVColIndex(column);

            if (dgvColIndex < 0)
            {
                return this.FixDataGridViewContentAlignment(base.RowHeadersDefaultCellStyle.Alignment);
            }
            //return this.FixDataGridViewContentAlignment(base.Columns[dgvColIndex].CellTemplate.Style.Alignment);
            return this.FixDataGridViewContentAlignment(base.Columns[dgvColIndex].DefaultCellStyle.Alignment);

        }

        /// <summary>
        /// Returns or sets the alignment of data in a column. This can be a standard column, a column within a band, or a column within a header. 
        /// This property is not available at design time (except indirectly through the FormatString property).
        /// object.ColAlignment(Index) [=value]
        /// value	An integer or constant that specifies the alignment of data in a column, as described:
        /// flexAlignLeftTop	0	The column content is aligned left, top.
        /// flexAlignLeftCenter	1	Default for strings. The column content is aligned left, center.
        /// flexAlignLeftBottom	2	The column content is aligned left, bottom.
        /// flexAlignCenterTop	3	The column content is aligned center, top.
        /// flexAlignCenterCenter	4	The column content is aligned center, center.
        /// flexAlignCenterBottom	5	The column content is aligned center, bottom.
        /// flexAlignRightTop	6	The column content is aligned right, top.
        /// flexAlignRightCenter	7	Default for numbers. The column content is aligned right, center.
        /// flexAlignRightBottom	8	The column content is aligned right, bottom.
        /// flexAlignGeneral	9	The column content is of general alignment. This is "left, center" for strings and "right, center" for numbers.
        /// Remarks
        /// Any column can have an alignment that is different from other columns. The ColAlignment property affects all cells in the specified column, including those in fixed rows.
        /// To set individual cell alignments, use the CellAlignment property. To set column alignments at design time, use the FormatString property.
        /// If the MSHFlexGrid is in vertical mode, the setting ColAlignment(3) may affect columns in multiple bands.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="alignment"></param>
        public void ColAlignment(int column, AlignmentSettings alignment)
        {
            int dgvColIndex = this.GetDGVColIndex(column);

            if (dgvColIndex < 0)
            {
                base.RowHeadersDefaultCellStyle.Alignment = this.FixAlignmentSettings(alignment);
            }
            else
            {
                base.Columns[dgvColIndex].DefaultCellStyle.Alignment = this.FixAlignmentSettings(alignment);
                base.Columns[dgvColIndex].HeaderCell.Style.Alignment = this.FixAlignmentSettings(alignment);
            }
        }

        public void ColAllowedKeys(int column, string allowedKeys)
        {
            int dgvColIndex = this.GetDGVColIndex(column);
            if (IsValidColumn(dgvColIndex))
            {
                Columns[dgvColIndex].UserData.AllowedKeys = allowedKeys;
                if (Columns[dgvColIndex].Editor == null)
                {
                    TextBox newColEditor = new TextBox();
                    Columns[dgvColIndex].Editor = newColEditor;
                }
                TextBox columnEditor = Columns[dgvColIndex].Editor as TextBox;
                if (columnEditor != null)
                {
                    columnEditor.AllowKeysOnClientKeyPress(allowedKeys);
                }
            }
        }

        /// <summary>
        /// Returns or sets the width of the column in the specified band, in logical twips. This property is not available at design time.
        /// Note   When using the MSFlexGrid, this property returns or sets the width of the specified column, in twips.
        /// Remarks
        /// You can use this property to set the width of any column at run time. For instructions on setting column widths at design time, see the FormatString property.
        /// You can set ColWidth to zero to create invisible columns, or to 1 to reset the column width to its default value, which depends on the size of the current font.
        /// When number is not specified, it defaults to 0. Hence, when the MSHFlexGrid is not bound to a hierarchical Recordset, using 0 and not specifying number both have 
        /// the same result. Note that number is an optional parameter for backward compatibility with the MSFlexGrid.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public int ColWidth(int column)
        {
            int dgvColIndex = this.GetDGVColIndex(column);

            if (dgvColIndex < 0)
            {
                //using (Graphics g = this.CreateGraphics())
                if (UseScaleFactor)
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(Convert.ToInt32(base.RowHeadersWidth / scaleFactor), ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
                else
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.RowHeadersWidth, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            else
            {
                //using (Graphics g = this.CreateGraphics())
                if (UseScaleFactor)
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(Convert.ToInt32(base.Columns[dgvColIndex].Width / scaleFactor), ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
                else
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Columns[dgvColIndex].Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }

        }

        /// <summary>
        /// Returns or sets the width of the column in the specified band, in logical twips. This property is not available at design time.
        /// Note   When using the MSFlexGrid, this property returns or sets the width of the specified column, in twips.
        /// Remarks
        /// You can use this property to set the width of any column at run time. For instructions on setting column widths at design time, see the FormatString property.
        /// You can set ColWidth to zero to create invisible columns, or to 1 to reset the column width to its default value, which depends on the size of the current font.
        /// When number is not specified, it defaults to 0. Hence, when the MSHFlexGrid is not bound to a hierarchical Recordset, using 0 and not specifying number both have 
        /// the same result. Note that number is an optional parameter for backward compatibility with the MSFlexGrid.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="width"></param>
        public void ColWidth(int column, int width)
        {
            int dgvColIndex = this.GetDGVColIndex(column);

            //FC:FINAL:BBE:#595-#596 - correct column size with factor to show it correct on the report
            if (UseScaleFactor)
            {
                width = Convert.ToInt32(width * scaleFactor);
            }

            if (dgvColIndex < 0)
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    int value = Convert.ToInt32(fcGraphics.ScaleX(width, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                    //Can't have a width < 2
                    //JEI:HARRIS:IT111 Can't have a width > 2000
                    value = value > 2000 ? 2000 : value;
                    base.RowHeadersWidth = Math.Max(2, value);
                }
            }
            else
            {
                //using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                    base.Columns[dgvColIndex].Width = Convert.ToInt32(fcGraphics.ScaleX(width, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }

            if (width == 0)
            {
                widthToZeroChanging = true;
                if (dgvColIndex >= 0)
                {
                    if (columnsVisibility.ContainsKey(dgvColIndex))
                    {
                        columnsVisibility[dgvColIndex] = base.Columns[dgvColIndex].Visible;
                    }
                    else
                    {
                        columnsVisibility.Add(dgvColIndex, base.Columns[dgvColIndex].Visible);
                    }
                }

                // have to distinguish between RowHeaderColumn and Columns
                if (dgvColIndex < 0)
                {
                    if (columnsVisibility.ContainsKey(dgvColIndex))
                    {
                        columnsVisibility[dgvColIndex] = base.RowHeadersVisible;
                    }
                    else
                    {
                        columnsVisibility.Add(dgvColIndex, base.RowHeadersVisible);
                    }
                    base.RowHeadersVisible = false;
                    
                }
                else
                {
                    base.Columns[dgvColIndex].Visible = false;
                }
                widthToZeroChanging = false;
            }
            else
            {
                widthToZeroChanging = true;
                if (columnsVisibility.ContainsKey(dgvColIndex))
                {
                    // have to distinguish between RowHeaderColumn and Columns
                    if (dgvColIndex < 0)
                    {
                        base.RowHeadersVisible = true;
                    }
                    else
                    {
                        base.Columns[dgvColIndex].Visible = columnsVisibility[dgvColIndex];
                    }
                    columnsVisibility.Remove(dgvColIndex);
                }
                widthToZeroChanging = false;
            }
            //FC:FINAL:MSH - issue #928: update property for recalculating width of the last visible column(if this need)
            this.ExtendLastCol = extendLastCol;
        }
        //RPU:
        /// <summary>
        /// Sets the format used to display numeric values.
        /// </summary>
        /// <param name="col">The Col parameter should be set to a value between zero and Cols - 1 to set the format for a given column, or to -1 to set the format for all columns.</param>
        /// <param name="format">This property allows you to define a format to be used for displaying numerical, Boolean, or date/time values. </param>
        public void ColFormat(int col, string format)
        {
            if (col != -1)
            {
                col = GetDGVColIndex(col);
                if (base.IsValidColumn(col))
                {
                    base.Columns[col].DefaultCellStyle.Format = format;
                }
                else
                {
                    base.RowHeadersDefaultCellStyle.Format = format;
                }
            }
            else
            {
                for (int i = 0; i < base.Columns.Count; i++)
                {
                    base.Columns[i].DefaultCellStyle.Format = format;
                }
            }

        }

        /// <summary>
        /// Returns the format used to display numeric values.
        /// </summary>
        /// <param name="col">The Col parameter should be set to a value between zero and Cols - 1 to set the format for a given column, or to -1 to set the format for all columns.</param>
        /// <param name="format">This property allows you to define a format to be used for displaying numerical, Boolean, or date/time values. </param>
        public string ColFormat(int col)
        {
            if (col != -1)
            {
                col = GetDGVColIndex(col);
                if (base.IsValidColumn(col))
                {
                    return base.Columns[col].DefaultCellStyle.Format;
                }
                else
                {
                    return base.RowHeadersDefaultCellStyle.Format;
                }
            }
            return "";
        }

        //RPU:
        /// <summary>
        /// Sets the data type for the column.
        /// </summary>
        /// <param name="col"> The Col parameter should be set to a value between zero and Cols - 1 to set the data type of a given column, or to -1 to set the data type of all columns.</param>
        /// <param name="dataType">The DataTypeSettings for the column</param>
        public void ColDataType(int col, DataTypeSettings datatype)
        {
            switch (datatype)
            {
                case DataTypeSettings.flexDTBoolean:
                    if (col != -1)
                    {   
                        int index = GetDGVColIndex(col);
                        if (this.IsValidColumn(index))
                        {
                            //FC:BCU - do not create checkbox column if it`s already created
                            if (base.Columns[index] is FCDataGridViewCheckBoxColumn)
                            {
                                return;
                            }
                            //FC:FINAL:BCU - clear rows when removing column otherwise error is raised
                            int rows = this.Rows;
                            //FC:FINAL:SBE - Harris #i610 - save current cell values
                            object[,] cellValues = new object[this.ColumnCount, this.RowCount];
                            object[] rowHeaderValues = new object[this.RowCount];
                            for (int rowIndex = 0; rowIndex < this.RowCount; rowIndex++)
                            {
                                rowHeaderValues[rowIndex] = base.Rows[rowIndex].HeaderCell.Value;
                                for (int colIndex = 0; colIndex < this.ColumnCount; colIndex++)
                                {
                                    cellValues[colIndex, rowIndex] = base[colIndex, rowIndex].Value;
                                }
                            }
                            this.Rows = this.FixedRows;
                            string headerText = base.Columns[index].HeaderText;
                            int width = base.Columns[index].Width;
                            bool visible = base.Columns[index].Visible;
                            base.Columns.RemoveAt(index);
                            FCDataGridViewCheckBoxColumn column = new FCDataGridViewCheckBoxColumn();
                            column.HeaderText = headerText;
                            column.Width = width;
                            column.Visible = visible;
                            base.Columns.Insert(index, column);

                            //FC:FINAL:BCU - restore rows
                            this.Rows = rows;
                            //FC:FINAL:SBE - Harris #i610 - restore cell values
                            for (int rowIndex = 0; rowIndex < this.RowCount; rowIndex++)
                            {
                                base.Rows[rowIndex].HeaderCell.Value = rowHeaderValues[rowIndex];
                                for (int colIndex = 0; colIndex < this.ColumnCount; colIndex++)
                                {
                                    base[colIndex, rowIndex].Value = cellValues[colIndex, rowIndex];
                                }
                            }
                            this.ColAlignment(col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                        }
                    }
                    else
                    {
                        int count = base.Columns.Count;
                        base.Columns.Clear();
                        for (int i = 0; i < count; i++)
                        {
                            base.Columns.Add(new FCDataGridViewCheckBoxColumn());
                        }
                    }
                    break;

                case DataTypeSettings.flexDTCurrency:
                    this.ColAlignment(col, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    //FC:FINAL:MSH - Add the type of column values
                    base.Columns[GetDGVColIndex(col)].ValueType = typeof(decimal);
                    break;
                case DataTypeSettings.flexDTDouble:
                    this.ColAlignment(col, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    //FC:FINAL:MSH - Add the type of column values
                    base.Columns[GetDGVColIndex(col)].ValueType = typeof(double);
                    break;
                case DataTypeSettings.flexDTLong:
                    //AM:HARRIS: only amounts should be right aligned
                    //this.ColAlignment(col, FCGrid.AlignmentSettings.flexAlignRightCenter);
					//FC:FINAL:MSH - Add the type of column values
					//HARRIS:DDU:#1964 - in vb6 Long (long integer) - 2,147,483,648 to 2,147,483,647
					//				   - in .net, vb6 long is int32 - 2,147,483,648 to 2,147,483,647
					base.Columns[GetDGVColIndex(col)].ValueType = typeof(int);
                    break;
				case DataTypeSettings.flexDTDate:
					//HARRIS:AM:#3013 - align dates to left
					this.ColAlignment(col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					//FC:FINAL:MSH - Add the type of column values
					base.Columns[GetDGVColIndex(col)].ValueType = typeof(DateTime);
                    break;
                case DataTypeSettings.flexDTEmpty:
                    //FC:FINAL:MSH - Add the type of column values
                    base.Columns[GetDGVColIndex(col)].ValueType = null;
                    break;
                case DataTypeSettings.flexDTString:
                    //FC:FINAL:MSH - Add the type of column values
                    base.Columns[GetDGVColIndex(col)].ValueType = typeof(string);
                    break;
                default:
                    //FC:FINAL:MSH - Add the type of column values
                    base.Columns[GetDGVColIndex(col)].ValueType = null;
                    break;
            }

        }
        //RPU:
        /// <summary>
        /// Returns the data type for the column.
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        public DataTypeSettings ColDataType(int col)
        {
            col = GetDGVColIndex(col);
            if (IsValidColumn(col))
            {
                if (base.Columns[col] is FCDataGridViewCheckBoxColumn)
                {
                    return DataTypeSettings.flexDTBoolean;
                }

                //FC:FINAL:SGA Harris #i2479 - implement ColDataType
                if (base.Columns[col].ValueType == typeof(decimal))
                {
                    return DataTypeSettings.flexDTCurrency;
                }
                else if (base.Columns[col].ValueType == typeof(double))
                {
                    return DataTypeSettings.flexDTDouble;
                }
                else if (base.Columns[col].ValueType == typeof(int))
                {
                    return DataTypeSettings.flexDTLong;
                }
                else if (base.Columns[col].ValueType == typeof(DateTime))
                {
                    return DataTypeSettings.flexDTDate;
                }
                else if (base.Columns[col].ValueType ==null)
                {
                    return DataTypeSettings.flexDTEmpty;
                }
                else if (base.Columns[col].ValueType == typeof(string))
                {
                    return DataTypeSettings.flexDTString;
                }
                else if (base.Columns[col].ValueType == typeof(bool))
                {
                    return DataTypeSettings.flexDTBoolean;
                }
            }
            return DataTypeSettings.flexDTEmpty;
        }

        public void ColImageList(int col, ImageList imageList)
        {
            int index = GetDGVColIndex(col);
            if (base.Columns[index] is DataGridViewImageColumn)
            {
                return;
            }

            int rows = this.Rows;
            this.Rows = this.FixedRows;
            string headerText = base.Columns[index].HeaderText;
            int width = base.Columns[index].Width;
            base.Columns.RemoveAt(index);
            DataGridViewImageColumn column = new DataGridViewImageColumn();
            column.HeaderText = headerText;
            column.Width = width;
            base.Columns.Insert(index, column);

            this.Rows = rows;
        }

        //RPU:
        /// <summary>
        /// Sets if a column is hidden.
        /// </summary>
        /// <param name="col">The Col parameter should be set to a value between zero and Cols - 1 to hide or show a given column, or to -1 to hide or show all columns.</param>
        /// <param name="hidden">True if the column is hiiden and false if it is visible</param>
        public void ColHidden(int col, bool hidden)
        {
            if (col != -1)
            {
                int dgvColIndex = GetDGVColIndex(col);
                if (dgvColIndex < 0)
                {   
                    base.RowHeadersVisible = !hidden;
                }
                else if (IsColValid(dgvColIndex))
                {
                    base.Columns[dgvColIndex].Visible = !hidden;
                }
            }
            else
            {
                foreach (DataGridViewColumn column in base.Columns)
                {
                    column.Visible = !hidden;
                }
            }
			//FC:FINAL:MSH - update property for recalculating width of the last visible column(if this need)
			this.ExtendLastCol = extendLastCol;
		}

        //RPU:
        /// <summary>
        /// Returns true if a column is hidden
        /// </summary>
        /// <param name="col">The Col parameter should be set to a value between zero and Cols - 1 to return a value for a specified column , or to -1 to return true only if all columns are hidden.</param>
        /// <returns></returns>
        public bool ColHidden(int col)
        {
            if (col != -1)
            {
                int dgvIndex = GetDGVColIndex(col);
                if (dgvIndex < 0)
                {
                    return !base.RowHeadersVisible;
                }
                else
                {
                    return !base.Columns[dgvIndex].Visible;
                }
            }
            else
            {
                foreach (DataGridViewColumn column in base.Columns)
                {
                    if (column.Visible == true)
                    {
                        return false;
                    }
                }
                return true;
            }

        }


        public bool ColReadOnly(int col)
        {
            if (col != -1)
            {
                int dgvIndex = GetDGVColIndex(col);
                if (dgvIndex < 0)
                {
                    return true;
                }
                else
                {
                    return base.Columns[dgvIndex].ReadOnly;
                }
            }

            return true;
        }

        public void SetColReadOnly(int col, bool readOnly)
        {
            if (col != -1)
            {
                int dgvColIndex = GetDGVColIndex(col);
                if (dgvColIndex >= 0 && IsColValid(dgvColIndex))
                {
                    base.Columns[dgvColIndex].ReadOnly = readOnly;
                }
            }            
        }

        public void ColEditMask(int col, string mask)
        {
            int dgvIndex = this.GetDGVColIndex(col);
            if (dgvIndex >= 0)
            {
                base.Columns[dgvIndex].UserData.EditMask = mask;
            }
        }

        /// <summary>
        /// Returns or sets the alignment of data in the fixed cells of a column in an MSHFlexGrid.
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public AlignmentSettings FixedAlignment(int column)
        {
            int dgvColIndex = this.GetDGVColIndex(column);

            if (dgvColIndex < 0)
            {
                return this.FixDataGridViewContentAlignment(base.RowHeadersDefaultCellStyle.Alignment);
            }
            else
            {
                return this.FixDataGridViewContentAlignment(base.Columns[dgvColIndex].HeaderCell.Style.Alignment);
            }
        }

        /// <summary>
        /// Returns or sets the alignment of data in the fixed cells of a column in an MSHFlexGrid.
        /// object.FixedAlignment(Index) [=value]
        /// value	An integer that determines the alignment of the data in the fixed cells, as described:
        /// flexAlignLeftTop	0	The column content is aligned left, top.
        /// flexAlignLeftCenter	1	Default for strings. The column content is aligned left, center.
        /// flexAlignLeftBottom	2	The column content is aligned left, bottom.
        /// flexAlignCenterTop	3	The column content is aligned center, top.
        /// flexAlignCenterCenter	4	The column content is aligned center, center.
        /// flexAlignCenterBottom	5	The column content is aligned center, bottom.
        /// flexAlignRightTop	6	The column content is aligned right, top.
        /// flexAlignRightCenter	7	Default for numbers. The column content is aligned right, center.
        /// flexAlignRightBottom	8	The column content is aligned right, bottom.
        /// flexAlignGeneral	9	The column content is of general alignment. This is "left, center" for strings and "right, center" for numbers.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="alignment"></param>
        public void FixedAlignment(int column, AlignmentSettings alignment)
        {
            int dgvColIndex = this.GetDGVColIndex(column);

            if (dgvColIndex < 0)
            {
                base.TopLeftHeaderCell.Style.Alignment = this.FixAlignmentSettings(alignment);
                base.RowHeadersDefaultCellStyle.Alignment = this.FixAlignmentSettings(alignment);

            }
            else
            {
                base.Columns[dgvColIndex].HeaderCell.Style.Alignment = this.FixAlignmentSettings(alignment);
                if (base.Columns[dgvColIndex].Frozen)
                {
                    base.Columns[dgvColIndex].DefaultCellStyle.Alignment = this.FixAlignmentSettings(alignment);
                }
            }
        }

        /// <summary>
        /// Removes a row from the control.
        /// </summary>
        /// <param name="index">The Row parameter determines which row should be removed from the control. If provided, it must be in the range between 0 and Rows-1, or an Invalid Index error will occur. If omitted, the last row is deleted.</param>
        public void RemoveItem(int index = -1)
        {
            if(index == -1)
            {
                index = this.Rows - 1;
            }
            //FC:FINAL:MSH - issue #883: after deleting item with child items value of this.Rows will be incorrect calculated,
            //because child items won't be counted if they exist
            int difference = this.RowCount;
            int dgvRowIndex = this.GetDGVRowIndex(index);
            if (this.IsValidRow(dgvRowIndex))
            {
                //AM:HARRIS:#1972 - in VB6 RowColChange is not fired when deleting a row
                disableEvents = true;
                base.Rows.RemoveAt(dgvRowIndex);
                disableEvents = false;
            }
            difference = difference - this.RowCount;
            m_rows -= difference;
            //m_rows--;
            
            //FC:FINAL:SBE - #i736 - synchronize Row property after row is removed
            dgvRowIndex = this.GetDGVRowIndex(m_currentRow);
            if (!IsValidRow(dgvRowIndex))
            {
                if (m_currentRow >= 0)
                {
                    m_currentRow--;
                }
            }
        }

        /// <summary>
        /// Adds an item to a ListBox or ComboBox control or adds a row to a MS Flex Grid control. Doesn't support named arguments.
        /// Syntax
        /// object.AddItem item, index
        /// The AddItem method syntax has these parts:
        /// Part	Description
        /// object	Required. An object expression that evaluates to an object in the Applies To list.
        /// item	Required. string expression specifying the item to add to the object. For the MS Flex Grid control only, use the tab character (character code 09) to separate multiple strings you 
        /// want to insert into each column of a newly added row.
        /// index	Optional. Integer specifying the position within the object where the new item or row is placed. For the first item in a ListBox or ComboBox control or for the first row in a 
        /// MS Flex Grid control, index is 0.
        /// Remarks
        /// If you supply a valid value for index, item is placed at that position within the object. If index is omitted, item is added at the proper sorted position (if the Sorted property is set to True)
        /// or to the end of the list (if Sorted is set to False).
        /// A ListBox or ComboBox control that is bound to a Data control doesn't support the AddItem method.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"></param>
        public void AddItem(string item, int index = -1)
        {
            // JSP: Cut the first columne because this is the row header
            // (retval.ToString().IndexOf('?', pos) > 0)

            int pos = item.IndexOf('\t', 0);
            string rowHeader = "";
            string[] rowItems = null;

            if (!String.IsNullOrEmpty(item))
            {
                //BCU - check if there is row header column
                if (pos > 0 && this.FixedCols > 0)
                {
                    rowHeader = item.Substring(0, pos);
                    item = item.Substring(pos + 1);
                    rowItems = item.Split('\t');
                }
                else if (this.FixedCols > 0)
                {
                    if (pos == 0)
                    {
                        item = item.Substring(1);
                        rowItems = item.Split('\t');
                    }
                    else if (!item.Contains('\t'))
                    {
                        rowHeader = item;
                        rowItems = new string[0];
                    }
                }
                else
                {
                    rowItems = item.Split('\t');
                }
            }
            
            int dgvRowIndex = this.GetDGVRowIndex(index);

            // set rows to unfrozen, so that new rows can be added to the DGV 
            for (int i = 0; i < this.GetDGVRowIndex(this.FixedRows); i++)
            {
                base.Rows[i].Frozen = false;
            }

            //AM:HARRIS:#2786 - in VB6 RowColChange is not fired when adding a row
            disableEvents = true;

            if (dgvRowIndex < 0)
            {
                if (base.Rows.Count == 0)
                {
                    dgvRowIndex = (rowItems != null) ? base.Rows.Add(rowItems) : base.Rows.Add();
                }
                else
                {
                    //AM:HARRIS:#3340 - add new row after last row
                    //var row = base.Rows.GetLastRow(DataGridViewElementStates.Visible);
                    //if(row != null)
                    //{
                    //    dgvRowIndex = row.Index + 1;
                    //}
                    //else
                    {
                        dgvRowIndex = base.Rows.GetLastRow().Index + 1;
                    }

                    if (rowItems != null)
                    {
                        base.Rows.Insert(dgvRowIndex, rowItems);
                    }
                    else
                    {
                        base.Rows.Insert(dgvRowIndex);
                    }
                }
            }
            else
            {
                if (rowItems != null)
                {
                    base.Rows.Insert(dgvRowIndex, rowItems);
                }
                else
                {
                    base.Rows.Insert(dgvRowIndex);
                }
            }

            disableEvents = false;

            if (rowHeader != "" )
            {
                base.Rows[dgvRowIndex].HeaderCell.Value = rowHeader;
            }

            // refroze the unfrozen rows
            for (int i = 0; i < this.GetDGVRowIndex(this.FixedRows); i++)
            {
                base.Rows[i].Frozen = true;
            }
            m_rows++;
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public void MoveTo(int leftOriginal, int topOriginal = -1, int widthOriginal = -1, int heightOriginal = -1)
        {
            FCUtils.MoveTo(this, leftOriginal, topOriginal, widthOriginal, heightOriginal);
        }

        /// <summary>
        /// Clears the contents of the MSHFlexGrid. This includes all text, pictures, and cell formatting. 
        /// This method does not affect the number of rows and columns within the MSHFlexGrid.
        /// Remarks
        /// To remove cells instead of just clearing them, use the RemoveItem method on each row to be removed.
        /// </summary>
        public void Clear(ClearWhereSettings settings = ClearWhereSettings.flexClearEverywhere)
        {
            //FC:FINAL:JEI:HARRIS IIT611: we just have to reset the cell calues, in VB6 also the informations for ColumnWidth, ColumnVisiblity, DefaultCellStyle.Format remains after a Clear call
            DataGridViewCellStyle defaultStyle = new DataGridViewCellStyle();
            bool rowStyleReset = false;
            for (int i = 0; i < base.Columns.Count; i++)
            {
                if (settings == ClearWhereSettings.flexClearEverywhere)
                {
                    base.Columns[i].HeaderText = string.Empty;
                }
                for (int j = 0; j < base.Rows.Count; j++)
                {
                    if (base.RowHeadersVisible)
                    {
                        if (base.Rows[j].HeaderCell.Style != defaultStyle)
                        {
                            base.Rows[j].HeaderCell.Style = defaultStyle.Clone();
                        }
                        if (base.Rows[j].HeaderCell.Control != null)
                        {
                            base.Rows[j].HeaderCell.Control = null;
                        }
						//DDU:#i2305 - check the value too even if the control is already null
						if (base.Rows[j].HeaderCell.Value != null)
						{
							base.Rows[j].HeaderCell.Value = null;
						}
                    }
                    if (!rowStyleReset)
                    {
                        base.Rows[j].DefaultCellStyle = defaultStyle.Clone();
                    }
                    if (base[i, j].Style != defaultStyle)
                    {
                        base[i, j].Style = defaultStyle.Clone();
                    }
                    base[i, j].Value = null;
                }
                rowStyleReset = true;
            }
            return;
            //int oldRows = this.Rows;
            //int oldCols = this.Cols;
            //int oldFixedRows = this.FixedRows;
            //int oldFixedCols = this.FixedCols;
            //this.Rows = 0;
            //this.Cols = 0;
            //this.Cols = oldCols;
            //this.Rows = oldRows;
            //this.FixedCols = oldFixedCols;
            //this.FixedRows = oldFixedRows;
        }

        /// <summary>
        /// Brings a given cell into view, scrolling the contents if necessary.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        public void ShowCell(int row, int col)
        {
            int dgvRow = GetDGVRowIndex(row);
            int dgvCol = GetDGVColIndex(col);
            base.ScrollCellIntoView(dgvCol, dgvRow);
        }


        public void Select(int row, int col)
        {
            int dgvRow = GetDGVRowIndex(row);
            int dgvCol = GetDGVColIndex(col);
            //AM:HARRIS:3522 - use first visible column when the column is the rowheader
            if (dgvCol == -1)
            {
                dgvCol = FirstVisibleCol;
            }
            //JEI:HARRIS:CurrentCell can only be set to a visible cell
            if (IsRowValid(dgvRow) && IsColValid(dgvCol) && base[dgvCol, dgvRow].Visible)
            {
                //AM:HARRIS:#2937, #2792 - force RowColChange if the cell is not selected (it happens for the first row)
                bool raiseCurrentCellChange = (!base[dgvCol, dgvRow].Selected && this.CurrentCell == base[dgvCol, dgvRow]);
                base[dgvCol, dgvRow].Selected = true;
				//FC:FINAL:RPU:#i790 - Set the current cell after we set the indexes
                //this.CurrentCell = base[dgvCol, dgvRow];
                //FC:FINAL:MSH - issue #912 - update indexes of the current cell
                m_currentRow = m_rowSel = row;
                m_currentColumn = m_colSel = col;
                //P2218:SBE:#4477 - switch to another cell only after exiting EditMode
                if (this.CurrentCell != null && this.IsCurrentCellInEditMode)
                {
                    if (this.EndEdit())
                    {
                        this.CurrentCell = base[dgvCol, dgvRow];
                    }
                }
                else
                {
                    this.CurrentCell = base[dgvCol, dgvRow];
                }
                if (raiseCurrentCellChange)
                {
                    OnCurrentCellChanged(new EventArgs());
                }
            }
        }

        /// <summary>
        /// Selects a range of cells.
        /// </summary>
        /// <param name="row">The row of first cell of selection(the upper-left one)</param>
        /// <param name="col">The column of first cell of selection(the upper-left one)</param>
        /// <param name="rowSel">The row of last cell of selection(the bottom-right one)</param>
        public void Select(int row, int col, int rowSel)
        {
            Select(row, col, rowSel, col);
        }

        //RPU:
        /// <summary>
        /// Selects a range of cells.
        /// </summary>
        /// <param name="row">The row of first cell of selection(the upper-left one)</param>
        /// <param name="col">The column of first cell of selection(the upper-left one)</param>
        /// <param name="rowSel">The row of last cell of selection(the bottom-right one)</param>
        /// <param name="colSel">The column of last cell of selection(the bottom-right one)</param>
        public void Select(int row, int col, int rowSel, int colSel)
        {
            base.ClearSelection();
            if (row == 0 && this.FixedRows > 0)
            {
                row++;
            }
            if (col == 0 && this.FixedCols > 0)
            {
                col++;
            }
            if (row <= rowSel && col <= colSel)
            {
                for (int i = row; i <= rowSel; i++)
                {
                    for (int j = col; j <= colSel; j++)
                        base[GetDGVColIndex(j), GetDGVRowIndex(i)].Selected = true;
                }
            }

            //FC:AM - update current row and selected row
            m_currentRow = row;
            m_rowSel = rowSel;
            //FC:FINAL:MSH - update current col and selected col
            m_currentColumn = col;
            m_colSel = colSel;
        }

        //FC:FINAL:MSH - Issue #797: Unselect all rows, because HighLight property doesn't work
        public void UnSelectAll()
        {
            for(int i = 0; i < this.Rows - 1; i++)
            {
                base.Rows[i].Selected = false;
            }

            m_currentRow = 0;
            m_rowSel = 0;
            m_currentColumn = 0;
            m_colSel = 0;
        }

        public void Cell(CellPropertySettings setting, int row1, int col1, int row2, int col2, dynamic value)
        {
            switch (setting)
            {
                case CellPropertySettings.flexcpAlignment:
                    //FC:FINAL:MSH - i.issue #1268: incorrect comparing int value with enum and converting int result to enum value
                    //FCGrid.AlignmentSettings alignment = value is FCGrid.AlignmentSettings ? value : FCGrid.AlignmentSettings.flexAlignLeftCenter;
                    FCGrid.AlignmentSettings alignment;
                    if(!Enum.TryParse(Convert.ToString(value as object), out alignment))
                    {
                        alignment = FCGrid.AlignmentSettings.flexAlignLeftCenter;
                    }
                    for(int i = row1; i <= row2; i++)
                    {
                        for(int j = col1; j <= col2; j++)
                        {
                            int col = GetDGVColIndex(j);
                            int row = GetDGVRowIndex(i);
                            if(row < -1)
                            {
                                return;
                            }
                            DataGridViewCell cell = GetDataGridViewCell(row, col);

                            switch(alignment)
                            {
                                case AlignmentSettings.flexAlignCenterBottom:
                                    cell.Style.Alignment = DataGridViewContentAlignment.BottomCenter;
                                    break;
                                case AlignmentSettings.flexAlignCenterCenter:
                                    cell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                                    break;
                                case AlignmentSettings.flexAlignCenterTop:
                                    cell.Style.Alignment = DataGridViewContentAlignment.TopCenter;
                                    break;
                                case AlignmentSettings.flexAlignLeftBottom:
                                    cell.Style.Alignment = DataGridViewContentAlignment.BottomLeft;
                                    break;
                                case AlignmentSettings.flexAlignLeftCenter:
                                    cell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                                    break;
                                case AlignmentSettings.flexAlignLeftTop:
                                    cell.Style.Alignment = DataGridViewContentAlignment.TopLeft;
                                    break;
                                case AlignmentSettings.flexAlignRightBottom:
                                    cell.Style.Alignment = DataGridViewContentAlignment.BottomRight;
                                    break;
                                case AlignmentSettings.flexAlignRightCenter:
                                    cell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                                    break;
                                case AlignmentSettings.flexAlignRightTop:
                                    cell.Style.Alignment = DataGridViewContentAlignment.TopRight;
                                    break;
                            }
                            if (row == -1)
                            {
                                if(col >= 0 && col < this.Columns.Count)
                                {
                                    this.Columns[col].HeaderCell.Style.Alignment = cell.Style.Alignment;
                                }
                            }
                        }
                    }
                    break;
                case CellPropertySettings.flexcpBackColor:
                    Color backColor = Color.Empty;
                    //FC:FINAL:SBE - Harris #1305 - convert uint32 to color
                    if (value is uint)
                    {
                        backColor = Uint32ToColor(value);
                    }
                    else
                    {
                        //FC:FINAL:SGA - Harris #i2285 - Color.Empty does not work well with ColorTranslator. ColorTranslator.FromOle(ColorTranslator.ToOle(Color.Empty)) will return Color.Black
                        backColor = value is Color ? value : value == null ? Color.Empty : ColorTranslator.FromOle(Convert.ToInt32(value));
                    }
                    for (int i = row1; i <= row2; i++)
                    {
                        //FC:FINAL:SBE - #167 - Harris - if style is applied to all cells in a row, apply style to entire row
                        if (col2 - col1 + 1 == base.ColumnCount)
                        {
                            int dgvRowIndex = GetDGVRowIndex(i);
                            if (IsValidRow(dgvRowIndex))
                            {
                                base.Rows[dgvRowIndex].DefaultCellStyle.BackColor = backColor;
                                base.Rows[dgvRowIndex].HeaderCell.Style.BackColor = backColor;
                            }
                            else
                            {
                                base.ColumnHeadersDefaultCellStyle.BackColor = backColor;
                                base.TopLeftHeaderCell.Style.BackColor = backColor;
                            }
                            for (int j = col1; j <= col2; j++)
                            {
                                GetDataGridViewCell(GetDGVRowIndex(i), GetDGVColIndex(j)).Style.BackColor = backColor;
                            }
                        }
                        else
                        {
                            for (int j = col1; j <= col2; j++)
                            {
                                GetDataGridViewCell(GetDGVRowIndex(i), GetDGVColIndex(j)).Style.BackColor = backColor;
                            }
                        }
                    }
                    break;
                case CellPropertySettings.flexcpForeColor:
                    Color foreColor = Color.Empty;
                    //FC:FINAL:SBE - Harris #1305 - convert uint32 to color
                    if (value is uint)
                    {
                        foreColor = Uint32ToColor(value);
                    }
                    else
                    {
                        foreColor = value is Color ? value : ColorTranslator.FromOle(Convert.ToInt32(value));
                    }
                    for (int i = row1; i <= row2; i++)
                        for (int j = col1; j <= col2; j++)
                        {
                            GetDataGridViewCell(GetDGVRowIndex(i), GetDGVColIndex(j)).Style.ForeColor = foreColor;
                        }
                    break;
                case CellPropertySettings.flexcpFontBold:
                    for (int i = row1; i <= row2; i++)
                        for (int j = col1; j <= col2; j++)
                        {
                            DataGridViewCell cell = GetDataGridViewCell(GetDGVRowIndex(i), GetDGVColIndex(j));
                            if (cell.Style.Font != null)
                            {
                                cell.Style.Font = new Font(cell.Style.Font, value ? FontStyle.Bold : FontStyle.Regular);
                            }
                            else
                            {
                                cell.Style.Font = new Font("default", 16F, value ? FontStyle.Bold : FontStyle.Regular, GraphicsUnit.Pixel);
                            }
                        }
                    break;
                case CellPropertySettings.flexcpFontSize:
                    for (int i = row1; i <= row2; i++)
                        for (int j = col1; j <= col2; j++)
                        {
                            Single fontSize = (Single)value;
                            DataGridViewCell cell = GetDataGridViewCell(GetDGVRowIndex(i), GetDGVColIndex(j));
                            if (cell.Style.Font != null)
                            {
                                cell.Style.Font = new Font("default", fontSize, cell.Style.Font.Style, cell.Style.Font.Unit);
                            }
                            else
                            {
                                cell.Style.Font = new Font("default", fontSize, FontStyle.Regular, GraphicsUnit.Pixel);
                            }
                        }
                    break;
                    //FC:FINAL:MSH - i.issue #1268: implement missing choice
                case CellPropertySettings.flexcpText:
                    for (int i = row1; i <= row2; i++)
                        for (int j = col1; j <= col2; j++)
                        {
                            this.TextMatrix(i, j, value);
                        }
                    break;
                case CellPropertySettings.flexcpChecked:
                    bool boolValue = false;
                    if (value is CellCheckedSettings)
                    {
                        boolValue = value == CellCheckedSettings.flexChecked;
                    }
                    else if (value is bool)
                    {
                        boolValue = value;
                    }
                    else
                    {
                        boolValue = Convert.ToBoolean(value);
                    }

                    for (int i = col1; i <= col2; i++)
                    {
                        int dgvCol = this.GetDGVColIndex(i);
                        if (IsValidColumn(dgvCol) && this.Columns[dgvCol] is DataGridViewCheckBoxColumn)
                        {
                            for (int j = row1; j <= row2; j++)
                            {
                                int dgvRow = this.GetDGVRowIndex(j);
                                if (IsValidRow(dgvRow))
                                {
                                    this[dgvCol, dgvRow].Value = boolValue;
                                    FCDataGridViewCheckBoxCell checkBoxCell = this[dgvCol, dgvRow] as FCDataGridViewCheckBoxCell;
                                    if (checkBoxCell != null)
                                    {
                                        checkBoxCell.ValueData = boolValue ? "-1" : "0";
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
        }

        public void Cell(CellPropertySettings setting, int row1, int col, int row2, dynamic v4)
        {
            Cell(setting, row1, col, row2, col, v4);
        }

        public void Cell(CellPropertySettings setting, int row, int col, dynamic value)
        {
            col = GetDGVColIndex(col);
            row = GetDGVRowIndex(row);
            switch (setting)
            {
                case CellPropertySettings.flexcpBackColor:
                    //FC:FINAL:SBE - Harris #1305 - convert uint32 to color
                    if (value is uint)
                    {
                        GetDataGridViewCell(row, col).Style.BackColor = Uint32ToColor(value);
                    }
                    else
                    {
                        //FC:FINAL:SBE - Harris #i2309 - Color.Empty does not work well with ColorTranslator. ColorTranslator.FromOle(ColorTranslator.ToOle(Color.Empty)) will return Color.Black
                        //GetDataGridViewCell(row, col).Style.BackColor = value is Color ? value : ColorTranslator.FromOle(Convert.ToInt32(value));
                        GetDataGridViewCell(row, col).Style.BackColor = value is Color ? value : value == null ? Color.Empty : ColorTranslator.FromOle(Convert.ToInt32(value));
                    }
                    break;
                case CellPropertySettings.flexcpForeColor:
                    //FC:FINAL:SBE - Harris #1305 - convert uint32 to color
                    if (value is uint)
                    {
                        GetDataGridViewCell(row, col).Style.ForeColor = Uint32ToColor(value);
                    }
                    else
                    {
                        GetDataGridViewCell(row, col).Style.ForeColor = value is Color ? value : ColorTranslator.FromOle(Convert.ToInt32(value));
                    }
                    break;
                case CellPropertySettings.flexcpPicture:
                    if (!(value is ImageListEntry))
                    {
                        break;
                    }
                    DataGridViewCell cell = GetDataGridViewCell(row, col);
                    if (cell.Control == null)
                    {
                        PictureBox pictureBox = new PictureBox();
						//FC:FINAL:MSH - i.issue #1746: we can't use DockStyle.Fill for cell.Control or all mouse handlers will be blocked and won't be handled (overlapping cell by control)
						//pictureBox.Dock = DockStyle.Fill;
						pictureBox.Dock = DockStyle.Right;
                        pictureBox.SizeMode = PictureBoxSizeMode.CenterImage;
                        if (!string.IsNullOrEmpty(value.ImageSource))
                        {
                            pictureBox.ImageSource = value.ImageSource;
                        }
                        else
                        {
                            pictureBox.Image = value.Image;
                        }
                        cell.Control = pictureBox;
                    }
                    else if (cell.Control is PictureBox)
                    {
                        (cell.Control as PictureBox).Image = value.Image;
                    }
                    
                    break;
                case CellPropertySettings.flexcpFontBold:
                    GetDataGridViewCell(row, col).Style.Font = new Font("default", 16F, value ? FontStyle.Bold : FontStyle.Regular, GraphicsUnit.Pixel);
                    break;
                case CellPropertySettings.flexcpData:
                    GetDataGridViewCell(row, col).UserData.Data = value;
                    break;
                // FC:FINAL:VGE - #i1169 Implementing flexcpAlignment
                case CellPropertySettings.flexcpAlignment:
                    {
                        //FC:FINAL:MSH - i.issue #1274: incorrect comparing int value with enum
                        //GetDataGridViewCell(row, col).Style.Alignment = alignments.First(x => x.Key == value).Value;
                        AlignmentSettings alignment;
                        if (!Enum.TryParse(Convert.ToString(value as object), out alignment))
                        {
                            //FC:FINAL:MSH - i.issue #1274: default value for preventing exceptions with LINQ
                            alignment = AlignmentSettings.flexAlignLeftCenter;
                        }
                        GetDataGridViewCell(row, col).Style.Alignment = alignments.First(x => x.Key == alignment).Value;
                        break;
                    }
                //FC:FINAL:MSH - i.issue #1269: implement missing choice
                case CellPropertySettings.flexcpText:
					//FC:FINAL:DSE:#i2277 If column contains a ComboList, value should be computed from the list
					//GetDataGridViewCell(row, col).Value = value;
					this.TextMatrix(GetFlexRowIndex(row), GetFlexColIndex(col), value);
                    break;
                case CellPropertySettings.flexcpChecked:
                    //FC:FINAL:BSE:#i2348 exception when converting to cellchecked settings
                    if (value is CellCheckedSettings)
                    {
                        CellCheckedSettings cellCheck = (CellCheckedSettings)value;
                        GetDataGridViewCell(row, col).Value = cellCheck == CellCheckedSettings.flexChecked ? true : false;
                    }
                    else if (value is bool)
                    {
                        GetDataGridViewCell(row, col).Value = value;
                    }
                    else
                    {
                        GetDataGridViewCell(row, col).Value = Convert.ToBoolean(value);
                    }
                    FCDataGridViewCheckBoxCell checkBoxCell = GetDataGridViewCell(row, col) as FCDataGridViewCheckBoxCell;
                    if(checkBoxCell != null)
                    {
                        checkBoxCell.ValueData = (bool)GetDataGridViewCell(row, col).Value ? "-1" : "0";
                    }
                    //AM:HARRIS:#3660 - when IsSubtotal is set to true the checkbox is not visible; but if it set with the Cell method then it becomes visible again
                    GetDataGridViewCell(row, col).Style.Padding = new Padding(0);
                    break;
            }
        }

        //FC:FINAL:BSE #2088 compute cell location using scrollbars position too
        int xOffset = 0;
        int yOffset = 0;

        protected override void OnScroll(ScrollEventArgs e)
        {
            if(e.ScrollOrientation == ScrollOrientation.HorizontalScroll)
            {
                xOffset = e.NewValue;
            } else
            {
                yOffset = e.NewValue;
            }
            base.OnScroll(e);
        }

        protected override void OnSelectionChanged(EventArgs e)
        {
            base.OnSelectionChanged(e);
            UpdateGridSelection();
        }

        private void UpdateGridSelection()
        {
            //P2218:AM:#4498 - don't update during column deletion because it will throw an out of index exception
            if (inColumnDisplayIndexChanged)
            {
                return;
            }
            //P2218:SBE:#4232 - update RowSel and ColSel properties
            var selectedRows = SelectedRows;
            if (selectedRows.Count > 0)
            {
                int startIndex = GetFlexRowIndex(selectedRows[0].Index);
                int endIndex = GetFlexRowIndex(selectedRows[selectedRows.Count - 1].Index);
                m_rowSel = (this.Row == startIndex) ? endIndex : startIndex;
            }
            else
            {
                int startIndex = -1;
                int endIndex = -1;
                for (int rowIndex = 0; rowIndex < base.Rows.Count; rowIndex++)
                {
                    for (int colIndex = 0; colIndex < base.Columns.Count; colIndex++)
                    {
                        var cellSelected = this[colIndex, rowIndex].Selected;
                        if (cellSelected)
                        {
                            if (startIndex == -1)
                            {
                                startIndex = rowIndex;
                            }
                            if (rowIndex > endIndex)
                            {
                                endIndex = rowIndex;
                            }
                        }
                    }
                }
                if (startIndex != -1 && endIndex != -1)
                {
                    startIndex = GetFlexRowIndex(startIndex);
                    endIndex = GetFlexRowIndex(endIndex);
                    m_rowSel = (this.Row == startIndex) ? endIndex : startIndex;
                }
                else
                {
                    m_rowSel = this.Row;
                }
            }

            var selectedCols = SelectedColumns;
            if (selectedCols.Count > 0)
            {
                int startIndex = GetFlexRowIndex(selectedCols[0].Index);
                int endIndex = GetFlexRowIndex(selectedCols[selectedCols.Count - 1].Index);
                m_colSel = (this.Col == startIndex) ? endIndex : startIndex;
            }
            else
            {
                int startIndex = -1;
                int endIndex = -1;
                for (int colIndex = 0; colIndex < base.Columns.Count; colIndex++)
                {
                    for (int rowIndex = 0; rowIndex < base.Rows.Count; rowIndex++)
                    {
                        var cellSelected = this[colIndex, rowIndex].Selected;
                        if (cellSelected)
                        {
                            if (startIndex == -1)
                            {
                                startIndex = colIndex;
                            }
                            if (rowIndex > endIndex)
                            {
                                endIndex = colIndex;
                            }
                        }
                    }
                }
                if (startIndex != -1 && endIndex != -1)
                {
                    startIndex = GetFlexColIndex(startIndex);
                    endIndex = GetFlexColIndex(endIndex);
                    m_colSel = (this.Col == startIndex) ? endIndex : startIndex;
                }
                else
                {
                    m_colSel = this.Col;
                }
            }
        }

        public dynamic Cell(CellPropertySettings setting, int row, int col)
        {
            //FC:FINAL:MSH - i.issue #1268: use dgv indexes in all methods, except TextMatrix, because in TextMatrix GetDGV...Index will be called 
            // again for indexes and returned value will be incorrect
            int dgvCol = GetDGVColIndex(col);
            int dgvRow = GetDGVRowIndex(row);
            switch (setting)
            {
                case CellPropertySettings.flexcpForeColor:
                    return ColorTranslator.ToOle(GetDataGridViewCell(dgvRow, dgvCol).Style.ForeColor);
                    break;

                case CellPropertySettings.flexcpBackColor:
                    //FC:FINAL:SBE - Harris #i2309 - Color.Empty does not work well with ColorTranslator. ColorTranslator.FromOle(ColorTranslator.ToOle(Color.Empty)) will return Color.Black
                    Color color = GetDataGridViewCell(dgvRow, dgvCol).Style.BackColor;
                    if (color.IsEmpty)
                    {
                        return null;
                    }
                    return ColorTranslator.ToOle(color);
                    break;

                case CellPropertySettings.flexcpTextDisplay:
                    //FC:FINAL:SGA Harris used the correct property
                    //AM:HARRIS:#i2150 - return as string (for boolean values we need to return "True" instead of true)
                    return GetDataGridViewCell(dgvRow, dgvCol).EditedFormattedValue?.ToString() ?? "";
                    break;
                case FCGrid.CellPropertySettings.flexcpLeft:
                    int left = GetCellDisplayRectangle(GetDataGridViewCell(dgvRow, dgvCol)).Left;
                    //FC:FINAl:BSE #2088 compute cell location using scrollbars position too
                    if (!base.Columns[dgvCol].Frozen)
                    {
                        left -= xOffset;
                    }
                    //using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                        return Convert.ToInt32(fcGraphics.ScaleX(left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                    break;
                case FCGrid.CellPropertySettings.flexcpTop:
                    int top = GetCellDisplayRectangle(GetDataGridViewCell(dgvRow, dgvCol)).Top;
                    //using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                        return Convert.ToInt32(fcGraphics.ScaleX(top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                    break;
                case FCGrid.CellPropertySettings.flexcpWidth:
                    int width = GetCellDisplayRectangle(GetDataGridViewCell(dgvRow, dgvCol)).Width;
                    //using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                        return Convert.ToInt32(fcGraphics.ScaleX(width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                    }
                    break;
                case CellPropertySettings.flexcpData:
                    return GetDataGridViewCell(dgvRow, dgvCol).UserData.Data;
                    break;
                case CellPropertySettings.flexcpValue:
                    return this.TextMatrix(row, col);
                    break;
                // FC:FINAL:VGE - #i1169 Implementing flexcpAlignment
                case CellPropertySettings.flexcpAlignment:
                    return alignments.First(x => x.Value == GetDataGridViewCell(dgvRow, dgvCol).Style.Alignment).Key;
                    break;
                case CellPropertySettings.flexcpText:
                    if (IsValidCell(dgvCol, dgvRow))
                    {
						//FC:FINAL:DSE:#i2277 When using ColComboList, flexcpText should return the value and not the display value
						//return this[dgvCol, dgvRow].FormattedValue;
						return this.TextMatrix(row, col);
                    }
                    else if (dgvRow < 0 && IsValidColumn(dgvCol))
                    {
                        return this.Columns[dgvCol].HeaderText;
                    }
                    else if (dgvCol < 0 && IsValidRow(dgvRow))
                    {
                        return base.Rows[dgvRow].HeaderCell.FormattedValue;
                    }
                    else if (dgvRow < 0 && dgvCol < 0)
                    {
                        return this.TopLeftHeaderCell.FormattedValue;
                    }
                    else
                    {
                        return "";
                    }
                    break;
                case CellPropertySettings.flexcpChecked:
                    if (IsValidColumn(dgvCol) && this.Columns[dgvCol] is DataGridViewCheckBoxColumn)
                    {
                        if (Convert.ToBoolean(GetDataGridViewCell(dgvRow, dgvCol).Value) == true)
                        {
                            return Convert.ToInt32(CellCheckedSettings.flexChecked);
                        }
                        else
                        {
                            return Convert.ToInt32(CellCheckedSettings.flexUnchecked);
                        }
                    }
                    return 0;
                    break;

                default: return 0;
            }
        }

        //FC:FINAL:RPU: Add Cell propery for an arbitrary range
        /// <remarks>
        /// When reading, only cell (row1, col1) is used. The only exception is when you read flexcpText property of a range. In this case, a clip string is returned containing the text in the whole section
        /// </remarks>
        public dynamic Cell(CellPropertySettings setting, int row1, int col1, int row2, int col2 )
        {
            if (setting != CellPropertySettings.flexcpText)
            {
                return Cell(setting, row1, col1);
            } else {
                string text = "";
                for(int row = row1; row <= row2; row++)
                {
                    for( int col = col1; col <= col2; col++)
                    {
                        text += this.TextMatrix(row, col) + "\t";
                    }
                    text += "\r\n";
                }
                return text;
            }
        }

        public dynamic Cell(CellPropertySettings setting)
        {
            return this.Cell(setting, this.Row, this.Col);
        }

        public void CellBorder(Color color, int left, int top, int right, int bottom, int vertical, int horizontal)
        {
            //throw new NotImplementedException();
        }

        public void CellToolTip(int row, int col, string tooltip)
        {
            this[this.GetDGVColIndex(col), this.GetDGVRowIndex(row)].ToolTipText = tooltip;
        }

        #endregion

        #region Internal Methods

        /// <summary>
        /// GetDataGridViewCell
        /// </summary>
        /// <param name="rowIndex"></param>
        /// <param name="columnIndex"></param>
        /// <returns></returns>
        internal DataGridViewCell GetDataGridViewCell(int rowIndex, int columnIndex)
        {
            DataGridViewCell cell = null;
            if (columnIndex == -1 && rowIndex < 0)
            {
                cell = this.TopLeftHeaderCell;
            }
            else if (rowIndex < 0 && columnIndex > -1)
            {
                cell = base.Columns[columnIndex].HeaderCell;
            }
            else if (columnIndex == -1 && rowIndex > -1)
            {
                cell = base.Rows[rowIndex].HeaderCell;
            }
            else
            {
                cell = base[columnIndex, rowIndex];
            }
            return cell;
        }

        /// <summary>
        /// GetCurrentCell
        /// </summary>
        /// <returns></returns>
        internal DataGridViewCell GetCurrentCell()
        {
            int dgvColIndex = this.GetDGVColIndex(this.Col);
            int dgvRowIndex = this.GetDGVRowIndex(this.Row);

            return GetDataGridViewCell(dgvRowIndex, dgvColIndex);
        }

        #endregion

        #region Protected Methods
        protected virtual void OnChangeEdit()
        {
            if (this.ChangeEdit != null)
            {
                this.ChangeEdit(this, EventArgs.Empty);
            }
        }
        //FC:FINAL:MSH - i.issue #1098: sending value in Grid, which isn't in combobox list will throw an exception(in VB6 it works without exceptions),
        //so for this we should prevent the execution of base.OnDataError handling
        protected override void OnDataError(DataGridViewDataErrorEventArgs e)
        {
            //AM:HARRIS:#2746 - display custom message on wrong inputs
            if (string.Compare("Input string was not in a correct format.", e.Exception.Message) == 0 && base.Columns[e.ColumnIndex].ValueType.IsNumeric())
            {
                if (!inErrorMessage)
                {
                    inErrorMessage = true;
                    MessageBox.Show("Invalid value. Please enter a number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    inErrorMessage = false;
                }
            }
            //FC:FINAL:SBE - In Wisej 2.0.24 the Exception.Message text was changed. Changed condition to work with new and previous exception message
            //else if (string.Compare("DataGridViewComboBoxCell value is not valid.", e.Exception.Message) != 0)
            else if (!e.Exception.Message.Contains("DataGridViewComboBoxCell value is not valid."))
            {
                base.OnDataError(e);
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            //CHE: in VB6 MSFlexGrid control doesn't trap arrow keys http://support.microsoft.com/kb/171733
            if (FlexKeyDown != null && !ArrowKeyPressed(e))
            {
                FlexKeyDown(this, e);
            }
            base.OnKeyDown(e);
        }

        protected override void OnCellMouseDown(DataGridViewCellMouseEventArgs e)
        {
            //P2218:AM:#3761 - moved setting of role from OnCellClick because is is now fired after OnClick
            this.role = e.Role;
            this.MouseRow = GetFlexRowIndex(e.RowIndex);
            this.MouseCol = GetFlexColIndex(e.ColumnIndex);
            base.OnCellMouseDown(e);
        }

        protected override void OnDoubleClick(EventArgs e)
        {
            CheckPosition();
            base.OnDoubleClick(e);
        }

        protected override void OnColumnHeaderMouseDoubleClick(DataGridViewCellMouseEventArgs e)
        {
            this.MouseRow = GetFlexRowIndex(e.RowIndex);
            this.MouseCol = GetFlexColIndex(e.ColumnIndex);
            base.OnColumnHeaderMouseDoubleClick(e);
            
            //FC:FINAl:SBE - not nedeed anymore. Now double click for header is triggered with DoubleClick event
            //this.OnDoubleClick(EventArgs.Empty);
        }

		protected override void OnColumnHeaderMouseClick(DataGridViewCellMouseEventArgs e)
		{
			this.Col = GetFlexColIndex(e.ColumnIndex);
			base.OnColumnHeaderMouseClick(e);
		}

		protected override void OnClick(EventArgs e)
        {
            CheckPosition();
            //AM:HARRIS:#i1720 - Click event should not be fired when clicking on the expand/collapse button
            //if (this.role != "open" && this.role != "close")
            //FC:FINAL:SGA Harris #i2513 - don't enter OnClick when cell is in edit mode
            if (this.role != "open" && this.role != "close" && !IsCurrentCellInEditMode)
            {
                base.OnClick(e);
            }
            this.role = null;
        }

        protected override void OnCellClick(DataGridViewCellEventArgs e)
        {
            if (GetFlexRowIndex(e.RowIndex) != this.MouseRow)
            {
                this.MouseRow = GetFlexRowIndex(e.RowIndex);
            }
            if (GetFlexColIndex(e.ColumnIndex) != this.MouseCol)
            {
                this.MouseCol = GetFlexColIndex(e.ColumnIndex);
            }
            base.OnCellClick(e);
            if (role == "open" || role == "close")
            {
                if (Collapsed != null)
                {
                    Collapsed(this, EventArgs.Empty);
                }
            }
        }

        protected override void OnCurrentCellChanged(EventArgs e)
        {
            if (disableEvents || IsFormInitializing())
            {
                return;
            }
            if (this.BeforeRowColChange != null && !beforeRowColChangeFired)
            {
                beforeRowColChangeFired = true;
                BeforeRowColChangeEventArgs args = new BeforeRowColChangeEventArgs();
                args.OldCol = this.Col;
                args.OldRow = this.Row;
                args.NewCol = this.GetFlexColIndex(this.CurrentCell?.ColumnIndex ?? -1);
                args.NewRow = this.GetFlexRowIndex(this.CurrentCell?.RowIndex ?? -1);
                this.BeforeRowColChange(this, args);
                if (args.Cancel)
                {
                    return;
                }
            }
            base.OnCurrentCellChanged(e);
            beforeRowColChangeFired = false;

        }

        protected override void OnColumnStateChanged(DataGridViewColumnStateChangedEventArgs e)
        {
            base.OnColumnStateChanged(e);
            if (!widthToZeroChanging)
            {
                if (e.StateChanged == DataGridViewElementStates.Visible)
                {
                    if (columnsVisibility.ContainsKey(e.Column.Index))
                    {
                        columnsVisibility[e.Column.Index] = e.Column.Visible;
                    }
                    if (extendLastCol)
                    {
                        ExtendLastVisibleColumn();
                    }
                    //FC:FINAL:MSH - issue #1131: disable AutoSize if somewhere ExtendLastCol was changed
                    else if (!extendLastCol)
                    {
                        DisableExtendedModeForColumns();
                    }
                }
            }
        }

        protected override void OnColumnDisplayIndexChanged(DataGridViewColumnEventArgs e)
        {
            base.OnColumnDisplayIndexChanged(e);
            if (!inColumnDisplayIndexChanged)
            {
                int column = GetFlexColIndex(e.Column.Index);
                int position = GetFlexColIndex(e.Column.DisplayIndex);
                //P2218:AM:#4117 - when the column position is changed by drag&drop, the position changes too, not only the display index
                ColPosition(column, position);
                if (AfterMoveColumn != null)
                {
                    AfterMoveColumnEventArgs args = new AfterMoveColumnEventArgs();
                    args.Column = column;
                    args.Position = position;
                    AfterMoveColumn(this, args);
                }
            }
        }

        protected override void OnCellMouseMove(DataGridViewCellMouseEventArgs e)
        {
            //JEI:HARRIS:IT273: get correct values for MouseRow and MouseCol
            if (GetFlexRowIndex(e.RowIndex) != this.MouseRow)
            {
                this.MouseRow = GetFlexRowIndex(e.RowIndex);
            }
            if (GetFlexColIndex(e.ColumnIndex) != this.MouseCol)
            {
                this.MouseCol = GetFlexColIndex(e.ColumnIndex);
            }
            base.OnCellMouseMove(e);
        }

        //protected override void OnRowEnter(DataGridViewCellEventArgs e)
        //{
        //    base.OnRowEnter(e);

        //    if (this.BeforeRowColChange != null)
        //    {
        //        this.BeforeRowColChange(this, EventArgs.Empty);
        //    }
        //}

        protected override void OnRowCollapsed(DataGridViewRowEventArgs e)
        {
            if (inExpandCollapseAll && SuppressExpandCollapseEventsOnExpandCollapseAll)
            {
                return;
            }
            base.OnRowCollapsed(e);
            //FC:FINAL:DSE - Harris #i1185 Force cell formatting (if cell values has been changed during row expanded / collapsed event handler)
            this.Refresh();
        }

        protected override void OnRowExpanded(DataGridViewRowEventArgs e)
        {
            if (inExpandCollapseAll && SuppressExpandCollapseEventsOnExpandCollapseAll)
            {
                return;
            }
            base.OnRowExpanded(e);
            //FC:FINAL:DSE - Harris #i1185 Force cell formatting (if cell values has been changed during row expanded / collapsed event handler)
            this.Refresh();
        }

        protected override void OnColumnRemoved(DataGridViewColumnEventArgs e)
        {
            if (IsFormInitializing())
            {
                return;
            }
            base.OnColumnRemoved(e);
            if (columnsVisibility.ContainsKey(e.Column.Index))
            {
                columnsVisibility.Remove(e.Column.Index);
            }
            RecalculateCols();
        }

        protected override void OnColumnAdded(DataGridViewColumnEventArgs e)
        {
            // CHE: by default, column SortMode is set to Automatic and when SelectionMode is ColumnHeaderSelect then exception is thrown:
            // "Column's SortMode cannot be set to Automatic while the DataGridView control's SelectionMode is set to ColumnHeaderSelect."
            //FC:FINAL:SBE - Harris #87 & #187 - ExplorerBar setting will change the sort mode on columns
            if (this.ExplorerBar == ExplorerBarSettings.flexExSort || this.ExplorerBar == ExplorerBarSettings.flexExSortAndMove ||
                this.ExplorerBar == ExplorerBarSettings.flexExSortShow || this.ExplorerBar == ExplorerBarSettings.flexExSortShowAndMove)
            {
                e.Column.SortMode = DataGridViewColumnSortMode.Automatic;
            }
            else
            {
                e.Column.SortMode = DataGridViewColumnSortMode.Programmatic;
            }

            base.OnColumnAdded(e);
            RecalculateCols();
            if (extendLastCol)
            {
                ExtendLastVisibleColumn();
            }
        }

        protected override void OnFontChanged(EventArgs e)
        {
            base.OnFontChanged(e);

            //CHE: recalculate RowHeight when changing grid Font
            //using (Graphics graphics = Graphics.FromHwnd(this.Handle))
            //{
            //    SizeF baselineSize = graphics.MeasureString("Test line", this.Font);

            //    int rowHeight = 0;
            //    //using (Graphics g = this.CreateGraphics())
            //    {
            //        fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
            //        rowHeight = Convert.ToInt32(fcGraphics.ScaleY(baselineSize.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
            //    }

            //    for (int rowIndex = 0; rowIndex < this.Rows; rowIndex++)
            //    {
            //        this.RowHeight(rowIndex, rowHeight);
            //    }
            //}

        }

        protected override void OnReadOnlyChanged(EventArgs e)
        {
            base.OnReadOnlyChanged(e);

            //SBE - fixed rows should not be editable, after readonly property is changed on entire grid
            for(int flexRow = 0; flexRow  < this.FixedRows; flexRow++)
            {
                int dgvRowIndex = this.GetDGVRowIndex(flexRow);
                if (IsValidRow(dgvRowIndex))
                {
                    base.Rows[dgvRowIndex].ReadOnly = true;
                }
            }
          
            //SGA Harris #i2224 - fixed columns should not be editable, after readonly property is changed on entire grid
            for (int flexCol = 0; flexCol < this.FixedCols; flexCol++)
            {
                int dgvColIndex = this.GetDGVColIndex(flexCol);

                if (IsValidColumn(dgvColIndex))
                {   
                    base.Columns[dgvColIndex].ReadOnly = true;
                }
            }            
        }
        
        private string GetColEditMask(int colIndex)
        {
            if (string.IsNullOrEmpty(this.EditMask) && base.Columns[colIndex].UserData.EditMask != null)
            {
                return base.Columns[colIndex].UserData.EditMask;
            }
            else
            {
                return this.EditMask;
            }
        }

        private string GetColComboList(int colIndex)
        {
            //AM:HARRIS:#1936 - first take the ColComboList value
            //if (string.IsNullOrEmpty(this.ComboList) && base.Columns[colIndex].UserData.ComboList != null)
            if (base.Columns[colIndex].UserData.ComboList != null)
            {
                return base.Columns[colIndex].UserData.ComboList;
            }
            else
            {
                return this.ComboList;
            }
        }

        private string GetComboCellValue(DataGridViewCell cell)
        {
            DataGridViewComboBoxColumn comboBoxColumn = cell.OwningColumn as DataGridViewComboBoxColumn;
            int comboValueIndex = 0;
            if (comboBoxColumn != null)
            {
                comboValueIndex = this.GetComboValueIndex(comboBoxColumn);
            }
                //FC:FINAL:MSH - i.issue #1025: in VB6 from comboBox will be returned value from 'Value', not from 'Text'
            if (!string.IsNullOrEmpty(Convert.ToString(cell.Value)))
            {
                return GetComboCellValue(cell.Value, comboValueIndex);
            }
            else
            {
                return GetComboCellValue(cell.FormattedValue);
            }            
        }

        private string GetComboCellValue(object formattedValue, int columnIndex = 0)
        {
            string value = Convert.ToString(formattedValue);

            if (!String.IsNullOrEmpty(value))
            {
                //FC:FINAL:DSE Use string with the format "*nnn;" to the first item (where nnn is the zero-based index of the column to be displayed) to display a different column
                //value = value.Split(new string[] { "\t" }, StringSplitOptions.None).First();
                string[] comboValues = value.Split(new string[] { "\t" }, StringSplitOptions.None);
                if(columnIndex < comboValues.Length)
                {
                    return comboValues[columnIndex];
                }
            }

            return value;
        }

        private int GetComboValueIndex(DataGridViewComboBoxColumn comboBoxColumn)
        {
            //FC:FINAL:DSE Use string with the format "*nnn;" to the first item (where nnn is the zero-based index of the column to be displayed) to display a different column
            string comboValueString;
            int comboValueIndex = 0;
            comboValueString = comboBoxColumn.UserData["displayIndex"];
            if (Information.IsNumeric(comboValueString))
            {
                comboValueIndex = Convert.ToInt16(Conversion.Val(comboValueString));
            }

            return comboValueIndex;
        }

        private Color Uint32ToColor(uint value)
        {
            int blue = Convert.ToInt32(value % 256);
            value = value / 256;
            int green = Convert.ToInt32(value % 256);
            value = value / 256;
            int red = Convert.ToInt32(value % 256);
            value = value / 256;
            int alpha = Convert.ToInt32(value % 256);

            return Color.FromArgb(alpha, red, green, blue);
        }

        protected override void OnCellValueChanged(DataGridViewCellEventArgs e)
        {
            //FC:FINAL:DSE:#i1152 Prevent recursive event firing if .TextMatrix is called from CellValueChanged event handler
            if (!fireCellValueChanged)
            {
                DataGridViewCell cell = this.GetDataGridViewCell(e.RowIndex, e.ColumnIndex);
                if(cell != null)
                {
                    //push modifications to the client (Modifications are sent to the client by the base method)
                    cell.Update();
                }
                return;
            }
            cellValueChangedRunning = true;
            base.OnCellValueChanged(e);
            cellValueChangedRunning = false;
        }

        protected override void OnCellBeginEdit(DataGridViewCellCancelEventArgs e)
        {
            editingValue = (e.ColumnIndex, e.RowIndex, base[e.ColumnIndex, e.RowIndex].Value);
            base.OnCellBeginEdit(e);
            if (!String.IsNullOrEmpty(this.GetColComboList(e.ColumnIndex)))
            {
                if (this.GetColComboList(e.ColumnIndex) == "...")
                {
                    FCButtonCell buttonCell = new FCButtonCell();
                    buttonCell.Height = base.Rows[e.RowIndex].Height;
                    buttonCell.Width = base.Columns[e.ColumnIndex].Width;
                    buttonCell.Text = base[e.ColumnIndex, e.RowIndex].FormattedValue.ToString();
                    buttonCell.ButtonClick += ButtonCell_ButtonClick;
                    buttonCell.Focusable = true;

                    base.Columns[e.ColumnIndex].Editor = buttonCell;
                    base.Columns[e.ColumnIndex].UserData.ComboBox = false;
                }
                else
                {
                    string[] tokens = this.GetColComboList(e.ColumnIndex).Split('|');
                    comboListItems = new List<string>();

                    for(int i = 0; i < tokens.Length; i++)
                    {
                        string token = tokens[i];
                        //FC:FINAL:BSE #2747 remove first empty token if combolist starts with "|"
                        if(i == 0)
                        {
                            if (string.IsNullOrEmpty(token))
                            {
                                continue;
                            }
                        }
                        //FC:FINAL:DSE:#1531 Save all combo list definition in the data source
                        //string item = token.TrimStart('#');

                        //if (item.IndexOf(";") != -1)
                        //{
                        //    string id = item.Substring(0, item.IndexOf(";"));
                        //    //FC:FINAL:DSE Use string with the format "*nnn;" to the first item (where nnn is the zero-based index of the column to be displayed) to display a different column
                        //    if (id.Contains("*"))
                        //    {
                        //        id = id.Substring(0, id.IndexOf("*"));
                        //    }
                        //    string value = item.Substring(item.IndexOf(";") + 1).Trim();
                        //    comboListItems.Add(new DataSourceItem(value, id));
                            
                        //}
                        //else
                        //{
                        //    comboListItems.Add(new DataSourceItem(item, item));
                        //}
                        comboListItems.Add(token);

                    }

                    //FC:FINAL:DSE:#1531 Implementation for multiple columns for ComboBox
                    //ComboBox comboBox = new ComboBox();
                    FCListViewComboBox comboBox = new FCListViewComboBox();
                    //comboBox.ListView.GridLines = false;
                    comboBox.DropDownStyle = this.GetColComboList(e.ColumnIndex).StartsWith("|") ? ComboBoxStyle.DropDown : ComboBoxStyle.DropDownList;
                    comboBox.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    comboBox.DataSource = comboListItems;
                    comboBox.OwningCell = base[e.ColumnIndex, e.RowIndex];
                    comboBox.Text = Convert.ToString(this[e.ColumnIndex, e.RowIndex].FormattedValue);
                    comboBox.DropDown += new EventHandler(ComboBox_DropDown);
                    comboBox.DropDownClosed += ComboBox_DropDownClosed;
                    comboBox.KeyDown += ComboBox_KeyDown; ;

                    //FC:FINAL:MSH - issue #1588: change height of dropdown area if it is bigger than height of displayed items 
                    if(comboBox.Items.Count > 0)
                    {
                        int itemsHeight = comboBox.ListView.Items.FirstOrDefault().RowHeight * comboBox.Items.Count + 5;
                        if (itemsHeight < comboBox.DropDownHeight)
                        {
                            comboBox.DropDownHeight = itemsHeight;
                        }
						else
						{
							//HARRIS:DDU:#2747, #4190 - remove some height to don't look like another empty row
							comboBox.DropDownHeight -= 25;
						}
                    }

                    //FC:FINAL:MSH - issue #1617: width of EditingControl can't less then column width (will be automatically stretched to column width)
                    int comboColsWidth = comboBox.Columns.Sum(x => x.Width);
                    if(comboColsWidth < base.Columns[e.ColumnIndex].Width)
                    {
                        comboBox.Width = base.Columns[e.ColumnIndex].Width;
                        //FC:FINAL:SBE - Harris #i2418 - add extra space to avoid scrollbars
                        comboBox.DropDownWidth = comboBox.Width + 20;
                        if (comboBox.Columns.Count == 1)
                        {
                            comboBox.Columns[0].Width = base.Columns[e.ColumnIndex].Width;
                        }
                    }
                    //FC:FINAl:SBE - when column width is to small, increase the with of the combobox dropdown list, to avoid scrollbars
                    else
                    {
                        comboBox.DropDownWidth = comboColsWidth + 20;
                    }
                    if (comboBox.Columns.Count > 1)
                    {
                        for(int i = 1; i < comboBox.Columns.Count; i++)
                        {
                            comboBox.Columns[i].Resizable = false;
                        }
                    }

                    base.Columns[e.ColumnIndex].Editor = comboBox;
                    base.Columns[e.ColumnIndex].UserData.ComboBox = true;
                }
            }
            else if(!String.IsNullOrEmpty(this.GetColEditMask(e.ColumnIndex)) && !(base.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn))
            {
                FCMaskedTextBox textBox = new FCMaskedTextBox();
                textBox.Mask = this.GetColEditMask(e.ColumnIndex);
                //JEI:HARRIS:IIT808+IT265 after enter some value into the MaskedTextBox we don't need the promt
                //textBox.TextMaskFormat = MaskFormat.IncludePromptAndLiterals;
                textBox.TextMaskFormat = MaskFormat.IncludeLiterals;
                textBox.HidePromptOnLeave = true;
                base.Columns[e.ColumnIndex].Editor = textBox;
            }
            else if (Columns[e.ColumnIndex].UserData.AllowedKeys != null)
            {
                //keep custom column editor
                base.Columns[e.ColumnIndex].UserData.ComboBox = false;
            }
            else
            {   
                base.Columns[e.ColumnIndex].Editor = null;
                base.Columns[e.ColumnIndex].UserData.ComboBox = false;
            }
        }

        internal void TriggerEndEdit(int columnIndex, int rowIndex)
        {
            this.OnCellEndEdit(new DataGridViewCellEventArgs(columnIndex, rowIndex));
        }

        protected override void OnCellEndEdit(DataGridViewCellEventArgs e)
        {
            if (this.EditingControl != null)
            {
                this.EditingControl.TextChanged -= new EventHandler(EditorTextChanged);
            }
            base.OnCellEndEdit(e);
            //P2218:AM:#4272 - when the editor is FCButtonCell and the value is not changed, it is not visible on the client
            if (base.Columns[e.ColumnIndex].Editor is FCButtonCell)
            {
                base[e.ColumnIndex, e.RowIndex].Value = null;
                base[e.ColumnIndex, e.RowIndex].Value = (base.Columns[e.ColumnIndex].Editor as FCButtonCell).Text;
            }
            if (cellValidated && editableByEditCell)
            {
                base.ReadOnly = true;
                editableByEditCell = false;
            }
        }

        protected override void OnCellValidated(DataGridViewCellEventArgs e)
        {
            base.OnCellValidated(e);
            int dgvRowIndex = GetDGVRowIndex(e.RowIndex);
            int dgvColIndex = GetDGVColIndex(e.ColumnIndex);
            //FC:FINAL:DSE IN VB6 Cell validating event for CheckBoxCell comes only on cell click (and not on cell leave too)
            FCDataGridViewCheckBoxCell checkBoxCell = base[e.ColumnIndex, e.RowIndex] as FCDataGridViewCheckBoxCell;
            if (checkBoxCell != null)
            {
                //FC:FINAL:DSE:#i918 in VB6, clicking on a check box cell changes the value of the ValueData property
                if (checkBoxCell.Value != null && checkBoxCell.Value != "" && Convert.ToBoolean(checkBoxCell.Value))
                {
                    checkBoxCell.ValueData = "-1";
                }
                else
                {
                    checkBoxCell.ValueData = "0";
                }
            }

            //P2218:AM:#4085 - moved code from OnCellEndEdit to fire AfterEdit after the cell has been validated
            if (editingValue.columnIndex == e.ColumnIndex && editingValue.rowIndex == e.RowIndex)
            {
                if (editingValue.cellValue != base[e.ColumnIndex, e.RowIndex].Value)
                {
                    if (AfterEdit != null)
                    {
                        AfterEdit(this, e);
                    }
                }
            }
            if (checkBoxCell != null)
            {
                //P2218:SBE:#4192 - if it is a checkboxcell, and user click on the same cell againg, CellBeginEdit is not triggered again. CellBeginEdit will be triggered only after changing the current cell
                editingValue.cellValue = base[e.ColumnIndex, e.RowIndex].Value;
            }
            else
            {
                editingValue = (-1, -1, null);
            }
        }

        protected override void OnCellValidating(DataGridViewCellValidatingEventArgs e)
        {
            //FC:FINAL:DSE Fire ValidateEdit event only if current cell has been modified
            if(!this.IsCurrentCellInEditMode)
            {
                return;
            }
            //P2218:AM:#4178 - avoid multiple calls - it happens when a messagebox is displayed during cell validation
            if (inCellValidating)
            {
                return;
            }
            else
            {
                inCellValidating = true;
            }
            //FC:FINAL:DDU:#i1744 - initialize row and col index
            int dgvRowIndex = GetDGVRowIndex(e.RowIndex);
			int dgvColIndex = GetDGVColIndex(e.ColumnIndex);
			//FC:FINAL:DSE IN VB6 Cell validating event for CheckBoxCell comes only on cell click (and not on cell leave too)
			FCDataGridViewCheckBoxCell checkBoxCell = base[e.ColumnIndex, e.RowIndex] as FCDataGridViewCheckBoxCell;
            DataGridViewComboBoxColumn comboboxColumn = base.Columns[e.ColumnIndex] as DataGridViewComboBoxColumn;
            if (checkBoxCell != null)
            {
                // Fire CellValidating only if the event was fired from click event
                if (checkBoxCell.ValidateFireFromClick)
                {
                    base.OnCellValidating(e);
                }
            }
            //FC:FINAL:MSH - i.issue #1708: Tag in cell won't be changed automatically after editing is complete and before CellValueChanged (in original value from .TextMatrix will be changed after cell validating)
            else if (comboboxColumn != null)
            {
                base.OnCellValidating(e);
                //FC:FINAL:MSH - i.issue #1708: change Tag value if validating wasn't canceled
                if (!e.Cancel)
                {
                    List<string> items = (List<string>)comboboxColumn.DataSource;
                    string[] tab = new string[] { "\t" };
                    int comboValueIndex = this.GetComboValueIndex(comboboxColumn);

                    foreach (string item in items)
                    {
                        string text = FCListViewComboBox.ExtractText(item);
                        text = text.Split(tab, StringSplitOptions.None)[comboValueIndex];
                        if (text == e.FormattedValue.ToString())
                        {
                            string val = FCListViewComboBox.ExtractValue(item);
                            if (val.Contains("\t"))
                            {
                                val = val.Split(tab, StringSplitOptions.None)[comboValueIndex];
                            }
                            //FC:FINAL:MSH - issue #1748: replace wrong comparing and use correct indexes
                            if (base[e.ColumnIndex, e.RowIndex].UserData.ComboValue == null || base[e.ColumnIndex, e.RowIndex].UserData.ComboValue.ToString() != val)
							{
								base[e.ColumnIndex, e.RowIndex].UserData.ComboValue = val;
							}
							break;
                        }
                    }
                }
            }
            else
            {
                base.OnCellValidating(e);
                //FC:FINAL:DSE:#1567 If ValidateEdit event returns cancel, the editor should contain the last cell value
                if (e.Cancel)
                {
                    if (base.EditingControl is TextBox || base.EditingControl is MaskedTextBox)
                    {
                        this.EditText = Convert.ToString(this[e.ColumnIndex, e.RowIndex].Value);
                    }
                }
            }
            cellValidated = !e.Cancel;
            inCellValidating = false;
        }

        private void ComboBox_DropDown(object sender, EventArgs e)
        {
            if (this.ComboDropDown != null)
            {
                this.ComboDropDown(sender, e);
            }
        }

        private void ComboBox_DropDownClosed(object sender, EventArgs e)
        {
            if (this.ComboCloseUp != null)
            {
                this.ComboCloseUp(sender, e);
            }
        }

        private void ButtonCell_ButtonClick(object sender, EventArgs e)
        {
            if (this.CellButtonClick != null)
            {
                this.CellButtonClick(sender, e);
            }
        }

        //TODO - implement key events on client side
        //protected override void OnEditingControlShowing(DataGridViewEditingControlShowingEventArgs e)
        //{
        //    base.OnEditingControlShowing(e);
        //    if (e.Control != null)
        //    {
        //        e.Control.KeyDown -= new KeyEventHandler(Editor_KeyDown);
        //        e.Control.KeyPress -= new KeyPressEventHandler(Editor_KeyPress);
        //        e.Control.KeyUp -= new KeyEventHandler(Editor_KeyUp);

        //        e.Control.KeyDown += new KeyEventHandler(Editor_KeyDown);
        //        e.Control.KeyPress += new KeyPressEventHandler(Editor_KeyPress);
        //        e.Control.KeyUp += new KeyEventHandler(Editor_KeyUp);
        //    }
        //}

        private void Editor_KeyUp(object sender, KeyEventArgs e)
        {
            if (this.KeyUpEdit != null)
            {
                this.KeyUpEdit(sender, e);
            }
        }

        private void Editor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (this.KeyPressEdit != null)
            {
                this.KeyPressEdit(sender, e);
            }
        }

        private void Editor_KeyDown(object sender, KeyEventArgs e)
        {
            if (this.KeyDownEdit != null)
            {
                this.KeyDownEdit(sender, e);
            }
        }

        private void ComboBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                (sender as Control).Text = "";
            }
        }

        protected override void OnCellFormatting(DataGridViewCellFormattingEventArgs e)
        {
            //P2218:AM:#3687 - avoid stack overflow exceptions (it happens when calling flexcpTextDisplay in CellFormatting)
            if (inCellFormatting)
            {
                return;
            }
            else
            {
                inCellFormatting = true;
            }
            base.OnCellFormatting(e);
            inCellFormatting = false;

            if (e.ColumnIndex > -1 && e.RowIndex > -1)
            {
                bool formatComboItem = false;
                //FC:FINAL:DSE Use string with the format "*nnn;" to the first item (where nnn is the zero-based index of the column to be displayed) to display a different column
                int comboValueIndex = 0;

                if (base.Columns[e.ColumnIndex] is DataGridViewComboBoxColumn)
                {
                    formatComboItem = true;
                    comboValueIndex = this.GetComboValueIndex(base.Columns[e.ColumnIndex] as DataGridViewComboBoxColumn);
                }
                else if (base.Columns[e.ColumnIndex].Editor != null)
                {
                    FCListViewComboBox comboBox = base.Columns[e.ColumnIndex].Editor as FCListViewComboBox;
                    if (comboBox != null)
                    {
                        formatComboItem = true;
                    }
                }

                if (formatComboItem)
                {
					DataGridViewCell cell = base[e.ColumnIndex, e.RowIndex];
                    e.Value = GetComboCellValue(e.Value, comboValueIndex);
                }
                else if (!string.IsNullOrEmpty(e.CellStyle.Format))
                {
                    //FC:FINAL:SBE - Harris #i2464 - don't format the cell value when current cell is edited. use the cell value for editing instead of FormattedValue
                    if (this.IsCurrentCellInEditMode && this.CurrentCell.RowIndex == e.RowIndex && this.CurrentCell.ColumnIndex == e.ColumnIndex)
                    {
                        //no need to format the editing cell
                    }
                    else
                    {
                        e.Value = Strings.Format(e.Value, e.CellStyle.Format);
                    }
                }
                else
                {
                    //P2218:SBE:#3942 - convert DateTime to string, when CellStyle.Format is not set. DateTime values, with time set to 12:00:00 AM should display only the date. FCConvert.ToString() resolves this problem
                    if (e.Value is DateTime dt)
                    {
                        e.Value = FCConvert.ToString(dt);
                    }
                }
				
				//AM:HARRIS: don't set the alignment for numbers (only amounts should be right aligned)
                //set alignment right for numeric and left for strings
				//if (e.CellStyle.Alignment == DataGridViewContentAlignment.NotSet)
				//{				
				//	if (Information.IsNumeric(e.Value))
				//	{
    //                    //AM:Harris: #1319 - check first for numeric type because values like 10.8512 are recognized as dates
    //                    e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
				//	}
    //                //SGA: reverse the order of operands (IsDate should be last)
    //                else if (e.Value is Boolean || e.Value == null || base.Columns[e.ColumnIndex] is DataGridViewCheckBoxColumn || Information.IsDate(e.Value))
    //                {
    //                    //avoid wrong align
    //                }
    //                else
				//	{
				//		e.CellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
				//	}
				//}
			}
        }

        protected override void OnCellParsing(DataGridViewCellParsingEventArgs e)
        {
            base.OnCellParsing(e);

            //FC:FINAL:SBE - Harris #136 - Parse combo text when combo value is typed, and set apply parsing flag, to avoid execution of cell.ParseFormattedValue() method, which will throw exception
            if (this.Columns[e.ColumnIndex] is DataGridViewComboBoxColumn && e.Value != null)
            {
                DataGridViewComboBoxColumn comboCol = this.Columns[e.ColumnIndex] as DataGridViewComboBoxColumn;
                bool parsed = false;
                if (comboCol.DataSource != null && comboCol.DataSource.GetType() == typeof(List<string>))
                {
                    List<string> comboSource = comboCol.DataSource as List<string>;
                    foreach (var item in comboSource)
                    {
                        if (FCListViewComboBox.ExtractValue(item) == e.Value.ToString())
                        {
                            parsed = true;
                            break;
                        }
                    }
                }
                if (parsed)
                {
                    e.ParsingApplied = true;
                }
            }
        }

        //AM: select the correct item in the combobox when the editing control is showing
        protected override void OnEditingControlShowing(DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.TextChanged += new EventHandler(EditorTextChanged);
            //if EditMaxLength was set before entering in edit mode, we have to set it
            if (editMaxLength > 0)
            {
                this.EditMaxLength = editMaxLength;
            }
			//FC:FINAL:MSH - i.issue #1857: change CharacterCasing if EditingControl is Textbox
			if (e.Control != null && (e.Control as TextBox) != null)
			{
				(e.Control as TextBox).CharacterCasing = this.CellCharacterCasing;
			}
			base.OnEditingControlShowing(e);
            FCListViewComboBox comboCol = e.Control as FCListViewComboBox;
            if (this.CurrentCell.Value != null)
            {
                if (comboCol != null && comboCol.DataSource != null && comboCol.DataSource.GetType() == typeof(List<string>))
                {
                    List<string> comboSource = comboCol.DataSource as List<string>;
                    bool itemFound = false;
                    string cellValue = GetComboCellValue(this.CurrentCell);
                    //search first in cell UserData
                    if (this.CurrentCell.UserData.ComboValue != null)
                    {
                        for (int i = 0; i < comboSource.Count; i++)
                        {
                            if (Convert.ToString(this.CurrentCell.UserData.ComboValue) == GetComboCellValue(FCListViewComboBox.ExtractValue(comboSource[i])))
                            {
                                comboCol.SelectedIndex = i;
                                itemFound = true;
                                break;
                            }
                        }
                    }
                    //FC:FINAL:SBE - Harris - #2556 - search first for combobox text
                    if (!itemFound)
                    {
                        string[] tab = new string[] { "\t" };
                        for (int i = 0; i < comboSource.Count; i++)
                        {
                            if (GetComboCellValue(FCListViewComboBox.ExtractText(comboSource[i])).Split(tab, StringSplitOptions.None).First() == cellValue)
                            {
                                comboCol.SelectedIndex = i;
                                itemFound = true;
                                break;
                            }
                        }
                    }
                    if (!itemFound)
                    {
                        for (int i = 0; i < comboSource.Count; i++)
                        {
                            //FC:FINAL:MSH - i.issue #1025: first compare with value because in original if we have in Grid combobox
                            // than will be returned 'Value'
                            if (GetComboCellValue(FCListViewComboBox.ExtractValue(comboSource[i])) == cellValue)
                            {
                                comboCol.SelectedIndex = i;
                                itemFound = true;
                                break;
                            }
                        }
                    }
                    //FC:FINAL:SBE - Harris - #421 - if item is not found, combo text is empty
                    if (!itemFound)
                    {
                        comboCol.Text = "";
                    }
                }
            }
        }

        private void EditorTextChanged(object sender, EventArgs e)
        {
            this.OnChangeEdit();
        }

        //AM:HARRIS:#1988 - OnWebDataRead is called during sorting; we need to restore the column index because after sorting the column changes to 0
        protected override object OnWebDataRead(int firstIndex, int lastIndex, ListSortDirection sortDirection, int sortIndex)
        {
            object ret = base.OnWebDataRead(firstIndex, lastIndex, sortDirection, sortIndex);
            if (IsValidColumn(sortIndex))
            {
                this.Col = this.GetFlexColIndex(sortIndex);
                DataGridViewCell cell = GetFirstVisibleCell();
                if (cell != null)
                {
                    this.Select(this.GetFlexRowIndex(cell.RowIndex), this.Col);
                }
            }
            return ret;
        }
        #endregion

        #region Private Methods

        private void SetSortModeOnColumns()
        {
            // CHE: by default, column SortMode is set to Automatic and when SelectionMode is ColumnHeaderSelect then exception is thrown:
            // "Column's SortMode cannot be set to Automatic while the DataGridView control's SelectionMode is set to ColumnHeaderSelect."

            foreach (DataGridViewColumn column in base.Columns)
            {
                column.SortMode = DataGridViewColumnSortMode.Programmatic;
            }
        }

        private void ExtendLastVisibleColumn()
        {
            for (int i = base.Columns.Count - 1; i >= 0; i--)
            {
                if (base.Columns[i].Visible)
                {
                    base.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    for (int j = 0; j < base.Columns.Count; j++)
                    {
                        if (j != i)
                        {
                            base.Columns[j].AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
                        }
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// Disable AutoSize for all columns
        /// </summary>
        private void DisableExtendedModeForColumns()
        {
            for (int j = 0; j < base.Columns.Count; j++)
            {
                base.Columns[j].AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
            }
        }

        private bool ArrowKeyPressed(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right)
            {
                return true;
            }
            return false;
        }

        private void CheckPosition()
        {
			//CHE: if Row or Col was set to 0 then CurrentCell was not changed (could not be set on rowheader or column header) 
			// and when you double click the first valid cell then CurrentCellChanged is not raised like in VB6 (RowColChange), explicitly call it
			//DDU:HARRIS:#i2409 - check if grid was not disposed already
			if (this.IsDisposed)
			{
				return;
			}
			if (this.CurrentCell != null)
            {
                int gridRowIndex = this.GetDGVRowIndex(m_currentRow);
                int gridColIndex = this.GetDGVColIndex(m_currentColumn);
                if (this.CurrentCell.RowIndex != gridRowIndex || this.CurrentCell.ColumnIndex != gridColIndex)
                {
                    inRowColSetter = true;
                    m_currentRow = this.GetFlexRowIndex(this.CurrentCell.RowIndex);
                    m_currentColumn = this.GetFlexColIndex(this.CurrentCell.ColumnIndex);
                    base.OnCurrentCellChanged(EventArgs.Empty);
                    inRowColSetter = false;
                }
            }
            //AM: when clicking on the row header CurrentCell is null
            else if(this.CurrentRow != null)
            {
                m_currentRow = this.GetFlexRowIndex(this.CurrentRow.Index);
                m_currentColumn = 0;
            }
        }

        public CollapsedSettings IsCollapsed(int lngGroupRow)
        {
            int dgvRow = GetDGVRowIndex(lngGroupRow);
            if (IsValidRow(dgvRow) && base.Rows[dgvRow].IsParent)
            {
                return base.Rows[dgvRow].IsExpanded ? CollapsedSettings.flexOutlineExpanded : CollapsedSettings.flexOutlineCollapsed;
            }
            return CollapsedSettings.flexOutlineExpanded;
        }

        public void IsCollapsed(int lngGroupRow, CollapsedSettings setting)
        {
            int dgvRow = GetDGVRowIndex(lngGroupRow);
            if (IsValidRow(dgvRow))
            {
                if (setting == CollapsedSettings.flexOutlineCollapsed)
                {
                    //FC:FINAL:SBE - Harris #225 - collapse row, only if it is not collapsed yet
                    if (base.Rows[dgvRow].IsExpanded)
                    {
                        base.Rows[dgvRow].Collapse();
                    }
                }
                else if (setting == CollapsedSettings.flexOutlineExpanded)
                {
                    //FC:FINAL:SBE - Harris #225 - expand row, only if it is not expanded yet
                    if (!base.Rows[dgvRow].IsExpanded)
                    {
                        base.Rows[dgvRow].Expand();
                    }
                }
            }
        }

        private void FCGrid_CellPaint(object sender, DataGridViewCellPaintEventArgs e)
        {
            DataGridViewCell cell = this[e.ColumnIndex, e.RowIndex];
            if(cell == null)
            {
                return;
            }
            //set general alignment: left for strings and right for numbers
            if (this.ColAlignment(GetFlexColIndex(e.ColumnIndex)) == AlignmentSettings.flexAlignGeneral)
            {
				if (Information.IsNumeric(cell.Value))
                {
                    cell.Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                }
                else
                {
                    cell.Style.Alignment = DataGridViewContentAlignment.MiddleLeft;
                }
            }

            if (this.DesignMode || this.CurrentCell == null)
            {
                return;
            }

            // TODO
            //if (this.CurrentCell.ColumnIndex == e.ColumnIndex && this.CurrentCell.RowIndex == e.RowIndex)
            //{
            //    cell.Style.SelectionBackColor = cell.Style.BackColor;
            //    cell.Style.SelectionForeColor = cell.Style.ForeColor;

            //    Rectangle newRect = new Rectangle(e.CellBounds.X + 1, e.CellBounds.Y + 1, e.CellBounds.Width - 4, e.CellBounds.Height - 4);

            //    switch (this.FocusRect)
            //    {
            //        case FocusRectSettings.flexFocusNone:
            //            {
            //                // default DGV behaviour
            //                break;
            //            }
            //        case FocusRectSettings.flexFocusLight:
            //            {
            //                DrawCellDashBorder(e, 1);
            //                break;
            //            }
            //        case FocusRectSettings.flexFocusHeavy:
            //            {
            //                DrawCellDashBorder(e, 3);
            //                break;
            //            }
            //    }
            }

        //    //CHE: do not color selection on rowheader
        //    if (e.ColumnIndex == -1)
        //    {
        //        e.CellStyle.SelectionBackColor = e.CellStyle.BackColor;
        //        e.CellStyle.SelectionForeColor = e.CellStyle.ForeColor;
        //    }
        //}

            // TODO
            //private void DrawCellDashBorder(DataGridViewCellPaintingEventArgs e, int width)
            //{
            //    //CHE: draw current cell "border" only if grid has focus
            //    if (!this.ContainsFocus)
            //    {
            //        return;
            //    }
            //    e.Paint(e.CellBounds, DataGridViewPaintParts.All & ~DataGridViewPaintParts.Border);
            //    using (Pen p = new Pen(Color.Gray, width))
            //    {
            //        p.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            //        Rectangle rect = e.CellBounds;
            //        rect.Width -= 2;
            //        rect.Height -= 2;
            //        e.Graphics.DrawRectangle(p, rect);
            //    }
            //    e.Handled = true;
            //}

        private void FCGrid_MouseUp(object sender, MouseEventArgs e)
		{
			//DDU:HARRIS:#i2409 - check if grid was not disposed already
			if (this.IsDisposed)
			{
				return;
			}
            if (this.CurrentCell != null)
            {
                this.RowSel = this.GetFlexRowIndex(this.CurrentCell.RowIndex);
                this.ColSel = this.GetFlexColIndex(this.CurrentCell.ColumnIndex);
                //CHE: when selecting entire column RowSel is set to last row and Row to first line
                if (this.Columns[this.CurrentCell.ColumnIndex].Selected)
                {
                    this.Row = this.FixedRows;
                    this.RowSel = this.GetFlexRowIndex(this.RowCount - 1);
                }
                //AM:HARRIS:#3999 - when selecting a row ColSel is set to the last column
                if (this.CurrentRow.Selected)
                {
                    this.ColSel = this.GetFlexColIndex(this.ColumnCount - 1);
                }
            }
            //FC:FINAL:RPU: #1403 - Set RowSel and ColSel also when clicking on the row header and the CurrentCell is null
            else if (this.CurrentRow != null)
            {
                this.RowSel = this.GetFlexRowIndex(this.CurrentRow.Index);
                this.ColSel = 0;
                //FC:FINAL:SBE - Harris #i2356 - iterate on Rows can throw exception if form is closed on grid cell/row double click. 
                //Client requests are processed asynchronously by Wisej. When MouseUp execution takes longer than double click, and form will be closed, exception is thrown during foreach
                this.Row = this.RowSel;
                //foreach(DataGridViewRow row in base.Rows)
                //{
                //    if(row.Selected)
                //    {
                //        this.Row = GetFlexRowIndex(row.Index);
                //        break;
                //    }
                //}
            }
        }

        private void FCGrid_CurrentCellChanged(object sender, EventArgs e)
        {
			//DDU:HARRIS:#i2409 - check if grid was not disposed already
			if (this.IsDisposed)
			{
				return;
			}
			if (this.CurrentCell != null)
            {
                int colIndex = this.CurrentCell.ColumnIndex;

                m_currentCellChanged = true;

                if (!inRowColSetter)
                {
                    fromCurrentCellChanged = true;

                    this.Row = this.GetFlexRowIndex(this.CurrentCell.RowIndex);
                    this.Col = this.GetFlexColIndex(colIndex);

                    fromCurrentCellChanged = false;
                }
            }
            //AM: when clicking on the row header CurrentCell is null
            else if (this.CurrentRow != null)
            {
                m_currentRow = this.GetFlexRowIndex(this.CurrentRow.Index);
                m_currentColumn = 0;
            }
        }

		private void FCGrid_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            if (e.Column.ValueType == typeof(DateTime))
            {
                DateTime dt1;
                DateTime dt2;

				//FC:FINAL:DDU:#1739 - fixed datetime null column comparison
				if (String.IsNullOrEmpty(FCConvert.ToString(e.CellValue1)))
				{
					dt1 = DateTime.FromOADate(0);
				}
				else
				{
					dt1 = DateTime.Parse(e.CellValue1.ToString());
				}
				if (String.IsNullOrEmpty(FCConvert.ToString(e.CellValue2)))
				{
					dt2 = DateTime.FromOADate(0);
				}
				else
				{
					dt2 = DateTime.Parse(e.CellValue2.ToString());
				}

                e.SortResult = dt1.CompareTo(dt2);

                e.Handled = true;
            }
            //FC:FINAL:MSH - i.issue #1344: implement comparing for correct sorting and calculation table values
            else if (e.Column.ValueType == typeof(double))
            {
                double x = Convert.ToDouble(e.CellValue1);
                double y = Convert.ToDouble(e.CellValue2);

                e.SortResult = x.CompareTo(y);
                e.Handled = true;
            }
            else if (e.Column.ValueType == typeof(long))
            {
                long x = Convert.ToInt64(e.CellValue1);
                long y = Convert.ToInt64(e.CellValue2);

                e.SortResult = x.CompareTo(y);
                e.Handled = true;
            }
			else if (e.Column.ValueType == typeof(int))
			{
				int x = Convert.ToInt32(e.CellValue1);
				int y = Convert.ToInt32(e.CellValue2);

				e.SortResult = x.CompareTo(y);
				e.Handled = true;
			}
			else if (e.Column.ValueType == typeof(string))
            {
                string x = Convert.ToString(e.CellValue1);
                string y = Convert.ToString(e.CellValue2);

                int x1 = 0;
                int y1 = 0;
                //AM:HARRIS:#2098 - for strings that are numbers use int comparison
                if (Int32.TryParse(x, out x1) && Int32.TryParse(y, out y1))
                {
                    e.SortResult = x1.CompareTo(y1);
                }
                else
                {
                    e.SortResult = x.CompareTo(y);
                }
                e.Handled = true;
            }
        }

        private void FCGrid_DragDrop(object sender, DragEventArgs e)
        {
            var grid = (DataGridView)sender;
            var dropRow = GetDragRow();
            var dragRow = e.Data.GetData(typeof(DataGridViewRow)) as DataGridViewRow;

            if (dropRow != dragRow && dragRow != null)
            {
                disableEvents = true;

                if (dropRow == null)
                {
                    grid.Rows.Remove(dragRow);
                    grid.Rows.Add(dragRow);
                }
                else
                {
                    var dropIndex = dropRow.Index;
                    grid.Rows.Remove(dragRow);
                    grid.Rows.Insert(dropIndex, dragRow);
                }

                disableEvents = false;

                if (AfterMoveRow != null)
                {
                    AfterMoveRowEventArgs args = new AfterMoveRowEventArgs();
                    args.Row = GetFlexRowIndex(dragRow.Index);
                    args.Position = GetFlexRowIndex(dropRow.Index);
                    AfterMoveRow(this, args);
                }
            }
        }

        private void FCGrid_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void FCGrid_DragStart(object sender, EventArgs e)
        {
            var dragRow = GetDragRow();
            if (dragRow != null)
            {
                this.DoDragDrop(dragRow, DragDropEffects.Move);
            }
        }

        private DataGridViewRow GetDragRow()
        {
            var position = this.PointToClient(Control.MousePosition);
            var clientArea = this.Parent as Panel;
            if (clientArea != null)
            {
                //TEMP: Wisej issue - vertical scrolling is not calculated for PointToClient
                position.Offset(clientArea.HorizontalScroll.Value, clientArea.VerticalScroll.Value);
            }
            var ht = this.HitTest(position);
            return ht.Row;
        }

        private void FCGrid_ColumnHeadersHeightChanged(object sender, EventArgs e)
        {
            this.AutoSizeGridHeight();
            for (int dgvIndex = 0; dgvIndex < this.Columns.Count; dgvIndex ++)
            {
                if (AlignExpandButton(dgvIndex)) break;
            }
        }

        private void Rows_CollectionChanged(object sender, CollectionChangeEventArgs e)
        {
            this.AutoSizeGridHeight();
        }

        private void AutoSizeGridHeight()
        {
            if (this.autoSizeOnRowChanging)
            {
                int height = 0;
                if (ColumnHeadersVisible)
                {
                    height += this.ColumnHeadersHeight;
                }
                //rows height
                foreach (var row in base.Rows)
                {
                    height += row.Height;
                }
                //add datagridview border
                height += 2;
                this.Height = height;
            }
        }

        private void SetSelectionColors(DataGridViewCellStyle cellStyle)
        {
            switch (this.HighLight)
            {
                // TODO
                //case HighLightSettings.flexHighlightNever:
                //    cellStyle.SelectionBackColor = cellStyle.BackColor;
                //    cellStyle.SelectionForeColor = InvertAColor(cellStyle.BackColor);
                //    break;
                //case HighLightSettings.flexHighlightAlways:
                //case HighLightSettings.flexHighlightWithFocus:
                //    cellStyle.SelectionBackColor = SystemColors.Highlight;
                //    cellStyle.SelectionForeColor = SystemColors.HighlightText;
                //    break;
            }
        }

        //private void FCGrid_MouseDown(object sender, MouseEventArgs e)
        //{
        //    if (this.AllowBigSelection && this.SelectionMode == SelectionModeSettings.flexSelectionFree)
        //    {
        //        Wisej.Web.DataGridView.HitTestInfo hti = this.HitTest(e.X, e.Y);

        //        if (hti.ColumnIndex == -1 && hti.RowIndex >= 0)
        //        {
        //            // row header click
        //            if (base.SelectionMode != DataGridViewSelectionMode.RowHeaderSelect)
        //            {
        //                base.SelectionMode = DataGridViewSelectionMode.RowHeaderSelect;
        //                //CHE: set SelectionBackColor and SelectionForeColor
        //                SetSelectionColors(this.RowHeadersDefaultCellStyle);
        //            }
        //        }
        //        else if (hti.RowIndex == -1 && hti.ColumnIndex >= 0)
        //        {
        //            // column header click
        //            if (base.SelectionMode != DataGridViewSelectionMode.ColumnHeaderSelect)
        //            {
        //                SetSortModeOnColumns();
        //                base.SelectionMode = DataGridViewSelectionMode.ColumnHeaderSelect;
        //                //CHE: set SelectionBackColor and SelectionForeColor
        //                SetSelectionColors(this.ColumnHeadersDefaultCellStyle);
        //            }
        //        }
        //    }
        //}

        private void RecalculateCols()
        {
            //CHE: for columns added in designer we need to adjust Cols property too
            if (this.Parent == null)
            {
                int realCols = base.ColumnCount + (this.FixedCols > 0 ? 1 : 0);
                if (m_cols != realCols)
                {
                    m_cols = realCols;
                }
            }
        }

        private bool IsFormInitializing()
        {
            FCForm form = this.FindForm() as FCForm;
            if (form != null && form.IsInitializing)
            {
                return true;
            }
            return false;
        }

        private DataGridViewContentAlignment FixAlignmentSettings(AlignmentSettings settings)
        {
            DataGridViewContentAlignment newSettings = DataGridViewContentAlignment.NotSet;
            switch (settings)
            {
                case AlignmentSettings.flexAlignLeftTop:
                    newSettings = DataGridViewContentAlignment.TopLeft;
                    break;
                case AlignmentSettings.flexAlignLeftCenter:
                    newSettings = DataGridViewContentAlignment.MiddleLeft;
                    break;
                case AlignmentSettings.flexAlignLeftBottom:
                    newSettings = DataGridViewContentAlignment.BottomLeft;
                    break;
                case AlignmentSettings.flexAlignCenterTop:
                    newSettings = DataGridViewContentAlignment.TopCenter;
                    break;
                case AlignmentSettings.flexAlignCenterCenter:
                    newSettings = DataGridViewContentAlignment.MiddleCenter;
                    break;
                case AlignmentSettings.flexAlignCenterBottom:
                    newSettings = DataGridViewContentAlignment.BottomCenter;
                    break;
                case AlignmentSettings.flexAlignRightTop:
                    newSettings = DataGridViewContentAlignment.TopRight;
                    break;
                case AlignmentSettings.flexAlignRightCenter:
                    newSettings = DataGridViewContentAlignment.MiddleRight;
                    break;
                case AlignmentSettings.flexAlignRightBottom:
                    newSettings = DataGridViewContentAlignment.BottomRight;
                    break;
                case AlignmentSettings.flexAlignGeneral:
                    newSettings = DataGridViewContentAlignment.NotSet;
                    break;
            }

            return newSettings;
        }

        private AlignmentSettings FixDataGridViewContentAlignment(DataGridViewContentAlignment settings)
        {
            AlignmentSettings newSettings = AlignmentSettings.flexAlignGeneral;
            switch (settings)
            {
                case DataGridViewContentAlignment.BottomCenter:
                    newSettings = AlignmentSettings.flexAlignCenterBottom;
                    break;
                case DataGridViewContentAlignment.BottomLeft:
                    newSettings = AlignmentSettings.flexAlignLeftBottom;
                    break;
                case DataGridViewContentAlignment.BottomRight:
                    newSettings = AlignmentSettings.flexAlignRightBottom;
                    break;
                case DataGridViewContentAlignment.MiddleCenter:
                    newSettings = AlignmentSettings.flexAlignCenterCenter;
                    break;
                case DataGridViewContentAlignment.MiddleLeft:
                    newSettings = AlignmentSettings.flexAlignLeftCenter;
                    break;
                case DataGridViewContentAlignment.MiddleRight:
                    newSettings = AlignmentSettings.flexAlignRightCenter;
                    break;
                case DataGridViewContentAlignment.NotSet:
                    newSettings = AlignmentSettings.flexAlignGeneral;
                    break;
                case DataGridViewContentAlignment.TopCenter:
                    newSettings = AlignmentSettings.flexAlignCenterTop;
                    break;
                case DataGridViewContentAlignment.TopLeft:
                    newSettings = AlignmentSettings.flexAlignLeftTop;
                    break;
                case DataGridViewContentAlignment.TopRight:
                    newSettings = AlignmentSettings.flexAlignRightTop;
                    break;
            }
            return newSettings;
        }

        private DataGridViewRowCollection GetFixedRows()
        {
            DataGridViewRowCollection fixedRows = new DataGridViewRowCollection(this);
            foreach (DataGridViewRow rowItem in base.Rows)
            {
                if (rowItem.Frozen)
                {
                    fixedRows.Add(rowItem);
                }
            }
            return fixedRows;
        }

        private void ResetForeColorFixed()
        {
            for (int i = 0; i < this.GetDGVColIndex(this.FixedCols); i++)
            {
                //if (i >= this.Columns.Count)
                //{
                //    break;
                //}
                this.Columns[i].DefaultCellStyle.ForeColor = m_foreColorFixedBackup;

                //also reset BackColor for FrozenColumns
                //this.Columns[i].DefaultCellStyle.BackColor = this.DefaultCellStyle.BackColor;
            }

            for (int j = 0; j < this.GetDGVRowIndex(this.FixedRows); j++)
            {
                //if (j >= base.Rows.Count)
                //{
                //    break;
                //}
                base.Rows[j].DefaultCellStyle.ForeColor = m_foreColorFixedBackup;

                //also reset BackColor for FrozenRows
                //this.Rows[j].DefaultCellStyle.BackColor = this.DefaultCellStyle.BackColor;
            }
        }

        private Color InvertAColor(Color ColorToInvert)
        {
            return Color.FromArgb((byte)~ColorToInvert.R, (byte)~ColorToInvert.G, (byte)~ColorToInvert.B);
        }

        /// <summary>
        /// Returns DGV Index.
        /// </summary>
        /// <param name="flexIndex"></param>
        /// <returns></returns>
        private int GetDGVColIndex(int flexIndex)
        {
            if (!HasColumns)
            {
                return -1;
            }
            if (FixedCols == 0)
            {
                return flexIndex;
            }
            return --flexIndex;
        }

        /// <summary>
        /// Returns DGV Index.
        /// </summary>
        /// <param name="flexIndex"></param>
        /// <returns></returns>
        private int GetDGVRowIndex(int flexIndex)
        {
            if (!HasRows)
            {
                return -1;
            }
            if (FixedRows == 0)
            {
                return flexIndex;
            }
            return flexIndex - this.FixedRows;
        }

        /// <summary>
        /// Returns Flex Index.
        /// </summary>
        /// <param name="dgvIndex"></param>
        /// <returns></returns>
        public int GetFlexColIndex(int dgvIndex)
        {
            if (!HasColumns)
            {
                return -1;
            }
            if (FixedCols == 0)
            {
                return dgvIndex;
            }
            return ++dgvIndex;
        }

        /// <summary>
        /// Returns Flex Index.
        /// </summary>
        /// <param name="dgvIndex"></param>
        /// <returns></returns>
        public int GetFlexRowIndex(int dgvIndex)
        {
            if (!HasRows)
            {
                return -1;
            }
            if (FixedRows == 0)
            {
                return dgvIndex;
            }
            return dgvIndex + this.FixedRows;
        }

        private DataGridViewCell GetFirstVisibleCell()
        {
            //get first visible cell
            for (int i = 0; i < base.Rows.Count; i++)
            {
                for (int j = 0; j < base.Columns.Count; j++)
                {
                    if (base.Rows[i].Cells[j].Visible)
                    {
                        return base.Rows[i].Cells[j];
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Check if the column is valid
        /// The index is for DataGridView
        /// </summary>
        /// <param name="col"></param>
        /// <returns></returns>
        private bool IsColValid(int col)
        {
            return col >= 0 && col < this.Columns.Count;
        }

        /// <summary>
        /// Check if the row is valid
        /// The index is for DataGridView
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private bool IsRowValid(int row)
        {
            return row >= 0 && row < base.Rows.Count;
        }

        #endregion

        #region Wisej missing methods

        //RPU
        /// <summary>
        /// Returns a list with all the selected columns from Grid
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public DataGridViewColumnCollection SelectedColumns
        {
            get
            {
                DataGridViewColumnCollection selectedCols = new DataGridViewColumnCollection(this);
                foreach (DataGridViewColumn col in base.Columns)
                {
                    if (col.Selected)
                    {
                        selectedCols.Add(col);
                    }
                }
                return selectedCols;
            }
        }
        public Color GridColor { get; set; }
        //RPU:
        public AutoSizeSettings AutoSizeMode
        {

            get
            {
                return this.autoSizeMode;
            }
            set
            {
                autoSizeMode = value;
            }
        }
        //RPU:Returns or sets the contents of a range.
        /// <summary>
        /// The string assigned to the Clip property may contain the contents of multiple rows and columns. Tab characters (Tab) indicate column breaks, and carriage return characters ('\r') indicate row breaks.
        ///The default row and column delimiters may be changed using the ClipSeparators property.
        ///When a string is assigned to the Clip property, only the selected cells are affected. If there are more cells in the selected region than are described in the clip string, the remaining cells are ignored.If there are more cells described in the clip string than in the selected region, the extraneous portion of the clip string is ignored.Empty entries in the Clip string will clear existing cell contents.
        /// </summary>
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Clip
        {
            get
            {
                string clip = "";
                for (int i = 0; i < base.Rows.Count; i++)
                {
                    for (int j = 0; j < base.Columns.Count; j++)
                    {
                        if (base[j, i].Selected)
                        {
                            clip += "\"" + base[j, i].Value + "\"" + " Tab ";
                        }
                    }
                    if (clip.Contains(" Tab "))
                    {
                        clip = clip.Remove(clip.LastIndexOf(" Tab "));
                    }
                    clip += " \r";
                }               
                return clip;
            }
            set
            {
                string[] rows = value.Split(new string[] { "\r" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < base.Rows.Count && i < rows.Length; i++)
                {
                    string[] columns = rows[i].Split(new string[] { "Tab" }, StringSplitOptions.RemoveEmptyEntries);
                    for (int j = 0; j < base.Columns.Count && j < columns.Length; j++)
                    {
                        base[j, i].Value = columns[j].Replace("\"", "").Trim();
                    }
                }
            }
        }

        [DefaultValue(ExplorerBarSettings.flexExNone)]
        public ExplorerBarSettings ExplorerBar
        {
            get
            {
                return this.explorerBar;
            }
            set
            {
                if (value != this.explorerBar)
                {
                    this.explorerBar = value;
                    DataGridViewColumnSortMode sortMode = (this.explorerBar == ExplorerBarSettings.flexExSort || this.explorerBar == ExplorerBarSettings.flexExSortAndMove ||
                        this.explorerBar == ExplorerBarSettings.flexExSortShow || this.explorerBar == ExplorerBarSettings.flexExSortShowAndMove) ?
                            DataGridViewColumnSortMode.Automatic :
                            DataGridViewColumnSortMode.Programmatic;
                    foreach (DataGridViewColumn column in this.Columns)
                    {
                        column.SortMode = sortMode;
                    }

                    if (explorerBar == ExplorerBarSettings.flexExMoveRows)
                    {
                        this.AllowDrag = true;
                        this.AllowDrop = true;

                        this.DragStart -= FCGrid_DragStart;
                        this.DragEnter -= FCGrid_DragEnter;
                        this.DragDrop -= FCGrid_DragDrop;

                        this.DragStart += FCGrid_DragStart;
                        this.DragEnter += FCGrid_DragEnter;
                        this.DragDrop += FCGrid_DragDrop;
                    }
                    else if (explorerBar == ExplorerBarSettings.flexExMove)
                    {
                        this.AllowUserToOrderColumns = true;
                    }
                }
            }
        }

        // FC:FINAL:VGE - #637 TextMatrix version added. It's using Textmatrix instead of base DataGridView values.
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string TextMatrixClip
        {
            get
            {
                string clip = "";
                for (int i = 1; i < this.Rows; i++)
                {
                    for (int j = 1; j < this.Cols; j++)
                    {
                        clip += "\"" + this.TextMatrix(i, j) + "\"" + " Tab ";
                    }
                    if (clip.Contains(" Tab "))
                    {
                        clip = clip.Remove(clip.LastIndexOf(" Tab "));
                    }
                    clip += " \r";
                }
                return clip;
            }
            set
            {
                string[] rows = value.Split(new string[] { "\r" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 1; i < this.Rows && (i-1) < rows.Length; i++)
                {
                    string[] columns = rows[i-1].Split(new string[] { "Tab" }, StringSplitOptions.RemoveEmptyEntries);
                    for (int j = 1; j < this.Cols && (j-1) < columns.Length; j++)
                    {
                        this.TextMatrix(i, j, columns[j - 1].Replace("\"", "").Trim());
                    }
                }
            }
        }

        /*[DefaultValue(ExplorerBarSettings.flexExNone)]
        public ExplorerBarSettings ExplorerBar
        {
            get
            {
                return this.explorerBar;
            }
            set
            {
                if (value != this.explorerBar)
                {
                    this.explorerBar = value;
                    DataGridViewColumnSortMode sortMode = (this.explorerBar == ExplorerBarSettings.flexExSort || this.explorerBar == ExplorerBarSettings.flexExSortAndMove ||
                        this.explorerBar == ExplorerBarSettings.flexExSortShow || this.explorerBar == ExplorerBarSettings.flexExSortShowAndMove) ?
                            DataGridViewColumnSortMode.Automatic :
                            DataGridViewColumnSortMode.Programmatic;
                    foreach (DataGridViewColumn column in this.Columns)
                    {
                        column.SortMode = sortMode;
                    }
                }
            }
        }*/

        //FC:TODO:AM
            [DefaultValue(OutlineBarSettings.flexOutlineBarNone)]
        public OutlineBarSettings OutlineBar { get; set; }

        //RPU:
        /// <summary>
        /// Returns or sets the number of frozen (editable but non-scrollable) columns.
        /// </summary>
        public int FrozenCols
        {
            get
            {
                int count = 0;
                foreach (DataGridViewColumn column in base.Columns)
                {
                    if (column.Frozen)
                    {
                        count++;
                    }
                }
                return count;
            }
            set
            {
                for (int i = 0; i < base.Columns.Count; i++)
                {
                    base.Columns[i].Frozen = (i < value) && base.Columns[i].Visible;
                }
            }
		}

        //RPU:
        /// <summary>
        /// Returns or sets the minimum row height, in twips
        /// </summary>
        public int RowHeightMin
        {
            get
            {
                return rowHeightMin;
            }
            set
            {

                rowHeightMin = value;
                foreach (DataGridViewRow row in base.Rows)
                {
                    //using (Graphics g = this.CreateGraphics())
                    {
                        fcGraphics.InitializeCoordinateSpace(this.Bounds, 0, 0);
                        //FC:FINAL:MSH - issue #908: minimum height can't be smaller than 2
                        var rowMinHeight = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                        row.MinimumHeight = rowMinHeight >= 2 ? rowMinHeight : 2;
                    }
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ComboIndex
        {
            get
            {
                FCListViewComboBox combobox = base.EditingControl as FCListViewComboBox;
                if (combobox != null && combobox.SelectedIndex != -1)
                {
                    return combobox.SelectedIndex;
                }
                return -1;
            }
            set
            {
                FCListViewComboBox combobox = base.EditingControl as FCListViewComboBox;
                if (combobox != null)
                {
                    combobox.SelectedIndex = value;
                }
            }
        }

        public string ComboText
        {
            get
            {
                FCListViewComboBox combobox = base.EditingControl as FCListViewComboBox;
                if (combobox != null && combobox.SelectedItem != null)
                {
                    return (combobox.SelectedItem as ListViewItem).SubItems[1].Text;
                }
                return "";
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int FrozenRows
        {
            get
            {
                return m_frozenRows;
            }
            set
            {
                m_frozenRows = value;
                for (int i = 0; i < this.GetDGVRowIndex(value); i++)
                {
                    base.Rows[i].Frozen = true;
                }
            }
        }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int RowHeightMax { get; set; }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ScrollTrack { get; set; }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Color SheetBorder { get; set; }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        //public int ComboCount { get; set; }
        // FC:FINAL:VGE - #830 ComboCount is never set and is always 0. Replaced with pure getter.
        public int ComboCount { get { return this.comboListItems?.Count ?? 0; } }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public AlignmentSettings CellAlignment { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int BottomRow { get; set; }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int RightCol { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Color ForeColorSel { get; set; }
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public object GridLines { get; set; }
        [DefaultValue(false)]
        public bool SuppressExpandCollapseEventsOnExpandCollapseAll { get; set; }

        //RPU:
        /// <summary>
        /// 
        /// </summary>
        /// <param name="column1"></param>
        /// <param name="column2"></param>
        /// <param name="equal"></param>
        /// <param name="extraSpace"></param>
        public void AutoSize(int column1, int column2 = -1, bool equal = false, float extraSpace = 0)
        {
            if (column2 == -1 && base.Columns.Count > 0)
            {
                column2 = column1;
            }

            if (AutoSizeMode == AutoSizeSettings.flexAutoSizeColWidth)
            {
                column1 = GetFlexColIndex(column1);
                column2 = GetFlexColIndex(column2);
                for (int i = column1; i <= column2; i++)
                {
                    if (IsValidColumn(i))
                    {
                        //AM:HARRIS:#3478 - AutoResizeColumn is not working well
                        //base.AutoResizeColumn(i, DataGridViewAutoSizeColumnMode.AllCells);
                        base.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                    }
                }
            }
            else
            {
                column1 = GetFlexRowIndex(column1);
                column2 = GetFlexRowIndex(column2);
                for (int i = column1; i < column2; i++)
                {
                    if (IsValidRow(i))
                    {
                        base.AutoResizeRow(i, DataGridViewAutoSizeRowMode.AllCells);
                    }
                }
            }
        }

        private Rectangle GetColumnDisplayRectangle(int colIndex, bool p)
        {
            throw new NotImplementedException();

        }

        private Rectangle GetCellDisplayRectangle(int col, int row, bool p3)
        {
            return GetCellDisplayRectangle(base[col, row]);
        }

        private Rectangle GetCellDisplayRectangle(DataGridViewCell cell)
        {
            DataGridViewColumn col = cell.OwningColumn;
            DataGridViewRow row = cell.OwningRow;
            if (row != null)
            {
                int x = 0;
                for (int i = 0; i < col.Index; i++)
                {
                    if (base.Columns[i].Visible)
                    {
                        x += base.Columns[i].Width;
                    }
                }
                if (base.RowHeadersVisible)
                {
                    x += base.RowHeadersWidth;
                }
                int y = 0;
                for (int i = 0; i < row.Index; i++)
                {
                    if (base.Rows[i].Visible)
                    {
                        y += base.Rows[i].Height;
                    }
                }
                if (base.ColumnHeadersVisible)
                {
                    y += base.ColumnHeadersHeight;
                }
                return new Rectangle(x, y, col.Width, row.Height);
            }
            else
            {
                return Rectangle.Empty;
            }
        }

        //RPU:
        /// <summary>
        /// Returns the list that is used as a drop-down on the specified column.
        /// </summary>
        /// <param name="col">The Col parameter should be set to a value between zero and Cols - 1 to return the ColComboList of a given column</param>
        /// <returns></returns>
        public string ColComboList(int col)
        {
            int dgvCol = this.GetDGVColIndex(col);
            if (dgvCol >= 0)
            {
                return base.Columns[dgvCol].UserData.ComboList;
            }
            else
            {
                return string.Empty;
            }
            //string toReturn = "";
            //try
            //{
            //    DataGridViewComboBoxColumn column = (base.Columns[col] as DataGridViewComboBoxColumn);
            //    List<DataSourceItem> items = (List<DataSourceItem>)column.DataSource;
            //    foreach (DataSourceItem item in items)
            //    {
            //        if (item != null)
            //        {
            //            toReturn += "#" + item.Value + ";" + item.Text + "|";
            //        }
            //    }
            //    return toReturn;
            //}
            //catch (Exception ex)
            //{
            //    return ex.Message;
            //}
        }

        //RPU:
        /// <summary>
        /// Sets the list to be used as a drop-down on the specified column.(the column must be of type DataGridViewComboBoxColumn)
        /// </summary>
        /// <param name="col">The Col parameter should be set to a value between zero and Cols - 1 to set the ColComboList of a given column, or to -1 to set the ColComboList of all columns that are of type DataGridViewComboBoxColumn.</param>
        /// <param name="values">The items from ComboList separated by a '|' charachter</param>
        public void ColComboList(int col, string values)
        {
            int dgvCol = this.GetDGVColIndex(col);
            if (dgvCol >= 0)
            {
                base.Columns[dgvCol].UserData.ComboList = values;
            }

            //FC:FINAL:DSE Use string with the format "*nnn;" to the first item (where nnn is the zero-based index of the column to be displayed) to display a different column
            string displayColumn = "0";

            //ellipsis is used for CellButton, and not for a combobox column
            if (String.IsNullOrEmpty(values) || values == "...")
            {
                //dgvCol = GetDGVColIndex(col);
                //FC:FINAL:BSE :#i2234 column data lost when removing/adding the column. code moved down 
                //if (IsValidColumn(dgvCol))
                //{
                //    DataGridViewComboBoxColumn column = (base.Columns[dgvCol] as DataGridViewComboBoxColumn);
                //    if (column != null)
                //    {
                //        base.Columns.RemoveAt(dgvCol);
                //        base.Columns.Insert(dgvCol, new DataGridViewTextBoxColumn());
                //    }
                //}
            }
            else
            {
                string[] tokens = values.Split('|');
                List<string> items = new List<string>();
                bool editable = false;

                foreach (string token in tokens)
                {
                    //FC:FINAL:DSE:#1531 Save all item definition in the combo box datasource
                    //if (token.IndexOf("#") != -1 
                    //    //FC:FINAL:VGE - #567 There is possibility that Item 'value' will have # in it triggering first condition.
                    //    && token.IndexOf(";") > token.IndexOf("#"))
                    //{
                    //    string id = token.Substring(token.IndexOf("#") + 1, token.IndexOf(";") - token.IndexOf("#") - 1);
                    //    string value = token.Substring(token.IndexOf(";") + 1).Trim();
                    //    //FC:FINAL:DSE Use string with the format "*nnn;" to the first item (where nnn is the zero-based index of the column to be displayed) to display a different column
                    //    if (id.Contains("*"))
                    //    {
                    //        string valueCol = id.Substring(id.IndexOf("*") + 1);
                    //        id = id.Substring(0, id.IndexOf("*"));
                    //        if(!String.IsNullOrEmpty(id) && Information.IsNumeric(id))
                    //        {
                    //            displayColumn = valueCol;
                    //        } 
                    //    }
                    //    items.Add(new DataSourceItem(value, id));
                    //    //When the value is also displayed we have to make the combobox editable otherwise the text is lost when the combobox is focused
                    //    editable = true;
                    //}
                    //else if (token.IndexOf(";") != -1)
                    //{
                    //    string id = token.Substring(0, token.IndexOf(";"));
                    //    string value = token.Substring(token.IndexOf(";") + 1).Trim();
                    //    //FC:FINAL:AM:HARRIS:#1054 for items like 0;RE|1;UT the values before ; are actually ignored and only the part after; are returned (e.g. when calling TextMatrix)
                    //    //items.Add(new DataSourceItem(value, id));
                    //    items.Add(new DataSourceItem(value, value));
                    //}
                    //else
                    //{
                    //    items.Add(new DataSourceItem(token, token));
                    //}
                    // Use only the first display column, the other items may contain null display column
                    if (displayColumn == "0")
                    {
                        displayColumn = FCListViewComboBox.ExtractDisplayColumn(token);
                    }
                    
                    items.Add(token);
                }
                if (col != -1)
                {
                    //dgvCol = GetDGVColIndex(col);
                    if (IsValidColumn(dgvCol))
                    {
                        DataGridViewComboBoxColumn column = (base.Columns[dgvCol] as DataGridViewComboBoxColumn);
                        if (column == null)
                        {
                            //FC:FINAL:SBE - Harris #i610 - save current cell values
                            object[,] cellValues = new object[this.ColumnCount, this.RowCount];
                            //FC:FINAL:SGA - Harris #2802 - save current cell styles
                            object[,] cellStyles = new object[this.ColumnCount, this.RowCount];
                            object[] rowHeaderValues = new object[this.RowCount];
							object[] rowLevels = new object[this.RowCount];
							bool[] rowExpanded = new bool[this.RowCount];
							Color[] rowColors = new Color[this.RowCount];
							for (int rowIndex = 0; rowIndex < this.RowCount; rowIndex++)
                            {
                                rowHeaderValues[rowIndex] = base.Rows[rowIndex].HeaderCell.Value;
								rowLevels[rowIndex] = base.Rows[rowIndex].UserData.Level;
								rowExpanded[rowIndex] = base.Rows[rowIndex].IsExpanded;
								rowColors[rowIndex] = base.Rows[rowIndex].DefaultCellStyle.BackColor;
                                for (int colIndex = 0; colIndex < this.ColumnCount; colIndex++)
                                {
                                    cellValues[colIndex, rowIndex] = base[colIndex, rowIndex].Value;
                                    cellStyles[colIndex, rowIndex] = base[colIndex, rowIndex].Style;
                                }
                            }
                            int rows = this.Rows;
                            this.Rows = this.FixedRows;

                            string headerText = base.Columns[dgvCol].HeaderText;
                            int width = base.Columns[dgvCol].Width;
                            DynamicObject userData = base.Columns[dgvCol].UserData as DynamicObject;
                            //FC:FINAL:DSE - Harris #i1248 Header text can contain Html codes
                            bool allowHtml = base.Columns[dgvCol].AllowHtml;
                            DataGridViewCellStyle cellStyle = base.Columns[dgvCol].DefaultCellStyle;
                            //FC:FINAL:RPU: #i1169 - Add also the headerStyle
                            DataGridViewCellStyle headerStyle = base.Columns[dgvCol].HeaderStyle;
							//FC:FINAL:MSH - issue #1889: save column visibility to apply it to the new column
							bool colVisibility = base.Columns[dgvCol].Visible;
							base.Columns.RemoveAt(dgvCol);
                            //FC:FINAL:BSE #i2234  code moved form above. if clearing ComboBoxList used a TextBoxColumn 
                            DataGridViewColumn newColumn;
                            if (String.IsNullOrEmpty(values) || values == "...")
                            {
                                newColumn = new DataGridViewTextBoxColumn();
                                column = null;
                            }
                            else
                            {
                                newColumn = new DataGridViewComboBoxColumn();
                                column = (DataGridViewComboBoxColumn) newColumn;
                            }
                            newColumn.HeaderText = headerText;
                            newColumn.Width = width;
                            //FC:FINAL:DSE - Harris #i1248 Header text can contain Html codes
                            newColumn.AllowHtml = allowHtml;
                            newColumn.DefaultCellStyle = cellStyle;
                            //FC:FINAL:RPU: #i1169 - Add also the headerStyle
                            newColumn.HeaderStyle = headerStyle;
							//FC:FINAL:MSH - issue #1889: apply visibility to the new column
							newColumn.Visible = colVisibility;
                            //copy dynamic properties from the initial column
                            if (userData != null && userData.Count > 0)
                            foreach (DynamicObject.Member current in userData)
                            {
                                newColumn.UserData[current.Name] = current.Value;
                            }
                            base.Columns.Insert(dgvCol, newColumn);

                            this.Rows = rows;
                            //FC:FINAL:SBE - Harris #i610 - restore cell values
                            //FC:FINAL:SGA - Harris #2802 - restore cell styles
                            for (int rowIndex = 0; rowIndex < this.RowCount; rowIndex++)
                            {
                                base.Rows[rowIndex].HeaderCell.Value = rowHeaderValues[rowIndex];
								//DDU:HARRIS - error at null parsing to int
								this.RowOutlineLevel(this.GetFlexRowIndex(rowIndex), FCConvert.ToInt32(rowLevels[rowIndex]));
								for (int colIndex = 0; colIndex < this.ColumnCount; colIndex++)
                                {
                                    base[colIndex, rowIndex].Value = cellValues[colIndex, rowIndex];
                                    base[colIndex, rowIndex].Style = (DataGridViewCellStyle)cellStyles[colIndex, rowIndex];
                                }
                            }
							for(int rowIndex = 0; rowIndex < this.RowCount; rowIndex++)
							{
								base.Rows[rowIndex].DefaultCellStyle.BackColor = rowColors[rowIndex];
								if(base.Rows[rowIndex].UserData.Level == 0 && !rowExpanded[rowIndex])
								{
									base.Rows[rowIndex].Collapse();
								} 
							}
                        }
                        if (column != null)
                        {
                            column.DropDownStyle = editable ? ComboBoxStyle.DropDown : ComboBoxStyle.DropDownList;
                            column.DataSource = items;
                            //FC:FINAL:DSE Use string with the format "*nnn;" to the first item (where nnn is the zero-based index of the column to be displayed) to display a different column
                            column.UserData["displayIndex"] = displayColumn;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < base.Columns.Count; i++)
                    {
                        DataGridViewComboBoxColumn column = base.Columns[i] as DataGridViewComboBoxColumn;
                        if (column != null)
                        {
                            column.DropDownStyle = editable ? ComboBoxStyle.DropDown : ComboBoxStyle.DropDownList;
                            column.DataSource = items;
                            column.ValueMember = "Value";
                            column.DisplayMember = "Text";
                            //FC:FINAL:DSE Use string with the format "*nnn;" to the first item (where nnn is the zero-based index of the column to be displayed) to display a different column
                            column.UserData["displayIndex"] = displayColumn;
                        }
                    }
                }
            }
        }

        //RPU:
        /// <summary>
        /// The ColPosition function returns the order in which is diplayed the column.
        /// </summary>
        /// <param name="col">The number of column, must be in the range 0 to Cols - 1</param>
        /// <returns></returns>
        public int ColPosition(int col)
        {
            return base.Columns[col].DisplayIndex;
        }

        //RPU:
        /// <summary>
        /// Moves a given column to a new position.
        /// </summary>
        /// <param name="col">The Col must be in the range 0 to Cols - 1</param>
        /// <param name="position">The position parameter</param>
        public void ColPosition(int col, int position)
        {
            //FC:FINAL:SBE - Harris #338 - move column instead of DisplayIndex change. TextMatrix and other functions will not use the DisplayIndex
            //base.Columns[col].DisplayIndex = position;
            int dgvColIndex = this.GetDGVColIndex(col);
            int dgvPosition = this.GetDGVColIndex(position);
            if (IsValidColumn(dgvColIndex) && IsValidColumn(dgvPosition))
            {
                var column = base.Columns[dgvColIndex];
                //keep col values
                object[] colValues = new object[base.Rows.Count];
                //FC:FINAL:MSH - i.issue #1686: save UserData before removing column(after creating new column UserData will be equal null)
                object[] colUserDataValues = new object[base.Rows.Count];
                var columnHeader = base.Columns[dgvColIndex].HeaderCell.Value;
                for (int rowIndex = 0; rowIndex < base.Rows.Count; rowIndex++)
                {
                    colValues[rowIndex] = base[dgvColIndex, rowIndex].Value;
                    //FC:FINAL:MSH - i.issue #1686: save UserData before removing column(after creating new column UserData will be equal null)
                    colUserDataValues[rowIndex] = base[dgvColIndex, rowIndex].UserData.Data;
                }
                //move column to new position
                inColumnDisplayIndexChanged = true;
                base.Columns.Remove(column);
                base.Columns.Insert(dgvPosition, column);
                inColumnDisplayIndexChanged = false;

                //restore col values
                base.Columns[dgvPosition].HeaderCell.Value = columnHeader;
                for (int rowIndex = 0; rowIndex < base.Rows.Count; rowIndex++)
                {
                    base[dgvPosition, rowIndex].Value = colValues[rowIndex];
                    //FC:FINAL:MSH - i.issue #1686: restore UserData after inserting new column
                    base[dgvPosition, rowIndex].UserData.Data = colUserDataValues[rowIndex];
                }
            }
        }

        //RPU:
        /// <summary>
        /// Returns a user-defined value associated with the given column.
        /// </summary>
        /// <param name="col">The Col must be in the range 0 to Cols - 1</param>
        /// <returns></returns>
        public object ColData(int col)
        {
            col = this.GetDGVColIndex(col);
            return base.Columns[col].UserData.ColData;
        }

        //RPU:
        /// <summary>
        /// Sets a user-defined value associated with the given column.
        /// </summary>
        /// <param name="col">The Col must be in the range 0 to Cols - 1</param>
        /// <param name="value">The value associated with the given column</param>
        public void ColData(int col, object value)
        {
            col = this.GetDGVColIndex(col);
            base.Columns[col].UserData.ColData = value;
        }

        //RPU:
        /// <summary>
        /// Returns true if a row is hidden.
        /// </summary>
        /// <param name="row">The Row parameter should be set to a value between zero and Rows - 1 to return a value for a specified row , or to -1 to return  to return true only if all rows are hidden.</param>
        /// <returns></returns>
        public bool RowHidden(int row)
        {
            if (row != -1)
            {
                int dgvRow = this.GetDGVRowIndex(row);
                if (IsRowValid(dgvRow))
                {
                    return !base.Rows[dgvRow].Visible;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                foreach (DataGridViewRow dvrow in base.Rows)
                {
                    if (dvrow.Visible == true)
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        //RPU:
        /// <summary>
        /// Sets if a row is hidden
        /// </summary>
        /// <param name="row">The Row parameter should be set to a value between zero and Rows - 1 to hide or show a given row, or to -1 to hide or show all rows.</param>
        /// <param name="value">True if the row is hidden and false if it is visible</param>
        public void RowHidden(int row, bool hidden)
        {
            if (row != -1)
            {
                int dgvRow = this.GetDGVRowIndex(row);
                if (IsRowValid(dgvRow))
                {
                    //base.Rows[dgvRow].Visible = !hidden;
                    //FC:FINAL:MSH - issue #997: don't change visibility of row if it's child row and parent row invisible or collapsed
                    if (base.Rows[dgvRow].IsChild && base.Rows[dgvRow].ParentRow != null) 
                    {
                        if(base.Rows[dgvRow].ParentRow.Visible && base.Rows[dgvRow].ParentRow.IsExpanded)
                        {
                            base.Rows[dgvRow].Visible = !hidden;                            
                        }
                    }
                    else
                    {
                        base.Rows[dgvRow].Visible = !hidden;
                    }
                }
                else if (dgvRow < 0 && this.ColumnHeadersVisible)
                {
                    this.ColumnHeadersVisible = false;
                    this.AutoSizeGridHeight();
                }
            }
            else
            {
                foreach (DataGridViewRow dvrow in base.Rows)
                {
                    dvrow.Visible = !hidden;
                }
            }
        }

        public bool RowReadOnly(int row)
        {
            if (row != -1)
            {
                int dgvRow = this.GetDGVRowIndex(row);
                if (IsRowValid(dgvRow))
                {
                    return base.Rows[dgvRow].ReadOnly;
                }
                else
                {
                    return true;
                }
            }

            return true;
        }

        public void SetRowReadOnly(int row, bool readOnly)
        {
            if (row != -1)
            {
                int dgvRow = this.GetDGVRowIndex(row);
                if (IsRowValid(dgvRow))
                {
                    base.Rows[dgvRow].ReadOnly = readOnly;                    
                }
            }
        }
        public void Outline(int level)
        {
            //FC:FINAL:SBE - Harris #226, #264 - CollapseAll is executed with minRowlevel is 0, which doesn't have any child rows. Execute collapse on next level in this case
            //check if collapse all is executed
            if (level < maxRowLevel)
            {
                bool collapseAllExecuted = false;
                for (int i = 0; i < base.Rows.Count; i++)
                {
                    if (base.Rows[i].UserData.Level == level && base.Rows[i].ChildRows.Count > 0)
                    {
                        base.Rows[i].CollapseAll();
                        collapseAllExecuted = true;
                    }
                }
                //if minRowLevel does not have child rows, then we have to collapseAll on next level
                if (!collapseAllExecuted && minRowLevel < maxRowLevel)
                {
                    minRowLevel = level + 1;
                    this.Outline(minRowLevel);
                }
            }
            else if (level == maxRowLevel)
            {
                for (int i = 0; i < base.Rows.Count; i++)
                {
                    //if (base.Rows[i].UserData.Level == level)
                    //{
                    //    base.Rows[i].CollapseAll();
                    //}
                    //else if (base.Rows[i].UserData.Level < level)
                    //{
                    //    base.Rows[i].ExpandAll();
                    //}
                    if (!base.Rows[i].IsChild)
                    {
                        base.Rows[i].ExpandAll();
                    }
                }
            }
        }

        #endregion
    }

    #region EventArgs
    public class BeforeRowColChangeEventArgs : CancelEventArgs
    {
        public int OldCol { get; set; }
        public int OldRow { get; set; }
        public int NewCol { get; set; }
        public int NewRow { get; set; }
    }

    public class AfterMoveRowEventArgs: EventArgs
    {
        public int Row { get; set; }
        public int Position { get; set; }
    }

    public class AfterMoveColumnEventArgs : EventArgs
    {
        public int Column { get; set; }
        public int Position { get; set; }
    }
    #endregion
}
