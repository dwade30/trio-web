﻿using System;
using Wisej.Web;

namespace fecherFoundation
{
    public partial class FCButtonCell : Wisej.Web.UserControl
    {
        public event EventHandler ButtonClick;

        public FCButtonCell()
        {
            InitializeComponent();
        }

        public override string Text
        {
            get
            {
                return this.label.Text;
            }

            set
            {
                this.label.Text = value;
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            this.button.Width = this.button.Height;
            this.label.Width = this.Width - this.button.Width;
        }

        private void button_Click(object sender, EventArgs e)
        {
            if (this.ButtonClick != null)
            {
                this.ButtonClick(sender, e);
            }
        }
    }
}
