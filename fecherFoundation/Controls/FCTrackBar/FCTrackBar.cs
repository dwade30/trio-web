﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation
{
    public class FCTrackBar : TrackBar
    {
        private ToolTip toolTip = new ToolTip();

        [DefaultValue("")]
        public string ToolTipText
        {
            get
            {
                return toolTip.GetToolTip(this);
            }
            set
            {
                toolTip.SetToolTip(this, value);
            }
        }

        public new int Value
        {
            get
            {
                return base.Value;
            }
            set
            {

                //In VB6 you don't get exception if Value is outside of minimum and maximum value
                if (value >= Minimum && value <= Maximum)
                {
                    base.Value = value;
                }
            }
        }
    }
}
