﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    [System.AttributeUsage(System.AttributeTargets.Class |
                           System.AttributeTargets.Struct,
                           AllowMultiple = false)  // multiuse attribute
    ]
    public class MetadataTag : System.Attribute
    {
        public string tag;
        public MetadataTag(string tag)
        {
            this.tag = tag;
        }
    }
}
