﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

namespace fecherFoundation
{
    internal class TypeManager
    {
        // caches for reflection information
        //CHE
        //public static Dictionary<Type, TypeEntry> m_reflectionCache;
        public static Dictionary<Type, TypeEntry> m_reflectionCache = new Dictionary<Type, TypeEntry>();
        public static TypeEntry m_cachedTypeEntry;

        /// <summary>
        /// Returns all the MemberInfo that match the requested member in the type.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <param name="flags"></param>
        /// <returns></returns>
        public static MemberInfo[] GetMember(Type type, string name)
        {
            Debug.Assert(type != null);
            Debug.Assert(name != null);

            TypeEntry entry = GetTypeEntry(type);
            return entry.GetMember(name);
        }

        /// <summary>
        /// Retrieves or returns the TypeEntry corresponding to the Type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static TypeEntry GetTypeEntry(Type type)
        {
            Debug.Assert(type != null);

            TypeEntry entry = m_cachedTypeEntry;

            // use last cached entry to speed up multiple lookups on the same type
            if (entry != null && entry.Type == type)
                return entry;

            lock (m_reflectionCache)
            {
                // load the requested type entry
                if (!m_reflectionCache.TryGetValue(type, out entry))
                {
                    entry = new TypeEntry(type);
                    m_reflectionCache[type] = entry;
                    m_cachedTypeEntry = entry;
                }
            }

            return entry;
        }



        #region TypeEntry cached structure

        public class TypeEntry
        {
            public Type Type;
            // caches the members for this type
            public Dictionary<string, MemberInfo[]> m_members;

            public TypeEntry(Type type)
            {
                Debug.Assert(type != null);

                this.Type = type;               
            }

            public MemberInfo[] GetMember(string name)
            {
                BindingFlags bflags =
                    BindingFlags.Public |
                    BindingFlags.Static |
                    BindingFlags.Instance |
                    BindingFlags.NonPublic |
                    BindingFlags.DeclaredOnly;

                MemberTypes types =
                    MemberTypes.Field | MemberTypes.Property | MemberTypes.Method | MemberTypes.Constructor;

                if (m_members == null)
                    m_members = new Dictionary<string, MemberInfo[]>(100);

                MemberInfo[] members = null;
                if (!m_members.TryGetValue(name, out members))
                {
                    // find members from the bottom up
                    List<MemberInfo> list = new List<MemberInfo>(5);
                    for (Type type = this.Type; type != null; type = type.BaseType)
                    {
                        MemberInfo[] typeMembers = type.GetMember(name, types, bflags);
                        if (typeMembers != null && typeMembers.Length > 0)
                        {
                            list.AddRange(typeMembers);

                            // only methods can be overloaded
                            if (typeMembers[0].MemberType != MemberTypes.Method)
                                break;
                        }
                    }                    

                    members = new MemberInfo[list.Count];
                    list.CopyTo(members);

                    m_members[name] = members;
                }

                return members;
            }
        }
        #endregion
    }    
}
