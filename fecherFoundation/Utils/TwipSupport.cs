﻿using System;
using System.Runtime.InteropServices;

namespace fecherFoundation
{
    public static class TwipSupport
    {

        /// <summary>
        /// Gets a value that is used to convert twips to pixels based on screen settings.
        /// </summary>
        /// 
        /// <returns>
        /// A Double that contains the conversion factor.
        /// </returns>
        public static double TwipsPerPixelX()
        {
            return PixelsToTwips(1, MeasureDirection.Horizontal);
        }

        /// <summary>
        /// Gets a value that is used to convert twips to pixels based on screen settings.
        /// </summary>
        /// 
        /// <returns>
        /// A Double that contains the conversion factor.
        /// </returns>
        public static double TwipsPerPixelY()
        {
            return PixelsToTwips(1, MeasureDirection.Vertical);
        }

        /// <summary>
        /// Converts an X coordinate from twips to pixels.
        /// </summary>
        /// 
        /// <returns>
        /// A Double that contains the X coordinate expressed in pixels.
        /// </returns>
        /// <param name="X">A Double that contains the X coordinate to convert.</param
        public static double TwipsToPixelsX(double twips)
        {
            return TwipsToPixels(twips, MeasureDirection.Horizontal);
        }

        /// <summary>
        /// Converts a Y coordinate from twips to pixels.
        /// </summary>
        /// 
        /// <returns>
        /// A Double that contains the Y coordinate expressed in pixels.
        /// </returns>
        /// <param name="Y">A Double that contains the X coordinate to convert.</param>
        public static double TwipsToPixelsY(double twips)
        {
            return TwipsToPixels(twips, MeasureDirection.Vertical);
        }

        /// <summary>
        /// Converts an X coordinate from pixels to twips.
        /// </summary>
        /// 
        /// <returns>
        /// A Double that contains the X coordinate expressed in twips.
        /// </returns>
        /// <param name="X">A Double that contains the X coordinate to convert.</param>
        public static double PixelsToTwipsX(double pixels)
        {
            return PixelsToTwips(pixels, MeasureDirection.Horizontal);
        }

        /// <summary>
        /// Converts a Y coordinate from pixels to twips.
        /// </summary>
        /// 
        /// <returns>
        /// A Double that contains the Y coordinate expressed in twips.
        /// </returns>
        /// <param name="Y">A Double that contains the Y coordinate to convert.</param>
        public static double PixelsToTwipsY(double pixels)
        {
            return PixelsToTwips(pixels, MeasureDirection.Vertical);
        }

        public static double TwipsToPixels(double twips, MeasureDirection direction)
        {
            return ((twips) * (GetPixelsPerInch(direction)) / 1440.0);
        }

        public static double PixelsToTwips(double pixels, MeasureDirection direction)
        {
            return (((pixels) * 1440.0) / (GetPixelsPerInch(direction)));
        }

        public static double GetPixelsPerInch(MeasureDirection direction)
        {
            double ppi;
            IntPtr dc = GetDC(IntPtr.Zero);

            if (direction == MeasureDirection.Horizontal)
                ppi = GetDeviceCaps(dc, 88); //DEVICECAP LOGPIXELSX
            else
                ppi = GetDeviceCaps(dc, 90); //DEVICECAP LOGPIXELSY

            ReleaseDC(IntPtr.Zero, dc);
            return ppi;
        }

        [DllImport("user32.dll")]
        static extern IntPtr GetDC(IntPtr hWnd);

        [DllImport("user32.dll")]
        static extern bool ReleaseDC(IntPtr hWnd, IntPtr hDC);

        [DllImport("gdi32.dll")]
        static extern int GetDeviceCaps(IntPtr hdc, int devCap);

        public enum MeasureDirection
        {
            Horizontal,
            Vertical
        }
    }
}

