﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using Wisej.Web;

namespace fecherFoundation
{
    static public class EventHelper
    {
        #region KeyEventArgs

        /// <summary>
        /// Gets the key event arguments.
        /// </summary>
        /// <param name="intKeyCode">The key code.</param>
        /// <param name="intShift">The shift.</param>
        /// <returns>The key event arguments.</returns>
        public static KeyEventArgs GetKeyEventArgs(int intKeyCode, int intShift)
        {
            Keys enmKeys = (Keys)intKeyCode;
            if ((intShift & 1) > 0)
            {
                enmKeys |= Keys.Shift;
            }
            if ((intShift & 2) > 0)
            {
                enmKeys |= Keys.Control;
            }
            if ((intShift & 4) > 0)
            {
                enmKeys |= Keys.Alt;
            }
            KeyEventArgs objEventArgs = new KeyEventArgs(enmKeys);

            return objEventArgs;
        }


        /// <summary>
        /// Gets the shift modifier.
        /// </summary>
        /// <param name="objKeyEventArgs">The <see cref="KeyEventArgs"/> instance containing the event data.</param>
        /// <returns>The shift modifier</returns>
        public static int GetShift(KeyEventArgs objKeyEventArgs)
        {
            return GetShift(objKeyEventArgs.Modifiers);
        }

        public static int GetShift()
        {
            return GetShift(Control.ModifierKeys);
        }

        public static int GetShift(Keys modifiers)
        {
            int intShift = 0;
            if (modifiers == Keys.Alt)
            {
                intShift |= 4;
            }
            if (modifiers == Keys.Control)
            {
                intShift |= 2;
            }
            if (modifiers == Keys.Shift)
            {
                intShift |= 1;
            }
            return intShift;
        }

        #endregion

        /// <summary>
        /// remove all events
        /// </summary>
        /// <param name="ctrl"></param>
        // define the binding flags for reflection
        private const BindingFlags bindingFlags = BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;

        public static void RemoveAllEvents(Control ctrl, bool includeChildControls)
        {
            if (includeChildControls)
            {
                foreach(Control child in ctrl.Controls)
                {
                    RemoveAllEvents(child, includeChildControls);
                }
            }

            EventHandlerList handlerlist = CopyEventHandlers.GetListOfEventHandler(ctrl);

            var handlers = CopyEventHandlers.BuildList(ctrl);

            // Now walk through list and remove all handlers
            foreach (var t in handlers)
            {
                foreach (Delegate d in t.Value)
                    handlerlist.RemoveHandler(t.Key, d);
            }
        }

        /// <summary>
        /// Check if a default Windows-Clipboard-Key was pressed
        /// </summary>
        /// <param name="objKeyEventArgs"></param>
        /// <returns>True, if a Clipboard-Key was pressed, otherwise false</returns>
        public static bool IsWindowsClipboardKey(KeyEventArgs objKeyEventArgs)
        {
            bool isWindowsClipboardKey = objKeyEventArgs.Control && (objKeyEventArgs.KeyCode == Keys.X || objKeyEventArgs.KeyCode == Keys.V || objKeyEventArgs.KeyCode == Keys.C);
            return isWindowsClipboardKey;
        }
        /// <summary>
        /// Check, if one special Clipboard-Key was pressed
        /// </summary>
        /// <param name="objKeyEventArgs"></param>
        /// <param name="key"></param>
        /// <returns>True, if a Clipboard-Key was pressed, otherwise false</returns>
        public static bool IsWindowsClipboardKeyCtrl_Key(KeyEventArgs objKeyEventArgs, Keys key)
        {
            bool isWindowsClipboardKey = objKeyEventArgs.Control && objKeyEventArgs.KeyCode == key;
            return isWindowsClipboardKey;
        }
    }
}