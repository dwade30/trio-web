﻿//http://www.codeproject.com/Articles/308536/How-to-copy-event-handlers-from-one-control-to-ano
namespace fecherFoundation {
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using Wisej.Web;
    using BF = System.Reflection.BindingFlags;

    public class CopyEventHandlers {
        /// <summary>       
        /// We use this field to hold some sort of mapping between the Control class's inner field's name that 
        /// the .NET Framework uses as a key to hold handlers attached to an event and the name of d event.
        /// </summary>
        private readonly Dictionary<string, string> _keyEventNameMapping = null;

        public CopyEventHandlers() {            
            _keyEventNameMapping = new Dictionary<string, string>{
                {"EventAutoSizeChanged", "AutoSizeChanged"},
                {"EventBackColor", "BackColorChanged"},
                {"EventBackgroundImage", "BackgroundImageChanged"},
                {"EventBackgroundImageLayout", "BackgroundImageLayoutChanged"},
                {"EventBindingContext", "BindingContextChanged"},
                {"EventCausesValidation", "CausesValidationChanged"},
                {"EventClick", "Click"},
                {"EventClientSize", "ClientSizeChanged"},
                {"EventContextMenu", "ContextMenuChanged"},
                {"EventContextMenuStrip", "ContextMenuStripChanged"},
                {"EventCursor", "CursorChanged"},
                {"EventDock", "DockChanged"},
                {"EventDoubleClick", "DoubleClick"},
                {"EventDragLeave", "DragLeave"},
                {"EventEnabled", "EnabledChanged"},
                {"EventEnter", "Enter"},
                {"EventFont", "FontChanged"},
                {"EventForeColor", "ForeColorChanged"},
                {"EventGotFocus", "GotFocus"},
                {"EventHandleCreated", "HandleCreated"},
                {"EventHandleDestroyed", "HandleDestroyed"},
                {"EventLeave", "Leave"},
                {"EventLocation", "LocationChanged"},
                {"EventLostFocus", "LostFocus"},
                {"EventMarginChanged", "MarginChanged"},
                {"EventMouseCaptureChanged", "MouseCaptureChanged"},
                {"EventMouseEnter", "MouseEnter"},
                {"EventMouseHover", "MouseHover"},
                {"EventMouseLeave", "MouseLeave"},
                {"EventMove", "Move"},
                {"EventPaddingChanged", "PaddingChanged"},
                {"EventParent", "ParentChanged"},
                {"EventRegionChanged", "RegionChanged"},
                {"EventResize", "Resize"},
                {"EventRightToLeft", "RightToLeftChanged"},
                {"EventSize", "SizeChanged"},
                {"EventStyleChanged", "StyleChanged"},
                {"EventSystemColorsChanged", "SystemColorsChanged"},
                {"EventTabIndex", "TabIndexChanged"},
                {"EventTabStop", "TabStopChanged"},
                {"EventText", "TextChanged"},
                {"EventValidated", "Validated"},
                {"EventVisible", "VisibleChanged"},
                {"EventKeyDown","KeyDown"}, 
                {"EventKeyUp","KeyUp"}, 
                {"EventMouseMove","MouseMove"},
                //IPI - add support for Component event
                {"EventDisposed", "Disposed"},
                //IPI - add support for CheckBox event
                {"EVENT_CHECKSTATECHANGED","EVENT_CHECKSTATECHANGED"},
                //IPI - add support for ComboBox event
                {"EVENT_SELECTEDINDEXCHANGED","EVENT_SELECTEDINDEXCHANGED"}, 
                {"EventChange", "Change"},
                {"EventDragDrop", "DragDrop"},
                {"EventDragOver", "DragOver"},
                {"EventKeyPress", "KeyPress"},
                {"EventMouseDown", "MouseDown"},
                {"EventMouseUp", "MouseUp"},
                {"EventSelectedIndexChanged", "SelectedIndexChanged"},
                {"EventValidating", "Validating"}
            };
        }

        public Dictionary<string, string> KeyEventNameMapping {
            get { return _keyEventNameMapping; }
        }

        /// <summary>
        /// Use this method to get a data structure that contains the name of the all events 
        /// that have handlers attached, the inner key that .NET use to point to those handlers and the
        /// list of listeners (the handlers themselves).
        /// </summary>
        /// <param name="ctrl">Control del cual queremos extraer todos los handlers.</param>
        /// <returns></returns>
        public Dictionary<string, Delegate[]> GetHandlersFrom(Control ctrl)
        {
            var handlers = BuildList(ctrl);

            if (handlers.Count == 0)
            {
                return null;
            }
            
            var ctrlHandlers = new Dictionary<string, Delegate[]>();
            foreach (object key in handlers.Keys)
            {
                var eventName = GetEventNameFromKey(ctrl, key);
                if (eventName != null)
                {
                    ctrlHandlers.Add(eventName, handlers[key]);
                }
            }

            if (ctrlHandlers.Count > 0)
                return ctrlHandlers;

            return null;
        }       

        /// <summary>
        /// Returns the event's name based on the specified key that 
        /// the .NET Framework use to holds the events handlers.
        /// </summary>
        /// <param name="ctrl">Control</param>
        /// <param name="key">The key (instance) that holds the handlers.</param>
        /// <returns>Event's name mapped to the supplied key.</returns>
        private string GetEventNameFromKey(Control ctrl, object key) {

            bool isComponent = ctrl is System.ComponentModel.Component;
            bool isCheckBox = ctrl is CheckBox;
            bool isComboBox = ctrl is ComboBox;
            BF bindingFlags = BF.GetField | BF.Static | BF.NonPublic | BF.DeclaredOnly;
            Type ctrlType = typeof(Control);
            Type compType = typeof(System.ComponentModel.Component);
            Type chkType = typeof(CheckBox);
            Type cmbType = typeof(ComboBox);

            foreach (var eventKey in KeyEventNameMapping.Keys) {
                var fieldInfo = ctrlType.GetField(eventKey, bindingFlags);
                if (fieldInfo == null && isComponent)
                {
                    fieldInfo = compType.GetField(eventKey, bindingFlags);
                }
                if (fieldInfo == null && isCheckBox)
                {
                    fieldInfo = chkType.GetField(eventKey, bindingFlags);
                }
                if (fieldInfo == null && isComboBox)
                {
                    fieldInfo = cmbType.GetField(eventKey, bindingFlags);
                }
                if (fieldInfo != null) {
                    var tmpkey = fieldInfo.GetValue(ctrl);
                    if (tmpkey == key)
                        return KeyEventNameMapping[eventKey];
                }
            }

            return null;
        }

        /// <summary>
        /// Get EventHandlerList of control from property "Events" via Reflection 
        /// </summary>
        /// <param name="ctrl"></param>
        /// <returns>list of handlers</returns>
        internal static EventHandlerList GetListOfEventHandler(Control ctrl)
        {
            return (EventHandlerList)ctrl
                .GetType()
                .GetProperty("Events", BF.GetProperty | BF.NonPublic | BF.Instance)
                .GetValue(ctrl, null);
        }

        /// <summary>
        /// wrapper method for original BuildList
        /// get Events property and info for head field first, then call BuildList
        /// </summary>
        /// <param name="ctrl">Control to build the event handler list</param>
        /// <returns></returns>
        internal static Dictionary<object, Delegate[]> BuildList(Control ctrl)
        {
            var ctrlEventsCollection = GetListOfEventHandler(ctrl);

            var headInfo = typeof(EventHandlerList).GetField("head", BF.Instance | BF.NonPublic);

            return BuildList(headInfo, ctrlEventsCollection);
        }

        //**************************************************************************************************************
        //Code borrowed from StackOverflow
        //http://stackoverflow.com/questions/91778/how-to-remove-all-event-handlers-from-a-control
        internal static Dictionary<object, Delegate[]> BuildList(FieldInfo headInfo, object eventHandlersList) {
            var handlers = new Dictionary<object, Delegate[]>();
            object head = headInfo.GetValue(eventHandlersList);
            if (head != null) {
                Type listEntryType = head.GetType();
                FieldInfo delegateInfo = listEntryType.GetField("handler", BF.Instance | BF.NonPublic);
                FieldInfo currentKeyInfo = listEntryType.GetField("key", BF.Instance | BF.NonPublic);
                FieldInfo nextKeyInfo = listEntryType.GetField("next", BF.Instance | BF.NonPublic);
                handlers = BuildListWalk(handlers, head, delegateInfo, currentKeyInfo, nextKeyInfo);
            }
            return handlers;
        }

        private static Dictionary<object, Delegate[]> BuildListWalk(Dictionary<object, Delegate[]> dict, object entry, FieldInfo delegateInfo, FieldInfo keyInfo, FieldInfo nextKeyInfo) {
            if (entry != null) {
                var del = (Delegate)delegateInfo.GetValue(entry);
                object key = keyInfo.GetValue(entry);
                object next = nextKeyInfo.GetValue(entry);

                if (del != null) {
                    Delegate[] listeners = del.GetInvocationList();
                    if (listeners.Length > 0) {
                        dict.Add(key, listeners);
                    }
                }
                if (next != null) {
                    dict = BuildListWalk(dict, next, delegateInfo, keyInfo, nextKeyInfo);
                }
            }
            return dict;
        }
        //**************************************************************************************************************
    }
}