﻿//http://www.codeproject.com/Articles/308536/How-to-copy-event-handlers-from-one-control-to-ano
namespace fecherFoundation {
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using Wisej.Web;
    using BF = System.Reflection.BindingFlags;

    public static class CopyEventsHandlersExtensionMethods {
        /// <summary>
        /// copy handlers
        /// </summary>
        /// <param name="handlers"></param>
        /// <param name="destination"></param>
        public static void CopyTo(this Dictionary<string, Delegate[]> handlers, Control destination)
        {
            if (handlers == null)
            {
                return;
            }
            var helper = new CopyEventHandlers();

            bool isComponent = destination is System.ComponentModel.Component;
            bool isCheckBox = destination is CheckBox;
            bool isComboBox = destination is ComboBox;
            BF bindingFlags = BF.GetField | BF.Static | BF.NonPublic | BF.DeclaredOnly;
            Type ctrlType = typeof(Control);
            Type compType = typeof(System.ComponentModel.Component);
            Type chkType = typeof(CheckBox);
            Type cmbType = typeof(ComboBox);

            var targetType = destination.GetType();
            var targetEventsInfo = targetType.GetProperty("Events", BF.Instance | BF.NonPublic);
            var targetEventHandlerList = (EventHandlerList)targetEventsInfo.GetValue(destination, null);

            foreach (var eventName in handlers.Keys) {
                var innerKeyFieldName = helper.KeyEventNameMapping.First(x => x.Value == eventName).Key;

                var info = ctrlType.GetField(innerKeyFieldName, bindingFlags);

                if (info == null && isComponent)
                {
                    info = compType.GetField(innerKeyFieldName, bindingFlags);
                }
                if (info == null && isCheckBox)
                {
                    info = chkType.GetField(innerKeyFieldName, bindingFlags);
                }
                if (info == null && isComboBox)
                {
                    info = cmbType.GetField(innerKeyFieldName, bindingFlags);
                }
                
                if (info != null) {
                    var innerKeyFieldVal = info.GetValue(destination);                    

                    var delegates = handlers[eventName];
                    foreach (var @delegate in delegates) {
                        targetEventHandlerList.AddHandler(innerKeyFieldVal, @delegate);
                    }
                }
            }
        }
    }
}
