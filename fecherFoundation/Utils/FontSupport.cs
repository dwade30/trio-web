﻿#if Winforms
using System.Windows.Forms;
#else
using Gizmox.WebGUI.Forms;
#endif
using System;
using System.Drawing;

namespace fecherFoundation
{
    public static class FontSupport
    {
        /// <summary>
        /// Returns a new <see cref="T:System.Drawing.Font"/> for a given Visual Basic 6.0 Font.
        /// </summary>
        /// 
        /// <returns>
        /// A <see cref="T:System.Drawing.Font"/> that matches the Visual Basic 6.0 Font.
        /// </returns>
        /// <param name="CurrentFont">A <see cref="T:System.Drawing.Font"/>.</param><param name="Name">A String that represents the Visual Basic 6.0 Font property.</param>
        public static System.Drawing.Font FontChangeName(System.Drawing.Font CurrentFont, string Name)
        {
            return new Font(Name, CurrentFont.Size, CurrentFont.Style, CurrentFont.Unit, CurrentFont.GdiCharSet, CurrentFont.GdiVerticalFont);
        }

        
        /// <summary>
        /// Changes the <see cref="P:System.Drawing.Font.Size"/> property for a font.
        /// </summary>
        /// 
        /// <returns>
        /// A <see cref="T:System.Drawing.Font"/> that matches the Visual Basic 6.0 FontSize.
        /// </returns>
        /// <param name="CurrentFont">A <see cref="T:System.Drawing.Font"/>.</param><param name="Size">A Single that represents the Visual Basic 6.0 FontSize property.</param>
        public static System.Drawing.Font FontChangeSize(System.Drawing.Font CurrentFont, float Size)
        {
            if (Math.Round((double)CurrentFont.SizeInPoints, 2) != Math.Round((double)Size, 2))
                return new System.Drawing.Font(CurrentFont.Name, Size, CurrentFont.Style, GraphicsUnit.Point, CurrentFont.GdiCharSet);
            else
                return CurrentFont;
        }

        public static System.Drawing.Font FontChangeStyle(System.Drawing.Font CurrentFont, FontStyle StyleBit, bool NewValue)
        {
            if ((CurrentFont.Style & StyleBit) != FontStyle.Regular == NewValue)
                return CurrentFont;
            FontStyle newStyle = CurrentFont.Style & ~StyleBit;
            if (NewValue)
                newStyle |= StyleBit;
            return new System.Drawing.Font(CurrentFont, newStyle);
        }

        /// <summary>
        /// Changes the <see cref="F:System.Drawing.FontStyle.Bold"/> style bit for a font.
        /// </summary>
        /// 
        /// <returns>
        /// A <see cref="T:System.Drawing.Font"/> with the new style applied.
        /// </returns>
        /// <param name="CurrentFont">A <see cref="T:System.Drawing.Font"/>.</param><param name="Bold">true to set the <see cref="F:System.Drawing.FontStyle.Bold"/> style bit; otherwise false.</param>
        public static System.Drawing.Font FontChangeBold(System.Drawing.Font CurrentFont, bool Bold)
        {
            return FontChangeStyle(CurrentFont, FontStyle.Bold, Bold);
        }

        /// <summary>
        /// Changes the <see cref="F:System.Drawing.FontStyle.Italic"/> style bit for a font.
        /// </summary>
        /// 
        /// <returns>
        /// A <see cref="T:System.Drawing.Font"/> with the new style applied.
        /// </returns>
        /// <param name="CurrentFont">A <see cref="T:System.Drawing.Font"/>.</param><param name="Italic">true to set the <see cref="F:System.Drawing.FontStyle.Italic"/> style bit; otherwise false.</param>
       public static System.Drawing.Font FontChangeItalic(System.Drawing.Font CurrentFont, bool Italic)
        {
            return FontChangeStyle(CurrentFont, FontStyle.Italic, Italic);
        }

        /// <summary>
        /// Changes the <see cref="F:System.Drawing.FontStyle.Strikeout"/> style bit for a font.
        /// </summary>
        /// 
        /// <returns>
        /// A <see cref="T:System.Drawing.Font"/> with the new style applied.
        /// </returns>
        /// <param name="CurrentFont">A <see cref="T:System.Drawing.Font"/>.</param><param name="Strikeout">true to set the <see cref="F:System.Drawing.FontStyle.Strikeout"/> style bit; otherwise false.</param>
        public static System.Drawing.Font FontChangeStrikeout(System.Drawing.Font CurrentFont, bool Strikeout)
        {
            return FontChangeStyle(CurrentFont, FontStyle.Strikeout, Strikeout);
        }

        /// <summary>
        /// Changes the <see cref="F:System.Drawing.FontStyle.Underline"/> style bit for a font.
        /// </summary>
        /// 
        /// <returns>
        /// A <see cref="T:System.Drawing.Font"/> with the new style applied.
        /// </returns>
        /// <param name="CurrentFont">A <see cref="T:System.Drawing.Font"/>.</param><param name="Underline">true to set the <see cref="F:System.Drawing.FontStyle.Underline"/> style bit; otherwise false.</param>
        public static System.Drawing.Font FontChangeUnderline(System.Drawing.Font CurrentFont, bool Underline)
        {
            return FontChangeStyle(CurrentFont, FontStyle.Underline, Underline);
        }
    }
}
