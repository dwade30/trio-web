﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using Wisej.Web;

namespace fecherFoundation.Utils
{
    /// <summary>
    /// Helper-Class for Handling Drag & Drop-Events
    /// </summary>
    public class FCDragDrop
    {
        #region Public Members
        // Source of a Drag-Drop-Operation
     
        #endregion

        #region Internal Members
        #endregion

        #region Private Members
        private static Control source = null;
        private static Image dragIcon = null;
        private static Cursor originalCursor = null;
        #endregion

        #region Constructors
        private FCDragDrop() { }
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties
        /// <summary>
        /// Source-Ctrl of the Drag&Drop-Action
        /// </summary>
        public static Control Source
        {
            get
            {
                return source;
            }
            set
            {
                source = value;
            }
        }
        /// <summary>
        /// Drag-Icon of the Drag&Drop-Action
        /// </summary>
        public static Image DragIcon
        {
            get
            {
                return dragIcon;
            }
            set
            {
                dragIcon = value;
            }
        }
        /// <summary>
        /// Save the Original-Cursor (before using an Icon for Drag&Drop)
        /// </summary>
        public static Cursor OriginalCursor
        {
            get
            {
                return originalCursor;
            }
            set
            {
                FCUtils.DisposeCursor(ref originalCursor);
                originalCursor = value;
            }
        }

        /// <summary>
        /// Name of the Source-Control for DragDrop
        /// </summary>
        public static string NameOfSource
        {
            get
            {
                string conrolName = FCUtils.GetOriginalControlName(Source);
                return conrolName ?? Source.Name ?? string.Empty;
            }
        }

        /// <summary>
        /// Tag of the Source-Control for DragDrop
        /// </summary>
        public static object TagOfSource
        {
            get
            {
                return Source.Tag ?? string.Empty;
            }
        }
        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods
        public static void Reset()
        {
            DragIcon = null;
            Source = null;
            OriginalCursor = null;
        }

        public static void Init(Control ctrl, Image img, Cursor originalCursor)
        {
            Source = ctrl;
            DragIcon = img;
            OriginalCursor = originalCursor;
        }
        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods
        #endregion
    }
}
