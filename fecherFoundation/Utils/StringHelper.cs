﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    public static class StringHelper
    {
        public static string ToStringES(this object obj)
        {
            if (obj != null)
            {
                if (obj.GetType().BaseType == typeof(Enum))
                {
                    return Convert.ToInt32(obj).ToString();
                }
                else if (obj is DateTime)
                {
                    DateTime dt = (DateTime)obj;
                    bool removeTime = dt.TimeOfDay == TimeSpan.Zero;
                    bool removeDate = dt.Year == 1899 && dt.Month == 12 && dt.Day == 30;
                    string format = "";
                    if (removeDate)
                    {
                        format = "HH:mm:ss";
                    }
                    else if (removeTime)
                    {
                        format = "d";
                    }
                    return dt.ToString(format);
                }
                else if (obj is Color)
                {
                    return ColorTranslator.ToOle((Color)obj).ToString();
                }

                return obj.ToString();
            }
            return null;
        }
    }
}
