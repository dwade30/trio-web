﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace fecherFoundation
{
    public static class FCConvert
    {
        private static CultureInfo enCultureInfo = new CultureInfo("en");
        
        /// <summary>
        /// to string
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToString(object obj)
        {
            if (obj != null)
            {
                if (obj.GetType().BaseType == typeof(Enum))
                {
                    return Convert.ToInt32(obj).ToString();
                }
                else if (obj is DateTime)
                {
                    DateTime dt = (DateTime)obj;
                    bool removeTime = dt.TimeOfDay == TimeSpan.Zero;
                    bool removeDate = dt.Year == 1899 && dt.Month == 12 && dt.Day == 30;
                    string format = "";
                    if (removeTime)
                    {
                        format = "MM/dd/yyyy";
                    }
					else if (removeDate)
                    {
                        format = "HH:mm:ss";
                    }
                    return dt.ToString(format);
                }
                //FC:FINAL:SGA Harris #i2449 - in case of decimal value in VB6 "0.#" format is used
				//HARRIS:DDU:#2328 - decimal like 0.01 is returned 0, so we need to check the second decimal too
                else if (obj.GetType() == typeof(decimal))
                {
                    return ((decimal)obj).ToString("0.##");
                }
                //FC:FINAL:SGA Harris #i2482 - use format as in original
                else if (obj.GetType() == typeof(TimeSpan))
                {
                    return Strings.Format((TimeSpan)obj, "h:mm AM/PM");
                }

                return obj.ToString();
            }
            return Convert.ToString(obj);





            //if (obj is DateTime)
            //{
            //    //CHE: check if it contains only time
            //    DateTime dt = Convert.ToDateTime(obj);
            //    if (dt.Date == DateTime.MinValue)
            //    {
            //        return dt.ToShortTimeString();
            //    }
            //    // always convert to ShortDateString
            //    return dt.ToShortDateString();
            //}
            //else
            //{
            //    return Convert.ToString(obj);
            //}
        }

        /// <summary>
        /// to string
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public static string ToString(object obj, System.Globalization.CultureInfo info)
        {
           return Convert.ToString(obj, info);
        }

		public static short ToInt16(dynamic value)
		{
            if (value == null)
            {
                return 0;
            }

            short result = 0;
            if (value.GetType() == typeof(Boolean))
            {
                result = (short)(value ? -1 : 0);
            }
            //AM:HARRIS:#3281: a value like 100.96 should be converted to 101
            else if (FCUtils.IsNumeric(value.GetType()))
            {
                result = Convert.ToInt16(value);
            }
            else if (value.GetType().BaseType == typeof(Enum))
            {
                result = Convert.ToInt16(value);
            }
            else if (value.GetType() == typeof(String))
            {
                result = Convert.ToInt16(FCConvert.ToDouble(value));
            }
            return result;
        }

        public static int ToInt32(dynamic value)
        {
            if (value == null)
            {
                return 0;
            }

            int result = 0;
            if (value.GetType() == typeof(Boolean))
            {
                result = value ? -1 : 0;
            }
            //AM:HARRIS:#3281: a value like 100.96 should be converted to 101
            else if (FCUtils.IsNumeric(value.GetType()))
            {
                result = Convert.ToInt32(value);
            }
            else if(value.GetType().BaseType == typeof(Enum))
            {
                result = Convert.ToInt32(value);
            }
            else if (value.GetType() == typeof(String))
            {
                result = Convert.ToInt32(FCConvert.ToDouble(value));
            }
            return result;
        }

		/// <summary>
		/// adjust date as in VB6 for year value between 0 and 99
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static DateTime ToDateTime(object value)
		{
			if (value is DateTime)
			{
				return (DateTime)value;
			}

			DateTime dt = DateTime.FromOADate(0);
			try
			{
				//DSE Exception when converting a numeric value
				if (Information.IsNumeric(value))
				{
					dt = DateTime.FromOADate(Convert.ToInt32(value));
				}
				//SGA: in case of a string try to convert using "en" format
				if (value is string)
				{
					string strVal = (string)value;
                    DateTime temp;
					if (DateTime.TryParse(strVal, enCultureInfo, DateTimeStyles.AllowWhiteSpaces | DateTimeStyles.NoCurrentDateDefault, out temp)) 
					{
                        //AM:HARRIS:#i2073 - when value contains just the time part (e.g. 12:00:00 AM) the date part should be DateTime.FromOADate(0)
                        if (temp != DateTime.MinValue)
                        {
                            if (temp.Date == DateTime.MinValue)
                            {
                                dt = new DateTime(dt.Year, dt.Month, dt.Day, temp.Hour, temp.Minute, temp.Second);
                            }
                            else
                            {
                                dt = temp;
                            }
                        }
                        
					}
				}
				else
				{
					//JEI: avoid exception for empty value
					if (value?.ToString().Length != 0)
					{
						dt = Convert.ToDateTime(value);
					}
				}
			}
			catch
			{

			}
			if (dt.Year >= 0 && dt.Year <= 29)
			{
				dt = new DateTime(2000 + dt.Year, dt.Month, dt.Day);
			}
			else if (dt.Year >= 30 && dt.Year < 100)
			{
				dt = new DateTime(1900 + dt.Year, dt.Month, dt.Day);
			}
			return dt;
		}

        public static bool ToBoolean(object o)
        {
            return CBool(o);
        }

        public static bool CBool(object o)
        {
            if (o is String)
            {
                if (String.IsNullOrEmpty(o as String))
                    return false;

                // JSP: test is String is true or false as string 
                if (o.ToString().ToLower() == "false")
                {
                    return false;
                }
                else if (o.ToString().ToLower() == "true")
                {
                    return true;
                }

                o = Convert.ToInt32(o);
            }

            return Convert.ToBoolean(o);
        }

        public static decimal ToDecimal(dynamic value)
        {
            if (value == null)
            {
                return 0;
            }

            decimal result = 0;
            if (value.GetType() == typeof(Boolean))
            {
                result = value ? -1 : 0;
            }
            else if (FCUtils.IsNumeric(value.GetType()))
            {
                result = Convert.ToDecimal(value);
            }
            else if (value.GetType().BaseType == typeof(Enum))
            {
                result = Convert.ToDecimal(value);
            }
            else if (value.GetType() == typeof(String))
            {
                Decimal.TryParse(value, NumberStyles.Any, enCultureInfo, out result);
            }
            return result;
        }

		public static double ToDouble(dynamic value)
		{
            if (value == null)
            {
                return 0;
            }

            double result = 0;
            if (value.GetType() == typeof(Boolean))
            {
                result = value ? -1 : 0;
            }
            else if (FCUtils.IsNumeric(value.GetType()))
            {
                result = Convert.ToDouble(value);
            }
            else if (value.GetType() == typeof(String))
            {
                Double.TryParse(value, NumberStyles.Any, enCultureInfo, out result);
            }
            return result;
        }

		public static float ToSingle(dynamic value)
		{
            if (value == null)
            {
                return 0;
            }

            float result = 0;
            if (value.GetType() == typeof(Boolean))
            {
                result = value ? -1 : 0;
            }
            else if (FCUtils.IsNumeric(value.GetType()))
            {
                result = Convert.ToSingle(value);
            }
            else if (value.GetType().BaseType == typeof(Enum))
            {
                result = Convert.ToSingle(value);
            }
            else if (value.GetType() == typeof(String))
            {
                Single.TryParse(value, NumberStyles.Any, enCultureInfo, out result);
            }
            return result;
        }

	}
}
