﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fecherFoundation
{
    public static class FCEnumHelper
    {
        public static object ToObject(Type enumType, int value)
        {
            return Enum.ToObject(enumType, value);
        }

        public static object ToObject( Type enumType, byte value)
        {
            return Enum.ToObject(enumType, Convert.ToInt32(value));
        }

        public static object ToObject( Type enumType, long value)
        {
            return Enum.ToObject(enumType, Convert.ToInt32(value));
        }

        public static object ToObject( Type enumType, object value)
        {
            try
            {
                return Enum.ToObject(enumType, Convert.ToInt32(value));
            }
            catch (System.Exception)
            {
                return null;
            }
        }

        public static object ToObject( Type enumType, sbyte value)
        {
            return Enum.ToObject(enumType, Convert.ToInt32(value));
        }

        public static object ToObject( Type enumType, short value)
        {
            return Enum.ToObject(enumType, Convert.ToInt32(value));
        }

        public static object ToObject( Type enumType, uint value)
        {
            return Enum.ToObject(enumType, Convert.ToInt32(value));
        }

        public static object ToObject( Type enumType, ulong value)
        {
            return Enum.ToObject(enumType, Convert.ToInt32(value));
        }

        public static object ToObject( Type enumType, ushort value)
        {
            return Enum.ToObject(enumType, Convert.ToInt32(value));
        }

    }
}
