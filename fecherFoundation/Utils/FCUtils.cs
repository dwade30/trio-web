﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using Wisej.Web;
using fecherFoundation.Extensions;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Reflection;
using fecherFoundation.VisualBasicLayer;
using Encoder = System.Drawing.Imaging.Encoder;

namespace fecherFoundation
{
    #region enums
    /// <summary>
    /// Provides constants for e.g. FontChangeGdiCharSet in VB6
    /// </summary>
    public enum GDICharSet : byte
    {
        ANSI = 0,
        DEFAULT = 1,
        SYMBOL = 2,
        SHIFTJIS = 128,
        HANGEUL = 129,
        HANGUL = 129,
        GB2312 = 134,
        CHINESEBIG5 = 136,
        OEM = 255,
        JOHAB = 130,
        HEBRÄISCH = 177,
        ARABISCH = 178,
        GRIECHISCH = 161,
        TÜRKISCH = 162,
        VIETNAMESISCH = 163,
        THAI = 222,
        OSTEUROPÄISCHE_SPRACHEN = 238,
        RUSSISCH = 204,
        MAC = 77,
        BALTISCH = 186
    }

    /// <summary>
    /// LoadPicture
    /// </summary>
    public enum PictureSizeConstants
    {
        vbLPSmall = 0,
        vbLPLarge = 1,
        vbLPSmallShell = 2,
        vbLPLargeShell = 3,
        vbLPCustom = 4
    }

    public enum ColorDepthConstants
    {
        vbLPDefault = 0,
        vbLPMonochrome = 1,
        vbLPVGAColor = 2,
        vbLPColor = 3
    }

    #endregion enums
    public static class FCUtils
    {
        #region Public Members
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        /// <summary>
        /// Stack for window handles.
        /// </summary>
        private static Stack<IntPtr> lockWindowUpdateStack = new Stack<System.IntPtr>();
        private static List<Font> createdFonts = new List<Font>();

        #endregion

        #region Constructors
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events
        #endregion

        #region Private Events
        #endregion

        #region Enums
        // if we specify CharSet.Auto instead of CharSet.Ansi, then the string will be unreadable
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public class LOGFONT
        {
            public int lfHeight = 0;
            public int lfWidth = 0;
            public int lfEscapement = 0;
            public int lfOrientation = 0;
            public int lfWeight = 0;
            public byte lfItalic = 0;
            public byte lfUnderline = 0;
            public byte lfStrikeOut = 0;
            public byte lfCharSet = 0;
            public byte lfOutPrecision = 0;
            public byte lfClipPrecision = 0;
            public byte lfQuality = 0;
            public byte lfPitchAndFamily = 0;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
            public string lfFaceName = string.Empty;
        }

        #endregion

        #region Properties
        public static Wisej.Web.Cursor ScreenCursor
        {
            get { return Application.OpenForms[0].Cursor; }
            set { Application.OpenForms[0].Cursor = value; }
        }
        #endregion

        #region Internal Properties

        internal static bool inCloneControlProperties = false;

        #endregion

        #region Public Methods

        /// <summary>
        /// Download file from server to client
        /// </summary>
        /// <param name="content">Content to write on file</param>
        /// <param name="fileName">Name of file without extension</param>
        /// <param name="extension">Extension type</param>
        public static void DownloadToClient(string content, string fileName, string extension)
        {
            FCFileSystem.FileOpen(1, Path.Combine(FCFileSystem.Statics.UserDataFolder, fileName + extension), OpenMode.Random, OpenAccess.Default, OpenShare.Default);
            FCFileSystem.Write(1, content);
            FCFileSystem.FileClose(1);
            using (var stream = new FileStream(Path.Combine(FCFileSystem.Statics.UserDataFolder, fileName + extension), FileMode.Open))
            {
                Application.Download(stream, fileName + extension);
            }
        }

        /// <summary>
        /// Get Wisej Download link for a server file
        /// </summary>
        /// <param name="filepath">server file path</param>
        /// <param name="fileDisplayName">download display name</param>
        /// <returns>download link</returns>
        public static string GetDownloadLink(string filepath, string fileDisplayName)
        {
            var downloadedObject = new
            {
                file = filepath,
                name = fileDisplayName,
                target = "_blank"
            };

            string s = Wisej.Core.WisejSerializer.Serialize(downloadedObject, Wisej.Core.WisejSerializerOptions.CamelCase);
            s = Convert.ToBase64String(System.Text.Encoding.Unicode.GetBytes(s));
            string fileDownloadLink = Application.StartupUrl;
            fileDownloadLink = fileDownloadLink.EndsWith("/") ? fileDownloadLink : fileDownloadLink + "/";
            fileDownloadLink = fileDownloadLink + "download.wx?x=" + s;

            return fileDownloadLink;
        }

        /// <summary>
        /// Get Wisej Download link for a MemoryStream
        /// </summary>
        /// <param name="stream">MemoryStream to be downloaded</param>
        /// <param name="fileDisplayName">download display name</param>
        /// <returns></returns>
        public static string GetDownloadLink(MemoryStream stream, string fileDisplayName)
        {
            string tempfilePath = GetTempFileNameForTransfer(Path.GetExtension(fileDisplayName));
            StreamToFile(stream, tempfilePath);
            return GetDownloadLink(tempfilePath, fileDisplayName);
        }

        private static void StreamToFile(MemoryStream stream, string tempFilePath)
        {
            stream.Position = 0;

            using (FileStream fileStream = new FileStream(tempFilePath, FileMode.Create))
            {
                stream.CopyTo(fileStream, 1024);
            }
        }

        /// <summary>
        /// Generate temporary file for downloading the file to client
        /// </summary>
        /// <param name="extension">File extension</param>
        /// <returns>generated file name (including path)</returns>
        public static string GetTempFileNameForTransfer(string extension)
        {
            string tempFolder = Path.Combine(Application.StartupPath, "Temp", "DownloadedFiles");
            if (!Directory.Exists(tempFolder))
            {
                Directory.CreateDirectory(tempFolder);
            }
            Guid guid = Guid.NewGuid();
            string tempFileName = guid.ToString() + extension;
            string tempFilePath = Path.Combine(tempFolder, tempFileName);
            while (File.Exists(tempFilePath))
            {
                guid = Guid.NewGuid();
                tempFileName = guid.ToString() + extension;
                tempFilePath = Path.Combine(tempFolder, tempFileName);
            }
            return tempFilePath;
        }

        public static string GetTempFilename(string extension)
        {
            var tempFolder = GetTempPath();
            Guid guid = Guid.NewGuid();
            var tempFilename = guid.ToString() + extension;
            var tempFilePath = Path.Combine(tempFolder, tempFilename);
            while (File.Exists(tempFilePath))
            {
                guid = Guid.NewGuid();
                tempFilename = guid.ToString() + extension;
                tempFilePath = Path.Combine(tempFolder, tempFilename);
            }

            return tempFilePath;
        }

        public static string GetTempPath()
        {
            var tempFolder = Path.Combine(Application.StartupPath, "Temp");
            if (!Directory.Exists(tempFolder))
            {
                Directory.CreateDirectory(tempFolder);
            }

            return tempFolder;
        }

        /// <summary>
		/// Downloads a stream.
		/// </summary>
        /// <param name="stream">The stream to send to the client.</param>
		/// <param name="fileName">The file name the client will use to save the stream.</param>
        public static void Download(Stream stream, string fileName)
        {
            Application.Download(stream, fileName);
        }

        /// <summary>
		/// Downloads a file from any path.
		/// </summary>
        /// <param name="filePath">The file to download.</param>
		/// <param name="fileName">The name of the file to save on the client.</param>
        public static void Download(string filePath, string fileName = null)
        {
            if (fileName == null)
            {
                fileName = Path.GetFileName(filePath);
            }
            using (var stream = File.OpenRead(filePath))
            {
                Application.Download(stream, fileName);
            }
        }

        /// <summary>
		/// Downloads and opens a stream.
		/// </summary>
        /// <param name="target">Specifies where to open the file. Leave empty or use "_self" to open in the current tab, _blank to open in a new tab.</param>
		/// <param name="stream">The stream to send to the client.</param>
		/// <param name="fileName">The file name the client will use to save the stream.</param>
        public static void DownloadAndOpen(string target, Stream stream, string fileName)
        {
            Application.DownloadAndOpen(target, stream, fileName);
        }

        /// <summary>
		/// Downloads and opens a file from any path.
		/// </summary>
        /// <param name="target">Specifies where to open the file. Leave empty or use "_self" to open in the current tab, _blank to open in a new tab.</param>
		/// <param name="filePath">The file to download.</param>
		/// <param name="fileName">The name of the file to save on the client.</param>
        public static void DownloadAndOpen(string target, string filePath, string fileName = null)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                fileName = Path.GetFileName(filePath);
            }
            using (var stream = File.OpenRead(filePath))
            {
                Application.DownloadAndOpen(target, stream, fileName);
            }
        }

        /// <summary>
		/// Downloads and opens a file from any path.
		/// </summary>
        public static void DownloadAndOpen(string path, string fileName)
        {
            FCUtils.DownloadAndOpen("_blank", path, fileName);
        }

        /// <summary>
        /// Fix sensitive characters from file names
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string FixFileName(string fileName)
        {
            if (fileName == null)
            {
                return "";
            }
            return fileName.Replace("/", "_").Replace("\\", "_").Replace(",", "_").Replace(" ", "");
        }

        public static void CloseFormsOnProject(bool boolLoadHide = false, string formName = "MDIParent")
        {
            Assembly assembly = Assembly.GetCallingAssembly();
            CloseChildForms(boolLoadHide, formName, assembly.GetName().Name);
        }

        /// <summary>
        /// Close all child forms of the application with the exception of the MDIParent form
        /// </summary>
        /// <param name="boolLoadHide"></param>
        public static void CloseChildForms(bool boolLoadHide = false, string formName = "MDIParent", string assemblyName = "")
        {
            if (!boolLoadHide)
            {
                //add a checker to run again the inspection in case of opening a form at QueryUnload() or Unload() when unloading or closing a form
                bool ok = true;

                while (ok)
                {
                    ok = false;
                    for (int i = 0; i < FCGlobal.Statics.Forms.Count; i++)
                    {
                        //close all forms if assemblyName parameter was not provided
                        if (string.IsNullOrEmpty(assemblyName))
                        {
                            ok = true;
                            break;
                        }
                        else if (FCGlobal.Statics.Forms[i].GetType().Assembly.GetName().Name == assemblyName && FCGlobal.Statics.Forms[i].Name != formName)
                        {
                            ok = true;
                            break;
                        }
                    }

                    if (ok)
                    {
                        Stack<Form> openForms = new Stack<Form>();
                        for (int i = 0; i < FCGlobal.Statics.Forms.Count; i++)
                        {
                            if (FCGlobal.Statics.Forms[i] != null && FCGlobal.Statics.Forms[i].Name != formName)
                            {
                                openForms.Push(FCGlobal.Statics.Forms[i]);
                            }
                        }
                        while (openForms.Count > 0)
                        {
                            //close all forms if assemblyName parameter was not provided
                            bool closeForm = true;
                            //if assemblyName parameter was set, check if there are opened forms in that assembly
                            if (!string.IsNullOrEmpty(assemblyName))
                            {
                                closeForm = false;
                                if (openForms.Peek().GetType().Assembly.GetName().Name == assemblyName)
                                {
                                    closeForm = true;
                                }
                            }

                            if (closeForm)
                            {
                                if (openForms.Peek() is FCForm)
                                {
                                    ((FCForm)openForms.Pop()).Unload();
                                }
                                else
                                {
                                    openForms.Pop().Close();
                                }
                            }
                            else
                            {
                                openForms.Pop();
                            }
                        }
                    }
                }
            }
        }
    

        //FC:FINAL:MSH - i.issue #1330: In VB6 Tab() returns the spaces, number of which calculated from start of the string.
        // Remove 1 space for making formatting similar with original app formatting
        public static string Tab(int count, string resString = null)
        {
            if(resString != null)
            {
                return new String(' ', count - resString.Length - 1);
            }
            else
            {
                return new String(' ', count - 1);
            }
        }

        public static void HookUpDelegates(object listener, object instance, bool attachHandler = true)
        {
            if (instance == null)
            {
                return;
            }

            // get current instance name
            string instanceName = "";
            PropertyInfo[] listenerFields = listener.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            Type intanceType = instance.GetType();
            foreach (PropertyInfo field in listenerFields)
            {
                if (field.PropertyType == intanceType || ((System.Reflection.TypeInfo)intanceType).ImplementedInterfaces.Contains(field.PropertyType))
                {
                    instanceName = field.Name;
                    break;
                }
            }

            foreach (EventInfo eventHandler in intanceType.GetEvents())
            {
                SetupEventHandler(listener, instanceName, eventHandler.Name, instance, attachHandler);
            }
        }        

        /// <summary>
        /// fix "'Provider' is an invalid connection string attribute" - remove from ConnectionString Provider for OracleConnection
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static string RemoveProvider(string connectionString)
        {
            string input = connectionString;
            string pattern = "Provider=.*?;";
            string replacement = "";
            System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex(pattern);
            connectionString = rgx.Replace(input, replacement);
            return connectionString;
        }


		/// <summary>
		/// Round
		/// </summary>
		/// <param name="sValue"></param>
		/// <returns></returns>
        public static int Round(string sValue)
        {
            double dValue = Convert.ToDouble(sValue);
            return Convert.ToInt32(Math.Round(dValue, 0));
        }

        /// <summary>
		/// Round
		/// </summary>
		/// <param name="sValue"></param>
		/// <returns></returns>
        public static double Round(string sValue, int numdecimalplaces)
        {
            double dValue = Convert.ToDouble(sValue);
            return Math.Round(dValue, numdecimalplaces);
        }

        /// <param name="value"></param>
		/// <returns></returns>
		public static double Round(double value, int numdecimalplaces = 0)
        {
            return Math.Round(value, numdecimalplaces);
        }

        public static double Round (decimal value, int numdecimalplaces = 0)
        {
            return (double) Math.Round(value, numdecimalplaces);
        }

        public static decimal ParseOrDefault(string str, decimal @default = 0.0M)
        {
            decimal result;
            if (decimal.TryParse(str, out result))
            {
                return result;
            }

            if (decimal.TryParse(str.Replace(',', '.'), out result))
            {
                return result;
            }

            return @default;
        }

        /// <summary>
        /// Fix
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static int Fix(double d)
		{
			double ret = d < 0 ? Math.Ceiling(d) : Math.Floor(d);
			return Convert.ToInt32(ret);
		}

        //FC:FINAL:DSE:#878 Random should generate different numbers each time is called. If reinitialized each time, it will generate same number
        private static Random random = new Random(0);
        private static int lastRandomSeed = 0;

        /// <summary>
        ///  Returns a nonnegative random number.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static float Rnd(int number = 0)
        {
            //return value less than 1
            if(lastRandomSeed != number)
            {
                random = new Random(number);
                lastRandomSeed = number;
            }
            return Convert.ToSingle(random.NextDouble());
        }

        public static int iDiv(double a, double b)
        {
            int ia = Convert.ToInt32(Math.Round(a));
            int ib = Convert.ToInt32(Math.Round(b));
            return ia / ib;
        }
        public static int iDiv(double a, int ib)
        {
            int ia = Convert.ToInt32(Math.Round(a));
            return ia / ib;
        }
        public static int iDiv(int ia, double b)
        {
            int ib = Convert.ToInt32(Math.Round(b));
            return ia / ib;
        }
		public static int iDiv(int ia, int ib)
		{
			return ia / ib;
		}

        public static int iMod(double a, double b)
        {
            int ia = Convert.ToInt32(Math.Round(a));
            int ib = Convert.ToInt32(Math.Round(b));
            return ia % ib;
        }
		public static int iMod(double a, int ib)
		{
			int ia = Convert.ToInt32(Math.Round(a));
			return ia % ib;
		}

        public static int iDiv(decimal a, int b)
        {
            int ia = Convert.ToInt32(Math.Round(a));
            return ia / b;
        }

        /// <summary>
        /// GetCommandLine
        /// </summary>
        /// <returns></returns>
        public static string GetCommandLine()
		{
			string CommandLine = "";
			string[] arguments = Environment.GetCommandLineArgs();
			for (int i = 1; i < arguments.Length; i++)
			{
				if (i != 1)
				{
					CommandLine += " ";
				}
				bool flSpace = arguments[i].Contains(" ");
				if (flSpace)
				{
					CommandLine += '"';
				}
				CommandLine += arguments[i];
				if (flSpace)
				{
					CommandLine += '"';
				}
			}
			return CommandLine;
		}

		/// <summary>
		/// GetByteFromString
		/// </summary>
		/// <param name="Buf"></param>
		/// <returns></returns>
		public static IntPtr GetByteFromString(String Buf)
		{
			return Marshal.StringToHGlobalAnsi(Buf);
		}

		/// <summary>
		/// GetStringFromByte
		/// </summary>
		/// <param name="Buf"></param>
		/// <param name="pBuf"></param>
		public static void GetStringFromByte(ref String Buf, IntPtr pBuf)
		{
			Buf = Marshal.PtrToStringAnsi(pBuf);
		}

        /// <summary>
        /// Copies a structure of type T into a byte array
        /// </summary>
        /// <typeparam name="T">generic type for structure</typeparam>
        /// <param name="structure">structure object</param>
        /// <returns>byte array with data from struct</returns>
        public static byte[] GetByteFromStruct<T>(T structure) where T : struct
        {

            byte[] byteArray = new byte[Marshal.SizeOf(structure)];
            var byteArrayPtr = GCHandle.Alloc(byteArray, GCHandleType.Pinned);
            try {
                Marshal.StructureToPtr(structure, byteArrayPtr.AddrOfPinnedObject(), true);
            }
            finally {
                byteArrayPtr.Free();
            }
            return byteArray;
        }

        /// <summary>
        /// Copies peace of unmanaged memory (given by an IntPtr) into a structure of type T 
        /// </summary>
        /// <typeparam name="T">generic type for structure (normally infered by compiler)</typeparam>
        /// <param name="structure">ref to structure</param>
        /// <param name="source">IntPtr to allocated unmanaged memory</param>
        public static void GetStructFromByte<T>(ref T structure, IntPtr source) where T : struct
        {
            structure = Marshal.PtrToStructure<T>(source);
        }

        /// <summary>
        /// Returns fixed string from array of char
        /// </summary>
        /// <param name="charArray"></param>
        /// <returns></returns>
        public static string FixedStringFromArray(char[] charArray)
        {
            if (charArray != null)
            {
                return new string(charArray);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// return array of char from string
        /// </summary>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        public static char[] FixedStringToArray(string stringValue, int length)
        {
	        if (stringValue.Length > length)
	        {
		        return stringValue.Substring(0, length).ToArray();
			}
	        else
	        {
		        return stringValue.PadRight(length).ToArray();
			}
        }

        /// <summary>
        /// IsNull
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static Boolean IsNull(Object Obj)
		{
			return Obj == null || Obj == DBNull.Value;
		}

        /// <summary>
        /// Check if dateTime value is empty (0 in VB6)
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static bool IsEmptyDateTime(DateTime dateTime)
        {
            return dateTime == DateTime.FromOADate(0) || (dateTime.Year == 1900 && dateTime.Month == 1 && dateTime.Day == 1);
        }

        /// <summary>
		/// Check if both Parameter are not null and different
		/// </summary>
		/// <param name="Obj"></param>
		/// <returns>true if </returns>
		public static Boolean CheckNotEqualNotNull(Object Obj1, Object Obj2)
        {
            if (Obj1 != null && Obj2 != null && !Obj1.Equals(Obj2))
            {
                return true;
            }
            return false;
        }

        public static bool IsEmpty(object obj)
        {
            return false;   //? obj == null;
        }

        //ASZ: "dim Wert as Variant, If Wert <> Empty Then" calculates to True if Wert is (int)0
        public static bool EqualsEmpty(object obj)
        {
            switch (Type.GetTypeCode(obj?.GetType()))
            {
                case TypeCode.String:
                    return (string)obj == String.Empty;

                case TypeCode.DateTime:
                    return (DateTime)obj == DateTime.FromOADate(0);

                case TypeCode.Boolean:
                    return (bool)obj == default(bool);

                case TypeCode.Double:
                case TypeCode.Single:
                    return Convert.ToDouble(obj) == 0;

                case TypeCode.Byte:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                    return Convert.ToInt32(obj) == 0;

                case TypeCode.Empty:
                    return IsNull(obj);

                default:
                {
                    throw new NotImplementedException();
                    return false;
                }
            }
        }

        public static int ObjectCompare(object objA, object objB)
        {
            if ((objA == null || objA == DBNull.Value) && (objB == null || objB == DBNull.Value))
            {
                return 0;
            }
            else if (objA == null || objA == DBNull.Value)
            {
                return -1;
            }
            else if (objB == null || objB == DBNull.Value)
            {
                return +1;
            }
            else
            {
                IComparable iA = objA as IComparable;
                IComparable iB = objB as IComparable;
                if (iA != null && iB != null)
                {
                    if (iA.GetType() == iB.GetType())
                    {
                        return iA.CompareTo(iB);
                    }
                    else
                    {
                        return ObjectEqualExtended(iA, iB);
                    }
                }
                else
                {
                    throw new NotImplementedException();
                    return 0;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objA"></param>
        /// <param name="objB"></param>
        /// <returns></returns>
        public static bool ObjectEqual(object objA, object objB)
        {
            return ObjectCompare(objA, objB) == 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objA"></param>
        /// <param name="objB"></param>
        /// <returns></returns>
        public static int ObjectEqualExtendedVisVersa(object objA, object objB)
        {
            int retVal = -1;
            retVal = ObjectEqualExtended(objA, objB);
            if (retVal != 0)
            {
                retVal = ObjectEqualExtended(objB, objA);
            }
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objA"></param>
        /// <param name="objB"></param>
        /// <returns></returns>
        public static int ObjectEqualExtended(object objA, object objB)
        {
            int retVal = -1;
            if (objA.GetType() == typeof(string))
            {
                switch (Type.GetTypeCode(objB.GetType()))
                {
                    case TypeCode.Double:
                        retVal = FCConvert.ToDouble(objA).CompareTo(objB);
                        break;
                    case TypeCode.Int32:
                        retVal = FCConvert.ToInt32(objA).CompareTo(objB);
                        break;
                    default:
                        break;
                }
            }
            return retVal;
        }

        public static bool IsNumeric(this Type type)
        {
            List<Type> numericTypes = 
                new List<Type>() { typeof(Byte), typeof(Decimal), typeof(Double),
                typeof(Int16), typeof(Int32), typeof(Int64), typeof(SByte),
                typeof(Single), typeof(UInt16), typeof(UInt32), typeof(UInt64)};
            return numericTypes.Contains(type);
        }

        /// <summary>
        /// TextWidth
        /// </summary>
        /// <param name="C"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        public static float TextWidth(Control C, string str)
		{
			SizeF sf = Graphics.FromHwnd(C.Handle).MeasureString(str, C.Font);
			return sf.Width;
		}

		/// <summary>
		/// CBool
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public static bool CBool(string s)
		{
			if (s.Length <= 0) return false;
			char ch = s[0];
			if (ch == 'T' || ch == 't') return true;
			if (ch == 'F' || ch == 'f') return false;
			return Conversion.Val(s) != 0;
		}

		/// <summary>
		/// LoadPicture
		/// </summary>
		/// <param name="file_name"></param>
		/// <returns></returns>
		public static Image LoadPicture(string file_name)
		{
			Bitmap src = new Bitmap(file_name);
			Bitmap dst = new Bitmap(src.Width, src.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            dst.MakeTransparent();
            using (Graphics g = Graphics.FromImage(dst))
            {
                g.DrawImage(src, 0, 0, src.Width, src.Height);
            }
            src.Dispose();
            return dst;
		}

        public static byte[] ResizeImageAsJpeg(byte[] imageData, int maxWidthInPixels, int maxHeightInPixels)
        {
            using (MemoryStream ms = new MemoryStream(imageData, 0, imageData.Length))
            {
                using (Image img = Image.FromStream(ms))
                {
                    int newWidth = img.Width;
                    int newHeight = img.Height;
                    if (img.Width > maxWidthInPixels || img.Height > maxHeightInPixels)
                    {
                        var ratio = Convert.ToDouble(img.Width) / Convert.ToDouble(img.Height);
                        newWidth = maxWidthInPixels;
                        newHeight = maxHeightInPixels;

                        if (maxWidthInPixels / ratio > maxHeightInPixels)
                        {
                            newWidth = (int)(maxHeightInPixels * ratio);
                        }
                        else
                        {
                            newHeight = (int) (maxWidthInPixels / ratio);
                        }   
                    }

                    EncoderParameters encoderParameters = new EncoderParameters(1);
                    ImageCodecInfo imageCodecInfo = ImageCodecInfo.GetImageEncoders().FirstOrDefault(e => e.MimeType == "image/jpeg");
                    Encoder imageEncoder = Encoder.Quality;
                    EncoderParameter encoderParameter = new EncoderParameter(imageEncoder, 90L);
                    encoderParameters.Param[0] = encoderParameter;
                    using (Bitmap b = new Bitmap(img, new Size(newWidth, newHeight)))
                    {
                        using (MemoryStream destinationStream = new MemoryStream())
                        {
                            b.Save(destinationStream, imageCodecInfo,encoderParameters);
                            return destinationStream.ToArray();
                        }
                    }

                }
            }
        }
        public static Image PictureFromBytes(byte[] imageData)
        {
            using (MemoryStream stream = new MemoryStream(imageData))
            {
                var src =  Image.FromStream(stream);
                Bitmap dst = new Bitmap(src.Width, src.Height, PixelFormat.Format24bppRgb);
                dst.MakeTransparent();
                
                using (Graphics g = Graphics.FromImage(dst))
                {
                    g.DrawImage(src,0,0,src.Width,src.Height);
                }
                src.Dispose();
                return dst;
            }
        }

        public static Image PictureFromBytes(byte[] imageData, float widthInInches, float heightInInches)
        {
            using (MemoryStream stream = new MemoryStream(imageData))
            {
                var src = Image.FromStream(stream);
                int maxWidth = (int) (widthInInches * src.HorizontalResolution);
                int maxHeight = (int)(heightInInches * src.VerticalResolution);
                int picWidth = maxWidth;
                int picHeight = maxHeight;                
                float ratio = Convert.ToSingle(src.Width) / Convert.ToSingle(src.Height);
                if (widthInInches / ratio > heightInInches)
                {
                    picWidth =(int)((heightInInches * ratio) * src.HorizontalResolution);
                }
                else
                {
                    picHeight = (int)((widthInInches / ratio) * src.VerticalResolution);
                }

                if (picWidth > maxWidth)
                {
                    picWidth = maxWidth;
                }

                if (picHeight > maxHeight)
                {
                    picHeight = maxHeight;
                }
                Bitmap dst = new Bitmap(picWidth, picHeight, PixelFormat.Format24bppRgb);
                
                dst.MakeTransparent();
                
                using (Graphics g = Graphics.FromImage(dst))
                {
                    g.DrawImage(src, 0, 0, picWidth, picHeight);
                }
                src.Dispose();
                return dst;
            }
        }

		/// <summary>
        /// get cursor 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Wisej.Web.Cursor GetTranslatedCursor(MousePointerConstants value)
        {
            Wisej.Web.Cursor c = null;
            switch (value)
            {
                case MousePointerConstants.vbDefault:
                    c = Cursors.Default;
                    break;
                case MousePointerConstants.vbArrow:
                    c = Cursors.Arrow;
                    break;
                case MousePointerConstants.vbCrosshair:
                    c = Cursors.Cross;
                    break;
                case MousePointerConstants.vbIbeam:
                    c = Cursors.IBeam;
                    break;
                case MousePointerConstants.vbArrowHourglass:
                    //c = Cursors.AppStarting;
                    break;
                case MousePointerConstants.vbIconPointer:
                    c = Cursors.Default;
                    break;
                case MousePointerConstants.vbSizePointer:
                    c = Cursors.SizeAll;
                    break;
                case MousePointerConstants.vbSizeNESW:
                    c = Cursors.SizeNESW;
                    break;
                case MousePointerConstants.vbSizeWE:
                    c = Cursors.SizeWE;
                    break;
                case MousePointerConstants.vbHourglass:
                    c = Cursors.WaitCursor;
                    break;
                case MousePointerConstants.vbNoDrop:
                    c = Cursors.No;
                    break;
                case MousePointerConstants.vbArrowQuestion:
                    c = Cursors.Help;
                    break;
                case MousePointerConstants.vbSizeAll:
                    c = Cursors.SizeAll;
                    break;
                case MousePointerConstants.vbCustom:
                    c = Cursors.Default;
                    break;
                default:
                    c = Cursors.Default;
                    break;
            }

            return c;
        }

        /// <summary>
        /// get the original height of the MainMenu if the current form has one
        /// </summary>
        /// <param name="ctrl"></param>
        /// <returns></returns>
        public static int GetMainMenuHeightOriginal(Control ctrl)
        {
            //CHE: consider MainMenu in fecherFoundation (in VB6 it's height is not included when considering Form Height/ScaleHeight or any Top value of the controls directly on the form)
            if (ctrl == null)
            {
                return 0;
            }
            int height = 0;
            Form form = null;
            if (ctrl is Form)
            {
                form = ctrl as Form;
            }
            else
            {
                form = ctrl.Parent as FCForm;
            }
            if (form != null && form.Menu != null)
            {
                FCMenuStrip menu = form.Menu as FCMenuStrip;
                if (menu != null)
                {
                    //if the menu is not visible, return a Height of 0. 
                    if (!menu.Visible)
                    {
                        return 0;
                    }
                    // TODO
                    //return menu.HeightOriginal;
                }
            }
            return height;
        }

        /// <summary>
        /// only dispose the fonts created by the application
        /// </summary>
        /// <param name="font"></param>
        public static void DisposeFont(Control control, Font font)
        {
            if (createdFonts.Contains(font))
            {
                //check if the font is inherited or not
                bool isInherited = CheckIsInherited(control, font);

                if (!isInherited)
                {
                    createdFonts.Remove(font);
                    //font.Dispose();
                }
            }
        }

        /// <summary>
        /// add font in list of created fonts
        /// </summary>
        /// <param name="font"></param>
        public static void AddFont(Font font)
        {
            if (!createdFonts.Contains(font))
            {
                createdFonts.Add(font);
            }
        }

        /// <summary>
        /// Returns true if the current application has focus, false otherwise
        /// </summary>
        /// <returns></returns>
        public static bool ApplicationIsActivated()
        {
            var activatedHandle = Externals.USER32.GetForegroundWindow();
            if (activatedHandle == IntPtr.Zero)
            {
                return false;       // No window is currently activated
            }

            var procId = Process.GetCurrentProcess().Id;
            int activeProcId;
            Externals.USER32.GetWindowThreadProcessId(activatedHandle, out activeProcId);
            return activeProcId == procId;
        }

        /// <summary>
        /// get original name, after translation name may contain index too
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public static string GetOriginalControlName(object control)
        {
            string name = "";
            int index = -1;
            Control ctrl = control as Control;
            if (ctrl != null)
            {
                name = ctrl.Name;
                index = ComponentExtension.GetIndex(ctrl);
            }
            else
            {
                FCToolStripMenuItem menu = control as FCToolStripMenuItem;
                if (menu != null)
                {
                    name = menu.Name;
                    // TODO
                    //index = ComponentExtension.GetIndex(menu);
                }
            }
            if (index != -1 && name.EndsWith("_" + index.ToString()))
            {
                name = name.Substring(0, name.Length - index.ToString().Length - 1);
            }
            return name;
        }

        /// <summary>
        /// Get the first Filename of a DragEventArgs - Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static string GetDragDropImageName(Wisej.Web.DragEventArgs e)
        {
            string fileName = string.Empty;
            string[] files = (string[])(e.Data.GetData(DataFormats.FileDrop));
            if (files != null && files.Length > 0)
            {
                fileName = files[0];
            }
            return fileName;
        }

        /// <summary>
        /// Gets the Tag ob sender as Control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static object GetTag(object sender)
        {
            object snd = sender as Control;
            object retValue = null;
            if (sender as Control != null)
            {
                retValue = ((Control)sender).Tag;
            }
            return retValue;
        }

        /// <summary>
        /// get image handle
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        public static IntPtr GetImageHandle(Image img)
        {
            if (img != null)
            {
                return ((System.Drawing.Bitmap)img).GetHbitmap();
            }
            return IntPtr.Zero;
        }

        /// <summary>
        /// create font
        /// </summary>
        /// <param name="name"></param>
        /// <param name="size"></param>
        /// <param name="style"></param>
        /// <returns></returns>
        public static Font CreateFont(string name, float size, FontStyle style, bool isPrinter = false)
        {
            Font font = new Font(name, GetActualPoints(size, isPrinter), style);
            FCUtils.AddFont(font);
            return font;
        }

        /// <summary>
        /// create font
        /// </summary>
        /// <param name="fontFamily"></param>
        /// <param name="size"></param>
        /// <param name="style"></param>
        /// <returns></returns>
        public static Font CreateFont(FontFamily fontFamily, float size, FontStyle style, bool isPrinter = false)
        {
            Font font = new Font(fontFamily, GetActualPoints(size, isPrinter), style);
            FCUtils.AddFont(font);
            return font;
        }

        /// <summary>
        /// create font
        /// </summary>
        /// <param name="fontFamily"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Font CreateFont(FontFamily fontFamily, float size, bool isPrinter = false)
        {
            Font font = new Font(fontFamily, GetActualPoints(size, isPrinter));
            FCUtils.AddFont(font);
            return font;
        }

        /// <summary>
        /// TODO: Implement Code
        /// </summary>
        /// <param name="font"></param>
        /// <param name="fontStyle"></param>
        /// <param name="bSet"></param>
        /// <returns></returns>
        public static Font SetFontStyle(Font font, FontStyle fontStyle, bool bSet)
        {
            if (bSet)
            {
                return new Font(font, fontStyle);
            }
            else
            {
                return font;
            }
        }

        /// <summary>
        /// get icon from object
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Icon GetIconFromObject(object value)
        {
            var icon = value as Icon;

            try
            {
                // If the value is an icon, return it, otherwise try to convert it
                if (icon != null) return icon;

                try
                {
                    if (value is Bitmap bmp)
                    {
                        // Get the icon from the bitmap
                        var cargt = new[] { typeof(IntPtr), typeof(bool) };
                        var ci = typeof(Icon).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, cargt, null);
                        var cargs = new[] { (object)bmp.GetHicon(), true };
                        icon = (Icon)ci?.Invoke(cargs);
                    }
                }
                catch (Exception ex)
                {
                    icon = null;
                }

                return icon;
            }
            finally
            {
                icon?.Dispose();
            }
        }

        /// <summary>
        /// Load an control array item.
        /// </summary>
        /// <param name="list">The list to initialize.</param>
        /// <param name="index">The index to initialize.</param>
        /// <returns></returns>
        public static void Load<T>(List<T> list, int index) where T : System.ComponentModel.Component
        {
            // If there is a valid list and index
            if (list != null && index >= 0)
            {
                // If we need to add items to list
                while (list.Count <= index)
                {
                    // Add items
                    list.Add(default(T));
                }

                // Create control instance
                T control = Activator.CreateInstance<T>();

                //set parent and events
                Control newControl = control as Control;
                if (newControl != null)
                {
                    for (int i = index - 1; i >= 0; i--)
                    {
                        //CHE: get first control from list, not the previous one
                        Control otherControlFromList = null;
                        for (int j = 0; j < index; j++)
                        {
                            if (list[j] != null)
                            {
                                otherControlFromList = list[j] as Control;
                                break;
                            }
                        }
                        if (otherControlFromList != null)
                        {
                            Control parent = otherControlFromList.Parent;
                            if (parent != null)
                            {
                                //set parent
                                //parent.Controls.Add(newControl);

                                inCloneControlProperties = true;

                                //Each control that is added with Load function, initially has the same properties as the original control
                                //copy control properties before attaching event handlers (setting some properties might raise some events)
                                //FC:FINAL:AM - Harris #i593 - no need to clone the properties
                                //CloneControlProperties(otherControlFromList, newControl);

                                inCloneControlProperties = false;

                                //set events
                                CopyEventHandlers copyHelper = new CopyEventHandlers();
                                copyHelper.GetHandlersFrom(otherControlFromList).CopyTo(newControl);

                                //set name
                                string originalName = FCUtils.GetOriginalControlName(otherControlFromList);
                                newControl.Name = originalName + "_" + index;
                                parent.Controls.Add(newControl);
                                //CHE: set ZOrder after the other control
                                int position = parent.Controls.GetChildIndex(otherControlFromList) + 1;
                                parent.Controls.SetChildIndex(newControl, position);
                            }
                            break;
                        }
                    }
                }

                // Set the control at the given index
                list[index] = control;
                control.SetControlArray(list);
            }
        }

        /// <summary>
        /// Load an control array item.
        /// </summary>
        /// <param name="list">The list to initialize.</param>
        /// <param name="index">The index to initialize.</param>
        /// <returns></returns>
        public static void Unload<T>(List<T> list, int index) where T : System.ComponentModel.Component
        {
            // If there is a valid list and index
            if (list != null && index >= 0)
            {
                Control control = list[index] as Control;
                control.Parent.Controls.Remove(control);
                control.Dispose();
                control = null;
                // Set the control at the given index
                list[index] = null;
                list.RemoveAt(index);
            }
        }

        /// <summary>
        /// add new menu item to the array list, set it's parent and event handler as other children
        /// </summary>
        /// <param name="list"></param>
        /// <param name="index"></param>
        /// <param name="parent"></param>
        public static void LoadMenuItem(List<object> list, int index, FCToolStripMenuItem parent, System.EventHandler clickHandler)
        {
            // If there is a valid list and index
            if (list != null && index >= 0)
            {
                // If we need to add items to list
                while (list.Count <= index)
                {
                    // Add items
                    list.Add(default(FCToolStripMenuItem));
                }

                // Create control instance
                FCToolStripMenuItem menuItem = new FCToolStripMenuItem();

                //set parent
                if (parent != null)
                {
                    parent.MenuItems.Add(menuItem);
                }

                //set events
                menuItem.Click += clickHandler;

                System.ComponentModel.Component otherMenuFromList = list[index - 1] as System.ComponentModel.Component;
                if (otherMenuFromList != null)
                {
                    //set name
                    string originalName = FCUtils.GetOriginalControlName(otherMenuFromList);
                    menuItem.Name = originalName + "_" + index;
                }

                // Set the control at the given index
                list[index] = menuItem;
                // TODO
                //menuItem.SetControlArray(list);
            }
        }

        /// <summary>
        /// load
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="list"></param>
        /// <param name="index"></param>
        // TODO
        //public static void Load(object owner, Wisej.Web.ToolStripItemCollection list, int index)
        //{
        //    Load(owner, list, index, new FCToolStripMenuItem());
        //}

        /// <summary>
        /// load
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="list"></param>
        /// <param name="index"></param>
        /// <param name="item"></param>
        // TODO
        //public static void Load(object owner, Wisej.Web.ToolStripItemCollection list, int index, Wisej.Web.ToolStripItem item)
        //{
        //    // If there is a valid list and index
        //    if (list != null && index >= 0)
        //    {
        //        // If we need to add items to list
        //        while (list.Count <= index)
        //        {
        //            // Add items
        //            list.Add(item);
        //        }
        //    }
        //}

        /// <summary>
        /// unload
        /// </summary>
        /// <param name="item"></param>
        // TODO
        //public static void Unload(MenuItem item)
        //{
        //    if (item != null)
        //    {
        //        FCToolStripMenuItem parent = item.OwnerItem as FCToolStripMenuItem;
        //        if (parent != null && parent.MenuItems.IndexOf(item) > 0)
        //        {
        //            parent.MenuItems.Remove(item);
        //            //also remove from Control Array
        //            List<object> list = item.GetControlArray() as List<object>;
        //            if (list != null)
        //            {
        //                int index = list.IndexOf(item);
        //                list.Remove(item);
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Unloads a form or control from memory.
        /// Syntax
        /// Unload object
        /// The object placeholder is the name of a Form object or control array element to unload.
        /// Remarks
        /// Unloading a form or control may be necessary or expedient in some cases where the memory used is needed for something else, or when you need to reset properties to their original values.
        /// Before a form is unloaded, the Query_Unload event procedure occurs, followed by the Form_Unload event procedure. Setting the cancel argument to True in either of these events prevents the form 
        /// from being unloaded. For MDIForm objects, the MDIForm object's Query_Unload event procedure occurs, followed by the Query_Unload event procedure and Form_Unload event procedure for each MDI child 
        /// form, and finally the MDIForm object's Form_Unload event procedure.
        /// When a form is unloaded, all controls placed on the form at run time are no longer accessible. Controls placed on the form at design time remain intact; however, any run-time changes to those 
        /// controls and their properties are lost when the form is reloaded. All changes to form properties are also lost. Accessing any controls on the form causes it to be reloaded.
        /// Note   When a form is unloaded, only the displayed component is unloaded. The code associated with the form module remains in memory.
        /// Only control array elements added to a form at run time can be unloaded with the Unload statement. The properties of unloaded controls are reinitialized when the controls are reloaded.
        /// </summary>
        /// <param name="control"></param>
        public static void Unload(object control)
        {
            if (control == null)
            {
                return;
            }
            //CNA: unload ToolStripItem
            MenuItem toolStripItem = control as MenuItem;
            if (toolStripItem != null)
            {
                Unload(toolStripItem);
                return;
            }
            //CHE: unload form
            FCForm frm = control as FCForm;
            if (frm != null)
            {
                frm.Unload();
                return;
            }
            control = null;
        }

        /// <summary>
        /// Loads a form or control into memory.
        /// Syntax
        /// Load object
        /// The object placeholder is the name of a Form object, MDIForm object, or control array element to load.
        /// Remarks
        /// You don't need to use the Load statement with forms unless you want to load a form without displaying it. Any reference to a form (except in a Set or If...TypeOf statement) automatically loads 
        /// it if it's not already loaded. For example, the Show method loads a form before displaying it. Once the form is loaded, its properties and controls can be altered by the application, whether or 
        /// not the form is actually visible. Under some circumstances, you may want to load all your forms during initialization and display them later as they're needed.
        /// When Visual Basic loads a Form object, it sets form properties to their initial values and then performs the Load event procedure. When an application starts, Visual Basic automatically loads 
        /// and displays the application's startup form.
        /// If you load a Form whose MDIChild property is set to True (in other words, the child form) before loading an MDIForm, the MDIForm is automatically loaded before the child form. MDI child forms 
        /// cannot be hidden, and thus are immediately visible after the Form_Load event procedure ends.
        /// The standard dialog boxes produced by Visual Basic functions such as MsgBox and InputBox do not need to be loaded, shown, or unloaded, but can simply be invoked directly.
        /// </summary>
        /// <param name="control"></param>
        public static void Load(object control)
        {
            FCForm frm = control as FCForm;
            if (frm != null)
            {
                frm.Show();
                return;
            }
        }

        /// <summary>
        /// Loads a graphic into a forms Picture property, a PictureBox control, or an Image control.
        /// Syntax
        /// LoadPicture([filename], [size], [colordepth],[x,y])
        /// The LoadPicture function syntax has these parts:
        /// Part	Description
        /// filename	Optional. String expression specifying a filename. Can include folder and drive. If no filename is specified LoadPicture clears the Image or PictureBox control.
        /// size	Optional variant. If filename is a cursor or icon file, specifies the desired image size.
        /// colordepth	Optional variant. If filename is a cursor or icon file, specifies the desired color depth.
        /// x	Optional variant, required if y is used. If filename is a cursor or icon file, specifies the width desired. In a file containing multiple separate images, the best possible match 
        /// is used if an image of that size is not available. X and y values are only used when colordepth is set to vbLPCustom. For icon files 255 is the maximum possible value.
        /// y	Optional variant, required if x is used. If filename is a cursor or icon file, specifies the height desired. In a file containing multiple separate images, the best possible match 
        /// is used if an image of that size is not available. For icon files 255 is the maximum possible value.
        /// Settings
        /// The settings for size are:
        /// Constant	Value	Description
        /// vbLPSmall	0	System small icon.
        /// vbLPLarge	1	System large icon size, as determined by the video driver.
        /// vbLPSmallShell	2	Shell small icon size, as determined by the Caption Buttons size setting on the Appearance tab in the Control Panel Display Properties dialog box.
        /// vbLPLargeShell	3	Shell large icon size, as determined by the Icon size setting on the Appearance tab in the Control Panel Display Properties dialog box.
        /// vbLPCustom	4	Custom size, values provided by x and y arguments
        /// The settings for colordepth are:
        /// Constant	Value	Description
        /// vbLPDefault	0	Best available match if the specified file is used.
        /// vbLPMonochrome	1	2 colors.
        /// vbLPVGAColor	2	16 colors.
        /// vbLPColor	3	256 colors.
        /// Remarks
        /// Graphics formats recognized by Visual Basic include bitmap (.bmp) files, icon (.ico) files, cursor (.cur) files, run-length encoded (.rle) files, metafile (.wmf) files, enhanced 
        /// metafiles (.emf), GIF (.gif) files, and JPEG (.jpg) files.
        /// Graphics are cleared from forms, picture boxes, and image controls by assigning LoadPicture with no argument.
        /// To load graphics for display in a PictureBox control, Image control, or as the background of a form, the return value of LoadPicture must be assigned to the Picture property of the object 
        /// on which the picture is displayed. For example:
        /// Set Picture = LoadPicture("PARTY.BMP")
        /// Set Picture1.Picture = LoadPicture("PARTY.BMP")
        /// To assign an icon to a form, set the return value of the LoadPicture function to the Icon property of the Form object:
        /// Set Form1.Icon = LoadPicture("MYICON.ICO")
        /// Icons can also be assigned to the DragIcon property of all controls except Timer controls and Menu controls. For example:
        /// Set Command1.DragIcon = LoadPicture("MYICON.ICO")
        /// Load a graphics file into the system Clipboard using LoadPicture as follows:
        /// Clipboard.SetData LoadPicture("PARTY.BMP")
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="size"></param>
        /// <param name="colorDepth"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static Image LoadPicture(string fileName = "", PictureSizeConstants size = PictureSizeConstants.vbLPSmall, ColorDepthConstants colorDepth = ColorDepthConstants.vbLPDefault, object x = null, object y = null)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                {
                    return null;
                }
            }
            var src = Image.FromFile(fileName);
            var dst = new System.Drawing.Bitmap(src.Width, src.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            Graphics.FromImage(dst).DrawImage(src, 0, 0, src.Width, src.Height);
            src.Dispose();
            return dst;
        }

        /// <summary>
        /// Saves a graphic from the Picture or Image property of an object or control (if one is associated with it) to a file.
        /// Syntax
        /// SavePicture picture, stringexpression
        /// The SavePicture statement syntax has these parts:
        /// Part	Description
        /// picture	Picture or Image control from which the graphics file is to be created.
        /// stringexpression	Filename of the graphics file to save.
        /// Remarks
        /// If a graphic was loaded from a file to the Picture property of an object, either at design time or at run time, and its a bitmap, icon, metafile, or enhanced metafile, 
        /// it's saved using the same format as the original file. If it is a GIF or JPEG file, it is saved as a bitmap file.
        /// Graphics in an Image property are always saved as bitmap (.bmp) files regardless of their original format.
        /// </summary>
        /// <param name="image"></param>
        /// <param name="fileName"></param>
        public static void SavePicture(Image image, string fileName)
        {
            image.Save(fileName);
        }

        /// <summary>
        /// Moves an MDIForm, Form, or control. Doesn't support named arguments.
        /// Syntax
        /// object.Move left, top, width, height
        /// The Move method syntax has these parts:
        /// Part	Description
        /// object	Optional. An object expression that evaluates to an object in the Applies To list. If object is omitted, the form with the focus is assumed to be object.
        /// left	Required. Single-precision value indicating the horizontal coordinate (x-axis) for the left edge of object.
        /// top	Optional. Single-precision value indicating the vertical coordinate (y-axis) for the top edge of object.
        /// width	Optional. Single-precision value indicating the new width of object.
        /// height	Optional. Single-precision value indicating the new height of object.
        /// Remarks
        /// Only the left argument is required. However, to specify any other arguments, you must specify all arguments that appear in the syntax before the argument you want to specify. 
        /// For example, you can't specify width without specifying left and top. Any trailing arguments that are unspecified remain unchanged.
        /// For forms and controls in a Frame control, the coordinate system is always in twips. Moving a form on the screen or moving a control in a Frame is always relative to the origin (0,0), 
        /// which is the upper-left corner. When moving a control on a Form object or in a PictureBox (or an MDI child form on an MDIForm object), the coordinate system of the container object 
        /// is used. The coordinate system or unit of measure is set with the ScaleMode property at design time. You can change the coordinate system at run time with the Scale method.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="leftOriginal"></param>
        /// <param name="topOriginal"></param>
        /// <param name="widthOriginal"></param>
        /// <param name="heightOriginal"></param>
        public static void MoveTo(dynamic control, int leftOriginal, int topOriginal, int widthOriginal = -1, int heightOriginal = -1)
        {
            if (topOriginal != -1)
            {
                control.TopOriginal = topOriginal;
            }
            control.LeftOriginal = leftOriginal;

            if (widthOriginal != -1)
            {
                control.WidthOriginal = widthOriginal;
            }
            if (heightOriginal != -1)
            {
                control.HeightOriginal = heightOriginal;
            }
        }

        /// <summary>
        /// GetAllControls - extension not working for object
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        public static List<Control> GetAllControls(Control container)
        {
            return container.GetAllControls();
        }

        public static void ArrayClear(Array array, int index, int length)
        {

            Array.Clear(array, index, length);

            if (array.Rank == 1)
                if (array.GetType().GetElementType() == typeof(string))
                {
                    for (int i = 0; i < length; i++)
                    {
                        array.SetValue("", i);
                    }
                }
            //FC:FINAL:DSE:#842 Clear a date field means putting 0 and not 1/1/0001
                else if(array.GetType().GetElementType() == typeof(DateTime))
                {
                    for(int i = 0; i < length; i++)
                    {
                        array.SetValue(DateTime.FromOADate(0), i);
                    }
                }           
        }

        public static void EraseSafe(Array array)
        {
            if (array == null)
            {
                return;
            }

            ArrayClear(array, 0, array.Length);

        }

        public static byte[] GetBytes(string str)
        {
            byte[] bytes = null;
            if (!string.IsNullOrEmpty(str))
            {
                bytes = new byte[str.Length * sizeof(char)];
                System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            }
            return bytes;
        }

        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        //CHE: GDI32
        public static bool PolygonGDI32(dynamic ctrl, Point[] lpPoints, int nCount)
        {
            bool retValue = false;
            FCPictureBox pict = ctrl as FCPictureBox;
            if (pict != null)
            {
                pict.FillPolygon(lpPoints, nCount);
            }
            else
            {
                retValue = Externals.GDI32.Polygon(ctrl.hdc, lpPoints, nCount);
            }
            return retValue;
        }

        //CHE: GDI32
        public static bool LineToGDI32(dynamic ctrl, int nXEnd, int nYEnd)
        {
            bool retValue = false;
            FCPictureBox pict = ctrl as FCPictureBox;
            if (pict != null)
            {
                retValue = Externals.GDI32.LineTo(pict.hDC, nXEnd, nYEnd);
                pict.ReleaseCurrentGraphicshDc();
            }
            else
            {
                retValue = Externals.GDI32.LineTo(ctrl.hdc, nXEnd, nYEnd);
            }
            return retValue;
        }

        //CHE: GDI32
        public static bool LineToGDI32(dynamic ctrl, int nXStart, int nYStart, int nXEnd, int nYEnd)
        {
            bool retValue = false;
            FCPictureBox pict = ctrl as FCPictureBox;
            if (pict != null)
            {
                pict.DrawLine(nXStart, nYStart, nXEnd, nYEnd);
                retValue = true;
            }
            return retValue;
        }
        //JSP: GDI32
        public static bool SetPixelGDI32(dynamic ctrl, int nX, int nY, int nColor)
        {
            bool retValue = false;
            FCPictureBox pict = ctrl as FCPictureBox;
            if (pict != null)
            {
                retValue = Externals.GDI32.SetPixelV(pict.hDC, nX, nY, nColor);
                pict.ReleaseCurrentGraphicshDc();
            }
            else
            {
                retValue = Externals.GDI32.SetPixelV(ctrl.hDC, nX, nY, nColor);
            }
            return retValue;

            // Painting the line with same coordinates for start and end don't works
            // return FCUtils.LineToGDI32(ctrl, nX, nY, nX+1, nY);
        }
        //CHE: GDI32
        public static bool MoveToExGDI32(dynamic ctrl, int X, int Y, Point p)
        {
            bool retValue = false;

            // Initialize unmanged memory to hold the struct.
            IntPtr lpPoint = Marshal.AllocHGlobal(Marshal.SizeOf(p));

            try
            {
                // Copy the struct to unmanaged memory.
                Marshal.StructureToPtr(p, lpPoint, false);

                FCPictureBox pict = ctrl as FCPictureBox;
                if (pict != null)
                {
                    retValue = Externals.GDI32.MoveToEx(pict.hDC, X, Y, lpPoint);
                    pict.ReleaseCurrentGraphicshDc();
                }
                else
                {
                    retValue = Externals.GDI32.MoveToEx(ctrl.hdc, X, Y, lpPoint);
                }

            }
            finally
            {
                // Free the unmanaged memory.
                Marshal.FreeHGlobal(lpPoint);
            }

            return retValue;
        }



        public static bool TextOutGDI32(dynamic ctrl, int nXStart, int nYStart, string lpString, int cbString)
        {
            bool retValue = false;
            FCPictureBox pict = ctrl as FCPictureBox;

            if (pict != null)
            {
                Externals.GDI32.SetBkMode(pict.hDC, fecherFoundation.Constants.TRANSPARENT);
                Externals.GDI32.SetTextColor(pict.hDC, ColorTranslator.ToWin32(((dynamic)pict).ForeColor));
                retValue = Externals.GDI32.TextOut(pict.hDC, nXStart, nYStart, lpString, cbString);

                pict.ReleaseCurrentGraphicshDc();
            }
            else
            {
                retValue = Externals.GDI32.TextOut(ctrl.hdc, nXStart, nYStart, lpString, cbString);
            }
            return retValue;
        }

        //CHE: GDI32
        public static bool StretchBltGDI32(dynamic ctrlDest, int nXOriginDest, int nYOriginDest,
            int nWidthDest, int nHeightDest,
            dynamic ctrlSrc, int nXOriginSrc, int nYOriginSrc, int nWidthSrc, int nHeightSrc,
            TernaryRasterOperations dwRop)
        {
            bool retValue = false;
            FCPictureBox pictDest = ctrlDest as FCPictureBox;
            FCPictureBox pictSrc = ctrlSrc as FCPictureBox;
            if (pictDest != null && pictSrc != null)
            {
                using (Graphics g = pictDest.GetGraphics())
                {
                    if (pictDest.ClipPath != null)
                    {
                        g.Clip = new Region(pictDest.ClipPath);
                    }
                    if (dwRop == TernaryRasterOperations.NOTSRCCOPY)
                    {
                        if (pictSrc.Image != null)
                        {
                            using (Bitmap bitmapImage = pictSrc.Image.Clone() as Bitmap)
                            {
                                Color pixelColor;
                                // create some image attributes
                                ImageAttributes attributes = new ImageAttributes();
                                attributes.SetColorMatrix(GetNegativColorMatrix());
                                g.DrawImage(bitmapImage, new Rectangle(nXOriginDest, nYOriginDest, nWidthDest, nHeightDest), nXOriginSrc, nYOriginSrc, nWidthSrc, nHeightSrc, GraphicsUnit.Pixel, attributes);
                            }
                        }
                    }
                    else
                    {
                        g.DrawImage(pictSrc.Image, new Rectangle(nXOriginDest, nYOriginDest, nWidthDest, nHeightDest), new Rectangle(nXOriginSrc, nYOriginSrc, nWidthSrc, nHeightSrc), GraphicsUnit.Pixel);
                        g.Dispose();
                    }
                    pictDest.Refresh();
                }
            }
            else
            {
                retValue = Externals.GDI32.StretchBlt(ctrlDest.hDC, nXOriginDest, nYOriginDest, nWidthDest, nHeightDest, ctrlSrc.hDC, nXOriginSrc, nYOriginSrc, nWidthSrc, nHeightSrc, dwRop);
            }
            return retValue;
        }

        //CHE: GDI32
        public static int SelectClipRgnGDI32(dynamic ctrl, IntPtr hrgn)
        {
            int retValue = 0;
            FCPictureBox pict = ctrl as FCPictureBox;
            if (pict != null)
            {
                retValue = Externals.GDI32.SelectClipRgn(pict.hDC, hrgn);
                pict.ReleaseCurrentGraphicshDc();
            }
            else
            {
                retValue = Externals.GDI32.SelectClipRgn(ctrl.hdc, hrgn);
            }
            return retValue;
        }

        //FC:FINAL:ZSA Implement Clip functionality for StretchBltGDI32 function
        public static void SelectClipRgnGDI32(dynamic ctrl, Point[] points, int length, int fillMode)
        {
            FCPictureBox pict = ctrl as FCPictureBox;
            if (pict != null)
            {
                pict.SetClipRegion(points, length, fillMode);
            }
        }

        //CHE: GDI32
        public static int SaveDCGDI32(dynamic ctrl)
        {
            int retValue = 0;
            FCPictureBox pict = ctrl as FCPictureBox;
            if (pict != null)
            {
                retValue = Externals.GDI32.SaveDC(pict.hDC);
                pict.ReleaseCurrentGraphicshDc();
            }
            else
            {
                retValue = Externals.GDI32.SaveDC(ctrl.hdc);
            }
            return retValue;
        }

        //CHE: GDI32
        public static bool RestoreDCGDI32(dynamic ctrl, int nSavedDC)
        {
            bool retValue = false;
            FCPictureBox pict = ctrl as FCPictureBox;
            if (pict != null)
            {
                retValue = Externals.GDI32.RestoreDC(pict.hDC, nSavedDC);
                pict.ReleaseCurrentGraphicshDc();
            }
            else
            {
                retValue = Externals.GDI32.RestoreDC(ctrl.hdc, nSavedDC);
            }
            return retValue;
        }

        /// <summary>
        /// FontChangeBold from VB6-Compatibilty upgraded
        /// </summary>
        /// <param name="font"></param>
        /// <param name="newBoldState"></param>
        /// <returns></returns>
        public static Font FontChangeBold(Font font, bool bold)
        {
            bool alreadyBold = (font.Style & FontStyle.Bold) == FontStyle.Bold;
            if (bold == alreadyBold) { return font; }
            if (bold) { return new Font(font, font.Style | FontStyle.Bold); }
            return new Font(font, font.Style & ~FontStyle.Bold);
        }

        /// <summary>
        /// FontChangeItalic from VB6-Compatibilty upgraded
        /// </summary>
        /// <param name="font"></param>
        /// <param name="italic"></param>
        /// <returns></returns>
        public static Font FontChangeItalic(Font font, bool italic)
        {
            bool alreadyItalic = (font.Style & FontStyle.Italic) == FontStyle.Italic;
            if (italic == alreadyItalic) { return font; }
            if (italic) { return new Font(font, font.Style | FontStyle.Italic); }
            return new Font(font, font.Style & ~FontStyle.Italic);
        }

        /// <summary>
        /// FontChangeUnderline from VB6-Compatibilty upgraded
        /// </summary>
        /// <param name="font"></param>
        /// <param name="underline"></param>
        /// <returns></returns>
        public static Font FontChangeUnderline(Font font, bool underline)
        {
            bool alreadyUnderline = (font.Style & FontStyle.Underline) == FontStyle.Underline;
            if (underline == alreadyUnderline) { return font; }
            if (underline) { return new Font(font, font.Style | FontStyle.Underline); }
            return new Font(font, font.Style & ~FontStyle.Underline);
        }

		/// <summary>
		 /// FontChangeSize from VB6-Compatibilty upgraded
		 /// </summary>
		 /// <param name="font"></param>
		 /// <param name="size"></param>
		 /// <returns></returns>
        public static Font FontChangeSize(Font font, float size)
		{
			return new Font(font.FontFamily, size, font.Style);
		}

		/// <summary>
		/// FontChangeSize from VB6-Compatibilty upgraded
		/// </summary>
		/// <param name="currentFont"></param>
		/// <param name="newFontName"></param>
		/// <returns></returns>
		public static Font FontChangeName(Font currentFont, string newFontName)
        {
            return new Font(newFontName, currentFont.Size, currentFont.Style);
        }

        /// <summary>
        /// FontChangeGdiCharSet from VB6-Compatibilty upgraded
        /// </summary>
        /// <param name="currentFont"></param>
        /// <param name="newFontName"></param>
        /// <returns></returns>
        public static Font FontChangeGdiCharSet(Font currentFont, GDICharSet gdiCharSet)
        {
            return new Font(currentFont.FontFamily.Name, currentFont.Size, currentFont.Style, currentFont.Unit, Convert.ToByte(gdiCharSet));
        }
        /// <summary>
        /// Returns pixelsToTwipsX
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double PixelsToTwipsX(double value)
        {
            double pixelsToTwipsX = FCScreen.TwipsPerPixelX * value;
            return pixelsToTwipsX;
        }
        /// <summary>
        /// Returns pixelsToTwipsY
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double PixelsToTwipsY(double value)
        {
            double pixelsToTwipsY = FCScreen.TwipsPerPixelY * value;
            return pixelsToTwipsY;
        }
        /// <summary>
        /// Returns pixelsToTwipsX
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double TwipsToPixelsX(double value)
        {
            double pixelsToTwipsX = value / FCScreen.TwipsPerPixelX;
            return pixelsToTwipsX;
        }

        /// <summary>
        /// Returns pixelsToTwipsY
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double TwipsToPixelsY(double value)
        {
            double pixelsToTwipsY = value / FCScreen.TwipsPerPixelY;
            return pixelsToTwipsY;
        }

        /// <summary>
        /// Check, if an Input-Control has the Focus
        /// </summary>
        /// <param name="control"></param>
        /// <returns>Control with the Focus</returns>
        public static Control IsInputControlFocused(Control control)
        {
            Control ctrlToCheck = null;
            foreach (Control ctrl in control.Controls)
            {
                ctrlToCheck = ctrl;
                if (ctrl.HasChildren == true)
                    ctrlToCheck = IsInputControlFocused(ctrl);
                if (ctrlToCheck != null)
                {
                    if (ctrlToCheck is FCTextBox && ctrl.Focused)
                        return ctrl as FCTextBox;
                    // TODO
                    //else if (ctrlToCheck is FCRichTextBox && ctrl.Focused)
                    //    return ctrl as FCRichTextBox;
                }
            }
            return null;
        }

        /// <summary>
        /// Unlocks the client browser from keep loading
        /// </summary>
        public static void UnlockUserInterface()
        {
            var session = Wisej.Web.Application.Current.Session;
            if (session != null)
            {
                var queedLocker = ((System.Reflection.PropertyInfo)((System.Reflection.TypeInfo)session.GetType().GetInterface("IWisejSynchronized")).GetMember("SyncLock")[0]).GetMethod.Invoke(session, new object[] { });
                var isEntered = queedLocker.GetType().GetMethod("IsEntered").Invoke(queedLocker, new object[] { });
                if ((bool)isEntered)
                {
                    queedLocker.GetType().GetMethod("Exit").Invoke(queedLocker, new object[] { });
                }
            }
        }

        /// <summary>
        /// Disables drawing in the specified window.
        /// </summary>
        /// <param name="handle">Handle of the window.</param>
        /// <remarks>
        /// It is absolutely neccessary to call <see cref="UnlockWindowUpdate"/> after drawing operations. Otherwise the
        /// window will be locked endless.
        /// </remarks>
        /// <revisionHistory>
        ///   <revision date="16.12.2015" version="RQ 000000" author="mdi">Erstellung.</revision>
        /// </revisionHistory>
        public static void LockWindowUpdate(IntPtr handle)
        {
            lockWindowUpdateStack.Push(handle);
            if (handle != IntPtr.Zero)
            {
                Externals.USER32.LockWindowUpdate(handle);
            }
        }

       

        /// <summary>
        /// Enables drawing in the specified window.
        /// </summary>
        /// <param name="handle">Handle of the window.</param>
        /// <exception cref="ArgumentException">Raised if the handle is invalid (e.g. wrong call order).</exception>
        /// <remarks>
        /// Erstellt am 16.12.2015.
        /// </remarks>
        /// <revisionHistory>
        ///   <revision date="16.12.2015" version="RQ 000000" author="mdi">Erstellung.</revision>
        /// </revisionHistory>
        public static void UnlockWindowUpdate(IntPtr handle)
        {
            if (lockWindowUpdateStack.Count > 0)
            {
                IntPtr previousHandle = lockWindowUpdateStack.Pop();
                if (previousHandle != handle)
                {
                    if (previousHandle != IntPtr.Zero)
                        throw new ArgumentException("UnlockWindowUpdate: Parameter handle is not correct");
                }

                if (previousHandle != IntPtr.Zero)
                {
                    Externals.USER32.LockWindowUpdate(IntPtr.Zero);
                }
                if (lockWindowUpdateStack.Count > 0)
                {
                    previousHandle = lockWindowUpdateStack.Peek();
                    Externals.USER32.LockWindowUpdate(previousHandle);
                }
            }
        }

        /// <summary>
        /// Suspends <see cref="LockWindowUpdate"/> immediately.
        /// </summary>
        /// <remarks>
        /// Erstellt am 16.12.2015.
        /// </remarks>
        /// <revisionHistory>
        ///   <revision date="16.12.2015" version="RQ 000000" author="mdi">Erstellung.</revision>
        /// </revisionHistory>
        public static void SuspendLockWindowUpdate()
        {
            Externals.USER32.LockWindowUpdate(IntPtr.Zero);
        }

        /// <summary>
        /// Resumes the <see cref="LockWindowUpdate"/>.
        /// </summary>
        /// <remarks>
        /// Erstellt am 16.12.2015.
        /// </remarks>
        /// <revisionHistory>
        ///   <revision date="16.12.2015" version="RQ 000000" author="mdi">Erstellung.</revision>
        /// </revisionHistory>
        public static void ResumeLockWindowUpdate()
        {
            if (lockWindowUpdateStack.Count > 0)
            {
                IntPtr handle = lockWindowUpdateStack.Peek();
                Externals.USER32.LockWindowUpdate(handle);
            }
        }

        /// <summary>
        /// Until the control is viable for the first time some back-end initialization never happens, part of that initialization is enabling the data binding. 
        /// You must call CreateControl(true) before data binding works. However, that method it is a protected method so you must do it though reflection or 
        /// by extending the control.
        /// All events will be deferred until the control has Created set to true.
        /// </summary>
        /// <param name="control"></param>
        public static void CreateControl(Control control)
        {
            if (control.Created)
            {
                return;
            }
            var method = control.GetType().GetMethod("CreateControl", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            var parameters = method.GetParameters();
            if (parameters.Length == 1 && parameters[0].ParameterType == typeof(bool))
            {
                method.Invoke(control, new object[] { true });
            }
        }

        public static void CallByName(dynamic owner, string name, CallType type, params object[] args)
        {
            var method = owner.GetType().GetMethod(name, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
            method.Invoke(owner, args);
        }

        public static bool IsValidDateTime(dynamic value)
        {
            return value is DateTime && ((DateTime)value).ToOADate() != 0;
        }

        /// <summary>
        /// Start async task
        /// </summary>
        /// <param name="form"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static System.Threading.Thread StartTask(FCForm form, Action action)
        {
            //P2218:SBE:#3274 - save the Thread during long running process. Thread can be stopped later, during form close
            System.Threading.Thread taskThread = null;
            Application.StartTask(() =>
            {
                taskThread = System.Threading.Thread.CurrentThread;
                action();
            });

            while (taskThread == null)
            {
                //wait for taskThread to be set
            }
            form.StartedTasks.Add(taskThread);
            return taskThread;
        }

        /// <summary>
        /// Method used to update the client asynchronously.
        /// </summary>
        /// <param name="component"></param>
        /// <param name="action"></param>
        public static void ApplicationUpdate(Wisej.Core.IWisejComponent component, Action action = null)
        {
            //check if the update is executed during logout. If there is a long running process stoped at logout, client cannot be updated and the code is locked at Application.Update call
            if (!App.MainForm.ExitingApplication)
            {
                Application.Update(component, action);
            }
        }
        #endregion

        #region Internal Methods

        /// <summary>
        /// Alls the controls count.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <returns></returns>
        internal static int AllControlsCount(Control control)
        {
            // Get child controls count
            int intCount = control.Controls.Count;

            // Count each control children recursively
            foreach (Control child in control.Controls)
            {
                // Get count for child control
                intCount += AllControlsCount(child);
            }

            return intCount;
        }

        /// <summary>
        /// Dispose an Image 
        /// </summary>
        /// <param name="img"></param>
        internal static void DisposeImage(ref Image img)
        {
            if (img != null)
            {
                img.Dispose();
                img = null;
            }
        }

        /// <summary>
        /// Dispose a Bitmap 
        /// </summary>
        /// <param name="bmp"></param>
        internal static void DisposeBitmap(ref Bitmap bmp)
        {
            if (bmp != null)
            {
                bmp.Dispose();
                bmp = null;
            }
        }

        /// <summary>
        /// Dispose a Cursor 
        /// </summary>
        /// <param name="cursor"></param>
        internal static void DisposeCursor(ref Cursor cursor)
        {
            if (cursor != null)
            {
                //cursor.Dispose();
                cursor = null;
            }
        }

        /// <summary>
        /// Get negativ ColorMatrix 
        /// </summary>
        /// <returns>ColorMatrix</returns>
        internal static ColorMatrix GetNegativColorMatrix()
        {
            return new ColorMatrix(new float[][]
                                {
                                new float[] {-1, 0, 0, 0, 0},
                                new float[] {0, -1, 0, 0, 0},
                                new float[] {0, 0, -1, 0, 0},
                                new float[] {0, 0, 0, 1, 0},
                                new float[] {1, 1, 1, 0, 1}
                                });
        }

        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private static void SetupEventHandler(object listener, string instanceName, string eventName, object instance, bool attachHandler = true)
        {
            try
            {
                //CHE: use IgnoreCase flag since VB6 is not case sensitive
                MethodInfo miHandler =
                    listener.GetType().GetMethod(String.Format("{0}_{1}", instanceName, eventName),
                        BindingFlags.IgnoreCase | BindingFlags.NonPublic | BindingFlags.Instance);

                if (miHandler == null)
                {
                    return;
                }

                EventInfo eventInfo = instance.GetType().GetEvent(eventName);
                Type tDelegate = eventInfo.EventHandlerType;

                // Create an instance of the delegate. Using the overloads 
                // of CreateDelegate that take MethodInfo is recommended. 
                //
                Delegate d = Delegate.CreateDelegate(tDelegate, listener, miHandler);

                // Get the "add" accessor of the event and invoke it late-
                // bound, passing in the delegate instance. This is equivalent 
                // to using the += operator in C#, or AddHandler in Visual 
                // Basic. The instance on which the "add" accessor is invoked
                // is the form; the arguments must be passed as an array. 
                //
                MethodInfo handler = null;
                if (attachHandler)
                {
                    handler = eventInfo.GetAddMethod();
                }
                else
                {
                    handler = eventInfo.GetRemoveMethod();
                }

                Object[] handlerArgs = { d };
                handler.Invoke(instance, handlerArgs);

            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// http://forums.codeguru.com/showthread.php?438686-Get-Available-font-sizes
        /// </summary>
        /// <param name="points"></param>
        /// <returns></returns>
        private static float GetActualPoints(float points, bool isPrinter = false)
        {
            if (isPrinter)
            {
                return points;
            }
            return (float)Math.Round(points / 0.75f) * 0.75f;
        }

        /// <summary>
        /// copy all control properties
        /// </summary>
        /// <param name="sourceCtrl"></param>
        /// <param name="targetControl"></param>
        private static void CloneControlProperties(Control sourceCtrl, Control targetControl)
        {
            PropertyDescriptorCollection targetCtrlProperties = TypeDescriptor.GetProperties(targetControl);

            PropertyDescriptorCollection sourceCtrlProperties = TypeDescriptor.GetProperties(sourceCtrl);
            PropertyDescriptor targetProperty = null;

            foreach (PropertyDescriptor myProperty in sourceCtrlProperties)
            {
                try
                {
                    if (myProperty.PropertyType.IsSerializable && myProperty.Name != "TabIndex" && myProperty.Name != "ListIndex" && myProperty.Name != "SelectedIndex")
                    {
                        targetProperty = targetCtrlProperties[myProperty.Name];
                        if (targetProperty != null)
                        {
                            targetProperty.SetValue(targetControl, myProperty.GetValue(sourceCtrl));
                            //CHE: create new control as not visible, even if the source is visible
                            if (myProperty.Name == "Visible")
                            {
                                targetProperty.SetValue(targetControl, false);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Trace.WriteLine(ex.Message);
                    //do nothing, just continue
                }

            }
        }

        private static bool CheckIsInherited(Control control, Font font)
        {
            if (control == null)
            {
                return false;
            }
            Control parent = control.Parent;
            if (parent == null)
            {
                return false;
            }
            if (object.Equals(parent.Font, font))
            {
                return true;
            }
            return CheckIsInherited(parent, font);
        }

        #endregion
    }
}
