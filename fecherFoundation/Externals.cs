﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Security;
using Wisej.Web;


namespace fecherFoundation
{
    /// <summary>
    /// External Methods declaration
    /// </summary>
    public static class Externals
    {
        #region USER32

        [SuppressUnmanagedCodeSecurity]
        public static class USER32
        {
            #region ScrollInfo

            [Serializable, StructLayout(LayoutKind.Sequential)]
            public struct SCROLLINFO
            {
                public int cbSize;
                public int fMask;
                public int nMin;
                public int nMax;
                public int nPage;
                public int nPos;
                public int nTrackPos;
            }

            #endregion

            /// <summary>
            /// LockWindowUpdate
            /// </summary>
            /// <param name="hWndLock"></param>
            /// <returns></returns>
            [DllImport("user32.dll")]
            public static extern bool LockWindowUpdate(IntPtr hWndLock);

            [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
            public static extern IntPtr GetForegroundWindow();

            [DllImport("user32.dll")]
            public static extern bool SetForegroundWindow(IntPtr hWnd);

            [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            public static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);

            [DllImport("user32", CharSet = CharSet.Auto)]
            public static extern IntPtr SendMessage(IntPtr hWnd, UInt32 msg, IntPtr wParam, IntPtr lParam);

            [DllImport("user32.dll", SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            public static extern bool GetScrollInfo(IntPtr hwnd, int fnBar, ref SCROLLINFO si);

            [DllImport("user32.dll")]
            public static extern bool GetCursorPos(ref Point lpPoint);
        }
        #endregion USER32

        #region GDI32
        [SuppressUnmanagedCodeSecurity]
        public static class GDI32
        {
            [DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            public static extern int SetBkMode(IntPtr hdc, int iBkMode);

            [DllImport("gdi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            public static extern uint SetTextColor(IntPtr hdc, int crColor);

            [DllImport("gdi32.dll")]
            public static extern bool Polygon(IntPtr hdc, Point[] lpPoints, int nCount);
            [DllImport("gdi32.dll")]
            public static extern bool LineTo(IntPtr hdc, int nXEnd, int nYEnd);
            [DllImport("gdi32.dll")]
            public static extern bool SetPixelV(IntPtr hdc, int nXEnd, int nYEnd, int nColor);
            [DllImport("gdi32.dll")]
            public static extern bool MoveToEx(IntPtr hdc, int X, int Y, IntPtr lpPoint);
            [DllImport("gdi32.dll", CharSet = CharSet.Auto)]
            public static extern bool TextOut(IntPtr hdc, int nXStart, int nYStart, string lpString, int cbString);
            [DllImport("gdi32.dll")]
            public static extern bool StretchBlt(IntPtr hdcDest, int nXOriginDest, int nYOriginDest,
                int nWidthDest, int nHeightDest,
                IntPtr hdcSrc, int nXOriginSrc, int nYOriginSrc, int nWidthSrc, int nHeightSrc,
                TernaryRasterOperations dwRop);
            [DllImport("gdi32.dll")]
            public static extern int SelectClipRgn(IntPtr hdc, IntPtr hrgn);
            [DllImport("gdi32.dll")]
            public static extern bool DeleteObject(IntPtr hObject);
            [DllImport("gdi32.dll")]
            public static extern int SaveDC(IntPtr hdc);
            [DllImport("gdi32.dll")]
            public static extern bool RestoreDC(IntPtr hdc, int nSavedDC);
            [DllImport("gdi32.dll", EntryPoint = "SelectObject")]
            public static extern IntPtr SelectObject([In] IntPtr hdc, [In] IntPtr hgdiobj);
            [DllImport("gdi32.dll", CharSet = CharSet.Auto)]
            public static extern IntPtr CreateFontIndirect([In, MarshalAs(UnmanagedType.LPStruct)] LOGFONT lplf);

        }
        
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public class LOGFONT
        {
            public const int LF_FACESIZE = 32;
            public int lfHeight;
            public int lfWidth;
            public int lfEscapement;
            public int lfOrientation;
            public int lfWeight;
            public byte lfItalic;
            public byte lfUnderline;
            public byte lfStrikeOut;
            public byte lfCharSet;
            public byte lfOutPrecision;
            public byte lfClipPrecision;
            public byte lfQuality;
            public byte lfPitchAndFamily;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = LF_FACESIZE)]
            public string lfFaceName = string.Empty;
        }

    #endregion GDI32

    #region WINSPOOL
    [SuppressUnmanagedCodeSecurity]
        public static class WINSPOOL
        {
            [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
            public static extern bool SetDefaultPrinter(string Name);
        }
        #endregion

        #region UXTHEME
        [SuppressUnmanagedCodeSecurity]
        public static class UXTHEME
        {
            [DllImport("UXTheme.dll", CharSet = CharSet.Unicode)]
            public static extern int SetWindowTheme(IntPtr hWnd, string subAppName, string subIdList);
        }
        #endregion
    }
}
