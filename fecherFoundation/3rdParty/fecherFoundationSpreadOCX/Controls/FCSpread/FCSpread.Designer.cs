﻿namespace fecherFoundationSpreadOCX
{
    public partial class FCSpread
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing )
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FCSpread));
            this.axfpSpread1 = new AxFPUSpreadADO.AxfpSpread();
            ((System.ComponentModel.ISupportInitialize)(this.axfpSpread1)).BeginInit();
            this.SuspendLayout();
            // 
            // axfpSpread1
            // 
            this.axfpSpread1.DataSource = null;
            this.axfpSpread1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axfpSpread1.Location = new System.Drawing.Point(0, 0);
            this.axfpSpread1.Name = "axfpSpread1";
            this.axfpSpread1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axfpSpread1.OcxState")));
            this.axfpSpread1.Size = new System.Drawing.Size(150, 150);
            this.axfpSpread1.TabIndex = 0;

            // 
            // FCSpread
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.axfpSpread1);
            this.Name = "FCSpread";
            ((System.ComponentModel.ISupportInitialize)(this.axfpSpread1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxFPUSpreadADO.AxfpSpread axfpSpread1;


    }
}
