﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fecherFoundation;
using FPUSpreadADO;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace fecherFoundationSpreadOCX
{
    /// <summary>
    /// in VB6 FPUSpreadADO.fpSpread
    /// http://helpcentral.componentone.com/NetHelp/Spread8/WebSiteHelp/titlea.html
    /// http://helpcentral.componentone.com/NetHelp/SpreadNet7/WF/webframe.html#spwin-comprops.html
    /// </summary>
    public partial class FCSpread : UserControl
    {
        #region Public Members

        public const int SpreadHeader = -1000;
        #endregion

        #region Internal Members
        #endregion

        #region Private Members

        private FCGraphics fcGraphics;
        private GraphicsFactory graphicsFactory;

        private bool disableEvents = false;

        private ToolTip toolTip = new ToolTip();

        //PJ:Raising manual Key-Up-Eveent (Pressing Key-Up / Key-Down-Events does not fire Key-Up after Key-Down and Key-LeaveCell while VB6 does it; Hints from the Internet are not working, here). So it will be manually raised
        private bool raiseKeyUpEvent = false;
        private AxFPUSpreadADO._DSpreadEvents_KeyUpEvent keyUpEventArgs;
        private object senderKeyUpEvent = null;
        private Timer keyUpWEventTimer;
        
        //PJ:Handling double-click-event: Should not occur e.g. after Click on a Row, then switch Context to another App (Notepad), switch back again and click on the same Row/Column 
        //following Events are fired: MouseDown, Click, DoubleClick
        private bool isDoubleClick = false;
        private int milliseconds = 0;
        private Timer doubleClickTimer;
        private Rectangle doubleClickRectangle = new Rectangle();

        #endregion

        #region Constructors

        public FCSpread()
        {
            InitializeComponent();
            this.components.Add(this.axfpSpread1);
            this.MaxRows = 500;
            this.OperationMode = OperationModeConstants.OperationModeNormal;
            this.ShadowColor = Color.LightGray;
            this.ShadowDark = Color.DarkGray;
            this.ShadowText = Color.Black;
            this.UnitType = UnitTypeConstants.UnitTypeVGABase;
            this.NoBeep = false;
            this.DAutoSizeCols = DAutoSizeColsConstants.DAutoSizeColsBest;
            this.ScrollBarShowMax = true;
            this.AutoCalc = true;
            this.FormulaSync = true;
            this.GridShowHoriz = true;
            this.GridShowVert = true;
            this.GridSolid = true;
            this.BackColorStyle = BackColorStyleConstants.BackColorStyleOverGrid;
            this.ProcessTab = false;
            this.EditModeReplace = false;
            this.EditModePermanent = false;
            this.ColHeadersShow = true;
            this.RowHeadersShow = true;
            this.UserResize = UserResizeConstants.UserResizeBoth;
            this.SelectBlockOptions = SelectBlockOptionsConstants.SelectBlockOptionsAll;
            this.GridColor = Color.LightGray;
            this.GrayAreaBackColor = Color.LightGray;
            this.BackColor = Color.White;
            this.ScrollBarExtMode = false;
            this.EditEnterAction = EditEnterActionConstants.EditEnterActionNone;
            this.MaxCols = 500;
            this.AutoClipboard = true;
            this.AllowMultiBlocks = false;
            this.MoveActiveOnFocus = true;
            this.ArrowsExitEditMode = false;
            this.VisibleRows = 0;
            this.VisibleCols = 0;
            this.ScrollBars = ScrollBarsConstants.ScrollBarsBoth;
            this.ColHeadersAutoText = HeaderDisplayConstants.DispLetters;
            this.Protect = true;
            this.ColHeaderRows = 1;
            this.RowHeadersAutoText = HeaderDisplayConstants.DispNumbers;
            this.RowHeaderCols = 1;
            this.ToolTipText = "";

            using (Graphics g = this.CreateGraphics())
            {
                graphicsFactory = new GraphicsFactory();
                fcGraphics = new FCGraphics(g, graphicsFactory);
            }
            
            this.axfpSpread1.ButtonClicked +=axfpSpread1_ButtonClicked;
            this.axfpSpread1.Change +=axfpSpread1_Change;
            this.axfpSpread1.ClickEvent +=axfpSpread1_ClickEvent;
            this.axfpSpread1.ColWidthChange +=axfpSpread1_ColWidthChange;
            this.axfpSpread1.ComboDropDown +=axfpSpread1_ComboDropDown;
            this.axfpSpread1.ComboSelChange +=axfpSpread1_ComboSelChange;
            this.axfpSpread1.DblClick +=axfpSpread1_DblClick;
            this.axfpSpread1.EditModeEvent +=axfpSpread1_EditModeEvent;
            this.axfpSpread1.EnterRow +=axfpSpread1_EnterRow;
            this.axfpSpread1.KeyDownEvent +=axfpSpread1_KeyDownEvent;
            this.axfpSpread1.KeyPressEvent +=axfpSpread1_KeyPressEvent;
            this.axfpSpread1.KeyUpEvent +=axfpSpread1_KeyUpEvent;
            this.axfpSpread1.LeaveCell +=axfpSpread1_LeaveCell;
            this.axfpSpread1.LeaveRow +=axfpSpread1_LeaveRow;
            this.axfpSpread1.MouseDownEvent +=axfpSpread1_MouseDownEvent;
            this.axfpSpread1.MouseMoveEvent +=axfpSpread1_MouseMoveEvent;
            this.axfpSpread1.MouseUpEvent +=axfpSpread1_MouseUpEvent;
            this.axfpSpread1.RightClick +=axfpSpread1_RightClick;
            this.axfpSpread1.TopLeftChange += axfpSpread1_TopLeftChange;

            this.doubleClickTimer = new Timer(this.components);
            this.doubleClickTimer.Interval = 100;
            this.doubleClickTimer.Tick +=  new EventHandler(DoubleClickTimer_Tick);
        }

        ~FCSpread()
        {
            this.axfpSpread1 = null;
        }
        #endregion

        #region Public Delegates
        #endregion

        #region Public Events

        public event _DSpreadEvents_ButtonClickedEventHandler ButtonClicked;
        public event _DSpreadEvents_ChangeEventHandler Change;
        public event _DSpreadEvents_ClickEventHandler ClickEvent;
        public event _DSpreadEvents_ColWidthChangeEventHandler ColWidthChange;
        public event _DSpreadEvents_ComboDropDownEventHandler ComboDropDown;
        public event _DSpreadEvents_ComboSelChangeEventHandler ComboSelChange;
        public event _DSpreadEvents_DblClickEventHandler DblClick;
        public event _DSpreadEvents_EditModeEventHandler EditModeEvent;
        public event _DSpreadEvents_EnterRowEventHandler EnterRow;
        public event _DSpreadEvents_KeyDownEventHandler KeyDownEvent;
        public event _DSpreadEvents_KeyPressEventHandler KeyPressEvent;
        public event _DSpreadEvents_KeyUpEventHandler KeyUpEvent;
        public event _DSpreadEvents_LeaveCellEventHandler LeaveCell;
        public event _DSpreadEvents_LeaveRowEventHandler LeaveRow;
        public event _DSpreadEvents_MouseDownEventHandler MouseDownEvent;
        public event _DSpreadEvents_MouseMoveEventHandler MouseMoveEvent;
        public event _DSpreadEvents_MouseUpEventHandler MouseUpEvent;
        public event _DSpreadEvents_RightClickEventHandler RightClick;
        public event _DSpreadEvents_TopLeftChangeEventHandler TopLeftChange;

        #endregion

        #region Private Events
        #endregion

        #region Enums
        #endregion

        #region Properties

        public MSDATASRC.DataSource DataSource
        {
            get
            {
                return this.axfpSpread1.DataSource;
            }
            set
            {
                this.axfpSpread1.DataSource = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public AxHost.State OcxState
        {
            get
            {
                return this.axfpSpread1.OcxState;
            }
            set
            {
                this.axfpSpread1.OcxState = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Color ForeColor
        {
            get
            {
                return this.axfpSpread1.ForeColor;
            }
            set
            {
                this.axfpSpread1.ForeColor = value;
            }
        }

        [DefaultValue(500)]
        public int MaxRows
        {
            get
            {
                return this.axfpSpread1.MaxRows;
            }
            set
            {
                this.axfpSpread1.MaxRows = value;
            }
        }

        public bool EditMode
        {
            get
            {
                return this.axfpSpread1.EditMode;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Row
        {
            get
            {
                return this.axfpSpread1.Row;
            }
            set
            {
                this.axfpSpread1.Row = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ActiveRow
        {
            get
            {
                return this.axfpSpread1.ActiveRow;
            }
            set
            {
                //this.axfpSpread1.ActiveRow = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Col
        {
            get
            {
                return this.axfpSpread1.Col;
            }
            set
            {
                this.axfpSpread1.Col = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int ActiveCol
        {
            get
            {
                return this.axfpSpread1.ActiveCol;
            }
            set
            {
                //this.axfpSpread1.ActiveCol = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Row2
        {
            get
            {
                return this.axfpSpread1.Row2;
            }
            set
            {
                this.axfpSpread1.Row2 = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopRow
        {
            get
            {
                return this.axfpSpread1.TopRow;
            }
            set
            {
                this.axfpSpread1.TopRow = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftCol
        {
            get
            {
                return this.axfpSpread1.LeftCol;
            }
            set
            {
                this.axfpSpread1.LeftCol = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int Col2
        {
            get
            {
                return this.axfpSpread1.Col2;
            }
            set
            {
                this.axfpSpread1.Col2 = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TypeEditLen
        {
            get
            {
                return this.axfpSpread1.TypeEditLen;
            }
            set
            {
                this.axfpSpread1.TypeEditLen = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Clip
        {
            get
            {
                return this.axfpSpread1.Clip;
            }
            set
            {
                this.axfpSpread1.Clip = value;
            }
        }

        [DefaultValue(OperationModeConstants.OperationModeNormal)]
        public OperationModeConstants OperationMode
        {
            get
            {
                return this.axfpSpread1.OperationMode;
            }
            set
            {
                this.axfpSpread1.OperationMode = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Value
        {
            get
            {
                return this.axfpSpread1.Value;
            }
            set
            {
                this.axfpSpread1.Value = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string ClipValue
        {
            get
            {
                return this.axfpSpread1.ClipValue;
            }
            set
            {
                this.axfpSpread1.ClipValue = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool Lock
        {
            get
            {
                return this.axfpSpread1.Lock;
            }
            set
            {
                this.axfpSpread1.Lock = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public CellTypeConstants CellType
        {
            get
            {
                return this.axfpSpread1.CellType;
            }
            set
            {
                this.axfpSpread1.CellType = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool BlockMode
        {
            get
            {
                return this.axfpSpread1.BlockMode;
            }
            set
            {
                this.axfpSpread1.BlockMode = value;
            }
        }

        //[DefaultValue(Color.LightGray)]
        public Color ShadowColor
        {
            get
            {
                return this.axfpSpread1.ShadowColor;
            }
            set
            {
                this.axfpSpread1.ShadowColor = value;
            }
        }

        //[DefaultValue(Color.DarkGray)]
        public Color ShadowDark
        {
            get
            {
                return this.axfpSpread1.ShadowDark;
            }
            set
            {
                this.axfpSpread1.ShadowDark = value;
            }
        }

        //[DefaultValue(Color.Black)]
        public Color ShadowText
        {
            get
            {
                return this.axfpSpread1.ShadowText;
            }
            set
            {
                this.axfpSpread1.ShadowText = value;
            }
        }

        [DefaultValue(UnitTypeConstants.UnitTypeVGABase)]
        public UnitTypeConstants UnitType
        {
            get
            {
                return this.axfpSpread1.UnitType;
            }
            set
            {
                this.axfpSpread1.UnitType = value;
            }
        }

        [DefaultValue(false)]
        public bool NoBeep
        {
            get
            {
                return this.axfpSpread1.NoBeep;
            }
            set
            {
                this.axfpSpread1.NoBeep = value;
            }
        }

        [DefaultValue(DAutoSizeColsConstants.DAutoSizeColsBest)]
        public DAutoSizeColsConstants DAutoSizeCols
        {
            get
            {
                return this.axfpSpread1.DAutoSizeCols;
            }
            set
            {
                this.axfpSpread1.DAutoSizeCols = value;
            }
        }

        [DefaultValue(true)]
        public bool ScrollBarShowMax
        {
            get
            {
                return this.axfpSpread1.ScrollBarShowMax;
            }
            set
            {
                this.axfpSpread1.ScrollBarShowMax = value;
            }
        }

        [DefaultValue(true)]
        public bool AutoCalc
        {
            get
            {
                return this.axfpSpread1.AutoCalc;
            }
            set
            {
                this.axfpSpread1.AutoCalc = value;
            }
        }

        [DefaultValue(true)]
        public bool FormulaSync
        {
            get
            {
                return this.axfpSpread1.FormulaSync;
            }
            set
            {
                this.axfpSpread1.FormulaSync = value;
            }
        }

        [DefaultValue(true)]
        public bool GridShowHoriz
        {
            get
            {
                return this.axfpSpread1.GridShowHoriz;
            }
            set
            {
                this.axfpSpread1.GridShowHoriz = value;
            }
        }

        [DefaultValue(true)]
        public bool GridShowVert
        {
            get
            {
                return this.axfpSpread1.GridShowVert;
            }
            set
            {
                this.axfpSpread1.GridShowVert = value;
            }
        }

        [DefaultValue(true)]
        public bool GridSolid
        {
            get
            {
                return this.axfpSpread1.GridSolid;
            }
            set
            {
                this.axfpSpread1.GridSolid = value;
            }
        }
        
        [DefaultValue(BackColorStyleConstants.BackColorStyleOverGrid)]
        public BackColorStyleConstants BackColorStyle
        {
            get
            {
                return this.axfpSpread1.BackColorStyle;
            }
            set
            {
                this.axfpSpread1.BackColorStyle = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ColHidden
        {
            get
            {
                return this.axfpSpread1.ColHidden;
            }
            set
            {
                this.axfpSpread1.ColHidden = value;
            }
        }

        [DefaultValue(false)]
        public bool ProcessTab
        {
            get
            {
                return this.axfpSpread1.ProcessTab;
            }
            set
            {
                this.axfpSpread1.ProcessTab = value;
            }
        }

        [DefaultValue(false)]
        public bool EditModeReplace
        {
            get
            {
                return this.axfpSpread1.EditModeReplace;
            }
            set
            {
                this.axfpSpread1.EditModeReplace = value;
            }
        }

        [DefaultValue(false)]
        public bool EditModePermanent
        {
            get
            {
                return this.axfpSpread1.EditModePermanent;
            }
            set
            {
                this.axfpSpread1.EditModePermanent = value;
            }
        }

        [DefaultValue(true)]
        public bool ColHeadersShow
        {
            get
            {
                return this.axfpSpread1.ColHeadersShow;
            }
            set
            {
                this.axfpSpread1.ColHeadersShow = value;
            }
        }

        [DefaultValue(true)]
        public bool RowHeadersShow
        {
            get
            {
                return this.axfpSpread1.RowHeadersShow;
            }
            set
            {
                this.axfpSpread1.RowHeadersShow = value;
            }
        }

        [DefaultValue(UserResizeConstants.UserResizeBoth)]
        public UserResizeConstants UserResize
        {
            get
            {
                return this.axfpSpread1.UserResize;
            }
            set
            {
                this.axfpSpread1.UserResize = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public UserResizeConstants2 UserResizeCol
        {
            get
            {
                return this.axfpSpread1.UserResizeCol;
            }
            set
            {
                this.axfpSpread1.UserResizeCol = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public UserResizeConstants2 UserResizeRow
        {
            get
            {
                return this.axfpSpread1.UserResizeRow;
            }
            set
            {
                this.axfpSpread1.UserResizeRow = value;
            }
        }

        [DefaultValue(SelectBlockOptionsConstants.SelectBlockOptionsAll)]
        public SelectBlockOptionsConstants SelectBlockOptions
        {
            get
            {
                return this.axfpSpread1.SelectBlockOptions;
            }
            set
            {
                this.axfpSpread1.SelectBlockOptions = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int DataRowCnt
        {
            get
            {
                return this.axfpSpread1.DataRowCnt;
            }
            set
            {
                this.axfpSpread1.DataRowCnt = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public TypeHAlignConstants TypeHAlign
        {
            get
            {
                return this.axfpSpread1.TypeHAlign;
            }
            set
            {
                this.axfpSpread1.TypeHAlign = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public TypeVAlignConstants TypeVAlign
        {
            get
            {
                return this.axfpSpread1.TypeVAlign;
            }
            set
            {
                this.axfpSpread1.TypeVAlign = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ReDraw
        {
            get
            {
                return this.axfpSpread1.ReDraw;
            }
            set
            {
                this.axfpSpread1.ReDraw = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelBlockCol
        {
            get
            {
                return this.axfpSpread1.SelBlockCol;
            }
            set
            {
                this.axfpSpread1.SelBlockCol = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelBlockCol2
        {
            get
            {
                return this.axfpSpread1.SelBlockCol2;
            }
            set
            {
                this.axfpSpread1.SelBlockCol2 = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelBlockRow
        {
            get
            {
                return this.axfpSpread1.SelBlockRow;
            }
            set
            {
                this.axfpSpread1.SelBlockRow = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelBlockRow2
        {
            get
            {
                return this.axfpSpread1.SelBlockRow2;
            }
            set
            {
                this.axfpSpread1.SelBlockRow2 = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontBold
        {
            get
            {
                return this.axfpSpread1.FontBold;
            }
            set
            {
                this.axfpSpread1.FontBold = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool TypeCheckCenter
        {
            get
            {
                return this.axfpSpread1.TypeCheckCenter;
            }
            set
            {
                this.axfpSpread1.TypeCheckCenter = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public TypeDateFormatConstants TypeDateFormat
        {
            get
            {
                return this.axfpSpread1.TypeDateFormat;
            }
            set
            {
                this.axfpSpread1.TypeDateFormat = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool TypeTimeSeconds
        {
            get
            {
                return this.axfpSpread1.TypeTimeSeconds;
            }
            set
            {
                this.axfpSpread1.TypeTimeSeconds = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool DisplayRowHeaders
        {
            get
            {
                return this.axfpSpread1.DisplayRowHeaders;
            }
            set
            {
                this.axfpSpread1.DisplayRowHeaders = value;
            }
        }

        //[DefaultValue(Color.LightGray)]
        public Color GridColor
        {
            get
            {
                return this.axfpSpread1.GridColor;
            }
            set
            {
                this.axfpSpread1.GridColor = value;
            }
        }

        //[DefaultValue(Color.LightGray)]
        public Color GrayAreaBackColor
        {
            get
            {
                return this.axfpSpread1.GrayAreaBackColor;
            }
            set
            {
                this.axfpSpread1.GrayAreaBackColor = value;
            }
        }

        //[DefaultValue(Color.White)]
        public new Color BackColor
        {
            get
            {
                return this.axfpSpread1.BackColor;
            }
            set
            {
                this.axfpSpread1.BackColor = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public TypeTextAlignVertConstants TypeTextAlignVert
        {
            get
            {
                return this.axfpSpread1.TypeTextAlignVert;
            }
            set
            {
                this.axfpSpread1.TypeTextAlignVert = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new string Text
        {
            get
            {
                object var = null;
                this.axfpSpread1.GetText(this.Col, this.Row, ref var);
                return Convert.ToString(var);
            }
            set
            {
                this.axfpSpread1.SetText(this.Col, this.Row, value);
            }
        }

        [DefaultValue(false)]
        public bool ScrollBarExtMode
        {
            get
            {
                return this.axfpSpread1.ScrollBarExtMode;
            }
            set
            {
                this.axfpSpread1.ScrollBarExtMode = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool TypeTextWordWrap
        {
            get
            {
                return this.axfpSpread1.TypeTextWordWrap;
            }
            set
            {
                this.axfpSpread1.TypeTextWordWrap = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string CellTag
        {
            get
            {
                return this.axfpSpread1.CellTag;
            }
            set
            {
                this.axfpSpread1.CellTag = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelectionCount
        {
            get
            {
                return this.axfpSpread1.SelectionCount;
            }
            set
            {
                this.axfpSpread1.SelectionCount = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string TypeComboBoxList
        {
            get
            {
                return this.axfpSpread1.TypeComboBoxList;
            }
            set
            {
                this.axfpSpread1.TypeComboBoxList = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool TypeComboBoxEditable
        {
            get
            {
                return this.axfpSpread1.TypeComboBoxEditable;
            }
            set
            {
                this.axfpSpread1.TypeComboBoxEditable = value;
            }
        }
        
        [DefaultValue(EditEnterActionConstants.EditEnterActionNone)]
        public EditEnterActionConstants EditEnterAction
        {
            get
            {
                return this.axfpSpread1.EditEnterAction;
            }
            set
            {
                this.axfpSpread1.EditEnterAction = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public Image TypePictPicture
        {
            get
            {
                return this.axfpSpread1.TypePictPicture;
            }
            set
            {
                this.axfpSpread1.TypePictPicture = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string TypeButtonText
        {
            get
            {
                return this.axfpSpread1.TypeButtonText;
            }
            set
            {
                this.axfpSpread1.TypeButtonText = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public short TypeComboBoxCurSel
        {
            get
            {
                return this.axfpSpread1.TypeComboBoxCurSel;
            }
            set
            {
                this.axfpSpread1.TypeComboBoxCurSel = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool TypeDateCentury
        {
            get
            {
                return this.axfpSpread1.TypeDateCentury;
            }
            set
            {
                this.axfpSpread1.TypeDateCentury = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public short TypeNumberDecPlaces
        {
            get
            {
                return this.axfpSpread1.TypeNumberDecPlaces;
            }
            set
            {
                this.axfpSpread1.TypeNumberDecPlaces = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool RowHidden
        {
            get
            {
                return this.axfpSpread1.RowHidden;
            }
            set
            {
                this.axfpSpread1.RowHidden = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontUnderline
        {
            get
            {
                return this.axfpSpread1.FontUnderline;
            }
            set
            {
                this.axfpSpread1.FontUnderline = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool IsBlockSelected
        {
            get
            {
                return this.axfpSpread1.IsBlockSelected;
            }
            set
            {
                this.axfpSpread1.IsBlockSelected = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool ChangeMade
        {
            get
            {
                return this.axfpSpread1.ChangeMade;
            }
            set
            {
                this.axfpSpread1.ChangeMade = value;
            }
        } 

        [DefaultValue(500)]
        public int MaxCols
        {
            get
            {
                return this.axfpSpread1.MaxCols;
            }
            set
            {
                this.axfpSpread1.MaxCols = value;
            }
        }

        [DefaultValue(true)]
        public bool AutoClipboard
        {
            get
            {
                return this.axfpSpread1.AutoClipboard;
            }
            set
            {
                this.axfpSpread1.AutoClipboard = value;
            }
        }

        [DefaultValue(false)]
        public bool AllowMultiBlocks
        {
            get
            {
                return this.axfpSpread1.AllowMultiBlocks;
            }
            set
            {
                this.axfpSpread1.AllowMultiBlocks = value;
            }
        }

        [DefaultValue(true)]
        public bool MoveActiveOnFocus
        {
            get
            {
                return this.axfpSpread1.MoveActiveOnFocus;
            }
            set
            {
                this.axfpSpread1.MoveActiveOnFocus = value;
            }
        }

        [DefaultValue(false)]
        public bool ArrowsExitEditMode
        {
            get
            {
                return this.axfpSpread1.ArrowsExitEditMode;
            }
            set
            {
                this.axfpSpread1.ArrowsExitEditMode = value;
            }
        }

        [DefaultValue(0)]
        public int VisibleRows
        {
            get
            {
                return this.axfpSpread1.VisibleRows;
            }
            set
            {
                this.axfpSpread1.VisibleRows = value;
            }
        }

        [DefaultValue(0)]
        public int VisibleCols
        {
            get
            {
                return this.axfpSpread1.VisibleCols;
            }
            set
            {
                this.axfpSpread1.VisibleCols = value;
            }
        }

        [DefaultValue(ScrollBarsConstants.ScrollBarsBoth)]
        public ScrollBarsConstants ScrollBars
        {
            get
            {
                return this.axfpSpread1.ScrollBars;
            }
            set
            {
                this.axfpSpread1.ScrollBars = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public double TypeNumberMax
        {
            get
            {
                return this.axfpSpread1.TypeNumberMax;
            }
            set
            {
                this.axfpSpread1.TypeNumberMax = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public double TypeNumberMin 
        {
            get
            {
                return this.axfpSpread1.TypeNumberMin;
            }
            set
            {
                this.axfpSpread1.TypeNumberMin = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool TypePictStretch
        {
            get
            {
                return this.axfpSpread1.TypePictStretch;
            }
            set
            {
                this.axfpSpread1.TypePictStretch = value;
            }
        }
        
        [DefaultValue(HeaderDisplayConstants.DispLetters)]
        public HeaderDisplayConstants ColHeadersAutoText
        {
            get
            {
                return this.axfpSpread1.ColHeadersAutoText;
            }
            set
            {
                this.axfpSpread1.ColHeadersAutoText = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool FontItalic
        {
            get
            {
                return this.axfpSpread1.FontItalic;
            }
            set
            {
                this.axfpSpread1.FontItalic = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool SelModeSelected
        {
            get
            {
                return this.axfpSpread1.SelModeSelected;
            }
            set
            {
                this.axfpSpread1.SelModeSelected = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool PrintBorder
        {
            get
            {
                return this.axfpSpread1.PrintBorder;
            }
            set
            {
                this.axfpSpread1.PrintBorder = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool PrintShadows
        {
            get
            {
                return this.axfpSpread1.PrintShadows;
            }
            set
            {
                this.axfpSpread1.PrintShadows = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool PrintRowHeaders
        {
            get
            {
                return this.axfpSpread1.PrintRowHeaders;
            }
            set
            {
                this.axfpSpread1.PrintRowHeaders = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool PrintColHeaders
        {
            get
            {
                return this.axfpSpread1.PrintColHeaders;
            }
            set
            {
                this.axfpSpread1.PrintColHeaders = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string PrintHeader
        {
            get
            {
                return this.axfpSpread1.PrintHeader;
            }
            set
            {
                this.axfpSpread1.PrintHeader = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int PrintMarginLeft
        {
            get
            {
                return this.axfpSpread1.PrintMarginLeft;
            }
            set
            {
                this.axfpSpread1.PrintMarginLeft = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int PrintMarginBottom
        {
            get
            {
                return this.axfpSpread1.PrintMarginBottom;
            }
            set
            {
                this.axfpSpread1.PrintMarginBottom = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public PrintOrientationConstants PrintOrientation
        {
            get
            {
                return this.axfpSpread1.PrintOrientation;
            }
            set
            {
                this.axfpSpread1.PrintOrientation = value;
            }
        }
        
        [DefaultValue(true)]
        public bool Protect
        {
            get
            {
                return this.axfpSpread1.Protect;
            }
            set
            {
                this.axfpSpread1.Protect = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public HeaderDisplayConstants RowHeaderDisplay
        {
            get
            {
                return this.axfpSpread1.RowHeaderDisplay;
            }
            set
            {
                this.axfpSpread1.RowHeaderDisplay = value;
            }
        }
        
        [DefaultValue(1)]
        public int ColHeaderRows
        {
            get
            {
                return this.axfpSpread1.ColHeaderRows;
            }
            set
            {
                this.axfpSpread1.ColHeaderRows = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public short TypeMaxEditLen
        {
            get
            {
                return this.axfpSpread1.TypeMaxEditLen;
            }
            set
            {
                this.axfpSpread1.TypeMaxEditLen = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool DisplayColHeaders
        {
            get
            {
                return this.axfpSpread1.DisplayColHeaders;
            }
            set
            {
                this.axfpSpread1.DisplayColHeaders = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int SelModeSelCount
        {
            get
            {
                return this.axfpSpread1.SelModeSelCount;
            }
            set
            {
                this.axfpSpread1.SelModeSelCount = value;
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public new IntPtr Handle
        {
            get
            {
                return this.axfpSpread1.Handle;
            }            
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public HeaderDisplayConstants ColHeaderDisplay
        {
            get
            {
                return this.axfpSpread1.ColHeaderDisplay;
            }
            set
            {
                this.axfpSpread1.ColHeaderDisplay = value;
            }
        }

        [DefaultValue(HeaderDisplayConstants.DispNumbers)]
        public HeaderDisplayConstants RowHeadersAutoText
        {
            get
            {
                return this.axfpSpread1.RowHeadersAutoText;
            }
            set
            {
                this.axfpSpread1.RowHeadersAutoText = value;
            }
        }
        
        [DefaultValue(1)]
        public int RowHeaderCols
        {
            get
            {
                return this.axfpSpread1.RowHeaderCols;
            }
            set
            {
                this.axfpSpread1.RowHeaderCols = value;
            }
        }          

        /// <summary>
        /// Returns or sets the Width as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int WidthOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Width, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Width = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Height as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int HeightOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Height, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Height = Convert.ToInt32(fcGraphics.ScaleY(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Top as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int TopOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleY(base.Top, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this))) - FCUtils.GetMainMenuHeightOriginal(this);
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Top = Convert.ToInt32(fcGraphics.ScaleY(value + FCUtils.GetMainMenuHeightOriginal(this), fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        /// <summary>
        /// Returns or sets the Left as in VB6, depending on the scale mode of the parent (form or picturebox)
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public int LeftOriginal
        {
            get
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    return Convert.ToInt32(fcGraphics.ScaleX(base.Left, ScaleModeConstants.vbPixels, fcGraphics.GetParentScaleMode(this)));
                }
            }
            set
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    base.Left = Convert.ToInt32(fcGraphics.ScaleX(value, fcGraphics.GetParentScaleMode(this), ScaleModeConstants.vbPixels));
                }
            }
        }

        [DefaultValue("")]
        public string ToolTipText
        {
            get
            {
                return toolTip.GetToolTip(this);
            }
            set
            {
                toolTip.SetToolTip(this, value);
            }
        }

        #endregion

        #region Internal Properties
        #endregion

        #region Public Methods

        public void set_RowHeight(int lRow, double param0)
        {
            float rowHeight = 0;
            //this.axfpSpread1.TwipsToRowHeight(lRow, (int)param0, ref rowHeight);
            rowHeight = (float)param0;
            this.axfpSpread1.set_RowHeight(lRow, rowHeight);
            //CHE: set flag if value was set for first row
            if (!this.axfpSpread1.ColHeadersShow && lRow == 0 && rowHeight > 0)
            {
                this.axfpSpread1.ColHeadersShow = true;
            }
        }

        public void set_ColWidth(int lCol, double param0)
        {
            float colWidth = 0;
            //this.axfpSpread1.TwipsToColWidth((int)param0, ref colWidth);
            colWidth = (float)param0;
            this.axfpSpread1.set_ColWidth(lCol, colWidth);
            //CHE: set flag if value was set for first column
            if (!this.axfpSpread1.RowHeadersShow && lCol == 0 && colWidth > 0)
            {
                this.axfpSpread1.RowHeadersShow = true;
            }
        }

        public double get_RowHeight(int lRow)
        {
            int twips = 0;
            this.axfpSpread1.RowHeightToTwips(lRow, (float)this.axfpSpread1.get_RowHeight(lRow), ref twips);
            return twips;
        }

        public double get_ColWidth(int lCol)
        {
            int twips = 0;
            this.axfpSpread1.ColWidthToTwips((float)this.axfpSpread1.get_ColWidth(lCol), ref twips);
            return twips;
        }

        public int get_SortKey(short nIndex)
        {
            return this.axfpSpread1.get_SortKey(nIndex);
        }

        public SortKeyOrderConstants get_SortKeyOrder(short nIndex)
        {
            return this.axfpSpread1.get_SortKeyOrder(nIndex);
        }

        public void set_SortKey(short nIndex, int param0)
        {
            this.axfpSpread1.set_SortKey(nIndex, param0);
        }

        public void set_SortKeyOrder(short nIndex, SortKeyOrderConstants param0)
        {
            this.axfpSpread1.set_SortKeyOrder(nIndex, param0);
        }

        public int GetMultiSelItem(int selPrev)
        {
            return this.axfpSpread1.GetMultiSelItem(selPrev);
        }

        public void SetActiveCell(int lCol, int lRow)
        {
            this.axfpSpread1.SetActiveCell(lCol, lRow);
        }

        public bool Sort(int lCol, int lRow, int lCol2, int lRow2, SortByConstants nSortBy)
        {
            return this.axfpSpread1.Sort(lCol, lRow, lCol2, lRow2, nSortBy);
        }

        public bool Sort(int lCol, int lRow, int lCol2, int lRow2, SortByConstants nSortBy, ref object sortKeys, ref object sortKeyOrders)
        {
            return this.axfpSpread1.Sort(lCol, lRow, lCol2, lRow2, nSortBy, ref sortKeys, ref sortKeyOrders);
        }

        public void ClearRange(int lCol, int lRow, int lCol2, int lRow2, bool bDataOnly)
        {
            this.axfpSpread1.ClearRange(lCol, lRow, lCol2, lRow2, bDataOnly);
        }

        public void ShowCell(int lCol, int lRow, PositionConstants nPosition)
        {
            this.axfpSpread1.ShowCell(lCol, lRow, nPosition);
        }

        public void ShowCell(int lIndex, ref object plCol, ref object plRow, ref object plCol2, ref object plRow2)
        {
            this.axfpSpread1.GetSelection(lIndex, ref plCol, ref plRow, ref plCol2, ref plRow2);
        }
        
        public void SwapRange(int lCol, int lRow, int lCol2, int lRow2, int lColDest, int lRowDest)
        {
            this.axfpSpread1.SwapRange(lCol, lRow, lCol2, lRow2, lColDest, lRowDest);
        }

        public void DeleteRows(int lRow, int lNumRows)
        {
            this.axfpSpread1.DeleteRows(lRow, lNumRows);
        }

        public void GetSelection(int lIndex, ref object plCol, ref object plRow, ref object plCol2, ref object plRow2)
        {
            plCol = plRow = plCol2 = plRow2 = null;
            this.axfpSpread1.GetSelection(lIndex, ref plCol, ref plRow, ref plCol2, ref plRow2);
        }

        public void SetSelection(int lCol, int lRow, int lCol2, int lRow2)
        {
            this.axfpSpread1.SetSelection(lCol, lRow, lCol2, lRow2);
        }

        public void GetCellFromScreenCoord(ref int col, ref int row, int x, int y)
        {
            this.axfpSpread1.GetCellFromScreenCoord(ref col, ref row, x, y);
        }

        public void ColWidthToTwips(float width, ref int twips)
        {
            //CHE: the ocx version is returning some high value I did not find the logic
            if (fcGraphics.GetParentScaleMode(this) == ScaleModeConstants.vbTwips)
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    twips = Convert.ToInt32(width + fcGraphics.ScaleX(1, ScaleModeConstants.vbPixels, ScaleModeConstants.vbTwips));
                }
            }
            else
            {
                this.axfpSpread1.ColWidthToTwips(width, ref twips);
            }
        }

        public void RowHeightToTwips(int row, float height, ref int twips)
        {
            this.axfpSpread1.RowHeightToTwips(row, height, ref twips);
        }

        public void GetClientArea(ref int width, ref int height)
        {
            this.axfpSpread1.GetClientArea(ref width, ref height);
        }
        
        public bool AddCellSpan(int lCol, int lRow, int lNumCols, int lNumRows)
        {
            return this.axfpSpread1.AddCellSpan(lCol, lRow, lNumCols, lNumRows);
        }
        
        public void SetText(int col, int row, object var)
        {
            this.axfpSpread1.SetText(col, row, var);
        }

        public void GetText(int col, int row, ref object var)
        {
            this.axfpSpread1.GetText(col, row, ref var);
        }
        
        public bool GetCellPos(int col, int row, ref int x, ref int y, ref int width, ref int height)
        {
            return this.axfpSpread1.GetCellPos(col, row, ref x, ref y, ref width, ref height);
        }

        public void PrintSheet(ref object flags)
        {
            this.axfpSpread1.PrintSheet(ref flags);
        }

        #endregion

        #region Internal Methods
        #endregion

        #region Protected Methods
        #endregion

        #region Private Methods

        private void axfpSpread1_TopLeftChange(object sender, AxFPUSpreadADO._DSpreadEvents_TopLeftChangeEvent e)
        {
            if (TopLeftChange != null)
            {
                TopLeftChange(e.oldLeft, e.oldTop, e.newLeft, e.newTop);
            }
        }

        private void axfpSpread1_RightClick(object sender, AxFPUSpreadADO._DSpreadEvents_RightClickEvent e)
        {
            if (RightClick != null)
            {
                RightClick(e.clickType, e.col, e.row, e.mouseX, e.mouseY);
            }
        }

        private void axfpSpread1_MouseUpEvent(object sender, AxFPUSpreadADO._DSpreadEvents_MouseUpEvent e)
        {
            if (MouseUpEvent != null)
            {
                MouseUpEvent(e.button, e.shift, e.x, e.y);
            }
        }

        private void axfpSpread1_MouseMoveEvent(object sender, AxFPUSpreadADO._DSpreadEvents_MouseMoveEvent e)
        {
            if (MouseMoveEvent != null)
            {
                MouseMoveEvent(e.button, e.shift, e.x, e.y);
            }
        }

        private void axfpSpread1_LeaveRow(object sender, AxFPUSpreadADO._DSpreadEvents_LeaveRowEvent e)
        {
            if (LeaveRow != null)
            {
                LeaveRow(e.row, e.rowWasLast, e.rowChanged, e.allCellsHaveData, e.newRow, e.newRowIsLast, ref e.cancel);
            }
        }

        private void axfpSpread1_MouseDownEvent(object sender, AxFPUSpreadADO._DSpreadEvents_MouseDownEvent e)
        {
            //PJ: Get Position of MouseDown-Event for evaluation of Double-Click-Event
            this.doubleClickRectangle = new Rectangle(
                   e.x - (SystemInformation.DoubleClickSize.Width / 2),
                   e.y - (SystemInformation.DoubleClickSize.Height / 2),
                   SystemInformation.DoubleClickSize.Width,
                   SystemInformation.DoubleClickSize.Height);

            if (MouseDownEvent != null)
            {
                Point transformedPoint = TransformCoordinate(e.x, e.y);
                MouseDownEvent(e.button, e.shift, transformedPoint.X, transformedPoint.Y);
            }
        }

        private void axfpSpread1_LeaveCell(object sender, AxFPUSpreadADO._DSpreadEvents_LeaveCellEvent e)
        {
            if (LeaveCell != null)
            {
                LeaveCell(e.col, e.row, e.newCol, e.newRow, ref e.cancel);
                //PJU:Manually raising Key-Up-Event in Key-LeaveCell
                if (raiseKeyUpEvent)
                {
                    this.keyUpWEventTimer = new Timer();
                    this.keyUpWEventTimer.Tick += KeyUpWEventTimerTick;
                    this.keyUpWEventTimer.Interval = 10;
                    this.keyUpWEventTimer.Start();
                }
            }
        }

        private void axfpSpread1_KeyUpEvent(object sender, AxFPUSpreadADO._DSpreadEvents_KeyUpEvent e)
        {
            if (KeyUpEvent != null)
            {
                KeyUpEvent(ref e.keyCode, e.shift);
            }
            InitKeyEventArgs();
        }

        private void axfpSpread1_KeyPressEvent(object sender, AxFPUSpreadADO._DSpreadEvents_KeyPressEvent e)
        {
            if (KeyPressEvent != null)
            {
                KeyPressEvent(ref e.keyAscii);
            }
        }

        private void axfpSpread1_KeyDownEvent(object sender, AxFPUSpreadADO._DSpreadEvents_KeyDownEvent e)
        {
            if (KeyDownEvent != null)
            {
                //PJU:Prepare manually raising Key-Up-Event in Key-LeaveCell
                InitKeyEventArgs();
                if (e.shift == 0)
                {
                    this.raiseKeyUpEvent = true;
                    this.keyUpEventArgs = new AxFPUSpreadADO._DSpreadEvents_KeyUpEvent(e.keyCode, e.shift);
                    senderKeyUpEvent = sender;
                }
                KeyDownEvent(ref e.keyCode, e.shift);
            }
        }

        private void axfpSpread1_EnterRow(object sender, AxFPUSpreadADO._DSpreadEvents_EnterRowEvent e)
        {
            if (EnterRow != null)
            {
                EnterRow(e.row, e.rowIsLast);
            }
        }

        private void axfpSpread1_EditModeEvent(object sender, AxFPUSpreadADO._DSpreadEvents_EditModeEvent e)
        {
            if (EditModeEvent != null)
            {
                EditModeEvent(e.col, e.row, e.mode, e.changeMade);
            }
        }

        private void axfpSpread1_DblClick(object sender, AxFPUSpreadADO._DSpreadEvents_DblClickEvent e)
        {
            if (DblClick != null && this.isDoubleClick)
            {
                DblClick(e.col, e.row);
            }
            this.isDoubleClick = false;
        }

        private void axfpSpread1_ComboSelChange(object sender, AxFPUSpreadADO._DSpreadEvents_ComboSelChangeEvent e)
        {
            if (ComboSelChange != null)
            {
                ComboSelChange(e.col, e.row);
            }
        }

        private void axfpSpread1_ComboDropDown(object sender, AxFPUSpreadADO._DSpreadEvents_ComboDropDownEvent e)
        {
            if (ComboDropDown != null)
            {
                ComboDropDown(e.col, e.row);
            }
        }

        private void axfpSpread1_ColWidthChange(object sender, AxFPUSpreadADO._DSpreadEvents_ColWidthChangeEvent e)
        {
            if (ColWidthChange != null)
            {
                ColWidthChange(e.col1, e.col2);
            }
        }

        private void axfpSpread1_ClickEvent(object sender, AxFPUSpreadADO._DSpreadEvents_ClickEvent e)
        {
            this.isDoubleClick = false;
            if (ClickEvent != null)
            {
                ClickEvent(e.col, e.row);
            }
            this.doubleClickTimer.Start();
            this.isDoubleClick = true;
        }

        private void axfpSpread1_Change(object sender, AxFPUSpreadADO._DSpreadEvents_ChangeEvent e)
        {
            if (Change != null)
            {
                Change(e.col, e.row);
            }
        }

        private void axfpSpread1_ButtonClicked(object sender, AxFPUSpreadADO._DSpreadEvents_ButtonClickedEvent e)
        {
            if (ButtonClicked != null)
            {
                ButtonClicked(e.col, e.row, e.buttonDown);
            }
        }

        private Point TransformCoordinate(int X, int Y)
        {
            Point result = new Point(X, Y);

            if (this.UnitType == UnitTypeConstants.UnitTypeTwips)
            {
                using (Graphics g = this.CreateGraphics())
                {
                    fcGraphics.InitializeCoordinateSpace(g, this.Bounds, 0, 0);
                    result.X = Convert.ToInt32(fcGraphics.ScaleX(X, ScaleModeConstants.vbPixels, ScaleModeConstants.vbTwips));
                    result.Y = Convert.ToInt32(fcGraphics.ScaleY(Y, ScaleModeConstants.vbPixels, ScaleModeConstants.vbTwips));
                }
            }

            return result;
        }

        /// <summary>
        /// Init Key-Up-Event-Members
        /// </summary>
        private void InitKeyEventArgs()
        {
            this.raiseKeyUpEvent = false;
            this.keyUpEventArgs = null;
            this.senderKeyUpEvent = null;
        }

        /// <summary>
        /// Timer for manually raising Key-Up-Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KeyUpWEventTimerTick(object sender, EventArgs e)
        {
            this.keyUpWEventTimer.Stop();
            this.keyUpWEventTimer.Dispose();
            if (raiseKeyUpEvent)
            {
                axfpSpread1_KeyUpEvent(senderKeyUpEvent, keyUpEventArgs);
            }
        }

        /// <summary>
        /// Handle Double-Click-Ebent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DoubleClickTimer_Tick(object sender, EventArgs e)
        {
            this.milliseconds += 100;
            // The timer has reached the double click time limit.
            if (this.milliseconds >= SystemInformation.DoubleClickTime)
            {
                this.doubleClickTimer.Stop();
                this.isDoubleClick = false;
                this.milliseconds = 0;
            }
        }

        #endregion

    }
}
