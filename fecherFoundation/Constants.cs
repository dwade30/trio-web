﻿using System;
using Wisej.Web;

namespace fecherFoundation
{
    public class Constants
    {
        #region constants
        public const int TRANSPARENT = 1;
        public const int OPAQUE = 2;

        public static string vbNullChar = "";
        public static string vbNullString = "";
        public static string vbCrLf = "\r\n";
        public static string vbTab = "\t";
        public static string vbNewLine = Environment.NewLine;
        public const int vbSunday = 1;
        public const int vbMonday = 2;
        public const int vbTuesday = 3;
        public const int vbWednesday = 4;
        public const int vbThursday = 5;
        public const int vbFriday = 6;
        public const int vbSaturday = 7;

        public const int vbUseSystem = 0;
        public const int vbFirstJan1 = 1;
        public const int vbFirstFourDays = 2;
        public const int vbFirstFullWeek = 3;

        public const int vbMaximizedFocus = 1;
        public const int vbNormalNoFocus = 5;

        /// <summary>
        /// Member of VBA.Constants
        /// Constant indicating error is being returned from a Visual Basic object
        /// </summary>
        public const int vbObjectError = -2147221504; //(&H80040000);

        public static FormWindowState vbNormal = FormWindowState.Normal;

        public static DateTime MinDate = new DateTime(1899, 12, 30);

        #region VBColors
        public static long vbColorControlDarkDark = 0x80000015;
        public static long vbColorControlLightLight = 0x80000014;
        public static long vbColorControlLight = 0x80000016;
        public static long vbColorControlDark = 0x80000010;
        public static long vbColorActiveBorder = 0x8000000A;
        public static long vbColorActiveCaption = 0x80000002;
        public static long vbColorAppWorkspace = 0x8000000C;
        public static long vbColorControl = 0x8000000F;
        public static long vbColorControlText = 0x80000012;
        public static long vbColorDesktop = 0x80000001;
        public static long vbColorGrayText = 0x80000011;
        public static long vbColorHighlight = 0x8000000D;
        public static long vbColorHighlightText = 0x8000000E;
        public static long vbColorInactiveBorder = 0x8000000B;
        public static long vbColorInactiveCaptionText = 0x80000013;
        public static long vbColorInactiveCaption = 0x80000003;
        public static long vbColorInfo = 0x80000018;
        public static long vbColorInfoText = 0x80000017;
        public static long vbColorMenu = 0x80000004;
        public static long vbColorMenuText = 0x80000007;
        public static long vbColorScrollBar = 0x80000000;
        public static long vbColorActiveCaptionText = 0x80000009;
        public static long vbColorWindow = 0x80000005;
        public static long vbColorWindowFrame = 0x80000006;
        public static long vbColorWindowText = 0x80000008;
        #endregion
    }
    #endregion constants
}
