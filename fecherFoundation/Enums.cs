﻿using System;
using Wisej.Web;

namespace fecherFoundation
{
    #region enums
    /// <summary>
    /// VBA.VbTriState
    /// </summary>
    public enum VbTriState
    {
        vbFalse = 0,
        vbTrue = -1,
        vbUseDefault = -2
    }
    /// <summary>
    ///     Specifies a raster-operation code. These codes define how the color data for the
    ///     source rectangle is to be combined with the color data for the destination
    ///     rectangle to achieve the final color.
    /// </summary>
    public enum TernaryRasterOperations : uint
    {
        /// <summary>dest = source</summary>
        SRCCOPY = 0x00CC0020,
        /// <summary>dest = source OR dest</summary>
        SRCPAINT = 0x00EE0086,
        /// <summary>dest = source AND dest</summary>
        SRCAND = 0x008800C6,
        /// <summary>dest = source XOR dest</summary>
        SRCINVERT = 0x00660046,
        /// <summary>dest = source AND (NOT dest)</summary>
        SRCERASE = 0x00440328,
        /// <summary>dest = (NOT source)</summary>
        NOTSRCCOPY = 0x00330008,
        /// <summary>dest = (NOT src) AND (NOT dest)</summary>
        NOTSRCERASE = 0x001100A6,
        /// <summary>dest = (source AND pattern)</summary>
        MERGECOPY = 0x00C000CA,
        /// <summary>dest = (NOT source) OR dest</summary>
        MERGEPAINT = 0x00BB0226,
        /// <summary>dest = pattern</summary>
        PATCOPY = 0x00F00021,
        /// <summary>dest = DPSnoo</summary>
        PATPAINT = 0x00FB0A09,
        /// <summary>dest = pattern XOR dest</summary>
        PATINVERT = 0x005A0049,
        /// <summary>dest = (NOT dest)</summary>
        DSTINVERT = 0x00550009,
        /// <summary>dest = BLACK</summary>
        BLACKNESS = 0x00000042,
        /// <summary>dest = WHITE</summary>
        WHITENESS = 0x00FF0062,
        /// <summary>
        /// Capture window as seen on screen.  This includes layered windows 
        /// such as WPF windows with AllowsTransparency="true"
        /// </summary>
        CAPTUREBLT = 0x40000000
    }
    /// <summary>
    /// DAO.RecordsetOptionEnum
    /// </summary>
    public enum RecordsetOptionEnum
    {
        dbAppendOnly = 8,
        dbConsistent = 32,
        dbDenyRead = 2,
        dbDenyWrite = 1,
        dbExecDirect = 2048,
        dbFailOnError = 128,
        dbForwardOnly = 256,
        dbInconsistent = 16,
        dbReadOnly = 4,
        dbRunAsync = 1024,
        dbSeeChanges = 512,
        dbSQLPassThrough = 64
    }

    /// <summary>
    /// VB.PictureBox and VB.Line DrawMode
    /// </summary>
    public enum DrawModeConstants
    {
        VbBlackness = 1,
        VbNotMergePen = 2,
        VbMaskNotPen = 3,
        VbNotCopyPen = 4,
        VbMaskPenNot = 5,
        VbInvert = 6,
        VbXorPen = 7,
        VbNotMaskPen = 8,
        VbMaskPen = 9,
        VbNotXorPen = 10,
        VbNop = 11,
        VbMergeNotPen = 12,
        VbCopyPen = 13,
        VbMergePenNot = 14,
        VbMergePen = 15,
        VbWhiteness = 16
    }

    /// <summary>
    /// Line Method
    /// </summary>
    public enum LineDrawStep
    {
        Relative,
        Absolute
    }

    /// <summary>
    /// Line Method
    /// </summary>
    public enum LineDrawConstants
    {
        B,
        BF,
        L   //Add value for line (this is the default)
    }

    /// <summary>
    /// Printer.ScaleMode
    /// </summary>
    public enum ScaleModeConstants
    {
        vbUser = 0,
        vbTwips = 1,
        vbPoints = 2,
        vbPixels = 3,
        vbCharacters = 4,
        vbInches = 5,
        vbMillimeters = 6,
        vbCentimeters = 7,
        vbHimetric = 8,
        vbContainerPosition = 9,
        vbContainerSize = 10
    }

    /// <summary>
    /// The FCThreeDLine can be displayed vertically or horizontally
    /// </summary>
    public enum DisplayMode : int
    {
        Horizontal = 0,
        Vertical = 1
    }

    /// <summary>
    /// VB.PictureBox.FillStyle
    /// </summary>
    public enum FillStyleConstants : int
    {
        vbFSSolid = 0,
        vbFSTransparent = 1,
        vbHorizontalLine = 2,
        vbVerticalLine = 3,
        vbUpwardDiagonal = 4,
        vbDownwardDiagonal = 5,
        vbCross = 6,
        vbDiagonalCross = 7
    }

    /// <summary>
    /// 
    /// </summary>
    public enum RasterOpConstants
    {
        vbDstInvert,
        vbMergeCopy,
        vbMergePaint,
        vbNotSrcCopy,
        vbNotSrcErase,
        vbPatCopy,
        vbPatInvert,
        vbPatPaint,
        vbSrcAnd,
        vbSrcCopy,
        vbSrcErase,
        vbSrcInvert,
        vbSrcPaint
    }

    /// <summary>
    /// 
    /// </summary>
    public enum DrawStyleConstants
    {
        vbSolid = 0,
        vbDash = 1,
        vbDot = 2,
        vbDashDot = 3,
        vbDashDotDot = 4,
        vbInvisible = 5,
        vbInsideSolid = 6
    }

    /// <summary>
    /// Printer.Orientation
    /// </summary>
    public enum PrinterObjectConstants
    {
        vbPRORPortrait = 1,
        vbPRORLandscape = 2
    }

    /// <summary>
    /// 
    /// </summary>
    public enum PictureTypeConstants
    {
        vbPicTypeNone = 0,
        vbPicTypeBitmap = 1,
        vbPicTypeMetafile = 2,
        vbPicTypeIcon = 3,
        vbPicTypeEMetaFile = 4
    }

    /// <summary>
    /// DataTypeEnum
    /// </summary>
    public enum DataTypeEnum
    {
        dbAttachment = 101,
        dbBigInt = 16,
        dbBinary = 9,
        dbBoolean = 1,
        dbByte = 2,
        dbChar = 18,
        dbComplexByte = 102,
        dbComplexDecimal = 108,
        dbComplexDouble = 106,
        dbComplexGUID = 107,
        dbComplexInteger = 103,
        dbComplexLong = 104,
        dbComplexSingle = 105,
        dbComplexText = 109,
        dbCurrency = 5,
        dbDate = 8,
        dbDecimal = 20,
        dbDouble = 7,
        dbFloat = 21,
        dbGUID = 15,
        dbInteger = 3,
        dbLong = 4,
        dbLongBinary = 11,
        dbMemo = 12,
        dbNumeric = 19,
        dbSingle = 6,
        dbText = 10,
        dbTime = 22,
        dbTimeStamp = 23,
        dbVarBinary = 17
    }

    /// <summary>
    /// DAO.RecordsetTypeEnum
    /// </summary>
    public enum RecordsetTypeEnum
    {
        dbOpenDynamic = 16,
        dbOpenDynaset = 2,
        dbOpenForwardOnly = 8,
        dbOpenSnapshot = 4,
        dbOpenTable = 1
    }

    /// <summary>
    /// DAO.DatabaseTypeEnum
    /// </summary>
    public enum DatabaseTypeEnum
    {
        dbDecrypt = 4,
        dbEncrypt = 2,
        dbVersion10 = 1,
        dbVersion11 = 8,
        dbVersion20 = 16,
        dbVersion30 = 32,
        dbVersion40 = 64
    }

    /// <summary>
    /// DAO.CollatingOrderEnum
    /// </summary>
    public enum CollatingOrderEnum
    {
        dbSortArabic = 1025,
        dbSortChineseSimplified = 2052,
        dbSortChineseTraditional = 1028,
        dbSortCyrillic = 1049,
        dbSortCzech = 1029,
        dbSortDutch = 1043,
        dbSortGeneral = 1033,
        dbSortGreek = 1032,
        dbSortHebrew = 1037,
        dbSortHungarian = 1038,
        dbSortIcelandic = 1039,
        dbSortJapanese = 1041,
        dbSortKorean = 1042,
        dbSortNeutral = 1024,
        dbSortNorwdan = 1030,
        dbSortPDXIntl = 1033,
        dbSortPDXNor = 1030,
        dbSortPDXSwe = 1053,
        dbSortPolish = 1045,
        dbSortSlovenian = 1060,
        dbSortSpanish = 1034,
        dbSortSwedFin = 1053,
        dbSortThai = 1054,
        dbSortTurkish = 1055,
        dbSortUndefined = -1
    }

    /// <summary>
    /// DAO.TableDefAttributeEnum
    /// </summary>
    public enum TableDefAttributeEnum
    {
        dbAttachedODBC = 536870912,
        dbAttachedTable = 1073741824,
        dbAttachExclusive = 65536,
        dbAttachSavePWD = 131072,
        dbHiddenObject = 1,
        dbSystemObject = -2147483646
    }

  
    /// <summary>
    /// Threed.SSPanel.Alignment
    /// </summary>
    public enum AlignPanelTextConstants
    {
        _LeftTop = 0,
        _LeftMiddle = 1,
        _LeftBottom = 2,
        _RightTop = 3,
        _RightMiddle = 4,
        _RightBottom = 5,
        _CenterTop = 6,
        _CenterMiddle = 7,
        _CenterBottom = 8,
    }

    /// <summary>
    /// Threed.SSFrame.Font3D
    /// Threed.SSPanel.Font3D
    /// </summary>
    public enum Font3DConstants
    {
        _None = 0,
        _RaisedLight = 1,
        _RaisedHeavy = 2,
        _InsetLight = 3,
        _InsetHeavy = 4
    }

    /// <summary>
    /// Threed.SSPanel.BevelOuter
    /// </summary>
    public enum BevelConstants
    {
        _None = 0,
        _Inset = 1,
        _Raised = 2
    }

   
    /// <summary>
    /// VBRUN.DragModeConstants
    /// </summary>
    public enum DragModeConstants
    {
        vbManual = 0,
        vbAutomatic = 1
    }

    /// <summary>
    /// VBRUN.DragConstants
    /// </summary>
    public enum DragConstants
    {
        vbCancel = 0,
        vbBeginDrag = 1,
        vbEndDrag = 2
    }

    /// <summary>
    /// OKOnly 0
    /// Displays OK button only.
    /// OKCancel 1
    /// Displays OK and Cancel buttons.
    /// AbortRetryIgnore 2
    /// Displays Abort, Retry, and Ignore buttons.
    /// YesNoCancel 3
    /// Displays Yes, No, and Cancel buttons.
    /// YesNo 4
    /// Displays Yes and No buttons.
    /// RetryCancel 5
    /// Displays Retry and Cancel buttons.
    /// Critical 16
    /// Displays Critical Message icon.
    /// Question 32
    /// Displays Warning Query icon.
    /// Exclamation 48
    /// Displays Warning Message icon.
    /// Information 64
    /// Displays Information Message icon.
    /// DefaultButton1 0
    /// First button is default.
    /// DefaultButton2 256
    /// Second button is default.
    /// DefaultButton3 512
    /// Third button is default.
    /// ApplicationModal 0
    /// Application is modal. The user must respond to the message box before continuing work in the current application.
    /// SystemModal 4096
    /// System is modal. All applications are suspended until the user responds to the message box.
    /// MsgBoxSetForeground 65536
    /// Specifies the message box window as the foreground window.
    /// MsgBoxRight 524288
    /// Text is right-aligned.
    /// MsgBoxRtlReading 1048576
    /// Specifies text should appear as right-to-left reading on Hebrew and Arabic systems.    /// 
    /// </summary>
    [Flags]
    public enum MsgBoxStyle
    {
        AbortRetryIgnore = 2,
        ApplicationModal = 0,
        Critical = 0x10,//16
        DefaultButton1 = 0,
        DefaultButton2 = 0x100,//256
        DefaultButton3 = 0x200,//512
        Exclamation = 0x30,//48
        Information = 0x40,//64
        MsgBoxHelp = 0x4000,
        MsgBoxRight = 0x80000,//524288
        MsgBoxRtlReading = 0x100000,//1048576
        MsgBoxSetForeground = 0x10000,
        OkCancel = 1,
        OkOnly = 0,
        Question = 0x20,//32
        RetryCancel = 5,
        SystemModal = 0x1000,
        YesNo = 4,
        YesNoCancel = 3
    }

    // Summary:
    //     Provides constants for compatibility with the shift parameter mask constants
    //     in Visual Basic 6.0.
    public enum ShiftConstants
    {
        // Summary:
        //     Equivalent to the Visual Basic 6.0 constant vbShiftMask.
        ShiftMask = 1,
        //
        // Summary:
        //     Equivalent to the Visual Basic 6.0 constant vbCtrlMask.
        CtrlMask = 2,
        //
        // Summary:
        //     Equivalent to the Visual Basic 6.0 constant vbAltMask.
        AltMask = 4,
    }

    // Summary:
    //     Provides constants for compatibility with the shift parameter mask constants
    //     in Visual Basic 6.0.
    public enum MouseButtonConstants
    {
        // Summary:
        //     Equivalent to the Visual Basic 6.0 constant vbLeftButton.
        LeftButton = 1,
        //
        // Summary:
        //     Equivalent to the Visual Basic 6.0 constant vbRightButton.
        RightButton = 2,
        //
        // Summary:
        //     Equivalent to the Visual Basic 6.0 constant vbMiddleButton.
        MiddleButton = 4,
    }

    public enum MousePointerConstants
    {
        vbDefault = 0,
        vbArrow = 1,
        vbCrosshair = 2,
        vbIbeam = 3,
        vbIconPointer = 4,
        vbSizePointer = 5,
        vbSizeNESW = 6,
        vbSizeNS = 7,
        vbSizeNWSE = 8,
        vbSizeWE = 9,
        vbUpArrow = 10,
        vbHourglass = 11,
        vbNoDrop = 12,
        vbArrowHourglass = 13,
        vbArrowQuestion = 14,
        vbSizeAll = 15,
        vbCustom = 99
    }

    #region FONT-Data
    public enum FontWeight : int
    {
        FW_DONTCARE = 0,
        FW_THIN = 100,
        FW_EXTRALIGHT = 200,
        FW_LIGHT = 300,
        FW_NORMAL = 400,
        FW_MEDIUM = 500,
        FW_SEMIBOLD = 600,
        FW_BOLD = 700,
        FW_EXTRABOLD = 800,
        FW_HEAVY = 900,
    }
    public enum FontCharSet : byte
    {
        ANSI_CHARSET = 0,
        DEFAULT_CHARSET = 1,
        SYMBOL_CHARSET = 2,
        SHIFTJIS_CHARSET = 128,
        HANGEUL_CHARSET = 129,
        HANGUL_CHARSET = 129,
        GB2312_CHARSET = 134,
        CHINESEBIG5_CHARSET = 136,
        OEM_CHARSET = 255,
        JOHAB_CHARSET = 130,
        HEBREW_CHARSET = 177,
        ARABIC_CHARSET = 178,
        GREEK_CHARSET = 161,
        TURKISH_CHARSET = 162,
        VIETNAMESE_CHARSET = 163,
        THAI_CHARSET = 222,
        EASTEUROPE_CHARSET = 238,
        RUSSIAN_CHARSET = 204,
        MAC_CHARSET = 77,
        BALTIC_CHARSET = 186,
    }
    public enum FontPrecision : byte
    {
        OUT_DEFAULT_PRECIS = 0,
        OUT_STRING_PRECIS = 1,
        OUT_CHARACTER_PRECIS = 2,
        OUT_STROKE_PRECIS = 3,
        OUT_TT_PRECIS = 4,
        OUT_DEVICE_PRECIS = 5,
        OUT_RASTER_PRECIS = 6,
        OUT_TT_ONLY_PRECIS = 7,
        OUT_OUTLINE_PRECIS = 8,
        OUT_SCREEN_OUTLINE_PRECIS = 9,
        OUT_PS_ONLY_PRECIS = 10,
    }
    public enum FontClipPrecision : byte
    {
        CLIP_DEFAULT_PRECIS = 0,
        CLIP_CHARACTER_PRECIS = 1,
        CLIP_STROKE_PRECIS = 2,
        CLIP_MASK = 0xf,
        CLIP_LH_ANGLES = (1 << 4),
        CLIP_TT_ALWAYS = (2 << 4),
        CLIP_DFA_DISABLE = (4 << 4),
        CLIP_EMBEDDED = (8 << 4),
    }
    public enum FontQuality : byte
    {
        DEFAULT_QUALITY = 0,
        DRAFT_QUALITY = 1,
        PROOF_QUALITY = 2,
        NONANTIALIASED_QUALITY = 3,
        ANTIALIASED_QUALITY = 4,
        CLEARTYPE_QUALITY = 5,
        CLEARTYPE_NATURAL_QUALITY = 6,
    }
    [Flags]
    public enum FontPitchAndFamily : byte
    {
        DEFAULT_PITCH = 0,
        FIXED_PITCH = 1,
        VARIABLE_PITCH = 2,
        FF_DONTCARE = (0 << 4),
        FF_ROMAN = (1 << 4),
        FF_SWISS = (2 << 4),
        FF_MODERN = (3 << 4),
        FF_SCRIPT = (4 << 4),
        FF_DECORATIVE = (5 << 4),
    }

    public enum CallType
    {
        Method,
        Get,
        Set,
        Let
    }

    #endregion

    #endregion enums

}
