using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fecherFoundation
{

	/// <summary>
	/// 
	/// </summary>
    public class Pane : Panel
    {
        static Random randomGen = new Random();
        private SSSplitter parentSplitter;
        internal SSSplitter ParentSplitter
        {
            get { return parentSplitter; }
            set { parentSplitter = value; }
        }
        private object containedControl;

        /// <summary>
        /// When setting the control to the pane, if the object is a string, search for it in the parent form controls list and add it to the pane controls collection
        /// If the object is a control, then add it to the panes control collection
        /// After adding the control to the panes control collection, set the docking to fill
        /// </summary>
        public object ContainedControl
        {
            get { return containedControl; }

            set
            {
                containedControl = value;
                if (containedControl == null)
                {
                    return;
                }
                Control control = null;
                if (containedControl.GetType() == typeof(string))
                {
                    control = this.parentSplitter.FindForm().Controls[containedControl.ToString()];
                }
                else if (containedControl is Control)
                {
                    control = containedControl as Control;
                }

                this.Controls.Add(control);
                control.Dock = DockStyle.Fill;
            }
        }

        public bool LockHeight { get; set; }
        public bool LockWidth { get; set; }

        public Pane()
        {
            //this.Size = new Size(0, 0);

            KnownColor[] names = (KnownColor[])Enum.GetValues(typeof(KnownColor));
            KnownColor randomColorName = names[randomGen.Next(names.Length)];
            this.BackColor = Color.FromKnownColor(randomColorName);
        }
    }
}
