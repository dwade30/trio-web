﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace fecherFoundation.Tests
{
    [TestClass]
    public class StringsTest
    {
        [TestMethod]
        public void TestRightB()
        {
            string testValue = null;
            string retVal = null;

            retVal = Strings.RightB(testValue, 2);
            Assert.AreEqual(retVal, "");

            retVal = null;
            retVal = Strings.RightB("ABC", 2);
            Assert.AreEqual(retVal, "C");
        }

        [TestMethod]
        public void TestReplace()
        {
            string retVal = null;

            retVal = Strings.Replace("ABCD", "CD", "EF");
            Assert.AreEqual(retVal, "ABEF");

            retVal = null;
            retVal = Strings.Replace(null, "", "");
            Assert.AreEqual(retVal, "");
        }
    }
}
