﻿namespace TrueDBGrid
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fctdbGrid1 = new fecherFoundation.FCTDBGrid();
            this.datPalletType = new fecherFoundation.FCData();
            ((System.ComponentModel.ISupportInitialize)(this.fctdbGrid1)).BeginInit();
            this.SuspendLayout();
            //
            // datPalletType
            //
            this.datPalletType.WhatsThisHelpID = 11706;
            this.datPalletType.RecordsetType = 1;  //Dynaset
            this.datPalletType.ReadOnly = false;
            this.datPalletType.Options = 0;
            this.datPalletType.Exclusive = false;
            this.datPalletType.DefaultType = 2;  //ODBC verwenden
            this.datPalletType.DefaultCursorType = 0;  //Standard-Cursor
            this.datPalletType.Name = "datPalletType";
            this.datPalletType.BackColor = System.Drawing.SystemColors.Control;
            this.datPalletType.ForeColor = System.Drawing.SystemColors.ControlText;
            this.datPalletType.ConnectAsString = "ODBC;DSN=DFT_LVS";
            this.datPalletType.DatabaseName = "";
            this.datPalletType.RecordSource = "SELECT * FROM tPalletType ORDER BY nID";
            this.datPalletType.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.datPalletType.Location = new System.Drawing.Point(0, 197);
            this.datPalletType.Size = new System.Drawing.Size(724, 23);
            this.datPalletType.Caption = "datPalletType";
            // 
            // fctdbGrid1
            // 
            this.fctdbGrid1.AllowAddNew = true;
            this.fctdbGrid1.AllowColMove = false;
            this.fctdbGrid1.AllowDelete = true;
            this.fctdbGrid1.AllowRowSelect = false;
            this.fctdbGrid1.AllowRowSizing = false;
            this.fctdbGrid1.BackgroundColor = System.Drawing.SystemColors.Window;
            this.fctdbGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.fctdbGrid1.DeadAreaBackColor = System.Drawing.SystemColors.AppWorkspace;
            this.fctdbGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fctdbGrid1.IsCaptionVisible = true;
            this.fctdbGrid1.Location = new System.Drawing.Point(0, 0);
            this.fctdbGrid1.Name = "fctdbGrid1";
            this.fctdbGrid1.RowHeight = 19F;
            this.fctdbGrid1.RowTemplate.Height = 19;
            this.fctdbGrid1.Size = new System.Drawing.Size(555, 492);
            this.fctdbGrid1.TabIndex = 0;
            this.fctdbGrid1.WhatsThisHelpID = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 492);
            this.Controls.Add(this.fctdbGrid1);
            this.Controls.Add(this.datPalletType);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fctdbGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private fecherFoundation.FCTDBGrid fctdbGrid1;
        public fecherFoundation.FCData datPalletType;
    }
}

