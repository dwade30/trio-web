﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using fecherFoundation;

namespace TrueDBGrid
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FillGrid();
            this.fctdbGrid1.Rows.Add();
        }

        public int FillGrid()
        {
            int SetGridParamPalType = 0;

            int nCol;
            int nColumn;
            FCValueItem vItem = null;
            int i;

            try
            {   // On Error GoTo SetGridParamPalTypeErrorHandle



                // remove all possibly existing columns first (will be created dynamically)
                while (fctdbGrid1.Columns.Count != 0)
                {
                    fctdbGrid1.Columns.Remove(0);
                }

                // init column counter
                nCol = -1;

                // add ID
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("nID");
                fctdbGrid1.Columns[nCol].HeaderText = "nID";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgRight);
                fctdbGrid1.Columns[nCol].SetColumnWidth(600);
                fctdbGrid1.Columns[nCol].Visible = true;

                // add name
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("sName");
                fctdbGrid1.Columns[nCol].HeaderText = "sName";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1300);

                // add PPS name
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("sPpsName");
                fctdbGrid1.Columns[nCol].HeaderText = "sPpsName";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgCenter);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1000);

                // add PLC Name
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("nPlcName");
                fctdbGrid1.Columns[nCol].HeaderText = "nPlcName";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgRight);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgRight);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1000);

                // add icon
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("sIcon");
                fctdbGrid1.Columns[nCol].HeaderText = "sIcon";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1200);

                // add length
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("nLength");
                fctdbGrid1.Columns[nCol].HeaderText = "nLength";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgRight);
                fctdbGrid1.Columns[nCol].SetColumnWidth(800);

                // add length tolerance
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("nTolLength");
                fctdbGrid1.Columns[nCol].HeaderText = "nTolL";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgRight);
                fctdbGrid1.Columns[nCol].SetColumnWidth(600);

                // add width
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("nWidth");
                fctdbGrid1.Columns[nCol].HeaderText = "nWidth";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgRight);
                fctdbGrid1.Columns[nCol].SetColumnWidth(800);

                // add width tolerance
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("nTolWidth");
                fctdbGrid1.Columns[nCol].HeaderText = "nTolW";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgRight);
                fctdbGrid1.Columns[nCol].SetColumnWidth(600);

                // add height
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("nHeight");
                fctdbGrid1.Columns[nCol].HeaderText = "nHeight";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgRight);
                fctdbGrid1.Columns[nCol].SetColumnWidth(800);

                // add height tolerance
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("nTolHeight");
                fctdbGrid1.Columns[nCol].HeaderText = "nTolH";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgRight);
                fctdbGrid1.Columns[nCol].SetColumnWidth(600);

                // add flag 'special pallet'
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("bIsSpecialPallet");
                fctdbGrid1.Columns[nCol].HeaderText = "bIsSpecialPallet";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgCenter);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1400);


                fctdbGrid1.Columns[nCol].ValueItems().Presentation(Presentation.dbgComboBox);
                vItem = new FCValueItem();
                vItem.Value = 0;
                vItem.DisplayValue = "No";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                vItem = new FCValueItem();
                vItem.Value = 1;
                vItem.DisplayValue = "Yes";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                fctdbGrid1.Columns[nCol].ValueItems().Translate = true;


                // add flag 'bCheckPallet' - if TRUE, the pallet has to be checked at pallet checker
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("bCheckPallet");
                fctdbGrid1.Columns[nCol].HeaderText = "bCheckPallet";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgCenter);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1400);


                fctdbGrid1.Columns[nCol].ValueItems().Presentation(Presentation.dbgComboBox);
                vItem = new FCValueItem();
                vItem.Value = 0;
                vItem.DisplayValue = "No";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                vItem = new FCValueItem();
                vItem.Value = 1;
                vItem.DisplayValue = "Yes";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                fctdbGrid1.Columns[nCol].ValueItems().Translate = true;


                // add flag 'use as pallet' - if TRUE, pallet can be use as pallet
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("bUseAsPallet");
                fctdbGrid1.Columns[nCol].HeaderText = "bUseAsPallet";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgCenter);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1400);


                fctdbGrid1.Columns[nCol].ValueItems().Presentation(Presentation.dbgComboBox);
                vItem = new FCValueItem();
                vItem.Value = 0;
                vItem.DisplayValue = "No";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                vItem = new FCValueItem();
                vItem.Value = 1;
                vItem.DisplayValue = "Yes";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                fctdbGrid1.Columns[nCol].ValueItems().Translate = true;


                // add flag 'use as TopBoard' - if TRUE, pallet can be use as top board
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("bUseAsTopBoard");
                fctdbGrid1.Columns[nCol].HeaderText = "bUseAsTopBoard";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgCenter);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1400);


                fctdbGrid1.Columns[nCol].ValueItems().Presentation(Presentation.dbgComboBox);
                vItem = new FCValueItem();
                vItem.Value = 0;
                vItem.DisplayValue = "No";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                vItem = new FCValueItem();
                vItem.Value = 1;
                vItem.DisplayValue = "Yes";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                fctdbGrid1.Columns[nCol].ValueItems().Translate = true;


                // add flag 'nTopBoardGenericType' - generic type of top board, e.g 1=pallet, 2=frame, 3=board, 4=paper board
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("nTopBoardGenericType");
                fctdbGrid1.Columns[nCol].HeaderText = "nTopBoardGenericType";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgCenter);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1400);


                fctdbGrid1.Columns[nCol].ValueItems().Presentation(Presentation.dbgComboBox);
                vItem = new FCValueItem();
                vItem.Value = 0;
                vItem.DisplayValue = "None";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                vItem = new FCValueItem();
                vItem.Value = 1;
                vItem.DisplayValue = "Pallet";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                vItem = new FCValueItem();
                vItem.Value = 2;
                vItem.DisplayValue = "Frame";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                vItem = new FCValueItem();
                vItem.Value = 3;
                vItem.DisplayValue = "Board";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                vItem = new FCValueItem();
                vItem.Value = 4;
                vItem.DisplayValue = "Paper";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);

                fctdbGrid1.Columns[nCol].ValueItems().Translate = true;


                // add flag 'bTurnTopBoard' - if TURE and pallet is type of top board, the pallet has to be turned before robot
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("bTurnTopBoard");
                fctdbGrid1.Columns[nCol].HeaderText = "bTurnTopBoard";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgCenter);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1400);


                fctdbGrid1.Columns[nCol].ValueItems().Presentation(Presentation.dbgComboBox);
                vItem = new FCValueItem();
                vItem.Value = 0;
                vItem.DisplayValue = "No";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                vItem = new FCValueItem();
                vItem.Value = 1;
                vItem.DisplayValue = "Yes";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                fctdbGrid1.Columns[nCol].ValueItems().Translate = true;


                // add flag 'bRevolvingSkid' - if TURE, pallet has a revolving skid
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("bRevolvingSkid");
                fctdbGrid1.Columns[nCol].HeaderText = "bRevolvingSkid";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgCenter);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1400);


                fctdbGrid1.Columns[nCol].ValueItems().Presentation(Presentation.dbgComboBox);
                vItem = new FCValueItem();
                vItem.Value = 0;
                vItem.DisplayValue = "No";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                vItem = new FCValueItem();
                vItem.Value = 1;
                vItem.DisplayValue = "Yes";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                fctdbGrid1.Columns[nCol].ValueItems().Translate = true;


                // add default strapping pos length
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("nDefaultStrapPosL");
                fctdbGrid1.Columns[nCol].HeaderText = "nDefaultStrapPosL";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgRight);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1800);

                // add default strapping pos width
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("nDefaultStrapPosW");
                fctdbGrid1.Columns[nCol].HeaderText = "nDefaultStrapPosW";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgRight);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1800);

                // add default strapping head length
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("cDefaultStrapHeadL");
                fctdbGrid1.Columns[nCol].HeaderText = "cDefaultStrapHeadL";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1800);

                fctdbGrid1.Columns[nCol].ValueItems().Presentation(Presentation.dbgComboBox);
                for (i = 0; i <= 10 - 1; i++)
                {
                    vItem.Value = i;
                    vItem.DisplayValue = i;
                    fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                    fctdbGrid1.Columns[nCol].ValueItems().Translate = true;
                } // i


                // add default strapping head width
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("cDefaultStrapHeadW");
                fctdbGrid1.Columns[nCol].HeaderText = "cDefaultStrapHeadW";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1800);

                fctdbGrid1.Columns[nCol].ValueItems().Presentation(Presentation.dbgComboBox);
                for (i = 0; i <= 10 - 1; i++)
                {
                    vItem.Value = i;
                    vItem.DisplayValue = i;
                    fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                    fctdbGrid1.Columns[nCol].ValueItems().Translate = true;
                } // i


                // add flag 'use bayonet' default strapping head length
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("bUseBayonetL");
                fctdbGrid1.Columns[nCol].HeaderText = "bUseBayonetL";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgRight);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgCenter);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1300);

                fctdbGrid1.Columns[nCol].ValueItems().Presentation(Presentation.dbgComboBox);
                vItem = new FCValueItem();
                vItem.Value = 0;
                vItem.DisplayValue = "No";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                vItem.Value = 1;
                vItem.DisplayValue = "Yes";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                fctdbGrid1.Columns[nCol].ValueItems().Translate = true;


                // add flag 'use bayonet' default strapping head width
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("bUseBayonetW");
                fctdbGrid1.Columns[nCol].HeaderText = "bUseBayonetW";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgCenter);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1300);

                fctdbGrid1.Columns[nCol].ValueItems().Presentation(Presentation.dbgComboBox);
                vItem = new FCValueItem();
                vItem.Value = 0;
                vItem.DisplayValue = "No";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                vItem.Value = 1;
                vItem.DisplayValue = "Yes";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                fctdbGrid1.Columns[nCol].ValueItems().Translate = true;


                // add flag 'pallet type is allowed to move on chain conveyors in length direction'
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("bAllowMoveOnChainL");
                fctdbGrid1.Columns[nCol].HeaderText = "bAllowMoveOnChainL";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgCenter);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1800);

                fctdbGrid1.Columns[nCol].ValueItems().Presentation(Presentation.dbgComboBox);
                vItem = new FCValueItem();
                vItem.Value = 0;
                vItem.DisplayValue = "No";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                vItem.Value = 1;
                vItem.DisplayValue = "Yes";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                fctdbGrid1.Columns[nCol].ValueItems().Translate = true;


                // add flag 'pallet type is allowed to move on chain conveyors in width direction'
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("bAllowMoveOnChainW");
                fctdbGrid1.Columns[nCol].HeaderText = "bAllowMoveOnChainW";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgCenter);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1800);

                fctdbGrid1.Columns[nCol].ValueItems().Presentation(Presentation.dbgComboBox);
                vItem = new FCValueItem();
                vItem.Value = 0;
                vItem.DisplayValue = "No";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                vItem.Value = 1;
                vItem.DisplayValue = "Yes";
                fctdbGrid1.Columns[nCol].ValueItems().Add(vItem);
                fctdbGrid1.Columns[nCol].ValueItems().Translate = true;


                // add nSkidOffsetL
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("nSkidOffsetL");
                fctdbGrid1.Columns[nCol].HeaderText = "nSkidOffsetL";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgRight);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1200);

                // add nSkidOffsetW
                nCol += 1;
                fctdbGrid1.Columns.Add(nCol);
                fctdbGrid1.Columns[nCol].DataField("nSkidOffsetW");
                fctdbGrid1.Columns[nCol].HeaderText = "nSkidOffsetW";
                fctdbGrid1.Columns[nCol].HeadAlignment(Alignment.dbgLeft);
                fctdbGrid1.Columns[nCol].Alignment(Alignment.dbgRight);
                fctdbGrid1.Columns[nCol].SetColumnWidth(1200);

                // set parameter for split 0 (default split)

                for (nColumn = 0; nColumn <= fctdbGrid1.Splits[0].Columns.Count - 1; nColumn++)
                {
                    fctdbGrid1.Splits[0].Columns[nColumn].Visible = true;
                } // nColumn

                fctdbGrid1.Splits[0].RecordSelectors = true;
                fctdbGrid1.Splits[0].ScrollBars = fecherFoundation.ScrollBars.dbgBoth;


                // set common columns paramters
                for (nColumn = 0; nColumn <= fctdbGrid1.Columns.Count - 1; nColumn++)
                {

                    fctdbGrid1.Columns[nColumn].HeadAlignment(Alignment.dbgCenter); // center
                    fctdbGrid1.Columns[nColumn].AllowSizing(true);

                } // nColumn

                // rebind the columns
                fctdbGrid1.ReBind();

                // With fctdbGrid1

                SetGridParamPalType = 0;
                return SetGridParamPalType;

            }
            catch
            {   // SetGridParamPalTypeErrorHandle:
                // Select Case Err.Number
                // End Select
                return SetGridParamPalType;

            }
        }
    }
}
