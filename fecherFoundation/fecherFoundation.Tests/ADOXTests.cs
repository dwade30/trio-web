﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Data;
using fecherFoundation.DataBaseLayer.ADOX;
using Microsoft.QualityTools.Testing.Fakes;
using System.Data.OleDb;
using System.Linq;
using System.Collections;

namespace fecherFoundation.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class ADOXTests
    {
        public ADOXTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        [ExpectedException(typeof(NotImplementedException))]
        public void AdoxCatalogActiveConnection_WrongProvider()
        {
            FCAdoxCatalog cat = new FCAdoxCatalog();

            cat.ActiveConnection = new System.Data.SqlClient.SqlConnection();
        }


        class AdoxHelper
        {
            public static DataTable GetSchema(string s, string[] restrictions = null)
            {
                DataRow r;
                var dt = new DataTable();

                if (s == "Tables")
                {
                    //return (from r in connection.GetSchema("Tables").AsEnumerable()
                    //select new FCTable(this, r.Field<string>("TABLE_SCHEMA"), r.Field<string>("TABLE_NAME"), r.Field<string>("TABLE_TYPE"))).ToList();

                    dt.Columns.Add("TABLE_SCHEMA");
                    dt.Columns.Add("TABLE_NAME");
                    dt.Columns.Add("TABLE_TYPE");

                    r = dt.NewRow();
                    r["TABLE_SCHEMA"] = "dbo";
                    r["TABLE_NAME"] = "table1";
                    r["TABLE_TYPE"] = "table";

                    dt.Rows.Add(r);
                }
                else if (s == "Columns")
                {
                    // return (from r in connection.GetSchema("Columns", new string[]{null, table.Schema, table.Name}).AsEnumerable()
                    //select new FCColumn(r.Field<string>("COLUMN_NAME"), r.Field<OleDbType>("DATA_TYPE"))).ToList();

                    // restictions contains schema (dbo) and table name (table1)
                    if (restrictions != null && restrictions[1] == "dbo" && restrictions[2] == "table1")
                    {
                        // return DataTable with 2 columns: 1. stringcolumn (string) 2. numcolumn (Int)
                        dt.Columns.Add("COLUMN_NAME");
                        dt.Columns.Add("DATA_TYPE", typeof(OleDbType));

                        r = dt.NewRow();
                        r["COLUMN_NAME"] = "stringcolumn";
                        r["DATA_TYPE"] = OleDbType.VarChar;
                        dt.Rows.Add(r);

                        r = dt.NewRow();
                        r["COLUMN_NAME"] = "numcolumn";
                        r["DATA_TYPE"] = OleDbType.Numeric;
                        dt.Rows.Add(r);
                    }
                }
                else if (s == "Indexes")
                {

                    //var list = from r in connection.GetSchema("Indexes", new string[] { null, table.Schema, null, null, table.Name }).AsEnumerable()
                    //select new FCIndex(this, table, r.Field<string>("INDEX_SCHEMA"), r.Field<string>("INDEX_NAME"), r.Field<bool>("PRIMARY_KEY"));


                    // return (from r in connection.GetSchema("Indexes", new string[] { null, index.Table.Schema, index.Name, null, index.Table.Name }).AsEnumerable()
                    // select index.Table.FindColumn( r.Field<string>("COLUMN_NAME")))

                    if (restrictions != null && restrictions[1] == "dbo" && restrictions[4] == "table1")
                    {
                        // return DataTable with 4 columns: 1. stringcolumn (string) 2. numcolumn (Int)
                        dt.Columns.Add("INDEX_SCHEMA");
                        dt.Columns.Add("INDEX_NAME");
                        dt.Columns.Add("PRIMARY_KEY", typeof(bool));
                        dt.Columns.Add("COLUMN_NAME");

                        r = dt.NewRow();
                        r["INDEX_SCHEMA"] = "dbo";
                        r["INDEX_NAME"] = "index 1";
                        r["PRIMARY_KEY"] = true;
                        r["COLUMN_NAME"] = "stringcolumn";
                        dt.Rows.Add(r);

                        r = dt.NewRow();
                        r["INDEX_SCHEMA"] = "dbo";
                        r["INDEX_NAME"] = "index 1";
                        r["PRIMARY_KEY"] = true;
                        r["COLUMN_NAME"] = "numcolumn";
                        dt.Rows.Add(r);

                        if (restrictions[2] == null)
                        {
                            // index name is empty => get all indexes

                            r = dt.NewRow();
                            r["INDEX_SCHEMA"] = "dbo";
                            r["INDEX_NAME"] = "index 2";
                            r["PRIMARY_KEY"] = false;
                            r["COLUMN_NAME"] = "icolumn2.1";
                            dt.Rows.Add(r);

                        }

                    }




                }

                return dt;
            }
        }


        [TestMethod]
        public void AdoxTables_OleDbProvider()
        {
            using (ShimsContext.Create())
            {
                // Arrange

                var shimOleDbConnection = new System.Data.OleDb.Fakes.ShimOleDbConnection()
                {
                    GetSchemaString = (s) => { return AdoxHelper.GetSchema(s); },
                    GetSchemaStringStringArray = (s, r) => { return AdoxHelper.GetSchema(s, r); }
                };


                FCAdoxCatalog cat = new FCAdoxCatalog();
                cat.ActiveConnection = (System.Data.OleDb.OleDbConnection)shimOleDbConnection;

                // Act

                var tables = cat.Tables;

                // Assert

                Assert.AreEqual(1, tables.Count);
                Assert.AreEqual("table1", tables[0].Name);
                Assert.AreSame(tables["table1"], tables[0]);

                Assert.AreEqual(2, tables[0].Columns.Count);
                Assert.AreEqual("stringcolumn", tables[0].Columns[0].Name);
                Assert.AreEqual("Numeric", tables[0].Columns[1].Type);
                Assert.AreSame(tables[0].Columns["numcolumn"], tables[0].Columns[1]);

                Assert.AreEqual(2, tables[0].Indexes.Count);
                Assert.AreEqual("index 1", tables[0].Indexes[0].Name);
                Assert.IsFalse(tables[0].Indexes[1].PrimaryKey);

                Assert.AreEqual(2, tables[0].Indexes[0].Columns.Count);
                Assert.AreEqual("stringcolumn", tables[0].Indexes[0].Columns[0].Name);
                Assert.AreEqual("numcolumn", tables[0].Indexes[0].Columns[1].Name);

                
                
            }
        }

    }
}
