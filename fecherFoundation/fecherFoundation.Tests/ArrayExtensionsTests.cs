﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using fecherFoundation.Extensions;
using System.Globalization;

namespace fecherFoundation.Tests
{
    [TestClass]
    public class ArrayExtensionsTests
    {

        [TestMethod]
        public void TestArrayDeepCopy()
        {
            var arrCI = new CultureInfo[] {
                            new CultureInfo("ar-SA"),
                            new CultureInfo("en-US"),
                            new CultureInfo("fr-FR"),
                            new CultureInfo("ja-JP")
            };


            var arrCIClone = (CultureInfo[])arrCI.Clone();
            var arrCIDeep = arrCI.DeepCopy();




            arrCIClone[3].DateTimeFormat.DateSeparator = "-";

            Assert.AreEqual(arrCI[0], arrCIClone[0]);

            Assert.AreSame(arrCI[0], arrCIClone[0]);
            Assert.AreEqual(arrCI[3].DateTimeFormat.DateSeparator, arrCIClone[3].DateTimeFormat.DateSeparator);


            arrCIDeep[3].DateTimeFormat.DateSeparator = "/";

            Assert.AreEqual(arrCI[0], arrCIClone[0]);
            Assert.AreNotSame(arrCI[0], arrCIDeep[0]);
            Assert.AreNotEqual(arrCI[3].DateTimeFormat.DateSeparator, arrCIDeep[3].DateTimeFormat.DateSeparator);

        }

    }
}
