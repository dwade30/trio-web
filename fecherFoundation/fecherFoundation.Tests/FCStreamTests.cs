﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace fecherFoundation.Tests
{
    /// <summary>
    /// Summary description for FCStreamTests
    /// </summary>
    [TestClass]
    public class FCStreamTests
    {
        public FCStreamTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void FCStream_InitialState()
        {
            using(FCStream stream = new FCStream())
            {
                stream.Type = StreamTypeEnum.adTypeBinary;

                stream.Open();

                Assert.AreEqual(0, stream.Position);

                Assert.AreEqual(0, stream.Size);

                Assert.AreEqual(StreamTypeEnum.adTypeBinary, stream.Type);

                stream.Close();
            }



        }

        [TestMethod]
        public void FCStream_Write()
        {
            using (FCStream stream = new FCStream())
            {
                byte[] buffer = { 1, 2, 3 };

                stream.Open();
                stream.Type = StreamTypeEnum.adTypeBinary;

                stream.Write(buffer);

                Assert.AreEqual(3, stream.Position);
                Assert.AreEqual(3, stream.Size);

                stream.Close();
            }
        }


        
        [TestMethod]
        public void FCStream_Read0()
        {
            FCStream stream = new FCStream();

            byte[] buffer = { 1, 2, 3 };

            stream.Open();
            stream.Type = StreamTypeEnum.adTypeBinary;

            stream.Write(buffer);

            Assert.AreEqual(3, stream.Position);
            Assert.AreEqual(3, stream.Size);

            stream.Position = 0;

            byte[] buf = (byte[])stream.Read();

            Assert.AreEqual(3, buf.Length, "buffer length");
            Assert.AreEqual(1, buf[0], "buf[0]");
            Assert.AreEqual(2, buf[1], "buf[1]");
            Assert.AreEqual(3, buf[2], "buf[2]");
            Assert.AreEqual(3, stream.Position);

            stream.Close();

        }

        [TestMethod]
        public void FCStream_Read1()
        {
            FCStream stream = new FCStream();

            byte[] buffer = { 1, 2, 3 };

            stream.Open();
            stream.Type = StreamTypeEnum.adTypeBinary;

            stream.Write(buffer);

            Assert.AreEqual(3, stream.Position);
            Assert.AreEqual(3, stream.Size);

            stream.Position = 0;

            byte[] buf = (byte[])stream.Read(2);

            Assert.AreEqual(2, buf.Length, "buffer length");
            Assert.AreEqual(1, buf[0], "buf[0]");
            Assert.AreEqual(2, buf[1], "buf[1]");
            Assert.AreEqual(2, stream.Position);


            buf = (byte[])stream.Read(2);

            Assert.AreEqual(1, buf.Length, "buffer length");
            Assert.AreEqual(3, buf[0], "buf[0]");
            Assert.AreEqual(3, stream.Position);

            stream.Close();

        }


        const string fileName = @"FCStream.SaveToFile.example";

        [TestMethod]
        public void FCStream_SaveToFile_adSaveCreateNotExists()
        {
            // SaveToFile uses adSaveCreateNotExist mode by default. So delete file if exists
            if (File.Exists(fileName)) File.Delete(fileName);

            FCStream stream = new FCStream();

            byte[] buffer = { 1, 2, 3 };

            stream.Open();
            stream.Type = StreamTypeEnum.adTypeBinary;

            stream.Write(buffer);

            Assert.AreEqual(3, stream.Position);
            Assert.AreEqual(3, stream.Size);

            stream.Position = 0;

            stream.SaveToFile(fileName);

            Assert.IsTrue(File.Exists(fileName), "file is created by SaveToFile");

            stream.Close();

            // check file
            byte[] readBuffer = new byte[4];
            int bytesRead = 0;
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader br = new BinaryReader(fs))
                {


                    bytesRead = br.Read(readBuffer, 0, 4);
                }
            } 
            
            Assert.AreEqual(3, bytesRead, "bytesRead");
            Assert.AreEqual(1, readBuffer[0], "readBuffer[0]");
            Assert.AreEqual(2, readBuffer[1], "readBuffer[1]");
            Assert.AreEqual(3, readBuffer[2], "readBuffer[2]");

        }

        [TestMethod]
        [ExpectedException(typeof(IOException))]
        public void FCStream_SaveToFile_adSaveCreateNotExists_Fail()
        {
            // SaveToFile uses adSaveCreateNotExist mode by default. create file => SaveToFile Fails
            if (!File.Exists(fileName)) File.Create(fileName);

            FCStream stream = new FCStream();

            byte[] buffer = { 1, 2, 3 };

            stream.Open();
            stream.Type = StreamTypeEnum.adTypeBinary;

            stream.Write(buffer);

            stream.SaveToFile(fileName);

            Assert.IsTrue(File.Exists(fileName), "file is created by SaveToFile");

            stream.Close();


        }



        [TestMethod]
        public void FCStream_SaveToFile_adSaveCreateOverwrite()
        {
            // we're using adSaveCreateOverwrite, check if File Is Overwritten
            File.AppendAllText(fileName, "ABCDEFGHIJKLMNOPQRSTUVWX");
            
            FCStream stream = new FCStream();

            byte[] buffer = { 1, 2, 3 };

            stream.Open();
            stream.Type = StreamTypeEnum.adTypeBinary;

            stream.Write(buffer);

            Assert.AreEqual(3, stream.Position);
            Assert.AreEqual(3, stream.Size);

            stream.Position = 0;

            stream.SaveToFile(fileName, SaveOptionsEnum.adSaveCreateOverWrite);
            
            Assert.IsTrue(File.Exists(fileName), "file is created by SaveToFile"); 
            
            stream.Close();

            // check file
            byte[] readBuffer = new byte[4]; 
            int bytesRead = 0;
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                Assert.AreEqual(3, fs.Length);

                using (BinaryReader br = new BinaryReader(fs))
                {


                    bytesRead = br.Read(readBuffer, 0, 4);
                }
            }    
            Assert.AreEqual(3, bytesRead, "bytesRead");
            Assert.AreEqual(1, readBuffer[0], "readBuffer[0]");
            Assert.AreEqual(2, readBuffer[1], "readBuffer[1]");
            Assert.AreEqual(3, readBuffer[2], "readBuffer[2]");

        }


        [TestMethod]
        public void FCStream_LoadFromFile()
        {
            if (!File.Exists(fileName))
                FCStream_SaveToFile_adSaveCreateOverwrite();

            Assert.IsTrue(File.Exists(fileName));



            FCStream stream = new FCStream();

            stream.Open();
            stream.Type = StreamTypeEnum.adTypeBinary;

            stream.LoadFromFile(fileName);

            byte[] buf = (byte[])stream.Read();

            Assert.AreEqual(3, buf.Length);
            Assert.AreEqual(1, buf[0], "buf[0]");
            Assert.AreEqual(2, buf[1], "buf[1]");
            Assert.AreEqual(3, buf[2], "buf[2]");
            Assert.AreEqual(3, stream.Size);

            stream.Close();

        }



        [TestMethod]
        public void FCStream_CopyTo_All()
        {
            FCStream streamFrom = new FCStream();
            FCStream streamTo = new FCStream();

            byte[] buffer = { 1, 2, 3 };

            streamFrom.Open();
            streamFrom.Type = StreamTypeEnum.adTypeBinary;

            streamFrom.Write(buffer);

            streamFrom.Position = 0;

            streamTo.Open();
            streamTo.Type = StreamTypeEnum.adTypeBinary;

            streamFrom.CopyTo(streamTo);
            streamFrom.Close();

            Assert.AreEqual(3, streamTo.Position);
            Assert.AreEqual(3, streamTo.Size);

            streamTo.Close();
        }

        [TestMethod]
        public void FCStream_CopyTo_2of3Bytes()
        {
            FCStream streamFrom = new FCStream();
            FCStream streamTo = new FCStream();

            byte[] buffer = { 1, 2, 3 };

            streamFrom.Open();
            streamFrom.Type = StreamTypeEnum.adTypeBinary;

            streamFrom.Write(buffer);

            streamTo.Open();
            streamTo.Type = StreamTypeEnum.adTypeBinary;

            streamFrom.Position = 1;
            streamFrom.CopyTo(streamTo);
            streamFrom.Close();

            Assert.AreEqual(2, streamTo.Position);
            Assert.AreEqual(2, streamTo.Size);

            streamTo.Position = 0;
            byte[] buf = (byte[])streamTo.Read();
            Assert.AreEqual(2, buf[0], "buf[0]");
            Assert.AreEqual(3, buf[1], "buf[1]");

            streamTo.Close();
        }


        [TestMethod]
        public void FCStream_CopyTo_1ByteInTheMiddle()
        {
            FCStream streamFrom = new FCStream();
            FCStream streamTo = new FCStream();

            byte[] buffer = { 1, 2, 3 };

            streamFrom.Open();
            streamFrom.Type = StreamTypeEnum.adTypeBinary;

            streamFrom.Write(buffer);

            streamTo.Open();
            streamTo.Type = StreamTypeEnum.adTypeBinary;

            streamFrom.Position = 1;
            streamFrom.CopyTo(streamTo, 1);
            streamFrom.Close();

            Assert.AreEqual(1, streamTo.Position);
            Assert.AreEqual(1, streamTo.Size);

            streamTo.Position = 0;
            byte[] buf = (byte[])streamTo.Read();
            Assert.AreEqual(2, buf[0], "buf[0]");

            streamTo.Close();
        }
    }
}
