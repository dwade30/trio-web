﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using GrapeCity.ActiveReports;
using System.Drawing;

#if TWCL0000
using TWCL0000;


#elif TWFA0000
using TWFA0000;


#elif TWAR0000
using TWAR0000;


#elif TWBL0000
using TWBL0000;


#elif TWRE0000
using TWRE0000;
#endif
namespace Global
{
	public class clsReportPrinterFunctions
	{
		//=========================================================
		string strPrinterFont = "";
		// name of printer font to use
		string strPrinterDeviceName = "";
		// device name of current printer

        public clsReportPrinterFunctions() : base()
		{
			strPrinterFont = "";
			strPrinterDeviceName = "";
		}

		public string GetFont(string strPrinterName, short intCPI, string strFont = "")
		{
			return string.Empty;
		}

		public void SetReportFontsByTag(GrapeCity.ActiveReports.SectionReport obReport, string strTag, string strFont = "", bool boolBold = false)
		{
			// accepts an activereport  and the tag value to look for
			// if you specify the font it will change the font
			// if you specify bold, it will bold it
			int x;
			int Y;
			for (Y = 0; Y <= obReport.Sections.Count - 1; Y++)
			{
				for (x = 0; x <= obReport.Sections[Y].Controls.Count - 1; x++)
				{
					if (Strings.UCase(obReport.Sections[Y].Controls[x].Tag.ToString()) == Strings.UCase(strTag))
					{
						if (strFont != string.Empty)
						{
							//FC:FINAL:SBE
							var currentFont = (obReport.Sections[Y].Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font;
							(obReport.Sections[Y].Controls[x] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new System.Drawing.Font(strFont, currentFont.Size, boolBold ? currentFont.Style & System.Drawing.FontStyle.Bold : currentFont.Style, currentFont.Unit);
							//obReport.Sections[Y].Controls[x].Font = new Font( strFont, //obReport.Sections[Y].Controls[x].Font.Size);
							//obReport.Sections[Y].Controls[x].Font.Bold = boolBold;
						}
					}
				}
				// x
			}
			// Y
		}

		public void ReportPrint(GrapeCity.ActiveReports.SectionReport obReport, bool boolShowPrinterDialog = false)
		{
            Global.Extensions.ExportToPDFAndPrint(obReport);
		}

		public string GetCurrentPrinterDeviceName()
		{
			string GetCurrentPrinterDeviceName = "";
			GetCurrentPrinterDeviceName = FCGlobal.Printer.DeviceName;
			return GetCurrentPrinterDeviceName;
		}
	}
}
