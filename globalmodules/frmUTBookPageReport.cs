//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;
using fecherFoundation;
using fecherFoundation.Extensions;
using modUTStatusPayments = Global.modUTFunctions;

#if TWUT0000
using TWUT0000;
#endif

namespace Global
{
    /// <summary>
    /// Summary description for frmUTBookPageReport.
    /// </summary>
    public partial class frmUTBookPageReport : BaseForm
    {

        public frmUTBookPageReport()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

            this.txtAccount = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
            this.txtAccount.AddControlArrayElement(txtAccount_0, 0);
            this.txtAccount.AddControlArrayElement(txtAccount_1, 1);
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null )
                _InstancePtr = this;
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmUTBookPageReport InstancePtr
        {
            get
            {
                if (_InstancePtr == null) // || _InstancePtr.IsDisposed
                    _InstancePtr = new frmUTBookPageReport();
                return _InstancePtr;
            }
        }
        protected static frmUTBookPageReport _InstancePtr = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>



        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               12/04/2006              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               12/04/2006              *
        // ********************************************************
        clsDRWrapper rsLien = new clsDRWrapper();
        bool boolDirty;
        bool boolLoaded;
        int intType;

        public void Init(short intPassType = 0)
        {
            try
            {   // On Error GoTo ERROR_HANDLER


                if (modUTStatusPayments.Statics.TownService == "W")
                {
                    cmbWS.Enabled = false;
                    cmbWS.Clear();
                    cmbWS.Items.Add("Water");
                    cmbWS.Text = "Water";
                    //optWS[1].Enabled = false;
                    //optWS[0].Checked = true;
                }
                else if (modUTStatusPayments.Statics.TownService == "S")
                {
                    cmbWS.Enabled = false;
                    cmbWS.Clear();
                    cmbWS.Items.Add("Sewer");
                    cmbWS.Text = "Sewer";
                    //optWS[0].Enabled = false;
                    //optWS[1].Checked = true;
                }
                else
                {
                    cmbWS.Enabled = true;
                    cmbWS.Clear();
                    cmbWS.Items.Add("Water");
                    cmbWS.Items.Add("Sewer");
                    cmbWS.Text = "Sewer";
                    //optWS[1].Checked = true;
                    //optWS[0].Enabled = true;
                    //optWS[0].Enabled = true;
                }

                // defaults to saving the lien book and page
                intType = intPassType;
                this.Show(App.MainForm);
                return;
            }
            catch
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void frmUTBookPageReport_Activated(object sender, System.EventArgs e)
        {
            if (!boolLoaded)
            {
                boolLoaded = true;
            }
        }

        private void frmUTBookPageReport_KeyDown(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);

            switch (KeyCode)
            {

                case Keys.F10:
                    {
                        KeyCode = (Keys)0;
                        break;
                    }
            } //end switch
        }

        private void frmUTBookPageReport_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmUTBookPageReport properties;
            //frmUTBookPageReport.FillStyle	= 0;
            //frmUTBookPageReport.ScaleWidth	= 5880;
            //frmUTBookPageReport.ScaleHeight	= 3900;
            //frmUTBookPageReport.LinkTopic	= "Form2";
            //frmUTBookPageReport.PaletteMode	= 1  'UseZOrder;
            //End Unmaped Properties

            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM); // this will set the size of the form to the smaller TRIO size (1/2 normal)
            modGlobalFunctions.SetTRIOColors(this); // this will set all the TRIO colors
        }

        private void frmUTBookPageReport_KeyPress(object sender, KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

            // catches the escape and enter keys
            if (KeyAscii == Keys.Escape)
            {
                KeyAscii = (Keys)0;
                this.Unload();
            }
            else if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                Support.SendKeys("{TAB}", false);
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void mnuFileExit_Click(object sender, System.EventArgs e)
        {
            this.Unload();
        }

        private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
        {
            if (ShowReport())
            {
                this.Unload();
            }
        }

        private void txtAccount_Enter(int Index, object sender, System.EventArgs e)
        {

            if (txtAccount[Index].Text != "")
            {
                txtAccount[Index].SelectionStart = 0;
                txtAccount[Index].SelectionLength = txtAccount[Index].Text.Length;
            }

        }
        private void txtAccount_Enter(object sender, System.EventArgs e)
        {
            int index = txtAccount.IndexOf((FCTextBox)sender);
            txtAccount_Enter(index, sender, e);
        }

        private bool ShowReport()
        {
            bool ShowReport = false;
            int lngEligible = 0;

            if (cmbBPType.Text == "Eligible for Lien")
            {
                lngEligible = 3;
            }
            else
            {
                lngEligible = 0;
            }

            if (Conversion.Val(txtAccount[0].Text) != 0)
            {
                if (Conversion.Val(txtAccount[1].Text) == 0 || Conversion.Val(txtAccount[1].Text) == Conversion.Val(txtAccount[0].Text))
                {
                    sarUTBookPageAccount.InstancePtr.Init(FCConvert.CBool(cmbWS.Text == "Water"), FCConvert.ToInt32(Conversion.Val(txtAccount[0].Text)), FCConvert.ToInt32(Conversion.Val(txtAccount[0].Text)), 0, 0, lngEligible);
                }
                else
                {
                    sarUTBookPageAccount.InstancePtr.Init(FCConvert.CBool(cmbWS.Text == "Water"), FCConvert.ToInt32(Conversion.Val(txtAccount[0].Text)), FCConvert.ToInt32(Conversion.Val(txtAccount[1].Text)), 0, 0, lngEligible);
                }
            }
            else
            {
                ShowReport = false;
                MessageBox.Show("Please enter a valid account number to start from.", "No Starting Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return ShowReport;
        }

    }
}