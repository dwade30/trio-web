﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace Global
{
	public class cLedgerDetailInfo
	{
		//=========================================================
		private int lngRecordID;
		private string strAccount = string.Empty;
		private int intPeriod;
		private string strFund = string.Empty;
		private string strLedgerAccount = string.Empty;
		private string strSuffix = string.Empty;
		private string strPostedDate = string.Empty;
		private string strTransactionDate = string.Empty;
		private string strRCB = string.Empty;
		private int lngJournalNumber;
		private string strDescription = string.Empty;
		private int lngWarrant;
		private string strCheckNumber = string.Empty;
		private int lngVendorNumber;
		private double dblAmount;
		private double dblDiscount;
		private double dblEncumbrance;
		private double dblAdjustments;
		private double dblLiquidated;
		private string strType = string.Empty;
		private int intOrderMonth;
		private string strProject = string.Empty;
		private int lngEncumbranceID;
		private string strStatus = string.Empty;

		public string Status
		{
			set
			{
				strStatus = value;
			}
			get
			{
				string Status = "";
				Status = strStatus;
				return Status;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public short Period
		{
			set
			{
				intPeriod = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short Period = 0;
				Period = FCConvert.ToInt16(intPeriod);
				return Period;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}

		public string LedgerAccount
		{
			set
			{
				strLedgerAccount = value;
			}
			get
			{
				string LedgerAccount = "";
				LedgerAccount = strLedgerAccount;
				return LedgerAccount;
			}
		}

		public string Suffix
		{
			set
			{
				strSuffix = value;
			}
			get
			{
				string Suffix = "";
				Suffix = strSuffix;
				return Suffix;
			}
		}

		public string PostedDate
		{
			set
			{
				strPostedDate = value;
			}
			get
			{
				string PostedDate = "";
				PostedDate = strPostedDate;
				return PostedDate;
			}
		}

		public string TransactionDate
		{
			set
			{
				strTransactionDate = value;
			}
			get
			{
				string TransactionDate = "";
				TransactionDate = strTransactionDate;
				return TransactionDate;
			}
		}

		public string RCB
		{
			set
			{
				strRCB = value;
			}
			get
			{
				string RCB = "";
				RCB = strRCB;
				return RCB;
			}
		}

		public int JournalNumber
		{
			set
			{
				lngJournalNumber = value;
			}
			get
			{
				int JournalNumber = 0;
				JournalNumber = lngJournalNumber;
				return JournalNumber;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public int Warrant
		{
			set
			{
				lngWarrant = value;
			}
			get
			{
				int Warrant = 0;
				Warrant = lngWarrant;
				return Warrant;
			}
		}

		public string CheckNumber
		{
			set
			{
				strCheckNumber = value;
			}
			get
			{
				string CheckNumber = "";
				CheckNumber = strCheckNumber;
				return CheckNumber;
			}
		}

		public int VendorNumber
		{
			set
			{
				lngVendorNumber = value;
			}
			get
			{
				int VendorNumber = 0;
				VendorNumber = lngVendorNumber;
				return VendorNumber;
			}
		}

		public double Amount
		{
			set
			{
				dblAmount = value;
			}
			get
			{
				double Amount = 0;
				Amount = dblAmount;
				return Amount;
			}
		}

		public double Discount
		{
			set
			{
				dblDiscount = value;
			}
			get
			{
				double Discount = 0;
				Discount = dblDiscount;
				return Discount;
			}
		}

		public double Encumbrance
		{
			set
			{
				dblEncumbrance = value;
			}
			get
			{
				double Encumbrance = 0;
				Encumbrance = dblEncumbrance;
				return Encumbrance;
			}
		}

		public double Adjustments
		{
			set
			{
				dblAdjustments = value;
			}
			get
			{
				double Adjustments = 0;
				Adjustments = dblAdjustments;
				return Adjustments;
			}
		}

		public double Liquidated
		{
			set
			{
				dblLiquidated = value;
			}
			get
			{
				double Liquidated = 0;
				Liquidated = dblLiquidated;
				return Liquidated;
			}
		}

		public string JournalType
		{
			set
			{
				strType = value;
			}
			get
			{
				string JournalType = "";
				JournalType = strType;
				return JournalType;
			}
		}

		public short OrderMonth
		{
			set
			{
				intOrderMonth = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short OrderMonth = 0;
				OrderMonth = FCConvert.ToInt16(intOrderMonth);
				return OrderMonth;
			}
		}

		public string Project
		{
			set
			{
				strProject = value;
			}
			get
			{
				string Project = "";
				Project = strProject;
				return Project;
			}
		}

		public int EncumbranceKey
		{
			set
			{
				lngEncumbranceID = value;
			}
			get
			{
				int EncumbranceKey = 0;
				EncumbranceKey = lngEncumbranceID;
				return EncumbranceKey;
			}
		}
	}
}
