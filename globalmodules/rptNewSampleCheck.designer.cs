﻿namespace Global
{
	/// <summary>
	/// Summary description for rptNewSampleCheck.
	/// </summary>
	partial class rptNewSampleCheck
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptNewSampleCheck));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ShapeTop = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.lblTop = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ShapeBottom = new GrapeCity.ActiveReports.SectionReportModel.Shape();
			this.lblBottom = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblTop)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBottom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.ShapeTop,
				this.lblTop,
				this.ShapeBottom,
				this.lblBottom
			});
			this.Detail.Height = 10.96875F;
			this.Detail.Name = "Detail";
			// 
			// ShapeTop
			// 
			this.ShapeTop.Height = 3.25F;
			this.ShapeTop.Left = 0F;
			this.ShapeTop.Name = "ShapeTop";
			this.ShapeTop.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.ShapeTop.Top = 0.03125F;
			this.ShapeTop.Width = 7.9375F;
			// 
			// lblTop
			// 
			this.lblTop.Height = 0.375F;
			this.lblTop.HyperLink = null;
			this.lblTop.Left = 0.03125F;
			this.lblTop.Name = "lblTop";
			this.lblTop.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTop.Text = "Top Check Stub";
			this.lblTop.Top = 1.5F;
			this.lblTop.Width = 7.875F;
			// 
			// ShapeBottom
			// 
			this.ShapeBottom.Height = 3.25F;
			this.ShapeBottom.Left = 0F;
			this.ShapeBottom.Name = "ShapeBottom";
			this.ShapeBottom.RoundingRadius = new GrapeCity.ActiveReports.Controls.CornersRadius(0F, null, null, null, null);
			this.ShapeBottom.Top = 6.75F;
			this.ShapeBottom.Width = 7.9375F;
			// 
			// lblBottom
			// 
			this.lblBottom.Height = 0.375F;
			this.lblBottom.HyperLink = null;
			this.lblBottom.Left = 0.03125F;
			this.lblBottom.Name = "lblBottom";
			this.lblBottom.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblBottom.Text = "Bottom Check Stub";
			this.lblBottom.Top = 8.25F;
			this.lblBottom.Width = 7.875F;
			// 
			// rptNewSampleCheck
			// 
			this.ReportStart += ActiveReport_ReportStart;
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.MasterReport = false;
			this.Name = "SampleCheck";
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.96875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblTop)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBottom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Shape ShapeTop;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTop;
		private GrapeCity.ActiveReports.SectionReportModel.Shape ShapeBottom;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBottom;
	}
}
