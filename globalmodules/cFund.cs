﻿//Fecher vbPorter - Version 1.0.0.27
using System.Collections.Generic;

namespace Global
{
	public class cFund
	{
		//=========================================================
		private int lngRecordID;
		private string strFund = string.Empty;
		private string strShortDescription = string.Empty;
		private string strDescription = string.Empty;
		private string strCashFund = string.Empty;
		private bool boolUseDueToDueFrom;
		private string strGLAccountType = string.Empty;
		private bool boolUpdated;
		private bool boolDeleted;
		private Dictionary<string, cBDAccountTitle> dictAccountTitles = new Dictionary<string, cBDAccountTitle>();

		public Dictionary<string, cBDAccountTitle> AccountTitles
		{
			get
			{
				Dictionary<string, cBDAccountTitle> AccountTitles = new Dictionary<string, cBDAccountTitle>();
				AccountTitles = dictAccountTitles;
				return AccountTitles;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
				IsUpdated = true;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
				IsUpdated = true;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
				IsUpdated = true;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string CashFund
		{
			set
			{
				strCashFund = value;
				IsUpdated = true;
			}
			get
			{
				string CashFund = "";
				CashFund = strCashFund;
				return CashFund;
			}
		}

		public bool UseDueToDueFrom
		{
			set
			{
				boolUseDueToDueFrom = value;
				IsUpdated = true;
			}
			get
			{
				bool UseDueToDueFrom = false;
				UseDueToDueFrom = boolUseDueToDueFrom;
				return UseDueToDueFrom;
			}
		}

		public string GLAccountType
		{
			set
			{
				strGLAccountType = value;
				IsUpdated = true;
			}
			get
			{
				string GLAccountType = "";
				GLAccountType = strGLAccountType;
				return GLAccountType;
			}
		}
	}
}
