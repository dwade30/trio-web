﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmGetStartingCMF.
	/// </summary>
	public partial class frmGetStartingCMF : BaseForm
	{
		public frmGetStartingCMF()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetStartingCMF InstancePtr
		{
			get
			{
				return (frmGetStartingCMF)Sys.GetInstance(typeof(frmGetStartingCMF));
			}
		}

		protected frmGetStartingCMF _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string strCMFNumber;
		string strIMPBNumber;
		bool boolOK;

		public bool Init(ref string strCMFNumberOut, ref string strIMPBNumberOut)
		{
			bool Init = false;
			boolOK = false;
			strCMFNumber = "";
			strIMPBNumber = "";
			this.Show(FormShowEnum.Modal);
			if (boolOK)
			{
				strCMFNumberOut = strCMFNumber;
				if (chkNoIMPB.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					strIMPBNumberOut = strIMPBNumber;
				}
				else
				{
					strIMPBNumberOut = "";
				}
			}
			else
			{
				strCMFNumberOut = "";
				strIMPBNumberOut = "";
			}
			Init = boolOK;
			Close();
			return Init;
		}

		private bool CheckCMFBarCode(string strMFCode, bool boolIMPB = false)
		{
			bool CheckCMFBarCode = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCheckDigit = "";
				string strTemp = "";
				int intCodeLen = 0;
				bool boolTemp;
				if (boolIMPB)
				{
					intCodeLen = 22;
				}
				else
				{
					intCodeLen = 20;
				}
				boolTemp = false;
				if (Information.IsNumeric(strMFCode))
				{
					if (strMFCode.Length == intCodeLen)
					{
						strCheckDigit = Strings.Right(strMFCode, 1);
						strTemp = Strings.Left(strMFCode, intCodeLen - 1);
						strTemp = modGlobalFunctions.CalcCMFCheckDigit(ref strTemp);
						// this will check the check digit to the calculated check digit to make sure that this is a valid number
						boolTemp = FCConvert.CBool(strTemp == strCheckDigit);
					}
				}
				CheckCMFBarCode = boolTemp;
				return CheckCMFBarCode;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Mail Form Code Calculation");
				CheckCMFBarCode = false;
			}
			return CheckCMFBarCode;
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			boolOK = false;
			Close();
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			strCMFNumber = txt128bcNumber.Text;
			strCMFNumber = strCMFNumber.Replace(" ", "");
			// this will remove any spaces in the string
			strCMFNumber = strCMFNumber.Replace("-", "");
			// this will remove any hyphens in the string
			if (CheckCMFBarCode(strCMFNumber, false))
			{
				if (chkNoIMPB.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolOK = true;
					strIMPBNumber = "";
				}
				else
				{
					strIMPBNumber = txtIMPBNumber.Text;
					strIMPBNumber = strIMPBNumber.Replace(" ", "");
					// this will remove any spaces in the string
					strIMPBNumber = strIMPBNumber.Replace("-", "");
					// this will remove any hyphens in the string
					if (CheckCMFBarCode(strIMPBNumber, true))
					{
						boolOK = true;
					}
					else
					{
						strIMPBNumber = "";
						strCMFNumber = "";
						FCMessageBox.Show("The IMPB tracking number is invalid. Please enter the correct 22-digit number.");
						txtIMPBNumber.Focus();
					}
				}
			}
			else
			{
				strIMPBNumber = "";
				strCMFNumber = "";
				FCMessageBox.Show("The Certified Mail barcode number is invalid. Please enter the correct 20-digit number.");
				txt128bcNumber.Focus();
			}
			if (boolOK)
			{
				//this.Hide();
				this.Close();
			}
		}

		private void frmGetStartingCMF_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetStartingCMF.ScaleWidth	= 6375;
			//frmGetStartingCMF.ScaleHeight	= 5355;
			//frmGetStartingCMF.LinkTopic	= "Form1";
			//vsElasticLight1.OleObjectBlob	= "frmGetStartingCMF.frx":0000";
			//End Unmaped Properties
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileProcess_Click(sender, e);
		}
	}
}
