//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;


namespace Global
{
	/// <summary>
	/// Summary description for rptConsumptionHistory.
	/// </summary>
	public partial class rptConsumptionHistory : FCSectionReport
	{

		public static rptConsumptionHistory InstancePtr
		{
			get
			{
				return (rptConsumptionHistory)Sys.GetInstance(typeof(rptConsumptionHistory));
			}
		}

		protected rptConsumptionHistory _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptConsumptionHistory()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		// nObj = 1
		//   0	rptConsumptionHistory	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/07/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/01/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngCount;
		int lngNegCount;
		string strAccount = "";

		public void Init(int lngAccountStart, int lngAccountEnd)
		{
			try
			{	// On Error GoTo ERROR_HANDLER
				// strAccount = " AND (ActualAccountNumber >= " & lngAccountStart & " AND ActualAccountNumber <= " & lngAccountEnd & ")"
				// rsData.OpenRecordset "SELECT * FROM Bill WHERE BillingRateKey > 0 " & strAccount & " ORDER BY BillingRateKey desc", strUTDatabase

				//rsData.OpenRecordset("SELECT * FROM MeterConsumption WHERE (AccountKey >= "+Convert.ToString(lngAccountStart)+" AND AccountKey <= "+Convert.ToString(lngAccountEnd)+") ORDER BY BillDate desc", modExtraModules.strUTDatabase);
				rsData.OpenRecordset("SELECT * FROM MeterConsumption INNER JOIN Master ON Master.id = MeterConsumption.AccountKey WHERE (AccountNumber >= " & lngAccountStart & " AND AccountNumber <= " & lngAccountEnd & ") ORDER BY BillDate desc", strUTDatabase
				// frmWait.Init "Please Wait..." & vbCrLf & "Loading Data"
				if (rsData.EndOfFile()) {
					MessageBox.Show("No consumption history found.", "No Consumption History", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Close();
					return;
				}

				if (lngAccountStart==lngAccountEnd) {
					//lblCriteria.Text = "Account : "+Convert.ToString(modUTStatusPayments.GetUTAccountNumber(ref lngAccountStart));
					lblCriteria.Text = "Account : " + Convert.ToString(lngAccountStart);
				} else {
					//lblCriteria.Text = "Account : "+Convert.ToString(modUTStatusPayments.GetUTAccountNumber(ref lngAccountStart))+" to "+Convert.ToString(modUTStatusPayments.GetUTAccountNumber(ref lngAccountEnd));
					lblCriteria.Text = "Account : " + Convert.ToString(lngAccountStart) + " to " + Convert.ToString(lngAccountEnd);
				}

				lblCriteria.Text = lblCriteria.Text+"\r\n"+"Report showing readings in units of : "+Convert.ToString(modExtraModules.Statics.glngTownReadingUnits)+" cu ft.";

				frmReportViewer.InstancePtr.Init(this);
				return;
			}
			catch
			{	// ERROR_HANDLER:
				MessageBox.Show("Error #"+Convert.ToString(Information.Err().Number)+" - "+Information.Err().Description+".", "Error Initializing Consumption Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(ref bool EOF)
		{
			EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyPress(ref short KeyAscii)
		{
			// captures the escape ID
			if (KeyAscii==27) {
				KeyAscii = 0;
				Close();
			}
		}

		private void ActiveReport_ReportEnd()
		{
			frmWait.InstancePtr.Hide();
		}

		private void ActiveReport_ReportStart()
		{
			try
			{	// On Error GoTo ERROR_HANDLER
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);

				FillHeaderLabels();

				return;
			}
			catch
			{	// ERROR_HANDLER:
				frmWait.InstancePtr.Hide();
				MessageBox.Show("Error #"+Convert.ToString(Information.Err().Number)+" - "+Information.Err().Description+".", "Error Starting Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/DD/YYYY");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss AMPM");
			lblMuniName.Text = modGlobalConstants.MuniName;
		}

		private void Detail_Format()
		{
			BindFields();
		}

		private void PageHeader_Format()
		{
			lblPageNumber.Text = "Page "+this.PageNumber;
		}

		private void BindFields()
		{
			if (!rsData.EndOfFile()) {
				fldMeterNumber.Text = rsData.Get_Fields_Int32("MeterNumber");
				fldBillDate.Text = Strings.Format(rsData.Get_Fields_DateTime("BillDate"), "MM/DD/YYYY");

				fldPrev.Text = rsData.Get_Fields_Int32("Begin");
				// TODO: Check the table for the column [End] and replace with corresponding Get_Field method
				fldCurr.Text = rsData.Get_Fields("End");
				// TODO: Check the table for the column [End] and replace with corresponding Get_Field method
				if (rsData.Get_Fields("End")-rsData.Get_Fields_Int32("Begin")>=0) {
					// TODO: Check the table for the column [End] and replace with corresponding Get_Field method
					fldConsumption.Text = rsData.Get_Fields("End")-rsData.Get_Fields_Int32("Begin");
				} else {
					fldConsumption.Text = "Lower Current";
				}

				if (Convert.ToBoolean(rsData.Get_Fields_Boolean("NegativeMeter"))) {
					fldNeg.Text = "Y";
					if (fldConsumption.Text!="Lower Current") {
						lngNegCount += Convert.ToInt32(fldConsumption.Text);
					}
				} else {
					fldNeg.Text = "N";
					if (fldConsumption.Text!="Lower Current") {
						lngCount += Convert.ToInt32(fldConsumption.Text);
					}
				}

				rsData.MoveNext();
			}
		}

		private void ReportFooter_Format()
		{
			fldTotal.Text = lngCount;
			fldNegConsumption.Text = lngNegCount;
		}

		private void rptConsumptionHistory_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptConsumptionHistory.Caption	= "Consumption History";
			//rptConsumptionHistory.Icon	= "rptConsumptionByAccount.dsx":0000";
			//rptConsumptionHistory.Left	= 0;
			//rptConsumptionHistory.Top	= 0;
			//rptConsumptionHistory.Width	= 11880;
			//rptConsumptionHistory.Height	= 8460;
			//rptConsumptionHistory.WindowState	= 2;
			//rptConsumptionHistory.SectionData	= "rptConsumptionByAccount.dsx":058A;
			//End Unmaped Properties
		}
	}
}
