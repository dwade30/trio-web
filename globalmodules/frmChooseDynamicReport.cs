﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using fecherFoundation;

namespace Global
{
	/// <summary>
	/// Summary description for frmChooseDynamicReport.
	/// </summary>
	public partial class frmChooseDynamicReport : BaseForm
	{
		public frmChooseDynamicReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChooseDynamicReport InstancePtr
		{
			get
			{
				return (frmChooseDynamicReport)Sys.GetInstance(typeof(frmChooseDynamicReport));
			}
		}

		protected frmChooseDynamicReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey
		// Date
		// 03/24/2005
		// ********************************************************
		const int CNSTGRIDCOLid = 0;
		const int CNSTGRIDCOLTITLE = 1;
		const int CNSTGRIDCOLDESCRIPTION = 2;
		// vbPorter upgrade warning: lngIDChosen As int	OnWrite(short, string)
		int lngIDChosen;
		int intRepType;
		string strModDB;
        int maxTitleColumnWidth = 80;
		// vbPorter upgrade warning: intReportType As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As int	OnWrite(bool, int)
		public int Init(string strMod, int intReportType, bool boolDeleting = false)
		{
			int Init = 0;
			// returns ID if something chosen, otherwise it returns 0.  It will return -1 if the recordset was empty
			if (boolDeleting)
				this.Text = "Delete";
			lngIDChosen = 0;
			intRepType = intReportType;
			strModDB = "tw" + strMod + "0000.vb1";
			Init = (false ? -1 : 0);
			if (!LoadGrid())
			{
				Init = lngIDChosen;
				Close();
				return Init;
			}
			this.Show(FormShowEnum.Modal, App.MainForm);
			Init = lngIDChosen;
			return Init;
		}

		private void frmChooseDynamicReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmChooseDynamicReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChooseDynamicReport.FillStyle	= 0;
			//frmChooseDynamicReport.ScaleWidth	= 5880;
			//frmChooseDynamicReport.ScaleHeight	= 4470;
			//frmChooseDynamicReport.LinkTopic	= "Form2";
			//frmChooseDynamicReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private bool LoadGrid()
		{
			bool LoadGrid = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				LoadGrid = false;
				Grid.Rows = 1;
				Grid.TextMatrix(0, CNSTGRIDCOLTITLE, "Title");
				Grid.TextMatrix(0, CNSTGRIDCOLDESCRIPTION, "Description");
				Grid.ColHidden(CNSTGRIDCOLid, true);
				clsLoad.OpenRecordset("select ID,title,description from dynamicreports order by title", strModDB);
				if (!clsLoad.EndOfFile())
				{
					while (!clsLoad.EndOfFile())
					{
						Grid.Rows += 1;
						Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLid, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
						Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLTITLE, FCConvert.ToString(clsLoad.Get_Fields_String("title")));
						Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
                        //FC:FINAL:MSH - i.issue #1723: calculate and save max width of column depend on text value (AutoSize doesn't work in WiseJ)
                        int tempWidth = System.Windows.Forms.TextRenderer.MeasureText(clsLoad.Get_Fields_String("title"), Grid.Font).Width;
                        if(maxTitleColumnWidth < tempWidth)
                        {
                            maxTitleColumnWidth = tempWidth;
                        }
                        clsLoad.MoveNext();
					}
				}
				else
				{
					lngIDChosen = -1;
					// signal nothing loaded and that it was empty
					return LoadGrid;
				}
				LoadGrid = true;
				return LoadGrid;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In LoadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadGrid;
		}

		private void ResizeGrid()
		{
            //FC:FINAL:MSH - i.issue #1723: set calculated column width here for recalculating size of last column if user resize form(AutoSize doesn't work in WiseJ)
            //Grid.AutoSize(CNSTGRIDCOLTITLE);
            Grid.ColWidth(CNSTGRIDCOLTITLE, FCConvert.ToInt32(FCUtils.PixelsToTwipsX(maxTitleColumnWidth)));
		}

		private void frmChooseDynamicReport_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.MouseRow < 1)
				return;
			mnuSaveContinue_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
				return;
			lngIDChosen = FCConvert.ToInt32(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLid));
			Close();
		}

		public void mnuSaveContinue_Click()
		{
			mnuSaveContinue_Click(mnuSaveContinue, new System.EventArgs());
		}

		private void cmdSaveAndContinue_Click(object sender, EventArgs e)
		{
			mnuSaveContinue_Click(mnuSaveContinue, EventArgs.Empty);
		}
	}
}
