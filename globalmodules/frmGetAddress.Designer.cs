﻿using fecherFoundation;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmGetAddress.
	/// </summary>
	partial class frmGetAddress : BaseForm
	{
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetAddress));
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtZip4 = new fecherFoundation.FCTextBox();
			this.txtZip = new fecherFoundation.FCTextBox();
			this.txtState = new fecherFoundation.FCTextBox();
			this.txtCity = new fecherFoundation.FCTextBox();
			this.txtAddress2 = new fecherFoundation.FCTextBox();
			this.txtAddress1 = new fecherFoundation.FCTextBox();
			this.txtName = new fecherFoundation.FCTextBox();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 422);
			this.BottomPanel.Size = new System.Drawing.Size(574, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(574, 362);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(574, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(102, 30);
			this.HeaderText.Text = "Address";
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.txtZip4);
			this.Frame1.Controls.Add(this.txtZip);
			this.Frame1.Controls.Add(this.txtState);
			this.Frame1.Controls.Add(this.txtCity);
			this.Frame1.Controls.Add(this.txtAddress2);
			this.Frame1.Controls.Add(this.txtAddress1);
			this.Frame1.Controls.Add(this.txtName);
			this.Frame1.Controls.Add(this.Label8);
			this.Frame1.Controls.Add(this.Label7);
			this.Frame1.Controls.Add(this.Label6);
			this.Frame1.Controls.Add(this.Label5);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Location = new System.Drawing.Point(10, 10);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(561, 320);
			this.Frame1.TabIndex = 0;
			this.Frame1.UseMnemonic = false;
			// 
			// txtZip4
			// 
			this.txtZip4.MaxLength = 4;
			this.txtZip4.AutoSize = false;
			this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip4.Location = new System.Drawing.Point(457, 260);
			this.txtZip4.Name = "txtZip4";
			this.txtZip4.Size = new System.Drawing.Size(75, 40);
			this.txtZip4.TabIndex = 12;
			// 
			// txtZip
			// 
			this.txtZip.MaxLength = 5;
			this.txtZip.AutoSize = false;
			this.txtZip.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip.Location = new System.Drawing.Point(318, 260);
			this.txtZip.Name = "txtZip";
			this.txtZip.Size = new System.Drawing.Size(105, 40);
			this.txtZip.TabIndex = 10;
			// 
			// txtState
			// 
			this.txtState.MaxLength = 2;
			this.txtState.AutoSize = false;
			this.txtState.BackColor = System.Drawing.SystemColors.Window;
			this.txtState.Location = new System.Drawing.Point(142, 260);
			this.txtState.Name = "txtState";
			this.txtState.Size = new System.Drawing.Size(92, 40);
			this.txtState.TabIndex = 8;
			// 
			// txtCity
			// 
			this.txtCity.AutoSize = false;
			this.txtCity.BackColor = System.Drawing.SystemColors.Window;
			this.txtCity.Location = new System.Drawing.Point(142, 200);
			this.txtCity.Name = "txtCity";
			this.txtCity.Size = new System.Drawing.Size(390, 40);
			this.txtCity.TabIndex = 6;
			// 
			// txtAddress2
			// 
			this.txtAddress2.AutoSize = false;
			this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress2.Location = new System.Drawing.Point(142, 140);
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Size = new System.Drawing.Size(390, 40);
			this.txtAddress2.TabIndex = 4;
			// 
			// txtAddress1
			// 
			this.txtAddress1.AutoSize = false;
			this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress1.Location = new System.Drawing.Point(142, 80);
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Size = new System.Drawing.Size(390, 40);
			this.txtAddress1.TabIndex = 3;
			// 
			// txtName
			// 
			this.txtName.AutoSize = false;
			this.txtName.BackColor = System.Drawing.SystemColors.Window;
			this.txtName.Location = new System.Drawing.Point(142, 20);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(390, 40);
			this.txtName.TabIndex = 1;
			// 
			// Label8
			// 
			this.Label8.AutoSize = true;
			this.Label8.Location = new System.Drawing.Point(272, 274);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(27, 16);
			this.Label8.TabIndex = 9;
			this.Label8.Text = "ZIP";
			this.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label7
			// 
			this.Label7.AutoSize = true;
			this.Label7.Location = new System.Drawing.Point(20, 274);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(47, 16);
			this.Label7.TabIndex = 7;
			this.Label7.Text = "STATE";
			this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(435, 274);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(13, 16);
			this.Label6.TabIndex = 11;
			this.Label6.Text = "-";
			this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label5
			// 
			this.Label5.AutoSize = true;
			this.Label5.Location = new System.Drawing.Point(20, 214);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(36, 16);
			this.Label5.TabIndex = 5;
			this.Label5.Text = "CITY";
			this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(20, 94);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(67, 16);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "ADDRESS";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(20, 34);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(43, 16);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "NAME";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(153, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(155, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Save & Continue";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmGetAddress
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(574, 530);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmGetAddress";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Address";
			this.Load += new System.EventHandler(this.frmGetAddress_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetAddress_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcess;
	}
}
