﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmBusy.
	/// </summary>
	public partial class frmBusy : BaseForm
	{
		public frmBusy()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//FC:FINAL:JEI:IIT607 TopMost of the MDI is true, to bring this form to front, we have to set TopMost to true
			this.TopMost = true;
			this.Image1.ImageSource = "ajax-loader";
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBusy InstancePtr
		{
			get
			{
				return (frmBusy)Sys.GetInstance(typeof(frmBusy));
			}
		}

		protected frmBusy _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private bool boolShowPleaseWait;
		private string strMessage = "";

		public string Message
		{
			get
			{
				string Message = "";
				Message = strMessage;
				return Message;
			}
			set
			{
				strMessage = value;
				lblMessage.Text = strMessage;
				//FC:FINAL:JEI:IIT607 update the changed text
				FCUtils.ApplicationUpdate(this);
			}
		}

		public bool ShowPleaseWait
		{
			set
			{
				boolShowPleaseWait = value;
				lblPleaseWait.Visible = value;
			}
			get
			{
				bool ShowPleaseWait = false;
				ShowPleaseWait = boolShowPleaseWait;
				return ShowPleaseWait;
			}
		}

		private void frmBusy_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBusy.FillStyle	= 0;
			//frmBusy.Appearance	= 0;
			//frmBusy.ScaleWidth	= 5655;
			//frmBusy.ScaleHeight	= 2985;
			//frmBusy.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			boolShowPleaseWait = false;
		}

		public void StartBusy()
		{
			Image1.Visible = true;
		}

		public void StopBusy()
		{
			Image1.Visible = false;
		}
	}
}
