﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;

namespace Global
{
	public class cPartyComment
	{
		//=========================================================
		private int lngID;
		private string strComment = "";
		private string strProgModule = "";
		private int lngPartyID;
		private string strEnteredBy = "";
		private DateTime dtLastModified;

		public int ID
		{
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
			set
			{
				lngID = value;
			}
		}

		public string Comment
		{
			get
			{
				string Comment = "";
				Comment = strComment;
				return Comment;
			}
			set
			{
				strComment = value;
			}
		}

		public string ProgModule
		{
			get
			{
				string ProgModule = "";
				ProgModule = strProgModule;
				return ProgModule;
			}
			set
			{
				strProgModule = value;
			}
		}

		public int PartyID
		{
			get
			{
				int PartyID = 0;
				PartyID = lngPartyID;
				return PartyID;
			}
			set
			{
				lngPartyID = value;
			}
		}

		public string EnteredBy
		{
			get
			{
				string EnteredBy = "";
				EnteredBy = strEnteredBy;
				return EnteredBy;
			}
			set
			{
				strEnteredBy = value;
			}
		}

		public DateTime LastModified
		{
			get
			{
				DateTime LastModified = System.DateTime.Now;
				LastModified = dtLastModified;
				return LastModified;
			}
			set
			{
				dtLastModified = value;
			}
		}
	}
}
