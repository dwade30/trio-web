﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;

#if TWMV0000
using TWMV0000;


#elif TWBD0000
using TWBD0000;


#elif TWCK0000
using TWCK0000;
#endif
namespace Global
{
	public class clsErrorHandlers
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Matthew Larrabee        *
		// Date           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               11/17/2002              *
		// ********************************************************
		public void SetErrorHandler(Exception ex)
		{
			// these variables are needed because once the program runs the 'On Error'
			// statement inside this proceedure then all of the information from the
			// real error is lost.
			// VBto upgrade warning: ErrorDescription As Variant --> As string
			// VBto upgrade warning: ErrorNumber As Variant, short --> As int	OnWriteFCConvert.ToInt16
			string ErrorDescription;
			int ErrorNumber;
			string ErrorSource;
			ErrorNumber = Information.Err(ex).Number;
			ErrorDescription = ex.GetBaseException().Message;
			ErrorSource = Information.Err(ex).Source;
			// VBto upgrade warning: strStack As Variant --> As string()
			string[] strStack = null;
			string strCallStack = "";
			int intCounter;
			frmError.InstancePtr.RichTextBox1.Text = string.Empty;
			strStack = Strings.Split(modErrorHandler.Statics.gstrStack, ";;", -1, CompareConstants.vbBinaryCompare);
			for (intCounter = 1; intCounter <= Information.UBound(strStack, 1); intCounter++)
			{
				frmError.InstancePtr.RichTextBox1.Text = frmError.InstancePtr.RichTextBox1.Text + strStack[intCounter] + "\r\n";
			}
			frmError.InstancePtr.Label1[0].Text = "Error has occured in:   " + Strings.Trim(ErrorSource);
			frmError.InstancePtr.Label1[1].Text = "Active routine is:          " + strStack[Information.UBound(strStack, 1)];
			frmError.InstancePtr.Label1[2].Text = "Error description is:       " + Strings.Trim(ErrorDescription);
			frmError.InstancePtr.Label1[3].Text = "Error number is:            " + Strings.Trim(FCConvert.ToString(ErrorNumber));
			// frmError.Label1(4) = "Line number is:                    " & Information.Erl()
			frmError.InstancePtr.Show(FCForm.FormShowEnum.Modal, App.MainForm);
		}
	}
}
