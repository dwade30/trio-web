﻿//Fecher vbPorter - Version 1.0.0.40
namespace Global
{
	public class cOpticalWriteSpeed
	{
		//=========================================================
		private int lngSpeed;
		private string strDesc = string.Empty;

		public string Description
		{
			set
			{
				strDesc = value;
			}
			get
			{
				string Description = "";
				Description = strDesc;
				return Description;
			}
		}

		public int WriteSpeed
		{
			set
			{
				lngSpeed = value;
			}
			get
			{
				int WriteSpeed = 0;
				WriteSpeed = lngSpeed;
				return WriteSpeed;
			}
		}
	}
}
