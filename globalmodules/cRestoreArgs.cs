﻿//Fecher vbPorter - Version 1.0.0.32
namespace Global
{
	public class cRestoreArgs
	{
		//=========================================================
		private int lngFileNumber;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cDatabaseInfo dbInfo = new cDatabaseInfo();
		private cDatabaseInfo dbInfo_AutoInitialized;

		private cDatabaseInfo dbInfo
		{
			get
			{
				if (dbInfo_AutoInitialized == null)
				{
					dbInfo_AutoInitialized = new cDatabaseInfo();
				}
				return dbInfo_AutoInitialized;
			}
			set
			{
				dbInfo_AutoInitialized = value;
			}
		}

		public cDatabaseInfo DatabaseInfo
		{
			get
			{
				cDatabaseInfo DatabaseInfo = null;
				DatabaseInfo = dbInfo;
				return DatabaseInfo;
			}
		}

		public int FileNumber
		{
			set
			{
				lngFileNumber = value;
			}
			get
			{
				int FileNumber = 0;
				FileNumber = lngFileNumber;
				return FileNumber;
			}
		}
	}
}
