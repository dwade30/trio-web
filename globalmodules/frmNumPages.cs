﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;

namespace Global
{
	/// <summary>
	/// Summary description for frmNumPages.
	/// </summary>
	public partial class frmNumPages : BaseForm
	{
		public frmNumPages()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmNumPages InstancePtr
		{
			get
			{
				return (frmNumPages)Sys.GetInstance(typeof(frmNumPages));
			}
		}

		protected frmNumPages _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intNumPages;
		int intStartPage;
		bool boolCancel;
		int intPageFrom;
		int intPageTo;
		// VBto upgrade warning: intNumberOfPages As short	OnWriteFCConvert.ToInt32(
		// VBto upgrade warning: intPageStart As short	OnWriteFCConvert.ToInt32(
		// VBto upgrade warning: obReport As object	OnWrite(rptCustomForms, rptReminderNoticeLabels, rptCustomMHLabels, object, rptCustomLabels)
		public bool Init(ref int intNumberOfPages, ref int intPageStart, GrapeCity.ActiveReports.SectionReport obReport = null, string strTitle = "Print Pages")
		{
			bool Init = false;
			// pass in the number of pages and a start page (if start is other than 1)
			// the function will set intpagestart to the startpage and intnumberofpages to the endpage
			// or just pass in a report and the function will figure the pages out and set the printers pages
			// you must send the first two or the last parameter
			boolCancel = true;
			Init = true;
			if (!(obReport == null))
			{
				intNumberOfPages = obReport.Document.Pages.Count;
				if (intNumberOfPages == 1)
				{
					Close();
					return Init;
				}
			}
			else if (intNumberOfPages == 1)
			{
				Close();
				return Init;
			}
			LblPages.Text = strTitle;
			txtStartPage.Text = FCConvert.ToString(intPageStart);
			intNumPages = intNumberOfPages;
			label2.Text = "OF " + FCConvert.ToString(intNumPages + intPageStart - 1);
			txtEndPage.Text = FCConvert.ToString(intNumPages + intPageStart - 1);
			intStartPage = intPageStart;
			intPageFrom = intStartPage;
			intPageTo = intNumPages;
			this.Show(FormShowEnum.Modal);
			Init = !boolCancel;
			if (!(obReport == null))
			{
				obReport.Document.Printer.PrinterSettings.ToPage = intPageTo;
				obReport.Document.Printer.PrinterSettings.FromPage = intPageFrom;
			}
			else
			{
				if (!boolCancel)
				{
					// if the beginning page is other than 1 you must use this formula to find the physical page numbers
					intNumberOfPages = (intPageTo - intPageStart + 1);
					intPageStart = (intPageFrom - intPageStart + 1);
				}
			}
			return Init;
		}

		private void frmNumPages_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmNumPages.Icon	= "frmNumPages.frx":0000";
			//frmNumPages.ScaleWidth	= 3885;
			//frmNumPages.ScaleHeight	= 2340;
			//frmNumPages.LinkTopic	= "Form2";
			//vsElasticLight1.OleObjectBlob	= "frmNumPages.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			//			Label1.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			//			Label2.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			//			lblPages.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
		}

		private void mnuCancel_Click(object sender, System.EventArgs e)
		{
			boolCancel = true;
			Close();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtEndPage.Text) < Conversion.Val(txtStartPage.Text))
			{
				FCMessageBox.Show("Invalid Print Range.", MsgBoxStyle.Exclamation, "Invalid Range");
				return;
			}
			if (Conversion.Val(txtStartPage.Text) < intStartPage)
			{
				FCMessageBox.Show("Invalid Start Page.", MsgBoxStyle.Exclamation, "Invalid Page");
				return;
			}
			if (Conversion.Val(txtEndPage.Text) > intNumPages + intStartPage - 1)
			{
				FCMessageBox.Show("Invalid End Page.", MsgBoxStyle.Exclamation, "Invalid Page");
				return;
			}
			boolCancel = false;
			intPageFrom = FCConvert.ToInt32(Math.Round(Conversion.Val(txtStartPage.Text)));
			intPageTo = FCConvert.ToInt32(Math.Round(Conversion.Val(txtEndPage.Text)));
			Close();
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuPrint_Click(sender, e);
		}
	}
}
