﻿using fecherFoundation;
using Wisej.Web;

namespace Global
{
    public class modPrinterFunctions
    {
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :                                       *
        // DATE           :                                       *
        // *
        // MODIFIED BY    :                                       *
        // LAST UPDATED   :               11/17/2002              *
        // ********************************************************
        public static DialogResult PrintXsForAlignment(string strMesag, short NumSpacesToLeft, int NumXs = 0, string PrnName = "")
        {
            DialogResult intRes;
            clsPrinterFunctions clsCTAPrinter = new clsPrinterFunctions();
            int NXs = 0;
            int NumSpaces;
            int OrigSpaces;
            DialogResult PrintXsForAlignment = DialogResult.Yes;
            //FC:FINAl:SBE - #1810, #1206 - use client printing for printer alignement
            if (!ClientPrintingSetup.InstancePtr.CheckConfiguration())
            {
                PrintXsForAlignment = DialogResult.Cancel;
                return PrintXsForAlignment;
            }
            NXs = Information.IsNothing(NumXs) ? 2 : NumXs;
            OrigSpaces = NumSpacesToLeft;
            NumSpaces = OrigSpaces;

            clsCTAPrinter.OpenPrinterObject(PrnName); // keep this so you get the right printer ready to go; without this it goes to the first printer in the list
            do
            {

                clsCTAPrinter.PrintXs(NumSpaces, NXs);

                intRes = FCMessageBox.Show(strMesag + "\r\n" + "Is it properly aligned?" + "\r\n" + "Click Yes if it is, No to try again or Cancel to quit printing", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Alignment");

                if (intRes != DialogResult.No) break;

                clsCTAPrinter.PrinterReverse();

            } while (true);
            
            return intRes;
        }

        //public static bool CheckForPrinterFont(string strFont)
        //{
        //    bool CheckForPrinterFont = false;
        //    int counter;
        //    CheckForPrinterFont = false;
        //    //FC:FINAL:MSH - i.issue #1834: wrong condition (index was out of range)
        //    //for (counter = 0; counter <= FCGlobal.Printer.FontCount; counter++)
        //    for (counter = 0; counter < FCGlobal.Printer.FontCount; counter++)
        //    {
        //        if (FCGlobal.Printer.Fonts[counter] == strFont)
        //        {
        //            CheckForPrinterFont = true;
        //        }
        //    }
        //    return CheckForPrinterFont;
        //}
    }
}
