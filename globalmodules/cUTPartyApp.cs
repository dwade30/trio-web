﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using System.Collections.Generic;
using TWSharedLibrary;

namespace Global
{
	public class cUTPartyApp : SharedApplication.ICentralPartyApp
	{
		//=========================================================
		const string strModuleCode = "UT";

		public bool ReferencesParty(int lngID)
		{
			bool ICentralPartyApp_ReferencesParty = false;
			ICentralPartyApp_ReferencesParty = false;
			bool retAnswer;
			retAnswer = false;
			if (lngID > 0)
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select count(id)as theCount from master where BillingPartyid = " + FCConvert.ToString(lngID) + " or SecondBillingPartyid = " + FCConvert.ToString(lngID) + " or ownerpartyid = " + FCConvert.ToString(lngID) + " or secondownerpartyid = " + FCConvert.ToString(lngID), "UtilityBilling");
				// TODO: Field [theCount] not found!! (maybe it is an alias?)
				if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
				{
					retAnswer = true;
				}
				ICentralPartyApp_ReferencesParty = retAnswer;
			}
			return ICentralPartyApp_ReferencesParty;
		}

		public List<object> AccountsReferencingParty(int lngID)
		{
			List<object> CentralPartyApp_AccountsReferencingParty = null;
			var theCollection = new List<object>();
			clsDRWrapper rsLoad = new clsDRWrapper();
			cCentralPartyReference pRef;
			if (lngID > 0)
			{
				rsLoad.OpenRecordset("select id,   AccountNumber from master where BillingPartyid = " + FCConvert.ToString(lngID) + " or SecondBillingPartyid = " + FCConvert.ToString(lngID) + " or ownerpartyid = " + FCConvert.ToString(lngID) + " or secondownerpartyid = " + FCConvert.ToString(lngID), "UtilityBilling");
				while (!rsLoad.EndOfFile())
				{
					//Application.DoEvents();
					pRef = new cCentralPartyReference();
					pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id"));
					// TODO: Check the table for the column [accountnumber] and replace with corresponding Get_Field method
					pRef.Identifier = FCConvert.ToString(rsLoad.Get_Fields("accountnumber"));
					pRef.IdentifierForSort = Strings.Right(Strings.StrDup(12, "0") + pRef.Identifier, 12);
					pRef.ModuleCode = strModuleCode;
					pRef.Description = "Utility Account";
					theCollection.Add(pRef);
					rsLoad.MoveNext();
				}
			}
			CentralPartyApp_AccountsReferencingParty = theCollection;
			return CentralPartyApp_AccountsReferencingParty;
		}

		public string ModuleCode()
		{
			string ICentralPartyApp_ModuleCode = "";
			ICentralPartyApp_ModuleCode = strModuleCode;
			return ICentralPartyApp_ModuleCode;
		}

		public bool ChangePartyID(int lngOriginalPartyID, int lngNewPartyID)
		{
			bool ICentralPartyApp_ChangePartyID = false;
			clsDRWrapper rsSave = new clsDRWrapper();
			string strSQL;
			try
			{
				// On Error GoTo ErrorHandler
				strSQL = "Update master set BillingPartyid = " + FCConvert.ToString(lngNewPartyID) + " where BillingPartyid = " + FCConvert.ToString(lngOriginalPartyID);
				rsSave.Execute(strSQL, "UtilityBilling");
				strSQL = "Update Master set SecondBillingPartyID = " + FCConvert.ToString(lngNewPartyID) + " where SecondBillingPartyID = " + FCConvert.ToString(lngOriginalPartyID);
				rsSave.Execute(strSQL, "UtilityBilling");
				strSQL = "Update Master set OwnerPartyID = " + FCConvert.ToString(lngNewPartyID) + " where OwnerPartyid = " + FCConvert.ToString(lngOriginalPartyID);
				rsSave.Execute(strSQL, "UtilityBilling");
				strSQL = "Update Master set SecondOwnerPartyID = " + FCConvert.ToString(lngNewPartyID) + " where secondownerpartyid = " + FCConvert.ToString(lngOriginalPartyID);
				rsSave.Execute(strSQL, "UtilityBilling");
				ICentralPartyApp_ChangePartyID = true;
				return ICentralPartyApp_ChangePartyID;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return ICentralPartyApp_ChangePartyID;
		}
	}
}
