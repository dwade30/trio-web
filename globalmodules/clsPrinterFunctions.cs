﻿using fecherFoundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using SharedApplication.Enums;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
    public class clsPrinterFunctions
    {
        //=========================================================
        // *************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION
        //
        // WRITTEN BY: MATTHEW S. LARRABEE
        // DATE:       JANUARY 30, 2002
        //
        // NOTES: THIS CODE IS SET UP TO WORK WITH AN OKIDATA MICROLINE 320
        // THE PRINTER DRIVER IS SET UP AS AN EPSON FX 286e
        //
        // **************************************************
        // DEFINED TYPE FOR THE WRITEPRINTER API FUNCTION
        [StructLayout(LayoutKind.Sequential)]
        public struct DOCINFO
        {
            public string pDocName;
            public string pOutputFile;
            public string pDatatype;
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public DOCINFO(int unusedParam)
            {
                pDatatype = String.Empty;
                pOutputFile = String.Empty;
                pDocName = String.Empty;
            }
        };
        // API FUNCTION CALLS
        //[DllImport("winspool.drv")]
        public static int ClosePrinter(int hPrinter,string machineName)
        {
            var p = new ParameterObjects();
            p.FunctionToExecute = FunctionToExecute.ClosePrinter;
            p.Parameter[0] = new ParameterObject(hPrinter, false);
            PrintingViaWebSockets.CheckResultReceived(ref p,machineName);
            return p.ReturnValue;
        }
        //[DllImport("winspool.drv")]
        public static int EndDocPrinter(int hPrinter,string machineName)
        {
            var p = new ParameterObjects();
            p.FunctionToExecute = FunctionToExecute.EndDocPrinter;
            p.Parameter[0] = new ParameterObject(hPrinter, false);
            PrintingViaWebSockets.CheckResultReceived(ref p,machineName);
            return p.ReturnValue;
        }

        //[DllImport("winspool.drv")]
        public static int EndPagePrinter(int hPrinter)
        {
            var p = new ParameterObjects();
            p.FunctionToExecute = FunctionToExecute.EndPagePrinter;
            p.Parameter[0] = new ParameterObject(hPrinter, false);
            PrintingViaWebSockets.CheckResultReceived(ref p, StaticSettings.gGlobalSettings.MachineOwnerID);
            return p.ReturnValue;
        }
        //VBtoInfo: 0/2
        //[DllImport("winspool.drv", EntryPoint = "OpenPrinterA")]
        public static int OpenPrinter(string pPrinterName, ref int phPrinter, int pDefault)
        {
            var p = new ParameterObjects();
            p.FunctionToExecute = FunctionToExecute.OpenPrinter;
            p.Parameter[0] = new ParameterObject(pPrinterName, false);
            p.Parameter[1] = new ParameterObject(phPrinter, true);
            p.Parameter[2] = new ParameterObject(pDefault, false);
            PrintingViaWebSockets.CheckResultReceived(ref p, StaticSettings.gGlobalSettings.MachineOwnerID);
            phPrinter = FCConvert.ToInt32(p.Parameter[1].Value);
            return p.ReturnValue;
        }
        //VBtoInfo: 1
        public int OpenPrinterWrp(ref string pPrinterName, ref int phPrinter, short pDefault)
        {
            int ret;
            //IntPtr ppPrinterName = FCUtils.GetByteFromString(pPrinterName);
            ret = OpenPrinter(pPrinterName, ref phPrinter, pDefault);
            //FCUtils.GetStringFromByte(ref pPrinterName, ppPrinterName);
            return ret;
        }
        //[DllImport("winspool.drv", EntryPoint = "StartDocPrinterA")]
        public static int StartDocPrinter(int hPrinter, int Level, ref DOCINFO pDocInfo)
        {
            var p = new ParameterObjects();
            p.FunctionToExecute = FunctionToExecute.StartDocPrinter;
            p.Parameter[0] = new ParameterObject(hPrinter, false);
            p.Parameter[1] = new ParameterObject(Level, false);
            p.Parameter[2] = new ParameterObject(pDocInfo, true);
            PrintingViaWebSockets.CheckResultReceived(ref p, StaticSettings.gGlobalSettings.MachineOwnerID);
            pDocInfo.pDatatype = p.Parameter[2].Value.pDatatype;
            pDocInfo.pDocName = p.Parameter[2].Value.pDocName;
            pDocInfo.pOutputFile = p.Parameter[2].Value.pOutputFile;
            return p.ReturnValue;
        }
        //[DllImport("winspool.drv")]
        public static int StartPagePrinter(int hPrinter)
        {
            var p = new ParameterObjects();
            p.FunctionToExecute = FunctionToExecute.StartPagePrinter;
            p.Parameter[0] = new ParameterObject(hPrinter, false);
            PrintingViaWebSockets.CheckResultReceived(ref p, StaticSettings.gGlobalSettings.MachineOwnerID);
            return p.ReturnValue;
        }
        //[DllImport("winspool.drv")]
        public static int WritePrinter(int hPrinter, string pBuf, int cdBuf, ref int pcWritten)
        {
            var p = new ParameterObjects();
            p.FunctionToExecute = FunctionToExecute.WritePrinter;
            p.Parameter[0] = new ParameterObject(hPrinter, false);
            p.Parameter[1] = new ParameterObject(pBuf, false);
            p.Parameter[2] = new ParameterObject(cdBuf, false);
            p.Parameter[3] = new ParameterObject(pcWritten, true);
            PrintingViaWebSockets.CheckResultReceived(ref p, StaticSettings.gGlobalSettings.MachineOwnerID);
            pcWritten = FCConvert.ToInt32(p.Parameter[3].Value);
            return p.ReturnValue;
        }

        public static int WritePrinter(string printerName, string pBuf, int cdBuf, ref int pcWritten)
        {
            var p = new ParameterObjects();
            p.FunctionToExecute = FunctionToExecute.WritePrinter;
            p.Parameter[0] = new ParameterObject(printerName, false);
            p.Parameter[1] = new ParameterObject(pBuf, false);
            p.Parameter[2] = new ParameterObject(cdBuf, false);
            p.Parameter[3] = new ParameterObject(pcWritten, true);
            PrintingViaWebSockets.CheckResultReceived(ref p, StaticSettings.gGlobalSettings.MachineOwnerID);
            pcWritten = FCConvert.ToInt32(p.Parameter[3].Value);
            return p.ReturnValue;
        }

        public static int PrintXs(string printerName, string pBuf, int cdBuf, ref int pcWritten)
        {
            var p = new ParameterObjects();
            p.FunctionToExecute = FunctionToExecute.PrintXs;
            p.Parameter[0] = new ParameterObject(printerName, false);
            p.Parameter[1] = new ParameterObject(pBuf, false);
            p.Parameter[2] = new ParameterObject(cdBuf, false);
            p.Parameter[3] = new ParameterObject(pcWritten, true);
            PrintingViaWebSockets.CheckResultReceived(ref p, StaticSettings.gGlobalSettings.MachineOwnerID);
            pcWritten = FCConvert.ToInt32(p.Parameter[3].Value);
            return p.ReturnValue;
        }

        public static int PrinterForward(string printerName, string pBuf, int cdBuf, ref int pcWritten)
        {
            var p = new ParameterObjects();
            p.FunctionToExecute = FunctionToExecute.PrinterForward;
            p.Parameter[0] = new ParameterObject(printerName, false);
            p.Parameter[1] = new ParameterObject(pBuf, false);
            p.Parameter[2] = new ParameterObject(cdBuf, false);
            p.Parameter[3] = new ParameterObject(pcWritten, true);
            PrintingViaWebSockets.CheckResultReceived(ref p, StaticSettings.gGlobalSettings.MachineOwnerID);
            pcWritten = FCConvert.ToInt32(p.Parameter[3].Value);
            return p.ReturnValue;
        }
        public static int PrinterReverse(string printerName, string pBuf, int cdBuf, ref int pcWritten)
        {
            var p = new ParameterObjects();
            p.FunctionToExecute = FunctionToExecute.PrinterReverse;
            p.Parameter[0] = new ParameterObject(printerName, false);
            p.Parameter[1] = new ParameterObject(pBuf, false);
            p.Parameter[2] = new ParameterObject(cdBuf, false);
            p.Parameter[3] = new ParameterObject(pcWritten, true);
            PrintingViaWebSockets.CheckResultReceived(ref p, StaticSettings.gGlobalSettings.MachineOwnerID);
            pcWritten = FCConvert.ToInt32(p.Parameter[3].Value);
            return p.ReturnValue;
        }

        public static List<Printer> GetClientPrinters()
        {
            var printers = new List<Printer>();
            var po = new ParameterObjects();
            po.Parameter[0] = new ParameterObject(printers, true);
            po.FunctionToExecute = FunctionToExecute.GetPrinters;
            var returnException = PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);
            if (returnException == null)
            {
                try
                {
                    if (po.Parameter[0].Value.GetType() == typeof(Wisej.Core.DynamicObject[]))
                    {
                        Printer p;
                        for (var i = 0; i < po.Parameter[0].Value.Length; i++)
                        {
                            p = new Printer();
                            p.Name = po.Parameter[0].Value[i].Name;
                            p.DriverName = po.Parameter[0].Value[i].DriverName;
                            p.PortName = po.Parameter[0].Value[i].PortName;
                            p.Default = po.Parameter[0].Value[i].Default;
                            printers.Add(p);
                        }
                    }

                    return printers;
                }
                catch (Exception ex)
                {
                    StaticSettings.GlobalTelemetryService.TrackException(ex);
                    PrintingViaWebSockets.WriteToLogFile("Exception: " + ex.Message, StaticSettings.gGlobalSettings.MachineOwnerID);
                    return null;
                }
            }
            return null;
        }

        public static void PrintRTFTextToPrinter(string printerName, string rtfText, GrapeCity.ActiveReports.PageSettings pageSettings)
        {
            var po = new ParameterObjects();
            po.Parameter[0] = new ParameterObject(printerName, false);
            po.Parameter[1] = new ParameterObject(rtfText, false);
            po.Parameter[2] = new ParameterObject(pageSettings, false);
            po.FunctionToExecute = FunctionToExecute.PrintRtf;
            PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);
        }

        public static void PrintTIFFToPrinter(string printerName, string tiffDownloadLink, GrapeCity.ActiveReports.PageSettings pageSettings)
        {
            var po = new ParameterObjects();
            po.Parameter[0] = new ParameterObject(printerName, false);
            po.Parameter[1] = new ParameterObject(tiffDownloadLink, false);
            po.Parameter[2] = new ParameterObject(pageSettings, false);
            po.FunctionToExecute = FunctionToExecute.PrintTiff;
            PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);
        }

        public static void PrintRDFToPrinter(string printerName, string rdfDownloadLink, GrapeCity.ActiveReports.PageSettings pageSettings, DirectPrintingParams printerSettings)
        {
            var po = new ParameterObjects();
            po.Parameter[0] = new ParameterObject(printerName, false);
            po.Parameter[1] = new ParameterObject(rdfDownloadLink, false);
            po.Parameter[2] = new ParameterObject(pageSettings, false);
            if (printerSettings != null)
            {
                po.Parameter[3] = new ParameterObject(printerSettings, false);
            }
            po.FunctionToExecute = FunctionToExecute.PrintRDF;
            PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);
        }

        public static void DownloadTRIOSketchFile(string trioSketchDownloadlink)
        {
            var po = new ParameterObjects();
            po.Parameter[0] = new ParameterObject(trioSketchDownloadlink, false);
            po.FunctionToExecute = FunctionToExecute.DownloadTRIOSketchFile;
            PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);
        }

        public static bool IsClientPrintingConnected(string machineName)
        {

            var webSocketConnetionOpen = PrintingViaWebSockets.IsWebSocketConnectionOpen(machineName);
            if (!webSocketConnetionOpen)
            {
                MessageBox.Show("Client printing is not configured. \r\n Please setup Printing Key in Settings -> Printer Setup and run TRIO Print Assistant on your local machine");
            }
            return webSocketConnetionOpen;
        }


        // VARIABLES
        private int lhPrinter;
        private int lReturn;
        private int lpcWritten;
        private int lDoc;
        private string sWrittenData = "";
        private string pstrPrinterName = string.Empty;
        //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
        private DOCINFO MyDocInfo = new DOCINFO(0);
        private readonly string ESC = Convert.ToString((char)27);
        private readonly string CRLF = "\r\n";
        private readonly string LF = Convert.ToString((char)10);
        private readonly string Value108 = Convert.ToString((char)108);
        private readonly string P = Convert.ToString((char)80);

        public bool OpenPrinterObject(string strPrinterName = "")
        {
            var ret = false;
            // ASSIGN THE PRINTER NAME
            pstrPrinterName = strPrinterName;
            var printers = GetClientPrinters();

            if (strPrinterName == string.Empty)
            {
                // NO PRINTER WAS SPECIFIED SO USE THE DEFAULT PRINTER
                pstrPrinterName = FCGlobal.Printer?.DeviceName ?? "";

            }

            //FC:FINAl:SBE - check if selected printer is part of the printer list.
            //TODO This logic is duplicated in ARViewer. Likely in other places as well. Consolidate in future release
            if (printers != null)
            {
                var selectedPrinter = printers.FirstOrDefault(p => p.Name == pstrPrinterName);
                if (selectedPrinter == null)
                    using (var selectPrinter = new FCSelectClientPrinter())
                    {
                        foreach (var clientPrinter in printers)
                        {
                            selectPrinter.AddPrinter(clientPrinter.Name);
                        }
                        selectPrinter.ShowDialog();
                        pstrPrinterName = selectPrinter.SelectedPrinter;
                    }
            }

            // MAKE SURE A PRINTER WAS SELECTED
            if (pstrPrinterName == string.Empty)
            {
                FCMessageBox.Show("Invalid printer selected.", MsgBoxStyle.Critical | MsgBoxStyle.OkOnly, "TRIO Software");
                return ret;
            }

            //// OPEN THE PRINTER WITH ITS NAME AND RETURN THE ID HANDLE IN LHPRINTER
            //lReturn = OpenPrinterWrp(ref pstrPrinterName, ref lhPrinter, 0);
            //if (lReturn == 0)
            //{
            //    // THIS PRINTER WAS NOT FOUND IN THE COLLECTION
            //    FCMessageBox.Show("The Printer Name you typed wasn't recognized.");
            //    return ret;
            //}
            //// START A PRINT JOB------THE ICON WILL SHOW IN THE TASK BAR AFTER THIS FUNCTION IS RUN
            //StartPrinter();
            // THE PRINTER WAS OPENED SUCCESSFULLY
            ret = true;
            return ret;
        }

        public void PrinterForward(int HalfInchMovements = 1)
        {

            int intCounter;

            // CHECK TO MAKE SURE THAT A PRINTER HAS BEEN SELECTED
            if (pstrPrinterName == string.Empty)
            {
                if (!OpenPrinterObject())
                {
                    return;
                }
            }
            for (intCounter = 1; intCounter <= HalfInchMovements; intCounter++)
            {
                // THIS WILL MOVE THE PAGE FORWARD 1/2"
                var po = new ParameterObjects();
                po.Parameter[0] = new ParameterObject(pstrPrinterName, false);
                po.FunctionToExecute = FunctionToExecute.PrinterForward;
                PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);

            }
        }

        //public void PrinterForwardInInches(float sngNumInches)
        //{
        //    // VBto upgrade warning: intDistance As short --> As int	OnWriteFCConvert.ToSingle(
        //    int intDistance;
        //    // specify how far to move forward in inches (can be fractional)
        //    if (pstrPrinterName == string.Empty)
        //    {
        //        if (!OpenPrinterObject())
        //        {
        //            return;
        //        }
        //    }
        //    intDistance = FCConvert.ToInt32(sngNumInches * 216);
        //    sWrittenData = ESC + "J" + FCConvert.ToString(Convert.ToChar(intDistance));
        //    lReturn = WritePrinter(lhPrinter, sWrittenData, sWrittenData.Length, ref lpcWritten);
        //}

        //public void PrinterReverseInInches(float sngNumInches)
        //{
        //    // VBto upgrade warning: intDistance As short --> As int	OnWriteFCConvert.ToSingle(
        //    int intDistance;
        //    // specify how far to move backward in inches (can be fractional)
        //    if (pstrPrinterName == string.Empty)
        //    {
        //        if (!OpenPrinterObject())
        //        {
        //            return;
        //        }
        //    }
        //    intDistance = FCConvert.ToInt32(sngNumInches * 216);
        //    sWrittenData = ESC + "j" + FCConvert.ToString(Convert.ToChar(intDistance));
        //    lReturn = WritePrinter(lhPrinter, sWrittenData, sWrittenData.Length, ref lpcWritten);
        //}

        public void PrinterReverse(int HalfInchMovements = 1)
        {

            int intCounter;
            // CHECK TO MAKE SURE THAT A PRINTER HAS BEEN SELECTED
            if (pstrPrinterName == string.Empty)
            {
                if (!OpenPrinterObject())
                {
                    return;
                }
            }
            for (intCounter = 1; intCounter <= HalfInchMovements; intCounter++)
            {
                // THIS WILL MOVE THE PAGE BACKWARD 1/2"
                var po = new ParameterObjects();
                po.Parameter[0] = new ParameterObject(pstrPrinterName, false);
                po.FunctionToExecute = FunctionToExecute.PrinterReverse;
                PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);
            }
        }

        //public void PrinterCrLf(int HalfInchMovements = 1)
        //{
        //    int intCounter;
        //    // CHECK TO MAKE SURE THAT A PRINTER HAS BEEN SELECTED
        //    if (pstrPrinterName == string.Empty)
        //    {
        //        // A PRINTER HAS NOT BEEN SELECTED SO TRY AND OPEN THE DEFAULT PRINTER
        //        if (!OpenPrinterObject())
        //        {
        //            return;
        //        }
        //    }
        //    for (intCounter = 1; intCounter <= HalfInchMovements; intCounter++)
        //    {
        //        // THIS WILL MOVE THE PAGE DOWN ONE LINE
        //        sWrittenData = CRLF;
        //        lReturn = WritePrinter(lhPrinter, sWrittenData, sWrittenData.Length, ref lpcWritten);
        //    }
        //}

        //public void PrintTextWithCrLf(string strText)
        //{
        //    // CHECK TO MAKE SURE THAT A PRINTER HAS BEEN SELECTED
        //    if (pstrPrinterName == string.Empty)
        //    {
        //        // A PRINTER HAS NOT BEEN SELECTED SO TRY AND OPEN THE DEFAULT PRINTER
        //        if (!OpenPrinterObject())
        //        {
        //            return;
        //        }
        //    }
        //    // THIS WILL PRINT THE SELECTED TEXT WITH A CARRIAGE RETURN and LINE FEED
        //    sWrittenData = strText + CRLF;
        //    // SEND THE TEXT TO THE PRINTER
        //    lReturn = WritePrinter(lhPrinter, sWrittenData, sWrittenData.Length, ref lpcWritten);
        //}

        public void PrintTextWithCr(string strText)
        {
            int intCounter/*unused?*/;
            // CHECK TO MAKE SURE THAT A PRINTER HAS BEEN SELECTED
            if (pstrPrinterName == string.Empty)
            {
                // A PRINTER HAS NOT BEEN SELECTED SO TRY AND OPEN THE DEFAULT PRINTER
                if (!OpenPrinterObject())
                {
                    return;
                }
            }
            // THIS WILL PRINT THE SELECTED TEXT WITH A CARRIAGE RETURN AND A LINE FEED
            sWrittenData = strText + CRLF;
            // SEND THE CODE TO THE PRITNER
            lReturn = WritePrinter(pstrPrinterName, sWrittenData, sWrittenData.Length, ref lpcWritten);

        }

        public void PrintXs(int intNumSpacesToLeft = 0, int intNumXsToPrint = 0)
        {

            var intNumXs = 0;
            var intNumSpaces = 0;
            string strTemp;
            intNumXs = Information.IsNothing(intNumXsToPrint) ? 2 : intNumXsToPrint;
            intNumSpaces = Information.IsNothing(intNumSpacesToLeft) ? 20 : intNumSpacesToLeft;
            sWrittenData = Strings.StrDup(intNumSpaces, " ");
            sWrittenData += Strings.StrDup(intNumXs, "X");
            strTemp = Strings.StrDup(78 - sWrittenData.Length, " ");

            var po = new ParameterObjects();
            po.Parameter[0] = new ParameterObject(pstrPrinterName, false);
            po.Parameter[1] = new ParameterObject(sWrittenData + strTemp, false);
            po.FunctionToExecute = FunctionToExecute.PrintXs;
            PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);

            System.Diagnostics.Trace.TraceInformation($"Printed text {sWrittenData} on {pstrPrinterName}");
        }

        public void PrintText(string strText)
        {

            // CHECK TO MAKE SURE THAT A PRINTER HAS BEEN SELECTED
            if (pstrPrinterName == string.Empty)
            {
                // A PRINTER HAS NOT BEEN SELECTED SO TRY AND OPEN THE DEFAULT PRINTER
                if (!OpenPrinterObject())
                {
                    return;
                }
            }
            // THIS WILL PRINT THE SELECTED TEXT
            sWrittenData = strText;
            // SEND THE TEXT TO THE PRINTER
            lReturn = WritePrinter(pstrPrinterName, sWrittenData, sWrittenData.Length, ref lpcWritten);

        }

        ~clsPrinterFunctions()
        {
            // CLOSE OUT THE PRINTING SESSION
            lReturn = EndPagePrinter(lhPrinter);
            lReturn = EndDocPrinter(lhPrinter, StaticSettings.gGlobalSettings.MachineOwnerID);
            lReturn = ClosePrinter(lhPrinter, StaticSettings.gGlobalSettings.MachineOwnerID);
        }

        public void CloseSession(string machineName)
        {
            // CLOSE OUT THE PRINTING SESSION
            lReturn = EndPagePrinter(lhPrinter);
            lReturn = EndDocPrinter(lhPrinter,machineName);
            lReturn = ClosePrinter(lhPrinter,machineName);
        }

        //public void PrinterReset()
        //{
        //    // CHECK TO MAKE SURE THAT A PRINTER HAS BEEN SELECTED
        //    if (pstrPrinterName == string.Empty)
        //    {
        //        if (!OpenPrinterObject())
        //        {
        //            return;
        //        }
        //    }
        //    // ESC @ is Clear Buffer & Initialize
        //    // CAN (24) is Clear Print Buffer
        //    sWrittenData = ESC + FCConvert.ToString(Convert.ToChar(64)) + FCConvert.ToString(Convert.ToChar(24));
        //    // SEND THE CODE TO THE PRINTER
        //    lReturn = WritePrinter(lhPrinter, sWrittenData, sWrittenData.Length, ref lpcWritten);

        //}

        public static string GetApplicationKey()
        {
            string printerJson = "";

            var po = new ParameterObjects();
            po.Parameter[0] = new ParameterObject(printerJson, true);
            po.FunctionToExecute = FunctionToExecute.GetApplicationKey;
            PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);
            printerJson = po.Parameter[0]?.Value;
            var ret = printerJson;
            return ret;
        }

        public static void SaveApplicationKey(string key)
        {
           var po = new ParameterObjects();
            po.Parameter[0] = new ParameterObject(key, false);
            po.FunctionToExecute = FunctionToExecute.SaveApplicationKey;
            PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);
        }
        public static List<Tuple<string, string>> GetMachinePrinters()
        {
            string printerJson = "";

            var po = new ParameterObjects();
            po.Parameter[0] = new ParameterObject(printerJson, true);
            po.FunctionToExecute = FunctionToExecute.GetMachinesPrinters;
            PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);
            printerJson = po.Parameter[0]?.Value;
            var ret = string.IsNullOrEmpty(printerJson)
                ? new List<Tuple<string, string>>()
                : JsonConvert.DeserializeObject<List<Tuple<string, string>>>(printerJson);
            return ret;
        }

        public static void SaveMachinePrinters(string printerJson)
        {
            var po = new ParameterObjects();
            po.Parameter[0] = new ParameterObject(printerJson, false);
            po.FunctionToExecute = FunctionToExecute.SaveMachinesPrinters;
            PrintingViaWebSockets.CheckResultReceived(ref po, StaticSettings.gGlobalSettings.MachineOwnerID);
        }

        /// <summary>
        /// Loads the printers.txt file from the machine into global settings
        /// </summary>
        public static void LoadMachinePrinters()
        {
            var printerJson = GetMachinePrinters();

            try
            {
                // load values into global settings
                foreach (var tuple in printerJson)
                {
                    var printerTypeName = $"{tuple.Item1}";
                    StaticSettings.GlobalSettingService.SaveSetting(SettingOwnerType.Machine, printerTypeName, tuple.Item2);
                }
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                Console.WriteLine(ex);

            }
        }
    }
}
