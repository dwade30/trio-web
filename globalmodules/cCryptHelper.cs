﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWPY0000
{
	public class cCryptHelper
	{
		//=========================================================
		private Chilkat.Crypt2 crypt = new Chilkat.Crypt2();
		private string strLastErrorMessage = "";
		private int lngLastErrorNumber;

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastErrorNumber;
				return LastErrorNumber;
			}
		}

		public string LastErrorDescription
		{
			get
			{
				string LastErrorDescription = "";
				LastErrorDescription = strLastErrorMessage;
				return LastErrorDescription;
			}
		}

		private void SetError(int lngError, string strError)
		{
			strLastErrorMessage = strError;
			lngLastErrorNumber = lngError;
		}

		public void ClearErrors()
		{
			strLastErrorMessage = "";
			lngLastErrorNumber = 0;
		}

		public cCryptHelper() : base()
		{
			//crypt.UnlockComponent("HARRISCrypt_vEoVPOQtXRFY");
            crypt.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");
        }
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public object GetSHA256HashOfFile(string strFile)
		{
			object GetSHA256HashOfFile = null;
			ClearErrors();
			crypt.HashAlgorithm = "sha256";
			crypt.EncodingMode = "hex";
			string strHash;
			strHash = crypt.HashFileENC(strFile);
			if (strHash == "")
			{
				SetError(9999, crypt.LastErrorText);
			}
			GetSHA256HashOfFile = strHash;
			return GetSHA256HashOfFile;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string)
		public object GetSHA256HashOfString(string strString)
		{
			object GetSHA256HashOfString = null;
			ClearErrors();
			crypt.HashAlgorithm = "sha256";
			crypt.EncodingMode = "hex";
			string strHash;
			strHash = crypt.HashStringENC(strString);
			if (strHash == "")
			{
				SetError(9999, crypt.LastErrorText);
			}
			GetSHA256HashOfString = strHash;
			return GetSHA256HashOfString;
		}
	}
}
