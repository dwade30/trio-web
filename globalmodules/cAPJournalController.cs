﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using fecherFoundation.Extensions;
using System;
using TWSharedLibrary;
using TWSharedLibrary.Data;
using Wisej.Web;

namespace Global
{
    public class cAPJournalController
    {
        //=========================================================
        private string strLastError = "";
        private int lngLastError;

        public string LastErrorMessage
        {
            get
            {
                string LastErrorMessage = "";
                LastErrorMessage = strLastError;
                return LastErrorMessage;
            }
        }

        public int LastErrorNumber
        {
            get
            {
                int LastErrorNumber = 0;
                LastErrorNumber = lngLastError;
                return LastErrorNumber;
            }
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As bool
        public bool HadError
        {
            get
            {
                bool HadError = false;
                HadError = lngLastError != 0;
                return HadError;
            }
        }

        public void ClearErrors()
        {
            strLastError = "";
            lngLastError = 0;
        }

        public int GetJournalEntryCount(int lngJournalNumber)
        {
            int GetJournalEntryCount = 0;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                int lngReturn;
                lngReturn = 0;
                rsLoad.OpenRecordset("Select count(ID) as entriescount from apjournaldetail where apjournalid in (select id from apjournal where journalnumber = " + FCConvert.ToString(lngJournalNumber) + ")", "Budgetary");
                // TODO: Field [entriescount] not found!! (maybe it is an alias?)
                lngReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("entriescount"))));
                rsLoad.DisposeOf();
                GetJournalEntryCount = lngReturn;
                return GetJournalEntryCount;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetJournalEntryCount;
        }

        public double GetJournalAmount(int lngJournalNumber)
        {
            double GetJournalAmount = 0;
            ClearErrors();
            clsDRWrapper rsLoad = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler

                double dblReturn;
                dblReturn = 0;
                rsLoad.OpenRecordset("Select sum (Amount) as journalamount from apjournal where journalnumber = " + FCConvert.ToString(lngJournalNumber), "Budgetary");
                // TODO: Field [journalamount] not found!! (maybe it is an alias?)
                dblReturn = FCConvert.ToDouble(rsLoad.Get_Fields("journalamount"));

                GetJournalAmount = dblReturn;
                return GetJournalAmount;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetJournalAmount;
        }

        public string GetPostedDate(int lngJournalNumber)
        {
            string GetPostedDate = "";
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                string strReturn;
                strReturn = "";
                rsLoad.OpenRecordset("select top 1 posteddate from apjournal where journalnumber = " + FCConvert.ToString(lngJournalNumber), "Budgetary");
                if (Information.IsDate(rsLoad.Get_Fields("PostedDate")))
                {
                    strReturn = FCConvert.ToString(rsLoad.Get_Fields_DateTime("PostedDate"));
                }
                GetPostedDate = strReturn;
                return GetPostedDate;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetPostedDate;
        }

        public cGenericCollection GetEnteredJournalsByJournalNumber(int lngJournalNumber)
        {
            cGenericCollection GetEnteredJournalsByJournalNumber = null;
            ClearErrors();

            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                cGenericCollection gColl = new cGenericCollection();
                cAPJournal apJourn;
                DateTime date;
                rsLoad.OpenRecordset("select * from APJournal where journalnumber = " + FCConvert.ToString(lngJournalNumber) + " and status = 'E' order by Description", "Budgetary");

                while (!rsLoad.EndOfFile())
                {
                    apJourn = new cAPJournal();

                    // TODO: Check the table for the column [amount] and replace with corresponding Get_Field method
                    apJourn.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));

                    if (Information.IsDate(rsLoad.Get_Fields("CheckDate"), out date))
                    {
                        apJourn.CheckDate = FCConvert.ToString(date);
                    }

                    // TODO: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                    apJourn.CheckNumber = FCConvert.ToString(rsLoad.Get_Fields("CheckNumber"));
                    apJourn.CREDITMEMOCORRECTION = rsLoad.Get_Fields_Boolean("CreditMemoCorrection");
                    apJourn.creditMemoRecord = rsLoad.Get_Fields_Int32("creditmemorecord");
                    apJourn.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
                    apJourn.EFTCheck = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("EFTCheck"));
                    apJourn.EncumbranceRecord = rsLoad.Get_Fields_Int32("EncumbranceRecord");

                    // TODO: Check the table for the column [frequency] and replace with corresponding Get_Field method
                    apJourn.Frequency = FCConvert.ToString(rsLoad.Get_Fields("frequency"));

                    // TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                    apJourn.JournalNumber = FCConvert.ToInt32(rsLoad.Get_Fields("JournalNumber"));

                    if (Information.IsDate(rsLoad.Get_Fields("Payable"), out date))
                    {
                        apJourn.PayableDate = FCConvert.ToString(date);
                    }

                    // TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
                    apJourn.Period = rsLoad.Get_Fields_Int32("Period");
                    if (Information.IsDate(rsLoad.Get_Fields("PostedDate"), out date))
                    {
                        apJourn.PostedDate = FCConvert.ToString(date);
                    }

                    apJourn.PrepaidCheck = rsLoad.Get_Fields_Boolean("PrepaidCheck");
                    apJourn.PrintedIndividual = rsLoad.Get_Fields_Boolean("PrintedIndividual");
                    apJourn.PurchaseOrderID = rsLoad.Get_Fields_Int32("PurchaseOrderID");
                    apJourn.Reference = rsLoad.Get_Fields_String("Reference");
                    apJourn.Returned = rsLoad.Get_Fields_String("Returned");

                    // TODO: Field [Separate] not found!! (maybe it is an alias?)
                    apJourn.Separate = FCConvert.ToBoolean(rsLoad.Get_Fields("Separate"));

                    // TODO: Field [SeparateCheckRecord] not found!! (maybe it is an alias?)
                    apJourn.SeparateCheckRecord = FCConvert.ToInt32(rsLoad.Get_Fields("SeparateCheckRecord"));
                    apJourn.Status = rsLoad.Get_Fields_String("Status");
                    apJourn.TempVendorAddress1 = rsLoad.Get_Fields_String("TempVendorAddress1");
                    apJourn.TempVendorAddress2 = rsLoad.Get_Fields_String("TempVendorAddress2");
                    apJourn.TempVendorAddress3 = rsLoad.Get_Fields_String("TempVendorAddress3");
                    apJourn.TempVendorAddress4 = rsLoad.Get_Fields_String("TempVendorAddress4");
                    apJourn.TempVendorCity = rsLoad.Get_Fields_String("TempVendorCity");
                    apJourn.TempVendorName = rsLoad.Get_Fields_String("TempVendorName");
                    apJourn.TempVendorState = rsLoad.Get_Fields_String("TempVendorState");
                    apJourn.TempVendorZip = rsLoad.Get_Fields_String("TempVendorZip");
                    apJourn.TempVendorZip4 = rsLoad.Get_Fields_String("TempVendorZip4");
                    apJourn.TotalEncumbrance = Conversion.Val(rsLoad.Get_Fields_Decimal("TotalEncumbrance"));
                    apJourn.TownOverride = rsLoad.Get_Fields_String("TownOverride");

                    if (Information.IsDate(rsLoad.Get_Fields("Until"), out date))
                    {
                        apJourn.UntilDate = FCConvert.ToString(date);
                    }

                    apJourn.UseAlternateCash = rsLoad.Get_Fields_Boolean("UseAlternateCash");
                    apJourn.AlternateCashAccount = rsLoad.Get_Fields_String("AlternateCashAccount");

                    // TODO: Check the table for the column [Warrant] and replace with corresponding Get_Field method
                    apJourn.Warrant = FCConvert.ToString(rsLoad.Get_Fields("Warrant"));
                    apJourn.VendorNumber = rsLoad.Get_Fields_Int32("VendorNumber");
                    apJourn.ID = rsLoad.Get_Fields_Int32("ID");
                    apJourn.IsUpdated = false;
                    gColl.AddItem(apJourn);
                    rsLoad.MoveNext();
                }


                GetEnteredJournalsByJournalNumber = gColl;

                return GetEnteredJournalsByJournalNumber;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetEnteredJournalsByJournalNumber;
        }

        public cAPJournal GetAPJournal(int lngID)
        {
            cAPJournal GetAPJournal = null;
            ClearErrors();

            // On Error GoTo ErrorHandler
            cGenericCollection gColl = new cGenericCollection();
            clsDRWrapper rsLoad = new clsDRWrapper();
            cAPJournal apJourn = null;
            DateTime date;

            try
            {
                rsLoad.OpenRecordset("select * from APJournal where id = " + FCConvert.ToString(lngID), "Budgetary");

                if (!rsLoad.EndOfFile())
                {
                    apJourn = new cAPJournal();

                    // TODO: Check the table for the column [amount] and replace with corresponding Get_Field method
                    apJourn.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));

                    if (Information.IsDate(rsLoad.Get_Fields("CheckDate"), out date))
                    {
                        apJourn.CheckDate = FCConvert.ToString(date);
                    }

                    // TODO: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                    apJourn.CheckNumber = FCConvert.ToString(rsLoad.Get_Fields("CheckNumber"));
                    apJourn.CREDITMEMOCORRECTION = rsLoad.Get_Fields_Boolean("CreditMemoCorrection");
                    apJourn.creditMemoRecord = rsLoad.Get_Fields_Int32("creditmemorecord");
                    apJourn.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
                    apJourn.EFTCheck = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("EFTCheck"));
                    apJourn.EncumbranceRecord = rsLoad.Get_Fields_Int32("EncumbranceRecord");

                    // TODO: Check the table for the column [frequency] and replace with corresponding Get_Field method
                    apJourn.Frequency = FCConvert.ToString(rsLoad.Get_Fields("frequency"));

                    // TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                    apJourn.JournalNumber = FCConvert.ToInt32(rsLoad.Get_Fields("JournalNumber"));

                    if (Information.IsDate(rsLoad.Get_Fields("Payable"), out date))
                    {
                        apJourn.PayableDate = FCConvert.ToString(date);
                    }

                    // TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
                    //FC:FINAL:CHN: Add explicit convert to prevent runtime errors.
                    // apJourn.Period = Math.Round(Conversion.Val(rsLoad.Get_Fields("Period"))));
                    apJourn.Period = FCConvert.ToInt16(rsLoad.Get_Fields("Period"));

                    if (Information.IsDate(rsLoad.Get_Fields("PostedDate"), out date))
                    {
                        apJourn.PostedDate = FCConvert.ToString(date);
                    }

                    apJourn.PrepaidCheck = rsLoad.Get_Fields_Boolean("PrepaidCheck");
                    apJourn.PrintedIndividual = rsLoad.Get_Fields_Boolean("PrintedIndividual");
                    apJourn.PurchaseOrderID = rsLoad.Get_Fields_Int32("PurchaseOrderID");
                    apJourn.Reference = rsLoad.Get_Fields_String("Reference");
                    apJourn.Returned = rsLoad.Get_Fields_String("Returned");
                    apJourn.Separate = rsLoad.Get_Fields_Boolean("Seperate");
                    apJourn.SeparateCheckRecord = rsLoad.Get_Fields_Int32("SeperateCheckRecord");
                    apJourn.Status = rsLoad.Get_Fields_String("Status");
                    apJourn.TempVendorAddress1 = rsLoad.Get_Fields_String("TempVendorAddress1");
                    apJourn.TempVendorAddress2 = rsLoad.Get_Fields_String("TempVendorAddress2");
                    apJourn.TempVendorAddress3 = rsLoad.Get_Fields_String("TempVendorAddress3");
                    apJourn.TempVendorAddress4 = FCConvert.ToString(rsLoad.Get_Fields_String("TempVendorAddress4"));
                    apJourn.TempVendorCity = rsLoad.Get_Fields_String("TempVendorCity");
                    apJourn.TempVendorName = rsLoad.Get_Fields_String("TempVendorName");
                    apJourn.TempVendorState = rsLoad.Get_Fields_String("TempVendorState");
                    apJourn.TempVendorZip = rsLoad.Get_Fields_String("TempVendorZip");
                    apJourn.TempVendorZip4 = rsLoad.Get_Fields_String("TempVendorZip4");
                    apJourn.TotalEncumbrance = Conversion.Val(rsLoad.Get_Fields_Decimal("TotalEncumbrance"));
                    apJourn.TownOverride = rsLoad.Get_Fields_String("TownOverride");

                    if (Information.IsDate(rsLoad.Get_Fields("Until"), out date))
                    {
                        apJourn.UntilDate = FCConvert.ToString(date);
                    }

                    apJourn.UseAlternateCash = rsLoad.Get_Fields_Boolean("UseAlternateCash");
                    apJourn.AlternateCashAccount = rsLoad.Get_Fields_String("AlternateCashAccount");

                    // TODO: Check the table for the column [Warrant] and replace with corresponding Get_Field method
                    apJourn.Warrant = FCConvert.ToString(rsLoad.Get_Fields("Warrant"));
                    apJourn.VendorNumber = rsLoad.Get_Fields_Int32("VendorNumber");
                    apJourn.ID = rsLoad.Get_Fields_Int32("ID");
                    apJourn.IsUpdated = false;
                }

                GetAPJournal = apJourn;

            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException()
                                 .Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }

            return GetAPJournal;
        }

        public cAPJournal GetFullAPJournal(int lngID)
        {
            cAPJournal GetFullAPJournal = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                cAPJournal aJourn;
                aJourn = GetAPJournal(lngID);
                if (!(aJourn == null))
                {
                    FillAPJournalEntries_6(aJourn.ID, aJourn.JournalEntries);
                }
                GetFullAPJournal = aJourn;
                return GetFullAPJournal;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetFullAPJournal;
        }

        private void FillAPJournalEntries_6(int lngID, cGenericCollection jDetails, string strArchive = "")
        {
            FillAPJournalEntries(lngID, ref jDetails, strArchive);
        }

        private void FillAPJournalEntries(int lngID, ref cGenericCollection jDetails, string strArchive = "")
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                cAPJournalDetail aDetail;
                jDetails.ClearList();

                if (strArchive != "")
                {
                    rsLoad.GroupName = strArchive;
                }

                rsLoad.OpenRecordset("select * from apjournaldetail where apjournalid = " + FCConvert.ToString(lngID), "Budgetary");

                while (!rsLoad.EndOfFile())
                {
                    ////////Application.DoEvents();
                    aDetail = new cAPJournalDetail();
                    FillAPDetail(ref aDetail, ref rsLoad);
                    jDetails.AddItem(aDetail);
                    rsLoad.MoveNext();
                }

            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
        }

        private void FillAPDetail(ref cAPJournalDetail aDetail, ref clsDRWrapper rs)
        {
            // TODO: Check the table for the column [account] and replace with corresponding Get_Field method
            aDetail.Account = rs.Get_Fields_String("account");
            // TODO: Check the table for the column [amount] and replace with corresponding Get_Field method
            aDetail.Amount = Conversion.Val(rs.Get_Fields("amount"));
            aDetail.APJournalID = rs.Get_Fields_Int32("Apjournalid");
            aDetail.CorrectedAmount = Conversion.Val(rs.Get_Fields_Decimal("CorrectedAmount"));
            aDetail.Description = rs.Get_Fields_String("Description");
            // TODO: Check the table for the column [Discount] and replace with corresponding Get_Field method
            aDetail.Discount = Conversion.Val(rs.Get_Fields("Discount"));
            aDetail.Encumbrance = Conversion.Val(rs.Get_Fields_Decimal("Encumbrance"));
            aDetail.EncumbranceDetailRecord = rs.Get_Fields_Int32("EncumbranceDetailRecord");
            aDetail.ID = rs.Get_Fields_Int32("ID");
            aDetail.Project = rs.Get_Fields_String("Project");
            aDetail.PurchaseOrderDetailsID = rs.Get_Fields_Int32("PurchaseOrderDetailsID");
            aDetail.RCB = rs.Get_Fields_String("rcb");
            // TODO: Check the table for the column [1099] and replace with corresponding Get_Field method
            aDetail.Ten99 = rs.Get_Fields_String("1099");
        }

        public cGenericCollection GetFullAPJournalsByJournalNumber(int lngJournalNumber, string strArchive = "")
        {
            cGenericCollection GetFullAPJournalsByJournalNumber = null;
            ClearErrors();
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                cGenericCollection gColl = new cGenericCollection();
                cAPJournal apJourn;
                DateTime date;

                if (strArchive != "")
                {
                    rsLoad.GroupName = strArchive;
                }

                rsLoad.OpenRecordset("select * from APJournal where journalnumber = " + FCConvert.ToString(lngJournalNumber) + "  order by Description", "Budgetary");

                while (!rsLoad.EndOfFile())
                {
                    apJourn = new cAPJournal();

                    // TODO: Check the table for the column [amount] and replace with corresponding Get_Field method
                    apJourn.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));

                    if (Information.IsDate(rsLoad.Get_Fields("CheckDate"), out date))
                    {
                        apJourn.CheckDate = FCConvert.ToString(date);
                    }

                    // TODO: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                    apJourn.CheckNumber = FCConvert.ToString(rsLoad.Get_Fields("CheckNumber"));
                    apJourn.CREDITMEMOCORRECTION = rsLoad.Get_Fields_Boolean("CreditMemoCorrection");
                    apJourn.creditMemoRecord = rsLoad.Get_Fields_Int32("creditmemorecord");
                    apJourn.Description = rsLoad.Get_Fields_String("Description");
                    apJourn.EFTCheck = rsLoad.Get_Fields_Boolean("EFTCheck");
                    apJourn.EncumbranceRecord = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("EncumbranceRecord"))));

                    // TODO: Check the table for the column [frequency] and replace with corresponding Get_Field method
                    apJourn.Frequency = FCConvert.ToString(rsLoad.Get_Fields("frequency"));

                    // TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                    apJourn.JournalNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("JournalNumber"))));

                    if (Information.IsDate(rsLoad.Get_Fields("Payable"), out date))
                    {
                        apJourn.PayableDate = FCConvert.ToString(date);
                    }

                    // TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
                    //FC:FINAL:CHN: Add explicit convert to prevent runtime errors.
                    // apJourn.Period = Math.Round(Conversion.Val(rsLoad.Get_Fields("Period"))));
                    apJourn.Period = FCConvert.ToInt16(Math.Round(Conversion.Val(rsLoad.Get_Fields("Period"))));

                    if (Information.IsDate(rsLoad.Get_Fields("PostedDate"), out date))
                    {
                        apJourn.PostedDate = FCConvert.ToString(date);
                    }

                    apJourn.PrepaidCheck = rsLoad.Get_Fields_Boolean("PrepaidCheck");
                    apJourn.PrintedIndividual = rsLoad.Get_Fields_Boolean("PrintedIndividual");
                    apJourn.PurchaseOrderID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("PurchaseOrderID"))));
                    apJourn.Reference = rsLoad.Get_Fields_String("Reference");
                    apJourn.Returned = rsLoad.Get_Fields_String("Returned");
                    apJourn.Separate = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("Seperate"));
                    apJourn.SeparateCheckRecord = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("SeperateCheckRecord"))));
                    apJourn.Status = rsLoad.Get_Fields_String("Status");
                    apJourn.TempVendorAddress1 = rsLoad.Get_Fields_String("TempVendorAddress1");
                    apJourn.TempVendorAddress2 = rsLoad.Get_Fields_String("TempVendorAddress2");
                    apJourn.TempVendorAddress3 = rsLoad.Get_Fields_String("TempVendorAddress3");
                    apJourn.TempVendorAddress4 = rsLoad.Get_Fields_String("TempVendorAddress4");
                    apJourn.TempVendorCity = rsLoad.Get_Fields_String("TempVendorCity");
                    apJourn.TempVendorName = rsLoad.Get_Fields_String("TempVendorName");
                    apJourn.TempVendorState = rsLoad.Get_Fields_String("TempVendorState");
                    apJourn.TempVendorZip = rsLoad.Get_Fields_String("TempVendorZip");
                    apJourn.TempVendorZip4 = rsLoad.Get_Fields_String("TempVendorZip4");
                    apJourn.TotalEncumbrance = Conversion.Val(rsLoad.Get_Fields_Decimal("TotalEncumbrance"));
                    apJourn.TownOverride = rsLoad.Get_Fields_String("TownOverride");

                    if (Information.IsDate(rsLoad.Get_Fields("Until"), out date))
                    {
                        apJourn.UntilDate = FCConvert.ToString(date);
                    }

                    apJourn.UseAlternateCash = rsLoad.Get_Fields_Boolean("UseAlternateCash");
                    apJourn.AlternateCashAccount = rsLoad.Get_Fields_String("AlternateCashAccount");

                    // TODO: Check the table for the column [Warrant] and replace with corresponding Get_Field method
                    apJourn.Warrant = FCConvert.ToString(rsLoad.Get_Fields("Warrant"));
                    apJourn.ID = rsLoad.Get_Fields_Int32("ID");
                    apJourn.IsUpdated = false;
                    apJourn.VendorNumber = rsLoad.Get_Fields_Int32("VendorNumber");
                    FillAPJournalEntries_6(apJourn.ID, apJourn.JournalEntries);
                    gColl.AddItem(apJourn);
                    rsLoad.MoveNext();
                }

                GetFullAPJournalsByJournalNumber = gColl;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetFullAPJournalsByJournalNumber;
        }

        public cGenericCollection GetAPJournalsByJournalNumber(int lngJournalNumber)
        {
            cGenericCollection GetAPJournalsByJournalNumber = null;
            ClearErrors();
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                cGenericCollection gColl = new cGenericCollection();
                cAPJournal apJourn;
                DateTime date;
                rsLoad.OpenRecordset("select * from APJournal where journalnumber = " + FCConvert.ToString(lngJournalNumber) + "  order by Description", "Budgetary");

                while (!rsLoad.EndOfFile())
                {
                    apJourn = new cAPJournal();

                    // TODO: Check the table for the column [amount] and replace with corresponding Get_Field method
                    apJourn.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));

                    if (Information.IsDate(rsLoad.Get_Fields("CheckDate"), out date))
                    {
                        apJourn.CheckDate = FCConvert.ToString(date);
                    }

                    // TODO: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                    apJourn.CheckNumber = FCConvert.ToString(rsLoad.Get_Fields("CheckNumber"));
                    apJourn.CREDITMEMOCORRECTION = rsLoad.Get_Fields_Boolean("CreditMemoCorrection");
                    apJourn.creditMemoRecord = rsLoad.Get_Fields_Int32("creditmemorecord");
                    apJourn.Description = rsLoad.Get_Fields_String("Description");
                    apJourn.EFTCheck = rsLoad.Get_Fields_Boolean("EFTCheck");
                    apJourn.EncumbranceRecord = rsLoad.Get_Fields_Int32("EncumbranceRecord");

                    // TODO: Check the table for the column [frequency] and replace with corresponding Get_Field method
                    apJourn.Frequency = FCConvert.ToString(rsLoad.Get_Fields("frequency"));

                    // TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                    apJourn.JournalNumber = FCConvert.ToInt32(rsLoad.Get_Fields("JournalNumber"));

                    if (Information.IsDate(rsLoad.Get_Fields("Payable"), out date))
                    {
                        apJourn.PayableDate = FCConvert.ToString(date);
                    }

                    // TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
                    apJourn.Period = FCConvert.ToInt16(rsLoad.Get_Fields("Period"));

                    if (Information.IsDate(rsLoad.Get_Fields("PostedDate"), out date))
                    {
                        apJourn.PostedDate = FCConvert.ToString(date);
                    }

                    apJourn.PrepaidCheck = rsLoad.Get_Fields_Boolean("PrepaidCheck");
                    apJourn.PrintedIndividual = rsLoad.Get_Fields_Boolean("PrintedIndividual");
                    apJourn.PurchaseOrderID = rsLoad.Get_Fields_Int32("PurchaseOrderID");
                    apJourn.Reference = rsLoad.Get_Fields_String("Reference");
                    apJourn.Returned = rsLoad.Get_Fields_String("Returned");
                    apJourn.Separate = rsLoad.Get_Fields_Boolean("Seperate");
                    apJourn.SeparateCheckRecord = rsLoad.Get_Fields_Int32("SeperateCheckRecord");
                    apJourn.Status = rsLoad.Get_Fields_String("Status");
                    apJourn.TempVendorAddress1 = rsLoad.Get_Fields_String("TempVendorAddress1");
                    apJourn.TempVendorAddress2 = rsLoad.Get_Fields_String("TempVendorAddress2");
                    apJourn.TempVendorAddress3 = rsLoad.Get_Fields_String("TempVendorAddress3");
                    apJourn.TempVendorAddress4 = rsLoad.Get_Fields_String("TempVendorAddress4");
                    apJourn.TempVendorCity = rsLoad.Get_Fields_String("TempVendorCity");
                    apJourn.TempVendorName = rsLoad.Get_Fields_String("TempVendorName");
                    apJourn.TempVendorState = rsLoad.Get_Fields_String("TempVendorState");
                    apJourn.TempVendorZip = rsLoad.Get_Fields_String("TempVendorZip");
                    apJourn.TempVendorZip4 = rsLoad.Get_Fields_String("TempVendorZip4");
                    apJourn.TotalEncumbrance = Conversion.Val(rsLoad.Get_Fields_Decimal("TotalEncumbrance"));
                    apJourn.TownOverride = rsLoad.Get_Fields_String("TownOverride");

                    if (Information.IsDate(rsLoad.Get_Fields("Until"), out date))
                    {
                        apJourn.UntilDate = FCConvert.ToString(date);
                    }

                    apJourn.UseAlternateCash = rsLoad.Get_Fields_Boolean("UseAlternateCash");
                    apJourn.AlternateCashAccount = rsLoad.Get_Fields_String("AlternateCashAccount");

                    // TODO: Check the table for the column [Warrant] and replace with corresponding Get_Field method
                    apJourn.Warrant = FCConvert.ToString(rsLoad.Get_Fields("Warrant"));
                    apJourn.ID = rsLoad.Get_Fields_Int32("ID");
                    apJourn.IsUpdated = false;
                    apJourn.VendorNumber = rsLoad.Get_Fields_Int32("VendorNumber");
                    gColl.AddItem(apJourn);
                    rsLoad.MoveNext();
                }

                GetAPJournalsByJournalNumber = gColl;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetAPJournalsByJournalNumber;
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As int
        public int GetAPJournalDetailCount(int lngJournalID)
        {
            int GetAPJournalDetailCount = 0;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                int lngReturn;
                rsLoad.OpenRecordset("Select count(ID) as thecount from APJournalDetail where APJournalID = " + FCConvert.ToString(lngJournalID), "Budgetary");
                // TODO: Field [thecount] not found!! (maybe it is an alias?)
                lngReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("thecount"))));
                GetAPJournalDetailCount = lngReturn;
                return GetAPJournalDetailCount;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetAPJournalDetailCount;
        }

        public void DeleteAPJournalDetailsByAPJournalID(int lngID)
        {
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                if (lngID > 0)
                {
                    clsDRWrapper rsEx = new clsDRWrapper();
                    rsEx.Execute("Delete from apjournaldetail where apjournalID = " + FCConvert.ToString(lngID), "Budgetary");
                }
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        public void DeleteAPJournal(int lngID)
        {
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rs = new clsDRWrapper();
                rs.Execute("Delete from APJournal where id = " + FCConvert.ToString(lngID), "Budgetary");
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        public void UpdateJournalDescription(int lngID, string strDescription)
        {
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rs = new clsDRWrapper();
                string strTemp;
                strTemp = strDescription.Replace("'", "''");
                rs.Execute("update journalmaster set description = '" + strDescription + "'  where id = " + FCConvert.ToString(lngID), "Budgetary");
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        public void DeleteAPJournalDetail(int lngID)
        {
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rs = new clsDRWrapper();
                rs.Execute("Delete from APJournalDetail where id = " + FCConvert.ToString(lngID), "Budgetary");
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        public void UpdateJournalStatus(int lngJournalNumber, string strorigStatus, string strNewStatus)
        {
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                clsDRWrapper rsSave = new clsDRWrapper();
                rsSave.Execute("Update apjournal set status = '" + strNewStatus + "' where status = '" + strorigStatus + "' and JournalNumber = " + FCConvert.ToString(lngJournalNumber), "Budgetary");
                return;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        public cGenericCollection GetAPJournalsByVendorNumber(int lngVendorNumber)
        {
            cGenericCollection GetAPJournalsByVendorNumber = null;
            ClearErrors();
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                cGenericCollection gColl = new cGenericCollection();
                cAPJournal apJourn;
                DateTime date;
                rsLoad.OpenRecordset("select * from APJournal where Vendornumber = " + FCConvert.ToString(lngVendorNumber) + " and status <> 'D' order by journalnumber", "Budgetary");

                while (!rsLoad.EndOfFile())
                {
                    apJourn = new cAPJournal();

                    // TODO: Check the table for the column [amount] and replace with corresponding Get_Field method
                    apJourn.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));

                    if (Information.IsDate(rsLoad.Get_Fields("CheckDate"), out date))
                    {
                        apJourn.CheckDate = FCConvert.ToString(date);
                    }

                    // TODO: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                    apJourn.CheckNumber = FCConvert.ToString(rsLoad.Get_Fields("CheckNumber"));
                    apJourn.CREDITMEMOCORRECTION = rsLoad.Get_Fields_Boolean("CreditMemoCorrection");
                    apJourn.creditMemoRecord = rsLoad.Get_Fields_Int32("creditmemorecord");
                    apJourn.Description = rsLoad.Get_Fields_String("Description");
                    apJourn.EFTCheck = rsLoad.Get_Fields_Boolean("EFTCheck");
                    apJourn.EncumbranceRecord = rsLoad.Get_Fields_Int32("EncumbranceRecord");

                    // TODO: Check the table for the column [frequency] and replace with corresponding Get_Field method
                    apJourn.Frequency = FCConvert.ToString(rsLoad.Get_Fields("frequency"));

                    // TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                    apJourn.JournalNumber = FCConvert.ToInt32(rsLoad.Get_Fields("JournalNumber"));

                    if (Information.IsDate(rsLoad.Get_Fields("Payable"), out date))
                    {
                        apJourn.PayableDate = FCConvert.ToString(date);
                    }

                    // TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
                    //FC:FINAL:CHN: Add explicit convert to prevent runtime errors.
                    // apJourn.Period = Math.Round(Conversion.Val(rsLoad.Get_Fields("Period"))));
                    apJourn.Period = FCConvert.ToInt16(rsLoad.Get_Fields("Period"));

                    if (Information.IsDate(rsLoad.Get_Fields("PostedDate"), out date))
                    {
                        apJourn.PostedDate = FCConvert.ToString(date);
                    }

                    apJourn.PrepaidCheck = rsLoad.Get_Fields_Boolean("PrepaidCheck");
                    apJourn.PrintedIndividual = rsLoad.Get_Fields_Boolean("PrintedIndividual");
                    apJourn.PurchaseOrderID = rsLoad.Get_Fields_Int32("PurchaseOrderID");
                    apJourn.Reference = rsLoad.Get_Fields_String("Reference");
                    apJourn.Returned = rsLoad.Get_Fields_String("Returned");
                    apJourn.Separate = rsLoad.Get_Fields_Boolean("Seperate");
                    apJourn.SeparateCheckRecord = rsLoad.Get_Fields_Int32("SeperateCheckRecord");
                    apJourn.Status = rsLoad.Get_Fields_String("Status");
                    apJourn.TempVendorAddress1 = rsLoad.Get_Fields_String("TempVendorAddress1");
                    apJourn.TempVendorAddress2 = rsLoad.Get_Fields_String("TempVendorAddress2");
                    apJourn.TempVendorAddress3 = rsLoad.Get_Fields_String("TempVendorAddress3");
                    apJourn.TempVendorAddress4 = rsLoad.Get_Fields_String("TempVendorAddress4");
                    apJourn.TempVendorCity = rsLoad.Get_Fields_String("TempVendorCity");
                    apJourn.TempVendorName = rsLoad.Get_Fields_String("TempVendorName");
                    apJourn.TempVendorState = rsLoad.Get_Fields_String("TempVendorState");
                    apJourn.TempVendorZip = rsLoad.Get_Fields_String("TempVendorZip");
                    apJourn.TempVendorZip4 = rsLoad.Get_Fields_String("TempVendorZip4");
                    apJourn.TotalEncumbrance = Conversion.Val(rsLoad.Get_Fields_Decimal("TotalEncumbrance"));
                    apJourn.TownOverride = rsLoad.Get_Fields_String("TownOverride");

                    if (Information.IsDate(rsLoad.Get_Fields("Until"), out date))
                    {
                        apJourn.UntilDate = FCConvert.ToString(date);
                    }

                    apJourn.UseAlternateCash = rsLoad.Get_Fields_Boolean("UseAlternateCash");
                    apJourn.AlternateCashAccount = rsLoad.Get_Fields_String("AlternateCashAccount");

                    // TODO: Check the table for the column [Warrant] and replace with corresponding Get_Field method
                    apJourn.Warrant = FCConvert.ToString(rsLoad.Get_Fields("Warrant"));
                    apJourn.ID = rsLoad.Get_Fields_Int32("ID");
                    apJourn.IsUpdated = false;
                    apJourn.VendorNumber = rsLoad.Get_Fields_Int32("VendorNumber");
                    gColl.AddItem(apJourn);
                    rsLoad.MoveNext();
                }

                GetAPJournalsByVendorNumber = gColl;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetAPJournalsByVendorNumber;
        }

        public void UpdateJournalPeriodAndPayable(ref cAPJournalMasterInfo apInfo)
        {
            try
            {
                var rsSave = new clsDRWrapper();
                var strSQL = "update APJournal set period = " + apInfo.Period + ",payable = '" + apInfo.PayableDate + "' where journalNumber = " + apInfo.JournalNumber;
                rsSave.Execute(strSQL, "Budgetary");
            }
            catch (Exception ex)
            {
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        public cGenericCollection GetUnpostedBriefs()
        {
            cGenericCollection GetUnpostedBriefs = null;
            var rsLoad = new clsDRWrapper();

            try
            {
                var strSQL = "Select * from JournalMaster where [Type] = 'AP' and status <> 'P' order by journalnumber";
                rsLoad.OpenRecordset(strSQL, "Budgetary");
                cAPJournalBrief brief;
                cGenericCollection briefs = new cGenericCollection();

                while (!rsLoad.EndOfFile())
                {
                    brief = MakeAPJournalBrief(ref rsLoad);
                    briefs.AddItem(brief);
                    rsLoad.MoveNext();
                }

                GetUnpostedBriefs = briefs;
            }
            catch (Exception ex)
            {
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetUnpostedBriefs;
        }

        private cAPJournalBrief MakeAPJournalBrief(ref clsDRWrapper record)
        {
            cAPJournalBrief brief = new cAPJournalBrief();
            brief.ID = record.Get_Fields_Int32("ID");
            brief.Description = record.Get_Fields_String("Description");
            brief.JournalNumber = record.Get_Fields_Int16("JournalNumber");
            brief.Period = record.Get_Fields_Int16("Period");
            brief.BankID = record.Get_Fields_Int32("BankNumber");
            brief.PayableDate = GetPayableDate(brief.JournalNumber);
            return brief;
        }

        private string GetPayableDate(Int32 lngJournalNumber)
        {
            ClearErrors();
            string strReturn = "";
            try
            {
                var rsLoad = new clsDRWrapper();
                rsLoad.OpenRecordset("select top 1 payable from apjournal where journalnumber = " + lngJournalNumber +
                                     " and not payable is null", "Budgetary");
                if (!rsLoad.EndOfFile())
                {
                    if (Information.IsDate(rsLoad.Get_Fields("Payable")))
                    {
                        strReturn = rsLoad.Get_Fields_String("Payable");
                    }
                }
                rsLoad.DisposeOf();
            }
            catch (Exception ex)
            {
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return strReturn;
        }
    }
}
