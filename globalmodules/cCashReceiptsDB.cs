﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fecherFoundation;

namespace Global
{
	public class cCashReceiptsDB
	{
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cVersionInfo GetVersion()
		{
			cVersionInfo GetVersion = null;
			try
			{   // On Error GoTo ErrorHandler
				ClearErrors();
				cVersionInfo tVer = new cVersionInfo();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from dbversion", "CashReceipts");
				if (!rsLoad.EndOfFile())
				{
					tVer.Build = rsLoad.Get_Fields_Int32("Build");
					tVer.Major = rsLoad.Get_Fields_Int32("Major");
					tVer.Minor = rsLoad.Get_Fields_Int32("Minor");
					tVer.Revision = rsLoad.Get_Fields_Int32("Revision");
				}
				GetVersion = tVer;
				return GetVersion;
			}
			catch
			{   // ErrorHandler:
				strLastError = Information.Err().Description;
				lngLastError = Information.Err().Number;
			}
			return GetVersion;
		}

		public void SetVersion(ref cVersionInfo nVersion)
		{
			try
			{   // On Error GoTo ErrorHandler
				if (!(nVersion == null))
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from dbversion", "CashReceipts");
					if (rsSave.EndOfFile())
					{
						rsSave.AddNew();
					}
					else
					{
						rsSave.Edit();
					}
					rsSave.Set_Fields("Major", nVersion.Major);
					rsSave.Set_Fields("Minor", nVersion.Minor);
					rsSave.Set_Fields("Revision", nVersion.Revision);
					rsSave.Set_Fields("Build", nVersion.Build);
					rsSave.Update();
				}
				return;
			}
			catch
			{   // ErrorHandler:
				strLastError = Information.Err().Description;
				lngLastError = Information.Err().Number;
			}
		}

		public bool CheckVersion()
		{
			bool CheckVersion = false;
			ClearErrors();
			try
			{   // On Error GoTo ErrorHandler
				cVersionInfo nVer = new cVersionInfo();
				cVersionInfo tVer = new cVersionInfo();
				cVersionInfo cVer;
				bool boolNeedUpdate;


				cVer = GetVersion();
				if (cVer == null)
				{
					cVer = new cVersionInfo();
				}
				nVer.Major = 1; // default to 1.0.0.0

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 0;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddLastYearCheckAndCcTables())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddReceiptIdentifier())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return false;
					}
				}

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 2;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddIdentifiersToArchive())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 3;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
	                if (Remove900SeriesReceiptTypes())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(ref nVer);
		                }
	                }
	                else
	                {
		                return false;
	                }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 4;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddIdentifiersToLastYearArchive())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 7;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (FillIdentifiers())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }


                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 8;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddCCPaymentID())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 10;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddElectronicPaymentLog())
                    {
						nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
							SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 11;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddFieldsToElectronicPaymentLog())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
					}
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 12;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddIsVoidedFlag())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
				CheckVersion = true;
				return CheckVersion;
			}
			catch
			{   // ErrorHandler:
				strLastError = Information.Err().Description;
				lngLastError = Information.Err().Number;
			}
			return CheckVersion;
		}

        private bool AddIsVoidedFlag()
        {
            try
            {
                clsDRWrapper rsUpdate = new clsDRWrapper();
                if (!FieldExists("Receipt", "IsVoided", "CashReceipts"))
                {
                    rsUpdate.Execute("Alter Table.[dbo].[Receipt] Add [IsVoided] bit NULL", "CashReceipts");
                    rsUpdate.Execute("UPDATE Receipt SET IsVoided = 0", "CashReceipts");
                }

                if (!FieldExists("LastYearReceipt", "IsVoided", "CashReceipts"))
                {
                    rsUpdate.Execute("Alter Table.[dbo].[LastYearReceipt] Add [IsVoided] bit NULL", "CashReceipts");
                    rsUpdate.Execute("UPDATE LastYearReceipt SET IsVoided = 0", "CashReceipts");
				}

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

		private bool Remove900SeriesReceiptTypes()
		{
			try
			{
				clsDRWrapper rsUpdate = new clsDRWrapper();

				rsUpdate.Execute("DELETE FROM Type WHERE TypeCode >= 900", "CashReceipts");
				return true;
			}
			catch (Exception e)
			{
				return false;
			}
			
		}

		private bool AddIdentifiersToArchive()
        {
            try
            {
                clsDRWrapper rsUpdate = new clsDRWrapper();
                if (!FieldExists("Archive", "ReceiptIdentifier", "CashReceipts"))
                {
                    rsUpdate.Execute("Alter Table.[dbo].[Archive] Add [ReceiptIdentifier] uniqueidentifier NULL", "CashReceipts");
                }

                if (!FieldExists("Archive", "TransactionIdentifier", "CashReceipts"))
                {
                    rsUpdate.Execute("Alter Table.[dbo].[Archive] Add [TransactionIdentifier] uniqueidentifier NULL", "CashReceipts");
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private bool AddIdentifiersToLastYearArchive()
        {
            try
            {
                clsDRWrapper rsUpdate = new clsDRWrapper();
                if (!FieldExists("LastYearArchive", "ReceiptIdentifier", "CashReceipts"))
                {
                    rsUpdate.Execute("Alter Table.[dbo].[LastYearArchive] Add [ReceiptIdentifier] uniqueidentifier NULL", "CashReceipts");
                }

                if (!FieldExists("LastYearArchive", "TransactionIdentifier", "CashReceipts"))
                {
                    rsUpdate.Execute("Alter Table.[dbo].[LastYearArchive] Add [TransactionIdentifier] uniqueidentifier NULL", "CashReceipts");
                }

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        private bool AddLastYearCheckAndCcTables()
		{
			string strSql;
			clsDRWrapper rsExec = new clsDRWrapper();

			try
			{
				if (!TableExists("LastYearCheckMaster", "CashReceipts"))
				{
					strSql = GetTableCreationHeaderSQL("LastYearCheckMaster");
					strSql = strSql + "[ReceiptNumber] [int] NULL,";
					strSql = strSql + "[CheckNumber] [nvarchar] (50) NULL,";
					strSql = strSql + "[BankNumber] [int] NULL,";
					strSql = strSql + "[Amount] [float] NULL,";
					strSql = strSql + "[EFT] [bit] NULL,";
					strSql = strSql + "[EPymtRefNumber] [nvarchar] (50) NULL,";
					strSql = strSql + "[Last4Digits] [nvarchar] (4) NULL,";
					strSql = strSql + "[AccountType] [nvarchar] (25) NULL,";
					strSql = strSql + "[OriginalReceiptKey] [int] NULL,";
					strSql = strSql + GetTablePKSql("LastYearCheckMaster");

					rsExec.Execute(strSql, "CashReceipts");
				}


				if (!TableExists("LastYearCCMaster", "CashReceipts"))
				{
					strSql = GetTableCreationHeaderSQL("LastYearCCMaster");
					strSql = strSql + "[ReceiptNumber] [int] NULL,";
					strSql = strSql + "[CCType] [int] NULL,";
					strSql = strSql + "[Amount] [float] NULL,";
					strSql = strSql + "[EPymtRefNumber] [nvarchar] (50) NULL,";
					strSql = strSql + "[Last4Digits] [nvarchar] (4) NULL,";
					strSql = strSql + "[OriginalReceiptKey] [int] NULL,";
					strSql = strSql + "[CryptCard] [nvarchar] (255) NULL,";
					strSql = strSql + "[CryptCardExpiration] [nvarchar] (255) NULL,";
					strSql = strSql + "[ConvenienceFee] [money] NULL,";
					strSql = strSql + "[MaskedCreditCardNumber] [nvarchar] (255) NULL,";
					strSql = strSql + "[AuthCode] [nvarchar] (255) NULL,";
					strSql = strSql + "[IsVISA] [bit] NULL,";
					strSql = strSql + "[IsDebit] [bit] NULL,";
					strSql = strSql + "[NonTaxAmount] [money] NULL,";
					strSql = strSql + "[AuthCodeConvenienceFee] [nvarchar] (255) NULL,";
					strSql = strSql + "[AuthCodeVISANonTax] [nvarchar] (255) NULL,";
					strSql = strSql + "[NonTaxConvenienceFee] [money] NULL,";
					strSql = strSql + "[AuthCodeNonTaxConvenienceFee] [nvarchar] (255) NULL,";
					strSql = strSql + "[SecureGUID] [nvarchar] (255) NULL,";
					strSql = strSql + "[EPmtAcctID] [nvarchar] (255) NULL,";
                    strSql = strSql + "[CCPaymentIdentifier] uniqueidentifier NOT NULL Default NEWID()";

                    strSql = strSql + GetTablePKSql("LastYearCCMaster");

					rsExec.Execute(strSql, "CashReceipts");
				}

				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		private bool AddReceiptIdentifier()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsUpdate = new clsDRWrapper();
				if (!FieldExists("Receipt", "ReceiptIdentifier", "CashReceipts"))
				{
					rsUpdate.Execute("Alter Table.[dbo].[Receipt] Add [ReceiptIdentifier] uniqueidentifier NULL", "CashReceipts");
				}

				if (!FieldExists("LastYearReceipt", "ReceiptIdentifier", "CashReceipts"))
				{
					rsUpdate.Execute("Alter Table.[dbo].[LastYearReceipt] Add [ReceiptIdentifier] uniqueidentifier NULL", "CashReceipts");
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

        private bool FillIdentifiers()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                var rsLoad = new clsDRWrapper();
                rsSave.Execute("Update Receipt set ReceiptIdentifier = NEWID() where ReceiptIdentifier is null", "CashReceipts");
                rsSave.Execute("Update LastYearReceipt set ReceiptIdentifier = NEWID() where ReceiptIdentifier is null",
                    "CashReceipts");
                //rsLoad.OpenRecordset("Select ReceiptKey,ReceiptIdentifier from Receipt", "CashReceipts");
                //rsSave.OpenRecordset("Select * from Archive where ReceiptIdentifier is null", "CashReceipts");
                //while (!rsSave.EndOfFile())
                //{
                //    if (rsLoad.FindFirst("ReceiptKey = " + rsSave.Get_Fields_Int32("ReceiptNumber")))
                //    {
                //        rsSave.Edit();
                //        rsSave.SetData("ReceiptIdentifier",rsLoad.Get_Fields_Guid("ReceiptIdentifier"));
                //        rsSave.Update();
                //    }
                //    rsSave.MoveNext();
                //}

                //rsLoad.OpenRecordset("Select ReceiptKey, ReceiptIdentifier from LastYearReceipt", "CashReceipts");
                //rsSave.OpenRecordset("Select * from LastYearArchive where ReceiptIdentifier is null", "CashReceipts");
                //while (!rsSave.EndOfFile())
                //{
                //    if (rsLoad.FindFirst("ReceiptKey = " + rsSave.Get_Fields_Int32("ReceiptNumber")))
                //    {
                //        rsSave.Edit();
                //        rsSave.SetData("ReceiptIdentifier",rsLoad.Get_Fields_Guid("ReceiptIdentifier"));
                //        rsSave.Update();
                //    }

                //    rsSave.MoveNext();
                //}
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddCCPaymentID()
        {
            try
            {
                clsDRWrapper rsUpdate = new clsDRWrapper();
                if (!FieldExists("CCMaster", "CCPaymentIdentifier", "CashReceipts"))
                {
                    rsUpdate.Execute("Alter Table.[dbo].[CCMaster] Add [CCPaymentIdentifier] uniqueidentifier NOT NULL Default NEWID()", "CashReceipts");
                }

                if (!FieldExists("LastYearCCMaster", "CCPaymentIdentifier", "CashReceipts"))
                {
                    rsUpdate.Execute("Alter Table.[dbo].[LastYearCCMaster] Add [CCPaymentIdentifier] uniqueidentifier NOT NULL Default NEWID()", "CashReceipts");
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddElectronicPaymentLog()
        {
            try
            {
				
				var rsUpdate = new clsDRWrapper();
                if (!TableExists("ElectronicPaymentLog", "CashReceipts"))
                {
                    var strSql = GetTableCreationHeaderSQL("ElectronicPaymentLog");
                    strSql = strSql + "[TransactionGroupId] [uniqueidentifier] NULL,";
                    strSql = strSql + "[StatusCode] [nvarchar] (100) NULL,";
                    strSql = strSql + "[Amount] [Decimal] (18,2) Not NULL,";
                    strSql = strSql + "[Fee] [Decimal] (18,2) NOT NULL,";
                    strSql = strSql + "[ReferenceId] [nvarchar] (255) NULL,";
                    strSql = strSql + "[ResponseData] [nvarchar] (1023) NULL,";
                    strSql = strSql + "[ErrorDescription] [nvarchar] (1023) NULL,";
                    strSql = strSql + "[Success] [bit] Not NULL,";
                    strSql = strSql + "[MerchantCode] [nvarchar] (100) NULL,";
                    strSql = strSql + "[InTestMode] bit Not NULL,";
                    strSql = strSql + "[ElectronicPaymentIdentifier] [uniqueidentifier] NULL,";
                    strSql = strSql + GetTablePKSql("ElectronicPaymentLog");

                    rsUpdate.Execute(strSql, "CashReceipts");
				}

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddFieldsToElectronicPaymentLog()
        {
            try
            {
				var rsUpdate = new clsDRWrapper();
                if (!FieldExists("ElectronicPaymentLog", "TellerID", "CashReceipts"))
                {
                    rsUpdate.Execute("alter table electronicpaymentlog add  TellerId [nvarchar] (100) NULL",
                        "CashReceipts");
                    rsUpdate.Execute("update electronicpaymentlog set tellerid = ''", "CashReceipts");
                }

                if (!FieldExists("ElectronicPaymentLog", "PaymentTime", "CashReceipts"))
                {
                    rsUpdate.Execute("alter table electronicpaymentlog add PaymentTime [datetime] NULL",
                        "CashReceipts");
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
		private bool TableExists(string strTable, string strDB)
		{
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			try
			{
				strSQL = "select * from sys.tables where name = '" + strTable + "' and type = 'U'";
				rsLoad.OpenRecordset(strSQL, strDB);
				if (rsLoad.EndOfFile())
				{
					return false;
				}
				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool FieldExists(string strTable, string strField, string strDB)
		{
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			try
			{
				strSQL = "Select column_name from information_schema.columns where column_name = '" + strField + "' and table_name = '" + strTable + "'";
				rsLoad.OpenRecordset(strSQL, strDB);

				if (!rsLoad.EndOfFile())
				{
					return true;
				}
				return false;
			}
			catch
			{
				return false;
			}
		}

		private string GetTableCreationHeaderSQL(string strTableName)
		{
			string GetTableCreationHeaderSQL = "";
			string strSQL;
			strSQL = "if not exists (select * from information_schema.tables where table_name = N'" + strTableName + "') " + "\r\n";
			strSQL += "Create Table [dbo].[" + strTableName + "] (";
			strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
			GetTableCreationHeaderSQL = strSQL;
			return GetTableCreationHeaderSQL;
		}

		private string GetTablePKSql(string strTableName)
		{
			string GetTablePKSql = "";
			string strSQL;
			strSQL = " Constraint [PK_" + strTableName + "] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
			GetTablePKSql = strSQL;
			return GetTablePKSql;
		}

		private string GetDataType(string strTableName, string strFieldName, string strDB)
		{
			string dataType = "";
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			strSQL = "select data_type feom information_schema.columns where table_name = '" + strTableName +  "' and column_name = '" + strFieldName + "'";
			rsLoad.OpenRecordset(strSQL, strDB);
			if (!rsLoad.EndOfFile())
			{
				dataType = rsLoad.Get_Fields_String("data_type");
			}

			return dataType;
		}


	}
}




