//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic;

namespace Global
{
	public class cNetHoster
	{

		//=========================================================
		const string Desired_Net_Version = "v4.0.30319";

		const string CLSID_CorRuntimeHost = "{CB2F6723-AB3A-11D2-9C40-00C04FA30A3E}";
		const string IID_ICorRuntimeHost = "{CB2F6722-AB3A-11D2-9C40-00C04FA30A3E}";
		const string IID_AppDomain = "{05F696DC-2B29-3663-AD8B -C4389CF2A713}";

		const int STARTUP_LOADER_OPTIMIZATION_SINGLE_DOMAIN = 2;

		// vbPorter upgrade warning: clrHost As CorRuntimeHost	OnWrite(object)
		private mscoree.CorRuntimeHost clrHost = new mscoree.CorRuntimeHost();
		private mscorlib.AppDomain cur_Domain = new mscorlib.AppDomain();



		[DllImport("OLE32")]
		private static extern int CLSIDFromString(int lpszCLSID, ref void* pclsid);

		[DllImport("msvbvm60.dll", EntryPoint = "__vbaObjSetAddref")]
		private static extern int ObjSetAddRef(ref object objDest, int pObject);


		[DllImport("Mscoree.dll")]
		private static extern int CorBindToRuntimeEx(int pwszVersion, int pwszBuildFlavor, int flags, ref void* rclsid, ref void* riid, ref int cor);

		// Returns a counted Object reference from the given pointer
		private object GetObjFromPtr(int Ptr)
		{
			object GetObjFromPtr = null;
			ObjSetAddRef(ref GetObjFromPtr, Ptr);
			return GetObjFromPtr;
		}

		private bool oleIID_2(string iid, ref byte[] bar) { return oleIID(ref iid, ref bar); }
		private bool oleIID(ref string iid, ref byte[] bar)
		{
			bool oleIID = false;
			int tmp;

			 /*? On Error Resume Next  */
			tmp = Information.UBound(bar, 1);

			 /*? On Error GoTo 0 */
			if (tmp<15) bar = new byte[15 + 1];
			if (CLSIDFromString(StrPtr(iid), ref bar[0])==0) oleIID = true;
			return oleIID;
		}


		public object GetObjectFromNet(string strDLLName, string strObjectName)
		{
			object GetObjectFromNet = null;
			string strPath;
			object retObject;
			strPath = Application.StartupPath+"\\"+strDLLName;
			retObject = GetObjectFromNetFullPath(strPath, strObjectName);

			GetObjectFromNet = retObject;
			return GetObjectFromNet;
		}

		public object GetObjectFromNetFullPath(string strDLLPath, string strObjectName)
		{
			object GetObjectFromNetFullPath = null;
			if (clrHost==null) {
				StartNet();
			}
			object retObject;
			retObject = cur_Domain.CreateInstanceFrom(strDLLPath, strObjectName).Unwrap;
			GetObjectFromNetFullPath = retObject;
			return GetObjectFromNetFullPath;
		}

		public void StartNet()
		{
			if (clrHost==null) {
				object cls_cor = null;
				int hr;
				byte []cid = new byte[15 + 1];
				byte []iid = new byte[15 + 1];

				oleIID_2(CLSID_CorRuntimeHost, ref cid);
				oleIID_2(IID_ICorRuntimeHost, ref iid);

				hr = CorBindToRuntimeEx(StrPtr(Desired_Net_Version), 0, STARTUP_LOADER_OPTIMIZATION_SINGLE_DOMAIN, ref cid[0], ref iid[0], Convert.ToInt32(cls_cor));

				clrHost = GetObjFromPtr(Convert.ToInt32(cls_cor));

				clrHost.Start();
				clrHost.GetDefaultDomain(cur_Domain);
			}
		}

		private void EndNet()
		{
			if (!(clrHost==null)) {
				clrHost.Stop();
			}
			clrHost = null;
		}



		~cNetHoster()
		{
			EndNet();
		}

	}
}