﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Drawing;

#if TWCR0000
using TWCR0000;


#elif TWUT0000
using modGlobal = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for sarUTDADetail.
	/// </summary>
	public partial class sarUTDADetail : FCSectionReport
	{
		// nObj = 1
		//   0	sarUTDADetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/06/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/15/2007              *
		// ********************************************************
		float lngWidth;
		clsDRWrapper rsData = new clsDRWrapper();
		string strSQL = "";
		clsDRWrapper rsReceiptNumber = new clsDRWrapper();
		bool boolWide;
		int intType;
		string strSQLAddition = "";
		clsDRWrapper rsBill = new clsDRWrapper();

		public sarUTDADetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarUTDADetail_ReportEnd;
		}

        private void SarUTDADetail_ReportEnd(object sender, EventArgs e)
        {
			rsData.DisposeOf();
            rsReceiptNumber.DisposeOf();
            rsBill.DisposeOf();

		}

        public static sarUTDADetail InstancePtr
		{
			get
			{
				return (sarUTDADetail)Sys.GetInstance(typeof(sarUTDADetail));
			}
		}

		protected sarUTDADetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true)
			{
				eArgs.EOF = false;
				BindFields();
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// Setup Totals
			intType = rptUTDailyAuditMaster.InstancePtr.intType;
			// intType = CInt(Me.Tag)
			if (modUTCalculations.Statics.gboolShowMapLotInUTAudit)
			{
				rsBill.OpenRecordset("SELECT ID AS Bill, WLienRecordNumber, SLienRecordNumber, MapLot, BName, BName2 FROM Bill", modExtraModules.strUTDatabase);
			}
			switch (intType)
			{
				case 0:
					{
						strSQLAddition = "AND Service <> 'S' ";
						break;
					}
				case 1:
					{
						strSQLAddition = "AND Service <> 'W' ";
						break;
					}
				case 2:
					{
						strSQLAddition = "";
						break;
					}
			}
			//end switch
			// kk05072014 trouts-89   Added alias to the subquery
			if (modGlobalConstants.Statics.gboolCR)
			{
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(Tax) AS TTax, SUM(PreLienInterest + CurrentInterest) AS Interest, SUM(LienCost) AS Costs FROM (SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND ReceiptNumber > 0) AS qTmp";
			}
			else
			{
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(Tax) AS TTax, SUM(PreLienInterest + CurrentInterest) AS Interest, SUM(LienCost) AS Costs FROM (SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I') AS qTmp";
			}
			rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			if (rsData.EndOfFile() != true)
			{
				// fill the totals in
				fldTotalPrin.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("Prin")), "#,##0.00");
				// TODO: Field [TTax] not found!! (maybe it is an alias?)
				fldTotalTax.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("TTax")), "#,##0.00");
				// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
				fldTotalInt.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("Interest")), "#,##0.00");
				if (fldTotalInt.Text == "0.00")
					fldTotalInt.Text = " 0.00";
				// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
				fldTotalCosts.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("Costs")), "#,##0.00");
				if (modGlobalConstants.Statics.gboolCR)
				{
					rsReceiptNumber.OpenRecordset("SELECT ReceiptKey, ReceiptNumber FROM Receipt", modExtraModules.strCRDatabase);
				}
			}
			else
			{
				fldTotalPrin.Text = " 0.00";
				fldTotalTax.Text = " 0.00";
				fldTotalInt.Text = " 0.00";
				fldTotalCosts.Text = " 0.00";
				fldTotalTotal.Text = " 0.00";
			}
			// calculate the total line
			fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldTotalPrin.Text) + FCConvert.ToDouble(fldTotalTax.Text) + FCConvert.ToDouble(fldTotalInt.Text) + FCConvert.ToDouble(fldTotalCosts.Text), "#,##0.00");
			// this will put the SQL string together and set the order
			if (modGlobalConstants.Statics.gboolCR)
			{
				if (arUTDailyAudit.InstancePtr.boolOrderByReceipt)
				{
					strSQL = "SELECT * FROM PaymentRec INNER JOIN Master ON PaymentRec.AccountKey = Master.ID WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND PaymentRec.Code <> 'I' AND ReceiptNumber > 0 ORDER BY ReceiptNumber, BillNumber, AccountNumber, RecordedTransactionDate";
				}
				else
				{
					strSQL = "SELECT * FROM PaymentRec INNER JOIN Master ON PaymentRec.AccountKey = Master.ID WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND PaymentRec.Code <> 'I' AND ReceiptNumber > 0 ORDER BY AccountNumber, BillNumber, RecordedTransactionDate";
				}
			}
			else
			{
				if (arUTDailyAudit.InstancePtr.boolOrderByReceipt)
				{
					strSQL = "SELECT * FROM PaymentRec INNER JOIN Master ON PaymentRec.AccountKey = Master.ID WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND PaymentRec.Code <> 'I' ORDER BY ReceiptNumber, BillNumber, AccountNumber, RecordedTransactionDate";
				}
				else
				{
					strSQL = "SELECT * FROM PaymentRec INNER JOIN Master ON PaymentRec.AccountKey = Master.ID WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND PaymentRec.Code <> 'I' ORDER BY AccountNumber, BillNumber, RecordedTransactionDate";
				}
			}
			rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			SetupFields();
			frmWait.InstancePtr.IncrementProgress();
			if (rsData.EndOfFile())
			{
				EndRoutine();
			}
		}

		private void SetupFields()
		{
			// this will adjust the fields to thier correct
			lngWidth = rptUTDailyAuditMaster.InstancePtr.lngWidth;
			boolWide = rptUTDailyAuditMaster.InstancePtr.boolLandscape;
			// header labels/lines
			if (!boolWide)
			{
				// portrait
				lblAccount.Left = 0;
				// kk05262014 trouts-89  lblYear.Left = 720
				lblYear.Left = 850 / 1440F;
				lblPrincipal.Left = 1350 / 1440F;
				lblTax.Left = 2520 / 1440F;
				lblInterest.Left = 3420 / 1440F;
				lblCosts.Left = 4320 / 1440F;
				lblTotal.Left = 5220 / 1440F;
				lblPer.Left = 6390 / 1440F;
				lblCode.Left = 6660 / 1440F;
				lblCash.Left = 6930 / 1440F;
				lblRef.Left = 7470 / 1440F;
				lblDate.Left = 8370 / 1440F;
				lblTeller.Left = 9360 / 1440F;
				lblRNum.Left = 9900 / 1440F;
				// kk05262014 trouts-89  lblAccount.Width = 720
				lblAccount.Width = 850 / 1440F;
				lblYear.Width = 600 / 1440F;
				lblPrincipal.Width = 1170 / 1440F;
				lblTax.Width = 900 / 1440F;
				lblInterest.Width = 900 / 1440F;
				lblCosts.Width = 900 / 1440F;
				lblTotal.Width = 1170 / 1440F;
				lblPer.Width = 270 / 1440F;
				lblCode.Width = 270 / 1440F;
				lblCash.Width = 540 / 1440F;
				lblRef.Width = 900 / 1440F;
				lblDate.Width = 990 / 1440F;
				lblTeller.Width = 540 / 1440F;
				lblRNum.Width = 900 / 1440F;
				lblAccount.Text = "Acct";
				lblYear.Text = "Bill";
				lblPrincipal.Text = "Prin";
				lblTax.Text = "Tax";
				lblInterest.Text = "Int";
				lblCosts.Text = "Cost";
				lblTotal.Text = "Total";
				lblPer.Text = "P";
				lblCode.Text = "C";
				// lblCash.Text =
				lblRef.Text = "Ref";
				lblDate.Text = "Date";
				// lblTeller.Text =
				lblRNum.Text = "Rec #";
			}
			else
			{
				// landscape
				lblAccount.Left = 0;
				lblYear.Left = 1080 / 1440F;
				lblPrincipal.Left = 1980 / 1440F;
				lblTax.Left = 3150 / 1440F;
				lblInterest.Left = 4320 / 1440F;
				lblCosts.Left = 5490 / 1440F;
				lblTotal.Left = 6660 / 1440F;
				lblPer.Left = 7920 / 1440F;
				lblCode.Left = 8190 / 1440F;
				lblCash.Left = 8640 / 1440F;
				lblRef.Left = 9270 / 1440F;
				lblDate.Left = 10620 / 1440F;
				lblTeller.Left = 11790 / 1440F;
				lblRNum.Left = 12600 / 1440F;
				// kk05262014 trouts-89  lblAccount.Width = 810
				lblAccount.Width = 850 / 1440F;
				// kk05262014 trouts-89  lblYear.Width = 720
				lblYear.Width = 700 / 1440F;
				lblPrincipal.Width = 1170 / 1440F;
				lblTax.Width = 1170 / 1440F;
				lblInterest.Width = 1170 / 1440F;
				lblCosts.Width = 1170 / 1440F;
				lblTotal.Width = 1260 / 1440F;
				lblPer.Width = 270 / 1440F;
				lblCode.Width = 270 / 1440F;
				lblCash.Width = 540 / 1440F;
				lblRef.Width = 1260 / 1440F;
				lblDate.Width = 1170 / 1440F;
				lblTeller.Width = 540 / 1440F;
				lblRNum.Width = 810 / 1440F;
			}
			lnHeader.X2 = lngWidth;
			this.PrintWidth = lngWidth;
			// Left
			// Detail section
			fldAccount.Left = lblAccount.Left;
			fldYear.Left = lblYear.Left;
			fldPrincipal.Left = lblPrincipal.Left;
			fldTax.Left = lblTax.Left;
			fldInterest.Left = lblInterest.Left;
			fldCosts.Left = lblCosts.Left;
			fldTotal.Left = lblTotal.Left;
			fldPer.Left = lblPer.Left;
			fldCode.Left = lblCode.Left;
			fldCash.Left = lblCash.Left;
			fldRef.Left = lblRef.Left;
			fldDate.Left = lblDate.Left;
			fldTeller.Left = lblTeller.Left;
			fldReceipt.Left = lblRNum.Left;
			// Footer Labels/Fields
			lblTotals.Left = lblYear.Left;
			fldTotalPrin.Left = lblPrincipal.Left;
			fldTotalTax.Left = lblTax.Left;
			fldTotalInt.Left = lblInterest.Left;
			fldTotalCosts.Left = lblCosts.Left;
			fldTotalTotal.Left = lblTotal.Left;
			// Widths
			// Detail
			fldAccount.Width = lblAccount.Width;
			fldYear.Width = lblYear.Width;
			fldPrincipal.Width = lblPrincipal.Width;
			fldTax.Width = lblTax.Width;
			fldInterest.Width = lblInterest.Width;
			fldCosts.Width = lblCosts.Width;
			fldTotal.Width = lblTotal.Width;
			fldPer.Width = lblPer.Width;
			fldCode.Width = lblCode.Width;
			fldCash.Width = lblCash.Width;
			fldRef.Width = lblRef.Width;
			fldDate.Width = lblDate.Width;
			fldTeller.Width = lblTeller.Width;
			fldReceipt.Width = lblRNum.Width;
			// Footer Labels/Fields
			lblTotals.Width = lblYear.Width;
			fldTotalPrin.Width = lblPrincipal.Width;
			fldTotalTax.Width = lblTax.Width;
			fldTotalInt.Width = lblInterest.Width;
			fldTotalCosts.Width = lblCosts.Width;
			fldTotalTotal.Width = lblTotal.Width;
			lnTotals.X1 = lblTotals.Left;
			lnTotals.X2 = fldTotalTotal.Left + fldTotalTotal.Width;
		}

		private void BindFields()
		{
			// this will fill the fields with the correct data
			string strWS = "";
			string strMapLot = "";
			string strName = "";
			frmWait.InstancePtr.IncrementProgress();
			if (modUTCalculations.Statics.gboolShowMapLotInUTAudit)
			{
				strWS = FCConvert.ToString(rsData.Get_Fields_String("Service"));
				fldMapLot.Top = 270 / 1440f;
				fldMapLot.Left = fldRef.Left;
				if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Lien")))
				{
					rsBill.FindFirstRecord(strWS + "LienRecordNumber", rsData.Get_Fields_Int32("BillKey"));
					if (!rsBill.NoMatch)
					{
						// fill the map lot field
						strMapLot = FCConvert.ToString(rsBill.Get_Fields_String("MapLot"));
						if (Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("BName2"))) != "")
						{
							strName = Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("BName"))) + " " + Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("BName2")));
						}
						else
						{
							strName = Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("BName")));
						}
					}
					else
					{
						strMapLot = "";
					}
				}
				else
				{
					rsBill.FindFirstRecord("Bill", rsData.Get_Fields_Int32("BillKey"));
					if (!rsBill.NoMatch)
					{
						// fill the map lot field
						strMapLot = FCConvert.ToString(rsBill.Get_Fields_String("MapLot"));
						if (Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("BName2"))) != "")
						{
							strName = Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("BName"))) + " " + Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("BName2")));
						}
						else
						{
							strName = Strings.Trim(FCConvert.ToString(rsBill.Get_Fields_String("BName")));
						}
					}
					else
					{
						strMapLot = "";
					}
				}
				// fldMapLot.Text = "Map Lot : " & strMapLot
				fldName.Text = strName;
			}
			else
			{
				fldMapLot.Text = "";
				fldName.Text = "";
			}
			// kk05262014 trouts-89
			// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
			if (FCConvert.ToString(rsData.Get_Fields("AccountNumber")).Length > 7)
			{
				fldAccount.Font = new Font(fldAccount.Font.Name, 9);
			}
			else
			{
				fldAccount.Font = new Font(fldAccount.Font.Name, 10);
			}
			//FC:FINAL:MSH - The same as in issue #706: system can't implicitly convert int to string
			//fldAccount.Text = rsData.Get_Fields("AccountNumber");
			// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
			fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
			fldYear.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BillNumber"));
			if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Lien")))
			{
				fldYear.Text = fldYear.Text + "*";
			}
			// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
			fldPrincipal.Text = Strings.Format(rsData.Get_Fields("Principal"), "#,##0.00");
			// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
			fldTax.Text = Strings.Format(rsData.Get_Fields("Tax"), "#,##0.00");
			fldInterest.Text = Strings.Format(rsData.Get_Fields_Decimal("PreLienInterest") + rsData.Get_Fields_Decimal("CurrentInterest"), " #,##0.00");
			fldCosts.Text = Strings.Format(rsData.Get_Fields_Decimal("LienCost"), "#,##0.00");
			// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
			// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
			fldTotal.Text = Strings.Format(rsData.Get_Fields("Principal") + rsData.Get_Fields("Tax") + rsData.Get_Fields_Decimal("PreLienInterest") + rsData.Get_Fields_Decimal("CurrentInterest") + rsData.Get_Fields_Decimal("LienCost"), "#,##0.00");
			// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
			fldPer.Text = FCConvert.ToString(rsData.Get_Fields("Period"));
			// TODO: Field [PaymentRec.Code] not found!! (maybe it is an alias?)
			fldCode.Text = FCConvert.ToString(rsData.Get_Fields("PaymentRec.Code"));
			if (FCConvert.ToString(rsData.Get_Fields_String("CashDrawer")) == "Y")
			{
				fldCash.Text = "Y";
			}
			else
			{
				if (FCConvert.ToString(rsData.Get_Fields_String("GeneralLedger")) == "Y")
				{
					fldCash.Text = "N Y";
				}
				else
				{
					fldCash.Text = "N N";
				}
			}
			fldRef.Text = FCConvert.ToString(rsData.Get_Fields_String("Reference"));
			fldTeller.Text = Strings.Left(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Teller"))) + "   ", 3);
			fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("RecordedTransactionDate"), "MM/dd/yy");
			if (modGlobalConstants.Statics.gboolCR)
			{
				// this will find the real receipt number
				rsReceiptNumber.FindFirstRecord("ReceiptKey", rsData.Get_Fields_Int32("ReceiptNumber"));
				if (!rsReceiptNumber.NoMatch)
				{
					fldReceipt.Text = FCConvert.ToString(rsReceiptNumber.Get_Fields_Int32("ReceiptNumber"));
				}
				else
				{
					//FC:FINAL:MSH - The same situation as in Issue #706: can't implicitly convert int to string
					//fldReceipt.Text = rsData.Get_Fields("ReceiptNumber");
					fldReceipt.Text = FCConvert.ToString(rsData.Get_Fields_Int32("ReceiptNumber"));
				}
			}
			else
			{
				//FC:FINAL:MSH - The same situation as in Issue #706: can't implicitly convert int to string
				//fldReceipt.Text = rsData.Get_Fields("ReceiptNumber");
				fldReceipt.Text = FCConvert.ToString(rsData.Get_Fields_Int32("ReceiptNumber"));
			}
			// move to the next record
			rsData.MoveNext();
		}

		private void EndRoutine()
		{
			// this will show a message that there are no records to process and hide the rest of the report
			lnHeader.Visible = false;
			lnTotals.Visible = false;
			lblCash.Visible = false;
			lblAccount.Visible = false;
			lblYear.Visible = false;
			lblPrincipal.Visible = false;
			lblTax.Visible = false;
			lblInterest.Visible = false;
			lblCosts.Visible = false;
			lblTotal.Visible = false;
			lblPer.Visible = false;
			lblCode.Visible = false;
			lblRef.Visible = false;
			lblDate.Visible = false;
			lblTeller.Visible = false;
			lblRNum.Visible = false;
			fldTotalPrin.Visible = false;
			fldTotalTax.Visible = false;
			fldTotalInt.Visible = false;
			fldTotalCosts.Visible = false;
			fldTotalTotal.Visible = false;
			lblTotals.Visible = false;
			// lblTotals.Width = lngWidth - lblTotals.Left
			// lblTotals.Text = "There are no records to process."
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (modUTCalculations.Statics.gboolShowMapLotInUTAudit)
			{
				Detail.Height = 540 / 1440F;
				fldMapLot.Visible = true;
				fldName.Visible = true;
				fldMapLot.Top = 270 / 1440F;
				fldName.Top = 270 / 1440F;
				fldName.Left = fldPrincipal.Left;
				fldMapLot.Left = fldTotal.Left;
			}
			else
			{
				Detail.Height = 270 / 1440F;
				fldMapLot.Visible = false;
				fldName.Visible = false;
			}
		}

		private void sarUTDADetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarUTDADetail.Caption	= "Daily Audit Detail";
			//sarUTDADetail.Icon	= "sarUTDADetail.dsx":0000";
			//sarUTDADetail.Left	= 0;
			//sarUTDADetail.Top	= 0;
			//sarUTDADetail.Width	= 11880;
			//sarUTDADetail.Height	= 8175;
			//sarUTDADetail.StartUpPosition	= 3;
			//sarUTDADetail.SectionData	= "sarUTDADetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
