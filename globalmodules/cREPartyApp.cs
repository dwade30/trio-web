﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using System.Collections.Generic;
using TWSharedLibrary;

namespace Global
{
	public class cREPartyApp : SharedApplication.ICentralPartyApp
	{
		//=========================================================
		const string strModuleCode = "RE";

		public bool ReferencesParty(int lngID)
		{
			bool ICentralPartyApp_ReferencesParty = false;
			ICentralPartyApp_ReferencesParty = false;
			bool retAnswer;
			retAnswer = false;
			if (lngID > 0)
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select count(id)as theCount from master where ownerpartyid = " + FCConvert.ToString(lngID) + " or secownerpartyid = " + FCConvert.ToString(lngID), "RealEstate");
				// TODO: Field [theCount] not found!! (maybe it is an alias?)
				if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
				{
					retAnswer = true;
				}
				else
				{
					rsLoad.OpenRecordset("select count(id) as theCount from Owners where partyid = " + FCConvert.ToString(lngID), "RealEstate");
					// TODO: Field [theCount] not found!! (maybe it is an alias?)
					if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
					{
						retAnswer = true;
					}
				}
				ICentralPartyApp_ReferencesParty = retAnswer;
			}
			return ICentralPartyApp_ReferencesParty;
		}

		public List<object> AccountsReferencingParty(int lngID)
		{
			List<object> CentralPartyApp_AccountsReferencingParty = null;
			var theCollection = new List<object>();
			clsDRWrapper rsLoad = new clsDRWrapper();
			cCentralPartyReference pRef;
			rsLoad.OpenRecordset("select id,rsaccount,rsmaplot,ownerpartyid, secownerpartyid from master where rscard = 1 and (ownerpartyid = " + FCConvert.ToString(lngID) + " or secownerpartyid = " + FCConvert.ToString(lngID) + ") order by rsaccount", "RealEstate");
			while (!rsLoad.EndOfFile())
			{
				//Application.DoEvents();
				pRef = new cCentralPartyReference();
				pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
				pRef.AlternateIdentifier = FCConvert.ToString(rsLoad.Get_Fields_String("rsmaplot"));
				pRef.Description = "Real Estate Account";
				pRef.Identifier = FCConvert.ToString(rsLoad.Get_Fields_Int32("rsaccount"));
				pRef.IdentifierForSort = Strings.Right(Strings.StrDup(12, "0") + pRef.Identifier, 12);
				pRef.ModuleCode = strModuleCode;
				theCollection.Add(pRef);
				rsLoad.MoveNext();
			}
			rsLoad.OpenRecordset("select id, account from owners where partyid = " + FCConvert.ToString(lngID) + " order by account", "RealEstate");
			while (!rsLoad.EndOfFile())
			{
				//Application.DoEvents();
				pRef = new cCentralPartyReference();
				pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id"));
				// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
				pRef.Identifier = FCConvert.ToString(rsLoad.Get_Fields("account"));
				pRef.IdentifierForSort = Strings.Right(Strings.StrDup(12, "0") + pRef.Identifier, 12);
				pRef.AlternateIdentifier = "";
				pRef.Description = "Real Estate Interested Party";
				pRef.ModuleCode = strModuleCode;
				theCollection.Add(pRef);
				rsLoad.MoveNext();
			}
			CentralPartyApp_AccountsReferencingParty = theCollection;
			return CentralPartyApp_AccountsReferencingParty;
		}

		public string ModuleCode()
		{
			string ICentralPartyApp_ModuleCode = "";
			ICentralPartyApp_ModuleCode = strModuleCode;
			return ICentralPartyApp_ModuleCode;
		}

		public bool ChangePartyID(int lngOriginalPartyID, int lngNewPartyID)
		{
			bool ICentralPartyApp_ChangePartyID = false;
			clsDRWrapper rsSave = new clsDRWrapper();
			string strSQL;
			try
			{
				// On Error GoTo ErrorHandler
				strSQL = "Update master set OwnerPartyID = " + FCConvert.ToString(lngNewPartyID) + " where OwnerPartyID = " + FCConvert.ToString(lngOriginalPartyID);
				rsSave.Execute(strSQL, "RealEstate");
				strSQL = "Update master set SecOwnerPartyID = " + FCConvert.ToString(lngNewPartyID) + " where SecOwnerPartyID = " + FCConvert.ToString(lngOriginalPartyID);
				rsSave.Execute(strSQL, "RealEstate");
				strSQL = "Update Owners set PartyID = " + FCConvert.ToString(lngNewPartyID) + " where PartyID = " + FCConvert.ToString(lngOriginalPartyID);
				rsSave.Execute(strSQL, "RealEstate");
				ICentralPartyApp_ChangePartyID = true;
				return ICentralPartyApp_ChangePartyID;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return ICentralPartyApp_ChangePartyID;
		}
	}
}
