﻿namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class arAllLienDischargeNotice
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arAllLienDischargeNotice));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldMainText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTopDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSigTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSigDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldACKNOWLEDGEMENT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBottomText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSigName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTitleBar = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLegalDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldNotaryLine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNotaryTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNotaryCommissionTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTownHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMuni = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.imgTownSeal = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.imgSig = new GrapeCity.ActiveReports.SectionReportModel.Picture();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.fldMainText)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTopDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSigTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSigDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldACKNOWLEDGEMENT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomText)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSigName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitleBar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLegalDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotaryLine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotaryTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotaryCommissionTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTownHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgTownSeal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgSig)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldMainText,
				this.fldTopDate,
				this.fldSigTitle,
				this.fldSigDate,
				this.fldACKNOWLEDGEMENT,
				this.fldBottomText,
				this.fldSigName,
				this.lblTitleBar,
				this.lblLegalDescription,
				this.lblMapLot,
				this.fldNotaryLine,
				this.fldNotaryTitle,
				this.fldNotaryCommissionTitle,
				this.fldAccount,
				this.lblTownHeader,
				this.fldMuni,
				this.imgTownSeal,
				this.imgSig,
				this.Line2
			});
			this.Detail.Height = 6.541667F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldMainText
			// 
			this.fldMainText.Height = 1.625F;
			this.fldMainText.Left = 0.25F;
			this.fldMainText.Name = "fldMainText";
			this.fldMainText.Style = "font-family: \'Tahoma\'";
			this.fldMainText.Text = null;
			this.fldMainText.Top = 1.3125F;
			this.fldMainText.Width = 6.4375F;
			// 
			// fldTopDate
			// 
			this.fldTopDate.Height = 0.1875F;
			this.fldTopDate.Left = 0.25F;
			this.fldTopDate.Name = "fldTopDate";
			this.fldTopDate.Style = "font-family: \'Tahoma\'";
			this.fldTopDate.Text = null;
			this.fldTopDate.Top = 2.9375F;
			this.fldTopDate.Width = 2.3125F;
			// 
			// fldSigTitle
			// 
			this.fldSigTitle.Height = 0.1875F;
			this.fldSigTitle.Left = 0.5625F;
			this.fldSigTitle.Name = "fldSigTitle";
			this.fldSigTitle.Style = "font-family: \'Tahoma\'";
			this.fldSigTitle.Text = null;
			this.fldSigTitle.Top = 3.375F;
			this.fldSigTitle.Width = 3.0625F;
			// 
			// fldSigDate
			// 
			this.fldSigDate.Height = 0.1875F;
			this.fldSigDate.Left = 3.5625F;
			this.fldSigDate.Name = "fldSigDate";
			this.fldSigDate.Style = "font-family: \'Tahoma\'";
			this.fldSigDate.Text = null;
			this.fldSigDate.Top = 3.75F;
			this.fldSigDate.Width = 3.125F;
			// 
			// fldACKNOWLEDGEMENT
			// 
			this.fldACKNOWLEDGEMENT.Height = 0.3125F;
			this.fldACKNOWLEDGEMENT.Left = 0.25F;
			this.fldACKNOWLEDGEMENT.Name = "fldACKNOWLEDGEMENT";
			this.fldACKNOWLEDGEMENT.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.fldACKNOWLEDGEMENT.Text = "ACKNOWLEDGEMENT";
			this.fldACKNOWLEDGEMENT.Top = 4.5F;
			this.fldACKNOWLEDGEMENT.Width = 6.75F;
			// 
			// fldBottomText
			// 
			this.fldBottomText.Height = 0.9375F;
			this.fldBottomText.Left = 0.25F;
			this.fldBottomText.Name = "fldBottomText";
			this.fldBottomText.Style = "font-family: \'Tahoma\'";
			this.fldBottomText.Text = null;
			this.fldBottomText.Top = 4.8125F;
			this.fldBottomText.Width = 6.75F;
			// 
			// fldSigName
			// 
			this.fldSigName.Height = 0.1875F;
			this.fldSigName.Left = 3.5625F;
			this.fldSigName.Name = "fldSigName";
			this.fldSigName.Style = "font-family: \'Tahoma\'";
			this.fldSigName.Text = null;
			this.fldSigName.Top = 3.375F;
			this.fldSigName.Width = 3.125F;
			// 
			// lblTitleBar
			// 
			this.lblTitleBar.Height = 0.25F;
			this.lblTitleBar.HyperLink = null;
			this.lblTitleBar.Left = 0.25F;
			this.lblTitleBar.Name = "lblTitleBar";
			this.lblTitleBar.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTitleBar.Text = "DISCHARGE OF MORTGAGE FOR TAX COLLECTOR\'S LIEN CERTIFICATE";
			this.lblTitleBar.Top = 0.25F;
			this.lblTitleBar.Width = 7F;
			// 
			// lblLegalDescription
			// 
			this.lblLegalDescription.Height = 0.25F;
			this.lblLegalDescription.HyperLink = null;
			this.lblLegalDescription.Left = 0.25F;
			this.lblLegalDescription.Name = "lblLegalDescription";
			this.lblLegalDescription.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblLegalDescription.Text = "Title 36, M.R.S.A. Section 943";
			this.lblLegalDescription.Top = 0.5F;
			this.lblLegalDescription.Width = 7F;
			// 
			// lblMapLot
			// 
			this.lblMapLot.Height = 0.25F;
			this.lblMapLot.HyperLink = null;
			this.lblMapLot.Left = 0.25F;
			this.lblMapLot.Name = "lblMapLot";
			this.lblMapLot.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblMapLot.Text = null;
			this.lblMapLot.Top = 0.75F;
			this.lblMapLot.Width = 7F;
			// 
			// fldNotaryLine
			// 
			this.fldNotaryLine.Height = 0.1875F;
			this.fldNotaryLine.Left = 3.6875F;
			this.fldNotaryLine.Name = "fldNotaryLine";
			this.fldNotaryLine.Style = "font-family: \'Tahoma\'";
			this.fldNotaryLine.Text = null;
			this.fldNotaryLine.Top = 5.9375F;
			this.fldNotaryLine.Width = 3F;
			// 
			// fldNotaryTitle
			// 
			this.fldNotaryTitle.Height = 0.1875F;
			this.fldNotaryTitle.Left = 3.6875F;
			this.fldNotaryTitle.Name = "fldNotaryTitle";
			this.fldNotaryTitle.Style = "font-family: \'Tahoma\'";
			this.fldNotaryTitle.Text = null;
			this.fldNotaryTitle.Top = 6.125F;
			this.fldNotaryTitle.Width = 3F;
			// 
			// fldNotaryCommissionTitle
			// 
			this.fldNotaryCommissionTitle.Height = 0.1875F;
			this.fldNotaryCommissionTitle.Left = 3.6875F;
			this.fldNotaryCommissionTitle.Name = "fldNotaryCommissionTitle";
			this.fldNotaryCommissionTitle.Style = "font-family: \'Tahoma\'";
			this.fldNotaryCommissionTitle.Text = null;
			this.fldNotaryCommissionTitle.Top = 6.3125F;
			this.fldNotaryCommissionTitle.Width = 3F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0.8125F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0.5625F;
			this.fldAccount.Width = 1.3125F;
			// 
			// lblTownHeader
			// 
			this.lblTownHeader.Height = 0.25F;
			this.lblTownHeader.HyperLink = null;
			this.lblTownHeader.Left = 0.25F;
			this.lblTownHeader.Name = "lblTownHeader";
			this.lblTownHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblTownHeader.Text = null;
			this.lblTownHeader.Top = 0F;
			this.lblTownHeader.Width = 7F;
			// 
			// fldMuni
			// 
			this.fldMuni.Height = 0.1875F;
			this.fldMuni.Left = 3.5625F;
			this.fldMuni.Name = "fldMuni";
			this.fldMuni.Style = "font-family: \'Tahoma\'";
			this.fldMuni.Text = null;
			this.fldMuni.Top = 3.5625F;
			this.fldMuni.Width = 3.125F;
			// 
			// imgTownSeal
			// 
			this.imgTownSeal.Height = 0.8125F;
			this.imgTownSeal.HyperLink = null;
			this.imgTownSeal.ImageData = null;
			this.imgTownSeal.Left = 0F;
			this.imgTownSeal.LineWeight = 1F;
			this.imgTownSeal.Name = "imgTownSeal";
			this.imgTownSeal.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.imgTownSeal.Top = 0F;
			this.imgTownSeal.Visible = false;
			this.imgTownSeal.Width = 0.8125F;
			// 
			// imgSig
			// 
			this.imgSig.Height = 0.5625F;
			this.imgSig.HyperLink = null;
			this.imgSig.ImageData = null;
			this.imgSig.Left = 3.5625F;
			this.imgSig.LineWeight = 1F;
			this.imgSig.Name = "imgSig";
			this.imgSig.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomLeft;
			this.imgSig.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
			this.imgSig.Top = 2.75F;
			this.imgSig.Width = 2.375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.5625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 3.3125F;
			this.Line2.Width = 2.4375F;
			this.Line2.X1 = 3.5625F;
			this.Line2.X2 = 6F;
			this.Line2.Y1 = 3.3125F;
			this.Line2.Y2 = 3.3125F;
			// 
			// arAllLienDischargeNotice
			//
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 2F;
			this.PageSettings.Margins.Left = 0.75F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.fldMainText)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTopDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSigTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSigDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldACKNOWLEDGEMENT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBottomText)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSigName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitleBar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLegalDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotaryLine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotaryTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotaryCommissionTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTownHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgTownSeal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgSig)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMainText;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTopDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSigTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSigDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldACKNOWLEDGEMENT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBottomText;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSigName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitleBar;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLegalDescription;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNotaryLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNotaryTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNotaryCommissionTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTownHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgTownSeal;
		private GrapeCity.ActiveReports.SectionReportModel.Picture imgSig;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
	}
}
