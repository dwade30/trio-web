using fecherFoundation;
using System;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
    /// <summary>
    /// Summary description for rptAutoPrepayAccounts.
    /// </summary>
    public partial class rptAutoPrepayAccounts : BaseSectionReport
    {

        public rptAutoPrepayAccounts()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            if (_InstancePtr == null)
                _InstancePtr = this;
            this.Name = "Accounts with Credits";
        }

        public static rptAutoPrepayAccounts InstancePtr
        {
            get
            {
                return (rptAutoPrepayAccounts)Sys.GetInstance(typeof(rptAutoPrepayAccounts));
            }
        }

        protected rptAutoPrepayAccounts _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            base.Dispose(disposing);
        }

        // nObj = 1
        //   0	rptAutoPrepayAccounts	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Corey Gray              *
        // DATE           :                                       *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               12/15/2005              *
        // ********************************************************

       // clsDRWrapper rsAccount = new clsDRWrapper();
        bool blnStarted;

        public void Init()
        {

            return;
            ErrorHandler:;
            MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + "  " + Information.Err().Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            //modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
            txtDate.Text = DateTime.Today.ToShortDateString();
            txtMuniname.Text = modGlobalConstants.Statics.MuniName;

            srptCreatedCredits.Report = new srptAutoPrepayAccounts();
            srptErrorAccounts.Report = new srptAutoPrepayAccountsError();

            srptCreatedCredits.Width = PrintWidth; // set the sizes of the sub reports
            srptErrorAccounts.Width = PrintWidth;

        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = blnStarted;
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            try
            {   // On Error GoTo ERROR_HANDLER

                blnStarted = true;

                return;
            }
            catch
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", "Error Detail Format", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void PageHeader_Format(object sender, EventArgs e)
        {
            txtPage.Text = "Page " + this.PageNumber;
        }

        private void rptAutoPrepayAccounts_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //rptAutoPrepayAccounts.Caption	= "Accounts with Credits";
            //rptAutoPrepayAccounts.Icon	= "rptAutoPrepayAccounts.dsx":0000";
            //rptAutoPrepayAccounts.Left	= 0;
            //rptAutoPrepayAccounts.Top	= 0;
            //rptAutoPrepayAccounts.Width	= 15240;
            //rptAutoPrepayAccounts.Height	= 11115;
            //rptAutoPrepayAccounts.StartUpPosition	= 3;
            //rptAutoPrepayAccounts.SectionData	= "rptAutoPrepayAccounts.dsx":058A;
            //End Unmaped Properties
        }
    }
}
