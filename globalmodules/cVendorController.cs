﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace Global
{
	public class cVendorController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorMessage)
		{
			lngLastError = lngErrorNumber;
			strLastError = strErrorMessage;
		}

		public cVendor GetVendor(int lngID)
		{
			cVendor GetVendor = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cVendor vend = null;
				rsLoad.OpenRecordset("select * from VendorMaster where id = " + FCConvert.ToString(lngID), "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					vend = new cVendor();
					vend.CheckAddress1 = FCConvert.ToString(rsLoad.Get_Fields_String("CheckAddress1"));
					vend.CheckAddress2 = FCConvert.ToString(rsLoad.Get_Fields_String("CheckAddress2"));
					vend.CheckAddress3 = FCConvert.ToString(rsLoad.Get_Fields_String("CheckAddress3"));
					vend.CheckAddress4 = FCConvert.ToString(rsLoad.Get_Fields_String("CheckAddress4"));
					vend.CheckName = FCConvert.ToString(rsLoad.Get_Fields_String("CheckName"));
					vend.CheckCity = FCConvert.ToString(rsLoad.Get_Fields_String("CheckCity"));
					vend.CheckZip = FCConvert.ToString(rsLoad.Get_Fields_String("CheckZip"));
					vend.CheckZip4 = FCConvert.ToString(rsLoad.Get_Fields_String("CheckZip4"));
					vend.CheckState = FCConvert.ToString(rsLoad.Get_Fields_String("CheckState"));
					vend.EMail = FCConvert.ToString(rsLoad.Get_Fields_String("Email"));
					vend.VendorNumber = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("VendorNumber"));
					// TODO: Check the table for the column [1099] and replace with corresponding Get_Field method
					vend.Ten99 = FCConvert.ToBoolean(rsLoad.Get_Fields("1099"));
					vend.Status = FCConvert.ToString(rsLoad.Get_Fields_String("Status"));
					vend.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					vend.IsUpdated = false;
				}
				GetVendor = vend;
				return GetVendor;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetVendor;
		}

		public cVendor GetVendorByVendorNumber(int lngVendorNumber)
		{
			cVendor GetVendorByVendorNumber = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cVendor vend = null;
				rsLoad.OpenRecordset("select * from VendorMaster where VendorNumber = " + FCConvert.ToString(lngVendorNumber), "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					vend = new cVendor();
					vend.CheckAddress1 = FCConvert.ToString(rsLoad.Get_Fields_String("CheckAddress1"));
					vend.CheckAddress2 = FCConvert.ToString(rsLoad.Get_Fields_String("CheckAddress2"));
					vend.CheckAddress3 = FCConvert.ToString(rsLoad.Get_Fields_String("CheckAddress3"));
					vend.CheckAddress4 = FCConvert.ToString(rsLoad.Get_Fields_String("CheckAddress4"));
					vend.CheckName = FCConvert.ToString(rsLoad.Get_Fields_String("CheckName"));
					vend.CheckCity = FCConvert.ToString(rsLoad.Get_Fields_String("CheckCity"));
					vend.CheckZip = FCConvert.ToString(rsLoad.Get_Fields_String("CheckZip"));
					vend.CheckZip4 = FCConvert.ToString(rsLoad.Get_Fields_String("CheckZip4"));
					vend.CheckState = FCConvert.ToString(rsLoad.Get_Fields_String("CheckState"));
					vend.EMail = FCConvert.ToString(rsLoad.Get_Fields_String("Email"));
					vend.VendorNumber = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("VendorNumber"));
					// TODO: Check the table for the column [1099] and replace with corresponding Get_Field method
					vend.Ten99 = FCConvert.ToBoolean(rsLoad.Get_Fields("1099"));
					vend.Status = FCConvert.ToString(rsLoad.Get_Fields_String("Status"));
					vend.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					vend.IsUpdated = false;
				}
				GetVendorByVendorNumber = vend;
				return GetVendorByVendorNumber;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetVendorByVendorNumber;
		}
	}
}
