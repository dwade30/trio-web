﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmQuestion.
	/// </summary>
	partial class frmQuestion : BaseForm
	{
		public fecherFoundation.FCComboBox cmbQuestion;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmQuestion));
			this.cmbQuestion = new fecherFoundation.FCComboBox();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 158);
			this.BottomPanel.Size = new System.Drawing.Size(535, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbQuestion);
			this.ClientArea.Size = new System.Drawing.Size(535, 98);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(535, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// cmbQuestion
			// 
			this.cmbQuestion.AutoSize = false;
			this.cmbQuestion.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbQuestion.FormattingEnabled = true;
			this.cmbQuestion.Location = new System.Drawing.Point(30, 30);
			this.cmbQuestion.Name = "cmbQuestion";
			this.cmbQuestion.Size = new System.Drawing.Size(440, 40);
			this.cmbQuestion.TabIndex = 1;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(179, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(120, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Save";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmQuestion
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(489, 266);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmQuestion";
			this.Text = " ";
			this.Load += new System.EventHandler(this.frmQuestion_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmQuestion_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmQuestion_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcess;
	}
}
