﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmInterestedParties.
	/// </summary>
	partial class frmInterestedParties : BaseForm
	{
		public fecherFoundation.FCGrid GridDelete;
		public fecherFoundation.FCGrid Grid;
		public fecherFoundation.FCHeader lblAccount;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInterestedParties));
			this.GridDelete = new fecherFoundation.FCGrid();
			this.Grid = new fecherFoundation.FCGrid();
			this.lblAccount = new fecherFoundation.FCHeader();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcess = new fecherFoundation.FCButton();
			this.cmdAddRecipient = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddRecipient)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 348);
			this.BottomPanel.Size = new System.Drawing.Size(789, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.GridDelete);
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Size = new System.Drawing.Size(789, 288);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdAddRecipient);
			this.TopPanel.Controls.Add(this.lblAccount);
			this.TopPanel.Size = new System.Drawing.Size(789, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.lblAccount, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddRecipient, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(101, 30);
			this.HeaderText.Text = "Account";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// GridDelete
			// 
			this.GridDelete.AllowSelection = false;
			this.GridDelete.AllowUserToResizeColumns = false;
			this.GridDelete.AllowUserToResizeRows = false;
			this.GridDelete.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDelete.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDelete.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDelete.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.BackColorSel = System.Drawing.Color.Empty;
			this.GridDelete.Cols = 1;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDelete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridDelete.ColumnHeadersHeight = 30;
			this.GridDelete.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridDelete.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDelete.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridDelete.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDelete.FixedCols = 0;
			this.GridDelete.FixedRows = 0;
			this.GridDelete.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.FrozenCols = 0;
			this.GridDelete.GridColor = System.Drawing.Color.Empty;
			this.GridDelete.Location = new System.Drawing.Point(271, 3);
			this.GridDelete.Name = "GridDelete";
			this.GridDelete.ReadOnly = true;
			this.GridDelete.RowHeadersVisible = false;
			this.GridDelete.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDelete.RowHeightMin = 0;
			this.GridDelete.Rows = 0;
			this.GridDelete.ShowColumnVisibilityMenu = false;
			this.GridDelete.Size = new System.Drawing.Size(64, 21);
			this.GridDelete.StandardTab = true;
			this.GridDelete.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDelete.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.GridDelete, null);
			this.GridDelete.Visible = false;
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.Cols = 11;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle4;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.FixedCols = 0;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(30, 45);
			this.Grid.Name = "Grid";
			this.Grid.ReadOnly = true;
			this.Grid.RowHeadersVisible = false;
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 1;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.Size = new System.Drawing.Size(748, 200);
			this.Grid.StandardTab = true;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.Grid, "Double-click on an entry to view/edit the address");
			this.Grid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_AfterEdit);
			this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.grid_KeyDown);
			this.Grid.Click += new System.EventHandler(this.Grid_Click);
			this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
			// 
			// lblAccount
			// 
			this.lblAccount.AppearanceKey = "Header";
			this.lblAccount.Font = new System.Drawing.Font("@header", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblAccount.Location = new System.Drawing.Point(137, 26);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(174, 30);
			this.lblAccount.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.lblAccount, null);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(347, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(95, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Save";
			this.ToolTip1.SetToolTip(this.btnProcess, null);
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// cmdAddRecipient
			// 
			this.cmdAddRecipient.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddRecipient.AppearanceKey = "toolbarButton";
			this.cmdAddRecipient.Location = new System.Drawing.Point(661, 28);
			this.cmdAddRecipient.Name = "cmdAddRecipient";
			this.cmdAddRecipient.Shortcut = Wisej.Web.Shortcut.F11;
			this.cmdAddRecipient.Size = new System.Drawing.Size(102, 24);
			this.cmdAddRecipient.TabIndex = 53;
			this.cmdAddRecipient.Text = "Add Recipient";
			this.ToolTip1.SetToolTip(this.cmdAddRecipient, null);
			this.cmdAddRecipient.Click += new System.EventHandler(this.cmdAddRecipient_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(603, 28);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Shortcut = Wisej.Web.Shortcut.F11;
			this.cmdDelete.Size = new System.Drawing.Size(54, 24);
			this.cmdDelete.TabIndex = 54;
			this.cmdDelete.Text = "Delete";
			this.ToolTip1.SetToolTip(this.cmdDelete, null);
			this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
			// 
			// frmInterestedParties
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(789, 456);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmInterestedParties";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Interested Parties";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmInterestedParties_Load);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmInterestedParties_KeyDown);
			this.Resize += new System.EventHandler(this.frmInterestedParties_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddRecipient)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
		public FCButton cmdAddRecipient;
		public FCButton cmdDelete;
	}
}
