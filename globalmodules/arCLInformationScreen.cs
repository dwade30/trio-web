﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arCLInformationScreen : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/01/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/30/2004              *
		// ********************************************************
		int lngRow;
		bool boolFirstTime;
		int lngMaxRow;
		int lngMaxCol;

		public arCLInformationScreen()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Information";
		}

		public static arCLInformationScreen InstancePtr
		{
			get
			{
				return (arCLInformationScreen)Sys.GetInstance(typeof(arCLInformationScreen));
			}
		}

		protected arCLInformationScreen _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public void Init(int lngPassRW, int lngPassCL)
		{
			lngMaxRow = lngPassRW;
			lngMaxCol = lngPassCL;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (boolFirstTime)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
				boolFirstTime = true;
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			SetHeaderString();
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			// this will print the row from the grid
			if (frmRECLStatus.InstancePtr.vsRateInfo.Cols > 0)
			{
				fldOut10.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 0);
				fldOut11.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 0);
				fldOut12.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 0);
				fldOut13.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 0);
				fldOut14.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 0);
				fldOut15.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 0);
				fldOut16.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 0);
				fldOut17.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 0);
				fldOut18.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 0);
				fldOut19.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 0);
				fldOut110.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 0);
				fldOut111.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 0);
				fldOut112.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 0);
				fldOut113.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 0);
				fldOut114.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 0);
				fldOut115.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 0);
				fldOut116.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 0);
				fldOut117.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 0);
				fldOut118.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 0);
				if (frmRECLStatus.InstancePtr.vsRateInfo.Rows > 19)
				{
					fldOut119.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 0);
				}
				// fldOut120.Text = .TextMatrix(20, 0)
				// fldOut121.Text = .TextMatrix(21, 0)
			}
			if (frmRECLStatus.InstancePtr.vsRateInfo.Cols > 1)
			{
				fldOut20.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 1);
				fldOut21.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 1);
				fldOut22.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 1);
				fldOut23.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 1);
				fldOut24.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1);
				fldOut25.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 1);
				fldOut26.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 1);
				fldOut27.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 1);
				fldOut28.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 1);
				fldOut29.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 1);
				fldOut210.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 1);
				fldOut211.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 1);
				fldOut212.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 1);
				fldOut213.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1);
				fldOut214.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 1);
				fldOut215.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1);
				fldOut216.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1);
				fldOut217.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 1);
				fldOut218.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 1);
				if (frmRECLStatus.InstancePtr.vsRateInfo.Rows > 19)
				{
					fldOut219.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1);
				}
				// fldOut220.Text = .TextMatrix(20, 1)
				// fldOut221.Text = .TextMatrix(21, 1)
			}
			if (frmRECLStatus.InstancePtr.vsRateInfo.Cols > 3)
			{
				fldOut30.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 3);
				fldOut31.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 3);
				fldOut32.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 3);
				fldOut33.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 3);
				fldOut34.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 3);
				fldOut35.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 3);
				fldOut36.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 3);
				fldOut37.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 3);
				fldOut38.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 3);
				fldOut39.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 3);
				fldOut310.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 3);
				fldOut311.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 3);
				fldOut312.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 3);
				fldOut313.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 3);
				fldOut314.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 3);
				fldOut315.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 3);
				fldOut316.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 3);
				fldOut317.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 3);
				fldOut318.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 3);
				// MAL@20071113: Removed call to Comments because the info spans multiple columns
				// If .rows > 19 Then
				// fldOut319.Text = .TextMatrix(19, 3)
				// End If
				// fldOut320.Text = .TextMatrix(20, 3)
				// fldOut321.Text = .TextMatrix(21, 3)
			}
			if (frmRECLStatus.InstancePtr.vsRateInfo.Cols > 4)
			{
				fldOut40.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 4);
				fldOut41.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 4);
				fldOut42.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 4);
				fldOut43.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 4);
				fldOut44.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 4);
				fldOut45.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 4);
				fldOut46.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 4);
				fldOut47.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 4);
				fldOut48.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 4);
				fldOut49.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 4);
				fldOut410.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 4);
				fldOut411.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 4);
				fldOut412.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 4);
				fldOut413.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 4);
				fldOut414.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 4);
				fldOut415.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 4);
				fldOut416.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 4);
				fldOut417.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 4);
				fldOut418.Text = frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 4);
				// MAL@20071113
				// If .rows > 19 Then
				// fldOut419.Text = .TextMatrix(19, 4)
				// End If
				// fldOut420.Text = .TextMatrix(20, 4)
				// fldOut421.Text = .TextMatrix(21, 4)
			}
		}

		private int FindNextVisibleRow(ref int lngR)
		{
			int FindNextVisibleRow = 0;
			// this function will start at the row number passed in and return the next row to print
			// this function will return -1 if there are no more rows to show
			int lngNext;
			int intLvl/*unused?*/;
			bool boolFound;
			int lngMax = 0;
			boolFound = false;
			lngNext = lngR + 1;
			lngMax = frmRECLStatus.InstancePtr.GRID.Rows;
			if (lngNext < lngMax)
			{
				boolFound = false;
				while (!(boolFound || frmRECLStatus.InstancePtr.GRID.RowOutlineLevel(lngNext) < 2))
				{
					if (frmRECLStatus.InstancePtr.GRID.IsCollapsed(LastParentRow(lngNext)) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						lngNext += 1;
						boolFound = false;
						if (lngNext >= lngMax)
						{
							return FindNextVisibleRow;
						}
					}
					else
					{
						boolFound = true;
					}
				}
				FindNextVisibleRow = lngNext;
				// If .IsCollapsed(lngNext) = flexOutlineCollapsed Then
				// intLvl = .RowOutlineLevel(lngNext)
				// Do
				// lngNext = lngNext + 1
				// If lngNext < lngMax Then
				// If intLvl = .RowOutlineLevel(lngNext) Then
				// FindNextVisibleRow = lngNext
				// boolFound = True
				// End If
				// End If
				// Loop Until boolFound Or lngNext >= lngMax
				// Else
				// FindNextVisibleRow = lngNext
				// End If
			}
			if (FindNextVisibleRow == 0)
				FindNextVisibleRow = -1;
			return FindNextVisibleRow;
		}
		// VBto upgrade warning: CurRow As Variant --> As int
		public int LastParentRow(int CurRow)
		{
			int LastParentRow = 0;
			int l;
			int curLvl;
			l = CurRow;
			curLvl = frmRECLStatus.InstancePtr.GRID.RowOutlineLevel(l);
			l -= 1;
			while (frmRECLStatus.InstancePtr.GRID.RowOutlineLevel(l) > 1)
			{
				l -= 1;
			}
			LastParentRow = l;
			return LastParentRow;
		}

		private void SetHeaderString()
		{
			// this will set the correct header for this account
			string strTemp = "";
			int lngAcctNum = 0;
			if (modStatusPayments.Statics.boolRE)
			{
				// determine if this is a Real Estate or Personal Property Account
				strTemp = "RE";
				lngAcctNum = StaticSettings.TaxCollectionValues.LastAccountRE;
			}
			else
			{
				strTemp = "PP";
				lngAcctNum = StaticSettings.TaxCollectionValues.LastAccountPP;
			}
			strTemp += " Account " + FCConvert.ToString(lngAcctNum) + " " + frmRECLStatus.InstancePtr.fraRateInfo.Text;
			strTemp += "\r\n" + "as of " + Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy");
			lblHeader.Text = strTemp;
			lblName.Text = "Name: " + frmRECLStatus.InstancePtr.lblOwnersName.Text;
			lblLocation.Text = "Location: " + frmRECLStatus.InstancePtr.lblLocation.Text;
			// lblMapLot.Text = "Map/Lot: " & frmRECLStatus.lblMapLot.Text
			// lblAcreage.Text = "Acreage: " & frmRECLStatus.lblAcreage.Text
			lblBookPage.Text = "Book Page: " + frmRECLStatus.InstancePtr.lblBookPage.Text;
			lblRef1.Text = "Ref1:";
			lblRef1Val.Text = frmRECLStatus.InstancePtr.lblReference1.Text;
		}
		// Private Sub CreatePerDiemTable()
		// Dim lngRW               As Long
		//
		// With frmRECLStatus.GRID
		// For lngRW = 1 To .rows - 1
		// If Trim(.TextMatrix(lngRW, 1)) <> "" Then
		// strLastYearText = Trim(.TextMatrix(lngRW, 1))
		// End If
		// If Val(.TextMatrix(lngRW, 13)) <> 0 Then
		// AddPerDiem lngRW
		// End If
		// Next
		// End With
		// End Sub
		// Private Sub AddPerDiem(lngRNum As Long)
		// Dim obNew               As Object
		// Dim strTemp             As String
		//
		// this will add another per diem line in the report footer
		// If lngPDNumber = 0 Then
		// lngPDNumber = 1
		//
		// fldPerDiem1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
		// fldPerDiem1.Text = Format(Val(frmRECLStatus.GRID.TextMatrix(lngRNum, 13)), "0.0000")
		//
		// If Trim(frmRECLStatus.GRID.TextMatrix(lngRNum, 1)) = "" Then
		// lblPerDiem1.Text = frmRECLStatus.GRID.TextMatrix(frmRECLStatus.LastParentRow(lngRNum), 1)
		// Else
		// lblPerDiem1.Text = frmRECLStatus.GRID.TextMatrix(lngRNum, 1)
		// End If
		//
		// dblPDTotal = dblPDTotal + Val(frmRECLStatus.GRID.TextMatrix(lngRNum, 13))
		// Else
		// increment the number of fields
		// lngPDNumber = lngPDNumber + 1
		// add a field
		// Set obNew = ReportFooter.Controls.Add("DDActiveReports2.Field")
		//
		// obNew.Name = "fldPerDiem" & lngPDNumber
		// obNew.Top = fldPerDiem1.Top + ((lngPDNumber - 1) * fldPerDiem1.Height)
		// obNew.Left = fldPerDiem1.Left
		// obNew.Width = fldPerDiem1.Width
		// obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
		// strTemp = obNew.Font
		// obNew.Font = fldPerDiem1.Font                       'this sets the font to the same as the field that is already created
		//
		// obNew.Text = Format(Val(frmRECLStatus.GRID.TextMatrix(lngRNum, 13)), "0.0000")
		// dblPDTotal = dblPDTotal + Val(frmRECLStatus.GRID.TextMatrix(lngRNum, 13))
		//
		// add a label
		// Set obNew = ReportFooter.Controls.Add("DDActiveReports2.Label")
		//
		// obNew.Name = "lblPerDiem" & lngPDNumber
		// obNew.Top = lblPerDiem1.Top + ((lngPDNumber - 1) * lblPerDiem1.Height)
		// obNew.Left = lblPerDiem1.Left
		// strTemp = obNew.Font
		// obNew.Font = fldPerDiem1.Font                       'this sets the font to the same as the field that is already created
		//
		// If Trim(frmRECLStatus.GRID.TextMatrix(lngRNum, 1)) = "" Then
		// obNew.Text = frmRECLStatus.GRID.TextMatrix(frmRECLStatus.LastParentRow(lngRNum), 1)
		// Else
		// obNew.Text = frmRECLStatus.GRID.TextMatrix(lngRNum, 1)
		// End If
		// End If
		// boolPerDiem = True
		// End Sub
		//
		// Private Sub ReportFooter_Format(object sender, EventArgs e)
		// Dim obNew               As Object
		//
		// If boolPerDiem Then
		// add a field
		// Set obNew = ReportFooter.Controls.Add("DDActiveReports2.Field")
		//
		// obNew.Name = "fldPerDiemTotal"
		// obNew.Top = fldPerDiem1.Top + (lngPDNumber * fldPerDiem1.Height)
		// obNew.Left = fldPerDiem1.Left
		// obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right
		// obNew.Width = fldPerDiem1.Width
		// obNew.Text = Format(dblPDTotal, "0.0000")
		//
		// add a label
		// Set obNew = ReportFooter.Controls.Add("DDActiveReports2.Label")
		//
		// obNew.Name = "lblPerDiemTotal"
		// obNew.Top = lblPerDiem1.Top + (lngPDNumber * lblPerDiem1.Height)
		// obNew.Left = lblPerDiem1.Left
		//
		// obNew.Text = "Total"
		//
		// add a line
		// Set obNew = ReportFooter.Controls.Add("DDActiveReports2.Line")
		//
		// obNew.Name = "lnPerDiemTotal"
		// obNew.X1 = fldPerDiem1.Left
		// obNew.X2 = fldPerDiem1.Left + fldPerDiem1.Width
		// obNew.Y1 = lblPerDiem1.Top + (lngPDNumber * lblPerDiem1.Height)
		// obNew.Y2 = obNew.Y1
		// Else
		// lblPerDiem1.Visible = False
		// lblPerDiemHeader.Visible = False
		// fldPerDiem1.Visible = False
		// Line3.Visible = False
		// End If
		// End Sub
		private void arCLInformationScreen_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arCLInformationScreen.Text	= "Account Information";
			//arCLInformationScreen.Icon	= "arCLInformationScreen.dsx":0000";
			//arCLInformationScreen.Left	= 0;
			//arCLInformationScreen.Top	= 0;
			//arCLInformationScreen.Width	= 11880;
			//arCLInformationScreen.Height	= 8595;
			//arCLInformationScreen.StartUpPosition	= 3;
			//arCLInformationScreen.SectionData	= "arCLInformationScreen.dsx":058A;
			//End Unmaped Properties
		}
	}
}
