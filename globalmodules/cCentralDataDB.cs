﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using TWSharedLibrary;

namespace Global
{
	public class cCentralDataDB
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cVersionInfo GetVersion()
		{
			cVersionInfo GetVersion = null;
			try
			{
				// On Error GoTo ErrorHandler
				ClearErrors();
				cVersionInfo tVer = new cVersionInfo();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from dbversion", "CentralData");
				if (!rsLoad.EndOfFile())
				{
					tVer.Build = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Build"));
					tVer.Major = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Major"));
					tVer.Minor = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Minor"));
					tVer.Revision = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Revision"));
				}
				GetVersion = tVer;
				return GetVersion;
			}
			catch (Exception ex)
			{
				strLastError = Information.Err(ex).Description;
				lngLastError = Information.Err(ex).Number;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetVersion;
		}

		public void SetVersion(ref cVersionInfo nVersion)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (!(nVersion == null))
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from dbversion", "CentralData");
					if (rsSave.EndOfFile())
					{
						rsSave.AddNew();
					}
					else
					{
						rsSave.Edit();
					}
					rsSave.Set_Fields("Major", nVersion.Major);
					rsSave.Set_Fields("Minor", nVersion.Minor);
					rsSave.Set_Fields("Revision", nVersion.Revision);
					rsSave.Set_Fields("Build", nVersion.Build);
					rsSave.Update();
				}
				return;
			}
			catch (Exception ex)
			{
				strLastError = Information.Err(ex).Description;
				lngLastError = Information.Err(ex).Number;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		public bool CheckVersion()
		{
			bool CheckVersion = false;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cVersionInfo nVer = new cVersionInfo();
				cVersionInfo tVer = new cVersionInfo();
				cVersionInfo cVer;
				bool boolNeedUpdate;
				cVer = GetVersion();
				if (cVer == null)
				{
					cVer = new cVersionInfo();
				}
				nVer.Major = 1;
				// default to 1.0.0.0
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddSettingsTable())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 2;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddLogTable())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 3;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddImportedPayments())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 4;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (UpdateImportedPayments())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 5;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddPayportSettings())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return false;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 6;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (RemoveUnneededPayportSettings())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return false;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 7;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (UpdatePayportSettingsOwnerInfo())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return false;
					}
				}

				CheckVersion = true;
				return CheckVersion;
			}
			catch (Exception ex)
			{
				strLastError = Information.Err(ex).Description;
				lngLastError = Information.Err(ex).Number;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return CheckVersion;
		}

		private bool FieldExists(string strTable, string strField, string strDB)
		{
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			try
			{
				strSQL = "Select column_name from information_schema.columns where column_name = '" + strField + "' and table_name = '" + strTable + "'";
				rsLoad.OpenRecordset(strSQL, strDB);

				if (!rsLoad.EndOfFile())
				{
					return true;
				}
				return false;
			}
			catch (Exception ex)
			{
                strLastError = Information.Err(ex).Description;
                lngLastError = Information.Err(ex).Number;
				StaticSettings.GlobalTelemetryService.TrackException(ex);
				return false;
			}
		}

		private bool UpdatePayportSettingsOwnerInfo()
		{
			try
			{
				clsDRWrapper rsUpdate = new clsDRWrapper();

				rsUpdate.OpenRecordset("UPDATE Settings SET Owner = '', OwnerType = '' WHERE SettingType = 'PayPortExport' AND SettingName = 'SyncRE'", "CentralData");
				rsUpdate.OpenRecordset("UPDATE Settings SET Owner = '', OwnerType = '' WHERE SettingType = 'PayPortExport' AND SettingName = 'SyncUB'", "CentralData");
				rsUpdate.OpenRecordset("UPDATE Settings SET Owner = '', OwnerType = '' WHERE SettingType = 'PayPortExport' AND SettingName = 'CombineUBBills'", "CentralData");
				rsUpdate.OpenRecordset("UPDATE Settings SET Owner = '', OwnerType = '' WHERE SettingType = 'PayPortExport' AND SettingName = 'CombinedUBBillType'", "CentralData");
				rsUpdate.OpenRecordset("UPDATE Settings SET Owner = '', OwnerType = '' WHERE SettingType = 'PayPortExport' AND SettingName = 'GeoCode'", "CentralData");
				rsUpdate.OpenRecordset("UPDATE Settings SET Owner = '', OwnerType = '' WHERE SettingType = 'PayPortExport' AND SettingName = 'SyncPP'", "CentralData");

				return true;
			}
			catch (Exception ex)
			{
                strLastError = Information.Err(ex).Description;
                lngLastError = Information.Err(ex).Number;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return false;
			}
		}

		private bool RemoveUnneededPayportSettings()
		{
			try
			{
				clsDRWrapper rsUpdate = new clsDRWrapper();

				rsUpdate.Execute("DELETE FROM Settings WHERE SettingType = 'PayPortExport' AND SettingName = 'UploadURL'", "CentralData");
				rsUpdate.Execute("DELETE FROM Settings WHERE SettingType = 'PayPortExport' AND SettingName = 'DownloadURL'", "CentralData");
				rsUpdate.Execute("DELETE FROM Settings WHERE SettingType = 'PayPortExport' AND SettingName = 'Username'", "CentralData");
				rsUpdate.Execute("DELETE FROM Settings WHERE SettingType = 'PayPortExport' AND SettingName = 'Password'", "CentralData");

				return true;
			}
			catch (Exception ex)
			{
                strLastError = Information.Err(ex).Description;
                lngLastError = Information.Err(ex).Number;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return false;
			}
		}

		private bool AddPayportSettings()
		{
			try
			{
				clsDRWrapper rsUpdate = new clsDRWrapper();

				rsUpdate.OpenRecordset("SELECT * FROM Settings WHERE SettingType = 'PayPortExport' AND SettingName = 'SyncRE'", "CentralData");
				if (rsUpdate.EndOfFile() || rsUpdate.BeginningOfFile())
				{
					rsUpdate.Execute("INSERT INTO Settings (SettingType, SettingName, SettingValue) VALUES ('PayPortExport', 'SyncRE', 'False')", "CentralData");
				}

				rsUpdate.OpenRecordset("SELECT * FROM Settings WHERE SettingType = 'PayPortExport' AND SettingName = 'SyncPP'", "CentralData");
				if (rsUpdate.EndOfFile() || rsUpdate.BeginningOfFile())
				{
					rsUpdate.Execute("INSERT INTO Settings (SettingType, SettingName, SettingValue) VALUES ('PayPortExport', 'SyncPP', 'False')", "CentralData");
				}

				rsUpdate.OpenRecordset("SELECT * FROM Settings WHERE SettingType = 'PayPortExport' AND SettingName = 'SyncUB'", "CentralData");
				if (rsUpdate.EndOfFile() || rsUpdate.BeginningOfFile())
				{
					rsUpdate.Execute("INSERT INTO Settings (SettingType, SettingName, SettingValue) VALUES ('PayPortExport', 'SyncUB', 'False')", "CentralData");
				}

				rsUpdate.OpenRecordset("SELECT * FROM Settings WHERE SettingType = 'PayPortExport' AND SettingName = 'CombineUBBills'", "CentralData");
				if (rsUpdate.EndOfFile() || rsUpdate.BeginningOfFile())
				{
					rsUpdate.Execute("INSERT INTO Settings (SettingType, SettingName, SettingValue) VALUES ('PayPortExport', 'CombineUBBills', 'False')", "CentralData");
				}

				rsUpdate.OpenRecordset("SELECT * FROM Settings WHERE SettingType = 'PayPortExport' AND SettingName = 'CombinedUBBillType'", "CentralData");
				if (rsUpdate.EndOfFile() || rsUpdate.BeginningOfFile())
				{
					rsUpdate.Execute("INSERT INTO Settings (SettingType, SettingName, SettingValue) VALUES ('PayPortExport', 'CombinedUBBillType', 'Utility')", "CentralData");
				}

				rsUpdate.OpenRecordset("SELECT * FROM Settings WHERE SettingType = 'PayPortExport' AND SettingName = 'GeoCode'", "CentralData");
				if (rsUpdate.EndOfFile() || rsUpdate.BeginningOfFile())
				{
					rsUpdate.Execute("INSERT INTO Settings (SettingType, SettingName, SettingValue) VALUES ('PayPortExport', 'GeoCode', '')", "CentralData");
				}

				rsUpdate.OpenRecordset("SELECT * FROM Settings WHERE SettingType = 'PayPortExport' AND SettingName = 'UploadURL'", "CentralData");
				if (rsUpdate.EndOfFile() || rsUpdate.BeginningOfFile())
				{
					rsUpdate.Execute("INSERT INTO Settings (SettingType, SettingName, SettingValue) VALUES ('PayPortExport', 'UploadURL', '')", "CentralData");
				}

				rsUpdate.OpenRecordset("SELECT * FROM Settings WHERE SettingType = 'PayPortExport' AND SettingName = 'DownloadURL'", "CentralData");
				if (rsUpdate.EndOfFile() || rsUpdate.BeginningOfFile())
				{
					rsUpdate.Execute("INSERT INTO Settings (SettingType, SettingName, SettingValue) VALUES ('PayPortExport', 'DownloadURL', '')", "CentralData");
				}

				rsUpdate.OpenRecordset("SELECT * FROM Settings WHERE SettingType = 'PayPortExport' AND SettingName = 'Username'", "CentralData");
				if (rsUpdate.EndOfFile() || rsUpdate.BeginningOfFile())
				{
					rsUpdate.Execute("INSERT INTO Settings (SettingType, SettingName, SettingValue) VALUES ('PayPortExport', 'Username', '')", "CentralData");
				}

				rsUpdate.OpenRecordset("SELECT * FROM Settings WHERE SettingType = 'PayPortExport' AND SettingName = 'Password'", "CentralData");
				if (rsUpdate.EndOfFile() || rsUpdate.BeginningOfFile())
				{
					rsUpdate.Execute("INSERT INTO Settings (SettingType, SettingName, SettingValue) VALUES ('PayPortExport', 'Password', '')", "CentralData");
				}

				return true;
			}
			catch (Exception ex)
			{
                strLastError = Information.Err(ex).Description;
                lngLastError = Information.Err(ex).Number;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return false;
			}
		}

		private bool UpdateImportedPayments()
		{
			try
			{
				clsDRWrapper rsUpdate = new clsDRWrapper();
				if (!FieldExists("ImportedPayments", "Processed", "CentralData"))
				{
					rsUpdate.Execute("Alter Table.[dbo].[ImportedPayments] Add [Processed] bit NULL", "CentralData");
					rsUpdate.Execute("Update ImportedPayments SET Processed = 1 WHERE AppliedId > 0", "CentralData");
					rsUpdate.Execute("Alter Table.[dbo].[ImportedPayments] DROP COLUMN AppliedId", "CentralData");
				}

				return true;
			}
			catch (Exception ex)
			{
                strLastError = Information.Err(ex).Description;
                lngLastError = Information.Err(ex).Number;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return false;
			}
		}

		private bool AddImportedPayments()
		{
			string strSql;
			clsDRWrapper rsExec = new clsDRWrapper();

			try
			{
				strSql = GetTableCreationHeaderSQL("ImportedPayments");
				strSql = strSql + "[BillId] [int] NOT NULL,";
				strSql = strSql + "[BillType] [varchar] (25) NOT NULL,";
				strSql = strSql + "[BillNumber] [varchar] (25) NOT NULL,";
				strSql = strSql + "[AccountNumber] [int] NOT NULL,";
				strSql = strSql + "[PaymentDate] [DateTime] NOT NULL,";
				strSql = strSql + "[PaymentAmount] [Decimal] (18,2) NOT NULL,";
				strSql = strSql + "[PaidBy] [varchar] (255) NOT NULL,";
				strSql = strSql + "[AppliedDateTime] [DateTime] NULL,";
				strSql = strSql + "[AppliedId] [int] NOT NULL,";
				strSql = strSql + "[PaymentIdentifier] [nvarchar] (64) NULL,";
				strSql = strSql + GetTablePKSql("ImportedPayments");

				rsExec.Execute(strSql, "CentralData");

				return true;
			}
			catch (Exception ex)
			{
                strLastError = Information.Err(ex).Description;
                lngLastError = Information.Err(ex).Number;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return false;
			}
			
		}

		private bool AddLogTable()
		{
			bool AddLogTable = false;
			string strSql;
			clsDRWrapper rsExec = new clsDRWrapper();
			strSql = "select * from information_schema.tables where table_name = N'ApplicationLog'";
			rsExec.OpenRecordset(strSql, "CentralData");
			if (rsExec.EndOfFile())
			{
				strSql = "CREATE TABLE [dbo].[ApplicationLog](" + "[ID] [int] IDENTITY(1,1) NOT NULL," + "[Entry] [nvarchar](MAX) NULL," + "[EntryTimeStamp] [datetime] NULL," + "[User] [nvarchar](50) NULL," + "[UserID] [int] NULL," + "[Origin] [nvarchar](50) NULL," + "CONSTRAINT [PK_ApplicationLog] PRIMARY KEY CLUSTERED " + "([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" + ") ON [PRIMARY]";
				rsExec.Execute(strSql, "CentralData");
			}
			AddLogTable = true;
			return AddLogTable;
		}

		private bool AddSettingsTable()
		{
			bool AddSettingsTable = false;
			cSettingsController contSet = new cSettingsController();
			AddSettingsTable = contSet.AddSettingsTable("CentralData");
			return AddSettingsTable;
		}

		private string GetTableCreationHeaderSQL(string strTableName)
		{
			string GetTableCreationHeaderSQL = "";
			string strSQL;
			strSQL = "if not exists (select * from information_schema.tables where table_name = N'" + strTableName + "') " + "\r\n";
			strSQL += "Create Table [dbo].[" + strTableName + "] (";
			strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
			GetTableCreationHeaderSQL = strSQL;
			return GetTableCreationHeaderSQL;
		}

		private string GetTablePKSql(string strTableName)
		{
			string GetTablePKSql = "";
			string strSQL;
			strSQL = " Constraint [PK_" + strTableName + "] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
			GetTablePKSql = strSQL;
			return GetTablePKSql;
		}
	}
}
