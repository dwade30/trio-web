﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;

namespace Global
{
	public class clsPostingJournalInfo
	{
		//=========================================================
		private int lngJournalNumber;
		// VBto upgrade warning: intPeriod As short --> As int	OnWrite(string)
		private int intPeriod;
		private string strType = string.Empty;
		private string strAPCheckDate = string.Empty;

		public int JournalNumber
		{
			set
			{
				lngJournalNumber = value;
			}
			get
			{
				int JournalNumber = 0;
				JournalNumber = lngJournalNumber;
				return JournalNumber;
			}
		}
		// VBto upgrade warning: intPer As string	OnReadFCConvert.ToInt32(
		public string Period
		{
			set
			{
				intPeriod = FCConvert.ToInt32(value);
			}
			// VBto upgrade warning: 'Return' As string	OnWriteFCConvert.ToInt32(
			get
			{
				string Period = "";
				Period = FCConvert.ToString(intPeriod);
				return Period;
			}
		}

		public string JournalType
		{
			set
			{
				strType = value;
			}
			get
			{
				string JournalType = "";
				JournalType = strType;
				return JournalType;
			}
		}

		public string CheckDate
		{
			set
			{
				strAPCheckDate = value;
			}
			get
			{
				string CheckDate = "";
				CheckDate = strAPCheckDate;
				return CheckDate;
			}
		}
	}
}
