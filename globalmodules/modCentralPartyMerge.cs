﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using SharedApplication.TaxCollections.AccountPayment;

namespace Global
{
	public class modCentralPartyMerge
	{
		//=========================================================
		public static void RemoveContactFromParty_2(int lngContactID)
		{
			RemoveContactFromParty(ref lngContactID);
		}

		public static void RemoveContactFromParty(ref int lngContactID)
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			rsUpdate.Execute("DELETE FROM ContactPhoneNumbers WHERE ContactID = " + FCConvert.ToString(lngContactID), "CentralParties");
			rsUpdate.Execute("DELETE FROM Contacts WHERE ID = " + FCConvert.ToString(lngContactID), "CentralParties");
		}
		// vbPorter upgrade warning: lngPartyID As int	OnWrite(string, int)

		public static void RemovePartiesPrimaryAddress(int partyID)
		{
			clsDRWrapper rsUpdate = new/*AsNew*/ clsDRWrapper();
			rsUpdate.Execute("Delete from addresses where PartyID = " + FCConvert.ToString(partyID) + " and AddressType = 'Primary'", "CentralParties");
		}

		public static void FlagPartyAsMerged(int lngPartyID)
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			rsUpdate.Execute("UPDATE Parties SET Merged = 1, AutoMerged = 1 WHERE ID = " + FCConvert.ToString(lngPartyID), "CentralPArties");
		}

		public static void FlagPartyAsAutoMerged(ref int lngPartyID)
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			rsUpdate.Execute("UPDATE Parties SET AutoMerged = 1 WHERE ID = " + FCConvert.ToString(lngPartyID), "CentralPArties");
		}

        public static void FixDuplicatePrimaryAddressIssue()
        {
            clsDRWrapper rsUpdate = new clsDRWrapper();
            clsDRWrapper rsParties = new clsDRWrapper();
            rsParties.OpenRecordset("SELECT PartyID FROM Addresses WHERE UPPER(AddressType) = 'PRIMARY' GROUP BY PartyID HAVING COUNT(PartyID) > 1", "CentralParties");
            if (!rsParties.EndOfFile() && !rsParties.BeginningOfFile())
            {
                do
                {
                    rsUpdate.OpenRecordset(
                        "SELECT * FROM Addresses WHERE PartyID = " + rsParties.Get_Fields_Int32("PartyId") +
                        " AND UPPER(AddressType) = 'PRIMARY'", "CentralParties");
                    if (!rsUpdate.EndOfFile() && !rsUpdate.BeginningOfFile())
                    {
                        rsUpdate.MoveNext();
                        while (!rsUpdate.EndOfFile())
                        {
							rsUpdate.Edit();
                            rsUpdate.Set_Fields("AddressType", "Override");
                            rsUpdate.Update();
                            rsUpdate.MoveNext();
						}
                    }

                    rsParties.MoveNext();
                } while (!rsParties.EndOfFile());
            }
        }

		public static void RenumberPhoneOrderForParty(int lngPartyID)
		{
			clsDRWrapper rsPhone = new clsDRWrapper();
			int intOrder;
			intOrder = 1;
			rsPhone.OpenRecordset("SELECT * FROM CentralPhoneNumbers WHERE PartyID = " + FCConvert.ToString(lngPartyID) + " ORDER BY PhoneOrder", "CentralParties");
			if (rsPhone.EndOfFile() != true && rsPhone.BeginningOfFile() != true)
			{
				do
				{
					rsPhone.Edit();
					rsPhone.Set_Fields("PhoneOrder", intOrder);
					rsPhone.Update();
					intOrder += 1;
					rsPhone.MoveNext();
				}
				while (rsPhone.EndOfFile() != true);
			}
		}

		public static void RemovePartyFromCentralParties_2(int lngPartyID)
		{
			RemovePartyFromCentralParties(lngPartyID);
		}

		public static void RemovePartyFromCentralParties(int lngPartyID)
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			clsDRWrapper rsPhone = new clsDRWrapper();
			rsUpdate.Execute("DELETE Addresses where PartyID = " + FCConvert.ToString(lngPartyID), "CentralParties");
			rsUpdate.Execute("DELETE CentralComments where PartyID = " + FCConvert.ToString(lngPartyID), "CentralParties");
			rsUpdate.Execute("DELETE CentralPhoneNumbers where PartyID = " + FCConvert.ToString(lngPartyID), "CentralParties");
			rsUpdate.OpenRecordset("SELECT * FROM Contacts WHERE PartyID = " + FCConvert.ToString(lngPartyID), "CentralParties");
			if (rsUpdate.EndOfFile() != true && rsUpdate.BeginningOfFile() != true)
			{
				do
				{
					rsPhone.Execute("DELETE FROM ContactPhoneNumbers WHERE ContactID = " + rsUpdate.Get_Fields_Int32("ID"), "CentralParties");
					rsUpdate.Delete();
					rsUpdate.Update();
					rsUpdate.MoveNext();
				}
				while (rsUpdate.EndOfFile() != true);
			}
			rsUpdate.Execute("DELETE Parties where ID = " + FCConvert.ToString(lngPartyID), "CentralParties");
		}

		public static void ChangePartyIDInCentralParties_6(int NewPartyID, int OriginalPartyID)
		{
			ChangePartyIDInCentralParties(NewPartyID, OriginalPartyID);
		}

		public static void ChangePartyIDInCentralParties(int NewPartyID, int OriginalPartyID)
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			clsDRWrapper rsPhone = new clsDRWrapper();
			int intOrder = 0;
			rsUpdate.Execute("Update Addresses set PartyID = " + FCConvert.ToString(NewPartyID) + " where PartyID = " + FCConvert.ToString(OriginalPartyID), "CentralParties");
			rsUpdate.Execute("Update CentralComments set PartyID = " + FCConvert.ToString(NewPartyID) + " where PartyID = " + FCConvert.ToString(OriginalPartyID), "CentralParties");
			rsUpdate.Execute("Update Contacts set PartyID = " + FCConvert.ToString(NewPartyID) + " where PartyID = " + FCConvert.ToString(OriginalPartyID), "CentralParties");
			rsPhone.OpenRecordset("SELECT * FROM CentralPhoneNumbers WHERE PartyID = " + FCConvert.ToString(NewPartyID) + " ORDER BY PhoneOrder DESC", "CentralParties");
			if (rsPhone.EndOfFile() != true && rsPhone.BeginningOfFile() != true)
			{
				intOrder = rsPhone.Get_Fields_Int32("PhoneOrder") + 1;
			}
			else
			{
				intOrder = 1;
			}
			rsPhone.OpenRecordset("SELECT * FROM CentralPhoneNumbers WHERE PartyID = " + FCConvert.ToString(OriginalPartyID) + " ORDER BY PhoneOrder", "CentralParties");
			if (rsPhone.EndOfFile() != true && rsPhone.BeginningOfFile() != true)
			{
				do
				{
					rsPhone.Edit();
					rsPhone.Set_Fields("PartyID", NewPartyID);
					rsPhone.Set_Fields("PhoneOrder", intOrder);
					rsPhone.Update();
					intOrder += 1;
					rsPhone.MoveNext();
				}
				while (rsPhone.EndOfFile() != true);
			}
		}

		public static void AddPartyIndexes()
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			if (modGlobalConstants.Statics.gboolMV)
			{
				rsUpdate.OpenRecordset("SELECT * FROM sys.indexes WHERE name = 'IX_FleetMaster_PartyID1' AND object_id = OBJECT_ID('dbo.FleetMaster')", "MotorVehicle");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_FleetMaster_PartyID1 ON dbo.FleetMaster (PartyID1) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_FleetMaster_PartyID2 ON dbo.FleetMaster (PartyID2) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_FleetMaster_PartyID3 ON dbo.FleetMaster (PartyID3) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_Master_PartyID1 ON dbo.Master (PartyID1) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_Master_PartyID2 ON dbo.Master (PartyID2) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_Master_PartyID3 ON dbo.Master (PartyID3) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_ArchiveMaster_PartyID1 ON dbo.ArchiveMaster (PartyID1) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_ArchiveMaster_PartyID2 ON dbo.ArchiveMaster (PartyID2) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_ArchiveMaster_PartyID3 ON dbo.ArchiveMaster (PartyID3) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_PendingActivityMaster_PartyID1 ON dbo.PendingActivityMaster (PartyID1) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_PendingActivityMaster_PartyID2 ON dbo.PendingActivityMaster (PartyID2) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_PendingActivityMaster_PartyID3 ON dbo.PendingActivityMaster (PartyID3) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_HeldRegistrationMaster_PartyID1 ON dbo.HeldRegistrationMaster (PartyID1) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_HeldRegistrationMaster_PartyID2 ON dbo.HeldRegistrationMaster (PartyID2) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_HeldRegistrationMaster_PartyID3 ON dbo.HeldRegistrationMaster (PartyID3) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_ActivityMaster_PartyID1 ON dbo.ActivityMaster (PartyID1) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_ActivityMaster_PartyID2 ON dbo.ActivityMaster (PartyID2) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_ActivityMaster_PartyID3 ON dbo.ActivityMaster (PartyID3) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_LongTermTrailerRegistrations_PartyID1 ON dbo.LongTermTrailerRegistrations (PartyID1) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_LongTermTrailerRegistrations_PartyID2 ON dbo.LongTermTrailerRegistrations (PartyID2) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_TransitPlates_PartyID ON dbo.TransitPlates (PartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_SpecialRegistration_PartyID ON dbo.SpecialRegistration (PartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "MotorVehicle");
				}
			}
			// Utility Billing
			if (modGlobalConstants.Statics.gboolUT)
			{
				rsUpdate.OpenRecordset("SELECT * FROM sys.indexes WHERE name = 'IX_master_BillingPartyID' AND object_id = OBJECT_ID('dbo.master')", "UtilityBilling");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_master_BillingPartyID ON dbo.master (BillingPartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "UtilityBilling");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_master_SecondBillingPartyID ON dbo.master (SecondBillingPartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "UtilityBilling");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_master_OwnerPartyID ON dbo.master (OwnerPartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "UtilityBilling");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_master_SecondOwnerPartyID ON dbo.master (SecondOwnerPartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "UtilityBilling");
				}
			}
			// Real Estate
			if (modGlobalConstants.Statics.gboolRE)
			{
				rsUpdate.OpenRecordset("SELECT * FROM sys.indexes WHERE name = 'IX_master_OwnerPartyID' AND object_id = OBJECT_ID('dbo.master')", "RealEstate");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_master_OwnerPartyID ON dbo.master (OwnerPartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "RealEstate");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_master_SecOwnerPartyID ON dbo.master (SecOwnerPartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "RealEstate");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_Owners_PartyID ON dbo.Owners (PartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "RealEstate");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_PreviousOwner_PartyID1 ON dbo.PreviousOwner (PartyID1) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "RealEstate");
				}
			}
			// Accounts Receivable
			if (modGlobalConstants.Statics.gboolAR)
			{
				rsUpdate.OpenRecordset("SELECT * FROM sys.indexes WHERE name = 'IX_CustomerMaster_PartyID' AND object_id = OBJECT_ID('dbo.CustomerMaster')", "AccountsReceivable");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_CustomerMaster_PartyID ON dbo.CustomerMaster (PartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "AccountsReceivable");
				}
			}
			// Clerk
			if (modGlobalConstants.Statics.gboolCK)
			{
				rsUpdate.OpenRecordset("SELECT * FROM sys.indexes WHERE name = 'IX_DogOwner_PartyID' AND object_id = OBJECT_ID('dbo.DogOwner')", "Clerk");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_DogOwner_PartyID ON dbo.DogOwner (PartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "Clerk");
				}
			}
			// Personal Property
			if (modGlobalConstants.Statics.gboolPP)
			{
				rsUpdate.OpenRecordset("SELECT * FROM sys.indexes WHERE name = 'IX_ppmaster_PartyID' AND object_id = OBJECT_ID('dbo.ppmaster')", "PersonalProperty");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_ppmaster_PartyID ON dbo.ppmaster (PartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "PersonalProperty");
				}
			}
			// Code Enforcement
			if (modGlobalConstants.Statics.gboolCE)
			{
				rsUpdate.OpenRecordset("SELECT * FROM sys.indexes WHERE name = 'IX_cemaster_Owner1ID' AND object_id = OBJECT_ID('dbo.cemaster')", "CodeEnforcement");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_cemaster_Owner1ID ON dbo.cemaster (Owner1ID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "CodeEnforcement");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_cemaster_Owner2ID ON dbo.cemaster (Owner2ID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "CodeEnforcement");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_Contractors_PartyID ON dbo.Contractors (PartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "CodeEnforcement");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_PermitContractors_PartyID ON dbo.PermitContractors (PartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "CodeEnforcement");
					rsUpdate.Execute("CREATE NONCLUSTERED INDEX IX_Inspectors_PartyID ON dbo.Inspectors (PartyID) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]", "CodeEnforcement");
				}
			}
		}

		public static void ChangePartyIDInOtherModules_6(int NewPartyID, int OriginalPartyID)
		{
			ChangePartyIDInOtherModules(ref NewPartyID, ref OriginalPartyID);
		}

		public static void ChangePartyIDInOtherModules(ref int NewPartyID, ref int OriginalPartyID)
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			// Motor Vehicle
			if (modGlobalConstants.Statics.gboolMV)
			{
				rsUpdate.Execute("Update FleetMaster set PartyID1 = " + FCConvert.ToString(NewPartyID) + " where PartyID1 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update FleetMaster set PartyID2 = " + FCConvert.ToString(NewPartyID) + " where PartyID2 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update FleetMaster set PartyID3 = " + FCConvert.ToString(NewPartyID) + " where PartyID3 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update Master set PartyID1 = " + FCConvert.ToString(NewPartyID) + " where PartyID1 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update Master set PartyID2 = " + FCConvert.ToString(NewPartyID) + " where PartyID2 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update Master set PartyID3 = " + FCConvert.ToString(NewPartyID) + " where PartyID3 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update PendingActivityMaster set PartyID1 = " + FCConvert.ToString(NewPartyID) + " where PartyID1 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update PendingActivityMaster set PartyID2 = " + FCConvert.ToString(NewPartyID) + " where PartyID2 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update PendingActivityMaster set PartyID3 = " + FCConvert.ToString(NewPartyID) + " where PartyID3 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update HeldRegistrationMaster set PartyID1 = " + FCConvert.ToString(NewPartyID) + " where PartyID1 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update HeldRegistrationMaster set PartyID2 = " + FCConvert.ToString(NewPartyID) + " where PartyID2 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update HeldRegistrationMaster set PartyID3 = " + FCConvert.ToString(NewPartyID) + " where PartyID3 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update LongTermTrailerRegistrations set PartyID1 = " + FCConvert.ToString(NewPartyID) + " where PartyID1 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update LongTermTrailerRegistrations set PartyID2 = " + FCConvert.ToString(NewPartyID) + " where PartyID2 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update ActivityMaster set PartyID1 = " + FCConvert.ToString(NewPartyID) + " where PartyID1 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update ActivityMaster set PartyID2 = " + FCConvert.ToString(NewPartyID) + " where PartyID2 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update ActivityMaster set PartyID3 = " + FCConvert.ToString(NewPartyID) + " where PartyID3 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update tblMasterTemp set PartyID1 = " + FCConvert.ToString(NewPartyID) + " where PartyID1 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update tblMasterTemp set PartyID2 = " + FCConvert.ToString(NewPartyID) + " where PartyID2 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update tblMasterTemp set PartyID3 = " + FCConvert.ToString(NewPartyID) + " where PartyID3 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update ArchiveMaster set PartyID1 = " + FCConvert.ToString(NewPartyID) + " where PartyID1 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update ArchiveMaster set PartyID2 = " + FCConvert.ToString(NewPartyID) + " where PartyID2 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update ArchiveMaster set PartyID3 = " + FCConvert.ToString(NewPartyID) + " where PartyID3 = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update TransitPlates set PartyID = " + FCConvert.ToString(NewPartyID) + " where PartyID = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
				rsUpdate.Execute("Update SpecialRegistration set PartyID = " + FCConvert.ToString(NewPartyID) + " where PartyID = " + FCConvert.ToString(OriginalPartyID), "MotorVehicle");
			}
			// Utility Billing
			if (modGlobalConstants.Statics.gboolUT)
			{
				rsUpdate.Execute("update master set BillingPartyID = " + FCConvert.ToString(NewPartyID) + " where BillingPartyID = " + FCConvert.ToString(OriginalPartyID), "UtilityBilling");
				rsUpdate.Execute("update master set SecondBillingPartyID = " + FCConvert.ToString(NewPartyID) + " where SecondBillingPartyID = " + FCConvert.ToString(OriginalPartyID), "UtilityBilling");
				rsUpdate.Execute("update master set OwnerPartyID = " + FCConvert.ToString(NewPartyID) + " where OwnerPartyID = " + FCConvert.ToString(OriginalPartyID), "UtilityBilling");
				rsUpdate.Execute("update master set SecondOwnerPartyID = " + FCConvert.ToString(NewPartyID) + " where SecondOwnerPartyID = " + FCConvert.ToString(OriginalPartyID), "UtilityBilling");
			}
			// Real Estate
			if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
			{
				rsUpdate.Execute("update master set OwnerPartyID = " + FCConvert.ToString(NewPartyID) + " where ownerpartyid = " + FCConvert.ToString(OriginalPartyID), "RealEstate");
				rsUpdate.Execute("update master set SecOwnerPartyID = " + FCConvert.ToString(NewPartyID) + " where SecOwnerpartyid = " + FCConvert.ToString(OriginalPartyID), "RealEstate");
				rsUpdate.Execute("Update Owners set PartyID = " + FCConvert.ToString(NewPartyID) + " where PartyID = " + FCConvert.ToString(OriginalPartyID), "RealEstate");
				rsUpdate.Execute("Update PreviousOwner set PartyID1 = " + FCConvert.ToString(NewPartyID) + " where PartyID1 = " + FCConvert.ToString(OriginalPartyID), "RealEstate");
			}
			// Accounts Receivable
			if (modGlobalConstants.Statics.gboolAR)
			{
				rsUpdate.Execute("Update CustomerMaster set PartyID = " + FCConvert.ToString(NewPartyID) + " where PartyID = " + FCConvert.ToString(OriginalPartyID), "AccountsReceivable");
			}
			// Clerk
			if (modGlobalConstants.Statics.gboolCK)
			{
				rsUpdate.Execute("update DogOwner set PartyID = " + FCConvert.ToString(NewPartyID) + " where PartyID = " + FCConvert.ToString(OriginalPartyID), "Clerk");
			}
			// Personal Property
			if (modGlobalConstants.Statics.gboolPP || modGlobalConstants.Statics.gboolBL)
			{
				rsUpdate.Execute("update ppmaster set PartyID = " + FCConvert.ToString(NewPartyID) + " where PartyID = " + FCConvert.ToString(OriginalPartyID), "PersonalProperty");
			}
			// Code Enforcement
			if (modGlobalConstants.Statics.gboolCE)
			{
				rsUpdate.Execute("update cemaster set Owner1ID = " + FCConvert.ToString(NewPartyID) + " where owner1id = " + FCConvert.ToString(OriginalPartyID), "CodeEnforcement");
				rsUpdate.Execute("update cemaster set Owner2ID = " + FCConvert.ToString(NewPartyID) + " where Owner2id = " + FCConvert.ToString(OriginalPartyID), "CodeEnforcement");
				rsUpdate.Execute("Update Contractors set PartyID = " + FCConvert.ToString(NewPartyID) + " where PartyID = " + FCConvert.ToString(OriginalPartyID), "CodeEnforcement");
				rsUpdate.Execute("Update PermitContractors set PartyID = " + FCConvert.ToString(NewPartyID) + " where PartyID = " + FCConvert.ToString(OriginalPartyID), "CodeEnforcement");
				rsUpdate.Execute("Update Inspectors set partyid = " + FCConvert.ToString(NewPartyID) + " where partyid = " + FCConvert.ToString(OriginalPartyID), "CodeEnforcement");
			}
		}

		public static string ReturnReferencedModuleDescriptions(ref int lngPartyID)
		{
			string ReturnReferencedModuleDescriptions = "";
			string strTemp;
			strTemp = "";
			if (modGlobalConstants.Statics.gboolAR)
			{
				if (PartyReferencedInModule_6(lngPartyID, "AR"))
				{
					strTemp += "Accounts Receivable, ";
				}
			}
			if (modGlobalConstants.Statics.gboolCE)
			{
				if (PartyReferencedInModule_6(lngPartyID, "CE"))
				{
					strTemp += "Code Enforcement, ";
				}
			}
			if (modGlobalConstants.Statics.gboolCK)
			{
				if (PartyReferencedInModule_6(lngPartyID, "CK"))
				{
					strTemp += "Clerk, ";
				}
			}
			if (modGlobalConstants.Statics.gboolMV)
			{
				if (PartyReferencedInModule_6(lngPartyID, "MV"))
				{
					strTemp += "Motor Vehicle, ";
				}
			}
			if (modGlobalConstants.Statics.gboolPP)
			{
				if (PartyReferencedInModule_6(lngPartyID, "PP"))
				{
					strTemp += "Personal Property, ";
				}
			}
			if (modGlobalConstants.Statics.gboolRE)
			{
				if (PartyReferencedInModule_6(lngPartyID, "RE"))
				{
					strTemp += "Real Estate, ";
				}
			}
			if (modGlobalConstants.Statics.gboolUT)
			{
				if (PartyReferencedInModule_6(lngPartyID, "UT"))
				{
					strTemp += "Utility Billing, ";
				}
			}
			if (strTemp != "")
			{
				strTemp = Strings.Left(strTemp, strTemp.Length - 2);
			}
			ReturnReferencedModuleDescriptions = strTemp;
			return ReturnReferencedModuleDescriptions;
		}

		public static bool PartyReferencedInModule_6(int lngPartyID, string strMod)
		{
			return PartyReferencedInModule(ref lngPartyID, ref strMod);
		}

		public static bool PartyReferencedInModule(ref int lngPartyID, ref string strMod)
		{
			bool PartyReferencedInModule = false;
			clsDRWrapper rsCheck = new clsDRWrapper();
			PartyReferencedInModule = false;
			if (strMod == "AR")
			{
				if (modGlobalConstants.Statics.gboolAR)
				{
					rsCheck.OpenRecordset("SELECT * FROM CustomerMaster WHERE PartyID = " + FCConvert.ToString(lngPartyID), "AccountsReceivable");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
				}
			}
			else if (strMod == "CK")
			{
				if (modGlobalConstants.Statics.gboolCK)
				{
					rsCheck.OpenRecordset("SELECT * FROM DogOwner WHERE PartyID = " + FCConvert.ToString(lngPartyID), "Clerk");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
				}
			}
			else if (strMod == "CE")
			{
				if (modGlobalConstants.Statics.gboolCE)
				{
					rsCheck.OpenRecordset("SELECT * FROM cemaster WHERE Owner1ID = " + FCConvert.ToString(lngPartyID) + " or Owner2ID = " + FCConvert.ToString(lngPartyID), "CodeEnforcement");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
					rsCheck.OpenRecordset("SELECT * FROM Contractors WHERE PartyID = " + FCConvert.ToString(lngPartyID), "CodeEnforcement");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
					rsCheck.OpenRecordset("SELECT * FROM PermitContractors WHERE PartyID = " + FCConvert.ToString(lngPartyID), "CodeEnforcement");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
					rsCheck.OpenRecordset("SELECT * FROM Inspectors WHERE partyid = " + FCConvert.ToString(lngPartyID), "CodeEnforcement");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
				}
			}
			else if (strMod == "MV")
			{
				if (modGlobalConstants.Statics.gboolMV)
				{
					rsCheck.OpenRecordset("SELECT * FROM FleetMaster WHERE PartyID1 = " + FCConvert.ToString(lngPartyID) + " OR PartyID2 = " + FCConvert.ToString(lngPartyID) + " OR PartyID3 = " + FCConvert.ToString(lngPartyID), "MotorVehicle");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
					rsCheck.OpenRecordset("SELECT * FROM Master WHERE PartyID1 = " + FCConvert.ToString(lngPartyID) + " OR PartyID2 = " + FCConvert.ToString(lngPartyID) + " OR PartyID3 = " + FCConvert.ToString(lngPartyID), "MotorVehicle");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
					rsCheck.OpenRecordset("SELECT * FROM PendingActivityMaster WHERE PartyID1 = " + FCConvert.ToString(lngPartyID) + " OR PartyID2 = " + FCConvert.ToString(lngPartyID) + " OR PartyID3 = " + FCConvert.ToString(lngPartyID), "MotorVehicle");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
					rsCheck.OpenRecordset("SELECT * FROM HeldRegistrationMaster WHERE PartyID1 = " + FCConvert.ToString(lngPartyID) + " OR PartyID2 = " + FCConvert.ToString(lngPartyID) + " OR PartyID3 = " + FCConvert.ToString(lngPartyID), "MotorVehicle");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
					rsCheck.OpenRecordset("SELECT * FROM LongTermTrailerRegistrations WHERE PartyID1 = " + FCConvert.ToString(lngPartyID) + " OR PartyID2 = " + FCConvert.ToString(lngPartyID), "MotorVehicle");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
					rsCheck.OpenRecordset("SELECT * FROM ActivityMaster WHERE PartyID1 = " + FCConvert.ToString(lngPartyID) + " OR PartyID2 = " + FCConvert.ToString(lngPartyID) + " OR PartyID3 = " + FCConvert.ToString(lngPartyID), "MotorVehicle");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
					rsCheck.OpenRecordset("SELECT * FROM ArchiveMaster WHERE PartyID1 = " + FCConvert.ToString(lngPartyID) + " OR PartyID2 = " + FCConvert.ToString(lngPartyID) + " OR PartyID3 = " + FCConvert.ToString(lngPartyID), "MotorVehicle");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
					rsCheck.OpenRecordset("SELECT * FROM TransitPlates WHERE PartyID = " + FCConvert.ToString(lngPartyID), "MotorVehicle");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
					rsCheck.OpenRecordset("SELECT * FROM SpecialRegistration WHERE PartyID = " + FCConvert.ToString(lngPartyID), "MotorVehicle");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
				}
			}
			else if (strMod == "PP")
			{
				if (modGlobalConstants.Statics.gboolPP)
				{
					rsCheck.OpenRecordset("SELECT * FROM ppmaster WHERE PartyID = " + FCConvert.ToString(lngPartyID), "PersonalProperty");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
				}
			}
			else if (strMod == "RE")
			{
				if (modGlobalConstants.Statics.gboolRE)
				{
					rsCheck.OpenRecordset("SELECT * FROM master WHERE OwnerPartyID = " + FCConvert.ToString(lngPartyID) + " OR SecOwnerPartyID = " + FCConvert.ToString(lngPartyID), "RealEstate");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
					rsCheck.OpenRecordset("SELECT * FROM Owners WHERE PartyID = " + FCConvert.ToString(lngPartyID), "RealEstate");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
					rsCheck.OpenRecordset("SELECT * FROM PreviousOwner WHERE PartyID1 = " + FCConvert.ToString(lngPartyID), "RealEstate");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
				}
			}
			else if (strMod == "UT")
			{
				if (modGlobalConstants.Statics.gboolUT)
				{
					rsCheck.OpenRecordset("SELECT * FROM master WHERE BillingPartyID = " + FCConvert.ToString(lngPartyID) + " or SecondBillingPartyID = " + FCConvert.ToString(lngPartyID) + " or OwnerPartyID = " + FCConvert.ToString(lngPartyID) + " or SecondOwnerPartyID = " + FCConvert.ToString(lngPartyID), "UtilityBilling");
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						PartyReferencedInModule = true;
						return PartyReferencedInModule;
					}
				}
			}
			return PartyReferencedInModule;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public static bool PartyReferenced(ref int lngPartyID)
		{
			bool PartyReferenced = false;
			PartyReferenced = false;
			if (modGlobalConstants.Statics.gboolAR)
			{
				if (PartyReferencedInModule_6(lngPartyID, "AR"))
				{
					PartyReferenced = true;
					return PartyReferenced;
				}
			}
			if (modGlobalConstants.Statics.gboolCE)
			{
				if (PartyReferencedInModule_6(lngPartyID, "CE"))
				{
					PartyReferenced = true;
					return PartyReferenced;
				}
			}
			if (modGlobalConstants.Statics.gboolCK)
			{
				if (PartyReferencedInModule_6(lngPartyID, "CK"))
				{
					PartyReferenced = true;
					return PartyReferenced;
				}
			}
			if (modGlobalConstants.Statics.gboolMV)
			{
				if (PartyReferencedInModule_6(lngPartyID, "MV"))
				{
					PartyReferenced = true;
					return PartyReferenced;
				}
			}
			if (modGlobalConstants.Statics.gboolPP)
			{
				if (PartyReferencedInModule_6(lngPartyID, "PP"))
				{
					PartyReferenced = true;
					return PartyReferenced;
				}
			}
			if (modGlobalConstants.Statics.gboolRE)
			{
				if (PartyReferencedInModule_6(lngPartyID, "RE"))
				{
					PartyReferenced = true;
					return PartyReferenced;
				}
			}
			if (modGlobalConstants.Statics.gboolUT)
			{
				if (PartyReferencedInModule_6(lngPartyID, "UT"))
				{
					PartyReferenced = true;
					return PartyReferenced;
				}
			}
			return PartyReferenced;
		}
	}
}
