﻿

using System.Collections.Generic;

namespace SharedApplication
{
	public interface ICentralPartyApp
	{
		//=========================================================
		bool ReferencesParty(int lngID);

		List<object> AccountsReferencingParty(int lngID);

		string ModuleCode();

		bool ChangePartyID(int lngOriginalPartyID, int lngNewPartyID);
	}
}
