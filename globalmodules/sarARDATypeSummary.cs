﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

#if TWCL0000
using TWCL0000;


#elif TWBD0000
using TWBD0000;


#elif TWAR0000
using TWAR0000;


#else
using TWCR0000;

#endif
namespace Global
{
	/// <summary>
	/// Summary description for sarARDATypeSummary.
	/// </summary>
	public partial class sarARDATypeSummary : FCSectionReport
	{
		// nObj = 1
		//   0	sarARDATypeSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/10/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/11/2005              *
		// ********************************************************
		float lngWidth;
		clsDRWrapper rsData = new clsDRWrapper();
		string strSQL = "";
		bool boolEndReport;
		decimal dblPrinSum;
		decimal dblIntSum;
		decimal dblTaxSum;
		bool boolWide;

		public sarARDATypeSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarARDATypeSummary_ReportEnd;
		}

        private void SarARDATypeSummary_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        public static sarARDATypeSummary InstancePtr
		{
			get
			{
				return (sarARDATypeSummary)Sys.GetInstance(typeof(sarARDATypeSummary));
			}
		}

		protected sarARDATypeSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true)
			{
				eArgs.EOF = false;
				BindFields();
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// default the totals
			fldTotalPrin.Text = " 0.00";
			fldTotalInt.Text = " 0.00";
			fldTotalTax.Text = " 0.00";
			fldTotalTotal.Text = " 0.00";
			// this will put the SQL string together and set the order
			if (modGlobalConstants.Statics.gboolCR)
			{
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(Interest) AS I, SUM(Tax) AS T, Code FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + "AND Code <> 'I' AND isnull(ReceiptNumber, 0) >= 0 GROUP BY Code ORDER BY Code";
			}
			else
			{
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(Interest) AS I, SUM(Tax) AS T, Code FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + "AND Code <> 'I' GROUP BY Code ORDER BY Code";
			}
			rsData.OpenRecordset(strSQL, modExtraModules.strARDatabase);
			SetupFields();
			frmWait.InstancePtr.IncrementProgress();
			if (rsData.EndOfFile())
			{
				EndRoutine();
			}
		}

		private void SetupFields()
		{
			// this will adjust the fields to thier correct
			boolWide = rptARDailyAuditMaster.InstancePtr.boolLandscape;
			lngWidth = rptARDailyAuditMaster.InstancePtr.lngWidth;
			// set the full page objects
			this.PrintWidth = lngWidth;
			// Me.Detail.Width = lngWidth
			lblHeader.Width = lngWidth - 1 / 1440f;
			lnHeader.X2 = lngWidth - 1 / 1440f;
			// header labels/lines
			if (!boolWide)
			{
				lblType.Left = 0;
				lblDesc.Left = 1350 / 1440f;
				lblPrincipal.Left = 4230 / 1440f;
				lblInterest.Left = 5670 / 1440f;
				lblTax.Left = 7110 / 1440f;
				lblTotal.Left = 8550 / 1440f;
			}
			else
			{
				lblType.Left = 0;
				lblDesc.Left = 1350 / 1440f;
				lblPrincipal.Left = 6120 / 1440f;
				lblInterest.Left = 8100 / 1440f;
				lblTax.Left = 10080 / 1440f;
				lblTotal.Left = 12060 / 1440f;
			}
			// detail section
			fldType.Left = lblType.Left;
			fldDesc.Left = lblDesc.Left;
			fldPrincipal.Left = lblPrincipal.Left;
			fldInterest.Left = lblInterest.Left;
			fldTax.Left = lblTax.Left;
			fldTotal.Left = lblTotal.Left;
			// footer labels/fields
			lblTotals.Left = lblDesc.Left;
			fldTotalPrin.Left = lblPrincipal.Left;
			fldTotalInt.Left = lblInterest.Left;
			fldTotalTax.Left = lblTax.Left;
			fldTotalTotal.Left = lblTotal.Left;
			lnTotals.X1 = lblPrincipal.Left;
			lnTotals.X2 = fldTotalTotal.Left + fldTotalTotal.Width;
		}

		private void BindFields()
		{
			// this will fill the fields with the correct data
			frmWait.InstancePtr.IncrementProgress();
			fldType.Text = rsData.Get_Fields_String("Code");
			fldDesc.Text = GetDesc_2(rsData.Get_Fields_String("Code"));
			fldPrincipal.Text = Strings.Format(rsData.Get_Fields_Double("Prin"), "#,##0.00");
			dblPrinSum += FCConvert.ToDecimal(rsData.Get_Fields_Double("Prin"));
			// TODO: Field [I] not found!! (maybe it is an alias?)
			fldInterest.Text = Strings.Format(rsData.Get_Fields("I"), "#,##0.00");
			// TODO: Field [I] not found!! (maybe it is an alias?)
			dblIntSum += rsData.Get_Fields("I");
			// TODO: Field [T] not found!! (maybe it is an alias?)
			fldTax.Text = Strings.Format(rsData.Get_Fields("T"), "#,##0.00");
			// TODO: Field [T] not found!! (maybe it is an alias?)
			dblTaxSum += rsData.Get_Fields("T");
			// TODO: Field [I] not found!! (maybe it is an alias?)
			// TODO: Field [T] not found!! (maybe it is an alias?)
			fldTotal.Text = Strings.Format(FCConvert.ToDecimal(rsData.Get_Fields_Double("Prin")) + rsData.Get_Fields("I") + rsData.Get_Fields("T"), "#,##0.00");
			// move to the next record
			rsData.MoveNext();
		}

		private void EndRoutine()
		{
			// this will show a message that there are no records to process and hide the rest of the report
			lnHeader.Visible = false;
			lblHeader.Visible = false;
			lnTotals.Visible = false;
			lblPrincipal.Visible = false;
			lblInterest.Visible = false;
			lblTax.Visible = false;
			lblTotal.Visible = false;
			fldTotalPrin.Visible = false;
			fldTotalInt.Visible = false;
			fldTotalTax.Visible = false;
			fldTotalTotal.Visible = false;
			lblTotals.Visible = false;
			// lblTotals.Width = lngWidth - lblTotals.Left
			// lblTotals.Text = "There are no records to process."
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalPrin.Text = Strings.Format(dblPrinSum, "#,##0.00");
			fldTotalInt.Text = Strings.Format(dblIntSum, "#,##0.00");
			fldTotalTax.Text = Strings.Format(dblTaxSum, "#,##0.00");
			// calculate the total line
			fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldTotalPrin.Text) + FCConvert.ToDouble(fldTotalInt.Text) + FCConvert.ToDouble(fldTotalTax.Text), "#,##0.00");
		}

		private string GetDesc_2(string strCode)
		{
			return GetDesc(ref strCode);
		}

		private string GetDesc(ref string strCode)
		{
			string GetDesc = "";
			if (Strings.UCase(strCode) == "C")
			{
				GetDesc = "Correction";
			}
			else if (Strings.UCase(strCode) == "P")
			{
				GetDesc = "Payment";
			}
			else
			{
				GetDesc = "";
			}
			return GetDesc;
		}

		private void sarARDATypeSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarARDATypeSummary.Caption	= "Daily Audit Detail";
			//sarARDATypeSummary.Icon	= "sarARDATypeSummary.dsx":0000";
			//sarARDATypeSummary.Left	= 0;
			//sarARDATypeSummary.Top	= 0;
			//sarARDATypeSummary.Width	= 11880;
			//sarARDATypeSummary.Height	= 8175;
			//sarARDATypeSummary.StartUpPosition	= 3;
			//sarARDATypeSummary.SectionData	= "sarARDATypeSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
