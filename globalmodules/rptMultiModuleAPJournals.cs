﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptMultiModuleAPJournals : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/05/2006              *
		// ********************************************************
		clsDRWrapper rsInfo = new clsDRWrapper();
		clsDRWrapper rsGeneralInfo = new clsDRWrapper();
		// VBto upgrade warning: curDebitTotal As double	OnWrite(short, double)
		decimal curDebitTotal;
		// VBto upgrade warning: curCreditTotal As double	OnWrite(short, double)
		decimal curCreditTotal;
		// VBto upgrade warning: curEncTotal As double	OnWrite(short, double)
		decimal curEncTotal;
		bool blnFirstControl;

		public rptMultiModuleAPJournals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptMultiModuleAPJournals_ReportEnd;
		}

        private void RptMultiModuleAPJournals_ReportEnd(object sender, EventArgs e)
        {
			rsInfo.DisposeOf();
            rsGeneralInfo.DisposeOf();

		}

        public static rptMultiModuleAPJournals InstancePtr
		{
			get
			{
				return (rptMultiModuleAPJournals)Sys.GetInstance(typeof(rptMultiModuleAPJournals));
			}
		}

		protected rptMultiModuleAPJournals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsGeneralInfo.Dispose();
				rsInfo.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsInfo.EndOfFile())
			{
				TRYAGAIN:
				;
				rsGeneralInfo.MoveNext();
				if (rsGeneralInfo.EndOfFile())
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
					rsInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + FCConvert.ToString(rsGeneralInfo.Get_Fields_Int32("ID")), "TWBD0000.vb1");
					if (rsInfo.BeginningOfFile() != true && rsInfo.EndOfFile() != true)
					{
						rsInfo.MoveLast();
						rsInfo.MoveFirst();
					}
					else
					{
						goto TRYAGAIN;
					}
				}
			}
			else
			{
				eArgs.EOF = false;
			}
			//Detail_Format();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsDesc = new clsDRWrapper();
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			rsDesc.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber), "TWBD0000.vb1");
			if (rsDesc.EndOfFile() != true && rsDesc.BeginningOfFile() != true)
			{
				lblDescription.Text = rsDesc.Get_Fields_String("Description");
			}
			else
			{
				lblDescription.Text = "UNKNOWN";
			}
			Label6.Text = "Journal No. " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + "       Post Date: " + DateTime.Today.ToShortDateString() + "       Type: " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalType;
			if (rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).CheckDate != "")
			{
				rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'P' AND CheckDate = '" + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).CheckDate + "' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " ORDER BY ID", "TWBD0000.vb1");
			}
			else
			{
				rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'P' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " ORDER BY ID", "TWBD0000.vb1");
			}
			rsInfo.OpenRecordset("SELECT * FROM APJournalDetail WHERE APJournalID = " + FCConvert.ToString(rsGeneralInfo.Get_Fields_Int32("ID")), "TWBD0000.vb1");
			if (rsInfo.BeginningOfFile() != true && rsInfo.EndOfFile() != true)
			{
				rsInfo.MoveLast();
				rsInfo.MoveFirst();
			}
			if (rsGeneralInfo.EndOfFile() != true && rsGeneralInfo.BeginningOfFile() != true)
			{
				rsGeneralInfo.MoveLast();
				rsGeneralInfo.MoveFirst();
			}
			curDebitTotal = 0;
			curCreditTotal = 0;
			curEncTotal = 0;
			blnFirstControl = false;
			Line2.Visible = false;
			rsDesc.DisposeOf();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (blnFirstControl == false)
			{
				if (FCConvert.ToString(rsInfo.Get_Fields_String("Project")) == "CTRL")
				{
					Line2.Visible = true;
					blnFirstControl = true;
				}
			}
			else
			{
				if (Line2.Visible == true)
				{
					Line2.Visible = false;
				}
			}
			Field3.Text = rsInfo.Get_Fields_String("Description");
			// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
			Field6.Text = rsInfo.Get_Fields_String("Account");
			// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
			Field1.Text = Strings.Format(rsGeneralInfo.Get_Fields("Period"), "00");
			if (rsInfo.Get_Fields_Decimal("Encumbrance") > 0)
			{
				Field8.Text = Strings.Format(rsInfo.Get_Fields_Decimal("Encumbrance"), "#,##0.00");
			}
			else
			{
				Field8.Text = "";
			}
			curEncTotal += FCConvert.ToDecimal(rsInfo.Get_Fields_Decimal("Encumbrance"));
			// TODO: Field [Check] not found!! (maybe it is an alias?)
			Field12.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields("Check"));
			Field2.Text = Strings.Format(rsGeneralInfo.Get_Fields_DateTime("CheckDate"), "MM/dd/yyyy");
			// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO: Check the table for the column [Discount] and replace with corresponding Get_Field method
			if (Conversion.Val(rsInfo.Get_Fields("Amount")) - Conversion.Val(rsInfo.Get_Fields("Discount")) - Conversion.Val(rsInfo.Get_Fields_Decimal("Encumbrance")) < 0)
			{
				// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Discount] and replace with corresponding Get_Field method
				Field14.Text = Strings.Format(((Conversion.Val(rsInfo.Get_Fields("Amount")) - Conversion.Val(rsInfo.Get_Fields("Discount")) - Conversion.Val(rsInfo.Get_Fields_Decimal("Encumbrance"))) * -1), "#,##0.00");
				Field7.Text = "";
				if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Revenue CTL")
				{
					// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Discount] and replace with corresponding Get_Field method
					curCreditTotal += FCConvert.ToDecimal(((Conversion.Val(rsInfo.Get_Fields("Amount")) - Conversion.Val(rsInfo.Get_Fields("Discount")) - Conversion.Val(rsInfo.Get_Fields_Decimal("Encumbrance"))) * -1));
				}
			}
			// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
			// TODO: Check the table for the column [Discount] and replace with corresponding Get_Field method
			else if (Conversion.Val(rsInfo.Get_Fields("Amount")) - Conversion.Val(rsInfo.Get_Fields("Discount")) - Conversion.Val(rsInfo.Get_Fields_Decimal("Encumbrance")) > 0)
			{
				// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Discount] and replace with corresponding Get_Field method
				Field7.Text = Strings.Format(Conversion.Val(rsInfo.Get_Fields("Amount")) - Conversion.Val(rsInfo.Get_Fields("Discount")) - Conversion.Val(rsInfo.Get_Fields_Decimal("Encumbrance")), "#,##0.00");
				Field14.Text = "";
				if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Revenue CTL")
				{
					// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Discount] and replace with corresponding Get_Field method
					curDebitTotal += FCConvert.ToDecimal((Conversion.Val(rsInfo.Get_Fields("Amount")) - Conversion.Val(rsInfo.Get_Fields("Discount")) - Conversion.Val(rsInfo.Get_Fields_Decimal("Encumbrance"))));
				}
			}
			else
			{
				Field7.Text = "";
				Field14.Text = "";
			}
			Field13.Text = rsGeneralInfo.Get_Fields_String("Reference");
			// TODO: Check the table for the column [Warrant] and replace with corresponding Get_Field method
			Field10.Text = FCConvert.ToString(rsGeneralInfo.Get_Fields("Warrant"));
			Field9.Text = rsInfo.Get_Fields_String("Project");
			if (FCConvert.ToString(rsInfo.Get_Fields_String("Project")) == "CTRL")
			{
				Field11.Text = "";
			}
			else
			{
				Field11.Text = modValidateAccount.GetFormat_6(FCConvert.ToString(rsGeneralInfo.Get_Fields_Int32("VendorNumber")), 5);
			}
			rsInfo.MoveNext();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (curEncTotal != 0)
			{
				if (rptMultiModulePosting.InstancePtr.PostInfo.PageBreakBetweenJournalsOnReport)
				{
					GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
				}
				else
				{
					GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				}
				srptEncLiquid.Report = rptMultiModuleEncumbranceLiquidation.InstancePtr;
				srptEncLiquid.Report.UserData = this.UserData;
			}
			else
			{
				GroupFooter1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				srptEncLiquid = null;
			}
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			if (curDebitTotal != 0)
			{
				fldDebitTotal.Text = Strings.Format(curDebitTotal, "#,##0.00");
			}
			else
			{
				fldDebitTotal.Text = "";
			}
			if (curCreditTotal != 0)
			{
				fldCreditTotal.Text = Strings.Format(curCreditTotal, "#,##0.00");
			}
			else
			{
				fldCreditTotal.Text = "";
			}
			if (curEncTotal > 0)
			{
				fldEncTotal.Text = Strings.Format(curEncTotal, "#,##0.00");
			}
			else
			{
				fldEncTotal.Text = "";
			}
			rptSubTotals.Report = rptMultiModuleJournalTotals.InstancePtr;
			rptSubTotals.Report.UserData = this.UserData;
		}

		private void rptMultiModuleAPJournals_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMultiModuleAPJournals.Icon	= "rptMultiModuleAPJournals.dsx":0000";
			//rptMultiModuleAPJournals.Left	= 0;
			//rptMultiModuleAPJournals.Top	= 0;
			//rptMultiModuleAPJournals.Width	= 11880;
			//rptMultiModuleAPJournals.Height	= 8595;
			//rptMultiModuleAPJournals.StartUpPosition	= 3;
			//rptMultiModuleAPJournals.SectionData	= "rptMultiModuleAPJournals.dsx":058A;
			//End Unmaped Properties
		}
	}
}
