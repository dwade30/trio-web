//Fecher vbPorter - Version 1.0.0.32
using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;


namespace TWPP0000
{
    /// <summary>
    /// Summary description for frmRefGlobalComment.
    /// </summary>
    public partial class frmRefGlobalComment : BaseForm
    {


        public frmRefGlobalComment()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        


        //=========================================================

        int intFrmSize;
        string strMod;
        string strTbl;
        string strFld;
        string strIDFld;
        int lngIDNumber;
        bool boolBlankComment;
        string strComment = "";






        private void frmRefGlobalComment_KeyDown(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);

            switch (KeyCode)
            {

                case Keys.Escape:
                    {
                        KeyCode = (Keys)0;
                        mnuExit_Click();
                        break;
                    }
            } //end switch
        }


        private void frmRefGlobalComment_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmRefGlobalComment properties;
            //frmRefGlobalComment.ScaleWidth	= 9045;
            //frmRefGlobalComment.ScaleHeight	= 7485;
            //frmRefGlobalComment.LinkTopic	= "Form1";
            //frmRefGlobalComment.LockControls	= -1  'True;

            //rtbText properties;
            //rtbText.ScrollBars	= 2;
            //rtbText.TextRTF	= $"frmRefGlobalComment.frx":058A;
            //End Unmaped Properties

            modGlobalFunctions.SetFixedSize(this, intFrmSize);
            modGlobalFunctions.SetTRIOColors(this);
        }

        // vbPorter upgrade warning: intFormSize As short	OnWriteFCConvert.ToInt32(
        public string Init(ref string strModule, ref string strTable, ref string strField, ref string strKeyField, ref int lngID, string strFormCaption = "Comment", string strControlCaption = "", short intFormSize = modGlobalConstants.TRIOWINDOWSIZEBIGGIE, bool boolNonEditable = false)
        {
            string Init = "";
            // parameters:
            // strmodule   the two letter abbrev. for your module
            // strtable    the name of the table the comment is in
            // strfield    the name of the field the comment is in
            // strkeyfield the name of the autoid field or account field etc. used to identify correct record
            // lngid       the autoid,account etc. number
            // strformcaption   the caption of the form.  it is "Comment" by default
            // strcontrolcaption an optional explanation of the data in the control
            // intFormSize the size of the window (triowindowsizebiggie etc.)
            // Returns:
            // whether the comment is blank or not
            clsDRWrapper clsSave = new clsDRWrapper();

            intFrmSize = intFormSize;

            if (boolNonEditable)
            {
                rtbText.Locked = true;
                mnuSave.Visible = false;
                //FC:FINAL:MSH - i.issue #1351: remove second 'Save' button
                //cmdSave.Visible = false;
                mnuSaveExit.Visible = false;
				cmdSaveExit.Visible = false;
				mnuSepar2.Visible = false;
            }

            this.Text = strFormCaption;
            lblTitle.Text = strControlCaption;

            strMod = strModule;
            strTbl = strTable;
            strFld = strField;
            strIDFld = strKeyField;
            lngIDNumber = lngID;
            intFrmSize = intFormSize;

            if (intFrmSize == modGlobalConstants.TRIOWINDOWSIZEBIGGIE)
            {
                //vsElasticLight1.Enabled = true;
            }


            clsSave.OpenRecordset("select " + strField + " from " + strTable + " where " + strKeyField + " = " + FCConvert.ToString(lngID), "tw" + strModule + "0000.vb1");
            if (!clsSave.EndOfFile())
            {
                rtbText.Text = FCConvert.ToString(clsSave.Get_Fields(strField));
                if (Strings.Trim(FCConvert.ToString(clsSave.Get_Fields(strField))) != string.Empty)
                {
                    boolBlankComment = false;
                }
                else
                {
                    boolBlankComment = true;
                }
                strComment = rtbText.Text;
            }
            else
            {
                rtbText.Text = "";
                strComment = "";
                boolBlankComment = true;
            }



			this.Show(FormShowEnum.Modal);
			Init = strComment;
            return Init;
        }

        private void frmRefGlobalComment_Resize(object sender, System.EventArgs e)
        {
            if (intFrmSize == modGlobalConstants.TRIOWINDOWSIZEBIGGIE) return;
            rtbText.Width = this.Width - (rtbText.Left * 2) - 100;
            lblTitle.Width = rtbText.Width;
            rtbText.Height = this.Height - rtbText.Top - 650;
        }

        private void mnuClear_Click(object sender, System.EventArgs e)
        {
            rtbText.Text = "";
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            Close();
        }
        public void mnuExit_Click()
        {
            mnuExit_Click(mnuExit, new System.EventArgs());
        }


        private void mnuPrint_Click(object sender, System.EventArgs e)
        {
            try
            {   // On Error GoTo ErrorHandler
                // MDIParent.InstancePtr.CommonDialog1.Flags = 0	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                // MDIParent.InstancePtr.CommonDialog1.Flags = (vbPorterConverter.cdlPDReturnDC+vbPorterConverter.cdlPDNoPageNums)	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                //- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
                // If rtbText.SelLength = 0 Then
                // MDIParent.InstancePtr.CommonDialog1.Flags = 0 /*? MDIParent.InstancePtr.CommonDialog1.Flags */	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"+vbPorterConverter.cdlPDAllPages	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                // MDIParent.InstancePtr.CommonDialog1.Flags = 0 /*? MDIParent.InstancePtr.CommonDialog1.Flags */	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"+vbPorterConverter.cdlPDSelection	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                //Information.Err().Clear();
				//FC:FINAL:MSH - i.issue #1351: incorrect element name
				//FC:FINAL:DDU:#i1290 - fixed null exception
				//if (MDIParent.InstancePtr.CommonDialog1_Printer == null)
				//{
				//	MDIParent.InstancePtr.CommonDialog1_Printer = new FCCommonDialog();
				//}
				//MDIParent.InstancePtr.CommonDialog1_Printer.ShowPrinter();
				//FC:FINAL:DDU:#i1563 - initialize CommonDialog1
				//MDIParent.InstancePtr.CommonDialog1 = new FCCommonDialog();
				//FC:FINAL:MSH - i.issue #1351: restore CancelError to avoid printing if 'Cancel' clicked
				//MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				//            MDIParent.InstancePtr.CommonDialog1.ShowPrinter();
				//            if (Information.Err().Number == 0)
				//            {
				//	//FC:FINAL:DDU:#i1290 - fixed null exception
				//	if (MDIParent.InstancePtr.CommonDialog1 == null)
				//	{
				//		MDIParent.InstancePtr.CommonDialog1 = new FCCommonDialog();
				//	}
				rtbText.SelPrint(0);
				//                FCGlobal.Printer.EndDoc();
				//            }
				// End If
				return;
            }
            catch (Exception ex)
            {   // ErrorHandler:
                if (!(Information.Err(ex).Number == 32755))
                {
                    MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuPrint_click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
        }

        private void mnuSave_Click(object sender, System.EventArgs e)
        {
            SaveComment();
        }

        private bool SaveComment()
        {
            bool SaveComment = false;
            clsDRWrapper clsSave = new clsDRWrapper();

            try
            {   // On Error GoTo ErrorHandler

                SaveComment = false;
                clsSave.OpenRecordset("select * from " + strTbl + " where " + strIDFld + " = " + FCConvert.ToString(lngIDNumber), "tw" + strMod + "0000.vb1");
                if (!clsSave.EndOfFile())
                {
                    clsSave.Edit();
                }
                else
                {
                    clsSave.AddNew();
                    clsSave.Set_Fields(strIDFld, lngIDNumber);
                }

                clsSave.Set_Fields(strFld, rtbText.Text);
                clsSave.Update();
                strComment = Strings.Trim(rtbText.Text);
                if (Strings.Trim(rtbText.Text) == string.Empty)
                {
                    boolBlankComment = true;
                }
                else
                {
                    boolBlankComment = false;
                }
                SaveComment = true;
                MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return SaveComment;
            }
            catch (Exception ex)
            {   // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveComment", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return SaveComment;
        }

        private void mnuSaveExit_Click(object sender, System.EventArgs e)
        {
            if (SaveComment())
            {
                mnuExit_Click();
            }
        }

    }
}