//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;

#if TWCR0000
using TWCR0000;


#elif TWBD0000
using TWBD0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmUTCLStatus.
	/// </summary>
	partial class frmUTCLStatus : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCButton> cmdDisc;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCFrame fraEditNote;
		public fecherFoundation.FCTextBox txtNote;
		public fecherFoundation.FCCheckBox chkPriority;
		public fecherFoundation.FCButton cmdNoteProcess;
		public fecherFoundation.FCButton cmdNoteCancel;
		public fecherFoundation.FCLabel lblRemainChars;
		public fecherFoundation.FCLabel lblRemCharsLabel;
		public fecherFoundation.FCLabel lblNote;
		public fecherFoundation.FCFrame fraRateInfo;
		public FCGrid vsRateInfo;
		public fecherFoundation.FCButton cmdRIClose;
		public fecherFoundation.FCFrame fraDiscount;
		public fecherFoundation.FCTextBox txtDiscDisc;
		public fecherFoundation.FCTextBox txtDiscPrin;
		public fecherFoundation.FCButton cmdDisc_2;
		public fecherFoundation.FCButton cmdDisc_1;
		public fecherFoundation.FCButton cmdDisc_0;
		public fecherFoundation.FCLabel lblOriginalTax;
		public fecherFoundation.FCLabel lblOriginalTaxLabel;
		public fecherFoundation.FCLabel lblDiscHiddenTotal;
		public fecherFoundation.FCLabel lblDiscTotal;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel lblDiscountInstructions;
		public fecherFoundation.FCPanel fraPayment;
		public fecherFoundation.FCMenuStrip MainMenu1;
		public FCGrid vsPayments;
		public fecherFoundation.FCTextBox txtTax;
		public fecherFoundation.FCTextBox txtTotalPendingDue;
		public fecherFoundation.FCTextBox txtCash;
		public fecherFoundation.FCTextBox txtCD;
		public fecherFoundation.FCComboBox cmbBillNumber;
		public fecherFoundation.FCTextBox txtPrincipal;
		public fecherFoundation.FCTextBox txtCosts;
		public fecherFoundation.FCTextBox txtReference;
		public fecherFoundation.FCTextBox txtTransactionDate2;
		public fecherFoundation.FCTextBox txtInterest;
		public fecherFoundation.FCTextBox txtComments;
		public fecherFoundation.FCTextBox txtBLID;
		public fecherFoundation.FCComboBox cmbCode;
		public fecherFoundation.FCComboBox cmbService;
		public Global.T2KDateBox txtTransactionDate;
		public FCGrid vsPeriod;
		public FCGrid txtAcctNumber;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel lblTax;
		public fecherFoundation.FCLabel lblTaxClub;
		public fecherFoundation.FCLabel Label1_9;
		public fecherFoundation.FCLabel lblTotalPendingDue;
		public fecherFoundation.FCLabel Label1_11;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel lblPrincipal;
		public fecherFoundation.FCLabel lblInterest;
		public fecherFoundation.FCLabel lblCosts;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label1_12;
		public fecherFoundation.FCPanel fraStatusLabels;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblOwnersName;
		public fecherFoundation.FCLabel lblMapLot;
		public fecherFoundation.FCLabel lblLocation;
		public FCCommonDialog CommonDialog1;
		public FCGrid WGRID;
		public FCGrid vsPreview;
		public FCGrid SGRID;
		public fecherFoundation.FCLabel lblGrpInfo;
		public fecherFoundation.FCLabel lblAcctComment;
		public fecherFoundation.FCLabel lblTaxAcquired;
		public fecherFoundation.FCLabel lblSewerHeading;
		public fecherFoundation.FCLabel lblWaterHeading;
		public fecherFoundation.FCPictureBox imgNote;
		public fecherFoundation.FCLabel lblPaymentInfo;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileShowMeterInfo;
		public fecherFoundation.FCToolStripMenuItem mnuFileAccountInfo;
		public fecherFoundation.FCToolStripMenuItem mnuFileSwitch;
		public fecherFoundation.FCToolStripMenuItem mnuFileShowSplitView;
		public fecherFoundation.FCToolStripMenuItem mnuFileComment;
		public fecherFoundation.FCToolStripMenuItem mnuFileEditNote;
		public fecherFoundation.FCToolStripMenuItem mnuFilePriority;
		public fecherFoundation.FCToolStripMenuItem mnuPayment;
		public fecherFoundation.FCToolStripMenuItem mnuFileOptionsLoadValidAccount;
		public fecherFoundation.FCToolStripMenuItem mnuPaymentClearPayment;
		public fecherFoundation.FCToolStripMenuItem mnuPaymentClearList;
		public fecherFoundation.FCToolStripMenuItem mnuFileDischarge;
		public fecherFoundation.FCToolStripMenuItem mnuFileConsumptionReport;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrintRateInfo;
		public fecherFoundation.FCToolStripMenuItem mnuPaymentSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuSpacer;
		public fecherFoundation.FCToolStripMenuItem mnuProcessExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUTCLStatus));
			this.fraEditNote = new fecherFoundation.FCFrame();
			this.txtNote = new fecherFoundation.FCTextBox();
			this.chkPriority = new fecherFoundation.FCCheckBox();
			this.cmdNoteProcess = new fecherFoundation.FCButton();
			this.cmdNoteCancel = new fecherFoundation.FCButton();
			this.lblRemainChars = new fecherFoundation.FCLabel();
			this.lblRemCharsLabel = new fecherFoundation.FCLabel();
			this.lblNote = new fecherFoundation.FCLabel();
			this.fraRateInfo = new fecherFoundation.FCFrame();
			this.vsRateInfo = new fecherFoundation.FCGrid();
			this.cmdRIClose = new fecherFoundation.FCButton();
			this.fraDiscount = new fecherFoundation.FCFrame();
			this.txtDiscDisc = new fecherFoundation.FCTextBox();
			this.txtDiscPrin = new fecherFoundation.FCTextBox();
			this.cmdDisc_2 = new fecherFoundation.FCButton();
			this.cmdDisc_1 = new fecherFoundation.FCButton();
			this.cmdDisc_0 = new fecherFoundation.FCButton();
			this.lblOriginalTax = new fecherFoundation.FCLabel();
			this.lblOriginalTaxLabel = new fecherFoundation.FCLabel();
			this.lblDiscHiddenTotal = new fecherFoundation.FCLabel();
			this.lblDiscTotal = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.lblDiscountInstructions = new fecherFoundation.FCLabel();
			this.fraPayment = new fecherFoundation.FCPanel();
			this.vsPayments = new fecherFoundation.FCGrid();
			this.txtTax = new fecherFoundation.FCTextBox();
			this.txtTotalPendingDue = new fecherFoundation.FCTextBox();
			this.txtCash = new fecherFoundation.FCTextBox();
			this.txtCD = new fecherFoundation.FCTextBox();
			this.cmbBillNumber = new fecherFoundation.FCComboBox();
			this.txtPrincipal = new fecherFoundation.FCTextBox();
			this.txtCosts = new fecherFoundation.FCTextBox();
			this.txtReference = new fecherFoundation.FCTextBox();
			this.txtTransactionDate2 = new fecherFoundation.FCTextBox();
			this.txtInterest = new fecherFoundation.FCTextBox();
			this.txtComments = new fecherFoundation.FCTextBox();
			this.txtBLID = new fecherFoundation.FCTextBox();
			this.cmbCode = new fecherFoundation.FCComboBox();
			this.cmbService = new fecherFoundation.FCComboBox();
			this.txtTransactionDate = new Global.T2KDateBox();
			this.txtAcctNumber = new fecherFoundation.FCGrid();
			this.Label1_8 = new fecherFoundation.FCLabel();
			this.Label1_4 = new fecherFoundation.FCLabel();
			this.lblTax = new fecherFoundation.FCLabel();
			this.lblTaxClub = new fecherFoundation.FCLabel();
			this.Label1_9 = new fecherFoundation.FCLabel();
			this.lblTotalPendingDue = new fecherFoundation.FCLabel();
			this.Label1_11 = new fecherFoundation.FCLabel();
			this.Label1_1 = new fecherFoundation.FCLabel();
			this.Label1_2 = new fecherFoundation.FCLabel();
			this.lblPrincipal = new fecherFoundation.FCLabel();
			this.lblInterest = new fecherFoundation.FCLabel();
			this.lblCosts = new fecherFoundation.FCLabel();
			this.Label1_10 = new fecherFoundation.FCLabel();
			this.Label1_3 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label1_12 = new fecherFoundation.FCLabel();
			this.vsPeriod = new fecherFoundation.FCGrid();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFileAccountInfo = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileShowMeterInfo = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSwitch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileShowSplitView = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileComment = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileEditNote = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePriority = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPayment = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileOptionsLoadValidAccount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPaymentClearList = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileDischarge = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileConsumptionReport = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrintRateInfo = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPaymentClearPayment = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPaymentSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSpacer = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessExit = new fecherFoundation.FCToolStripMenuItem();
			this.fraStatusLabels = new fecherFoundation.FCPanel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label1_0 = new fecherFoundation.FCLabel();
			this.lblOwnersName = new fecherFoundation.FCLabel();
			this.lblMapLot = new fecherFoundation.FCLabel();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.lblLocation = new fecherFoundation.FCLabel();
			this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
			this.WGRID = new fecherFoundation.FCGrid();
			this.vsPreview = new fecherFoundation.FCGrid();
			this.SGRID = new fecherFoundation.FCGrid();
			this.lblGrpInfo = new fecherFoundation.FCLabel();
			this.lblAcctComment = new fecherFoundation.FCLabel();
			this.lblTaxAcquired = new fecherFoundation.FCLabel();
			this.lblSewerHeading = new fecherFoundation.FCLabel();
			this.lblWaterHeading = new fecherFoundation.FCLabel();
			this.imgNote = new fecherFoundation.FCPictureBox();
			this.lblPaymentInfo = new fecherFoundation.FCLabel();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdProcessEffective = new fecherFoundation.FCButton();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.cmdProcessGoTo = new fecherFoundation.FCButton();
			this.cmdPaymentPreview = new fecherFoundation.FCButton();
			this.cmdProcessChangeAccount = new fecherFoundation.FCButton();
			this.cmdFileMortgageHolderInfo = new fecherFoundation.FCButton();
			this.cmdSavePayments = new fecherFoundation.FCButton();
			this.cmdPaymentSaveExit = new fecherFoundation.FCButton();
			this.lblACHInfo = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraEditNote)).BeginInit();
			this.fraEditNote.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPriority)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNoteProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNoteCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).BeginInit();
			this.fraRateInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsRateInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRIClose)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDiscount)).BeginInit();
			this.fraDiscount.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDisc_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDisc_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDisc_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPayment)).BeginInit();
			this.fraPayment.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsPayments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTransactionDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcctNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraStatusLabels)).BeginInit();
			this.fraStatusLabels.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.WGRID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsPreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SGRID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.imgNote)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessEffective)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessGoTo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPaymentPreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessChangeAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileMortgageHolderInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSavePayments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPaymentSaveExit)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSavePayments);
			this.BottomPanel.Controls.Add(this.cmdPaymentSaveExit);
			this.BottomPanel.Location = new System.Drawing.Point(0, 703);
			this.BottomPanel.Size = new System.Drawing.Size(1078, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblACHInfo);
			this.ClientArea.Controls.Add(this.SGRID);
			this.ClientArea.Controls.Add(this.fraEditNote);
			this.ClientArea.Controls.Add(this.fraDiscount);
			this.ClientArea.Controls.Add(this.fraPayment);
			this.ClientArea.Controls.Add(this.fraStatusLabels);
			this.ClientArea.Controls.Add(this.WGRID);
			this.ClientArea.Controls.Add(this.vsPreview);
			this.ClientArea.Controls.Add(this.lblSewerHeading);
			this.ClientArea.Controls.Add(this.lblWaterHeading);
			this.ClientArea.Controls.Add(this.lblPaymentInfo);
			this.ClientArea.Controls.Add(this.fraRateInfo);
			this.ClientArea.Size = new System.Drawing.Size(1078, 643);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdProcessEffective);
			this.TopPanel.Controls.Add(this.cmdProcessGoTo);
			this.TopPanel.Controls.Add(this.cmdFilePrint);
			this.TopPanel.Controls.Add(this.cmdPaymentPreview);
			this.TopPanel.Controls.Add(this.cmdProcessChangeAccount);
			this.TopPanel.Controls.Add(this.cmdFileMortgageHolderInfo);
			this.TopPanel.Controls.Add(this.lblTaxAcquired);
			this.TopPanel.Controls.Add(this.imgNote);
			this.TopPanel.Controls.Add(this.lblGrpInfo);
			this.TopPanel.Controls.Add(this.lblAcctComment);
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.TopPanel.Controls.SetChildIndex(this.lblAcctComment, 0);
			this.TopPanel.Controls.SetChildIndex(this.lblGrpInfo, 0);
			this.TopPanel.Controls.SetChildIndex(this.imgNote, 0);
			this.TopPanel.Controls.SetChildIndex(this.lblTaxAcquired, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileMortgageHolderInfo, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdProcessChangeAccount, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPaymentPreview, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdProcessGoTo, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdProcessEffective, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// fraEditNote
			// 
			this.fraEditNote.AppearanceKey = "groupBoxNoBorders";
			this.fraEditNote.BackColor = System.Drawing.Color.White;
			this.fraEditNote.Controls.Add(this.txtNote);
			this.fraEditNote.Controls.Add(this.chkPriority);
			this.fraEditNote.Controls.Add(this.cmdNoteProcess);
			this.fraEditNote.Controls.Add(this.cmdNoteCancel);
			this.fraEditNote.Controls.Add(this.lblRemainChars);
			this.fraEditNote.Controls.Add(this.lblRemCharsLabel);
			this.fraEditNote.Controls.Add(this.lblNote);
			this.fraEditNote.Location = new System.Drawing.Point(794, 83);
			this.fraEditNote.Name = "fraEditNote";
			this.fraEditNote.Size = new System.Drawing.Size(456, 391);
			this.fraEditNote.TabIndex = 58;
			this.fraEditNote.Visible = false;
			// 
			// txtNote
			// 
			this.txtNote.AcceptsReturn = true;
			this.txtNote.BackColor = System.Drawing.SystemColors.Window;
			this.txtNote.Location = new System.Drawing.Point(20, 85);
			this.txtNote.MaxLength = 255;
			this.txtNote.Multiline = true;
			this.txtNote.Name = "txtNote";
			this.txtNote.Size = new System.Drawing.Size(419, 140);
			this.txtNote.TabIndex = 72;
			this.txtNote.TextChanged += new System.EventHandler(this.txtNote_TextChanged);
			this.txtNote.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNote_KeyPress);
			// 
			// chkPriority
			// 
			this.chkPriority.Location = new System.Drawing.Point(20, 285);
			this.chkPriority.Name = "chkPriority";
			this.chkPriority.Size = new System.Drawing.Size(179, 23);
			this.chkPriority.TabIndex = 71;
			this.chkPriority.Text = "Display pop-up reminder?";
			// 
			// cmdNoteProcess
			// 
			this.cmdNoteProcess.AppearanceKey = "actionButton";
			this.cmdNoteProcess.Location = new System.Drawing.Point(20, 332);
			this.cmdNoteProcess.Name = "cmdNoteProcess";
			this.cmdNoteProcess.Size = new System.Drawing.Size(65, 40);
			this.cmdNoteProcess.TabIndex = 70;
			this.cmdNoteProcess.Text = "OK";
			this.cmdNoteProcess.Click += new System.EventHandler(this.cmdNoteProcess_Click);
			// 
			// cmdNoteCancel
			// 
			this.cmdNoteCancel.AppearanceKey = "actionButton";
			this.cmdNoteCancel.Location = new System.Drawing.Point(105, 332);
			this.cmdNoteCancel.Name = "cmdNoteCancel";
			this.cmdNoteCancel.Size = new System.Drawing.Size(65, 40);
			this.cmdNoteCancel.TabIndex = 69;
			this.cmdNoteCancel.Text = "Cancel";
			this.cmdNoteCancel.Click += new System.EventHandler(this.cmdNoteCancel_Click);
			// 
			// lblRemainChars
			// 
			this.lblRemainChars.Location = new System.Drawing.Point(174, 247);
			this.lblRemainChars.Name = "lblRemainChars";
			this.lblRemainChars.Size = new System.Drawing.Size(25, 18);
			this.lblRemainChars.TabIndex = 76;
			this.lblRemainChars.Text = "255";
			// 
			// lblRemCharsLabel
			// 
			this.lblRemCharsLabel.Location = new System.Drawing.Point(20, 247);
			this.lblRemCharsLabel.Name = "lblRemCharsLabel";
			this.lblRemCharsLabel.Size = new System.Drawing.Size(147, 18);
			this.lblRemCharsLabel.TabIndex = 75;
			this.lblRemCharsLabel.Text = "CHARACTERS REMAINING";
			// 
			// lblNote
			// 
			this.lblNote.Location = new System.Drawing.Point(20, 30);
			this.lblNote.Name = "lblNote";
			this.lblNote.Size = new System.Drawing.Size(419, 35);
			this.lblNote.TabIndex = 73;
			this.lblNote.Text = "EDIT THE TEXT BELOW TO CHANGE IT OR CLEAR THE TEXT TO REMOVE THE NOTE FROM THE AC" +
    "COUNT";
			// 
			// fraRateInfo
			// 
			this.fraRateInfo.BackColor = System.Drawing.SystemColors.Window;
			this.fraRateInfo.Controls.Add(this.vsRateInfo);
			this.fraRateInfo.Controls.Add(this.cmdRIClose);
			this.fraRateInfo.Location = new System.Drawing.Point(748, 490);
			this.fraRateInfo.Name = "fraRateInfo";
			this.fraRateInfo.Size = new System.Drawing.Size(1048, 660);
			this.fraRateInfo.TabIndex = 48;
			this.fraRateInfo.Text = "Year Information";
			this.fraRateInfo.Visible = false;
			// 
			// vsRateInfo
			// 
			this.vsRateInfo.Cols = 5;
			this.vsRateInfo.ColumnHeadersVisible = false;
			this.vsRateInfo.FixedCols = 0;
			this.vsRateInfo.FixedRows = 0;
			this.vsRateInfo.Location = new System.Drawing.Point(20, 30);
			this.vsRateInfo.Name = "vsRateInfo";
			this.vsRateInfo.RowHeadersVisible = false;
			this.vsRateInfo.Rows = 20;
			this.vsRateInfo.ShowFocusCell = false;
			this.vsRateInfo.Size = new System.Drawing.Size(1008, 550);
			this.vsRateInfo.TabIndex = 62;
			this.vsRateInfo.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsRateInfo_MouseMoveEvent);
			this.vsRateInfo.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsRateInfo_BeforeEdit);
			this.vsRateInfo.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsRateInfo_ValidateEdit);
			this.vsRateInfo.CurrentCellChanged += new System.EventHandler(this.vsRateInfo_RowColChange);
			this.vsRateInfo.DoubleClick += new System.EventHandler(this.vsRateInfo_DblClick);
			// 
			// cmdRIClose
			// 
			this.cmdRIClose.AppearanceKey = "actionButton";
			this.cmdRIClose.Location = new System.Drawing.Point(459, 600);
			this.cmdRIClose.Name = "cmdRIClose";
			this.cmdRIClose.Size = new System.Drawing.Size(83, 40);
			this.cmdRIClose.TabIndex = 49;
			this.cmdRIClose.Text = "Close";
			this.cmdRIClose.Click += new System.EventHandler(this.cmdRIClose_Click);
			// 
			// fraDiscount
			// 
			this.fraDiscount.AppearanceKey = "groupBoxNoBorders";
			this.fraDiscount.Controls.Add(this.txtDiscDisc);
			this.fraDiscount.Controls.Add(this.txtDiscPrin);
			this.fraDiscount.Controls.Add(this.cmdDisc_2);
			this.fraDiscount.Controls.Add(this.cmdDisc_1);
			this.fraDiscount.Controls.Add(this.cmdDisc_0);
			this.fraDiscount.Controls.Add(this.lblOriginalTax);
			this.fraDiscount.Controls.Add(this.lblOriginalTaxLabel);
			this.fraDiscount.Controls.Add(this.lblDiscHiddenTotal);
			this.fraDiscount.Controls.Add(this.lblDiscTotal);
			this.fraDiscount.Controls.Add(this.Label11);
			this.fraDiscount.Controls.Add(this.Label10);
			this.fraDiscount.Controls.Add(this.Label9);
			this.fraDiscount.Controls.Add(this.lblDiscountInstructions);
			this.fraDiscount.Location = new System.Drawing.Point(863, 43);
			this.fraDiscount.Name = "fraDiscount";
			this.fraDiscount.Size = new System.Drawing.Size(262, 334);
			this.fraDiscount.TabIndex = 36;
			this.fraDiscount.Visible = false;
			// 
			// txtDiscDisc
			// 
			this.txtDiscDisc.BackColor = System.Drawing.SystemColors.Window;
			this.txtDiscDisc.Location = new System.Drawing.Point(112, 176);
			this.txtDiscDisc.MaxLength = 12;
			this.txtDiscDisc.Name = "txtDiscDisc";
			this.txtDiscDisc.Size = new System.Drawing.Size(93, 40);
			this.txtDiscDisc.TabIndex = 46;
			this.txtDiscDisc.Validating += new System.ComponentModel.CancelEventHandler(this.txtDiscDisc_Validating);
			this.txtDiscDisc.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDiscDisc_KeyPress);
			// 
			// txtDiscPrin
			// 
			this.txtDiscPrin.BackColor = System.Drawing.SystemColors.Window;
			this.txtDiscPrin.Location = new System.Drawing.Point(112, 116);
			this.txtDiscPrin.MaxLength = 12;
			this.txtDiscPrin.Name = "txtDiscPrin";
			this.txtDiscPrin.Size = new System.Drawing.Size(93, 40);
			this.txtDiscPrin.TabIndex = 41;
			this.txtDiscPrin.Validating += new System.ComponentModel.CancelEventHandler(this.txtDiscPrin_Validating);
			this.txtDiscPrin.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDiscPrin_KeyPress);
			// 
			// cmdDisc_2
			// 
			this.cmdDisc_2.AppearanceKey = "actionButton";
			this.cmdDisc_2.Location = new System.Drawing.Point(178, 274);
			this.cmdDisc_2.Name = "cmdDisc_2";
			this.cmdDisc_2.Size = new System.Drawing.Size(64, 40);
			this.cmdDisc_2.TabIndex = 40;
			this.cmdDisc_2.Text = "Cancel";
			this.cmdDisc_2.Click += new System.EventHandler(this.cmdDisc_Click);
			// 
			// cmdDisc_1
			// 
			this.cmdDisc_1.AppearanceKey = "actionButton";
			this.cmdDisc_1.Location = new System.Drawing.Point(99, 274);
			this.cmdDisc_1.Name = "cmdDisc_1";
			this.cmdDisc_1.Size = new System.Drawing.Size(59, 40);
			this.cmdDisc_1.TabIndex = 39;
			this.cmdDisc_1.Text = "No";
			this.cmdDisc_1.Click += new System.EventHandler(this.cmdDisc_Click);
			// 
			// cmdDisc_0
			// 
			this.cmdDisc_0.AppearanceKey = "actionButton";
			this.cmdDisc_0.Location = new System.Drawing.Point(20, 274);
			this.cmdDisc_0.Name = "cmdDisc_0";
			this.cmdDisc_0.Size = new System.Drawing.Size(59, 40);
			this.cmdDisc_0.TabIndex = 38;
			this.cmdDisc_0.Text = "Yes";
			this.cmdDisc_0.Click += new System.EventHandler(this.cmdDisc_Click);
			// 
			// lblOriginalTax
			// 
			this.lblOriginalTax.Location = new System.Drawing.Point(116, 78);
			this.lblOriginalTax.Name = "lblOriginalTax";
			this.lblOriginalTax.Size = new System.Drawing.Size(84, 18);
			this.lblOriginalTax.TabIndex = 54;
			this.lblOriginalTax.Text = "0.00";
			this.lblOriginalTax.TextAlign = System.Drawing.ContentAlignment.TopRight;
			this.ToolTip1.SetToolTip(this.lblOriginalTax, "Double click to discount the original tax amount.");
			this.lblOriginalTax.DoubleClick += new System.EventHandler(this.lblOriginalTax_DoubleClick);
			// 
			// lblOriginalTaxLabel
			// 
			this.lblOriginalTaxLabel.Location = new System.Drawing.Point(20, 78);
			this.lblOriginalTaxLabel.Name = "lblOriginalTaxLabel";
			this.lblOriginalTaxLabel.Size = new System.Drawing.Size(90, 18);
			this.lblOriginalTaxLabel.TabIndex = 53;
			this.lblOriginalTaxLabel.Text = "ORIGINAL TAX";
			this.ToolTip1.SetToolTip(this.lblOriginalTaxLabel, "Double click to discount the original tax amount.");
			this.lblOriginalTaxLabel.DoubleClick += new System.EventHandler(this.lblOriginalTaxLabel_DoubleClick);
			// 
			// lblDiscHiddenTotal
			// 
			this.lblDiscHiddenTotal.Location = new System.Drawing.Point(14, 282);
			this.lblDiscHiddenTotal.Name = "lblDiscHiddenTotal";
			this.lblDiscHiddenTotal.Size = new System.Drawing.Size(57, 25);
			this.lblDiscHiddenTotal.TabIndex = 47;
			this.lblDiscHiddenTotal.Visible = false;
			// 
			// lblDiscTotal
			// 
			this.lblDiscTotal.Location = new System.Drawing.Point(112, 236);
			this.lblDiscTotal.Name = "lblDiscTotal";
			this.lblDiscTotal.Size = new System.Drawing.Size(103, 18);
			this.lblDiscTotal.TabIndex = 45;
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(20, 236);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(50, 18);
			this.Label11.TabIndex = 44;
			this.Label11.Text = "TOTAL";
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(20, 190);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(69, 18);
			this.Label10.TabIndex = 43;
			this.Label10.Text = "DISCOUNT";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(20, 130);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(69, 18);
			this.Label9.TabIndex = 42;
			this.Label9.Text = "PRINCIPAL";
			// 
			// lblDiscountInstructions
			// 
			this.lblDiscountInstructions.Location = new System.Drawing.Point(20, 30);
			this.lblDiscountInstructions.Name = "lblDiscountInstructions";
			this.lblDiscountInstructions.Size = new System.Drawing.Size(218, 28);
			this.lblDiscountInstructions.TabIndex = 37;
			this.lblDiscountInstructions.Text = "IS THIS PRINCIPAL PAYMENT AND DISCOUNT CORRECT?";
			this.lblDiscountInstructions.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// fraPayment
			// 
			this.fraPayment.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fraPayment.AppearanceKey = "groupBoxNoBorders";
			this.fraPayment.Controls.Add(this.vsPayments);
			this.fraPayment.Controls.Add(this.txtTax);
			this.fraPayment.Controls.Add(this.txtTotalPendingDue);
			this.fraPayment.Controls.Add(this.txtCash);
			this.fraPayment.Controls.Add(this.txtCD);
			this.fraPayment.Controls.Add(this.cmbBillNumber);
			this.fraPayment.Controls.Add(this.txtPrincipal);
			this.fraPayment.Controls.Add(this.txtCosts);
			this.fraPayment.Controls.Add(this.txtReference);
			this.fraPayment.Controls.Add(this.txtTransactionDate2);
			this.fraPayment.Controls.Add(this.txtInterest);
			this.fraPayment.Controls.Add(this.txtComments);
			this.fraPayment.Controls.Add(this.txtBLID);
			this.fraPayment.Controls.Add(this.cmbCode);
			this.fraPayment.Controls.Add(this.cmbService);
			this.fraPayment.Controls.Add(this.txtTransactionDate);
			this.fraPayment.Controls.Add(this.txtAcctNumber);
			this.fraPayment.Controls.Add(this.Label1_8);
			this.fraPayment.Controls.Add(this.Label1_4);
			this.fraPayment.Controls.Add(this.lblTax);
			this.fraPayment.Controls.Add(this.lblTaxClub);
			this.fraPayment.Controls.Add(this.Label1_9);
			this.fraPayment.Controls.Add(this.lblTotalPendingDue);
			this.fraPayment.Controls.Add(this.Label1_11);
			this.fraPayment.Controls.Add(this.Label1_1);
			this.fraPayment.Controls.Add(this.Label1_2);
			this.fraPayment.Controls.Add(this.lblPrincipal);
			this.fraPayment.Controls.Add(this.lblInterest);
			this.fraPayment.Controls.Add(this.lblCosts);
			this.fraPayment.Controls.Add(this.Label1_10);
			this.fraPayment.Controls.Add(this.Label1_3);
			this.fraPayment.Controls.Add(this.Label8);
			this.fraPayment.Controls.Add(this.Label1_12);
			this.fraPayment.Controls.Add(this.vsPeriod);
			this.fraPayment.Location = new System.Drawing.Point(10, 528);
			this.fraPayment.Name = "fraPayment";
			this.fraPayment.Size = new System.Drawing.Size(976, 348);
			this.fraPayment.TabIndex = 22;
			this.fraPayment.Visible = false;
			// 
			// vsPayments
			// 
			this.vsPayments.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsPayments.Cols = 11;
			this.vsPayments.FixedCols = 0;
			this.vsPayments.Location = new System.Drawing.Point(20, 214);
			this.vsPayments.Name = "vsPayments";
			this.vsPayments.RowHeadersVisible = false;
			this.vsPayments.Rows = 1;
			this.vsPayments.ShowFocusCell = false;
			this.vsPayments.Size = new System.Drawing.Size(936, 114);
			this.vsPayments.TabIndex = 63;
			this.vsPayments.KeyDown += new Wisej.Web.KeyEventHandler(this.vsPayments_KeyDownEvent);
			// 
			// txtTax
			// 
			this.txtTax.BackColor = System.Drawing.SystemColors.Window;
			this.txtTax.Location = new System.Drawing.Point(839, 115);
			this.txtTax.MaxLength = 12;
			this.txtTax.Name = "txtTax";
			this.txtTax.Size = new System.Drawing.Size(78, 40);
			this.txtTax.TabIndex = 55;
			this.txtTax.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtTax.Enter += new System.EventHandler(this.txtTax_Enter);
			this.txtTax.Validating += new System.ComponentModel.CancelEventHandler(this.txtTax_Validating);
			this.txtTax.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTax_KeyDown);
			this.txtTax.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTax_KeyPress);
			// 
			// txtTotalPendingDue
			// 
			this.txtTotalPendingDue.BackColor = System.Drawing.SystemColors.Window;
			this.txtTotalPendingDue.Location = new System.Drawing.Point(694, 30);
			this.txtTotalPendingDue.Name = "txtTotalPendingDue";
			this.txtTotalPendingDue.Size = new System.Drawing.Size(87, 40);
			this.txtTotalPendingDue.TabIndex = 11;
			this.txtTotalPendingDue.Text = "0.00";
			this.txtTotalPendingDue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtTotalPendingDue.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTotalPendingDue_KeyPress);
			// 
			// txtCash
			// 
			this.txtCash.Appearance = 0;
			this.txtCash.BackColor = System.Drawing.Color.FromArgb(192, 192, 192);
			this.txtCash.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtCash.Enabled = false;
			this.txtCash.Location = new System.Drawing.Point(556, 115);
			this.txtCash.MaxLength = 1;
			this.txtCash.Name = "txtCash";
			this.txtCash.Size = new System.Drawing.Size(40, 40);
			this.txtCash.TabIndex = 4;
			this.txtCash.Text = "Y";
			this.txtCash.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtCash.Enter += new System.EventHandler(this.txtCash_Enter);
			this.txtCash.DoubleClick += new System.EventHandler(this.txtCash_DoubleClick);
			this.txtCash.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCash_KeyDown);
			this.txtCash.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCash_KeyPress);
			// 
			// txtCD
			// 
			this.txtCD.Appearance = 0;
			this.txtCD.BackColor = System.Drawing.Color.FromArgb(192, 192, 192);
			this.txtCD.BorderStyle = Wisej.Web.BorderStyle.None;
			this.txtCD.Location = new System.Drawing.Point(511, 115);
			this.txtCD.MaxLength = 1;
			this.txtCD.Name = "txtCD";
			this.txtCD.Size = new System.Drawing.Size(40, 40);
			this.txtCD.TabIndex = 3;
			this.txtCD.Text = "Y";
			this.txtCD.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.txtCD.Enter += new System.EventHandler(this.txtCD_Enter);
			this.txtCD.DoubleClick += new System.EventHandler(this.txtCD_DoubleClick);
			this.txtCD.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCD_KeyDown);
			this.txtCD.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCD_KeyPress);
			// 
			// cmbBillNumber
			// 
			this.cmbBillNumber.BackColor = System.Drawing.SystemColors.Window;
			this.cmbBillNumber.Location = new System.Drawing.Point(125, 115);
			this.cmbBillNumber.Name = "cmbBillNumber";
			this.cmbBillNumber.Size = new System.Drawing.Size(88, 40);
			this.cmbBillNumber.TabIndex = 12;
			this.cmbBillNumber.SelectedIndexChanged += new System.EventHandler(this.cmbBillNumber_SelectedIndexChanged);
			this.cmbBillNumber.DropDown += new System.EventHandler(this.cmbBillNumber_DropDown);
			this.cmbBillNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbBillNumber_KeyDown);
			// 
			// txtPrincipal
			// 
			this.txtPrincipal.BackColor = System.Drawing.SystemColors.Window;
			this.txtPrincipal.Location = new System.Drawing.Point(751, 115);
			this.txtPrincipal.MaxLength = 17;
			this.txtPrincipal.Name = "txtPrincipal";
			this.txtPrincipal.Size = new System.Drawing.Size(78, 40);
			this.txtPrincipal.TabIndex = 7;
			this.txtPrincipal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtPrincipal.Enter += new System.EventHandler(this.txtPrincipal_Enter);
			this.txtPrincipal.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrincipal_Validating);
			this.txtPrincipal.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPrincipal_KeyDown);
			this.txtPrincipal.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPrincipal_KeyPress);
			// 
			// txtCosts
			// 
			this.txtCosts.BackColor = System.Drawing.SystemColors.Window;
			this.txtCosts.Location = new System.Drawing.Point(1017, 115);
			this.txtCosts.MaxLength = 11;
			this.txtCosts.Name = "txtCosts";
			this.txtCosts.Size = new System.Drawing.Size(78, 40);
			this.txtCosts.TabIndex = 9;
			this.txtCosts.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtCosts.Enter += new System.EventHandler(this.txtCosts_Enter);
			this.txtCosts.Validating += new System.ComponentModel.CancelEventHandler(this.txtCosts_Validating);
			this.txtCosts.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCosts_KeyDown);
			this.txtCosts.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCosts_KeyPress);
			// 
			// txtReference
			// 
			this.txtReference.BackColor = System.Drawing.SystemColors.Window;
			this.txtReference.Location = new System.Drawing.Point(343, 115);
			this.txtReference.MaxLength = 6;
			this.txtReference.Name = "txtReference";
			this.txtReference.Size = new System.Drawing.Size(88, 40);
			this.txtReference.TabIndex = 64;
			this.txtReference.Enter += new System.EventHandler(this.txtReference_Enter);
			this.txtReference.Validating += new System.ComponentModel.CancelEventHandler(this.txtReference_Validating);
			this.txtReference.KeyDown += new Wisej.Web.KeyEventHandler(this.txtReference_KeyDown);
			// 
			// txtTransactionDate2
			// 
			this.txtTransactionDate2.BackColor = System.Drawing.SystemColors.Window;
			this.txtTransactionDate2.Location = new System.Drawing.Point(493, 30);
			this.txtTransactionDate2.MaxLength = 10;
			this.txtTransactionDate2.Name = "txtTransactionDate2";
			this.txtTransactionDate2.Size = new System.Drawing.Size(55, 40);
			this.txtTransactionDate2.TabIndex = 13;
			this.txtTransactionDate2.Visible = false;
			// 
			// txtInterest
			// 
			this.txtInterest.BackColor = System.Drawing.SystemColors.Window;
			this.txtInterest.Location = new System.Drawing.Point(923, 115);
			this.txtInterest.MaxLength = 12;
			this.txtInterest.Name = "txtInterest";
			this.txtInterest.Size = new System.Drawing.Size(78, 40);
			this.txtInterest.TabIndex = 8;
			this.txtInterest.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtInterest.Enter += new System.EventHandler(this.txtInterest_Enter);
			this.txtInterest.Validating += new System.ComponentModel.CancelEventHandler(this.txtInterest_Validating);
			this.txtInterest.KeyDown += new Wisej.Web.KeyEventHandler(this.txtInterest_KeyDown);
			this.txtInterest.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtInterest_KeyPress);
			// 
			// txtComments
			// 
			this.txtComments.BackColor = System.Drawing.SystemColors.Window;
			this.txtComments.Location = new System.Drawing.Point(410, 165);
			this.txtComments.MaxLength = 255;
			this.txtComments.Name = "txtComments";
			this.txtComments.Size = new System.Drawing.Size(455, 40);
			this.txtComments.TabIndex = 10;
			this.txtComments.KeyDown += new Wisej.Web.KeyEventHandler(this.txtComments_KeyDown);
			// 
			// txtBLID
			// 
			this.txtBLID.BackColor = System.Drawing.SystemColors.Window;
			this.txtBLID.Location = new System.Drawing.Point(20, 217);
			this.txtBLID.Name = "txtBLID";
			this.txtBLID.Size = new System.Drawing.Size(152, 40);
			this.txtBLID.TabIndex = 23;
			this.txtBLID.Visible = false;
			// 
			// cmbCode
			// 
			this.cmbCode.BackColor = System.Drawing.SystemColors.Window;
			this.cmbCode.Location = new System.Drawing.Point(441, 115);
			this.cmbCode.Name = "cmbCode";
			this.cmbCode.Size = new System.Drawing.Size(60, 40);
			this.cmbCode.Sorted = true;
			this.cmbCode.TabIndex = 2;
			this.cmbCode.SelectedIndexChanged += new System.EventHandler(this.cmbCode_SelectedIndexChanged);
			this.cmbCode.DropDown += new System.EventHandler(this.cmbCode_DropDown);
			this.cmbCode.Enter += new System.EventHandler(this.cmbCode_Enter);
			this.cmbCode.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbCode_KeyDown);
			// 
			// cmbService
			// 
			this.cmbService.BackColor = System.Drawing.SystemColors.Window;
			this.cmbService.Location = new System.Drawing.Point(20, 115);
			this.cmbService.Name = "cmbService";
			this.cmbService.Size = new System.Drawing.Size(95, 40);
			this.cmbService.Sorted = true;
			this.cmbService.TabIndex = 1;
			this.cmbService.SelectedIndexChanged += new System.EventHandler(this.cmbService_SelectedIndexChanged);
			this.cmbService.DropDown += new System.EventHandler(this.cmbService_DropDown);
			this.cmbService.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbService_KeyDown);
			// 
			// txtTransactionDate
			// 
			this.txtTransactionDate.Location = new System.Drawing.Point(223, 115);
			this.txtTransactionDate.Mask = "##/##/####";
			this.txtTransactionDate.MaxLength = 10;
			this.txtTransactionDate.Name = "txtTransactionDate";
			this.txtTransactionDate.Size = new System.Drawing.Size(113, 22);
			this.txtTransactionDate.TabIndex = 51;
			this.txtTransactionDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtTransactionDate_Validate);
			this.txtTransactionDate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTransactionDate_KeyDownEvent);
			// 
			// txtAcctNumber
			// 
			this.txtAcctNumber.Cols = 1;
			this.txtAcctNumber.ColumnHeadersVisible = false;
			this.txtAcctNumber.FixedCols = 0;
			this.txtAcctNumber.FixedRows = 0;
			this.txtAcctNumber.Location = new System.Drawing.Point(603, 115);
			this.txtAcctNumber.Name = "txtAcctNumber";
			this.txtAcctNumber.RowHeadersVisible = false;
			this.txtAcctNumber.Rows = 1;
			this.txtAcctNumber.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
			this.txtAcctNumber.ShowFocusCell = false;
			this.txtAcctNumber.Size = new System.Drawing.Size(140, 42);
			this.txtAcctNumber.TabIndex = 6;
			this.txtAcctNumber.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.txtAcctNumber_ValidateEdit);
			this.txtAcctNumber.Enter += new System.EventHandler(this.txtAcctNumber_Enter);
			this.txtAcctNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtAcctNumber_Validating);
			this.txtAcctNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAcctNumber_KeyDownEvent);
			// 
			// Label1_8
			// 
			this.Label1_8.Location = new System.Drawing.Point(531, 90);
			this.Label1_8.Name = "Label1_8";
			this.Label1_8.Size = new System.Drawing.Size(22, 15);
			this.Label1_8.TabIndex = 27;
			this.Label1_8.Text = "AC";
			this.Label1_8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label1_8, "Affect Cash");
			// 
			// Label1_4
			// 
			this.Label1_4.AutoSize = true;
			this.Label1_4.Location = new System.Drawing.Point(20, 90);
			this.Label1_4.Name = "Label1_4";
			this.Label1_4.Size = new System.Drawing.Size(35, 16);
			this.Label1_4.TabIndex = 66;
			this.Label1_4.Text = "SVC";
			this.Label1_4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblTax
			// 
			this.lblTax.AutoSize = true;
			this.lblTax.Location = new System.Drawing.Point(815, 90);
			this.lblTax.Name = "lblTax";
			this.lblTax.Size = new System.Drawing.Size(34, 16);
			this.lblTax.TabIndex = 56;
			this.lblTax.Text = "TAX";
			this.lblTax.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblTaxClub
			// 
			this.lblTaxClub.Location = new System.Drawing.Point(194, 179);
			this.lblTaxClub.Name = "lblTaxClub";
			this.lblTaxClub.TabIndex = 52;
			this.lblTaxClub.Text = "TC";
			this.ToolTip1.SetToolTip(this.lblTaxClub, "Tax Club Payment");
			this.lblTaxClub.Visible = false;
			this.lblTaxClub.DoubleClick += new System.EventHandler(this.lblTaxClub_DoubleClick);
			// 
			// Label1_9
			// 
			this.Label1_9.AutoSize = true;
			this.Label1_9.Location = new System.Drawing.Point(487, 90);
			this.Label1_9.Name = "Label1_9";
			this.Label1_9.Size = new System.Drawing.Size(27, 16);
			this.Label1_9.TabIndex = 26;
			this.Label1_9.Text = "CD";
			this.Label1_9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.ToolTip1.SetToolTip(this.Label1_9, "Cash Drawer");
			// 
			// lblTotalPendingDue
			// 
			this.lblTotalPendingDue.Location = new System.Drawing.Point(573, 44);
			this.lblTotalPendingDue.Name = "lblTotalPendingDue";
			this.lblTotalPendingDue.TabIndex = 50;
			this.lblTotalPendingDue.Text = "TOTAL PENDING";
			// 
			// Label1_11
			// 
			this.Label1_11.Location = new System.Drawing.Point(585, 90);
			this.Label1_11.Name = "Label1_11";
			this.Label1_11.Size = new System.Drawing.Size(45, 15);
			this.Label1_11.TabIndex = 35;
			this.Label1_11.Text = "ACCT #";
			// 
			// Label1_1
			// 
			this.Label1_1.AutoSize = true;
			this.Label1_1.Location = new System.Drawing.Point(232, 90);
			this.Label1_1.Name = "Label1_1";
			this.Label1_1.Size = new System.Drawing.Size(44, 16);
			this.Label1_1.TabIndex = 34;
			this.Label1_1.Text = "DATE";
			// 
			// Label1_2
			// 
			this.Label1_2.AutoSize = true;
			this.Label1_2.Location = new System.Drawing.Point(430, 90);
			this.Label1_2.Name = "Label1_2";
			this.Label1_2.Size = new System.Drawing.Size(47, 16);
			this.Label1_2.TabIndex = 33;
			this.Label1_2.Text = "CODE";
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.AutoSize = true;
			this.lblPrincipal.Location = new System.Drawing.Point(720, 90);
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Size = new System.Drawing.Size(79, 16);
			this.lblPrincipal.TabIndex = 32;
			this.lblPrincipal.Text = "PRINCIPAL";
			this.lblPrincipal.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblInterest
			// 
			this.lblInterest.AutoSize = true;
			this.lblInterest.Location = new System.Drawing.Point(876, 90);
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Size = new System.Drawing.Size(74, 16);
			this.lblInterest.TabIndex = 31;
			this.lblInterest.Text = "INTEREST";
			this.lblInterest.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblCosts
			// 
			this.lblCosts.AutoSize = true;
			this.lblCosts.Location = new System.Drawing.Point(966, 90);
			this.lblCosts.Name = "lblCosts";
			this.lblCosts.Size = new System.Drawing.Size(54, 16);
			this.lblCosts.TabIndex = 30;
			this.lblCosts.Text = "COSTS";
			this.lblCosts.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label1_10
			// 
			this.Label1_10.AutoSize = true;
			this.Label1_10.Location = new System.Drawing.Point(352, 90);
			this.Label1_10.Name = "Label1_10";
			this.Label1_10.Size = new System.Drawing.Size(35, 16);
			this.Label1_10.TabIndex = 29;
			this.Label1_10.Text = "REF";
			// 
			// Label1_3
			// 
			this.Label1_3.AutoSize = true;
			this.Label1_3.Location = new System.Drawing.Point(125, 90);
			this.Label1_3.Name = "Label1_3";
			this.Label1_3.Size = new System.Drawing.Size(35, 16);
			this.Label1_3.TabIndex = 28;
			this.Label1_3.Text = "BILL";
			this.Label1_3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label8
			// 
			this.Label8.AutoSize = true;
			this.Label8.Location = new System.Drawing.Point(20, 179);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(177, 16);
			this.Label8.TabIndex = 25;
			this.Label8.Text = "PENDING TRANSACTIONS";
			// 
			// Label1_12
			// 
			this.Label1_12.AutoSize = true;
			this.Label1_12.Location = new System.Drawing.Point(304, 179);
			this.Label1_12.Name = "Label1_12";
			this.Label1_12.Size = new System.Drawing.Size(77, 16);
			this.Label1_12.TabIndex = 24;
			this.Label1_12.Text = "COMMENT";
			// 
			// vsPeriod
			// 
			this.vsPeriod.Cols = 5;
			this.vsPeriod.ColumnHeadersVisible = false;
			this.vsPeriod.FixedCols = 0;
			this.vsPeriod.FixedRows = 0;
			this.vsPeriod.Location = new System.Drawing.Point(20, 30);
			this.vsPeriod.Name = "vsPeriod";
			this.vsPeriod.RowHeadersVisible = false;
			this.vsPeriod.Rows = 1;
			this.vsPeriod.ShowFocusCell = false;
			this.vsPeriod.Size = new System.Drawing.Size(453, 42);
			this.vsPeriod.TabIndex = 64;
			this.vsPeriod.DoubleClick += new System.EventHandler(this.vsPeriod_DblClick);
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileAccountInfo,
            this.mnuFileShowMeterInfo,
            this.mnuFileSwitch,
            this.mnuFileShowSplitView,
            this.mnuFileComment,
            this.mnuFileEditNote,
            this.mnuFilePriority,
            this.mnuPayment,
            this.mnuFileDischarge,
            this.mnuFileConsumptionReport,
            this.mnuFilePrintRateInfo});
			this.MainMenu1.Name = null;
			// 
			// mnuFileAccountInfo
			// 
			this.mnuFileAccountInfo.Index = 0;
			this.mnuFileAccountInfo.Name = "mnuFileAccountInfo";
			this.mnuFileAccountInfo.Shortcut = Wisej.Web.Shortcut.F8;
			this.mnuFileAccountInfo.Text = "Show Account Info";
			this.mnuFileAccountInfo.Click += new System.EventHandler(this.mnuFileAccountInfo_Click);
			// 
			// mnuFileShowMeterInfo
			// 
			this.mnuFileShowMeterInfo.Index = 1;
			this.mnuFileShowMeterInfo.Name = "mnuFileShowMeterInfo";
			this.mnuFileShowMeterInfo.Text = "Show Meter Detail";
			this.mnuFileShowMeterInfo.Click += new System.EventHandler(this.mnuFileShowMeterInfo_Click);
			// 
			// mnuFileSwitch
			// 
			this.mnuFileSwitch.Index = 2;
			this.mnuFileSwitch.Name = "mnuFileSwitch";
			this.mnuFileSwitch.Shortcut = Wisej.Web.Shortcut.F9;
			this.mnuFileSwitch.Text = "Show Water Bills";
			this.mnuFileSwitch.Click += new System.EventHandler(this.mnuFileSwitch_Click);
			// 
			// mnuFileShowSplitView
			// 
			this.mnuFileShowSplitView.Index = 3;
			this.mnuFileShowSplitView.Name = "mnuFileShowSplitView";
			this.mnuFileShowSplitView.Text = "Show Split View";
			this.mnuFileShowSplitView.Visible = false;
			this.mnuFileShowSplitView.Click += new System.EventHandler(this.mnuFileShowSplitView_Click);
			// 
			// mnuFileComment
			// 
			this.mnuFileComment.Index = 4;
			this.mnuFileComment.Name = "mnuFileComment";
			this.mnuFileComment.Text = "Account Comment";
			this.mnuFileComment.Click += new System.EventHandler(this.mnuFileComment_Click);
			// 
			// mnuFileEditNote
			// 
			this.mnuFileEditNote.Index = 5;
			this.mnuFileEditNote.Name = "mnuFileEditNote";
			this.mnuFileEditNote.Text = "Add/Edit Note";
			this.mnuFileEditNote.Click += new System.EventHandler(this.mnuFileEditNote_Click);
			// 
			// mnuFilePriority
			// 
			this.mnuFilePriority.Index = 6;
			this.mnuFilePriority.Name = "mnuFilePriority";
			this.mnuFilePriority.Text = "Set Note Priority";
			this.mnuFilePriority.Visible = false;
			// 
			// mnuPayment
			// 
			this.mnuPayment.Index = 7;
			this.mnuPayment.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileOptionsLoadValidAccount,
            this.mnuPaymentClearList});
			this.mnuPayment.Name = "mnuPayment";
			this.mnuPayment.Text = "Options";
			this.mnuPayment.Visible = false;
			// 
			// mnuFileOptionsLoadValidAccount
			// 
			this.mnuFileOptionsLoadValidAccount.Index = 0;
			this.mnuFileOptionsLoadValidAccount.Name = "mnuFileOptionsLoadValidAccount";
			this.mnuFileOptionsLoadValidAccount.Shortcut = Wisej.Web.Shortcut.ShiftF2;
			this.mnuFileOptionsLoadValidAccount.Text = "Load Valid Account";
			this.mnuFileOptionsLoadValidAccount.Click += new System.EventHandler(this.mnuFileOptionsLoadValidAccount_Click);
			// 
			// mnuPaymentClearList
			// 
			this.mnuPaymentClearList.Index = 1;
			this.mnuPaymentClearList.Name = "mnuPaymentClearList";
			this.mnuPaymentClearList.Text = "Clear Pending Transactions";
			this.mnuPaymentClearList.Click += new System.EventHandler(this.mnuPaymentClearList_Click);
			// 
			// mnuFileDischarge
			// 
			this.mnuFileDischarge.Index = 8;
			this.mnuFileDischarge.Name = "mnuFileDischarge";
			this.mnuFileDischarge.Text = "Print Lien Discharge Notice";
			this.mnuFileDischarge.Visible = false;
			this.mnuFileDischarge.Click += new System.EventHandler(this.mnuFileDischarge_Click);
			// 
			// mnuFileConsumptionReport
			// 
			this.mnuFileConsumptionReport.Index = 9;
			this.mnuFileConsumptionReport.Name = "mnuFileConsumptionReport";
			this.mnuFileConsumptionReport.Text = "Print Consumption Report";
			this.mnuFileConsumptionReport.Click += new System.EventHandler(this.mnuFileConsumptionReport_Click);
			// 
			// mnuFilePrintRateInfo
			// 
			this.mnuFilePrintRateInfo.Index = 10;
			this.mnuFilePrintRateInfo.Name = "mnuFilePrintRateInfo";
			this.mnuFilePrintRateInfo.Text = "Print Account/Payment Info";
			this.mnuFilePrintRateInfo.Visible = false;
			this.mnuFilePrintRateInfo.Click += new System.EventHandler(this.mnuFilePrintRateInfo_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Enabled = false;
			this.mnuProcess.Index = -1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuPaymentClearPayment
			// 
			this.mnuPaymentClearPayment.Index = -1;
			this.mnuPaymentClearPayment.Name = "mnuPaymentClearPayment";
			this.mnuPaymentClearPayment.Text = "Clear Payment Boxes";
			this.mnuPaymentClearPayment.Click += new System.EventHandler(this.mnuPaymentClearPayment_Click);
			// 
			// mnuPaymentSeperator
			// 
			this.mnuPaymentSeperator.Index = -1;
			this.mnuPaymentSeperator.Name = "mnuPaymentSeperator";
			this.mnuPaymentSeperator.Text = "-";
			// 
			// mnuSpacer
			// 
			this.mnuSpacer.Index = -1;
			this.mnuSpacer.Name = "mnuSpacer";
			this.mnuSpacer.Text = "-";
			// 
			// mnuProcessExit
			// 
			this.mnuProcessExit.Index = -1;
			this.mnuProcessExit.Name = "mnuProcessExit";
			this.mnuProcessExit.Text = "Exit";
			this.mnuProcessExit.Click += new System.EventHandler(this.mnuProcessExit_Click);
			// 
			// fraStatusLabels
			// 
			this.fraStatusLabels.AppearanceKey = "groupBoxNoBorders";
			this.fraStatusLabels.Controls.Add(this.Label4);
			this.fraStatusLabels.Controls.Add(this.Label3);
			this.fraStatusLabels.Controls.Add(this.Label1_0);
			this.fraStatusLabels.Controls.Add(this.lblOwnersName);
			this.fraStatusLabels.Controls.Add(this.lblMapLot);
			this.fraStatusLabels.Controls.Add(this.lblAccount);
			this.fraStatusLabels.Controls.Add(this.lblLocation);
			this.fraStatusLabels.Location = new System.Drawing.Point(30, 0);
			this.fraStatusLabels.Name = "fraStatusLabels";
			this.fraStatusLabels.Size = new System.Drawing.Size(555, 124);
			this.fraStatusLabels.TabIndex = 14;
			this.fraStatusLabels.Visible = false;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 86);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(55, 18);
			this.Label4.TabIndex = 19;
			this.Label4.Text = "MAP LOT";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 58);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(62, 18);
			this.Label3.TabIndex = 18;
			this.Label3.Text = "LOCATION";
			// 
			// Label1_0
			// 
			this.Label1_0.Location = new System.Drawing.Point(20, 30);
			this.Label1_0.Name = "Label1_0";
			this.Label1_0.Size = new System.Drawing.Size(60, 18);
			this.Label1_0.TabIndex = 17;
			this.Label1_0.Text = "NAME";
			// 
			// lblOwnersName
			// 
			this.lblOwnersName.BackColor = System.Drawing.Color.Transparent;
			this.lblOwnersName.Location = new System.Drawing.Point(92, 30);
			this.lblOwnersName.Name = "lblOwnersName";
			this.lblOwnersName.Size = new System.Drawing.Size(380, 18);
			this.lblOwnersName.TabIndex = 16;
			this.lblOwnersName.Visible = false;
			// 
			// lblMapLot
			// 
			this.lblMapLot.BackColor = System.Drawing.Color.Transparent;
			this.lblMapLot.Location = new System.Drawing.Point(92, 86);
			this.lblMapLot.Name = "lblMapLot";
			this.lblMapLot.Size = new System.Drawing.Size(151, 18);
			this.lblMapLot.TabIndex = 21;
			this.lblMapLot.Visible = false;
			// 
			// lblAccount
			// 
			this.lblAccount.BackColor = System.Drawing.Color.Transparent;
			this.lblAccount.Location = new System.Drawing.Point(92, 30);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(53, 19);
			this.lblAccount.TabIndex = 15;
			this.lblAccount.Visible = false;
			// 
			// lblLocation
			// 
			this.lblLocation.BackColor = System.Drawing.Color.Transparent;
			this.lblLocation.Location = new System.Drawing.Point(92, 58);
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Size = new System.Drawing.Size(317, 18);
			this.lblLocation.TabIndex = 20;
			this.lblLocation.Visible = false;
			// 
			// CommonDialog1
			// 
			this.CommonDialog1.Name = "CommonDialog1";
			this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
			// 
			// WGRID
			// 
			this.WGRID.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.WGRID.Cols = 20;
			this.WGRID.Location = new System.Drawing.Point(30, 159);
			this.WGRID.Name = "WGRID";
			this.WGRID.Rows = 1;
			this.WGRID.ShowFocusCell = false;
			this.WGRID.Size = new System.Drawing.Size(0, 319);
			this.WGRID.TabIndex = 60;
			this.WGRID.Visible = false;
			this.WGRID.Collapsed += new System.EventHandler(this.GRID_RowCollapsed);
			this.WGRID.CurrentCellChanged += new System.EventHandler(this.WGRID_RowColChange);
			this.WGRID.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.WGRID_MouseDownEvent);
			this.WGRID.Enter += new System.EventHandler(this.WGRID_Enter);
			this.WGRID.Click += new System.EventHandler(this.WGRID_ClickEvent);
			this.WGRID.DoubleClick += new System.EventHandler(this.WGRID_DblClick);
			this.WGRID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.WGRID_KeyPressEvent);
			// 
			// vsPreview
			// 
			this.vsPreview.Cols = 11;
			this.vsPreview.Location = new System.Drawing.Point(841, 416);
			this.vsPreview.Name = "vsPreview";
			this.vsPreview.Rows = 1;
			this.vsPreview.ShowFocusCell = false;
			this.vsPreview.Size = new System.Drawing.Size(329, 319);
			this.vsPreview.TabIndex = 61;
			this.vsPreview.Visible = false;
			this.vsPreview.Click += new System.EventHandler(this.vsPreview_ClickEvent);
			// 
			// SGRID
			// 
			this.SGRID.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.SGRID.Cols = 20;
			this.SGRID.Location = new System.Drawing.Point(-66, 159);
			this.SGRID.Name = "SGRID";
			this.SGRID.Rows = 1;
			this.SGRID.ShowFocusCell = false;
			this.SGRID.Size = new System.Drawing.Size(182, 319);
			this.SGRID.TabIndex = 59;
			this.SGRID.Visible = false;
			this.SGRID.Collapsed += new System.EventHandler(this.SGRID_RowCollapsed);
			this.SGRID.CurrentCellChanged += new System.EventHandler(this.SGRID_RowColChange);
			this.SGRID.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.SGRID_MouseDownEvent);
			this.SGRID.Enter += new System.EventHandler(this.SGRID_Enter);
			this.SGRID.Click += new System.EventHandler(this.SGRID_ClickEvent);
			this.SGRID.DoubleClick += new System.EventHandler(this.SGRID_DblClick);
			this.SGRID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.SGRID_KeyPressEvent);
			// 
			// lblGrpInfo
			// 
			this.lblGrpInfo.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.lblGrpInfo.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
			this.lblGrpInfo.Location = new System.Drawing.Point(190, 0);
			this.lblGrpInfo.Name = "lblGrpInfo";
			this.lblGrpInfo.Size = new System.Drawing.Size(26, 23);
			this.lblGrpInfo.TabIndex = 74;
			this.lblGrpInfo.Text = "G";
			this.lblGrpInfo.Visible = false;
			// 
			// lblAcctComment
			// 
			this.lblAcctComment.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.lblAcctComment.Location = new System.Drawing.Point(110, 0);
			this.lblAcctComment.Name = "lblAcctComment";
			this.lblAcctComment.Size = new System.Drawing.Size(26, 23);
			this.lblAcctComment.TabIndex = 65;
			this.lblAcctComment.Text = "C";
			this.lblAcctComment.Visible = false;
			this.lblAcctComment.Click += new System.EventHandler(this.lblAcctComment_Click);
			// 
			// lblTaxAcquired
			// 
			this.lblTaxAcquired.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
			this.lblTaxAcquired.Location = new System.Drawing.Point(70, 0);
			this.lblTaxAcquired.Name = "lblTaxAcquired";
			this.lblTaxAcquired.Size = new System.Drawing.Size(26, 23);
			this.lblTaxAcquired.TabIndex = 67;
			this.lblTaxAcquired.Text = "TA";
			this.lblTaxAcquired.Visible = false;
			// 
			// lblSewerHeading
			// 
			this.lblSewerHeading.Location = new System.Drawing.Point(462, 124);
			this.lblSewerHeading.Name = "lblSewerHeading";
			this.lblSewerHeading.Size = new System.Drawing.Size(369, 15);
			this.lblSewerHeading.TabIndex = 58;
			this.lblSewerHeading.Text = "SEWER";
			this.lblSewerHeading.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblSewerHeading.DoubleClick += new System.EventHandler(this.lblSewerHeading_DoubleClick);
			// 
			// lblWaterHeading
			// 
			this.lblWaterHeading.Location = new System.Drawing.Point(30, 124);
			this.lblWaterHeading.Name = "lblWaterHeading";
			this.lblWaterHeading.Size = new System.Drawing.Size(371, 16);
			this.lblWaterHeading.TabIndex = 57;
			this.lblWaterHeading.Text = "WATER";
			this.lblWaterHeading.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.lblWaterHeading.DoubleClick += new System.EventHandler(this.lblWaterHeading_DoubleClick);
			// 
			// imgNote
			// 
			this.imgNote.BorderStyle = Wisej.Web.BorderStyle.None;
			this.imgNote.Location = new System.Drawing.Point(30, 0);
			this.imgNote.Name = "imgNote";
			this.imgNote.Size = new System.Drawing.Size(26, 23);
			this.imgNote.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.imgNote.Visible = false;
			this.imgNote.DoubleClick += new System.EventHandler(this.imgNote_DoubleClick);
			// 
			// lblPaymentInfo
			// 
			this.lblPaymentInfo.Location = new System.Drawing.Point(30, 23);
			this.lblPaymentInfo.Name = "lblPaymentInfo";
			this.lblPaymentInfo.Size = new System.Drawing.Size(541, 18);
			this.lblPaymentInfo.TabIndex = 5;
			// 
			// cmdProcessEffective
			// 
			this.cmdProcessEffective.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdProcessEffective.Location = new System.Drawing.Point(260, 29);
			this.cmdProcessEffective.Name = "cmdProcessEffective";
			this.cmdProcessEffective.Shortcut = Wisej.Web.Shortcut.F2;
			this.cmdProcessEffective.Size = new System.Drawing.Size(102, 24);
			this.cmdProcessEffective.TabIndex = 1;
			this.cmdProcessEffective.Text = "Effective Date";
			this.cmdProcessEffective.Click += new System.EventHandler(this.mnuProcessEffective_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrint.Location = new System.Drawing.Point(504, 29);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Shortcut = Wisej.Web.Shortcut.F4;
			this.cmdFilePrint.Size = new System.Drawing.Size(138, 24);
			this.cmdFilePrint.TabIndex = 2;
			this.cmdFilePrint.Text = "Print Account Detail";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// cmdProcessGoTo
			// 
			this.cmdProcessGoTo.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdProcessGoTo.Location = new System.Drawing.Point(368, 29);
			this.cmdProcessGoTo.Name = "cmdProcessGoTo";
			this.cmdProcessGoTo.Shortcut = Wisej.Web.Shortcut.F3;
			this.cmdProcessGoTo.Size = new System.Drawing.Size(130, 24);
			this.cmdProcessGoTo.TabIndex = 3;
			this.cmdProcessGoTo.Text = "Go To Status View";
			this.cmdProcessGoTo.Click += new System.EventHandler(this.mnuProcessGoTo_Click);
			// 
			// cmdPaymentPreview
			// 
			this.cmdPaymentPreview.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPaymentPreview.Location = new System.Drawing.Point(648, 29);
			this.cmdPaymentPreview.Name = "cmdPaymentPreview";
			this.cmdPaymentPreview.Shortcut = Wisej.Web.Shortcut.F5;
			this.cmdPaymentPreview.Size = new System.Drawing.Size(86, 24);
			this.cmdPaymentPreview.TabIndex = 4;
			this.cmdPaymentPreview.Text = "Preview";
			this.cmdPaymentPreview.Visible = false;
			this.cmdPaymentPreview.Click += new System.EventHandler(this.mnuPaymentPreview_Click);
			// 
			// cmdProcessChangeAccount
			// 
			this.cmdProcessChangeAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdProcessChangeAccount.Location = new System.Drawing.Point(740, 29);
			this.cmdProcessChangeAccount.Name = "cmdProcessChangeAccount";
			this.cmdProcessChangeAccount.Shortcut = Wisej.Web.Shortcut.F6;
			this.cmdProcessChangeAccount.Size = new System.Drawing.Size(120, 24);
			this.cmdProcessChangeAccount.TabIndex = 5;
			this.cmdProcessChangeAccount.Text = "Change Account";
			this.cmdProcessChangeAccount.Click += new System.EventHandler(this.mnuProcessChangeAccount_Click);
			// 
			// cmdFileMortgageHolderInfo
			// 
			this.cmdFileMortgageHolderInfo.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileMortgageHolderInfo.Location = new System.Drawing.Point(866, 29);
			this.cmdFileMortgageHolderInfo.Name = "cmdFileMortgageHolderInfo";
			this.cmdFileMortgageHolderInfo.Shortcut = Wisej.Web.Shortcut.F7;
			this.cmdFileMortgageHolderInfo.Size = new System.Drawing.Size(194, 24);
			this.cmdFileMortgageHolderInfo.TabIndex = 6;
			this.cmdFileMortgageHolderInfo.Text = "Mortgage Holder Information";
			this.cmdFileMortgageHolderInfo.Click += new System.EventHandler(this.mnuFileMortgageHolderInfo_Click);
			// 
			// cmdSavePayments
			// 
			this.cmdSavePayments.AppearanceKey = "acceptButton";
			this.cmdSavePayments.Enabled = false;
			this.cmdSavePayments.Location = new System.Drawing.Point(592, 30);
			this.cmdSavePayments.Name = "cmdSavePayments";
			this.cmdSavePayments.Shortcut = Wisej.Web.Shortcut.F11;
			this.cmdSavePayments.Size = new System.Drawing.Size(170, 48);
			this.cmdSavePayments.TabIndex = 8;
			this.cmdSavePayments.Text = "Save Payments";
			this.cmdSavePayments.Click += new System.EventHandler(this.mnuPaymentSave_Click);
			// 
			// cmdPaymentSaveExit
			// 
			this.cmdPaymentSaveExit.AppearanceKey = "acceptButton";
			this.cmdPaymentSaveExit.Enabled = false;
			this.cmdPaymentSaveExit.Location = new System.Drawing.Point(349, 30);
			this.cmdPaymentSaveExit.Name = "cmdPaymentSaveExit";
			this.cmdPaymentSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPaymentSaveExit.Size = new System.Drawing.Size(216, 48);
			this.cmdPaymentSaveExit.TabIndex = 9;
			this.cmdPaymentSaveExit.Text = "Save Payments & Exit";
			this.cmdPaymentSaveExit.Click += new System.EventHandler(this.mnuPaymentSaveExit_Click);
			// 
			// lblACHInfo
			// 
			this.lblACHInfo.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
			this.lblACHInfo.Location = new System.Drawing.Point(0, 45);
			this.lblACHInfo.Name = "lblACHInfo";
			this.lblACHInfo.Size = new System.Drawing.Size(50, 14);
			this.lblACHInfo.TabIndex = 75;
			this.lblACHInfo.Text = "A";
			this.lblACHInfo.Visible = false;
			// 
			// frmUTCLStatus
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1078, 811);
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmUTCLStatus";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Status Screen";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmUTCLStatus_Load);
			this.Activated += new System.EventHandler(this.frmUTCLStatus_Activated);
			this.Enter += new System.EventHandler(this.frmUTCLStatus_Enter);
			this.Resize += new System.EventHandler(this.frmUTCLStatus_Resize);
			this.Appear += new System.EventHandler(this.FrmUTCLStatus_Appear);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUTCLStatus_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraEditNote)).EndInit();
			this.fraEditNote.ResumeLayout(false);
			this.fraEditNote.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPriority)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNoteProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNoteCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).EndInit();
			this.fraRateInfo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsRateInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdRIClose)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDiscount)).EndInit();
			this.fraDiscount.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdDisc_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDisc_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDisc_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPayment)).EndInit();
			this.fraPayment.ResumeLayout(false);
			this.fraPayment.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsPayments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTransactionDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcctNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraStatusLabels)).EndInit();
			this.fraStatusLabels.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.WGRID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsPreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SGRID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.imgNote)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessEffective)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessGoTo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPaymentPreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessChangeAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileMortgageHolderInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSavePayments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPaymentSaveExit)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdFilePrint;
		public FCButton cmdProcessEffective;
		public FCButton cmdProcessGoTo;
		public FCButton cmdPaymentPreview;
		public FCButton cmdProcessChangeAccount;
		public FCButton cmdFileMortgageHolderInfo;
		public FCButton cmdSavePayments;
		public FCButton cmdPaymentSaveExit;
		public FCLabel Label1_0;
		public FCLabel lblAccount;
		private FCLabel lblACHInfo;
	}
}