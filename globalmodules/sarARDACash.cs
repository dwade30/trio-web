﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
#if TWCR0000
using TWCR0000;


#else
using TWAR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for sarARDACash.
	/// </summary>
	public partial class sarARDACash : BaseSectionReport
	{
		// nObj = 1
		//   0	sarARDACash	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/10/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/11/2005              *
		// ********************************************************
		bool boolWide;
		float lngWidth;
		clsDRWrapper rsPayment = new clsDRWrapper();
		clsDRWrapper rsCorrection = new clsDRWrapper();
		clsDRWrapper rsBillType = new clsDRWrapper();
		string strSQL;
		int lngBillType;

		public sarARDACash()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarARDACash_ReportEnd;
		}

        private void SarARDACash_ReportEnd(object sender, EventArgs e)
        {
			rsPayment.DisposeOf();
            rsCorrection.DisposeOf();
            rsBillType.DisposeOf();

		}

        public static sarARDACash InstancePtr
		{
			get
			{
				return (sarARDACash)Sys.GetInstance(typeof(sarARDACash));
			}
		}

		protected sarARDACash _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsBillType.EndOfFile())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			SetupFields();
			SetupValues();
			// lngBillType = Val(Year(Now) & "1")
			// If rsPayment.EndOfFile <> True Then
			// If rsPayment.Fields("BillType") < lngBillType Then
			// lngBillType = rsPayment.Fields("BillType")
			// End If
			// End If
			// 
			// If rsCorrection.EndOfFile <> True Then
			// If rsCorrection.Fields("BillType") < lngBillType Then
			// lngBillType = rsCorrection.Fields("BillType")
			// End If
			// End If
			frmWait.InstancePtr.IncrementProgress();
		}

		private void SetupFields()
		{
			boolWide = rptARDailyAuditMaster.InstancePtr.boolLandscape;
			lngWidth = rptARDailyAuditMaster.InstancePtr.lngWidth;
			this.PrintWidth = lngWidth;
			// Header Section
			if (!boolWide)
			{
				// Narrow
				lblBillType.Left = 0;
				lblPrincipal.Left = 900 / 1440f;
				lblPaymentPrin.Left = 900 / 1440f;
				lblCorrectionPrin.Left = 2160 / 1440f;
				lblInterest.Left = 3420 / 1440f;
				lblPaymentInt.Left = 3420 / 1440f;
				lblCorrectionInt.Left = 4680 / 1440f;
				lblTax.Left = 5940 / 1440f;
				lblPaymentTax.Left = 5940 / 1440f;
				lblCorrectionTax.Left = 7200 / 1440f;
				lblTotal.Left = 8460 / 1440f;
			}
			else
			{
				// Wide
				lblBillType.Left = 0;
				lblPrincipal.Left = 2250 / 1440f;
				lblPaymentPrin.Left = 2250 / 1440f;
				lblCorrectionPrin.Left = 3510 / 1440f;
				lblInterest.Left = 5400 / 1440f;
				lblPaymentInt.Left = 5400 / 1440f;
				lblCorrectionInt.Left = 6660 / 1440f;
				lblTax.Left = 8550 / 1440f;
				lblPaymentTax.Left = 8550 / 1440f;
				lblCorrectionTax.Left = 9810 / 1440f;
				lblTotal.Left = 11790 / 1440f;
			}
			lnHeader.X2 = lngWidth;
			lblHeader.Width = lngWidth;
			// Detail Section
			fldBillType.Left = lblBillType.Left;
			fldPaymentPrin.Left = lblPaymentPrin.Left;
			fldCorrectionPrin.Left = lblCorrectionPrin.Left;
			fldPaymentInterest.Left = lblPaymentInt.Left;
			fldCorrectionInt.Left = lblCorrectionInt.Left;
			fldPaymentTax.Left = lblPaymentTax.Left;
			fldCorrectionTax.Left = lblCorrectionTax.Left;
			fldTotal.Left = lblTotal.Left;
			// Footer Section
			fldTotalPaymentPrin.Left = lblPaymentPrin.Left;
			fldTotalCorrectionPrin.Left = lblCorrectionPrin.Left;
			fldTotalPaymentInt.Left = lblPaymentInt.Left;
			fldTotalCorrectionInt.Left = lblCorrectionInt.Left;
			fldTotalPaymentTax.Left = lblPaymentTax.Left;
			fldTotalCorrectionTax.Left = lblCorrectionTax.Left;
			fldTotalTotal.Left = lblTotal.Left;
			lnTotals.X1 = fldTotalPaymentPrin.Left;
			lnTotals.X2 = fldTotalTotal.Left + fldTotalTotal.Width;
		}

		private void SetupValues()
		{
			// this will get all of the values ready for reporting
			clsDRWrapper rsTotals = new clsDRWrapper();
			strSQL = "SELECT BillType FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + "AND Code <> 'I' GROUP BY BillType";
			rsBillType.OpenRecordset(strSQL, modExtraModules.strARDatabase);
			if (rsBillType.EndOfFile() != true && rsBillType.BeginningOfFile() != true)
			{
				// set the payments recordset
				strSQL = "SELECT BillType, SUM(Principal) AS Prin, SUM(Tax) AS T, SUM(Interest) AS I FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + "AND Code <> 'I' AND (CashDrawer = 'Y' OR GeneralLedger = 'Y') AND (Principal + Tax + Interest) > 0 GROUP BY BillType ORDER BY BillType";
				rsPayment.OpenRecordset(strSQL, modExtraModules.strARDatabase);
				// set the correction recordset
				strSQL = "SELECT BillType, SUM(Principal) AS Prin, SUM(Tax) AS T, SUM(Interest) AS I FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + "AND Code <> 'I' AND (CashDrawer = 'Y' OR GeneralLedger = 'Y') AND (Principal + Tax + Interest) <= 0 GROUP BY BillType ORDER BY BillType";
				rsCorrection.OpenRecordset(strSQL, modExtraModules.strARDatabase);
				// find the totals and show them in the footer
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(Tax) AS T, SUM(Interest) AS I FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + "AND Code <> 'I' AND (CashDrawer = 'Y' OR GeneralLedger = 'Y') AND (Principal + Tax + Interest) > 0";
				rsTotals.OpenRecordset(strSQL, modExtraModules.strARDatabase);
				if (rsTotals.EndOfFile() != true)
				{
					fldTotalPaymentPrin.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields_Double("Prin")), "#,##0.00");
					// TODO: Field [I] not found!! (maybe it is an alias?)
					fldTotalPaymentInt.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("I")), "#,##0.00");
					// TODO: Field [T] not found!! (maybe it is an alias?)
					fldTotalPaymentTax.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("T")), "#,##0.00");
				}
				else
				{
					fldTotalPaymentPrin.Text = "0.00";
					fldTotalPaymentInt.Text = "0.00";
					fldTotalPaymentTax.Text = "0.00";
				}
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(Tax) AS T, SUM(Interest) AS I FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + "AND Code <> 'I' AND (CashDrawer = 'Y' OR GeneralLedger = 'Y') AND (Principal + Tax + Interest) <= 0";
				rsTotals.OpenRecordset(strSQL, modExtraModules.strARDatabase);
				if (rsTotals.EndOfFile() != true)
				{
					fldTotalCorrectionPrin.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields_Double("Prin")), "#,##0.00");
					// TODO: Field [I] not found!! (maybe it is an alias?)
					fldTotalCorrectionInt.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("I")), "#,##0.00");
					// TODO: Field [T] not found!! (maybe it is an alias?)
					fldTotalCorrectionTax.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("T")), "#,##0.00");
				}
				else
				{
					fldTotalCorrectionPrin.Text = "0.00";
					fldTotalCorrectionInt.Text = "0.00";
					fldTotalCorrectionTax.Text = "0.00";
				}
				// this will show the end total
				fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldTotalPaymentPrin.Text) + FCConvert.ToDouble(fldTotalPaymentInt.Text) + FCConvert.ToDouble(fldTotalPaymentTax.Text) + FCConvert.ToDouble(fldTotalCorrectionPrin.Text) + FCConvert.ToDouble(fldTotalCorrectionInt.Text) + FCConvert.ToDouble(fldTotalCorrectionTax.Text), "#,##0.00");
			}
			else
			{
				this.Close();
			}
		}
		// vbPorter upgrade warning: lngBT As int	OnWriteFCConvert.ToDouble(
		private void BindFields_2(int lngBT)
		{
			BindFields(ref lngBT);
		}

		private void BindFields(ref int lngBT)
		{
			// this will put the current record BillType in the fields
			double dblSum;
			fldBillType.Text = FCConvert.ToString(lngBT);
			frmWait.InstancePtr.IncrementProgress();
			dblSum = 0;
			if (rsPayment.EndOfFile() != true)
			{
				rsPayment.FindFirstRecord("BillType", lngBT);
				if (rsPayment.NoMatch)
				{
					fldPaymentPrin.Text = "0.00";
					fldPaymentInterest.Text = "0.00";
					fldPaymentTax.Text = "0.00";
				}
				else
				{
					fldPaymentPrin.Text = Strings.Format(Conversion.Val(rsPayment.Get_Fields_Double("Prin")), "#,##0.00");
					// TODO: Field [I] not found!! (maybe it is an alias?)
					fldPaymentInterest.Text = Strings.Format(Conversion.Val(rsPayment.Get_Fields("I")), "#,##0.00");
					// TODO: Field [T] not found!! (maybe it is an alias?)
					fldPaymentTax.Text = Strings.Format(Conversion.Val(rsPayment.Get_Fields("T")), "#,##0.00");
					// TODO: Field [I] not found!! (maybe it is an alias?)
					// TODO: Field [T] not found!! (maybe it is an alias?)
					dblSum = Conversion.Val(rsPayment.Get_Fields_Double("Prin")) + Conversion.Val(rsPayment.Get_Fields("I")) + Conversion.Val(rsPayment.Get_Fields("T"));
				}
			}
			else
			{
				fldPaymentPrin.Text = "0.00";
				fldPaymentInterest.Text = "0.00";
				fldPaymentTax.Text = "0.00";
			}
			if (rsCorrection.EndOfFile() != true)
			{
				rsCorrection.FindFirstRecord("BillType", lngBT);
				if (rsCorrection.NoMatch)
				{
					fldCorrectionPrin.Text = "0.00";
					fldCorrectionInt.Text = "0.00";
					fldCorrectionTax.Text = "0.00";
				}
				else
				{
					fldCorrectionPrin.Text = Strings.Format(Conversion.Val(rsCorrection.Get_Fields_Double("Prin")), "#,##0.00");
					// TODO: Field [I] not found!! (maybe it is an alias?)
					fldCorrectionInt.Text = Strings.Format(Conversion.Val(rsCorrection.Get_Fields("I")), "#,##0.00");
					// TODO: Field [T] not found!! (maybe it is an alias?)
					fldCorrectionTax.Text = Strings.Format(Conversion.Val(rsCorrection.Get_Fields("T")), "#,##0.00");
					// TODO: Field [I] not found!! (maybe it is an alias?)
					// TODO: Field [T] not found!! (maybe it is an alias?)
					dblSum += Conversion.Val(rsCorrection.Get_Fields_Double("Prin")) + Conversion.Val(rsCorrection.Get_Fields("I")) + Conversion.Val(rsCorrection.Get_Fields("T"));
				}
			}
			else
			{
				fldCorrectionPrin.Text = "0.00";
				fldCorrectionInt.Text = "0.00";
				fldCorrectionTax.Text = "0.00";
			}
			fldTotal.Text = Strings.Format(dblSum, "#,##0.00");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (rsBillType.EndOfFile() != true)
			{
				// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
				BindFields_2(FCConvert.ToInt32(Conversion.Val(rsBillType.Get_Fields("BillType"))));
				rsBillType.MoveNext();
				// move to the next BillType and check to see if there are any BillTypes that need to be shown
			}
		}

		private void sarARDACash_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarARDACash.Caption	= "Daily Audit Cash Summary";
			//sarARDACash.Icon	= "sarARDACash.dsx":0000";
			//sarARDACash.Left	= 0;
			//sarARDACash.Top	= 0;
			//sarARDACash.Width	= 11880;
			//sarARDACash.Height	= 8415;
			//sarARDACash.StartUpPosition	= 3;
			//sarARDACash.SectionData	= "sarARDACash.dsx":058A;
			//End Unmaped Properties
		}
	}
}
