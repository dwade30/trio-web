//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
#if TWRE0000
using modGlobalRoutines = TWRE0000.modMDIParent;
#elif TWUT0000
using modGlobalRoutines = TWUT0000.modMain;
#elif TWCK0000
using modGlobalRoutines = TWCK0000.modGNBas;
#elif TWCE0000
using modGlobalRoutines = TWCE0000.modMDIParent;
#endif

namespace Global
{
    /// <summary>
    /// Summary description for frmScanDocument.
    /// </summary>
    public partial class frmScanDocument : BaseForm
    {
        public frmScanDocument()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmScanDocument InstancePtr
        {
            get
            {
                return (frmScanDocument)Sys.GetInstance(typeof(frmScanDocument));
            }
        }
        protected frmScanDocument _InstancePtr = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>



        //=========================================================
        // ********************************************************
        // Property of TRIO Software Corporation

        // Written By
        // Date


        // ********************************************************
        int counter;
        string strFile; // file name to be saved

        private void chkClearBuffer_CheckedChanged(object sender, System.EventArgs e)
        {
            // set whether or not to clear image buffer or leave what has already been scanned when a new document is scanned
            if (chkClearBuffer.CheckState == CheckState.Checked)
            {
                this.Scanner1.ClearImageBuffer = true;
            }
            else
            {
                this.Scanner1.ClearImageBuffer = false;
            }
        }

        private void chkShowTWAIN_CheckedChanged(object sender, System.EventArgs e)
        {
            // sets whether or not to show the TWAIN interface when using the scanner control
            if (chkShowTWAIN.CheckState == CheckState.Checked)
            {
                Scanner1.ShowTwainUI = true;
            }
            else
            {
                Scanner1.ShowTwainUI = false;
            }
        }

        private void cmdClearText_Click(object sender, System.EventArgs e)
        {
            // hide the text that was shown on the image
            Scanner1.ShowText = false;
        }

        private void cmdDeletePage_Click(object sender, System.EventArgs e)
        {
            // delete the current page of a document
            Scanner1.DeletePage(Scanner1.GetActivePageNo()); // delete page you are currently on

            Scanner1.SetActivePageNo(1); // set document to show the first page of the document
            lblTotalPage.Text = FCConvert.ToString(this.Scanner1.TotalPage); // update total numbe rof pages in the document
            lblCurrentPage.Text = FCConvert.ToString(this.Scanner1.GetActivePageNo()); // update what page number you are on
        }

        private void cmdDrawText_Click(object sender, System.EventArgs e)
        {
            // shows text typed in by user on the image
            Scanner1.DrawText(FCConvert.ToInt16(txtTextLeft.Text), FCConvert.ToInt16(txtTextTop.Text), txtText1.Text + "\r\n" + txtText2.Text);
            Scanner1.ShowText = true;
        }

        private void cmdGamma_Click(object sender, System.EventArgs e)
        {
            // sets the gamma property of the image control
            this.Scanner1.Gamma = FCConvert.ToDouble(txtGamma.Text);
        }

        private void cmdHue_Click(object sender, System.EventArgs e)
        {
            // sets the hue property of the image control
            this.Scanner1.Hue = FCConvert.ToDouble(txtHue.Text);
        }

        private void cmdContrast_Click(object sender, System.EventArgs e)
        {
            // sets the contrast property of the image control
            this.Scanner1.Contrast = FCConvert.ToDouble(txtContrast.Text);
        }

        private void cmdBrightness_Click(object sender, System.EventArgs e)
        {
            // sets the brightness property fo the image control
            this.Scanner1.Brightness = FCConvert.ToDouble(txtBrightness.Text);
        }

        private void cmdSaturation_Click(object sender, System.EventArgs e)
        {
            // sets the saturation property of the image control
            this.Scanner1.Saturation = FCConvert.ToDouble(txtSaturation.Text);
        }

        private void cmdScan_Click(object sender, System.EventArgs e)
        {
            // scan a document using the selected scanner
            Scanner1.SelectImageSourceByIndex(cboImageSource.SelectedIndex); // get the selected scanner

            if (chkEnableDuplex.CheckState == CheckState.Checked)
            { // set duplex of the selected scanner based on the checkbox on the form
                Scanner1.DuplexEnabled = true;
            }
            else
            {
                Scanner1.DuplexEnabled = false;
            }

            if (chkEnableFeeder.CheckState == CheckState.Checked)
            { // set whether or not to use the feeder based on the check box on the form
                Scanner1.FeederEnabled = true;
            }
            else
            {
                Scanner1.FeederEnabled = false;
            }

            switch (cboDPI.SelectedIndex)
            { // set the DPI to used based on user selection

                case 0:
                    {
                        Scanner1.DPI = 96;
                        break;
                    }
                case 1:
                    {
                        Scanner1.DPI = 200;
                        break;
                    }
                case 2:
                    {
                        Scanner1.DPI = 300;
                        break;
                    }
                case 3:
                    {
                        Scanner1.DPI = 600;
                        break;
                    }
            } //end switch

            // set the capture area for the scanner to use
            if (txtCapHeight.Text == "11.5" && txtCapLeft.Text == "0" && txtCapTop.Text == "0" && txtCapWidth.Text == "8.23")
            {
                Scanner1.SetCaptureArea(0, 0, 0, 0);
            }
            else
            {
                Scanner1.SetCaptureArea(FCConvert.ToDouble(txtCapLeft.Text), FCConvert.ToDouble(txtCapTop.Text), FCConvert.ToDouble(txtCapWidth.Text), FCConvert.ToDouble(txtCapHeight.Text));
            }

            // set pixel type to default which is color
            Scanner1.PixelType = -1;

            // scan the document
            Scanner1.Scan();
        }

        private void cmdUpdatePage_Click(object sender, System.EventArgs e)
        {
            this.Scanner1.ApplyChange(); // save changes to current page you are editing whether they be zooming in or out, rotating, or changing the gamma, saturation, brightness, hue, contrast, or text
        }

        private void frmScanDocument_Activated(object sender, System.EventArgs e)
        {
			if (modGlobalRoutines.FormExist(this))
			{
				return;
            }
            this.Refresh();
        }

        private void frmScanDocument_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmScanDocument.FillStyle	= 0;
            //frmScanDocument.ScaleWidth	= 9045;
            //frmScanDocument.ScaleHeight	= 6930;
            //frmScanDocument.LinkTopic	= "Form2";
            //frmScanDocument.LockControls	= true;
            //frmScanDocument.PaletteMode	= 1  'UseZOrder;
            //End Unmaped Properties

            int I;
            int ICount;

            counter = 1;

            ICount = this.Scanner1.GetNumImageSources(); // get number of possible scannersa to use

            for (I = 0; I <= ICount - 1; I++)
            {
                cboImageSource.Items.Add(Scanner1.GetImageSourceName(I)); // shwo scanner options that the user can select
            }
            if (cboImageSource.Items.Count > 0)
            { // defualt to the first scanner in the list
                cboImageSource.SelectedIndex = 0;
            }

            cboDPI.Items.Add("Onscreen Viewing 96dpi"); // initialive DPI combo box with possible selections
            cboDPI.Items.Add("Fax 200dpi");
            cboDPI.Items.Add("OCR Text 300dpi");
            cboDPI.Items.Add("Laser Print Fine 600dpi");
            cboDPI.SelectedIndex = 0;

            Scanner1.Border = true; // set initial options for scanner control
            Scanner1.HighQuality = true;

            Scanner1.LicenseKey = "8180 single developer license";

            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            modGlobalFunctions.SetTRIOColors(this);
        }

        private void frmScanDocument_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

            // catches the escape and enter keys
            if (KeyAscii == Keys.Escape)
            {
                KeyAscii = (Keys)0;
                Close();
            }
            else if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                Support.SendKeys("{TAB}", false);
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void mnuFileNextPage_Click(object sender, System.EventArgs e)
        {
            if (Scanner1.GetActivePageNo() < Scanner1.TotalPage)
            { // if we are not on the last page
                Scanner1.SetActivePageNo(Scanner1.GetActivePageNo() + 1); // show the next page
                lblCurrentPage.Text = FCConvert.ToString(Scanner1.GetActivePageNo()); // update page number we are on
            }
        }

        private void mnuFilePreviousPage_Click(object sender, System.EventArgs e)
        {
            if (Scanner1.GetActivePageNo() > 1)
            { // if we are not on the first page
                Scanner1.SetActivePageNo(Scanner1.GetActivePageNo() - 1); // show previous page
                lblCurrentPage.Text = FCConvert.ToString(Scanner1.GetActivePageNo()); // update the page number we are on
            }
        }

        private void mnuFileResetCapture_Click(object sender, System.EventArgs e)
        {
            txtCapLeft.Text = FCConvert.ToString(0); // reset capture options to what they are defaulted to when the form shows
            txtCapTop.Text = FCConvert.ToString(0);
            txtCapWidth.Text = FCConvert.ToString(8.23);
            txtCapHeight.Text = FCConvert.ToString(11.5);
        }

        private void mnuFileRotate_Click(object sender, System.EventArgs e)
        {
            this.Scanner1.Rotate90(); // rotate the image 90 degrees
        }

        private void mnuFileZoomAspectRatio_Click(object sender, System.EventArgs e)
        {
            Scanner1.View = 8; // show the image in aspect ratio
            Scanner1.Focus();
        }

        private void mnuFileZoomZoomIn_Click(object sender, System.EventArgs e)
        {
            if (Scanner1.View > 7)
            { // if view is greater than 7 then we are lloking at aspect ratio or image is fit into control
                Scanner1.View = 5; // so set view to 100 percent of the file size
                Scanner1.Focus();
            }
            else if (Scanner1.View < 7)
            { // if the view is on a normal percentage nad not the highest percentage possible
                Scanner1.View += 1; // increase the percentage option
                Scanner1.Focus();
            }
        }

        private void mnuFileZoomZoomOut_Click(object sender, System.EventArgs e)
        {
            if (Scanner1.View > 7)
            { // if view is greater than 7 then we are lloking at aspect ratio or image is fit into control
                Scanner1.View = 5; // so set view to 100 percent of the file size
                Scanner1.Focus();
            }
            else if (Scanner1.View > 1)
            { // if the view is a normal percentage option and not on the lowest percentage option
                Scanner1.View -= 1; // decrease the percentage option
                Scanner1.Focus();
            }
        }

        private void mnuProcessQuit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void mnuProcessSave_Click(object sender, System.EventArgs e)
        {
            // vbPorter upgrade warning: result As short --> As int	OnWrite(bool)
            int result;
            string strType = "";

            // get file name to use from the user
            strFile = "";
            strFile = Interaction.InputBox("Please enter a name for this file", "Enter File Name", null/*, -1, -1*/);
            if (strFile != "")
            { // if the user entered a name
                strFile = FCFileSystem.Statics.UserDataFolder + "\\ScannedDocuments\\" + strFile + ".pdf"; // set the entire file name
            }
            else
            {
                MessageBox.Show("Save cancelled", "Save Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information); // if nto show message and exit save routine
                return;
            }
            //Scanner1.TIFCompression = CompressionLZW; // set TIF compression to be used if saved in TIF format

            result = (this.Scanner1.SaveAllPage2PDF(strFile, true, 1) ? -1 : 0); // save in PDF format including all the pages in the image buffer in a multi page PDF file

            if (FCConvert.ToBoolean(result))
            {
                MessageBox.Show("Save " + strFile + " Complete");
            }
            else
            {
                MessageBox.Show("Save fail");
            }
            Close();
        }

        private void mnuZoomFitToWindow_Click(object sender, System.EventArgs e)
        {
            Scanner1.View = 9; // set view to be fitting the image to the scanner control size
            Scanner1.Focus();
        }

        private void Scanner1_EndScan(object sender, System.EventArgs e)
        {
            Scanner1.View = 9; // set view to fit to size
            lblTotalPage.Text = FCConvert.ToString(this.Scanner1.TotalPage); // set total number o fpages
            lblCurrentPage.Text = FCConvert.ToString(this.Scanner1.GetActivePageNo()); // set what page we are viewing
        }

        public string Init()
        {
            string Init = "";
			this.Show(FormShowEnum.Modal); // show screen
			Init = strFile; // return file name if one was saved
            return Init;
        }

    }
}