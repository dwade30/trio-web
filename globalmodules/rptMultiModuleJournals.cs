﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptMultiModuleJournals : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/05/2006              *
		// ********************************************************
		clsDRWrapper rsInfo = new clsDRWrapper();
		// VBto upgrade warning: curDebitTotal As double	OnWrite(short, double)
		decimal curDebitTotal;
		// VBto upgrade warning: curCreditTotal As double	OnWrite(short, double)
		decimal curCreditTotal;
		bool blnFirstControl;
		clsDRWrapper rsControlInfo = new clsDRWrapper();
		bool blnFirstRecord;
		bool blnLineShown;
		bool blnOOB;

		public rptMultiModuleJournals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += RptMultiModuleJournals_ReportEnd;
		}

        private void RptMultiModuleJournals_ReportEnd(object sender, EventArgs e)
        {
            rsInfo.DisposeOf();
        }

        public static rptMultiModuleJournals InstancePtr
		{
			get
			{
				return (rptMultiModuleJournals)Sys.GetInstance(typeof(rptMultiModuleJournals));
			}
		}

		protected rptMultiModuleJournals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
				rsControlInfo.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			CheckAgain:
			;
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (blnFirstControl)
				{
					eArgs.EOF = rsControlInfo.EndOfFile();
					if (!eArgs.EOF)
					{
						// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (((rsControlInfo.Get_Fields("Amount"))) == 0)
						{
							goto CheckAgain;
						}
					}
				}
				else
				{
					if (rsInfo.EndOfFile())
					{
						blnFirstRecord = true;
						blnFirstControl = true;
						goto CheckAgain;
					}
					else
					{
						eArgs.EOF = false;
						// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (((rsInfo.Get_Fields("Amount"))) == 0)
						{
							goto CheckAgain;
						}
					}
				}
			}
			else
			{
				if (blnFirstControl)
				{
					rsControlInfo.MoveNext();
					eArgs.EOF = rsControlInfo.EndOfFile();
					if (!eArgs.EOF)
					{
						// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (((rsControlInfo.Get_Fields("Amount"))) == 0)
						{
							goto CheckAgain;
						}
					}
				}
				else
				{
					rsInfo.MoveNext();
					if (rsInfo.EndOfFile())
					{
						blnFirstRecord = true;
						blnFirstControl = true;
						goto CheckAgain;
					}
					else
					{
						eArgs.EOF = false;
						// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (((rsInfo.Get_Fields("Amount"))) == 0)
						{
							goto CheckAgain;
						}
					}
				}
			}
			//Detail_Format();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDesc = new clsDRWrapper();
				//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
				////////Application.DoEvents();
				rsInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE RCB <> 'L' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period, "TWBD0000.vb1");
				rsControlInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE RCB = 'L' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period, "TWBD0000.vb1");
				if (rsInfo.BeginningOfFile() != true && rsInfo.EndOfFile() != true)
				{
					rsInfo.MoveLast();
					rsInfo.MoveFirst();
				}
				blnFirstRecord = true;
				blnLineShown = false;
				rsDesc.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber), "TWBD0000.vb1");
				if (rsDesc.EndOfFile() != true && rsDesc.BeginningOfFile() != true)
				{
					lblDescription.Text = rsDesc.Get_Fields_String("Description");
					blnOOB = FCConvert.ToBoolean(rsDesc.Get_Fields_Boolean("PostedOOB"));
				}
				else
				{
					lblDescription.Text = "UNKNOWN";
					blnOOB = false;
				}
				Label6.Text = "Journal No. " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + "       Post Date: " + DateTime.Today.ToShortDateString() + "       Type: " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalType;
				curDebitTotal = 0;
				curCreditTotal = 0;
				blnFirstControl = false;
				Line2.Visible = false;
				rsDesc.DisposeOf();
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Journal Start Error");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (blnFirstControl)
				{
					if (!blnLineShown)
					{
						Line2.Visible = true;
						blnLineShown = true;
					}
					else
					{
						Line2.Visible = false;
					}
				}
				if (!blnFirstControl)
				{
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					Field1.Text = Strings.Format(rsInfo.Get_Fields("Period"), "00");
					Field2.Text = Strings.Format(rsInfo.Get_Fields_DateTime("JournalEntriesDate"), "MM/dd/yyyy");
					Field3.Text = rsInfo.Get_Fields_String("Description");
					Field4.Text = rsInfo.Get_Fields_String("RCB");
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "W")
					{
						Field5.Text = "C";
					}
					else
					{
						// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
						Field5.Text = FCConvert.ToString(rsInfo.Get_Fields("Type"));
					}
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					Field6.Text = rsInfo.Get_Fields_String("Account");
					Field7.Text = rsInfo.Get_Fields_String("Project");
					if (FCConvert.ToString(rsInfo.Get_Fields_String("RCB")) != "C")
					{
						// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsInfo.Get_Fields_String("RCB")) != "E" && FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) < 0)
						{
							// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field8.Text = Strings.Format(rsInfo.Get_Fields("Amount") * -1, "#,##0.00");
							Field9.Text = "";
							if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Revenue CTL")
							{
								// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curCreditTotal += (FCConvert.ToDecimal(rsInfo.Get_Fields("Amount")) * -1);
							}
						}
						else
						{
							// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field9.Text = Strings.Format(rsInfo.Get_Fields("Amount"), "#,##0.00");
							Field8.Text = "";
							// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
							if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Revenue CTL" && (FCConvert.ToString(rsInfo.Get_Fields("Type")) != "P" || FCConvert.ToString(rsInfo.Get_Fields_String("RCB")) != "E"))
							{
								// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curDebitTotal += FCConvert.ToDecimal(rsInfo.Get_Fields("Amount"));
							}
						}
					}
					else
					{
						// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsInfo.Get_Fields("Amount")) < 0)
						{
							// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field9.Text = Strings.Format(rsInfo.Get_Fields("Amount"), "#,##0.00");
							Field8.Text = "";
							if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Revenue CTL")
							{
								// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curDebitTotal += FCConvert.ToDecimal(rsInfo.Get_Fields("Amount"));
							}
						}
						else
						{
							// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field8.Text = Strings.Format(rsInfo.Get_Fields("Amount") * -1, "#,##0.00");
							Field9.Text = "";
							if (FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsInfo.Get_Fields_String("Description")) != "Revenue CTL")
							{
								// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curCreditTotal += (FCConvert.ToDecimal(rsInfo.Get_Fields("Amount")) * -1);
							}
						}
					}
				}
				else
				{
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					Field1.Text = Strings.Format(rsControlInfo.Get_Fields("Period"), "00");
					// TODO: Field [Date] not found!! (maybe it is an alias?)
					Field2.Text = FCConvert.ToString(rsControlInfo.Get_Fields("Date"));
					Field3.Text = rsControlInfo.Get_Fields_String("Description");
					Field4.Text = rsControlInfo.Get_Fields_String("RCB");
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsControlInfo.Get_Fields("Type")) == "W")
					{
						Field5.Text = "C";
					}
					else
					{
						// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
						Field5.Text = FCConvert.ToString(rsControlInfo.Get_Fields("Type"));
					}
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					Field6.Text = rsControlInfo.Get_Fields_String("Account");
					Field7.Text = rsControlInfo.Get_Fields_String("Project");
					if (FCConvert.ToString(rsControlInfo.Get_Fields_String("RCB")) != "C")
					{
						// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsControlInfo.Get_Fields_String("RCB")) != "E" && FCConvert.ToInt32(rsControlInfo.Get_Fields("Amount")) < 0)
						{
							// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field8.Text = Strings.Format(rsControlInfo.Get_Fields("Amount") * -1, "#,##0.00");
							Field9.Text = "";
							if (FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Revenue CTL")
							{
								// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curCreditTotal += (FCConvert.ToDecimal(rsControlInfo.Get_Fields("Amount")) * -1);
							}
						}
						else
						{
							// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field9.Text = Strings.Format(rsControlInfo.Get_Fields("Amount"), "#,##0.00");
							Field8.Text = "";
							if (FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Revenue CTL")
							{
								// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curDebitTotal += FCConvert.ToDecimal(rsControlInfo.Get_Fields("Amount"));
							}
						}
					}
					else
					{
						// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsControlInfo.Get_Fields("Amount")) < 0)
						{
							// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field9.Text = Strings.Format(rsControlInfo.Get_Fields("Amount"), "#,##0.00");
							Field8.Text = "";
							if (FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Revenue CTL")
							{
								// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curDebitTotal += FCConvert.ToDecimal(rsControlInfo.Get_Fields("Amount"));
							}
						}
						else
						{
							// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
							Field8.Text = Strings.Format(rsControlInfo.Get_Fields("Amount") * -1, "#,##0.00");
							Field9.Text = "";
							if (FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Expense CTL" && FCConvert.ToString(rsControlInfo.Get_Fields_String("Description")) != "Revenue CTL")
							{
								// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
								curCreditTotal += (FCConvert.ToDecimal(rsControlInfo.Get_Fields("Amount")) * -1);
							}
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Journal Format Error");
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			////////Application.DoEvents();
			if (blnOOB)
			{
				lblBalance.Visible = true;
			}
			else
			{
				lblBalance.Visible = false;
			}
			fldDebitTotal.Text = Strings.Format(curDebitTotal, "#,##0.00");
			fldCreditTotal.Text = Strings.Format(curCreditTotal, "#,##0.00");
			rptSubTotals.Report = rptMultiModuleJournalTotals.InstancePtr;
			rptSubTotals.Report.UserData = this.UserData;
		}

		private void rptMultiModuleJournals_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMultiModuleJournals.Icon	= "rptMultiModuleJournals.dsx":0000";
			//rptMultiModuleJournals.Left	= 0;
			//rptMultiModuleJournals.Top	= 0;
			//rptMultiModuleJournals.Width	= 11880;
			//rptMultiModuleJournals.Height	= 8595;
			//rptMultiModuleJournals.StartUpPosition	= 3;
			//rptMultiModuleJournals.SectionData	= "rptMultiModuleJournals.dsx":058A;
			//End Unmaped Properties
		}
	}
}
