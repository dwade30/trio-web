﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmPickRegionalTown.
	/// </summary>
	public partial class frmPickRegionalTown : BaseForm
	{
		public frmPickRegionalTown()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPickRegionalTown InstancePtr
		{
			get
			{
				return (frmPickRegionalTown)Sys.GetInstance(typeof(frmPickRegionalTown));
			}
		}

		protected frmPickRegionalTown _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// Property of TRIO Software Corporation
		/// </summary>
		/// <summary>
		/// Written By
		/// </summary>
		/// <summary>
		/// Corey Gray
		/// </summary>
		/// <summary>
		/// Date
		/// </summary>
		/// <summary>
		/// 09/13/2005
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		int intReturn;
		private bool boolAllowCombinedAsChoice;
		private int intDefault;
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Init(short intDefaultTown = -1, bool boolAllowCombined = false, string strMessage = "")
		{
			short Init = 0;
			int x;
			lblMessage.Text = strMessage;
			Init = -1;
			boolAllowCombinedAsChoice = boolAllowCombined;
			intDefault = intDefaultTown;
			intReturn = -1;
			if (intDefault >= 0)
			{
				for (x = 0; x <= cmbTown.Items.Count - 1; x++)
				{
					if (cmbTown.ItemData(x) == intDefault)
					{
						cmbTown.SelectedIndex = x;
						break;
					}
				}
				// x
			}
			this.Show(FormShowEnum.Modal);
			Init = FCConvert.ToInt16(intReturn);
			return Init;
		}

		private void frmPickRegionalTown_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPickRegionalTown_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPickRegionalTown properties;
			//frmPickRegionalTown.FillStyle	= 0;
			//frmPickRegionalTown.ScaleWidth	= 3885;
			//frmPickRegionalTown.ScaleHeight	= 1995;
			//frmPickRegionalTown.LinkTopic	= "Form2";
			//frmPickRegionalTown.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			FillCombo();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void FillCombo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			int x;
			strSQL = "Select * from tblRegions ";
			if (!boolAllowCombinedAsChoice)
			{
				strSQL += " where townnumber > 0 ";
			}
			strSQL += " order by townnumber ";
			clsLoad.OpenRecordset(strSQL, "CentralData");
			cmbTown.Clear();
			while (!clsLoad.EndOfFile())
			{
				cmbTown.AddItem(clsLoad.Get_Fields_String("TownName"));
				// TODO: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
				cmbTown.ItemData(cmbTown.NewIndex, FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("TownNumber"))));
				clsLoad.MoveNext();
			}
			// If intDefault >= 0 Then
			// For x = 0 To .ListCount - 1
			// If .ItemData(x) = intDefault Then
			// .listindex = x
			// Exit For
			// End If
			// Next x
			// End If
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (cmbTown.SelectedIndex >= 0)
			{
				intReturn = cmbTown.ItemData(cmbTown.SelectedIndex);
				Close();
			}
		}
	}
}
