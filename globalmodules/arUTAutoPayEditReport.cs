using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using SharedApplication.Extensions;
using TWSharedLibrary;
using TWSharedLibrary.Data;
using Wisej.Web;

namespace Global
{
    /// <summary>
    /// Summary description for arUTAutoPayEditReport.
    /// </summary>
    public partial class arUTAutoPayEditReport : BaseSectionReport
    {

        public arUTAutoPayEditReport()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            if (_InstancePtr == null)
                _InstancePtr = this;
            this.Name = "Auto-Pay Edit Report";
            this.PageStart += ArUTAutoPayEditReport_PageStart;
            this.FetchData += ArUTAutoPayEditReport_FetchData;
            this.ReportStart += ArUTAutoPayEditReport_ReportStart;
            this.detail.Format += Detail_Format;
            this.reportFooter1.Format += ReportFooter1_Format;
            this.ReportEnd += ArUTAutoPayEditReport_ReportEnd;
        }

        private void ArUTAutoPayEditReport_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        private void ArUTAutoPayEditReport_PageStart(object sender, EventArgs e)
        { 
            lblPage.Text = "Page " + this.PageNumber.ToString();
        }

        protected arUTAutoPayEditReport _InstancePtr = null;
        public static arUTAutoPayEditReport InstancePtr
        {
            get
            {
                return (arUTAutoPayEditReport)Sys.GetInstance(typeof(arUTAutoPayEditReport));
            }
        }

        private autoPayRptType reportType;
        private bool boolPreview = false;
        private string strBatchNumber = "";
        private clsDRWrapper rsData;
        private double dblTotal = 0;
        private int lngNumOfAccounts = 0;
        public void Init(autoPayRptType passRptType,bool boolPassPreview, string strPassBatchNumber)
        {
            reportType = passRptType;
            boolPreview = boolPassPreview;
            strBatchNumber = strPassBatchNumber;

            frmReportViewer.InstancePtr.Init(this);
        }

        private void ArUTAutoPayEditReport_ReportStart(object sender, EventArgs e)
        {
            string strSQL = "";
            string strQWhere = "";

            dblTotal = 0;
            lngNumOfAccounts = 0;

            lblDate.Text = DateTime.Now.FormatAndPadShortDate();
            lblTime.Text = DateTime.Now.FormatAndPadTime();
            lblMuniName.Text = modGlobalConstants.Statics.MuniName;

            switch (reportType)
            {
                case autoPayRptType.autoPayRptTypePost:
                    if (boolPreview)
                    {
                        strQWhere = " and ACHDone = 1 and LineNumber > 0 and Posted = 0 and ACHFlag = ";
                        lblPageHeader.Text = "ACH Payment Preview";
                    }
                    else
                    {
                        strQWhere = " and ACHDone = 1 and LineNumber > 0 and Posted = 1 and ACHFlag = 1 ";
                        lblPageHeader.Text = "ACH Payment Report";
                    }

                    lblDate1.Text = "DW Date";
                    break;
                case autoPayRptType.autoPayRptTypeACH:
                    if (boolPreview)
                    {
                        strQWhere = " and ACHDone = 0 And Posted = 0 and ACHFlag = 1 ";
                        lblPageHeader.Text = "ACH File Preview";
                    }
                    else
                    {
                        strQWhere = " and ACHDone = 1 and Posted = 0 and ACHFlag = 1 ";
                        lblPageHeader.Text = "ACH File Report";
                    }

                    lblDate1.Text = "DW Date";
                    break;
                case autoPayRptType.autoPayRptTypeProc:
                    strQWhere = " And ACHDone = 0 and Posted = 0 and ACHFlag = 1 ";
                    lblPageHeader.Text = "ACH Process Report";
                    lblDate1.Text = "Proc Date";
                    break;
                case autoPayRptType.autoPayRptTypePreNote:
                    if (boolPreview)
                    {
                        strQWhere = " And ACHDone = 0 and Posted = 0 and ACHFlag = 1 ";
                        lblPageHeader.Text = "PreNote File Preview";
                    }
                    else
                    {
                        strQWhere = " And ACHDone = 1 and Posted = 0 and ACHFlag = 1 ";
                        lblPageHeader.Text = "PreNote File Report";
                    }

                    lblDate1.Text = "Proc Date";
                    break;
            }

            strSQL = "Select * from tblAutoPay where batchnumber = '" + strBatchNumber + "'" + strQWhere.Trim() +
                     " order by accountnumber";
            rsData = new clsDRWrapper();
            rsData.OpenRecordset(strSQL, "UtilityBilling");
            if (!rsData.EndOfFile())
            {
                lblBatchNumber.Text = "Batch Number: " + strBatchNumber;
                switch (reportType)
                {
                    case autoPayRptType.autoPayRptTypePost:
                        if (!boolPreview)
                        {
                            lblProcessDate.Text =
                                "Payments Posted: " + rsData.Get_Fields_DateTime("PostedDate").FormatAndPadShortDate();
                        }
                        else
                        {
                            lblProcessDate.Text = "";
                        }
                        break;
                    case autoPayRptType.autoPayRptTypeACH:
                        lblProcessDate.Text = "";
                        break;
                    case autoPayRptType.autoPayRptTypeProc:
                        lblProcessDate.Text = "Payment Date: " +
                                              rsData.Get_Fields_DateTime("EffPmtDate").FormatAndPadShortDate();
                        break;
                    case autoPayRptType.autoPayRptTypePreNote:
                        lblProcessDate.Text = "Process Date: " +
                                              rsData.Get_Fields_DateTime("EffPmtDate").FormatAndPadShortDate();
                        break;
                }
            }
        }

        private void ArUTAutoPayEditReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = rsData.EndOfFile();
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            if (rsData != null && !rsData.EndOfFile())
            {
                BindFields();
                rsData.MoveNext();
            }
        }

        private void BindFields()
        {
            try
            {
                var rsBill = new clsDRWrapper();

                string strWS = rsData.Get_Fields_String("Service");
                if (reportType != autoPayRptType.autoPayRptTypePreNote)
                {
                    rsBill.OpenRecordset("Select * from Bill where id = " + rsData.Get_Fields_Int32("BillKey"),
                        "UtilityBIlling");
                }
                else
                {
                    rsBill.OpenRecordset(
                        "Select UTBook as Book, 0 as BillNumber, 0 as SLienRecordNumber, 0 as WLienRecordNumber from Master where Accountnumber = " +
                        rsData.Get_Fields_Int32("AccountNumber"), "UtilityBilling");
                }

                if (!rsBill.EndOfFile())
                {
                    if (rsBill.Get_Fields_Int32(strWS + "LienRecordNumber") == 0)
                    {
                        fldAccount.Text = rsData.Get_Fields_Int32("AccountNumber").ToString();
                        fldName.Text = rsData.Get_Fields_String("Name");
                        fldBook.Text = rsData.Get_Fields_Int32("Book").ToString();
                        fldBill.Text = rsData.Get_Fields_Int32("BillNumber").ToString();
                        fldAmount.Text =
                            fecherFoundation.Strings.Format(rsData.Get_Fields_Double("ACHAmount"), "#,##0.00");
                        switch (reportType)
                        {
                            case autoPayRptType.autoPayRptTypePost:
                                fldDate1.Text = rsData.Get_Fields_DateTime("EffPmtDate").FormatAndPadShortDate();
                                break;
                            case autoPayRptType.autoPayRptTypeACH:
                                fldDate1.Text = rsData.Get_Fields_DateTime("EffPmtDate").FormatAndPadShortDate();
                                break;
                            case autoPayRptType.autoPayRptTypeProc:
                                fldDate1.Text = rsData.Get_Fields_DateTime("EffPmtDate").FormatAndPadShortDate();
                                break;
                            case autoPayRptType.autoPayRptTypePreNote:
                                fldDate1.Text = rsData.Get_Fields_DateTime("EffPmtDate").FormatAndPadShortDate();
                                fldAmount.Text = "0.00";
                                break;
                            default:
                                fldDate1.Text = rsData.Get_Fields_DateTime("BillDate").FormatAndPadShortDate();
                                break;
                        }

                        dblTotal += rsData.Get_Fields_Double("ACHAmount");
                        lngNumOfAccounts += 1;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error Binding Fields", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void ReportFooter1_Format(object sender, EventArgs e)
        {
            lblTotals.Text = "Count: " + lngNumOfAccounts + "   Total:";
            if (reportType != autoPayRptType.autoPayRptTypePreNote)
            {
                fldTotalTotal.Text = fecherFoundation.Strings.Format(dblTotal, "#,##0.00");
            }
        }
    }

    public enum autoPayRptType
    { 
        autoPayRptTypePost = 0,
        autoPayRptTypeACH = 1,
        autoPayRptTypeProc = 2,
        autoPayRptTypePreNote = 3

    }
}

