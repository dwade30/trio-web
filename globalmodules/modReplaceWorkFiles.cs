﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	public class modReplaceWorkFiles
	{
		// VBto upgrade warning: 'Return' As Variant --> As string
		public static string StripColon(string strValue)
		{
			string StripColon = "";
			if (strValue.Length == 5 && Strings.Right(strValue, 1) == ":")
			{
				StripColon = Strings.Left(strValue, 4);
			}
			else
			{
				StripColon = strValue;
			}
			return StripColon;
		}
		// VBto upgrade warning: boolSave As object	OnWrite(bool)
		public static void EntryFlagFile(bool boolSave = false, string NewValue = "", bool boolUseRegistry = false)
		{
			// If boolUseRegistry Or gboolUseRegistryOnly Then
			App.DoEvents();
			if (boolSave == true)
			{
				// if true then write to the registry
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "EntryFlag", NewValue);
			}
			else if (boolSave == false)
			{
                // if false then read in from the registry and save it in gstrEntryFlag
                //FC:FINAL:AM:#2263 - re-read the entry all the time
                //if (Strings.Trim(modGlobalConstants.Statics.gstrEntryFlag) == "")
				{
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "EntryFlag", ref modGlobalConstants.Statics.gstrEntryFlag);
				}
			}
			// 
		}

		public static void SetDatabase()
		{
			Statics.secRS = null;
			Statics.secRS.OpenRecordset("SELECT * FROM Users WHERE ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "'", "ClientSettings");
		}

		public static void UpdateGlobalVariable(string strVariableName, object NewValue)
		{
			try
			{
				// On Error GoTo ErrorHandler
				Statics.rsVariables = new clsDRWrapper();
				Statics.rsVariables.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
				if (Statics.rsVariables.EndOfFile())
				{
					Statics.rsVariables.AddNew();
				}
				else
				{
					Statics.rsVariables.Edit();
				}
				Statics.rsVariables.Set_Fields(strVariableName, NewValue);
				Statics.rsVariables.Update();
				Statics.rsVariables = null;
				// Set dbVariables = Nothing
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Update Global Variables");
			}
		}

		public static void GetGlobalVariables()
		{
			try
			{
				// On Error GoTo ErrorHandler
				bool boolDoNotUseArchive;
				Statics.rsVariables = new clsDRWrapper();
				// If Not rsVariables.DoesFieldExist("ArchiveYear", "GlobalVariables", "SystemSettings") Then   'make sure that the ArchiveYear field exists
				boolDoNotUseArchive = true;
				// if it does not, then set this boolean to true so that strArchiveYear will get set to a null string
				// End If
				Statics.rsVariables.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
				// open the globalvariables table
				modGlobalConstants.Statics.MuniName = Strings.Trim(FCConvert.ToString(Statics.rsVariables.Get_Fields_String("MUNINAME")));
				modGlobalConstants.Statics.gstrCityTown = FCConvert.ToString(Statics.rsVariables.Get_Fields_String("CityTown"));
				// MAL@20080509: Add check for town seal path
				// Tracker Reference: 13691
				if (FCFileSystem.FileExists("TownSeal.pic"))
				{
					modGlobalConstants.Statics.gstrTownSealPath = FCFileSystem.Statics.UserDataFolder;
					if (Strings.Right(modGlobalConstants.Statics.gstrTownSealPath, 1) != "\\")
					{
						modGlobalConstants.Statics.gstrTownSealPath += "\\";
					}
					modGlobalConstants.Statics.gstrTownSealPath += "TownSeal.pic";
				}
				else
				{
					modGlobalConstants.Statics.gstrTownSealPath = "";
				}

                
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Get Global Variables");
			}
		}
		// VBto upgrade warning: 'Return' As object	OnWrite(string, object)
		public static object GetVariable(string strName)
		{
			object GetVariable = null;
			try
			{
				// On Error GoTo ErrorHandler
				Statics.rsVariables = new clsDRWrapper();
				Statics.rsVariables.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
				if (Statics.rsVariables.EndOfFile())
				{
					GetVariable = string.Empty;
				}
				else
				{
					GetVariable = Statics.rsVariables.Get_Fields(strName);
				}
				if (FCUtils.IsNull(GetVariable))
					GetVariable = "";
				return GetVariable;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Get Variable");
			}
			return GetVariable;
		}

		public class StaticVariables
		{
			// ********************************************************
			// Last Updated   :               01/14/2004              *
			// MODIFIED BY    :               Jim Bertolino           *
			// *
			// Date           :                                       *
			// WRITTEN BY     :                                       *
			// *
			// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
			// ********************************************************
			//=========================================================
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			//public clsDRWrapper secRS = new clsDRWrapper();
			//public clsDRWrapper rsVariables = new clsDRWrapper();
			public clsDRWrapper secRS_AutoInitialized;
			public clsDRWrapper rsVariables_AutoInitialized;

			public clsDRWrapper secRS
			{
				get
				{
					if (secRS_AutoInitialized == null)
					{
						secRS_AutoInitialized = new clsDRWrapper();
					}
					return secRS_AutoInitialized;
				}
				set
				{
					secRS_AutoInitialized = value;
				}
			}

			public clsDRWrapper rsVariables
			{
				get
				{
					if (rsVariables_AutoInitialized == null)
					{
						rsVariables_AutoInitialized = new clsDRWrapper();
					}
					return rsVariables_AutoInitialized;
				}
				set
				{
					rsVariables_AutoInitialized = value;
				}
			}

			
			public string gstrReturn = "";
			public string gstrNetworkFlag = "";
			public int gintSecurityID;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
