﻿
/**
 * Initializes the widget.
 *
 * This function is called when the InitScript property of
 * wisej.web.Widget changes.
 *
 * "this" refers to the container which is a wisej.web.Widget instance.
 *
 * The widget has an inner container with id = "container" that can
 * be used referring to this.container.
 *
 */
//JEI
this.init = function () {

    var me = this;
    // prepare the configuration map.
    // [$]options is a placeholder that is replaced with the options    
    var options = $options;

    if (this.arviewer == null) {
        // create the inner input control.
        var el = this.container;
        var id = "viewerContainer";
        el.innerHTML = "<div id=\"" + id + "\" style=\"width: 100%; height: 100%\"/>";

        // save a reference to knob widget.
        this.arviewer = $("#" + id);   
    }

    if (this.viewer != null) {
        this.viewer.destroy();
    }

    this.viewer = GrapeCity.ActiveReports.Viewer({
        element: '#viewerContainer',
        reportService: {
            url: options.reportServiceUrl
        },
        uiType: 'desktop',
        //,
        //localeUri: 'Scripts/i18n/Localeuri.txt'
    });
    $(window).bind('beforeunload', function () {
        this.viewer.destroy();
    });


    var reportOption = {
        id: options.report 
    };
    if (this.viewer != null) {
        this.viewer.option('report', reportOption);
    }
    
}




