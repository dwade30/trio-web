﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;
#if TWAR0000
using TWAR0000;


#else
using TWCR0000;

#endif
namespace Global
{
	public class modARCalculations
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/22/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/23/2005              *
		// ********************************************************
		public static void ARSetup()
		{
			int intError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper WWK = new clsDRWrapper();
				clsDRWrapper WAR = new clsDRWrapper();
				if (WWK.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings"))
				{
					modGlobalConstants.Statics.gstrCityTown = FCConvert.ToString(WWK.Get_Fields_String("CityTown"));
				}
				WAR.OpenRecordset("SELECT * FROM Customize", modExtraModules.strARDatabase);
				if (!WAR.EndOfFile())
				{
					// Collections Database
					modGlobal.Statics.gintBasis = FCConvert.ToInt32(WAR.Get_Fields_Int32("Basis"));
					StaticSettings.gGlobalAccountsReceivableSettings.gintBasis = modGlobal.Statics.gintBasis;

					modARStatusPayments.Statics.gboolShowLastARAccountInCR = FCConvert.CBool(WAR.Get_Fields_Boolean("ShowLastARAccountInCR"));
					StaticSettings.gGlobalAccountsReceivableSettings.ShowLastARAccountInCR =
						modARStatusPayments.Statics.gboolShowLastARAccountInCR;
				}
				intError = 39;
				// this will setup the town seal information
				if (FCFileSystem.FileExists("TOWNSEAL.PIC"))
				{
					modGlobalConstants.Statics.gstrTownSealPath = Path.Combine(FCFileSystem.Statics.UserDataFolder, "TOWNSEAL.PIC");
				}
				else
				{
					modGlobalConstants.Statics.gstrTownSealPath = "";
				}
				intError = 40;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "AR Collection Setup Error - " + FCConvert.ToString(intError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static double CalculateAccountAR(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXtraInt)
		{
			double dblCurPrin = 0;
			double dblCurInt = 0;
			DateTime dtLastInterestDate = DateTime.Now;
			double dblPerDiem = 0;
			return CalculateAccountAR_CS(rsTemp, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, dtLastInterestDate, ref dblPerDiem);
		}

		public static double CalculateAccountAR(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXtraInt, ref double dblPerDiem)
		{
			double dblCurPrin = 0;
			double dblCurInt = 0;
			DateTime dtLastInterestDate = DateTime.Now;
			return CalculateAccountAR_CS(rsTemp, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, dtLastInterestDate, ref dblPerDiem);
		}

		public static double CalculateAccountAR(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXtraInt, bool boolShowError)
		{
			double dblCurPrin = 0;
			double dblCurInt = 0;
			DateTime dtLastInterestDate = DateTime.Now;
			double dblPerDiem = 0;
			return CalculateAccountAR_CS(rsTemp, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, dtLastInterestDate, ref dblPerDiem, boolShowError: boolShowError);
		}

		public static double CalculateAccountAR(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXtraInt, DateTime dtLastInterestDate, ref double dblPerDiem)
		{
			double dblCurPrin = 0;
			double dblCurInt = 0;
			return CalculateAccountAR_CS(rsTemp, dtDate, ref dblXtraInt, ref dblCurPrin, ref dblCurInt, dtLastInterestDate, ref dblPerDiem);
		}
		// was
		//Public Function CalculateAccountAR(
		//ByRef rsTemp As clsDRWrapper,
		//dtDate As Date,
		//ByRef dblXtraInt As Double,
		//Optional ByRef dblCurPrin As Double = 0,
		//Optional ByRef dblCurInt As Double = 0,
		//Optional boolShowError As Boolean = False,
		//Optional dtLastInterestDate As Date,
		//Optional dblCurTax As Double = 0,
		//Optional dblPerDiem As Double,
		//Optional boolForceDemandCalculation As Boolean = False
		//) As Double
		//
		// FC:FINAL:MW
		// - param rsTemp is object reference => no ref
		// - param cblCurPrin and dblCurInt => can't specify default value for ref params; provide func def with different signatrue
		// - all optional params must be at the end of param list => move dtLastInterestDate and dblPerDiem
		public static double CalculateAccountAR_CS(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXtraInt, ref double dblCurPrin/* = 0 */, ref double dblCurInt/* = 0 */, DateTime dtLastInterestDate, ref double dblPerDiem, bool boolShowError = false, double dblCurTax = 0, bool boolForceDemandCalculation = false)
		{
			double CalculateAccountAR = 0;
			// this will calculate the total amount owed on this person's bill at the payment date
			clsDRWrapper rsCalc = new clsDRWrapper();
			bool boolContinue;
			DateTime dtStartDate;
			DateTime dtDueDate = DateTime.FromOADate(0);
			double dblPrinDuePerPeriod = 0;
			int intCT;
			double dblIntRate = 0;
			DateTime dtIntPaidDate = DateTime.FromOADate(0);
			double dblPrinPaid = 0;
			double dblTaxPaid = 0;
			double dblIntPaid = 0;
			string strIntType = "";
			bool boolAutoInterest = false;
			bool boolPerDiem = false;
			double dblFlatRate = 0;
			int intDayAdded = 0;
			boolContinue = true;
			// get the tax rate from the rate rec
			if (rsCalc.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsTemp.Get_Fields_Int32("BillNumber"), modExtraModules.strARDatabase))
			{
				if (rsCalc.EndOfFile() != true && rsCalc.BeginningOfFile() != true)
				{
					dblIntRate = rsCalc.Get_Fields_Double("IntRate");
					dblFlatRate = FCConvert.ToDouble(rsCalc.Get_Fields_Decimal("FlatRate"));
				}
				else
				{
					if (boolShowError)
						MessageBox.Show("Error retrieving Rate Table Information.", "Rate Information Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					boolContinue = false;
				}
			}
			else
			{
				if (boolShowError)
					MessageBox.Show("Error retrieving Rate Table Information.", "Rate Information Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				boolContinue = false;
			}
			if (boolContinue)
			{
				strIntType = FCConvert.ToString(rsCalc.Get_Fields_String("InterestMethod"));
				boolAutoInterest = FCConvert.CBool(Strings.Left(strIntType, 1) == "A");
				boolPerDiem = FCConvert.CBool(Strings.Right(strIntType, 1) == "P");
				if (Information.IsDate(rsTemp.Get_Fields("IntPaidDate")))
				{
					dtIntPaidDate = rsTemp.Get_Fields_DateTime("IntPaidDate");
				}
				if (dtIntPaidDate.ToOADate() == 0)
				{
					// if the date has not been set then get the bill creation date from the rate record
					if (rsCalc.Get_Fields_DateTime("IntStart").ToOADate() != 0)
					{
						dtIntPaidDate = (DateTime)rsCalc.Get_Fields_DateTime("IntStart");
					}
				}
				// amount paid
				// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
				dblPrinPaid = FCConvert.ToDouble(rsTemp.Get_Fields("PrinPaid"));
				// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
				dblTaxPaid = FCConvert.ToDouble(rsTemp.Get_Fields("TaxPaid"));
				// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
				dblIntPaid = FCConvert.ToDouble(rsTemp.Get_Fields("IntPaid"));
				// this is the net principal
				dblCurPrin = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("PrinOwed")) - dblPrinPaid;
				// this is the net tax
				dblCurTax = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("TaxOwed")) - dblTaxPaid;
				// this is the net tax
				dblCurInt = 0 - dblIntPaid;
				// this is the amount to have interest calculated on
				dblPrinDuePerPeriod = dblCurPrin;
				// fill the interest dates
				dtStartDate = (DateTime)rsCalc.Get_Fields_DateTime("IntStart");
				if (dtIntPaidDate.ToOADate() > dtStartDate.ToOADate())
				{
					intDayAdded = 0;
					dtStartDate = dtIntPaidDate;
				}
				else
				{
					intDayAdded = 1;
				}
				// dtDueDate = rsCalc.Fields("End")
				if (dtIntPaidDate.ToOADate() > dtDueDate.ToOADate())
				{
					dtDueDate = dtIntPaidDate;
				}
				// find out how much interest is due
				// DJW 6/5/2008 Took out Multiply by -1
				// DJW 11/14/2013 added back in -1 multiplier
				// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
				dblCurInt = FCConvert.ToDouble(rsTemp.Get_Fields("IntAdded")) * -1;
				// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
				dblCurInt -= FCConvert.ToDouble(rsTemp.Get_Fields("IntPaid"));
				// find out current interest
				if (dtDate.ToOADate() < dtDueDate.ToOADate() && dtDueDate.ToOADate() > 0)
				{
					// this is the base scenerio...payment is before interest starts
					dblXtraInt = 0;
					dtLastInterestDate = dtDueDate;
				}
				else
				{
					if (dtDate.ToOADate() < dtIntPaidDate.ToOADate())
					{
						// this is if interest has been charged to the period already
						dblXtraInt = 0;
						dtLastInterestDate = dtIntPaidDate;
					}
					else
					{
						dtLastInterestDate = dtStartDate;
						if (boolAutoInterest || boolForceDemandCalculation)
						{
							if (dblPrinDuePerPeriod > 0)
							{
								if (boolPerDiem)
								{
									dblXtraInt = FCConvert.ToDouble(CalculateARInterest(dblPrinDuePerPeriod, ref dtStartDate, ref dtDate, ref dblIntRate, ref modGlobal.Statics.gintBasis, intDayAdded, modExtraModules.Statics.dblOverPayRate, ref dblPerDiem));
								}
								else
								{
									// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
									if (rsTemp.Get_Fields("IntAdded") == 0)
									{
										dblXtraInt = dblFlatRate;
									}
								}
							}
							else
							{
								// if there is no principal due or if this is an on-demand bill then return 0.00
								dblXtraInt = 0;
							}
						}
					}
				}
			}
			if (boolContinue)
			{
				dblCurInt += dblXtraInt;
				CalculateAccountAR = dblCurPrin + dblCurTax + dblCurInt;
			}
			else
			{
				// reset values
				dblCurPrin = 0;
				dblCurInt = 0;
				dblCurTax = 0;
				CalculateAccountAR = 0;
			}
			return CalculateAccountAR;
		}

		public static double CalculateAccountARTotal_2(int lngAccountID, double dblTotalCurrentInterest = 0, double dblTotalChargedInterest = 0, DateTime? dtDate = null)
		{
			return CalculateAccountARTotal(lngAccountID, ref dblTotalCurrentInterest, ref dblTotalChargedInterest, ref dtDate);
		}

		public static double CalculateAccountARTotal(int lngAccountID, ref double dblTotalCurrentInterest, ref double dblTotalChargedInterest, ref DateTime? dtDateTemp)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			double CalculateAccountARTotal = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will calculate the total remaining balance and
				// return it as a double for the account and type passed in
				clsDRWrapper rsCL = new clsDRWrapper();
				string strSQL;
				double dblTotal = 0;
				double dblXtraInt = 0;
				if (dtDate.ToOADate() == 0)
				{
					dtDate = DateTime.Today;
				}
				strSQL = "SELECT * FROM Bill WHERE BillStatus <> 'V' AND ActualAccountNumber = " + FCConvert.ToString(lngAccountID);
				rsCL.OpenRecordset(strSQL, modExtraModules.strARDatabase);
				if (!rsCL.EndOfFile())
				{
					while (!rsCL.EndOfFile())
					{
						// calculate each year
						dblTotal += CalculateAccountAR(rsCL, dtDate, ref dblXtraInt);
						dblTotalCurrentInterest += dblXtraInt;
						// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
						dblTotalChargedInterest += ((FCConvert.ToDouble(rsCL.Get_Fields("IntAdded")) * -1) - FCConvert.ToDouble(rsCL.Get_Fields("IntPaid")));
						rsCL.MoveNext();
					}
				}
				else
				{
					// nothing found
					dblTotal = 0;
				}
				CalculateAccountARTotal = dblTotal;
				return CalculateAccountARTotal;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CalculateAccountARTotal = 0;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Calculate Account Total Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CalculateAccountARTotal;
		}

		public static int CreateDemandJournal()
		{
			int CreateDemandJournal = 0;
			clsDRWrapper rsCheck = new clsDRWrapper();
			int lngJournal = 0;
			string strSQL = "";
			clsDRWrapper rsStatusCheck = new clsDRWrapper();
			bool blnVoid;
			int lngJournal2 = 0;
			clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
			clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
			CreateDemandJournal = 0;
			rsStatusCheck.OpenRecordset("SELECT DISTINCT BillStatus FROM Bill WHERE IsNull(JournalNumber, 0) = 0", "TWAR0000.vb1");
			blnVoid = false;
			if (rsStatusCheck.EndOfFile() != true && rsStatusCheck.BeginningOfFile() != true)
			{
				if (rsStatusCheck.RecordCount() > 1)
				{
					do
					{
						if (FCConvert.ToString(rsStatusCheck.Get_Fields_String("BillStatus")) == "V")
						{
							blnVoid = true;
							break;
						}
						rsStatusCheck.MoveNext();
					}
					while (rsStatusCheck.EndOfFile() != true);
				}
			}
			if (blnVoid)
			{
				strSQL = "(";
				rsCheck.OpenRecordset("SELECT * FROM Bill WHERE BillType <> 0 AND isnull(JournalNumber, 0) = 0 AND BillStatus = 'V'", "TWAR0000.vb1");
				if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
				{
					if (modGlobalConstants.Statics.gboolBD)
					{
						do
						{
							strSQL += rsCheck.Get_Fields_Int32("ID") + ", ";
							rsCheck.MoveNext();
						}
						while (rsCheck.EndOfFile() != true);
						strSQL = Strings.Left(strSQL, strSQL.Length - 2) + ")";
						lngJournal = 0;
						CreateJournalEntry_3(ref lngJournal, strSQL);
						if (lngJournal == 0)
						{
							MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a journal.  You must go into the Budgetary System > Data Entry > Build Incomplete Journals to finish creating the Budgetary Journal.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
				strSQL = "(";
				rsCheck.OpenRecordset("SELECT * FROM Bill WHERE BillType <> 0 AND isnull(JournalNumber, 0) = 0 AND BillStatus <> 'V'", "TWAR0000.vb1");
				if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
				{
					if (modGlobalConstants.Statics.gboolBD)
					{
						do
						{
							strSQL += rsCheck.Get_Fields_Int32("ID") + ", ";
							rsCheck.MoveNext();
						}
						while (rsCheck.EndOfFile() != true);
						strSQL = Strings.Left(strSQL, strSQL.Length - 2) + ")";
						lngJournal2 = 0;
						CreateJournalEntry_3(ref lngJournal2, strSQL);
						if (lngJournal == 0)
						{
							MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a journal.  You must go into the Budgetary System > Data Entry > Build Incomplete Journals to finish creating the Budgetary Journal.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						else
						{
							if (modSecurity.ValidPermissions_8(null, modGlobal.AUTOPOSTBILLINGJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptARBills))
							{
								if (MessageBox.Show("The void entries have been saved into journal " + Strings.Format(lngJournal, "0000") + " and the billing entries have been saved into journal " + Strings.Format(lngJournal2, "0000") + ".  Would you like to post the journals?", "Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									clsJournalInfo = new clsPostingJournalInfo();
									clsJournalInfo.JournalNumber = lngJournal;
									clsJournalInfo.JournalType = "GJ";
									clsJournalInfo.Period = FCConvert.ToString(DateTime.Today.Month);
									clsJournalInfo.CheckDate = "";
									clsPostInfo.AddJournal(clsJournalInfo);
									clsJournalInfo = new clsPostingJournalInfo();
									clsJournalInfo.JournalNumber = lngJournal2;
									clsJournalInfo.JournalType = "GJ";
									clsJournalInfo.Period = FCConvert.ToString(DateTime.Today.Month);
									clsJournalInfo.CheckDate = "";
									clsPostInfo.AddJournal(clsJournalInfo);
									clsPostInfo.AllowPreview = true;
									clsPostInfo.PageBreakBetweenJournalsOnReport = true;
									clsPostInfo.WaitForReportToEnd = true;
									clsPostInfo.PostJournals();
								}
							}
							else
							{
								MessageBox.Show("The void entries have been saved into journal " + Strings.Format(lngJournal, "0000") + " and the billing entries have been saved into journal " + Strings.Format(lngJournal2, "0000"), "Entries Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							CreateDemandJournal = lngJournal2;
						}
					}
				}
				else
				{
					// MsgBox "No Information Found", vbInformation, "No Info"
				}
			}
			else
			{
				strSQL = "(";
				rsCheck.OpenRecordset("SELECT * FROM Bill WHERE BillType <> 0 AND isnull(JournalNumber, 0) = 0", "TWAR0000.vb1");
				if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
				{
					if (modGlobalConstants.Statics.gboolBD)
					{
						do
						{
							strSQL += rsCheck.Get_Fields_Int32("ID") + ", ";
							rsCheck.MoveNext();
						}
						while (rsCheck.EndOfFile() != true);
						strSQL = Strings.Left(strSQL, strSQL.Length - 2) + ")";
						lngJournal = 0;
						CreateJournalEntry_3(ref lngJournal, strSQL);
						if (lngJournal == 0)
						{
							MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a journal.  You must go into the Budgetary System > Data Entry > Build Incomplete Journals to finish creating the Budgetary Journal.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						else
						{
							if (modSecurity.ValidPermissions_8(null, modGlobal.AUTOPOSTBILLINGJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptARBills))
							{
								if (MessageBox.Show("The billing entries have been saved into journal " + Strings.Format(lngJournal, "0000") + ".  Would you like to post the journal?", "Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									clsJournalInfo = new clsPostingJournalInfo();
									clsJournalInfo.JournalNumber = lngJournal;
									clsJournalInfo.JournalType = "GJ";
									clsJournalInfo.Period = FCConvert.ToString(DateTime.Today.Month);
									clsJournalInfo.CheckDate = "";
									clsPostInfo.AddJournal(clsJournalInfo);
									clsPostInfo.AllowPreview = true;
									clsPostInfo.WaitForReportToEnd = true;
									clsPostInfo.PostJournals();
								}
							}
							else
							{
								MessageBox.Show("The billing entries have been saved into journal " + Strings.Format(lngJournal, "0000"), "Entries Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							CreateDemandJournal = lngJournal;
						}
					}
				}
				else
				{
					// MsgBox "No Information Found", vbInformation, "No Info"
				}
			}
			return CreateDemandJournal;
		}

		public static void CreateJournalEntry_3(ref int lngJournal, string strSQL = "")
		{
			CreateJournalEntry(ref lngJournal, -1, strSQL);
		}

		public static void CreateJournalEntry(ref int lngJournal, int lngBillKey = -1, string strSQL = "")
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_ErrorTrap = 1;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				clsDRWrapper rsEntries = new clsDRWrapper();
				clsDRWrapper rsBillInfo = new clsDRWrapper();
				clsDRWrapper rsDefaultInfo = new clsDRWrapper();
				// vbPorter upgrade warning: curTotals As Decimal	OnWrite(short, Decimal)
				Decimal[] curTotals = new Decimal[5 + 1];
				int counter;
				// vbPorter upgrade warning: curTotal As Decimal	OnWrite(short, Decimal)
				Decimal curTotal;
				clsDRWrapper Master = new clsDRWrapper();
				// vbPorter upgrade warning: curSalesTax As Decimal	OnWrite(short, Decimal)
				Decimal curSalesTax;
				clsDRWrapper rsBillTypes = new clsDRWrapper();
				// vbPorter upgrade warning: curPrePayment As Decimal	OnWrite(short, Decimal)
				Decimal curPrePayment;
				rsEntries.OmitNullsOnInsert = true;
				if (lngBillKey != -1)
				{
					rsBillTypes.OpenRecordset("SELECT DISTINCT BillType FROM Bill WHERE ID = " + FCConvert.ToString(lngBillKey), "TWAR0000.vb1");
				}
				else
				{
					rsBillTypes.OpenRecordset("SELECT DISTINCT BillType FROM Bill WHERE ID IN " + strSQL, "TWAR0000.vb1");
				}
				if (lngBillKey != -1)
				{
					// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
					rsBillInfo.OpenRecordset("SELECT * FROM Bill WHERE BillType = " + rsBillTypes.Get_Fields("BillType") + " AND ID = " + FCConvert.ToString(lngBillKey), "TWAR0000.vb1");
				}
				else
				{
					// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
					rsBillInfo.OpenRecordset("SELECT * FROM Bill WHERE BillType = " + rsBillTypes.Get_Fields("BillType") + " AND ID IN " + strSQL, "TWAR0000.vb1");
				}
				if (lngJournal != 0)
				{
					// do nothing
				}
				else
				{
					// get journal number
					if (modBudgetaryAccounting.LockJournal() == false)
					{
						// add entries to incomplete journals table
					}
					else
					{
						Master.OpenRecordset(("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC"), "TWBD0000.vb1");
						if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
						{
							Master.MoveLast();
							Master.MoveFirst();
							// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
							lngJournal = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
						}
						else
						{
							lngJournal = 1;
						}
						Master.AddNew();
						Master.Set_Fields("JournalNumber", lngJournal);
						Master.Set_Fields("Status", "E");
						Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
						Master.Set_Fields("StatusChangeDate", DateTime.Today);
						if (FCConvert.ToString(rsBillInfo.Get_Fields_String("BillStatus")) == "V")
						{
							Master.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " A/R - Void");
						}
						else
						{
							Master.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " A/R - Bill Run");
						}
						Master.Set_Fields("Type", "GJ");
						Master.Set_Fields("Period", DateTime.Today.Month);
						Master.Update();
						Master.Reset();
						modBudgetaryAccounting.UnlockJournal();
					}
				}
				if (rsBillTypes.EndOfFile() != true && rsBillTypes.BeginningOfFile() != true)
				{
					curSalesTax = 0;
					do
					{
						curTotal = 0;
						curPrePayment = 0;
						if (lngBillKey != -1)
						{
							// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
							rsBillInfo.OpenRecordset("SELECT * FROM Bill WHERE BillType = " + rsBillTypes.Get_Fields("BillType") + " AND ID = " + FCConvert.ToString(lngBillKey), "TWAR0000.vb1");
						}
						else
						{
							// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
							rsBillInfo.OpenRecordset("SELECT * FROM Bill WHERE BillType = " + rsBillTypes.Get_Fields("BillType") + " AND ID IN " + strSQL, "TWAR0000.vb1");
						}
						for (counter = 1; counter <= 6; counter++)
						{
							curTotals[counter - 1] = 0;
						}
						if (rsBillInfo.EndOfFile() != true && rsBillInfo.BeginningOfFile() != true)
						{
							do
							{
								// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
								rsDefaultInfo.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + rsBillInfo.Get_Fields("BillType"), "TWAR0000.vb1");
								for (counter = 1; counter <= 6; counter++)
								{
									if (FCConvert.ToString(rsBillInfo.Get_Fields_String("BillStatus")) == "V")
									{
										// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
										// TODO: Check the table for the column [Quantity] and replace with corresponding Get_Field method
										curTotals[counter - 1] += FCConvert.ToDecimal(modGlobal.Round(FCConvert.ToDouble(rsBillInfo.Get_Fields("Amount" + FCConvert.ToString(counter))) * rsBillInfo.Get_Fields("Quantity" + FCConvert.ToString(counter)), 2));
									}
									else
									{
										// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
										// TODO: Check the table for the column [Quantity] and replace with corresponding Get_Field method
										curTotals[counter - 1] -= FCConvert.ToDecimal(modGlobal.Round(FCConvert.ToDouble(rsBillInfo.Get_Fields("Amount" + FCConvert.ToString(counter))) * rsBillInfo.Get_Fields("Quantity" + FCConvert.ToString(counter)), 2));
									}
								}
								if (FCConvert.ToString(rsBillInfo.Get_Fields_String("BillStatus")) != "V")
								{
									curSalesTax -= rsBillInfo.Get_Fields_Decimal("TaxOwed");
								}
								curPrePayment += FCConvert.ToDecimal(Conversion.Val(rsBillInfo.Get_Fields_Decimal("PrePaymentAmount") + ""));
								rsBillInfo.Edit();
								rsBillInfo.Set_Fields("JournalNumber", lngJournal);
								rsBillInfo.Update();
								rsBillInfo.MoveNext();
							}
							while (rsBillInfo.EndOfFile() != true);
							rsBillInfo.MoveFirst();
						}
						vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorTrap;
						/* On Error GoTo ErrorTrap */
						if (lngJournal != 0)
						{
							rsEntries.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0", "TWBD0000.vb1");
							for (counter = 1; counter <= 6; counter++)
							{
								if (curTotals[counter - 1] != 0)
								{
									rsEntries.AddNew();
									rsEntries.Set_Fields("Type", "G");
									rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
									if (FCConvert.ToString(rsDefaultInfo.Get_Fields_String("TypeTitle")).Length <= 19)
									{
										rsEntries.Set_Fields("Description", "A/R - " + rsDefaultInfo.Get_Fields_String("TypeTitle"));
									}
									else
									{
										rsEntries.Set_Fields("Description", "A/R - " + Strings.Left(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("TypeTitle")), 19));
									}
									rsEntries.Set_Fields("JournalNumber", lngJournal);
									// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
									rsEntries.Set_Fields("Account", rsDefaultInfo.Get_Fields("Account" + FCConvert.ToString(counter)));
									rsEntries.Set_Fields("Amount", curTotals[counter - 1]);
									curTotal += curTotals[counter - 1];
									rsEntries.Set_Fields("WarrantNumber", 0);
									rsEntries.Set_Fields("Period", DateTime.Today.Month);
									rsEntries.Set_Fields("RCB", "R");
									rsEntries.Set_Fields("Status", "E");
									rsEntries.Update();
								}
							}
						}
						else
						{
							rsEntries.OpenRecordset("SELECT * FROM TempJournalEntries WHERE ID = 0", "TWBD0000.vb1");
							for (counter = 1; counter <= 6; counter++)
							{
								if (curTotals[counter - 1] != 0)
								{
									rsEntries.AddNew();
									rsEntries.Set_Fields("Type", "G");
									//FC:Bug:BBE:#504 - the column JournalEntriesDate do not exist in TempJournalEntries. There only exists the column TempJournalEntriesDate.
									rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
									if (FCConvert.ToString(rsDefaultInfo.Get_Fields_String("TypeTitle")).Length <= 19)
									{
										rsEntries.Set_Fields("Description", "A/R - " + rsDefaultInfo.Get_Fields_String("TypeTitle"));
									}
									else
									{
										rsEntries.Set_Fields("Description", "A/R - " + Strings.Left(FCConvert.ToString(rsDefaultInfo.Get_Fields_String("TypeTitle")), 19));
									}
									rsEntries.Set_Fields("JournalNumber", lngJournal);
									rsEntries.Set_Fields("Account", rsDefaultInfo.Get_Fields_String("DefaultAccount" + FCConvert.ToString(counter)));
									rsEntries.Set_Fields("Amount", curTotals[counter - 1]);
									curTotal += curTotals[counter - 1];
									rsEntries.Set_Fields("WarrantNumber", 0);
									rsEntries.Set_Fields("Period", DateTime.Today.Month);
									rsEntries.Set_Fields("RCB", "R");
									rsEntries.Set_Fields("Status", "E");
									rsEntries.Update();
								}
							}
						}
						rsEntries.AddNew();
						rsEntries.Set_Fields("Type", "G");
						rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
						if (FCConvert.ToString(rsBillInfo.Get_Fields_String("BillStatus")) == "V")
						{
							rsEntries.Set_Fields("Description", "A/R - Void");
						}
						else
						{
							rsEntries.Set_Fields("Description", "A/R - Receivable");
						}
						rsEntries.Set_Fields("JournalNumber", lngJournal);
						rsEntries.Set_Fields("Account", rsDefaultInfo.Get_Fields_String("ReceivableAccount"));
						rsEntries.Set_Fields("Amount", curTotal * -1);
						rsEntries.Set_Fields("WarrantNumber", 0);
						rsEntries.Set_Fields("Period", DateTime.Today.Month);
						rsEntries.Set_Fields("RCB", "R");
						rsEntries.Set_Fields("Status", "E");
						rsEntries.Update();
						// If pre payment amounts have been applied to bills in this journal then remove the money
						// from the pre payment receivable acocunt and add it to the bill type receivable account
						if (curPrePayment != 0)
						{
							// PrePay Receivable Account
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", "G");
							rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
							rsEntries.Set_Fields("Description", "A/R - PrePay Receivable");
							rsEntries.Set_Fields("JournalNumber", lngJournal);
							rsEntries.Set_Fields("Account", GetARVariable("PrePayReceivableAccount"));
							rsEntries.Set_Fields("Amount", curPrePayment);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", DateTime.Today.Month);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
							// Bill Type Receivable Account
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", "G");
							rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
							rsEntries.Set_Fields("Description", "A/R - Receivable");
							rsEntries.Set_Fields("JournalNumber", lngJournal);
							rsEntries.Set_Fields("Account", rsDefaultInfo.Get_Fields_String("ReceivableAccount"));
							rsEntries.Set_Fields("Amount", curPrePayment * -1);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", DateTime.Today.Month);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
						}
						rsBillTypes.MoveNext();
					}
					while (rsBillTypes.EndOfFile() != true);
					// If sales tax has been charged on this bill and the option to record sales tax is set to record when bills are created then make entries for the sales tax
					if (curSalesTax != 0 && FCConvert.ToString(GetARVariable("RecordSalesTax")) == "B")
					{
						// Sales Tax Receivable Account
						rsEntries.AddNew();
						rsEntries.Set_Fields("Type", "G");
						rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
						rsEntries.Set_Fields("Description", "A/R - Sales Tax");
						rsEntries.Set_Fields("JournalNumber", lngJournal);
						rsEntries.Set_Fields("Account", GetARVariable("SalesTaxReceivableAccount"));
						rsEntries.Set_Fields("Amount", curSalesTax * -1);
						rsEntries.Set_Fields("WarrantNumber", 0);
						rsEntries.Set_Fields("Period", DateTime.Today.Month);
						rsEntries.Set_Fields("RCB", "R");
						rsEntries.Set_Fields("Status", "E");
						rsEntries.Update();
						// Sales Tax Payable Account
						rsEntries.AddNew();
						rsEntries.Set_Fields("Type", "G");
						rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
						rsEntries.Set_Fields("Description", "A/R - Sales Tax");
						rsEntries.Set_Fields("JournalNumber", lngJournal);
						rsEntries.Set_Fields("Account", GetARVariable("SalesTaxPayableAccount"));
						rsEntries.Set_Fields("Amount", curSalesTax);
						rsEntries.Set_Fields("WarrantNumber", 0);
						rsEntries.Set_Fields("Period", DateTime.Today.Month);
						rsEntries.Set_Fields("RCB", "R");
						rsEntries.Set_Fields("Status", "E");
						rsEntries.Update();
					}
				}
				return;
				ErrorTrap:
				;
				if (lngJournal != 0)
				{
					rsBillInfo.Execute("UPDATE Bill SET JournalNumber = 0 WHERE JournalNumber = " + FCConvert.ToString(lngJournal), "TWAR0000.vb1");
				}
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err().Number) + "  " + Information.Err().Description);
			}
			catch (Exception ex)
			{
				switch (vOnErrorGoToLabel)
				{
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_ErrorTrap:
						//? goto ErrorTrap;
						break;
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(string, object)
		public static object GetARVariable(string strName)
		{
			object GetARVariable = null;
			// this function will get any variable from the budgetary table in the GN database
			clsDRWrapper rsVariables = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				rsVariables.Reset();
				rsVariables.OpenRecordset("SELECT * FROM Customize", "TWAR0000.VB1");
				if (rsVariables.EndOfFile())
				{
					GetARVariable = string.Empty;
				}
				else
				{
					GetARVariable = rsVariables.Get_Fields(strName);
				}
				if (fecherFoundation.FCUtils.IsNull(GetARVariable))
					GetARVariable = "";
				return GetARVariable;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Get BD Variables Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetARVariable;
		}
		// vbPorter upgrade warning: curAmount As Decimal	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: intBasis As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: UsingStartDate As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As Decimal	OnWrite(short, double)
		public static Decimal CalculateARInterest(Double curAmount, ref DateTime dtInterestStartDate, ref DateTime dtDateNow, ref double dblRateAsDecimal, ref int intBasis, int UsingStartDate/* = 0*/, double dblOverPaymentIntRate/*= 0*/, ref double dblPerDiem)
		{
			Decimal CalculateARInterest = 0;
			// vbPorter upgrade warning: dblPrincipalDue As double	OnWriteFCConvert.ToDecimal(
			double dblPrincipalDue;
			int intDays;
			int lngPerDays;
			dblPrincipalDue = FCConvert.ToDouble(curAmount * -1);
			intDays = intBasis;
			if (intDays == 0)
				intDays = 360;
			lngPerDays = DateAndTime.DateDiff("d", dtInterestStartDate, dtDateNow) + UsingStartDate + 1;
			if (lngPerDays < 1)
			{
				CalculateARInterest = 0;
				dblPerDiem = Financial.IPmt((dblRateAsDecimal / intDays), 1, 1, dblPrincipalDue, 0);
			}
			else if (curAmount == 0)
			{
				CalculateARInterest = 0;
				dblPerDiem = 0;
			}
			else
			{
				if (intDays == 0)
					intDays = 365;
				if (curAmount > 0)
				{
					dblPerDiem = Financial.IPmt((dblRateAsDecimal / intDays), 1, lngPerDays, dblPrincipalDue, 0);
				}
				else
				{
					dblPerDiem = Financial.IPmt((dblOverPaymentIntRate / intDays), 1, lngPerDays, dblPrincipalDue, 0);
				}
				CalculateARInterest = FCConvert.ToDecimal(modCLCalculations.Rounding(FCConvert.ToSingle(dblPerDiem * lngPerDays), 2));
			}
			return CalculateARInterest;
		}
	}
}
