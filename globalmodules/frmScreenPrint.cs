﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmScreenPrint.
	/// </summary>
	public partial class frmScreenPrint : BaseForm
	{
		public frmScreenPrint()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmScreenPrint InstancePtr
		{
			get
			{
				return (frmScreenPrint)Sys.GetInstance(typeof(frmScreenPrint));
			}
		}

		protected frmScreenPrint _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :                                       *
		// Date           :               01/04/2002              *
		// *
		// MODIFIED BY    :                                       *
		// Last Updated   :               11/17/2002              *
		// ********************************************************
		private void SetupGrid()
		{
			// two columns.  The caption of the window and a hidden column containing the forms actual name
			// column 0 is the caption, it is visible.  Column 1 is the form name VB uses. It is invisible.
			Grid1.ColHidden(1, true);
			Grid1.ColWidth(0, Grid1.WidthOriginal);
			Grid1.Rows = 1;
			Grid1.TextMatrix(0, 0, "Form Name");
			// Go through the forms collection and list the caption of each form except for this print window and the mdiparent
			foreach (Form frmForm in FCGlobal.Statics.Forms)
			{
				if ((Strings.UCase(frmForm.Name) != "MDIPARENT") && (Strings.UCase(frmForm.Name) != "FRMSCREENPRINT"))
				{
					Grid1.AddItem(Strings.Trim(frmForm.Text) + "\t" + frmForm.Name);
				}
			}
			// frmForm
		}

		private void FormsPrintScreen()
		{
			try
			{
				// Calls the print dialog box to choose a printer then calls the PrinttheForm routine for
				// each window name that was highlighted in the grid
				int x;
				//CommonDialog1.CancelError = true;
				//// Change error handling so we can detect if the user hit the cancel button
				///*? On Error Resume Next  */
				//CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.PrinterConstants.cdlPDHidePrintToFile;    // - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				////FC:FINAL:DSE:#667 Upon cancelling the print dialog, there should be no errors thrown.
				//try
				//{
				//    CommonDialog1.ShowPrinter();
				//} catch (Exception ex)
				//{
				//}
				if (Information.Err().Number == 0)
				{
					// they chose a printer so prepare to start printing.  We'll change the mouse pointer to
					// show that something is happening
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				}
				else
				{
					MessageBox.Show("Print cancelled by you.");
					Information.Err().Clear();
					return;
				}
				// cycle through the grid and print a window if its gridcell is highlighted
				for (x = 1; x <= (Grid1.Rows - 1); x++)
				{
					if (Grid1.IsSelected(x))
					{
						// send the VB name for the form
						PrinttheForm(Grid1.TextMatrix(x, 1));
					}
				}
				// x
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show(" Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Print Forms");
			}
		}

		private void PrinttheForm(string strFormName)
		{
			double dblRatio = 0;
			int lngOWidth = 0;
			int lngOHeight = 0;
			// cycle through the forms until you find a match between the form name and the name sent to this routine
			foreach (FCForm frmForm in FCGlobal.Statics.Forms)
			{
				if (Strings.UCase(strFormName) == Strings.UCase(frmForm.Name))
				{
					// it matched.  Start the printer, print the form and then close the printer
					dblRatio = FCConvert.ToDouble(frmForm.Height / frmForm.Width);
					lngOWidth = frmForm.Width;
					lngOHeight = frmForm.Height;
					frmForm.Width = FCConvert.ToInt32(7.5 * 1440);
					frmForm.Height = FCConvert.ToInt32(dblRatio * frmForm.Width);
					////////Application.DoEvents();
					//FCGlobal.Printer.Print();
					frmForm.PrintForm();
					//FCGlobal.Printer.EndDoc();
					frmForm.Width = lngOWidth;
					frmForm.Height = lngOHeight;
					break;
				}
			}
			// frmForm
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			FormsPrintScreen();
			// exit after you print
			Close();
		}

		private void frmScreenPrint_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmScreenPrint.ScaleWidth	= 5880;
			//frmScreenPrint.ScaleHeight	= 4395;
			//frmScreenPrint.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
		}

		private void Grid1_ClickEvent(object sender, System.EventArgs e)
		{
			// Selection has been turned off. This routine does it manually
			// Check to make sure a valid grid cell has been clicked on
			if ((Grid1.Row < 0) || (Grid1.Row > (Grid1.Rows - 1)))
				return;
			if (Grid1.IsSelected(Grid1.Row))
			{
				// if it is already selected, deselect it and change the background to white and the
				// foreground (font color) to black
				Grid1.IsSelected(Grid1.Row, false);
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid1.Row, 0, Grid1.BackColor);
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid1.Row, 0, Grid1.ForeColor);
			}
			else
			{
				// it isn't selected, so select it and change the background to the highlight color
				// and the foreground color (font color) to white so it is still readable
				Grid1.IsSelected(Grid1.Row, true);
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid1.Row, 0, Grid1.BackColorSel);
				Grid1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid1.Row, 0, Grid1.BackColor);
			}
			Grid1.Refresh();
		}
	}
}
