﻿using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel;
using System;
using System.Collections.Generic;
using Global;

namespace Global
{
	public interface ICentralPartyCondenseInterface
	{
		void StopProcess();

		bool GetCondensedPartiesHintsAsynchronously(int intMaxNumGroups);

		bool GetCondensedPartiesHintsInRangeAsynchronously(int intMaxNumGroups, string strStart, string strEnd);

		bool GetCondensedPartiesHintsFromListAsynchronously(ref System.Collections.ObjectModel.Collection<int> srcIDs);

		bool GetCondensedPartiesHintsFromListWithLimitAsynchronously(ref System.Collections.ObjectModel.Collection<int> srcIDs, int intMaxGroups);

		bool CombineParties(int KeepPartyID, int MergePartyID);

		event EventHandler UpdateMessage;
		//delegate void UpdateMessageEventHandler(double dblProgress, string strMessage);
		event EventHandler CondensedPartiesHintsCompleted;
		//delegate void CondensedPartiesHintsCompletedEventHandler(System.Collections.ObjectModel.Collection<System.Collections.ObjectModel.Collection<int>> HintsCollection);
		event EventHandler CondensedPartiesHintsCancelled;
		//delegate void CondensedPartiesHintsCancelledEventHandler();
		string ConnectionString
		{
			get;
			set;
		}

		bool IsPartyReferenced(int PartyID);

		List<int> AccountsReferencingParty(int PartyID, string strMod);

		List<int> GetPartyHintsByParty(string strFirstName, string strMiddleName, string strLastName, string strDesig, int intPartyType);
	}

	public interface IPartyGUIInterface
	{
		void ShowCentralPartyCondenseView(ref System.Collections.ObjectModel.Collection<System.Collections.ObjectModel.Collection<int>> tColl, string strConnectionString);
	}

	public interface IBackupAndRestoreService
	{
		event EventHandler<BackupCompleteArgs> BackupCompleted;
		event EventHandler<BackupPercentageArgs> BackupPercentageComplete;
		event EventHandler<RestoreCompleteArgs> RestoreCompleted;
		event EventHandler<RestorePercentageArgs> RestorePercentageComplete;

		void Backup(dbConnInfo tConnectionInfo, ref string strError, bool blnShowError = true, string strBackupPath = "", bool boolCopyOnly = false);

		void Restore(DatabaseInfo temp, ref string strError, int intFileNumber = -1, bool blnShowError = true);

		string GetBackupPath(string strConnectionString);

		string GetDataPath(string strConnectionString);
		//System.Collections.ObjectModel.Collection<RestoreFileDetails> GetBackupHeadersFromFile(string strConnectionString, string strBackupPath);
		List<RestoreFileDetails> GetBackupHeadersFromFile(string strConnectionString, string strBackupPath);
		//System.Collections.ObjectModel.Collection<RestoreFileDetails> GetBackupHeaders(string strConnectionString);
		List<RestoreFileDetails> GetBackupHeaders(string strConnectionString);
	}

	public class BackupCompleteArgs : EventArgs
	{
		public bool Success
		{
			get;
			set;
		}

		public string ErrorMessage
		{
			get;
			set;
		}
	}

	public class BackupPercentageArgs : EventArgs
	{
		public int PercentComplete
		{
			get;
			set;
		}
	}

	public class RestorePercentageArgs : EventArgs
	{
		public int PercentComplete
		{
			get;
			set;
		}
	}

	public class RestoreCompleteArgs : EventArgs
	{
		public bool Success
		{
			get;
			set;
		}

		public string ErrorMessage
		{
			get;
			set;
		}
	}

	public class dbConnInfo
	{
		public string ConnectionString
		{
			get;
			set;
		}

		public string ConnectionName
		{
			get;
			set;
		}

		private System.Collections.Generic.Dictionary<string, string> _Items = new System.Collections.Generic.Dictionary<string, string>();
		//public string Item
		//{
		//    get
		//    {
		//        string strVal = "";
		//        _Items.TryGetValue(strItemName, ref strVal);
		//        return strVal;
		//    }
		//    set
		//    {
		//        _Items[strItemName] = value;
		//    }
		//}
		public System.Collections.Generic.Dictionary<string, string> Items
		{
			get
			{
				return _Items;
			}
		}
	}

	public class DatabaseInfo
	{
		private string _Description = string.Empty;

		public string Description
		{
			get
			{
				return _Description;
			}
			set
			{
				_Description = value;
			}
		}

		private string _DatabaseName = string.Empty;

		public string DatabaseName
		{
			get
			{
				return _DatabaseName;
			}
			set
			{
				_DatabaseName = value;
			}
		}

		private string _DataSource = string.Empty;

		public string DataSource
		{
			get
			{
				return _DataSource;
			}
			set
			{
				_DataSource = value;
			}
		}

		private string _User = string.Empty;

		public string User
		{
			get
			{
				return _User;
			}
			set
			{
				_User = value;
			}
		}

		private string _Password = string.Empty;

		public string Password
		{
			get
			{
				return _Password;
			}
			set
			{
				_Password = value;
			}
		}

		private DateTime _LatestBackup;

		public DateTime LatestBackup
		{
			get
			{
				return _LatestBackup;
			}
			set
			{
				_LatestBackup = value;
			}
		}

		private string _DestDatabase = string.Empty;

		public string DestinationDatabase
		{
			get
			{
				return _DestDatabase;
			}
			set
			{
				_DestDatabase = value;
			}
		}

		public string RestorePoints
		{
			get
			{
				return _RestoreDetails.Count.ToString();
			}
		}

		private string _Type = string.Empty;

		public string Type
		{
			get
			{
				return _Type;
			}
			set
			{
				_Type = value;
			}
		}

		private string _Tag = string.Empty;

		public string Tag
		{
			get
			{
				return _Tag;
			}
			set
			{
				_Tag = value;
			}
		}

		private string _restorePath = string.Empty;

		public string RestorePath
		{
			get
			{
				return _restorePath;
			}
			set
			{
				_restorePath = value;
			}
		}

		private List<RestoreFileDetails> _RestoreDetails = new List<RestoreFileDetails>();

		public List<RestoreFileDetails> RestoreDetails
		{
			get
			{
				return _RestoreDetails;
			}
			set
			{
				_RestoreDetails = value;
			}
		}
	}

	public class InteropServiceLocator
	{
		private AggregateCatalog aggCat = new AggregateCatalog();
		private CompositionContainer cont = null/* TODO Change to default(_) if this is not a reference type */;

		public InteropServiceLocator()
		{
			aggCat.Catalogs.Add(new AssemblyCatalog(typeof(InteropServiceLocator).Assembly));
			cont = new CompositionContainer(aggCat);
			aggCat.Catalogs.Add(new DirectoryCatalog("."));
		}

		public T GetService<T>()
		{
			try
			{
				System.Collections.Generic.IEnumerable<Lazy<T>> FoundServices = null;
				FoundServices = cont.GetExports<T>();
				//if (FoundServices != null)
				if (!FoundServices.Equals(null))
				{
					int count = 0;
					object zeroitem = null;
					foreach (var item in FoundServices)
					{
						count++;
						if (zeroitem == null)
						{
							zeroitem = item;
						}
					}
					if (count > 0)
					{
						object tOb = null;
						//tOb = FoundServices(0).Value;
						tOb = ((Lazy<T>)zeroitem).Value;
						return (T)tOb;
					}
					else
						return default(T);
				}
				else
				{
					return default(T);
				}
			}
			catch (Exception ex)
			{
				return default(T);
			}
			return default(T);
		}

		public System.Collections.Generic.IEnumerable<Lazy<T>> GetInterfaces<T>()
		{
			try
			{
				System.Collections.Generic.IEnumerable<Lazy<T>> FoundServices = null;
				FoundServices = cont.GetExports<T>();
				return FoundServices;
			}
			catch (Exception ex)
			{
				return null;
			}
		}
	}
}
