﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cRESecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cRESecuritySetup() : base()
		{
			strThisModule = "RE";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem(" Entry Real Estate", "", 1, false, "CNSTENTRYINTOREALESTATE"));
			AddItem_2(CreateItem("Accept Correlation", "", 9, false, ""));
			AddItem_2(CreateItem("Account Associations", "", 100, true, ""));
			AddItem_2(CreateItem("Account Associations", "Group Maintenance", 103, false, ""));
			AddItem_2(CreateItem("Account Associations", "Mortgage Maintenance", 102, false, ""));
			AddItem_2(CreateItem("Add Accounts", "", 41, false, ""));
			AddItem_2(CreateItem("Compute Menu", "", 63, true, ""));
			AddItem_2(CreateItem("Compute Menu", "Audit Billing Amounts", 71, false, ""));
			AddItem_2(CreateItem("Compute Menu", "Calculate Accounts", 27, false, ""));
			AddItem_2(CreateItem("Compute Menu", "Calculate Exemption", 21, false, ""));
			AddItem_2(CreateItem("Compute Menu", "Commit to Billing", 13, false, ""));
			AddItem_2(CreateItem("Compute Menu", "Summary", 8, false, ""));
			AddItem_2(CreateItem("Compute Menu", "Tax Rate Calculator", 113, false, ""));
			AddItem_2(CreateItem("Cost Update Menu", "", 24, true, ""));
			AddItem_2(CreateItem("Cost Update Menu", "Income Approach Text/Rates", 68, false, ""));
			AddItem_2(CreateItem("Cost Update Menu", "Update Menu", 30, false, ""));
			AddItem_2(CreateItem("Customize", "", 7, false, ""));
			AddItem_2(CreateItem("File Maintenance", "", 69, true, ""));
			AddItem_2(CreateItem("File Maintenance", "Account Associations", 100, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Assessing Reports", 83, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Backup", 6, false, ""));
			// call AddItem(CreateItem("File Maintenance","City/State/Zip Defaults",81,False,""))
			AddItem_2(CreateItem("File Maintenance", "Create Archives", 112, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Database Extract", 84, false, ""));
			AddItem_2(CreateItem("File Maintenance", "DB Extract to Disk", 85, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Purge Sale File", 70, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Real Estate Settings", 82, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Restore", 5, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Run Archive Data", 111, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Undelete Accounts", 67, false, ""));
			AddItem_2(CreateItem("File Maintenance", "User Extract", 86, false, ""));
			AddItem_2(CreateItem("Group Maintenance", "", 103, true, ""));
			AddItem_2(CreateItem("Group Maintenance", "Edit / Add Groups", 107, false, ""));
			AddItem_2(CreateItem("Import/Export Menu", "", 108, true, ""));
			AddItem_2(CreateItem("Import/Export Menu", "Import/Export Name Etc.", 109, false, ""));
			AddItem_2(CreateItem("Import/Export Menu", "Import/Export Pocket PC", 110, false, ""));
			AddItem_2(CreateItem("Long Screen", "", 16, true, ""));
			AddItem_2(CreateItem("Long Screen", "Add Accounts", 41, false, ""));
			AddItem_2(CreateItem("Long Screen", "Add Cards", 42, false, ""));
			AddItem_2(CreateItem("Long Screen", "Calculate Accounts", 27, false, ""));
			AddItem_2(CreateItem("Long Screen", "Comments", 26, false, ""));
			AddItem_2(CreateItem("Long Screen", "Delete Accounts", 35, false, ""));
			AddItem_2(CreateItem("Long Screen", "Delete Cards", 36, false, ""));
			AddItem_2(CreateItem("Long Screen", "Delete Dwell/Comm Data", 50, false, ""));
			AddItem_2(CreateItem("Long Screen", "Edit Addresses", 62, false, ""));
			AddItem_2(CreateItem("Long Screen", "Edit Commercial", 76, false, ""));
			AddItem_2(CreateItem("Long Screen", "Edit Dwelling", 75, false, ""));
			AddItem_2(CreateItem("Long Screen", "Edit Outbuilding", 77, false, ""));
			AddItem_2(CreateItem("Long Screen", "Edit Property", 78, false, ""));
			AddItem_2(CreateItem("Long Screen", "Edit Sale Data", 93, false, ""));
			AddItem_2(CreateItem("Long Screen", "Income Approach", 98, false, ""));
			AddItem_2(CreateItem("Long Screen", "Save Accounts", 39, false, ""));
			// Call AddItem(CreateItem("Long Screen", "View Documents", 114, False, ""))
			AddItem_2(CreateItem("Long Screen", "View Pictures", 40, false, ""));
			AddItem_2(CreateItem("Long Screen", "View Sketches", 94, false, ""));
			AddItem_2(CreateItem("Mortgage Maintenance", "", 102, true, ""));
			AddItem_2(CreateItem("Mortgage Maintenance", "Edit / Add Mortgage Holders", 105, false, ""));
			AddItem_2(CreateItem("Print Routines", "", 89, true, ""));
			AddItem_2(CreateItem("Print Routines", "Extract for Listings/Labels", 92, false, ""));
			AddItem_2(CreateItem("Print Routines", "Labels", 91, false, ""));
			AddItem_2(CreateItem("Print Routines", "Listings", 90, false, ""));
			AddItem_2(CreateItem("Sales Analysis", "", 22, false, ""));
			AddItem_2(CreateItem("Sales Records", "", 65, false, ""));
			AddItem_2(CreateItem("Short Screen", "", 15, true, ""));
			AddItem_2(CreateItem("Short Screen", "Add Accounts", 41, false, ""));
			AddItem_2(CreateItem("Short Screen", "Comments", 26, false, ""));
			AddItem_2(CreateItem("Short Screen", "Delete Accounts", 35, false, ""));
			AddItem_2(CreateItem("Short Screen", "Edit Addresses", 62, false, ""));
			AddItem_2(CreateItem("Short Screen", "Edit Land, Bldg, Exempt Values", 61, false, ""));
			AddItem_2(CreateItem("Short Screen", "Save Accounts", 39, false, ""));
			AddItem_2(CreateItem("Short Screen", "View Pictures", 40, false, ""));
			AddItem_2(CreateItem("Undelete Accounts", "", 67, false, ""));
			AddItem_2(CreateItem("Update Menu", "", 30, true, ""));
			AddItem_2(CreateItem("Update Menu", "Cost Files", 20, false, ""));
			AddItem_2(CreateItem("Update Menu", "Exemption Codes", 88, false, ""));
			AddItem_2(CreateItem("Update Menu", "Fractional Acreage Exp.", 52, false, ""));
			AddItem_2(CreateItem("Update Menu", "Land Category Table", 53, false, ""));
			AddItem_2(CreateItem("Update Menu", "Land Schedule Table", 54, false, ""));
			AddItem_2(CreateItem("Update Menu", "Outbuilding SQFT Table", 55, false, ""));
			AddItem_2(CreateItem("Update Menu", "Residential Size Factor", 56, false, ""));
			AddItem_2(CreateItem("Documents", "", 114, true, ""));
			AddItem_2(CreateItem("Documents", "Edit Documents", 115, false, ""));
			// Call AddItem(CreateItem("View Documents", "Edit Documents", 115, False, ""))
			AddItem_2(CreateItem("Pictures", "", 40, true, ""));
			AddItem_2(CreateItem("View Pictures", "Add Pictures", 34, false, ""));
			AddItem_2(CreateItem("View Pictures", "Delete Pictures", 37, false, ""));
			AddItem_2(CreateItem("Sketches", "", 94, true, ""));
			AddItem_2(CreateItem("Sketches", "Add Sketches", 96, false, ""));
			AddItem_2(CreateItem("Sketches", "Delete Sketches", 97, false, ""));
		}
	}
}
