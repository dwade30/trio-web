﻿//Fecher vbPorter - Version 1.0.0.40
using System.Xml;

namespace Global
{
	public class cWebAPI
	{
		//=========================================================
		string strUrl = "";
		string strPath = "";
		string strJSON = "";
		bool blnUseHTTPS;

		public string URL
		{
			get
			{
				string URL = "";
				URL = strUrl;
				return URL;
			}
			set
			{
				strUrl = value;
			}
		}

		public string Path
		{
			get
			{
				string Path = "";
				Path = strPath;
				return Path;
			}
			set
			{
				strPath = value;
			}
		}

		public string JSONData
		{
			get
			{
				string JSONData = "";
				JSONData = strJSON;
				return JSONData;
			}
			set
			{
				strJSON = value;
			}
		}

		public bool UseSecureHTTP
		{
			get
			{
				bool UseSecureHTTP = false;
				UseSecureHTTP = blnUseHTTPS;
				return UseSecureHTTP;
			}
			set
			{
				blnUseHTTPS = value;
			}
		}

		public string GetQuickGetString()
		{
			string GetQuickGetString = "";
			Chilkat.Http http = new Chilkat.Http();
			bool success;
			string strWebAPIPath = "";
			success = http.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");
            if (success != true)
            {
                GetQuickGetString = "";
                return GetQuickGetString;
            }
            if (blnUseHTTPS)
			{
				strWebAPIPath = "https://" + strUrl;
			}
			else
			{
				strWebAPIPath = "http://" + strUrl;
			}
			if (strPath != "")
			{
				strWebAPIPath += "/" + strPath;
			}
			GetQuickGetString = http.QuickGetStr(strWebAPIPath);
			//FC:FINAL:SBE - in VB6 there result of QuickGetStr doesn't contain XML tags for string value
			//XmlDocument document = new XmlDocument();
			//document.LoadXml(GetQuickGetString);
			//GetQuickGetString = document.InnerText;
			return GetQuickGetString;
		}

		public Chilkat.HttpResponse SendGet()
		{
			Chilkat.HttpRequest req = new Chilkat.HttpRequest();
			Chilkat.Http http = new Chilkat.Http();
			bool success;
			string strWebAPIPath = "";
			success = http.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");
            if (success != true)
			{
				return null;
			}
			// First, remove default header fields that would be automatically
			// sent.  (These headers are harmless, and shouldn't need to
			// be suppressed, but just in case...)
			http.AcceptCharset = "";
			http.UserAgent = "";
			http.AcceptLanguage = "";
			// Suppress the Accept-Encoding header by disallowing
			// a gzip response:
			http.AllowGzip = false;
			Chilkat.HttpResponse resp = new Chilkat.HttpResponse();
			string test = "";
			if (blnUseHTTPS)
			{
				strWebAPIPath = "https://" + strUrl;
			}
			else
			{
				strWebAPIPath = "http://" + strUrl;
			}
			if (strPath != "")
			{
				strWebAPIPath += "/" + strPath;
			}
			return http.QuickGetObj(strWebAPIPath);
		}

		public Chilkat.HttpResponse SendPost()
		{
			Chilkat.HttpRequest req = new Chilkat.HttpRequest();
			Chilkat.Http http = new Chilkat.Http();
			bool success;
			string strWebAPIPath = "";
			success = http.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");
   //         if (success != true)
			//{
			//	return null;
			//}
			// First, remove default header fields that would be automatically
			// sent.  (These headers are harmless, and shouldn't need to
			// be suppressed, but just in case...)
			http.AcceptCharset = "";
			http.UserAgent = "";
			http.AcceptLanguage = "";
			// Suppress the Accept-Encoding header by disallowing
			// a gzip response:
			http.AllowGzip = false;
			Chilkat.HttpResponse resp = new Chilkat.HttpResponse();
			string test = "";
			if (blnUseHTTPS)
			{
				strWebAPIPath = "https://" + strUrl;
			}
			else
			{
				strWebAPIPath = "http://" + strUrl;
			}
			if (strPath != "")
			{
				strWebAPIPath += "/" + strPath;
			}
			return http.PostJson2(strWebAPIPath, "application/json", strJSON);
		}
	}
}
