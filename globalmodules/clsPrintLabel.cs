﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;

namespace Global
{
	public class clsPrintLabel
	{
		//=========================================================
		const int CNSTLASERLABEL = 0;
		const int CNSTDYMOTYPELABEL = 2;

		private struct LabelRec
		{
			public int ID;
			public string Caption;
			public string Description;
			public float LabelWidth;
			public float LabelHeight;
			// VBto upgrade warning: PageWidth As int	OnWrite(int, double)
			public float PageWidth;
			public float PageHeight;
			public int LabelsWide;
			public float LabelsHigh;
			public int LabelType;
			public int UserID;
			public float HSpace;
			public float VSpace;
			public bool Visible;
			public float LSpace;
			public float RSpace;
			public float TopMargin;
			public float BottomMargin;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public LabelRec(int unusedParam)
			{
				this.ID = 0;
				this.Caption = String.Empty;
				this.Description = String.Empty;
				this.LabelHeight = 0;
				this.LabelWidth = 0;
				this.PageHeight = 0;
				this.PageWidth = 0;
				this.LabelsHigh = 0;
				this.LabelsWide = 0;
				this.LabelType = 0;
				this.UserID = 0;
				this.HSpace = 0;
				this.VSpace = 0;
				this.Visible = false;
				this.LSpace = 0;
				this.RSpace = 0;
				this.TopMargin = 0;
				this.BottomMargin = 0;
			}
		};

		private LabelRec[] aryLabels = null;
		private int lngCurLabel;

		public clsPrintLabel() : base()
		{
			ResetLabels();
		}

		public void ResetTypes()
		{
			ResetLabels();
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short TypeCount
		{
			get
			{
				short TypeCount = 0;
				TypeCount = FCConvert.ToInt16(Information.UBound(aryLabels, 1) + 1);
				return TypeCount;
			}
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short AddType()
		{
			short AddType = 0;
			int intReturn = 0;
			if (Information.UBound(aryLabels, 1) == 1 && aryLabels[1].Caption == "" && aryLabels[1].ID == 0)
			{
				intReturn = 1;
			}
			else
			{
				Array.Resize(ref aryLabels, Information.UBound(aryLabels, 1) + 1 + 1);
				intReturn = Information.UBound(aryLabels, 1);
			}
			AddType = FCConvert.ToInt16(intReturn);
			return AddType;
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Get_IndexFromID(int lngID)
		{
			short IndexFromID = 0;
			int x;
			int intReturn;
			intReturn = -1;
			for (x = 0; x <= Information.UBound(aryLabels, 1); x++)
			{
				if (aryLabels[x].ID == lngID)
				{
					intReturn = x;
					break;
				}
			}
			// x
			IndexFromID = FCConvert.ToInt16(intReturn);
			return IndexFromID;
		}

		public int Get_ID(int intIndex)
		{
			int ID = 0;
			ID = aryLabels[intIndex].ID;
			return ID;
		}

		public string Get_Caption(int intIndex)
		{
			string Caption = "";
			Caption = aryLabels[intIndex].Caption;
			return Caption;
		}

		public void Set_Caption(int intIndex, ref string strCaption)
		{
			aryLabels[intIndex].Caption = strCaption;
		}

		public string Get_Description(int intIndex)
		{
			string Description = "";
			Description = aryLabels[intIndex].Description;
			return Description;
		}

		public void Set_Description(int intIndex, ref string strDescription)
		{
			aryLabels[intIndex].Description = strDescription;
		}

		public float Get_LabelWidth(int intIndex)
		{
			float LabelWidth = 0;
			LabelWidth = aryLabels[intIndex].LabelWidth;
			return LabelWidth;
		}

		public float Get_LabelHeight(int intIndex)
		{
			float LabelHeight = 0;
			LabelHeight = aryLabels[intIndex].LabelHeight;
			return LabelHeight;
		}

		public float Get_PageHeight(int intIndex)
		{
			float PageHeight = 0;
			PageHeight = aryLabels[intIndex].PageHeight;
			return PageHeight;
		}

		public void Set_PageHeight(int intIndex, ref int lngHeight)
		{
			aryLabels[intIndex].PageHeight = lngHeight;
		}

		public float Get_PageWidth(int intIndex)
		{
			float PageWidth = 0;
			PageWidth = aryLabels[intIndex].PageWidth;
			return PageWidth;
		}

		public int Get_LabelsWide(int intIndex)
		{
			int LabelsWide = 0;
			LabelsWide = aryLabels[intIndex].LabelsWide;
			return LabelsWide;
		}

		public void Set_LabelsWide(int intIndex, ref int lngWide)
		{
			aryLabels[intIndex].LabelsWide = lngWide;
		}

		public float Get_LabelsHigh(int intIndex)
		{
			float LabelsHigh = 0;
			LabelsHigh = aryLabels[intIndex].LabelsHigh;
			return LabelsHigh;
		}

		public void Set_LabelsHigh(int intIndex, ref int lngHigh)
		{
			aryLabels[intIndex].LabelsHigh = lngHigh;
		}

		public int Get_LabelType(int intIndex)
		{
			int LabelType = 0;
			LabelType = aryLabels[intIndex].LabelType;
			return LabelType;
		}

		public void Set_LabelType(int intIndex, ref int lngType)
		{
			aryLabels[intIndex].LabelType = lngType;
		}

		public int Get_UserID(int intIndex)
		{
			int UserID = 0;
			UserID = aryLabels[intIndex].UserID;
			return UserID;
		}

		public void Set_UserID(int intIndex, ref int lngID)
		{
			aryLabels[intIndex].UserID = lngID;
		}

		public float Get_HorizontalSpace(int intIndex)
		{
			float HorizontalSpace = 0;
			HorizontalSpace = aryLabels[intIndex].HSpace;
			return HorizontalSpace;
		}

		public void Set_HorizontalSpace(int intIndex, ref int lngHSpace)
		{
			aryLabels[intIndex].HSpace = lngHSpace;
		}

		public float Get_VerticalSpace(int intIndex)
		{
			float VerticalSpace = 0;
			VerticalSpace = aryLabels[intIndex].VSpace;
			return VerticalSpace;
		}

		public void Set_VerticalSpace(int intIndex, ref int lngVSpace)
		{
			aryLabels[intIndex].VSpace = lngVSpace;
		}

		public float Get_LeftMargin(int intIndex)
		{
			float LeftMargin = 0;
			LeftMargin = aryLabels[intIndex].LSpace;
			return LeftMargin;
		}

		public void Set_LeftMargin(int intIndex, ref int lngLMargin)
		{
			aryLabels[intIndex].LSpace = lngLMargin;
		}

		public float Get_RightMargin(int intIndex)
		{
			float RightMargin = 0;
			RightMargin = aryLabels[intIndex].RSpace;
			return RightMargin;
		}

		public void Set_RightMargin(int intIndex, ref int lngRMargin)
		{
			aryLabels[intIndex].RSpace = lngRMargin;
		}

		public float Get_TopMargin(int intIndex)
		{
			float TopMargin = 0;
			TopMargin = aryLabels[intIndex].TopMargin;
			return TopMargin;
		}

		public void Set_TopMargin(int intIndex, ref int lngTMargin)
		{
			aryLabels[intIndex].TopMargin = lngTMargin;
		}

		public float Get_BottomMargin(int intIndex)
		{
			float BottomMargin = 0;
			BottomMargin = aryLabels[intIndex].BottomMargin;
			return BottomMargin;
		}

		public void Set_BottomMargin(int intIndex, ref int lngBMargin)
		{
			aryLabels[intIndex].BottomMargin = lngBMargin;
		}

		public bool Get_Visible(int intIndex)
		{
			bool Visible = false;
			Visible = aryLabels[intIndex].Visible;
			return Visible;
		}

		public void Set_Visible(int intIndex, bool boolVisible)
		{
			aryLabels[intIndex].Visible = boolVisible;
		}

        public bool Get_IsLaserLabel(int intIndex)
		{
			bool IsLaserLabel = false;
			if (aryLabels[intIndex].LabelType == CNSTLASERLABEL)
			{
				IsLaserLabel = true;
			}
			else
			{
				IsLaserLabel = false;
			}
			return IsLaserLabel;
		}

		public bool Get_IsDymoLabel(int intIndex)
		{
			bool IsDymoLabel = false;
			if (aryLabels[intIndex].LabelType == CNSTDYMOTYPELABEL)
			{
				IsDymoLabel = true;
			}
			else
			{
				IsDymoLabel = false;
			}
			return IsDymoLabel;
		}

		public void SetAsDymo(int intIndex)
		{
			aryLabels[intIndex].LabelType = CNSTDYMOTYPELABEL;
		}

		public void SetAsLaser(int intIndex)
		{
			aryLabels[intIndex].LabelType = CNSTLASERLABEL;
		}

		public bool Get_Defined(int intIndex)
		{
			bool Defined = false;
			if (Information.UBound(aryLabels, 1) < intIndex)
			{
				Defined = false;
			}
			else
			{
				if (aryLabels[intIndex].Caption == "" && aryLabels[intIndex].ID == 0 && aryLabels[intIndex].UserID == 0)
				{
					Defined = false;
				}
				else
				{
					Defined = true;
				}
			}
			// End If
			return Defined;
		}

		private void ResetLabels()
		{
			FCUtils.EraseSafe(aryLabels);
			aryLabels = new LabelRec[10 + 1];
			// MAL@20080812: Changed from 16 ; Tracker Reference: 14365
			int x;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			for (int i = 0; i < 11; i++)
			{
				aryLabels[i] = new LabelRec(0);
			}
			x = 0;
			
			aryLabels[x].ID = modLabels.CNSTLBLTYPEDYMO4150;
			aryLabels[x].LabelType = CNSTDYMOTYPELABEL;
			aryLabels[x].PageHeight = 5040 / 1440f;
			aryLabels[x].PageWidth = 1620 / 1440f;
			aryLabels[x].Caption = "Avery 4150";
			aryLabels[x].Visible = false;
			aryLabels[x].UserID = 0;
			aryLabels[x].LabelWidth = (1620 - 225) / 1440f;
			aryLabels[x].LabelHeight = (5040 - 225) / 1440f;
			aryLabels[x].LabelsHigh = 1;
			aryLabels[x].LabelsWide = 1;
			aryLabels[x].TopMargin = 0;
			aryLabels[x].VSpace = 0;
			aryLabels[x].HSpace = 0;
			aryLabels[x].RSpace = 0;
			aryLabels[x].LSpace = 0;
			aryLabels[x].Description = "Style 4150 for Dymo Label Printer.  1 1/8 in. X 3.5 in.";
			aryLabels[x].BottomMargin = 0;
			x += 1;
			aryLabels[x].ID = modLabels.CNSTLBLTYPE5160;
			aryLabels[x].Caption = "Avery 5160,5260,5970";
			aryLabels[x].LabelHeight = 1;
			aryLabels[x].LabelWidth = 3780 / 1440f;
			aryLabels[x].LabelsHigh = 10;
			aryLabels[x].LabelsWide = 3;
			aryLabels[x].PageHeight = 15840 / 1440f;
			aryLabels[x].PageWidth = 12240 / 1440f;
			aryLabels[x].LabelType = CNSTLASERLABEL;
			aryLabels[x].UserID = 0;
			aryLabels[x].Visible = true;
			aryLabels[x].VSpace = 0;
			aryLabels[x].TopMargin = 720 / 1440f;
			aryLabels[x].HSpace = 180 / 1440f;
			aryLabels[x].LSpace = 270 / 1440f;
			aryLabels[x].Description = "Style 5160, 5260, 5560, 5660, 5960, 5970, 5971, 5972, 5979, 5980, 6241, 6460. Sheet of 3 x 10. 1 in. X 2 5/8 in.";
			aryLabels[x].RSpace = 270 / 1440f;
			x += 1;
			aryLabels[x].ID = modLabels.CNSTLBLTYPE5161;
			aryLabels[x].Caption = "Avery 5161,5261,5661";
			aryLabels[x].LabelHeight = 1;
			aryLabels[x].LabelWidth = 5760 / 1440f;
			aryLabels[x].LabelsHigh = 10;
			aryLabels[x].LabelsWide = 2;
			aryLabels[x].PageHeight = 15840 / 1440f;
			aryLabels[x].PageWidth = 12240 / 1440f;
			aryLabels[x].Visible = true;
			aryLabels[x].UserID = 0;
			aryLabels[x].VSpace = 0;
			aryLabels[x].HSpace = 270 / 1440f;
			aryLabels[x].LSpace = 270 / 1440f;
			aryLabels[x].RSpace = 270 / 1440f;
			aryLabels[x].TopMargin = 720 / 1440f;
			aryLabels[x].Description = "Style 5161, 5261, 5661, 5961. Sheet of 2 X 10. 1 in. X 4 in.";
			aryLabels[x].LabelType = CNSTLASERLABEL;
			x += 1;
			aryLabels[x].ID = modLabels.CNSTLBLTYPE5162;
			aryLabels[x].Caption = "Avery 5162,5262,5662";
			aryLabels[x].LabelHeight = 1920 / 1440f;
			aryLabels[x].LabelWidth = 5760 / 1440f;
			aryLabels[x].LabelsHigh = 7;
			aryLabels[x].LabelsWide = 2;
			aryLabels[x].PageHeight = 15840 / 1440f;
			aryLabels[x].PageWidth = 12240 / 1440f;
			aryLabels[x].Visible = true;
			aryLabels[x].UserID = 0;
			aryLabels[x].VSpace = 0;
			aryLabels[x].TopMargin = 1170 / 1440f;
			aryLabels[x].LSpace = 270 / 1440f;
			aryLabels[x].RSpace = 270 / 1440f;
			aryLabels[x].HSpace = 270 / 1440f;
			aryLabels[x].Description = "Style 5162, 5262, 5662, 5962. Sheet of 2 X 7. 1 1/3 in. X 4 in.";
			aryLabels[x].LabelType = CNSTLASERLABEL;
			x += 1;
			aryLabels[x].ID = modLabels.CNSTLBLTYPE5163;
			aryLabels[x].Caption = "Avery 5163,5263,5663";
			aryLabels[x].LabelHeight = 2880 / 1440f;
			aryLabels[x].LabelWidth = 5760 / 1440f;
			aryLabels[x].LabelsHigh = 5;
			aryLabels[x].LabelsWide = 2;
			aryLabels[x].PageHeight = 15840 / 1440f;
			aryLabels[x].PageWidth = 12240 / 1440f;
			aryLabels[x].Visible = true;
			aryLabels[x].UserID = 0;
			aryLabels[x].VSpace = 0;
			aryLabels[x].TopMargin = 720 / 1440f;
			aryLabels[x].LSpace = 270 / 1440f;
			aryLabels[x].RSpace = 270 / 1440f;
			aryLabels[x].HSpace = 225 / 1440f;
			aryLabels[x].Description = "Style 5163, 5263, 5663, 5963. Sheet of 2 X 5. 2 in. X 4 in.";
			aryLabels[x].LabelType = CNSTLASERLABEL;
			x += 1;
			aryLabels[x].ID = modLabels.CNSTLBLTYPE5026;
			aryLabels[x].Caption = "Avery 5026,5027";
			aryLabels[x].LabelHeight = 1;
			aryLabels[x].LabelWidth = 5040 / 1440f;
			aryLabels[x].LabelsHigh = 9;
			aryLabels[x].LabelsWide = 2;
			aryLabels[x].PageHeight = 15840 / 1440f;
			aryLabels[x].PageWidth = 12240 / 1440f;
			aryLabels[x].Visible = true;
			aryLabels[x].UserID = 0;
			aryLabels[x].VSpace = 180 / 1440f;
			aryLabels[x].TopMargin = 900 / 1440f;
			aryLabels[x].LSpace = 720 / 1440f;
			aryLabels[x].RSpace = 720 / 1440f;
			aryLabels[x].BottomMargin = 360 / 1440f;
			aryLabels[x].HSpace = 720 / 1440f;
			aryLabels[x].Description = "Style 5026, 5027. Sheet of 2 X 9.  1 in. X 3.5 in.";
			aryLabels[x].LabelType = CNSTLASERLABEL;
			x += 1;
			aryLabels[x].ID = modLabels.CNSTLBLTYPE5066;
			aryLabels[x].Caption = "Avery 5066,5266,8066";
			aryLabels[x].LabelHeight = 960 / 1440f;
			aryLabels[x].LabelWidth = 5040 / 1440f;
			aryLabels[x].LabelsHigh = 15;
			aryLabels[x].LabelsWide = 2;
			aryLabels[x].PageHeight = 15840 / 1440f;
			aryLabels[x].PageWidth = 12240 / 1440f;
			aryLabels[x].Visible = true;
			aryLabels[x].UserID = 0;
			aryLabels[x].VSpace = 0;
			aryLabels[x].TopMargin = 720 / 1440f;
			aryLabels[x].LSpace = 1170 / 1440f;
			aryLabels[x].RSpace = 720 / 1440f;
			aryLabels[x].HSpace = 720 / 1440f;
			aryLabels[x].BottomMargin = 720 / 1440f;
			aryLabels[x].Description = "Style 5066, 5266, 5366, 5666, 5766, 5866, 5966, 8066, 8366. Sheet of 2 X 15.  .66 in. X 3.5 in.";
			aryLabels[x].LabelType = CNSTLASERLABEL;
			x += 1;
			aryLabels[x].ID = modLabels.CNSTLBLTYPECERTMAILLASER;
			aryLabels[x].Visible = false;
			aryLabels[x].LabelType = CNSTLASERLABEL;
			aryLabels[x].PageHeight = 15840 / 1440f;
			aryLabels[x].PageWidth = 12240 / 1440f;
			aryLabels[x].Description = "Certified mail form from Hygrade for laser printers";
			aryLabels[x].UserID = 0;
			x += 1;
			aryLabels[x].ID = modLabels.CNSTLBLTYPEDYMO30256;
			aryLabels[x].LabelType = CNSTDYMOTYPELABEL;
			aryLabels[x].Caption = "Dymo 30256";
			aryLabels[x].Visible = false;
			aryLabels[x].UserID = 0;
			aryLabels[x].LabelWidth = 5760 / 1440f;
			aryLabels[x].LabelHeight = 3330 / 1440f;
			aryLabels[x].LabelsHigh = 1;
			aryLabels[x].LabelsWide = 1;
			aryLabels[x].TopMargin = 184 / 1440f;
			aryLabels[x].RSpace = 0;
			aryLabels[x].LSpace = 184 / 1440f;
			aryLabels[x].Description = "Dymo Label Printer Style 30256.  2 5/16 in. X 4 in.";
			aryLabels[x].BottomMargin = 0;
			x += 1;
			aryLabels[x].ID = modLabels.CNSTLBLTYPEDYMO30320;
			aryLabels[x].LabelType = CNSTDYMOTYPELABEL;
			aryLabels[x].Caption = "Dymo 30320";
			aryLabels[x].Visible = false;
			aryLabels[x].UserID = 0;
			aryLabels[x].LabelWidth = 5040 / 1440f;
			aryLabels[x].LabelHeight = 1620 / 1440f;
			aryLabels[x].LabelsHigh = 1;
			aryLabels[x].LabelsWide = 1;
			aryLabels[x].TopMargin = 184 / 1440f;
			aryLabels[x].RSpace = 0;
			aryLabels[x].LSpace = 184 / 1440f;
			aryLabels[x].Description = "Dymo Label Printer Style 30320.  1 1/8 in. X 3 1/2 in.";
			aryLabels[x].BottomMargin = 0;
			x += 1;
			aryLabels[x].ID = modLabels.CNSTLBLTYPEDYMO30252;
			aryLabels[x].LabelType = CNSTDYMOTYPELABEL;
			aryLabels[x].Caption = "Dymo 30252";
			aryLabels[x].Visible = false;
			aryLabels[x].UserID = 0;
			aryLabels[x].LabelWidth = 5040 / 1440f;
			aryLabels[x].LabelHeight = 1584 / 1440f;
			aryLabels[x].LabelsHigh = 1;
			aryLabels[x].LabelsWide = 1;
			aryLabels[x].TopMargin = 184 / 1440f;
			aryLabels[x].LSpace = 184 / 1440f;
			aryLabels[x].RSpace = 0;
			aryLabels[x].Description = "Dymo Label Printer Style 30252.  1.1 in. X 3.5 in.";
			aryLabels[x].BottomMargin = 0;
		}
	}
}
