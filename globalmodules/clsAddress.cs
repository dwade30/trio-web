﻿namespace Global
{
	public class clsAddress
	{
		//=========================================================
		private string strName = string.Empty;
		private string[] strAddress = new string[5];
		private string strCity = string.Empty;
		private string strState = string.Empty;
		private string strZip = string.Empty;
		private string strZipExt = string.Empty;
		private bool boolEditable;

		public string Addressee
		{
			set
			{
				strName = value;
			}
			get
			{
				string Addressee = "";
				Addressee = strName;
				return Addressee;
			}
		}

		public void Set_Address(short intAddress, string straddr)
		{
			strAddress[intAddress] = straddr;
		}

		public string Get_Address(short intAddress)
		{
			string Address = "";
			Address = strAddress[intAddress];
			return Address;
		}

		public string City
		{
			set
			{
				strCity = value;
			}
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
		}

		public bool Editable
		{
			set
			{
				boolEditable = value;
			}
			get
			{
				bool Editable = false;
				Editable = boolEditable;
				return Editable;
			}
		}

		public string State
		{
			set
			{
				strState = value;
			}
			get
			{
				string State = "";
				State = strState;
				return State;
			}
		}

		public string Zip
		{
			set
			{
				strZip = value;
			}
			get
			{
				string Zip = "";
				Zip = strZip;
				return Zip;
			}
		}

		public string ZipExtension
		{
			set
			{
				strZipExt = value;
			}
			get
			{
				string ZipExtension = "";
				ZipExtension = strZipExt;
				return ZipExtension;
			}
		}

		public bool GetAddress()
		{
			bool GetAddress = false;
			GetAddress = false;
			if (frmGetAddress.InstancePtr.Init(this, boolEditable))
			{
				GetAddress = true;
			}
			return GetAddress;
		}

		public clsAddress() : base()
		{
			strAddress[0] = "";
			strAddress[1] = "";
			strAddress[2] = "";
			strAddress[3] = "";
			strAddress[4] = "";
			strCity = "";
			strState = "";
			strZip = "";
			strZipExt = "";
			strName = "";
		}        
	}
}
