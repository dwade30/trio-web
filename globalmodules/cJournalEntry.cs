﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace Global
{
	public class cJournalEntry
	{
		//=========================================================
		private int lngRecordID;
		private string strJournalType = string.Empty;
		private string strJournalEntriesDate = "";
		private string strEntryDescription = string.Empty;
		private int lngVendorNumber;
		private int lngJournalNumber;
		private string strCheckNumber = string.Empty;
		private string strAccount = string.Empty;
		private string strProject = string.Empty;
		private double dblEntryAmount;
		private int lngWarrantNumber;
		private int intJournalPeriod;
		private string strRCB = string.Empty;
		private string strEntryStatus = string.Empty;
		private string strPostedDate = "";
		private string strPO = string.Empty;
		private string str1099 = string.Empty;
		private bool boolCarryForward;
		private int lngBankNumber;
		private bool boolUpdated;
		private bool boolDeleted;

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string JournalType
		{
			set
			{
				strJournalType = value;
				IsUpdated = true;
			}
			get
			{
				string JournalType = "";
				JournalType = strJournalType;
				return JournalType;
			}
		}

		public string JournalEntriesDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strJournalEntriesDate = value;
				}
				else
				{
					strJournalEntriesDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string JournalEntriesDate = "";
				JournalEntriesDate = strJournalEntriesDate;
				return JournalEntriesDate;
			}
		}

		public string Description
		{
			set
			{
				strEntryDescription = value;
				IsUpdated = true;
			}
			get
			{
				string Description = "";
				Description = strEntryDescription;
				return Description;
			}
		}

		public int VendorNumber
		{
			set
			{
				lngVendorNumber = value;
				IsUpdated = true;
			}
			get
			{
				int VendorNumber = 0;
				VendorNumber = lngVendorNumber;
				return VendorNumber;
			}
		}

		public int JournalNumber
		{
			set
			{
				lngJournalNumber = value;
				IsUpdated = true;
			}
			get
			{
				int JournalNumber = 0;
				JournalNumber = lngJournalNumber;
				return JournalNumber;
			}
		}

		public string CheckNumber
		{
			set
			{
				strCheckNumber = value;
				IsUpdated = true;
			}
			get
			{
				string CheckNumber = "";
				CheckNumber = strCheckNumber;
				return CheckNumber;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
				IsUpdated = true;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public string Project
		{
			set
			{
				strProject = value;
				IsUpdated = true;
			}
			get
			{
				string Project = "";
				Project = strProject;
				return Project;
			}
		}

		public double Amount
		{
			set
			{
				dblEntryAmount = value;
				IsUpdated = true;
			}
			get
			{
				double Amount = 0;
				Amount = dblEntryAmount;
				return Amount;
			}
		}

		public int WarrantNumber
		{
			set
			{
				lngWarrantNumber = value;
				IsUpdated = true;
			}
			get
			{
				int WarrantNumber = 0;
				WarrantNumber = lngWarrantNumber;
				return WarrantNumber;
			}
		}

		public short Period
		{
			set
			{
				intJournalPeriod = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short Period = 0;
				Period = FCConvert.ToInt16(intJournalPeriod);
				return Period;
			}
		}

		public string RCB
		{
			set
			{
				strRCB = value;
				IsUpdated = true;
			}
			get
			{
				string RCB = "";
				RCB = strRCB;
				return RCB;
			}
		}

		public string Status
		{
			set
			{
				strEntryStatus = value;
				IsUpdated = true;
			}
			get
			{
				string Status = "";
				Status = strEntryStatus;
				return Status;
			}
		}

		public string PostedDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strPostedDate = value;
				}
				else
				{
					strPostedDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string PostedDate = "";
				PostedDate = strPostedDate;
				return PostedDate;
			}
		}

		public string PO
		{
			set
			{
				strPO = value;
				IsUpdated = true;
			}
			get
			{
				string PO = "";
				PO = strPO;
				return PO;
			}
		}

		public string Code1099
		{
			set
			{
				str1099 = value;
				IsUpdated = true;
			}
			get
			{
				string Code1099 = "";
				Code1099 = str1099;
				return Code1099;
			}
		}

		public bool CarryForward
		{
			set
			{
				boolCarryForward = value;
				IsUpdated = true;
			}
			get
			{
				bool CarryForward = false;
				CarryForward = boolCarryForward;
				return CarryForward;
			}
		}

		public int BankNumber
		{
			set
			{
				lngBankNumber = value;
				IsUpdated = true;
			}
			get
			{
				int BankNumber = 0;
				BankNumber = lngBankNumber;
				return BankNumber;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public bool IsCredit()
		{
			bool IsCredit = false;
			bool boolCredit;
			boolCredit = false;
			if (Strings.LCase(RCB) != "c")
			{
				if (Amount < 0 && !(RCB == "E" && JournalType == "P"))
				{
					boolCredit = true;
				}
			}
			else
			{
				if (Amount > 0)
				{
					boolCredit = true;
				}
			}
			IsCredit = boolCredit;
			return IsCredit;
		}

		public bool IsCorrection()
		{
			bool IsCorrection = false;
			IsCorrection = Strings.LCase(RCB) == "c";
			return IsCorrection;
		}

		public bool IsBudgetAdjustment()
		{
			bool IsBudgetAdjustment = false;
			IsBudgetAdjustment = Strings.LCase(RCB) == "b";
			return IsBudgetAdjustment;
		}

		public bool IsPayrollContract()
		{
			bool IsPayrollContract = false;
			bool boolReturn = false;
			if (strRCB == "E")
			{
				if (strJournalType == "P" && dblEntryAmount < 0)
				{
					boolReturn = true;
				}
			}
			IsPayrollContract = boolReturn;
			return IsPayrollContract;
		}
	}
}
