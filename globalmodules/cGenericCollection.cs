﻿using fecherFoundation;

namespace Global
{
	public class cGenericCollection
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private FCCollection lstDetails = new FCCollection();
		private FCCollection lstDetails_AutoInitialized;

		private FCCollection lstDetails
		{
			get
			{
				if (lstDetails_AutoInitialized == null)
				{
					lstDetails_AutoInitialized = new FCCollection();
				}
				return lstDetails_AutoInitialized;
			}
			set
			{
				lstDetails_AutoInitialized = value;
			}
		}

		private int intCurrentIndex;

		public cGenericCollection() : base()
		{
			intCurrentIndex = -1;
		}

		public void ClearList()
		{
			lstDetails.Clear();
		}

		public void AddItem(dynamic tItem)
		{
			lstDetails.Add(tItem);
		}

		public void InsertItemBefore(object tItem)
		{
			if (tItem == null)
			{
				return;
			}

			if (intCurrentIndex > 0)
			{
				lstDetails.Add(tItem, null, intCurrentIndex);
			}
			else
			{
				lstDetails.Add(tItem);
			}
		}

		public void InsertItemAfter(ref object tItem)
		{
			if (intCurrentIndex > 0)
			{
				lstDetails.Add(tItem, null, null, intCurrentIndex);
			}
			else
			{
				lstDetails.Add(tItem);
			}
		}

		public void MoveFirst()
		{
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			if (!(lstDetails_AutoInitialized == null))
			{
				if (!FCUtils.IsEmpty(lstDetails))
				{
					if (lstDetails.Count >= 0)
					{
						intCurrentIndex = -1;
						MoveNext();
					}
					else
					{
						intCurrentIndex = -1;
					}
				}
				else
				{
					intCurrentIndex = -1;
				}
			}
			else
			{
				intCurrentIndex = -1;
			}
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short MoveNext()
		{
			short MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (intCurrentIndex == -1)
				intCurrentIndex = 0;
			if (!FCUtils.IsEmpty(lstDetails))
			{
				if (intCurrentIndex > lstDetails.Count)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex <= lstDetails.Count)
					{
						intCurrentIndex += 1;
						if (intCurrentIndex > lstDetails.Count)
						{
							intReturn = -1;
							break;
						}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MoveNext = FCConvert.ToInt16(intReturn);
			}
			else
			{
				intCurrentIndex = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short MovePrevious()
		{
			short MovePrevious = 0;
			int intReturn;
			intReturn = -1;
			MovePrevious = -1;
			if (intCurrentIndex == -1)
			{
				MovePrevious = MoveLast();
				return MovePrevious;
			}
			if (!FCUtils.IsEmpty(lstDetails))
			{
				if (intCurrentIndex == 1)
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentIndex >= 1)
					{
						intCurrentIndex -= 1;
						if (intCurrentIndex < 1)
						{
							intReturn = -1;
							break;
						}
						//else if (Information.IsReference(lstDetails[intCurrentIndex]))
						//{
						//    if (lstDetails[intCurrentIndex] == null)
						//    {
						//    }
						//    else
						//    {
						//        intReturn = intCurrentIndex;
						//        break;
						//    }
						//}
						else
						{
							intReturn = intCurrentIndex;
							break;
						}
					}
				}
				if (intReturn == 0)
					intReturn = -1;
				intCurrentIndex = intReturn;
				MovePrevious = FCConvert.ToInt16(intReturn);
			}
			else
			{
				intCurrentIndex = -1;
				MovePrevious = -1;
			}
			return MovePrevious;
		}

		public bool IsCurrent()
		{
			bool IsCurrent = false;
			if (intCurrentIndex > 0)
			{
				IsCurrent = true;
			}
			else
			{
				IsCurrent = false;
			}
			return IsCurrent;
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short ItemCount()
		{
			short ItemCount = 0;
			if (!FCUtils.IsEmpty(lstDetails))
			{
				ItemCount = FCConvert.ToInt16(lstDetails.Count);
			}
			else
			{
				ItemCount = 0;
			}
			return ItemCount;
		}

		public object GetCurrentItem()
		{
			object GetCurrentItem = null;
			object TRec;
			TRec = null;
			if (!FCUtils.IsEmpty(lstDetails))
			{
				if (intCurrentIndex > 0)
				{
					{
						TRec = lstDetails[intCurrentIndex];
					}
				}
			}
			
			return TRec;
		}

		public object GetItemByIndex(int intIndex)
		{
			object GetItemByIndex = null;
			object TRec;
			TRec = null;
			if (!FCUtils.IsEmpty(lstDetails) && intIndex > 0)
			{
				intCurrentIndex = intIndex;
				{
					TRec = lstDetails[intIndex];
				}
			}
			{
				GetItemByIndex = TRec;
			}
			return GetItemByIndex;
		}
        public object GetItemAtIndex(int intIndex)
        {
            object TRec = null;
            if (!FCUtils.IsEmpty(lstDetails) && intIndex > 0)
            {
                TRec = lstDetails[intIndex];
            }

            return TRec;
        }

        public void RemoveCurrent()
		{
			if (!FCUtils.IsEmpty(lstDetails) && intCurrentIndex > 0)
			{
				int intNewIndex = 0;
				intNewIndex = intCurrentIndex - 1;
				if (intNewIndex < 1)
					intNewIndex = -1;
				lstDetails.Remove(intCurrentIndex);
				intCurrentIndex = intNewIndex;
			}
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short MoveLast()
		{
			short MoveLast = 0;
			int intReturn;
			intReturn = -1;
			MoveLast = -1;
			if (!FCUtils.IsEmpty(lstDetails))
			{
				intCurrentIndex = lstDetails.Count;
				if (intCurrentIndex == 0)
				{
					intCurrentIndex = -1;
				}
				intReturn = intCurrentIndex;
				MoveLast = FCConvert.ToInt16(intReturn);
			}
			return MoveLast;
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public int GetCurrentIndex()
		{
			int GetCurrentIndex = 0;
			GetCurrentIndex = intCurrentIndex;
			return GetCurrentIndex;
		}

		public FCCollection Items
		{
			get
			{
				return lstDetails;
			}
			set
			{
				lstDetails = value;
			}
		}
	}
}
