﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using SharedApplication;
using SharedApplication.Budgetary.Enums;
using TWSharedLibrary;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWFA0000
using TWFA0000;


#elif TWBD0000
using TWBD0000;


#elif TWAR0000
using TWAR0000;


#elif TWCR0000
using TWCR0000;


#elif TWBL0000
using TWBL0000;
using modGlobal = TWBL0000.modGlobalRoutines;


#elif TWGNENTY
using TWGNENTY;
using modGlobal = TWGNENTY.modGNBas;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;


#elif TWMV0000
using modGlobal = TWMV0000.modGlobalRoutines;


#elif TWPY0000
using modGlobal = TWPY0000.modGlobalRoutines;
#endif
namespace Global
{
	public class modValidateAccount
	{
		// flag for budgetary account validation
		public static bool AccountValidate(string tEmp, bool blnValidateMulti = false)
		{
			bool AccountValidate = false;
			clsDRWrapper rsRegionInfo = new clsDRWrapper();
			clsDRWrapper rsExpObjInfo = new clsDRWrapper();
			// this determines which type of account is being used
			// E, R, G and M are the only types allowed
			if (Strings.Left(tEmp, 1) == "E")
			{
				// E is for Expense
				if (blnValidateMulti && modRegionalTown.IsRegionalTown() && Conversion.Val(modBudgetaryAccounting.GetDepartment(tEmp)) < 100)
				{
					rsExpObjInfo.DefaultDB = "Twbd0000.vb1";
					if (!modAccountTitle.Statics.ObjFlag)
					{
						rsExpObjInfo.OpenRecordset("SELECT * FROM ExpObjTitles WHERE IsNull(BreakdownCode, 0) <> 0 AND Expense = '" + modBudgetaryAccounting.GetExpense(tEmp) + "' AND Object = '" + Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2))), "0") + "'");
					}
					else
					{
						rsExpObjInfo.OpenRecordset("SELECT * FROM ExpObjTitles WHERE IsNull(BreakdownCode, 0) <> 0 AND Expense = '" + modBudgetaryAccounting.GetExpense(tEmp) + "'");
					}
					if (rsExpObjInfo.EndOfFile() != true && rsExpObjInfo.BeginningOfFile() != true)
					{
						rsRegionInfo.OpenRecordset("SELECT * FROM RegionalTownBreakDown WHERE Code = " + FCConvert.ToString(rsExpObjInfo.Get_Fields_Int32("BreakdownCode")), "CentralData");
						if (rsRegionInfo.EndOfFile() != true && rsRegionInfo.BeginningOfFile() != true)
						{
							do
							{
								// TODO: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
								AccountValidate = ValidExpense(Strings.Left(tEmp, 2) + FCConvert.ToString(rsRegionInfo.Get_Fields("TownNumber")) + Strings.Right(tEmp, tEmp.Length - 3));
								if (AccountValidate == false)
								{
									break;
								}
								rsRegionInfo.MoveNext();
							}
							while (rsRegionInfo.EndOfFile() != true);
						}
						else
						{
							AccountValidate = false;
						}
					}
					else
					{
						// if there is no breakdown code set up for the expense object then try to validate using the breakdown code of the department division
						if (!modAccountTitle.Statics.ExpDivFlag)
						{
							rsExpObjInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE IsNull(BreakdownCode, 0) <> 0 AND Department = '1" + Strings.Right(modBudgetaryAccounting.GetDepartment(tEmp), FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) - 1)) + "' AND Division = '" + Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))), "0") + "'");
						}
						else
						{
							rsExpObjInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE IsNull(BreakdownCode, 0) <> 0 AND Department = '1" + Strings.Right(modBudgetaryAccounting.GetDepartment(tEmp), FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) - 1)) + "'");
						}
						if (rsExpObjInfo.EndOfFile() != true && rsExpObjInfo.BeginningOfFile() != true)
						{
							rsRegionInfo.OpenRecordset("SELECT * FROM RegionalTownBreakDown WHERE Code = " + FCConvert.ToString(rsExpObjInfo.Get_Fields_Int32("BreakdownCode")), "CentralData");
							if (rsRegionInfo.EndOfFile() != true && rsRegionInfo.BeginningOfFile() != true)
							{
								do
								{
									// TODO: Check the table for the column [TownNumber] and replace with corresponding Get_Field method
									AccountValidate = ValidExpense(Strings.Left(tEmp, 2) + FCConvert.ToString(rsRegionInfo.Get_Fields("TownNumber")) + Strings.Right(tEmp, tEmp.Length - 3));
									if (AccountValidate == false)
									{
										break;
									}
									rsRegionInfo.MoveNext();
								}
								while (rsRegionInfo.EndOfFile() != true);
							}
							else
							{
								AccountValidate = false;
							}
						}
						else
						{
							AccountValidate = false;
						}
					}
				}
				else
				{
					AccountValidate = ValidExpense(tEmp);
				}
			}
			else if (Strings.Left(tEmp, 1) == "R")
			{
				// R is for Revenue
				AccountValidate = ValidRevenue(tEmp);
			}
			else if (Strings.Left(tEmp, 1) == "G")
			{
				// G is for General Entry
				AccountValidate = ValidLedger(tEmp);
				// ElseIf Left(tEmp, 1) = "P" Then             'P is for School Expense
				// AccountValidate = ValidSchoolExpense(tEmp)
				// ElseIf Left(tEmp, 1) = "V" Then             'V is for School Revenue
				// AccountValidate = ValidSchoolRevenue(tEmp)
				// ElseIf Left(tEmp, 1) = "L" Then             'L is for School General Entry
				// AccountValidate = ValidSchoolLedger(tEmp)
			}
			else if (Strings.Left(tEmp, 1) == "M")
			{
				// M is for Manual
				if (tEmp.Length > 2)
				{
					AccountValidate = true;
				}
				else
				{
					AccountValidate = false;
				}
			}
			else
			{
				AccountValidate = false;
			}
			return AccountValidate;
		}

		public static bool ValidExpense(string Account)
		{
			bool ValidExpense = false;
			// this function will return true if the expense account number is valid else it will return false
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "ValidExpense"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Function
			// ResumeCode:
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rs = new clsDRWrapper();
				string Dept = "";
				string Div = "";
				string tempDiv = "";
				string tempObj = "";
				// Dim DivFlag     As Boolean
				// Dim ObjFlag     As Boolean
				string Expense = "";
				string Object = "";
				string tempAccount;
				string strDB;
				// VBto upgrade warning: intTotLen As short --> As int	OnWriteFCConvert.ToDouble(
				int intTotLen;
				int intHyphen;
				strDB = "TWBD0000.vb1";
				// If Val(Mid$(Exp, 3, 2)) = 0 Then
				// DivFlag = True
				// End If
				// If Val(Mid$(Exp, 7, 2)) = 0 Then
				// ObjFlag = True                                  'set the objflag to true
				// End If
				if (modAccountTitle.Statics.ExpDivFlag)
				{
					tempDiv = "0";
				}
				else
				{
					tempDiv = modGlobal.PadToString(0, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
					// separates the account number into it's pieces
				}
				if (modAccountTitle.Statics.ObjFlag)
				{
					tempObj = "0";
				}
				else
				{
					tempObj = modGlobal.PadToString(0, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
				}
				tempAccount = Strings.Right(Account, Account.Length - 2);
				// holds the numeric part of the account number...
				intHyphen = 3;
				if (modAccountTitle.Statics.ExpDivFlag)
					intHyphen -= 1;
				if (modAccountTitle.Statics.ObjFlag)
					intHyphen -= 1;
				intTotLen = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Exp, 2)) + Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) + Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) + Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)));
				if (intTotLen == tempAccount.Length - intHyphen)
				{
					// subtract the number of hyphens because of the hyphens
					Dept = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2)));
					tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2)) + 1));
					// this is what is left after being hacked up
					if (!modAccountTitle.Statics.ExpDivFlag)
					{
						// if false
						Div = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)));
						// then cut is up some more
						tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) + 1));
						Expense = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)));
						if (!modAccountTitle.Statics.ObjFlag)
						{
							tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) + 1));
							Object = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)));
						}
					}
					else
					{
						Expense = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)));
						if (!modAccountTitle.Statics.ObjFlag)
						{
							tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) + 1));
							Object = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)));
						}
					}
					if (!modAccountTitle.Statics.ExpDivFlag)
					{
						// check the division in the database to see if it is a valid division
						rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Dept + "' AND Division = '" + Div + "'", strDB);
						// here is dave's favorite control structure...the massively nested if then else statement
						if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
						{
							if (!modAccountTitle.Statics.ObjFlag)
							{
								rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + Expense + "' AND Object = '" + tempObj + "'", strDB);
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + Expense + "' AND Object = '" + Object + "'", strDB);
									if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
									{
										ValidExpense = CheckValidAccount(Account);
										return ValidExpense;
									}
									else
									{
										ValidExpense = false;
										return ValidExpense;
									}
								}
								else
								{
									ValidExpense = false;
									return ValidExpense;
								}
							}
							else
							{
								rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + Expense + "' AND Object = '" + tempObj + "'", strDB);
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									ValidExpense = CheckValidAccount(Account);
									return ValidExpense;
								}
								else
								{
									ValidExpense = false;
									return ValidExpense;
								}
							}
						}
						else
						{
							ValidExpense = false;
							return ValidExpense;
						}
					}
					else
					{
						rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Dept + "' AND Division = '" + tempDiv + "'", strDB);
						if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
						{
							if (!modAccountTitle.Statics.ObjFlag)
							{
								rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + Expense + "' AND Object = '" + tempObj + "'", strDB);
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + Expense + "' AND Object = '" + Object + "'", strDB);
									if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
									{
										ValidExpense = CheckValidAccount(Account);
										return ValidExpense;
									}
									else
									{
										ValidExpense = false;
										return ValidExpense;
									}
								}
								else
								{
									ValidExpense = false;
									return ValidExpense;
								}
							}
							else
							{
								rs.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + Expense + "' AND Object = '" + tempObj + "'", strDB);
								if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
								{
									// this is the last chack so it is possible to have this account
									ValidExpense = CheckValidAccount(Account);
									// this checks for an account that is already created else this function fails anyway
									return ValidExpense;
								}
								else
								{
									ValidExpense = false;
									// account number is bad
									return ValidExpense;
								}
							}
						}
						else
						{
							ValidExpense = false;
							// account number is bad
							return ValidExpense;
						}
					}
				}
				else
				{
					ValidExpense = false;
					// account number is bad
					return ValidExpense;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				ValidExpense = false;
			}
			return ValidExpense;
		}

		public static bool ValidRevenue(string Account)
		{
			bool ValidRevenue = false;
			// this follows the exact format as the validexpense function except it is looking for a valid revenue account number <=== surpised?
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "ValidRevenue"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Function
			// ResumeCode:
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rs = new clsDRWrapper();
				string Dept = "";
				string Div = "";
				string tempRevDiv = "";
				bool DivFlag = false;
				string Revenue = "";
				string tempAccount;
				string strDB;
				// VBto upgrade warning: intTotLen As short --> As int	OnWriteFCConvert.ToDouble(
				int intTotLen;
				int intHyphen = 0;
				strDB = "TWBD0000.vb1";
				if (modAccountTitle.Statics.RevDivFlag)
				{
                    //ZeroDiv could be '0' or '00' when div not used for Revenue accounts, but are used on Expense accounts.
					tempRevDiv = Statics.strZeroDiv;
				}
				else
				{
					tempRevDiv = modGlobal.PadToString(0, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)))));
				}
				tempAccount = Strings.Right(Account, Account.Length - 2);
				if (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)) == 0)
				{
					DivFlag = true;
				}
				if (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)) == 0)
				{
					intHyphen = 1;
				}
				else
				{
					intHyphen = 2;
				}
				intTotLen = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Rev, 2)) + Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)) + Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)));
				if (intTotLen == tempAccount.Length - intHyphen)
				{
					// subtract the number of hyphens because of the hyphens
					Dept = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Rev, 2)));
					tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Rev, 2)) + 1));
					if (!modAccountTitle.Statics.RevDivFlag)
					{
						Div = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)));
						tempAccount = Strings.Right(tempAccount, (tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)) + 1)));
						Revenue = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)));
					}
					else
					{
						Revenue = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)));
					}
					if (!DivFlag)
					{
						rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Dept + "' AND Division = '" + Div + "'", strDB);
						if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
						{
							rs.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + Dept + "' AND Division = '" + Div + "' AND Revenue = '" + Revenue + "'", strDB);
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								ValidRevenue = CheckValidAccount(Account);
								return ValidRevenue;
							}
							else
							{
								ValidRevenue = false;
								return ValidRevenue;
							}
						}
						else
						{
							ValidRevenue = false;
							return ValidRevenue;
						}
					}
					else
					{
						rs.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + Dept + "' AND Division = '" + tempRevDiv + "'", strDB);
						if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
						{
							rs.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + Dept + "' AND Revenue = '" + Revenue + "'", strDB);
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								ValidRevenue = CheckValidAccount(Account);
								return ValidRevenue;
							}
							else
							{
								ValidRevenue = false;
								return ValidRevenue;
							}
						}
						else
						{
							ValidRevenue = false;
							return ValidRevenue;
						}
					}
				}
				else
				{
					ValidRevenue = false;
					return ValidRevenue;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				ValidRevenue = false;
			}
			return ValidRevenue;
		}

		public static bool ValidLedger(string Account)
		{
			bool ValidLedger = false;
			// this follows the exact format as the validexpense function except it is looking for a valid general ledger account number <=== surpised yet?
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "ValidLedger"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Function
			// ResumeCode:
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rs = new clsDRWrapper();
				string Fund = "";
				string acct = "";
				string tempAcct;
				bool YearFlag = false;
				string Year = "";
				string tempAccount;
				string strDB;
				// VBto upgrade warning: intTotLen As short --> As int	OnWriteFCConvert.ToDouble(
				int intTotLen;
				int intHyphen = 0;
				strDB = "TWBD0000.vb1";
				tempAcct = modGlobal.PadToString(0, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
				tempAccount = Strings.Right(Account, Account.Length - 2);
				if (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2)) == 0)
				{
					YearFlag = true;
				}
				if (!YearFlag)
				{
					intHyphen = 2;
				}
				else
				{
					intHyphen = 1;
				}
				intTotLen = FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)) + Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)) + Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2)));
				if (intTotLen == tempAccount.Length - intHyphen)
				{
					// subtract the number of hyphens
					Fund = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)));
					tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)) + 1));
					acct = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)));
					if (!YearFlag)
					{
						tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)) + 1));
						Year = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2)));
					}
					rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE Fund = '" + Fund + "' AND Account = '" + acct + "'", strDB);
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						if (!YearFlag)
						{
							rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE Fund = '" + Fund + "' AND Account = '" + acct + "' AND [Year] = '" + Year + "'", strDB);
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								ValidLedger = CheckValidAccount(Account);
								return ValidLedger;
							}
							else
							{
								ValidLedger = false;
								return ValidLedger;
							}
						}
						else
						{
							rs.OpenRecordset("SELECT * FROM LedgerTitles WHERE Fund = '" + Fund + "' AND Account = '" + acct + "'", strDB);
							if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
							{
								ValidLedger = CheckValidAccount(Account);
								return ValidLedger;
							}
							else
							{
								ValidLedger = false;
								return ValidLedger;
							}
						}
					}
					else
					{
						ValidLedger = false;
						return ValidLedger;
					}
				}
				else
				{
					ValidLedger = false;
					return ValidLedger;
				}
				return ValidLedger;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				ValidLedger = false;
			}
			return ValidLedger;
		}

		public static bool CheckValidAccount(string x)
		{
			bool CheckValidAccount = false;
			// this function checks to see if this account is allowed, not just possible
			// On Error GoTo CallErrorRoutine
			// gstrCurrentRoutine = "CheckValidAccount"
			// Dim Mouse As clsMousePointer
			// Set Mouse = New clsMousePointer
			// GoTo ResumeCode
			// CallErrorRoutine:
			// Call SetErrorHandler
			// Exit Function
			// ResumeCode:
			clsDRWrapper rs = new clsDRWrapper();
			string AcctType;
			string F1;
			string F2 = "";
			string F3 = "";
			string F4 = "";
			string F5 = "";
			bool boolAccountCreated;
			string AcctFormat = "";
			int tempSpace;
			string strHoldAccount;
			strHoldAccount = x;
			// If ValidAcctCheck = 3 Then
			// CheckValidAccount = True
			// Exit Function
			// Else
			AcctType = Strings.Left(x, 1);
			x = Strings.Right(x, x.Length - 2);
			tempSpace = Strings.InStr(1, x, "-");
			F1 = Strings.Left(x, tempSpace - 1);
			x = Strings.Right(x, x.Length - tempSpace);
			tempSpace = Strings.InStr(1, x, "-");
			if (tempSpace > 0)
			{
				F2 = Strings.Left(x, tempSpace - 1);
				x = Strings.Right(x, x.Length - tempSpace);
			}
			else if (x.Length > 0)
			{
				F2 = x;
				x = "";
			}
			if (x != "")
			{
				tempSpace = Strings.InStr(1, x, "-");
				if (tempSpace > 0)
				{
					F3 = Strings.Left(x, tempSpace - 1);
					x = Strings.Right(x, x.Length - tempSpace);
				}
				else if (x.Length > 0)
				{
					F3 = x;
					x = "";
				}
			}
			if (x != "")
			{
				tempSpace = Strings.InStr(1, x, "-");
				if (tempSpace > 0)
				{
					F4 = Strings.Left(x, tempSpace - 1);
					x = Strings.Right(x, x.Length - tempSpace);
				}
				else if (x.Length > 0)
				{
					F4 = x;
					x = "";
				}
			}
			if (x != "")
			{
				F5 = x;
			}
			boolAccountCreated = AccountCreated(ref AcctType, ref F1, ref F2, ref F3, ref F4, ref F5);
			// this checks in the account master to see if there is an account created already
			if (boolAccountCreated)
			{
				CheckValidAccount = true;
				// yes there is an account already
			}
			else
			{
				if (Statics.ValidAcctCheck == 2)
				{
					// no there is no account, but it is a valid account
					tempSpace = FCConvert.ToInt32(FCMessageBox.Show(strHoldAccount + " is not a Valid Account.  Would you like to add it to the Valid Accounts file?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Invalid Account Warning"));
					if (tempSpace == FCConvert.ToInt32(DialogResult.Yes))
					{
						rs.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + strHoldAccount + "'", "TWBD0000.vb1");
						if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
						{
							rs.Edit();
							rs.Set_Fields("Valid", 1);
							// sets this account to valid
							rs.Update();
						}
						else
						{
							rs.OmitNullsOnInsert = true;
							rs.AddNew();
							// adds a new record in the AccountMaster table
							rs.Set_Fields("Account", strHoldAccount);
							rs.Set_Fields("AccountType", AcctType);
							// sets the account type
							rs.Set_Fields("FirstAccountField", F1);
							// sets the first piece of the account number
							rs.Set_Fields("SecondAccountField", F2);
							// sets the second piece of the account number
							rs.Set_Fields("ThirdAccountField", F3);
							// sets the third piece of the account number
							rs.Set_Fields("FourthAccountField", F4);
							// sets the fourth piece of the account number
							rs.Set_Fields("FifthAccountField", F5);
							// sets the fifth piece of the account number
							rs.Set_Fields("DateCreated", DateTime.Today);
							// sets the date created to the current date
							rs.Set_Fields("Valid", 1);
							// sets this account to valid
							rs.Update();
						}
						CheckValidAccount = true;
					}
					else if (tempSpace == FCConvert.ToInt32(DialogResult.No))
					{
						CheckValidAccount = true;
					}
					else
					{
						CheckValidAccount = false;
					}
				}
				else if (Statics.ValidAcctCheck == 3)
				{
					rs.OpenRecordset("SELECT * FROM AccountMaster WHERE Account = '" + strHoldAccount + "'", "TWBD0000.vb1");
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						rs.Edit();
						rs.Set_Fields("Valid", 1);
						// sets this account to valid
						rs.Update();
					}
					else
					{
						rs.OmitNullsOnInsert = true;
						rs.AddNew();
						// adds a new record in the AccountMaster table
						rs.Set_Fields("Account", strHoldAccount);
						rs.Set_Fields("AccountType", AcctType);
						// sets the account type
						rs.Set_Fields("FirstAccountField", F1);
						// sets the first piece of the account number
						rs.Set_Fields("SecondAccountField", F2);
						// sets the second piece of the account number
						rs.Set_Fields("ThirdAccountField", F3);
						// sets the third piece of the account number
						rs.Set_Fields("FourthAccountField", F4);
						// sets the fourth piece of the account number
						rs.Set_Fields("FifthAccountField", F5);
						// sets the fifth piece of the account number
						rs.Set_Fields("DateCreated", DateTime.Today);
						// sets the date created to the current date
						rs.Set_Fields("Valid", 1);
						// sets this account to valid
						rs.Update();
					}
					CheckValidAccount = true;
				}
				else
				{
					FCMessageBox.Show(strHoldAccount + " is not set up in your valid accounts file", MsgBoxStyle.Information, "Invalid Account");
					CheckValidAccount = false;
				}
			}
			return CheckValidAccount;
		}

		private static bool AccountCreated(ref string strType, ref string F1, ref string F2, ref string F3, ref string F4, ref string F5)
		{
			bool AccountCreated = false;
			// this function will return True if the account passed in is already created and false otherwise
			string strDB;
			clsDRWrapper rs = new clsDRWrapper();
			string strSQL;
			strDB = "TWBD0000.vb1";
			strSQL = "SELECT * FROM AccountMaster WHERE AccountType = '" + strType + "'";
			// only check for the fields passed in
			if (F1 != "")
				strSQL += " AND FirstAccountField = '" + F1 + "'";
			if (F2 != "")
				strSQL += " AND SecondAccountField = '" + F2 + "'";
			if (F3 != "")
				strSQL += " AND ThirdAccountField = '" + F3 + "'";
			if (F4 != "")
				strSQL += " AND FourthAccountField = '" + F4 + "'";
			if (F5 != "")
				strSQL += " AND FifthAccountField = '" + F5 + "'";
			rs.OpenRecordset(strSQL, strDB);
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if there is no record meeting the standard
				if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("Valid")))
				{
					AccountCreated = true;
				}
				else
				{
					AccountCreated = false;
				}
			}
			else
			{
				AccountCreated = false;
			}
			return AccountCreated;
		}
		//FC:FINAL:BBE - add overload with signature (dynamic, int) -> string
		public static string GetFormat_6(dynamic str, int x)
		{
			return GetFormat(FCConvert.ToString(str), ref x);
		}
		// VBto upgrade warning: str As string	OnWrite(string, int)
		// VBto upgrade warning: x As short	OnWrite(string, double, short)
		public static string GetFormat_6(string str, int x)
		{
			return GetFormat(str, ref x);
		}

		public static string GetFormat(string str, ref int x)
		{
			string GetFormat = "";
			if (Conversion.Val(str) < 0)
			{
				switch (x)
				{
					case 0:
					case 1:
						{
							GetFormat = Strings.Format(FCConvert.ToString(Conversion.Val(str)), "0");
							break;
						}
					default:
						{
							GetFormat = Strings.Format(FCConvert.ToString(Conversion.Val(str)), Strings.StrDup((x - 1), "0"));
							break;
						}
				}
				//end switch
			}
			else
			{
				switch (x)
				{
					case 0:
					case 1:
						{
							GetFormat = Strings.Format(FCConvert.ToString(Conversion.Val(str)), "0");
							break;
						}
					case 2:
						{
							GetFormat = Strings.Format(FCConvert.ToString(Conversion.Val(str)), "00");
							break;
						}
					case 3:
						{
							GetFormat = Strings.Format(FCConvert.ToString(Conversion.Val(str)), "000");
							break;
						}
					case 4:
						{
							GetFormat = Strings.Format(FCConvert.ToString(Conversion.Val(str)), "0000");
							break;
						}
					case 5:
						{
							GetFormat = Strings.Format(FCConvert.ToString(Conversion.Val(str)), "00000");
							break;
						}
					case 6:
						{
							GetFormat = Strings.Format(FCConvert.ToString(Conversion.Val(str)), "000000");
							break;
						}
					case 7:
						{
							GetFormat = Strings.Format(FCConvert.ToString(Conversion.Val(str)), "0000000");
							break;
						}
					case 8:
						{
							GetFormat = Strings.Format(FCConvert.ToString(Conversion.Val(str)), "00000000");
							break;
						}
					case 9:
						{
							GetFormat = Strings.Format(FCConvert.ToString(Conversion.Val(str)), "000000000");
							break;
						}
					case 10:
						{
							GetFormat = Strings.Format(FCConvert.ToString(Conversion.Val(str)), "0000000000");
							break;
						}
					case 11:
						{
							GetFormat = Strings.Format(FCConvert.ToString(Conversion.Val(str)), "00000000000");
							break;
						}
					default:
						{
							GetFormat = Strings.Format(FCConvert.ToString(Conversion.Val(str)), Strings.StrDup(x, "0"));
							break;
						}
				}
				//end switch
			}
			return GetFormat;
		}

		public static bool SetBDFormats(bool boolShowError = false)
		{
			bool SetBDFormats = false;
			// this function will hit the GN database and return the Account Formats into the format variables
			// also it will return True if no problems were experienced
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBD = new clsDRWrapper();
				if (rsBD.OpenRecordset("SELECT * FROM Budgetary", "TWBD0000.vb1"))
				{
					if (FCUtils.IsNull(rsBD.Get_Fields_String("ValidAccountUse")) == false)
					{
						if (Conversion.Val(rsBD.Get_Fields_String("ValidAccountUse")) != 0)
						{
							SetBDAccountTolerance_2(FCConvert.ToInt32(Conversion.Val(rsBD.Get_Fields_String("ValidAccountUse"))));
						}
						else
						{
							SetBDAccountTolerance_2(1);
						}
					}
					else
					{
						SetBDAccountTolerance_2(1);
					}
				}
				else
				{
					SetBDFormats = false;
				}
				modAccountTitle.SetAccountFormats();
				SetBDFormats = true;

                
				

				Statics.strZeroDiv = modAccountTitle.Statics.ExpDivFlag ? "0" : GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)));

                return SetBDFormats;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				if (boolShowError)
					FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message, MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Set Budgetary Formats Error");
				SetBDFormats = false;
			}
			return SetBDFormats;
		}
		// VBto upgrade warning: lngTol As int	OnWrite(double, short)
		public static void SetBDAccountTolerance_2(int lngTol)
		{
			SetBDAccountTolerance(ref lngTol);
		}

		public static void SetBDAccountTolerance(ref int lngTol)
		{
			// this function will set the Tolerance of the Valid Account Module
			// 0 or 1     = Only allow Valid Accounts that are already setup
			// 2          = Allow any Valid Account to be added if it does not exist
			// 3          = Allow any account number that fits the format
			Statics.ValidAcctCheck = lngTol;
			StaticSettings.gGlobalBudgetaryAccountSettings.ValidAcctCheck =
					(ValidAccountSetting)Statics.ValidAcctCheck;
			
		}


		public class StaticVariables
		{
            //public bool gboolSchoolAccounts;
			//public bool gboolTownAccounts;
            public int ValidAcctCheck;
            public string strZeroDiv = "";
        }

        public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
