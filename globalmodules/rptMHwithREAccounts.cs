﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptMHWithREAccounts : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/21/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/15/2004              *
		// ********************************************************
		clsDRWrapper clsre = new clsDRWrapper();
		clsDRWrapper clsMortgage = new clsDRWrapper();
		// clsAssoc = new clsDRWrapper();
		bool boolTax;
		// whether we should print tax info or not
		//clsDRWrapper clsTaxRE = new clsDRWrapper();
		int lngBYear;
		// billing year
		public rptMHWithREAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Real Estate List by Mortgage Holder";
            this.ReportEnd += RptMHWithREAccounts_ReportEnd;
		}

        private void RptMHWithREAccounts_ReportEnd(object sender, EventArgs e)
        {
			clsre.DisposeOf();
            clsMortgage.DisposeOf();

		}

        public static rptMHWithREAccounts InstancePtr
		{
			get
			{
				return (rptMHWithREAccounts)Sys.GetInstance(typeof(rptMHWithREAccounts));
			}
		}

		protected rptMHWithREAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
				clsre.Dispose();
				clsMortgage.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("TheBinder");
			this.Fields["TheBinder"].Value = 0;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsre.EndOfFile();
			if (!eArgs.EOF)
			{
				// do it here or the groupheader info will be behind the times
				this.Fields["TheBinder"].Value = clsre.Get_Fields_Int32("mortgageholderid");
			}
		}
		// VBto upgrade warning: strStart As string	OnWriteFCConvert.ToDouble(
		// VBto upgrade warning: strEnd As string	OnWriteFCConvert.ToDouble(
		public void Init(bool boolNameOrder, string strStart = "", string strEnd = "", bool boolSingleAccount = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSQL = "";
				string strOrder;
				string str1 = "";
				string str2 = "";
				string str3 = "";
				string str4 = "";
				string strWhere = "";
				string strDbGNC;
				if (boolSingleAccount)
				{
					txtTitle.Text = "Mortgage Holder " + strStart + " Information";
					fldReportName.Text = "with Real Estate Accounts";
				}
				else
				{
					txtTitle.Text = "Mortgage Holder List";
					fldReportName.Text = "with Real Estate Accounts";
				}
				strOrder = "";
				if (boolNameOrder)
				{
					if (strEnd == string.Empty)
					{
						if (strStart == string.Empty)
						{
							strWhere = "";
						}
						else
						{
							strWhere = " and name >= '" + strStart + "' ";
						}
					}
					else
					{
						strWhere = " AND name >= '" + strStart + "    ' AND name <= '" + strEnd + "zzzzz' ";
					}
					strOrder = "order by Name, Mortgageholderid, rsName";
				}
				else
				{
					if (strEnd == string.Empty)
					{
						if (strStart == string.Empty)
						{
							strWhere = "";
						}
						else
						{
							strWhere = " AND mortgageholderid >= " + FCConvert.ToString(Conversion.Val(strStart)) + " ";
						}
					}
					else
					{
						strWhere = " AND mortgageholderid >= " + FCConvert.ToString(Conversion.Val(strStart)) + " AND mortgageholderid <= " + FCConvert.ToString(Conversion.Val(strEnd)) + " ";
					}
					strOrder = " order by Mortgageholderid, RsName";
				}
				strDbGNC = clsre.Get_GetFullDBName("CentralData") + ".dbo.";
				string strMasterJoin;
				strMasterJoin = GetMasterJoin();
				// Call clsre.OpenRecordset("SELECT * FROM Master INNER JOIN (" & strDbGNC & "MoRtGaGeHoLdErS INNER JOIN " & strDbGNC & "mortgageassociation ON (MORtgageassociation.mortgageholderid = mortgageholders.ID)) on (mortgageassociation.account = master.rsaccount)  where mortgageassociation.module = 'RE' and not (master.rsdeleted = 1) and master.rscard = 1 " & strWhere, "twre0000.vb1")
				clsre.OpenRecordset(strMasterJoin + " INNER JOIN (" + strDbGNC + "MoRtGaGeHoLdErS INNER JOIN " + strDbGNC + "mortgageassociation ON (MORtgageassociation.mortgageholderid = mortgageholders.ID)) on (mortgageassociation.account = mparty.rsaccount)  where mortgageassociation.module = 'RE' and not (mparty.rsdeleted = 1) and mparty.rscard = 1 " + strWhere + strOrder, "twre0000.vb1");
				if (clsre.EndOfFile())
				{
					FCMessageBox.Show("No records found.", MsgBoxStyle.Information, "No Match");
					Cancel();
					return;
				}
				// clsre.InsertName "OwnerPartyID,SecOwnerPartyID", "Own1,Own2", False, True, True, "", False, "", True, ""
				// If boolNameOrder Then
				// clsre.ReOrder "Name, Mortgageholderid, Own1FullName"
				// Else
				// clsre.ReOrder "Mortgageholderid, Own1FullName"
				// End If
				// Me.Show , MDIParent
				frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "", false, false);
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + "\r\n" + "In Init", MsgBoxStyle.Critical, "Error");
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strAddress1 = "";
			string strAddress2 = "";
			string strAddress3 = "";
			string strAddress4 = "";
			if (!clsre.EndOfFile())
			{
				// set the group binder
				this.Fields["TheBinder"].Value = clsre.Get_Fields_Int32("mortgageholderid");
				txtaccount.Text = FCConvert.ToString(clsre.Get_Fields_Int32("rsaccount"));
				// txtName.Text = clsre.Fields("Own1FullName")
				txtName.Text = clsre.Get_Fields_String("rsname");
				txtLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("rslocnumalph")) + " " + FCConvert.ToString(clsre.Get_Fields_String("rslocapt"))) + " " + FCConvert.ToString(clsre.Get_Fields_String("rslocstreet")));
				txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("RSMapLot")));
				fldBookPage.Text = Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("BookPage")));
				clsre.MoveNext();
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			string strAddress1;
			string strAddress2;
			string strAddress3;
			string strAddress4;
			int intRowCt = 0;
			clsMortgage.OpenRecordset("select * from mortgageholders where ID = " + this.Fields["TheBinder"].Value, "CentralData");
			// "twre0000.vb1")
			if (clsMortgage.EndOfFile())
				return;
			txtNumber.Text = this.Fields["thebinder"].Value.ToString();
			txtMortgageName.Text = clsMortgage.Get_Fields_String("name");
			strAddress1 = FCConvert.ToString(clsMortgage.Get_Fields_String("address1"));
			strAddress2 = FCConvert.ToString(clsMortgage.Get_Fields_String("address2"));
			// straddress3 = Trim(Trim(clsMortgage.Fields("streetnumber") & " " & clsMortgage.Fields("apt")) & " " & clsMortgage.Fields("streetname"))
			strAddress3 = FCConvert.ToString(clsMortgage.Get_Fields_String("address3"));
			if (strAddress3 == "0")
				strAddress3 = "";
			strAddress4 = Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("city")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("state")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("zip")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("zip4")));
			// now condense this info
			if (strAddress1 == string.Empty)
			{
				strAddress1 = strAddress2;
				strAddress2 = strAddress3;
				strAddress3 = strAddress4;
				strAddress4 = "";
				intRowCt += 1;
				if (strAddress2 == string.Empty)
				{
					strAddress2 = strAddress3;
					strAddress3 = strAddress4;
					strAddress4 = "";
					// intRowCt = intRowCt + 1
					if (strAddress2 == string.Empty)
					{
						strAddress2 = strAddress3;
						strAddress3 = strAddress4;
						strAddress4 = "";
						// intRowCt = intRowCt + 1
					}
				}
			}
			if (strAddress2 == string.Empty)
			{
				strAddress2 = strAddress3;
				strAddress3 = strAddress4;
				strAddress4 = "";
				intRowCt += 1;
				if (strAddress2 == string.Empty)
				{
					strAddress2 = strAddress3;
					strAddress3 = strAddress4;
					strAddress4 = "";
					// intRowCt = intRowCt + 1
				}
			}
			if (strAddress3 == string.Empty)
			{
				strAddress3 = strAddress4;
				strAddress4 = "";
				intRowCt += 1;
			}
			txtAddress1.Text = strAddress1;
			txtAddress2.Text = strAddress2;
			txtAddress3.Text = strAddress3;
			txtAddress4.Text = strAddress4;
			lblAccount.Top = txtAddress1.Top + ((4 - intRowCt) * txtAddress1.Height);
			lblName.Top = lblAccount.Top;
			lblMapLot.Top = lblAccount.Top;
			lblLocation.Top = lblAccount.Top;
			lblBookPage.Top = lblAccount.Top;
			lnAccountHeader.Y1 = lblAccount.Top + lblAccount.Height;
			lnAccountHeader.Y2 = lnAccountHeader.Y1;
			GroupHeader1.Height = lnAccountHeader.Y1 + 20 / 1440F;
			// Detail.Height = lnAccountHeader.Y1 + 3
		}

		private string GetMasterJoin()
		{
			string GetMasterJoin = "";
			// select master.*, cpo.FullNameLastFirst as RSName,cpso.FullNameLastFirst as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4 from TRIO_Test_Live_RealEstate.dbo.master left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpo on (master.ownerpartyid = cpo.ID) left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpso on (master.SecOwnerPartyID = cpso.ID)
			string strReturn;
			strReturn = "select master.*, cpo.FullNameLF as RSName,cpso.FullNameLF as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4, cpo.email as email from ";
			clsDRWrapper tLoad = new clsDRWrapper();
			string strTemp;
			strTemp = tLoad.Get_GetFullDBName("RealEstate");
			strReturn += strTemp + ".dbo.master left join ";
			strTemp = tLoad.Get_GetFullDBName("CentralParties");
			strTemp += ".dbo.PartyAndAddressView ";
			strReturn += strTemp + " as cpo on (master.ownerpartyid = cpo.ID) left join ";
			strReturn += strTemp + " as cpso on (master.SecOwnerPartyID = cpso.ID)";
			strReturn = " select * from (" + strReturn + ") mparty ";
			GetMasterJoin = strReturn;
			return GetMasterJoin;
		}

		private void rptMHWithREAccounts_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMHWithREAccounts.Caption	= "Real Estate List by Mortgage Holder";
			//rptMHWithREAccounts.Icon	= "rptMHwithREAccounts.dsx":0000";
			//rptMHWithREAccounts.Left	= 0;
			//rptMHWithREAccounts.Top	= 0;
			//rptMHWithREAccounts.Width	= 11880;
			//rptMHWithREAccounts.Height	= 8490;
			//rptMHWithREAccounts.StartUpPosition	= 3;
			//rptMHWithREAccounts.SectionData	= "rptMHwithREAccounts.dsx":058A;
			//End Unmaped Properties
		}
	}
}
