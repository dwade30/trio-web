﻿namespace Global
{
	/// <summary>
	/// Summary description for sarARDACash.
	/// </summary>
	partial class sarARDACash
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarARDACash));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBillType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentPrin = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCorrectionPrin = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCorrectionInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCorrectionTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldPaymentPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCorrectionPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPaymentInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCorrectionInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPaymentTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCorrectionTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotalCash = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalPaymentPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCorrectionPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPaymentInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCorrectionInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPaymentTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCorrectionTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrectionPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrectionInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrectionTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCorrectionPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCorrectionInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCorrectionTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPaymentPrin,
				this.fldCorrectionPrin,
				this.fldPaymentInterest,
				this.fldCorrectionInt,
				this.fldPaymentTax,
				this.fldCorrectionTax,
				this.fldTotal,
				this.fldBillType
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader
			});
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblBillType,
				this.lblTotal,
				this.lblPaymentPrin,
				this.lblCorrectionPrin,
				this.lblPaymentInt,
				this.lblCorrectionInt,
				this.lblPaymentTax,
				this.lblCorrectionTax,
				this.lblPrincipal,
				this.lblInterest,
				this.lblTax,
				this.lnHeader
			});
			this.GroupHeader1.Height = 0.3125F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTotalCash,
				this.fldTotalPaymentPrin,
				this.fldTotalCorrectionPrin,
				this.fldTotalPaymentInt,
				this.fldTotalCorrectionInt,
				this.fldTotalPaymentTax,
				this.fldTotalCorrectionTax,
				this.fldTotalTotal,
				this.lnTotals
			});
			this.GroupFooter1.Height = 0.3125F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "- - - - - - Cash - - - - - -";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 9.375F;
			// 
			// lblBillType
			// 
			this.lblBillType.Height = 0.1875F;
			this.lblBillType.HyperLink = null;
			this.lblBillType.Left = 0F;
			this.lblBillType.Name = "lblBillType";
			this.lblBillType.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblBillType.Text = "Type";
			this.lblBillType.Top = 0.125F;
			this.lblBillType.Width = 0.625F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 8.1875F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.125F;
			this.lblTotal.Width = 1.0625F;
			// 
			// lblPaymentPrin
			// 
			this.lblPaymentPrin.Height = 0.1875F;
			this.lblPaymentPrin.HyperLink = null;
			this.lblPaymentPrin.Left = 1.5625F;
			this.lblPaymentPrin.Name = "lblPaymentPrin";
			this.lblPaymentPrin.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPaymentPrin.Text = "Payment";
			this.lblPaymentPrin.Top = 0.125F;
			this.lblPaymentPrin.Width = 0.875F;
			// 
			// lblCorrectionPrin
			// 
			this.lblCorrectionPrin.Height = 0.1875F;
			this.lblCorrectionPrin.HyperLink = null;
			this.lblCorrectionPrin.Left = 2.4375F;
			this.lblCorrectionPrin.Name = "lblCorrectionPrin";
			this.lblCorrectionPrin.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblCorrectionPrin.Text = "Correction";
			this.lblCorrectionPrin.Top = 0.125F;
			this.lblCorrectionPrin.Width = 0.875F;
			// 
			// lblPaymentInt
			// 
			this.lblPaymentInt.Height = 0.1875F;
			this.lblPaymentInt.HyperLink = null;
			this.lblPaymentInt.Left = 3.75F;
			this.lblPaymentInt.Name = "lblPaymentInt";
			this.lblPaymentInt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPaymentInt.Text = "Payment";
			this.lblPaymentInt.Top = 0.125F;
			this.lblPaymentInt.Width = 0.875F;
			// 
			// lblCorrectionInt
			// 
			this.lblCorrectionInt.Height = 0.1875F;
			this.lblCorrectionInt.HyperLink = null;
			this.lblCorrectionInt.Left = 4.625F;
			this.lblCorrectionInt.Name = "lblCorrectionInt";
			this.lblCorrectionInt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblCorrectionInt.Text = "Correction";
			this.lblCorrectionInt.Top = 0.125F;
			this.lblCorrectionInt.Width = 0.875F;
			// 
			// lblPaymentTax
			// 
			this.lblPaymentTax.Height = 0.1875F;
			this.lblPaymentTax.HyperLink = null;
			this.lblPaymentTax.Left = 5.9375F;
			this.lblPaymentTax.Name = "lblPaymentTax";
			this.lblPaymentTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPaymentTax.Text = "Payment";
			this.lblPaymentTax.Top = 0.125F;
			this.lblPaymentTax.Width = 0.875F;
			// 
			// lblCorrectionTax
			// 
			this.lblCorrectionTax.Height = 0.1875F;
			this.lblCorrectionTax.HyperLink = null;
			this.lblCorrectionTax.Left = 6.8125F;
			this.lblCorrectionTax.Name = "lblCorrectionTax";
			this.lblCorrectionTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblCorrectionTax.Text = "Correction";
			this.lblCorrectionTax.Top = 0.125F;
			this.lblCorrectionTax.Width = 0.875F;
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 1.5625F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblPrincipal.Text = "Principal";
			this.lblPrincipal.Top = 0F;
			this.lblPrincipal.Width = 1.75F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 3.75F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 0F;
			this.lblInterest.Width = 1.75F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 5.9375F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 0F;
			this.lblTax.Width = 1.75F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 0.3125F;
			this.lnHeader.Width = 6.9375F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 6.9375F;
			this.lnHeader.Y1 = 0.3125F;
			this.lnHeader.Y2 = 0.3125F;
			// 
			// fldPaymentPrin
			// 
			this.fldPaymentPrin.Height = 0.1875F;
			this.fldPaymentPrin.Left = 1.5625F;
			this.fldPaymentPrin.Name = "fldPaymentPrin";
			this.fldPaymentPrin.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPaymentPrin.Text = "0.00";
			this.fldPaymentPrin.Top = 0F;
			this.fldPaymentPrin.Width = 0.875F;
			// 
			// fldCorrectionPrin
			// 
			this.fldCorrectionPrin.Height = 0.1875F;
			this.fldCorrectionPrin.Left = 2.4375F;
			this.fldCorrectionPrin.Name = "fldCorrectionPrin";
			this.fldCorrectionPrin.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCorrectionPrin.Text = "0.00";
			this.fldCorrectionPrin.Top = 0F;
			this.fldCorrectionPrin.Width = 0.875F;
			// 
			// fldPaymentInterest
			// 
			this.fldPaymentInterest.Height = 0.1875F;
			this.fldPaymentInterest.Left = 3.75F;
			this.fldPaymentInterest.Name = "fldPaymentInterest";
			this.fldPaymentInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPaymentInterest.Text = "0.00";
			this.fldPaymentInterest.Top = 0F;
			this.fldPaymentInterest.Width = 0.875F;
			// 
			// fldCorrectionInt
			// 
			this.fldCorrectionInt.Height = 0.1875F;
			this.fldCorrectionInt.Left = 4.625F;
			this.fldCorrectionInt.Name = "fldCorrectionInt";
			this.fldCorrectionInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCorrectionInt.Text = "0.00";
			this.fldCorrectionInt.Top = 0F;
			this.fldCorrectionInt.Width = 0.875F;
			// 
			// fldPaymentTax
			// 
			this.fldPaymentTax.Height = 0.1875F;
			this.fldPaymentTax.Left = 5.9375F;
			this.fldPaymentTax.Name = "fldPaymentTax";
			this.fldPaymentTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPaymentTax.Text = "0.00";
			this.fldPaymentTax.Top = 0F;
			this.fldPaymentTax.Width = 0.875F;
			// 
			// fldCorrectionTax
			// 
			this.fldCorrectionTax.Height = 0.1875F;
			this.fldCorrectionTax.Left = 6.8125F;
			this.fldCorrectionTax.Name = "fldCorrectionTax";
			this.fldCorrectionTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCorrectionTax.Text = "0.00";
			this.fldCorrectionTax.Top = 0F;
			this.fldCorrectionTax.Width = 0.875F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 8.1875F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal.Text = "0.00";
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1.0625F;
			// 
			// fldBillType
			// 
			this.fldBillType.Height = 0.1875F;
			this.fldBillType.Left = 0F;
			this.fldBillType.Name = "fldBillType";
			this.fldBillType.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldBillType.Text = null;
			this.fldBillType.Top = 0F;
			this.fldBillType.Width = 0.6875F;
			// 
			// lblTotalCash
			// 
			this.lblTotalCash.Height = 0.1875F;
			this.lblTotalCash.HyperLink = null;
			this.lblTotalCash.Left = 0F;
			this.lblTotalCash.Name = "lblTotalCash";
			this.lblTotalCash.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTotalCash.Text = "Totals:";
			this.lblTotalCash.Top = 0.0625F;
			this.lblTotalCash.Width = 0.5625F;
			// 
			// fldTotalPaymentPrin
			// 
			this.fldTotalPaymentPrin.Height = 0.1875F;
			this.fldTotalPaymentPrin.Left = 1.5625F;
			this.fldTotalPaymentPrin.Name = "fldTotalPaymentPrin";
			this.fldTotalPaymentPrin.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPaymentPrin.Text = "0.00";
			this.fldTotalPaymentPrin.Top = 0.0625F;
			this.fldTotalPaymentPrin.Width = 0.875F;
			// 
			// fldTotalCorrectionPrin
			// 
			this.fldTotalCorrectionPrin.Height = 0.1875F;
			this.fldTotalCorrectionPrin.Left = 2.4375F;
			this.fldTotalCorrectionPrin.Name = "fldTotalCorrectionPrin";
			this.fldTotalCorrectionPrin.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCorrectionPrin.Text = "0.00";
			this.fldTotalCorrectionPrin.Top = 0.0625F;
			this.fldTotalCorrectionPrin.Width = 0.875F;
			// 
			// fldTotalPaymentInt
			// 
			this.fldTotalPaymentInt.Height = 0.1875F;
			this.fldTotalPaymentInt.Left = 3.75F;
			this.fldTotalPaymentInt.Name = "fldTotalPaymentInt";
			this.fldTotalPaymentInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPaymentInt.Text = "0.00";
			this.fldTotalPaymentInt.Top = 0.0625F;
			this.fldTotalPaymentInt.Width = 0.875F;
			// 
			// fldTotalCorrectionInt
			// 
			this.fldTotalCorrectionInt.Height = 0.1875F;
			this.fldTotalCorrectionInt.Left = 4.625F;
			this.fldTotalCorrectionInt.Name = "fldTotalCorrectionInt";
			this.fldTotalCorrectionInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCorrectionInt.Text = "0.00";
			this.fldTotalCorrectionInt.Top = 0.0625F;
			this.fldTotalCorrectionInt.Width = 0.875F;
			// 
			// fldTotalPaymentTax
			// 
			this.fldTotalPaymentTax.Height = 0.1875F;
			this.fldTotalPaymentTax.Left = 5.9375F;
			this.fldTotalPaymentTax.Name = "fldTotalPaymentTax";
			this.fldTotalPaymentTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPaymentTax.Text = "0.00";
			this.fldTotalPaymentTax.Top = 0.0625F;
			this.fldTotalPaymentTax.Width = 0.875F;
			// 
			// fldTotalCorrectionTax
			// 
			this.fldTotalCorrectionTax.Height = 0.1875F;
			this.fldTotalCorrectionTax.Left = 6.8125F;
			this.fldTotalCorrectionTax.Name = "fldTotalCorrectionTax";
			this.fldTotalCorrectionTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCorrectionTax.Text = "0.00";
			this.fldTotalCorrectionTax.Top = 0.0625F;
			this.fldTotalCorrectionTax.Width = 0.875F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 8.1875F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTotal.Text = "0.00";
			this.fldTotalTotal.Top = 0.0625F;
			this.fldTotalTotal.Width = 1.0625F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 0.5625F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0.0625F;
			this.lnTotals.Width = 6.375F;
			this.lnTotals.X1 = 0.5625F;
			this.lnTotals.X2 = 6.9375F;
			this.lnTotals.Y1 = 0.0625F;
			this.lnTotals.Y2 = 0.0625F;
			// 
			// sarARDACash
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrectionPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrectionInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrectionTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCorrectionPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCorrectionInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCorrectionTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCorrectionPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCorrectionInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCorrectionTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillType;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentPrin;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCorrectionPrin;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCorrectionInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCorrectionTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalCash;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaymentPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCorrectionPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaymentInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCorrectionInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaymentTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCorrectionTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
	}
}
