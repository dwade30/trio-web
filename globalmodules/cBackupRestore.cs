﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using System.Collections.Generic;
using fecherFoundation;
using System.IO;
using SharedApplication.Backups;
using TWSharedLibrary;
using StringBuilder = System.Text.StringBuilder;

namespace Global
{
    public class cBackupRestore
    {
        //=========================================================
        private string strLastError = "";
        private int lngLastError;

        public string LastErrorMessage
        {
            get
            {
                string LastErrorMessage = "";
                LastErrorMessage = strLastError;
                return LastErrorMessage;
            }
        }

        public int LastErrorNumber
        {
            get
            {
                int LastErrorNumber = 0;
                LastErrorNumber = lngLastError;
                return LastErrorNumber;
            }
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As bool
        public bool HadError
        {
            get
            {
                bool HadError = false;
                HadError = lngLastError != 0;
                return HadError;
            }
        }

        public void ClearErrors()
        {
            strLastError = "";
            lngLastError = 0;
        }

        private void SetError(int lngErrorNumber, string strErrorMessage)
        {
            lngLastError = lngErrorNumber;
            strLastError = strErrorMessage;
        }

        public List<backupItem> GetBackupListing(string strBackupPath)
        {
            clsDRWrapper rsExec = new clsDRWrapper();
            var lst = new List<backupItem>();
            try
            {
                // tell the db server you want all the files in the backups folder
                // files will be like PresqueIsle_Budgetary_12-07-2020_152709.bak
                // so we parse out the name and the date

                StringBuilder strSQL = new StringBuilder();
                strSQL.AppendLine("IF OBJECT_ID('tempdb..#DirectoryTree') IS NOT NULL");
                strSQL.AppendLine("DROP TABLE #DirectoryTree;");
                strSQL.AppendLine("CREATE TABLE #DirectoryTree (");
                strSQL.AppendLine("id           INT            IDENTITY (1, 1),");
                strSQL.AppendLine("subdirectory NVARCHAR (512),");
                strSQL.AppendLine("depth        INT           ,");
                strSQL.AppendLine("isfile       BIT           ");
                strSQL.AppendLine(");");
                strSQL.AppendLine("INSERT #DirectoryTree (subdirectory, depth, isfile)");
                strSQL.AppendLine($"EXECUTE Master.sys.xp_dirtree '{strBackupPath}', 1, 1;");
                strSQL.AppendLine("SELECT   subdirectory,");
                strSQL.AppendLine("LEFT(subdirectory, len(subdirectory) - 22) AS [filename],");
                //PresqueIsle_Budgetary_12-07-2020_152709.bak
                strSQL.AppendLine("LEFT(RIGHT(subdirectory, 21), 10) + ' ' + LEFT(RIGHT(subdirectory, 10), 2) + ':' + LEFT(RIGHT(subdirectory, 8), 2) + ':' + LEFT(RIGHT(subdirectory, 6), 2) AS [filedate]");
                strSQL.AppendLine("FROM     #DirectoryTree");
                strSQL.AppendLine("WHERE    isfile = 1");
                strSQL.AppendLine("AND RIGHT(subdirectory, 4) = '.BAK'");
                strSQL.AppendLine($"AND LEFT(subdirectory, {rsExec.MegaGroup.Length}) = '{rsExec.MegaGroup}'");
                strSQL.AppendLine("ORDER BY subdirectory;");

                rsExec.OpenRecordset(strSQL.ToString(), "SystemSettings");

                if (rsExec.RecordCount() > 0)
                {
                    rsExec.MoveLast();
                    rsExec.MoveFirst();
                }

                DateTime dt;
                while (!rsExec.EndOfFile())
                {
                    if (DateTime.TryParse(rsExec.Get_Fields("filedate"), out dt))
                    {
                        lst.Add(new backupItem
                        {
                            BackupDate = dt, 
                            FileName = rsExec.Get_Fields("filename")
                        });
                    }

                    rsExec.MoveNext();
                }
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsExec.DisposeOf();
            }

            return lst;
        }

        private string BackupScript(string strDBName, string strBackupPath, bool boolCopyOnly)
        {
            var strSQL = new StringBuilder();
            strSQL.AppendLine("Backup Database " + strDBName);
            strSQL.AppendLine(" TO DISK = '" + strBackupPath + "' ");
            strSQL.AppendLine(" with NOINIT ");
            if (boolCopyOnly)
            {
                strSQL.AppendLine(", COPY_ONLY ");
            }
            strSQL.AppendLine(", Name = '" + strDBName + " Backup'");
            strSQL.AppendLine(", Description = 'Full backup of " + strDBName + "'");
            
            return strSQL.ToString();
        }

        public void MakeBackup(string strDBName, string strBackupPathAndName, bool boolCopyOnly)
        {
            ClearErrors();
            clsDRWrapper rsExec = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                string strSQL;
                strSQL = BackupScript(strDBName, strBackupPathAndName, boolCopyOnly);
                rsExec.Execute(strSQL, "SystemSettings");

                return;
            }
            catch
            {
                // ErrorHandler:
                SetError(Information.Err().Number, Information.Err().Description);
            }
            finally
            {
                rsExec.DisposeOf();
            }
        }

        public void RestoreAs(string strBackupPathAndName, string strNewDBName, int intFileNumToRestore = -1)
        {
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                cGenericCollection headers;
                headers = GetBackupHeadersFromFile(strBackupPathAndName);
                cRestoreFileDetails fDetail;
                cRestoreArgs restInfo = new cRestoreArgs();
                headers.MoveFirst();
                restInfo.DatabaseInfo.DestinationDatabase = strNewDBName;
                restInfo.FileNumber = 1;
                restInfo.DatabaseInfo.RestorePath = strBackupPathAndName;
                int intFileNumber;
                intFileNumber = 1;
                // vbPorter upgrade warning: tDate As DateTime	OnWrite(string)
                DateTime tDate;
                tDate = FCConvert.ToDateTime("1/1/2000");
                while (headers.IsCurrent())
                {
                    //Application.DoEvents();
                    fDetail = (cRestoreFileDetails)headers.GetCurrentItem();
                    if (intFileNumToRestore > 0 && fDetail.FileNumber == intFileNumToRestore)
                    {
                        restInfo.FileNumber = fDetail.FileNumber;
                        restInfo.DatabaseInfo.DatabaseName = fDetail.DatabaseName;
                    }
                    else
                    {
                        if (Information.IsDate(fDetail.BackupDate))
                        {
                            if (DateAndTime.DateDiff("d", tDate, FCConvert.ToDateTime(fDetail.BackupDate)) > 0)
                            {
                                tDate = FCConvert.ToDateTime(fDetail.BackupDate);
                                restInfo.FileNumber = fDetail.FileNumber;
                                restInfo.DatabaseInfo.DatabaseName = fDetail.DatabaseName;
                            }
                        }
                    }
                    headers.MoveNext();
                }
                string strSQL;
                strSQL = GetRestoreScript(ref restInfo);
                clsDRWrapper rsExec = new clsDRWrapper();
                if (!rsExec.Execute(strSQL, "SystemSettings", false))
                {
                    SetError(rsExec.ErrorNumber, rsExec.ErrorDescription);
                }
                rsExec.DisposeOf();
            }
            catch
            {
                // ErrorHandler:
                SetError(Information.Err().Number, Information.Err().Description);
            }
        }

        private string GetScriptRestoreHeader(string strBackupFileFullPath)
        {
            string GetScriptRestoreHeader = "";
            string strSQL;
            strSQL = "Restore HeaderOnly from DISK = N'" + strBackupFileFullPath + "' with NOUNLOAD";
            GetScriptRestoreHeader = strSQL;
            return GetScriptRestoreHeader;
        }
        /// <summary>
        /// collection of cRestoreFileDetails
        /// </summary>
        // vbPorter upgrade warning: strBackupFileFullPath As Variant --> As string
        public cGenericCollection GetBackupHeadersFromFile(string strBackupFileFullPath)
        {
            cGenericCollection GetBackupHeadersFromFile = null;
            string strSQL;
            cGenericCollection tColl = new cGenericCollection();
            strSQL = GetScriptRestoreHeader(strBackupFileFullPath);
            clsDRWrapper rsLoad = new clsDRWrapper();
            rsLoad.OpenRecordset(strSQL, strDBName: "SystemSettings", boolAutoCreateCommands: false);
            cRestoreFileDetails fDetail;
            while (!rsLoad.EndOfFile())
            {
                //Application.DoEvents();
                fDetail = new cRestoreFileDetails();
                // TODO: Field [DatabaseName] not found!! (maybe it is an alias?)
                fDetail.DatabaseName = FCConvert.ToString(rsLoad.Get_Fields("DatabaseName"));
                fDetail.FileNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_String("position"))));
                // TODO: Field [BackupStartDate] not found!! (maybe it is an alias?)
                fDetail.BackupDate = FCConvert.ToString(rsLoad.Get_Fields("BackupStartDate"));
                // TODO: Field [BackupType] not found!! (maybe it is an alias?)
                switch (FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields("BackupType"))))
                {
                    case 1:
                        {
                            fDetail.BackupType = "Full";
                            break;
                        }
                    default:
                        {
                            fDetail.BackupType = "Incremental";
                            break;
                        }
                }
                //end switch
                // TODO: Field [BackupName] not found!! (maybe it is an alias?)
                fDetail.Description = FCConvert.ToString(rsLoad.Get_Fields("BackupName"));
                tColl.AddItem(fDetail);
                rsLoad.MoveNext();
            }
            GetBackupHeadersFromFile = tColl;
            return GetBackupHeadersFromFile;
        }

        private string GetRestoreScript(ref cRestoreArgs restInfo)
        {
            string GetRestoreScript = "";
            string strDataPath;
            string strReturn;
            string strRestorePath;
            string strDestinationDatabase;
            string strSourceDatabase;
            string strLogPath;
            clsDRWrapper rsLoad = new clsDRWrapper();
            rsLoad.OpenRecordset("select datapath = serverproperty('InstanceDefaultDataPath') ", "SystemSettings");
            // TODO: Field [datapath] not found!! (maybe it is an alias?)
            strDataPath = FCConvert.ToString(rsLoad.Get_Fields("datapath"));
            rsLoad.OpenRecordset("select logpath = serverproperty('InstanceDefaultLogPath')", "SystemSettings");
            // TODO: Field [logpath] not found!! (maybe it is an alias?)
            strLogPath = FCConvert.ToString(rsLoad.Get_Fields("logpath"));
            strDestinationDatabase = restInfo.DatabaseInfo.DestinationDatabase;
            strSourceDatabase = restInfo.DatabaseInfo.DatabaseName;
            strRestorePath = restInfo.DatabaseInfo.RestorePath;
            if (Strings.Right(strDataPath, 1) != "/" && Strings.Right(strDataPath, 1) != "\\")
            {
                strDataPath += "\\";
            }
            if (Strings.Right(strLogPath, 1) != "/" && Strings.Right(strLogPath, 1) != "\\")
            {
                strLogPath += "\\";
            }
            strReturn = "RESTORE database [" + strDestinationDatabase + "] FROM DISK = N'" + strRestorePath + "' WITH FILE = " + restInfo.FileNumber + ", MOVE N'" + strSourceDatabase + "' TO N'" + strDataPath + strDestinationDatabase + ".mdf'";
            strReturn += ", MOVE N'" + strSourceDatabase + "_log' to N'" + strLogPath + strDestinationDatabase + "_log.ldf', REPLACE, STATS=10";
            GetRestoreScript = strReturn;
            return GetRestoreScript;
        }

    }
}
