﻿//Fecher vbPorter - Version 1.0.0.32
namespace Global
{
	public class cDatabaseInfo
	{
		//=========================================================
		private string strDescription = string.Empty;
		private string strDatabaseName = string.Empty;
		private string strDataSource = string.Empty;
		private string strDestinationDatabase = string.Empty;
		private string strRestorePath = string.Empty;

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string DatabaseName
		{
			set
			{
				strDatabaseName = value;
			}
			get
			{
				string DatabaseName = "";
				DatabaseName = strDatabaseName;
				return DatabaseName;
			}
		}

		public string DataSource
		{
			set
			{
				strDataSource = value;
			}
			get
			{
				string DataSource = "";
				DataSource = strDataSource;
				return DataSource;
			}
		}

		public string DestinationDatabase
		{
			set
			{
				strDestinationDatabase = value;
			}
			get
			{
				string DestinationDatabase = "";
				DestinationDatabase = strDestinationDatabase;
				return DestinationDatabase;
			}
		}

		public string RestorePath
		{
			set
			{
				strRestorePath = value;
			}
			get
			{
				string RestorePath = "";
				RestorePath = strRestorePath;
				return RestorePath;
			}
		}
	}
}
