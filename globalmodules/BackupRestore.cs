﻿//Imports SQLDBUtil
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Data;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Data.SqlClient;
using fecherFoundation;

namespace Global
{
	public class BackupCompletedArgs : EventArgs
	{
		private bool _Success = false;

		public bool Success
		{
			get
			{
				return _Success;
			}
			set
			{
				_Success = value;
			}
		}

		private string _ErrorMessage = "";

		public string ErrorMessage
		{
			get
			{
				return _ErrorMessage;
			}
			set
			{
				_ErrorMessage = value;
			}
		}
	}

	public class BackupPercentageCompletedArgs : EventArgs
	{
		private int _PercentComplete = 0;

		public int PercentComplete
		{
			get
			{
				return _PercentComplete;
			}
			set
			{
				_PercentComplete = value;
			}
		}
	}

	public class RestoreCompletedArgs : EventArgs
	{
		private bool _Success = false;

		public bool Success
		{
			get
			{
				return _Success;
			}
			set
			{
				_Success = value;
			}
		}

		private string _ErrorMessage = "";

		public string ErrorMessage
		{
			get
			{
				return _ErrorMessage;
			}
			set
			{
				_ErrorMessage = value;
			}
		}
	}

	public class RestorePercentageCompletedArgs : EventArgs
	{
		private int _PercentComplete = 0;

		public int PercentComplete
		{
			get
			{
				return _PercentComplete;
			}
			set
			{
				_PercentComplete = value;
			}
		}
	}

	public class BackupRestore
	{
		public delegate void BackupCompletedEventHandler(object sender, BackupCompletedArgs e);

		public delegate void RestoreCompletedEventHandler(object sender, RestoreCompletedArgs e);

		IBackupAndRestoreService tServ;

		public event BackupCompletedEventHandler BackupCompleted;
		public event EventHandler BackupPercentageCompleted;
		public event RestoreCompletedEventHandler RestoreCompleted;
		public event EventHandler RestorePercentageCompleted;

		public void Backup(string strConnectionString, string strBackupFullFileName = "", bool boolCopyOnly = false)
		{
			SqlConnectionStringBuilder tbld = new SqlConnectionStringBuilder(strConnectionString);
			string strError = "";
			dbConnInfo tInfo = new dbConnInfo();
			tInfo.ConnectionString = strConnectionString;
			tInfo.Items["DataSource"] = tbld.DataSource;
			tInfo.Items["InitialCatalog"] = tbld.InitialCatalog;
			tInfo.Items["UserName"] = tbld.UserID;
			tInfo.Items["Password"] = tbld.Password;
			if (tServ != null)
			{
				tServ.Backup(tInfo, ref strError, false, strBackupFullFileName, boolCopyOnly);
			}
			else
			{
				//TODO:JEI
				////MsgBox("Backup Service could not be created", MsgBoxStyle.Critical, "Error");
			}
		}

		public RestoreFileDetails[] GetBackupHeadersFromFile(string strConnectionString, string strBackupPath)
		{
			return tServ.GetBackupHeadersFromFile(strConnectionString, strBackupPath).ToArray();
		}

		public RestoreFileDetails[] GetBackupHeaders(string strConnectionString)
		{
			return tServ.GetBackupHeaders(strConnectionString).ToArray();
		}

		public void Restore(string strConnectionString, string strRestoreAsDBName, string strBackupPath, string strSourceDatabaseName = "", int intFileNumber = -1)
		{

			SqlConnectionStringBuilder tbld = new SqlConnectionStringBuilder(strConnectionString);
			string strError = "";
			DatabaseInfo tInfo = new DatabaseInfo();
			//FC:FINAl:SBE - fix issue from original application
			//if (strSourceDatabaseName != "")
			if (strSourceDatabaseName == "")
			{
				tInfo.DatabaseName = strRestoreAsDBName;
				tInfo.DestinationDatabase = strRestoreAsDBName;
			}
			else
			{
				tInfo.DatabaseName = strSourceDatabaseName;
				tInfo.DestinationDatabase = strRestoreAsDBName;
			}
			//tInfo.DestinationDatabase = strRestoreAsDBName
			tInfo.DataSource = tbld.DataSource;
			tInfo.Password = tbld.Password;
			tInfo.User = tbld.UserID;
			tInfo.RestorePath = strBackupPath;
			tServ.Restore(tInfo, ref strError, intFileNumber);
		}

		public string GetBackupPath(string strConnectionString)
		{
			return tServ.GetBackupPath(strConnectionString);
		}

		public string GetDataPath(string strConnectionString)
		{
			return tServ.GetDataPath(strConnectionString);
		}

		public List<DBFileInfo> GetListOfTRIODatabases(string strMegaGroup, string strGroup, string strDatabase)
		{
			List<DBFileInfo> tReturn = new List<DBFileInfo>();
			try
			{
				string strSQL = "";
				strSQL = "select * from master.dbo.sysdatabases order by name";

				ConnectionPool tPool = new ConnectionPool();
				string strName;
				System.Data.Common.DbCommand tCmd = null;
				System.Data.Common.DbDataReader tRdr = null;
				ConnectionInfo tInfo = null;
				string[] strAry;
				char[] strSep = {
					'_'
				};
				string strLoadedGroup = "";
				string strLoadedMegaGroup = "";
				string strLoadedDB = "";
				string strUnderScore = "";
				bool boolMatch = false;
				DBFileInfo tDBInfo = null;
				tInfo = tPool.GetConnectionInformation("SystemSettings", "", "");
				if (tInfo != null)
				{
					SqlConnectionStringBuilder tSB = new SqlConnectionStringBuilder(tInfo.GetConnectionString());
					tSB.InitialCatalog = "master";
					SqlConnection tcon = new SqlConnection(tSB.ConnectionString);
					tCmd = tcon.CreateCommand();
					tCmd.CommandText = strSQL;
					tcon.Open();
					tRdr = tCmd.ExecuteReader();
					while (tRdr.Read())
					{
						strName = tRdr["Name"].ToString();
						strAry = strName.Split(strSep);
						if (strAry.Length > 3)
						{
							if (strAry[0].ToLower() == "trio")
							{
								boolMatch = true;
								strLoadedDB = strAry[strAry.Length - 1];
								strLoadedMegaGroup = strAry[1];
								strUnderScore = "";
								int x;
								for (x = 2; x <= strAry.Length - 2; x++)
								{
									strLoadedGroup = strUnderScore + strAry[x];
									strUnderScore = "_";
								}
								if (strDatabase != "")
								{
									if (strDatabase.ToLower() != strLoadedDB.ToLower())
									{
										boolMatch = false;
									}
								}
								if (strMegaGroup != "")
								{
									if (strLoadedMegaGroup.ToLower() != strMegaGroup.ToLower())
									{
										boolMatch = false;
									}
								}
								if (strGroup != "")
								{
									if (strGroup.ToLower() != strLoadedGroup.ToLower() & strGroup.ToLower() != strAry[2].ToLower())
									{
										boolMatch = false;
									}
								}
								if (boolMatch)
								{
									tDBInfo = new DBFileInfo();
									tDBInfo.DatabaseName = tRdr["name"].ToString();
									tDBInfo.dbid = FCConvert.ToInt32(tRdr["dbid"]);
									tDBInfo.FileName = tRdr["filename"].ToString();
									tReturn.Add(tDBInfo);
								}
							}
						}
					}
					tRdr.Close();
					tcon.Close();
					tRdr.Dispose();
					tCmd.Dispose();
					tcon.Dispose();
				}
			}
			catch (Exception ex)
			{
			}
			return tReturn;
		}

		public DBFileInfo[] GetArrayOfTRIODatabases(string strMegaGroup, string strGroup, string strDatabase)
		{
			DBFileInfo[] tReturn = {

			};
			try
			{
				tReturn = GetListOfTRIODatabases(strMegaGroup, strGroup, strDatabase).ToArray();
			}
			catch (Exception ex)
			{
			}
			return tReturn;
		}

		private void tServ_BackupCompleted(object sender, BackupCompleteArgs tArgs)
		{
			BackupCompletedArgs tArg = new BackupCompletedArgs();
			tArg.ErrorMessage = tArgs.ErrorMessage;
			tArg.Success = tArgs.Success;
			if (BackupCompleted != null)
			{
				BackupCompleted(this, tArg);
			}
		}

		private void tServ_BackupPercentageComplete(object sender, BackupPercentageArgs tArgs)
		{
			BackupPercentageCompletedArgs tArg = new BackupPercentageCompletedArgs();
			tArg.PercentComplete = tArgs.PercentComplete;
			if (BackupPercentageCompleted != null)
			{
				BackupPercentageCompleted(this, tArg);
			}
		}

		private void tServ_RestoreCompleted(object sender, RestoreCompleteArgs tArgs)
		{
			RestoreCompletedArgs tArg = new RestoreCompletedArgs();
			tArg.ErrorMessage = tArgs.ErrorMessage;
			tArg.Success = tArgs.Success;
			if (RestoreCompleted != null)
			{
				RestoreCompleted(this, tArg);
			}
		}

		private void tServ_RestorePercentageComplete(object sender, RestorePercentageArgs tArgs)
		{
			RestorePercentageCompletedArgs tArg = new RestorePercentageCompletedArgs();
			tArg.PercentComplete = tArgs.PercentComplete;
			if (RestorePercentageCompleted != null)
			{
				RestorePercentageCompleted(this, tArg);
			}
		}

		public BackupRestore()
		{
			InteropServiceLocator tSLoc = new InteropServiceLocator();
			tServ = tSLoc.GetService<IBackupAndRestoreService>();
			if (tServ == null)
			{
				tServ = new BackupAndRestoreService();
			}
			if (tServ != null)
			{
				tServ.BackupCompleted += tServ_BackupCompleted;
				tServ.BackupPercentageComplete += tServ_BackupPercentageComplete;
				tServ.RestoreCompleted += tServ_RestoreCompleted;
				tServ.RestorePercentageComplete += tServ_RestorePercentageComplete;
			}
		}
	}

	public class DBFileInfo
	{
		private string _DatabaseName = "";

		public string DatabaseName
		{
			get
			{
				return _DatabaseName;
			}
			set
			{
				_DatabaseName = value;
			}
		}

		private string _FileName = "";

		public string FileName
		{
			get
			{
				return _FileName;
			}
			set
			{
				_FileName = value;
			}
		}

		public int dbid
		{
			get
			{
				return m_dbid;
			}
			set
			{
				m_dbid = value;
			}
		}

		private int m_dbid;
	}

	public class RestoreFileDetails
	{
		private int _FileNumber;

		public int FileNumber
		{
			get
			{
				return _FileNumber;
			}
			set
			{
				_FileNumber = value;
			}
		}

		private string _BackupType = string.Empty;

		public string BackupType
		{
			get
			{
				return _BackupType;
			}
			set
			{
				_BackupType = value;
			}
		}

		private string _Description = string.Empty;

		public string Description
		{
			get
			{
				return _Description;
			}
			set
			{
				_Description = value;
			}
		}

		private DateTime _BackupDate;

		public DateTime BackupDate
		{
			get
			{
				return _BackupDate;
			}
			set
			{
				_BackupDate = value;
			}
		}
	}
}
