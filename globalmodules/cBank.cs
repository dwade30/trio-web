﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Microsoft.VisualBasic;

namespace Global
{
	public class cBank
	{
		//=========================================================
		private int lngRecordID;
		private int lngBankNumber;
		private string strBankName = string.Empty;
		private double dblBalance;
		private bool boolCurrentBank;
		private string strStatementDate = "";
		private string strFund = string.Empty;
		private string strCashAccount = string.Empty;
		private string strTownCashAccount = string.Empty;
		private string strRoutingNumber = string.Empty;
		private string strBankAccountNumber = string.Empty;
		private string strAccountType = string.Empty;
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int BankNumber
		{
			set
			{
				lngBankNumber = value;
				IsUpdated = true;
			}
			get
			{
				int BankNumber = 0;
				BankNumber = lngBankNumber;
				return BankNumber;
			}
		}

		public string Name
		{
			set
			{
				strBankName = value;
				IsUpdated = true;
			}
			get
			{
				string Name = "";
				Name = strBankName;
				return Name;
			}
		}

		public double Balance
		{
			set
			{
				dblBalance = value;
				IsUpdated = true;
			}
			get
			{
				double Balance = 0;
				Balance = dblBalance;
				return Balance;
			}
		}

		public bool IsCurrentBank
		{
			set
			{
				boolCurrentBank = value;
				IsUpdated = true;
			}
			get
			{
				bool IsCurrentBank = false;
				IsCurrentBank = boolCurrentBank;
				return IsCurrentBank;
			}
		}

		public string StatementDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strStatementDate = value;
				}
				else
				{
					strStatementDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string StatementDate = "";
				StatementDate = strStatementDate;
				return StatementDate;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
				IsUpdated = true;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}

		public string CashAccount
		{
			set
			{
				strCashAccount = value;
				IsUpdated = true;
			}
			get
			{
				string CashAccount = "";
				CashAccount = strCashAccount;
				return CashAccount;
			}
		}

		public string TownCashAccount
		{
			set
			{
				strTownCashAccount = value;
				IsUpdated = true;
			}
			get
			{
				string TownCashAccount = "";
				TownCashAccount = strTownCashAccount;
				return TownCashAccount;
			}
		}

		public string RoutingNumber
		{
			set
			{
				strRoutingNumber = value;
				IsUpdated = true;
			}
			get
			{
				string RoutingNumber = "";
				RoutingNumber = strRoutingNumber;
				return RoutingNumber;
			}
		}

		public string BankAccountNumber
		{
			set
			{
				strBankAccountNumber = value;
				IsUpdated = true;
			}
			get
			{
				string BankAccountNumber = "";
				BankAccountNumber = strBankAccountNumber;
				return BankAccountNumber;
			}
		}

		public string AccountType
		{
			set
			{
				strAccountType = value;
				IsUpdated = true;
			}
			get
			{
				string AccountType = "";
				AccountType = strAccountType;
				return AccountType;
			}
		}
	}
}
