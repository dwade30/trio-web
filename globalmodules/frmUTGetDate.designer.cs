//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;
using Global;

namespace TWUT0000
{
    /// <summary>
    /// Summary description for frmUTGetDate.
    /// </summary>
    partial class frmUTGetDate : BaseForm
    {
        public Global.T2KDateBox txtDate;
        public fecherFoundation.FCButton cmdOk;
        public fecherFoundation.FCLabel lblInstructions;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
                _InstancePtr = null;
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUTGetDate));
			this.txtDate = new Global.T2KDateBox();
			this.cmdOk = new fecherFoundation.FCButton();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdOk);
			this.BottomPanel.Location = new System.Drawing.Point(0, 220);
			this.BottomPanel.Size = new System.Drawing.Size(403, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtDate);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Size = new System.Drawing.Size(403, 160);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(403, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(157, 30);
            //FC:FINAL:CHN - issue #1139: remove text and allow only numeric values for date
            this.HeaderText.Text = ""; // "Date Change";
			// 
			// txtDate
			// 
			this.txtDate.Location = new System.Drawing.Point(30, 94);
            //FC:FINAL:CHN - issue #1139: remove text and allow only numeric values for date
            this.txtDate.Mask = "00/00/0000"; // "##/##/####";
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(115, 40);
			this.txtDate.TabIndex = 1;
			this.txtDate.Text = "  /  /";
			this.txtDate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtDate_KeyDownEvent);
			this.txtDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtDate_Validate);
			// 
			// cmdOk
			// 
			this.cmdOk.AppearanceKey = "acceptButton";
			this.cmdOk.Location = new System.Drawing.Point(151, 30);
			this.cmdOk.Name = "cmdOk";
			this.cmdOk.Size = new System.Drawing.Size(90, 48);
            this.cmdOk.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdOk.TabIndex = 2;
			this.cmdOk.Text = "Process";
			this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 44);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(318, 30);
			this.lblInstructions.TabIndex = 0;
			this.lblInstructions.Text = "PLEASE ENTER DATE TO USE FOR PROCESSING THIS TRANSACTION";
			// 
			// frmUTGetDate
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(403, 328);
			this.FillColor = 0;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.FixedToolWindow;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmUTGetDate";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Date Change";
			this.Load += new System.EventHandler(this.frmUTGetDate_Load);
			this.Activated += new System.EventHandler(this.frmUTGetDate_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUTGetDate_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUTGetDate_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
			this.ResumeLayout(false);

        }
        #endregion
    }
}