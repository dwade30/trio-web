﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWAR0000
using TWAR0000;


#elif TWBD0000
using TWBD0000;


#elif TWCR0000
using TWCR0000;


#elif TWFA0000
using TWFA0000;


#elif TWBL0000
using TWBL0000;
using modGlobal = TWBL0000.modMDIParent;


#elif TWGNENTY
using TWGNENTY;
using modGlobal = TWGNENTY.modGlobalRoutines;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;


#elif TWMV0000
using modGlobal = TWMV0000.modGlobalRoutines;


#elif TWPY0000
using modGlobal = TWPY0000.modGlobalRoutines;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmSelectPostingJournal.
	/// </summary>
	public partial class frmSelectPostingJournal : BaseForm
	{
		public frmSelectPostingJournal()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSelectPostingJournal InstancePtr
		{
			get
			{
				return (frmSelectPostingJournal)Sys.GetInstance(typeof(frmSelectPostingJournal));
			}
		}

		protected frmSelectPostingJournal _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		public string strReportType = "";

		private void CancelButton_Click(object sender, System.EventArgs e)
		{
			modBudgetaryAccounting.Statics.lngSelectedJournal = -1;
			Close();
		}

		private void cboJournals_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ToolTip1.SetToolTip(cboJournals, cboJournals.Text);
		}

		private void frmSelectPostingJournal_Activated(object sender, System.EventArgs e)
		{
			clsDRWrapper rsJournals = new clsDRWrapper();
			if (modGlobal.FormExist(this))
			{
				return;
			}
			rsJournals.OpenRecordset("SELECT DISTINCT JournalNumber, Description FROM JournalMaster WHERE Status <> 'P' AND Status <> 'D' ORDER BY JournalNumber", "TWBD0000.vb1");
			cboJournals.Clear();
			if (rsJournals.EndOfFile() != true && rsJournals.BeginningOfFile() != true)
			{
				do
				{
					// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					cboJournals.AddItem(modValidateAccount.GetFormat_6(rsJournals.Get_Fields("JournalNumber"), 4) + " " + rsJournals.Get_Fields_String("Description"));
					rsJournals.MoveNext();
				}
				while (rsJournals.EndOfFile() != true);
				cboJournals.SelectedIndex = 0;
				ToolTip1.SetToolTip(cboJournals, cboJournals.Text);
			}
			cboJournals.Focus();
		}

		private void frmSelectPostingJournal_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSelectPostingJournal.ScaleWidth	= 3915;
			//frmSelectPostingJournal.ScaleHeight	= 2310;
			//frmSelectPostingJournal.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void OKButton_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsJournalCheck = new clsDRWrapper();
			if (cboJournals.SelectedIndex != -1)
			{
				modBudgetaryAccounting.Statics.lngSelectedJournal = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboJournals.Text, 4))));
				Close();
				return;
			}
			else
			{
				MessageBox.Show("You must enter a journal number before you may proceed.", "No Journal Entered", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
		}
	}
}
