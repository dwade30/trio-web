﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using TWSharedLibrary;
#if TWBL0000
using TWBL0000;


#elif TWRE0000
using TWRE0000;
using modMain = TWRE0000.modGlobalRoutines;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for rptTaxRateForm.
	/// </summary>
	public partial class rptTaxRateForm : BaseSectionReport
	{
		public rptTaxRateForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private SharedApplication.RealEstate.MunicipalValuationReturn valuationReturn;
		public rptTaxRateForm(SharedApplication.RealEstate.MunicipalValuationReturn valuationReturn) : this()
		{
			this.valuationReturn = valuationReturn;
		}


		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Tax Rate Calculation Form";
		}

		public static rptTaxRateForm InstancePtr
		{
			get
			{
				return (rptTaxRateForm)Sys.GetInstance(typeof(rptTaxRateForm));
			}
		}

		protected rptTaxRateForm _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTaxRateForm	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool boolFromDatabase;
		const int CNSTGRIDROWREVAL = 0;
		const int CNSTGRIDROWPPVAL = 1;
		const int CNSTGRIDROWTOTTAXVAL = 2;
		const int CNSTGRIDROWTOTHOMESTEAD = 3;
		const int CNSTGRIDROWHALFTOTHOMESTEAD = 4;
		const int CNSTGRIDROWTOTBETE = 5;
		const int CNSTGRIDROWBETEReimValue = 6;
		const int CNSTGRIDROWTOTVALUATIONBASE = 7;
		/// <summary>
		/// Private Const CNSTGRIDROWFISCALYEAR = 7
		/// </summary>
		const int CNSTGRIDROWCOUNTYTAX = 8;
		const int CNSTGRIDROWMUNIAPPROPRIATION = 9;
		const int CNSTGRIDROWTIF = 10;
		const int CNSTGRIDROWEDUCATION = 11;
		const int CNSTGRIDROWTOTAPPROPRIATIONS = 12;
		const int CNSTGRIDROWSTATEREVENUESHARING = 13;
		const int CNSTGRIDROWOTHERREVENUE = 14;
		const int CNSTGRIDROWTOTALDEDUCTIONS = 15;
		const int CNSTGRIDROWNETRAISED = 16;
		private double dblHsteadReimburseRate;

		public void Init(bool boolUseDatabase = false, int lngTown = 0)
		{
			boolFromDatabase = boolUseDatabase;
			if (lngTown > 0)
			{
				#if ModuleID_1
								                modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = lngTown;
                txtMunicipality.Visible = true;
                int lngTownNumber = modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber;
                txtMunicipality.Text = modRegionalTown.GetTownKeyName(ref lngTownNumber);
#else
				modGlobalVariables.Statics.CustomizedInfo.intCurrentTown = lngTown;
				txtMunicipality.Visible = true;
				txtMunicipality.Text = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
				#endif
			}
			else
			{
				txtMunicipality.Text = modGlobalConstants.Statics.MuniName;
			}
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			dblHsteadReimburseRate = 0.7;
			if (FCConvert.ToString(this.UserData) == "MVR")
			{
				boolFromDatabase = true;
				this.ReportHeader.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
				#if ModuleID_1
								                if (modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber > 0)
                {
                    txtMunicipality.Visible = true;
                    int lngTownNumber = modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber;
                    txtMunicipality.Text = modRegionalTown.GetTownKeyName(ref lngTownNumber);
                }
                else
                {
                    txtMunicipality.Text = modGlobalConstants.Statics.MuniName;
                }


#else
				if (modGlobalVariables.Statics.CustomizedInfo.intCurrentTown > 0)
				{
					txtMunicipality.Visible = true;
					txtMunicipality.Text = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
				}
				else
				{
					txtMunicipality.Text = modGlobalConstants.Statics.MuniName;
				}
				#endif
			}
			lbl5btext.Visible = true;
			#if ModuleID_1
						            clsDRWrapper rsLoad = new clsDRWrapper();
            int intYearToShow = 0;

            rsLoad.OpenRecordset("select * from taxratecalculation where townnumber  = " + FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber)), modGlobalVariables.strREDatabase);
            if (!boolFromDatabase)
            {
                intYearToShow = DateTime.Today.Year;
                if (FCConvert.ToString(this.UserData) != "MVR")
                {
                    if (Information.IsDate(frmTaxRateCalc.InstancePtr.t2kBegin.Text))
                    {
                        intYearToShow = FCConvert.ToDateTime(frmTaxRateCalc.InstancePtr.t2kBegin.Text).Year;
                    }
                }

                if (frmTaxRateCalc.InstancePtr.chkUseEB.CheckState == Wisej.Web.CheckState.Checked)
                {
                    txtTitle.Text = "MAINE REVENUE SERVICES - " + FCConvert.ToString(intYearToShow) + " ENHANCED BETE MUNICIPAL TAX RATE CALCULATION FORM";
                    Field163.Text = "Enhanced Total of all reimbursable BETE Exempt Valuation";
                    Field132.Text = "5.  Total of all BETE exempt valuation";
                    lbl5btext.Visible = false;
                    Field167.Visible = false;
                    Field168.Visible = false;
                }
                else
                {
                    txtTitle.Text = FCConvert.ToString(intYearToShow) + " MUNICIPAL TAX RATE CALCULATION STANDARD FORM";
                    Field163.Text = "(b) BETE exemption reimbursement value";
                    Field132.Text = "5. Total exempt value of all BETE qualified property";
                    Field167.Visible = true;
                    Field168.Visible = true;
                }
            }
            else
            {
                if (!rsLoad.EndOfFile())
                {
					//  THESE CAN'T REFER TO THE GRID ON THE OLD FORM
                    if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("useEnhancedbete")))
                    {
                        txtTitle.Text = valuationReturn.ReportYear + " ENHANCED MUNICIPAL TAX RATE CALCULATION FORM";
                        Field163.Text = "Enhanced Total of all reimbursable BETE Exempt Valuation";
                        Field132.Text = "5.  Total of all BETE exempt valuation";
                        lbl5btext.Visible = false;
                        Field167.Visible = false;
                        Field168.Visible = false;
                    }
                    else
                    {
                        txtTitle.Text = valuationReturn.ReportYear + " MUNICIPAL TAX RATE CALCULATION STANDARD FORM";
                        // Field163.Text = "(b) The statutory standard reimbursement for " & frmMVRViewer.Grid.TextMatrix(CNSTMVRROWYEAR, 0) & " is 50%"
                        Field163.Text = "(b) BETE exemption reimbursement value";
                        Field132.Text = "5. Total exempt value of all BETE qualified property";
                        Field167.Visible = true;
                        Field168.Visible = true;
                    }
                }
                else
                {
                    txtTitle.Text = valuationReturn.ReportYear + " MUNICIPAL TAX RATE CALCULATION STANDARD FORM";
                    Field163.Text = "(b) BETE exemption reimbursement value";
                    Field132.Text = "5. Total exempt value of all BETE qualified property";
                    Field167.Visible = true;
                    Field168.Visible = true;
                }
            }

#else
			if (frmTaxRateCalc.InstancePtr.chkUseEB.CheckState == Wisej.Web.CheckState.Checked)
			{
				txtTitle.Text = FCConvert.ToString(DateTime.Today.Year) + " ENHANCED BETE MUNICIPAL TAX RATE CALCULATION FORM";
				Field163.Text = "Enhanced Total of all reimbursable BETE Exempt Valuation";
				lbl5btext.Visible = false;
				Field167.Visible = false;
				Field168.Visible = false;
			}
			else
			{
				txtTitle.Text = FCConvert.ToString(DateTime.Today.Year) + " MUNICIPAL TAX RATE CALCULATION STANDARD FORM";
				Field163.Text = "(b) BETE exemption reimbursement value";
				Field132.Text = "5. Total exempt value of all BETE qualified property";
				Field167.Visible = true;
				Field168.Visible = true;
			}
			#endif
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);

		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strTemp = "";
			double dblBETEReimRate = 0;
			if (!boolFromDatabase)
			{
				txtRealEstate.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWREVAL, 2);
				txtPersonalProperty.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWPPVAL, 2);
				txtTotalTaxable.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWTOTTAXVAL, 2);
				txtBETEValuation.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWTOTBETE, 2);
				txtTotalHomestead.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWTOTHOMESTEAD, 2);
				txtHalfHomestead.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWHALFTOTHOMESTEAD, 2);
				txtTotalBase.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWTOTVALUATIONBASE, 2);
				txtCountyTax.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWCOUNTYTAX, 2);
				txtMunicipalAppropriation.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWMUNIAPPROPRIATION, 2);
				txtTIF.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWTIF, 2);
				txtSchool.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWEDUCATION, 2);
				txtTotalAppropriations.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWTOTAPPROPRIATIONS, 2);
				txtRevenueSharing.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWSTATEREVENUESHARING, 2);
				txtOtherRevenue.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWOTHERREVENUE, 2);
				txtTotalDeductions.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWTOTALDEDUCTIONS, 2);
				txtNetToBeRaised.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWNETRAISED, 2);
				txtNet1.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWNETRAISED, 2);
				txtNet2.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWNETRAISED, 2);
				txtNet3.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWNETRAISED, 2);
				txtNet4.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWNETRAISED, 2);
				txtBase1.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWTOTVALUATIONBASE, 2);
				txtBase2.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWTOTVALUATIONBASE, 2);
				txtTotTaxable.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWTOTTAXVAL, 2);
				txtHomesteadVal.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWHALFTOTHOMESTEAD, 2);
				txtBETEVal.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWBETEReimValue, 2);
				txtBETEReimburseValue.Text = frmTaxRateCalc.InstancePtr.GridForm1.TextMatrix(CNSTGRIDROWBETEReimValue, 2);
				txtMaxTax.Text = frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(0, 3);
				txtMaxTax2.Text = frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(0, 3);
				txtMinRate.Text = frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(1, 3);
				txtMaxRate.Text = frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(2, 3);
				txtTaxRate.Text = frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(3, 3);
				txtTaxRate2.Text = frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(3, 3);
				txtTaxRate3.Text = frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(3, 3);
				txtTaxCommitment.Text = frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(4, 3);
				txtHomestead.Text = frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(6, 3);
				txtBETE.Text = frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(7, 3);
				txtMaxOverlay.Text = frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(5, 3);
				txtOverlay.Text = frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(8, 3);
				txt19Plus21.Text = Strings.Format(FCConvert.ToDouble(frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(4, 3)) + FCConvert.ToDouble(frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(6, 3)) + FCConvert.ToDouble(frmTaxRateCalc.InstancePtr.GridForm2.TextMatrix(7, 3)), "#,###,###,###,##0.00");
				dblBETEReimRate = frmTaxRateCalc.InstancePtr.BETEReimbursementRate;
			}
			else
			{
				clsDRWrapper clsLoad = new clsDRWrapper();
				double dblTotalAppropriations = 0;
				double dblNetToBeRaised = 0;
				double dblTotalRevenue = 0;
				// vbPorter upgrade warning: dblMaxOverLay As double	OnWrite(string)
				double dblMaxOverLay = 0;
				double lngPPValuation = 0;
				double lngREValuation = 0;
				double lngHomestead = 0;
				// vbPorter upgrade warning: lngHalfHomestead As double	OnWrite(string)
				double lngHalfHomestead = 0;
				double dblTotVal = 0;
				double dblMaxTax = 0;
				double dblTotTaxed = 0;
				double dblTaxRate = 0;
				double dblHomesteadReim = 0;
				string strSQLWhere = "";
				double dblBETEReim = 0;
				double lngBETEExempt = 0;
				double lngBETEReimValue = 0;
				string strSQL = "";
				#if ModuleID_1
								                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    strSQLWhere = "";
                    strSQL = "select * from customize";
                }
                else
                {
                    strSQLWhere = " and isnull(ritrancode,0) = " + modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber;
                    strSQL = "select * from townspecific where townnumber = " + modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber;
                }

#else
				if (!modRegionalTown.IsRegionalTown())
				{
					strSQLWhere = "";
					strSQL = "select * from customize";
				}
				else
				{
					strSQLWhere = " and isnull(ritrancode,0) = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
					strSQL = "select * from townspecific where townnumber = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
				}
				#endif
				clsLoad.OpenRecordset(strSQL, modGlobalVariables.strPPDatabase);
				if (!clsLoad.EndOfFile())
				{
					dblBETEReimRate = Conversion.Val(clsLoad.Get_Fields_Double("betereimburserate"));
				}
				else
				{
					dblBETEReimRate = 0;
				}
				clsLoad.OpenRecordset("select sum(convert(decimal,LASTLANDVAL)) as landsum,sum(convert(decimal,LASTbldgVAL)) as bldgsum,sum(convert(decimal,rlexemption)) as exemptsum from master where rsaccount > 0 and not rsdeleted = 1 " + strSQLWhere, modGlobalVariables.strREDatabase);				
				lngREValuation = Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")) + Conversion.Val(clsLoad.Get_Fields("landsum"));
				clsLoad.OpenRecordset("select sum(convert(decimal,homesteadvalue)) as homesteadsum from master where rsaccount > 0 and not rsdeleted = 1 and rscard = 1 " + strSQLWhere, modGlobalVariables.strREDatabase);
				// TODO: Field [homesteadsum] not found!! (maybe it is an alias?)
				lngHomestead = Conversion.Val(clsLoad.Get_Fields("homesteadsum"));
				lngHalfHomestead = FCConvert.ToDouble(Strings.Format(lngHomestead * dblHsteadReimburseRate, "0"));
				#if ModuleID_1
								                clsLoad.OpenRecordset("select * from taxratecalculation where isnull(townnumber,0) = " + FCConvert.ToString(Conversion.Val(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber)), modGlobalVariables.strREDatabase);

#else
				clsLoad.OpenRecordset("select * from taxratecalculation where isnull(townnumber,0) = " + FCConvert.ToString(FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown)), modGlobalVariables.strREDatabase);
				#endif
				dblTaxRate = Conversion.Val(clsLoad.Get_Fields_Double("taxrate"));
				lngPPValuation = Conversion.Val(clsLoad.Get_Fields_Int32("personalpropertyVALUATION"));
				lngBETEExempt = Conversion.Val(clsLoad.Get_Fields_Int32("beteexempt"));
				lngBETEReimValue = Conversion.Val(clsLoad.Get_Fields_Double("betereimbursevalue"));
				txtBETEValuation.Text = Strings.Format(lngBETEExempt, "#,###,###,##0");
				strTemp = Strings.Format(clsLoad.Get_Fields_DateTime("fiscalstartdate"), "MM/dd/yy") + " to " + Strings.Format(clsLoad.Get_Fields_DateTime("fiscalenddate"), "MM/dd/yy");
				// txtDate.Text = strTemp
				txtCountyTax.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("countytax")), "#,###,###,##0.00");
				txtMunicipalAppropriation.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("municipalappropriation")), "#,###,###,##0.00");
				txtTIF.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("tif")), "#,###,###,##0.00");
				txtSchool.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("education")), "#,###,###,##0.00");
				txtRevenueSharing.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("statesharing")), "#,###,###,##0.00");
				txtOtherRevenue.Text = Strings.Format(Conversion.Val(clsLoad.Get_Fields_Double("other")), "#,###,###,##0.00");
				txtTaxRate.Text = Strings.Format(dblTaxRate, "0.000000");
				txtTaxRate2.Text = Strings.Format(dblTaxRate, "0.000000");
				txtTaxRate3.Text = Strings.Format(dblTaxRate, "0.000000");
				dblTotalAppropriations = Conversion.Val(clsLoad.Get_Fields_Double("countytax")) + Conversion.Val(clsLoad.Get_Fields_Double("municipalappropriation")) + Conversion.Val(clsLoad.Get_Fields_Double("tif")) + Conversion.Val(clsLoad.Get_Fields_Double("education"));
				txtTotalAppropriations.Text = Strings.Format(dblTotalAppropriations, "#,###,###,##0.00");
				dblTotalRevenue = Conversion.Val(clsLoad.Get_Fields_Double("Statesharing")) + Conversion.Val(clsLoad.Get_Fields_Double("other"));
				txtTotalDeductions.Text = Strings.Format(dblTotalRevenue, "#,###,###,##0.00");
				dblNetToBeRaised = dblTotalAppropriations - dblTotalRevenue;
				txtNetToBeRaised.Text = Strings.Format(dblNetToBeRaised, "#,###,###,##0.00");
				txtNet1.Text = txtNetToBeRaised.Text;
				txtNet2.Text = txtNetToBeRaised.Text;
				txtNet3.Text = txtNetToBeRaised.Text;
				txtNet4.Text = txtNetToBeRaised.Text;
				dblMaxOverLay = FCConvert.ToDouble(Strings.Format(modMain.Round(dblNetToBeRaised * 0.05, 2), "#,###,##0.00"));
				txtMaxOverlay.Text = Strings.Format(dblMaxOverLay, "#,###,##0.00");
				dblMaxTax = modMain.Round(dblMaxOverLay + dblNetToBeRaised, 2);
				txtMaxTax.Text = Strings.Format(dblMaxTax, "#,###,###,##0.00");
				txtMaxTax2.Text = txtMaxTax.Text;
				txtPersonalProperty.Text = Strings.Format(lngPPValuation, "#,###,###,##0");
				dblTotVal = lngREValuation + lngPPValuation + lngHalfHomestead + lngBETEReimValue;
				txtTotTaxable.Text = Strings.Format(lngREValuation + lngPPValuation, "#,###,###,##0");
				txtBase1.Text = Strings.Format(dblTotVal, "#,###,###,##0");
				txtBase2.Text = txtBase1.Text;
				txtHomesteadVal.Text = Strings.Format(lngHalfHomestead, "#,###,###,##0");
				txtBETEVal.Text = Strings.Format(lngBETEReimValue, "#,###,###,##0");
				txtMinRate.Text = Strings.Format(modMain.Round(dblNetToBeRaised / dblTotVal, 6), "0.000000");
				txtMaxRate.Text = Strings.Format(modMain.Round(dblMaxTax / dblTotVal, 6), "0.000000");
				dblTotTaxed = modMain.Round(dblTaxRate * (lngREValuation + lngPPValuation), 2);
				txtTaxCommitment.Text = Strings.Format(dblTotTaxed, "#,###,###,##0.00");
				dblHomesteadReim = modMain.Round(dblTaxRate * lngHalfHomestead, 2);
				txtBETEReimburseValue.Text = Strings.Format(lngBETEReimValue, "#,###,###,##0");
				dblBETEReim = modMain.Round(dblTaxRate * lngBETEReimValue, 2);
				txtHomestead.Text = Strings.Format(dblHomesteadReim, "#,###,###,##0.00");
				txtBETE.Text = Strings.Format(dblBETEReim, "#,###,###,##0.00");
				txtHalfHomestead.Text = Strings.Format(lngHalfHomestead, "#,###,###,##0");
				txtRealEstate.Text = Strings.Format(lngREValuation, "#,###,###,##0");
				txtTotalTaxable.Text = Strings.Format(lngREValuation + lngPPValuation, "#,###,###,##0");
				txtTotalHomestead.Text = Strings.Format(lngHomestead, "#,###,###,##0");
				txtTotalBase.Text = Strings.Format(dblTotVal, "#,###,###,##0");
				txtOverlay.Text = Strings.Format(dblTotTaxed + dblHomesteadReim + dblBETEReim - dblNetToBeRaised, "#,###,###,##0.00");
				txt19Plus21.Text = Strings.Format(dblTotTaxed + dblHomesteadReim + dblBETEReim, "#,###,###,##0.00");
				clsLoad.Dispose();
			}
			if (dblBETEReimRate > 0)
			{
				lbl5btext.Text = "(line 5(a) multiplied by " + Strings.Format(dblBETEReimRate, "0.0#") + ")";
			}
		}

		private void rptTaxRateForm_Load(object sender, System.EventArgs e)
		{
		}
	}
}
