﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using Wisej.Web;
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using System.IO;
#if TWCE0000
using TWCE0000;
#endif

namespace Global
{
	/// <summary>
	/// Summary description for frmGlobalComment.
	/// </summary>
	public partial class frmGlobalComment : BaseForm
	{
		public frmGlobalComment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGlobalComment InstancePtr
		{
			get
			{
				return (frmGlobalComment)Sys.GetInstance(typeof(frmGlobalComment));
			}
		}

		protected frmGlobalComment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		//=========================================================
		short intFrmSize;
		string strMod;
		string strTbl;
		string strFld;
		string strIDFld;
		int lngIDNumber;
		bool boolBlankComment;

		private void frmGlobalComment_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmGlobalComment_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGlobalComment properties;
			//frmGlobalComment.ScaleWidth	= 9045;
			//frmGlobalComment.ScaleHeight	= 7485;
			//frmGlobalComment.LinkTopic	= "Form1";
			//frmGlobalComment.LockControls	= -1  'True;
			//rtbText properties;
			//rtbText.ScrollBars	= 2;
			//rtbText.TextRTF	= $"frmGlobalComment.frx":058A;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, intFrmSize);
			modGlobalFunctions.SetTRIOColors(this);
		}

		public bool Init(string strModule, string strTable, string strField, string strKeyField, int lngID, string strFormCaption = "Comment", string strControlCaption = "", short intFormSize = modGlobalConstants.TRIOWINDOWSIZEBIGGIE, bool boolNonEditable = false, bool boolModal = false)
		{
			bool Init = false;
			// parameters:
			// strmodule   the two letter abbrev. for your module
			// strtable    the name of the table the comment is in
			// strfield    the name of the field the comment is in
			// strkeyfield the name of the autoid field or account field etc. used to identify correct record
			// lngid       the autoid,account etc. number
			// strformcaption   the caption of the form.  it is "Comment" by default
			// strcontrolcaption an optional explanation of the data in the control
			// intFormSize the size of the window (triowindowsizebiggie etc.)
			// Returns:
			// whether the comment is blank or not
			clsDRWrapper clsSave = new clsDRWrapper();
			intFrmSize = intFormSize;
			if (boolNonEditable)
			{
				rtbText.Locked = true;
				mnuSave.Visible = false;
				mnuSaveExit.Visible = false;
				mnuSepar2.Visible = false;
			}
			this.Text = strFormCaption;
			lblTitle.Text = strControlCaption;
			strMod = strModule;
			strTbl = strTable;
			strFld = strField;
			strIDFld = strKeyField;
			lngIDNumber = lngID;
			intFrmSize = intFormSize;
			if (intFrmSize == modGlobalConstants.TRIOWINDOWSIZEBIGGIE)
			{
				//vsElasticLight1.Enabled = true;
			}
			// size the control accordingly
			// Select Case intFrmSize
			// Case TRIOWINDOWMINI
			// rtbText.Width = 2775
			// rtbText.Height = 645
			// lblTitle.Width = 2775
			// Case TRIOWINDOWSIZEBIGGIE
			// don't need to do anything
			// Case TRIOWINDOWSIZEMEDIUM
			// rtbText.Width = 5745
			// rtbText.Height = 6450
			// lblTitle.Width = 5745
			// Case TRIOWINDOWSIZESMALL
			// rtbText.Width = 3825
			// rtbText.Height = 1605
			// lblTitle.Width = 3825
			// End Select
			clsSave.OpenRecordset("select " + strField + " from " + strTable + " where " + strKeyField + " = " + FCConvert.ToString(lngID), "tw" + strModule + "0000.vb1");
			if (!clsSave.EndOfFile())
			{
				rtbText.Text = FCConvert.ToString(clsSave.Get_Fields(strField));
				if (fecherFoundation.Strings.Trim(FCConvert.ToString(clsSave.Get_Fields(strField))) != string.Empty)
				{
					boolBlankComment = false;
				}
				else
				{
					boolBlankComment = true;
				}
			}
			else
			{
				rtbText.Text = "";
				boolBlankComment = true;
			}
			if (boolModal)
			{
				this.Show(FCForm.FormShowEnum.Modal);
			}
			else
			{
				this.Show(App.MainForm);
			}
			Init = boolBlankComment;
			return Init;
		}

		private void frmGlobalComment_Resize(object sender, System.EventArgs e)
		{
			if (intFrmSize == modGlobalConstants.TRIOWINDOWSIZEBIGGIE)
				return;
			rtbText.WidthOriginal = frmGlobalComment.InstancePtr.WidthOriginal - (rtbText.LeftOriginal * 2) - 100;
			lblTitle.WidthOriginal = rtbText.WidthOriginal;
			rtbText.HeightOriginal = this.HeightOriginal - rtbText.TopOriginal - 650;
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			rtbText.Text = "";
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				//fecherFoundation.Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1.Flags = 0	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				// MDIParent.InstancePtr.CommonDialog1.Flags = (vbPorterConverter.cdlPDReturnDC+vbPorterConverter.cdlPDNoPageNums)	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				// If rtbText.SelLength = 0 Then
				// MDIParent.InstancePtr.CommonDialog1.Flags = 0 /*? MDIParent.InstancePtr.CommonDialog1.Flags */	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"+vbPorterConverter.cdlPDAllPages	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				// MDIParent.InstancePtr.CommonDialog1.Flags = 0 /*? MDIParent.InstancePtr.CommonDialog1.Flags */	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"+vbPorterConverter.cdlPDSelection	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//fecherFoundation.Information.Err().Clear();
				//MDIParent.InstancePtr.CommonDialog1.ShowPrinter();
				//if (fecherFoundation.Information.Err().Number == 0)
				//{
				rtbText.SelPrint(0);
				//	FCGlobal.Printer.EndDoc();
				//}
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (!(fecherFoundation.Information.Err().Number == 32755))
				{
					MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err().Number) + "  " + fecherFoundation.Information.Err().Description + "\r\n" + "In mnuPrint_click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveComment();
		}

		private bool SaveComment()
		{
			bool SaveComment = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveComment = false;
				clsSave.OpenRecordset("select * from " + strTbl + " where " + strIDFld + " = " + FCConvert.ToString(lngIDNumber), "tw" + strMod + "0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
					clsSave.Set_Fields(strIDFld, lngIDNumber);
				}
				clsSave.Set_Fields(strFld, rtbText.Text);
				clsSave.Update();
				if (fecherFoundation.Strings.Trim(rtbText.Text) == string.Empty)
				{
					boolBlankComment = true;
				}
				else
				{
					boolBlankComment = false;
				}
				SaveComment = true;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveComment;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err().Number) + "  " + fecherFoundation.Information.Err().Description + "\r\n" + "In SaveComment", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveComment;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveComment())
			{
				mnuExit_Click();
			}
		}
	}
}
