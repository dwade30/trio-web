﻿namespace Global
{
	public class cEmailDataAttachment
	{
		//=========================================================
		private string strAttachmentName = string.Empty;
		private byte[] bytData = null;
		private bool boolHasData;
		private string strNewName = string.Empty;

		public string NewName
		{
			get
			{
				if (strNewName != "")
				{
					return strNewName;
				}
				else
				{
					return strAttachmentName;
				}
			}
			set
			{
				strNewName = value;
			}
		}

		public bool IncludesData
		{
			get
			{
				bool IncludesData = false;
				IncludesData = boolHasData;
				return IncludesData;
			}
		}

		public byte[] GetAttachmentData()
		{
			byte[] GetAttachmentData = null;
			GetAttachmentData = bytData;
			return GetAttachmentData;
		}

		public void SetAttachmentData(ref byte[] dataArray)
		{
			bytData = dataArray;
			boolHasData = true;
		}

		public string AttachmentName
		{
			set
			{
				strAttachmentName = value;
			}
			get
			{
				string AttachmentName = "";
				AttachmentName = strAttachmentName;
				return AttachmentName;
			}
		}

		public cEmailDataAttachment() : base()
		{
			boolHasData = false;
		}
	}
}
