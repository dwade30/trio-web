﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptAddExtraLienFees : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/04/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/04/2005              *
		// ********************************************************
		int lngTotalAccounts;
		int lngCount;
		// this will keep track of which row I am on in the grid
		double dblDemandFee;
		bool boolDone;
		double dblTotalDemand;
		int[] lngLRN = null;

		public rptAddExtraLienFees()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Fees";
		}

		public static rptAddExtraLienFees InstancePtr
		{
			get
			{
				return (rptAddExtraLienFees)Sys.GetInstance(typeof(rptAddExtraLienFees));
			}
		}

		protected rptAddExtraLienFees _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public void Init(int[] lngPassLRN)
		{
			lngLRN = lngPassLRN;
			lngTotalAccounts = Information.UBound(lngLRN, 1);
			if (lngTotalAccounts > 0)
			{
				// frmReportViewer.Init Me, , vbModal
				this.Run();
			}
			else
			{
				Cancel();
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (boolDone)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// save this report for future access
			if (this.Document.Pages.Count > 0)
			{
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "ExtraCLLienFee.RDF"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			lngCount = 1;
		}

		private void BindFields()
		{
			// this will fill the information into the fields
			clsDRWrapper rsCL = new clsDRWrapper();
			NEXTACCOUNT:
			;
			fldAcct.Text = "";
			fldName.Text = "";
			fldTotal.Text = "";
			if (!boolDone)
			{
				rsCL.OpenRecordset("SELECT LienRecordNumber, Account, Name1, Name2 FROM BillingMaster WHERE LienRecordNumber = " + FCConvert.ToString(lngLRN[lngCount]), modExtraModules.strCLDatabase);
				if (!rsCL.EndOfFile())
				{
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					fldAcct.Text = rsCL.Get_Fields_String("Account");
					if (Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("Name2"))) != "")
					{
						fldName.Text = Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("Name1"))) + " and " + Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("Name2")));
					}
					else
					{
						fldName.Text = Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("Name1")));
					}
					fldTotal.Text = "5.00";
					dblTotalDemand += 5;
				}
				else
				{
					lngCount += 1;
					if (lngCount >= Information.UBound(lngLRN, 1))
					{
						boolDone = true;
					}
					goto NEXTACCOUNT;
				}
				if (lngCount >= Information.UBound(lngLRN, 1))
				{
					boolDone = true;
				}
				else
				{
					lngCount += 1;
				}
			}
			else
			{
				// clear the fields
				fldAcct.Text = "";
				fldName.Text = "";
				fldTotal.Text = "";
			}
			rsCL.Dispose();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			lblNumberOfAccounts.Text = "There were " + FCConvert.ToString(lngTotalAccounts) + " accounts processed.";
			lblTotalTotal.Text = Strings.Format(dblTotalDemand, "#,##0.00");
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		
	}
}
