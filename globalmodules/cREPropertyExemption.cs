﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWXF0000
{
	public class cREPropertyExemption
	{
		//=========================================================
		private short intExemptCode;
		private double dblExemptValue;
		private double dblPercentage;

		public double Percentage
		{
			set
			{
				dblPercentage = value;
			}
			get
			{
				double Percentage = 0;
				Percentage = dblPercentage;
				return Percentage;
			}
		}

		public short ExemptCode
		{
			set
			{
				intExemptCode = value;
			}
			get
			{
				short ExemptCode = 0;
				ExemptCode = intExemptCode;
				return ExemptCode;
			}
		}

		public double ExemptValue
		{
			set
			{
				dblExemptValue = value;
			}
			get
			{
				double ExemptValue = 0;
				ExemptValue = dblExemptValue;
				return ExemptValue;
			}
		}
	}
}
