﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using fecherFoundation;

namespace Global
{
	public class cAccountsReceivableDB
	{
		private string strLastError = "";
		private int lngLastError;
        const string DatabaseToUpdate = "AccountsReceivable";
        public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cVersionInfo GetVersion()
		{
			cVersionInfo GetVersion = null;
			try
			{   // On Error GoTo ErrorHandler
				ClearErrors();
				cVersionInfo tVer = new cVersionInfo();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from dbversion", "AccountsReceivable");
				if (!rsLoad.EndOfFile())
				{
					tVer.Build = rsLoad.Get_Fields_Int32("Build");
					tVer.Major = rsLoad.Get_Fields_Int32("Major");
					tVer.Minor = rsLoad.Get_Fields_Int32("Minor");
					tVer.Revision = rsLoad.Get_Fields_Int32("Revision");
				}
				GetVersion = tVer;
				return GetVersion;
			}
			catch
			{   // ErrorHandler:
				strLastError = Information.Err().Description;
				lngLastError = Information.Err().Number;
			}
			return GetVersion;
		}
		
		public void SetVersion(ref cVersionInfo nVersion)
		{
			try
			{   // On Error GoTo ErrorHandler
				if (!(nVersion == null))
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from dbversion", "AccountsReceivable");
					if (rsSave.EndOfFile())
					{
						rsSave.AddNew();
					}
					else
					{
						rsSave.Edit();
					}
					rsSave.Set_Fields("Major", nVersion.Major);
					rsSave.Set_Fields("Minor", nVersion.Minor);
					rsSave.Set_Fields("Revision", nVersion.Revision);
					rsSave.Set_Fields("Build", nVersion.Build);
					rsSave.Update();
				}
				return;
			}
			catch
			{   // ErrorHandler:
				strLastError = Information.Err().Description;
				lngLastError = Information.Err().Number;
			}
		}

		public bool CheckVersion()
		{
			bool CheckVersion = false;
			ClearErrors();
			try
			{   // On Error GoTo ErrorHandler
				cVersionInfo nVer = new cVersionInfo();
				cVersionInfo tVer = new cVersionInfo();
				cVersionInfo cVer;
				bool boolNeedUpdate;
                clsDRWrapper rsTest = new clsDRWrapper();
                if (!rsTest.DBExists(DatabaseToUpdate))
                {
                    return CheckVersion;
                }

                cVer = GetVersion();
				if (cVer == null)
				{
					cVer = new cVersionInfo();
				}
				nVer.Major = 1; // default to 1.0.0.0

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (OldCDBS())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}

				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 1;
				if (tVer.IsNewer(cVer))
				{
					if (AddCorrelationIdentifier())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 2;
				if (tVer.IsNewer(cVer))
				{
					if (AddCustomerPartyView())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 3;
				if (tVer.IsNewer(cVer))
				{
					if (AddPreviousInterestPaidDateToBill())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 4;
				if (tVer.IsNewer(cVer))
				{
					if (AddTransactionIdToPaymentRec())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 5;
				if (tVer.IsNewer(cVer))
				{
					if (FixChargedInterestDate())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				CheckVersion = true;
				return CheckVersion;
			}
			catch
			{   // ErrorHandler:
				strLastError = Information.Err().Description;
				lngLastError = Information.Err().Number;
			}
			return CheckVersion;
		}

		private bool FixChargedInterestDate()
		{
			try
			{
				clsDRWrapper rsUpdate = new clsDRWrapper();

				rsUpdate.Execute("UPDATE PaymentRec SET CHGINTDate = NULL WHERE CHGINTDate = '12/30/1899'",
					"AccountsReceivable");

				return true;
			}
			catch 
			{
				return false;
			}
		}

		private bool AddTransactionIdToPaymentRec()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsUpdate = new clsDRWrapper();
				if (!FieldExists("PaymentRec", "TransactionIdentifier", "AccountsReceivable"))
				{
					rsUpdate.Execute("Alter Table.[dbo].[PaymentRec] Add [TransactionIdentifier] uniqueidentifier NULL", "AccountsReceivable");

					rsUpdate.OpenRecordset("SELECT * FROM PaymentRec WHERE CHGINTNumber <> 0", "AccountsReceivable");
					if (rsUpdate.BeginningOfFile() != true && rsUpdate.EndOfFile() != true)
					{
						do
						{
							var transId = Guid.NewGuid();
							clsDRWrapper rsChargedInt = new clsDRWrapper();

							rsChargedInt.OpenRecordset(
								"SELECT * FROM PaymentRec WHERE Id = " + rsUpdate.Get_Fields_Int32("CHGINTNumber"), "AccountsReceivable");
							if (rsChargedInt.BeginningOfFile() != true && rsChargedInt.EndOfFile() != true)
							{
								rsChargedInt.Edit();
								rsChargedInt.Set_Fields("TransactionIdentifier", transId);
								rsChargedInt.Update();

								rsUpdate.Edit();
								rsUpdate.Set_Fields("TransactionIdentifier", transId);
								rsUpdate.Update();
							}

							rsUpdate.MoveNext();
						} while (rsUpdate.EndOfFile() != true);
					}
				}
				
				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool AddPreviousInterestPaidDateToBill()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsUpdate = new clsDRWrapper();
				if (!FieldExists("Bill", "PreviousInterestPaidDate", "AccountsReceivable"))
				{
					rsUpdate.Execute("Alter Table.[dbo].[Bill] Add [PreviousInterestPaidDate] datetime NULL", "AccountsReceivable");
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool AddCustomerPartyView()
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();

			try
			{
				rsUpdate.OpenRecordset("select* FROM sys.views where name = 'CustomerPartyView'", "TWAR0000.vb1");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("CREATE VIEW [dbo].[CustomerPartyView] as SELECT cust.*, party.PartyGuid, party.PartyType, party.FirstName, party.MiddleName, Party.LastName, " + 
					                 "Party.Designation, party.Email, party.WebAddress, party.Address1, party.Address2, party.Address3, " +
									 "party.City, party.State, party.Zip, party.Country, party.OverrideName, party.FullName, party.FullNameLF FROM dbo.CustomerMaster as cust" +
					                 " INNER JOIN " + rsUpdate.Get_GetFullDBName("CentralParties") + ".dbo.PartyAndAddressView as party " +
									 "ON cust.PartyID = party.ID", "AccountsReceivable");
				}
				
				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool AddCorrelationIdentifier()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsUpdate = new clsDRWrapper();
				if (!FieldExists("PaymentRec", "CorrelationIdentifier", "AccountsReceivable"))
				{
					rsUpdate.Execute("Alter Table.[dbo].[PaymentRec] Add [CorrelationIdentifier] uniqueidentifier NULL", "AccountsReceivable");
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool OldCDBS()
		{
			bool OldCDBS = false;
			clsDRWrapper rsUpdate = new clsDRWrapper();

			rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Bill' AND COLUMN_NAME = 'CreatedBy'", "TWAR0000.vb1");
			if (rsUpdate.RecordCount() == 0)
			{
				rsUpdate.Execute("ALTER TABLE Bill ADD CreatedBy nvarchar(255) NULL", "TWAR0000.vb1");
			}
			UpdateCustomBillCodes();

			OldCDBS = true;
			return OldCDBS;
		}

		public static void UpdateCustomBillCodes()
		{
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM CustomBillCodes", "TWAR0000.vb1");
			if (rs.FindFirstRecord("FieldID", 5) == true)
			{
				rs.Edit();
				rs.Set_Fields("SpecialCase", true);
				rs.Update();
			}
			if (rs.FindFirstRecord("FieldID", 7) == true)
			{
				rs.Edit();
				rs.Set_Fields("SpecialCase", true);
				rs.Update();
			}
			if (rs.FindFirstRecord("FieldID", 9) == true)
			{
				rs.Edit();
				rs.Set_Fields("SpecialCase", true);
				rs.Update();
			}
			if (rs.FindFirstRecord("FieldID", 11) == true)
			{
				rs.Edit();
				rs.Set_Fields("SpecialCase", true);
				rs.Update();
			}
			if (rs.FindFirstRecord("FieldID", 13) == true)
			{
				rs.Edit();
				rs.Set_Fields("SpecialCase", true);
				rs.Update();
			}
			if (rs.FindFirstRecord("FieldID", 15) == true)
			{
				rs.Edit();
				rs.Set_Fields("SpecialCase", true);
				rs.Update();
			}
		}

		private bool TableExists(string strTable, string strDB)
		{
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			try
			{
				strSQL = "select * from sys.tables where name = '" + strTable + "' and type = 'U'";
				rsLoad.OpenRecordset(strSQL, strDB);
				if (rsLoad.EndOfFile())
				{
					return true;
				}
				return false;
			}
			catch
			{
				return false;
			}
		}

		private bool FieldExists(string strTable, string strField, string strDB)
		{
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			try
			{
				strSQL = "Select column_name from information_schema.columns where column_name = '" + strField + "' and table_name = '" + strTable + "'";
				rsLoad.OpenRecordset(strSQL, strDB);

				if (!rsLoad.EndOfFile())
				{
					return true;
				}
				return false;
			}
			catch
			{
				return false;
			}
		}

	}
}



