﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmInput.
	/// </summary>
	public partial class frmInput : BaseForm
	{
		public frmInput()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmInput InstancePtr
		{
			get
			{
				return (frmInput)Sys.GetInstance(typeof(frmInput));
			}
		}

		protected frmInput _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		string strPointer;
		double dblTemp;
		int lngTemp;
		// VBto upgrade warning: dttemp As DateTime	OnWrite(short, string)
		DateTime dttemp;
		bool boolCancel;
		modGlobalConstants.InputDTypes intDaytaType;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			strPointer = "";
			dblTemp = 0;
			dttemp = DateTime.FromOADate(0);
			lngTemp = 0;
			Close();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			boolCancel = false;
			switch (intDaytaType)
			{
				case modGlobalConstants.InputDTypes.idtString:
					{
						strPointer = txtInput.Text;
						break;
					}
				case modGlobalConstants.InputDTypes.idtDate:
					{
						if (Information.IsDate(T2KDateInput.Text))
						{
							dttemp = FCConvert.ToDateTime(T2KDateInput.Text);
						}
						else
						{
							dttemp = DateTime.FromOADate(0);
						}
						break;
					}
				case modGlobalConstants.InputDTypes.idtNumber:
					{
						dblTemp = FCConvert.ToDouble(T2KDecimal.Text);
						break;
					}
				case modGlobalConstants.InputDTypes.idtWholeNumber:
					{
						if (T2KWhole.Visible)
						{
							//lngTemp = FCConvert.ToInt32(FCConvert.ToDouble(T2KWhole.Text));
							// FC:FINAL:VGE - #i1205 Removing commas added on client side.
							lngTemp = FCConvert.ToInt32(FCConvert.ToDouble(T2KWhole.Text.Replace(",", "")));
						}
						else
						{
							if (Conversion.Val(txtInput.Text) > 0)
							{
								lngTemp = FCConvert.ToInt32(FCConvert.ToDouble(txtInput.Text));
							}
							else
							{
								lngTemp = 0;
							}
						}
						break;
					}
				case modGlobalConstants.InputDTypes.idtPhoneNumber:
					{
						strPointer = FormatPhone(T2KPhoneInput.Text);
						break;
					}
			}
			//end switch
			this.Unload();
		}
		// VBto upgrade warning: strInput As object	OnWrite(int, string, DateTime, double)	OnRead(string, DateTime, double, int)
		public bool Init(ref object strInput, string strTitle = "", string strDescription = "", int lngBoxWidth = 2160, bool boolHideText = false, modGlobalConstants.InputDTypes intDataType = modGlobalConstants.InputDTypes.idtString, string strInitValue = "", bool boolDontFormatInput = false, bool startWithBlank = false)
		{
			bool Init = false;
			try
			{
				// On Error GoTo ErrorHandler
				intDaytaType = intDataType;
				this.Text = strTitle;
				this.lblDescription.Text = strDescription;
				Init = false;
				if (boolHideText)
				{
					txtInput.PasswordChar = '*';
				}
				boolCancel = true;
				// Set strPointer = strInput
				txtInput.WidthOriginal = lngBoxWidth;
				txtInput.LeftOriginal = FCConvert.ToInt32((this.WidthOriginal - lngBoxWidth) / 2.0);
				txtInput.Text = strInitValue;
				switch (intDataType)
				{
					case modGlobalConstants.InputDTypes.idtString:
						{
                            // string
                            //FC:FINAL:CHN - i.issue #1691: Hide TopPanel to fix gap.
                            this.TopPanel.Height = 0;
							strPointer = FCConvert.ToString(strInput);
							break;
						}
					case modGlobalConstants.InputDTypes.idtDate:
						{
							// date
							T2KDateInput.Width = txtInput.Width;
							T2KDateInput.Height = txtInput.Height;
							T2KDateInput.Left = txtInput.Left;
							T2KDateInput.Top = txtInput.Top;
							T2KDateInput.Visible = true;
							T2KPhoneInput.Visible = false;
							txtInput.Visible = false;
							if (Information.IsDate(strInput))
							{
								dttemp = Convert.ToDateTime(strInput);
								T2KDateInput.Text = Strings.Format(dttemp, "MM/dd/yyyy");
							}
							else if (!startWithBlank)
							{
								dttemp = DateTime.FromOADate(0);
								T2KDateInput.Text = "00/00/0000";
							}
                            else
                            {
                                dttemp = DateTime.FromOADate(0);
                                T2KDateInput.Text = "";
                            }
							break;
						}
					case modGlobalConstants.InputDTypes.idtNumber:
						{
							// double
							dblTemp = Conversion.Val(strInput);
							T2KDecimal.Visible = true;
                            if (!startWithBlank)
                            {
                                T2KDecimal.Text = Strings.Format(dblTemp, "0.00");
                            }
                            else
                            {
                                T2KDecimal.Text = "";
                            }
                            T2KDecimal.Width = txtInput.Width;
							T2KDecimal.Height = txtInput.Height;
							T2KDecimal.Left = txtInput.Left;
							T2KDecimal.Top = txtInput.Top;
							txtInput.Visible = false;
							T2KDateInput.Visible = false;
							T2KPhoneInput.Visible = false;
							break;
						}
					case modGlobalConstants.InputDTypes.idtWholeNumber:
						{
							lngTemp = FCConvert.ToInt32(strInput);
							if (!boolDontFormatInput)
							{
								T2KWhole.Visible = true;
                                if (!startWithBlank)
                                {
                                    T2KWhole.Text = FCConvert.ToString(lngTemp);
                                }
                                else
                                {
                                    T2KWhole.Text = "";
                                }
                                T2KWhole.Width = txtInput.Width;
								T2KWhole.Height = txtInput.Height;
								T2KWhole.Left = txtInput.Left;
								T2KWhole.Top = txtInput.Top;
								txtInput.Visible = false;
							}
							else
							{
								txtInput.TextAlign = HorizontalAlignment.Right;
								txtInput.Visible = true;
								T2KWhole.Visible = false;
								T2KWhole.Text = "";
                                if (!startWithBlank)
                                {
                                    txtInput.Text = FCConvert.ToString(lngTemp);
                                }
                                else
                                {
                                    txtInput.Text = "";
                                }
                                txtInput.SelectionLength = txtInput.Text.Length;
							}
							break;
						}
					case modGlobalConstants.InputDTypes.idtPhoneNumber:
						{
							strPointer = FormatPhone(FCConvert.ToString(strInput));
							if (FCConvert.ToString(strInput) != string.Empty)
							{
								T2KPhoneInput.Text = strPointer;
							}
							T2KPhoneInput.Visible = true;
							T2KDateInput.Visible = false;
							txtInput.Visible = false;
							T2KPhoneInput.Left = txtInput.Left;
							T2KPhoneInput.Height = txtInput.Height;
							T2KPhoneInput.Width = txtInput.Width;
							T2KPhoneInput.Top = txtInput.Top;
							break;
						}
				}
				//end switch
				this.Show(FormShowEnum.Modal);
				Init = !boolCancel;
				switch (intDaytaType)
				{
					case modGlobalConstants.InputDTypes.idtString:
						{
							strInput = strPointer;
							break;
						}
					case modGlobalConstants.InputDTypes.idtDate:
						{
							//FC:FINAL:MSH - i.issue #1786: in original is returned short date (without time)
							//strInput = dttemp;
							strInput = dttemp.ToShortDateString();
							break;
						}
					case modGlobalConstants.InputDTypes.idtNumber:
						{
							strInput = dblTemp;
							break;
						}
					case modGlobalConstants.InputDTypes.idtWholeNumber:
						{
							strInput = lngTemp;
							break;
						}
					case modGlobalConstants.InputDTypes.idtPhoneNumber:
						{
							strInput = strPointer;
							break;
						}
				}
				//end switch
				return Init;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In Init", MsgBoxStyle.Critical, "Error");
			}
			return Init;
		}

		private string FormatPhone(string strPhoneNumber)
		{
			string FormatPhone = "";
			string strTemp;
			int x;
			string strPhone;
			try
			{
				// On Error GoTo ErrorHandler
				strTemp = Strings.Trim(strPhoneNumber);
				strPhone = "";
				if (strTemp != string.Empty)
				{
					for (x = 1; x <= strTemp.Length; x++)
					{
						if (Information.IsNumeric(Strings.Mid(strTemp, x, 1)))
						{
							strPhone += Strings.Mid(strTemp, x, 1);
						}
					}
					// x
					if (strPhone.Length > 10)
					{
						strPhone = Strings.Left(strPhone, 10);
					}
					strPhone = Strings.Format(strPhone, "0000000000");
					strPhone = "(" + Strings.Mid(strPhone, 1, 3) + ")" + Strings.Mid(strPhone, 4, 3) + "-" + Strings.Mid(strPhone, 7);
				}
				else
				{
					strPhone = "(000)000-0000";
				}
				FormatPhone = strPhone;
				return FormatPhone;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In FormatPhone", MsgBoxStyle.Critical, "Error");
			}
			return FormatPhone;
		}

		private void frmInput_Activated(object sender, System.EventArgs e)
		{
			if (txtInput.Visible)
			{
				txtInput.Focus();
			}
			else if (T2KDateInput.Visible)
			{
				T2KDateInput.Focus();
			}
			else if (T2KDecimal.Visible)
			{
				T2KDecimal.Focus();
			}
			else if (T2KWhole.Visible)
			{
				T2KWhole.Focus();
			}
			else if (T2KPhoneInput.Visible)
			{
				T2KPhoneInput.Focus();
			}
		}

		private void frmInput_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmInput.Icon	= "frmInput.frx":0000";
			//frmInput.ScaleWidth	= 4680;
			//frmInput.ScaleHeight	= 2160;
			//frmInput.LinkTopic	= "Form1";
			//frmInput.LockControls	= true;
			//T2KWhole.BorderStyle	= 1;
			//T2KWhole.Appearance	= 1;
			//T2KWhole.MaxLength	= 20;
			//T2KDecimal.BorderStyle	= 1;
			//T2KDecimal.Appearance	= 1;
			//T2KDecimal.MaxLength	= 20;
			//T2KPhoneInput.BorderStyle	= 1;
			//T2KPhoneInput.Appearance	= 1;
			//T2KPhoneInput.MaxLength	= 14;
			//T2KDateInput.BorderStyle	= 1;
			//T2KDateInput.Appearance	= 1;
			//T2KDateInput.MaxLength	= 10;
			//cmdOK.Default	= true;
			//vsElasticLight1.OleObjectBlob	= "frmInput.frx":058A";
			//End Unmaped Properties
			// Call SetFixedSize(Me, TRIOWINDOWMINI)
		}
	}
}
