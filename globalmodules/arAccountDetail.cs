﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using TWSharedLibrary;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWBD0000
using TWBD0000;


#else
using TWCR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arAccountDetail : FCSectionReport
	{
		int lngRow;
		int lngPDNumber;
		int lngPDTop;
		double dblPDTotal;
		bool boolPerDiem;
		string strLastYearText = "";
		int lngAcctNum;
		bool boolShowCurrentAssessment;
		clsDRWrapper rsRK = new clsDRWrapper();

		public arAccountDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static arAccountDetail InstancePtr
		{
			get
			{
				return (arAccountDetail)Sys.GetInstance(typeof(arAccountDetail));
			}
		}

		protected arAccountDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (lngRow < 0)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				clsDRWrapper rsSettings = new clsDRWrapper();
				if (modStatusPayments.Statics.boolRE)
				{
					rsSettings.OpenRecordset("SELECT AcctDetailAssesment FROM Collections", modExtraModules.strCLDatabase);
					string VBtoVar = rsSettings.Get_Fields_String("AcctDetailAssesment");
					if (VBtoVar == "C")
					{
						boolShowCurrentAssessment = true;
					}
					else if (VBtoVar == "B")
					{
						boolShowCurrentAssessment = false;
					}
					else
					{
						boolShowCurrentAssessment = FCMessageBox.Show("Would you like to show the current assessment as it is in the Real Estate Database?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Show Assessments?") == DialogResult.Yes;
					}
				}
				else
				{
					boolShowCurrentAssessment = FCMessageBox.Show("Would you like to show the current assessment as it is in the Personal Property database?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Show Current?") == DialogResult.Yes;
				}
				SetHeaderString();
				if (modStatusPayments.Statics.boolRE)
				{
					rsRK.OpenRecordset("SELECT BillingYear, LienRecordNumber AS LRN, BillingMaster.RateKey AS RK, RateType FROM BillingMaster INNER JOIN RateRec ON BillingMaster.RateKey = RateRec.ID WHERE Account = " + FCConvert.ToString(lngAcctNum) + " AND BillingType = 'RE'", modExtraModules.strCLDatabase);
				}
				else
				{
					rsRK.OpenRecordset("SELECT BillingYear, LienRecordNumber AS LRN, BillingMaster.RateKey AS RK, RateType FROM BillingMaster INNER JOIN RateRec ON BillingMaster.RateKey = RateRec.ID WHERE Account = " + FCConvert.ToString(lngAcctNum) + " AND BillingType = 'PP'", modExtraModules.strCLDatabase);
				}
				boolPerDiem = false;
				// MAL@20080918 ; Tracker Reference: 11810
				if (modGlobalConstants.Statics.gblnShowReceiptNumonDetail)
				{
					lblAcct.Text = "Year/Rec #";
				}
				else
				{
					lblAcct.Text = "Year";
				}
				lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
				lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				lblMuniName.Text = modGlobalConstants.Statics.MuniName;
				CreatePerDiemTable();
				//FC:FINAL:AM:#i71 - add the new field here
				CreatePerDiemField();
				lngRow = 0;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Error Starting Account Detail");
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			lngRow = FindNextVisibleRow(ref lngRow);
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			// this will print the row from the grid
			int lngReceipt = 0;
			string strAllow = "";
			lnTotals.Visible = false;
			// reset the visible property to false so that only a subtotal line will have a horizontal line above it
			lnTotals.LineWeight = 0;
			fldComment.Text = "";
			fldComment.Visible = false;
			fldComment.Top = 0;
			if (lngRow >= 0)
			{
				// check for the signal to exit the loop
				// figure out what type of line it is
				if (frmRECLStatus.InstancePtr.GRID.RowOutlineLevel(lngRow) == 1)
				{
					// is it a header line
					HideFields_2(false);
					// Year
					fldYear.Text = frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColYear);
					rsRK.FindFirstRecord("Billingyear", modExtraModules.FormatYear(fldYear.Text));
					if (!rsRK.NoMatch)
					{
						// TODO: Field [LRN] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsRK.Get_Fields("LRN")) != 0)
						{
							fldYear.Text = fldYear.Text + " L";
						}
						else
						{
							string VBtoVar = rsRK.Get_Fields_String("RateType");
							if (VBtoVar == "L")
							{
								fldYear.Text = fldYear.Text + " L";
							}
							else if (VBtoVar == "S")
							{
								fldYear.Text = fldYear.Text + " S";
							}
							else
							{
								fldYear.Text = fldYear.Text + " R";
							}
						}
					}
					// Date
					fldDate.Text = frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColDate);
					// Ref
					fldRef.Text = frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColReference);
					// Per
					fldPer.Text = "";
					// Code
					fldCode.Text = "";
					// Principal
					fldPrincipal.Text = Strings.Format(Conversion.Val(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColPrincipal)), "#,##0.00");
					// Interest
					fldInterest.Text = Strings.Format(Conversion.Val(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColInterest)), "#,##0.00");
					// Costs
					fldCosts.Text = Strings.Format(Conversion.Val(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColCosts)), "#,##0.00");
					// Total
					fldTotal.Text = Strings.Format(Conversion.Val(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColTotalAmount)), "#,##0.00");
					// MAL@20080918: Remove extra line between Bill and Payment lines
					// Tracker Reference: 11810
					fldComment.Text = "";
					fldComment.Visible = false;
					fldComment.Top = 0;
					this.Detail.Height = fldYear.Top + fldYear.Height;
				}
				else if (Strings.Left(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColReference), 10) == "Billed To:")
				{
					// is it a name line
					HideFields_2(true);
					// Name
					fldName.Text = frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColReference) + " " + frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColCode);
				}
				else
				{
					if (Strings.Left(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColYear), 14) == "Account Totals")
					{
						// is it the last line?
						HideFields_2(false);
						fldName.Visible = true;
						fldYear.Visible = false;
						fldDate.Visible = false;
						fldRef.Visible = false;
						fldPer.Visible = false;
						fldCode.Visible = false;
						fldName.Left = 0;
						fldName.Width = fldPrincipal.Left;
						// show the line above this row and make it darker
						lnTotals.Visible = true;
						lnTotals.LineWeight = 2;
						// total line
						fldName.Text = frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColYear);
						// Principal
						fldPrincipal.Text = Strings.Format(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColPrincipal), "#,##0.00");
						// Interest
						fldInterest.Text = Strings.Format(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColInterest), "#,##0.00");
						// Costs
						fldCosts.Text = Strings.Format(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColCosts), "#,##0.00");
						// Total
						fldTotal.Text = Strings.Format(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColTotalAmount), "#,##0.00");
					}
					else if (frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow - 1, frmRECLStatus.InstancePtr.lngColGridCode) == "=")
					{
						// is this line a hidden totals line?
						HideFields_2(true);
						fldName.Text = "";
					}
					else
					{
						HideFields_2(false);
						fldYear.Text = "";
						// Date
						fldDate.Text = frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColDate);
						// Ref
						fldRef.Text = frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColReference);
						if (Strings.Left(fldRef.Text, 5) == "Total")
						{
							// subtotal line
							lnTotals.Visible = true;
							lnTotals.LineWeight = 1;
						}
						else
						{
							lnTotals.Visible = false;
						}
						// Comment
						if (Strings.Trim(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColComment)) != "")
						{
							fldComment.Text = frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColComment);
							fldComment.Visible = true;
							fldComment.Top = 270 / 1440F;
							this.Detail.Height = fldComment.Top + fldComment.Height;
						}
						else
						{
							fldComment.Text = "";
							fldComment.Visible = false;
							fldComment.Top = 0;
							this.Detail.Height = fldYear.Top + fldYear.Height;
						}
						// Per
						fldPer.Text = frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColPeriod);
						// Code
						fldCode.Text = frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColCode);
						// Principal
						fldPrincipal.Text = Strings.Format(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColPrincipal), "#,##0.00");
						// Interest
						fldInterest.Text = Strings.Format(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColInterest), "#,##0.00");
						// Costs
						fldCosts.Text = Strings.Format(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColCosts), "#,##0.00");
						// Total
						fldTotal.Text = Strings.Format(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColTotalAmount), "#,##0.00");
						// MAL@20080718: Add receipt number to report if option is selected
						// Tracker Reference: 11810
						strAllow = "P, Y, I, C, A, R, U, F, D, N";
						if (modGlobalConstants.Statics.gblnShowReceiptNumonDetail && FCConvert.ToBoolean(Strings.InStr(1, strAllow, fldCode.Text, CompareConstants.vbTextCompare)))
						{
							lngReceipt = GetReceiptNumber(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRow, frmRECLStatus.InstancePtr.lngColPaymentID));
							if (lngReceipt > 0)
							{
								fldYear.Text = lngReceipt.ToString();
							}
							else
							{
								fldYear.Text = "";
							}
						}
						else
						{
							fldYear.Text = "";
						}
					}
				}
			}
			else
			{
				fldYear.Text = "";
				fldDate.Text = "";
				fldRef.Text = "";
				fldPer.Text = "";
				fldCode.Text = "";
				fldPrincipal.Text = "";
				fldInterest.Text = "";
				fldCosts.Text = "";
				fldTotal.Text = "";
				fldName.Text = "";
			}
		}

		private int FindNextVisibleRow(ref int lngR)
		{
			int FindNextVisibleRow = 0;
			// this function will start at the row number passed in and return the next row to print
			// this function will return -1 if there are no more rows to show
			int lngNext;
			int intLvl/*unused?*/;
			bool boolFound;
			int lngMax = 0;
			boolFound = false;
			lngNext = lngR + 1;
			lngMax = frmRECLStatus.InstancePtr.GRID.Rows;
			if (lngNext < lngMax)
			{
				boolFound = false;
				while (!(boolFound || frmRECLStatus.InstancePtr.GRID.RowOutlineLevel(lngNext) < 2))
				{
					if (frmRECLStatus.InstancePtr.GRID.IsCollapsed(LastParentRow(lngNext)) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						lngNext += 1;
						boolFound = false;
						if (lngNext >= lngMax)
						{
							return FindNextVisibleRow;
						}
					}
					else
					{
						boolFound = true;
					}
				}
				FindNextVisibleRow = lngNext;
			}
			if (FindNextVisibleRow == 0)
				FindNextVisibleRow = -1;
			return FindNextVisibleRow;
		}
		// VBto upgrade warning: CurRow As Variant --> As int
		public int LastParentRow(int CurRow)
		{
			int LastParentRow = 0;
			int l;
			int curLvl;
			l = CurRow;
			curLvl = frmRECLStatus.InstancePtr.GRID.RowOutlineLevel(l);
			l -= 1;
			while (frmRECLStatus.InstancePtr.GRID.RowOutlineLevel(l) > 1)
			{
				l -= 1;
			}
			LastParentRow = l;
			return LastParentRow;
		}

		private void HideFields_2(bool boolHide)
		{
			HideFields(ref boolHide);
		}

		private void HideFields(ref bool boolHide)
		{
			// this sub will either hide or show the fields depending on which variable is shown
			fldYear.Visible = !boolHide;
			fldDate.Visible = !boolHide;
			fldRef.Visible = !boolHide;
			fldPer.Visible = !boolHide;
			fldCode.Visible = !boolHide;
			fldPrincipal.Visible = !boolHide;
			fldInterest.Visible = !boolHide;
			fldCosts.Visible = !boolHide;
			fldTotal.Visible = !boolHide;
			fldComment.Visible = !boolHide;
			fldName.Visible = boolHide;
			if (boolHide)
			{
				// makes sure that the name field is in the proper position
				fldName.Left = 1440 / 1440F;
				fldName.Width = this.PrintWidth - 2880 / 1440F;
				// Tracker Reference: 11810
				fldComment.Top = 0;
				this.Detail.Height = fldYear.Top + fldYear.Height;
			}
		}

		private void SetHeaderString()
		{
			// this will set the correct header for this account
			string strTemp = "";
			if (modStatusPayments.Statics.boolRE)
			{
				// determine if this is a Real Estate or Personal Property Account
				strTemp = "RE";
				lngAcctNum = StaticSettings.TaxCollectionValues.LastAccountRE;
			}
			else
			{
				strTemp = "PP";
				lngAcctNum = StaticSettings.TaxCollectionValues.LastAccountPP;
				// hide the header labels/line
				lblLand.Visible = false;
				lblLandValue.Visible = false;
				lblBuilding.Visible = false;
				lblBuildingValue.Visible = false;
				lblExempt.Visible = false;
				lblExemptValue.Visible = false;
				// lblValualtionValue.Visible = False
				// lblValuationTotal.Visible = False
				lblMapLot.Visible = false;
				lblAcreage.Visible = false;
				lblBookPage.Visible = false;
				Line2.Visible = false;
			}
			strTemp += " Account " + FCConvert.ToString(lngAcctNum) + " Detail";
			strTemp += "\r\n" + "as of " + Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy");
			lblHeader.Text = strTemp;
			lblName.Text = "Name: " + frmRECLStatus.InstancePtr.lblOwnersName.Text;
			lblLocation.Text = "Location: " + frmRECLStatus.InstancePtr.lblLocation.Text;
			if (modStatusPayments.Statics.boolRE)
			{
				lblValuationTotal.Text = "Total:";
				lblMapLot.Text = "Map/Lot: " + frmRECLStatus.InstancePtr.lblMapLot.Text;
				lblAcreage.Text = "Acreage: " + frmRECLStatus.InstancePtr.lblAcreage.Text;
				lblBookPage.Text = "Book Page: " + frmRECLStatus.InstancePtr.lblBookPage.Text;
				lblRef1.Text = "Ref1:";
				lblRef1Val.Text = frmRECLStatus.InstancePtr.lblReference1.Text;
				if (boolShowCurrentAssessment)
				{
					lblLandValue.Text = frmRECLStatus.InstancePtr.lblLandValue.Text;
					lblBuildingValue.Text = frmRECLStatus.InstancePtr.lblBuildingValue.Text;
					lblExemptValue.Text = frmRECLStatus.InstancePtr.lblExemptValue.Text;
					lblValualtionValue.Text = frmRECLStatus.InstancePtr.lblTotalValue.Text;
				}
				else
				{
					lblAssessmentYear.Text = "As of " + frmRECLStatus.InstancePtr.lblYear.Text;
					lblLandValue.Text = frmRECLStatus.InstancePtr.lblLandValue2.Text;
					lblBuildingValue.Text = frmRECLStatus.InstancePtr.lblBuildingValue2.Text;
					lblExemptValue.Text = frmRECLStatus.InstancePtr.lblExemptValue2.Text;
					lblValualtionValue.Text = frmRECLStatus.InstancePtr.lblTotalValue2.Text;
				}
			}
			else
			{
				lblMapLot.Text = "";
				lblAcreage.Text = "";
				lblBookPage.Text = "";
				lblRef1.Text = "";
				lblRef1Val.Text = "";
				lblValuationTotal.Text = "Assessment:";
				if (boolShowCurrentAssessment)
				{
					lblValualtionValue.Text = frmRECLStatus.InstancePtr.vsPPCategory.TextMatrix(10, 1);
				}
				else
				{
					lblAssessmentYear.Text = "As of " + frmRECLStatus.InstancePtr.vsPPCategory.TextMatrix(0, 2);
					lblValualtionValue.Text = frmRECLStatus.InstancePtr.vsPPCategory.TextMatrix(10, 2);
				}
			}
			// fill the period due field
			CalculatePeriodDue();
			FillMailingAddress();
		}

		private void CreatePerDiemTable()
		{
			int lngRW;
			for (lngRW = 1; lngRW <= frmRECLStatus.InstancePtr.GRID.Rows - 1; lngRW++)
			{
				if (Strings.Trim(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRW, 1)) != "")
				{
					strLastYearText = Strings.Trim(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRW, frmRECLStatus.InstancePtr.lngColYear));
				}
				if (Conversion.Val(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRW, frmRECLStatus.InstancePtr.lngColPerDiem)) != 0)
				{
					AddPerDiem(ref lngRW);
				}
			}
		}

		private void AddPerDiem(ref int lngRNum)
		{
			GrapeCity.ActiveReports.SectionReportModel.TextBox obNew/*unused?*/;
			string strTemp = "";
			// this will add another per diem line in the report footer
			if (lngPDNumber == 0)
			{
				lngPDNumber = 1;
				fldPerDiem1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fldPerDiem1.Text = Strings.Format(Conversion.Val(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRNum, frmRECLStatus.InstancePtr.lngColPerDiem)), "0.0000");
				if (Strings.Trim(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRNum, frmRECLStatus.InstancePtr.lngColYear)) == "")
				{
					lblPerDiem1.Text = frmRECLStatus.InstancePtr.GRID.TextMatrix(frmRECLStatus.InstancePtr.LastParentRow(lngRNum), frmRECLStatus.InstancePtr.lngColYear);
				}
				else
				{
					lblPerDiem1.Text = frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRNum, frmRECLStatus.InstancePtr.lngColYear);
				}
				dblPDTotal += Conversion.Val(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRNum, frmRECLStatus.InstancePtr.lngColPerDiem));
			}
			else
			{
				// increment the number of fields
				lngPDNumber += 1;
				// add a field
				obNew = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("fldPerDiem" + FCConvert.ToString(lngPDNumber));
				//obNew.Name = "fldPerDiem" + FCConvert.ToString(lngPDNumber);
				obNew.Top = fldPerDiem1.Top + ((lngPDNumber - 1) * fldPerDiem1.Height);
				obNew.Left = fldPerDiem1.Left;
				obNew.Width = fldPerDiem1.Width;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				strTemp = obNew.Font.ToString();
				obNew.Font = fldPerDiem1.Font;
				// this sets the font to the same as the field that is already created
				obNew.Text = Strings.Format(Conversion.Val(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRNum, frmRECLStatus.InstancePtr.lngColPerDiem)), "0.0000");
				dblPDTotal += Conversion.Val(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRNum, frmRECLStatus.InstancePtr.lngColPerDiem));
				//ReportFooter.Controls.Add(obNew);
				// add a label
				GrapeCity.ActiveReports.SectionReportModel.Label obLabel = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblPerDiem" + FCConvert.ToString(lngPDNumber));
				//obLabel.Name = "lblPerDiem" + FCConvert.ToString(lngPDNumber);
				obLabel.Top = lblPerDiem1.Top + ((lngPDNumber - 1) * lblPerDiem1.Height);
				obLabel.Left = lblPerDiem1.Left;
				strTemp = obLabel.Font.ToString();
				obLabel.Font = fldPerDiem1.Font;
				// this sets the font to the same as the field that is already created
				if (Strings.Trim(frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRNum, frmRECLStatus.InstancePtr.lngColYear)) == "")
				{
					obLabel.Text = frmRECLStatus.InstancePtr.GRID.TextMatrix(frmRECLStatus.InstancePtr.LastParentRow(lngRNum), frmRECLStatus.InstancePtr.lngColYear);
				}
				else
				{
					obLabel.Text = frmRECLStatus.InstancePtr.GRID.TextMatrix(lngRNum, frmRECLStatus.InstancePtr.lngColYear);
				}
				//ReportFooter.Controls.Add(obLabel);
			}
			boolPerDiem = true;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER				
				int intEx1 = 0;
				int intEx2 = 0;
				int intEx3 = 0;
				string strEx1 = "";
				string strEx2 = "";
				string strEx3 = "";
				// set the exempt codes
				if (modStatusPayments.Statics.boolRE)
				{
					modGlobalFunctions.GetExemptCodesandDescriptions("RE",  StaticSettings.TaxCollectionValues.LastAccountRE, ref intEx1, ref strEx1, ref intEx2, ref strEx2, ref intEx3, ref strEx3);
				}
				else
				{
					modGlobalFunctions.GetExemptCodesandDescriptions("PP",  StaticSettings.TaxCollectionValues.LastAccountPP, ref intEx1, ref strEx1, ref intEx2, ref strEx2, ref intEx3, ref strEx3);
				}
				lblEx.Visible = false;
				if (intEx1 != 0)
				{
					fldEx1.Text = Strings.Format(intEx1, "00") + " - " + strEx1;
					lblEx.Visible = true;
				}
				else
				{
					fldEx1.Text = "";
				}
				if (intEx2 != 0)
				{
					fldEx2.Text = Strings.Format(intEx2, "00") + " - " + strEx2;
					lblEx.Visible = true;
				}
				else
				{
					fldEx2.Text = "";
				}
				if (intEx3 != 0)
				{
					fldEx3.Text = Strings.Format(intEx3, "00") + " - " + strEx3;
					lblEx.Visible = true;
				}
				else
				{
					fldEx3.Text = "";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Report Footer");
			}
		}

		private void CreatePerDiemField()
		{
			if (boolPerDiem)
			{
				// add a field
				GrapeCity.ActiveReports.SectionReportModel.TextBox obNew = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>("fldPerDiemTotal");
				//obNew.Name = "fldPerDiemTotal";
				obNew.Top = fldPerDiem1.Top + (lngPDNumber * fldPerDiem1.Height);
				obNew.Left = fldPerDiem1.Left;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				obNew.Width = fldPerDiem1.Width;
				obNew.Text = Strings.Format(dblPDTotal, "0.0000");
				//ReportFooter.Controls.Add(obNew);
				// add a label
				GrapeCity.ActiveReports.SectionReportModel.Label obLabel = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Label>("lblPerDiemTotal");
				//obLabel.Name = "lblPerDiemTotal";
				obLabel.Top = lblPerDiem1.Top + (lngPDNumber * lblPerDiem1.Height);
				obLabel.Left = lblPerDiem1.Left;
				obLabel.Text = "Total";
				//ReportFooter.Controls.Add(obLabel);
				// add a line
				GrapeCity.ActiveReports.SectionReportModel.Line obLine = ReportFooter.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.Line>("lnPerDiemTotal");
				//obLine.Name = "lnPerDiemTotal";
				obLine.X1 = fldPerDiem1.Left;
				obLine.X2 = fldPerDiem1.Left + fldPerDiem1.Width;
				obLine.Y1 = lblPerDiem1.Top + (lngPDNumber * lblPerDiem1.Height);
				obLine.Y2 = obLine.Y1;
				//ReportFooter.Controls.Add(obLine);
				// kk10222014 trocls-35  Adding controls here doesn't trigger the footer to grow
				if (ReportFooter.Height < (fldPerDiem1.Top + (lngPDNumber + 1) * fldPerDiem1.Height))
				{
					ReportFooter.Height = fldPerDiem1.Top + (lngPDNumber + 1) * fldPerDiem1.Height;
				}
			}
			else
			{
				lblPerDiem1.Visible = false;
				lblPerDiemHeader.Visible = false;
				fldPerDiem1.Visible = false;
				Line3.Visible = false;
			}
		}

		private void CalculatePeriodDue()
		{
			// this sub will fill the period due label
			clsDRWrapper rsB = new clsDRWrapper();
			string strTemp = "";
			double Per1 = 0;
			double Per2 = 0;
			double Per3 = 0;
			double Per4 = 0;
			double dblPaid = 0;
			double dblOtherPer1 = 0;
			if (modStatusPayments.Statics.boolRE)
			{
				rsB.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcctNum) + " AND RateKey > 0 AND ISNULL(LienRecordNumber,0) = 0 AND BillingType = 'RE' ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
			}
			else
			{
				rsB.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcctNum) + " AND RateKey > 0 AND ISNULL(LienRecordNumber,0) = 0 AND BillingType = 'PP' ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
			}
			if (!rsB.EndOfFile())
			{
				Per1 = Conversion.Val(rsB.Get_Fields_Decimal("TaxDue1") - FCConvert.ToDecimal(modStatusPayments.Statics.AbatePaymentsArray[rsB.Get_Fields_Int32("BillingYear") % 1000].Per1));
				Per2 = Conversion.Val(rsB.Get_Fields_Decimal("TaxDue2") - FCConvert.ToDecimal(modStatusPayments.Statics.AbatePaymentsArray[rsB.Get_Fields_Int32("BillingYear") % 1000].Per2));
				Per3 = Conversion.Val(rsB.Get_Fields_Decimal("TaxDue3") - FCConvert.ToDecimal(modStatusPayments.Statics.AbatePaymentsArray[rsB.Get_Fields_Int32("BillingYear") % 1000].Per3));
				Per4 = Conversion.Val(rsB.Get_Fields_Decimal("TaxDue4") - FCConvert.ToDecimal(modStatusPayments.Statics.AbatePaymentsArray[rsB.Get_Fields_Int32("BillingYear") % 1000].Per4));
				dblPaid = Conversion.Val(rsB.Get_Fields_Decimal("PrincipalPaid") - (FCConvert.ToDecimal(modStatusPayments.Statics.AbatePaymentsArray[rsB.Get_Fields_Int32("BillingYear") % 1000].Per1) + FCConvert.ToDecimal(modStatusPayments.Statics.AbatePaymentsArray[rsB.Get_Fields_Int32("BillingYear") % 1000].Per2) + FCConvert.ToDecimal(modStatusPayments.Statics.AbatePaymentsArray[rsB.Get_Fields_Int32("BillingYear") % 1000].Per3) + FCConvert.ToDecimal(modStatusPayments.Statics.AbatePaymentsArray[rsB.Get_Fields_Int32("BillingYear") % 1000].Per4)));
				dblOtherPer1 = Conversion.Val(((rsB.Get_Fields_Decimal("InterestCharged") * -1) + rsB.Get_Fields_Decimal("DemandFees")) - (rsB.Get_Fields_Decimal("InterestPaid") + rsB.Get_Fields_Decimal("DemandFeesPaid")));
				if (Per1 >= dblPaid)
				{
					Per1 -= dblPaid;
					dblPaid = 0;
				}
				else
				{
					dblPaid -= Per1;
					Per1 = 0;
					if (Per2 >= dblPaid)
					{
						Per2 -= dblPaid;
						dblPaid = 0;
					}
					else
					{
						dblPaid -= Per2;
						Per2 = 0;
						if (Per3 >= dblPaid)
						{
							Per3 -= dblPaid;
							dblPaid = 0;
						}
						else
						{
							dblPaid -= Per3;
							Per3 = 0;
							if (Per4 > dblPaid)
							{
								Per4 -= dblPaid;
								dblPaid = 0;
							}
							else
							{
								dblPaid -= Per4;
								Per4 = 0;
							}
						}
					}
				}
				Per1 += modStatusPayments.Statics.dblCurrentInt[FCConvert.ToInt16(rsB.Get_Fields_Int32("BillingYear")) - modExtraModules.DEFAULTSUBTRACTIONVALUE] + dblOtherPer1;
				strTemp = modExtraModules.FormatYear(FCConvert.ToString(rsB.Get_Fields_Int32("BillingYear"))) + " Period Due: ";
				if (Per1 != 0)
				{
					strTemp += "\r\n" + "     1) " + Strings.Format(Per1, "#,##0.00") + "  ";
				}
				if (Per2 != 0)
				{
					strTemp += "\r\n" + "     2) " + Strings.Format(Per2, "#,##0.00") + "  ";
				}
				if (Per3 != 0)
				{
					strTemp += "\r\n" + "     3) " + Strings.Format(Per3, "#,##0.00") + "  ";
				}
				if (Per4 != 0)
				{
					strTemp += "\r\n" + "     4) " + Strings.Format(Per4, "#,##0.00") + "  ";
				}
				fldPerDue.Text = strTemp;
			}
			else
			{
				fldPerDue.Text = "";
			}
		}

		private void FillMailingAddress()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRE = new clsDRWrapper();
				string strTemp = "";
				if (modStatusPayments.Statics.boolRE)
				{
					// kk 120912  rsRE.OpenRecordset "SELECT * FROM Master WHERE RSAccount = " & lngAcctNum, strREDatabase
					rsRE.OpenRecordset("SELECT pOwn.Address1, pOwn.Address2, pOwn.City, pOwn.State, pOwn.Zip " + "FROM Master INNER JOIN " + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn ON Master.OwnerPartyID = pOwn.ID " + "WHERE RSAccount = " + FCConvert.ToString(lngAcctNum), modExtraModules.strREDatabase);
					if (!rsRE.EndOfFile())
					{
						// kk rsRE.InsertName "OwnerPartyID", "Own1", False, True, True, "", False, "", True, ""
						lblMailingAddressTitle.Text = "Mailing Address:";
						strTemp = FCConvert.ToString(rsRE.Get_Fields_String("Address1"));
						if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("Address2"))) != "")
						{
							strTemp += "\r\n" + FCConvert.ToString(rsRE.Get_Fields_String("Address2"));
						}
						if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("City"))) != "")
						{
							strTemp += "\r\n" + FCConvert.ToString(rsRE.Get_Fields_String("City")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("State")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("Zip"));
						}
						lblMailingAddress.Text = strTemp;
					}
				}
				else
				{
					// kk 120912  rsRE.OpenRecordset "SELECT * FROM PPMaster WHERE Account = " & lngAcctNum, strPPDatabase
					rsRE.OpenRecordset("SELECT pOwn.Address1, pOwn.Address2, pOwn.City, pOwn.State, pOwn.Zip " + "FROM PPMaster INNER JOIN " + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn ON PPMaster.PartyID = pOwn.ID " + "WHERE Account = " + FCConvert.ToString(lngAcctNum), modExtraModules.strPPDatabase);
					if (!rsRE.EndOfFile())
					{
						// kk rsRE.InsertName "PartyID", "Own1", False, True, True, "", False, "", True, ""
						lblMailingAddressTitle.Text = "Mailing Address:";
						if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("Address1"))) != "")
						{
							strTemp = FCConvert.ToString(rsRE.Get_Fields_String("Address1"));
						}
						if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("Address2"))) != "")
						{
							strTemp += "\r\n" + Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("Address2")));
						}
						if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("City"))) != "")
						{
							strTemp += "\r\n" + Strings.Trim(Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("City"))) + " " + FCConvert.ToString(rsRE.Get_Fields_String("State"))) + " " + FCConvert.ToString(rsRE.Get_Fields_String("Zip"));
						}
						lblMailingAddress.Text = strTemp;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Adding Mailing Address");
			}
		}

		private int GetReceiptNumber(string strPaymentID)
		{
			int GetReceiptNumber = 0;
			// Tracker Reference: 11810
			clsDRWrapper rsPymt = new clsDRWrapper();
			int lngResult = 0;
			if (strPaymentID != "")
			{
				rsPymt.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(FCConvert.ToInt32(FCConvert.ToDouble(strPaymentID))), modExtraModules.strCLDatabase);
				if (rsPymt.RecordCount() > 0)
				{
					lngResult = FCConvert.ToInt32(rsPymt.Get_Fields_Int32("ReceiptNumber"));
				}
			}
			else
			{
				lngResult = 0;
			}
			GetReceiptNumber = lngResult;
			return GetReceiptNumber;
		}

		private void arAccountDetail_Load(object sender, System.EventArgs e)
		{

		}
	}
}
