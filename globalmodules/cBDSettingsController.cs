﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace Global
{
	public class cBDSettingsController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngErrorNumber, string strErrorDescription)
		{
			strLastError = strErrorDescription;
			lngLastError = lngErrorNumber;
		}

		public cBDSettings GetBDSettings()
		{
			cBDSettings GetBDSettings = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cGenericCollection retColl = new cGenericCollection();
				cBDSettings bSettings = new cBDSettings();
				string strSQL;
				strSQL = "select * from Budgetary";
				rsLoad.OpenRecordset(strSQL, "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					bSettings.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					bSettings.ACHFederalID = FCConvert.ToString(rsLoad.Get_Fields_String("achfederalid"));
					bSettings.ACHFederalIDPrefix = FCConvert.ToInt16(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int16("achfederalidprefix"))));
					bSettings.ACHTownName = FCConvert.ToString(rsLoad.Get_Fields_String("achtownname"));
					bSettings.AllowManualWarrantNumbers = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("allowmanualwarrantnumbers"));
					bSettings.APSeq = FCConvert.ToString(rsLoad.Get_Fields_String("apseq"));
					bSettings.AutomaticInterest = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("automaticinterest"));
					bSettings.AutoNumberPO = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoNumberPO"));
					bSettings.AutoPostAP = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostAP"));
					bSettings.AutoPostAPCorr = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostAPCorr"));
					bSettings.AutoPostARBills = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostARBills"));
					bSettings.AutoPostBLCommitments = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AUtoPostBLCommitments"));
					bSettings.AutoPostBLSupplementals = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostBLSupplementals"));
					bSettings.AutoPostBudgetTransfer = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("autopostbudgettransfer"));
					bSettings.AutoPostCD = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostCD"));
					bSettings.AutoPostCLLiens = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostCLLiens"));
					bSettings.AutoPostCLTA = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostCLTA"));
					bSettings.AutoPostCM = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostCM"));
					bSettings.AutoPostCR = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostCR"));
					bSettings.AutoPostCRDailyAudit = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostCRDailyAudit"));
					bSettings.AutoPostCreateOpeningAdjustments = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostCreateOpeningAdjustments"));
					bSettings.AutoPostEN = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("Autoposten"));
					bSettings.AutoPostFADepreciation = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostFADepreciation"));
					bSettings.AutoPostGJ = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AUtopostgj"));
					bSettings.AutoPostMARR = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostMARR"));
					bSettings.AutoPostPYProcesses = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostPYProcesses"));
					bSettings.AutoPostReversingJournals = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostReversingJournals"));
					bSettings.AutoPostUBBills = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostUBBills"));
					bSettings.AutoPostUBLiens = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutoPostUBLiens"));
					bSettings.BalancedACHFile = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("BalancedACHFile"));
					bSettings.BalanceValidation = FCConvert.ToInt16(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("BalanceValidation"))));
					bSettings.CashDue = FCConvert.ToString(rsLoad.Get_Fields_String("CashDue"));
					bSettings.CheckFormat = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("CheckFormat"))));
					bSettings.CheckIcon = FCConvert.ToString(rsLoad.Get_Fields_String("CheckICon"));
					bSettings.CheckLaserAdjustment = Conversion.Val(rsLoad.Get_Fields_Double("CheckLaserAdjustment"));
					bSettings.CheckRef = FCConvert.ToString(rsLoad.Get_Fields_String("CheckRef"));
					bSettings.ConvertedCityStateZip = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ConvertedCityStateZip"));
					bSettings.Due = FCConvert.ToString(rsLoad.Get_Fields_String("Due"));
					bSettings.DueFrom = FCConvert.ToString(rsLoad.Get_Fields_String("DueFrom"));
					bSettings.DueTo = FCConvert.ToString(rsLoad.Get_Fields_String("DueTo"));
					bSettings.EncByDept = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("EncByDept"));
					bSettings.EncCarryForward = FCConvert.ToInt16(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int16("EncCarryForward"))));
					bSettings.EOYStatus = FCConvert.ToString(rsLoad.Get_Fields_String("EOYStatus"));
					bSettings.ExcludeAPDueToFrom = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ExcludeAPDueToFrom"));
					bSettings.ExcludeCRDueToFrom = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ExcludeCRDueToFrom"));
					bSettings.ExcludePYDueToFrom = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ExcludePYDueToFrom"));
					bSettings.Expenditure = FCConvert.ToString(rsLoad.Get_Fields_String("Expenditure"));
					bSettings.FiscalStart = FCConvert.ToString(rsLoad.Get_Fields_String("FiscalStart"));
					bSettings.FixedPastYearBudget = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("FixedPastYearBudget"));
					bSettings.ImmediateDestinationName = FCConvert.ToString(rsLoad.Get_Fields_String("ImmediateDestinationName"));
					bSettings.ImmediateDestinationRoutingNumber = FCConvert.ToString(rsLoad.Get_Fields_String("ImmediateDestinationRoutingNumber"));
					bSettings.ImmediateOriginName = FCConvert.ToString(rsLoad.Get_Fields_String("ImmediateOriginName"));
					bSettings.ImmediateOriginODFINumber = FCConvert.ToString(rsLoad.Get_Fields_String("ImmediateOriginODFINumber"));
					bSettings.ImmediateOriginRoutingNumber = FCConvert.ToString(rsLoad.Get_Fields_String("ImmediateOriginRoutingNumber"));
					bSettings.LabelLaserAdjustment = Conversion.Val(rsLoad.Get_Fields_Double("LabelLaserAdjustment"));
					bSettings.LandscapeWarrant = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("LandscapeWarrant"));
					bSettings.LaserAdjustment = Conversion.Val(rsLoad.Get_Fields_Double("LaserAdjustment"));
					bSettings.LaserAdjustment1099 = Conversion.Val(rsLoad.Get_Fields_Double("1099LaserAdjustment"));
					bSettings.LaserAdjustment1099Horizontal = Conversion.Val(rsLoad.Get_Fields_Double("1099LaserAdjustmentHorizontal"));
					bSettings.Ledger = FCConvert.ToString(rsLoad.Get_Fields_String("Ledger"));
					bSettings.PayDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("PayDate"));
					bSettings.PrintSignature = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("PrintSignature"));
					bSettings.RecapOrderByAccounts = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("RecapOrderByAccounts"));
					bSettings.ReportAdjustment = Conversion.Val(rsLoad.Get_Fields_Double("ReportAdjustment"));
					bSettings.ReturnAddress1 = FCConvert.ToString(rsLoad.Get_Fields_String("ReturnAddress1"));
					bSettings.ReturnAddress2 = FCConvert.ToString(rsLoad.Get_Fields_String("ReturnAddress2"));
					bSettings.ReturnAddress3 = FCConvert.ToString(rsLoad.Get_Fields_String("ReturnAddress3"));
					bSettings.ReturnAddress4 = FCConvert.ToString(rsLoad.Get_Fields_String("ReturnAddress4"));
					bSettings.Revenue = FCConvert.ToString(rsLoad.Get_Fields_String("Revenue"));
					bSettings.SameVendorOnly = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("SameVendorOnly"));
					bSettings.ShadeReports = FCConvert.ToString(rsLoad.Get_Fields_String("ShadeReports"));
					bSettings.ShowIcon = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ShowIcon"));
					bSettings.StatementDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatementDate"));
					bSettings.SummaryReportOption = FCConvert.ToString(rsLoad.Get_Fields_String("SummaryReportOption"));
					bSettings.Use1099Laser = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("1099Laser"));
					bSettings.UseProjectNumber = FCConvert.ToString(rsLoad.Get_Fields_String("UseProjectNumber"));
					bSettings.UsePUCChartOfAccounts = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("PUCChartOfAccounts"));
					bSettings.UseTownSeal = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("UseTownSeal"));
					bSettings.ValidAccountBudgetProcess = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("ValidAccountBudgetProcess"));
					bSettings.ValidAccountUse = FCConvert.ToString(rsLoad.Get_Fields_String("ValidAccountUse"));
					bSettings.Version = FCConvert.ToString(rsLoad.Get_Fields_String("Version"));
					bSettings.WarrantPrevDeptSummary = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("WarrantPrevDeptSummary"));
					bSettings.WarrantPrevOutput = FCConvert.ToString(rsLoad.Get_Fields_String("WarrantPrevOutput"));
					bSettings.WarrantPrintOption = FCConvert.ToString(rsLoad.Get_Fields_String("WarrantPrintOption"));
					bSettings.IsUpdated = false;
				}
				GetBDSettings = bSettings;
				return GetBDSettings;
			}
			catch (Exception ex)
			{
				SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetBDSettings;
		}
	}
}
