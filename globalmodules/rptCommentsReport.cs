﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

#if TWCR0000
using TWCR0000;


#elif TWUT0000
using modGlobal = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for rptCommentsReport.
	/// </summary>
	public partial class rptCommentsReport : BaseSectionReport
	{
		// nObj = 1
		//   0	rptCommentsReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		clsDRWrapper rs = new clsDRWrapper();
		int lngAccount;
		// kk07282016 trout-1131 Add ability to filter by Date and Account Number
		DateTime dtStartDate;
		DateTime dtEndDate;
		int lngStartAcct;
		int lngEndAcct;

		public rptCommentsReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Comments Report";
            this.ReportEnd += RptCommentsReport_ReportEnd;
		}

        private void RptCommentsReport_ReportEnd(object sender, EventArgs e)
        {
            rs.DisposeOf();
        }

        public static rptCommentsReport InstancePtr
		{
			get
			{
				return (rptCommentsReport)Sys.GetInstance(typeof(rptCommentsReport));
			}
		}

		protected rptCommentsReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			rs.Dispose();
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rs.MoveNext();
				eArgs.EOF = rs.EndOfFile();
			}
		}

		public void Init(bool modalDialog, int lngAcct = -1)
		{
			lngAccount = lngAcct;
			if (lngAcct > 0)
			{
				frmReportViewer.InstancePtr.Init(this, string.Empty, FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
			}
			else
			{
				// kk06192017 trout-1252 Change so if user exits the setup form, report is canceled
				if (frmSetupCommentsReport.InstancePtr.Init(ref dtStartDate, ref dtEndDate, ref lngStartAcct, ref lngEndAcct))
				{
					frmReportViewer.InstancePtr.Init(this);
				}
				else
				{
					Cancel();
					this.Close();
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// kk07282016 trout-1131 Add ability to filter by Date and Account Number
			string strDateFilter = "";
			string strAcctFilter = "";
			string strTemp = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lblRptDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblRptTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			blnFirstRecord = true;
			if (lngAccount == -1)
			{
				strDateFilter = "";
				strAcctFilter = "";
				if (dtStartDate.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
				{
					// Set up  the date range filter
					strDateFilter = " AND ISNULL(Updated, '6/30/2016') >= '" + Strings.Format(dtStartDate, "MM/dd/yyyy") + "'";
					strTemp = "From " + Strings.Format(dtStartDate, "MM/dd/yyyy");
					if (dtEndDate.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
					{
						strDateFilter += " AND ISNULL(Updated, '6/30/2016') <= '" + Strings.Format(dtEndDate, "MM/dd/yyyy") + "'";
						strTemp += " to " + Strings.Format(dtEndDate, "MM/dd/yyyy");
					}
				}
				if (lngStartAcct != 0)
				{
					// Set up the account range filter
					strAcctFilter = " AND AccountNumber >= " + FCConvert.ToString(lngStartAcct);
					if (strTemp == "")
					{
						strTemp = "Accounts " + FCConvert.ToString(lngStartAcct);
					}
					else
					{
						strTemp += "; Accounts " + FCConvert.ToString(lngStartAcct);
					}
					if (lngEndAcct != 0)
					{
						strAcctFilter += " AND AccountNumber <= " + FCConvert.ToString(lngEndAcct);
						strTemp += " through " + FCConvert.ToString(lngEndAcct);
					}
				}
				if (strTemp != "")
				{
					lblSubtitle.Text = strTemp;
					lblSubtitle.Visible = true;
				}
				else
				{
					lblSubtitle.Visible = false;
				}
				rs.OpenRecordset("SELECT AccountNumber, pBill.FullNameLF AS Name, StreetNumber, StreetName, CommentRec.Comment " + "FROM Master INNER JOIN COmmentRec ON Master.ID = CommentRec.AccountKey INNER JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID " + "WHERE RTRIM(CommentRec.Comment) <> ''" + strDateFilter + strAcctFilter + " ORDER BY AccountNumber", modExtraModules.strUTDatabase);
			}
			else
			{
				rs.OpenRecordset("SELECT AccountNumber, pBill.FullNameLF AS Name, StreetNumber, StreetName, CommentRec.Comment " + "FROM Master INNER JOIN COmmentRec ON Master.ID = CommentRec.AccountKey INNER JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID " + "WHERE Master.ID = " + FCConvert.ToString(lngAccount) + " AND RTRIM(CommentRec.Comment & '') <> '' ORDER BY AccountNumber", modExtraModules.strUTDatabase);
			}
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
			fldAccount.Text = FCConvert.ToString(rs.Get_Fields("AccountNumber"));
			fldName.Text = FCConvert.ToString(rs.Get_Fields_String("Name"));
			if (!rs.IsFieldNull("StreetNumber"))
			{
				fldLocation.Text = FCConvert.ToString(rs.Get_Fields_Int32("StreetNumber")) + " " + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("StreetName")));
			}
			else
			{
				fldLocation.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("StreetName")));
			}
			fldComment.Text = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Comment")));
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNo.Text = "Page " + this.PageNumber;
		}

		private void rptCommentsReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCommentsReport.Caption	= "Comments Report";
			//rptCommentsReport.Icon	= "rptCommentsReport.dsx":0000";
			//rptCommentsReport.Left	= 0;
			//rptCommentsReport.Top	= 0;
			//rptCommentsReport.Width	= 15240;
			//rptCommentsReport.Height	= 11115;
			//rptCommentsReport.StartUpPosition	= 3;
			//rptCommentsReport.SectionData	= "rptCommentsReport.dsx":508A;
			//End Unmaped Properties
		}

		private void rptCommentsReport_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
