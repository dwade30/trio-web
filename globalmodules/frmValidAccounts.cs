﻿using System;
using fecherFoundation;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWAR0000
using TWAR0000;


#elif TWFA0000
using TWFA0000;


#elif TWBL0000
using TWBL0000;
using modGlobal = TWBL0000.modMDIParent;


#elif TWGNENTY
using TWGNENTY;
using modGlobal = TWGNENTY.modGlobalRoutines;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;


#elif TWPY0000
using modGlobal = TWPY0000.modGlobalRoutines;


#elif TWMV0000
using modGlobal = TWMV0000.modGlobalRoutines;
#endif
namespace Global
{
    /// <summary>
    /// Summary description for frmLoadValidAccounts.
    /// </summary>
    public partial class frmLoadValidAccounts : BaseForm
    {
        public frmLoadValidAccounts()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmLoadValidAccounts InstancePtr
        {
            get
            {
                return (frmLoadValidAccounts)Sys.GetInstance(typeof(frmLoadValidAccounts));
            }
        }

        protected frmLoadValidAccounts _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ************************************************
        // Property of TRIO Software Corporation          *
        // *
        // WRITTEN BY :           Dave Wade               *
        // DATE       :                                   *
        // *
        // MODIFIED BY:           Jim Bertolino           *
        // DATE       :           09/14/2006              *
        // *
        // ************************************************
        // Must have a global variable called gstrPassAccountFromForm
        // it is declared in monGlobalConstants
        // it will return the account in gstrPassAccountFromForm or through the init function
        // if it is cancelled or not account is selected then it will set the variable to ""
        clsDRWrapper rsDeptDiv = new clsDRWrapper();
        clsDRWrapper rsExpObj = new clsDRWrapper();
        clsDRWrapper rsRev = new clsDRWrapper();
        clsDRWrapper rsLedger = new clsDRWrapper();
        
        string strAccountNumber = "";
        string strDefaultAccount;
        int lngColAccount;
        int lngColDesc1;
        int lngColDesc2;
        int lngColDesc3;
        int lngColDesc4;
        
        string strAcctTypeToShow;

        public string Init(string strPassDefaultAccount, string strAccountTypeToShow = "")
        {
            string Init = "";
            try
            {

                // this function will show the form and return the account selected
                strDefaultAccount = strPassDefaultAccount;
                strAcctTypeToShow = strAccountTypeToShow;
                Show(FormShowEnum.Modal);
                Init = strAccountNumber;
                return Init;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                ShowErrorMessage(ex, "Init");
            }
            return Init;
        }

        private void frmLoadValidAccounts_Load(object sender, System.EventArgs e)
        {

            try
            {

                // col numbers
                lngColAccount = 0;
                lngColDesc1 = 1;
                lngColDesc2 = 2;
                lngColDesc3 = 3;
                lngColDesc4 = 4;
                modGlobalConstants.Statics.gstrPassAccountFromForm = "";
                strAccountNumber = "";
                rsDeptDiv.OpenRecordset("SELECT * FROM DeptDivTitles", "TWBD0000.vb1");
                rsExpObj.OpenRecordset("SELECT * FROM ExpObjTitles", "TWBD0000.vb1");
                rsLedger.OpenRecordset("SELECT * FROM LedgerTitles", "TWBD0000.vb1");
                rsRev.OpenRecordset("SELECT * FROM RevTitles", "TWBD0000.vb1");
               
                FormatGrid();
                vsAcct.TextMatrix(0, lngColAccount, "Account");
                vsAcct.TextMatrix(0, lngColDesc1, "Dept/Fund");
                vsAcct.TextMatrix(0, lngColDesc2, "Div/Acct");
				vsAcct.TextMatrix(0, lngColDesc3, "Rev/Exp");
                vsAcct.TextMatrix(0, lngColDesc4, "Obj");

                if (vsAcct.Enabled && vsAcct.Visible)
                {
                    vsAcct.Focus();
                }
                modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
                modGlobalFunctions.SetTRIOColors(this);

                GetData();
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                ShowErrorMessage(ex, "Form Load");
            }
        }

        private void frmLoadValidAccounts_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            try
            {

                // catches the escape and enter keys
                if (KeyAscii == 27)
                {
                    KeyAscii = 0;
                    Close();
                }
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                ShowErrorMessage(ex, "Form KeyPress");
                e.KeyChar = Strings.Chr(KeyAscii);
            }
        }

        private void frmLoadValidAccounts_Resize(object sender, System.EventArgs e)
        {
            try
            {

                FormatGrid();
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                ShowErrorMessage(ex, "Form Resize");
            }
        }

        private void mnuProcessQuit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void GetData()
        {
            int counter;
            string temp = "";

            //! Load frmWait; // show the wait form
            frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Building Valid Accounts List";
            frmWait.InstancePtr.Show(App.MainForm);
            frmWait.InstancePtr.Refresh();
        ContinueLoadingData:
            
        var rs = new clsDRWrapper();              

            try
            {
                if (strAcctTypeToShow == "")
                {
                    rs.OpenRecordset("SELECT * FROM AccountMaster WHERE Valid = 1 ORDER BY AccountType, FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField, FifthAccountField", "TWBD0000.vb1");
                }
                else
                {
                    rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = '" + strAcctTypeToShow + "' AND Valid = 1 ORDER BY AccountType, FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField, FifthAccountField ", "TWBD0000.vb1");
                }

                if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                {
                    rs.MoveLast();
                    rs.MoveFirst();
                    vsAcct.Rows = rs.RecordCount() + 1;
                    vsAcct.Row = 1;

                    for (counter = 1; counter <= rs.RecordCount(); counter++)
                    {
                        var accountNumber = FCConvert.ToString(rs.Get_Fields("Account"));

                        switch (FCConvert.ToString(rs.Get_Fields_String("AccountType")))
                        {
                            case "E":
                                GetExp(accountNumber);
                                
                                break;

                            case "R":
                                GetRev(accountNumber);

                                break;

                            case "G":
                                GetLedger(accountNumber);

                                break;
                        }


                        vsAcct.TextMatrix(vsAcct.Row, lngColAccount, accountNumber);

                        if (vsAcct.Row < vsAcct.Rows - 1)
                        {
                            vsAcct.Row += 1;
                        }

                        rs.MoveNext();
                    }

                    //FC:FINAL:BSE:#4154 focus should start at the top of the grid
                    vsAcct.Row = 1;

                    FindMatchingAccount();
                }

                frmWait.InstancePtr.Unload();
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                if (Information.Err(ex).Number == 401)
                {
                    goto ContinueLoadingData;
                }
                else
                {
                    ShowErrorMessage(ex, "Form Load");
                }
            }
            finally
            {
                rs.DisposeOf();
            }
        }

        private void GetExp(string x)
        {

            try
            {

                string Dept;
                string Div = "";
                string tempDiv;
                string tempObj;
                string Expense = "";
                string Object = "";
                string tempAccount;
                tempDiv = FormatExp(3);
                tempObj = FormatExp(7);
                tempAccount = Strings.Right(x, x.Length - 2);
                Dept = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2)));
                tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2)) + 1));
                //var expThree = FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2));
                //var expFive = FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2));
                //var expSeven = FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2));

               // var tempAccountNumber = Strings.Right(tempAccount, tempAccount.Length - expFive );
              // var tempAccountNumber = tempAccount.Right(tempAccount.Length - expFive - 1);
               var breakdownArray = tempAccount.Split('-');
                if (!modAccountTitle.Statics.ExpDivFlag)
                {
                    Div = breakdownArray[0];
                   // Div = Strings.Left(tempAccount.Replace("-",""), expThree);
                   Expense = breakdownArray[1];
                    //tempAccount = Strings.Right(tempAccount, tempAccount.Length - expThree );
                    //Expense = Strings.Left(tempAccount, expFive);
                    if (!modAccountTitle.Statics.ObjFlag)
                    {
                        //tempAccount = tempAccountNumber;
                        //Object = Strings.Right(tempAccount, expSeven);
                        Object = breakdownArray[2];
                    }
                }
                else
                {
                    Expense = breakdownArray[0];
                   // Expense = Strings.Left(tempAccount, expFive);
                    if (!modAccountTitle.Statics.ObjFlag)
                    {
                        Object = breakdownArray[1];
                        //tempAccount = tempAccountNumber;
                        //Object = Strings.Left(tempAccount.Replace("-",""), expSeven);
                    }
                }

                var deptDescription = FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription"));
                var expDescription = FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription"));

                if (!modAccountTitle.Statics.ExpDivFlag)
                {
                    rsDeptDiv.FindFirstRecord2("Department,Division", Dept + "," + tempDiv, ",");
                    if (rsDeptDiv.NoMatch != true)
                    {
	                    deptDescription = FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription"));
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, deptDescription);
                        rsDeptDiv.FindFirstRecord2("Department,Division", Dept + "," + Div, ",");
                        if (rsDeptDiv.NoMatch != true)
                        {
	                        deptDescription = FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription"));
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, deptDescription);
                            if (!modAccountTitle.Statics.ObjFlag)
                            {
                                rsExpObj.FindFirstRecord2("Expense,Object", Expense + "," + tempObj, ",");
                                if (rsExpObj.NoMatch != true)
                                {
	                                expDescription = FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription"));
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, expDescription);
                                    rsExpObj.FindFirstRecord2("Expense,Object", Expense + "," + Object, ",");
                                    if (rsExpObj.NoMatch != true)
                                    {
	                                    expDescription = FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription"));
										vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, expDescription);
                                    }
                                    else
                                    {
                                        SetMatrixToUndefined();
                                    }
                                }
                                else
                                {
                                    SetMatrixToUndefined();
                                }
                            }
                            else
                            {
                                rsExpObj.FindFirstRecord2("Expense,Object", Expense + "," + tempObj, ",");
                                if (rsExpObj.NoMatch != true)
                                {
	                                expDescription = FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription"));
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, expDescription);
                                }
                                else
                                {
                                    SetMatrixToUndefined();
                                }
                            }
                        }
                        else
                        {
                            SetMatrixToUndefined();
                        }
                    }
                    else
                    {
                        SetMatrixToUndefined();
                    }
                }
                else
                {
                    rsDeptDiv.FindFirstRecord2("Department,Division", Dept + "," + tempDiv, ",");
                    if (rsDeptDiv.NoMatch != true)
                    {
	                    deptDescription = FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription"));
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, FCConvert.ToString(deptDescription));
                        if (!modAccountTitle.Statics.ObjFlag)
                        {
                            rsExpObj.FindFirstRecord2("Expense,Object", Expense + "," + tempObj, ",");
                            if (rsExpObj.NoMatch != true)
                            {
	                            expDescription = FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription"));
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, expDescription);
                                rsExpObj.FindFirstRecord2("Expense,Object", Expense + "," + Object, ",");
                                if (rsExpObj.NoMatch != true)
                                {
	                                expDescription = FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription"));
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, expDescription);

                                }
                                else
                                {
                                    SetMatrixToUndefined();

                                }
                            }
                            else
                            {
                                SetMatrixToUndefined();

                            }
                        }
                        else
                        {
                            rsExpObj.FindFirstRecord2("Expense,Object", Expense + "," + tempObj, ",");
                            if (rsExpObj.NoMatch != true)
                            {
	                            expDescription = FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription"));
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, expDescription);

                            }
                            else
                            {
                                SetMatrixToUndefined();

                            }
                        }
                    }
                    else
                    {
                        SetMatrixToUndefined();

                    }
                }

            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                ShowErrorMessage(ex, "Get EXP");
            }
        }

        private void SetMatrixToUndefined(int numColumns = 4)
        {
            const string undefined = "Undefined";
            vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, undefined);
            vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, undefined);
            vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, undefined);
            vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, undefined);
        }

        private static string FormatExp(int start)
        {
            return modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, start, 2)))));
        }

        private void GetRev(string x)
        {
            try
            {

                clsDRWrapper rs2 = new clsDRWrapper();
                string Dept;
                string Div = "";
                string tempExpDiv;
                string Revenue = "";
                string tempAccount;
                tempExpDiv = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
                tempAccount = Strings.Right(x, x.Length - 2);
                Dept = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Rev, 2)));
                tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Rev, 2)) + 1));
                if (!modAccountTitle.Statics.RevDivFlag)
                {
                    Div = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)));
                    tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)) + 1));
                    Revenue = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)));
                }
                else
                {
                    Revenue = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)));
                }
                if (!modAccountTitle.Statics.RevDivFlag)
                {
                    rsDeptDiv.FindFirstRecord2("Department,Division", Dept + "," + tempExpDiv, ",");
                    if (rsDeptDiv.NoMatch != true)
                    {
                        vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
                        rsDeptDiv.FindFirstRecord2("Department,Division", Dept + "," + Div, ",");
                        if (rsDeptDiv.NoMatch != true)
                        {
                            vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
                            rsRev.FindFirstRecord2("Department,Division,Revenue", Dept + "," + Div + "," + Revenue, ",");
                            if (rsRev.NoMatch != true)
                            {
                                vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, FCConvert.ToString(rsRev.Get_Fields_String("ShortDescription")));

                            }
                            else
                            {
                                SetMatrixToUndefined();
                            }
                        }
                        else
                        {
                            SetMatrixToUndefined();
                        }
                    }
                    else
                    {
                        SetMatrixToUndefined();
                    }
                }
                else
                {
                    rsDeptDiv.FindFirstRecord2("Department,Division", Dept + "," + tempExpDiv, ",");
                    if (rsDeptDiv.NoMatch != true)
                    {
                        vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
                        rsRev.FindFirstRecord2("Department,Revenue", Dept + "," + Revenue, ",");
                        if (rsRev.NoMatch != true)
                        {
                            vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, FCConvert.ToString(rsRev.Get_Fields_String("ShortDescription")));

                        }
                        else
                        {
                            SetMatrixToUndefined();
                        }
                    }
                    else
                    {
                        SetMatrixToUndefined();
                    }
                }

            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                ShowErrorMessage(ex, "Get EXP");
            }
        }

        private void GetLedger(string x)
        {
            try
            {

                clsDRWrapper rs2 = new clsDRWrapper();
                string Fund;
                string Acct;
                string tempAcct;
                string Year = "";
                string tempAccount;
                tempAcct = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
                tempAccount = Strings.Right(x, x.Length - 2);
                Fund = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)));
                tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)) + 1));
                Acct = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)));
                if (!modAccountTitle.Statics.YearFlag)
                {
                    tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)) + 1));
                    Year = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2)));
                }
                rsLedger.FindFirstRecord2("Fund,Account", Fund + "," + tempAcct, ",");
                if (rsLedger.NoMatch != true)
                {
                    vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, FCConvert.ToString(rsLedger.Get_Fields_String("ShortDescription")));
                    if (!modAccountTitle.Statics.YearFlag)
                    {
                        rsLedger.FindFirstRecord2("Fund,Account,Year", Fund + "," + Acct + "," + Year, ",");
                        if (rsLedger.NoMatch != true)
                        {
                            vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, FCConvert.ToString(rsLedger.Get_Fields_String("ShortDescription")));

                        }
                        else
                        {
                            SetMatrixToUndefined();
                        }
                    }
                    else
                    {
                        rsLedger.FindFirstRecord2("Fund,Account", Fund + "," + Acct, ",");
                        if (rsLedger.NoMatch != true)
                        {
                            vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, FCConvert.ToString(rsLedger.Get_Fields_String("ShortDescription")));

                        }
                        else
                        {
                            SetMatrixToUndefined();
                        }
                    }
                }
                else
                {
                    SetMatrixToUndefined();
                }

            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                ShowErrorMessage(ex, "Get Ledger");
            }
        }

        private void mnuProcessSave_Click(object sender, System.EventArgs e)
        {
            vsAcct_DblClick(vsAcct, EventArgs.Empty);
        }

        private void vsAcct_Click(object sender, EventArgs e)
        {
            try
            {

                vsAcct.Select(vsAcct.Row, 0, vsAcct.Row, vsAcct.Cols - 1);
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                ShowErrorMessage(ex, "vsAcct_Click");
            }
        }

        private static void ShowErrorMessage(Exception ex, string methodName)
        {
            FCMessageBox.Show("Error #"
                              + FCConvert.ToString(Information.Err(ex)
                                                              .Number)
                              + " - "
                              + ex.GetBaseException()
                                  .Message
                              + ".", MsgBoxStyle.Critical, methodName);
        }

        private void vsAcct_DblClick(object sender, EventArgs e)
        {
            try
            {

                string strLabel;
                strLabel = vsAcct.TextMatrix(vsAcct.MouseRow, lngColAccount);
                modGlobalConstants.Statics.gstrPassAccountFromForm = strLabel;
                strAccountNumber = strLabel;
                Close();
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                ShowErrorMessage(ex, "vsAcct_DBLClick");
            }
        }

        private void vsAcct_KeyDown(object sender, KeyEventArgs e)
        {
            int KeyCode = e.KeyValue;
            try
            {

                switch (e.KeyCode)
                {
                    case Keys.Up:
                        {
                            if (vsAcct.Row == 1) KeyCode = 0;

                            break;
                        }

                    case Keys.Down:
                        {
                            if (vsAcct.Row == vsAcct.Rows - 1) KeyCode = 0;

                            break;
                        }

                    case Keys.E:
                        KeyCode = 0;
                        vsAcct.Row = 1;
                        vsAcct.TopRow = 1;

                        break;

                    case Keys.G:
                        {
                            KeyCode = 0;
                            SetRow("G");

                            break;
                        }

                    case Keys.R:
                        {
                            KeyCode = 0;
                            SetRow("R");

                            break;
                        }

                    case Keys.P:
                        {
                            KeyCode = 0;
                            SetRow("P");

                            break;
                        }

                    case Keys.V:
                        {
                            KeyCode = 0;
                            SetRow("V");

                            break;
                        }

                    case Keys.L:
                        {
                            KeyCode = 0;
                            SetRow("L");

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                ShowErrorMessage(ex, "vsAcct_KeyDown");
            }
        }

        private void SetRow(string searchFor)
        {
            for (var counter = 1; counter <= vsAcct.Rows - 1; counter++)
                if (Strings.Left(vsAcct.TextMatrix(counter, lngColAccount), 1) == searchFor)
                {
                    vsAcct.Row = counter;
                    vsAcct.TopRow = counter;

                    break;
                }
        }

        private void vsAcct_KeyPress(object sender, KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            string strLabel = "";
            if (KeyAscii == 13)
            {
                // enter ID
                strLabel = vsAcct.TextMatrix(vsAcct.Row, lngColAccount);
                modGlobalConstants.Statics.gstrPassAccountFromForm = strLabel;
                strAccountNumber = strLabel;
                Close();
            }
        }

        private void FormatGrid()
        {
            vsAcct.Cols = 5;
            var lngWid = vsAcct.WidthOriginal;
            vsAcct.ColWidth(lngColAccount, FCConvert.ToInt32(lngWid * 0.2936));
            vsAcct.ColWidth(lngColDesc1, FCConvert.ToInt32(lngWid * 0.1716));
            vsAcct.ColWidth(lngColDesc2, FCConvert.ToInt32(lngWid * 0.1716));
            vsAcct.ColWidth(lngColDesc3, FCConvert.ToInt32(lngWid * 0.1716));
            vsAcct.ColWidth(lngColDesc4, FCConvert.ToInt32(lngWid * 0.1716));
        }

        private void FindMatchingAccount()
        {
            try
            {

                // this routine will find an account similar to the one that is in the account box already
                int intLen;
                bool boolFound = false;
                int lngRow = 0;
                intLen = strDefaultAccount.Length;
                while (!(intLen <= 0 || boolFound))
                {
                    lngRow = vsAcct.FindRow(Strings.Left(strDefaultAccount, intLen), 1, lngColAccount, true, false);
                    if (lngRow > 0)
                    {
                        boolFound = true;
                    }
                    else
                    {
                        intLen -= 1;
                    }
                }
                if (boolFound)
                {
                    vsAcct.Select(lngRow, lngColAccount);
                    vsAcct.TopRow = lngRow;

                }
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                ShowErrorMessage(ex, "Error Finding Default Account");
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            mnuProcessSave_Click(sender, e);
        }

        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing) components?.Dispose();

            rsDeptDiv?.DisposeOf();
            rsExpObj?.DisposeOf();
            rsLedger?.DisposeOf();
            rsRev?.DisposeOf();

            base.Dispose(disposing);
        }
    }
}
