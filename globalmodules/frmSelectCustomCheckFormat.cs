﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using Global;
using fecherFoundation;

#if TWBD0000
using TWBD0000;


#elif TWPY0000
using modGlobal = TWPY0000.modGlobalRoutines;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmSelectCustomCheckFormat.
	/// </summary>
	public partial class frmSelectCustomCheckFormat : BaseForm
	{
		public frmSelectCustomCheckFormat()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSelectCustomCheckFormat InstancePtr
		{
			get
			{
				return (frmSelectCustomCheckFormat)Sys.GetInstance(typeof(frmSelectCustomCheckFormat));
			}
		}

		protected frmSelectCustomCheckFormat _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By         Dave Wade
		// Date               6/2/04
		// This form will be used if the town has set up multiple
		// custom check formats.  They will be asked to select the
		// custom check format they wish to use.  If there is only
		// 1 format setup the form will just set the variable for
		// the format to use and unload itself
		// ********************************************************
		private bool boolShowNonNegotiable;
		private int intNonNegotiableFormat;

		private void frmSelectCustomCheckFormat_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			cboCheckSelection.SelectedIndex = 0;
			this.Refresh();
		}

		private void frmSelectCustomCheckFormat_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmSelectCustomCheckFormat_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:VGE - #776 'Process' button sould do the same as save to actually update Custom printing type.
			this.mnuProcessSave_Click(sender, e);
			//Close();
		}
		// vbPorter upgrade warning: boolShowNonNegotiableFormat As Variant --> As bool
		// vbPorter upgrade warning: 'Return' As Variant --> As short	OnWriteFCConvert.ToInt32(
		public short Init(bool boolShowNonNegotiableFormat = false)
		{
			short Init = 0;
			clsDRWrapper rsCheckInfo = new clsDRWrapper();
			int intFormatID = 0;
			int counter;
			boolShowNonNegotiable = boolShowNonNegotiableFormat;
			intNonNegotiableFormat = 0;
			rsCheckInfo.OpenRecordset("SELECT * FROM CustomChecks ORDER BY Description");
			if (rsCheckInfo.RecordCount() == 1)
			{
				modCustomChecks.Statics.intCustomCheckFormatToUse = FCConvert.ToInt32(rsCheckInfo.Get_Fields_Int32("ID"));
				intNonNegotiableFormat = modCustomChecks.Statics.intCustomCheckFormatToUse;
				Init = FCConvert.ToInt16(modCustomChecks.Statics.intCustomCheckFormatToUse);
				Close();
			}
			else
			{
				cboCheckSelection.Clear();
				if (boolShowNonNegotiable)
				{
					lblNegotiable.Visible = true;
					lblNonNegotiable.Visible = true;
					cmbNonNegotiable.Visible = true;
					cmbNonNegotiable.Clear();
				}
				else
				{
					lblNegotiable.Visible = false;
					lblNonNegotiable.Visible = false;
					cmbNonNegotiable.Visible = false;
				}
				do
				{
					cboCheckSelection.AddItem(Strings.Format(rsCheckInfo.Get_Fields_Int32("ID"), "00") + " - " + rsCheckInfo.Get_Fields_String("Description"));
					cboCheckSelection.ItemData(cboCheckSelection.NewIndex, FCConvert.ToInt32(rsCheckInfo.Get_Fields_Int32("ID")));
					if (boolShowNonNegotiable)
					{
						cmbNonNegotiable.AddItem(Strings.Format(rsCheckInfo.Get_Fields_Int32("ID"), "00") + " - " + rsCheckInfo.Get_Fields_String("Description"));
						cmbNonNegotiable.ItemData(cmbNonNegotiable.NewIndex, FCConvert.ToInt32(rsCheckInfo.Get_Fields_Int32("ID")));
					}
					rsCheckInfo.MoveNext();
				}
				while (rsCheckInfo.EndOfFile() != true);
				intFormatID = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("PYCustomCheckFormat", "PY"))));
				if (intFormatID > 0)
				{
					for (counter = 0; counter <= cboCheckSelection.Items.Count - 1; counter++)
					{
						if (intFormatID == cboCheckSelection.ItemData(counter))
						{
							cboCheckSelection.SelectedIndex = counter;
							break;
						}
					}
				}
				if (boolShowNonNegotiable)
				{
					intFormatID = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("PYNonNegotiableCustomCheckFormat", "PY"))));
					if (intFormatID > 0)
					{
						for (counter = 0; counter <= cmbNonNegotiable.Items.Count - 1; counter++)
						{
							if (intFormatID == cmbNonNegotiable.ItemData(counter))
							{
								cmbNonNegotiable.SelectedIndex = counter;
								break;
							}
						}
					}
				}
				this.Show(FormShowEnum.Modal, App.MainForm);
				if (!boolShowNonNegotiable)
				{
					Init = FCConvert.ToInt16(modCustomChecks.Statics.intCustomCheckFormatToUse);
				}
				else
				{
					Init = FCConvert.ToInt16(intNonNegotiableFormat);
				}
			}
			return Init;
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (cboCheckSelection.SelectedIndex != -1)
			{
			}
			else
			{
				MessageBox.Show("You must select a check format before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (boolShowNonNegotiable)
			{
				if (cmbNonNegotiable.SelectedIndex < 0)
				{
					MessageBox.Show("You must select a non-negotiable format before you may proceed.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			modCustomChecks.Statics.intCustomCheckFormatToUse = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboCheckSelection.Text, 2))));
			modRegistry.SaveRegistryKey("PYCustomCheckFormat", FCConvert.ToString(modCustomChecks.Statics.intCustomCheckFormatToUse), "PY");
			if (boolShowNonNegotiable)
			{
				intNonNegotiableFormat = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbNonNegotiable.Text, 2))));
				modRegistry.SaveRegistryKey("PYNonNegotiableCustomCheckFormat", FCConvert.ToString(intNonNegotiableFormat), "PY");
			}
			Close();
		}
	}
}
