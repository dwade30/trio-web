﻿//Fecher vbPorter - Version 1.0.0.32
using fecherFoundation;

namespace Global
{
	public class cRestoreFileDetails
	{
		//=========================================================
		private int lngFileNumber;
		private string strBackupType = string.Empty;
		private string strDescription = string.Empty;
		private string strBackupDate = "";
		private string strDatabaseName = string.Empty;

		public string DatabaseName
		{
			set
			{
				strDatabaseName = value;
			}
			get
			{
				string DatabaseName = "";
				DatabaseName = strDatabaseName;
				return DatabaseName;
			}
		}

		public int FileNumber
		{
			set
			{
				lngFileNumber = value;
			}
			get
			{
				int FileNumber = 0;
				FileNumber = lngFileNumber;
				return FileNumber;
			}
		}

		public string BackupType
		{
			set
			{
				strBackupType = value;
			}
			get
			{
				string BackupType = "";
				BackupType = strBackupType;
				return BackupType;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string BackupDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strBackupDate = value;
				}
				else
				{
					strBackupDate = "";
				}
			}
			get
			{
				string BackupDate = "";
				BackupDate = strBackupDate;
				return BackupDate;
			}
		}
	}
}
