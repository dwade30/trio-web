﻿namespace Global
{
	public class modCentralParties
	{
		//=========================================================
		public const int gintNumberOfModuleTypes = 16;
		public const int CNSTCentralPartiesEdit = 2;
		// VBto upgrade warning: intModuleIndex As short	OnWriteFCConvert.ToInt32(
		// VBto upgrade warning: 'Return' As Variant --> As string
		public static string GetModuleCode(int intModuleIndex)
		{
			string GetModuleCode = "";
			switch (intModuleIndex)
			{
				case 1:
					{
						GetModuleCode = "AR";
						break;
					}
				case 2:
					{
						GetModuleCode = "BD";
						break;
					}
				case 3:
					{
						GetModuleCode = "GN";
						break;
					}
				case 4:
					{
						GetModuleCode = "MV";
						break;
					}
				case 5:
					{
						GetModuleCode = "RE";
						break;
					}
				case 6:
					{
						GetModuleCode = "RB";
						break;
					}
				case 7:
					{
						GetModuleCode = "PP";
						break;
					}
				case 8:
					{
						GetModuleCode = "E9";
						break;
					}
				case 9:
					{
						GetModuleCode = "CK";
						break;
					}
				case 10:
					{
						GetModuleCode = "CR";
						break;
					}
				case 11:
					{
						GetModuleCode = "CL";
						break;
					}
				case 12:
					{
						GetModuleCode = "CE";
						break;
					}
				case 13:
					{
						GetModuleCode = "PY";
						break;
					}
				case 14:
					{
						GetModuleCode = "UT";
						break;
					}
				case 15:
					{
						GetModuleCode = "VR";
						break;
					}
				case 16:
					{
						GetModuleCode = "FA";
						break;
					}
				default:
					{
						GetModuleCode = "";
						break;
					}
			}
			//end switch
			return GetModuleCode;
		}
		// VBto upgrade warning: intModuleIndex As short	OnWriteFCConvert.ToInt32(
		// VBto upgrade warning: 'Return' As Variant --> As string
		public static string GetModuleDescription(int intModuleIndex)
		{
			string GetModuleDescription = "";
			switch (intModuleIndex)
			{
				case 1:
					{
						GetModuleDescription = "Accounts Receivable";
						break;
					}
				case 2:
					{
						GetModuleDescription = "Budgetary";
						break;
					}
				case 3:
					{
						GetModuleDescription = "General Entry";
						break;
					}
				case 4:
					{
						GetModuleDescription = "Motor Vehicle";
						break;
					}
				case 5:
					{
						GetModuleDescription = "Real Estate";
						break;
					}
				case 6:
					{
						GetModuleDescription = "Redbook";
						break;
					}
				case 7:
					{
						GetModuleDescription = "Personal Property";
						break;
					}
				case 8:
					{
						GetModuleDescription = "E911";
						break;
					}
				case 9:
					{
						GetModuleDescription = "Clerk";
						break;
					}
				case 10:
					{
						GetModuleDescription = "Cash Receipts";
						break;
					}
				case 11:
					{
						GetModuleDescription = "Collections";
						break;
					}
				case 12:
					{
						GetModuleDescription = "Code Enforcement";
						break;
					}
				case 13:
					{
						GetModuleDescription = "Payroll";
						break;
					}
				case 14:
					{
						GetModuleDescription = "Utility Billing";
						break;
					}
				case 15:
					{
						GetModuleDescription = "Voter Registration";
						break;
					}
				case 16:
					{
						GetModuleDescription = "Fixed Assets";
						break;
					}
				default:
					{
						GetModuleDescription = "";
						break;
					}
			}
			//end switch
			return GetModuleDescription;
		}
		// VBto upgrade warning: 'Return' As Variant --> As string
		public static string GetModuleDescriptionByCode_2(string strModuleCode)
		{
			return GetModuleDescriptionByCode(ref strModuleCode);
		}

		public static string GetModuleDescriptionByCode(ref string strModuleCode)
		{
			string GetModuleDescriptionByCode = "";
			if (strModuleCode == "AR")
			{
				GetModuleDescriptionByCode = "Accounts Receivable";
			}
			else if (strModuleCode == "BD")
			{
				GetModuleDescriptionByCode = "Budgetary";
			}
			else if (strModuleCode == "GN")
			{
				GetModuleDescriptionByCode = "General Entry";
			}
			else if (strModuleCode == "MV")
			{
				GetModuleDescriptionByCode = "Motor Vehicle";
			}
			else if (strModuleCode == "RE")
			{
				GetModuleDescriptionByCode = "Real Estate";
			}
			else if (strModuleCode == "RB")
			{
				GetModuleDescriptionByCode = "Redbook";
			}
			else if (strModuleCode == "PP")
			{
				GetModuleDescriptionByCode = "Personal Property";
			}
			else if (strModuleCode == "E9")
			{
				GetModuleDescriptionByCode = "E911";
			}
			else if (strModuleCode == "CK")
			{
				GetModuleDescriptionByCode = "Clerk";
			}
			else if (strModuleCode == "CR")
			{
				GetModuleDescriptionByCode = "Cash Receipts";
			}
			else if (strModuleCode == "CL")
			{
				GetModuleDescriptionByCode = "Collections";
			}
			else if (strModuleCode == "CE")
			{
				GetModuleDescriptionByCode = "Code Enforcement";
			}
			else if (strModuleCode == "PY")
			{
				GetModuleDescriptionByCode = "Payroll";
			}
			else if (strModuleCode == "UT")
			{
				GetModuleDescriptionByCode = "Utility Billing";
			}
			else if (strModuleCode == "VR")
			{
				GetModuleDescriptionByCode = "Voter Registration";
			}
			else if (strModuleCode == "FA")
			{
				GetModuleDescriptionByCode = "Fixed Assets";
			}
			else
			{
				GetModuleDescriptionByCode = "";
			}
			return GetModuleDescriptionByCode;
		}

		public static bool IsModuleActive_2(string strModuleCode)
		{
			return IsModuleActive(ref strModuleCode);
		}

		public static bool IsModuleActive(ref string strModuleCode)
		{
			bool IsModuleActive = false;
			if (strModuleCode == "AR")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolAR;
			}
			else if (strModuleCode == "BD")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolBD;
			}
			else if (strModuleCode == "GN")
			{
				IsModuleActive = true;
			}
			else if (strModuleCode == "MV")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolMV;
			}
			else if (strModuleCode == "RE")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolRE;
			}
			else if (strModuleCode == "RB")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolRB;
			}
			else if (strModuleCode == "PP")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolPP;
			}
			else if (strModuleCode == "E9")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolE9;
			}
			else if (strModuleCode == "CK")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolCK;
			}
			else if (strModuleCode == "CR")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolCR;
			}
			else if (strModuleCode == "CL")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolCL;
			}
			else if (strModuleCode == "CE")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolCE;
			}
			else if (strModuleCode == "PY")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolPY;
			}
			else if (strModuleCode == "UT")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolUT;
			}
			else if (strModuleCode == "VR")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolVR;
			}
			else if (strModuleCode == "FA")
			{
				IsModuleActive = modGlobalConstants.Statics.gboolFA;
			}
			else
			{
				IsModuleActive = false;
			}
			return IsModuleActive;
		}

		public static int GetModuleIndex(string strModuleCode)
		{
			int GetModuleIndex;
			switch (strModuleCode.ToLower())
			{
				case "ar":
					GetModuleIndex = 1;
					break;
				case "bd":
					GetModuleIndex = 2;
					break;
				case "gn":
					GetModuleIndex = 3;
					break;
				case "mv":
					GetModuleIndex = 4;
					break;
				case "re":
					GetModuleIndex = 5;
					break;
				case "rb":
					GetModuleIndex = 6;
					break;
				case "pp":
					GetModuleIndex = 7;
					break;
				case "e9":
					GetModuleIndex = 8;
					break;
				case "ck":
					GetModuleIndex = 9;
					break;
				case "cr":
					GetModuleIndex = 10;
					break;
				case "cl":
					GetModuleIndex = 11;
					break;
				case "ce":
					GetModuleIndex = 12;
					break;
				case "py":
					GetModuleIndex = 13;
					break;
				case "ut":
					GetModuleIndex = 14;
					break;
				case "vr":
					GetModuleIndex = 15;
					break;
				case "fa":
					GetModuleIndex = 16;
					break;
				default:
					GetModuleIndex = 0;
					break;
			}
			return GetModuleIndex;
		}
	}
}
