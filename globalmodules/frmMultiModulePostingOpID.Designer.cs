﻿using fecherFoundation;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmMultiModulePostingOpID.
	/// </summary>
	partial class frmMultiModulePostingOpID : BaseForm
	{
		public fecherFoundation.FCTextBox txtOpId;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMultiModulePostingOpID));
            this.txtOpId = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 105);
            this.BottomPanel.Size = new System.Drawing.Size(251, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtOpId);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(271, 261);
            this.ClientArea.Controls.SetChildIndex(this.Label1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtOpId, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(271, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(129, 28);
            this.HeaderText.Text = "Operator ID";
            // 
            // txtOpId
            // 
            this.txtOpId.AutoComplete = Wisej.Web.AutoComplete.Off;
            this.txtOpId.BackColor = System.Drawing.SystemColors.Window;
            this.txtOpId.Location = new System.Drawing.Point(30, 65);
            this.txtOpId.MaxLength = 3;
            this.txtOpId.Name = "txtOpId";
            this.txtOpId.Size = new System.Drawing.Size(188, 40);
            this.txtOpId.TabIndex = 0;
            this.txtOpId.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(195, 15);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "PLEASE INPUT YOUR INITIALS";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmMultiModulePostingOpID
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(271, 321);
            this.ControlBox = false;
            this.KeyPreview = true;
            this.Name = "frmMultiModulePostingOpID";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Operator ID";
            this.Load += new System.EventHandler(this.frmMultiModulePostingOpID_Load);
            this.Activated += new System.EventHandler(this.frmMultiModulePostingOpID_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMultiModulePostingOpID_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion
	}
}
