﻿namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class rptUTMortgage
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptUTMortgage));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtaccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtEscrow = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtBill = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHolderNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHolderName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.fldTotalTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHolder = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotalDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.txtaccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEscrow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHolderNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHolderName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHolder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtaccount,
				this.txtName,
				this.txtEscrow,
				this.txtBill,
				this.txtHolderNumber,
				this.txtHolderName,
				this.fldTotalDue
			});
			this.Detail.Height = 0.1875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtaccount
			// 
			this.txtaccount.Height = 0.1875F;
			this.txtaccount.Left = 0F;
			this.txtaccount.MultiLine = false;
			this.txtaccount.Name = "txtaccount";
			this.txtaccount.Style = "font-family: \'Tahoma\'";
			this.txtaccount.Text = "Field1";
			this.txtaccount.Top = 0F;
			this.txtaccount.Width = 0.625F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.625F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'";
			this.txtName.Text = "Field1";
			this.txtName.Top = 0F;
			this.txtName.Width = 2.125F;
			// 
			// txtEscrow
			// 
			this.txtEscrow.Height = 0.1875F;
			this.txtEscrow.Left = 2.75F;
			this.txtEscrow.MultiLine = false;
			this.txtEscrow.Name = "txtEscrow";
			this.txtEscrow.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtEscrow.Text = "Field1";
			this.txtEscrow.Top = 0F;
			this.txtEscrow.Width = 0.5F;
			// 
			// txtBill
			// 
			this.txtBill.Height = 0.1875F;
			this.txtBill.Left = 3.25F;
			this.txtBill.MultiLine = false;
			this.txtBill.Name = "txtBill";
			this.txtBill.Style = "font-family: \'Tahoma\'; text-align: center";
			this.txtBill.Text = "Field2";
			this.txtBill.Top = 0F;
			this.txtBill.Width = 0.4375F;
			// 
			// txtHolderNumber
			// 
			this.txtHolderNumber.Height = 0.1875F;
			this.txtHolderNumber.Left = 3.6875F;
			this.txtHolderNumber.MultiLine = false;
			this.txtHolderNumber.Name = "txtHolderNumber";
			this.txtHolderNumber.Style = "font-family: \'Tahoma\'";
			this.txtHolderNumber.Text = "Field3";
			this.txtHolderNumber.Top = 0F;
			this.txtHolderNumber.Width = 0.5F;
			// 
			// txtHolderName
			// 
			this.txtHolderName.Height = 0.1875F;
			this.txtHolderName.Left = 4.1875F;
			this.txtHolderName.MultiLine = false;
			this.txtHolderName.Name = "txtHolderName";
			this.txtHolderName.Style = "font-family: \'Tahoma\'";
			this.txtHolderName.Text = "Field4";
			this.txtHolderName.Top = 0F;
			this.txtHolderName.Width = 2.375F;
			// 
			// fldTotalDue
			// 
			this.fldTotalDue.Height = 0.1875F;
			this.fldTotalDue.Left = 6.5625F;
			this.fldTotalDue.MultiLine = false;
			this.fldTotalDue.Name = "fldTotalDue";
			this.fldTotalDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalDue.Text = null;
			this.fldTotalDue.Top = 0F;
			this.fldTotalDue.Width = 0.9375F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalTotalDue,
				this.Line1
			});
			this.ReportFooter.Height = 0.375F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// fldTotalTotalDue
			// 
			this.fldTotalTotalDue.Height = 0.1875F;
			this.fldTotalTotalDue.Left = 6.25F;
			this.fldTotalTotalDue.MultiLine = false;
			this.fldTotalTotalDue.Name = "fldTotalTotalDue";
			this.fldTotalTotalDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTotalDue.Text = null;
			this.fldTotalTotalDue.Top = 0.125F;
			this.fldTotalTotalDue.Width = 1.25F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 6.3125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.0625F;
			this.Line1.Width = 1.1875F;
			this.Line1.X1 = 6.3125F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 0.0625F;
			this.Line1.Y2 = 0.0625F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniname,
				this.txtDate,
				this.txtTitle,
				this.txtPage,
				this.lblPage,
				this.lblAccount,
				this.lblName,
				this.lblHolder,
				this.Label1,
				this.Label2,
				this.lblNumber,
				this.txtTime,
				this.lblTotalDue,
				this.Line2
			});
			this.PageHeader.Height = 0.9479167F;
			this.PageHeader.Name = "PageHeader";
			// 
			// txtMuniname
			// 
			this.txtMuniname.Height = 0.1875F;
			this.txtMuniname.Left = 0F;
			this.txtMuniname.MultiLine = false;
			this.txtMuniname.Name = "txtMuniname";
			this.txtMuniname.Style = "font-family: \'Tahoma\'";
			this.txtMuniname.Text = null;
			this.txtMuniname.Top = 0F;
			this.txtMuniname.Width = 2.6875F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 6.4375F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.0625F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.25F;
			this.txtTitle.Left = 0F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Utility Account List with Mortgage Holders";
			this.txtTitle.Top = 0F;
			this.txtTitle.Width = 7.5F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 7.0625F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPage.Text = null;
			this.txtPage.Top = 0.1875F;
			this.txtPage.Width = 0.4375F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.4375F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblPage.Text = "Page";
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 0.625F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.MultiLine = false;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 0.75F;
			this.lblAccount.Width = 0.625F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.625F;
			this.lblName.MultiLine = false;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.75F;
			this.lblName.Width = 1.1875F;
			// 
			// lblHolder
			// 
			this.lblHolder.Height = 0.1875F;
			this.lblHolder.HyperLink = null;
			this.lblHolder.Left = 4.1875F;
			this.lblHolder.MultiLine = false;
			this.lblHolder.Name = "lblHolder";
			this.lblHolder.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblHolder.Text = "Mortgage Holder";
			this.lblHolder.Top = 0.75F;
			this.lblHolder.Width = 1.3125F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.6875F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label1.Text = "Escrow";
			this.Label1.Top = 0.75F;
			this.Label1.Width = 0.5625F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 3.25F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.Label2.Text = "Bill";
			this.Label2.Top = 0.75F;
			this.Label2.Width = 0.4375F;
			// 
			// lblNumber
			// 
			this.lblNumber.Height = 0.1875F;
			this.lblNumber.HyperLink = null;
			this.lblNumber.Left = 3.6875F;
			this.lblNumber.Name = "lblNumber";
			this.lblNumber.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblNumber.Text = "MH #";
			this.lblNumber.Top = 0.75F;
			this.lblNumber.Width = 0.5F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.1875F;
			this.txtTime.Left = 0F;
			this.txtTime.MultiLine = false;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.1875F;
			this.txtTime.Width = 1.6875F;
			// 
			// lblTotalDue
			// 
			this.lblTotalDue.Height = 0.5F;
			this.lblTotalDue.HyperLink = null;
			this.lblTotalDue.Left = 6.6875F;
			this.lblTotalDue.Name = "lblTotalDue";
			this.lblTotalDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTotalDue.Text = "Total Due as of";
			this.lblTotalDue.Top = 0.4375F;
			this.lblTotalDue.Width = 0.8125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.9375F;
			this.Line2.Width = 7.5F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7.5F;
			this.Line2.Y1 = 0.9375F;
			this.Line2.Y2 = 0.9375F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptUTMortgage
			//
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.txtaccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEscrow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHolderNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHolderName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHolder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtEscrow;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBill;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHolderNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHolderName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHolder;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
