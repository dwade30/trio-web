﻿namespace Global
{
	/// <summary>
	/// Summary description for arUTMeterDetail.
	/// </summary>
	partial class arUTMeterDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arUTMeterDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldBookSeq = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMeterSize = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAvgConsumption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFrequency = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMeterDigits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblBookSeq = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMeterSize = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMeterDigits = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFreq = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAvgConsumption = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldService = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblService = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldRateCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblRateCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSerial = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblSerial = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCombine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCombine = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMultiplier = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMultiplier = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldRemote = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblRemote = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldReplacement = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblReplacement = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTWRT2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableWType2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBillTableWRT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableWType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTWRT3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableWType3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTWRT4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableWType4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTWRT1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableWType1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTWRT5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableWType5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTWAmt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBTWAmt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBTWAmt3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBTWAmt4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBTWAmt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBTWAmt5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableW1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTSRT2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableSType2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBillTableSRT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableSType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTSRT3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableSType3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTSRT4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableSType4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTSRT1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableSType1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTSRT5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableSType5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTSAmt2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillTableSAmt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBTSAmt3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBTSAmt4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBTSAmt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBTSAmt5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableS1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTaxableW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTaxable = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBillableW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillable = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTaxableS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillableS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblPercentTableW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPercentTableS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTWRTAdj = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableWAdj = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTWAmtAdj = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBTSRTAdj = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillTableSTypeAdj = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBTSAmtAdj = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAdjustDesc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAdjustDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.srptMeterBillingDetail = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOwner = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTypeCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblREAcctTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRENumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAddress4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBillTo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccountTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBillToTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOwnerTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLocationTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTypeCodeTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMapLotTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSeq)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMeterSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAvgConsumption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFrequency)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMeterDigits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBookSeq)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMeterSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMeterDigits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFreq)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAvgConsumption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldService)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblService)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRateCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRateCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSerial)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSerial)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCombine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCombine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMultiplier)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMultiplier)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemote)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRemote)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReplacement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReplacement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWRT2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableWType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillTableWRT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableWType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWRT3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableWType3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWRT4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableWType4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWRT1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableWType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWRT5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableWType5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWAmt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWAmt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWAmt3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWAmt4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWAmt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWAmt5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableW1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSRT2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableSType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillTableSRT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableSType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSRT3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableSType3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSRT4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableSType4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSRT1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableSType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSRT5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableSType5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSAmt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillTableSAmt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSAmt3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSAmt4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSAmt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSAmt5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableS1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxableW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillableW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxableS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillableS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPercentTableW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPercentTableS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWRTAdj)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableWAdj)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWAmtAdj)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSRTAdj)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableSTypeAdj)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSAmtAdj)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAdjustDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTypeCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblREAcctTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRENumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillToTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOwnerTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocationTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTypeCodeTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMapLotTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldBookSeq,
            this.fldMeterSize,
            this.fldAvgConsumption,
            this.fldFrequency,
            this.fldMeterDigits,
            this.lnTotals,
            this.lblBookSeq,
            this.lblMeterSize,
            this.lblMeterDigits,
            this.lblFreq,
            this.lblAvgConsumption,
            this.fldService,
            this.lblService,
            this.fldRateCode,
            this.lblRateCode,
            this.fldSerial,
            this.lblSerial,
            this.fldCombine,
            this.lblCombine,
            this.fldMultiplier,
            this.lblMultiplier,
            this.fldRemote,
            this.lblRemote,
            this.fldReplacement,
            this.lblReplacement,
            this.fldBTWRT2,
            this.lblBillTableWType2,
            this.fldBillTableWRT,
            this.lblBillTableWType,
            this.fldBTWRT3,
            this.lblBillTableWType3,
            this.fldBTWRT4,
            this.lblBillTableWType4,
            this.fldBTWRT1,
            this.lblBillTableWType1,
            this.fldBTWRT5,
            this.lblBillTableWType5,
            this.fldBTWAmt2,
            this.fldBTWAmt,
            this.fldBTWAmt3,
            this.fldBTWAmt4,
            this.fldBTWAmt1,
            this.fldBTWAmt5,
            this.lblBillTableW1,
            this.fldBTSRT2,
            this.lblBillTableSType2,
            this.fldBillTableSRT,
            this.lblBillTableSType,
            this.fldBTSRT3,
            this.lblBillTableSType3,
            this.fldBTSRT4,
            this.lblBillTableSType4,
            this.fldBTSRT1,
            this.lblBillTableSType1,
            this.fldBTSRT5,
            this.lblBillTableSType5,
            this.fldBTSAmt2,
            this.fldBillTableSAmt,
            this.fldBTSAmt3,
            this.fldBTSAmt4,
            this.fldBTSAmt1,
            this.fldBTSAmt5,
            this.lblBillTableS1,
            this.fldTaxableW,
            this.lblTaxable,
            this.fldBillableW,
            this.lblBillable,
            this.fldTaxableS,
            this.fldBillableS,
            this.lblPercentTableW,
            this.lblPercentTableS,
            this.fldBTWRTAdj,
            this.lblBillTableWAdj,
            this.fldBTWAmtAdj,
            this.fldBTSRTAdj,
            this.lblBillTableSTypeAdj,
            this.fldBTSAmtAdj,
            this.lblAdjustDesc,
            this.fldAdjustDesc,
            this.srptMeterBillingDetail});
			this.Detail.Height = 2.197917F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldBookSeq
			// 
			this.fldBookSeq.Height = 0.1875F;
			this.fldBookSeq.Left = 0.875F;
			this.fldBookSeq.Name = "fldBookSeq";
			this.fldBookSeq.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldBookSeq.Text = null;
			this.fldBookSeq.Top = 0.1875F;
			this.fldBookSeq.Width = 0.8125F;
			// 
			// fldMeterSize
			// 
			this.fldMeterSize.Height = 0.1875F;
			this.fldMeterSize.Left = 0.875F;
			this.fldMeterSize.Name = "fldMeterSize";
			this.fldMeterSize.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldMeterSize.Text = null;
			this.fldMeterSize.Top = 0.375F;
			this.fldMeterSize.Width = 0.8125F;
			// 
			// fldAvgConsumption
			// 
			this.fldAvgConsumption.Height = 0.1875F;
			this.fldAvgConsumption.Left = 3F;
			this.fldAvgConsumption.Name = "fldAvgConsumption";
			this.fldAvgConsumption.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldAvgConsumption.Text = null;
			this.fldAvgConsumption.Top = 0.5625F;
			this.fldAvgConsumption.Width = 1.3125F;
			// 
			// fldFrequency
			// 
			this.fldFrequency.Height = 0.1875F;
			this.fldFrequency.Left = 0.875F;
			this.fldFrequency.Name = "fldFrequency";
			this.fldFrequency.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldFrequency.Text = null;
			this.fldFrequency.Top = 0.75F;
			this.fldFrequency.Width = 0.8125F;
			// 
			// fldMeterDigits
			// 
			this.fldMeterDigits.Height = 0.1875F;
			this.fldMeterDigits.Left = 0.875F;
			this.fldMeterDigits.Name = "fldMeterDigits";
			this.fldMeterDigits.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldMeterDigits.Text = null;
			this.fldMeterDigits.Top = 0.5625F;
			this.fldMeterDigits.Width = 0.8125F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 1F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0.0625F;
			this.lnTotals.Visible = false;
			this.lnTotals.Width = 5.5625F;
			this.lnTotals.X1 = 1F;
			this.lnTotals.X2 = 6.5625F;
			this.lnTotals.Y1 = 0.0625F;
			this.lnTotals.Y2 = 0.0625F;
			// 
			// lblBookSeq
			// 
			this.lblBookSeq.Height = 0.1875F;
			this.lblBookSeq.HyperLink = null;
			this.lblBookSeq.Left = 0F;
			this.lblBookSeq.Name = "lblBookSeq";
			this.lblBookSeq.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBookSeq.Text = "Book / Seq:";
			this.lblBookSeq.Top = 0.1875F;
			this.lblBookSeq.Width = 0.875F;
			// 
			// lblMeterSize
			// 
			this.lblMeterSize.Height = 0.1875F;
			this.lblMeterSize.HyperLink = null;
			this.lblMeterSize.Left = 0F;
			this.lblMeterSize.Name = "lblMeterSize";
			this.lblMeterSize.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblMeterSize.Text = "Meter Size:";
			this.lblMeterSize.Top = 0.375F;
			this.lblMeterSize.Width = 0.875F;
			// 
			// lblMeterDigits
			// 
			this.lblMeterDigits.Height = 0.1875F;
			this.lblMeterDigits.HyperLink = null;
			this.lblMeterDigits.Left = 0F;
			this.lblMeterDigits.Name = "lblMeterDigits";
			this.lblMeterDigits.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblMeterDigits.Text = "Meter Digits:";
			this.lblMeterDigits.Top = 0.5625F;
			this.lblMeterDigits.Width = 0.875F;
			// 
			// lblFreq
			// 
			this.lblFreq.Height = 0.1875F;
			this.lblFreq.HyperLink = null;
			this.lblFreq.Left = 0F;
			this.lblFreq.Name = "lblFreq";
			this.lblFreq.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblFreq.Text = "Frequency:";
			this.lblFreq.Top = 0.75F;
			this.lblFreq.Width = 0.875F;
			// 
			// lblAvgConsumption
			// 
			this.lblAvgConsumption.Height = 0.1875F;
			this.lblAvgConsumption.HyperLink = null;
			this.lblAvgConsumption.Left = 1.75F;
			this.lblAvgConsumption.Name = "lblAvgConsumption";
			this.lblAvgConsumption.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblAvgConsumption.Text = "Avg Consumption:";
			this.lblAvgConsumption.Top = 0.5625F;
			this.lblAvgConsumption.Width = 1.25F;
			// 
			// fldService
			// 
			this.fldService.Height = 0.1875F;
			this.fldService.Left = 0.875F;
			this.fldService.Name = "fldService";
			this.fldService.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldService.Text = null;
			this.fldService.Top = 0.9375F;
			this.fldService.Width = 0.8125F;
			// 
			// lblService
			// 
			this.lblService.Height = 0.1875F;
			this.lblService.HyperLink = null;
			this.lblService.Left = 0F;
			this.lblService.Name = "lblService";
			this.lblService.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblService.Text = "Service:";
			this.lblService.Top = 0.9375F;
			this.lblService.Width = 0.875F;
			// 
			// fldRateCode
			// 
			this.fldRateCode.Height = 0.1875F;
			this.fldRateCode.Left = 0.875F;
			this.fldRateCode.Name = "fldRateCode";
			this.fldRateCode.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldRateCode.Text = null;
			this.fldRateCode.Top = 1.125F;
			this.fldRateCode.Width = 0.8125F;
			// 
			// lblRateCode
			// 
			this.lblRateCode.Height = 0.1875F;
			this.lblRateCode.HyperLink = null;
			this.lblRateCode.Left = 0F;
			this.lblRateCode.Name = "lblRateCode";
			this.lblRateCode.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblRateCode.Text = "Rate Code:";
			this.lblRateCode.Top = 1.125F;
			this.lblRateCode.Width = 0.875F;
			// 
			// fldSerial
			// 
			this.fldSerial.Height = 0.1875F;
			this.fldSerial.Left = 3F;
			this.fldSerial.Name = "fldSerial";
			this.fldSerial.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldSerial.Text = null;
			this.fldSerial.Top = 0.1875F;
			this.fldSerial.Width = 1.3125F;
			// 
			// lblSerial
			// 
			this.lblSerial.Height = 0.1875F;
			this.lblSerial.HyperLink = null;
			this.lblSerial.Left = 1.75F;
			this.lblSerial.Name = "lblSerial";
			this.lblSerial.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblSerial.Text = "Serial Number:";
			this.lblSerial.Top = 0.1875F;
			this.lblSerial.Width = 1.25F;
			// 
			// fldCombine
			// 
			this.fldCombine.Height = 0.1875F;
			this.fldCombine.Left = 3F;
			this.fldCombine.Name = "fldCombine";
			this.fldCombine.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldCombine.Text = null;
			this.fldCombine.Top = 0.75F;
			this.fldCombine.Width = 1.3125F;
			// 
			// lblCombine
			// 
			this.lblCombine.Height = 0.1875F;
			this.lblCombine.HyperLink = null;
			this.lblCombine.Left = 1.75F;
			this.lblCombine.Name = "lblCombine";
			this.lblCombine.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblCombine.Text = "Combined:";
			this.lblCombine.Top = 0.75F;
			this.lblCombine.Width = 1.25F;
			// 
			// fldMultiplier
			// 
			this.fldMultiplier.Height = 0.1875F;
			this.fldMultiplier.Left = 3F;
			this.fldMultiplier.Name = "fldMultiplier";
			this.fldMultiplier.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldMultiplier.Text = null;
			this.fldMultiplier.Top = 0.9375F;
			this.fldMultiplier.Width = 1.3125F;
			// 
			// lblMultiplier
			// 
			this.lblMultiplier.Height = 0.1875F;
			this.lblMultiplier.HyperLink = null;
			this.lblMultiplier.Left = 1.75F;
			this.lblMultiplier.Name = "lblMultiplier";
			this.lblMultiplier.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblMultiplier.Text = "Multiplier:";
			this.lblMultiplier.Top = 0.9375F;
			this.lblMultiplier.Width = 1.25F;
			// 
			// fldRemote
			// 
			this.fldRemote.Height = 0.1875F;
			this.fldRemote.Left = 3F;
			this.fldRemote.Name = "fldRemote";
			this.fldRemote.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldRemote.Text = null;
			this.fldRemote.Top = 0.375F;
			this.fldRemote.Width = 1.3125F;
			// 
			// lblRemote
			// 
			this.lblRemote.Height = 0.1875F;
			this.lblRemote.HyperLink = null;
			this.lblRemote.Left = 1.75F;
			this.lblRemote.Name = "lblRemote";
			this.lblRemote.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblRemote.Text = "Remote Number:";
			this.lblRemote.Top = 0.375F;
			this.lblRemote.Width = 1.25F;
			// 
			// fldReplacement
			// 
			this.fldReplacement.Height = 0.1875F;
			this.fldReplacement.Left = 3F;
			this.fldReplacement.Name = "fldReplacement";
			this.fldReplacement.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldReplacement.Text = null;
			this.fldReplacement.Top = 1.125F;
			this.fldReplacement.Width = 1.3125F;
			// 
			// lblReplacement
			// 
			this.lblReplacement.Height = 0.1875F;
			this.lblReplacement.HyperLink = null;
			this.lblReplacement.Left = 1.75F;
			this.lblReplacement.Name = "lblReplacement";
			this.lblReplacement.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblReplacement.Text = "Replacement:";
			this.lblReplacement.Top = 1.125F;
			this.lblReplacement.Width = 1.25F;
			// 
			// fldBTWRT2
			// 
			this.fldBTWRT2.Height = 0.1875F;
			this.fldBTWRT2.Left = 4.9375F;
			this.fldBTWRT2.Name = "fldBTWRT2";
			this.fldBTWRT2.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTWRT2.Text = null;
			this.fldBTWRT2.Top = 0.75F;
			this.fldBTWRT2.Width = 0.375F;
			// 
			// lblBillTableWType2
			// 
			this.lblBillTableWType2.Height = 0.1875F;
			this.lblBillTableWType2.HyperLink = null;
			this.lblBillTableWType2.Left = 4.375F;
			this.lblBillTableWType2.Name = "lblBillTableWType2";
			this.lblBillTableWType2.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillTableWType2.Text = null;
			this.lblBillTableWType2.Top = 0.75F;
			this.lblBillTableWType2.Width = 0.5625F;
			// 
			// fldBillTableWRT
			// 
			this.fldBillTableWRT.Height = 0.1875F;
			this.fldBillTableWRT.Left = 4.9375F;
			this.fldBillTableWRT.Name = "fldBillTableWRT";
			this.fldBillTableWRT.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.fldBillTableWRT.Text = "RT";
			this.fldBillTableWRT.Top = 0.375F;
			this.fldBillTableWRT.Width = 0.375F;
			// 
			// lblBillTableWType
			// 
			this.lblBillTableWType.Height = 0.1875F;
			this.lblBillTableWType.HyperLink = null;
			this.lblBillTableWType.Left = 4.375F;
			this.lblBillTableWType.Name = "lblBillTableWType";
			this.lblBillTableWType.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.lblBillTableWType.Text = "Type";
			this.lblBillTableWType.Top = 0.375F;
			this.lblBillTableWType.Width = 0.5625F;
			// 
			// fldBTWRT3
			// 
			this.fldBTWRT3.Height = 0.1875F;
			this.fldBTWRT3.Left = 4.9375F;
			this.fldBTWRT3.Name = "fldBTWRT3";
			this.fldBTWRT3.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTWRT3.Text = null;
			this.fldBTWRT3.Top = 0.9375F;
			this.fldBTWRT3.Width = 0.375F;
			// 
			// lblBillTableWType3
			// 
			this.lblBillTableWType3.Height = 0.1875F;
			this.lblBillTableWType3.HyperLink = null;
			this.lblBillTableWType3.Left = 4.375F;
			this.lblBillTableWType3.Name = "lblBillTableWType3";
			this.lblBillTableWType3.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillTableWType3.Text = null;
			this.lblBillTableWType3.Top = 0.9375F;
			this.lblBillTableWType3.Width = 0.5625F;
			// 
			// fldBTWRT4
			// 
			this.fldBTWRT4.Height = 0.1875F;
			this.fldBTWRT4.Left = 4.9375F;
			this.fldBTWRT4.Name = "fldBTWRT4";
			this.fldBTWRT4.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTWRT4.Text = null;
			this.fldBTWRT4.Top = 1.125F;
			this.fldBTWRT4.Width = 0.375F;
			// 
			// lblBillTableWType4
			// 
			this.lblBillTableWType4.Height = 0.1875F;
			this.lblBillTableWType4.HyperLink = null;
			this.lblBillTableWType4.Left = 4.375F;
			this.lblBillTableWType4.Name = "lblBillTableWType4";
			this.lblBillTableWType4.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillTableWType4.Text = null;
			this.lblBillTableWType4.Top = 1.125F;
			this.lblBillTableWType4.Width = 0.5625F;
			// 
			// fldBTWRT1
			// 
			this.fldBTWRT1.Height = 0.1875F;
			this.fldBTWRT1.Left = 4.9375F;
			this.fldBTWRT1.Name = "fldBTWRT1";
			this.fldBTWRT1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTWRT1.Text = null;
			this.fldBTWRT1.Top = 0.5625F;
			this.fldBTWRT1.Width = 0.375F;
			// 
			// lblBillTableWType1
			// 
			this.lblBillTableWType1.Height = 0.1875F;
			this.lblBillTableWType1.HyperLink = null;
			this.lblBillTableWType1.Left = 4.375F;
			this.lblBillTableWType1.Name = "lblBillTableWType1";
			this.lblBillTableWType1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillTableWType1.Text = null;
			this.lblBillTableWType1.Top = 0.5625F;
			this.lblBillTableWType1.Width = 0.5625F;
			// 
			// fldBTWRT5
			// 
			this.fldBTWRT5.Height = 0.1875F;
			this.fldBTWRT5.Left = 4.9375F;
			this.fldBTWRT5.Name = "fldBTWRT5";
			this.fldBTWRT5.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTWRT5.Text = null;
			this.fldBTWRT5.Top = 1.3125F;
			this.fldBTWRT5.Width = 0.375F;
			// 
			// lblBillTableWType5
			// 
			this.lblBillTableWType5.Height = 0.1875F;
			this.lblBillTableWType5.HyperLink = null;
			this.lblBillTableWType5.Left = 4.375F;
			this.lblBillTableWType5.Name = "lblBillTableWType5";
			this.lblBillTableWType5.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillTableWType5.Text = null;
			this.lblBillTableWType5.Top = 1.3125F;
			this.lblBillTableWType5.Width = 0.5625F;
			// 
			// fldBTWAmt2
			// 
			this.fldBTWAmt2.Height = 0.1875F;
			this.fldBTWAmt2.Left = 5.3125F;
			this.fldBTWAmt2.Name = "fldBTWAmt2";
			this.fldBTWAmt2.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTWAmt2.Text = null;
			this.fldBTWAmt2.Top = 0.75F;
			this.fldBTWAmt2.Width = 0.5625F;
			// 
			// fldBTWAmt
			// 
			this.fldBTWAmt.Height = 0.1875F;
			this.fldBTWAmt.Left = 5.3125F;
			this.fldBTWAmt.Name = "fldBTWAmt";
			this.fldBTWAmt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.fldBTWAmt.Text = "Amt";
			this.fldBTWAmt.Top = 0.375F;
			this.fldBTWAmt.Width = 0.5625F;
			// 
			// fldBTWAmt3
			// 
			this.fldBTWAmt3.Height = 0.1875F;
			this.fldBTWAmt3.Left = 5.3125F;
			this.fldBTWAmt3.Name = "fldBTWAmt3";
			this.fldBTWAmt3.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTWAmt3.Text = null;
			this.fldBTWAmt3.Top = 0.9375F;
			this.fldBTWAmt3.Width = 0.5625F;
			// 
			// fldBTWAmt4
			// 
			this.fldBTWAmt4.Height = 0.1875F;
			this.fldBTWAmt4.Left = 5.3125F;
			this.fldBTWAmt4.Name = "fldBTWAmt4";
			this.fldBTWAmt4.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTWAmt4.Text = null;
			this.fldBTWAmt4.Top = 1.125F;
			this.fldBTWAmt4.Width = 0.5625F;
			// 
			// fldBTWAmt1
			// 
			this.fldBTWAmt1.Height = 0.1875F;
			this.fldBTWAmt1.Left = 5.3125F;
			this.fldBTWAmt1.Name = "fldBTWAmt1";
			this.fldBTWAmt1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTWAmt1.Text = null;
			this.fldBTWAmt1.Top = 0.5625F;
			this.fldBTWAmt1.Width = 0.5625F;
			// 
			// fldBTWAmt5
			// 
			this.fldBTWAmt5.Height = 0.1875F;
			this.fldBTWAmt5.Left = 5.3125F;
			this.fldBTWAmt5.Name = "fldBTWAmt5";
			this.fldBTWAmt5.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTWAmt5.Text = null;
			this.fldBTWAmt5.Top = 1.3125F;
			this.fldBTWAmt5.Width = 0.5625F;
			// 
			// lblBillTableW1
			// 
			this.lblBillTableW1.Height = 0.1875F;
			this.lblBillTableW1.HyperLink = null;
			this.lblBillTableW1.Left = 4.375F;
			this.lblBillTableW1.Name = "lblBillTableW1";
			this.lblBillTableW1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.lblBillTableW1.Text = "Water";
			this.lblBillTableW1.Top = 0.1875F;
			this.lblBillTableW1.Width = 1.5F;
			// 
			// fldBTSRT2
			// 
			this.fldBTSRT2.Height = 0.1875F;
			this.fldBTSRT2.Left = 6.5F;
			this.fldBTSRT2.Name = "fldBTSRT2";
			this.fldBTSRT2.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTSRT2.Text = null;
			this.fldBTSRT2.Top = 0.75F;
			this.fldBTSRT2.Width = 0.375F;
			// 
			// lblBillTableSType2
			// 
			this.lblBillTableSType2.Height = 0.1875F;
			this.lblBillTableSType2.HyperLink = null;
			this.lblBillTableSType2.Left = 5.9375F;
			this.lblBillTableSType2.Name = "lblBillTableSType2";
			this.lblBillTableSType2.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillTableSType2.Text = null;
			this.lblBillTableSType2.Top = 0.75F;
			this.lblBillTableSType2.Width = 0.5625F;
			// 
			// fldBillTableSRT
			// 
			this.fldBillTableSRT.Height = 0.1875F;
			this.fldBillTableSRT.Left = 6.5F;
			this.fldBillTableSRT.Name = "fldBillTableSRT";
			this.fldBillTableSRT.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.fldBillTableSRT.Text = "RT";
			this.fldBillTableSRT.Top = 0.375F;
			this.fldBillTableSRT.Width = 0.375F;
			// 
			// lblBillTableSType
			// 
			this.lblBillTableSType.Height = 0.1875F;
			this.lblBillTableSType.HyperLink = null;
			this.lblBillTableSType.Left = 5.9375F;
			this.lblBillTableSType.Name = "lblBillTableSType";
			this.lblBillTableSType.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.lblBillTableSType.Text = "Type";
			this.lblBillTableSType.Top = 0.375F;
			this.lblBillTableSType.Width = 0.5625F;
			// 
			// fldBTSRT3
			// 
			this.fldBTSRT3.Height = 0.1875F;
			this.fldBTSRT3.Left = 6.5F;
			this.fldBTSRT3.Name = "fldBTSRT3";
			this.fldBTSRT3.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTSRT3.Text = null;
			this.fldBTSRT3.Top = 0.9375F;
			this.fldBTSRT3.Width = 0.375F;
			// 
			// lblBillTableSType3
			// 
			this.lblBillTableSType3.Height = 0.1875F;
			this.lblBillTableSType3.HyperLink = null;
			this.lblBillTableSType3.Left = 5.9375F;
			this.lblBillTableSType3.Name = "lblBillTableSType3";
			this.lblBillTableSType3.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillTableSType3.Text = null;
			this.lblBillTableSType3.Top = 0.9375F;
			this.lblBillTableSType3.Width = 0.5625F;
			// 
			// fldBTSRT4
			// 
			this.fldBTSRT4.Height = 0.1875F;
			this.fldBTSRT4.Left = 6.5F;
			this.fldBTSRT4.Name = "fldBTSRT4";
			this.fldBTSRT4.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTSRT4.Text = null;
			this.fldBTSRT4.Top = 1.125F;
			this.fldBTSRT4.Width = 0.375F;
			// 
			// lblBillTableSType4
			// 
			this.lblBillTableSType4.Height = 0.1875F;
			this.lblBillTableSType4.HyperLink = null;
			this.lblBillTableSType4.Left = 5.9375F;
			this.lblBillTableSType4.Name = "lblBillTableSType4";
			this.lblBillTableSType4.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillTableSType4.Text = null;
			this.lblBillTableSType4.Top = 1.125F;
			this.lblBillTableSType4.Width = 0.5625F;
			// 
			// fldBTSRT1
			// 
			this.fldBTSRT1.Height = 0.1875F;
			this.fldBTSRT1.Left = 6.5F;
			this.fldBTSRT1.Name = "fldBTSRT1";
			this.fldBTSRT1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTSRT1.Text = null;
			this.fldBTSRT1.Top = 0.5625F;
			this.fldBTSRT1.Width = 0.375F;
			// 
			// lblBillTableSType1
			// 
			this.lblBillTableSType1.Height = 0.1875F;
			this.lblBillTableSType1.HyperLink = null;
			this.lblBillTableSType1.Left = 5.9375F;
			this.lblBillTableSType1.Name = "lblBillTableSType1";
			this.lblBillTableSType1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillTableSType1.Text = null;
			this.lblBillTableSType1.Top = 0.5625F;
			this.lblBillTableSType1.Width = 0.5625F;
			// 
			// fldBTSRT5
			// 
			this.fldBTSRT5.Height = 0.1875F;
			this.fldBTSRT5.Left = 6.5F;
			this.fldBTSRT5.Name = "fldBTSRT5";
			this.fldBTSRT5.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTSRT5.Text = null;
			this.fldBTSRT5.Top = 1.3125F;
			this.fldBTSRT5.Width = 0.375F;
			// 
			// lblBillTableSType5
			// 
			this.lblBillTableSType5.Height = 0.1875F;
			this.lblBillTableSType5.HyperLink = null;
			this.lblBillTableSType5.Left = 5.9375F;
			this.lblBillTableSType5.Name = "lblBillTableSType5";
			this.lblBillTableSType5.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillTableSType5.Text = null;
			this.lblBillTableSType5.Top = 1.3125F;
			this.lblBillTableSType5.Width = 0.5625F;
			// 
			// fldBTSAmt2
			// 
			this.fldBTSAmt2.Height = 0.1875F;
			this.fldBTSAmt2.Left = 6.875F;
			this.fldBTSAmt2.Name = "fldBTSAmt2";
			this.fldBTSAmt2.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTSAmt2.Text = null;
			this.fldBTSAmt2.Top = 0.75F;
			this.fldBTSAmt2.Width = 0.5625F;
			// 
			// fldBillTableSAmt
			// 
			this.fldBillTableSAmt.Height = 0.1875F;
			this.fldBillTableSAmt.Left = 6.875F;
			this.fldBillTableSAmt.Name = "fldBillTableSAmt";
			this.fldBillTableSAmt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.fldBillTableSAmt.Text = "Amt";
			this.fldBillTableSAmt.Top = 0.375F;
			this.fldBillTableSAmt.Width = 0.5625F;
			// 
			// fldBTSAmt3
			// 
			this.fldBTSAmt3.Height = 0.1875F;
			this.fldBTSAmt3.Left = 6.875F;
			this.fldBTSAmt3.Name = "fldBTSAmt3";
			this.fldBTSAmt3.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTSAmt3.Text = null;
			this.fldBTSAmt3.Top = 0.9375F;
			this.fldBTSAmt3.Width = 0.5625F;
			// 
			// fldBTSAmt4
			// 
			this.fldBTSAmt4.Height = 0.1875F;
			this.fldBTSAmt4.Left = 6.875F;
			this.fldBTSAmt4.Name = "fldBTSAmt4";
			this.fldBTSAmt4.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTSAmt4.Text = null;
			this.fldBTSAmt4.Top = 1.125F;
			this.fldBTSAmt4.Width = 0.5625F;
			// 
			// fldBTSAmt1
			// 
			this.fldBTSAmt1.Height = 0.1875F;
			this.fldBTSAmt1.Left = 6.875F;
			this.fldBTSAmt1.Name = "fldBTSAmt1";
			this.fldBTSAmt1.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTSAmt1.Text = null;
			this.fldBTSAmt1.Top = 0.5625F;
			this.fldBTSAmt1.Width = 0.5625F;
			// 
			// fldBTSAmt5
			// 
			this.fldBTSAmt5.Height = 0.1875F;
			this.fldBTSAmt5.Left = 6.875F;
			this.fldBTSAmt5.Name = "fldBTSAmt5";
			this.fldBTSAmt5.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTSAmt5.Text = null;
			this.fldBTSAmt5.Top = 1.3125F;
			this.fldBTSAmt5.Width = 0.5625F;
			// 
			// lblBillTableS1
			// 
			this.lblBillTableS1.Height = 0.1875F;
			this.lblBillTableS1.HyperLink = null;
			this.lblBillTableS1.Left = 5.9375F;
			this.lblBillTableS1.Name = "lblBillTableS1";
			this.lblBillTableS1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.lblBillTableS1.Text = "Sewer";
			this.lblBillTableS1.Top = 0.1875F;
			this.lblBillTableS1.Width = 1.5F;
			// 
			// fldTaxableW
			// 
			this.fldTaxableW.Height = 0.1875F;
			this.fldTaxableW.Left = 1.6875F;
			this.fldTaxableW.Name = "fldTaxableW";
			this.fldTaxableW.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldTaxableW.Text = null;
			this.fldTaxableW.Top = 1.625F;
			this.fldTaxableW.Width = 0.625F;
			// 
			// lblTaxable
			// 
			this.lblTaxable.Height = 0.1875F;
			this.lblTaxable.HyperLink = null;
			this.lblTaxable.Left = 0F;
			this.lblTaxable.Name = "lblTaxable";
			this.lblTaxable.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblTaxable.Text = "Taxable Percentage:";
			this.lblTaxable.Top = 1.625F;
			this.lblTaxable.Width = 1.6875F;
			// 
			// fldBillableW
			// 
			this.fldBillableW.Height = 0.1875F;
			this.fldBillableW.Left = 1.6875F;
			this.fldBillableW.Name = "fldBillableW";
			this.fldBillableW.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBillableW.Text = null;
			this.fldBillableW.Top = 1.8125F;
			this.fldBillableW.Width = 0.625F;
			// 
			// lblBillable
			// 
			this.lblBillable.Height = 0.1875F;
			this.lblBillable.HyperLink = null;
			this.lblBillable.Left = 0F;
			this.lblBillable.Name = "lblBillable";
			this.lblBillable.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillable.Text = "Billable Percentage:";
			this.lblBillable.Top = 1.8125F;
			this.lblBillable.Width = 1.6875F;
			// 
			// fldTaxableS
			// 
			this.fldTaxableS.Height = 0.1875F;
			this.fldTaxableS.Left = 2.3125F;
			this.fldTaxableS.Name = "fldTaxableS";
			this.fldTaxableS.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldTaxableS.Text = null;
			this.fldTaxableS.Top = 1.625F;
			this.fldTaxableS.Width = 0.625F;
			// 
			// fldBillableS
			// 
			this.fldBillableS.Height = 0.1875F;
			this.fldBillableS.Left = 2.3125F;
			this.fldBillableS.Name = "fldBillableS";
			this.fldBillableS.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBillableS.Text = null;
			this.fldBillableS.Top = 1.8125F;
			this.fldBillableS.Width = 0.625F;
			// 
			// lblPercentTableW
			// 
			this.lblPercentTableW.Height = 0.1875F;
			this.lblPercentTableW.HyperLink = null;
			this.lblPercentTableW.Left = 1.6875F;
			this.lblPercentTableW.Name = "lblPercentTableW";
			this.lblPercentTableW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.lblPercentTableW.Text = "Water";
			this.lblPercentTableW.Top = 1.4375F;
			this.lblPercentTableW.Width = 0.625F;
			// 
			// lblPercentTableS
			// 
			this.lblPercentTableS.Height = 0.1875F;
			this.lblPercentTableS.HyperLink = null;
			this.lblPercentTableS.Left = 2.3125F;
			this.lblPercentTableS.Name = "lblPercentTableS";
			this.lblPercentTableS.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.lblPercentTableS.Text = "Sewer";
			this.lblPercentTableS.Top = 1.4375F;
			this.lblPercentTableS.Width = 0.625F;
			// 
			// fldBTWRTAdj
			// 
			this.fldBTWRTAdj.Height = 0.1875F;
			this.fldBTWRTAdj.Left = 4.9375F;
			this.fldBTWRTAdj.Name = "fldBTWRTAdj";
			this.fldBTWRTAdj.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTWRTAdj.Text = null;
			this.fldBTWRTAdj.Top = 1.5F;
			this.fldBTWRTAdj.Width = 0.375F;
			// 
			// lblBillTableWAdj
			// 
			this.lblBillTableWAdj.Height = 0.1875F;
			this.lblBillTableWAdj.HyperLink = null;
			this.lblBillTableWAdj.Left = 4.375F;
			this.lblBillTableWAdj.Name = "lblBillTableWAdj";
			this.lblBillTableWAdj.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillTableWAdj.Text = "Adjust:";
			this.lblBillTableWAdj.Top = 1.5F;
			this.lblBillTableWAdj.Width = 0.5625F;
			// 
			// fldBTWAmtAdj
			// 
			this.fldBTWAmtAdj.Height = 0.1875F;
			this.fldBTWAmtAdj.Left = 5.3125F;
			this.fldBTWAmtAdj.Name = "fldBTWAmtAdj";
			this.fldBTWAmtAdj.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTWAmtAdj.Text = null;
			this.fldBTWAmtAdj.Top = 1.5F;
			this.fldBTWAmtAdj.Width = 0.5625F;
			// 
			// fldBTSRTAdj
			// 
			this.fldBTSRTAdj.Height = 0.1875F;
			this.fldBTSRTAdj.Left = 6.5F;
			this.fldBTSRTAdj.Name = "fldBTSRTAdj";
			this.fldBTSRTAdj.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTSRTAdj.Text = null;
			this.fldBTSRTAdj.Top = 1.5F;
			this.fldBTSRTAdj.Width = 0.375F;
			// 
			// lblBillTableSTypeAdj
			// 
			this.lblBillTableSTypeAdj.Height = 0.1875F;
			this.lblBillTableSTypeAdj.HyperLink = null;
			this.lblBillTableSTypeAdj.Left = 5.9375F;
			this.lblBillTableSTypeAdj.Name = "lblBillTableSTypeAdj";
			this.lblBillTableSTypeAdj.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillTableSTypeAdj.Text = "Adjust:";
			this.lblBillTableSTypeAdj.Top = 1.5F;
			this.lblBillTableSTypeAdj.Width = 0.5625F;
			// 
			// fldBTSAmtAdj
			// 
			this.fldBTSAmtAdj.Height = 0.1875F;
			this.fldBTSAmtAdj.Left = 6.875F;
			this.fldBTSAmtAdj.Name = "fldBTSAmtAdj";
			this.fldBTSAmtAdj.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldBTSAmtAdj.Text = null;
			this.fldBTSAmtAdj.Top = 1.5F;
			this.fldBTSAmtAdj.Width = 0.5625F;
			// 
			// lblAdjustDesc
			// 
			this.lblAdjustDesc.Height = 0.1875F;
			this.lblAdjustDesc.HyperLink = null;
			this.lblAdjustDesc.Left = 4.375F;
			this.lblAdjustDesc.Name = "lblAdjustDesc";
			this.lblAdjustDesc.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblAdjustDesc.Text = "Adjust Description:";
			this.lblAdjustDesc.Top = 1.6875F;
			this.lblAdjustDesc.Width = 1.5F;
			// 
			// fldAdjustDesc
			// 
			this.fldAdjustDesc.Height = 0.1875F;
			this.fldAdjustDesc.Left = 5.9375F;
			this.fldAdjustDesc.Name = "fldAdjustDesc";
			this.fldAdjustDesc.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldAdjustDesc.Text = null;
			this.fldAdjustDesc.Top = 1.6875F;
			this.fldAdjustDesc.Width = 1.5F;
			// 
			// srptMeterBillingDetail
			// 
			this.srptMeterBillingDetail.CloseBorder = false;
			this.srptMeterBillingDetail.Height = 0.0625F;
			this.srptMeterBillingDetail.Left = 0F;
			this.srptMeterBillingDetail.Name = "srptMeterBillingDetail";
			this.srptMeterBillingDetail.Report = null;
			this.srptMeterBillingDetail.Top = 2.0625F;
			this.srptMeterBillingDetail.Width = 10.46875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblMuniName,
            this.lblTime,
            this.lblDate,
            this.lblPage,
            this.lblOwner,
            this.lblLocation,
            this.lblMapLot,
            this.lblAccount,
            this.lblTypeCode,
            this.lblREAcctTitle,
            this.lblRENumber,
            this.lblAddress4,
            this.lblBillTo,
            this.lblAccountTitle,
            this.lblBillToTitle,
            this.lblOwnerTitle,
            this.lblLocationTitle,
            this.lblTypeCodeTitle,
            this.lblMapLotTitle});
			this.PageHeader.Height = 1.25F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.375F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
			this.lblHeader.Text = "Meter Detail";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 10.46875F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 3.125F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.25F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 9.25F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.25F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 9.25F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.25F;
			// 
			// lblOwner
			// 
			this.lblOwner.Height = 0.1875F;
			this.lblOwner.HyperLink = null;
			this.lblOwner.Left = 1.5625F;
			this.lblOwner.Name = "lblOwner";
			this.lblOwner.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblOwner.Text = null;
			this.lblOwner.Top = 0.875F;
			this.lblOwner.Width = 3.3125F;
			// 
			// lblLocation
			// 
			this.lblLocation.Height = 0.1875F;
			this.lblLocation.HyperLink = null;
			this.lblLocation.Left = 1.5625F;
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblLocation.Text = null;
			this.lblLocation.Top = 1.0625F;
			this.lblLocation.Width = 3.3125F;
			// 
			// lblMapLot
			// 
			this.lblMapLot.Height = 0.1875F;
			this.lblMapLot.HyperLink = null;
			this.lblMapLot.Left = 5.8125F;
			this.lblMapLot.Name = "lblMapLot";
			this.lblMapLot.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblMapLot.Text = null;
			this.lblMapLot.Top = 0.5F;
			this.lblMapLot.Width = 1.25F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 1.5625F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblAccount.Text = null;
			this.lblAccount.Top = 0.5F;
			this.lblAccount.Width = 1F;
			// 
			// lblTypeCode
			// 
			this.lblTypeCode.Height = 0.1875F;
			this.lblTypeCode.HyperLink = null;
			this.lblTypeCode.Left = 4F;
			this.lblTypeCode.Name = "lblTypeCode";
			this.lblTypeCode.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblTypeCode.Text = null;
			this.lblTypeCode.Top = 0.5F;
			this.lblTypeCode.Width = 0.6875F;
			// 
			// lblREAcctTitle
			// 
			this.lblREAcctTitle.Height = 0.1875F;
			this.lblREAcctTitle.HyperLink = null;
			this.lblREAcctTitle.Left = 4.9375F;
			this.lblREAcctTitle.Name = "lblREAcctTitle";
			this.lblREAcctTitle.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblREAcctTitle.Text = "RE Account";
			this.lblREAcctTitle.Top = 0.6875F;
			this.lblREAcctTitle.Width = 0.875F;
			// 
			// lblRENumber
			// 
			this.lblRENumber.Height = 0.1875F;
			this.lblRENumber.HyperLink = null;
			this.lblRENumber.Left = 5.8125F;
			this.lblRENumber.Name = "lblRENumber";
			this.lblRENumber.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblRENumber.Text = null;
			this.lblRENumber.Top = 0.6875F;
			this.lblRENumber.Width = 1.25F;
			// 
			// lblAddress4
			// 
			this.lblAddress4.Height = 0.1875F;
			this.lblAddress4.HyperLink = null;
			this.lblAddress4.Left = 4.9375F;
			this.lblAddress4.Name = "lblAddress4";
			this.lblAddress4.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblAddress4.Text = null;
			this.lblAddress4.Top = 1.0625F;
			this.lblAddress4.Visible = false;
			this.lblAddress4.Width = 2.125F;
			// 
			// lblBillTo
			// 
			this.lblBillTo.Height = 0.1875F;
			this.lblBillTo.HyperLink = null;
			this.lblBillTo.Left = 1.5625F;
			this.lblBillTo.Name = "lblBillTo";
			this.lblBillTo.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillTo.Text = null;
			this.lblBillTo.Top = 0.6875F;
			this.lblBillTo.Width = 3.3125F;
			// 
			// lblAccountTitle
			// 
			this.lblAccountTitle.Height = 0.1875F;
			this.lblAccountTitle.HyperLink = null;
			this.lblAccountTitle.Left = 0.8125F;
			this.lblAccountTitle.Name = "lblAccountTitle";
			this.lblAccountTitle.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblAccountTitle.Text = "Account:";
			this.lblAccountTitle.Top = 0.5F;
			this.lblAccountTitle.Width = 0.75F;
			// 
			// lblBillToTitle
			// 
			this.lblBillToTitle.Height = 0.1875F;
			this.lblBillToTitle.HyperLink = null;
			this.lblBillToTitle.Left = 0.8125F;
			this.lblBillToTitle.Name = "lblBillToTitle";
			this.lblBillToTitle.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblBillToTitle.Text = "Tenant:";
			this.lblBillToTitle.Top = 0.6875F;
			this.lblBillToTitle.Width = 0.75F;
			// 
			// lblOwnerTitle
			// 
			this.lblOwnerTitle.Height = 0.1875F;
			this.lblOwnerTitle.HyperLink = null;
			this.lblOwnerTitle.Left = 0.8125F;
			this.lblOwnerTitle.Name = "lblOwnerTitle";
			this.lblOwnerTitle.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblOwnerTitle.Text = "Owner:";
			this.lblOwnerTitle.Top = 0.875F;
			this.lblOwnerTitle.Width = 0.75F;
			// 
			// lblLocationTitle
			// 
			this.lblLocationTitle.Height = 0.1875F;
			this.lblLocationTitle.HyperLink = null;
			this.lblLocationTitle.Left = 0.8125F;
			this.lblLocationTitle.Name = "lblLocationTitle";
			this.lblLocationTitle.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblLocationTitle.Text = "Location:";
			this.lblLocationTitle.Top = 1.0625F;
			this.lblLocationTitle.Width = 0.75F;
			// 
			// lblTypeCodeTitle
			// 
			this.lblTypeCodeTitle.Height = 0.1875F;
			this.lblTypeCodeTitle.HyperLink = null;
			this.lblTypeCodeTitle.Left = 3.125F;
			this.lblTypeCodeTitle.Name = "lblTypeCodeTitle";
			this.lblTypeCodeTitle.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblTypeCodeTitle.Text = "Type Code:";
			this.lblTypeCodeTitle.Top = 0.5F;
			this.lblTypeCodeTitle.Width = 0.875F;
			// 
			// lblMapLotTitle
			// 
			this.lblMapLotTitle.Height = 0.1875F;
			this.lblMapLotTitle.HyperLink = null;
			this.lblMapLotTitle.Left = 4.9375F;
			this.lblMapLotTitle.Name = "lblMapLotTitle";
			this.lblMapLotTitle.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblMapLotTitle.Text = "Map Lot:";
			this.lblMapLotTitle.Top = 0.5F;
			this.lblMapLotTitle.Width = 0.875F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// arUTMeterDetail
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.48958F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldBookSeq)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMeterSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAvgConsumption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFrequency)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMeterDigits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBookSeq)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMeterSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMeterDigits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFreq)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAvgConsumption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldService)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblService)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRateCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRateCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSerial)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSerial)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCombine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCombine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMultiplier)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMultiplier)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemote)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRemote)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReplacement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReplacement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWRT2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableWType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillTableWRT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableWType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWRT3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableWType3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWRT4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableWType4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWRT1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableWType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWRT5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableWType5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWAmt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWAmt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWAmt3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWAmt4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWAmt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWAmt5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableW1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSRT2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableSType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillTableSRT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableSType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSRT3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableSType3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSRT4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableSType4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSRT1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableSType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSRT5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableSType5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSAmt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillTableSAmt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSAmt3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSAmt4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSAmt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSAmt5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableS1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxableW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillableW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxableS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillableS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPercentTableW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPercentTableS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWRTAdj)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableWAdj)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTWAmtAdj)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSRTAdj)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTableSTypeAdj)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBTSAmtAdj)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjustDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAdjustDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTypeCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblREAcctTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRENumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillTo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillToTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOwnerTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocationTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTypeCodeTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMapLotTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookSeq;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMeterSize;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAvgConsumption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFrequency;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMeterDigits;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBookSeq;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMeterSize;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMeterDigits;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFreq;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAvgConsumption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldService;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblService;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRateCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRateCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSerial;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSerial;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCombine;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCombine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMultiplier;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMultiplier;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRemote;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRemote;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReplacement;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReplacement;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTWRT2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableWType2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillTableWRT;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableWType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTWRT3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableWType3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTWRT4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableWType4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTWRT1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableWType1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTWRT5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableWType5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTWAmt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTWAmt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTWAmt3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTWAmt4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTWAmt1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTWAmt5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableW1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTSRT2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableSType2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillTableSRT;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableSType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTSRT3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableSType3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTSRT4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableSType4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTSRT1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableSType1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTSRT5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableSType5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTSAmt2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillTableSAmt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTSAmt3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTSAmt4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTSAmt1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTSAmt5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableS1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxableW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillableW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillable;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxableS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillableS;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPercentTableW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPercentTableS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTWRTAdj;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableWAdj;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTWAmtAdj;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTSRTAdj;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTableSTypeAdj;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBTSAmtAdj;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAdjustDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAdjustDesc;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptMeterBillingDetail;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOwner;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTypeCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblREAcctTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRENumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillTo;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillToTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOwnerTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLocationTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTypeCodeTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLotTitle;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
