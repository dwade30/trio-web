﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;

#if TWAR0000
using TWAR0000;


#elif TWCR0000
using TWCR0000;

#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmARGetMasterAccount.
	/// </summary>
	public partial class frmARGetMasterAccount : BaseForm
	{
		public frmARGetMasterAccount()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.cmbSearchBy.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmARGetMasterAccount InstancePtr
		{
			get
			{
				return (frmARGetMasterAccount)Sys.GetInstance(typeof(frmARGetMasterAccount));
			}
		}

		protected frmARGetMasterAccount _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/22/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/02/2005              *
		// ********************************************************
		string strSQL = "";
		clsDRWrapper rs = new clsDRWrapper();
		int intAction;
		int lngColHidden;
		int lngColAccountNumber;
		int lngColName;
		int lngColLocation;
		int intType;
		int lngColInvNum;
		// 0 - Master/Meter Screen, 1 - Collection Status Screen, 2 - Collection Payment Screen
		public void Init(short intPassType, bool boolAutoAction = false, int lngAcct = 0)
		{
			string strKey = "";
			intType = intPassType;
			this.Show(App.MainForm);
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "ARLastAccountNumber", ref strKey);
			////Application.DoEvents();
			if ((modGlobalConstants.Statics.gboolCR && modARStatusPayments.Statics.gboolShowLastARAccountInCR) || !modGlobalConstants.Statics.gboolCR)
			{
				txtGetAccountNumber.Text = FCConvert.ToString(Conversion.Val(strKey));
			}
			if (txtGetAccountNumber.Enabled && txtGetAccountNumber.Visible)
			{
				txtGetAccountNumber.Focus();
			}
			txtGetAccountNumber.SelectionStart = 0;
			txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
			// highlights text
		}

		public void StartProgram_2(int lngCustNumber)
		{
			StartProgram(ref lngCustNumber);
		}

		public void StartProgram(ref int lngCustNumber)
		{
			// this will load the account master screen
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Account");
			//FC:FINAL:SBE - #278 - Close() is called later at the same level, but another form is opened in the meantime. Use Hide() before showing another form
			//this.Hide();
			Close();
			switch (intType)
			{
				case 0:
					{
						// master/meter screen
						// save the last UT account
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "ARLastAccountNumber", modGlobal.PadToString_8(lngCustNumber, 6));
						modExtraModules.GetCustomer(lngCustNumber);
						break;
					}
				case 1:
				case 2:
				case 3:
					{
						// collection status or payment screen
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "ARLastAccountNumber", modGlobal.PadToString_8(lngCustNumber, 6));
						frmARCLStatus.InstancePtr.Init(FCConvert.ToInt16(intType), lngCustNumber);
						break;
					}
			}
			//end switch
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			cmbSearchBy.SelectedIndex = 0;
			txtSearch.Text = "";
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			int PrevAccount = 0;
			// holds the last account last accessed
			if (intAction == 0)
			{
				if (Conversion.Val(txtGetAccountNumber.Text) != 0)
				{
					PrevAccount = modARStatusPayments.Statics.lngCurrentCustomerIDAR;
					modARStatusPayments.Statics.lngCurrentCustomerIDAR = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
					// SQL statement to find account number
					strSQL = "SELECT * from CustomerMaster WHERE CustomerID = " + FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR);
					rs.OpenRecordset(strSQL, modExtraModules.strARDatabase);
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("Deleted")))
						{
							MessageBox.Show("Account has been deleted.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SetAct(0);
							modARStatusPayments.Statics.lngCurrentCustomerIDAR = PrevAccount;
							if ((modGlobalConstants.Statics.gboolCR && modARStatusPayments.Statics.gboolShowLastARAccountInCR) || !modGlobalConstants.Statics.gboolCR)
							{
								txtGetAccountNumber.Text = FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR);
								// puts last account accessed in the textbox
							}
							txtGetAccountNumber.Focus();
							txtGetAccountNumber.SelectionStart = 0;
							txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
							// highlights text
							return;
						}
						else
						{
							StartProgram_2(FCConvert.ToInt32(rs.Get_Fields_Int32("CustomerID")));
						}
					}
					else
					{
						MessageBox.Show("There is no account with that number.", "Error Loading Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						modARStatusPayments.Statics.lngCurrentCustomerIDAR = PrevAccount;
						if ((modGlobalConstants.Statics.gboolCR && modARStatusPayments.Statics.gboolShowLastARAccountInCR) || !modGlobalConstants.Statics.gboolCR)
						{
							txtGetAccountNumber.Text = FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR);
							// puts last account accessed in the textbox
						}
						txtGetAccountNumber.Focus();
						txtGetAccountNumber.SelectionStart = 0;
						txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
						// highlights text
					}
				}
				else
				{
					MessageBox.Show("Please select a valid customer.", "Missing Customer Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					if (txtGetAccountNumber.Visible && txtGetAccountNumber.Enabled)
					{
						txtGetAccountNumber.Focus();
					}
					// Make_New_Master_Account                             '0 = create new master account record
				}
			}
			else if (intAction == 1)
			{
				SelectAccount(vsSearch.Row);
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void Make_New_Master_Account()
		{
			StartProgram_2(-1);
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
			// returns to one form higher in the form hierarchy
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			int lngCurrentAccountKey;
			// first check to see if they clicked an option button
			//if (cmbSearchBy.SelectedIndex == 0)
			//{
			//    MessageBox.Show("Please choose a search option.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			//    return;
			//}
			// loads and shows wait message
			frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Searching");
			// searching the database
			if (Strings.Trim(txtSearch.Text) != "")
			{
				if (cmbSearchBy.Text == "Name")
				{
					// Name
					// strSQL = "SELECT * FROM CustomerMaster WHERE Name > '" & Trim(txtSearch.Text) & "    ' AND Name < '" & Trim(txtSearch.Text) & "zzzz' AND Deleted = False"
					// builds the SQL statement to select all records
					rs.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rs.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE p.FullName >= '" + Strings.Trim(txtSearch.Text) + "    ' AND p.FullName <= '" + Strings.Trim(txtSearch.Text) + "zzzz' AND IsNull(Deleted, 0) = 0 ORDER BY p.FullName", modExtraModules.strARDatabase);
					// rs.OpenRecordset strSQL, strARDatabase  'selects all the records and fill the recordset
				}
				else if (cmbSearchBy.Text == "Address")
				{
					// Address
					// strSQL = "SELECT * FROM CustomerMaster WHERE ((Address1 > '" & Trim(txtSearch.Text) & "    ' AND Address1 < '" & Trim(txtSearch.Text) & "zzzz') or (AltAddress1 > '" & Trim(txtSearch.Text) & "    ' AND AltAddress1 < '" & Trim(txtSearch.Text) & "zzzz')) AND Deleted = False"
					// builds the SQL statement to select all records
					rs.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rs.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE p.Address1 >= '" + Strings.Trim(txtSearch.Text) + "    ' AND p.Address1 <= '" + Strings.Trim(txtSearch.Text) + "zzzz' AND IsNull(Deleted, 0) = 0 ORDER BY p.FullName", modExtraModules.strARDatabase);
					// rs.OpenRecordset strSQL, strARDatabase  'selects all the records and fill the recordset
				}
				else if (cmbSearchBy.Text == "Invoice")
				{
					strSQL = "select m.*, IIF(b.PrinPaid >= b.PrinOwed,cast(1 as bit) ,cast(0 as bit)) as Paid from (select c.*, p.* from CustomerMaster as c Cross Apply " + rs.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p where isnull (Deleted,0) = 0";

					strSQL = strSQL + ") as m inner join Bill b on m.ID = b.AccountKey where b.InvoiceNumber = '" + Strings.Trim(txtSearch.Text) + "' and b.BillStatus = 'A'  order by m.FullName";
					//builds the SQL statement to select all records
					rs.OpenRecordset(strSQL, modExtraModules.strARDatabase); //selects all the records and fill the recordset
				}
			}
			else
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Please enter a search string.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			// if the table opened was the meter table, then get a new recordset from the Master table with the correct account numbers
			// If rs.EndOfFile <> True And rs.BeginningOfFile <> True Then
			// StartProgram rs.Get_Fields("CustomerID")
			// Exit Sub
			// End If
			// the recordset is filled with the records that meet the search criteria
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if not at the beginning or the end of the recordset
				if (rs.RecordCount() > 1)
				{
					// if there are records in the recordset then
					// build a screen for the user to choose the correct account
					FillSearchGrid();
					SetAct(1);
					// load the account list grid
					frmWait.InstancePtr.Unload();
					// unloads the clockscreen
				}
				else
				{
					// if there is only one account found, then show the master screen
					StartProgram_2(FCConvert.ToInt32(rs.Get_Fields_Int32("CustomerID")));
				}
			}
			else
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("No accounts found.", "No Results", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}
		// vbPorter upgrade warning: intAct As short	OnWriteFCConvert.ToInt32(
		private void SetAct(int intAct)
		{
			// this will set the form and controls
			intAction = intAct;
			//FC:FINAL:BBE:#292 - correct visiblity for vsSearch
			//FC:FINAL:AM: don't set the location
			if (intAction == 0)
			{
				lblPaidInvoicesMsg.Visible = false;
				// beginning search screen
				//    fraCriteria.Top = FCConvert.ToInt32((this.Height - fraCriteria.Height) / 2.0);
				//    fraCriteria.Left = FCConvert.ToInt32((this.Width - fraCriteria.Width) / 2.0);
				//    fraCriteria.Visible = true;
				vsSearch.Visible = false;
			}
			else if (intAction == 1)
			{
				// shows the list of accounts to choose from
				//    vsSearch.Top = 1000; // (Me.Height - vsSearch.Height) / 2
				//    vsSearch.Left = FCConvert.ToInt32((this.Width - vsSearch.Width) / 2.0);
				if (cmbSearchBy.Text == "Invoice")
				{
					lblPaidInvoicesMsg.Visible = true;
				}
				else
				{
					lblPaidInvoicesMsg.Visible = false;
				}
				vsSearch.Visible = true;
				//    fraCriteria.Visible = false;
			}
			//FC:FINAL:DDU:#2864 - aligned column on left
			vsSearch.ColAlignment(lngColAccountNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void frmARGetMasterAccount_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
				return;
			vsSearch.Visible = false;
			fraCriteria.Visible = true;
			// set label captions on form
			if ((modGlobalConstants.Statics.gboolCR && modARStatusPayments.Statics.gboolShowLastARAccountInCR) || !modGlobalConstants.Statics.gboolCR)
			{
				txtGetAccountNumber.Text = FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR);
				// puts last account accessed in the textbox
			}
		}

		private void frmARGetMasterAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				if (Strings.Trim(txtSearch.Text) != "" && intAction == 0)
				{
					// check to see if there is any text in the search string first
					cmdSearch_Click();
					// if so, then use the search
				}
				else
				{
					// else use the account number
					cmdGetAccountNumber_Click();
				}
			}
			else if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (vsSearch.Visible)
				{
					SetAct(0);
				}
				else
				{
					Close();
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmARGetMasterAccount_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmARGetMasterAccount.ScaleWidth	= 9300;
			//frmARGetMasterAccount.ScaleHeight	= 7875;
			//frmARGetMasterAccount.LinkTopic	= "Form1";
			//frmARGetMasterAccount.LockControls	= -1  'True;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Select Master Account";
			lngColHidden = 0;
			lngColAccountNumber = 1;
			lngColName = 2;
			lngColLocation = 3;
			lngColInvNum = 4;
		}

		private void frmARGetMasterAccount_Resize(object sender, System.EventArgs e)
		{
			SetAct(intAction);
			if (intAction == 1)
			{
				// if the grid is visible
				FormatGrid();
				// this will resize the cols
				//FC:FINAL:RPU:#i528 Don't adjust the height of grid because this was done in designer
				//SetGridHeight(); // this will resize the grid height
			}
		}

		private void mnuFileClearSearch_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuFileSearch_Click(object sender, System.EventArgs e)
		{
			cmdSearch_Click();
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuSelectAccount_Click(object sender, System.EventArgs e)
		{
			cmdGetAccountNumber_Click();
		}

		private void mnuUndeleteMaster_Click(object sender, System.EventArgs e)
		{
			int AcctNum = 0;
			string strAcctNum;
			int lngKey;
			// prompts the user for the account number to be undeleted
			strAcctNum = Interaction.InputBox("Please enter the Account Number that you wish to Undelete", "Undelete", "000000");
			if (Information.IsNumeric(strAcctNum) == false || Conversion.Val(strAcctNum) == 0)
			{
				// checks input for a 0 string or a non numeric string
				MessageBox.Show("Account Numbers must be numeric and nonzero.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			else
			{
				AcctNum = FCConvert.ToInt32(Math.Round(Conversion.Val(strAcctNum)));
				// converts the string to a long
				clsDRWrapper rsUD = new clsDRWrapper();
				// holds the recordset to get undeleted
				strSQL = "SELECT * FROM CustomerMaster WHERE AccountNumber = " + FCConvert.ToString(AcctNum) + " AND Deleted = 1";
				rsUD.OpenRecordset(strSQL, modExtraModules.strARDatabase);
				// searches for the account
				if (rsUD.RecordCount() == 0)
				{
					// if it is not found tell the user and return to the get account screen
					MessageBox.Show("No account with that number has been deleted.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					modGlobalFunctions.AddCYAEntry_8("AR", "Undelete Account : " + FCConvert.ToString(AcctNum));
					rsUD.Edit();
					// if found then undelete the record and send the user to the master account edit screen
					lngKey = FCConvert.ToInt32(rsUD.Get_Fields_Int32("ID"));
					rsUD.Set_Fields("Deleted", false);
					rsUD.Update();
					MessageBox.Show("Account Number #" + FCConvert.ToString(AcctNum) + " has been undeleted.", "Successful Undeletion", MessageBoxButtons.OK, MessageBoxIcon.Information);
					StartProgram(ref AcctNum);
				}
			}
		}

		private void txtGetAccountNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// traps the backspace ID and all keys that are non numeric
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
				// any ID other than backspace or number keys are not allowed
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// keyascii 39 is the forward slash and 124 is the pipe
			// SQL cannot deal with these characters and will cause an untrappable error
			if (KeyAscii == Keys.Right || KeyAscii == Keys.F13)
				KeyAscii = (Keys)0;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void FillSearchGrid()
		{
			// this will fill the grid with all of the accounts that match the search criteria
			string strAcct = "";
			string strName = "";
			string strLocation = "";
			string strInvNum = "";
			bool boolFlag;
			// reset all of the sizes and clear the grid
			FormatGrid(true);
			while (!rs.EndOfFile())
			{
				////Application.DoEvents();
				strAcct = FCConvert.ToString(rs.Get_Fields_Int32("CustomerID"));
				strName = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("FullName")));
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Address2"))) != "")
				{
					strLocation = Strings.Trim(rs.Get_Fields_String("Address1") + " " + rs.Get_Fields_String("Address2"));
				}
				else
				{
					strLocation = Strings.Trim(" " + rs.Get_Fields_String("Address1"));
				}
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Address3"))) != "")
				{
					strLocation = Strings.Trim(strLocation + " " + Strings.Trim(" " + rs.Get_Fields_String("Address3")));
				}
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("City"))) != "")
				{
					strLocation = Strings.Trim(strLocation + " " + Strings.Trim(" " + rs.Get_Fields_String("City")));
				}
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Zip"))) != "")
				{
					strLocation = Strings.Trim(strLocation + " " + Strings.Trim(" " + rs.Get_Fields_String("Zip")));
				}
				if (cmbSearchBy.Text == "Invoice")
				{
					strInvNum = Strings.Trim(rs.Get_Fields_String("InvoiceNumber"));
				}
				else
				{
					strInvNum = "";
				}
				boolFlag = FCConvert.CBool(rs.Get_Fields_Boolean("Paid") == true);
				// actually add the row to the grid
				vsSearch.AddItem("");
				vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAccountNumber, strAcct);
				vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, strName);
				vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, strLocation);
				vsSearch.TextMatrix(vsSearch.Rows - 1, lngColInvNum, strInvNum);
				vsSearch.TextMatrix(vsSearch.Rows - 1, lngColHidden, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
				if (boolFlag)
				{
					vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, Color.Yellow);
				}
				rs.MoveNext();
			}
			//FC:FINAL:RPU:#i528 Don't adjust the height of the grid, this is fixed in designer
			//SetGridHeight();
		}

		private void FormatGrid(bool boolReset = false)
		{
			int lngWid = 0;
			lngWid = vsSearch.WidthOriginal;
			if (boolReset)
			{
				vsSearch.Rows = 1;
				vsSearch.TextMatrix(0, lngColAccountNumber, "Acct");
				vsSearch.TextMatrix(0, lngColName, "Name");
				vsSearch.TextMatrix(0, lngColLocation, "Address");
				vsSearch.TextMatrix(0, lngColInvNum, "Invoice");
				vsSearch.Cols = 5;
				vsSearch.ExtendLastCol = true;
			}
			if (cmbSearchBy.Text == "Invoice")
			{
				vsSearch.ColWidth(lngColAccountNumber, FCConvert.ToInt32(lngWid * 0.1));
				vsSearch.ColWidth(lngColLocation, FCConvert.ToInt32(lngWid * 0.29));
				vsSearch.ColWidth(lngColName, FCConvert.ToInt32(lngWid * 0.29));
				vsSearch.ColWidth(lngColInvNum, FCConvert.ToInt32(lngWid * 0.29));
			}
			else
			{
				vsSearch.ColWidth(lngColHidden, 0);
				vsSearch.ColWidth(lngColAccountNumber, FCConvert.ToInt32(lngWid * 0.19));
				vsSearch.ColWidth(lngColLocation, FCConvert.ToInt32(lngWid * 0.39));
				vsSearch.ColWidth(lngColName, FCConvert.ToInt32(lngWid * 0.39));
                vsSearch.ColWidth(lngColInvNum, 0);
			}
			vsSearch.AllowUserResizing = FCGrid.AllowUserResizeSettings.flexResizeColumns;
		}

		private void SetGridHeight()
		{
			// this will check to see how many rows the grid has and adjust the height accordingly
			int lngRW = 0;
			lngRW = vsSearch.Rows;
			if ((lngRW * vsSearch.RowHeight(0)) + 70 > this.HeightOriginal * 0.8)
			{
				vsSearch.HeightOriginal = FCConvert.ToInt32(this.HeightOriginal * 0.8);
				vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			}
			else
			{
				vsSearch.HeightOriginal = (lngRW * vsSearch.RowHeight(0)) + 70;
				vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			}
		}

		private void SelectAccount(int lngRow)
		{
			// this routine will take the row passed and find the account number
			// then send the user to the Account Master that they have selected
			int lngAcct = 0;
			int lngKey;
			if (lngRow > 0)
			{
				// make sure that a valid row is being passed in
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(vsSearch.TextMatrix(lngRow, lngColAccountNumber))));
				// get the account number from the grid
				lngKey = FCConvert.ToInt32(Math.Round(Conversion.Val(vsSearch.TextMatrix(lngRow, lngColHidden))));
				// get the account number from the grid
				if (lngAcct > 0)
				{
					// if it is a valid account number
					StartProgram(ref lngAcct);
					// call the master screen
				}
			}
			SetAct(0);
		}

		private void vsSearch_DblClick(object sender, System.EventArgs e)
		{
			SelectAccount(vsSearch.MouseRow);
		}
	}
}
