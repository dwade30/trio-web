﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWBD0000
using TWBD0000;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmPrePayYear.
	/// </summary>
	public partial class frmPrePayYear : BaseForm
	{
		public frmPrePayYear()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPrePayYear InstancePtr
		{
			get
			{
				return (frmPrePayYear)Sys.GetInstance(typeof(frmPrePayYear));
			}
		}

		protected frmPrePayYear _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               06/30/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               05/17/2005              *
		// ********************************************************
		int intType;
		int intYear;
		int lngAcct;

		public int Init(short intPassType, int lngAcctKey = 0, string strHeading = "")
		{
			int Init = 0;
			// this will call show the form and return a date at the end
			lngAcct = lngAcctKey;
			intType = intPassType;
			if (Strings.Trim(strHeading) != "")
			{
				this.Text = strHeading;
			}
			this.Show(FormShowEnum.Modal);
			Init = intYear;
			return Init;
		}

		private void cmbYear_DropDown(object sender, System.EventArgs e)
		{
			if (intType == 2)
			{
				modAPIsConst.SendMessageByNum(this.cmbYear.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
			}
		}

		private void frmPrePayYear_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (KeyCode == Keys.Escape)
				{
					// frmCLGetAccount.intPrePayYear = -1
					intYear = -1;
					Close();
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Prepayment Keydown Error");
			}
		}

		private void frmPrePayYear_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (KeyAscii == 13)
				{
					switch (intType)
					{
						case 0:
						case 1:
							{
								if (Strings.Trim(cmbYear.Items[cmbYear.SelectedIndex].ToString()) != "")
								{
									intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(cmbYear.Items[cmbYear.SelectedIndex].ToString()))));
									Close();
								}
								else
								{
									FCMessageBox.Show("Please enter a year.", MsgBoxStyle.OkOnly, "Required");
									if (cmbYear.Enabled && cmbYear.Visible)
									{
										cmbYear.Focus();
									}
								}
								break;
							}
						case 2:
							{
								if (Strings.Trim(cmbYear.Items[cmbYear.SelectedIndex].ToString()) != "")
								{
									intYear = cmbYear.ItemData(cmbYear.SelectedIndex);
									Close();
								}
								else
								{
									FCMessageBox.Show("Please enter a meter.", MsgBoxStyle.OkOnly, "Required");
									if (cmbYear.Enabled && cmbYear.Visible)
									{
										cmbYear.Focus();
									}
								}
								break;
							}
					}
					//end switch
				}
				// this will make sure that the characters are all uppercase
				if (KeyAscii >= 97 && KeyAscii <= 122)
				{
					KeyAscii -= 32;
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Prepayment Keypress Error");
				e.KeyChar = Strings.Chr(KeyAscii);
			}
		}

		private void frmPrePayYear_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPrePayYear.Icon	= "frmPrePayYear.frx":0000";
			//frmPrePayYear.ScaleWidth	= 2895;
			//frmPrePayYear.ScaleHeight	= 1470;
			//frmPrePayYear.LinkTopic	= "Form1";
			//Font.Size	= "8.25";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsElasticLight1.OleObjectBlob	= "frmPrePayYear.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWMINI);
			// frmCLGetAccount.intPrePayYear = -1
			FillYearCombo();
			this.Left = FCConvert.ToInt32((FCGlobal.Screen.Width - this.Width) / 2.0);
			this.Top = FCConvert.ToInt32((FCGlobal.Screen.Height - this.Height) / 2.0);
		}

		private void FillYearCombo()
		{
			// VBto upgrade warning: strTemp As string	OnWrite(string, int)
			string strTemp = "";
			clsDRWrapper rsBookYear = new clsDRWrapper();
			cmbYear.Clear();
			// this will fill the combo with the current billed year and the next billed year
			switch (intType)
			{
				case 0:
					{
						// Prepayment Year
						lblOutput.Text = "Select the year to add a prepayment.";
						if (Conversion.Val(modGlobal.Statics.gstrLastYearBilled) > 0)
						{
							strTemp = Strings.Left(modGlobal.Statics.gstrLastYearBilled, 4);
							cmbYear.AddItem(modExtraModules.FormatYear(strTemp + "1"));
							cmbYear.AddItem(modExtraModules.FormatYear((Conversion.Val(strTemp) + 1).ToString() + "1"));
							cmbYear.AddItem(modExtraModules.FormatYear((Conversion.Val(strTemp) + 2).ToString() + "1"));
						}
						else
						{
							strTemp = FCConvert.ToString(DateTime.Today.Year - 1);
							cmbYear.AddItem(modExtraModules.FormatYear(strTemp + "1"));
							cmbYear.AddItem(modExtraModules.FormatYear((Conversion.Val(strTemp) + 1).ToString() + "1"));
							cmbYear.AddItem(modExtraModules.FormatYear((Conversion.Val(strTemp) + 2).ToString() + "1"));
						}
						break;
					}
				case 1:
					{
						// Updated Book Page Report
						lblOutput.Text = "Select the year.";
						rsBookYear.OpenRecordset("SELECT Distinct BillingYear FROM BillingMaster ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
						while (!rsBookYear.EndOfFile())
						{
							cmbYear.AddItem(modExtraModules.FormatYear(FCConvert.ToString(rsBookYear.Get_Fields_Int32("BillingYear"))));
							rsBookYear.MoveNext();
						}
						break;
					}
				case 2:
					{
						// Meters
						lblOutput.Text = "Select the correct meter.";
						rsBookYear.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(lngAcct) + " ORDER BY MeterNumber desc", modExtraModules.strUTDatabase);
						if (!rsBookYear.EndOfFile())
						{
							while (!rsBookYear.EndOfFile())
							{
								// TODO: Check the table for the column [Sequence] and replace with corresponding Get_Field method
								cmbYear.AddItem(modGlobal.PadToString(rsBookYear.Get_Fields_Int32("MeterNumber"), 2) + " Book - " + modGlobal.PadToString(rsBookYear.Get_Fields_Int32("BookNumber"), 4) + " Seq - " + modGlobal.PadToString(rsBookYear.Get_Fields("Sequence"), 4));
								cmbYear.ItemData(cmbYear.NewIndex, FCConvert.ToInt32(rsBookYear.Get_Fields_Int32("ID")));
								rsBookYear.MoveNext();
							}
						}
						else
						{
							FCMessageBox.Show("No meters are set up for this account.  Please add a meter before adding a prepayment.", MsgBoxStyle.Exclamation, "Missing Meter");
							intYear = -1;
						}
						break;
					}
			}
			//end switch
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			// frmCLGetAccount.intPrePayYear = -1
			intYear = -1;
			Close();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			// frmCLGetAccount.intPrePayYear = Val(FormatYear(cmbYear.List(cmbYear.ListIndex)))
			if (cmbYear.SelectedIndex >= 0)
			{
				switch (intType)
				{
					case 0:
					case 1:
						{
							intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(cmbYear.Items[cmbYear.SelectedIndex].ToString()))));
							break;
						}
					case 2:
						{
							intYear = cmbYear.ItemData(cmbYear.SelectedIndex);
							break;
						}
				}
				//end switch
			}
			else
			{
				intYear = -1;
			}
			Close();
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}
	}
}
