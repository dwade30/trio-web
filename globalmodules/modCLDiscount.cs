﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWBD0000
using TWBD0000;


#elif TWCR0000
using TWCR0000;


#elif TWBL0000
using TWBL0000;
using modExtraModules = TWBL0000.modGlobalVariables;
using modGlobal = TWBL0000.modCollectionsRelated;
using modUseCR = TWBL0000.modGlobalRoutines;
#endif
namespace Global
{
	public class modCLDiscount
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/13/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/22/2004              *
		// *
		// *
		// If a module other than CR uses this module then that   *
		// will have to have a dummy function called              *
		// GetNextReceiptNumber.                                  *
		// ********************************************************
		//public static void testdiscountroutine()
		//{
		//	// this function will be created in BL
		//	clsDRWrapper rsTest = new clsDRWrapper();
		//	double dblReturnAmount = 0;
		//	double dblDiscount = 0;
		//	// test
		//	rsTest.OpenRecordset("SELECT * FROM BillingMaster WHERE TaxDue1 > 1 AND BillingYear = 20041", modExtraModules.strCLDatabase);
		//	while (!rsTest.EndOfFile())
		//	{
		//		// code
		//		dblDiscount = 0;
		//		if (rsTest.Get_Fields_Decimal("PrincipalPaid") > 0)
		//		{
		//			// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
		//			if (PrePaymentsPlusDiscountFinishesAccount(FCConvert.ToInt32(rsTest.Get_Fields("Account")), FCConvert.ToInt32(rsTest.Get_Fields_Int32("BillingYear")), FCConvert.ToString(rsTest.Get_Fields_String("BillingType")), ref dblReturnAmount, ref dblDiscount))
		//			{
		//				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
		//				AddDiscountAmountToPendingTable_78(dblDiscount, FCConvert.ToInt32(rsTest.Get_Fields("Account")), FCConvert.ToInt32(rsTest.Get_Fields_Int32("BillingYear")), FCConvert.ToString(rsTest.Get_Fields_String("BillingType")));
		//			}
		//		}
		//		rsTest.MoveNext();
		//		// test
		//	}
		//}

		public static bool PrePaymentsPlusDiscountFinishesAccount(ref int lngAccountNumber, int lngBillingYear, string strType, ref double dblReturnAmount/*= 0*/, ref double dblReturnDisc/*= 0*/)
		{
			bool PrePaymentsPlusDiscountFinishesAccount = false;
			double dblDiscountTotal = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will check the amount owed on the account and find out if the total discount was applied
				// at this point will it pay the account off...if so, the this function will return TRUE else it will return FALSE
				// need to pass in the account number, billing year (full format, ie. 20041) and the type (RE or PP)
				// the variable dblReturnAmount will be filled with the amount after the calculation happens in case it is needed
				// the variable dblReturnDisc will be filled with the amount of the discount
				clsDRWrapper rsBill = new clsDRWrapper();
				double dblTotalPrin = 0;
				if (strType == "RE")
				{
					rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingYear = " + FCConvert.ToString(lngBillingYear) + " AND BillingType = '" + strType + "'", modExtraModules.strCLDatabase);
					if (!rsBill.EndOfFile())
					{
						// record found
						dblTotalPrin = Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue4"));
						dblDiscountTotal = CalculateDiscount(dblTotalPrin, 0, true);
						dblReturnDisc = dblDiscountTotal;
						// this is to return
						dblReturnAmount = dblTotalPrin - Conversion.Val(rsBill.Get_Fields_Decimal("PrincipalPaid")) - dblDiscountTotal;
						if (dblReturnAmount <= 0 && dblDiscountTotal > 0 && dblTotalPrin > 0)
						{
							PrePaymentsPlusDiscountFinishesAccount = true;
						}
						else
						{
							PrePaymentsPlusDiscountFinishesAccount = false;
						}
					}
					else
					{
						// not found
						// return false
						dblReturnAmount = 0;
						PrePaymentsPlusDiscountFinishesAccount = false;
					}
				}
				else if (strType == "PP")
				{
					rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingYear = " + FCConvert.ToString(lngBillingYear) + " AND BillingType = '" + strType + "'", modExtraModules.strCLDatabase);
					if (!rsBill.EndOfFile())
					{
						// record found
						dblTotalPrin = Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue4"));
						dblDiscountTotal = CalculateDiscount(dblTotalPrin, 0, true);
						dblReturnDisc = dblDiscountTotal;
						// this is to return
						dblReturnAmount = dblTotalPrin - Conversion.Val(rsBill.Get_Fields_Decimal("PrincipalPaid")) - dblDiscountTotal;
						if (dblReturnAmount <= 0 && dblDiscountTotal > 0 && dblTotalPrin > 0)
						{
							PrePaymentsPlusDiscountFinishesAccount = true;
						}
						else
						{
							PrePaymentsPlusDiscountFinishesAccount = false;
						}
					}
					else
					{
						// not found
						// return false
						dblReturnAmount = 0;
						PrePaymentsPlusDiscountFinishesAccount = false;
					}
				}
				return PrePaymentsPlusDiscountFinishesAccount;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				dblDiscountTotal = 0;
				PrePaymentsPlusDiscountFinishesAccount = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Calculating Account");
			}
			return PrePaymentsPlusDiscountFinishesAccount;
		}

		public static double CalculateDiscount(double dblPrin, double dblDiscPay = 0, bool boolPassPrincipalPaid = false)
		{
			double CalculateDiscount = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function returns the amount of discount for the principal passed in dblPrin
				// if dblPrin is passed a value of 0 and dblDiscPay has a value passed in, this is treated as
				// how much the user wants to pay in cash and the program will calculate the discount for just
				// the amount that is to be paid
				clsDRWrapper rsDisc = new clsDRWrapper();
				double dblDiscRate;
				double dblDisc = 0;
				// find the rate
				dblDiscRate = 0;
				rsDisc.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
				if (rsDisc.RecordCount() > 0)
				{
					// TODO: Check the table for the column [DiscountPercent] and replace with corresponding Get_Field method
					dblDiscRate = Conversion.Val(rsDisc.Get_Fields("DiscountPercent"));
					// kk09302014 trocl-1197    ' / 10000
					// rsDisc.Edit
					// rsDisc.Get_Fields("DiscountPercent") = 300
					// rsDisc.Update
				}
				// If boolPassPrincipalPaid Then   'use the passed in prin as the total
				// If dblPrin > 0 Then                 'if the principal is passed in
				// dblDisc = (dblPrin - (dblPrin * (1 - dblDiscRate)))
				// dblDisc = dblPrin * dblDiscRate
				// ElseIf dblDiscPay <> 0 Then         'if the principal that is to be paid is passed in
				// dblDisc = ((dblDiscPay / (1 - dblDiscRate)) - dblDiscPay)
				// dblDisc = dblDiscPay * dblDiscRate
				// Else                                'nothing is passed in
				// CalculateDiscount = 0
				// End If
				// Else
				// If dblPrin > 0 Then                 'if the principal is passed in
				// dblDisc = (dblPrin * dblDiscRate)
				// ElseIf dblDiscPay <> 0 Then         'if the principal that is to be paid is passed in
				// dblDisc = dblDiscPay * dblDiscRate
				// Else                                'nothing is passed in
				// CalculateDiscount = 0
				// End If
				// End If
				if (dblDiscPay != 0)
				{
					dblDisc = (dblPrin * dblDiscRate);
					dblDisc *= (dblDiscPay / (dblPrin - dblDisc));
				}
				else
				{
					dblDisc = (dblPrin * dblDiscRate);
				}
				#if !TWBL0000
				CalculateDiscount = modGlobal.Round(dblDisc, 2);
				#else
								                CalculateDiscount = modMain.Round(dblDisc, 2);
#endif
				rsDisc.Reset();
				return CalculateDiscount;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Calculating Discount");
			}
			return CalculateDiscount;
		}

		public static bool AddDiscountAmountToBill(double dblDiscountAmount, int lngAccountNumber, int lngBillingYear, string strType, string strBDAccountNumber, int lngCloseOut = 0, int lngReceiptNumber = 0)
		{
			bool AddDiscountAmountToBill = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will create a payment record for the account passed in and adjust the bill record
				// so that CL can create discount records when creating bills, the total discount amount
				// will have to be decreased in accounts recievable or just taken out of the BL amount that
				// will be set to become the recievables
				clsDRWrapper rsBill = new clsDRWrapper();
				clsDRWrapper rsPay = new clsDRWrapper();
				rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingYear = " + FCConvert.ToString(lngBillingYear) + " AND BillingType = '" + strType + "'", modExtraModules.strCLDatabase);
				if (!rsBill.EndOfFile())
				{
					// bill is found
					// create a payment and adjust the billing values
					rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0", modExtraModules.strCLDatabase);
					rsPay.AddNew();
					rsPay.Set_Fields("Account", lngAccountNumber);
					rsPay.Set_Fields("Year", lngBillingYear);
					rsPay.Set_Fields("BillKey", rsBill.Get_Fields_Int32("ID"));
					rsPay.Set_Fields("ActualSystemDate", DateTime.Now);
					rsPay.Set_Fields("EffectiveInterestDate", DateTime.Now);
					rsPay.Set_Fields("RecordedTransactionDate", DateTime.Now);
					rsPay.Set_Fields("Reference", "DISCOUNT");
					rsPay.Set_Fields("Period", "A");
					rsPay.Set_Fields("Code", "D");
					rsPay.Set_Fields("ReceiptNumber", lngReceiptNumber);
					#if !TWBL0000
					rsPay.Set_Fields("Principal", modGlobal.Round(dblDiscountAmount, 2));
					#else
										                    rsPay.Set_Fields("Principal", modMain.Round(dblDiscountAmount, 2));
#endif
					rsPay.Set_Fields("CashDrawer", "N");
					rsPay.Set_Fields("GeneralLedger", "N");
					rsPay.Set_Fields("BudgetaryAccountNumber", strBDAccountNumber);
					rsPay.Set_Fields("BillCode", Strings.Left(strType, 1));
					rsPay.Set_Fields("DailyCloseOut", lngCloseOut);
					rsPay.Update();
					// update the bill record
					rsBill.Edit();
					rsBill.Set_Fields("PrincipalPaid", Conversion.Val(rsBill.Get_Fields_Decimal("PrincipalPaid")) + dblDiscountAmount);
					rsBill.Update();
					AddDiscountAmountToBill = true;
				}
				else
				{
					AddDiscountAmountToBill = false;
				}
				return AddDiscountAmountToBill;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				AddDiscountAmountToBill = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Adding Discount Record");
			}
			return AddDiscountAmountToBill;
		}

		public static bool AddDiscountAmountToPendingTable_78(double dblDiscountAmount, int lngAccountNumber, int lngBillingYear, string strType)
		{
			return AddDiscountAmountToPendingTable(ref dblDiscountAmount, ref lngAccountNumber, ref lngBillingYear, ref strType);
		}

		public static bool AddDiscountAmountToPendingTable(ref double dblDiscountAmount, ref int lngAccountNumber, ref int lngBillingYear, ref string strType)
		{
			bool AddDiscountAmountToPendingTable = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will create a temporary payment record for the account passed in
				// so that CL can create discount records when the user finalizes the billing, the total discount amount
				// will have to be entered into a general receipt so that accounts recievables can be affected
				clsDRWrapper rsBill = new clsDRWrapper();
				clsDRWrapper rsPay = new clsDRWrapper();
				rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingYear = " + FCConvert.ToString(lngBillingYear) + " AND BillingType = '" + strType + "'", modExtraModules.strCLDatabase);
				if (!rsBill.EndOfFile())
				{
					// bill is found
					// create a payment and adjust the billing values
					rsPay.OpenRecordset("SELECT * FROM PendingDiscount WHERE ID = 0", modExtraModules.strCLDatabase);
					rsPay.AddNew();
					rsPay.Set_Fields("Account", lngAccountNumber);
					rsPay.Set_Fields("Year", lngBillingYear);
					rsPay.Set_Fields("BillKey", rsBill.Get_Fields_Int32("ID"));
					rsPay.Set_Fields("ActualSystemDate", DateTime.Now);
					#if !TWBL0000
					rsPay.Set_Fields("Principal", modGlobal.Round(dblDiscountAmount, 2));
					#else
										                    rsPay.Set_Fields("Principal", modMain.Round(dblDiscountAmount, 2));
#endif
					rsPay.Set_Fields("BillType", Strings.Left(strType, 1));
					rsPay.Update();
					AddDiscountAmountToPendingTable = true;
				}
				else
				{
					AddDiscountAmountToPendingTable = false;
				}
				return AddDiscountAmountToPendingTable;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				AddDiscountAmountToPendingTable = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Adding Discount Record");
			}
			return AddDiscountAmountToPendingTable;
		}

		public static bool FinalizePendingDiscounts()
		{
			bool FinalizePendingDiscounts = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will go through the PendingDiscounts table and create Discount records
				// and affect the bill using the function AddDiscountAmountToBill and then take the total
				// of all the discounts and create a Receipt if the user has CR so that the correct accounts
				// are affected in BD if needed, this iwll also create a report of all the entries made
				// and then clear the PendingDiscounts table
				clsDRWrapper rsDiscount = new clsDRWrapper();
				clsDRWrapper rsArchive = new clsDRWrapper();
				clsDRWrapper rsRec = new clsDRWrapper();
				int lngCT = 0;
				int lngRecNum = 0;
				int lngRecKey = 0;
				string strType = "";
				string strDiscAccount = "";
				string strPrinAccount = "";
				double dblTotalDiscount = 0;
				if (modGlobalConstants.Statics.gboolCR)
				{
					// this will create an archive record and a receipt record
					// get the next receipt number from the locked table
					lngRecNum = modUseCR.GetNextReceiptNumber();
					// find the discount account in receipt type 90
					rsRec.OpenRecordset("SELECT * FROM Type WHERE TypeCode = 90", modExtraModules.strCRDatabase);
					if (!rsRec.EndOfFile())
					{
						if (Strings.Trim(FCConvert.ToString(rsRec.Get_Fields_String("Account1"))) != "")
						{
							strPrinAccount = Strings.Trim(FCConvert.ToString(rsRec.Get_Fields_String("Account1")));
						}
						else
						{
							strPrinAccount = "M Principal";
						}
						if (Strings.Trim(FCConvert.ToString(rsRec.Get_Fields_String("Account3"))) != "")
						{
							strDiscAccount = Strings.Trim(FCConvert.ToString(rsRec.Get_Fields_String("Account3")));
						}
						else
						{
							strDiscAccount = "M Discount";
						}
					}
					else
					{
						strDiscAccount = "M Discount";
						strPrinAccount = "M Principal";
					}
					// create a receipt record and get the ID from it
					rsRec.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptKey = 0", modExtraModules.strCRDatabase);
					rsRec.AddNew();
					rsRec.Set_Fields("ReceiptNumber", lngRecNum);
					// lngRecKey = rsRec.Get_Fields("ReceiptKey")
					// finish the receipt record
					rsRec.Set_Fields("ReceiptDate", DateTime.Now.ToString("MM/dd/yy h:mm:ss tt"));
					rsRec.Set_Fields("PaidBy", "Automatic Discount");
					rsRec.Update();
					lngRecKey = FCConvert.ToInt32(rsRec.Get_Fields_Int32("ReceiptKey"));
				}
				else
				{
					strDiscAccount = "M Discount";
					strPrinAccount = "M Principal";
				}
				rsDiscount.OpenRecordset("SELECT * FROM PendingDiscount", modExtraModules.strCLDatabase);
				while (!rsDiscount.EndOfFile())
				{
					// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsDiscount.Get_Fields("BillType")) == "R")
					{
						strType = "RE";
					}
					else
					{
						strType = "PP";
					}
					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
					AddDiscountAmountToBill(Conversion.Val(rsDiscount.Get_Fields("Principal")), FCConvert.ToInt32(rsDiscount.Get_Fields("Account")), FCConvert.ToInt32(rsDiscount.Get_Fields("Year")), strType, strDiscAccount, 0, lngRecKey);
					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
					dblTotalDiscount += Conversion.Val(rsDiscount.Get_Fields("Principal"));
					lngCT += 1;
					rsDiscount.MoveNext();
				}
				rsDiscount.Execute("DELETE FROM PendingDiscount", modExtraModules.strCLDatabase);
				if (modGlobalConstants.Statics.gboolCR)
				{
					// finish the receipt record
					// rsRec.Get_Fields("Date") = Now
					// rsRec.Get_Fields("PaidBy") = "Automatic Discount"
					// rsRec.Update
					// and create an archive records
					rsArchive.OpenRecordset("SELECT * FROM Archive WHERE ID = 0", modExtraModules.strCRDatabase);
					rsArchive.AddNew();
					if (strType == "RE")
					{
						rsArchive.Set_Fields("ReceiptType", 90);
					}
					else
					{
						rsArchive.Set_Fields("ReceiptType", 92);
					}
					rsArchive.Set_Fields("ReceiptTitle", "Automatic Discount");
					rsArchive.Set_Fields("Account1", strPrinAccount);
					rsArchive.Set_Fields("Amount1", dblTotalDiscount);
					rsArchive.Set_Fields("Comment", "Automatic Discount Entry");
					rsArchive.Set_Fields("Name", "Automatic Discount Entry");
					rsArchive.Set_Fields("Control1", "A");
					rsArchive.Set_Fields("Control2", "D");
					rsArchive.Set_Fields("ReceiptNumber", lngRecKey);
					rsArchive.Set_Fields("Split", 1);
					rsArchive.Set_Fields("ArchiveDate", DateTime.Now);
					rsArchive.Set_Fields("ActualSystemDate", DateTime.Now);
					rsArchive.Set_Fields("CollectionCode", "D");
					rsArchive.Set_Fields("AffectCashDrawer", false);
					rsArchive.Set_Fields("AffectCash", false);
					rsArchive.Set_Fields("DefaultAccount", strDiscAccount);
					rsArchive.Update();
				}
				return FinalizePendingDiscounts;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FinalizePendingDiscounts = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Finalize Pending Discount Records");
			}
			return FinalizePendingDiscounts;
		}
	}
}
