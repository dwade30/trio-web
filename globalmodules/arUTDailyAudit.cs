﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

#if TWCR0000
using TWCR0000;


#elif TWUT0000
using modGlobal = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for arUTDailyAudit.
	/// </summary>
	public partial class arUTDailyAudit : BaseSectionReport
	{
		// nObj = 1
		//   0	arUTDailyAudit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/06/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/29/2004              *
		// ********************************************************
		public bool boolWide;
		// True if this report is in Wide Format
		public float lngWidth;
		// This is the Print Width of the report
		bool boolDone;
		public bool boolOrderByReceipt;
		// order by receipt
		bool boolCloseOutAudit;
		// Actually close out the payments
		public bool boolLandscape;
		// is the report to be printed landscape?
		int intType;
		// 0 - Water, 1 - Sewer, 2 - Both
		public arUTDailyAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static arUTDailyAudit InstancePtr
		{
			get
			{
				return (arUTDailyAudit)Sys.GetInstance(typeof(arUTDailyAudit));
			}
		}

		protected arUTDailyAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// this should let the detail section fire once
			if (boolDone)
			{
				eArgs.EOF = boolDone;
			}
			else
			{
				eArgs.EOF = boolDone;
				boolDone = true;
			}
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			//clsDRWrapper rsCloseOut = new clsDRWrapper();
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsOrder = new clsDRWrapper();
			string strUTReportType = "";
			StartUp();
			// kk06042015 trout-1166  Customize saves this in UtilityBilling, not CollectionControl
			// There is no process that updates CollectionControl. Was this table linked to CL at one point?
			// If rsOrder.OpenRecordset("SELECT * FROM CollectionControl", strUTDatabase, , , , , , False) Then
			if (rsOrder.OpenRecordset("SELECT AuditSeqReceipt FROM UtilityBilling", modExtraModules.strUTDatabase, 0, false, 0, false, string.Empty, false))
			{
				if (rsOrder.EndOfFile() != true)
				{
					if (FCConvert.ToBoolean(rsOrder.Get_Fields_Boolean("AuditSeqReceipt")))
					{
						boolOrderByReceipt = true;
					}
					else
					{
						boolOrderByReceipt = false;
					}
				}
				else
				{
					rsOrder.AddNew();
					rsOrder.Set_Fields("AuditSeqReceipt", true);
					rsOrder.Update();
					boolOrderByReceipt = true;
				}
			}
			else
			{
				MessageBox.Show("An error has occured while determining your default Audit sort order.  This will not affect the data.  The order has been set to Receipt Number.", "Collections Table Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				boolOrderByReceipt = true;
			}
			switch (intType)
			{
				case 0:
					{
						// water
						strUTReportType = "Service <> 'S' AND ";
						break;
					}
				case 1:
					{
						// sewer
						strUTReportType = "Service <> 'W' AND ";
						break;
					}
				default:
					{
						// closeout
						strUTReportType = "";
						break;
					}
			}
			//end switch
			if (modGlobalConstants.Statics.gboolCR)
			{
				rsOrder.OpenRecordset("SELECT * FROM PaymentRec WHERE " + strUTReportType + "ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " AND ReceiptNumber > 0", modExtraModules.strUTDatabase);
			}
			else
			{
				rsOrder.OpenRecordset("SELECT * FROM PaymentRec WHERE " + strUTReportType + "ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut), modExtraModules.strUTDatabase);
			}
			if (rsOrder.EndOfFile())
			{
				lblOutput.Visible = true;
				lblOutput.Text = "There are no collection records to process at this time.";
			}
			else
			{
				lblOutput.Visible = false;
				sarDADetailOb.Visible = true;
				sarDACashOb.Visible = true;
				sarDAPreviousPeriodOb.Visible = true;
				sarDANonCashOb.Visible = true;
				sarDATypeSummaryOB.Visible = true;
				sarDABookSummary.Visible = true;
				// set the subreports
				sarDADetailOb.Report = new sarUTDADetail();
				sarDACashOb.Report = new sarUTDACash();
				sarDAPreviousPeriodOb.Report = new sarUTDAPreviousPeriod();
				sarDANonCashOb.Report = new sarUTDANonCash();
				sarDATypeSummaryOB.Report = new sarUTDATypeSummary();
				sarDABookSummary.Report = new sarUTDABookSummary();
				// strreporttype
				sarDACashOb.Report.UserData = Strings.Trim(intType.ToString());
				sarDAPreviousPeriodOb.Report.UserData = Strings.Trim(intType.ToString());
				sarDANonCashOb.Report.UserData = Strings.Trim(intType.ToString());
				sarDATypeSummaryOB.Report.UserData = Strings.Trim(intType.ToString());
				sarDABookSummary.Report.UserData = Strings.Trim(intType.ToString());
			}
			rsOrder.DisposeOf();
            // StartUp boolShowRE
			// Screen.MousePointer = 11
			// frmWait.Init "Please Wait..." & vbCrLf & "Loading", True
		}

		private void SetupFields()
		{
			// find out which way the user wants to print
			boolWide = boolLandscape;
			if (boolWide)
			{
				// wide format
				lngWidth = 13500 / 1440F;
				Document.Printer.Landscape = true;
			}
			else
			{
				// narrow format
				lngWidth = 10800 / 1440F;
				Document.Printer.Landscape = false;
			}
			this.PrintWidth = lngWidth;
			lblPreview.Width = lngWidth;
			sarDADetailOb.Width = lngWidth;
			sarDACashOb.Width = lngWidth;
			sarDAPreviousPeriodOb.Width = lngWidth;
			sarDANonCashOb.Width = lngWidth;
			sarDATypeSummaryOB.Width = lngWidth - 1 / 1440f;
			sarDABookSummary.Width = lngWidth - 1 / 1440f;
			sarDADetailOb.Left = 0;
			sarDACashOb.Left = 0;
			sarDAPreviousPeriodOb.Left = 0;
			sarDANonCashOb.Left = 0;
			sarDATypeSummaryOB.Left = 0;
			sarDABookSummary.Left = 0;
			lblOutput.Width = lngWidth;
			lblHeader.Width = lngWidth;
			lblDate.Left = lngWidth - lblDate.Width;
			lblPage.Left = lngWidth - lblPage.Width;
		}

		private void StartUp()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
				lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				lblMuniName.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
				intType = rptUTDailyAuditMaster.InstancePtr.intType;
				// these are passed from rptUTDailyAuditMaster
				boolCloseOutAudit = rptUTDailyAuditMaster.InstancePtr.boolCloseOutAudit;
				if (boolCloseOutAudit)
				{
					lblPreview.Visible = false;
				}
				else
				{
					lblPreview.Visible = true;
				}
				boolLandscape = rptUTDailyAuditMaster.InstancePtr.boolLandscape;
				switch (intType)
				{
					case 0:
						{
							lblHeader.Text = "Utility Billing Water Daily Audit Report";
							// kk trouts-6 02282013  Change Water to Stormwater for Bangor
							if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
							{
								lblHeader.Text = "Utility Billing Stormwater Daily Audit Report";
							}
							break;
						}
					case 1:
						{
							lblHeader.Text = "Utility Billing Sewer Daily Audit Report";
							break;
						}
					case 2:
						{
							lblHeader.Text = "Utility Billing Daily Audit Report";
							break;
						}
				}
				//end switch
				SetupFields();
				frmWait.InstancePtr.IncrementProgress();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error On Start Up", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + rptUTDailyAuditMaster.InstancePtr.PageNumber;
		}

		private void arUTDailyAudit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arUTDailyAudit.Caption	= "Daily Audit Report";
			//arUTDailyAudit.Icon	= "arUTDailyAudit.dsx":0000";
			//arUTDailyAudit.Left	= 0;
			//arUTDailyAudit.Top	= 0;
			//arUTDailyAudit.Width	= 11880;
			//arUTDailyAudit.Height	= 8565;
			//arUTDailyAudit.StartUpPosition	= 3;
			//arUTDailyAudit.SectionData	= "arUTDailyAudit.dsx":058A;
			//End Unmaped Properties
		}
	}
}
