﻿using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	public class clsBudgetaryPosting
	{
		//=========================================================
		private clsPostingJournalInfo[] clsJournals = null;
		private int intJournalCount;
		private string strDBPath = string.Empty;
		private bool blnBreak;
		private bool blnSavePostingReport;
		private string strSavedReportFileName = string.Empty;
		private bool blnAllowPreview;
		private bool blnShowModally;

		public clsPostingJournalInfo Get_JournalsToPost(int Index)
		{
			clsPostingJournalInfo JournalsToPost = null;
			JournalsToPost = clsJournals[Index];
			return JournalsToPost;
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short JournalCount
		{
			get
			{
				short JournalCount = 0;
				JournalCount = FCConvert.ToInt16(intJournalCount);
				return JournalCount;
			}
		}

		public string DBPath
		{
			set
			{
				strDBPath = value;
			}
			get
			{
				string DBPath = "";
				DBPath = strDBPath;
				return DBPath;
			}
		}

		public bool SavePostingReport
		{
			set
			{
				blnSavePostingReport = value;
			}
			get
			{
				bool SavePostingReport = false;
				SavePostingReport = blnSavePostingReport;
				return SavePostingReport;
			}
		}

		public bool WaitForReportToEnd
		{
			set
			{
				blnShowModally = value;
			}
			get
			{
				bool WaitForReportToEnd = false;
				WaitForReportToEnd = blnShowModally;
				return WaitForReportToEnd;
			}
		}

		public bool AllowPreview
		{
			set
			{
				blnAllowPreview = value;
			}
			get
			{
				bool AllowPreview = false;
				AllowPreview = blnAllowPreview;
				return AllowPreview;
			}
		}

		public string SavedReportFileName
		{
			set
			{
				strSavedReportFileName = value;
			}
			get
			{
				string SavedReportFileName = "";
				SavedReportFileName = strSavedReportFileName;
				return SavedReportFileName;
			}
		}

		public bool PageBreakBetweenJournalsOnReport
		{
			set
			{
				blnBreak = value;
			}
			get
			{
				bool PageBreakBetweenJournalsOnReport = false;
				PageBreakBetweenJournalsOnReport = blnBreak;
				return PageBreakBetweenJournalsOnReport;
			}
		}

		public void AddJournal(clsPostingJournalInfo clsJournal)
		{
			int counter;
			for (counter = 0; counter <= intJournalCount - 1; counter++)
			{
				if (clsJournals[counter].JournalNumber == clsJournal.JournalNumber && (clsJournals[counter].Period == clsJournal.Period || FCConvert.ToInt32(clsJournal.Period) == 0))
				{
					return;
				}
			}
			Array.Resize(ref clsJournals, intJournalCount + 1);
			clsJournals[intJournalCount] = clsJournal;
			intJournalCount += 1;
		}

		public void RemoveJournal(ref clsPostingJournalInfo clsJournal)
		{
			int counter;
			int counter2;
			for (counter = 0; counter <= intJournalCount - 1; counter++)
			{
				if (clsJournals[counter].JournalNumber == clsJournal.JournalNumber)
				{
					for (counter2 = counter + 1; counter2 <= intJournalCount - 1; counter2++)
					{
						clsJournals[counter2 - 1] = clsJournals[counter2];
					}
					intJournalCount -= 1;
					if (intJournalCount > 0)
					{
						Array.Resize(ref clsJournals, intJournalCount - 1 + 1);
					}
					else
					{
						clsJournals = new clsPostingJournalInfo[0 + 1];
					}
				}
			}
		}

		public void ClearJournals()
		{
			clsJournals = new clsPostingJournalInfo[0 + 1];
			intJournalCount = 0;
		}

		public bool PostJournals(List<string> batchReports = null)
		{
			bool PostJournals = false;
			for (var counter = 0; counter <= intJournalCount - 1; counter++)
			{
				App.DoEvents();
				if (!modBudgetaryAccounting.PostJournal(clsJournals[counter].JournalNumber, strDBPath))
				{
					return false;
				}
			}
			// make sure at least 1 was checked for posting before going to the report
			if (intJournalCount > 0)
			{
				rptMultiModulePosting.InstancePtr.Init(this, batchReports: batchReports);
				frmWait.InstancePtr.Unload();
				PostJournals = true;
			}
			return PostJournals;
		}

		public clsBudgetaryPosting() : base()
		{
			intJournalCount = 0;
			clsJournals = new clsPostingJournalInfo[0 + 1];
			strDBPath = "";
			blnBreak = false;
			blnSavePostingReport = false;
			strSavedReportFileName = "";
			blnAllowPreview = true;
		}
	}
}
