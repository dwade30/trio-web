﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using fecherFoundation;

namespace Global
{
	/// <summary>
	/// Summary description for frmComment.
	/// </summary>
	partial class frmComment : BaseForm
	{
		public fecherFoundation.FCGrid fgrdComments;
		public fecherFoundation.FCLabel lblTitle;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.fgrdComments = new fecherFoundation.FCGrid();
            this.lblTitle = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fgrdComments)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 420);
            this.BottomPanel.Size = new System.Drawing.Size(809, 12);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fgrdComments);
            this.ClientArea.Controls.Add(this.lblTitle);
            this.ClientArea.Size = new System.Drawing.Size(829, 443);
            this.ClientArea.Controls.SetChildIndex(this.lblTitle, 0);
            this.ClientArea.Controls.SetChildIndex(this.fgrdComments, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(829, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(108, 28);
            this.HeaderText.Text = "Comment";
            // 
            // fgrdComments
            // 
            this.fgrdComments.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fgrdComments.Cols = 4;
            this.fgrdComments.ExtendLastCol = true;
            this.fgrdComments.Location = new System.Drawing.Point(30, 30);
            this.fgrdComments.Name = "fgrdComments";
            this.fgrdComments.Rows = 50;
            this.fgrdComments.Size = new System.Drawing.Size(776, 390);
            this.fgrdComments.TabIndex = 0;
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(0, 24);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(606, 16);
            this.lblTitle.TabIndex = 1;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 0;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmComment
            // 
            this.ClientSize = new System.Drawing.Size(829, 503);
            this.KeyPreview = true;
            this.Name = "frmComment";
            this.Text = "Comment";
            this.Load += new System.EventHandler(this.frmComment_Load);
            this.Activated += new System.EventHandler(this.frmComment_Activated);
            this.Resize += new System.EventHandler(this.frmComment_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmComment_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fgrdComments)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
