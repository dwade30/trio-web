﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmUTDateChange.
	/// </summary>
	public partial class frmUTDateChange : BaseForm
	{
		public frmUTDateChange()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUTDateChange InstancePtr
		{
			get
			{
				return (frmUTDateChange)Sys.GetInstance(typeof(frmUTDateChange));
			}
		}

		protected frmUTDateChange _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/21/2003              *
		// ********************************************************
		DateTime dtNew;
		int intFormType;
		bool boolEffectChecked;

		public void Init(string strText, short intType, ref DateTime dtEffectiveDate)
		{
			// strText is the actual sting that will be shown above the date box
			// intType will tell me which place in my code called this form
			if (intType == 1)
			{
				chkEffect.Visible = true;
			}
			lblInstructions.Text = strText;
			txtDate.Text = Strings.Format(dtEffectiveDate, "MM/dd/yyyy");
			intFormType = intType;
			this.Show(FormShowEnum.Modal, App.MainForm);
			if (txtDate.Visible && txtDate.Enabled)
			{
				txtDate.Focus();
			}
		}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intAns As short, int --> As DialogResult
			DialogResult intAns;
			DateTime datLowDate;
			DateTime datHighDate;
			switch (intFormType)
			{
				case 1:
					{
						// Effective Interest Date from frmRECLStatus
						if (Information.IsDate(txtDate.Text))
						{
							// MAL@20080304: Correct date functions
							// Tracker Reference: 12581
							// datLowDate = CDate(Month(Date) & "/" & Day(Date) & "/" & Year(Date)) - 1
							// datHighDate = CDate(Month(Date) & "/" & Day(Date) & "/" & Year(Date)) + 1
							datLowDate = DateAndTime.DateAdd("yyyy", -1, DateTime.Today);
							datHighDate = DateAndTime.DateAdd("yyyy", 1, DateTime.Today);
							if (DateAndTime.DateValue(txtDate.Text).ToOADate() < datLowDate.ToOADate() || DateAndTime.DateValue(txtDate.Text).ToOADate() > datHighDate.ToOADate())
							{
								intAns = MessageBox.Show("The date you are entering is different from today's date by more than a year.  Are you sure you wish to use this date?", "Change Date?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
								if (intAns == DialogResult.No)
								{
									goto UnloadTag;
								}
							}
							dtNew = DateAndTime.DateValue(txtDate.Text);
							boolEffectChecked = FCConvert.CBool(chkEffect.CheckState == Wisej.Web.CheckState.Checked);
							this.Hide();
							frmUTCLStatus.InstancePtr.GetNewEffectiveInterestDate(ref dtNew, boolEffectChecked);
						}
						UnloadTag:
						;
						Close();
						break;
					}
			}
			//end switch
		}

		public void cmdOk_Click()
		{
			cmdOk_Click(cmdOk, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmUTDateChange_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
				return;
		}

		private void frmUTDateChange_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						Support.SendKeys("{tab}", false);
						break;
					}
			}
			//end switch
		}

		private void frmUTDateChange_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmUTDateChange_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUTDateChange.ScaleWidth	= 3015;
			//frmUTDateChange.ScaleHeight	= 2445;
			//frmUTDateChange.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			if (modUTCalculations.Statics.gboolDefaultUTRTDToAutoChange)
			{
				chkEffect.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkEffect.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void txtDate_KeyDownEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Return:
					{
						// if the user hits the enter ID on the textbox when they are done editing the date
						cmdOk_Click();
						break;
					}
			}
			//end switch
		}

		private void txtDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(Strings.Left(txtDate.Text, 2)) > 12)
			{
				e.Cancel = true;
				MessageBox.Show("Please use the date format MM/dd/yyyy.", "Invalid Date Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
	}
}
