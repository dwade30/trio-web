﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace Global
{
	/// <summary>
	/// Summary description for frmRECLStatus.
	/// </summary>
	partial class frmRECLStatus : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public System.Collections.Generic.List<fecherFoundation.FCButton> cmdDisc;
		public System.Collections.Generic.List<fecherFoundation.FCButton> cmdWOProcess;
		public fecherFoundation.FCFrame fraRateInfo;
		public fecherFoundation.FCGrid vsRateInfo;
		public fecherFoundation.FCButton cmdRIClose;
		public fecherFoundation.FCPanel fraStatusLabels;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCLabel lblExemptValue2;
		public fecherFoundation.FCLabel lblExemptValue;
		public fecherFoundation.FCLabel lblBuildingValue2;
		public fecherFoundation.FCLabel lblBuildingValue;
		public fecherFoundation.FCLine Line3;
		public fecherFoundation.FCLine Line2;
		public fecherFoundation.FCLabel lblTotalValue2;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel lblExempt;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblLandValue;
		public fecherFoundation.FCLabel lblBuilding;
		public fecherFoundation.FCLabel lblLand;
		public fecherFoundation.FCLabel lblTotal;
		public fecherFoundation.FCLabel lblTotalValue;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel lblBookPage;
		public fecherFoundation.FCLabel lblReference1;
		public fecherFoundation.FCLabel lblAcreage;
		public fecherFoundation.FCLabel lblOwnersName;
		public fecherFoundation.FCLabel lblMapLot;
		public fecherFoundation.FCLabel lblLocation;
		public fecherFoundation.FCCommonDialog CommonDialog1;
		public fecherFoundation.FCPanel fraPPStatusLabels;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblPPAccountShow;
		public fecherFoundation.FCLabel lblPPAddress;
		public fecherFoundation.FCLabel lblPPAccount;
		public fecherFoundation.FCLabel lblPPOwnersName;
		public fecherFoundation.FCPanel fraPayment;
		public fecherFoundation.FCTextBox txtTotalPendingDue;
		public fecherFoundation.FCTextBox txtCash;
		public fecherFoundation.FCGrid vsPayments;
		public fecherFoundation.FCTextBox txtPrincipal;
		public fecherFoundation.FCTextBox txtCosts;
		public fecherFoundation.FCTextBox txtTransactionDate2;
		public fecherFoundation.FCTextBox txtInterest;
		public fecherFoundation.FCTextBox txtComments;
		public fecherFoundation.FCTextBox txtBLID;
		public fecherFoundation.FCGrid txtAcctNumber;
		public T2KDateBox txtTransactionDate;
		public fecherFoundation.FCGrid vsPeriod;
		public fecherFoundation.FCComboBox cmbPeriod;
		public fecherFoundation.FCComboBox cmbCode;
		public fecherFoundation.FCTextBox txtReference;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCTextBox txtCD;
		public fecherFoundation.FCLabel lblTaxClub;
		public fecherFoundation.FCLabel Label1_9;
		public fecherFoundation.FCLabel lblTotalPendingDue;
		public fecherFoundation.FCLabel Label1_11;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel lblPrincipal;
		public fecherFoundation.FCLabel lblInterest;
		public fecherFoundation.FCLabel lblCosts;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label1_12;
		public fecherFoundation.FCFrame fraDiscount;
		public fecherFoundation.FCButton cmdDisc_3;
		public fecherFoundation.FCTextBox txtDiscDisc;
		public fecherFoundation.FCTextBox txtDiscPrin;
		public fecherFoundation.FCButton cmdDisc_2;
		public fecherFoundation.FCButton cmdDisc_1;
		public fecherFoundation.FCButton cmdDisc_0;
		public fecherFoundation.FCLabel lblOriginalTax;
		public fecherFoundation.FCLabel lblOriginalTaxLabel;
		public fecherFoundation.FCLabel lblDiscHiddenTotal;
		public fecherFoundation.FCLabel lblDiscTotal;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel lblDiscountInstructions;
		public fecherFoundation.FCGrid vsPreview;
		public fecherFoundation.FCFrame fraEditNote;
		public fecherFoundation.FCCheckBox chkShowInRE;
		public fecherFoundation.FCButton cmdNoteCancel;
		public fecherFoundation.FCButton cmdNoteProcess;
		public fecherFoundation.FCCheckBox chkPriority;
		public fecherFoundation.FCTextBox txtNote;
		public fecherFoundation.FCLabel lblRemainChars;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel lblNote;
		public fecherFoundation.FCGrid GRID;
		public fecherFoundation.FCFrame fraWriteOff;
		public fecherFoundation.FCTextBox txtWOCosts;
		public fecherFoundation.FCTextBox txtWOPrincipal;
		public fecherFoundation.FCTextBox txtWOCurInterest;
		public fecherFoundation.FCTextBox txtWOPLI;
		public fecherFoundation.FCButton cmdWOProcess_0;
		public fecherFoundation.FCButton cmdWOProcess_1;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel lblWOPLI;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel lblWOTotal;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel lblGrpInfo;
		public fecherFoundation.FCLabel lblAssocAcct;
		public fecherFoundation.FCLabel lblAcctComment;
		public fecherFoundation.FCLabel lblTaxAcquired;
		public fecherFoundation.FCLabel lblPaymentInfo;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessGoTo;
		public fecherFoundation.FCToolStripMenuItem mnuFileMortgageHolderInfo;
		public fecherFoundation.FCToolStripMenuItem mnuFileInterestedParty;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreviousAccount;
		public fecherFoundation.FCToolStripMenuItem mnuFileNextAccount;
		public fecherFoundation.FCToolStripMenuItem mnuFileAccountInfo;
		public fecherFoundation.FCToolStripMenuItem mnuFileTaxInfoSheet;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrintRateInfo;
		public fecherFoundation.FCToolStripMenuItem mnuFileComment;
		public fecherFoundation.FCToolStripMenuItem mnuFileREComment;
		public fecherFoundation.FCToolStripMenuItem mnuFileEditNote;
		public fecherFoundation.FCToolStripMenuItem mnuFilePriority;
		public fecherFoundation.FCToolStripMenuItem mnuFileReprintReceipt;
		public fecherFoundation.FCToolStripMenuItem mnuPayment;
		public fecherFoundation.FCToolStripMenuItem mnuFileOptionLoadValidAccount;
		public fecherFoundation.FCToolStripMenuItem mnuPaymentClearPayment;
		public fecherFoundation.FCToolStripMenuItem mnuPaymentClearList;
		public fecherFoundation.FCToolStripMenuItem mnuFileOptionsBlankBill;
		public fecherFoundation.FCToolStripMenuItem mnuFileDischarge;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrintAltLDN;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrintBPReport;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrintGroupInfo;
		public fecherFoundation.FCLabel lblAssocAcct_Pmt;
		public fecherFoundation.FCLabel lblGrpInfo_Pmt;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRECLStatus));
            this.fraRateInfo = new fecherFoundation.FCFrame();
            this.cmdRIClose = new fecherFoundation.FCButton();
            this.vsRateInfo = new fecherFoundation.FCGrid();
            this.lblRateInfo = new Wisej.Web.Label();
            this.fraDiscount = new fecherFoundation.FCFrame();
            this.cmdDisc_3 = new fecherFoundation.FCButton();
            this.txtDiscDisc = new fecherFoundation.FCTextBox();
            this.txtDiscPrin = new fecherFoundation.FCTextBox();
            this.cmdDisc_2 = new fecherFoundation.FCButton();
            this.cmdDisc_1 = new fecherFoundation.FCButton();
            this.cmdDisc_0 = new fecherFoundation.FCButton();
            this.lblOriginalTax = new fecherFoundation.FCLabel();
            this.lblOriginalTaxLabel = new fecherFoundation.FCLabel();
            this.lblDiscHiddenTotal = new fecherFoundation.FCLabel();
            this.lblDiscTotal = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.lblDiscountInstructions = new fecherFoundation.FCLabel();
            this.fraStatusLabels = new fecherFoundation.FCPanel();
            this.lblLandValue2 = new fecherFoundation.FCLabel();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Line1 = new fecherFoundation.FCLine();
            this.lblExemptValue2 = new fecherFoundation.FCLabel();
            this.lblExemptValue = new fecherFoundation.FCLabel();
            this.lblBuildingValue2 = new fecherFoundation.FCLabel();
            this.lblBuildingValue = new fecherFoundation.FCLabel();
            this.lblOwnersName = new fecherFoundation.FCLabel();
            this.Line3 = new fecherFoundation.FCLine();
            this.Line2 = new fecherFoundation.FCLine();
            this.lblTotalValue2 = new fecherFoundation.FCLabel();
            this.lblYear = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.lblExempt = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblLandValue = new fecherFoundation.FCLabel();
            this.lblBuilding = new fecherFoundation.FCLabel();
            this.lblLand = new fecherFoundation.FCLabel();
            this.lblTotal = new fecherFoundation.FCLabel();
            this.lblTotalValue = new fecherFoundation.FCLabel();
            this.lblBookPage = new fecherFoundation.FCLabel();
            this.lblReference1 = new fecherFoundation.FCLabel();
            this.lblAcreage = new fecherFoundation.FCLabel();
            this.lblMapLot = new fecherFoundation.FCLabel();
            this.lblLocation = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
            this.fraPPStatusLabels = new fecherFoundation.FCPanel();
            this.vsPPCategory = new fecherFoundation.FCGrid();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblPPAccountShow = new fecherFoundation.FCLabel();
            this.lblPPAddress = new fecherFoundation.FCLabel();
            this.lblPPOwnersName = new fecherFoundation.FCLabel();
            this.lblPPAccount = new fecherFoundation.FCLabel();
            this.fraPayment = new fecherFoundation.FCPanel();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.txtTotalPendingDue = new fecherFoundation.FCTextBox();
            this.txtCash = new fecherFoundation.FCTextBox();
            this.vsPayments = new fecherFoundation.FCGrid();
            this.txtPrincipal = new fecherFoundation.FCTextBox();
            this.txtCosts = new fecherFoundation.FCTextBox();
            this.txtTransactionDate2 = new fecherFoundation.FCTextBox();
            this.txtInterest = new fecherFoundation.FCTextBox();
            this.txtComments = new fecherFoundation.FCTextBox();
            this.txtBLID = new fecherFoundation.FCTextBox();
            this.txtAcctNumber = new fecherFoundation.FCGrid();
            this.txtTransactionDate = new Global.T2KDateBox();
            this.vsPeriod = new fecherFoundation.FCGrid();
            this.cmbPeriod = new fecherFoundation.FCComboBox();
            this.cmbCode = new fecherFoundation.FCComboBox();
            this.txtReference = new fecherFoundation.FCTextBox();
            this.cmbYear = new fecherFoundation.FCComboBox();
            this.txtCD = new fecherFoundation.FCTextBox();
            this.lblTaxClub = new fecherFoundation.FCLabel();
            this.Label1_9 = new fecherFoundation.FCLabel();
            this.lblTotalPendingDue = new fecherFoundation.FCLabel();
            this.Label1_11 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.lblPrincipal = new fecherFoundation.FCLabel();
            this.lblInterest = new fecherFoundation.FCLabel();
            this.lblCosts = new fecherFoundation.FCLabel();
            this.Label1_10 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label1_8 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label1_12 = new fecherFoundation.FCLabel();
            this.vsPreview = new fecherFoundation.FCGrid();
            this.fraEditNote = new fecherFoundation.FCFrame();
            this.chkShowInRE = new fecherFoundation.FCCheckBox();
            this.cmdNoteCancel = new fecherFoundation.FCButton();
            this.cmdNoteProcess = new fecherFoundation.FCButton();
            this.chkPriority = new fecherFoundation.FCCheckBox();
            this.txtNote = new fecherFoundation.FCTextBox();
            this.lblRemainChars = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.lblNote = new fecherFoundation.FCLabel();
            this.GRID = new fecherFoundation.FCGrid();
            this.fraWriteOff = new fecherFoundation.FCFrame();
            this.txtWOCosts = new fecherFoundation.FCTextBox();
            this.txtWOPrincipal = new fecherFoundation.FCTextBox();
            this.txtWOCurInterest = new fecherFoundation.FCTextBox();
            this.txtWOPLI = new fecherFoundation.FCTextBox();
            this.cmdWOProcess_0 = new fecherFoundation.FCButton();
            this.cmdWOProcess_1 = new fecherFoundation.FCButton();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.lblWOPLI = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.lblWOTotal = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label19 = new fecherFoundation.FCLabel();
            this.lblGrpInfo = new fecherFoundation.FCLabel();
            this.lblAssocAcct = new fecherFoundation.FCLabel();
            this.lblAcctComment = new fecherFoundation.FCLabel();
            this.lblTaxAcquired = new fecherFoundation.FCLabel();
            this.lblPaymentInfo = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuProcessGoTo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileMortgageHolderInfo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileInterestedParty = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePreviousAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileNextAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileAccountInfo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileTaxInfoSheet = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrintRateInfo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileREComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileEditNote = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePriority = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileReprintReceipt = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPayment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileOptionLoadValidAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPaymentClearPayment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPaymentClearList = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileOptionsBlankBill = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileDischarge = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrintAltLDN = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrintBPReport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrintGroupInfo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.lblAssocAcct_Pmt = new fecherFoundation.FCLabel();
            this.lblGrpInfo_Pmt = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdFileOptionsPrint = new fecherFoundation.FCButton();
            this.cmdProcessEffective = new fecherFoundation.FCButton();
            this.cmdPaymentPreview = new fecherFoundation.FCButton();
            this.cmdProcessChangeAccount = new fecherFoundation.FCButton();
            this.btnSaveExit = new fecherFoundation.FCButton();
            this.imgNote = new fecherFoundation.FCPictureBox();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).BeginInit();
            this.fraRateInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRIClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsRateInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDiscount)).BeginInit();
            this.fraDiscount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDisc_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDisc_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDisc_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDisc_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraStatusLabels)).BeginInit();
            this.fraStatusLabels.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPPStatusLabels)).BeginInit();
            this.fraPPStatusLabels.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPPCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPayment)).BeginInit();
            this.fraPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcctNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraEditNote)).BeginInit();
            this.fraEditNote.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowInRE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNoteCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNoteProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPriority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GRID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWriteOff)).BeginInit();
            this.fraWriteOff.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdWOProcess_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdWOProcess_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileOptionsPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessEffective)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPaymentPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessChangeAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNote)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnSaveExit);
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 713);
            this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraPayment);
            this.ClientArea.Controls.Add(this.fraWriteOff);
            this.ClientArea.Controls.Add(this.fraStatusLabels);
            this.ClientArea.Controls.Add(this.fraRateInfo);
            this.ClientArea.Controls.Add(this.lblPaymentInfo);
            this.ClientArea.Controls.Add(this.fraEditNote);
            this.ClientArea.Controls.Add(this.GRID);
            this.ClientArea.Controls.Add(this.vsPreview);
            this.ClientArea.Controls.Add(this.fraPPStatusLabels);
            this.ClientArea.Controls.Add(this.fraDiscount);
            this.ClientArea.Size = new System.Drawing.Size(1078, 606);
            this.ClientArea.Controls.SetChildIndex(this.fraDiscount, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraPPStatusLabels, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsPreview, 0);
            this.ClientArea.Controls.SetChildIndex(this.GRID, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraEditNote, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPaymentInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraRateInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraStatusLabels, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraWriteOff, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraPayment, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.lblAssocAcct);
            this.TopPanel.Controls.Add(this.lblGrpInfo);
            this.TopPanel.Controls.Add(this.cmdProcessChangeAccount);
            this.TopPanel.Controls.Add(this.cmdPaymentPreview);
            this.TopPanel.Controls.Add(this.cmdProcessEffective);
            this.TopPanel.Controls.Add(this.cmdFileOptionsPrint);
            this.TopPanel.Controls.Add(this.lblAssocAcct_Pmt);
            this.TopPanel.Controls.Add(this.lblPPAccount);
            this.TopPanel.Controls.Add(this.lblAccount);
            this.TopPanel.Controls.Add(this.lblAcctComment);
            this.TopPanel.Controls.Add(this.lblTaxAcquired);
            this.TopPanel.Controls.Add(this.lblGrpInfo_Pmt);
            this.TopPanel.Controls.Add(this.imgNote);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.imgNote, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblGrpInfo_Pmt, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblTaxAcquired, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblAcctComment, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblAccount, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblPPAccount, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblAssocAcct_Pmt, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileOptionsPrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdProcessEffective, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPaymentPreview, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdProcessChangeAccount, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblGrpInfo, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblAssocAcct, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(101, 30);
            this.HeaderText.Text = "Account";
            // 
            // fraRateInfo
            // 
            this.fraRateInfo.AppearanceKey = "popupFrame";
            this.fraRateInfo.BackColor = System.Drawing.Color.FromName("@window");
            this.fraRateInfo.BorderStyle = 2;
            this.fraRateInfo.Controls.Add(this.cmdRIClose);
            this.fraRateInfo.Controls.Add(this.vsRateInfo);
            this.fraRateInfo.Location = new System.Drawing.Point(2, 0);
            this.fraRateInfo.Name = "fraRateInfo";
            this.fraRateInfo.Size = new System.Drawing.Size(600, 630);
            this.fraRateInfo.TabIndex = 102;
            this.fraRateInfo.Visible = false;
            // 
            // cmdRIClose
            // 
            this.cmdRIClose.AppearanceKey = "actionButton";
            this.cmdRIClose.Location = new System.Drawing.Point(421, 580);
            this.cmdRIClose.Name = "cmdRIClose";
            this.cmdRIClose.Size = new System.Drawing.Size(87, 40);
            this.cmdRIClose.TabIndex = 104;
            this.cmdRIClose.Text = "Close";
            this.cmdRIClose.Click += new System.EventHandler(this.cmdRIClose_Click);
            // 
            // vsRateInfo
            // 
            this.vsRateInfo.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsRateInfo.Cols = 5;
            this.vsRateInfo.ColumnHeadersVisible = false;
            this.vsRateInfo.FixedCols = 0;
            this.vsRateInfo.FixedRows = 0;
            this.vsRateInfo.Location = new System.Drawing.Point(15, 30);
            this.vsRateInfo.Name = "vsRateInfo";
            this.vsRateInfo.RowHeadersVisible = false;
            this.vsRateInfo.Rows = 22;
            this.vsRateInfo.Size = new System.Drawing.Size(570, 540);
            this.vsRateInfo.TabIndex = 103;
            this.vsRateInfo.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsRateInfo_CellFormatting);
            this.vsRateInfo.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsRateInfo_BeforeEdit);
            this.vsRateInfo.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsRateInfo_ValidateEdit);
            this.vsRateInfo.CurrentCellChanged += new System.EventHandler(this.vsRateInfo_RowColChange);
            this.vsRateInfo.DoubleClick += new System.EventHandler(this.vsRateInfo_DblClick);
            // 
            // lblRateInfo
            // 
            this.lblRateInfo.AutoSize = true;
            this.lblRateInfo.Font = new System.Drawing.Font("default", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblRateInfo.Location = new System.Drawing.Point(12, 0);
            this.lblRateInfo.Name = "lblRateInfo";
            this.lblRateInfo.Size = new System.Drawing.Size(200, 23);
            this.lblRateInfo.TabIndex = 105;
            this.lblRateInfo.Text = "YEAR INFORMATION";
            // 
            // fraDiscount
            // 
            this.fraDiscount.Controls.Add(this.cmdDisc_3);
            this.fraDiscount.Controls.Add(this.txtDiscDisc);
            this.fraDiscount.Controls.Add(this.txtDiscPrin);
            this.fraDiscount.Controls.Add(this.cmdDisc_2);
            this.fraDiscount.Controls.Add(this.cmdDisc_1);
            this.fraDiscount.Controls.Add(this.cmdDisc_0);
            this.fraDiscount.Controls.Add(this.lblOriginalTax);
            this.fraDiscount.Controls.Add(this.lblOriginalTaxLabel);
            this.fraDiscount.Controls.Add(this.lblDiscHiddenTotal);
            this.fraDiscount.Controls.Add(this.lblDiscTotal);
            this.fraDiscount.Controls.Add(this.Label11);
            this.fraDiscount.Controls.Add(this.Label10);
            this.fraDiscount.Controls.Add(this.Label9);
            this.fraDiscount.Controls.Add(this.lblDiscountInstructions);
            this.fraDiscount.Location = new System.Drawing.Point(27, 76);
            this.fraDiscount.Name = "fraDiscount";
            this.fraDiscount.Size = new System.Drawing.Size(447, 352);
            this.fraDiscount.TabIndex = 54;
            this.fraDiscount.Visible = false;
            // 
            // cmdDisc_3
            // 
            this.cmdDisc_3.Location = new System.Drawing.Point(230, 299);
            this.cmdDisc_3.Name = "cmdDisc_3";
            this.cmdDisc_3.Size = new System.Drawing.Size(62, 27);
            this.cmdDisc_3.TabIndex = 68;
            this.cmdDisc_3.Text = "Recalc";
            this.ToolTip1.SetToolTip(this.cmdDisc_3, "Recalculate the discount on the original principal.");
            this.cmdDisc_3.Click += new System.EventHandler(this.cmdDisc_Click);
            // 
            // txtDiscDisc
            // 
            this.txtDiscDisc.BackColor = System.Drawing.SystemColors.Window;
            this.txtDiscDisc.Location = new System.Drawing.Point(128, 169);
            this.txtDiscDisc.MaxLength = 12;
            this.txtDiscDisc.Name = "txtDiscDisc";
            this.txtDiscDisc.Size = new System.Drawing.Size(115, 40);
            this.txtDiscDisc.TabIndex = 60;
            this.txtDiscDisc.Validating += new System.ComponentModel.CancelEventHandler(this.txtDiscDisc_Validating);
            this.txtDiscDisc.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDiscDisc_KeyPress);
            // 
            // txtDiscPrin
            // 
            this.txtDiscPrin.BackColor = System.Drawing.SystemColors.Window;
            this.txtDiscPrin.Location = new System.Drawing.Point(128, 114);
            this.txtDiscPrin.MaxLength = 12;
            this.txtDiscPrin.Name = "txtDiscPrin";
            this.txtDiscPrin.Size = new System.Drawing.Size(115, 40);
            this.txtDiscPrin.TabIndex = 58;
            this.txtDiscPrin.Validating += new System.ComponentModel.CancelEventHandler(this.txtDiscPrin_Validating);
            this.txtDiscPrin.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDiscPrin_KeyPress);
            // 
            // cmdDisc_2
            // 
            this.cmdDisc_2.Location = new System.Drawing.Point(300, 299);
            this.cmdDisc_2.Name = "cmdDisc_2";
            this.cmdDisc_2.Size = new System.Drawing.Size(62, 27);
            this.cmdDisc_2.TabIndex = 67;
            this.cmdDisc_2.Text = "Cancel";
            this.cmdDisc_2.Click += new System.EventHandler(this.cmdDisc_Click);
            // 
            // cmdDisc_1
            // 
            this.cmdDisc_1.Location = new System.Drawing.Point(160, 299);
            this.cmdDisc_1.Name = "cmdDisc_1";
            this.cmdDisc_1.Size = new System.Drawing.Size(62, 27);
            this.cmdDisc_1.TabIndex = 66;
            this.cmdDisc_1.Text = "No";
            this.cmdDisc_1.Click += new System.EventHandler(this.cmdDisc_Click);
            // 
            // cmdDisc_0
            // 
            this.cmdDisc_0.Location = new System.Drawing.Point(90, 299);
            this.cmdDisc_0.Name = "cmdDisc_0";
            this.cmdDisc_0.Size = new System.Drawing.Size(62, 27);
            this.cmdDisc_0.TabIndex = 65;
            this.cmdDisc_0.Text = "Yes";
            this.cmdDisc_0.Click += new System.EventHandler(this.cmdDisc_Click);
            // 
            // lblOriginalTax
            // 
            this.lblOriginalTax.Location = new System.Drawing.Point(128, 53);
            this.lblOriginalTax.Name = "lblOriginalTax";
            this.lblOriginalTax.Size = new System.Drawing.Size(88, 40);
            this.lblOriginalTax.TabIndex = 57;
            this.lblOriginalTax.Text = "0.00";
            this.lblOriginalTax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ToolTip1.SetToolTip(this.lblOriginalTax, "Double click to discount the original tax amount.");
            this.lblOriginalTax.DoubleClick += new System.EventHandler(this.lblOriginalTax_DoubleClick);
            // 
            // lblOriginalTaxLabel
            // 
            this.lblOriginalTaxLabel.Location = new System.Drawing.Point(33, 66);
            this.lblOriginalTaxLabel.Name = "lblOriginalTaxLabel";
            this.lblOriginalTaxLabel.Size = new System.Drawing.Size(94, 19);
            this.lblOriginalTaxLabel.TabIndex = 56;
            this.lblOriginalTaxLabel.Text = "ORIGINAL TAX";
            this.ToolTip1.SetToolTip(this.lblOriginalTaxLabel, "Double click to discount the original tax amount.");
            this.lblOriginalTaxLabel.DoubleClick += new System.EventHandler(this.lblOriginalTaxLabel_DoubleClick);
            // 
            // lblDiscHiddenTotal
            // 
            this.lblDiscHiddenTotal.Location = new System.Drawing.Point(254, 184);
            this.lblDiscHiddenTotal.Name = "lblDiscHiddenTotal";
            this.lblDiscHiddenTotal.Size = new System.Drawing.Size(59, 25);
            this.lblDiscHiddenTotal.TabIndex = 64;
            this.lblDiscHiddenTotal.Visible = false;
            // 
            // lblDiscTotal
            // 
            this.lblDiscTotal.Location = new System.Drawing.Point(128, 229);
            this.lblDiscTotal.Name = "lblDiscTotal";
            this.lblDiscTotal.Size = new System.Drawing.Size(88, 40);
            this.lblDiscTotal.TabIndex = 63;
            this.lblDiscTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(33, 236);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(72, 19);
            this.Label11.TabIndex = 62;
            this.Label11.Text = "TOTAL";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(33, 184);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(72, 19);
            this.Label10.TabIndex = 61;
            this.Label10.Text = "DISCOUNT";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(33, 126);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(72, 19);
            this.Label9.TabIndex = 59;
            this.Label9.Text = "PRINCIPAL";
            // 
            // lblDiscountInstructions
            // 
            this.lblDiscountInstructions.Location = new System.Drawing.Point(6, 4);
            this.lblDiscountInstructions.Name = "lblDiscountInstructions";
            this.lblDiscountInstructions.Size = new System.Drawing.Size(433, 44);
            this.lblDiscountInstructions.TabIndex = 55;
            this.lblDiscountInstructions.Text = "IS THIS PRINCIPAL PAYMENT AND DISCOUNT CORRECT?";
            this.lblDiscountInstructions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraStatusLabels
            // 
            this.fraStatusLabels.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraStatusLabels.Controls.Add(this.lblLandValue2);
            this.fraStatusLabels.Controls.Add(this.fcLabel1);
            this.fraStatusLabels.Controls.Add(this.Label6);
            this.fraStatusLabels.Controls.Add(this.Label7);
            this.fraStatusLabels.Controls.Add(this.Line1);
            this.fraStatusLabels.Controls.Add(this.lblExemptValue2);
            this.fraStatusLabels.Controls.Add(this.lblExemptValue);
            this.fraStatusLabels.Controls.Add(this.lblBuildingValue2);
            this.fraStatusLabels.Controls.Add(this.lblBuildingValue);
            this.fraStatusLabels.Controls.Add(this.lblOwnersName);
            this.fraStatusLabels.Controls.Add(this.Line3);
            this.fraStatusLabels.Controls.Add(this.Line2);
            this.fraStatusLabels.Controls.Add(this.lblTotalValue2);
            this.fraStatusLabels.Controls.Add(this.lblYear);
            this.fraStatusLabels.Controls.Add(this.Label5);
            this.fraStatusLabels.Controls.Add(this.lblExempt);
            this.fraStatusLabels.Controls.Add(this.Label4);
            this.fraStatusLabels.Controls.Add(this.Label3);
            this.fraStatusLabels.Controls.Add(this.lblLandValue);
            this.fraStatusLabels.Controls.Add(this.lblBuilding);
            this.fraStatusLabels.Controls.Add(this.lblLand);
            this.fraStatusLabels.Controls.Add(this.lblTotal);
            this.fraStatusLabels.Controls.Add(this.lblTotalValue);
            this.fraStatusLabels.Controls.Add(this.lblBookPage);
            this.fraStatusLabels.Controls.Add(this.lblReference1);
            this.fraStatusLabels.Controls.Add(this.lblAcreage);
            this.fraStatusLabels.Controls.Add(this.lblMapLot);
            this.fraStatusLabels.Controls.Add(this.lblLocation);
            this.fraStatusLabels.Location = new System.Drawing.Point(3, 3);
            this.fraStatusLabels.Name = "fraStatusLabels";
            this.fraStatusLabels.Size = new System.Drawing.Size(0, 91);
            this.fraStatusLabels.TabIndex = 25;
            this.fraStatusLabels.Visible = false;
            // 
            // lblLandValue2
            // 
            this.lblLandValue2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblLandValue2.Location = new System.Drawing.Point(-95, 20);
            this.lblLandValue2.Name = "lblLandValue2";
            this.lblLandValue2.Size = new System.Drawing.Size(82, 18);
            this.lblLandValue2.TabIndex = 33;
            this.lblLandValue2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblLandValue2, "From Real Estate Short Screen");
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(30, 13);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(47, 17);
            this.fcLabel1.TabIndex = 52;
            this.fcLabel1.Text = "NAME";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(30, 89);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(75, 17);
            this.Label6.TabIndex = 39;
            this.Label6.Text = "ACREAGE";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(456, 89);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(35, 17);
            this.Label7.TabIndex = 46;
            this.Label7.Text = "REF";
            // 
            // Line1
            // 
            this.Line1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.Line1.LineColor = System.Drawing.Color.FromArgb(230, 234, 238);
            this.Line1.Location = new System.Drawing.Point(-246, 87);
            this.Line1.Name = "Line1";
            this.Line1.Size = new System.Drawing.Size(231, 1);
            this.Line1.X1 = 357F;
            this.Line1.X2 = 588F;
            this.Line1.Y1 = 65F;
            this.Line1.Y2 = 65F;
            // 
            // lblExemptValue2
            // 
            this.lblExemptValue2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblExemptValue2.Location = new System.Drawing.Point(-95, 64);
            this.lblExemptValue2.Name = "lblExemptValue2";
            this.lblExemptValue2.Size = new System.Drawing.Size(82, 18);
            this.lblExemptValue2.TabIndex = 45;
            this.lblExemptValue2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblExemptValue
            // 
            this.lblExemptValue.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblExemptValue.Location = new System.Drawing.Point(-184, 64);
            this.lblExemptValue.Name = "lblExemptValue";
            this.lblExemptValue.Size = new System.Drawing.Size(82, 18);
            this.lblExemptValue.TabIndex = 44;
            this.lblExemptValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblExemptValue, "From Real Estate Short Screen");
            // 
            // lblBuildingValue2
            // 
            this.lblBuildingValue2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblBuildingValue2.Location = new System.Drawing.Point(-95, 42);
            this.lblBuildingValue2.Name = "lblBuildingValue2";
            this.lblBuildingValue2.Size = new System.Drawing.Size(82, 18);
            this.lblBuildingValue2.TabIndex = 38;
            this.lblBuildingValue2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblBuildingValue
            // 
            this.lblBuildingValue.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblBuildingValue.Location = new System.Drawing.Point(-184, 42);
            this.lblBuildingValue.Name = "lblBuildingValue";
            this.lblBuildingValue.Size = new System.Drawing.Size(82, 18);
            this.lblBuildingValue.TabIndex = 37;
            this.lblBuildingValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblBuildingValue, "From Real Estate Short Screen");
            // 
            // lblOwnersName
            // 
            this.lblOwnersName.BackColor = System.Drawing.Color.Transparent;
            this.lblOwnersName.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblOwnersName.ForeColor = System.Drawing.Color.FromArgb(53, 64, 82);
            this.lblOwnersName.Location = new System.Drawing.Point(108, 10);
            this.lblOwnersName.MinimumSize = new System.Drawing.Size(200, 0);
            this.lblOwnersName.Name = "lblOwnersName";
            this.lblOwnersName.Size = new System.Drawing.Size(538, 19);
            this.lblOwnersName.TabIndex = 28;
            this.lblOwnersName.Visible = false;
            // 
            // Line3
            // 
            this.Line3.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.Line3.LineColor = System.Drawing.Color.FromArgb(230, 234, 238);
            this.Line3.Location = new System.Drawing.Point(-56, 16);
            this.Line3.Name = "Line3";
            this.Line3.Size = new System.Drawing.Size(50, 1);
            this.Line3.X1 = 546F;
            this.Line3.X2 = 588F;
            this.Line3.Y1 = 16F;
            this.Line3.Y2 = 16F;
            // 
            // Line2
            // 
            this.Line2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.Line2.LineColor = System.Drawing.Color.FromArgb(230, 234, 238);
            this.Line2.Location = new System.Drawing.Point(-152, 16);
            this.Line2.Name = "Line2";
            this.Line2.Size = new System.Drawing.Size(60, 1);
            this.Line2.X1 = 461F;
            this.Line2.X2 = 503F;
            this.Line2.Y1 = 16F;
            this.Line2.Y2 = 16F;
            // 
            // lblTotalValue2
            // 
            this.lblTotalValue2.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblTotalValue2.Location = new System.Drawing.Point(-95, 92);
            this.lblTotalValue2.Name = "lblTotalValue2";
            this.lblTotalValue2.Size = new System.Drawing.Size(82, 18);
            this.lblTotalValue2.TabIndex = 50;
            this.lblTotalValue2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblYear
            // 
            this.lblYear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new System.Drawing.Point(-49, 0);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(45, 17);
            this.lblYear.TabIndex = 30;
            this.lblYear.Text = "YEAR";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label5
            // 
            this.Label5.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(-161, 0);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(73, 17);
            this.Label5.TabIndex = 29;
            this.Label5.Text = "CURRENT";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label5, "From Real Estate Short Screen");
            // 
            // lblExempt
            // 
            this.lblExempt.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblExempt.Location = new System.Drawing.Point(-248, 64);
            this.lblExempt.Name = "lblExempt";
            this.lblExempt.Size = new System.Drawing.Size(63, 17);
            this.lblExempt.TabIndex = 43;
            this.lblExempt.Text = "EXEMPT";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(200, 89);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(68, 17);
            this.Label4.TabIndex = 41;
            this.Label4.Text = "MAP LOT";
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(30, 52);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(78, 17);
            this.Label3.TabIndex = 34;
            this.Label3.Text = "LOCATION";
            // 
            // lblLandValue
            // 
            this.lblLandValue.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblLandValue.Location = new System.Drawing.Point(-184, 20);
            this.lblLandValue.Name = "lblLandValue";
            this.lblLandValue.Size = new System.Drawing.Size(82, 18);
            this.lblLandValue.TabIndex = 32;
            this.lblLandValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblLandValue, "From Real Estate Short Screen");
            // 
            // lblBuilding
            // 
            this.lblBuilding.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblBuilding.Location = new System.Drawing.Point(-248, 42);
            this.lblBuilding.Name = "lblBuilding";
            this.lblBuilding.Size = new System.Drawing.Size(73, 17);
            this.lblBuilding.TabIndex = 36;
            this.lblBuilding.Text = "BUILDING";
            // 
            // lblLand
            // 
            this.lblLand.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblLand.Location = new System.Drawing.Point(-248, 20);
            this.lblLand.Name = "lblLand";
            this.lblLand.Size = new System.Drawing.Size(45, 17);
            this.lblLand.TabIndex = 31;
            this.lblLand.Text = "LAND";
            // 
            // lblTotal
            // 
            this.lblTotal.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblTotal.Location = new System.Drawing.Point(-247, 92);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(52, 17);
            this.lblTotal.TabIndex = 51;
            this.lblTotal.Text = "TOTAL";
            // 
            // lblTotalValue
            // 
            this.lblTotalValue.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblTotalValue.Location = new System.Drawing.Point(-184, 92);
            this.lblTotalValue.Name = "lblTotalValue";
            this.lblTotalValue.Size = new System.Drawing.Size(82, 18);
            this.lblTotalValue.TabIndex = 49;
            this.lblTotalValue.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.ToolTip1.SetToolTip(this.lblTotalValue, "From Real Estate Short Screen");
            // 
            // lblBookPage
            // 
            this.lblBookPage.BackColor = System.Drawing.Color.Transparent;
            this.lblBookPage.Location = new System.Drawing.Point(500, 59);
            this.lblBookPage.Name = "lblBookPage";
            this.lblBookPage.Size = new System.Drawing.Size(284, 17);
            this.lblBookPage.TabIndex = 48;
            this.lblBookPage.Visible = false;
            // 
            // lblReference1
            // 
            this.lblReference1.BackColor = System.Drawing.Color.Transparent;
            this.lblReference1.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblReference1.ForeColor = System.Drawing.Color.FromArgb(53, 64, 82);
            this.lblReference1.Location = new System.Drawing.Point(504, 84);
            this.lblReference1.Name = "lblReference1";
            this.lblReference1.Size = new System.Drawing.Size(246, 35);
            this.lblReference1.TabIndex = 47;
            // 
            // lblAcreage
            // 
            this.lblAcreage.BackColor = System.Drawing.Color.Transparent;
            this.lblAcreage.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblAcreage.ForeColor = System.Drawing.Color.FromArgb(53, 64, 82);
            this.lblAcreage.Location = new System.Drawing.Point(108, 84);
            this.lblAcreage.Name = "lblAcreage";
            this.lblAcreage.Size = new System.Drawing.Size(55, 28);
            this.lblAcreage.TabIndex = 40;
            // 
            // lblMapLot
            // 
            this.lblMapLot.BackColor = System.Drawing.Color.Transparent;
            this.lblMapLot.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblMapLot.ForeColor = System.Drawing.Color.FromArgb(53, 64, 82);
            this.lblMapLot.Location = new System.Drawing.Point(274, 84);
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Size = new System.Drawing.Size(157, 28);
            this.lblMapLot.TabIndex = 42;
            this.lblMapLot.Visible = false;
            // 
            // lblLocation
            // 
            this.lblLocation.BackColor = System.Drawing.Color.Transparent;
            this.lblLocation.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblLocation.ForeColor = System.Drawing.Color.FromArgb(53, 64, 82);
            this.lblLocation.Location = new System.Drawing.Point(108, 47);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(538, 28);
            this.lblLocation.TabIndex = 35;
            this.lblLocation.Visible = false;
            // 
            // lblAccount
            // 
            this.lblAccount.AppearanceKey = "Header";
            this.lblAccount.BackColor = System.Drawing.Color.Transparent;
            this.lblAccount.Location = new System.Drawing.Point(135, 26);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(132, 35);
            this.lblAccount.TabIndex = 27;
            this.lblAccount.Visible = false;
            // 
            // CommonDialog1
            // 
            this.CommonDialog1.Name = "CommonDialog1";
            this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
            // 
            // fraPPStatusLabels
            // 
            this.fraPPStatusLabels.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraPPStatusLabels.Controls.Add(this.vsPPCategory);
            this.fraPPStatusLabels.Controls.Add(this.Label2);
            this.fraPPStatusLabels.Controls.Add(this.lblPPAccountShow);
            this.fraPPStatusLabels.Controls.Add(this.lblPPAddress);
            this.fraPPStatusLabels.Controls.Add(this.lblPPOwnersName);
            this.fraPPStatusLabels.Location = new System.Drawing.Point(1, 0);
            this.fraPPStatusLabels.Name = "fraPPStatusLabels";
            this.fraPPStatusLabels.Size = new System.Drawing.Size(0, 120);
            this.fraPPStatusLabels.TabIndex = 18;
            this.fraPPStatusLabels.Visible = false;
            // 
            // vsPPCategory
            // 
            this.vsPPCategory.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.vsPPCategory.Cols = 3;
            this.vsPPCategory.FixedCols = 0;
            this.vsPPCategory.Location = new System.Drawing.Point(-477, 0);
            this.vsPPCategory.MinimumSize = new System.Drawing.Size(457, 110);
            this.vsPPCategory.Name = "vsPPCategory";
            this.vsPPCategory.RowHeadersVisible = false;
            this.vsPPCategory.Rows = 1;
            this.vsPPCategory.Size = new System.Drawing.Size(457, 110);
            this.vsPPCategory.StandardTab = false;
            this.vsPPCategory.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsPPCategory.TabIndex = 24;
            this.vsPPCategory.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsPPCategory_CellFormatting);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(27, 57);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(59, 35);
            this.Label2.TabIndex = 23;
            this.Label2.Text = "MAILING ADDRESS";
            // 
            // lblPPAccountShow
            // 
            this.lblPPAccountShow.Location = new System.Drawing.Point(27, 19);
            this.lblPPAccountShow.Name = "lblPPAccountShow";
            this.lblPPAccountShow.Size = new System.Drawing.Size(59, 18);
            this.lblPPAccountShow.TabIndex = 20;
            this.lblPPAccountShow.Text = "NAME";
            // 
            // lblPPAddress
            // 
            this.lblPPAddress.BackColor = System.Drawing.Color.Transparent;
            this.lblPPAddress.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblPPAddress.ForeColor = System.Drawing.Color.FromArgb(53, 64, 82);
            this.lblPPAddress.Location = new System.Drawing.Point(113, 49);
            this.lblPPAddress.Name = "lblPPAddress";
            this.lblPPAddress.Size = new System.Drawing.Size(378, 61);
            this.lblPPAddress.TabIndex = 22;
            // 
            // lblPPOwnersName
            // 
            this.lblPPOwnersName.BackColor = System.Drawing.Color.Transparent;
            this.lblPPOwnersName.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblPPOwnersName.ForeColor = System.Drawing.Color.FromArgb(53, 64, 82);
            this.lblPPOwnersName.Location = new System.Drawing.Point(113, 14);
            this.lblPPOwnersName.Name = "lblPPOwnersName";
            this.lblPPOwnersName.Size = new System.Drawing.Size(378, 28);
            this.lblPPOwnersName.TabIndex = 21;
            // 
            // lblPPAccount
            // 
            this.lblPPAccount.AppearanceKey = "Header";
            this.lblPPAccount.BackColor = System.Drawing.Color.Transparent;
            this.lblPPAccount.Location = new System.Drawing.Point(135, 26);
            this.lblPPAccount.Name = "lblPPAccount";
            this.lblPPAccount.Size = new System.Drawing.Size(132, 35);
            this.lblPPAccount.TabIndex = 19;
            // 
            // fraPayment
            // 
            this.fraPayment.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraPayment.Controls.Add(this.fcLabel2);
            this.fraPayment.Controls.Add(this.txtTotalPendingDue);
            this.fraPayment.Controls.Add(this.txtCash);
            this.fraPayment.Controls.Add(this.vsPayments);
            this.fraPayment.Controls.Add(this.txtPrincipal);
            this.fraPayment.Controls.Add(this.txtCosts);
            this.fraPayment.Controls.Add(this.txtTransactionDate2);
            this.fraPayment.Controls.Add(this.txtInterest);
            this.fraPayment.Controls.Add(this.txtComments);
            this.fraPayment.Controls.Add(this.txtBLID);
            this.fraPayment.Controls.Add(this.txtAcctNumber);
            this.fraPayment.Controls.Add(this.txtTransactionDate);
            this.fraPayment.Controls.Add(this.vsPeriod);
            this.fraPayment.Controls.Add(this.cmbPeriod);
            this.fraPayment.Controls.Add(this.cmbCode);
            this.fraPayment.Controls.Add(this.txtReference);
            this.fraPayment.Controls.Add(this.cmbYear);
            this.fraPayment.Controls.Add(this.txtCD);
            this.fraPayment.Controls.Add(this.lblTaxClub);
            this.fraPayment.Controls.Add(this.Label1_9);
            this.fraPayment.Controls.Add(this.lblTotalPendingDue);
            this.fraPayment.Controls.Add(this.Label1_11);
            this.fraPayment.Controls.Add(this.Label1_1);
            this.fraPayment.Controls.Add(this.Label1_2);
            this.fraPayment.Controls.Add(this.lblPrincipal);
            this.fraPayment.Controls.Add(this.lblInterest);
            this.fraPayment.Controls.Add(this.lblCosts);
            this.fraPayment.Controls.Add(this.Label1_10);
            this.fraPayment.Controls.Add(this.Label1_3);
            this.fraPayment.Controls.Add(this.Label1_8);
            this.fraPayment.Controls.Add(this.Label8);
            this.fraPayment.Controls.Add(this.Label1_12);
            this.fraPayment.Location = new System.Drawing.Point(22, 313);
            this.fraPayment.MaximumSize = new System.Drawing.Size(2000, 400);
            this.fraPayment.MinimumSize = new System.Drawing.Size(800, 320);
            this.fraPayment.Name = "fraPayment";
            this.fraPayment.Size = new System.Drawing.Size(800, 400);
            this.fraPayment.TabIndex = 69;
            this.fraPayment.Visible = false;
            // 
            // fcLabel2
            // 
            this.fcLabel2.AutoSize = true;
            this.fcLabel2.Location = new System.Drawing.Point(414, 67);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(47, 17);
            this.fcLabel2.TabIndex = 102;
            this.fcLabel2.Text = "CODE";
            // 
            // txtTotalPendingDue
            // 
            this.txtTotalPendingDue.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.txtTotalPendingDue.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotalPendingDue.Location = new System.Drawing.Point(585, 4);
            this.txtTotalPendingDue.LockedOriginal = true;
            this.txtTotalPendingDue.Name = "txtTotalPendingDue";
            this.txtTotalPendingDue.ReadOnly = true;
            this.txtTotalPendingDue.Size = new System.Drawing.Size(205, 40);
            this.txtTotalPendingDue.TabIndex = 73;
            this.txtTotalPendingDue.Text = "0.00";
            this.txtTotalPendingDue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtTotalPendingDue.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTotalPendingDue_KeyPress);
            // 
            // txtCash
            // 
            this.txtCash.BackColor = System.Drawing.Color.FromName("@window");
            this.txtCash.Enabled = false;
            this.txtCash.Location = new System.Drawing.Point(524, 85);
            this.txtCash.MaxLength = 1;
            this.txtCash.Name = "txtCash";
            this.txtCash.Size = new System.Drawing.Size(42, 40);
            this.txtCash.TabIndex = 95;
            this.txtCash.Text = "Y";
            this.txtCash.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtCash.Enter += new System.EventHandler(this.txtCash_Enter);
            this.txtCash.DoubleClick += new System.EventHandler(this.txtCash_DoubleClick);
            this.txtCash.TextChanged += new System.EventHandler(this.txtCash_TextChanged);
            this.txtCash.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCash_KeyDown);
            this.txtCash.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCash_KeyPress);
            // 
            // vsPayments
            // 
            this.vsPayments.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsPayments.Cols = 13;
            this.vsPayments.FixedCols = 0;
            this.vsPayments.Location = new System.Drawing.Point(8, 186);
            this.vsPayments.MinimumSize = new System.Drawing.Size(800, 128);
            this.vsPayments.Name = "vsPayments";
            this.vsPayments.RowHeadersVisible = false;
            this.vsPayments.Rows = 1;
            this.vsPayments.Size = new System.Drawing.Size(800, 208);
            this.vsPayments.TabIndex = 99;
            this.vsPayments.KeyDown += new Wisej.Web.KeyEventHandler(this.vsPayments_KeyDown);
            // 
            // txtPrincipal
            // 
            this.txtPrincipal.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrincipal.Location = new System.Drawing.Point(723, 85);
            this.txtPrincipal.MaxLength = 17;
            this.txtPrincipal.Name = "txtPrincipal";
            this.txtPrincipal.Size = new System.Drawing.Size(137, 40);
            this.txtPrincipal.TabIndex = 91;
            this.txtPrincipal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrincipal.Enter += new System.EventHandler(this.txtPrincipal_Enter);
            this.txtPrincipal.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrincipal_Validating);
            this.txtPrincipal.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPrincipal_KeyDown);
            this.txtPrincipal.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPrincipal_KeyPress);
            // 
            // txtCosts
            // 
            this.txtCosts.BackColor = System.Drawing.SystemColors.Window;
            this.txtCosts.Location = new System.Drawing.Point(1013, 86);
            this.txtCosts.MaxLength = 11;
            this.txtCosts.Name = "txtCosts";
            this.txtCosts.Size = new System.Drawing.Size(97, 40);
            this.txtCosts.TabIndex = 93;
            this.txtCosts.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCosts.Enter += new System.EventHandler(this.txtCosts_Enter);
            this.txtCosts.Validating += new System.ComponentModel.CancelEventHandler(this.txtCosts_Validating);
            this.txtCosts.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCosts_KeyDown);
            this.txtCosts.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCosts_KeyPress);
            // 
            // txtTransactionDate2
            // 
            this.txtTransactionDate2.Appearance = 0;
            this.txtTransactionDate2.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactionDate2.BorderStyle = Wisej.Web.BorderStyle.None;
            this.txtTransactionDate2.Location = new System.Drawing.Point(240, 135);
            this.txtTransactionDate2.MaxLength = 10;
            this.txtTransactionDate2.Name = "txtTransactionDate2";
            this.txtTransactionDate2.Size = new System.Drawing.Size(57, 40);
            this.txtTransactionDate2.TabIndex = 72;
            this.txtTransactionDate2.Visible = false;
            // 
            // txtInterest
            // 
            this.txtInterest.BackColor = System.Drawing.SystemColors.Window;
            this.txtInterest.Location = new System.Drawing.Point(868, 85);
            this.txtInterest.MaxLength = 12;
            this.txtInterest.Name = "txtInterest";
            this.txtInterest.Size = new System.Drawing.Size(137, 40);
            this.txtInterest.TabIndex = 92;
            this.txtInterest.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtInterest.Enter += new System.EventHandler(this.txtInterest_Enter);
            this.txtInterest.Validating += new System.ComponentModel.CancelEventHandler(this.txtInterest_Validating);
            this.txtInterest.KeyDown += new Wisej.Web.KeyEventHandler(this.txtInterest_KeyDown);
            this.txtInterest.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtInterest_KeyPress);
            // 
            // txtComments
            // 
            this.txtComments.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtComments.BackColor = System.Drawing.SystemColors.Window;
            this.txtComments.Location = new System.Drawing.Point(415, 137);
            this.txtComments.MaxLength = 255;
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(379, 40);
            this.txtComments.TabIndex = 94;
            this.txtComments.KeyDown += new Wisej.Web.KeyEventHandler(this.txtComments_KeyDown);
            // 
            // txtBLID
            // 
            this.txtBLID.BackColor = System.Drawing.SystemColors.Window;
            this.txtBLID.Location = new System.Drawing.Point(1116, 86);
            this.txtBLID.Name = "txtBLID";
            this.txtBLID.Size = new System.Drawing.Size(34, 40);
            this.txtBLID.TabIndex = 101;
            this.txtBLID.Visible = false;
            // 
            // txtAcctNumber
            // 
            this.txtAcctNumber.Cols = 1;
            this.txtAcctNumber.ColumnHeadersVisible = false;
            this.txtAcctNumber.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.txtAcctNumber.FixedCols = 0;
            this.txtAcctNumber.FixedRows = 0;
            this.txtAcctNumber.FocusRect = fecherFoundation.FCGrid.FocusRectSettings.flexFocusNone;
            this.txtAcctNumber.Location = new System.Drawing.Point(572, 84);
            this.txtAcctNumber.Name = "txtAcctNumber";
            this.txtAcctNumber.ReadOnly = false;
            this.txtAcctNumber.RowHeadersVisible = false;
            this.txtAcctNumber.Rows = 1;
            this.txtAcctNumber.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.txtAcctNumber.Size = new System.Drawing.Size(143, 42);
            this.txtAcctNumber.TabIndex = 90;
            this.txtAcctNumber.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.txtAcctNumber_ValidateEdit);
            this.txtAcctNumber.GotFocus += new System.EventHandler(this.txtAcctNumber_GotFocus);
            this.txtAcctNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAcctNumber_KeyDown);
            // 
            // txtTransactionDate
            // 
            this.txtTransactionDate.Location = new System.Drawing.Point(110, 85);
            this.txtTransactionDate.Mask = "##/##/####";
            this.txtTransactionDate.MaxLength = 10;
            this.txtTransactionDate.Name = "txtTransactionDate";
            this.txtTransactionDate.Size = new System.Drawing.Size(113, 22);
            this.txtTransactionDate.TabIndex = 85;
            // 
            // vsPeriod
            // 
            this.vsPeriod.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsPeriod.Cols = 5;
            this.vsPeriod.ColumnHeadersVisible = false;
            this.vsPeriod.FixedCols = 0;
            this.vsPeriod.FixedRows = 0;
            this.vsPeriod.Location = new System.Drawing.Point(8, 4);
            this.vsPeriod.Name = "vsPeriod";
            this.vsPeriod.RowHeadersVisible = false;
            this.vsPeriod.Rows = 1;
            this.vsPeriod.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.vsPeriod.Size = new System.Drawing.Size(437, 40);
            this.vsPeriod.TabIndex = 70;
            this.vsPeriod.TabStop = false;
            this.vsPeriod.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsPeriod_CellFormatting);
            this.vsPeriod.DoubleClick += new System.EventHandler(this.vsPeriod_DblClick);
            // 
            // cmbPeriod
            // 
            this.cmbPeriod.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPeriod.Location = new System.Drawing.Point(351, 85);
            this.cmbPeriod.Name = "cmbPeriod";
            this.cmbPeriod.Size = new System.Drawing.Size(57, 40);
            this.cmbPeriod.Sorted = true;
            this.cmbPeriod.TabIndex = 87;
            this.cmbPeriod.SelectedIndexChanged += new System.EventHandler(this.cmbPeriod_SelectedIndexChanged);
            this.cmbPeriod.DropDown += new System.EventHandler(this.cmbPeriod_DropDown);
            this.cmbPeriod.Enter += new System.EventHandler(this.cmbPeriod_Enter);
            this.cmbPeriod.Validating += new System.ComponentModel.CancelEventHandler(this.cmbPeriod_Validating);
            this.cmbPeriod.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbPeriod_KeyDown);
            // 
            // cmbCode
            // 
            this.cmbCode.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCode.Location = new System.Drawing.Point(414, 85);
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.Size = new System.Drawing.Size(57, 40);
            this.cmbCode.Sorted = true;
            this.cmbCode.TabIndex = 88;
            this.cmbCode.SelectedIndexChanged += new System.EventHandler(this.cmbCode_SelectedIndexChanged);
            this.cmbCode.DropDown += new System.EventHandler(this.cmbCode_DropDown);
            this.cmbCode.Enter += new System.EventHandler(this.cmbCode_Enter);
            this.cmbCode.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbCode_KeyDown);
            // 
            // txtReference
            // 
            this.txtReference.BackColor = System.Drawing.SystemColors.Window;
            this.txtReference.Location = new System.Drawing.Point(229, 85);
            this.txtReference.MaxLength = 6;
            this.txtReference.Name = "txtReference";
            this.txtReference.Size = new System.Drawing.Size(115, 40);
            this.txtReference.TabIndex = 86;
            this.txtReference.Enter += new System.EventHandler(this.txtReference_Enter);
            this.txtReference.Validating += new System.ComponentModel.CancelEventHandler(this.txtReference_Validating);
            this.txtReference.KeyDown += new Wisej.Web.KeyEventHandler(this.txtReference_KeyDown);
            // 
            // cmbYear
            // 
            this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
            this.cmbYear.Location = new System.Drawing.Point(8, 85);
            this.cmbYear.Name = "cmbYear";
            this.cmbYear.Size = new System.Drawing.Size(96, 40);
            this.cmbYear.Sorted = true;
            this.cmbYear.TabIndex = 84;
            this.cmbYear.SelectedIndexChanged += new System.EventHandler(this.cmbYear_SelectedIndexChanged);
            this.cmbYear.DropDown += new System.EventHandler(this.cmbYear_DropDown);
            this.cmbYear.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbYear_KeyDown);
            // 
            // txtCD
            // 
            this.txtCD.BackColor = System.Drawing.Color.FromName("@window");
            this.txtCD.Location = new System.Drawing.Point(476, 85);
            this.txtCD.MaxLength = 1;
            this.txtCD.Name = "txtCD";
            this.txtCD.Size = new System.Drawing.Size(42, 40);
            this.txtCD.TabIndex = 89;
            this.txtCD.Text = "Y";
            this.txtCD.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtCD.Enter += new System.EventHandler(this.txtCD_Enter);
            this.txtCD.DoubleClick += new System.EventHandler(this.txtCD_DoubleClick);
            this.txtCD.TextChanged += new System.EventHandler(this.txtCD_TextChanged);
            this.txtCD.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCD_KeyDown);
            this.txtCD.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCD_KeyPress);
            // 
            // lblTaxClub
            // 
            this.lblTaxClub.AutoSize = true;
            this.lblTaxClub.Location = new System.Drawing.Point(205, 148);
            this.lblTaxClub.Name = "lblTaxClub";
            this.lblTaxClub.Size = new System.Drawing.Size(26, 17);
            this.lblTaxClub.TabIndex = 97;
            this.lblTaxClub.Text = "TC";
            this.ToolTip1.SetToolTip(this.lblTaxClub, "Tax Club Payment");
            this.lblTaxClub.Visible = false;
            this.lblTaxClub.Click += new System.EventHandler(this.lblTaxClub_Click);
            this.lblTaxClub.DoubleClick += new System.EventHandler(this.lblTaxClub_DoubleClick);
            // 
            // Label1_9
            // 
            this.Label1_9.AutoSize = true;
            this.Label1_9.Location = new System.Drawing.Point(476, 67);
            this.Label1_9.Name = "Label1_9";
            this.Label1_9.Size = new System.Drawing.Size(27, 17);
            this.Label1_9.TabIndex = 76;
            this.Label1_9.Text = "CD";
            this.Label1_9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label1_9, "Cash Drawer");
            // 
            // lblTotalPendingDue
            // 
            this.lblTotalPendingDue.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.lblTotalPendingDue.AutoSize = true;
            this.lblTotalPendingDue.Location = new System.Drawing.Point(459, 20);
            this.lblTotalPendingDue.Name = "lblTotalPendingDue";
            this.lblTotalPendingDue.Size = new System.Drawing.Size(117, 17);
            this.lblTotalPendingDue.TabIndex = 71;
            this.lblTotalPendingDue.Text = "TOTAL PENDING";
            // 
            // Label1_11
            // 
            this.Label1_11.AutoSize = true;
            this.Label1_11.Location = new System.Drawing.Point(572, 67);
            this.Label1_11.Name = "Label1_11";
            this.Label1_11.Size = new System.Drawing.Size(56, 17);
            this.Label1_11.TabIndex = 83;
            this.Label1_11.Text = "ACCT #";
            // 
            // Label1_1
            // 
            this.Label1_1.AutoSize = true;
            this.Label1_1.Location = new System.Drawing.Point(110, 67);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(44, 17);
            this.Label1_1.TabIndex = 80;
            this.Label1_1.Text = "DATE";
            // 
            // Label1_2
            // 
            this.Label1_2.AutoSize = true;
            this.Label1_2.Location = new System.Drawing.Point(351, 67);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(35, 17);
            this.Label1_2.TabIndex = 74;
            this.Label1_2.Text = "PER";
            // 
            // lblPrincipal
            // 
            this.lblPrincipal.AutoSize = true;
            this.lblPrincipal.Location = new System.Drawing.Point(723, 67);
            this.lblPrincipal.Name = "lblPrincipal";
            this.lblPrincipal.Size = new System.Drawing.Size(80, 17);
            this.lblPrincipal.TabIndex = 77;
            this.lblPrincipal.Text = "PRINCIPAL";
            this.lblPrincipal.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblInterest
            // 
            this.lblInterest.AutoSize = true;
            this.lblInterest.Location = new System.Drawing.Point(868, 67);
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Size = new System.Drawing.Size(74, 17);
            this.lblInterest.TabIndex = 78;
            this.lblInterest.Text = "INTEREST";
            this.lblInterest.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblCosts
            // 
            this.lblCosts.AutoSize = true;
            this.lblCosts.Location = new System.Drawing.Point(1013, 68);
            this.lblCosts.Name = "lblCosts";
            this.lblCosts.Size = new System.Drawing.Size(54, 17);
            this.lblCosts.TabIndex = 79;
            this.lblCosts.Text = "COSTS";
            this.lblCosts.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label1_10
            // 
            this.Label1_10.AutoSize = true;
            this.Label1_10.Location = new System.Drawing.Point(229, 67);
            this.Label1_10.Name = "Label1_10";
            this.Label1_10.Size = new System.Drawing.Size(35, 17);
            this.Label1_10.TabIndex = 82;
            this.Label1_10.Text = "REF";
            // 
            // Label1_3
            // 
            this.Label1_3.AutoSize = true;
            this.Label1_3.Location = new System.Drawing.Point(8, 67);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(45, 17);
            this.Label1_3.TabIndex = 81;
            this.Label1_3.Text = "YEAR";
            // 
            // Label1_8
            // 
            this.Label1_8.AutoSize = true;
            this.Label1_8.Location = new System.Drawing.Point(524, 67);
            this.Label1_8.Name = "Label1_8";
            this.Label1_8.Size = new System.Drawing.Size(27, 17);
            this.Label1_8.TabIndex = 75;
            this.Label1_8.Text = "AC";
            this.Label1_8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label1_8, "Affect Cash");
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(14, 152);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(179, 17);
            this.Label8.TabIndex = 98;
            this.Label8.Text = "PENDING TRANSACTIONS";
            // 
            // Label1_12
            // 
            this.Label1_12.AutoSize = true;
            this.Label1_12.Location = new System.Drawing.Point(344, 150);
            this.Label1_12.Name = "Label1_12";
            this.Label1_12.Size = new System.Drawing.Size(77, 17);
            this.Label1_12.TabIndex = 96;
            this.Label1_12.Text = "COMMENT";
            // 
            // vsPreview
            // 
            this.vsPreview.Cols = 11;
            this.vsPreview.Location = new System.Drawing.Point(660, 10);
            this.vsPreview.Name = "vsPreview";
            this.vsPreview.Rows = 1;
            this.vsPreview.Size = new System.Drawing.Size(584, 320);
            this.vsPreview.TabIndex = 52;
            this.vsPreview.Visible = false;
            this.vsPreview.Click += new System.EventHandler(this.vsPreview_Click);
            // 
            // fraEditNote
            // 
            this.fraEditNote.Controls.Add(this.chkShowInRE);
            this.fraEditNote.Controls.Add(this.cmdNoteCancel);
            this.fraEditNote.Controls.Add(this.cmdNoteProcess);
            this.fraEditNote.Controls.Add(this.chkPriority);
            this.fraEditNote.Controls.Add(this.txtNote);
            this.fraEditNote.Controls.Add(this.lblRemainChars);
            this.fraEditNote.Controls.Add(this.Label15);
            this.fraEditNote.Controls.Add(this.lblNote);
            this.fraEditNote.Location = new System.Drawing.Point(176, 43);
            this.fraEditNote.Name = "fraEditNote";
            this.fraEditNote.Size = new System.Drawing.Size(461, 350);
            this.fraEditNote.TabIndex = 105;
            this.fraEditNote.Visible = false;
            // 
            // chkShowInRE
            // 
            this.chkShowInRE.Location = new System.Drawing.Point(67, 242);
            this.chkShowInRE.Name = "chkShowInRE";
            this.chkShowInRE.Size = new System.Drawing.Size(222, 24);
            this.chkShowInRE.TabIndex = 111;
            this.chkShowInRE.Text = "Display pop-up in Real Estate?";
            // 
            // cmdNoteCancel
            // 
            this.cmdNoteCancel.AppearanceKey = "actionButton";
            this.cmdNoteCancel.Location = new System.Drawing.Point(282, 287);
            this.cmdNoteCancel.Name = "cmdNoteCancel";
            this.cmdNoteCancel.Size = new System.Drawing.Size(95, 40);
            this.cmdNoteCancel.TabIndex = 110;
            this.cmdNoteCancel.Text = "Cancel";
            this.cmdNoteCancel.Click += new System.EventHandler(this.cmdNoteCancel_Click);
            // 
            // cmdNoteProcess
            // 
            this.cmdNoteProcess.AppearanceKey = "actionButton";
            this.cmdNoteProcess.Location = new System.Drawing.Point(103, 287);
            this.cmdNoteProcess.Name = "cmdNoteProcess";
            this.cmdNoteProcess.Size = new System.Drawing.Size(95, 40);
            this.cmdNoteProcess.TabIndex = 109;
            this.cmdNoteProcess.Text = "OK";
            this.cmdNoteProcess.Click += new System.EventHandler(this.cmdNoteProcess_Click);
            // 
            // chkPriority
            // 
            this.chkPriority.Location = new System.Drawing.Point(67, 217);
            this.chkPriority.Name = "chkPriority";
            this.chkPriority.Size = new System.Drawing.Size(192, 24);
            this.chkPriority.TabIndex = 108;
            this.chkPriority.Text = "Display pop-up reminder?";
            // 
            // txtNote
            // 
            this.txtNote.AcceptsReturn = true;
            this.txtNote.BackColor = System.Drawing.SystemColors.Window;
            this.txtNote.Location = new System.Drawing.Point(16, 83);
            this.txtNote.MaxLength = 255;
            this.txtNote.Multiline = true;
            this.txtNote.Name = "txtNote";
            this.txtNote.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.txtNote.Size = new System.Drawing.Size(428, 85);
            this.txtNote.TabIndex = 106;
            this.txtNote.TextChanged += new System.EventHandler(this.txtNote_TextChanged);
            this.txtNote.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNote_KeyPress);
            // 
            // lblRemainChars
            // 
            this.lblRemainChars.Location = new System.Drawing.Point(417, 169);
            this.lblRemainChars.Name = "lblRemainChars";
            this.lblRemainChars.Size = new System.Drawing.Size(25, 18);
            this.lblRemainChars.TabIndex = 113;
            this.lblRemainChars.Text = "255";
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(169, 169);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(155, 18);
            this.Label15.TabIndex = 112;
            this.Label15.Text = "CHARACTERS REMAINING";
            // 
            // lblNote
            // 
            this.lblNote.Location = new System.Drawing.Point(16, 41);
            this.lblNote.Name = "lblNote";
            this.lblNote.Size = new System.Drawing.Size(428, 31);
            this.lblNote.TabIndex = 107;
            this.lblNote.Text = "EDIT THE TEXT BELOW TO CHANGE IT OR CLEAR THE TEXT TO REMOVE THE NOTE FROM THE AC" +
    "COUNT";
            // 
            // GRID
            // 
            this.GRID.Cols = 1;
            this.GRID.Location = new System.Drawing.Point(4, 110);
            this.GRID.Name = "GRID";
            this.GRID.Rows = 1;
            this.GRID.Size = new System.Drawing.Size(563, 190);
            this.GRID.TabIndex = 53;
            this.GRID.Visible = false;
            this.GRID.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GRID_CellFormatting);
            this.GRID.CurrentCellChanged += new System.EventHandler(this.Grid_RowColChange);
            this.GRID.RowExpanded += new Wisej.Web.DataGridViewRowEventHandler(this.GRID_RowExpanded);
            this.GRID.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.GRID_RowCollapsed);
            this.GRID.Click += new System.EventHandler(this.Grid_Click);
            this.GRID.DoubleClick += new System.EventHandler(this.Grid_DblClick);
            this.GRID.MouseDown += new Wisej.Web.MouseEventHandler(this.GRID_MouseDown);
            // 
            // fraWriteOff
            // 
            this.fraWriteOff.Controls.Add(this.txtWOCosts);
            this.fraWriteOff.Controls.Add(this.txtWOPrincipal);
            this.fraWriteOff.Controls.Add(this.txtWOCurInterest);
            this.fraWriteOff.Controls.Add(this.txtWOPLI);
            this.fraWriteOff.Controls.Add(this.cmdWOProcess_0);
            this.fraWriteOff.Controls.Add(this.cmdWOProcess_1);
            this.fraWriteOff.Controls.Add(this.Label12);
            this.fraWriteOff.Controls.Add(this.Label18);
            this.fraWriteOff.Controls.Add(this.lblWOPLI);
            this.fraWriteOff.Controls.Add(this.Label13);
            this.fraWriteOff.Controls.Add(this.Label16);
            this.fraWriteOff.Controls.Add(this.lblWOTotal);
            this.fraWriteOff.Controls.Add(this.Label14);
            this.fraWriteOff.Controls.Add(this.Label19);
            this.fraWriteOff.Name = "fraWriteOff";
            this.fraWriteOff.Size = new System.Drawing.Size(286, 306);
            this.fraWriteOff.TabIndex = 1;
            this.fraWriteOff.Visible = false;
            // 
            // txtWOCosts
            // 
            this.txtWOCosts.BackColor = System.Drawing.SystemColors.Window;
            this.txtWOCosts.Location = new System.Drawing.Point(162, 158);
            this.txtWOCosts.MaxLength = 12;
            this.txtWOCosts.Name = "txtWOCosts";
            this.txtWOCosts.Size = new System.Drawing.Size(100, 40);
            this.txtWOCosts.TabIndex = 10;
            this.txtWOCosts.Enter += new System.EventHandler(this.txtWOCosts_Enter);
            this.txtWOCosts.Leave += new System.EventHandler(this.txtWOCosts_Leave);
            this.txtWOCosts.TextChanged += new System.EventHandler(this.txtWOCosts_TextChanged);
            // 
            // txtWOPrincipal
            // 
            this.txtWOPrincipal.BackColor = System.Drawing.SystemColors.Window;
            this.txtWOPrincipal.Location = new System.Drawing.Point(162, 33);
            this.txtWOPrincipal.MaxLength = 12;
            this.txtWOPrincipal.Name = "txtWOPrincipal";
            this.txtWOPrincipal.Size = new System.Drawing.Size(100, 40);
            this.txtWOPrincipal.TabIndex = 4;
            this.txtWOPrincipal.Enter += new System.EventHandler(this.txtWOPrincipal_Enter);
            this.txtWOPrincipal.Leave += new System.EventHandler(this.txtWOPrincipal_Leave);
            this.txtWOPrincipal.TextChanged += new System.EventHandler(this.txtWOPrincipal_TextChanged);
            // 
            // txtWOCurInterest
            // 
            this.txtWOCurInterest.BackColor = System.Drawing.SystemColors.Window;
            this.txtWOCurInterest.Location = new System.Drawing.Point(162, 74);
            this.txtWOCurInterest.MaxLength = 12;
            this.txtWOCurInterest.Name = "txtWOCurInterest";
            this.txtWOCurInterest.Size = new System.Drawing.Size(100, 40);
            this.txtWOCurInterest.TabIndex = 6;
            this.txtWOCurInterest.Enter += new System.EventHandler(this.txtWOCurInterest_Enter);
            this.txtWOCurInterest.Leave += new System.EventHandler(this.txtWOCurInterest_Leave);
            this.txtWOCurInterest.TextChanged += new System.EventHandler(this.txtWOCurInterest_TextChanged);
            // 
            // txtWOPLI
            // 
            this.txtWOPLI.BackColor = System.Drawing.SystemColors.Window;
            this.txtWOPLI.Location = new System.Drawing.Point(162, 116);
            this.txtWOPLI.MaxLength = 12;
            this.txtWOPLI.Name = "txtWOPLI";
            this.txtWOPLI.Size = new System.Drawing.Size(100, 40);
            this.txtWOPLI.TabIndex = 8;
            this.txtWOPLI.Enter += new System.EventHandler(this.txtWOPLI_Enter);
            this.txtWOPLI.Leave += new System.EventHandler(this.txtWOPLI_Leave);
            this.txtWOPLI.TextChanged += new System.EventHandler(this.txtWOPLI_TextChanged);
            // 
            // cmdWOProcess_0
            // 
            this.cmdWOProcess_0.Location = new System.Drawing.Point(70, 247);
            this.cmdWOProcess_0.Name = "cmdWOProcess_0";
            this.cmdWOProcess_0.Size = new System.Drawing.Size(62, 27);
            this.cmdWOProcess_0.TabIndex = 14;
            this.cmdWOProcess_0.Text = "Process";
            this.cmdWOProcess_0.Click += new System.EventHandler(this.cmdWOProcess_Click);
            // 
            // cmdWOProcess_1
            // 
            this.cmdWOProcess_1.Location = new System.Drawing.Point(162, 247);
            this.cmdWOProcess_1.Name = "cmdWOProcess_1";
            this.cmdWOProcess_1.Size = new System.Drawing.Size(62, 27);
            this.cmdWOProcess_1.TabIndex = 15;
            this.cmdWOProcess_1.Text = "Cancel";
            this.cmdWOProcess_1.Click += new System.EventHandler(this.cmdWOProcess_Click);
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(32, 172);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(120, 20);
            this.Label12.TabIndex = 9;
            this.Label12.Text = "COSTS";
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(32, 88);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(72, 20);
            this.Label18.TabIndex = 5;
            this.Label18.Text = "INTEREST";
            // 
            // lblWOPLI
            // 
            this.lblWOPLI.Location = new System.Drawing.Point(32, 130);
            this.lblWOPLI.Name = "lblWOPLI";
            this.lblWOPLI.Size = new System.Drawing.Size(120, 20);
            this.lblWOPLI.TabIndex = 7;
            this.lblWOPLI.Text = "PRE-LIEN INTEREST";
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(32, 51);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(76, 20);
            this.Label13.TabIndex = 3;
            this.Label13.Text = "PRINCIPAL";
            this.ToolTip1.SetToolTip(this.Label13, "Double click to discount the original tax amount.");
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(32, 211);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(72, 20);
            this.Label16.TabIndex = 11;
            this.Label16.Text = "TOTAL";
            // 
            // lblWOTotal
            // 
            this.lblWOTotal.Location = new System.Drawing.Point(162, 211);
            this.lblWOTotal.Name = "lblWOTotal";
            this.lblWOTotal.Size = new System.Drawing.Size(100, 20);
            this.lblWOTotal.TabIndex = 12;
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(14, 177);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(60, 25);
            this.Label14.TabIndex = 13;
            this.Label14.Visible = false;
            // 
            // Label19
            // 
            this.Label19.Location = new System.Drawing.Point(8, 17);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(259, 29);
            this.Label19.TabIndex = 2;
            this.Label19.Text = "ENTER THE AMOUNTS TO CORRECT";
            this.Label19.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblGrpInfo
            // 
            this.lblGrpInfo.AutoSize = true;
            this.lblGrpInfo.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblGrpInfo.ForeColor = System.Drawing.Color.Red;
            this.lblGrpInfo.Location = new System.Drawing.Point(127, 0);
            this.lblGrpInfo.Name = "lblGrpInfo";
            this.lblGrpInfo.Size = new System.Drawing.Size(27, 25);
            this.lblGrpInfo.TabIndex = 116;
            this.lblGrpInfo.Text = "G";
            this.lblGrpInfo.Click += new System.EventHandler(this.lblGrpInfo_Click);
            // 
            // lblAssocAcct
            // 
            this.lblAssocAcct.AutoSize = true;
            this.lblAssocAcct.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblAssocAcct.ForeColor = System.Drawing.Color.Red;
            this.lblAssocAcct.Location = new System.Drawing.Point(192, 1);
            this.lblAssocAcct.Name = "lblAssocAcct";
            this.lblAssocAcct.Size = new System.Drawing.Size(40, 25);
            this.lblAssocAcct.TabIndex = 115;
            this.lblAssocAcct.Text = "RE";
            this.lblAssocAcct.Visible = false;
            // 
            // lblAcctComment
            // 
            this.lblAcctComment.AutoSize = true;
            this.lblAcctComment.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblAcctComment.ForeColor = System.Drawing.Color.Red;
            this.lblAcctComment.Location = new System.Drawing.Point(74, 0);
            this.lblAcctComment.Name = "lblAcctComment";
            this.lblAcctComment.Size = new System.Drawing.Size(25, 25);
            this.lblAcctComment.TabIndex = 17;
            this.lblAcctComment.Text = "C";
            this.lblAcctComment.Visible = false;
            this.lblAcctComment.Click += new System.EventHandler(this.lblAcctComment_Click);
            this.lblAcctComment.DoubleClick += new System.EventHandler(this.lblAcctComment_DoubleClick);
            // 
            // lblTaxAcquired
            // 
            this.lblTaxAcquired.AutoSize = true;
            this.lblTaxAcquired.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblTaxAcquired.ForeColor = System.Drawing.Color.Red;
            this.lblTaxAcquired.Location = new System.Drawing.Point(109, 0);
            this.lblTaxAcquired.Name = "lblTaxAcquired";
            this.lblTaxAcquired.Size = new System.Drawing.Size(37, 25);
            this.lblTaxAcquired.TabIndex = 16;
            this.lblTaxAcquired.Text = "TA";
            this.ToolTip1.SetToolTip(this.lblTaxAcquired, "The account has been Tax Acquired.");
            this.lblTaxAcquired.Visible = false;
            // 
            // lblPaymentInfo
            // 
            this.lblPaymentInfo.Location = new System.Drawing.Point(30, 20);
            this.lblPaymentInfo.Name = "lblPaymentInfo";
            this.lblPaymentInfo.Size = new System.Drawing.Size(609, 17);
            this.lblPaymentInfo.TabIndex = 106;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessGoTo,
            this.mnuFileMortgageHolderInfo,
            this.mnuFileInterestedParty,
            this.mnuFilePrint,
            this.mnuFilePreviousAccount,
            this.mnuFileNextAccount,
            this.mnuFileAccountInfo,
            this.mnuFileTaxInfoSheet,
            this.mnuFilePrintRateInfo,
            this.mnuFileComment,
            this.mnuFileREComment,
            this.mnuFileEditNote,
            this.mnuFilePriority,
            this.mnuFileReprintReceipt,
            this.mnuPayment,
            this.mnuFileDischarge,
            this.mnuFilePrintAltLDN,
            this.mnuFilePrintBPReport,
            this.mnuFilePrintGroupInfo});
            this.MainMenu1.Name = null;
            // 
            // mnuProcessGoTo
            // 
            this.mnuProcessGoTo.Index = 0;
            this.mnuProcessGoTo.Name = "mnuProcessGoTo";
            this.mnuProcessGoTo.Shortcut = Wisej.Web.Shortcut.ShiftF3;
            this.mnuProcessGoTo.Text = "Go To Status View";
            this.mnuProcessGoTo.Click += new System.EventHandler(this.mnuProcessGoTo_Click);
            // 
            // mnuFileMortgageHolderInfo
            // 
            this.mnuFileMortgageHolderInfo.Index = 1;
            this.mnuFileMortgageHolderInfo.Name = "mnuFileMortgageHolderInfo";
            this.mnuFileMortgageHolderInfo.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuFileMortgageHolderInfo.Text = "Mortgage Holder Information";
            this.mnuFileMortgageHolderInfo.Click += new System.EventHandler(this.mnuFileMortgageHolderInfo_Click);
            // 
            // mnuFileInterestedParty
            // 
            this.mnuFileInterestedParty.Index = 2;
            this.mnuFileInterestedParty.Name = "mnuFileInterestedParty";
            this.mnuFileInterestedParty.Shortcut = Wisej.Web.Shortcut.ShiftF4;
            this.mnuFileInterestedParty.Text = "Interested Party Information";
            this.mnuFileInterestedParty.Click += new System.EventHandler(this.mnuFileInterestedParty_Click);
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 3;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuFilePrint.Text = "Print Account Detail";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuFilePreviousAccount
            // 
            this.mnuFilePreviousAccount.Index = 4;
            this.mnuFilePreviousAccount.Name = "mnuFilePreviousAccount";
            this.mnuFilePreviousAccount.Shortcut = Wisej.Web.Shortcut.F7;
            this.mnuFilePreviousAccount.Text = "Previous Account";
            this.mnuFilePreviousAccount.Visible = false;
            this.mnuFilePreviousAccount.Click += new System.EventHandler(this.mnuFilePreviousAccount_Click);
            // 
            // mnuFileNextAccount
            // 
            this.mnuFileNextAccount.Index = 5;
            this.mnuFileNextAccount.Name = "mnuFileNextAccount";
            this.mnuFileNextAccount.Shortcut = Wisej.Web.Shortcut.F8;
            this.mnuFileNextAccount.Text = "Next Account";
            this.mnuFileNextAccount.Visible = false;
            this.mnuFileNextAccount.Click += new System.EventHandler(this.mnuFileNextAccount_Click);
            // 
            // mnuFileAccountInfo
            // 
            this.mnuFileAccountInfo.Index = 6;
            this.mnuFileAccountInfo.Name = "mnuFileAccountInfo";
            this.mnuFileAccountInfo.Text = "Show Account Info";
            this.mnuFileAccountInfo.Click += new System.EventHandler(this.mnuFileAccountInfo_Click);
            // 
            // mnuFileTaxInfoSheet
            // 
            this.mnuFileTaxInfoSheet.Index = 7;
            this.mnuFileTaxInfoSheet.Name = "mnuFileTaxInfoSheet";
            this.mnuFileTaxInfoSheet.Text = "Print Tax Information Sheet";
            this.mnuFileTaxInfoSheet.Click += new System.EventHandler(this.mnuFileTaxInfoSheet_Click);
            // 
            // mnuFilePrintRateInfo
            // 
            this.mnuFilePrintRateInfo.Index = 8;
            this.mnuFilePrintRateInfo.Name = "mnuFilePrintRateInfo";
            this.mnuFilePrintRateInfo.Text = "Print Rate/Payment Info";
            this.mnuFilePrintRateInfo.Visible = false;
            this.mnuFilePrintRateInfo.Click += new System.EventHandler(this.mnuFilePrintRateInfo_Click);
            // 
            // mnuFileComment
            // 
            this.mnuFileComment.Index = 9;
            this.mnuFileComment.Name = "mnuFileComment";
            this.mnuFileComment.Text = "Account Comment";
            this.mnuFileComment.Click += new System.EventHandler(this.mnuFileComment_Click);
            // 
            // mnuFileREComment
            // 
            this.mnuFileREComment.Index = 10;
            this.mnuFileREComment.Name = "mnuFileREComment";
            this.mnuFileREComment.Text = "Add RE / CL Comment";
            this.mnuFileREComment.Click += new System.EventHandler(this.mnuFileREComment_Click);
            // 
            // mnuFileEditNote
            // 
            this.mnuFileEditNote.Index = 11;
            this.mnuFileEditNote.Name = "mnuFileEditNote";
            this.mnuFileEditNote.Text = "Add/Edit Note";
            this.mnuFileEditNote.Click += new System.EventHandler(this.mnuFileEditNote_Click);
            // 
            // mnuFilePriority
            // 
            this.mnuFilePriority.Enabled = false;
            this.mnuFilePriority.Index = 12;
            this.mnuFilePriority.Name = "mnuFilePriority";
            this.mnuFilePriority.Text = "Set Note Priority";
            this.mnuFilePriority.Visible = false;
            this.mnuFilePriority.Click += new System.EventHandler(this.mnuFilePriority_Click);
            // 
            // mnuFileReprintReceipt
            // 
            this.mnuFileReprintReceipt.Index = 13;
            this.mnuFileReprintReceipt.Name = "mnuFileReprintReceipt";
            this.mnuFileReprintReceipt.Text = "Reprint Receipt";
            this.mnuFileReprintReceipt.Visible = false;
            this.mnuFileReprintReceipt.Click += new System.EventHandler(this.mnuFileReprintReceipt_Click);
            // 
            // mnuPayment
            // 
            this.mnuPayment.Index = 14;
            this.mnuPayment.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileOptionLoadValidAccount,
            this.mnuPaymentClearPayment,
            this.mnuPaymentClearList,
            this.mnuFileOptionsBlankBill});
            this.mnuPayment.Name = "mnuPayment";
            this.mnuPayment.Text = "Options";
            this.mnuPayment.Visible = false;
            // 
            // mnuFileOptionLoadValidAccount
            // 
            this.mnuFileOptionLoadValidAccount.Index = 0;
            this.mnuFileOptionLoadValidAccount.Name = "mnuFileOptionLoadValidAccount";
            this.mnuFileOptionLoadValidAccount.Shortcut = Wisej.Web.Shortcut.ShiftF2;
            this.mnuFileOptionLoadValidAccount.Text = "Load Valid Account";
            this.mnuFileOptionLoadValidAccount.Click += new System.EventHandler(this.mnuFileOptionLoadValidAccount_Click);
            // 
            // mnuPaymentClearPayment
            // 
            this.mnuPaymentClearPayment.Index = 1;
            this.mnuPaymentClearPayment.Name = "mnuPaymentClearPayment";
            this.mnuPaymentClearPayment.Text = "Clear Payment Boxes";
            this.mnuPaymentClearPayment.Click += new System.EventHandler(this.mnuPaymentClearPayment_Click);
            // 
            // mnuPaymentClearList
            // 
            this.mnuPaymentClearList.Index = 2;
            this.mnuPaymentClearList.Name = "mnuPaymentClearList";
            this.mnuPaymentClearList.Text = "Clear Pending Transactions";
            this.mnuPaymentClearList.Click += new System.EventHandler(this.mnuPaymentClearList_Click);
            // 
            // mnuFileOptionsBlankBill
            // 
            this.mnuFileOptionsBlankBill.Enabled = false;
            this.mnuFileOptionsBlankBill.Index = 3;
            this.mnuFileOptionsBlankBill.Name = "mnuFileOptionsBlankBill";
            this.mnuFileOptionsBlankBill.Text = "Create Blank Billing Record";
            this.mnuFileOptionsBlankBill.Visible = false;
            this.mnuFileOptionsBlankBill.Click += new System.EventHandler(this.mnuFileOptionsBlankBill_Click);
            // 
            // mnuFileDischarge
            // 
            this.mnuFileDischarge.Index = 15;
            this.mnuFileDischarge.Name = "mnuFileDischarge";
            this.mnuFileDischarge.Text = "Print Lien Discharge Notice";
            this.mnuFileDischarge.Visible = false;
            this.mnuFileDischarge.Click += new System.EventHandler(this.mnuFileDischarge_Click);
            // 
            // mnuFilePrintAltLDN
            // 
            this.mnuFilePrintAltLDN.Index = 16;
            this.mnuFilePrintAltLDN.Name = "mnuFilePrintAltLDN";
            this.mnuFilePrintAltLDN.Text = "Print Lien Discharge Report";
            this.mnuFilePrintAltLDN.Visible = false;
            this.mnuFilePrintAltLDN.Click += new System.EventHandler(this.mnuFilePrintAltLDN_Click);
            // 
            // mnuFilePrintBPReport
            // 
            this.mnuFilePrintBPReport.Index = 17;
            this.mnuFilePrintBPReport.Name = "mnuFilePrintBPReport";
            this.mnuFilePrintBPReport.Text = "Print Book Page Report";
            this.mnuFilePrintBPReport.Click += new System.EventHandler(this.mnuFilePrintBPReport_Click);
            // 
            // mnuFilePrintGroupInfo
            // 
            this.mnuFilePrintGroupInfo.Index = 18;
            this.mnuFilePrintGroupInfo.Name = "mnuFilePrintGroupInfo";
            this.mnuFilePrintGroupInfo.Text = "Print Group Listing";
            this.mnuFilePrintGroupInfo.Visible = false;
            this.mnuFilePrintGroupInfo.Click += new System.EventHandler(this.mnuFilePrintGroupInfo_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Enabled = false;
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // lblAssocAcct_Pmt
            // 
            this.lblAssocAcct_Pmt.AutoSize = true;
            this.lblAssocAcct_Pmt.BackColor = System.Drawing.Color.FromName("@window");
            this.lblAssocAcct_Pmt.Font = new System.Drawing.Font("defaultBold", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblAssocAcct_Pmt.ForeColor = System.Drawing.Color.Red;
            this.lblAssocAcct_Pmt.Location = new System.Drawing.Point(192, 1);
            this.lblAssocAcct_Pmt.Name = "lblAssocAcct_Pmt";
            this.lblAssocAcct_Pmt.Size = new System.Drawing.Size(40, 25);
            this.lblAssocAcct_Pmt.TabIndex = 100;
            this.lblAssocAcct_Pmt.Text = "RE";
            this.lblAssocAcct_Pmt.Visible = false;
            // 
            // lblGrpInfo_Pmt
            // 
            this.lblGrpInfo_Pmt.BackColor = System.Drawing.Color.Transparent;
            this.lblGrpInfo_Pmt.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblGrpInfo_Pmt.ForeColor = System.Drawing.Color.Red;
            this.lblGrpInfo_Pmt.Location = new System.Drawing.Point(127, 0);
            this.lblGrpInfo_Pmt.Name = "lblGrpInfo_Pmt";
            this.lblGrpInfo_Pmt.Size = new System.Drawing.Size(27, 25);
            this.lblGrpInfo_Pmt.TabIndex = 114;
            this.lblGrpInfo_Pmt.Click += new System.EventHandler(this.lblGrpInfo_Pmt_Click);
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Enabled = false;
            this.btnProcess.Location = new System.Drawing.Point(223, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F11;
            this.btnProcess.Size = new System.Drawing.Size(211, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Save Payments";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdFileOptionsPrint
            // 
            this.cmdFileOptionsPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileOptionsPrint.Location = new System.Drawing.Point(641, 29);
            this.cmdFileOptionsPrint.Name = "cmdFileOptionsPrint";
            this.cmdFileOptionsPrint.Shortcut = Wisej.Web.Shortcut.F9;
            this.cmdFileOptionsPrint.Size = new System.Drawing.Size(154, 24);
            this.cmdFileOptionsPrint.TabIndex = 59;
            this.cmdFileOptionsPrint.Text = "Print Receipt And Save";
            this.cmdFileOptionsPrint.Click += new System.EventHandler(this.cmdFileOptionsPrint_Click);
            // 
            // cmdProcessEffective
            // 
            this.cmdProcessEffective.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdProcessEffective.Location = new System.Drawing.Point(869, 29);
            this.cmdProcessEffective.Name = "cmdProcessEffective";
            this.cmdProcessEffective.Shortcut = Wisej.Web.Shortcut.F2;
            this.cmdProcessEffective.Size = new System.Drawing.Size(97, 24);
            this.cmdProcessEffective.TabIndex = 61;
            this.cmdProcessEffective.Text = "Effective Date";
            this.cmdProcessEffective.Click += new System.EventHandler(this.cmdProcessEffective_Click);
            // 
            // cmdPaymentPreview
            // 
            this.cmdPaymentPreview.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPaymentPreview.Location = new System.Drawing.Point(799, 29);
            this.cmdPaymentPreview.Name = "cmdPaymentPreview";
            this.cmdPaymentPreview.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdPaymentPreview.Size = new System.Drawing.Size(66, 24);
            this.cmdPaymentPreview.TabIndex = 64;
            this.cmdPaymentPreview.Text = "Preview";
            this.cmdPaymentPreview.Click += new System.EventHandler(this.cmdPaymentPreview_Click);
            // 
            // cmdProcessChangeAccount
            // 
            this.cmdProcessChangeAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdProcessChangeAccount.Location = new System.Drawing.Point(969, 29);
            this.cmdProcessChangeAccount.Name = "cmdProcessChangeAccount";
            this.cmdProcessChangeAccount.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdProcessChangeAccount.Size = new System.Drawing.Size(117, 24);
            this.cmdProcessChangeAccount.TabIndex = 65;
            this.cmdProcessChangeAccount.Text = "Change Account";
            this.cmdProcessChangeAccount.Click += new System.EventHandler(this.cmdProcessChangeAccount_Click);
            // 
            // btnSaveExit
            // 
            this.btnSaveExit.AppearanceKey = "acceptButton";
            this.btnSaveExit.Enabled = false;
            this.btnSaveExit.Location = new System.Drawing.Point(445, 30);
            this.btnSaveExit.Name = "btnSaveExit";
            this.btnSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnSaveExit.Size = new System.Drawing.Size(211, 48);
            this.btnSaveExit.TabIndex = 2;
            this.btnSaveExit.Text = "Save Payments & Exit";
            this.btnSaveExit.Click += new System.EventHandler(this.btnSaveExit_Click);
            // 
            // imgNote
            // 
            this.imgNote.AllowDrop = false;
            this.imgNote.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgNote.Image = ((System.Drawing.Image)(resources.GetObject("imgNote.Image")));
            this.imgNote.Location = new System.Drawing.Point(27, 0);
            this.imgNote.Name = "imgNote";
            this.imgNote.Size = new System.Drawing.Size(26, 23);
            this.imgNote.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgNote.Visible = false;
            this.imgNote.Click += new System.EventHandler(this.imgNote_Click);
            this.imgNote.DoubleClick += new System.EventHandler(this.imgNote_DoubleClick);
            // 
            // frmRECLStatus
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmRECLStatus";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Collections Status";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmRECLStatus_Load);
            this.Activated += new System.EventHandler(this.frmRECLStatus_Activated);
            this.Enter += new System.EventHandler(this.frmRECLStatus_Enter);
            this.Resize += new System.EventHandler(this.frmRECLStatus_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRECLStatus_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).EndInit();
            this.fraRateInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdRIClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsRateInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDiscount)).EndInit();
            this.fraDiscount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdDisc_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDisc_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDisc_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDisc_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraStatusLabels)).EndInit();
            this.fraStatusLabels.ResumeLayout(false);
            this.fraStatusLabels.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPPStatusLabels)).EndInit();
            this.fraPPStatusLabels.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsPPCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPayment)).EndInit();
            this.fraPayment.ResumeLayout(false);
            this.fraPayment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcctNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraEditNote)).EndInit();
            this.fraEditNote.ResumeLayout(false);
            this.fraEditNote.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowInRE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNoteCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNoteProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPriority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GRID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWriteOff)).EndInit();
            this.fraWriteOff.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdWOProcess_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdWOProcess_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileOptionsPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessEffective)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPaymentPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessChangeAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNote)).EndInit();
            this.ResumeLayout(false);

		}

        



        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
		public FCButton cmdFileOptionsPrint;
		public FCButton cmdProcessEffective;
		public FCButton cmdPaymentPreview;
		public FCButton cmdProcessChangeAccount;
		private Label lblRateInfo;
		public FCLabel fcLabel1;
		public FCGrid vsPPCategory;
		private FCButton btnSaveExit;
		public FCLabel fcLabel2;
        public FCLabel lblLandValue2;
        public FCPictureBox imgNote;
    }
}
