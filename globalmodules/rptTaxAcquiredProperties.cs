﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptTaxAcquiredProperties : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/11/2007              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/11/2007              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		//clsDRWrapper rsLN = new clsDRWrapper();
		int lngNumOfAccts;
		double dblPDTotal;
		int lngPDNumber;
		int intArrIndex;
		int lngPONumber;

		public rptTaxAcquiredProperties()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Tax Acquired Properties";
		}

		public static rptTaxAcquiredProperties InstancePtr
		{
			get
			{
				return (rptTaxAcquiredProperties)Sys.GetInstance(typeof(rptTaxAcquiredProperties));
			}
		}

		protected rptTaxAcquiredProperties _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData?.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// Dim intCT               As Integer
			// For intCT = 1 To Detail.Controls.Count - 1
			// If UCase(Left(Detail.Controls(intCT).Name, 8)) = "FLDTAXES" Then
			// Detail.Controls(intCT).Visible = True
			// Detail.Controls(intCT).Top = fldTaxes1.Top + (fldTaxes1.Height * (intCT - 1))
			// Detail.Controls(intCT).Left = fldTaxes1.Left
			// ElseIf UCase(Left(Detail.Controls(intCT).Name, 8)) = "LBLTAXES" Then
			// Detail.Controls(intCT).Visible = True
			// Detail.Controls(intCT).Top = fldTaxes1.Top + (fldTaxes1.Height * (intCT - 1))
			// Detail.Controls(intCT).Top = lblTaxes1.Left
			// End If
			// Next
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			// catch the esc key and unload the report
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			intArrIndex = 1;
			// trocls-130 10.5.17 kjr fix name/addr error, use cparty
			//rsData.OpenRecordset("SELECT RSAccount, RSName, RSSecOwner, RSMapLot, RSAddr1, RSAddr2, RSAddr3, RSState, RSZip, RSZip4, RSLOCNUMALPH, RSLOCAPT, RSLOCSTREET, RSCard, TaxAcquired FROM Master WHERE TaxAcquired = 1 AND RSCard = 1", modExtraModules.strREDatabase);
			rsData.OpenRecordset("SELECT RSAccount, OwnerPartyID, SecOwnerPartyID,DeedName1,DeedName2, RSLOCNUMALPH, RSLOCAPT, RSLOCSTREET, RSCard, TaxAcquired FROM Master WHERE TaxAcquired = 1 AND RSCard = 1", modExtraModules.strREDatabase);
			if (rsData.EndOfFile())
			{
				MessageBox.Show("There are no accounts that have been saved as Tax Acquired.", "No Accounts Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
				Cancel();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!rsData.EndOfFile())
				{
					//Application.DoEvents();
					BindFields();
					sarDetailOB.Report = new sarTaxAcquiredDetail();
					sarDetailOB.Report.UserData = rsData.Get_Fields_Int32("RSAccount");
					lngNumOfAccts += 1;
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				rsData.MoveLast();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Detail Format Error");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			//clsDRWrapper clsTemp = new clsDRWrapper();
			cParty CParty = new cParty();
			string strOwner2;
			cPartyController gCPCtlr = new cPartyController();
			// reset variables
			ResetValues();
			if (!rsData.EndOfFile())
			{				
				strOwner2 = rsData.Get_Fields_String("DeedName2");
				CParty = gCPCtlr.GetParty(rsData.Get_Fields_Int32("OwnerPartyID"));
				lblName.Text = rsData.Get_Fields_String("DeedName1");
				if (strOwner2 != "")
				{
					lblName.Text = lblName.Text + " & " + strOwner2;
				}
				fldAcct.Text = FCConvert.ToString(rsData.Get_Fields_Int32("RSAccount"));
				fldName.Text = CParty.FullNameLastFirst;
				fldMapLot.Text = rsData.Get_Fields_String("RSMapLot");
				fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("RSLOCNUMALPH")) + FCConvert.ToString(rsData.Get_Fields_String("RSLOCAPT")) + " " + FCConvert.ToString(rsData.Get_Fields_String("RSLOCSTREET")));
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (lngNumOfAccts == 1)
			{
				fldFooter.Text = "1 account processed.";
			}
			else
			{
				fldFooter.Text = FCConvert.ToString(lngNumOfAccts) + " accounts processed.";
			}
		}
		
		private void ResetValues()
		{
			
		}


	}
}
