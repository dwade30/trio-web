﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmWait.
	/// </summary>
	public partial class frmWait : BaseForm
	{
		public frmWait()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//FC:FINAL:JEI:IIT607 TopMost of the MDI is true, to bring this form to front, we have to set TopMost to true
			this.TopMost = true;
			this.Image1.ImageSource = "ajax-loader";
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmWait InstancePtr
		{
			get
			{
				return (frmWait)Sys.GetInstance(typeof(frmWait));
			}
		}

		protected frmWait _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// Date           :               10/03/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               11/12/2002              *
		// ********************************************************
		const int SWP_NOMOVE = 2;
		const int SWP_NOSIZE = 1;
		const int flags = SWP_NOMOVE | SWP_NOSIZE;
		const int HWND_TOPMOST = -1;
		const int HWND_NOTOPMOST = -2;
		int lngSuccess;
		private int CurrIndex;
		private int intMaxIndex;

		private void frmWait_Activated(object sender, System.EventArgs e)
		{
			//FC:FINAL:RPU:#i1545 - Don't do this because the form will become too high
			//this.Height = this.Height + 15;
		}

		private void frmWait_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmWait.Icon	= "frmWait.frx":0000";
			//frmWait.ScaleWidth	= 5850;
			//frmWait.ScaleHeight	= 3255;
			//frmWait.LinkTopic	= "Form1";
			//Font.Size	= "8.25";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//Images.NumListImages	= 12;
			//Images.NumListImages	= 18;
			//End Unmaped Properties
		}

		public void Init(string strMessage, bool boolUseProgress = false, int lngProgressMax = 100, bool boolShowCounter = false, bool blnShowModal = false)
		{
			// this function will setup the frmWait screen
			// strMessage                 - This will be the message displayed on the screen to the user
			// boolUseProgress            - This will show a progress bar instead of the clock image
			// lngProgressMax             - This will set the max limit of the progress bar
			// boolShowCounter            - This will make the counter label visible so the user can see the actual numbers
			// move the window to the correct position
			this.Left = (FCGlobal.Screen.Width - this.Width) / 2;
			this.Top = ((FCGlobal.Screen.Height - this.Height) / 2) - 100;
			lblMessage.Text = strMessage;
			if (boolUseProgress)
			{
				// use the progress bar
				if (lngProgressMax > 0)
				{
					prgProgress.Maximum = lngProgressMax;
					// set the max/min/value
				}
				else
				{
					prgProgress.Maximum = 100;
				}
				prgProgress.Minimum = 0;
				prgProgress.Value = 0;
				prgProgress.Visible = true;
				// show the progress bar
				lblCounter.Text = "0 / " + FCConvert.ToString(lngProgressMax);
				// reset the counter
				lblCounter.Visible = boolShowCounter;
				// show the counter if the boolean is set to true
			}
			else
			{
				// use the clock
				prgProgress.Visible = false;
				// hide the progress bar/counter
				lblCounter.Visible = false;
				Image1.Visible = true;
				// show the clock
			}
			StartBusy();
			// show the form
			//FC:FINAL:JEI:IIT607 only call is modeless
			this.Show(FormShowEnum.Modeless);
			this.Refresh();
		}

		public void IncrementProgress(int lngAmount = 1)
		{
			// This will increment the progress bar by 1 unless otherwise specified
			prgProgress.Value = (prgProgress.Value + lngAmount) % prgProgress.Maximum;
			//FC:FINAL:DSE:#2042 Do not refresh window on the client multiple times
			//prgProgress.Refresh();
			lblCounter.Text = FCConvert.ToString(prgProgress.Value) + " / " + FCConvert.ToString(prgProgress.Maximum);
			//FC:FINAL:DSE:#2042 Do not refresh window on the client multiple times
			//lblCounter.Refresh();
			//FC:FINAL:JEI:IIT607 update the progress bar
			FCUtils.ApplicationUpdate(this);
		}

		public void StartBusy()
		{
			Image1.Visible = true;
		}

		public void StopBusy()
		{
			Image1.Visible = false;
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			StopBusy();
		}
	}
}
