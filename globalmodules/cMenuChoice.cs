﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cMenuChoice
	{
		//=========================================================
		private int lngSecurityCode;
		private string strMenuCaption = string.Empty;
		private string strToolTip = string.Empty;
		private string strDescription = string.Empty;
		private string strCode = string.Empty;
		private int lngCommandCode;
		private bool boolEnabled;

		public bool Enabled
		{
			set
			{
				boolEnabled = value;
			}
			get
			{
				bool Enabled = false;
				Enabled = boolEnabled;
				return Enabled;
			}
		}

		public int CommandCode
		{
			set
			{
				lngCommandCode = value;
			}
			get
			{
				int CommandCode = 0;
				CommandCode = lngCommandCode;
				return CommandCode;
			}
		}

		public int SecurityCode
		{
			set
			{
				lngSecurityCode = value;
			}
			get
			{
				int SecurityCode = 0;
				SecurityCode = lngSecurityCode;
				return SecurityCode;
			}
		}

		public string MenuCaption
		{
			set
			{
				strMenuCaption = value;
			}
			get
			{
				string MenuCaption = "";
				MenuCaption = strMenuCaption;
				return MenuCaption;
			}
		}

		public string ToolTip
		{
			set
			{
				strToolTip = value;
			}
			get
			{
				string ToolTip = "";
				ToolTip = strToolTip;
				return ToolTip;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string CommandName
		{
			set
			{
				strCode = value;
			}
			get
			{
				string CommandName = "";
				CommandName = strCode;
				return CommandName;
			}
		}

		public cMenuChoice() : base()
		{
			boolEnabled = true;
		}
	}
}
