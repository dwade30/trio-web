﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

#if TWCR0000
using TWCR0000;


#elif TWUT0000
using modGlobal = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for sarUTDACash.
	/// </summary>
	public partial class sarUTDACash : FCSectionReport
	{
		// nObj = 1
		//   0	sarUTDACash	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/06/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/02/2004              *
		// ********************************************************
		bool boolWide;
		float lngWidth;
		clsDRWrapper rsPayment = new clsDRWrapper();
		clsDRWrapper rsCorrection = new clsDRWrapper();
		clsDRWrapper rsYear = new clsDRWrapper();
		string strSQL = "";
		int lngYear;
		bool boolRECash;
		string strSQLCondition = "";
		int intType;

		public sarUTDACash()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarUTDACash_ReportEnd;
		}

        private void SarUTDACash_ReportEnd(object sender, EventArgs e)
        {
			rsPayment.DisposeOf();
            rsCorrection.DisposeOf();
            rsYear.DisposeOf();

		}

        public static sarUTDACash InstancePtr
		{
			get
			{
				return (sarUTDACash)Sys.GetInstance(typeof(sarUTDACash));
			}
		}

		protected sarUTDACash _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsYear.EndOfFile())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// strSQLCondition = Me.Tag
			// intType = rptUTDailyAuditMaster.intType
			intType = FCConvert.ToInt16(this.UserData);
			SetupFields();
			SetupValues();
			// lngYear = Val(Year(Now) & "1")
			if (rsPayment.EndOfFile() != true)
			{
				if (FCConvert.ToInt32(rsPayment.Get_Fields_Int32("BillNumber")) < lngYear)
				{
					lngYear = FCConvert.ToInt32(rsPayment.Get_Fields_Int32("BillNumber"));
				}
			}
			if (rsCorrection.EndOfFile() != true)
			{
				if (FCConvert.ToInt32(rsCorrection.Get_Fields_Int32("BillNumber")) < lngYear)
				{
					lngYear = FCConvert.ToInt32(rsCorrection.Get_Fields_Int32("BillNumber"));
				}
			}
			frmWait.InstancePtr.IncrementProgress();
		}

		private void SetupFields()
		{
			boolWide = rptUTDailyAuditMaster.InstancePtr.boolLandscape;
			lngWidth = rptUTDailyAuditMaster.InstancePtr.lngWidth;
			this.PrintWidth = lngWidth;
			// Header Section
			if (!boolWide)
			{
				// Narrow
				lblYear.Left = 0;
				lblPrincipal.Left = 810 / 1440F;
				lblPaymentPrin.Left = 810 / 1440F;
				lblCorrectionPrin.Left = 1890 / 1440F;
				lblTax.Left = 2970 / 1440F;
				lblPaymentTax.Left = 2970 / 1440F;
				lblCorrectionTax.Left = 4050 / 1440F;
				lblInterest.Left = 5130 / 1440F;
				lblPaymentInt.Left = 5130 / 1440F;
				lblCorrectionInt.Left = 6210 / 1440F;
				lblCosts.Left = 7290 / 1440F;
				lblPaymentCosts.Left = 7290 / 1440F;
				lblCorrectionCosts.Left = 8370 / 1440F;
				lblTotal.Left = 9450 / 1440F;
				// shrink the fields
				lblYear.Width = 900 / 1440F;
				lblPrincipal.Width = 2160 / 1440F;
				lblPaymentPrin.Width = 1080 / 1440F;
				lblCorrectionPrin.Width = (1080 / 1440F) + 0.1F;
				lblTax.Width = 2160 / 1440F;
				lblPaymentTax.Width = 1080 / 1440F;
				lblCorrectionTax.Width = (1080 / 1440F) + 0.1F;
				lblInterest.Width = 2160 / 1440F;
				lblPaymentInt.Width = 1080 / 1440F;
				lblCorrectionInt.Width = (1080 / 1440F) + 0.1F;
				lblCosts.Width = 2160 / 1440F;
				lblPaymentCosts.Width = 1080 / 1440F;
				lblCorrectionCosts.Width = (1080 / 1440F) + 0.1F;
				lblTotal.Width = 1350 / 1440F;
			}
			else
			{
				// Wide
				lblYear.Left = 0;
				lblPrincipal.Left = 1350 / 1440F;
				lblPaymentPrin.Left = 1350 / 1440F;
				lblCorrectionPrin.Left = 2610 / 1440F;
				lblTax.Left = 3960 / 1440F;
				lblPaymentTax.Left = 3960 / 1440F;
				lblCorrectionTax.Left = 5220 / 1440F;
				lblInterest.Left = 6570 / 1440F;
				lblPaymentInt.Left = 6570 / 1440F;
				lblCorrectionInt.Left = 7830 / 1440F;
				lblCosts.Left = 9180 / 1440F;
				lblPaymentCosts.Left = 9180 / 1440F;
				lblCorrectionCosts.Left = 10440 / 1440F;
				lblTotal.Left = 11970 / 1440F;
			}
			lnHeader.X2 = lngWidth;
			lblHeader.Width = lngWidth;
			// Detail Section
			fldYear.Left = lblYear.Left;
			fldPaymentPrin.Left = lblPaymentPrin.Left;
			fldCorrectionPrin.Left = lblCorrectionPrin.Left;
			fldPaymentTax.Left = lblPaymentTax.Left;
			fldCorrectionTax.Left = lblCorrectionTax.Left;
			fldPaymentInterest.Left = lblPaymentInt.Left;
			fldCorrectionInt.Left = lblCorrectionInt.Left;
			fldPaymentCosts.Left = lblPaymentCosts.Left;
			fldCorrectionCosts.Left = lblCorrectionCosts.Left;
			fldTotal.Left = lblTotal.Left;
			// Footer Section
			fldTotalPaymentPrin.Left = lblPaymentPrin.Left;
			fldTotalCorrectionPrin.Left = lblCorrectionPrin.Left;
			fldTotalPaymentTax.Left = lblPaymentTax.Left;
			fldTotalCorrectionTax.Left = lblCorrectionTax.Left;
			fldTotalPaymentInt.Left = lblPaymentInt.Left;
			fldTotalCorrectionInt.Left = lblCorrectionInt.Left;
			fldTotalPaymentCost.Left = lblPaymentCosts.Left;
			fldTotalCorrectionCosts.Left = lblCorrectionCosts.Left;
			fldTotalTotal.Left = lblTotal.Left;
			// Widths
			// Detail Section
			fldYear.Width = lblYear.Width;
			fldPaymentPrin.Width = lblPaymentPrin.Width;
			fldCorrectionPrin.Width = lblCorrectionPrin.Width;
			fldPaymentTax.Width = lblPaymentTax.Width;
			fldCorrectionTax.Width = lblCorrectionTax.Width;
			fldPaymentInterest.Width = lblPaymentInt.Width;
			fldCorrectionInt.Width = lblCorrectionInt.Width;
			fldPaymentCosts.Width = lblPaymentCosts.Width;
			fldCorrectionCosts.Width = lblCorrectionCosts.Width;
			fldTotal.Width = lblTotal.Width;
			// Footer Section
			fldTotalPaymentPrin.Width = lblPaymentPrin.Width;
			fldTotalCorrectionPrin.Width = lblCorrectionPrin.Width;
			fldTotalPaymentTax.Width = lblPaymentTax.Width;
			fldTotalCorrectionTax.Width = lblCorrectionTax.Width;
			fldTotalPaymentInt.Width = lblPaymentInt.Width;
			fldTotalCorrectionInt.Width = lblCorrectionInt.Width;
			fldTotalPaymentCost.Width = lblPaymentCosts.Width;
			fldTotalCorrectionCosts.Width = lblCorrectionCosts.Width;
			fldTotalTotal.Width = lblTotal.Width;
			lnTotals.X1 = fldTotalPaymentPrin.Left;
			lnTotals.X2 = fldTotalTotal.Left + fldTotalTotal.Width;
		}

		private void SetupValues()
		{
			// this will get all of the values ready for reporting
			clsDRWrapper rsTotals = new clsDRWrapper();
			string strSQLAddition = "";
			switch (intType)
			{
				case 0:
					{
						strSQLAddition = "AND Service <> 'S' ";
						break;
					}
				case 1:
					{
						strSQLAddition = "AND Service <> 'W' ";
						break;
					}
				case 2:
					{
						strSQLAddition = "";
						break;
					}
			}
			//end switch
			if (modGlobalConstants.Statics.gboolCR)
			{
				strSQL = "SELECT BillNumber FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND (CashDrawer = 'Y' AND GeneralLedger = 'Y') AND ReceiptNumber > 0 GROUP BY BillNumber";
			}
			else
			{
				strSQL = "SELECT BillNumber FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND (CashDrawer = 'Y' AND GeneralLedger = 'Y') GROUP BY BillNumber";
			}
			rsYear.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			if (rsYear.EndOfFile() != true && rsYear.BeginningOfFile() != true)
			{
				// set the payments recordset
				strSQL = "SELECT BillNumber, SUM(Principal) AS Prin, SUM(Tax) AS TTax, SUM(LienCost) AS Costs, SUM(PreLienInterest + CurrentInterest) AS Interest FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND (CashDrawer = 'Y' AND GeneralLedger = 'Y') AND (Principal + LienCost + PreLienInterest + CurrentInterest) > 0 GROUP BY BillNumber ORDER BY BillNumber";
				rsPayment.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				// set the correction recordset
				strSQL = "SELECT BillNumber, SUM(Principal) AS Prin, SUM(Tax) AS TTax, SUM(LienCost) AS Costs, SUM(PreLienInterest + CurrentInterest) AS Interest FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND (CashDrawer = 'Y' AND GeneralLedger = 'Y') AND (Principal + LienCost + PreLienInterest + CurrentInterest) <= 0 GROUP BY BillNumber ORDER BY BillNumber";
				rsCorrection.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				// find the totals and show them in the footer
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(Tax) AS TTax, SUM(LienCost) AS Costs, SUM(PreLienInterest + CurrentInterest) AS Interest FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND (CashDrawer = 'Y' AND GeneralLedger = 'Y') AND (Principal + LienCost + PreLienInterest + CurrentInterest) > 0";
				rsTotals.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (rsTotals.EndOfFile() != true)
				{
					fldTotalPaymentPrin.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields_Double("Prin")), "#,##0.00");
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					fldTotalPaymentInt.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("Interest")), "#,##0.00");
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					fldTotalPaymentCost.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("Costs")), "#,##0.00");
					// TODO: Field [TTax] not found!! (maybe it is an alias?)
					fldTotalPaymentTax.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("TTax")), "#,##0.00");
				}
				else
				{
					fldTotalPaymentPrin.Text = "0.00";
					fldTotalPaymentInt.Text = "0.00";
					fldTotalPaymentCost.Text = "0.00";
					fldTotalPaymentTax.Text = "0.00";
				}
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(Tax) AS TTax, SUM(LienCost) AS Costs, SUM(PreLienInterest + CurrentInterest) AS Interest FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND (CashDrawer = 'Y' AND GeneralLedger = 'Y') AND (Principal + LienCost + PreLienInterest + CurrentInterest) <= 0";
				rsTotals.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (rsTotals.EndOfFile() != true)
				{
					fldTotalCorrectionPrin.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields_Double("Prin")), "#,##0.00");
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					fldTotalCorrectionInt.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("Interest")), "#,##0.00");
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					fldTotalCorrectionCosts.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("Costs")), "#,##0.00");
					// TODO: Field [TTax] not found!! (maybe it is an alias?)
					fldTotalCorrectionTax.Text = Strings.Format(Conversion.Val(rsTotals.Get_Fields("TTax")), "#,##0.00");
				}
				else
				{
					fldTotalCorrectionPrin.Text = "0.00";
					fldTotalCorrectionInt.Text = "0.00";
					fldTotalCorrectionCosts.Text = "0.00";
					fldTotalCorrectionTax.Text = "0.00";
				}
				// this will show the end total
				fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldTotalPaymentPrin.Text) + FCConvert.ToDouble(fldTotalPaymentTax.Text) + FCConvert.ToDouble(fldTotalPaymentInt.Text) + FCConvert.ToDouble(fldTotalPaymentCost.Text) + FCConvert.ToDouble(fldTotalCorrectionPrin.Text) + FCConvert.ToDouble(fldTotalCorrectionInt.Text) + FCConvert.ToDouble(fldTotalCorrectionCosts.Text), "#,##0.00");
				rsTotals.DisposeOf();
            }
			else
			{
				Cancel();
				this.Close();
			}
		}

		private void BindFields_2(int lngYR)
		{
			BindFields(ref lngYR);
		}

		private void BindFields(ref int lngYR)
		{
			// this will put the current record year in the fields
			double dblSum;
			fldYear.Text = FCConvert.ToString(lngYR);
			frmWait.InstancePtr.IncrementProgress();
			dblSum = 0;
			if (rsPayment.EndOfFile() != true)
			{
				rsPayment.FindFirstRecord("BillNumber", lngYR);
				if (rsPayment.NoMatch)
				{
					fldPaymentPrin.Text = "0.00";
					fldPaymentInterest.Text = "0.00";
					fldPaymentCosts.Text = "0.00";
					fldPaymentTax.Text = "0.00";
				}
				else
				{
					fldPaymentPrin.Text = Strings.Format(Conversion.Val(rsPayment.Get_Fields_Double("Prin")), "#,##0.00");
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					fldPaymentInterest.Text = Strings.Format(Conversion.Val(rsPayment.Get_Fields("Interest")), "#,##0.00");
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					fldPaymentCosts.Text = Strings.Format(Conversion.Val(rsPayment.Get_Fields("Costs")), "#,##0.00");
					// TODO: Field [TTax] not found!! (maybe it is an alias?)
					fldPaymentTax.Text = Strings.Format(Conversion.Val(rsPayment.Get_Fields("TTax")), "#,##0.00");
					// TODO: Field [TTax] not found!! (maybe it is an alias?)
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					dblSum = Conversion.Val(rsPayment.Get_Fields_Double("Prin")) + Conversion.Val(rsPayment.Get_Fields("TTax")) + Conversion.Val(rsPayment.Get_Fields("Interest")) + Conversion.Val(rsPayment.Get_Fields("Costs"));
				}
			}
			else
			{
				fldPaymentPrin.Text = "0.00";
				fldPaymentInterest.Text = "0.00";
				fldPaymentCosts.Text = "0.00";
				fldPaymentTax.Text = "0.00";
			}
			if (rsCorrection.EndOfFile() != true)
			{
				rsCorrection.FindFirstRecord("BillNumber", lngYR);
				if (rsCorrection.NoMatch)
				{
					fldCorrectionPrin.Text = "0.00";
					fldCorrectionInt.Text = "0.00";
					fldCorrectionCosts.Text = "0.00";
					fldCorrectionTax.Text = "0.00";
				}
				else
				{
					fldCorrectionPrin.Text = Strings.Format(Conversion.Val(rsCorrection.Get_Fields_Double("Prin")), "#,##0.00");
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					fldCorrectionInt.Text = Strings.Format(Conversion.Val(rsCorrection.Get_Fields("Interest")), "#,##0.00");
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					fldCorrectionCosts.Text = Strings.Format(Conversion.Val(rsCorrection.Get_Fields("Costs")), "#,##0.00");
					// TODO: Field [TTax] not found!! (maybe it is an alias?)
					fldCorrectionTax.Text = Strings.Format(Conversion.Val(rsCorrection.Get_Fields("TTax")), "#,##0.00");
					// TODO: Field [TTax] not found!! (maybe it is an alias?)
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					dblSum += Conversion.Val(rsCorrection.Get_Fields_Double("Prin")) + Conversion.Val(rsCorrection.Get_Fields("TTax")) + Conversion.Val(rsCorrection.Get_Fields("Interest")) + Conversion.Val(rsCorrection.Get_Fields("Costs"));
				}
			}
			else
			{
				fldCorrectionPrin.Text = "0.00";
				fldCorrectionInt.Text = "0.00";
				fldCorrectionCosts.Text = "0.00";
				fldCorrectionTax.Text = "0.00";
			}
			fldTotal.Text = Strings.Format(dblSum, "#,##0.00");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (rsYear.EndOfFile() != true)
			{
				BindFields_2(FCConvert.ToInt32(rsYear.Get_Fields_Int32("BillNumber")));
				rsYear.MoveNext();
				// move to the next year and check to see if there are any years that need to be shown
			}
		}

		private void sarUTDACash_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarUTDACash.Caption	= "Daily Audit Cash Summary";
			//sarUTDACash.Icon	= "sarUTDACash.dsx":0000";
			//sarUTDACash.Left	= 0;
			//sarUTDACash.Top	= 0;
			//sarUTDACash.Width	= 11880;
			//sarUTDACash.Height	= 8415;
			//sarUTDACash.StartUpPosition	= 3;
			//sarUTDACash.SectionData	= "sarUTDACash.dsx":058A;
			//End Unmaped Properties
		}
	}
}
