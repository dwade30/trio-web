﻿//Fecher vbPorter - Version 1.0.0.40
using Wisej.Web;
using fecherFoundation;
using System.Xml;
using System.IO;

namespace Global
{
	public class cSubscriptionFileController
	{
		//=========================================================
		// <summary>
		//
		// </summary>
		// <param name="strConnectionInfo">The path and file name of the xml file to load</param>
		// <param name="strConnName">The muni name </param>
		// <returns></returns>
		// <remarks></remarks>
		public cSubscriptionInfo LoadSubscriptions(string strConnectionInfo, string strConnName)
		{
			cSubscriptionInfo LoadSubscriptions = null;
			cSubscriptionInfo tReturn = new cSubscriptionInfo();
			if (!(strConnName == ""))
			{
				cSubscriptionInfo tSub = new cSubscriptionInfo();
				string strEncrypted = "";
				string strDecrypted = "";
				string strSalt = "";
				// Dim tCrypt As New ChilkatCrypt2
				cCryptoUtility tCryptUtil = new cCryptoUtility();
				// tCrypt.UnlockComponent ("HARRISCrypt_vEoVPOQtXRFY")
				// tCrypt.HashAlgorithm = "sha512"
				// tCrypt.CryptAlgorithm = "aes"
				strSalt = tCryptUtil.HashString(strConnName);
				// Dim strSalt As String = Cryptography.CryptoUtility.ComputeSHA512Hash(strConnName)
				if (File.Exists(strConnectionInfo))
				{
					strEncrypted = File.ReadAllText(strConnectionInfo);
					if (strEncrypted != "")
					{
						strDecrypted = tCryptUtil.DecryptByPassword(strEncrypted, strConnName, strSalt);
						XmlDocument tDoc = new XmlDocument();
						// tDoc = XDocument.Parse(strDecrypted)
						tDoc.LoadXml(strDecrypted);
						{
							tSub = LoadSubscriptionFromXML(ref tDoc, strConnName);
							if (!(tSub == null))
							{
								tReturn = tSub;
							}
						}
					}
				}
			}
			LoadSubscriptions = tReturn;
			return LoadSubscriptions;
		}

		public cSubscriptionInfo LoadSubscriptionFromXML(ref XmlDocument tDoc, string strConnName)
		{
			cSubscriptionInfo LoadSubscriptionFromXML = null;
			cSubscriptionInfo tSub = new cSubscriptionInfo();
			FillSubscriptonFromXML(ref tDoc, ref tSub, strConnName);
			LoadSubscriptionFromXML = tSub;
			return LoadSubscriptionFromXML;
		}
		//
		public void FillSubscriptonFromXML(ref XmlDocument tDoc, ref cSubscriptionInfo tSub, string strConnName)
		{
			try
			{
				// On Error GoTo ErrorHandler
				bool boolProcess = false;
				if (!(tDoc == null) && !(tSub == null))
				{
					XmlElement tmpElement;
					XmlNode curNode;
					XmlNode tNode;
					XmlNode subItem;
					XmlNodeList subItems;
					XmlNode subGroup;
					XmlNodeList subGroups;
					XmlNodeList nodesList;
					nodesList = tDoc.SelectNodes("//Subscriptions");
					foreach (XmlNode SubscriptionNode in nodesList)
					{
						//Application.DoEvents();
						boolProcess = false;
						tNode = SubscriptionNode.Attributes.GetNamedItem("name");
						if (!(tNode == null))
						{
							if (Strings.LCase(tNode.Value) == Strings.LCase(strConnName))
							{
								tSub.Name = tNode.Value;
								boolProcess = true;
							}
						}
						if (boolProcess)
						{
							subItems = SubscriptionNode.SelectNodes("//SubscriptionItem");
							foreach (XmlNode item in subItems)
							{
								tNode = item.Attributes.GetNamedItem("name");
								if (!(tNode == null))
								{
									tSub.AddItem(tNode.Value, item.InnerText, "", "");
								}
							}
							subGroups = SubscriptionNode.SelectNodes("//SubscriptionGroup");
							if (!(subGroups == null))
							{
								foreach (XmlNode item in subGroups)
								{
									//Application.DoEvents();
									ParseGroup(item, "", ref tSub);
								}
							}
						}
					}
				}
				return;
			}
			catch
			{
				// ErrorHandler:
			}
		}

		private void ParseGroup(XmlNode tElement, string strParentGroup, ref cSubscriptionInfo tSub)
		{
			string strGroup = "";
			if (!(tElement == null))
			{
				XmlNode tAtt;
				tAtt = tElement.Attributes.GetNamedItem("name");
				if (!(tAtt == null))
				{
					strGroup = tAtt.Value;
				}
				XmlNodeList tItems;
				tItems = tElement.SelectNodes("//SubscriptionItem");
				XmlNode tGroup = null;
				XmlNodeList tGroups = null;
				if (!(tItems == null))
				{
					foreach (XmlNode tItem in tItems)
					{
						//Application.DoEvents();
						tAtt = tItem.Attributes.GetNamedItem("name");
						if (!(tAtt == null))
						{
							tSub.AddItem(tAtt.Value, tItem.Value, strGroup, strParentGroup);
						}
					}
				}
				tGroups = tElement.SelectNodes("//SubscriptionGroup");
				if (!(tGroups == null))
				{
					ParseGroup(tGroup, strGroup, ref tSub);
				}
			}
		}
		//
		// <summary>
		// The path and file name of the xml file to save
		// </summary>
		// <param name="strConnectionInfo"></param>
		// <param name="strConnName">The group name used in db connections.  I.E. Trioville from TRIO_Trioville_Live_SystemSettings</param>
		// <returns></returns>
		// <remarks></remarks>
		// Public Function SaveSubscriptions(ByRef SubscriptionInformation As SubscriptionInfo, ByVal strConnectionInfo As String, ByVal strConnName As String) As Boolean Implements ISubscriptionController.SaveSubscriptions
		// Try
		// Dim tDoc As XDocument = Nothing
		// tDoc = XMLFromSubscription(SubscriptionInformation, strConnName)
		// If Not tDoc Is Nothing Then
		// Dim strSalt As String = Cryptography.CryptoUtility.ComputeSHA512Hash(strConnName)
		// Dim strEncrypted As String = Cryptography.CryptoUtility.EncryptByPassword(tDoc.ToString, strConnName, strSalt)
		// System.IO.File.WriteAllText(strConnectionInfo, strEncrypted)
		// Return True
		// Else
		// Return False
		// End If
		// Catch ex As Exception
		// Return False
		// End Try
		// End Function
		//
		// Private Function XMLFromSubscription(ByRef tSub As SubscriptionInfo, ByVal strConnName As String) As XDocument
		// Try
		// Dim tDoc As New XDocument
		// Dim nElem As XElement = Nothing
		// Dim tGroup As XElement = Nothing
		// Dim tPGroup As XElement = Nothing
		// Dim SubscriptionsNode As XElement = New XElement("Subscriptions")
		// Dim tAtt As XAttribute = Nothing
		// Dim strTemp As String = ""
		// If strConnName <> "" Then
		// SubscriptionsNode.SetAttributeValue("name", strConnName)
		// Else
		// SubscriptionsNode.SetAttributeValue("name", tSub.Name)
		// End If
		//
		//
		// For Each tItem As SubscriptionItem In tSub.Items
		// nElem = New XElement("SubscriptionItem")
		// nElem.SetValue (tItem.ItemValue)
		// nElem.SetAttributeValue("name", tItem.Name)
		// If tItem.ParentGroup <> "" Then
		// strTemp = tItem.ParentGroup
		// tPGroup = SubscriptionsNode.Elements("SubscriptionGroup").Where(Function(o) o.Attribute("name") = strTemp).SingleOrDefault
		// If tPGroup Is Nothing Then
		// tPGroup = New XElement("SubscriptionGroup")
		// tPGroup.SetAttributeValue("name", strTemp)
		// SubscriptionsNode.Add (tPGroup)
		// End If
		// strTemp = tItem.Group
		// tGroup = tPGroup.Elements("SubscriptionGroup").Where(Function(o) o.Attribute("name") = strTemp).SingleOrDefault
		// If tGroup Is Nothing Then
		// tGroup = New XElement("SubscriptionGroup")
		// tGroup.SetAttributeValue("name", strTemp)
		// tPGroup.Add (tGroup)
		// End If
		// tGroup.Add (nElem)
		// ElseIf tItem.Group <> "" Then
		// strTemp = tItem.Group
		// tGroup = SubscriptionsNode.Elements("SubscriptionGroup").Where(Function(o) o.Attribute("name") = strTemp).SingleOrDefault
		// If tGroup Is Nothing Then
		// tGroup = New XElement("SubscriptionGroup")
		// tGroup.SetAttributeValue("name", strTemp)
		// SubscriptionsNode.Add (tGroup)
		// End If
		// tGroup.Add (nElem)
		// Else
		// SubscriptionsNode.Add (nElem)
		// End If
		// Next
		// tDoc.Add (SubscriptionsNode)
		// Return (tDoc)
		// Catch ex As Exception
		// Return Nothing
		// End Try
		// End Function
		//
	}
}
