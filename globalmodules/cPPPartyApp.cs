﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using System.Collections.Generic;

namespace Global
{
	public class cPPPartyApp : SharedApplication.ICentralPartyApp
	{
		//=========================================================
		const string strModuleCode = "PP";

		public bool ReferencesParty(int lngID)
		{
			bool ICentralPartyApp_ReferencesParty = false;
			ICentralPartyApp_ReferencesParty = false;
			bool retAnswer;
			retAnswer = false;
			if (lngID > 0)
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select count(id)as theCount from ppmaster where partyid = " + FCConvert.ToString(lngID), "PersonalProperty");
				// TODO: Field [theCount] not found!! (maybe it is an alias?)
				if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
				{
					retAnswer = true;
				}
				ICentralPartyApp_ReferencesParty = retAnswer;
			}
			return ICentralPartyApp_ReferencesParty;
		}

		public List<object> AccountsReferencingParty(int lngID)
		{
			List<object> CentralPartyApp_AccountsReferencingParty = null;
			var theCollection = new List<object>();
			clsDRWrapper rsLoad = new clsDRWrapper();
            rsLoad.OpenRecordset("select  id, account from ppmaster where partyid = " + FCConvert.ToString(lngID) + " order by account", "PersonalProperty");
			while (!rsLoad.EndOfFile())
			{
				var pRef = new cCentralPartyReference();
				pRef.ModuleCode = strModuleCode;
				pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
				pRef.Description = "Personal Property Account";
				pRef.Identifier = FCConvert.ToString(rsLoad.Get_Fields("account"));
				pRef.IdentifierForSort = Strings.Right(Strings.StrDup(12, "0") + pRef.Identifier, 12);
				theCollection.Add(pRef);
				rsLoad.MoveNext();
			}
			CentralPartyApp_AccountsReferencingParty = theCollection;
			return CentralPartyApp_AccountsReferencingParty;
		}

		public string ModuleCode()
		{
			string ICentralPartyApp_ModuleCode = "";
			ICentralPartyApp_ModuleCode = strModuleCode;
			return ICentralPartyApp_ModuleCode;
		}

		public bool ChangePartyID(int lngOriginalPartyID, int lngNewPartyID)
		{
			bool ICentralPartyApp_ChangePartyID = false;
			clsDRWrapper rsSave = new clsDRWrapper();
			string strSQL;
			try
			{
				// On Error GoTo ErrorHandler
				strSQL = "Update ppmaster set partyid = " + FCConvert.ToString(lngNewPartyID) + " where partyid = " + FCConvert.ToString(lngOriginalPartyID);
				rsSave.Execute(strSQL, "PersonalProperty");
				ICentralPartyApp_ChangePartyID = true;
				return ICentralPartyApp_ChangePartyID;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return ICentralPartyApp_ChangePartyID;
		}
	}
}
