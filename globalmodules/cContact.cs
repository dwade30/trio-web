﻿using fecherFoundation;

namespace Global
{
	public class cContact
	{
		//=========================================================
		private int lngID;
		private string strDescription = string.Empty;
		private string strName = string.Empty;
		private string strEmail = string.Empty;
		private string strComment = string.Empty;
		private int lngPartyID;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private FCCollection PhoneColl = new FCCollection();
		private FCCollection PhoneColl_AutoInitialized;

		private FCCollection PhoneColl
		{
			get
			{
				if (PhoneColl_AutoInitialized == null)
				{
					PhoneColl_AutoInitialized = new FCCollection();
				}
				return PhoneColl_AutoInitialized;
			}
			set
			{
				PhoneColl_AutoInitialized = value;
			}
		}

		public void Clear()
		{
			PhoneColl = new FCCollection();
			lngID = 0;
			strDescription = "";
			strName = "";
			strEmail = "";
			strComment = "";
			lngPartyID = 0;
		}

		public FCCollection PhoneNumbers
		{
			get
			{
				FCCollection PhoneNumbers = null;
				PhoneNumbers = PhoneColl;
				return PhoneNumbers;
			}
		}

		public int ID
		{
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
			set
			{
				lngID = value;
			}
		}

		public string Description
		{
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
			set
			{
				strDescription = value;
			}
		}

		public string Name
		{
			get
			{
				string Name = "";
				Name = strName;
				return Name;
			}
			set
			{
				strName = value;
			}
		}

		public string Email
		{
			get
			{
				string Email = "";
				Email = strEmail;
				return Email;
			}
			set
			{
				strEmail = value;
			}
		}

		public string Comment
		{
			get
			{
				string Comment = "";
				Comment = strComment;
				return Comment;
			}
			set
			{
				strComment = value;
			}
		}

		public int PartyID
		{
			get
			{
				int PartyID = 0;
				PartyID = lngPartyID;
				return PartyID;
			}
			set
			{
				lngPartyID = value;
			}
		}
	}
}
