﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cCLSecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cCLSecuritySetup() : base()
		{
			strThisModule = "CL";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem(" Enter Collections", "", 1, false, ""));
			AddItem_2(CreateItem("Daily Audit Report", "", 4, true, ""));
			AddItem_2(CreateItem("Daily Audit Report", "Audit Preview", 20, false, ""));
			AddItem_2(CreateItem("Daily Audit Report", "Print Audit", 23, false, ""));
			AddItem_2(CreateItem("Daily Audit Report", "Teller Audit", 22, false, ""));
			AddItem_2(CreateItem("File Maintenance", "", 7, true, ""));
			AddItem_2(CreateItem("File Maintenance", "Customize", 43, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Edit Bill Information", 46, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Rate Record Update", 45, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Remove From Tax Acquired", 47, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Tax Service Extract", 44, false, ""));
			AddItem_2(CreateItem("Foreclosure Report", "", 26, false, ""));
			AddItem_2(CreateItem("Interested Parties", "", 34, false, ""));
			AddItem_2(CreateItem("Lien Process", "", 12, true, ""));
			AddItem_2(CreateItem("Lien Process", "30 Day Notice", 14, false, ""));
			AddItem_2(CreateItem("Lien Process", "Lien Edit Report", 13, false, ""));
			AddItem_2(CreateItem("Lien Process", "Lien Maturity", 16, false, ""));
			AddItem_2(CreateItem("Lien Process", "Remove Lien", 17, false, ""));
			AddItem_2(CreateItem("Lien Process", "Transfer Tax To Lien", 15, false, ""));
			AddItem_2(CreateItem("Lien Process", "Edit LDN Book/Page", 38, false, ""));
			AddItem_2(CreateItem("Lien Process", "Edit Lien Book/Page", 37, false, ""));
			AddItem_2(CreateItem("Lien Process", "Lien Date Chart", 39, false, ""));
			AddItem_2(CreateItem("Lien Process", "Reverse Demand Fees", 36, false, ""));
			AddItem_2(CreateItem("Lien Process", "Create Opening Adjustments", 41, false, ""));
			AddItem_2(CreateItem("Lien Process", "Update Bill Addresses", 42, false, ""));
			AddItem_2(CreateItem("Lien Process", "Updated Book Page Report", 40, false, ""));
			AddItem_2(CreateItem("Load Back", "", 6, false, ""));
			AddItem_2(CreateItem("Mortgage Holder", "", 8, false, ""));
			AddItem_2(CreateItem("Payments", "", 2, false, ""));
			AddItem_2(CreateItem("Payment Activity Report","",49,false,""));
			AddItem_2(CreateItem("Print Book Page Report", "", 33, false, ""));
			AddItem_2(CreateItem("Print Certificate Of Recommitment", "", 29, false, ""));
			AddItem_2(CreateItem("Print Certificate of Settlement", "", 28, false, ""));
			AddItem_2(CreateItem("Print Lien Discharge", "", 27, false, ""));
			AddItem_2(CreateItem("Print Loadback Report", "", 30, false, ""));
			AddItem_2(CreateItem("Purge Records", "", 24, false, ""));
			AddItem_2(CreateItem("Reminder Notices", "", 10, false, ""));
			AddItem_2(CreateItem("Reprint Daily Audit", "", 5, false, ""));
			AddItem_2(CreateItem("Reprint Last CMF Report", "", 32, false, ""));
			AddItem_2(CreateItem("Reprint Last Purge Report", "", 31, false, ""));
			AddItem_2(CreateItem("Reprint Last Receipt", "", 9, false, ""));
			AddItem_2(CreateItem("Status Lists", "", 3, false, ""));
			AddItem_2(CreateItem("Tax Club", "", 11, false, ""));
			AddItem_2(CreateItem("Tax Club Reports", "", 25, false, ""));
			AddItem_2(CreateItem("Tax Rate Report", "", 35, false, ""));
			AddItem_2(CreateItem("View Account Status", "", 18, false, ""));
		}
	}
}
