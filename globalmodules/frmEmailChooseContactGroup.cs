﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmEmailChooseContactGroup.
	/// </summary>
	public partial class frmEmailChooseContactGroup : BaseForm
	{
		public frmEmailChooseContactGroup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:TEMP:AM:#i124 - use empty images
			this.ImageList1.Images.AddRange(new ImageListEntry[2] {
				new ImageListEntry(),
				new ImageListEntry()
			});
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEmailChooseContactGroup InstancePtr
		{
			get
			{
				return (frmEmailChooseContactGroup)Sys.GetInstance(typeof(frmEmailChooseContactGroup));
			}
		}

		protected frmEmailChooseContactGroup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private struct EMailSelectType
		{
			public bool boolContact;
			public string strQuickName;
			public int lngID;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public EMailSelectType(int unusedParam)
			{
				this.boolContact = false;
				this.strQuickName = String.Empty;
				this.lngID = 0;
			}
		};

		private EMailSelectType ReturnInfo = new EMailSelectType(0);
		private EMailSelectType[] MultiReturnInfo = null;
		private bool boolUseContacts;
		private bool boolUseGroups;
		private bool boolGroupDetail;
		const int CNSTGRIDCOLid = 1;
		const int CNSTGRIDCOLTREE = 0;
		const int CNSTGRIDCOLTYPE = 3;
		const int CNSTGRIDCOLUSE = 2;
		const int CNSTGRIDCOLQUICKNAME = 4;
		const int CNSTGRIDCOLDESC = 5;
		const int CNSTGRIDCOLEMAIL = 6;
		private string strCap = "";
		private bool boolPicError;
		private bool boolMultipleChoices;
		private bool boolAllowNew;

		public bool Init(bool boolShowContacts, bool boolShowGroups, ref int lngChosenID, ref string strReturnQuickName, bool boolShowGroupDetail)
		{
			string strReturnInfo = "";
			return Init(boolShowContacts, boolShowGroups, ref lngChosenID, ref strReturnQuickName, boolShowGroupDetail, false, ref strReturnInfo);
		}

		public bool Init(bool boolShowContacts, bool boolShowGroups, ref int lngChosenID, ref string strReturnQuickName, bool boolShowGroupDetail, bool boolAllowMultiple, ref string strReturnInfo/* = "" */, bool boolAddNew = false)
		{
			bool Init = false;
			// returns true if a contact,false if a group
			try
			{
				// On Error GoTo ErrorHandler
				boolMultipleChoices = boolAllowMultiple;
				boolAllowNew = boolAddNew;
				boolPicError = false;
				MultiReturnInfo = new EMailSelectType[1 + 1];
				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
				MultiReturnInfo[0] = new EMailSelectType(0);
				MultiReturnInfo[1] = new EMailSelectType(0);
				MultiReturnInfo[0].lngID = 0;
				MultiReturnInfo[1].lngID = 0;
				iLabel1:
				Init = true;
				iLabel2:
				lngChosenID = 0;
				lngChosenID = -1;
				iLabel3:
				ReturnInfo.boolContact = true;
				iLabel4:
				ReturnInfo.lngID = 0;
				ReturnInfo.lngID = -1;
				iLabel5:
				ReturnInfo.strQuickName = "";
				iLabel6:
				boolUseContacts = boolShowContacts;
				iLabel7:
				boolUseGroups = boolShowGroups;
				iLabel8:
				boolGroupDetail = boolShowGroupDetail;
				iLabel9:
				if (boolUseContacts && !boolUseGroups)
				{
					iLabel10:
					strCap = "Select Contact";
					iLabel11:
					;
				}
				else if (boolUseGroups && !boolUseContacts)
				{
					iLabel12:
					strCap = "Select Group";
				}
				else
				{
					iLabel13:
					strCap = "Select Contact / Group";
				}
				iLabel14:
				this.Show(FormShowEnum.Modal);
				if (boolMultipleChoices)
				{
					int x;
					string strReturn = "";
					strReturn = "";
					for (x = 0; x <= Information.UBound(MultiReturnInfo, 1); x++)
					{
						// comma and semicolon delimited
						if (MultiReturnInfo[x].lngID > 0)
						{
							if (MultiReturnInfo[x].boolContact)
							{
								strReturn += "true,";
							}
							else
							{
								strReturn += "false,";
							}
							strReturn += FCConvert.ToString(MultiReturnInfo[x].lngID) + ",";
							strReturn += MultiReturnInfo[x].strQuickName + ";";
						}
					}
					// x
					if (Strings.Trim(strReturn) != string.Empty)
					{
						// take off last ;
						strReturn = Strings.Mid(strReturn, 1, strReturn.Length - 1);
					}
					strReturnInfo = strReturn;
					ReturnInfo.lngID = MultiReturnInfo[0].lngID;
					ReturnInfo.strQuickName = MultiReturnInfo[0].strQuickName;
					ReturnInfo.boolContact = MultiReturnInfo[0].boolContact;
				}
				iLabel15:
				Init = ReturnInfo.boolContact;
				iLabel16:
				lngChosenID = ReturnInfo.lngID;
				iLabel17:
				strReturnQuickName = ReturnInfo.strQuickName;
				return Init;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In Init line " + Information.Erl(), MsgBoxStyle.Critical, "Error");
			}
			return Init;
		}

		private void frmEmailChooseContactGroup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmEmailChooseContactGroup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEmailChooseContactGroup.Icon	= "frmEmailChooseContactGroup.frx":0000";
			//frmEmailChooseContactGroup.FillStyle	= 0;
			//frmEmailChooseContactGroup.ScaleWidth	= 5880;
			//frmEmailChooseContactGroup.ScaleHeight	= 4170;
			//frmEmailChooseContactGroup.LinkTopic	= "Form2";
			//frmEmailChooseContactGroup.LockControls	= true;
			//frmEmailChooseContactGroup.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Courier New";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//Grid.BackColor	= "-2147483643";
			//			//Grid.ForeColor	= "-2147483640";
			//Grid.BorderStyle	= 1;
			//Grid.FillStyle	= 0;
			//Grid.Appearance	= 1;
			//Grid.GridLines	= 1;
			//Grid.WordWrap	= 0;
			//Grid.ScrollBars	= 2;
			//Grid.RightToLeft	= 0;
			//Grid._cx	= 9975;
			//Grid._cy	= 5583;
			//Grid._ConvInfo	= 1;
			//Grid.MousePointer	= 0;
			//Grid.BackColorFixed	= -2147483633;
			//			//Grid.ForeColorFixed	= -2147483630;
			//Grid.BackColorSel	= -2147483635;
			//			//Grid.ForeColorSel	= -2147483634;
			//Grid.BackColorBkg	= -2147483636;
			//Grid.BackColorAlternate	= -2147483643;
			//Grid.GridColor	= -2147483633;
			//Grid.GridColorFixed	= -2147483632;
			//Grid.TreeColor	= -2147483632;
			//Grid.FloodColor	= 192;
			//Grid.SheetBorder	= -2147483642;
			//Grid.FocusRect	= 1;
			//Grid.HighLight	= 1;
			//Grid.AllowSelection	= false;
			//Grid.AllowBigSelection	= false;
			//Grid.AllowUserResizing	= 0;
			//Grid.SelectionMode	= 1;
			//Grid.GridLinesFixed	= 2;
			//Grid.GridLineWidth	= 1;
			//Grid.RowHeightMin	= 0;
			//Grid.RowHeightMax	= 0;
			//Grid.ColWidthMin	= 0;
			//Grid.ColWidthMax	= 0;
			//Grid.ExtendLastCol	= true;
			//Grid.FormatString	= "";
			//Grid.ScrollTrack	= true;
			//Grid.ScrollTips	= false;
			//Grid.MergeCells	= 0;
			//Grid.MergeCompare	= 0;
			//Grid.AutoResize	= true;
			//Grid.AutoSizeMode	= 0;
			//Grid.AutoSearch	= 0;
			//Grid.AutoSearchDelay	= 2;
			//Grid.MultiTotals	= true;
			//Grid.SubtotalPosition	= 1;
			//Grid.OutlineBar	= 5;
			//Grid.OutlineCol	= 0;
			//Grid.Ellipsis	= 0;
			//Grid.ExplorerBar	= 1;
			//Grid.PicturesOver	= false;
			//Grid.PictureType	= 0;
			//Grid.TabBehavior	= 0;
			//Grid.OwnerDraw	= 0;
			//Grid.ShowComboButton	= true;
			//Grid.TextStyle	= 0;
			//Grid.TextStyleFixed	= 0;
			//Grid.OleDragMode	= 0;
			//Grid.OleDropMode	= 0;
			//Grid.ComboSearch	= 3;
			//Grid.AutoSizeMouse	= true;
			//Grid.AllowUserFreezing	= 0;
			//Grid.BackColorFrozen	= 0;
			//			//Grid.ForeColorFrozen	= 0;
			//Grid.WallPaperAlignment	= 9;
			//Images.NumListImages	= 2;
			//vsElasticLight1.OleObjectBlob	= "frmEmailChooseContactGroup.frx":137E";
			//End Unmaped Properties
			try
			{
				// On Error GoTo ErrorHandler
				iLabel1:
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
				iLabel2:
				modGlobalFunctions.SetTRIOColors(this);
				this.Text = strCap;
				this.HeaderText.Text = strCap;
				App.DoEvents();
				iLabel3:
				SetupGrid();
				iLabel4:
				LoadGrid();
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In Form_Load in line " + Information.Erl(), MsgBoxStyle.Critical, "Error");
			}
		}

		private void SetupGrid()
		{
			try
			{
				if (boolMultipleChoices)
					Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				iLabel1:
				Grid.Rows = 1;
				iLabel2:
				Grid.TextMatrix(0, CNSTGRIDCOLQUICKNAME, "Quickname");
				iLabel3:
				Grid.TextMatrix(0, CNSTGRIDCOLDESC, "Name/Description");
				iLabel4:
				Grid.TextMatrix(0, CNSTGRIDCOLEMAIL, "E-mail");
				iLabel5:
				Grid.TextMatrix(0, CNSTGRIDCOLTYPE, "Type");
				iLabel6:
				Grid.ColHidden(CNSTGRIDCOLid, true);
				iLabel7:
				if (boolUseGroups && !boolUseContacts && !boolGroupDetail)
				{
					iLabel8:
					Grid.ColHidden(CNSTGRIDCOLEMAIL, true);
				}
				Grid.ColDataType(CNSTGRIDCOLUSE, FCGrid.DataTypeSettings.flexDTBoolean);
				iLabel9:
				if (boolUseGroups && boolGroupDetail)
				{
					iLabel10:
					Grid.ColHidden(CNSTGRIDCOLTREE, false);
				}
				else
				{
					iLabel11:
					Grid.ColHidden(CNSTGRIDCOLTREE, true);
				}
				if (boolMultipleChoices)
				{
					Grid.ColHidden(CNSTGRIDCOLUSE, false);
				}
				else
				{
					Grid.ColHidden(CNSTGRIDCOLUSE, true);
				}
				/*? On Error Resume Next  */
				iLabel12:
				Grid.ColImageList(CNSTGRIDCOLTYPE, ImageList1);
				if (Information.Err().Number > 0)
				{
					boolPicError = true;
				}
				iLabel13:
				//Grid.ColAlignment(CNSTGRIDCOLTYPE, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "in SetupGrid in line " + Information.Erl(), MsgBoxStyle.Critical, "Error");
			}
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			if (!Grid.ColHidden(CNSTGRIDCOLUSE))
			{
				Grid.ColWidth(CNSTGRIDCOLUSE, FCConvert.ToInt32(0.06 * GridWidth));
			}
			Grid.ColWidth(CNSTGRIDCOLTYPE, FCConvert.ToInt32(0.09 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLQUICKNAME, FCConvert.ToInt32(0.2 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLDESC, FCConvert.ToInt32(0.3 * GridWidth));
			if (boolGroupDetail && boolUseGroups)
			{
				Grid.ColWidth(CNSTGRIDCOLTREE, FCConvert.ToInt32(0.04 * GridWidth));
			}
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsGroup = new clsDRWrapper();
				int lngRow = 0;
				int lngGroupRow;
				Grid.Rows = 1;
				if (boolUseContacts)
				{
					if (boolAllowNew)
					{
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(0));
						if (boolPicError)
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, CNSTGRIDCOLTYPE, ImageList1.Images[0]);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, "");
							Grid.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, lngRow, CNSTGRIDCOLTYPE, FCGrid.PictureAlignmentSettings.flexPicAlignCenterCenter);
							Grid.RowData(lngRow, 0);
						}
						Grid.TextMatrix(lngRow, CNSTGRIDCOLUSE, FCConvert.ToString(false));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(0));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLEMAIL, "");
						Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, "New Contact");
						Grid.TextMatrix(lngRow, CNSTGRIDCOLQUICKNAME, "");
						if (boolUseGroups && boolGroupDetail)
						{
							Grid.RowOutlineLevel(lngRow, 1);
							Grid.IsSubtotal(lngRow, true);
						}
					}
					rsLoad.OpenRecordset("select * from emailcontacts order by nickname,email", "SystemSettings");
					while (!rsLoad.EndOfFile())
					{
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(0));
						if (boolPicError)
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, CNSTGRIDCOLTYPE, ImageList1.Images[0]);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, "");
							Grid.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, lngRow, CNSTGRIDCOLTYPE, FCGrid.PictureAlignmentSettings.flexPicAlignCenterCenter);
							Grid.RowData(lngRow, 0);
						}
						Grid.TextMatrix(lngRow, CNSTGRIDCOLUSE, FCConvert.ToString(false));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(rsLoad.Get_Fields_Int32("contactid")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLEMAIL, FCConvert.ToString(rsLoad.Get_Fields_String("email")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, FCConvert.ToString(rsLoad.Get_Fields_String("firstname")) + " " + FCConvert.ToString(rsLoad.Get_Fields_String("lastname")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLQUICKNAME, FCConvert.ToString(rsLoad.Get_Fields_String("nickname")));
						if (boolUseGroups && boolGroupDetail)
						{
							Grid.RowOutlineLevel(lngRow, 1);
							Grid.IsSubtotal(lngRow, true);
						}
						rsLoad.MoveNext();
					}
				}
				if (boolUseGroups)
				{
					if (boolAllowNew)
					{
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(1));
						if (boolPicError)
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, CNSTGRIDCOLTYPE, ImageList1.Images[2 - 1]);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, "");
							Grid.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, lngRow, CNSTGRIDCOLTYPE, FCGrid.PictureAlignmentSettings.flexPicAlignCenterCenter);
							Grid.RowData(lngRow, 1);
						}
						Grid.TextMatrix(lngRow, CNSTGRIDCOLUSE, FCConvert.ToString(false));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(0));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLEMAIL, "");
						Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, "New Group");
						Grid.TextMatrix(lngRow, CNSTGRIDCOLQUICKNAME, "");
						lngGroupRow = lngRow;
						if (boolGroupDetail)
						{
							Grid.RowOutlineLevel(lngRow, 1);
							Grid.IsSubtotal(lngRow, true);
						}
					}
					rsLoad.OpenRecordset("select * from emailgroups order by nickname,description", "SystemSettings");
					while (!rsLoad.EndOfFile())
					{
						Grid.Rows += 1;
						lngRow = Grid.Rows - 1;
						Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(1));
						if (boolPicError)
						{
							Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, CNSTGRIDCOLTYPE, ImageList1.Images[2 - 1]);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, "");
							Grid.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, lngRow, CNSTGRIDCOLTYPE, FCGrid.PictureAlignmentSettings.flexPicAlignCenterCenter);
							Grid.RowData(lngRow, 1);
						}
						Grid.TextMatrix(lngRow, CNSTGRIDCOLUSE, FCConvert.ToString(false));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(rsLoad.Get_Fields_Int32("groupid")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLEMAIL, "");
						Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, FCConvert.ToString(rsLoad.Get_Fields_String("description")));
						Grid.TextMatrix(lngRow, CNSTGRIDCOLQUICKNAME, FCConvert.ToString(rsLoad.Get_Fields_String("nickname")));
						lngGroupRow = lngRow;
						if (boolGroupDetail)
						{
							Grid.RowOutlineLevel(lngRow, 1);
							Grid.IsSubtotal(lngRow, true);
							rsGroup.OpenRecordset("select * from emailcontacts inner join emailassociations on (emailcontacts.contactid = emailassociations.contactid) where groupid = " + FCConvert.ToString(rsLoad.Get_Fields_Int32("groupid")) + " order by nickname,email", "SystemSettings");
							while (!rsGroup.EndOfFile())
							{
								Grid.Rows += 1;
								lngRow = Grid.Rows - 1;
								Grid.RowOutlineLevel(lngRow, 2);
								Grid.IsSubtotal(lngRow, true);
								Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, FCConvert.ToString(0));
								if (boolPicError)
								{
									Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRow, CNSTGRIDCOLTYPE, ImageList1.Images[0]);
									Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, "");
									Grid.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, lngRow, CNSTGRIDCOLTYPE, FCGrid.PictureAlignmentSettings.flexPicAlignCenterCenter);
									Grid.RowData(lngRow, 0);
								}
								Grid.TextMatrix(lngRow, CNSTGRIDCOLUSE, FCConvert.ToString(false));
								// TODO: Field [emailcontacts.contactid] not found!! (maybe it is an alias?)
								Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(rsGroup.Get_Fields("emailcontacts.contactid")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLEMAIL, FCConvert.ToString(rsGroup.Get_Fields_String("email")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLDESC, FCConvert.ToString(rsGroup.Get_Fields_String("Firstname")) + " " + FCConvert.ToString(rsGroup.Get_Fields_String("lastname")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLQUICKNAME, FCConvert.ToString(rsGroup.Get_Fields_String("nickname")));
								Grid.IsCollapsed(lngGroupRow, FCGrid.CollapsedSettings.flexOutlineCollapsed);
								rsGroup.MoveNext();
							}
						}
						rsLoad.MoveNext();
					}
				}
                //FC:FINAL:SBE - redesign grid level tree task
                modColorScheme.ColorGrid(Grid);
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In LoadGrid", MsgBoxStyle.Critical, "Error");
			}
		}

		private void frmEmailChooseContactGroup_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_DblClick(object sender, EventArgs e)
		{
			SaveInfo();
		}

		private void grid_KeyDown(object sender, KeyEventArgs e)
		{
			if (Grid.Row > 0)
			{
				if (e.KeyCode == Keys.Return)
				{
					SaveInfo();
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void SaveInfo()
		{
			try
			{
				// On Error GoTo ErrorHandler
				int lngRow = 0;
				if (!boolMultipleChoices)
				{
					if (Grid.Row > 0)
					{
						lngRow = Grid.Row;
						if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == 0)
						{
							ReturnInfo.boolContact = true;
						}
						else
						{
							ReturnInfo.boolContact = false;
						}
						if (boolPicError)
						{
							if (Conversion.Val(Grid.RowData(lngRow)) == 0)
							{
								ReturnInfo.boolContact = true;
							}
							else
							{
								ReturnInfo.boolContact = false;
							}
						}
						ReturnInfo.lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLid))));
						ReturnInfo.strQuickName = Grid.TextMatrix(lngRow, CNSTGRIDCOLQUICKNAME);
						Close();
					}
					else
					{
						FCMessageBox.Show("You must make a selection first", MsgBoxStyle.Exclamation, "No Selection");
						return;
					}
				}
				else
				{
					bool boolMadeChoice = false;
					int intCount = 0;
					boolMadeChoice = false;
					intCount = 0;
					for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
					{
						if (FCConvert.CBool(Grid.TextMatrix(lngRow, CNSTGRIDCOLUSE)))
						{
							boolMadeChoice = true;
							if (Information.UBound(MultiReturnInfo, 1) < intCount)
							{
								Array.Resize(ref MultiReturnInfo, intCount + 1);
								//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
								MultiReturnInfo[intCount] = new EMailSelectType(0);
							}
							if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) == 0)
							{
								MultiReturnInfo[intCount].boolContact = true;
							}
							else
							{
								MultiReturnInfo[intCount].boolContact = false;
							}
							if (boolPicError)
							{
								if (Conversion.Val(Grid.RowData(lngRow)) == 0)
								{
									MultiReturnInfo[intCount].boolContact = true;
								}
								else
								{
									MultiReturnInfo[intCount].boolContact = false;
								}
							}
							MultiReturnInfo[intCount].lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLid))));
							MultiReturnInfo[intCount].strQuickName = Grid.TextMatrix(lngRow, CNSTGRIDCOLQUICKNAME);
							intCount += 1;
						}
					}
					// lngRow
					if (!boolMadeChoice)
					{
						FCMessageBox.Show("You must make a selection first", MsgBoxStyle.Exclamation, "No Selection");
					}
					else
					{
						Close();
					}
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In SaveInfo", MsgBoxStyle.Critical, "Error");
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuSaveContinue_Click(sender, e);
		}
	}
}
