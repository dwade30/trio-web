//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;
using fecherFoundation;
using Scripting;
#if TWGNENTY
using TWGNENTY;
#endif

namespace Global
{
	/// <summary>
	/// Summary description for frmSetupSignatures.
	/// </summary>
	partial class frmSetupSignatures : BaseForm
	{
		public fecherFoundation.FCFrame framMain;
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCGrid GridDelete;
		public fecherFoundation.FCGrid gridUser;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame framSignature;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOk;
		public fecherFoundation.FCButton cmdBrowse;
		public fecherFoundation.FCTextBox txtConfirmPassword;
		public fecherFoundation.FCTextBox txtPassword;
		public fecherFoundation.FCPictureBox imgSignature;
		public fecherFoundation.FCLabel lblConfirm;
		public fecherFoundation.FCLabel lblPassword;
		public fecherFoundation.FCLabel Shape1;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddUser;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
				_InstancePtr = null;
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmSetupSignatures));
			this.components = new System.ComponentModel.Container();
			this.framMain = new fecherFoundation.FCFrame();
			this.cmbType = new fecherFoundation.FCComboBox();
            this.GridDelete = new fecherFoundation.FCGrid();
			this.gridUser = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
			this.framSignature = new fecherFoundation.FCFrame();
			this.cmdClear = new fecherFoundation.FCButton();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdOk = new fecherFoundation.FCButton();
			this.cmdBrowse = new fecherFoundation.FCButton();
			this.txtConfirmPassword = new fecherFoundation.FCTextBox();
			this.txtPassword = new fecherFoundation.FCTextBox();
			this.imgSignature = new fecherFoundation.FCPictureBox();
			this.lblConfirm = new fecherFoundation.FCLabel();
			this.lblPassword = new fecherFoundation.FCLabel();
			this.Shape1 = new fecherFoundation.FCLabel();
			this.MainMenu1 = new Wisej.Web.MainMenu();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddUser = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.SuspendLayout();
			//
			// framMain
			//
			this.framMain.Controls.Add(this.cmbType);
			this.framMain.Controls.Add(this.GridDelete);
			this.framMain.Controls.Add(this.gridUser);
			this.framMain.Controls.Add(this.Label1);
			this.framMain.Name = "framMain";
			this.framMain.TabIndex = 10;
			this.framMain.Location = new System.Drawing.Point(4, 19);
			this.framMain.Size = new System.Drawing.Size(390, 127);
			this.framMain.Text = "";
			this.framMain.BackColor = System.Drawing.SystemColors.Control;
			//
			// cmbType
			//
			this.cmbType.Name = "cmbType";
			this.cmbType.TabIndex = 0;
			this.cmbType.Location = new System.Drawing.Point(50, 11);
			this.cmbType.Size = new System.Drawing.Size(293, 22);
			this.cmbType.Text = "";
			this.cmbType.BackColor = System.Drawing.SystemColors.Window;
			this.cmbType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
			//
			// GridDelete
			//
			this.GridDelete.Name = "GridDelete";
			this.GridDelete.Enabled = true;
			this.GridDelete.Visible = false;
			this.GridDelete.TabIndex = 11;
			this.GridDelete.Location = new System.Drawing.Point(301, 85);
			this.GridDelete.Size = new System.Drawing.Size(69, 12);
			this.GridDelete.Rows = 0;
			this.GridDelete.Cols = 1;
			this.GridDelete.FixedRows = 0;
			this.GridDelete.FixedCols = 0;
			this.GridDelete.ExtendLastCol = false;
			this.GridDelete.ExplorerBar = 0;
			this.GridDelete.TabBehavior = 0;
			this.GridDelete.Editable = 0;
			this.GridDelete.FrozenRows = 0;
			this.GridDelete.FrozenCols = 0;
			//
			// gridUser
			//
			this.gridUser.Name = "gridUser";
			this.gridUser.Enabled = true;
			this.gridUser.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.gridUser, "Double click on an entry to edit signature or change password");
			this.gridUser.Location = new System.Drawing.Point(28, 35);
			this.gridUser.Size = new System.Drawing.Size(337, 90);
			this.gridUser.Rows = 1;
			this.gridUser.Cols = 7;
			this.gridUser.FixedRows = 1;
			this.gridUser.FixedCols = 0;
			this.gridUser.ExtendLastCol = true;
			this.gridUser.ExplorerBar = 0;
			this.gridUser.TabBehavior = 0;
			this.gridUser.Editable = 0;
			this.gridUser.FrozenRows = 0;
			this.gridUser.FrozenCols = 0;
			this.gridUser.CellEndEdit += new DataGridViewCellEventHandler(this.gridUser_AfterEdit);
			this.gridUser.DoubleClick += new System.EventHandler(this.gridUser_DblClick);
			this.gridUser.KeyDown += new KeyEventHandler(this.gridUser_KeyDownEvent);
			this.gridUser.KeyDownEdit += new KeyEventHandler(this.gridUser_KeyDownEdit);
			this.gridUser.CurrentCellChanged += new System.EventHandler(this.gridUser_RowColChange);
			//
			// Label1
			//
			this.Label1.Name = "Label1";
			this.Label1.TabIndex = 12;
			this.Label1.Location = new System.Drawing.Point(7, 14);
			this.Label1.Size = new System.Drawing.Size(43, 20);
			this.Label1.Text = "Type";
			this.Label1.BackColor = System.Drawing.SystemColors.Control;
			//this.Label1.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// framSignature
			//
			this.framSignature.Controls.Add(this.cmdClear);
			this.framSignature.Controls.Add(this.cmdCancel);
			this.framSignature.Controls.Add(this.cmdOk);
			this.framSignature.Controls.Add(this.cmdBrowse);
			this.framSignature.Controls.Add(this.txtConfirmPassword);
			this.framSignature.Controls.Add(this.txtPassword);
			this.framSignature.Controls.Add(this.imgSignature);
			this.framSignature.Controls.Add(this.lblConfirm);
			this.framSignature.Controls.Add(this.lblPassword);
			this.framSignature.Controls.Add(this.Shape1);
			this.framSignature.Name = "framSignature";
			this.framSignature.Enabled = false;
			this.framSignature.TabIndex = 2;
			this.framSignature.Location = new System.Drawing.Point(4, 145);
			this.framSignature.Size = new System.Drawing.Size(390, 133);
			this.framSignature.Text = "Signature";
			this.framSignature.BackColor = System.Drawing.SystemColors.Control;
			//this.framSignature.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// cmdClear
			//
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Enabled = false;
			this.cmdClear.TabIndex = 13;
			this.cmdClear.Location = new System.Drawing.Point(275, 73);
			this.cmdClear.Size = new System.Drawing.Size(79, 23);
			this.cmdClear.Text = "Clear";
			this.cmdClear.BackColor = System.Drawing.SystemColors.Control;
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			//
			// cmdCancel
			//
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Enabled = false;
			this.cmdCancel.TabIndex = 9;
			this.cmdCancel.Location = new System.Drawing.Point(193, 110);
			this.cmdCancel.Size = new System.Drawing.Size(53, 23);
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.BackColor = System.Drawing.SystemColors.Control;
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			//
			// cmdOk
			//
			this.cmdOk.Name = "cmdOk";
			this.cmdOk.Enabled = false;
			this.cmdOk.TabIndex = 8;
			this.cmdOk.Location = new System.Drawing.Point(139, 110);
			this.cmdOk.Size = new System.Drawing.Size(52, 23);
			this.cmdOk.Text = "OK";
			this.cmdOk.BackColor = System.Drawing.SystemColors.Control;
			this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
			//
			// cmdBrowse
			//
			this.cmdBrowse.Name = "cmdBrowse";
			this.cmdBrowse.Enabled = false;
			this.cmdBrowse.TabIndex = 7;
			this.cmdBrowse.Location = new System.Drawing.Point(187, 73);
			this.cmdBrowse.Size = new System.Drawing.Size(79, 23);
			this.cmdBrowse.Text = "Browse";
			this.cmdBrowse.BackColor = System.Drawing.SystemColors.Control;
			this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
			//
			// txtConfirmPassword
			//
			this.txtConfirmPassword.Name = "txtConfirmPassword";
			this.txtConfirmPassword.TabIndex = 4;
			this.txtConfirmPassword.Location = new System.Drawing.Point(12, 74);
			this.txtConfirmPassword.Size = new System.Drawing.Size(131, 40);
			this.txtConfirmPassword.Text = "";
			this.txtConfirmPassword.BackColor = System.Drawing.SystemColors.Window;
			//
			// txtPassword
			//
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.TabIndex = 3;
			this.txtPassword.Location = new System.Drawing.Point(12, 33);
			this.txtPassword.Size = new System.Drawing.Size(131, 40);
			this.txtPassword.Text = "";
			this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
			//
			// imgSignature
			//
			this.imgSignature.Name = "imgSignature";
			this.imgSignature.Location = new System.Drawing.Point(166, 15);
			this.imgSignature.Size = new System.Drawing.Size(207, 53);
			this.imgSignature.BorderStyle = Wisej.Web.BorderStyle.Solid;
			this.imgSignature.BackColor = System.Drawing.SystemColors.Control;
			this.imgSignature.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			//
			// lblConfirm
			//
			this.lblConfirm.Name = "lblConfirm";
			this.lblConfirm.TabIndex = 6;
			this.lblConfirm.Location = new System.Drawing.Point(14, 58);
			this.lblConfirm.Size = new System.Drawing.Size(128, 14);
			this.lblConfirm.Text = "Confirm Password";
			this.lblConfirm.BackColor = System.Drawing.SystemColors.Control;
			this.lblConfirm.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			//
			// lblPassword
			//
			this.lblPassword.Name = "lblPassword";
			this.lblPassword.TabIndex = 5;
			this.lblPassword.Location = new System.Drawing.Point(14, 14);
			this.lblPassword.Size = new System.Drawing.Size(128, 14);
			this.lblPassword.Text = "New Password";
			this.lblPassword.BackColor = System.Drawing.SystemColors.Control;
			this.lblPassword.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			//
			// Shape1
			//
			this.Shape1.Name = "Shape1";
			this.Shape1.Visible = false;
			this.Shape1.Location = new System.Drawing.Point(166, 15);
			this.Shape1.Size = new System.Drawing.Size(207, 53);
			this.Shape1.Text = "";
            this.Shape1.BorderStyle = 1;
			//
			// mnuProcess
			//
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {this.mnuAddUser, this.mnuDelete, this.mnuSepar2, this.mnuSave, this.mnuSaveContinue, this.Seperator, this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "&File";
			//
			// mnuAddUser
			//
			this.mnuAddUser.Name = "mnuAddUser";
			this.mnuAddUser.Text = "Add New Signature";
			this.mnuAddUser.Click += new System.EventHandler(this.mnuAddUser_Click);
			//
			// mnuDelete
			//
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete Signature";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			//
			// mnuSepar2
			//
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			//
			// mnuSave
			//
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Text = "&Save";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			//
			// mnuSaveContinue
			//
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Text = "S&ave && Continue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			//
			// Seperator
			//
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			//
			// mnuExit
			//
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "E&xit    (Esc)";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {this.mnuProcess});
			//
			// frmSetupSignatures
			//
			this.ClientSize = new System.Drawing.Size(396, 257);
			this.ClientArea.Controls.Add(this.framMain);
			this.ClientArea.Controls.Add(this.framSignature);
			this.Menu = this.MainMenu1;
			this.Name = "frmSetupSignatures";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmSetupSignatures.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSetupSignatures_KeyDown);
			this.Load += new System.EventHandler(this.frmSetupSignatures_Load);
			this.Text = "Signatures";
			this.GridDelete.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridDelete)).EndInit();
			this.gridUser.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridUser)).EndInit();
			this.Label1.ResumeLayout(false);
			this.framMain.ResumeLayout(false);
			this.framSignature.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}