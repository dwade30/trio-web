﻿//Fecher vbPorter - Version 1.0.0.40
namespace Global
{
	public class cSecuritySetupItem
	{
		//=========================================================
		private string strModuleName = "";
		private string strParentFunction = "";
		private string strChildFunction = "";
		private int lngFunctionID;
		private bool boolUseViewOnly;
		private string strConstant = "";

		public string ModuleName
		{
			get
			{
				string ModuleName = "";
				ModuleName = strModuleName;
				return ModuleName;
			}
			set
			{
				strModuleName = value;
			}
		}

		public string ParentFunction
		{
			get
			{
				string ParentFunction = "";
				ParentFunction = strParentFunction;
				return ParentFunction;
			}
			set
			{
				strParentFunction = value;
			}
		}

		public string ChildFunction
		{
			get
			{
				string ChildFunction = "";
				ChildFunction = strChildFunction;
				return ChildFunction;
			}
			set
			{
				strChildFunction = value;
			}
		}

		public int FunctionID
		{
			get
			{
				int FunctionID = 0;
				FunctionID = lngFunctionID;
				return FunctionID;
			}
			set
			{
				lngFunctionID = value;
			}
		}

		public bool UseViewOnly
		{
			get
			{
				bool UseViewOnly = false;
				UseViewOnly = boolUseViewOnly;
				return UseViewOnly;
			}
			set
			{
				boolUseViewOnly = value;
			}
		}

		public string ConstantName
		{
			get
			{
				string ConstantName = "";
				ConstantName = strConstant;
				return ConstantName;
			}
			set
			{
				strConstant = value;
			}
		}
	}
}
