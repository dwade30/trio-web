﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmCustomBillAutoPop.
	/// </summary>
	public partial class frmCustomBillAutoPop : BaseForm
	{
		public frmCustomBillAutoPop()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomBillAutoPop InstancePtr
		{
			get
			{
				return (frmCustomBillAutoPop)Sys.GetInstance(typeof(frmCustomBillAutoPop));
			}
		}

		protected frmCustomBillAutoPop _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// Property of TRIO Software Corporation
		/// </summary>
		/// <summary>
		/// Written By
		/// </summary>
		/// <summary>
		/// corey gray
		/// </summary>
		/// <summary>
		/// Date
		/// </summary>
		/// <summary>
		/// 03/02/2005
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		const int CNSTGRIDCOLDESCRIPTION = 0;
		const int CNSTGRIDCOLTEXT = 1;
		const int CNSTGRIDCOLID = 2;
		string strModuleDB;
		/// <summary>
		/// database to load custom format from
		/// </summary>
		int lngFID;
		string strReturn;

		private void frmCustomBillAutoPop_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCustomBillAutoPop_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomBillAutoPop properties;
			//frmCustomBillAutoPop.FillStyle	= 0;
			//frmCustomBillAutoPop.ScaleWidth	= 5880;
			//frmCustomBillAutoPop.ScaleHeight	= 4290;
			//frmCustomBillAutoPop.LinkTopic	= "Form2";
			//frmCustomBillAutoPop.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			LoadGrid();
		}

		public string Init(int lngFormatID, string strModDB)
		{
			string Init = "";
			// returns a | delimited string
			clsDRWrapper clsLoad = new clsDRWrapper();
			strReturn = "QUIT";
			strModuleDB = strModDB;
			lngFID = lngFormatID;
			clsLoad.OpenRecordset("select * from custombillfields where FORMATID = " + FCConvert.ToString(lngFID) + " and fieldid = " + FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP), strModuleDB);
			if (clsLoad.EndOfFile())
			{
				Init = "";
				Close();
				return Init;
			}
			this.Show(FormShowEnum.Modal);
			Init = strReturn;
			return Init;
		}

		private void LoadGrid()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			string[] strAry = null;
			clsLoad.OpenRecordset("select * from custombillfields where FORMATid = " + FCConvert.ToString(lngFID) + " and fieldid = " + FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP) + " order by ID", strModuleDB);
			while (!clsLoad.EndOfFile())
			{
				Grid.Rows += 1;
				lngRow = Grid.Rows - 1;
				Grid.TextMatrix(lngRow, CNSTGRIDCOLID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
				Grid.TextMatrix(lngRow, CNSTGRIDCOLDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("usertext")));
				Grid.RowData(lngRow, false);
				if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("extraparameters"))) != string.Empty)
				{
					strAry = Strings.Split(FCConvert.ToString(clsLoad.Get_Fields_String("extraparameters")), ";", -1, CompareConstants.vbTextCompare);
					if (Strings.UCase(strAry[0]) == "SAVE")
					{
						Grid.RowData(lngRow, true);
						if (Information.UBound(strAry, 1) > 0)
						{
							Grid.TextMatrix(lngRow, CNSTGRIDCOLTEXT, strAry[1]);
						}
					}
				}
				clsLoad.MoveNext();
			}
		}

		private void frmCustomBillAutoPop_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			strReturn = "QUIT";
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGrid()
		{
			Grid.Cols = 3;
			Grid.Rows = 1;
			Grid.FixedCols = 1;
			Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			Grid.ColHidden(CNSTGRIDCOLID, true);
			Grid.TextMatrix(0, CNSTGRIDCOLDESCRIPTION, "User Defined Item");
			Grid.TextMatrix(0, CNSTGRIDCOLTEXT, "");
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLDESCRIPTION, FCConvert.ToInt32(0.3 * GridWidth));
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			int x;
			string strTemp = "";
			clsDRWrapper clsSave = new clsDRWrapper();
			string strMessage = "";
			Grid.Row = 0;
			//Application.DoEvents();
			for (x = 1; x <= Grid.Rows - 1; x++)
			{
				if (x > 1)
					strTemp += "|";
				strTemp += Grid.TextMatrix(x, CNSTGRIDCOLID) + "|";
				strTemp += Grid.TextMatrix(x, CNSTGRIDCOLTEXT);
				if (FCConvert.ToBoolean(Grid.RowData(x)))
				{
					strMessage = Grid.TextMatrix(x, CNSTGRIDCOLTEXT);
					strMessage = modGlobalFunctions.EscapeQuotes(strMessage);
					clsSave.Execute("update custombillfields set extraparameters = 'Save;" + strMessage + "' where ID = " + Grid.TextMatrix(x, CNSTGRIDCOLID), strModuleDB);
				}
			}
			// x
			strReturn = strTemp;
			Close();
		}

		private void cmdSaveAndContinue_Click(object sender, EventArgs e)
		{
			mnuSaveContinue_Click(mnuSaveContinue, EventArgs.Empty);
		}
	}
}
