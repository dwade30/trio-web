//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWBD0000
using TWBD0000;


#elif TWCR0000
using TWCR0000;

#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmLienPartialPayment.
	/// </summary>
	partial class frmLienPartialPayment : BaseForm
	{
		public fecherFoundation.FCFrame fraVariables;
		public FCGrid vsData;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel lblYear;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmLienPartialPayment));
			this.fraVariables = new fecherFoundation.FCFrame();
			this.vsData = new FCGrid();
			this.lblName = new fecherFoundation.FCLabel();
			this.lblYear = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.SuspendLayout();
			//
			// fraVariables
			//
			this.fraVariables.Controls.Add(this.vsData);
			this.fraVariables.Controls.Add(this.lblName);
			this.fraVariables.Controls.Add(this.lblYear);
			this.fraVariables.Name = "fraVariables";
			this.fraVariables.TabIndex = 0;
			this.fraVariables.Location = new System.Drawing.Point(10, 33);
			this.fraVariables.Size = new System.Drawing.Size(377, 265);
			this.fraVariables.Text = "Confirm Data";
			//this.fraVariables.BackColor = System.Drawing.SystemColors.Control;
			////this.fraVariables.Font = new System.Drawing.Font("Tahoma", 9.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// vsData
			//
			this.vsData.Name = "vsData";
			this.vsData.Enabled = true;
			this.vsData.TabIndex = 1;
			this.vsData.Location = new System.Drawing.Point(12, 50);
			this.vsData.Size = new System.Drawing.Size(353, 202);
			this.vsData.Cols = 3;
			this.vsData.Editable = FCGrid.EditableSettings.flexEDNone;
			this.vsData.FixedCols = 1;
			this.vsData.FixedRows = 0;
			this.vsData.FrozenCols = 0;
			this.vsData.FrozenRows = 0;
			this.vsData.Rows = 1;
			this.vsData.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			//this.vsData.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vsData.OcxState")));
			this.vsData.CellBeginEdit += new DataGridViewCellCancelEventHandler(this.vsData_BeforeEdit);
			this.vsData.KeyPressEdit += new KeyPressEventHandler(this.vsData_KeyPressEdit);
			this.vsData.CurrentCellChanged += new System.EventHandler(this.vsData_RowColChange);
			//
			// lblName
			//
			this.lblName.Name = "lblName";
			this.lblName.TabIndex = 3;
			this.lblName.Location = new System.Drawing.Point(16, 31);
			this.lblName.Size = new System.Drawing.Size(345, 15);
			this.lblName.Text = "";
			//this.lblName.BackColor = System.Drawing.SystemColors.Control;
			this.lblName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			//
			// lblYear
			//
			this.lblYear.Name = "lblYear";
			this.lblYear.TabIndex = 2;
			this.lblYear.Location = new System.Drawing.Point(121, 14);
			this.lblYear.Size = new System.Drawing.Size(130, 15);
			this.lblYear.Text = "";
			//this.lblYear.BackColor = System.Drawing.SystemColors.Control;
			this.lblYear.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			//
			// mnuFile
			//
			this.mnuFile.MenuItems.AddRange(new fecherFoundation.FCToolStripMenuItem[] {
				this.mnuFileSaveExit,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			//
			// mnuFileSaveExit
			//
			this.mnuFileSaveExit.Name = "mnuFileSaveExit";
			this.mnuFileSaveExit.Text = "Save & Continue";
			this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			//
			// Seperator
			//
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			//
			// mnuFileExit
			//
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new fecherFoundation.FCToolStripMenuItem[] {
				this.mnuFile
			});
			//
			// frmLienPartialPayment
			//
			this.ClientSize = new System.Drawing.Size(396, 296);
			this.ClientArea.Controls.Add(this.fraVariables);
			this.Menu = this.MainMenu1;
			this.Name = "frmLienPartialPayment";
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Activated += new System.EventHandler(this.frmLienPartialPayment_Activated);
			this.Load += new System.EventHandler(this.frmLienPartialPayment_Load);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLienPartialPayment_KeyPress);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Resize += new System.EventHandler(this.frmLienPartialPayment_Resize);
			this.Text = "Partial Payment ";
			this.vsData.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
			this.fraVariables.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}