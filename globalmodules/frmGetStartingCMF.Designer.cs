﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmGetStartingCMF.
	/// </summary>
	partial class frmGetStartingCMF : BaseForm
	{
		public fecherFoundation.FCCheckBox chkNoIMPB;
		public fecherFoundation.FCFrame fraIMPB;
		public fecherFoundation.FCTextBox txtIMPBNumber;
		public fecherFoundation.FCLabel lblIMPBHelp;
		public fecherFoundation.FCFrame fra128bc;
		public fecherFoundation.FCTextBox txt128bcNumber;
		public fecherFoundation.FCLabel lbl128bcHelp;
		public fecherFoundation.FCLabel lblInputHelp;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetStartingCMF));
			this.chkNoIMPB = new fecherFoundation.FCCheckBox();
			this.fraIMPB = new fecherFoundation.FCFrame();
			this.txtIMPBNumber = new fecherFoundation.FCTextBox();
			this.lblIMPBHelp = new fecherFoundation.FCLabel();
			this.fra128bc = new fecherFoundation.FCFrame();
			this.txt128bcNumber = new fecherFoundation.FCTextBox();
			this.lbl128bcHelp = new fecherFoundation.FCLabel();
			this.lblInputHelp = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileProcess = new fecherFoundation.FCToolStripMenuItem();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkNoIMPB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraIMPB)).BeginInit();
			this.fraIMPB.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fra128bc)).BeginInit();
			this.fra128bc.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 421);
			this.BottomPanel.Size = new System.Drawing.Size(793, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkNoIMPB);
			this.ClientArea.Controls.Add(this.fraIMPB);
			this.ClientArea.Controls.Add(this.fra128bc);
			this.ClientArea.Controls.Add(this.lblInputHelp);
			this.ClientArea.Size = new System.Drawing.Size(793, 361);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(793, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(317, 35);
			this.HeaderText.Text = "Setup Certified Mail Forms";
			// 
			// chkNoIMPB
			// 
			this.chkNoIMPB.Location = new System.Drawing.Point(30, 150);
			this.chkNoIMPB.Name = "chkNoIMPB";
			this.chkNoIMPB.Size = new System.Drawing.Size(233, 24);
			this.chkNoIMPB.TabIndex = 1;
			this.chkNoIMPB.Text = "Do not use IMPB Tracking Number";
			// 
			// fraIMPB
			// 
			this.fraIMPB.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraIMPB.Controls.Add(this.txtIMPBNumber);
			this.fraIMPB.Controls.Add(this.lblIMPBHelp);
            this.fraIMPB.FormatCaption = false;
			this.fraIMPB.Location = new System.Drawing.Point(30, 198);
			this.fraIMPB.Name = "fraIMPB";
			this.fraIMPB.Size = new System.Drawing.Size(735, 104);
			this.fraIMPB.TabIndex = 2;
			this.fraIMPB.Text = "Enter The Starting IMPB Tracking Number";
			this.fraIMPB.UseMnemonic = false;
			// 
			// txtIMPBNumber
			// 
			this.txtIMPBNumber.AutoSize = false;
			this.txtIMPBNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtIMPBNumber.Location = new System.Drawing.Point(20, 30);
			this.txtIMPBNumber.Name = "txtIMPBNumber";
			//FC:FINAL:CHN - issue #1171: Large fields needed for CMF numbers.
			this.txtIMPBNumber.Size = new System.Drawing.Size(250, 40);
			// was (207, 40)
			this.txtIMPBNumber.TabIndex = 0;
			// 
			// lblIMPBHelp
			// 
			this.lblIMPBHelp.AutoSize = true;
			this.lblIMPBHelp.Location = new System.Drawing.Point(20, 79);
			this.lblIMPBHelp.Name = "lblIMPBHelp";
			this.lblIMPBHelp.Size = new System.Drawing.Size(740, 17);
			this.lblIMPBHelp.TabIndex = 1;
			this.lblIMPBHelp.Text = "THIS IS THE 22-DIGIT BARCODE NUMBER PRINTED IN BOX 1 AND ON THE FRONT OF THE CERT" + "IFIED MAIL FORM";
			this.lblIMPBHelp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fra128bc
			// 
			this.fra128bc.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fra128bc.Controls.Add(this.txt128bcNumber);
			this.fra128bc.Controls.Add(this.lbl128bcHelp);
			this.fra128bc.Location = new System.Drawing.Point(30, 30);
			this.fra128bc.Name = "fra128bc";
			this.fra128bc.Size = new System.Drawing.Size(735, 104);
			this.fra128bc.TabIndex = 0;
			this.fra128bc.Text = "ENTER THE STARTING BARCODE NUMBER";
			this.fra128bc.UseMnemonic = false;
			// 
			// txt128bcNumber
			// 
			this.txt128bcNumber.AutoSize = false;
			this.txt128bcNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txt128bcNumber.Location = new System.Drawing.Point(20, 30);
			this.txt128bcNumber.Name = "txt128bcNumber";
			//FC:FINAL:CHN - issue #1171: Large fields needed for CMF numbers.
			this.txt128bcNumber.Size = new System.Drawing.Size(250, 40);
			// was (207, 40)
			this.txt128bcNumber.TabIndex = 0;
			// 
			// lbl128bcHelp
			// 
			this.lbl128bcHelp.AutoSize = true;
			this.lbl128bcHelp.Location = new System.Drawing.Point(20, 78);
			this.lbl128bcHelp.Name = "lbl128bcHelp";
			this.lbl128bcHelp.Size = new System.Drawing.Size(735, 17);
			this.lbl128bcHelp.TabIndex = 1;
			this.lbl128bcHelp.Text = "THIS IS THE 20-DIGIT BARCODE NUMBER PRINTED ON THE CERTIFIED MAIL LABEL AND IN BO" + "X 2 OF THE FORM";
			this.lbl128bcHelp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblInputHelp
			// 
			this.lblInputHelp.AutoSize = true;
			this.lblInputHelp.Location = new System.Drawing.Point(30, 332);
			this.lblInputHelp.Name = "lblInputHelp";
			this.lblInputHelp.Size = new System.Drawing.Size(378, 17);
			this.lblInputHelp.TabIndex = 3;
			this.lblInputHelp.Text = "YOU MAY USE SPACES OR HYPHENS TO GROUP DIGITS";
			this.lblInputHelp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(336, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(120, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Process";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = -1;
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// mnuFileProcess
			// 
			this.mnuFileProcess.Index = -1;
			this.mnuFileProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileProcess.Text = "Process";
			this.mnuFileProcess.Click += new System.EventHandler(this.mnuFileProcess_Click);
			// 
			// frmGetStartingCMF
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(793, 522);
			this.FillColor = 0;
			this.Name = "frmGetStartingCMF";
			this.Text = "Setup Certified Mail Forms";
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkNoIMPB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraIMPB)).EndInit();
			this.fraIMPB.ResumeLayout(false);
			this.fraIMPB.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fra128bc)).EndInit();
			this.fra128bc.ResumeLayout(false);
			this.fra128bc.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcess;
		public FCToolStripMenuItem mnuFileExit;
		public FCToolStripMenuItem mnuFileProcess;
	}
}
