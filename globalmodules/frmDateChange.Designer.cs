//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;

#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmDateChange.
	/// </summary>
	partial class frmDateChange : BaseForm
	{
		public fecherFoundation.FCCheckBox chkEffect;
		public Global.T2KDateBox txtDate;
		public fecherFoundation.FCButton cmdQuit;
		public fecherFoundation.FCButton cmdOk;
		public fecherFoundation.FCLabel lblInstructions;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.chkEffect = new fecherFoundation.FCCheckBox();
			this.txtDate = new Global.T2KDateBox();
			this.cmdQuit = new fecherFoundation.FCButton();
			this.cmdOk = new fecherFoundation.FCButton();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEffect)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdOk);
			this.BottomPanel.Controls.Add(this.cmdQuit);
			this.BottomPanel.Location = new System.Drawing.Point(0, 173);
			this.BottomPanel.Size = new System.Drawing.Size(419, 64);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Controls.Add(this.chkEffect);
			this.ClientArea.Controls.Add(this.txtDate);
			this.ClientArea.Location = new System.Drawing.Point(0, 10);
			this.ClientArea.Size = new System.Drawing.Size(419, 163);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(419, 10);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// chkEffect
			// 
			this.chkEffect.Checked = true;
			this.chkEffect.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkEffect.Location = new System.Drawing.Point(42, 126);
			this.chkEffect.Name = "chkEffect";
			this.chkEffect.Size = new System.Drawing.Size(228, 23);
			this.chkEffect.TabIndex = 4;
			this.chkEffect.Text = "Affect Recorded Transaction Date";
			this.chkEffect.Visible = false;
			// 
			// txtDate
			// 
			this.txtDate.Location = new System.Drawing.Point(148, 60);
			this.txtDate.Mask = "00/00/0000";
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(122, 22);
			this.txtDate.TabIndex = 0;
			this.txtDate.Text = "  /  /";
			this.txtDate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtDate_KeyDownEvent);
			this.txtDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtDate_Validate);
			// 
			// cmdQuit
			// 
			this.cmdQuit.AppearanceKey = "actionButton";
			this.cmdQuit.Location = new System.Drawing.Point(116, 12);
			this.cmdQuit.Name = "cmdQuit";
			this.cmdQuit.Size = new System.Drawing.Size(73, 40);
			this.cmdQuit.TabIndex = 2;
			this.cmdQuit.Text = "Cancel";
			this.cmdQuit.Click += new System.EventHandler(this.cmdQuit_Click);
			// 
			// cmdOk
			// 
			this.cmdOk.AppearanceKey = "actionButton";
			this.cmdOk.Location = new System.Drawing.Point(19, 12);
			this.cmdOk.Name = "cmdOk";
			this.cmdOk.Size = new System.Drawing.Size(78, 40);
			this.cmdOk.TabIndex = 1;
			this.cmdOk.Text = "Process";
			this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(10, 11);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(390, 43);
			this.lblInstructions.TabIndex = 3;
			this.lblInstructions.Text = "PLEASE ENTER DATE TO USE FOR PROCESSING THIS TRANSACTION";
			this.lblInstructions.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// frmDateChange
			// 
			this.ClientSize = new System.Drawing.Size(419, 237);
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmDateChange";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Date Change";
			this.Load += new System.EventHandler(this.frmDateChange_Load);
			this.Activated += new System.EventHandler(this.frmDateChange_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmDateChange_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDateChange_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEffect)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdQuit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion
	}
}