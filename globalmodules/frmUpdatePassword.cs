﻿//Fecher vbPorter - Version 1.0.0.27
using Wisej.Web;
using fecherFoundation;
using Global;
using System;

namespace Global
{
	/// <summary>
	/// Summary description for frmUpdatePassword.
	/// </summary>
	public partial class frmUpdatePassword : BaseForm
	{
		public frmUpdatePassword()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUpdatePassword InstancePtr
		{
			get
			{
				return (frmUpdatePassword)Sys.GetInstance(typeof(frmUpdatePassword));
			}
		}

		protected frmUpdatePassword _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private string strNewPass = string.Empty;

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			strNewPass = "";
			Close();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			if (Strings.UCase(txtPassword1.Text) != Strings.UCase(txtPassword2.Text))
			{
				MessageBox.Show("Passwords do not match.", "No Match", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				txtPassword1.Text = "";
				txtPassword2.Text = "";
			}
			else
			{
				strNewPass = txtPassword1.Text;
				Close();
			}
		}

		private void frmUpdatePassword_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmUpdatePassword_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUpdatePassword.FillStyle	= 0;
			//frmUpdatePassword.ScaleWidth	= 3885;
			//frmUpdatePassword.ScaleHeight	= 2745;
			//frmUpdatePassword.LinkTopic	= "Form2";
			//frmUpdatePassword.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			// SetFixedSize Me, TRIOWINDOWSIZESMALL
			// SetTRIOColors Me
		}

		public string Init()
		{
			string Init = "";
			strNewPass = "";
			this.Show(FormShowEnum.Modal, App.MainForm);
			Init = strNewPass;
			return Init;
		}

		private void mnuExit_Click()
		{
			Close();
		}
	}
}
