//Fecher vbPorter - Version 1.0.0.32
using System;
using System.Linq.Expressions;
using Wisej.Web;
using fecherFoundation;

namespace Global
{
    public class cPPDatabase
    {

        //=========================================================

        private string strLastError = "";
        private int lngLastError;

        public string LastErrorMessage
        {
            get
            {
                string LastErrorMessage = "";
                LastErrorMessage = strLastError;
                return LastErrorMessage;
            }
        }

        public int LastErrorNumber
        {
            get
            {
                int LastErrorNumber = 0;
                LastErrorNumber = lngLastError;
                return LastErrorNumber;
            }
        }

        public void ClearErrors()
        {
            strLastError = "";
            lngLastError = 0;
        }

        public cVersionInfo GetVersion()
        {
            cVersionInfo GetVersion = null;
            try
            {   // On Error GoTo ErrorHandler
                ClearErrors();
                cVersionInfo tVer = new cVersionInfo();
                clsDRWrapper rsLoad = new clsDRWrapper();
                rsLoad.OpenRecordset("select * from dbversion", "PersonalProperty");
                if (!rsLoad.EndOfFile())
                {
                    tVer.Build = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Build"));
                    tVer.Major = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Major"));
                    tVer.Minor = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Minor"));
                    tVer.Revision = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Revision"));
                }
                GetVersion = tVer;
                return GetVersion;
            }
            catch (Exception ex)
            {   // ErrorHandler:
                strLastError = Information.Err(ex).Description;
                lngLastError = Information.Err(ex).Number;
            }
            return GetVersion;
        }


        public void SetVersion(cVersionInfo nVersion)
        {
            try
            {   // On Error GoTo ErrorHandler
                if (!(nVersion == null))
                {
                    clsDRWrapper rsSave = new clsDRWrapper();
                    rsSave.OpenRecordset("select * from dbversion", "PersonalProperty");
                    if (rsSave.EndOfFile())
                    {
                        rsSave.AddNew();
                    }
                    else
                    {
                        rsSave.Edit();
                    }
                    rsSave.Set_Fields("Major", nVersion.Major);
                    rsSave.Set_Fields("Minor", nVersion.Minor);
                    rsSave.Set_Fields("Revision", nVersion.Revision);
                    rsSave.Set_Fields("Build", nVersion.Build);
                    rsSave.Update();
                }
                return;
            }
            catch (Exception ex)
            {   // ErrorHandler:
                strLastError = Information.Err(ex).Description;
                lngLastError = Information.Err(ex).Number;
            }
        }

        public bool CheckVersion()
        {
            bool CheckVersion = false;
            ClearErrors();
            try
            {   // On Error GoTo ErrorHandler
                cVersionInfo nVer = new cVersionInfo();
                cVersionInfo tVer = new cVersionInfo();
                cVersionInfo cVer;
                bool boolNeedUpdate;
                clsDRWrapper rsTest = new clsDRWrapper();
                if (!rsTest.DBExists("PersonalProperty"))
                {
                    return CheckVersion;
                }

                cVer = GetVersion();
                if (cVer == null)
                {
                    cVer = new cVersionInfo();
                }
                nVer.Major = 1; // default to 1.0.0.0

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 0;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (MakeIndexes())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddSettingsTable())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 2;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (CheckCommentLength())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 3;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (CheckRatioTrendTable())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 4;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddYearsClaimedField())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 5;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddCategoryType())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 7;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddAccountPartyView())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 8;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddAccountPartyAddressView())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 9;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
	                if (AddExemptMVRCategory())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(nVer);
		                }
	                }
	                else
	                {
		                return false;
	                }
                }

				CheckVersion = true;
                return CheckVersion;
            }
            catch (Exception ex)
            {   // ErrorHandler:
                strLastError = Information.Err(ex).Description;
                lngLastError = Information.Err(ex).Number;
            }
            return CheckVersion;
        }

        private bool CheckCommentLength()
        {
            bool CheckCommentLength = false;
            try
            {   // On Error GoTo ErrorHandler
                if (Strings.LCase(GetDataType("PPMaster", "Comment", "PersonalProperty")) == "nvarchar")
                {
                    clsDRWrapper rsSave = new clsDRWrapper();
                    string strSQL = "";
                    strSQL = "Alter table PPMaster alter column [Comment] varchar(max) null";
                    rsSave.Execute(strSQL, "PersonalProperty");
                }
                CheckCommentLength = true;
                return CheckCommentLength;
            }
            catch (Exception ex)
            {   // ErrorHandler:
                strLastError = Information.Err(ex).Description;
                lngLastError = Information.Err(ex).Number;
            }
            return CheckCommentLength;
        }

        private bool AddSettingsTable()
        {
            bool AddSettingsTable = false;
            cSettingsController contSet = new cSettingsController();
            AddSettingsTable = contSet.AddSettingsTable("PersonalProperty");
            return AddSettingsTable;
        }

        private bool MakeIndexes()
        {
            bool MakeIndexes = false;
            try
            {   // On Error GoTo ErrorHandler
                MakePPIndex("PPMaster", "ix_Account", "Account");
                MakePPIndex("AccountList", "ix_Account", "Account");
                MakePPIndex("BusinessCodes", "ix_BusinessCode", "BusinessCode");
                MakePPIndex("ExemptCodes", "ix_Code", "Code");
                MakePPIndex("Owners", "ix_Account", "Account");
                MakePPIndex("PPItemized", "ix_AccountLine", "Account,Line");
                MakePPIndex("PPLeased", "ix_AccountLine", "Account,Line");
                MakePPIndex("PPMaster", "ix_Account", "Account");
                MakePPIndex("PPValuations", "ix_ValueKey", "ValueKey");
                MakePPIndex("StreetCodes", "ix_StreetCode", "StreetCode");
                MakePPIndex("TranCodes", "ix_TranCode", "TranCode");
                MakePPIndex("TrendCapRates", "ix_Year", "Year");

                MakeIndexes = true;
                return MakeIndexes;
            }
            catch (Exception ex)
            {   // ErrorHandler:
                MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                MakeIndexes = false;
            }
            return MakeIndexes;
        }

        private void MakePPIndex(string strTableName, string strName, string strFields)
        {
            MakeIndex("PersonalProperty", strTableName, strName, strFields);
        }

        private void MakeIndex(string strDBName, string strTableName, string strName, string strFields)
        {
            string strSQL = "";
            clsDRWrapper rsSave = new clsDRWrapper();
            rsSave.OpenRecordset(GetFindIndexSQL(strTableName, strName), strDBName);
            if (rsSave.EndOfFile())
            {
                rsSave.Execute(GetAddIndexSQL(strTableName, strName, strFields), strDBName);
            }
        }

        private string GetFindIndexSQL(string strTableName, string strName)
        {
            string GetFindIndexSQL = "";
            string strSQL;

            strSQL = "Select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[" + strTableName + "]') and name =N'" + strName + "'";
            GetFindIndexSQL = strSQL;
            return GetFindIndexSQL;
        }

        private string GetAddIndexSQL(string strTableName, string strName, string strFields)
        {
            string GetAddIndexSQL = "";
            string[] strAry = null;
            strAry = Strings.Split(strFields, ",", -1, CompareConstants.vbBinaryCompare);
            string strSQL;
            strSQL = "CREATE NONCLUSTERED INDEX [" + strName + "] on [dbo].[" + strTableName + "] (";
            int x;
            for (x = 0; x <= Information.UBound(strAry, 1); x++)
            {
                if (x > 0)
                {
                    strSQL += ",[" + strAry[x] + "] ASC";
                }
                else
                {
                    strSQL += "[" + strAry[x] + "] ASC";
                }
            } // x
            strSQL += ") WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
            GetAddIndexSQL = strSQL;
            return GetAddIndexSQL;
        }

        private string GetDataType(string strTableName, string strFieldName, string strDB)
        {
            string GetDataType = "";
            string strSQL;
            clsDRWrapper rsLoad = new clsDRWrapper();
            strSQL = "select data_type from information_schema.columns where table_name = '" + strTableName + "' and column_name = '" + strFieldName + "'";
            rsLoad.OpenRecordset(strSQL, strDB);
            if (!rsLoad.EndOfFile())
            {
                // TODO: Field [data_type] not found!! (maybe it is an alias?)
                GetDataType = FCConvert.ToString(rsLoad.Get_Fields("data_type"));
            }
            return GetDataType;
        }

        private bool CheckRatioTrendTable()
        {
            bool CheckRatioTrendTable = false;
            try
            {   // On Error GoTo ErrorHandler
                string strSQL;
                clsDRWrapper rsSave = new clsDRWrapper();
                strSQL = "Select * from sys.columns where name = N'S/D' and object_id = object_id(N'RatioTrends')";
                rsSave.OpenRecordset(strSQL, "PersonalProperty");
                if (!rsSave.EndOfFile())
                {
                    strSQL = "sp_rename 'RatioTrends.[S/D]', 'SD'";
                    rsSave.Execute(strSQL, "PersonalProperty");
                }
                CheckRatioTrendTable = true;
                return CheckRatioTrendTable;
            }
            catch (Exception ex)
            {   // ErrorHandler:
            }
            return CheckRatioTrendTable;
        }

        private bool FieldExists(string strTable, string strField, string strDB)
        {
            bool FieldExists = false;
            try
            {   // On Error GoTo ErrorHandler
                string strSQL;
                clsDRWrapper rsLoad = new clsDRWrapper();
                strSQL = "Select column_name from information_schema.columns where column_name = '" + strField + "' and table_name = '" + strTable + "'";
                rsLoad.OpenRecordset(strSQL, strDB);
                if (!rsLoad.EndOfFile())
                {
                    FieldExists = true;
                }
                return FieldExists;
            }
            catch (Exception ex)
            {   // ErrorHandler:

            }
            return FieldExists;
        }

        private bool AddYearsClaimedField()
        {
            bool AddYearsClaimedField = false;
            try
            {   // On Error GoTo ErrorHandler
                string strSQL = "";
                clsDRWrapper rsSave = new clsDRWrapper();
                if (!FieldExists("PPItemized", "YearsClaimed", "PersonalProperty"))
                {
                    strSQL = "alter table PPItemized Add  YearsClaimed [int] NULL";
                    rsSave.Execute(strSQL, "PersonalProperty");
                    rsSave.Execute("update ppitemized set yearsclaimed = 0", "PersonalProperty");
                }
                if (!FieldExists("PPLeased", "YearsClaimed", "PersonalProperty"))
                {
                    strSQL = "Alter table PPLeased Add YearsClaimed [int] Null";
                    rsSave.Execute(strSQL, "PersonalProperty");
                    rsSave.Execute("update ppleased set yearsclaimed = 0", "PersonalProperty");
                }

                AddYearsClaimedField = true;
                return AddYearsClaimedField;
            }
            catch (Exception ex)
            {   // ErrorHandler:
            }
            return AddYearsClaimedField;
        }

        private bool AddCategoryType()
        {
            bool AddCategoryType = false;
            try
            {   // On Error GoTo ErrorHandler
                string strSQL = "";
                clsDRWrapper rsSave = new clsDRWrapper();
                if (!FieldExists("RatioTrends", "CategoryType", "PersonalProperty"))
                {
                    strSQL = "alter table ratiotrends add CategoryType [int] NULL";
                    rsSave.Execute(strSQL, "PersonalProperty");
                    rsSave.Execute("update ratiotrends set categorytype = 0", "PersonalProperty");
                }
                AddCategoryType = true;
                return AddCategoryType;
            }
            catch (Exception ex)
            {   // ErrorHandler:
            }
            return AddCategoryType;
        }

        private bool AddAccountPartyView()
        {
            clsDRWrapper rsUpdate = new clsDRWrapper();

            try
            {
                rsUpdate.OpenRecordset("select* FROM sys.views where name = 'AccountPartyView'", "PersonalProperty");
                if (!rsUpdate.EndOfFile())
                {
                    rsUpdate.Execute("Drop View [dbo].[AccountPartyView]", "PersonalProperty");
                }
                //if (rsUpdate.RecordCount() == 0)
                //{
                    rsUpdate.Execute("CREATE VIEW [dbo].[AccountPartyView] as " +
                                     "SELECT PPMaster.*, parties.PartyGuid, parties.PartyType, parties.FirstName, parties.MiddleName, Parties.LastName, Parties.Designation, parties.EMail, parties.WebAddress " +
                                     "FROM dbo.PPMaster INNER JOIN " +
                                     rsUpdate.Get_GetFullDBName("CentralParties") +
                                     ".dbo.Parties ON PPMaster.PartyID = parties.ID " , "PersonalProperty");
                //}

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddAccountPartyAddressView()
        {
            clsDRWrapper rsUpdate = new clsDRWrapper();

            try
            {
                rsUpdate.OpenRecordset("select* FROM sys.views where name = 'AccountPartyAddressView'", "PersonalProperty");
                if (!rsUpdate.EndOfFile())
                {
                    rsUpdate.Execute("Drop View [dbo].[AccountPartyAddressView]", "PersonalProperty");
                }

                rsUpdate.Execute("CREATE VIEW [dbo].[AccountPartyAddressView] as " +
                                 "Select tbl1.* , Address1, Address2, Address3, City, State, Zip, Country, AddressType from (" +
                                 "SELECT PPMaster.*, parties.PartyGuid, parties.PartyType, parties.FirstName, parties.MiddleName, Parties.LastName, Parties.Designation, parties.EMail, parties.WebAddress " +
                                 "FROM dbo.PPMaster INNER JOIN " +
                                 rsUpdate.Get_GetFullDBName("CentralParties") +
                                 ".dbo.Parties ON PPMaster.PartyID = parties.ID " + 
                                ") tbl1 Left join " +
                                rsUpdate.Get_GetFullDBName("CentralParties") +
                                ".dbo.Addresses on tbl1.PartyID = Addresses.PartyID " +
                                "Where Addresses.AddressType = 'Primary'", "PersonalProperty");

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddExemptMVRCategory()
        {
	        string strsql = "";
	        clsDRWrapper rsSave = new clsDRWrapper();
	        try
	        {
		        if (!FieldExists("ExemptCodes", "MVRCategory", "PersonalProperty"))
		        {
			        strsql = "alter table exemptcodes add MVRCategory nvarchar(50) NULL";
			        rsSave.Execute(strsql, "PersonalProperty");
			        rsSave.Execute("update exemptcodes set MVRCategory = ''", "PersonalProperty");
		        }
		        return true;
	        }
	        catch (Exception e)
	        {
		        return false;
	        }
        }

    }
}
