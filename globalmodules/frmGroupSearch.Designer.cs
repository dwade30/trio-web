﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmGroupSearch.
	/// </summary>
	partial class frmGroupSearch : BaseForm
	{
		public FCGrid SearchGrid;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGroupSearch));
			this.SearchGrid = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 582);
			this.BottomPanel.Size = new System.Drawing.Size(673, 35);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.SearchGrid);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(673, 522);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(673, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(165, 30);
			this.HeaderText.Text = "Group Search";
			// 
			// SearchGrid
			// 
			this.SearchGrid.AllowSelection = false;
			this.SearchGrid.AllowUserToResizeColumns = false;
			this.SearchGrid.AllowUserToResizeRows = false;
			this.SearchGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.SearchGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.SearchGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.SearchGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.SearchGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.SearchGrid.BackColorSel = System.Drawing.Color.Empty;
			this.SearchGrid.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.SearchGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.SearchGrid.ColumnHeadersHeight = 30;
			this.SearchGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.SearchGrid.DefaultCellStyle = dataGridViewCellStyle2;
			this.SearchGrid.DragIcon = null;
			this.SearchGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.SearchGrid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.SearchGrid.ExtendLastCol = true;
			this.SearchGrid.FixedCols = 0;
			this.SearchGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.SearchGrid.FrozenCols = 0;
			this.SearchGrid.GridColor = System.Drawing.Color.Empty;
			this.SearchGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.SearchGrid.Location = new System.Drawing.Point(30, 80);
			this.SearchGrid.Name = "SearchGrid";
			this.SearchGrid.ReadOnly = true;
			this.SearchGrid.RowHeadersVisible = false;
			this.SearchGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.SearchGrid.RowHeightMin = 0;
			this.SearchGrid.Rows = 1;
			this.SearchGrid.ScrollTipText = null;
			this.SearchGrid.ShowColumnVisibilityMenu = false;
			this.SearchGrid.Size = new System.Drawing.Size(514, 434);
			this.SearchGrid.StandardTab = true;
			this.SearchGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.SearchGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.SearchGrid.TabIndex = 1;
			this.SearchGrid.DoubleClick += new System.EventHandler(this.SearchGrid_DblClick);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(479, 29);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "DOUBLE CLICK ON AN ENTRY TO OPEN THAT GROUP";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 0;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmGroupSearch
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(673, 617);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmGroupSearch";
			this.ShowInTaskbar = false;
			this.Text = "Group Search";
			this.Load += new System.EventHandler(this.frmGroupSearch_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGroupSearch_KeyDown);
			this.Resize += new System.EventHandler(this.frmGroupSearch_Resize);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
