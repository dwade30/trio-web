﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

#if TWBD0000
using TWBD0000;
#endif
namespace Global
{
	public class cCreditMemoController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cCreditMemo GetCreditMemo(int lngID)
		{
			cCreditMemo GetCreditMemo = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCreditMemo credMemo = null;
				rsLoad.OpenRecordset("select * from creditmemo where id = " + FCConvert.ToString(lngID), "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					credMemo = new cCreditMemo();
					credMemo.Adjustments = FCConvert.ToDouble(rsLoad.Get_Fields_Decimal("adjustments"));
					// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
					credMemo.Amount = Conversion.Val(rsLoad.Get_Fields("Amount"));
					if (Information.IsDate(rsLoad.Get_Fields("creditmemodate")))
					{
						credMemo.CreditMemoDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("CreditMemoDate"));
					}
					else
					{
						credMemo.CreditMemoDate = "";
					}
					credMemo.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					credMemo.Fund = FCConvert.ToString(rsLoad.Get_Fields("Fund"));
					// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					credMemo.JournalNumber = FCConvert.ToInt32(rsLoad.Get_Fields("JournalNumber"));
					credMemo.LiquidatedAmount = Conversion.Val(rsLoad.Get_Fields_Decimal("Liquidated"));
					credMemo.MemoNumber = FCConvert.ToString(rsLoad.Get_Fields_String("MemoNumber"));
					credMemo.OriginalAPRecord = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("OriginalAPRecord"))));
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					credMemo.Period = FCConvert.ToInt32(rsLoad.Get_Fields("Period"));
					if (Information.IsDate(rsLoad.Get_Fields("PostedDate")))
					{
						credMemo.PostedDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("PostedDate"));
					}
					else
					{
						credMemo.PostedDate = "";
					}
					credMemo.Status = FCConvert.ToString(rsLoad.Get_Fields_String("Status"));
					credMemo.VendorNumber = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("VendorNumber"));
					credMemo.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					credMemo.IsUpdated = false;
				}
				GetCreditMemo = credMemo;
				return GetCreditMemo;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetCreditMemo;
		}

		public cGenericCollection GetEnteredCreditMemosByJournalNumber(int lngJournalNumber)
		{
			cGenericCollection GetEnteredCreditMemosByJournalNumber = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCreditMemo credMemo;
				rsLoad.OpenRecordset("select * from CreditMemo where journalnumber = " + FCConvert.ToString(lngJournalNumber) + " and status = 'E' order by Description", "Budgetary");
				while (!rsLoad.EndOfFile())
				{
					////////Application.DoEvents();
					credMemo = new cCreditMemo();
					credMemo.Adjustments = FCConvert.ToDouble(rsLoad.Get_Fields_Decimal("adjustments"));
					// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
					credMemo.Amount = Conversion.Val(rsLoad.Get_Fields("Amount"));
					if (Information.IsDate(rsLoad.Get_Fields("creditmemodate")))
					{
						credMemo.CreditMemoDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("CreditMemoDate"));
					}
					else
					{
						credMemo.CreditMemoDate = "";
					}
					credMemo.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					credMemo.Fund = FCConvert.ToString(rsLoad.Get_Fields("Fund"));
					// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					credMemo.JournalNumber = FCConvert.ToInt32(rsLoad.Get_Fields("JournalNumber"));
					credMemo.LiquidatedAmount = Conversion.Val(rsLoad.Get_Fields_Decimal("Liquidated"));
					credMemo.MemoNumber = FCConvert.ToString(rsLoad.Get_Fields_String("MemoNumber"));
					credMemo.OriginalAPRecord = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("OriginalAPRecord"))));
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					credMemo.Period = FCConvert.ToInt32(rsLoad.Get_Fields("Period"));
					if (Information.IsDate(rsLoad.Get_Fields("PostedDate")))
					{
						credMemo.PostedDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("PostedDate"));
					}
					else
					{
						credMemo.PostedDate = "";
					}
					credMemo.Status = FCConvert.ToString(rsLoad.Get_Fields_String("Status"));
					credMemo.VendorNumber = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("VendorNumber"));
					credMemo.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					credMemo.IsUpdated = false;
					gColl.AddItem(credMemo);
					rsLoad.MoveNext();
				}
				GetEnteredCreditMemosByJournalNumber = gColl;
				return GetEnteredCreditMemosByJournalNumber;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetEnteredCreditMemosByJournalNumber;
		}

		public void DeleteCreditMemoDetailsByCreditMemoID(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (lngID > 0)
				{
					clsDRWrapper rsEx = new clsDRWrapper();
					rsEx.Execute("Delete from CreditMemoDetail where CreditMemoID = " + FCConvert.ToString(lngID), "Budgetary");
				}
				return;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		public void DeleteCreditMemo(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from CreditMemo where id = " + FCConvert.ToString(lngID), "Budgetary");
				return;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		public void DeleteCreditMemoDetail(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from CreditMemoDetail where id = " + FCConvert.ToString(lngID), "Budgetary");
				return;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}
		// Public Function GetFullCreditMemoByAPJournal(ByVal lngAPID As Long) As cGenericCollection
		// ClearErrors
		// On Error GoTo ErrorHandler
		// Dim rs As New clsDRWrapper
		// Dim credMemo As cCreditMemo
		// Dim collMemos As New cGenericCollection
		// Call rs.OpenRecordset("select * from creditmemo where OriginalAPRecord = " & lngAPID & " order by id", "Budgetary")
		// Do While Not rs.EndOfFile
		// Set credMemo = New cCreditMemo
		// Call FillCreditMemo(credMemo, rs)
		// Call FillCreditMemoDetails(credMemo.ID, credMemo.CreditMemoDetails)
		// Call collMemos.AddItem(credMemo)
		// rs.MoveNext
		// Loop
		// Set GetFullCreditMemosByAPJournal = collMemos
		// Exit Function
		// ErrorHandler:
		// lngLastError = Err.Number
		// strLastError = Err.Description
		// End Function
		public cCreditMemo GetFullCreditMemo(int lngID)
		{
			cCreditMemo GetFullCreditMemo = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				cCreditMemo credMemo = null;
				rs.OpenRecordset("select * from creditmemo where id = " + FCConvert.ToString(lngID), "Budgetary");
				if (!rs.EndOfFile())
				{
					credMemo = new cCreditMemo();
					FillCreditMemo(ref credMemo, ref rs);
					FillCreditMemoDetails_6(credMemo.ID, credMemo.CreditMemoDetails);
				}
				GetFullCreditMemo = credMemo;
				return GetFullCreditMemo;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetFullCreditMemo;
		}

		public cCreditMemo GetFullCreditMemoByAPJournalID(int lngID)
		{
			cCreditMemo GetFullCreditMemoByAPJournalID = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				cCreditMemo credMemo = null;
				rs.OpenRecordset("select * from creditmemo where originalaprecord = " + FCConvert.ToString(lngID), "Budgetary");
				if (!rs.EndOfFile())
				{
					credMemo = new cCreditMemo();
					FillCreditMemo(ref credMemo, ref rs);
					FillCreditMemoDetails_6(credMemo.ID, credMemo.CreditMemoDetails);
				}
				GetFullCreditMemoByAPJournalID = credMemo;
				return GetFullCreditMemoByAPJournalID;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetFullCreditMemoByAPJournalID;
		}

		private void FillCreditMemo(ref cCreditMemo credMemo, ref clsDRWrapper rsLoad)
		{
			if (!(credMemo == null) && !(rsLoad == null))
			{
				if (!rsLoad.EndOfFile())
				{
					credMemo.Adjustments = FCConvert.ToDouble(rsLoad.Get_Fields_Decimal("adjustments"));
					// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
					credMemo.Amount = Conversion.Val(rsLoad.Get_Fields("Amount"));
					if (Information.IsDate(rsLoad.Get_Fields("creditmemodate")))
					{
						credMemo.CreditMemoDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("CreditMemoDate"));
					}
					else
					{
						credMemo.CreditMemoDate = "";
					}
					credMemo.Description = rsLoad.Get_Fields_String("Description");
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					credMemo.Fund = rsLoad.Get_Fields_String("Fund");
					// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					credMemo.JournalNumber = rsLoad.Get_Fields("JournalNumber");
					credMemo.LiquidatedAmount = Conversion.Val(rsLoad.Get_Fields_Decimal("Liquidated"));
					credMemo.MemoNumber = rsLoad.Get_Fields_String("MemoNumber");
					credMemo.OriginalAPRecord = rsLoad.Get_Fields_Int32("OriginalAPRecord");
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					credMemo.Period = FCConvert.ToInt16(rsLoad.Get_Fields("Period"));
					if (Information.IsDate(rsLoad.Get_Fields("PostedDate")))
					{
						credMemo.PostedDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("PostedDate"));
					}
					else
					{
						credMemo.PostedDate = "";
					}
					credMemo.Status = rsLoad.Get_Fields_String("Status");
					credMemo.VendorNumber = rsLoad.Get_Fields_Int32("VendorNumber");
					credMemo.ID = rsLoad.Get_Fields_Int32("ID");
					credMemo.IsUpdated = false;
				}
			}
		}

		private void FillCreditMemoDetails_6(int lngID, cGenericCollection memoDetails)
		{
			FillCreditMemoDetails(lngID, ref memoDetails);
		}

		private void FillCreditMemoDetails(int lngID, ref cGenericCollection memoDetails)
		{
			if (lngID > 0 && !(memoDetails == null))
			{
				memoDetails.ClearList();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCreditMemoDetail mDetail;
				rsLoad.OpenRecordset("select * from CreditMemoDetail where creditmemoid = " + FCConvert.ToString(lngID), "Budgetary");
				while (!rsLoad.EndOfFile())
				{
					////////Application.DoEvents();
					mDetail = new cCreditMemoDetail();
					// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
					mDetail.Account = FCConvert.ToString(rsLoad.Get_Fields("account"));
					mDetail.Adjustments = Conversion.Val(rsLoad.Get_Fields_Decimal("Adjustments"));
					// TODO: Check the table for the column [amount] and replace with corresponding Get_Field method
					mDetail.Amount = Conversion.Val(rsLoad.Get_Fields("amount"));
					mDetail.CreditMemoID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("CreditMemoID"));
					mDetail.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					mDetail.Liquidated = Conversion.Val(rsLoad.Get_Fields_Decimal("Liquidated"));
					mDetail.Project = FCConvert.ToString(rsLoad.Get_Fields_String("Project"));
					// TODO: Check the table for the column [1099] and replace with corresponding Get_Field method
					mDetail.Ten99 = FCConvert.ToString(rsLoad.Get_Fields("1099"));
					memoDetails.AddItem(mDetail);
					rsLoad.MoveNext();
				}
			}
		}
	}
}
