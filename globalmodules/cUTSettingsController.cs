﻿namespace Global
{
    public class cUTSettingsController
    {
        public cUTSettings LoadSettings()
        {
            var utSet = LoadCustomize();
            if (modGlobalConstants.Statics.MuniName.ToLower() == "brunswick sewer district")
            {
                utSet.AllowAutoPay = true;
            }
            else
            {
                utSet.AllowAutoPay = false;
            }

            return utSet;
        }

        private cUTSettings LoadCustomize()
        {
            var rsLoad = new clsDRWrapper();
            var customizeSettings = new cUTSettings();
            rsLoad.OpenRecordset("Select * from UtilityBilling", "UtilityBilling");
            if (!rsLoad.EndOfFile())
            {
                if (rsLoad.Get_Fields_Int32("Basis") == 360)
                {
                    customizeSettings.UnitsInYear = 360;
                }
                else
                {
                    customizeSettings.UnitsInYear = 365;
                }

                if (rsLoad.Get_Fields_Boolean("PayWaterFirst"))
                {
                    customizeSettings.PayFirstService = "W";
                }
                else
                {
                    customizeSettings.PayFirstService = "S";
                }

                customizeSettings.TownServiceType = rsLoad.Get_Fields_String("Service");
                customizeSettings.AutoPayOption = rsLoad.Get_Fields_Int32("Autopayoption");
            }

            return customizeSettings;
        }
    }
}