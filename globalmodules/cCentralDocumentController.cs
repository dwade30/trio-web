﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation.VisualBasicLayer;
using fecherFoundation;
using System.IO;
using SharedApplication.CentralDocuments;

namespace Global
{
	public class cCentralDocumentController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		private void SetError(int lngError, string strError)
		{
			strLastError = strError;
			lngLastError = lngError;
		}

		public cCentralDocument GetCentralDocument(int lngID)
		{
			cCentralDocument GetCentralDocument = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCentralDocument cenDoc = new cCentralDocument();
				rsLoad.OpenRecordset("select * from [Documents] where id = " + FCConvert.ToString(lngID), "CentralDocuments");
				if (!rsLoad.EndOfFile())
				{
					cenDoc = new cCentralDocument();
					cenDoc.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					cenDoc.AltReference = FCConvert.ToString(rsLoad.Get_Fields_String("AltReference"));
					cenDoc.ItemDescription = FCConvert.ToString(rsLoad.Get_Fields_String("ItemDescription"));
					cenDoc.ItemName = FCConvert.ToString(rsLoad.Get_Fields_String("ItemName"));
					cenDoc.MediaType = FCConvert.ToString(rsLoad.Get_Fields_String("MediaType"));
					cenDoc.ReferenceID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ReferenceID"))));
					cenDoc.ReferenceType = FCConvert.ToString(rsLoad.Get_Fields_String("ReferenceType"));
					cenDoc.DataGroup = FCConvert.ToString(rsLoad.Get_Fields_String("ReferenceGroup"));
					cenDoc.ItemData = (byte[])rsLoad.Get_Fields_Binary("ItemData");
                    cenDoc.DocumentIdentifier = rsLoad.Get_Fields_Guid("DocumentIdentifier");
                    cenDoc.DateCreated = rsLoad.Get_Fields_DateTime("DateCreated");
					cenDoc.IsUpdated = false;
				}
				GetCentralDocument = cenDoc;
				return GetCentralDocument;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return GetCentralDocument;
		}

		public cCentralDocumentInfo GetCentralDocumentInfo(int lngID)
		{
			cCentralDocumentInfo GetCentralDocumentInfo = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCentralDocumentInfo cenDoc = new cCentralDocumentInfo();
				rsLoad.OpenRecordset("select * from [Documents] where id = " + FCConvert.ToString(lngID), "CentralDocuments");
				if (!rsLoad.EndOfFile())
				{
					cenDoc = new cCentralDocumentInfo();
					cenDoc.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					cenDoc.AltReference = FCConvert.ToString(rsLoad.Get_Fields_String("AltReference"));
					cenDoc.ItemDescription = FCConvert.ToString(rsLoad.Get_Fields_String("ItemDescription"));
					cenDoc.ItemName = FCConvert.ToString(rsLoad.Get_Fields_String("ItemName"));
					cenDoc.MediaType = FCConvert.ToString(rsLoad.Get_Fields_String("MediaType"));
					cenDoc.ReferenceID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ReferenceID"))));
					cenDoc.ReferenceType = FCConvert.ToString(rsLoad.Get_Fields_String("ReferenceType"));
					cenDoc.DataGroup = FCConvert.ToString(rsLoad.Get_Fields_String("ReferenceGroup"));
					cenDoc.IsUpdated = false;
				}
				GetCentralDocumentInfo = cenDoc;
				return GetCentralDocumentInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return GetCentralDocumentInfo;
		}

		public cGenericCollection GetCentralDocumentInfoesByReference(string DataGroup, ref string referenceType, int referenceId)
		{
			cGenericCollection GetCentralDocumentInfoesByReference = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cCentralDocumentInfo cenDoc;
				rsLoad.OpenRecordset($"select * from [Documents] where ReferenceGroup = '{DataGroup}' and ReferenceType = '{referenceType}' and ReferenceId = {FCConvert.ToString(referenceId)} order by ItemName", "CentralDocuments");
				while (!rsLoad.EndOfFile())
				{
					cenDoc = new cCentralDocumentInfo();
					cenDoc.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					cenDoc.AltReference = FCConvert.ToString(rsLoad.Get_Fields_String("AltReference"));
					cenDoc.ItemDescription = FCConvert.ToString(rsLoad.Get_Fields_String("ItemDescription"));
					cenDoc.ItemName = FCConvert.ToString(rsLoad.Get_Fields_String("ItemName"));
					cenDoc.MediaType = FCConvert.ToString(rsLoad.Get_Fields_String("MediaType"));
					cenDoc.ReferenceID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ReferenceID"))));
					cenDoc.ReferenceType = FCConvert.ToString(rsLoad.Get_Fields_String("ReferenceType"));
					cenDoc.DataGroup = FCConvert.ToString(rsLoad.Get_Fields_String("ReferenceGroup"));
					cenDoc.IsUpdated = false;
					gColl.AddItem(cenDoc);
					rsLoad.MoveNext();
				}
				GetCentralDocumentInfoesByReference = gColl;
				return GetCentralDocumentInfoesByReference;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return GetCentralDocumentInfoesByReference;
		}

		public cGenericCollection GetCentralDocumentInfoesByAltReference(string strDataGroup, string strReferenceType, string strAltReference)
		{
			cGenericCollection GetCentralDocumentInfoesByAltReference = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rs = new clsDRWrapper();
				cCentralDocumentInfo cenDoc;
				rs.OpenRecordset("select * from [Documents] where ReferenceGroup = '" + strDataGroup + "' and ReferenceType = '" + strReferenceType + "' and AltReference  = '" + strAltReference + "' order by ItemName", "CentralDocuments");
				while (!rs.EndOfFile())
				{
					cenDoc = new cCentralDocumentInfo();
					cenDoc.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					cenDoc.AltReference = FCConvert.ToString(rs.Get_Fields_String("AltReference"));
					cenDoc.ItemDescription = FCConvert.ToString(rs.Get_Fields_String("ItemDescription"));
					cenDoc.ItemName = FCConvert.ToString(rs.Get_Fields_String("ItemName"));
					cenDoc.MediaType = FCConvert.ToString(rs.Get_Fields_String("MediaType"));
					cenDoc.ReferenceID = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields_Int32("ReferenceID"))));
					cenDoc.ReferenceType = FCConvert.ToString(rs.Get_Fields_String("ReferenceType"));
					cenDoc.DataGroup = FCConvert.ToString(rs.Get_Fields_String("ReferenceGroup"));
					cenDoc.IsUpdated = false;
					gColl.AddItem(cenDoc);
					rs.MoveNext();
				}
				GetCentralDocumentInfoesByAltReference = gColl;
				return GetCentralDocumentInfoesByAltReference;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return GetCentralDocumentInfoesByAltReference;
		}

		public void DeleteCentralDocument(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from [Documents] where id = " + FCConvert.ToString(lngID), "CentralDocuments");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		public void DeleteCentralDocumentsByReference(string strDataGroup, string strReferenceType, int lngReferenceID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from [Documents] where ReferenceGroup = '" + strDataGroup + "' and ReferenceType = '" + strReferenceType + "' and ReferenceId = " + FCConvert.ToString(lngReferenceID), "CentralDocuments");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		public void DeleteCentralDocumentsByAltReference(string strDataGroup, string strReferenceType, string strAltReference)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from [Documents] where ReferenceGroup = '" + strDataGroup + "' and ReferenceType = '" + strReferenceType + "' and AltReference = '" + strAltReference + "'", "CentralDocuments");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		public void SaveCentralDocumentInfo(ref cCentralDocumentInfo cenDoc)
		{
			// creating an info (no binary data) is not legitimate
			// if id is 0 or record isn't found this is an error
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (cenDoc.IsDeleted)
				{
					DeleteCentralDocument(cenDoc.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				if (cenDoc.ID < 1)
				{
					SetError(9999, "Cannot create empty documents");
					return;
				}
				rsSave.OpenRecordset("select * from [documents] where id = " + cenDoc.ID, "CentralDocuments");
				if (!rsSave.EndOfFile())
				{
					UpdateCentralDocumentInfo(ref cenDoc);
				}
				else
				{
					SetError(9999, "Central document record not found");
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		public void SaveCentralDocument(ref cCentralDocument cenDoc)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (cenDoc.IsDeleted)
				{
					DeleteCentralDocument(cenDoc.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from [documents] where id = " + cenDoc.ID, "CentralDocuments");
				if (!rsSave.EndOfFile())
				{
					UpdateCentralDocument(ref cenDoc);
				}
				else
				{
					if (cenDoc.ID > 0)
					{
						SetError(9999, "Central document record not found");
						return;
					}
					InsertCentralDocument(ref cenDoc);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		private void UpdateCentralDocumentInfo(ref cCentralDocumentInfo cenDoc)
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.SetStoredProcedure("Update_DocumentInfo", "CentralDocuments");
				rsSave.Set_ParameterValue("@ID", cenDoc.ID);
				rsSave.Set_ParameterValue("@AltReference", cenDoc.AltReference);
				rsSave.Set_ParameterValue("@ItemDescription", cenDoc.ItemDescription);
				rsSave.Set_ParameterValue("@ItemName", cenDoc.ItemName);
				rsSave.Set_ParameterValue("@MediaType", cenDoc.MediaType);
				rsSave.Set_ParameterValue("@ReferenceId", cenDoc.ReferenceID);
				rsSave.Set_ParameterValue("@DataGroup", cenDoc.DataGroup);
				rsSave.Set_ParameterValue("@ReferenceType", cenDoc.ReferenceType);
				rsSave.ExecuteStoredProcedure(false);
				cenDoc.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		private void UpdateCentralDocument(ref cCentralDocument cenDoc)
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.SetStoredProcedure("Update_Document", "CentralDocuments");
				rsSave.Set_ParameterValue("@ID", cenDoc.ID);
				rsSave.Set_ParameterValue("@AltReference", cenDoc.AltReference);
				rsSave.Set_ParameterValue("@ItemDescription", cenDoc.ItemDescription);
				rsSave.Set_ParameterValue("@ItemName", cenDoc.ItemName);
				rsSave.Set_ParameterValue("@MediaType", cenDoc.MediaType);
				rsSave.Set_ParameterValue("@ReferenceId", cenDoc.ReferenceID);
				rsSave.Set_ParameterValue("@DataGroup", cenDoc.DataGroup);
				rsSave.Set_ParameterValue("@ReferenceType", cenDoc.ReferenceType);
				rsSave.Set_ParameterValue("@ItemData", cenDoc.ItemData);
				rsSave.ExecuteStoredProcedure(false);
				cenDoc.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}

		private void InsertCentralDocument(ref cCentralDocument cenDoc)
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.SetStoredProcedure("Insert_Document", "CentralDocuments");
				rsSave.Set_ParameterValue("@ID", 0);
				rsSave.Set_ParameterValue("@AltReference", cenDoc.AltReference);
				rsSave.Set_ParameterValue("@ItemDescription", cenDoc.ItemDescription);
				rsSave.Set_ParameterValue("@ItemName", cenDoc.ItemName);
				rsSave.Set_ParameterValue("@MediaType", cenDoc.MediaType);
				rsSave.Set_ParameterValue("@ReferenceId", cenDoc.ReferenceID);
				rsSave.Set_ParameterValue("@DataGroup", cenDoc.DataGroup);
				rsSave.Set_ParameterValue("@ReferenceType", cenDoc.ReferenceType);
				rsSave.Set_ParameterValue("@ItemData", cenDoc.ItemData);
                rsSave.Set_ParameterValue("@DocumentIdentifier",cenDoc.DocumentIdentifier);
                rsSave.Set_ParameterValue("@DateCreated",DateTime.Now);
				rsSave.ExecuteStoredProcedure(false);
				cenDoc.ID = FCConvert.ToInt32(rsSave.Get_ParameterValue("@ID"));
				cenDoc.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
		}
		// vbPorter upgrade warning: strItemDescription As Variant --> As string
		public cCentralDocument MakeCentralDocumentFromFile(string strFileName, string strItemDescription, string strReferenceType, int lngReferenceID, string strAltReference, string strDataGroup, byte[] ItemData = null)
		{
			cCentralDocument MakeCentralDocumentFromFile = null;
			try
			{
				// On Error GoTo ErrorHandler
				ClearErrors();
				cCentralDocument cenDoc = new cCentralDocument();
				//int intFileHandle = 0;
				//byte[] bytes = null;
				if (!string.IsNullOrEmpty(strFileName))
				{
					cenDoc = new cCentralDocument();
					string strTemp = "";
					strTemp = Path.GetFileNameWithoutExtension(strFileName);
					cenDoc.ItemName = strTemp;
					strTemp = Path.GetExtension(strFileName);
					//FC:FINAL:JEI remove leading dot
					strTemp = strTemp.TrimStart('.');
					if (Strings.LCase(strTemp) == "pdf")
					{
						cenDoc.MediaType = "application/pdf";
					}
					else if ((Strings.LCase(strTemp) == "jpg") || (Strings.LCase(strTemp) == "jpeg"))
					{
						cenDoc.MediaType = "image/jpeg";
					}
					else if (Strings.LCase(strTemp) == "bmp")
					{
						cenDoc.MediaType = "image/bmp";
					}
					else if (Strings.LCase(strTemp) == "png")
					{
						cenDoc.MediaType = "image/png";
					}
					else if (Strings.LCase(strTemp) == "gif")
					{
						cenDoc.MediaType = "image/gif";
					}
					else if ((Strings.LCase(strTemp) == "tif") || (Strings.LCase(strTemp) == "tiff"))
					{
						cenDoc.MediaType = "image/tif";
					}
					else
					{
						Information.Err().Raise(9999, null, "Unknown file extension", null, null);
					}
					//FC:FINAL:JEI new implementation
					//intFileHandle = FCFileSystem.FreeFile();
					//FCFileSystem.FileOpen(intFileHandle, strFileName, fecherFoundation.VisualBasicLayer.OpenMode.Binary, (fecherFoundation.VisualBasicLayer.OpenAccess)(-1), (fecherFoundation.VisualBasicLayer.OpenShare)(-1), -1);
					//if (FCFileSystem.LOF(FCConvert.ToInt16(intFileHandle)) > 0)
					//{
					//    bytes = new byte[FCFileSystem.LOF(FCConvert.ToInt16(intFileHandle)) - 1 + 1];
					//    FCFileSystem.FileGet(intFileHandle, ref bytes);
					//}
					//FCFileSystem.FileClose(intFileHandle);
					//cenDoc.ItemData = bytes;
					cenDoc.ItemData = ItemData;
					cenDoc.ItemDescription = strItemDescription;
					cenDoc.ReferenceType = strReferenceType;
					cenDoc.ReferenceID = lngReferenceID;
					cenDoc.AltReference = strAltReference;
					cenDoc.DataGroup = strDataGroup;
				}
				else
				{
					Information.Err().Raise(9999, null, "Document file not found", null, null);
				}
				MakeCentralDocumentFromFile = cenDoc;
				return MakeCentralDocumentFromFile;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(Information.Err(ex).Number, Information.Err(ex).Description);
			}
			return MakeCentralDocumentFromFile;
		}
	}
}
