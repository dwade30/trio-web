﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace Global
{
	public class modModDayCode
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/09/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/09/2003              *
		// ********************************************************
		public static bool CheckWinModCode(int lngCode, string strMod)
		{
			bool CheckWinModCode = false;
			// this function will return true if the code passed in matches
			// the code generated from the function ReturnWinModCode
			if (lngCode == ReturnWinModCode_234(strMod, modGlobalConstants.Statics.MuniName, DateTime.Today.Month, DateTime.Today.Day, DateTime.Today.Year))
			{
				CheckWinModCode = true;
			}
			else
			{
				CheckWinModCode = false;
			}
			return CheckWinModCode;
		}

		public static int ReturnWinModCode_234(string strMod, string strMuniName, int lngMo, int lngDay, int lngYear)
		{
			return ReturnWinModCode(ref strMod, ref strMuniName, ref lngMo, ref lngDay, ref lngYear);
		}

		public static int ReturnWinModCode(ref string strMod, ref string strMuniName, ref int lngMo, ref int lngDay, ref int lngYear)
		{
			int ReturnWinModCode = 0;
			// this will return todays code for the type and muniname passed in
			int lngTemp;
			int XORNumber;
			lngYear += 42;
			lngTemp = lngYear + Convert.ToByte(Strings.LCase(Strings.Left(strMuniName, 2))[0]);
			lngTemp *= 256;
			// shift 8 bits
			lngTemp += (lngMo * 17) + Convert.ToByte(Strings.LCase(Strings.Left(strMod, 1))[0]);
			lngTemp += (((lngDay + 2) + Convert.ToByte(Strings.UCase(Strings.Right(strMod, 1))[0])) * 512);
			XORNumber = 113;
			XORNumber = (XORNumber * 256) + 71;
			lngTemp = lngTemp ^ XORNumber;
			ReturnWinModCode = lngTemp;
			return ReturnWinModCode;
		}
	}
}
