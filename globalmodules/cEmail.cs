﻿namespace Global
{
	public class cEmail
	{
		//=========================================================
		private string strReplyTo = string.Empty;
		private string strFromAddress = string.Empty;
		private string strFromName = string.Empty;
		private bool boolReturnReceipt;
		private string strSubject = string.Empty;
		private string strBounceAddress = string.Empty;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collSendTos = new cGenericCollection();
		//private cGenericCollection collCCs = new cGenericCollection();
		//private cGenericCollection collAttachments = new cGenericCollection();
		private cGenericCollection collSendTos_AutoInitialized;

		private cGenericCollection collSendTos
		{
			get
			{
				if (collSendTos_AutoInitialized == null)
				{
					collSendTos_AutoInitialized = new cGenericCollection();
				}
				return collSendTos_AutoInitialized;
			}
			set
			{
				collSendTos_AutoInitialized = value;
			}
		}

		private cGenericCollection collCCs_AutoInitialized;

		private cGenericCollection collCCs
		{
			get
			{
				if (collCCs_AutoInitialized == null)
				{
					collCCs_AutoInitialized = new cGenericCollection();
				}
				return collCCs_AutoInitialized;
			}
			set
			{
				collCCs_AutoInitialized = value;
			}
		}

		private cGenericCollection collAttachments_AutoInitialized;

		private cGenericCollection collAttachments
		{
			get
			{
				if (collAttachments_AutoInitialized == null)
				{
					collAttachments_AutoInitialized = new cGenericCollection();
				}
				return collAttachments_AutoInitialized;
			}
			set
			{
				collAttachments_AutoInitialized = value;
			}
		}

		private string strPlainTextBody = string.Empty;
		private string strHTMLBody = string.Empty;
		private bool boolHighPriority;

		public bool IsHighPriority
		{
			set
			{
				boolHighPriority = value;
			}
			get
			{
				bool IsHighPriority = false;
				IsHighPriority = boolHighPriority;
				return IsHighPriority;
			}
		}

		public string ReplyToAddress
		{
			set
			{
				strReplyTo = value;
			}
			get
			{
				string ReplyToAddress = "";
				ReplyToAddress = strReplyTo;
				return ReplyToAddress;
			}
		}

		public string FromAddress
		{
			set
			{
				strFromAddress = value;
			}
			get
			{
				string FromAddress = "";
				FromAddress = strFromAddress;
				return FromAddress;
			}
		}

		public string FromName
		{
			set
			{
				strFromName = value;
			}
			get
			{
				string FromName = "";
				FromName = strFromName;
				return FromName;
			}
		}

		public bool ReturnReceipt
		{
			set
			{
				boolReturnReceipt = value;
			}
			get
			{
				bool ReturnReceipt = false;
				ReturnReceipt = boolReturnReceipt;
				return ReturnReceipt;
			}
		}

		public string Subject
		{
			set
			{
				strSubject = value;
			}
			get
			{
				string Subject = "";
				Subject = strSubject;
				return Subject;
			}
		}

		public string BounceAddress
		{
			set
			{
				strBounceAddress = value;
			}
			get
			{
				string BounceAddress = "";
				BounceAddress = strBounceAddress;
				return BounceAddress;
			}
		}

		public cGenericCollection SendTos
		{
			get
			{
				cGenericCollection SendTos = null;
				SendTos = collSendTos;
				return SendTos;
			}
		}

		public cGenericCollection CCs
		{
			get
			{
				cGenericCollection CCs = null;
				CCs = collCCs;
				return CCs;
			}
		}

		public cGenericCollection Attachments
		{
			get
			{
				return collAttachments;
			}
		}

		public string PlainTextBody
		{
			set
			{
				strPlainTextBody = value;
			}
			get
			{
				string PlainTextBody = "";
				PlainTextBody = strPlainTextBody;
				return PlainTextBody;
			}
		}

		public string HtmlBody
		{
			set
			{
				strHTMLBody = value;
			}
			get
			{
				string HtmlBody = "";
				HtmlBody = strHTMLBody;
				return HtmlBody;
			}
		}
	}
}
