﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmGroupSearch.
	/// </summary>
	public partial class frmGroupSearch : BaseForm
	{
		private System.ComponentModel.IContainer components;

		public frmGroupSearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGroupSearch InstancePtr
		{
			get
			{
				return (frmGroupSearch)Sys.GetInstance(typeof(frmGroupSearch));
			}
		}

		protected frmGroupSearch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		const int AccountCol = 0;
		const int NameCol = 1;
		const int LocMapCol = 2;
		const int GroupNoCol = 3;
		const int GroupIDCol = 4;

		public void SetupSearchGrid(string strLocMapName)
		{
			SearchGrid.TextMatrix(0, AccountCol, "Account");
			SearchGrid.TextMatrix(0, NameCol, "Name");
			SearchGrid.TextMatrix(0, LocMapCol, strLocMapName);
			SearchGrid.TextMatrix(0, GroupNoCol, "Group");
			SearchGrid.ColHidden(GroupIDCol, true);
            //FC:FINAL:AM:#3397 - set column alignment
            SearchGrid.ColAlignment(GroupNoCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
        }

		private void frmGroupSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmGroupSearch_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGroupSearch properties;
			//frmGroupSearch.ScaleWidth	= 9045;
			//frmGroupSearch.ScaleHeight	= 7215;
			//frmGroupSearch.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this);
		}

		private void frmGroupSearch_Resize(object sender, System.EventArgs e)
		{
			ResizeSearchGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void ResizeSearchGrid()
		{
			int GridWidth = 0;
			GridWidth = SearchGrid.WidthOriginal;
			SearchGrid.ColWidth(AccountCol, FCConvert.ToInt32(0.1 * GridWidth));
			SearchGrid.ColWidth(NameCol, FCConvert.ToInt32(0.4 * GridWidth));
			SearchGrid.ColWidth(LocMapCol, FCConvert.ToInt32(0.35 * GridWidth));
			SearchGrid.ColWidth(GroupNoCol, FCConvert.ToInt32(0.1 * GridWidth));
		}

		private void SearchGrid_DblClick(object sender, System.EventArgs e)
		{
			//FC:FINAL:DDU:#i1419 - call close before show
			int temp = FCConvert.ToInt32(SearchGrid.TextMatrix(SearchGrid.Row, GroupIDCol));
			//! Load frmGroup;
			Close();
			frmGroup.InstancePtr.Init(temp);
			// Unload frmGetGroup
		}
	}
}
