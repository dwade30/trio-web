﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cBLSecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cBLSecuritySetup() : base()
		{
			strThisModule = "BL";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem(" Enter Tax Billing", "", 1, false, ""));
			AddItem_2(CreateItem("Account Associations", "", 11, true, ""));
			AddItem_2(CreateItem("Account Associations", "Group Maintenance", 13, false, ""));
			AddItem_2(CreateItem("Account Associations", "Mortgage Maintenance", 12, false, ""));
			AddItem_2(CreateItem("Audit", "", 2, false, ""));
			AddItem_2(CreateItem("Billing Menu", "", 4, true, ""));
			AddItem_2(CreateItem("Billing Menu", "Out-Printing", 6, false, ""));
			AddItem_2(CreateItem("Billing Menu", "Print Bills", 5, false, ""));
			AddItem_2(CreateItem("Edit/Add Tax Rates", "", 21, false, ""));
			AddItem_2(CreateItem("File Maintenance", "", 14, true, ""));
			AddItem_2(CreateItem("File Maintenance", "Choose Bill Image", 18, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Customize Settings", 16, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Discount Percent", 17, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Distribution Percentages", 15, false, ""));
			AddItem_2(CreateItem("Group Maintenance", "", 13, true, ""));
			AddItem_2(CreateItem("Group Maintenance", "Edit/Add Groups", 19, false, ""));
			AddItem_2(CreateItem("Mortgage Maintenance", "", 12, true, ""));
			AddItem_2(CreateItem("Mortgage Maintenance", "Edit/Add Mortgage Holders", 20, false, ""));
			AddItem_2(CreateItem("Printing", "", 8, true, ""));
			AddItem_2(CreateItem("Printing", "Labels", 10, false, ""));
			AddItem_2(CreateItem("Printing", "Listings", 9, false, ""));
			AddItem_2(CreateItem("Supplemental Bills", "", 7, false, ""));
			AddItem_2(CreateItem("Transfer Billing Data", "", 3, false, ""));
		}
	}
}
