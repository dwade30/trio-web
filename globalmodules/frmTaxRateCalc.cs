﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;

#if TWBL0000
using TWBL0000;


#elif TWRE0000
using TWRE0000;
using modMain = TWRE0000.modGlobalRoutines;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmTaxRateCalc.
	/// </summary>
	public partial class frmTaxRateCalc : BaseForm
	{
		public frmTaxRateCalc()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTaxRateCalc InstancePtr
		{
			get
			{
				return (frmTaxRateCalc)Sys.GetInstance(typeof(frmTaxRateCalc));
			}
		}

		protected frmTaxRateCalc _InstancePtr = null;
		
		double lngREValuation;
		double lngPPValuation;
		double lngHomestead;
		// vbPorter upgrade warning: lngHalfHomestead As double	OnWrite(string)
		double lngHalfHomestead;
		double lngNetToBeRaised;
		double dblBETEExempt;
		// vbPorter upgrade warning: dblBETEReimValue As double	OnWrite(string, short, double)
		double dblBETEReimValue;
		double dblTaxRate;
		double dblMinTaxRate;
		double dblMaxTaxRate;
		bool boolPPEditable;
		double dblBETERate;
		private bool boolUseEnhancedBETE;
		private bool boolEnhancedBETEEligible;
		const int CNSTGRIDROWREVAL = 0;
		const int CNSTGRIDROWPPVAL = 1;
		const int CNSTGRIDROWTOTTAXVAL = 2;
		const int CNSTGRIDROWTOTHOMESTEAD = 3;
		const int CNSTGRIDROWHALFTOTHOMESTEAD = 4;
		const int CNSTGRIDROWTOTBETE = 5;
		const int CNSTGRIDROWBETEReimValue = 6;
		const int CNSTGRIDROWTOTVALUATIONBASE = 7;
		/// <summary>
		/// Private Const CNSTGRIDROWFISCALYEAR = 7
		/// </summary>
		const int CNSTGRIDROWCOUNTYTAX = 8;
		const int CNSTGRIDROWMUNIAPPROPRIATION = 9;
		const int CNSTGRIDROWTIF = 10;
		const int CNSTGRIDROWEDUCATION = 11;
		const int CNSTGRIDROWTOTAPPROPRIATIONS = 12;
		const int CNSTGRIDROWSTATEREVENUESHARING = 13;
		const int CNSTGRIDROWOTHERREVENUE = 14;
		const int CNSTGRIDROWTOTALDEDUCTIONS = 15;
		const int CNSTGRIDROWNETRAISED = 16;
		const int CNSTEBGRIDRowTotBETE = 0;
		const int CNSTEBGridRowTotBETENonTif = 1;
		const int CNSTEBGridRowBETEPercent = 2;
		const int CNSTEBGridRowBETESubjectStandardReim = 3;
		const int CNSTEBGridRowTotPP = 4;
		const int CNSTEBGridRowTotTaxableREPP = 5;
		const int CNSTEBGridRowTotEnhancedNonTif = 6;
		const int CNSTEBGridRowPPFactor = 7;
		const int CNSTEBGridRowPPFactorDiv2 = 8;
		const int CNSTEBGridRowPPFactorDive2Plus50 = 9;
		const int CNSTEBGridRowTotBeteNonStandardNonTif = 10;
		const int CNSTEBGridRowEnteredTifPercent = 11;
		const int CNSTEBGridRowTifPercent = 12;
		const int CNSTEBGridRowTotBeteTif = 13;
		const int CNSTEBGridRowTotBETETifReim = 14;
		const int CNSTEBGridRowTotBeteReim = 15;
		private bool boolDataChanged;

		private double RecalcEnhancedBETE()
		{
			double RecalcEnhancedBETE = 0;
			double dblTotBETENonTif;
			// vbPorter upgrade warning: dblTotBETETifReim As double	OnWrite(short, string)
			double dblTotBETETifReim;
			double dblTotBETETifVal = 0;
			// vbPorter upgrade warning: dblBETEStandardReim As double	OnWrite(short, string)
			double dblBETEStandardReim;
			double dblTotPP;
			double dblTotREPPTaxableVal;
			double dblTotNonTifEnhanced;
			double dblTifPerc;
			double dblTifActualPerc;
			double dblPPFactor;
			double dblPPFactDiv2;
			double dblPPFactDiv2Plus50 = 0;
			double dblTotBETEEnhancedReim;
			bool boolTest = false;
			dblTifPerc = 1;
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowEnteredTifPercent, 2)) > 0)
			{
				dblTifPerc = FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowEnteredTifPercent, 2).Replace(" ", ""));
			}
			else
			{
				dblTifPerc = 0;
			}
			if (dblTifPerc < 0)
				dblTifPerc = 0;
			dblTifPerc /= 100;
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteTif, 2)) > 0)
			{
				dblTotBETETifVal = FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteTif, 2).Replace(" ", ""));
			}
			else
			{
				dblTotBETETifVal = 0;
			}
			if (boolTest)
			{
				lngREValuation = 387769326;
				lngPPValuation = 495258868;
				dblBETERate = 0.5;
				dblTifPerc = 0.9;
				dblBETEExempt = 89854026;
				dblTotBETETifVal = 250000;
			}
			dblTotREPPTaxableVal = lngREValuation + lngPPValuation;
			dblTotPP = lngPPValuation + dblBETEExempt;
			dblTifActualPerc = dblTifPerc;
			if (dblBETERate > dblTifPerc)
			{
				dblTifActualPerc = dblBETERate;
			}
			// if enhanced percent is greater than = tif percent
			dblTotBETENonTif = dblBETEExempt;
			dblBETEStandardReim = 0;
			dblTotBETETifReim = 0;
			dblTotNonTifEnhanced = dblTotBETENonTif;
			dblPPFactor = dblTotPP / (dblTotNonTifEnhanced + dblTotREPPTaxableVal);
			dblPPFactDiv2 = dblPPFactor / 2;
			// dblPPFactor = Format(dblPPFactor, "0.0000")
			// dblPPFactDiv2 = Format(dblPPFactor / 2, "0.0000")
			// round after so to agree with the excel sheet from the state
			if (dblPPFactor > 0.05)
			{
				dblPPFactDiv2Plus50 = dblPPFactDiv2 + 0.5;
			}
			else
			{
				dblPPFactDiv2Plus50 = 0;
			}
			dblTotBETEEnhancedReim = FCConvert.ToDouble(Strings.Format(dblPPFactDiv2Plus50 * dblTotNonTifEnhanced, "0"));
			// enhanced perc is < tif percent
			if (dblPPFactDiv2Plus50 < dblTifPerc)
			{
				dblTotBETENonTif = dblBETEExempt - dblTotBETETifVal;
				if (dblTotBETENonTif < 0)
					dblTotBETENonTif = 0;
				dblTotNonTifEnhanced = dblTotBETENonTif;
				dblTotBETETifReim = FCConvert.ToDouble(Strings.Format(dblTifActualPerc * dblTotBETETifVal, "0.00"));
				dblTotBETEEnhancedReim = FCConvert.ToDouble(Strings.Format(dblPPFactDiv2Plus50 * dblTotNonTifEnhanced, "0"));
			}
			dblPPFactor = FCConvert.ToDouble(Strings.Format(dblPPFactor, "0.0000"));
			dblPPFactDiv2 = FCConvert.ToDouble(Strings.Format(dblPPFactDiv2, "0.0000"));
			dblPPFactDiv2Plus50 = FCConvert.ToDouble(Strings.Format(dblPPFactDiv2Plus50, "0.0000"));
			if (dblPPFactDiv2Plus50 < dblBETERate)
			{
				dblBETEStandardReim = FCConvert.ToDouble(Strings.Format(dblBETERate * dblTotBETENonTif, "0.00"));
				dblTotBETEEnhancedReim = 0;
			}
			GridEnhancedBETE.TextMatrix(CNSTEBGRIDRowTotBETE, 2, Strings.Format(dblBETEExempt, "#,###,###,##0"));
			GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBETENonTif, 2, Strings.Format(dblTotBETENonTif, "#,###,###,##0"));
			GridEnhancedBETE.TextMatrix(CNSTEBGridRowBETEPercent, 2, Strings.Format(dblBETERate * 100, "0.00"));
			GridEnhancedBETE.TextMatrix(CNSTEBGridRowBETESubjectStandardReim, 2, Strings.Format(dblBETEStandardReim, "#,###,###,##0"));
			GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotPP, 2, Strings.Format(dblTotPP, "#,###,###,##0"));
			GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotTaxableREPP, 2, Strings.Format(dblTotREPPTaxableVal, "#,###,###,##0"));
			GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotEnhancedNonTif, 2, Strings.Format(dblTotBETENonTif, "#,###,###,##0"));
			GridEnhancedBETE.TextMatrix(CNSTEBGridRowPPFactor, 2, Strings.Format(dblPPFactor * 100, "0.00"));
			GridEnhancedBETE.TextMatrix(CNSTEBGridRowPPFactorDiv2, 2, Strings.Format(dblPPFactDiv2 * 100, "0.00"));
			GridEnhancedBETE.TextMatrix(CNSTEBGridRowPPFactorDive2Plus50, 2, Strings.Format(dblPPFactDiv2Plus50 * 100, "0.00"));
			GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteNonStandardNonTif, 2, Strings.Format(dblTotBETEEnhancedReim, "#,###,###,##0"));
			GridEnhancedBETE.TextMatrix(CNSTEBGridRowTifPercent, 2, Strings.Format(dblTifActualPerc * 100, "0.00"));
			GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteTif, 2, Strings.Format(dblTotBETETifVal, "#,###,###,##0"));
			GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBETETifReim, 2, Strings.Format(dblTotBETETifReim, "#,###,###,##0"));
			double dblTotBETECalcReim;
			dblTotBETECalcReim = dblTotBETEEnhancedReim + dblTotBETETifReim + dblBETEStandardReim;
			GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteReim, 2, Strings.Format(dblTotBETECalcReim, "#,###,###,##0"));
			RecalcEnhancedBETE = dblTotBETECalcReim;
			return RecalcEnhancedBETE;
		}

		public double BETEReimbursementRate
		{
			get
			{
				double BETEReimbursementRate = 0;
				BETEReimbursementRate = dblBETERate;
				return BETEReimbursementRate;
			}
		}

		private void chkUseEB_CheckedChanged(object sender, System.EventArgs e)
		{
			// If chkUseEB.Value = vbChecked Then
			// cmdShowEB.Visible = True
			// Else
			// cmdShowEB.Visible = False
			// End If
		}

		private void chkUseEB_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// If chkUseEB.Value = vbChecked Then
			// cmdEBOK.Visible = True
			// Else
			// cmdEBOK.Visible = False
			// End If
		}

		private void cmdEBOK_Click(object sender, System.EventArgs e)
		{
			framEnhancedBETE.Visible = false;
			ShowMainFormControls(true);
		}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			//FileSystemObject fso = new FileSystemObject();
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strWhere = "";
			string strTemp = "";
			string strType = "";
			string strStart = "";
			string strEnd = "";
			// vbPorter upgrade warning: dtPPDate As DateTime	OnWriteFCConvert.ToInt16(
			DateTime dtPPDate;
			// vbPorter upgrade warning: dtBLDate As DateTime	OnWriteFCConvert.ToInt16(
			DateTime dtBLDate;
			string strSQL = "";
			double dblHsteadReimburseRate = 0.7;
			try
			{
				// On Error GoTo ErrorHandler
				if (cmbRange.SelectedIndex == 0)
				{
					strWhere = " where rsaccount > 0 and (not rsdeleted = 1)";
				}
				else
				{
					strStart = Strings.Trim(txtStart.Text);
					if (strStart == string.Empty)
					{
						MessageBox.Show("You must enter a starting range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					strEnd = Strings.Trim(txtEnd.Text);
					if (strEnd == string.Empty)
					{
						MessageBox.Show("You must enter an ending range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (cmbType.SelectedIndex == 0)
					{
						strType = "Rsaccount";
					}
					else if (cmbType.SelectedIndex == 1)
					{
						strType = "rsname";
						strStart = "'" + strStart + "'";
						strEnd = "'" + strEnd + "zzz'";
					}
					else if (cmbType.SelectedIndex == 2)
					{
						strType = "rsmaplot";
						strStart = "'" + strStart + "'";
						strEnd = "'" + strEnd + "'";
					}
					else if (cmbType.SelectedIndex == 3)
					{
						strType = "rslocstreet";
						strStart = "'" + strStart + "'";
						strEnd = "'" + strEnd + "zzz'";
					}
					else if (cmbType.SelectedIndex == 4)
					{
						strType = "ritrancode";
					}
					else if (cmbType.SelectedIndex == 5)
					{
						strType = "rilandcode";
					}
					else if (cmbType.SelectedIndex == 6)
					{
						strType = "ribldgcode";
					}
					#if ModuleID_1
										                    if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                    {
                        strWhere = " where rsaccount > 0 and (not rsdeleted = 1)  and " + strType + " between " + strStart + " and " + strEnd;
                    }
                    else
                    {
                        strWhere = " where rsaccount > 0 and (not rsdeleted = 1)  and cast(ritrancode as int) = " + FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))) + " and " + strType + " between " + strStart + " and " + strEnd;
                    }

#else
					if (!modRegionalTown.IsRegionalTown())
					{
						strWhere = " where rsaccount > 0 and (not rsdeleted = 1)  and " + strType + " between " + strStart + " and " + strEnd;
					}
					else
					{
						strWhere = " where rsaccount > 0 and (not rsdeleted = 1)  and cast(ritrancode as int) = " + FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))) + " and " + strType + " between " + strStart + " and " + strEnd;
					}
					#endif
				}
				// If File.Exists(strREDatabase) Then
				if (clsLoad.DBExists("RealEstate"))
				{
					if (cmbSource.SelectedIndex == 0)
					{
						clsLoad.OpenRecordset("select sum(cast(lastlandval as bigint)) as landsum,sum(cast(lastbldgval as bigint)) as bldgsum,sum(cast(rlexemption as bigint)) as exemptsum from master " + strWhere, modGlobalVariables.strREDatabase);
						lngREValuation = Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")) + Conversion.Val(clsLoad.Get_Fields("landsum"));
						clsLoad.OpenRecordset("select sum(cast(homesteadvalue as bigint)) as homesteadsum from master " + strWhere + " and rscard = 1", modGlobalVariables.strREDatabase);
						// TODO: Field [homesteadsum] not found!! (maybe it is an alias?)
						lngHomestead = Conversion.Val(clsLoad.Get_Fields("homesteadsum"));
					}
					else
					{
						clsLoad.OpenRecordset("select sum(cast(rllandval as bigint)) as landsum,sum(cast(rlbldgval as bigint)) as bldgsum,sum(cast(CORREXEMPTION as bigint)) AS exemptsum from master " + strWhere, modGlobalVariables.strREDatabase);
						// TODO: Field [bldgsum] not found!! (maybe it is an alias?)
						// TODO: Field [exemptsum] not found!! (maybe it is an alias?)
						// TODO: Field [landsum] not found!! (maybe it is an alias?)
						lngREValuation = Conversion.Val(clsLoad.Get_Fields("bldgsum")) - Conversion.Val(clsLoad.Get_Fields("exemptsum")) + Conversion.Val(clsLoad.Get_Fields("landsum"));
						clsLoad.OpenRecordset("select sum(cast(currhomesteadvalue as bigint)) as homesteadsum from master " + strWhere + " and rscard = 1", modGlobalVariables.strREDatabase);
						// TODO: Field [homesteadsum] not found!! (maybe it is an alias?)
						lngHomestead = Conversion.Val(clsLoad.Get_Fields("homesteadsum"));
					}
				}
				else
				{
					lngREValuation = 0;
				}
				lngHalfHomestead = FCConvert.ToDouble(Strings.Format(lngHomestead * dblHsteadReimburseRate, "0"));
				if (clsLoad.DBExists("PersonalProperty"))
				{
					// If File.Exists(strPPDatabase) Then
					clsLoad.OpenRecordset("select * from modules", "SystemSettings");
					dtPPDate = DateTime.FromOADate(0);
					dtBLDate = DateTime.FromOADate(0);
					if (Information.IsDate(clsLoad.Get_Fields("ppdate")))
					{
						dtPPDate = (DateTime)clsLoad.Get_Fields_DateTime("ppdate");
					}
					if (Information.IsDate(clsLoad.Get_Fields("BLDate")))
					{
						dtBLDate = (DateTime)clsLoad.Get_Fields_DateTime("bldate");
					}
					#if ModuleID_1
										                    if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                    {
                        strWhere = " and cast(trancode as int) = " + FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0)));
                        strSQL = "select * from townspecific where townnumber = " + FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0)));
                    }
                    else
                    {
                        strWhere = "";
                        strSQL = "select * from customize";
                    }

#else
					if (modRegionalTown.IsRegionalTown())
					{
						strWhere = " and cast(trancode as int) = " + FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0)));
						strSQL = "select * from townspecific where townnumber = " + FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0)));
					}
					else
					{
						strWhere = "";
						strSQL = "select * from customize";
					}
					#endif
					clsLoad.OpenRecordset(strSQL, modGlobalVariables.strPPDatabase);
					if (!clsLoad.EndOfFile())
					{
						dblBETERate = Conversion.Val(clsLoad.Get_Fields_Double("betereimburserate"));
						//if (dblBETERate == 0.6)
						//{
						//	MessageBox.Show("The BETE reimbursement rate is set at 60%." + "\r\n" + "For most municipalities it will be 50% this year." + "\r\n" + "If your rate is incorrect please go to Personal Property>File Maintenance>Customize and change it before running the tax rate calculator.", "BETE Rate Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						//}
					}
					else
					{
						dblBETERate = 0.5;
					}
					if (DateAndTime.DateDiff("d", DateTime.Today, dtPPDate) < 0)
					{
						if (DateAndTime.DateDiff("d", DateTime.Today, dtBLDate) < 0)
						{
							lngPPValuation = 0;
							dblBETEExempt = 0;
						}
						else
						{
							clsLoad.OpenRecordset("select sum(cast(value as bigint)) as valsum,sum(cast(exemption as bigint)) as exemptsum, sum(cast(cat1exempt + cat2exempt + cat3exempt + cat4exempt + cat5exempt + cat6exempt + cat7exempt + cat8exempt + cat9exempt as bigint)) as betesum from ppmaster INNER join ppvaluations on (ppmaster.account = ppvaluations.valuekey) where account > 0 and not deleted = 1 " + strWhere, modGlobalVariables.strPPDatabase);
							// TODO: Field [valSUM] not found!! (maybe it is an alias?)
							// TODO: Field [exemptSUM] not found!! (maybe it is an alias?)
							lngPPValuation = Conversion.Val(clsLoad.Get_Fields("valSUM")) - Conversion.Val(clsLoad.Get_Fields("exemptSUM"));
							// TODO: Field [betesum] not found!! (maybe it is an alias?)
							dblBETEExempt = Conversion.Val(clsLoad.Get_Fields("betesum"));
						}
					}
					else
					{
						clsLoad.OpenRecordset("select sum(cast(value as bigint)) as valsum,sum(cast(exemption as bigint)) as exemptsum, sum(cast(cat1exempt + cat2exempt + cat3exempt + cat4exempt + cat5exempt + cat6exempt + cat7exempt + cat8exempt + cat9exempt as bigint)) as betesum from ppmaster INNER join ppvaluations on (ppmaster.account = ppvaluations.valuekey) where account > 0 and not deleted = 1 " + strWhere, modGlobalVariables.strPPDatabase);
						// TODO: Field [valSUM] not found!! (maybe it is an alias?)
						// TODO: Field [exemptSUM] not found!! (maybe it is an alias?)
						lngPPValuation = Conversion.Val(clsLoad.Get_Fields("valSUM")) - Conversion.Val(clsLoad.Get_Fields("exemptSUM"));
						// TODO: Field [betesum] not found!! (maybe it is an alias?)
						dblBETEExempt = Conversion.Val(clsLoad.Get_Fields("betesum"));
					}
					dblBETEReimValue = FCConvert.ToDouble(Strings.Format(dblBETEExempt * dblBETERate, "0"));
					boolPPEditable = true;
				}
				else
				{
					lngPPValuation = 0;
					dblBETEExempt = 0;
					dblBETEReimValue = 0;
					dblBETERate = 1;
					boolPPEditable = true;
				}
				GridForm1.TextMatrix(CNSTGRIDROWREVAL, 2, Strings.Format(lngREValuation, "#,###,###,##0"));
				GridForm1.TextMatrix(CNSTGRIDROWPPVAL, 2, Strings.Format(lngPPValuation, "#,###,###,##0"));
				GridForm1.TextMatrix(CNSTGRIDROWTOTBETE, 2, Strings.Format(dblBETEExempt, "#,###,###,##0"));
				GridForm1.TextMatrix(CNSTGRIDROWBETEReimValue, 2, Strings.Format(dblBETEReimValue, "#,###,###,##0"));
				GridForm1.TextMatrix(CNSTGRIDROWTOTTAXVAL, 2, Strings.Format(lngREValuation + lngPPValuation, "#,###,###,##0"));
				GridForm1.TextMatrix(CNSTGRIDROWTOTHOMESTEAD, 2, Strings.Format(lngHomestead, "#,###,###,##0"));
				GridForm1.TextMatrix(CNSTGRIDROWHALFTOTHOMESTEAD, 2, Strings.Format(lngHalfHomestead, "#,###,###,##0"));
				GridForm1.TextMatrix(CNSTGRIDROWTOTVALUATIONBASE, 2, Strings.Format(lngREValuation + lngPPValuation + lngHalfHomestead + dblBETEExempt, "#,###,###,##0"));
				// GridForm1.TextMatrix(CNSTGRIDROWFISCALYEAR, 1) = "Municipal Fiscal Year   " & t2kBegin.Text & "  to  " & t2kEnd.Text
				int intCurTown = 0;
				if (Information.IsDate(t2kBegin.Text) && Information.IsDate(t2kEnd.Text))
				{
					#if ModuleID_1
										                    if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                    {
                        gridTownCode.TextMatrix(0, 0, modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
                        intCurTown = modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber;
                    }

#else
					if (modRegionalTown.IsRegionalTown())
					{
						gridTownCode.TextMatrix(0, 0, FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
						intCurTown = modGlobalVariables.Statics.CustomizedInfo.intCurrentTown;
					}
					#endif
					clsLoad.OpenRecordset("select * from taxratecalculation where isnull(townnumber,0) = " + FCConvert.ToString(intCurTown), modGlobalVariables.strREDatabase);
					if (!clsLoad.EndOfFile())
					{
						clsLoad.Edit();
						clsLoad.Set_Fields("townnumber", intCurTown);
					}
					else
					{
						clsLoad.AddNew();
					}
					clsLoad.Set_Fields("fiscalstartdate", Strings.Format(t2kBegin.Text, "MM/dd/yyyy"));
					clsLoad.Set_Fields("fiscalenddate", Strings.Format(t2kEnd.Text, "MM/dd/yyyy"));
					clsLoad.Update();
				}
				ReCalc();
				framInit.Visible = false;
				ShowMainFormControls(true);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In cmdOK_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowMainFormControls(bool visible)
		{
			chkUseEB.Visible = visible;
			cmdShowEB.Visible = visible;
			GridForm1.Visible = visible;
			GridForm2.Visible = visible;
			cmdSave.Visible = visible;
			cmdLoad.Visible = visible;
			cmdPrint.Visible = visible;
			cmdPrintBETEWorksheet.Visible = visible;
			cmdPrintCertification.Visible = visible;
		}

		private void cmdShowEB_Click(object sender, System.EventArgs e)
		{
			framEnhancedBETE.Visible = true;
			ShowMainFormControls(false);
		}

		private void frmTaxRateCalc_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmTaxRateCalc_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTaxRateCalc properties;
			//frmTaxRateCalc.ScaleWidth	= 9225;
			//frmTaxRateCalc.ScaleHeight	= 7770;
			//frmTaxRateCalc.LinkTopic	= "Form1";
			//End Unmaped Properties
			clsDRWrapper clsLoad = new clsDRWrapper();
			int intMonth = 0;
			int intDay = 0;
			int intCurTown = 0;
			try
			{
                // On Error GoTo ErrorHandler
                GridForm1.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
                GridForm2.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignRightCenter);
                SetupGridTownCode();
				#if ModuleID_1
								                if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    gridTownCode.TextMatrix(0, 0, modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
                    intCurTown = modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber;
                }

#else
				if (modRegionalTown.IsRegionalTown())
				{
					gridTownCode.TextMatrix(0, 0, FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
					intCurTown = modGlobalVariables.Statics.CustomizedInfo.intCurrentTown;
				}
				#endif
				iLabel1:
				clsLoad.OpenRecordset("select * from taxratecalculation where isnull(townnumber,0) = " + FCConvert.ToString(intCurTown), modGlobalVariables.strREDatabase);
				iLabel2:
				if (!clsLoad.EndOfFile())
				{
					iLabel3:
					if (Information.IsDate(clsLoad.Get_Fields("Fiscalstartdate")))
					{
						iLabel4:
						intMonth = ((DateTime)clsLoad.Get_Fields_DateTime("fiscalstartdate")).Month;
						iLabel5:
						intDay = ((DateTime)clsLoad.Get_Fields_DateTime("fiscalstartdate")).Day;
						iLabel6:
						if (intMonth == 1)
						{
							iLabel7:
							if (((DateTime)clsLoad.Get_Fields_DateTime("fiscalstartdate")).Year > DateTime.Today.Year)
							{
								iLabel8:
								t2kBegin.Text = Strings.Format(clsLoad.Get_Fields_DateTime("Fiscalstartdate"), "MM/dd/yyyy");
							}
							else
							{
								iLabel10:
								t2kBegin.Text = Strings.Format(FCConvert.ToString(intMonth) + "/" + FCConvert.ToString(intDay) + "/" + FCConvert.ToString(DateTime.Today.Year), "MM/dd/yyyy");
							}
						}
						else
						{
							iLabel11:
							if (((DateTime)clsLoad.Get_Fields_DateTime("fiscalstartdate")).Year > (DateTime.Today.Year - 1))
							{
								iLabel12:
								t2kBegin.Text = Strings.Format(clsLoad.Get_Fields_DateTime("fiscalstartdate"), "MM/dd/yyyy");
							}
							else
							{
								iLabel13:
								t2kBegin.Text = Strings.Format(FCConvert.ToString(intMonth) + "/" + FCConvert.ToString(intDay) + "/" + FCConvert.ToString(DateTime.Today.Year - 1), "MM/dd/yyyy");
							}
						}
					}
					iLabel14:
					if (Information.IsDate(clsLoad.Get_Fields("fiscalenddate")))
					{
						iLabel15:
						intMonth = ((DateTime)clsLoad.Get_Fields_DateTime("fiscalenddate")).Month;
						iLabel16:
						intDay = ((DateTime)clsLoad.Get_Fields_DateTime("fiscalenddate")).Day;
						iLabel17:
						if (((DateTime)clsLoad.Get_Fields_DateTime("fiscalenddate")).Year > (DateTime.Today.Year))
						{
							iLabel18:
							t2kEnd.Text = Strings.Format(clsLoad.Get_Fields_DateTime("fiscalenddate"), "MM/dd/yyyy");
						}
						else
						{
							iLabel19:
							t2kEnd.Text = Strings.Format(FCConvert.ToString(intMonth) + "/" + FCConvert.ToString(intDay) + "/" + FCConvert.ToString(DateTime.Today.Year), "MM/dd/yyyy");
						}
					}
					if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("useenhancedbete")))
					{
						chkUseEB.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkUseEB.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				iLabel20:
				boolPPEditable = false;
				iLabel21:
				if (!modGlobalConstants.Statics.gboolUse256Colors)
				{
					//iLabel22: this.BackColor = ColorTranslator.FromOle(Information.RGB(190, 190, 190));
				}
				iLabel23:
				SetupGrids();
				iLabel24:
				modGlobalFunctions.SetFixedSize(this);
				#if ModuleID_1
								                if (modREMain.Statics.boolShortRealEstate)
                {
                    if (cmbSource.Items.Contains("Current Values"))
                    {
                        cmbSource.Items.Remove("Current Values");
                    }
                }

#else
				if (cmbSource.Items.Contains("Current Values"))
				{
					cmbSource.Items.Remove("Current Values");
				}
				#endif
				//FC:FINAL:MSH - i.issue #1073: invoke handler for fields initializing
				optRange_CheckedChanged(cmbRange, new EventArgs());
				//FC:FINAL:DDU:#i1073 - align framinit in center
				framInit.Left = FCConvert.ToInt32((this.Width - framInit.Width) / 2.0F);
                GridForm1.RowHeight(-1, 350);
                GridForm2.RowHeight(-1, 350);

                return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In form_Load line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmTaxRateCalc_Resize(object sender, System.EventArgs e)
		{
			ResizeGrids();
			ResizeGridTownCode();
		}

		private void GridEnhancedBETE_RowColChange(object sender, System.EventArgs e)
		{
			GridEnhancedBETE.Editable = FCGrid.EditableSettings.flexEDNone;
			GridEnhancedBETE.Col = 2;
			switch (GridEnhancedBETE.Row)
			{
				case CNSTEBGridRowEnteredTifPercent:
					{
						GridEnhancedBETE.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						GridEnhancedBETE.EditCell();
						break;
					}
				case CNSTEBGridRowTotBeteTif:
					{
						GridEnhancedBETE.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						GridEnhancedBETE.EditCell();
						break;
					}
				default:
					{
						GridEnhancedBETE.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void GridEnhancedBETE_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = GridEnhancedBETE.GetFlexRowIndex(e.RowIndex);
			int col = GridEnhancedBETE.GetFlexColIndex(e.ColumnIndex);
			if (row == CNSTEBGridRowEnteredTifPercent)
			{
				GridEnhancedBETE.EditText = GridEnhancedBETE.EditText.Replace(" ", "");
				GridEnhancedBETE.EditText = Strings.Format(GridEnhancedBETE.EditText, "#,###,###,##0.00");
			}
			else if (row == CNSTEBGridRowTotBeteTif)
			{
				GridEnhancedBETE.EditText = GridEnhancedBETE.EditText.Replace(" ", "");
				GridEnhancedBETE.EditText = Strings.Format(GridEnhancedBETE.EditText, "#,###,###,##0");
				// GridEnhancedBETE.TextMatrix(Row, Col) = GridEnhancedBETE.EditText
			}
			GridEnhancedBETE.TextMatrix(row, col, GridEnhancedBETE.EditText);
			boolDataChanged = true;
			ReCalc();
		}

		private void GridForm1_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				GridForm1.Editable = FCGrid.EditableSettings.flexEDNone;
				GridForm1.Col = 2;
				switch (GridForm1.Row)
				{
					case CNSTGRIDROWPPVAL:
					case CNSTGRIDROWTOTBETE:
						{
							if (boolPPEditable)
							{
								GridForm1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								GridForm1.EditCell();
							}
							break;
						}
					case CNSTGRIDROWCOUNTYTAX:
					case CNSTGRIDROWMUNIAPPROPRIATION:
					case CNSTGRIDROWTIF:
					case CNSTGRIDROWEDUCATION:
					case CNSTGRIDROWSTATEREVENUESHARING:
					case CNSTGRIDROWOTHERREVENUE:
						{
							GridForm1.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							GridForm1.EditCell();
							break;
						}
					default:
						{
							GridForm1.Row = CNSTGRIDROWCOUNTYTAX;
							GridForm1.EditCell();
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GridForm1_RowColChange", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GridForm1_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = GridForm1.GetFlexRowIndex(e.RowIndex);
			int col = GridForm1.GetFlexColIndex(e.ColumnIndex);
			if (row != CNSTGRIDROWPPVAL && row != CNSTGRIDROWTOTBETE)
			{
				GridForm1.EditText = GridForm1.EditText.Replace(" ", "");
				GridForm1.EditText = Strings.Format(GridForm1.EditText, "#,###,###,##0.00");
			}
			else
			{
				GridForm1.EditText = GridForm1.EditText.Replace(" ", "");
				GridForm1.EditText = Strings.Format(GridForm1.EditText, "#,###,###,##0");
			}
			GridForm1.TextMatrix(row, col, GridForm1.EditText);
			boolDataChanged = true;
			ReCalc();
		}

		private void GridForm2_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				GridForm2.Row = 3;
				GridForm2.Col = 3;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GridForm2_RowColChange", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GridForm2_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = GridForm2.GetFlexRowIndex(e.RowIndex);
				int col = GridForm2.GetFlexColIndex(e.ColumnIndex);
				if (Conversion.Val(GridForm2.EditText) < dblMinTaxRate || Conversion.Val(GridForm2.EditText) > dblMaxTaxRate)
				{
					MessageBox.Show("You must enter a tax rate in the valid range.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //FC:FINAL:CHN - issue #1544: Fix second validating on cancel.
                    // e.Cancel = true;
                    GridForm2.EditText = GridForm2.TextMatrix(row, col);
                    return;
				}
				GridForm2.EditText = Strings.Format(modMain.Round(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(GridForm2.EditText))), 6), "##0.000000");
				GridForm2.TextMatrix(row, col, GridForm2.EditText);
				dblTaxRate = Conversion.Val(GridForm2.EditText);
				boolDataChanged = true;
				ReCalc();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GridForm2_ValidateEdit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void ResizeGrids()
		{
			int GridWidth = 0;
			try
			{
				// On Error GoTo ErrorHandler
				// With GridInput
				// GridWidth = .Width
				// .ColWidth(0) = 0.75 * GridWidth
				// .ColWidth(1) = 0.21 * GridWidth
				// .Height = .RowHeight(0) * 9 + 60
				// End With
				//FC:FINAL:DDU:#i1073 - done by designer
				iLabel1:
				GridWidth = GridForm1.WidthOriginal;
				iLabel2:
				GridForm1.ColWidth(0, FCConvert.ToInt32(0.06 * GridWidth));
				iLabel3:
				GridForm1.ColWidth(1, FCConvert.ToInt32(0.76 * GridWidth));
				iLabel4:
				GridForm1.ColWidth(2, FCConvert.ToInt32(0.16 * GridWidth));
				iLabel5:
				//GridForm1.HeightOriginal = GridForm1.RowHeight(0) * 17 + 60;
				iLabel6:
				GridWidth = GridForm2.WidthOriginal;
				iLabel7:
				GridForm2.ColWidth(0, FCConvert.ToInt32(0.06 * GridWidth));
				iLabel8:
				GridForm2.ColWidth(1, FCConvert.ToInt32(0.38 * GridWidth));
				iLabel9:
				GridForm2.ColWidth(2, FCConvert.ToInt32(0.39 * GridWidth));
				iLabel10:
				GridForm2.ColWidth(3, FCConvert.ToInt32(0.15 * GridWidth));
				iLabel11:
				//GridForm2.HeightOriginal = GridForm2.RowHeight(0) * 9 + 60;
				GridWidth = GridEnhancedBETE.WidthOriginal;
				GridEnhancedBETE.ColWidth(0, FCConvert.ToInt32(0.06 * GridWidth));
				GridEnhancedBETE.ColWidth(1, FCConvert.ToInt32(0.74 * GridWidth));
				GridEnhancedBETE.ColWidth(2, FCConvert.ToInt32(0.16 * GridWidth));
				//GridEnhancedBETE.HeightOriginal = GridEnhancedBETE.RowHeight(0) * 16 + 60;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ResizeGrids in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGrids()
		{
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				// With GridInput
				// .Rows = 2
				// .Cols = 2
				// .ExtendLastCol = True
				// .ForeColor = RGB(1, 1, 1)
				// .ForeColorFixed = RGB(1, 1, 1)
				// End With
				// Call FillGridInput
				iLabel1:
				GridForm1.BackColorFixed = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			// RGB(190, 190, 190)
			iLabel2:
				GridForm1.GridColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
				iLabel3:
				GridForm1.GridColorFixed = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
				iLabel4:
				GridForm1.Rows = 17;
				iLabel5:
				GridForm1.Cols = 3;
				iLabel6:
				GridForm1.ExtendLastCol = true;
				iLabel7:
				GridForm1.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
				iLabel8:
				GridForm1.ForeColorFixed = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 1, 14, 1) = RGB(180, 185, 250)
				iLabel9:
				GridForm1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 1, 15, 1, Color.White);
				iLabel10:
				if (!modGlobalConstants.Statics.gboolUse256Colors)
				{
					iLabel11:
					GridForm1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, CNSTGRIDROWTOTALDEDUCTIONS, 2, Information.RGB(225, 0, 0));
					iLabel12:
					GridForm1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CNSTGRIDROWREVAL, 2, CNSTGRIDROWTOTVALUATIONBASE, 2, Information.RGB(220, 220, 220));
					iLabel13:
					GridForm1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CNSTGRIDROWTOTAPPROPRIATIONS, 2, Information.RGB(220, 220, 220));
					iLabel14:
					GridForm1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CNSTGRIDROWTOTALDEDUCTIONS, 2, CNSTGRIDROWNETRAISED, 2, Information.RGB(220, 220, 220));
				}
				else
				{
					iLabel15:
					GridForm1.Cell(FCGrid.CellPropertySettings.flexcpForeColor, CNSTGRIDROWTOTALDEDUCTIONS, 2, modGlobalConstants.Statics.TRIOCOLORRED);
					iLabel16:
					GridForm1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CNSTGRIDROWREVAL, 2, CNSTGRIDROWTOTVALUATIONBASE, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
					iLabel17:
					GridForm1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CNSTGRIDROWTOTAPPROPRIATIONS, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
					iLabel18:
					GridForm1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CNSTGRIDROWTOTALDEDUCTIONS, 2, CNSTGRIDROWNETRAISED, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
				}
				iLabel19:
				GridForm1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CNSTGRIDROWPPVAL, 2, Color.White);
				GridForm1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CNSTGRIDROWTOTBETE, 2, Color.White);
				GridForm1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CNSTGRIDROWBETEReimValue, 2, Color.White);
				iLabel20:
				for (x = 0; x <= GridForm1.Rows - 1; x++)
				{
					if (x < CNSTGRIDROWTOTHOMESTEAD)
					{
						iLabel21:
						GridForm1.TextMatrix(x, 0, FCConvert.ToString(x + 1) + ".");
					}
					else if (x == CNSTGRIDROWTOTHOMESTEAD)
					{
						GridForm1.TextMatrix(x, 0, FCConvert.ToString(x + 1) + "a.");
						//GridForm1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, CNSTGRIDROWTOTHOMESTEAD, 0, FCGrid.AlignmentSettings.flexAlignRightCenter);
					}
					else if (x == CNSTGRIDROWHALFTOTHOMESTEAD)
					{
						GridForm1.TextMatrix(x, 0, FCConvert.ToString(x) + "b.");
						//GridForm1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, CNSTGRIDROWHALFTOTHOMESTEAD, 0, FCGrid.AlignmentSettings.flexAlignRightCenter);
					}
					else if (x == CNSTGRIDROWTOTBETE)
					{
						GridForm1.TextMatrix(x, 0, FCConvert.ToString(x) + "a.");
						//GridForm1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, CNSTGRIDROWTOTBETE, 0, FCGrid.AlignmentSettings.flexAlignRightCenter);
					}
					else if (x == CNSTGRIDROWBETEReimValue)
					{
						GridForm1.TextMatrix(x, 0, FCConvert.ToString(x - 1) + "b.");
						//GridForm1.Cell(FCGrid.CellPropertySettings.flexcpAlignment, CNSTGRIDROWBETEReimValue, 0, FCGrid.AlignmentSettings.flexAlignRightCenter);
					}
					else
					{
						GridForm1.TextMatrix(x, 0, FCConvert.ToString(x - 1) + ".");
					}
				}
				// x
				iLabel22:
				GridForm1.TextMatrix(CNSTGRIDROWREVAL, 1, "Local Taxable Real Estate Valuation");
				iLabel23:
				GridForm1.TextMatrix(CNSTGRIDROWPPVAL, 1, "Local Taxable Personal Property Valuation");
				iLabel24:
				GridForm1.TextMatrix(CNSTGRIDROWTOTTAXVAL, 1, "Total Taxable Valuation (Line 1 plus Line2)");
				iLabel25:
				GridForm1.TextMatrix(CNSTGRIDROWTOTHOMESTEAD, 1, "Total of all Homestead Exempt Valuation");
				GridForm1.TextMatrix(CNSTGRIDROWHALFTOTHOMESTEAD, 1, "Total of all Homestead Exempt Valuation times .7");
				GridForm1.TextMatrix(CNSTGRIDROWTOTBETE, 1, "Total of all BETE Exempt Valuation");
				GridForm1.TextMatrix(CNSTGRIDROWBETEReimValue, 1, "BETE Reimbursement Value");
				iLabel26:
				GridForm1.TextMatrix(CNSTGRIDROWTOTVALUATIONBASE, 1, "Total Valuation Base (Line 3 plus Line 4b plus Line 5)");
				// 27        .TextMatrix(CNSTGRIDROWFISCALYEAR, 1) = "Municipal Fiscal Year"
				iLabel28:
				GridForm1.TextMatrix(CNSTGRIDROWCOUNTYTAX, 1, "County Tax");
				iLabel29:
				GridForm1.TextMatrix(CNSTGRIDROWMUNIAPPROPRIATION, 1, "Municipal Appropriation");
				iLabel30:
				GridForm1.TextMatrix(CNSTGRIDROWTIF, 1, "TIF financing plan amount");
				iLabel31:
				GridForm1.TextMatrix(CNSTGRIDROWEDUCATION, 1, "Local Educational Appropriation");
				iLabel32:
				GridForm1.TextMatrix(CNSTGRIDROWTOTAPPROPRIATIONS, 1, "Total Appropriations (Add Lines 7 through 10)");
				iLabel33:
				GridForm1.TextMatrix(CNSTGRIDROWSTATEREVENUESHARING, 1, "State Municipal Revenue Sharing");
				iLabel34:
				GridForm1.TextMatrix(CNSTGRIDROWOTHERREVENUE, 1, "Other Revenues");
				iLabel35:
				GridForm1.TextMatrix(CNSTGRIDROWTOTALDEDUCTIONS, 1, "Total Deductions (Line 12 plus Line 13)");
				iLabel36:
				GridForm1.TextMatrix(CNSTGRIDROWNETRAISED, 1, "Net to be raised by local property tax (Line 11 minus Line 14)");
				iLabel37:
				if (boolPPEditable)
				{
					iLabel38:
					GridForm1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CNSTGRIDROWPPVAL, 2, Color.White);
					GridForm1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CNSTGRIDROWTOTBETE, 2, Color.White);
					GridForm1.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CNSTGRIDROWBETEReimValue, 2, Color.White);
                }
                //FC:FINAL:CHN - issue #1550: Fix grid column alignment.
                GridForm1.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
                iLabel39:
				GridForm2.GridColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
				iLabel40:
				GridForm2.GridColorFixed = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
				iLabel41:
				GridForm2.BackColorFixed = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			// RGB(190, 190, 190)
			iLabel42:
				GridForm2.Rows = 9;
				iLabel43:
				GridForm2.Cols = 4;
				iLabel44:
				GridForm2.ExtendLastCol = true;
				iLabel45:
				GridForm2.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
				iLabel46:
				GridForm2.ForeColorFixed = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 1, 7, 2) = RGB(180, 185, 250)
				iLabel47:
				GridForm2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 1, 8, 2, Color.White);
				iLabel48:
				if (!modGlobalConstants.Statics.gboolUse256Colors)
				{
					iLabel49:
					GridForm2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 3, 2, 3, Information.RGB(220, 220, 220));
					iLabel50:
					GridForm2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, 3, 8, 3, Information.RGB(220, 220, 220));
				}
				else
				{
					iLabel51:
					GridForm2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 3, 2, 3, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
					iLabel52:
					GridForm2.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, 3, 8, 3, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
				}
				iLabel53:
				for (x = 0; x <= GridForm2.Rows - 1; x++)
				{
					if (x < 3)
					{
						iLabel54:
						GridForm2.TextMatrix(x, 0, FCConvert.ToString(x + 16) + ".");
					}
					else if (x > 3)
					{
						GridForm2.TextMatrix(x, 0, FCConvert.ToString(x + 15) + ".");
					}
				}
				// x
				iLabel55:
				GridForm2.TextMatrix(0, 2, "Maximum Allowable Tax");
				iLabel56:
				GridForm2.TextMatrix(1, 2, "Minimum Tax Rate");
				iLabel57:
				GridForm2.TextMatrix(2, 2, "Maximum Tax Rate");
				iLabel58:
				GridForm2.TextMatrix(3, 2, "Tax Rate");
				iLabel59:
				GridForm2.TextMatrix(4, 2, "Tax for Commitment");
				iLabel60:
				GridForm2.TextMatrix(5, 2, "Maximum Overlay");
				iLabel61:
				GridForm2.TextMatrix(6, 2, "Homestead Reimbursement");
				GridForm2.TextMatrix(7, 2, "BETE Reimbursement");
				iLabel62:
				GridForm2.TextMatrix(8, 2, "Overlay");
				iLabel63:
				GridForm2.Row = 3;
				iLabel64:
				GridForm2.Col = 3;
				iLabel65:
				GridForm2.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				GridEnhancedBETE.BackColorFixed = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				// RGB(190, 190, 190)
				GridEnhancedBETE.GridColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
				GridEnhancedBETE.GridColorFixed = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLACK);
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowBETEPercent, 1, "BETE rate");
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowBETESubjectStandardReim, 1, "BETE standard reimbursement");
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowPPFactor, 1, "Personal property factor");
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowPPFactorDiv2, 1, "Factor / 2");
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowPPFactorDive2Plus50, 1, "Plus 50%");
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowEnteredTifPercent, 1, "Municipal retention rate");
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowTifPercent, 1, "Effective retention rate");
				GridEnhancedBETE.TextMatrix(CNSTEBGRIDRowTotBETE, 1, "Total BETE exempt");
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteNonStandardNonTif, 1, "Enhanced reimbursement");
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBETENonTif, 1, "BETE exempt not in a TIF district");
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteReim, 1, "Total of all reimbursable BETE exempt valuation");
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteTif, 1, "BETE exempt property in a TIF district");
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBETETifReim, 1, "TIF BETE exempt subject to reimbursement");
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotEnhancedNonTif, 1, "BETE exempt property subject to enhanced reimbursement");
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotPP, 1, "Total of all business personal property");
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotTaxableREPP, 1, "Total taxable real and personal property");
				GridEnhancedBETE.TextMatrix(0, 0, "1a.");
				GridEnhancedBETE.TextMatrix(1, 0, "1b.");
				GridEnhancedBETE.TextMatrix(2, 0, "1c.");
				GridEnhancedBETE.TextMatrix(3, 0, "1d.");
				GridEnhancedBETE.TextMatrix(4, 0, "2a.");
				GridEnhancedBETE.TextMatrix(5, 0, "2b.");
				GridEnhancedBETE.TextMatrix(6, 0, "2c.");
				GridEnhancedBETE.TextMatrix(7, 0, "2d.");
				GridEnhancedBETE.TextMatrix(8, 0, "2e.");
				GridEnhancedBETE.TextMatrix(9, 0, "2f.");
				GridEnhancedBETE.TextMatrix(10, 0, "2g.");
				GridEnhancedBETE.TextMatrix(12, 0, "");
				GridEnhancedBETE.TextMatrix(11, 0, "3a.");
				GridEnhancedBETE.TextMatrix(13, 0, "3b.");
				GridEnhancedBETE.TextMatrix(14, 0, "3c.");
				GridEnhancedBETE.TextMatrix(15, 0, "4a.");
				if (!modGlobalConstants.Statics.gboolUse256Colors)
				{
					GridEnhancedBETE.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 2, 15, 2, Information.RGB(220, 220, 220));
				}
				else
				{
					GridEnhancedBETE.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 2, 15, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
				}
				GridEnhancedBETE.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CNSTEBGridRowEnteredTifPercent, 2, Color.White);
				GridEnhancedBETE.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CNSTEBGridRowTotBeteTif, 2, Color.White);
                //FC:FINAL:CHN - issue #1550: Fix grid column.
                GridEnhancedBETE.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
                return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SetupGrids line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		/// <summary>
		/// Private Sub FillGridInput()
		/// </summary>
		/// <summary>
		/// With GridInput
		/// </summary>
		/// <summary>
		/// .TextMatrix(0, 0) = "County Tax"
		/// </summary>
		/// <summary>
		/// .TextMatrix(1, 0) = "Municipal Appropriations"
		/// </summary>
		/// <summary>
		/// .TextMatrix(2, 0) = "TIF Amount"
		/// </summary>
		/// <summary>
		/// .TextMatrix(3, 0) = "School/Education Appropriation"
		/// </summary>
		/// <summary>
		/// .TextMatrix(4, 0) = "Revenue Sharing"
		/// </summary>
		/// <summary>
		/// .TextMatrix(5, 0) = "Other Revenue"
		/// </summary>
		/// <summary>
		/// .TextMatrix(6, 0) = "Municipal Fiscal Year Start Date"
		/// </summary>
		/// <summary>
		/// .TextMatrix(7, 0) = "Municipal Fiscal Year End Date"
		/// </summary>
		/// <summary>
		/// .TextMatrix(8, 0) = "Tax Rate"
		/// </summary>
		/// <summary>
		/// End With
		/// </summary>
		/// <summary>
		/// End Sub
		/// </summary>
		private void ReCalc()
		{
			double dblCountyTax = 0;
			double dblMuniApp = 0;
			double dblTif = 0;
			double dblSchool = 0;
			double dblStateRev = 0;
			double dblOther = 0;
			double dblTotDeductions = 0;
			double dblTotApprop = 0;
			// vbPorter upgrade warning: dblMaxOverLay As double	OnWrite(string)
			double dblMaxOverLay = 0;
			double dblTotNet = 0;
			double dblTotVal = 0;
			double dblTotTaxed = 0;
			double dblTaxPlusHomestead;
			double dblHomesteadReim = 0;
			double dblBETEReim = 0;
			// vbPorter upgrade warning: dblTotTaxable As double	OnWrite(string)
			double dblTotTaxable = 0;
			try
			{
				// On Error GoTo ErrorHandler
				if (chkUseEB.CheckState == Wisej.Web.CheckState.Checked)
				{
					boolUseEnhancedBETE = true;
				}
				else
				{
					boolUseEnhancedBETE = false;
				}
				boolEnhancedBETEEligible = false;
				iLabel1:
				if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWPPVAL, 2)) > 0)
				{
					iLabel2:
					lngPPValuation = FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWPPVAL, 2).Replace(" ", ""));
				}
				else
				{
					iLabel3:
					lngPPValuation = 0;
				}
				if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWTOTBETE, 2)) > 0)
				{
					if (dblBETEExempt != FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWTOTBETE, 2).Replace(" ", "")) || boolUseEnhancedBETE)
					{
						dblBETEExempt = FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWTOTBETE, 2).Replace(" ", ""));
						if (dblBETERate > 0)
						{
							dblBETEReimValue = FCConvert.ToDouble(Strings.Format(dblBETERate * dblBETEExempt, "0"));
							if (boolUseEnhancedBETE)
							{
								dblBETEReimValue = RecalcEnhancedBETE();
							}
							GridForm1.TextMatrix(CNSTGRIDROWBETEReimValue, 2, Strings.Format(dblBETEReimValue, "###,###,###,##0"));
						}
						else
						{
							dblBETEReimValue = FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWBETEReimValue, 2).Replace(" ", ""));
						}
					}
					else
					{
						dblBETEReimValue = FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWBETEReimValue, 2).Replace(" ", ""));
					}
				}
				else
				{
					dblBETEExempt = 0;
					dblBETEReimValue = 0;
				}
				dblTotTaxable = lngREValuation + lngPPValuation;
				if ((0.05 * dblTotTaxable) > lngPPValuation)
				{
					boolEnhancedBETEEligible = false;
				}
				else
				{
					boolEnhancedBETEEligible = true;
				}
				iLabel4:
				GridForm1.TextMatrix(CNSTGRIDROWTOTTAXVAL, 2, Strings.Format(dblTotTaxable, "#,###,###,##0")); 
				iLabel5:
				GridForm1.TextMatrix(CNSTGRIDROWTOTVALUATIONBASE, 2, Strings.Format(lngREValuation + lngPPValuation + lngHalfHomestead + dblBETEReimValue, "#,###,###,##0"));
				iLabel6:
				if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWCOUNTYTAX, 2)) > 0)
				{
					iLabel7:
					dblCountyTax = FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWCOUNTYTAX, 2).Replace(" ", ""));
				}
				else
				{
					iLabel8:
					dblCountyTax = 0;
				}
				iLabel9:
				if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWMUNIAPPROPRIATION, 2)) > 0)
				{
					iLabel10:
					dblMuniApp = FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWMUNIAPPROPRIATION, 2).Replace(" ", ""));
				}
				else
				{
					iLabel11:
					dblMuniApp = 0;
				}
				iLabel12:
				if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWTIF, 2)) > 0)
				{
					iLabel13:
					dblTif = FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWTIF, 2).Replace(" ", ""));
				}
				else
				{
					iLabel14:
					dblTif = 0;
				}
				iLabel15:
				if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWEDUCATION, 2)) > 0)
				{
					iLabel16:
					dblSchool = FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWEDUCATION, 2).Replace(" ", ""));
				}
				else
				{
					iLabel17:
					dblSchool = 0;
				}
				iLabel18:
				dblTotApprop = dblCountyTax + dblMuniApp + dblTif + dblSchool;
				iLabel19:
				GridForm1.TextMatrix(CNSTGRIDROWTOTAPPROPRIATIONS, 2, Strings.Format(dblTotApprop, "#,###,###,##0.00"));
				iLabel20:
				if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWSTATEREVENUESHARING, 2)) > 0)
				{
					iLabel21:
					dblStateRev = FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWSTATEREVENUESHARING, 2).Replace(" ", ""));
				}
				else
				{
					iLabel22:
					dblStateRev = 0;
				}
				iLabel23:
				if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWOTHERREVENUE, 2)) > 0)
				{
					iLabel24:
					dblOther = FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWOTHERREVENUE, 2).Replace(" ", ""));
				}
				else
				{
					iLabel25:
					dblOther = 0;
				}
				iLabel26:
				dblTotDeductions = dblStateRev + dblOther;
				iLabel27:
				GridForm1.TextMatrix(CNSTGRIDROWTOTALDEDUCTIONS, 2, Strings.Format(dblTotDeductions, "#,###,##0.00"));
				iLabel28:
				dblTotNet = modMain.Round(dblTotApprop - dblTotDeductions, 2);
				iLabel29:
				GridForm1.TextMatrix(CNSTGRIDROWNETRAISED, 2, Strings.Format(dblTotNet, "#,###,###,##0.00"));
				iLabel30:
				GridForm2.TextMatrix(5, 1, Strings.Format(dblTotNet, "#,###,##0.00") + "  X  .05");
				iLabel31:
				dblMaxOverLay = FCConvert.ToDouble(Strings.Format(modMain.Round(dblTotNet * 0.05, 2), "#,###,##0.00"));
				iLabel32:
				GridForm2.TextMatrix(5, 3, Strings.Format(dblMaxOverLay, "#,###,##0.00"));
				// 33        .TextMatrix(0, 1) = Format(dblTotNet, "#,###,###,##0.00") & "  X  .05  +  " & Format(dblTotNet, "#,###,###,##0.00")
				GridForm2.TextMatrix(0, 1, Strings.Format(dblTotNet, "#,###,###,##0.00") + "  X  1.05");
				iLabel34:
				GridForm2.TextMatrix(0, 3, Strings.Format(modMain.Round(dblMaxOverLay + dblTotNet, 2), "#,###,###,##0.00"));
				iLabel35:
				GridForm2.TextMatrix(1, 1, Strings.Format(dblTotNet, "#,###,##0.00") + "  /  " + GridForm1.TextMatrix(CNSTGRIDROWTOTVALUATIONBASE, 2));
				iLabel36:
				dblTotVal = lngREValuation + lngPPValuation + lngHalfHomestead + dblBETEReimValue;
				// If dblTotVal > 0 Then
				// 37        .TextMatrix(1, 3) = Format(Round(dblTotNet / dblTotVal, 6), "##0.000000")
				// Else
				// .TextMatrix(1, 3) = 0
				// End If
				// If dblTotVal <> 0 Then
				// 38         dblMinTaxRate = Round(dblTotNet / dblTotVal, 6)
				// Else
				// dblMinTaxRate = 0
				// End If
				// 39        .TextMatrix(2, 1) = Format(dblMaxOverLay + dblTotNet, "#,###,##0.00") & "  /  " & GridForm1.TextMatrix(CNSTGRIDROWTOTVALUATIONBASE, 2)
				// If dblTotVal <> 0 Then
				// 40        dblMaxTaxRate = RoundDown((dblMaxOverLay + dblTotNet) / dblTotVal, 6)
				// Else
				// dblMaxTaxRate = 0
				// End If
				// 41        If dblTaxRate < dblMinTaxRate Or dblTaxRate > dblMaxTaxRate Then
				// 42            dblTaxRate = 0
				// 43            .TextMatrix(3, 3) = "0.000000"
				// End If
				iLabel37:
				GridForm2.TextMatrix(1, 3, Strings.Format(modMain.Round(dblTotNet / dblTotVal, 6), "##0.000000"));
				iLabel38:
				dblMinTaxRate = modMain.Round(dblTotNet / dblTotVal, 6);
				iLabel39:
				GridForm2.TextMatrix(2, 1, Strings.Format(dblMaxOverLay + dblTotNet, "#,###,##0.00") + "  /  " + GridForm1.TextMatrix(CNSTGRIDROWTOTVALUATIONBASE, 2));
				iLabel40:
				dblMaxTaxRate = modMain.RoundDown_8((dblMaxOverLay + dblTotNet) / dblTotVal, 6);
				iLabel41:
				if (dblTaxRate < dblMinTaxRate || dblTaxRate > dblMaxTaxRate)
				{
					iLabel42:
					dblTaxRate = 0;
					iLabel43:
					GridForm2.TextMatrix(3, 3, "0.000000");
				}
				iLabel44:
				GridForm2.TextMatrix(2, 3, Strings.Format(dblMaxTaxRate, "##0.000000"));
				iLabel45:
				GridForm2.TextMatrix(4, 1, Strings.Format(lngREValuation + lngPPValuation, "#,###,###,##0") + "  *  " + GridForm2.TextMatrix(3, 3));
				iLabel46:
				dblTotTaxed = modMain.Round(dblTaxRate * (lngREValuation + lngPPValuation), 2);
				iLabel47:
				GridForm2.TextMatrix(4, 3, Strings.Format(dblTotTaxed, "#,###,###,##0.00"));
				// 03/12/2005 State law change.  Only 50% reimbursement for homestead
				iLabel48:
				GridForm2.TextMatrix(6, 1, GridForm1.TextMatrix(CNSTGRIDROWHALFTOTHOMESTEAD, 2) + "  X       " + GridForm2.TextMatrix(3, 3));
				iLabel49:
				dblHomesteadReim = modMain.Round(dblTaxRate * lngHalfHomestead, 2);
				iLabel50:
				GridForm2.TextMatrix(6, 3, Strings.Format(dblHomesteadReim, "#,###,###,##0.00"));
				GridForm2.TextMatrix(7, 1, Strings.Format(dblBETEReimValue, "#,###,###,##0.00") + "  X       " + GridForm2.TextMatrix(3, 3));
				dblBETEReim = modMain.Round(dblTaxRate * dblBETEReimValue, 2);
				GridForm2.TextMatrix(7, 3, Strings.Format(dblBETEReim, "#,###,###,##0.00"));
				iLabel51:
				// .TextMatrix(8, 1) = .TextMatrix(4, 3) & "  -  " & GridForm1.TextMatrix(CNSTGRIDROWNETRAISED, 2)
				GridForm2.TextMatrix(8, 1, Strings.Format(dblTotTaxed + dblHomesteadReim + dblBETEReim, "#,###,###,##0.00") + "  -  " + GridForm1.TextMatrix(CNSTGRIDROWNETRAISED, 2));
				iLabel52:
				GridForm2.TextMatrix(8, 3, Strings.Format(dblTotTaxed + dblHomesteadReim + dblBETEReim - dblTotNet, "#,###,###,##0.00"));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ReCalc in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuLoad_Click(object sender, System.EventArgs e)
		{
			// Loads all non-calculated fields
			clsDRWrapper clsLoad = new clsDRWrapper();
			int intTotValues;
			try
			{
				// On Error GoTo ErrorHandler
				iLabel1:
				clsLoad.OpenRecordset("Select * from TaxRateCalculation where isnull(townnumber,0)  = " + FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))), modGlobalVariables.strREDatabase);
				iLabel2:
				if (clsLoad.EndOfFile())
				{
					iLabel3:
					MessageBox.Show("No saved data to load", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				iLabel4:
				intTotValues = 0;
				iLabel5:
				if (boolPPEditable)
				{
					iLabel6:
					if (Conversion.Val(clsLoad.Get_Fields_Int32("Personalpropertyvaluation")) > 0)
					{
						iLabel7:
						intTotValues += 1;
					}
				}
				iLabel8:
				if (Conversion.Val(clsLoad.Get_Fields_Double("Countytax")) > 0)
				{
					iLabel9:
					intTotValues += 1;
				}
				iLabel10:
				if (Conversion.Val(clsLoad.Get_Fields_Double("MunicipalAppropriation")) > 0)
				{
					iLabel11:
					intTotValues += 1;
				}
				iLabel12:
				if (Conversion.Val(clsLoad.Get_Fields_Double("TIF")) > 0)
				{
					iLabel13:
					intTotValues += 1;
				}
				iLabel14:
				if (Conversion.Val(clsLoad.Get_Fields_Double("Education")) > 0)
				{
					iLabel15:
					intTotValues += 1;
				}
				iLabel16:
				if (Conversion.Val(clsLoad.Get_Fields_Double("StateSharing")) > 0)
				{
					iLabel17:
					intTotValues += 1;
				}
				iLabel18:
				if (Conversion.Val(clsLoad.Get_Fields_Double("other")) > 0)
				{
					iLabel19:
					intTotValues += 1;
				}
				iLabel20:
				if (intTotValues == 0)
				{
					iLabel21:
					MessageBox.Show("No saved data to load", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				iLabel22:
				if (boolPPEditable)
				{
					iLabel23:
					GridForm1.TextMatrix(CNSTGRIDROWPPVAL, 2, Strings.Format(clsLoad.Get_Fields_Int32("PersonalPropertyValuation"), "#,###,###,##0"));
					GridForm1.TextMatrix(CNSTGRIDROWTOTBETE, 2, Strings.Format(clsLoad.Get_Fields_Int32("BETEExempt"), "#,###,###,##0"));
					GridForm1.TextMatrix(CNSTGRIDROWBETEReimValue, 2, Strings.Format(clsLoad.Get_Fields_Double("BETEReimburseValue"), "#,###,###,##0"));
				}
				// If chkUseEB.Value = vbChecked Then
				// clsSave.Fields("useenhancedbete") = True
				// Else
				// clsSave.Fields("useenhancedbete") = False
				// End If
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowEnteredTifPercent, 2, Strings.Format(clsLoad.Get_Fields_Double("tifmuniretentionperc") * 100, "0.00"));
				GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteTif, 2, FCConvert.ToString(clsLoad.Get_Fields_Double("tifbeteval")));
				iLabel24:
				GridForm1.TextMatrix(CNSTGRIDROWCOUNTYTAX, 2, Strings.Format(clsLoad.Get_Fields_Double("CountyTax"), "#,###,###,##0.00"));
				iLabel25:
				GridForm1.TextMatrix(CNSTGRIDROWMUNIAPPROPRIATION, 2, Strings.Format(clsLoad.Get_Fields_Double("MunicipalAppropriation"), "#,###,###,##0.00"));
				iLabel26:
				GridForm1.TextMatrix(CNSTGRIDROWTIF, 2, Strings.Format(clsLoad.Get_Fields_Double("TIF"), "#,###,##0.00"));
				iLabel27:
				GridForm1.TextMatrix(CNSTGRIDROWEDUCATION, 2, Strings.Format(clsLoad.Get_Fields_Double("Education"), "#,###,##0.00"));
				iLabel28:
				GridForm1.TextMatrix(CNSTGRIDROWSTATEREVENUESHARING, 2, Strings.Format(clsLoad.Get_Fields_Double("StateSharing"), "#,###,##0.00"));
				iLabel29:
				GridForm1.TextMatrix(CNSTGRIDROWOTHERREVENUE, 2, Strings.Format(clsLoad.Get_Fields_Double("Other"), "#,###,##0.00"));
				iLabel30:
				ReCalc();
				iLabel31:
				if (Conversion.Val(clsLoad.Get_Fields_Double("TaxRate")) >= dblMinTaxRate)
				{
					iLabel32:
					if (Conversion.Val(clsLoad.Get_Fields_Double("TaxRate")) <= dblMaxTaxRate)
					{
						iLabel33:
						GridForm2.TextMatrix(3, 3, FCConvert.ToString(clsLoad.Get_Fields_Double("TaxRate")));
						iLabel34:
						dblTaxRate = Conversion.Val(clsLoad.Get_Fields_Double("TaxRate"));
						iLabel35:
						ReCalc();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuLoad in line " + Information.Erl(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			// rptTaxRateCalc.Show , MDIParent
			// rptTaxRateForm.Show , MDIParent
			// Call frmReportViewer.Init(rptTaxRateForm)
			#if ModuleID_1
						            rptTaxRateForm.InstancePtr.Init(false, modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);

#else
			rptTaxRateForm.InstancePtr.Init(false, modGlobalVariables.Statics.CustomizedInfo.intCurrentTown);
			#endif
		}

		private void mnuPrintBETEWorksheet_Click(object sender, System.EventArgs e)
		{
			CheckEnhancedBETEEligibility();
			PrintBETEWorksheet();
		}

		private void mnuPrintCertification_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(GridForm2.TextMatrix(3, 3)) <= 0)
			{
				MessageBox.Show("You must choose a tax rate first", "No Tax Rate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			if (boolDataChanged)
			{
				if (MessageBox.Show("Data has been changed" + "\r\n" + "Save & Continue?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (!SaveData())
						return;
				}
				else
				{
					return;
				}
			}
			frmCertificateOfAssessment.InstancePtr.Init(lngREValuation, FCConvert.ToInt32(Conversion.Val(gridTownCode.TextMatrix(0, 0))), t2kBegin.Text, t2kEnd.Text);
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveData();
		}

		private bool SaveData()
		{
			bool SaveData = false;
			SaveData = false;
			// saves non-calculated info including personal property valuation if there is no
			// twpp0000.vb1 file present.
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				#if ModuleID_1
								                if (!modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
                {
                    clsSave.OpenRecordset("select * from TaxRateCalculation where isnull(townnumber,0) = 0", modGlobalVariables.strREDatabase);
                }
                else
                {
                    clsSave.OpenRecordset("select * from TaxRateCalculation where townnumber = " + FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))), modGlobalVariables.strREDatabase);
                }

#else
				if (!modRegionalTown.IsRegionalTown())
				{
					clsSave.OpenRecordset("select * from TaxRateCalculation where isnull(townnumber,0) = 0", modGlobalVariables.strREDatabase);
				}
				else
				{
					clsSave.OpenRecordset("select * from TaxRateCalculation where townnumber = " + FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))), modGlobalVariables.strREDatabase);
				}
				#endif
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
					// TODO: Check the table for the column [townnumber] and replace with corresponding Get_Field method
					clsSave.Set_Fields("townnumber", FCConvert.ToString(Conversion.Val(clsSave.Get_Fields("townnumber"))));
				}
				else
				{
					clsSave.AddNew();
					clsSave.Set_Fields("townnumber", FCConvert.ToString(Conversion.Val(gridTownCode.TextMatrix(0, 0))));
				}
				if (chkUseEB.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsSave.Set_Fields("useenhancedbete", true);
				}
				else
				{
					clsSave.Set_Fields("useenhancedbete", false);
				}
				if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowEnteredTifPercent, 2)) > 0)
				{
					clsSave.Set_Fields("tifmuniretentionperc", FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowEnteredTifPercent, 2).Replace(" ", "")) / 100);
				}
				else
				{
					clsSave.Set_Fields("tifmuniretentionperc", 0);
				}
				if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteTif, 2)) > 0)
				{
					clsSave.Set_Fields("tifbeteval", FCConvert.ToInt32(FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteTif, 2).Replace(" ", ""))));
				}
				else
				{
					clsSave.Set_Fields("tifbeteval", 0);
				}
				if (Information.IsDate(t2kBegin.Text))
				{
					clsSave.Set_Fields("Taxyear", FCConvert.ToDateTime(t2kBegin.Text).Year);
				}
				else
				{
					clsSave.Set_Fields("taxyear", DateTime.Today.Year);
				}
				if (boolPPEditable)
				{
					if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWPPVAL, 2)) > 0)
					{
						clsSave.Set_Fields("personalpropertyvaluation", FCConvert.ToInt32(FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWPPVAL, 2).Replace(" ", ""))));
					}
					else
					{
						clsSave.Set_Fields("personalpropertyvaluation", 0);
					}
					if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWTOTBETE, 2)) > 0)
					{
						clsSave.Set_Fields("BETEExempt", FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWTOTBETE, 2).Replace(" ", "")));
					}
					else
					{
						clsSave.Set_Fields("BETEExempt", 0);
					}
					if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWBETEReimValue, 2)) > 0)
					{
						clsSave.Set_Fields("BETEReimburseValue", FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWBETEReimValue, 2).Replace(" ", "")));
					}
					else
					{
						clsSave.Set_Fields("BETEReimburseValue", 0);
					}
				}
				else
				{
					clsSave.Set_Fields("personalpropertyvaluation", 0);
					clsSave.Set_Fields("BETEExempt", 0);
					clsSave.Set_Fields("BETEReimValue", 0);
				}
				clsSave.Set_Fields("reamount", FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWREVAL, 2).Replace(" ", "")));
				if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWCOUNTYTAX, 2)) > 0)
				{
					clsSave.Set_Fields("CountyTax", FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWCOUNTYTAX, 2).Replace(" ", "")));
				}
				else
				{
					clsSave.Set_Fields("CountyTax", 0);
				}
				if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWMUNIAPPROPRIATION, 2)) > 0)
				{
					clsSave.Set_Fields("MunicipalAppropriation", FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWMUNIAPPROPRIATION, 2).Replace(" ", "")));
				}
				else
				{
					clsSave.Set_Fields("MunicipalAppropriation", 0);
				}
				if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWTIF, 2)) > 0)
				{
					clsSave.Set_Fields("TIF", FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWTIF, 2).Replace(" ", "")));
				}
				else
				{
					clsSave.Set_Fields("TIF", 0);
				}
				if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWEDUCATION, 2)) > 0)
				{
					clsSave.Set_Fields("Education", FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWEDUCATION, 2).Replace(" ", "")));
				}
				else
				{
					clsSave.Set_Fields("Education", 0);
				}
				if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWSTATEREVENUESHARING, 2)) > 0)
				{
					clsSave.Set_Fields("StateSharing", FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWSTATEREVENUESHARING, 2).Replace(" ", "")));
				}
				else
				{
					clsSave.Set_Fields("StateSharing", 0);
				}
				if (Conversion.Val(GridForm1.TextMatrix(CNSTGRIDROWOTHERREVENUE, 2)) > 0)
				{
					clsSave.Set_Fields("Other", FCConvert.ToDouble(GridForm1.TextMatrix(CNSTGRIDROWOTHERREVENUE, 2).Replace(" ", "")));
				}
				else
				{
					clsSave.Set_Fields("Other", 0);
				}
				if (Conversion.Val(GridForm2.TextMatrix(3, 3)) > 0)
				{
					clsSave.Set_Fields("TaxRate", FCConvert.ToDouble(GridForm2.TextMatrix(3, 3).Replace(" ", "")));
				}
				else
				{
					clsSave.Set_Fields("TaxRate", 0);
				}
				if (Conversion.Val(GridForm2.TextMatrix(6, 3)) > 0)
				{
					clsSave.Set_Fields("HomesteadReimbursement", FCConvert.ToDouble(GridForm2.TextMatrix(6, 3).Replace(" ", "")));
				}
				else
				{
					clsSave.Set_Fields("HomesteadReimbursement", 0);
				}
				if (Conversion.Val(GridForm2.TextMatrix(7, 3)) > 0)
				{
					clsSave.Set_Fields("BETEReimbursement", FCConvert.ToDouble(GridForm2.TextMatrix(7, 3).Replace(" ", "")));
				}
				else
				{
					clsSave.Set_Fields("BETEReimbursement", 0);
				}
				if (Conversion.Val(GridForm2.TextMatrix(8, 3)) > 0)
				{
					clsSave.Set_Fields("Overlay", FCConvert.ToDouble(GridForm2.TextMatrix(8, 3).Replace(" ", "")));
				}
				else
				{
					clsSave.Set_Fields("Overlay", 0);
				}
				clsSave.Update();
				boolDataChanged = false;
				SaveData = true;
				CheckEnhancedBETEEligibility();
                //FC:FINAL:AM:#3222 - add save message
                MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				mnuExit_Click();
			}
		}

		private void optRange_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						framRange.Visible = false;
						cmbType.Visible = false;
						//FC:FINAL:MSH - i.issue #1073: hide label for "Type" combobox
						lblType.Visible = false;
						break;
					}
				case 1:
					{
						framRange.Visible = true;
						cmbType.Visible = true;
						//FC:FINAL:MSH - i.issue #1073: show label for "Type" combobox
						lblType.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbRange.SelectedIndex);
			optRange_CheckedChanged(index, sender, e);
		}

		private void SetupGridTownCode()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strTemp = "";
			#if ModuleID_1
						            if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
            {
                strTemp = "";
                clsLoad.OpenRecordset("select * from tblTranCode where code > 0 order by code", modGlobalVariables.strREDatabase);
                while (!clsLoad.EndOfFile())
                {
                    strTemp += "#" + clsLoad.Get_Fields_Int32("code") + ";" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields_Int32("code") + "|";
                    clsLoad.MoveNext();
                }
                if (strTemp != string.Empty)
                {
                    strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
                }
                gridTownCode.ColComboList(0, strTemp);
                gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
                gridTownCode.Visible = true;
            }
            else
            {
                gridTownCode.Rows = 1;
                gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
                gridTownCode.Visible = false;
            }

#else
			if (modRegionalTown.IsRegionalTown())
			{
				strTemp = "";
				clsLoad.OpenRecordset("select * from tblTranCode where code > 0 order by code", modGlobalVariables.strREDatabase);
				while (!clsLoad.EndOfFile())
				{
					strTemp += "#" + clsLoad.Get_Fields_Int32("code") + ";" + clsLoad.Get_Fields_String("description") + "\t" + clsLoad.Get_Fields_Int32("code") + "|";
					clsLoad.MoveNext();
				}
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				gridTownCode.ColComboList(0, strTemp);
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = true;
			}
			else
			{
				gridTownCode.Rows = 1;
				gridTownCode.TextMatrix(0, 0, FCConvert.ToString(0));
				gridTownCode.Visible = false;
			}
			#endif
		}

		private void ResizeGridTownCode()
		{
			//FC:FINAL:DDU:#i1073 - done by designer
			//gridTownCode.HeightOriginal = gridTownCode.RowHeight(0) + 60;
		}

		private void optSource_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (Index == 1)
			{
				bool boolShowWarn = false;
				boolShowWarn = false;
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from status", modGlobalVariables.strREDatabase);
				if (Information.IsDate(rsLoad.Get_Fields("batchcalc")))
				{
					if (rsLoad.Get_Fields_DateTime("batchcalc").ToOADate() != 0)
					{
						if (Information.IsDate(rsLoad.Get_Fields("currentexemptions")))
						{
							if (rsLoad.Get_Fields_DateTime("currentexemptions").ToOADate() != 0)
							{
								if (DateAndTime.DateDiff("d", (DateTime)rsLoad.Get_Fields_DateTime("currentexemptions"), (DateTime)rsLoad.Get_Fields_DateTime("batchcalc")) > 0)
								{
									boolShowWarn = true;
								}
							}
							else
							{
								boolShowWarn = true;
							}
						}
						else
						{
							boolShowWarn = true;
						}
						if (boolShowWarn)
						{
							MessageBox.Show("A batch calculation has been performed since the last time current exemptions were calculated." + "\r\n" + "Current exemption values may not be correct.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
				else
				{
					if (!Information.IsDate(rsLoad.Get_Fields("currentexemptions")))
					{
						MessageBox.Show("There is no record of current exemptions being calculated." + "\r\n" + "Current exemption values may not be correct.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
			}
		}

		private void optSource_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbSource.SelectedIndex);
			optSource_CheckedChanged(index, sender, e);
		}

		private void PrintBETEWorksheet()
		{
			cEnhancedBETE theModel = new cEnhancedBETE();
			#if ModuleID_1
						            theModel.Municipality = modGlobalVariables.Statics.CustomizedInfo.CurrentTownName;

#else
			theModel.Municipality = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
			if (theModel.Municipality == "")
			{
				theModel.Municipality = modGlobalConstants.Statics.MuniName;
			}
			#endif
			theModel.ReportYear = DateTime.Now.Year;
			// section 1
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGRIDRowTotBETE, 2)) > 0)
			{
				theModel.TotalValuationBETEQualifiedExemptProperty = FCConvert.ToInt32(FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGRIDRowTotBETE, 2).Replace(" ", "")));
			}
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBETENonTif, 2)) > 0)
			{
				theModel.TotalValuationBETENotInTIF = FCConvert.ToInt32(FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBETENonTif, 2).Replace(" ", "")));
			}
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowBETEPercent, 2)) > 0)
			{
				theModel.BETEPercent = FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowBETEPercent, 2).Replace(" ", "")) / 100;
			}
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowBETESubjectStandardReim, 2)) > 0)
			{
				theModel.TotalValuationBETEStandardReim = FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowBETESubjectStandardReim, 2).Replace(" ", ""));
			}
			// section 2
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotPP, 2)) > 0)
			{
				theModel.TotalValueBusinessPersonalProperty = FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotPP, 2).Replace(" ", ""));
			}
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotTaxableREPP, 2)) > 0)
			{
				theModel.TotalValueTaxableProperty = FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotTaxableREPP, 2).Replace(" ", ""));
			}
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotEnhancedNonTif, 2)) > 0)
			{
				theModel.TotalValuationBETEEnhancedReimNotInTif = FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotEnhancedNonTif, 2).Replace(" ", ""));
			}
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowPPFactor, 2)) > 0)
			{
				theModel.PersonalPropertyFactor = FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowPPFactor, 2).Replace(" ", "")) / 100;
			}
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowPPFactorDiv2, 2)) > 0)
			{
				theModel.HalfFactor = FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowPPFactorDiv2, 2).Replace(" ", "")) / 100;
			}
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowPPFactorDive2Plus50, 2)) > 0)
			{
				theModel.HalfPlus50 = FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowPPFactorDive2Plus50, 2).Replace(" ", "")) / 100;
			}
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteNonStandardNonTif, 2)) > 0)
			{
				theModel.TotalValuationBeteSubjectEnhancedReim = FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteNonStandardNonTif, 2).Replace(" ", ""));
			}
			// section 3
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowEnteredTifPercent, 2)) > 0)
			{
				theModel.RetentionPercentage = FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowEnteredTifPercent, 2).Replace(" ", "")) / 100;
			}
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTifPercent, 2)) > 0)
			{
				theModel.EffectivePercentage = FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTifPercent, 2).Replace(" ", "")) / 100;
			}
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteTif, 2)) > 0)
			{
				theModel.BETEValuationInTIF = FCConvert.ToInt32(FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteTif, 2).Replace(" ", "")));
			}
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBETETifReim, 2)) > 0)
			{
				theModel.TIFBETEQualifiedValuationReimbursable = FCConvert.ToInt32(FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBETETifReim, 2).Replace(" ", "")));
			}
			// section 4
			if (Conversion.Val(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteReim, 2)) > 0)
			{
				theModel.TotalReimbursableBETE = FCConvert.ToInt32(FCConvert.ToDouble(GridEnhancedBETE.TextMatrix(CNSTEBGridRowTotBeteReim, 2).Replace(" ", "")));
			}
			rptEnhancedBETELeadSheet.InstancePtr.Init(ref theModel);
		}

		private void CheckEnhancedBETEEligibility()
		{
			if (boolUseEnhancedBETE)
			{
				if (!boolEnhancedBETEEligible)
				{
					MessageBox.Show("The calculator is configured to use the Enhanced BETE form but the current calculation indicates you may not be eligible", "BETE", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
		}

		private void cmbType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbType.SelectedIndex == 0)
			{
				optRange_CheckedChanged(sender, e);
			}
			else if (cmbType.SelectedIndex == 1)
			{
				optRange_CheckedChanged(sender, e);
			}
			else if (cmbType.SelectedIndex == 2)
			{
				optRange_CheckedChanged(sender, e);
			}
			else if (cmbType.SelectedIndex == 3)
			{
				optRange_CheckedChanged(sender, e);
			}
			else if (cmbType.SelectedIndex == 4)
			{
				optRange_CheckedChanged(sender, e);
			}
			else if (cmbType.SelectedIndex == 5)
			{
				optRange_CheckedChanged(sender, e);
			}
			else if (cmbType.SelectedIndex == 6)
			{
				optRange_CheckedChanged(sender, e);
			}
		}

		private void cmbSource_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbSource.SelectedIndex == 0)
			{
				optSource_CheckedChanged(sender, e);
			}
			else if (cmbSource.SelectedIndex == 1)
			{
				optSource_CheckedChanged(sender, e);
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSave_Click(mnuSave, EventArgs.Empty);
		}
	}
}
