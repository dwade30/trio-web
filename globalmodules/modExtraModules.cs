﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Runtime.InteropServices;

namespace Global
{
	public class modExtraModules
	{
		public const string strARDatabase = "TWAR0000.vb1";
		public const string strBDDatabase = "TWBD0000.vb1";
        public const string strCLDatabase = "TWCL0000.vb1";
		public const string strCKDatabase = "TWCK0000.vb1";
		public const string strCRDatabase = "TWCR0000.vb1";
        public const string strFADatabase = "TWFA0000.vb1";
		public const string strMVDatabase = "TWMV0000.vb1";
		public const string strPPDatabase = "TWPP0000.vb1";
		public const string strPYDatabase = "TWPY0000.vb1";
		public const string strREDatabase = "TWRE0000.vb1";
		public const string strUTDatabase = "TWUT0000.vb1";
        public const int Infinite = -1;
		public const int DEFAULTSUBTRACTIONVALUE = 19800;
		
		public static bool IsThisCR()
		{
			bool IsThisCR = false;
			// this will return true if this is the CR executable, false otherwise
			if (Strings.UCase(App.EXEName) == "TWCR0000" && modGlobalConstants.Statics.gboolCR)
			{
				IsThisCR = true;
			}
			else
			{
				IsThisCR = false;
			}
			return IsThisCR;
		}
		// VBto upgrade warning: strYear As string	OnWrite(string, int)
		public static string FormatYear(string strYear)
		{
			string FormatYear = "";
			if (Strings.InStr(1, strYear, "-") > 0)
			{
				if (Strings.InStr(1, strYear, "*") > 0)
				{
					FormatYear = Strings.Left(strYear, 4) + FCConvert.ToString(Conversion.Val(Strings.Right(strYear, 2)));
				}
				else
				{
					FormatYear = Strings.Left(strYear, 4) + Strings.Right(strYear, 1);
				}
			}
			else
			{
				if (Strings.InStr(1, strYear, "*") > 0)
				{
					FormatYear = Strings.Left(strYear, 4) + "-" + FCConvert.ToString(Conversion.Val(Strings.Right(strYear, 2)));
				}
				else
				{
					FormatYear = Strings.Left(strYear, 4) + "-" + Strings.Right(strYear, 1);
				}
			}
			return FormatYear;
		}

		//public static void GetCustomer(int lngCustNumber)
		//{
		//	// This is a dummy routine for AR
		//}

		public class StaticVariables
		{
			// ********************************************************
			// LAST UPDATED   :               06/07/2006              *
			// MODIFIED BY    :               Jim Bertolino           *
			// *
			// DATE           :               08/08/2002              *
			// WRITTEN BY     :               Jim Bertolino           *
			// *
			// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
			// ********************************************************
			//=========================================================
			//public int CurrentAccountRE;
			//public int CurrentAccountPP;
			public double dblOverPayRate;
			public double dblDefaultInterestRate;
			public string gstrRegularIntMethod = "";
			public bool gboolShowLienAmountOnBill;
			public bool gboolUsingCR;
			// this is True if this execuatble IS CR
			public int glngTownReadingUnits;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
