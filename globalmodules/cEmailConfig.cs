﻿using fecherFoundation;

namespace Global
{
	public class cEmailConfig
	{
		//=========================================================
		private int lngRecordID;
		private string strAuthMethod = string.Empty;
		private string strSMTPHost = string.Empty;
		private string strLoginDomain = string.Empty;
		private string strPassword = string.Empty;
		private int lngSMTPPort;
		private bool boolSMTPSSL;
		private string strUserName = string.Empty;
		private bool boolTLS;
		private string strDefaultFromName = string.Empty;
		private string strDefaultFromAddress = string.Empty;
		private bool boolUpdated;
		private bool boolDeleted;
		private int lngUserID;

		public int UserID
		{
			set
			{
				lngUserID = value;
			}
			get
			{
				int UserID = 0;
				UserID = lngUserID;
				return UserID;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string DefaultFromName
		{
			set
			{
				IsUpdated = true;
				strDefaultFromName = value;
			}
			get
			{
				string DefaultFromName = "";
				DefaultFromName = strDefaultFromName;
				return DefaultFromName;
			}
		}

		public string DefaultFromAddress
		{
			set
			{
				strDefaultFromAddress = value;
				IsUpdated = true;
			}
			get
			{
				string DefaultFromAddress = "";
				DefaultFromAddress = strDefaultFromAddress;
				return DefaultFromAddress;
			}
		}

		public short AuthCode
		{
			set
			{
				strAuthMethod = AuthStringFromCode(value);
				IsUpdated = true;
			}
			get
			{
				short AuthCode = 0;
				AuthCode = AuthCodeFromString(strAuthMethod);
				return AuthCode;
			}
		}

		public string AuthMethod
		{
			set
			{
				strAuthMethod = value;
				IsUpdated = true;
			}
			get
			{
				string AuthMethod = "";
				AuthMethod = strAuthMethod;
				return AuthMethod;
			}
		}

		public string SMTPHost
		{
			set
			{
				strSMTPHost = value;
				IsUpdated = true;
			}
			get
			{
				string SMTPHost = "";
				SMTPHost = strSMTPHost;
				return SMTPHost;
			}
		}

		public string LoginDomain
		{
			set
			{
				strLoginDomain = value;
				IsUpdated = true;
			}
			get
			{
				string LoginDomain = "";
				LoginDomain = strLoginDomain;
				return LoginDomain;
			}
		}

		public string Password
		{
			set
			{
				strPassword = value;
				IsUpdated = true;
			}
			get
			{
				string Password = "";
				Password = strPassword;
				return Password;
			}
		}

		public int SMTPPort
		{
			set
			{
				lngSMTPPort = value;
				IsUpdated = true;
			}
			get
			{
				int SMTPPort = 0;
				SMTPPort = lngSMTPPort;
				return SMTPPort;
			}
		}

		public bool UseSSL
		{
			set
			{
				boolSMTPSSL = value;
				IsUpdated = true;
			}
			get
			{
				bool UseSSL = false;
				UseSSL = boolSMTPSSL;
				return UseSSL;
			}
		}

		public string UserName
		{
			set
			{
				strUserName = value;
				IsUpdated = true;
			}
			get
			{
				string UserName = "";
				UserName = strUserName;
				return UserName;
			}
		}

		public bool UseTransportLayerSecurity
		{
			set
			{
				boolTLS = value;
				IsUpdated = true;
			}
			get
			{
				bool UseTransportLayerSecurity = false;
				UseTransportLayerSecurity = boolTLS;
				return UseTransportLayerSecurity;
			}
		}
		// VBto upgrade warning: 'Return' As Variant --> As string
		private string AuthStringFromCode(int intCode)
		{
			string AuthStringFromCode = "";
			string strReturn;
			strReturn = "";
			switch (intCode)
			{
				case -1:
					{
						strReturn = "";
						break;
					}
				case 0:
					{
						strReturn = "LOGIN";
						break;
					}
				case 1:
					{
						strReturn = "NTLM";
						break;
					}
				case 2:
					{
						strReturn = "CRAM-MD5";
						break;
					}
				case 3:
					{
						strReturn = "PLAIN";
						break;
					}
				case 5:
					{
						strReturn = "NONE";
						break;
					}
			}
			//end switch
			AuthStringFromCode = strReturn;
			return AuthStringFromCode;
		}
		// VBto upgrade warning: 'Return' As Variant --> As short	OnWriteFCConvert.ToInt32(
		private short AuthCodeFromString(string strCode)
		{
			short AuthCodeFromString = 0;
			int intReturn;
			intReturn = -1;
			if (Strings.UCase(strCode) == "")
			{
				intReturn = -1;
			}
			else if (Strings.UCase(strCode) == "LOGIN")
			{
				intReturn = 0;
			}
			else if (Strings.UCase(strCode) == "NTLM")
			{
				intReturn = 1;
			}
			else if (Strings.UCase(strCode) == "CRAM-MD5")
			{
				intReturn = 2;
			}
			else if (Strings.UCase(strCode) == "PLAIN")
			{
				intReturn = 3;
			}
			else if (Strings.UCase(strCode) == "NONE")
			{
				intReturn = 5;
			}
			AuthCodeFromString = FCConvert.ToInt16(intReturn);
			return AuthCodeFromString;
		}

		public string AuthMethodDescription
		{
			get
			{
				string AuthMethodDescription = "";
				string strReturn = "";
				if (Strings.UCase(strAuthMethod) == "LOGIN")
				{
					strReturn = "Login";
				}
				else if (Strings.UCase(strAuthMethod) == "NTLM")
				{
					strReturn = "NTLM";
				}
				else if (Strings.UCase(strAuthMethod) == "CRAM-MD5")
				{
					strReturn = "CRAM-MD5";
				}
				else if (Strings.UCase(strAuthMethod) == "PLAIN")
				{
					strReturn = "Plain";
				}
				else if (Strings.UCase(strAuthMethod) == "NONE")
				{
					strReturn = "None";
				}
				else
				{
					strReturn = "Automatic";
				}
				AuthMethodDescription = strReturn;
				return AuthMethodDescription;
			}
		}
	}
}
