//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;
using fecherFoundation;

namespace Global
{
    /// <summary>
    /// Summary description for sarUTBookPageLDN.
    /// </summary>
    public partial class sarUTBookPageLDN : FCSectionReport
    {

        public sarUTBookPageLDN()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            if (_InstancePtr == null)
                _InstancePtr = this;
            this.Name = "sarUTBookPageAccount";
        }

        public static sarUTBookPageLDN InstancePtr
        {
            get
            {
                return (sarUTBookPageLDN)Sys.GetInstance(typeof(sarUTBookPageLDN));
            }
        }

        protected sarUTBookPageLDN _InstancePtr = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                rsData.Dispose();
            }
            base.Dispose(disposing);
        }

        // nObj = 1
        //   0	sarUTBookPageLDN	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               12/04/2006              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               12/04/2006              *
        // ********************************************************
        int lngAcct;
        clsDRWrapper rsData = new clsDRWrapper();
        string strWS;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
			eArgs.EOF = rsData.EndOfFile();
        }

        private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
        {
            switch (KeyCode)
            {

                case Keys.Escape:
                    {
                        this.Close();
                        break;
                    }
            } //end switch
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            string strSQL;
            string strQryFlds = "";
            // DELETE ME XXXXXXXXXXXXXXXXXX
            //modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);

            strWS = sarUTBookPageAccount.InstancePtr.strWS;

            this.Detail.ColumnCount = 2;
            lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
            strSQL = "SELECT * FROM (SELECT BillNumber, SLienRecordNumber, WLienRecordNumber FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID WHERE ActualAccountNumber = " + FCConvert.ToString(lngAcct) + ") AS Liens INNER JOIN DischargeNeeded ON Liens." + strWS + "LienRecordNumber = DischargeNeeded.LienKey";
            rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
            if (rsData.EndOfFile())
            {
                fldBookPage.Text = ""; // "No Matches"
                lblHeader.Visible = false;
                GroupHeader1.Height = 0;
            }
        }

		private void Detail_Format(object sender, EventArgs e)
        {
            BindFields();
        }

        private void BindFields()
        {
            try
            {   // On Error GoTo ERROR_HANDLER
                string strBook = "";
                string strPage = "";
                int intLienKey = 0;

                if (!rsData.EndOfFile())
                {
                    // TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
                    if (FCConvert.ToString(rsData.Get_Fields("Book")) != "")
                    {
                        // TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
                        strBook = "B" + rsData.Get_Fields("Book");
                    }
                    else
                    {
                        strBook = "B        ";
                    }
                    // TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
                    if (FCConvert.ToString(rsData.Get_Fields("Page")) != "")
                    {
                        // TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
                        strPage = "P" + rsData.Get_Fields("Page");
                    }
                    else
                    {
                        strPage = "P        ";
                    }
                    fldBookPage.Text = strBook + " " + strPage;
                    fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("DatePaid"), "MM/dd/yyyy");
                    fldYear.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BillNumber"));
                    intLienKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("LienKey"));
                    rsData.MoveNext();
                    CheckNext:;
                    if (!rsData.EndOfFile())
                    {
                        if (FCConvert.ToInt32(rsData.Get_Fields_Int32("LienKey")) == intLienKey)
                        {
                            fldYear.Text = fldYear.Text + ", " + rsData.Get_Fields_Int32("BillNumber");
                            rsData.MoveNext();
                            goto CheckNext;
                        }
                    }
                }
                return;
            }
            catch
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", "Error In Book Page Report - LN", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void sarUTBookPageLDN_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //sarUTBookPageLDN properties;
            //sarUTBookPageLDN.Caption	= "Account Detail";
            //sarUTBookPageLDN.Icon	= "sarUTBookPageLDN.dsx":0000";
            //sarUTBookPageLDN.Left	= 0;
            //sarUTBookPageLDN.Top	= 0;
            //sarUTBookPageLDN.Width	= 11880;
            //sarUTBookPageLDN.Height	= 8595;
            //sarUTBookPageLDN.StartUpPosition	= 3;
            //sarUTBookPageLDN.SectionData	= "sarUTBookPageLDN.dsx":058A;
            //End Unmaped Properties
        }
    }
}
