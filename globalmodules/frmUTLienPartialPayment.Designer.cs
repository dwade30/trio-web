﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmUTLienPartialPayment.
	/// </summary>
	partial class frmUTLienPartialPayment : BaseForm
	{
		public fecherFoundation.FCFrame fraVariables;
		public FCGrid vsData;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.fraVariables = new fecherFoundation.FCFrame();
			this.vsData = new fecherFoundation.FCGrid();
			this.lblName = new fecherFoundation.FCLabel();
			this.lblYear = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFileSaveExit = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraVariables)).BeginInit();
			this.fraVariables.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSaveExit)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFileSaveExit);
			this.BottomPanel.Location = new System.Drawing.Point(0, 427);
			this.BottomPanel.Size = new System.Drawing.Size(453, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraVariables);
			this.ClientArea.Size = new System.Drawing.Size(453, 367);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(453, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(186, 30);
			this.HeaderText.Text = "Partial Payment";
			// 
			// fraVariables
			// 
			this.fraVariables.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraVariables.Controls.Add(this.vsData);
			this.fraVariables.Controls.Add(this.lblName);
			this.fraVariables.Controls.Add(this.lblYear);
			this.fraVariables.Location = new System.Drawing.Point(30, 30);
			this.fraVariables.Name = "fraVariables";
			this.fraVariables.Size = new System.Drawing.Size(398, 325);
			this.fraVariables.TabIndex = 0;
			this.fraVariables.Text = "Confirm Data";
			// 
			// vsData
			// 
			this.vsData.AllowSelection = false;
			this.vsData.AllowUserToResizeColumns = false;
			this.vsData.AllowUserToResizeRows = false;
			this.vsData.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsData.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsData.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsData.BackColorBkg = System.Drawing.Color.Empty;
			this.vsData.BackColorFixed = System.Drawing.Color.Empty;
			this.vsData.BackColorSel = System.Drawing.Color.Empty;
			this.vsData.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsData.ColumnHeadersHeight = 30;
			this.vsData.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsData.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsData.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsData.DragIcon = null;
			this.vsData.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsData.FixedRows = 0;
			this.vsData.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsData.FrozenCols = 0;
			this.vsData.GridColor = System.Drawing.Color.Empty;
			this.vsData.GridColorFixed = System.Drawing.Color.Empty;
			this.vsData.Location = new System.Drawing.Point(20, 90);
			this.vsData.Name = "vsData";
			this.vsData.ReadOnly = true;
			this.vsData.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsData.RowHeightMin = 0;
			this.vsData.Rows = 1;
			this.vsData.ScrollTipText = null;
			this.vsData.ShowColumnVisibilityMenu = false;
			this.vsData.Size = new System.Drawing.Size(360, 221);
			this.vsData.StandardTab = true;
			this.vsData.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsData.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsData.TabIndex = 1;
			this.vsData.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsData_KeyPressEdit);
			this.vsData.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsData_BeforeEdit);
			this.vsData.CurrentCellChanged += new System.EventHandler(this.vsData_RowColChange);
			// 
			// lblName
			// 
			this.lblName.Location = new System.Drawing.Point(21, 60);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(345, 15);
			this.lblName.TabIndex = 3;
			// 
			// lblYear
			// 
			this.lblYear.Location = new System.Drawing.Point(20, 30);
			this.lblYear.Name = "lblYear";
			this.lblYear.Size = new System.Drawing.Size(130, 15);
			this.lblYear.TabIndex = 2;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileSaveExit,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileSaveExit
			// 
			this.mnuFileSaveExit.Index = 0;
			this.mnuFileSaveExit.Name = "mnuFileSaveExit";
			this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSaveExit.Text = "Save & Continue";
			this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 2;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdFileSaveExit
			// 
			this.cmdFileSaveExit.AppearanceKey = "acceptButton";
			this.cmdFileSaveExit.Location = new System.Drawing.Point(145, 30);
			this.cmdFileSaveExit.Name = "cmdFileSaveExit";
			this.cmdFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileSaveExit.Size = new System.Drawing.Size(160, 48);
			this.cmdFileSaveExit.TabIndex = 0;
			this.cmdFileSaveExit.Text = "Save & Continue";
			this.cmdFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// frmUTLienPartialPayment
			// 
			this.ClientSize = new System.Drawing.Size(453, 535);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmUTLienPartialPayment";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Partial Payment ";
			this.Load += new System.EventHandler(this.frmUTLienPartialPayment_Load);
			this.Activated += new System.EventHandler(this.frmUTLienPartialPayment_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUTLienPartialPayment_KeyPress);
			this.Resize += new System.EventHandler(this.frmUTLienPartialPayment_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraVariables)).EndInit();
			this.fraVariables.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSaveExit)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdFileSaveExit;
	}
}
