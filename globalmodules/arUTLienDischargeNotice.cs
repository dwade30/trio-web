﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWUT0000
using modGlobal = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for arUTLienDischargeNotice.
	/// </summary>
	public partial class arUTLienDischargeNotice : BaseSectionReport
	{
		// nObj = 1
		//   0	arUTLienDischargeNotice	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/16/2004              *
		// *
		// MODIFIED BY    :               Melissa Lamprecht       *
		// LAST UPDATED   :               09/18/2007              *
		// ********************************************************
		string strMainText;
		string strTreasurerName = "";
		DateTime dtPaymentDate;
		string strOwnerName = "";
		DateTime dtLienDate;
		string strBook = "";
		string strPage = "";
		string strCounty = "";
		string strHisHer = "";
		string strMuniName = "";
		int lngLineLen;
		string strSignerDesignation = "";
		string strSignerName = "";
		DateTime dtCommissionExpiration;
		string strTitle = "";
        private string strWS = "";
		DateTime dtTreasSignDate;
		bool boolUseAbatementLine;
		DateTime dtAppearedDate;
        bool boolWater;

        public arUTLienDischargeNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Discharge Notice";
		}

		public static arUTLienDischargeNotice InstancePtr
		{
			get
			{
				return (arUTLienDischargeNotice)Sys.GetInstance(typeof(arUTLienDischargeNotice));
			}
		}

		protected arUTLienDischargeNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void SetStrings()
		{
			string strWS = "";
			string strAppearedDate = "";
			// MAL@20070918
			if (dtAppearedDate.ToOADate() != 0)
			{
				strAppearedDate = Strings.Format(dtAppearedDate, "MMMM dd, yyyy");
			}
			else
			{
				strAppearedDate = Strings.StrDup(15, "_");
			}
			lngLineLen = 35;
			// this is how many underscores are in the printed lines
			lblTownHeader.Text = strMuniName;
			fldMuni.Text = lblTownHeader.Text;
			if (boolWater)
			{
				// kk12162015 trouts-154  Change WATER to STORMWATER for Bangor
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					lblTitleBar.Text = "DISCHARGE OF MORTGAGE FOR STORMWATER CERTIFICATE";
					strWS = "stormwater";
				}
				else
				{
					lblTitleBar.Text = "DISCHARGE OF MORTGAGE FOR WATER CERTIFICATE";
					strWS = "water";
				}
				// kk 01232013 trout-817  Remmove legal ref from Discharge Notices
				// lblLegalDescription.Text = "Title 38, M.R.S.A. Section 1208"
				lblLegalDescription.Text = "";
				// kk12162015    strWS = "water"
			}
			else
			{
				lblTitleBar.Text = "DISCHARGE OF MORTGAGE FOR SEWER CERTIFICATE";
				// lblLegalDescription.Text = "Title 38, M.R.S.A. Section 1208"
				lblLegalDescription.Text = "";
				strWS = "sewer";
			}
			strMainText = "\t" + "I, " + strTreasurerName + ", in my capacity as " + strTitle + " of the ";
			strMainText += strMuniName + ", hereby acknowledge that on " + Strings.Format(dtPaymentDate, "MMMM dd, yyyy");
			if (boolUseAbatementLine)
			{
				strMainText += " ________________________ granted an abatement of the tax obligation secured by the " + strWS + " lien against property assessed to ";
			}
			else
			{
				strMainText += " I received full payment and satisfaction of the debt secured by the " + strWS + " lien against property assessed to ";
			}
			strMainText += strOwnerName + " created by the recording of a " + strWS + " lien certificate dated ";
			strMainText += Strings.Format(dtLienDate, "MM/dd/yyyy") + " in Book " + strBook + ", at Page " + strPage + " of the ";
			strMainText += strCounty + " County Registry of Deeds, and in consideration thereof I hereby discharge said " + strWS + " lien.";
			fldMainText.Text = strMainText;
			fldTopDate.Text = "";
			// "Dated: " & Format(dtPaymentDate, "MMMM dd, yyyy")
			// fldSigLine.Text = String(lngLineLen, "_")
			fldSigTitle.Text = "";
			// "Treasurer"
			fldSigName.Text = strTreasurerName + ", " + strTitle;
			fldSigDate.Text = "Dated: " + Strings.Format(dtTreasSignDate, "MMMM dd, yyyy");
			// fldACKNOWLEDGEMENT = "State of Maine"
			fldACKNOWLEDGEMENT.Text = "ACKNOWLEDGEMENT";
			strMainText = strMuniName + "\r\n";
			strMainText += "State of Maine" + "\r\n" + strCounty + " County, ss." + "\r\n" + "\r\n";
			// strMainText = strMainText & "Personally appeared before me the above-named " & strTreasurerName        'MAL@20070918
			strMainText += "Personally appeared before me, on " + strAppearedDate + ", the above-named " + strTreasurerName;
			strMainText += ", who acknowledged the foregoing to be ";
			strMainText += strHisHer + " free act and deed in " + strHisHer + " capacity as " + strTitle + ".";
			fldBottomText.Text = strMainText;
			fldNotaryLine.Text = Strings.StrDup(lngLineLen, "_");
			if (strSignerName != "" && strSignerDesignation != "")
			{
				fldNotaryTitle.Text = strSignerName + ", " + strSignerDesignation;
				// "Notary Public / Attorney at Law"
			}
			else if (strSignerName != "")
			{
				fldNotaryTitle.Text = strSignerName;
			}
			else
			{
				fldNotaryTitle.Text = strSignerDesignation;
			}
			// fldNotaryPrintLine.Text = String(lngLineLen, "_")
			// fldNotaryPrintTitle.Text = strSignerName ' "Printed Name"
			// fldNotaryCommissionLine.Text = String(lngLineLen, "_")
			if (dtCommissionExpiration.ToOADate() != 0)
			{
				fldNotaryCommissionTitle.Text = "My commission expires: " + Strings.Format(dtCommissionExpiration, "MMMM dd, yyyy");
			}
			else
			{
				fldNotaryCommissionTitle.Text = "My commission expires: ";
			}
			// fldNotaryTitle.Text = strSignerName & ", " & strSignerDesignation  '"Notary Public / Attorney at Law"
			// fldNotaryPrintLine.Text = String(lngLineLen, "_")
			// fldNotaryPrintTitle.Text = strSignerName ' "Printed Name"
			// fldNotaryCommissionLine.Text = String(lngLineLen, "_")
			// fldNotaryCommissionTitle.Text = "My commission expires: " & Format(dtCommissionExpiration, "MMMM dd, yyyy")
		}

		public void Init(bool boolPassWater, string strPassTeasName, string strPassMuni, string strPassCounty, string strPassName1, string strPassName2, DateTime dtPassPayDate, DateTime dtPassLienDate, string strPassBook, string strPassPage, string strPassHisHer, string strPassSignerName, string strPassSignerDesignation, DateTime dtPassCommissionExpiration, bool modalDialog, string strPassMapLot = "", string strPassTitle = "Treasurer", int lngPassAccount = 0, DateTime? dtPassTreasSignDateTemp = null, DateTime? dtPassAppearedDateTemp = null, bool boolAbatementPayment = false)
		{
			DateTime dtPassTreasSignDate = dtPassTreasSignDateTemp ?? DateTime.Now;
			DateTime dtPassAppearedDate = dtPassAppearedDateTemp ?? DateTime.Now;
			// MAL@20070918: Added Appeared Date
			// this routine will set all of the variables needed
			if (boolPassWater)
			{
				boolWater = true;
				strWS = "W";
			}
			else
			{
				boolWater = false;
				strWS = "S";
			}
			// Row - 1  'Treasurer Name
			strTreasurerName = strPassTeasName;
			// Row - 2  'Muni Name
			strMuniName = strPassMuni;
			// Row - 3  'County
			strCounty = strPassCounty;
			// Row - 4  'Name1
			strOwnerName = strPassName1;
			// Row - 5  'Name2
			if (Strings.Trim(strPassName2) != "")
			{
				strOwnerName += " " + strPassName2;
			}
			// Row - 6  'Payment Date
			dtPaymentDate = dtPassPayDate;
			// Row - 7  'Filing Date
			dtLienDate = dtPassLienDate;
			// treasurer signing date
			dtTreasSignDate = dtPassTreasSignDate;
			// Row - 8  'Book
			strBook = strPassBook;
			if (Strings.Trim(strBook) == "" || Conversion.Val(strBook) == 0)
			{
				strBook = Strings.StrDup(10, "_");
			}
			// Row - 9  'Page
			strPage = strPassPage;
			if (Strings.Trim(strPage) == "" || Conversion.Val(strPage) == 0)
			{
				strPage = Strings.StrDup(10, "_");
			}
			// abatement used to discharge lien
			boolUseAbatementLine = boolAbatementPayment;
			// Row - 10 'His/Her
			strHisHer = Strings.LCase(strPassHisHer);
			if (Strings.Trim(strHisHer) == "")
			{
				strHisHer = "his/her";
			}
			// title
			strTitle = strPassTitle;
			// Row - 11 'Signer Name
			strSignerName = strPassSignerName;
			// Row - 12 'Signer's Designation
			strSignerDesignation = strPassSignerDesignation;
			// Row - 13 ' Commission expiration date
			dtCommissionExpiration = dtPassCommissionExpiration;
			// MAL@20070918
			// Appeared Date
			dtAppearedDate = dtPassAppearedDate;
			lblMapLot.Text = "Map Lot : " + strPassMapLot;
			// account number
			if (lngPassAccount > 0)
			{
				fldAccount.Text = modGlobal.PadToString_8(lngPassAccount, 5);
			}
			else
			{
				fldAccount.Text = "";
			}
			if (modGlobal.Statics.gdblLienSigAdjust != 0)
			{
				imgSig.Visible = true;
				imgSig.BringToFront();
				Line2.SendToBack();
				imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrUtilitySigPath);
				// kk02032016 Load the UT sig file  -   LoadPicture(gstrTreasSigPath)
				imgSig.Top += FCConvert.ToSingle(modGlobal.Statics.gdblLienSigAdjust);
				// and apply the signature adjustment
			}
			else
			{
				imgSig.Visible = false;
			}
			frmReportViewer.InstancePtr.Init(this, string.Empty, FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal:modalDialog);
			// kk10312014 trout-1093  Preview popping up behind receipt form
			frmUTLienDischargeNotice.InstancePtr.Unload();
			// Me.Show vbModal, MDIParent
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// SetVariables
			// kk05082014 trout-1082  Add print adjustments to UT
			// set the top and bottom margins
			// kk06122014 trout-10825  Conflicting Globals in CR - change gdblLDNAdjustment... to gdblUTLDNAdjustment...
			// kgk 11-4-2011 trocl-823  Print adjustments not working - added from arLienDischargeNotice
			// kk02042016 trout-1197  New margins effective 10/2015 - 1.5" Top 1st page, 1.5" Bottom last page
			this.PageSettings.Margins.Top = (FCConvert.ToSingle((1.5 * 1440) + (modUTCalculations.Statics.gdblUTLDNAdjustmentTop * 1440))) / 1440f;
			this.PageSettings.Margins.Bottom = (FCConvert.ToSingle((1.5 * 1440) + (modUTCalculations.Statics.gdblUTLDNAdjustmentBottom * 1440))) / 1440f;
			this.PageSettings.Margins.Left = (FCConvert.ToSingle((0.75 * 1440) + (modUTCalculations.Statics.gdblUTLDNAdjustmentSide * 1440))) / 1440f;
			this.PageSettings.Margins.Right = this.PageSettings.Margins.Left;
			this.PrintWidth = FCConvert.ToSingle(8.5) - (this.PageSettings.Margins.Left + this.PageSettings.Margins.Right);
			SetStrings();
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		private void arUTLienDischargeNotice_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arUTLienDischargeNotice.Caption	= "Lien Discharge Notice";
			//arUTLienDischargeNotice.Icon	= "arUTLienDischargeNotice.dsx":0000";
			//arUTLienDischargeNotice.Left	= 0;
			//arUTLienDischargeNotice.Top	= 0;
			//arUTLienDischargeNotice.Width	= 16995;
			//arUTLienDischargeNotice.Height	= 12135;
			//arUTLienDischargeNotice.StartUpPosition	= 3;
			//arUTLienDischargeNotice.SectionData	= "arUTLienDischargeNotice.dsx":058A;
			//End Unmaped Properties
		}
	}
}
