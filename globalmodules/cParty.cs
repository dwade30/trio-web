﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using SharedApplication.Extensions;

namespace Global
{
	public class cParty
	{
		//=========================================================
		private int lngID;
		private string strPartyGuid = string.Empty;
		private int intPartyType;
		private string strFirstName = string.Empty;
		private string strMiddleName = string.Empty;
		private string strLastName = string.Empty;
		private string strDesignation = string.Empty;
		private string strEmail = string.Empty;
		private string strWebAddress = string.Empty;
		private DateTime dtDateCreated;
		private string strCreatedBy = string.Empty;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private FCCollection AddColl = new FCCollection();
		//private FCCollection CommentColl = new FCCollection();
		//private FCCollection PhoneColl = new FCCollection();
		//private FCCollection ContactColl = new FCCollection();
		private FCCollection AddColl_AutoInitialized;

		private FCCollection AddColl
		{
			get
			{
				if (AddColl_AutoInitialized == null)
				{
					AddColl_AutoInitialized = new FCCollection();
				}
				return AddColl_AutoInitialized;
			}
			set
			{
				AddColl_AutoInitialized = value;
			}
		}

		private FCCollection CommentColl_AutoInitialized;

		private FCCollection CommentColl
		{
			get
			{
				if (CommentColl_AutoInitialized == null)
				{
					CommentColl_AutoInitialized = new FCCollection();
				}
				return CommentColl_AutoInitialized;
			}
			set
			{
				CommentColl_AutoInitialized = value;
			}
		}

		private FCCollection PhoneColl_AutoInitialized;

		private FCCollection PhoneColl
		{
			get
			{
				if (PhoneColl_AutoInitialized == null)
				{
					PhoneColl_AutoInitialized = new FCCollection();
				}
				return PhoneColl_AutoInitialized;
			}
			set
			{
				PhoneColl_AutoInitialized = value;
			}
		}

		private FCCollection ContactColl_AutoInitialized;

		private FCCollection ContactColl
		{
			get
			{
				if (ContactColl_AutoInitialized == null)
				{
					ContactColl_AutoInitialized = new FCCollection();
				}
				return ContactColl_AutoInitialized;
			}
			set
			{
				ContactColl_AutoInitialized = value;
			}
		}

		public void ClearIDs()
		{
			lngID = 0;
			if (AddColl.Count > 0)
			{
				foreach (cPartyAddress tAdd in AddColl)
				{
					tAdd.ID = 0;
				}
			}
			if (PhoneColl.Count > 0)
			{
				foreach (cPartyPhoneNumber tPhone in PhoneColl)
				{
					tPhone.ID = 0;
				}
			}
			if (CommentColl.Count > 0)
			{
				foreach (cPartyComment tComment in CommentColl)
				{
					tComment.ID = 0;
				}
			}
			if (ContactColl.Count > 0)
			{
				foreach (cContact tContact in ContactColl)
				{
					tContact.ID = 0;
					if (tContact.PhoneNumbers.Count > 0)
					{
						foreach (cContact tPhone in tContact.PhoneNumbers)
						{
							tPhone.ID = 0;
						}
					}
				}
			}
		}

		public void Clear()
		{
			AddColl.Clear();
			CommentColl.Clear();
			PhoneColl.Clear();
			ContactColl.Clear();
			strPartyGuid = "";
			intPartyType = 0;
			strFirstName = "";
			strMiddleName = "";
			strLastName = "";
			strDesignation = "";
			strEmail = "";
			strWebAddress = "";
			strCreatedBy = "";
			lngID = 0;
		}

		public cPartyAddress GetPrimaryAddress()
		{
			cPartyAddress GetPrimaryAddress = null;
			cPartyAddress tAddress;
			tAddress = null;
			if (AddColl.Count > 0)
			{
				cPartyAddress tAdd;
				foreach (cPartyAddress tAdd_foreach in AddColl)
				{
					tAdd = tAdd_foreach;
					if (Strings.LCase(tAdd.AddressType) == "primary")
					{
						tAddress = tAdd;
						break;
					}
					tAdd = null;
				}
				// tAdd
			}
			GetPrimaryAddress = tAddress;
			return GetPrimaryAddress;
		}

		public cPartyAddress GetAddress(string strProgModule = "", int lngModAccountID = 0, string strDate = "")
		{
			cPartyAddress GetAddress = null;
			cPartyAddress tAddress = new cPartyAddress();
			int intCurDay;
			int intCurMonth;
			bool boolSoFar = false;
			if (strDate == "")
			{
				strDate = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			}
			intCurDay = FCConvert.ToDateTime(strDate).Day;
			intCurMonth = FCConvert.ToDateTime(strDate).Month;
			if (AddColl.Count > 0)
			{
				cPartyAddress tAdd;
				bool boolBetterMatch = false;
				foreach (cPartyAddress tAdd_foreach in AddColl)
				{
					tAdd = tAdd_foreach;
					boolSoFar = true;
					// If LCase(strAddressType) <> LCase(tAdd.AddressType) And Trim(tAdd.AddressType) <> "" Then
					// boolSoFar = False
					// End If
					if (Strings.LCase(strProgModule) != Strings.LCase(tAdd.ProgModule) && Strings.Trim(tAdd.ProgModule) != "")
					{
						boolSoFar = false;
					}
					if (lngModAccountID != tAdd.ModAccountID && tAdd.ModAccountID != 0)
					{
						boolSoFar = false;
					}
					if (boolSoFar)
					{
						if (tAdd.Seasonal)
						{
							if (tAdd.StartMonth <= tAdd.EndMonth)
							{
								if (intCurMonth >= tAdd.StartMonth && intCurMonth <= tAdd.EndMonth)
								{
									if (intCurDay >= tAdd.StartDay || intCurMonth > tAdd.StartMonth)
									{
										if (!(intCurDay <= tAdd.EndDay || intCurMonth < tAdd.EndMonth))
										{
											boolSoFar = false;
										}
									}
									else
									{
										boolSoFar = false;
									}
								}
								else
								{
									boolSoFar = false;
								}
							}
							else
							{
								if (intCurMonth >= tAdd.StartMonth || intCurMonth <= tAdd.EndMonth)
								{
									if (intCurDay >= tAdd.StartDay || intCurMonth != tAdd.StartMonth)
									{
										if (!(intCurDay <= tAdd.EndDay || intCurMonth != tAdd.EndMonth))
										{
											boolSoFar = false;
										}
									}
									else
									{
										boolSoFar = false;
									}
								}
								else
								{
									boolSoFar = false;
								}
							}
						}
					}
					if (boolSoFar)
					{
						if (tAddress == null)
						{
							tAddress = tAdd;
						}
						else
						{
							// compare for best match
							boolBetterMatch = true;
							if (lngModAccountID != 0)
							{
								if (tAddress.ModAccountID == lngModAccountID && tAdd.ModAccountID != lngModAccountID)
								{
									boolBetterMatch = false;
								}
							}
							if (strProgModule != "")
							{
								if (Strings.LCase(tAddress.ProgModule) == Strings.LCase(strProgModule) && Strings.LCase(tAdd.ProgModule) != Strings.LCase(strProgModule))
								{
									boolBetterMatch = false;
								}
							}
							if (boolBetterMatch)
							{
								if (tAddress.Seasonal && !tAdd.Seasonal)
								{
									boolBetterMatch = false;
								}
							}
							// If strAddressType <> "" Then
							// If LCase(tAddress.AddressType) = LCase(strAddressType) And LCase(tAdd.AddressType) <> LCase(strAddressType) Then
							// boolBetterMatch = False
							// End If
							// End If
							if (boolBetterMatch)
							{
								tAddress = tAdd;
							}
						}
					}
					tAdd = null;
				}
				// tAdd
			}
			GetAddress = tAddress;
			return GetAddress;
		}

		public FCCollection GetModuleSpecificComments(string strProgModule = "")
		{
			FCCollection GetModuleSpecificComments = null;
			FCCollection tComments = new FCCollection();
			bool blnMatch = false;
			if (CommentColl.Count > 0)
			{
				foreach (cPartyComment tComm in CommentColl)
				{
					blnMatch = true;
					if (Strings.LCase(strProgModule) != Strings.LCase(tComm.ProgModule) && Strings.Trim(tComm.ProgModule) != "")
					{
						blnMatch = false;
					}
					if (blnMatch)
					{
						tComments.Add(tComm);
					}
				}
				// tComm
			}
			GetModuleSpecificComments = tComments;
			return GetModuleSpecificComments;
		}

		public int ID
		{
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
			set
			{
				lngID = value;
			}
		}

		public FCCollection Contacts
		{
			get
			{
				FCCollection Contacts = null;
				Contacts = ContactColl;
				return Contacts;
			}
		}

		public FCCollection PhoneNumbers
		{
			get
			{
				FCCollection PhoneNumbers = null;
				PhoneNumbers = PhoneColl;
				return PhoneNumbers;
			}
		}

		public FCCollection Comments
		{
			get
			{
				FCCollection Comments = null;
				Comments = CommentColl;
				return Comments;
			}
		}

		public string FullName
		{
			get
			{
				string FullName = "";
				FullName = Strings.Trim(Strings.Trim(Strings.Trim(Strings.Trim(strFirstName) + " " + strMiddleName) + " " + strLastName) + " " + strDesignation);
				return FullName;
			}
		}

		public string FullNameLastFirst
		{
			get
			{
				string FullNameLastFirst = "";
				if (Strings.Trim(strLastName) != "")
				{
					FullNameLastFirst = Strings.Trim(Strings.Trim(Strings.Trim(strLastName) + ", " + strFirstName + " " + strMiddleName) + " " + strDesignation);
				}
				else
				{
					FullNameLastFirst = Strings.Trim(Strings.Trim(strFirstName + " " + strMiddleName) + " " + strDesignation);
				}
				return FullNameLastFirst;
			}
		}

        public string FullNameMiddleInitial
        {
            get
            {
                string FullName = "";
                FullName = Strings.Trim(Strings.Trim(Strings.Trim(Strings.Trim(strFirstName) + " " + strMiddleName.Left(1)) + " " + strLastName) + " " + strDesignation);
                return FullName;
            }
        }
		public FCCollection Addresses
		{
			get
			{
				FCCollection Addresses = null;
				Addresses = AddColl;
				return Addresses;
			}
		}

		public string PartyGUID
		{
			get
			{
				string PartyGUID = "";
				PartyGUID = strPartyGuid;
				return PartyGUID;
			}
			set
			{
				strPartyGuid = value;
			}
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public int PartyType
		{
			get
			{
				return intPartyType;
			}
			set
			{
				intPartyType = value;
			}
		}

		public string FirstName
		{
			get
			{
				string FirstName = "";
				FirstName = strFirstName;
				return FirstName;
			}
			set
			{
				strFirstName = value;
			}
		}

		public string MiddleName
		{
			get
			{
				string MiddleName = "";
				MiddleName = strMiddleName;
				return MiddleName;
			}
			set
			{
				strMiddleName = value;
			}
		}

		public string LastName
		{
			get
			{
				string LastName = "";
				LastName = strLastName;
				return LastName;
			}
			set
			{
				strLastName = value;
			}
		}

		public string Designation
		{
			get
			{
				string Designation = "";
				Designation = strDesignation;
				return Designation;
			}
			set
			{
				strDesignation = value;
			}
		}

		public string Email
		{
			get
			{
				string Email = "";
				Email = strEmail;
				return Email;
			}
			set
			{
				strEmail = value;
			}
		}

		public string WebAddress
		{
			get
			{
				string WebAddress = "";
				WebAddress = strWebAddress;
				return WebAddress;
			}
			set
			{
				strWebAddress = value;
			}
		}

		public DateTime DateCreated
		{
			get
			{
				DateTime DateCreated = System.DateTime.Now;
				DateCreated = dtDateCreated;
				return DateCreated;
			}
			set
			{
				dtDateCreated = value;
			}
		}

		public string CreatedBy
		{
			get
			{
				string CreatedBy = "";
				CreatedBy = strCreatedBy;
				return CreatedBy;
			}
			set
			{
				strCreatedBy = value;
			}
		}

		public cParty() : base()
		{
			Clear();
		}
	}
}
