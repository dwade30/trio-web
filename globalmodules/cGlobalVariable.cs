﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using System;

namespace Global
{
	public class cGlobalVariable
	{
		//=========================================================
		private int lngID;
		private string strMuniName = "";
		private string strCityTown = "";
		private bool boolUseSecurity;
		private DateTime dtMaxDate;
		private bool boolDefaultPermission;
		private int intViolations;
		private string strReleaseNumber = "";
		private string strArchiveYear = "";
		private int intMenuBackColor;
		private bool boolUseRegistryOnly;
		//private string strFTPAddress = "";
		private string strFTPPassword = "";
		private string strFTPUser = "";
		private string strBulkMail1 = "";
		private string strBulkMail2 = "";
		private string strBulkMail3 = "";
		private string strBulkMail4 = "";
		private string strBulkMail5 = "";
		private bool boolUseMultipleTown;
		private string strSMTPServer = "";
		private string strSMTPUser = "";
		private string strSMTPPassword = "";
		private string strUserEmail = "";
		private int intAuthorizationType;
		private DateTime dtUpdateDate;
		private bool boolUpdateNotification;
		private bool boolSetUpdateNotification;
		private int lngPort;
		private bool boolUseSSL;
		private string strEmailFromName = string.Empty;

		public string FromName
		{
			set
			{
				strEmailFromName = value;
			}
			get
			{
				string FromName = "";
				FromName = strEmailFromName;
				return FromName;
			}
		}

		public bool UseSSL
		{
			set
			{
				boolUseSSL = value;
			}
			get
			{
				bool UseSSL = false;
				UseSSL = boolUseSSL;
				return UseSSL;
			}
		}

		public int SMTPPort
		{
			set
			{
				lngPort = value;
			}
			get
			{
				int SMTPPort = 0;
				SMTPPort = lngPort;
				return SMTPPort;
			}
		}

		public int ID
		{
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
			set
			{
				lngID = value;
			}
		}

		public string MuniName
		{
			get
			{
				string MuniName = "";
				MuniName = strMuniName;
				return MuniName;
			}
			set
			{
				strMuniName = value;
			}
		}

		public string CityTown
		{
			get
			{
				string CityTown = "";
				CityTown = strCityTown;
				return CityTown;
			}
			set
			{
				strCityTown = value;
			}
		}

		public bool UseSecurity
		{
			get
			{
				bool UseSecurity = false;
				UseSecurity = boolUseSecurity;
				return UseSecurity;
			}
			set
			{
				boolUseSecurity = value;
			}
		}

		public DateTime MaxDate
		{
			get
			{
				DateTime MaxDate = System.DateTime.Now;
				MaxDate = dtMaxDate;
				return MaxDate;
			}
			set
			{
				dtMaxDate = value;
			}
		}

		public bool DefaultPermission
		{
			get
			{
				bool DefaultPermission = false;
				DefaultPermission = boolDefaultPermission;
				return DefaultPermission;
			}
			set
			{
				boolDefaultPermission = value;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Violations
		{
			get
			{
				short Violations = 0;
				Violations = FCConvert.ToInt16(intViolations);
				return Violations;
			}
			set
			{
				intViolations = value;
			}
		}

		public string ReleaseNumber
		{
			get
			{
				string ReleaseNumber = "";
				ReleaseNumber = strReleaseNumber;
				return ReleaseNumber;
			}
			set
			{
				strReleaseNumber = value;
			}
		}

		public string ArchiveYear
		{
			get
			{
				string ArchiveYear = "";
				ArchiveYear = strArchiveYear;
				return ArchiveYear;
			}
			set
			{
				strArchiveYear = value;
			}
		}

		public int MenuBackColor
		{
			get
			{
				int MenuBackColor = 0;
				MenuBackColor = intMenuBackColor;
				return MenuBackColor;
			}
			set
			{
				intMenuBackColor = value;
			}
		}

		public bool UseRegistryOnly
		{
			get
			{
				bool UseRegistryOnly = false;
				UseRegistryOnly = boolUseRegistryOnly;
				return UseRegistryOnly;
			}
			set
			{
				boolUseRegistryOnly = value;
			}
		}

		//public string FTPAddress
		//{
		//	get
		//	{
		//		string FTPAddress = "";
		//		FTPAddress = strFTPAddress;
		//		return FTPAddress;
		//	}
		//	set
		//	{
		//		strFTPAddress = value;
		//	}
		//}

		public string FTPPassword
		{
			get
			{
				string FTPPassword = "";
				FTPPassword = strFTPPassword;
				return FTPPassword;
			}
			set
			{
				strFTPPassword = value;
			}
		}

		public string FTPUser
		{
			get
			{
				string FTPUser = "";
				FTPUser = strFTPUser;
				return FTPUser;
			}
			set
			{
				strFTPUser = value;
			}
		}

		public string BulkMail1
		{
			get
			{
				string BulkMail1 = "";
				BulkMail2 = strBulkMail1;
				return BulkMail1;
			}
			set
			{
				strBulkMail1 = value;
			}
		}

		public string BulkMail2
		{
			get
			{
				string BulkMail2 = "";
				BulkMail2 = strBulkMail2;
				return BulkMail2;
			}
			set
			{
				strBulkMail2 = value;
			}
		}

		public string BulkMail3
		{
			get
			{
				string BulkMail3 = "";
				BulkMail3 = strBulkMail3;
				return BulkMail3;
			}
			set
			{
				strBulkMail3 = value;
			}
		}

		public string BulkMail4
		{
			get
			{
				string BulkMail4 = "";
				BulkMail4 = strBulkMail4;
				return BulkMail4;
			}
			set
			{
				strBulkMail4 = value;
			}
		}

		public string BulkMail5
		{
			get
			{
				string BulkMail5 = "";
				BulkMail5 = strBulkMail5;
				return BulkMail5;
			}
			set
			{
				strBulkMail5 = value;
			}
		}

		public bool UseMultipleTown
		{
			get
			{
				bool UseMultipleTown = false;
				UseMultipleTown = boolUseMultipleTown;
				return UseMultipleTown;
			}
			set
			{
				boolUseMultipleTown = value;
			}
		}

		public string SMTPServer
		{
			get
			{
				string SMTPServer = "";
				SMTPServer = strSMTPServer;
				return SMTPServer;
			}
			set
			{
				strSMTPServer = value;
			}
		}

		public string SMTPUser
		{
			get
			{
				string SMTPUser = "";
				SMTPUser = strSMTPUser;
				return SMTPUser;
			}
			set
			{
				strSMTPUser = value;
			}
		}

		public string SMTPPassword
		{
			get
			{
				string SMTPPassword = "";
				SMTPPassword = strSMTPPassword;
				return SMTPPassword;
			}
			set
			{
				strSMTPPassword = value;
			}
		}

		public string UserEmail
		{
			get
			{
				string UserEmail = "";
				UserEmail = strUserEmail;
				return UserEmail;
			}
			set
			{
				strUserEmail = value;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short AuthorizationType
		{
			get
			{
				short AuthorizationType = 0;
				AuthorizationType = FCConvert.ToInt16(intAuthorizationType);
				return AuthorizationType;
			}
			set
			{
				intAuthorizationType = value;
			}
		}

		public DateTime UpdateDate
		{
			get
			{
				DateTime UpdateDate = System.DateTime.Now;
				UpdateDate = dtUpdateDate;
				return UpdateDate;
			}
			set
			{
				dtUpdateDate = value;
			}
		}

		public bool UpdateNotification
		{
			get
			{
				bool UpdateNotification = false;
				UpdateNotification = boolUpdateNotification;
				return UpdateNotification;
			}
			set
			{
				boolUpdateNotification = value;
			}
		}

		public bool SetUpdateNotification
		{
			get
			{
				bool SetUpdateNotification = false;
				SetUpdateNotification = boolSetUpdateNotification;
				return SetUpdateNotification;
			}
			set
			{
				boolSetUpdateNotification = value;
			}
		}
	}
}
