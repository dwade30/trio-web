﻿using System;
using System.Drawing.Printing;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using GrapeCity.ActiveReports;
using Wisej.Web;
using iTextSharp.text.pdf;
using TWSharedLibrary;
using Wisej.Core;
//using System.IO.Compression;

#if TWCL0000
using TWCL0000;


#elif TWFA0000
using TWFA0000;


#elif TWAR0000
using TWAR0000;


#elif TWBL0000
using TWBL0000;


#elif TWRE0000
using TWRE0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmReportViewer.
	/// </summary>
	public partial class frmReportViewer : BaseForm
	{
		public frmReportViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;

            base.Cancelled += Report_Cancelled;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReportViewer InstancePtr
		{
			get
			{
				return (frmReportViewer)Sys.GetInstance(typeof(frmReportViewer));
			}
		}

		protected frmReportViewer _InstancePtr = null;

		bool boolShowPrintDialog;
		bool boolPrintDuplex;
		bool boolDuplexForcePairs;
		string strDuplexLabel;
		bool boolUseFilename;
		int intCopies;
		bool pboolOverRideDuplex;
		int intFormModal;
		bool boolEmail;
		string strEmailAttachmentName;
		bool boolAutoPrintDuplex;
		bool boolCantChoosePages;
		bool blnDontClose;
		private bool cancelled = false;

		private FCSectionReport currentReport = null;

        // VBto upgrade warning: rptObj As object	OnWrite(rptTaxAcquiredProperties, rptLoadbackReportMaster, arTextDump, ar30DayNotice, arLienProcess, arLienMaturity, arLienProcessEditReport, rptOutstandingPeriodBalances, rptOutprintingFormat, arLienTransferReport, rptInterestedPartyMasterList, rptCustomForms, rptReminderNoticeLabels, rptCustomMHLabels, rptDemandFeeList, rptReminderLaserPostCard, rptReminderForm, rptCLDailyAuditMaster, rptReminderMailer, rptTaxClubOutstanding, rptTaxClubMaster, rptTaxServiceSummary, rptLienDischargeBP, rptTaxClubBooklet, rptTaxClubActivity, rptLienDateLine, rptCustomLabels, rptCertificateOfSettlement, rptCertificateOfRecommitment, arLienPartialPayment, rptTaxRateReport, rptREIntParty, rptMHWithREAccounts, arWaiverOfForeclosure, rptBankruptcy, rptPurge, rptLienRemovalPayments, arStatusLists, rptOutstandingBalances, rptOutstandingLienBalances, rptOutstandingBalancesAll, arLienStatusReport, rptStatusListAccountDetail, rptAuditStatusAll, rptBankruptAccounts, rptAccountInformationSheet, arBatchListing, arLienDischargeNotice, rptREbyHolderSplit, arBookPageAccount, rptGroupInformation, arAllLienDischargeNotice, rptUpdatedAddressList, rptTaxAcquiredList, rptReminderPostCard, rptLienMaturityFees, rptMHWithUTAccounts, rptUTbyHolder, rptUTIntParty, arAccountDetail, arCLInformationScreen, rptREbyHolder, rptMortgageHolderMasterList, rptREMortgage, rptUTMortgage, rptUpdatedBookPage, rptPrintUnprintedPartialPaymentWaivers, rptEmailContacts, rptMultiModulePosting)
        public void Init(string strFileName = "", string strFileTitle = "TRIO Software",
            bool boolOverRideDuplex = false, bool boolAllowEmail = true, string strAttachmentName = "",
            bool boolDontAllowPagesChoice = false, bool blnDontAllowCloseTilReportDone = false, bool showModal = false,
            bool showProgressDialog = true, bool allowCancel = false)
        {
            Init(null,"",0,false,false,"Pages",false,
                strFileName , strFileTitle,boolOverRideDuplex, boolAllowEmail, strAttachmentName , boolDontAllowPagesChoice, blnDontAllowCloseTilReportDone , showModal , showProgressDialog, allowCancel);
        }
        public void Init(FCSectionReport rptObj = null, string strPrinter = "", int intModal = 0, bool boolDuplex = false, bool boolDuplexPairsMandatory = false, string strDuplexTitle = "Pages", bool boolLandscape = false, string strFileName = "", string strFileTitle = "TRIO Software", bool boolOverRideDuplex = false, bool boolAllowEmail = true, string strAttachmentName = "", bool boolDontAllowPagesChoice = false, bool blnDontAllowCloseTilReportDone = false, bool showModal = false, bool showProgressDialog = true, bool allowCancel = false)
		{

            // if printing duplex and it is a normal type report, boolDuplexForcePairs should be false
            // if printing duplex and Front and Back pages must be paired the way they are in the report (such as property cards) set boolDuplexForcePairs = true
            // set the duplex title if pages is not a good description for what is printing (such as property cards)
            // strAttachment name is the name to call the attachment.  It will be created with a meaningless temp name.  This will be the filename the email receiver will see
            try
			{
				// On Error GoTo ErrorHandler
				string strTemp = "";
				boolCantChoosePages = boolDontAllowPagesChoice;
				boolAutoPrintDuplex = false;
                boolPrintDuplex = boolDuplex;
                pboolOverRideDuplex = boolOverRideDuplex;
                boolDuplexForcePairs = boolDuplexPairsMandatory;
                strDuplexLabel = strDuplexTitle;
                strEmailAttachmentName = strAttachmentName;
				intCopies = 1;
				boolEmail = boolAllowEmail;
				blnDontClose = blnDontAllowCloseTilReportDone;
				boolUseFilename = false;
                currentReport = rptObj;
                cancelled = false;

				if (strFileName != "")
				{
					// make sure that the file exists
					if (File.Exists(strFileName))
					{
						//FC:FINAL:SBE - #i340 - set report name instead of loading to ReportSource.Document
						//ARViewer21.ReportSource.Document.Load(strFileName);
						ARViewer21.ReportName = strFileName;
						boolUseFilename = true;
						this.Text = strFileTitle;
					}
					else
					{
						return;
					}
				}
				else
				{
					// load the report from the object passed in
					this.Text = rptObj.Name;
					//FC:FINAL:SBE - #96 - dispose current viewer, and create a new one when ReportSource is changed
					if (ARViewer21.ReportSource != null)
					{
                        if (!ARViewer21.ReportSource.Disposing)
                        {
                            ARViewer21.ReportSource.Dispose();
                        }
						ARViewer21.Dispose();
						ARViewer21 = null;
						ARViewer21 = new ARViewer();
						ARViewer21.Dock = DockStyle.Fill;
						this.ClientArea.Controls.Add(ARViewer21);
						this.ClientArea.Controls.SetChildIndex(ARViewer21, 0);
					}
                    //FC:FINAL:SBE - #2519 - run report asynchronously to avoid thread abort exception for long running Reports
                    // THIS CAN LEAVE ARViewer21.ReportSource NULL FOR AN INDETERMINATE PERIOD OF TIME
                    // WHICH CAN CAUSE AN EXCEPTION BELOW ON ARViewer21.Printer
                    if (showProgressDialog)
                    {
                        rptObj.PageEnd += (s, e) =>
                        {
                            if (rptObj.FetchDataStopped)
                            {
                                return;
                            }
                            this.UpdateWait($"Running report ... Page {rptObj.PageNumber}");
                        };
                        this.ShowWait(disableformClosing: false, enableCancel: allowCancel);


                        Application.StartTask(() =>
                        {
                            try
                            {
                                ARViewer21.ReportSource = rptObj;
                                //FC:FINAL:AM:#3832 - check if report has been already generated (by calling Run)
                                if (rptObj.State != SectionReport.ReportState.Completed)
                                {
                                    rptObj.RunReport();
                                }
                            }
                            finally
                            {
								if (!cancelled)
								{
                                    this.EndWait();
                                    SetprinterOptions(strPrinter, boolLandscape);
								}
                            }

                        });
                    }
                    else
                    {
                        ARViewer21.ReportSource = rptObj;
                    }
				}

				if (boolEmail && Strings.Trim(strEmailAttachmentName) == string.Empty)
				{

					strTemp = this.Text;
					strTemp = Strings.Replace(strTemp, "/", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Replace(strTemp, "\\", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Replace(strTemp, ",", "", 1, -1, CompareConstants.vbTextCompare);
					strTemp = Strings.Replace(strTemp, " ", "", 1, -1, CompareConstants.vbTextCompare);
					strEmailAttachmentName = strTemp;
				}
				
                if (!showProgressDialog)
                {
                    SetprinterOptions(strPrinter, boolLandscape);
                }
				intFormModal = intModal;
				
				//FC:FINAL:SBE - add new flag for modal. Show report viewer as modal, when it is opened from the main screen (Gear menu)
				if (showModal)
				{
                    this.Show(FormShowEnum.Modal);
				}
				else
				{
					this.Show(App.MainForm);
                    //FC:FINAL:SBE - #3913, #3969, #3993, #3996, #4045  - update client side, to force active form
                    FCUtils.ApplicationUpdate(App.MainForm);
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In frmReportViewer_Init", MsgBoxStyle.Critical, "Error");
			}
		}

        private void SetprinterOptions(string strPrinter, bool boolLandscape)
        {
            if (strPrinter != string.Empty)
            {
                if (Strings.UCase(ARViewer21.Printer.PrinterName) != Strings.UCase(strPrinter))
                {
                    ARViewer21.Printer.PrinterName = strPrinter;
                }

                boolShowPrintDialog = false;
                if (boolLandscape)
                {
                    ARViewer21.Printer.PrinterSettings.DefaultPageSettings.Landscape = true;
                }
            }
            else
            {
                boolShowPrintDialog = true;
            }
        }

        private void Report_Cancelled(object sender, EventArgs e)
        {
            cancelled = true;
			currentReport.StopFetchingData();
            Close();
        }

		private void ARViewer21_KeyDown(ref Keys KeyCode, ref short Shift)
		{
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}


		private void frmReportViewer_Activated(object sender, System.EventArgs e)
		{

		}

		private void frmReportViewer_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmReportViewer_Load(object sender, System.EventArgs e)
		{
			this.Top = App.MainForm.MdiClient.Top + 46;
			this.Left = App.MainForm.MdiClient.Left;
			this.Width = App.MainForm.MdiClient.Width;
			this.Height = App.MainForm.MdiClient.Height - 46;
			// Call SetFixedSize(Me, TRIOWINDOWSIZEBIGGIE)
			int cnt;
			int lngID = 0;			
			if (boolEmail)
			{
				mnuEmail.Visible = true;
				toolBarButtonEmailPDF.Visible = true;
				toolBarButtonEmailRTF.Visible = true;
			}
			else
			{
				mnuEmail.Visible = false;
				toolBarButtonEmailPDF.Visible = false;
				toolBarButtonEmailRTF.Visible = false;
			}
		}
		// VBto upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (blnDontClose)
				{
					if (!(ARViewer21.ReportSource == null))
					{
						if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
						{
							FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
							e.Cancel = true;
							return;
						}
					}
				}
				
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				if (Information.Err(ex).Number == 91)
				{
					// do nothing
				}
				else
				{
					FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In Report Viewer Unload", MsgBoxStyle.Exclamation, "Error");
				}
			}
		}

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            try
            {
                // On Error GoTo ErrorHandler
                if (!boolUseFilename)
                {
                    //VBtoConverter.UnloadControl(ARViewer21.ReportSource);
                    if (ARViewer21.ReportSource != null && !cancelled)
                    {
                        ARViewer21.ReportSource.StopFetchingData();
                    }
                }

                if (this.Text.ToLower().Contains("checks"))
                    MessageBox.Show("Remove checks from printer before continuing.", "TRIO Software", MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In Report Viewer Unload", MsgBoxStyle.Exclamation, "Error");
            }
            base.OnFormClosing(e);
        }

        //FC:FINAL:SBE - #70 - move code from FormClosing to FormClosed. If other forms are displayed during form closing, the Visible property is wrong
        protected override void OnFormClosed(FormClosedEventArgs e)
		{
			base.OnFormClosed(e);
			try
			{
                // On Error GoTo ErrorHandler
                //if (!boolUseFilename)
                //{
                    //VBtoConverter.UnloadControl(ARViewer21.ReportSource);
                    if (ARViewer21.ReportSource != null)
                    {
                        //ARViewer21.ReportSource.StopFetchingData();
                        ARViewer21.ReportSource.Document?.Dispose();
                        if (!ARViewer21.ReportSource.Disposing)
                        {
                            ARViewer21.ReportSource.Dispose();
                        }
                        ARViewer21.ReportSource = null;
                    }
                //}

                System.Diagnostics.Process pc = System.Diagnostics.Process.GetCurrentProcess();
                pc.MaxWorkingSet = pc.MinWorkingSet;
				// Doevents
				//FC:FINAL:AM: if another form is opened when the report viewer is closed this won't be activated correctly
				//if (App.MainForm.Enabled && App.MainForm.Visible)
				//{
				//	App.MainForm.Focus();
				//}
				//return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				if (Information.Err(ex).Number == 91)
				{
					// do nothing
				}
				else
				{
					FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In Report Viewer Unload", MsgBoxStyle.Exclamation, "Error");
				}
			}
		}

		private void mnuEmailPDF_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
				string strList = "";
				//FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
				string fileName = string.Empty;
				FCSectionReport report = null;
				if (!Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
				{
					Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
				}
				//string fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + ARViewer21.ReportSource.Name + ".pdf";
				//FC:FINAL:RPU:#1363 - If the ReportSource is null load the report name from frmReportViewer
                this.Controls.Add(new FCLabel {Text = $"*** Create pdf start"});
                //FCMessageBox.Show($"*** Create pdf start", MsgBoxStyle.Information, "App Debugging - Ignore");
				if (ARViewer21.ReportSource != null)
				{
					//FC:FINAL:DSE Exception if report name contains sensitive characters
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".pdf";
                    this.Controls.Add(new FCLabel {Text = $"*** filename = {fileName}"});
                    //FCMessageBox.Show($"*** filename = {fileName}", MsgBoxStyle.Information, "App Debugging - Ignore");
					report = ARViewer21.ReportSource;
				}
				else
				{
					report = new FCSectionReport();

                    if (ARViewer21.ReportName != null)
                    {
                        this.Controls.Add(new FCLabel {Text = $"*** viewer report name = {ARViewer21.ReportName}"});
                        //FCMessageBox.Show($"*** viewer report name = {ARViewer21.ReportName}", MsgBoxStyle.Information, "App Debugging - Ignore");

                        report.Document.Load(ARViewer21.ReportName);
                    }
                    else
                    {
                        this.Controls.Add(new FCLabel {Text = $"*** viewer report name is null"});
                       // FCMessageBox.Show($"*** viewer report name is null", MsgBoxStyle.Information, "App Debugging - Ignore");

                    }

                    //FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(this.Text) + ".pdf";
                    this.Controls.Add(new FCLabel {Text = $"*** filename = {fileName}"});
                    //FCMessageBox.Show($"*** filename = {fileName}", MsgBoxStyle.Information, "App Debugging - Ignore");

				}
				//FC:FINAL:RPU:#1363 - Use report.Document
				//a.Export(ARViewer21.ReportSource.Document, fileName);
				a.Export(report.Document, fileName);
                //FCMessageBox.Show($"*** exported", MsgBoxStyle.Information, "App Debugging - Ignore");

				// FC:FINAL: RPU:#1363 - No more need for report
				report = null;
				if (!File.Exists(fileName))
				{
					FCMessageBox.Show("Could not create report file to send as attachment", MsgBoxStyle.Exclamation, "Cannot E-mail");
                }
				else
				{
					if (Strings.Trim(strEmailAttachmentName) != string.Empty)
					{
						strList = fileName + ";" + strEmailAttachmentName + ".pdf";
					}
					else
					{
						strList = fileName + Strings.Replace(fileName, ".tmp", "") + "pdf";
					}
					frmEMail.InstancePtr.Init(strList, "", "", "", false, true, true, showAsModalForm: true);
				}
            }
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In mnuEmailPDF_Click");
			}
		}

		private void mnuEMailRTF_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
				string strList = "";
				//FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
				string fileName = string.Empty;
				FCSectionReport report = null;
				if (!Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
				{
					Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
				}
				//string fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + ARViewer21.ReportSource.Document.Name + ".rtf";
				//FC:FINAL:RPU:#1363 - If the ReportSource is null load the report name from frmReportViewer
				if (!(ARViewer21.ReportSource == null))
				{
					//FC:FINAL:DSE:#1802 Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".rtf";
					report = ARViewer21.ReportSource;
				}
				else
				{
					report = new FCSectionReport();
					report.Document.Load(ARViewer21.ReportName);
					//FC:FINAL:DSE:#1802 Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(this.Text) + ".rtf";
				}
				//FC:FINAL:RPU:#1363 - Use report.Document
				//a.Export(ARViewer21.ReportSource.Document, fileName);
				a.Export(report.Document, fileName);
				// FC:FINAL: RPU:#1363 - No more need for report
				report = null;
				if (!File.Exists(fileName))
				{
					FCMessageBox.Show("Could not create report file to send as attachment", MsgBoxStyle.Exclamation, "Cannot E-mail");
					return;
				}
				else
				{
					if (Strings.Trim(strEmailAttachmentName) != string.Empty)
					{
						strList = fileName + ";" + strEmailAttachmentName + ".rtf";
					}
					else
					{
						strList = fileName + Strings.Replace(fileName, ".tmp", "") + "pdf";
					}
					frmEMail.InstancePtr.Init(strList, "", "", "", false, true, true, showAsModalForm: true);
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In mnuEmailRTF_Click");
			}
		}

		private void mnuExcel_Click(object sender, System.EventArgs e)
		{
			try
			{
				string strOldDir;
				string strFileName = "";
				string strFullName = "";
				string strPathName = "";
				//FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
				FCSectionReport report = null;
				string fileName = string.Empty;
				if (!(ARViewer21.ReportSource == null))
				{
					if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
					{
						FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
						return;
					}
				}
				//strOldDir = Application.StartupPath;
				//Information.Err(ex).Clear();
				//App.MainForm.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNNoChangeDir;
				//// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//App.MainForm.CommonDialog1.CancelError = true;
				///*? On Error Resume Next  */
				//App.MainForm.CommonDialog1.FileName = "";
				//App.MainForm.CommonDialog1.Filter = "*.xls";
				//App.MainForm.CommonDialog1.DefaultExt = "xls";
				//App.MainForm.CommonDialog1.InitDir = Application.StartupPath;
				//App.MainForm.CommonDialog1.ShowSave();
				//if (Information.Err(ex).Number == 0)
				//{
				//	strFileName = Path.GetFileNameWithoutExtension(App.MainForm.CommonDialog1.FileName);
				//	// name without extension
				//	strFullName = Path.GetFileName(App.MainForm.CommonDialog1.FileName);
				//	// name with extension
				//	strPathName = Directory.GetParent(App.MainForm.CommonDialog1.FileName).FullName;
				//	if (Strings.Right(strPathName, 1) != "\\")
				//		strPathName += "\\";
				//	GrapeCity.ActiveReports.Export.Excel.Section.XlsExport a = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
				//	if (!Directory.Exists(strPathName + strFileName))
				//	{
				//		Directory.CreateDirectory(strPathName + strFileName);
				//		strPathName += strFileName + "\\";
				//	}
				//	// a.FileName = .FileName
				//	//a.FileName = strPathName + strFullName;
				//	string fileName = strPathName = strFullName;
				//	//a.Version = 8;
				//	a.Export(ARViewer21.ReportSource.Document, fileName);
				//	if (!File.Exists(fileName))
				//	{
				//		FCMessageBox.Show("No file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that the directory is not write protected and that there is enough free space to store the file", MsgBoxStyle.Exclamation, "Not Successful");
				//		// MsgBox "No file created" & vbNewLine & "Export not successful", MsgBoxStyle.Exclamation, "Not Exported"
				//	}
				//	else
				//	{
				//		FileInfo fl = new FileInfo(fileName);
				//		if (FCConvert.ToInt32(fl.Length) > 0)
				//		{
				//			FCMessageBox.Show("Export to file  " + App.MainForm.CommonDialog1.FileName + "  was completed successfully.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
				//		}
				//		else
				//		{
				//			FCMessageBox.Show("Empty file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that there is enough free space to create the file", MsgBoxStyle.Exclamation, "Not Exported");
				//		}
				//	}
				//}
				//else
				//{
				//	FCMessageBox.Show("There was an error saving the file.", MsgBoxStyle.Critical, null);
				//}
				//// ChDrive strOldDir
				//// ChDir strOldDir
				//modAPIsConst.SetCurrentDirectoryWrp(ref strOldDir);
				GrapeCity.ActiveReports.Export.Excel.Section.XlsExport a = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
				//string fileName = ARViewer21.ReportSource.Name + ".xls";
				//FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
				if (!(ARViewer21.ReportSource == null))
				{
					report = ARViewer21.ReportSource;
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".xls";
				}
				else
				{
					report = new FCSectionReport();
					report.Document.Load(ARViewer21.ReportName);
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = FCUtils.FixFileName(this.Text) + ".xls";
				}
				using (MemoryStream stream = new MemoryStream())
				{
					//FC:FINAL:RPU:#1363 - Use report.Document
					//a.Export(ARViewer21.ReportSource.Document, stream);
					a.Export(report.Document, stream);
					stream.Position = 0;
					//PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
					//FCUtils.Download(stream, fileName);
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
					// FC:FINAL: RPU:#1363 - No more need for report
					report = null;
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In Excel export.", MsgBoxStyle.Critical, "Error");
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuHTML_Click(object sender, System.EventArgs e)
		{
			try
			{
				string strFileName = "";
				DialogResult intResp = 0;
				string strOldDir;
				string strFullName = "";
				//FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
				FCSectionReport report = null;
				string fileName = string.Empty;
				if (!(ARViewer21.ReportSource == null))
				{
					if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
					{
						FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
						return;
					}
				}
				//strOldDir = Application.StartupPath;
				//Information.Err(ex).Clear();
				//App.MainForm.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNNoChangeDir;
				//// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//App.MainForm.CommonDialog1.CancelError = true;
				///*? On Error Resume Next  */
				//App.MainForm.CommonDialog1.FileName = "";
				//App.MainForm.CommonDialog1.Filter = "*.htm";
				//App.MainForm.CommonDialog1.DefaultExt = "htm";
				//App.MainForm.CommonDialog1.InitDir = Application.StartupPath;
				//App.MainForm.CommonDialog1.ShowSave();
				//if (Information.Err(ex).Number == 0)
				//{
				//	GrapeCity.ActiveReports.Export.Html.Section.HtmlExport a = new GrapeCity.ActiveReports.Export.Html.Section.HtmlExport();
				//	strFileName = Path.GetFileNameWithoutExtension(App.MainForm.CommonDialog1.FileName);
				//	//a.FileNamePrefix = strFileName;
				//	strFullName = Path.GetFileName(App.MainForm.CommonDialog1.FileName);
				//	string HTMLOutputPath = Directory.GetParent(App.MainForm.CommonDialog1.FileName).FullName;
				//	//a"OutputPath = Path.GetDirectoryName(App.MainForm.CommonDialog1.FileName);
				//	//a.MHTOutput = false;
				//	a.MultiPage = false;
				//	if (ARViewer21.ReportSource.Document.Pages.Count > 1)
				//	{
				//		intResp = FCMessageBox.Show("Do you want to save all pages as one HTML page?", MsgBoxStyle.YesNo, "Single or multiple files?");
				//		if (intResp == DialogResult.No)
				//			a.MultiPage = true;
				//	}
				//	if (!Directory.Exists(HTMLOutputPath + strFileName))
				//	{
				//		Directory.CreateDirectory(HTMLOutputPath + strFileName);
				//		HTMLOutputPath = HTMLOutputPath + strFileName;
				//	}
				//	a.Export(ARViewer21.ReportSource.Document, HTMLOutputPath);
				//	FCMessageBox.Show("Export to file  " + HTMLOutputPath + strFullName + "  was completed successfully." + "\r\n" + "Please note that supporting files may also have been created in the same directory.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
				//}
				//else
				//{
				//	FCMessageBox.Show("There was an error saving the file.", MsgBoxStyle.Critical, null);
				//}
				///* On Error GoTo ErrorHandler */// ChDrive strOldDir
				//// ChDir strOldDir
				//modAPIsConst.SetCurrentDirectoryWrp(ref strOldDir);
				GrapeCity.ActiveReports.Export.Html.Section.HtmlExport a = new GrapeCity.ActiveReports.Export.Html.Section.HtmlExport();
				//FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
				if (!(ARViewer21.ReportSource == null))
				{
					report = ARViewer21.ReportSource;
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = FCUtils.FixFileName(ARViewer21.ReportSource.Name);
				}
				else
				{
					report = new FCSectionReport();
					report.Document.Load(ARViewer21.ReportName);
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = FCUtils.FixFileName(this.Text);
				}
				//FC:FINAL:RPU:#1363 - Use the report variable
				//if (ARViewer21.ReportSource.Document.Pages.Count > 1)
				if (report.Document.Pages.Count > 1)
				{
					DialogResult res = FCMessageBox.Show("Do you want to save all pages as one HTML page?", MsgBoxStyle.YesNo, "Single or multiple files?");
					if (res == DialogResult.No)
					{
						a.MultiPage = true;
					}
				}
				if (a.MultiPage)
				{
					string zipFilePath = GetTempFileName(".zip");
					//FC:FINAL:RPU:#1363 - Concatenate just extension
					//string fileName = ARViewer21.ReportSource.Name + ".zip";
					fileName += ".zip";
					var zip = fecherFoundation.ZipFile.Open(zipFilePath, System.IO.Compression.ZipArchiveMode.Create);
					//FC:FINAL:RPU:#1363 - Use the report variable
					//for (int i = 0; i < ARViewer21.ReportSource.Document.Pages.Count; i++)
					for (int i = 0; i < report.Document.Pages.Count; i++)
					{
						string currentPage = (i + 1).ToString();
						//string htmlFileName = ARViewer21.ReportSource.Name + "_Page" + currentPage + ".html";
						string htmlFileName = report.Name + "_Page" + currentPage + ".html";
						string tempFilePath = GetTempFileName(".html");
						//a.Export(ARViewer21.ReportSource.Document, tempFilePath, currentPage);
						a.Export(report.Document, tempFilePath, currentPage);
						zip.CreateEntryFromFile(tempFilePath, htmlFileName, System.IO.Compression.CompressionLevel.Optimal);
					}
					zip.Dispose();
					//FC:FINAL:DSE use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
					//FCUtils.Download(zipFilePath, fileName);
					FCUtils.DownloadAndOpen("_blank", zipFilePath, fileName);
					//FC:FINAL:RPU:#1363 - No more need for report
					report = null;
				}
				else
				{
					//FC:FINAL:RPU:#1363 - Concatenate just extension
					//string fileName = ARViewer21.ReportSource.Name + ".html";
					fileName += ".html";
					string tempFilePath = GetTempFileName(".html");
					//FC:FINAL:RPU:#1363 - Use the report variable
					//a.Export(ARViewer21.ReportSource.Document, tempFilePath);
					a.Export(report.Document, tempFilePath);
					//PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
					//FCUtils.Download(stream, fileName);
					FCUtils.DownloadAndOpen("_blank", tempFilePath, fileName);
					//FC:FINAL:RPU:#1363 - No more need for report
					report = null;
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In HTML export", MsgBoxStyle.Critical, "Error");
			}
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
            if (ARViewer21.ReportSource != null)
            {
                if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
                {
                    FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
                    return;
                }
            }
            try
            {
                FCSectionReport report;
                if (ARViewer21.ReportSource != null)
                {
                    report = ARViewer21.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document?.Load(ARViewer21.ReportName);
                }
                report.ExportToPDFAndPrint();
                return;
            }
            catch (Exception ex)
            {
				StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In mnuPrint", MsgBoxStyle.Critical, "Error");
            }
        }						

		private string GetTempFileName(string extension)
		{
			string applicationName = Path.GetFileName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory));
			//string tempFolder = Path.Combine(Path.GetTempPath(), "Wisej", applicationName, "Temp", "PDF");
			string tempFolder = Path.Combine(Application.StartupPath, "Temp", "ReportViewer");
			if (!Directory.Exists(tempFolder))
			{
				Directory.CreateDirectory(tempFolder);
			}
			Guid guid = Guid.NewGuid();
			string tempFileName = guid.ToString() + extension;
			string tempFilePath = Path.Combine(tempFolder, tempFileName);
			while (File.Exists(tempFilePath))
			{
				guid = Guid.NewGuid();
				tempFileName = guid.ToString() + extension;
				tempFilePath = Path.Combine(tempFolder, tempFileName);
			}
			return tempFilePath;
		}

		private void mnuRTF_Click(object sender, System.EventArgs e)
		{
			try
			{
				string strOldDir;
				//FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
				FCSectionReport report = null;
				string fileName = string.Empty;
				if (!(ARViewer21.ReportSource == null))
				{
					if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
					{
						FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
						return;
					}
				}
				strOldDir = Application.StartupPath;
				Information.Err().Clear();
				//FC:FINAL:AM: download instead the file
				//App.MainForm.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNNoChangeDir;
				//// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//App.MainForm.CommonDialog1.CancelError = true;
				///*? On Error Resume Next  */
				//App.MainForm.CommonDialog1.FileName = "";
				//App.MainForm.CommonDialog1.Filter = "*.rtf";
				//App.MainForm.CommonDialog1.DefaultExt = "rtf";
				//App.MainForm.CommonDialog1.InitDir = Application.StartupPath;
				//App.MainForm.CommonDialog1.ShowSave();
				//if (Information.Err(ex).Number == 0)
				//{
				//GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
				//string fileName = App.MainForm.CommonDialog1.FileName;                    
				//a.Export(ARViewer21.ReportSource.Document, fileName);
				//if (!File.Exists(fileName))
				//{
				//                   // If Left(UCase(fso.GetDriveName(a.FileName)), 1) = "A" Then
				//	FCMessageBox.Show("No file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that the directory is not write protected and that there is enough free space to store the file", MsgBoxStyle.Exclamation, "Not Exported");
				//	// Else
				//	// MsgBox "No file created" & vbNewLine & "Export not successful", MsgBoxStyle.Exclamation, "Not Exported"
				//	// End If
				//}
				//else
				//{
				//	FileInfo fl = new FileInfo(fileName);
				//	if (FCConvert.ToInt32(fl.Length) > 0)
				//	{
				//		FCMessageBox.Show("Export to file  " + App.MainForm.CommonDialog1.FileName + "  was completed successfully.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
				//	}
				//	else
				//	{
				//		FCMessageBox.Show("Empty file created" + "\r\n" + "Export not successful" + "\r\n" + "Check to make sure that there is enough free space to create the file", MsgBoxStyle.Exclamation, "Not Exported");
				//	}
				//}
				//}
				//else
				//{
				//	FCMessageBox.Show("There was an error saving the file.", MsgBoxStyle.Critical, null);
				//}
				/* On Error GoTo ErrorHandler */// ChDrive strOldDir
				// ChDir strOldDir
				//modAPIsConst.SetCurrentDirectoryWrp(ref strOldDir);
				GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
				//string fileName = ARViewer21.ReportSource.Name + ".rtf";
				//FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
				if (!(ARViewer21.ReportSource == null))
				{
					report = ARViewer21.ReportSource;
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".rtf";
				}
				else
				{
					report = new FCSectionReport();
					report.Document.Load(ARViewer21.ReportName);
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = FCUtils.FixFileName(this.Text) + ".rtf";
				}
				using (MemoryStream stream = new MemoryStream())
				{
					//FC:FINAL:RPU:#1363 - Use report.Document
					//a.Export(ARViewer21.ReportSource.Document, stream);
					a.Export(report.Document, stream);
					stream.Position = 0;
					//PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
					//FCUtils.Download(stream, fileName);
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
					//FC:FINAL:RPU:#1363 - No more need for report
					report = null;
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In RTF export", MsgBoxStyle.Critical, "Error");
			}
		}

		private void mnuExportPDF_Click(object sender, System.EventArgs e)
		{
			try
			{
				string strOldDir;
				//FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
				FCSectionReport report = null;
				string fileName = string.Empty;
				if (!(ARViewer21.ReportSource == null))
				{
					if (ARViewer21.ReportSource.State == GrapeCity.ActiveReports.SectionReport.ReportState.InProgress)
					{
						FCMessageBox.Show("Report not done", MsgBoxStyle.Exclamation, "Report Loading");
						return;
					}
				}
				
				GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
				//string fileName = ARViewer21.ReportSource.Name + ".pdf";
				//FC:FINAL:RPU:#1363 - If the ReportSource is null load the report from file
				if (ARViewer21.ReportSource != null)
				{
					report = ARViewer21.ReportSource;
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = FCUtils.FixFileName(ARViewer21.ReportSource.Name) + ".pdf";
				}
				else
				{
					report = new FCSectionReport();
                    report.Document?.Load(ARViewer21.ReportName);

                    //FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = FCUtils.FixFileName(this.Text) + ".pdf";
				}
				//a.Export(ARViewer21.ReportSource.Document, Path.Combine(Application.StartupPath,fileName));
				using (MemoryStream stream = new MemoryStream())
				{
					//FC:FINAL:RPU:#1363 - Use report.Document
					//a.Export(ARViewer21.ReportSource.Document, stream);
                    if (report.Document != null)
                    {
                        a.Export(report.Document, stream);
                        stream.Position = 0;

                        //PPJ:FINAL:BCU - use Application.DownloadAndOpen instead of Application.Download otherwise raport will get corrupted/disposed
                        //FCUtils.Download(stream, fileName);
                        FCUtils.DownloadAndOpen("_blank", stream, fileName);
                    }

                    //FC:FINAL:RPU:#1363 - No more need for report
					report = null;
				}
            }
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In PDF export", MsgBoxStyle.Critical, "Error");
			}
		}

		private void toolBar1_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
		{
			if (e.Button == toolBarButtonEmailPDF)
			{
				this.mnuEmailPDF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonEmailRTF)
			{
				this.mnuEMailRTF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonExportPDF)
			{
				this.mnuExportPDF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonExportRTF)
			{
				this.mnuRTF_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonExportHTML)
			{
				this.mnuHTML_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == toolBarButtonExportExcel)
			{
				this.mnuExcel_Click(e.Button, EventArgs.Empty);
			}
			else if (e.Button == this.toolBarButtonPrint)
			{
				this.mnuPrint_Click(e.Button, EventArgs.Empty);
			}
		}
	}
}
