﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cCentralPartyReference
	{
		//=========================================================
		private int lngRecordID;
		private string strModule = string.Empty;
		private string strIdentifier = string.Empty;
		private string strAltIdentifier = string.Empty;
		private string strDescription = string.Empty;
		private string strSortIdentifier = string.Empty;

		public string IdentifierForSort
		{
			set
			{
				strSortIdentifier = value;
			}
			get
			{
				string IdentifierForSort = "";
				IdentifierForSort = strSortIdentifier;
				return IdentifierForSort;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string ModuleCode
		{
			set
			{
				strModule = value;
			}
			get
			{
				string ModuleCode = "";
				ModuleCode = strModule;
				return ModuleCode;
			}
		}

		public string Identifier
		{
			set
			{
				strIdentifier = value;
			}
			get
			{
				string Identifier = "";
				Identifier = strIdentifier;
				return Identifier;
			}
		}

		public string AlternateIdentifier
		{
			set
			{
				strAltIdentifier = value;
			}
			get
			{
				string AlternateIdentifier = "";
				AlternateIdentifier = strAltIdentifier;
				return AlternateIdentifier;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}
	}
}
