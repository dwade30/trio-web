﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;

namespace Global
{
	/// <summary>
	/// Summary description for frmTaxRateCalc.
	/// </summary>
	partial class frmTaxRateCalc : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSource;
		public fecherFoundation.FCLabel lblSource;
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCFrame framEnhancedBETE;
		public fecherFoundation.FCButton cmdEBOK;
		public FCGrid GridEnhancedBETE;
		public fecherFoundation.FCCheckBox chkUseEB;
		public fecherFoundation.FCButton cmdShowEB;
		public fecherFoundation.FCFrame framInit;
		public fecherFoundation.FCButton cmdOk;
		public fecherFoundation.FCFrame framRange;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame1;
		public Global.T2KDateBox t2kBegin;
		public Global.T2KDateBox t2kEnd;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame Frame3;
		public FCGrid gridTownCode;
		public FCGrid GridForm1;
		public FCGrid GridForm2;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuLoad;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintCertification;
		public fecherFoundation.FCToolStripMenuItem mnuPrintBETEWorksheet;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private System.ComponentModel.IContainer components;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTaxRateCalc));
			this.cmbSource = new fecherFoundation.FCComboBox();
			this.lblSource = new fecherFoundation.FCLabel();
			this.cmbType = new fecherFoundation.FCComboBox();
			this.lblType = new fecherFoundation.FCLabel();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.lblRange = new fecherFoundation.FCLabel();
			this.framEnhancedBETE = new fecherFoundation.FCFrame();
			this.cmdEBOK = new fecherFoundation.FCButton();
			this.GridEnhancedBETE = new fecherFoundation.FCGrid();
			this.chkUseEB = new fecherFoundation.FCCheckBox();
			this.cmdShowEB = new fecherFoundation.FCButton();
			this.framInit = new fecherFoundation.FCFrame();
			this.cmdOk = new fecherFoundation.FCButton();
			this.framRange = new fecherFoundation.FCFrame();
			this.txtEnd = new fecherFoundation.FCTextBox();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.t2kBegin = new Global.T2KDateBox();
			this.t2kEnd = new Global.T2KDateBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Frame3 = new fecherFoundation.FCFrame();
			this.gridTownCode = new fecherFoundation.FCGrid();
			this.GridForm1 = new fecherFoundation.FCGrid();
			this.GridForm2 = new fecherFoundation.FCGrid();
			this.mnuLoad = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintCertification = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrintBETEWorksheet = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdPrintBETEWorksheet = new fecherFoundation.FCButton();
			this.cmdPrintCertification = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdLoad = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.framEnhancedBETE)).BeginInit();
			this.framEnhancedBETE.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdEBOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridEnhancedBETE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkUseEB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdShowEB)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framInit)).BeginInit();
			this.framInit.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framRange)).BeginInit();
			this.framRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kBegin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kEnd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridForm1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridForm2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintBETEWorksheet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintCertification)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoad)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 712);
			this.BottomPanel.Size = new System.Drawing.Size(1082, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.framEnhancedBETE);
			this.ClientArea.Controls.Add(this.cmdShowEB);
			this.ClientArea.Controls.Add(this.chkUseEB);
			this.ClientArea.Controls.Add(this.framInit);
			this.ClientArea.Controls.Add(this.GridForm1);
			this.ClientArea.Controls.Add(this.GridForm2);
			this.ClientArea.Size = new System.Drawing.Size(1102, 628);
			this.ClientArea.Controls.SetChildIndex(this.GridForm2, 0);
			this.ClientArea.Controls.SetChildIndex(this.GridForm1, 0);
			this.ClientArea.Controls.SetChildIndex(this.framInit, 0);
			this.ClientArea.Controls.SetChildIndex(this.chkUseEB, 0);
			this.ClientArea.Controls.SetChildIndex(this.cmdShowEB, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			this.ClientArea.Controls.SetChildIndex(this.framEnhancedBETE, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdLoad);
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Controls.Add(this.cmdPrintCertification);
			this.TopPanel.Controls.Add(this.cmdPrintBETEWorksheet);
			this.TopPanel.Size = new System.Drawing.Size(1102, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintBETEWorksheet, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrintCertification, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdLoad, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(230, 30);
			this.HeaderText.Text = "Tax Rate Calculator";
			// 
			// cmbSource
			// 
			this.cmbSource.Items.AddRange(new object[] {
            "Billing Values",
            "Current Values"});
			this.cmbSource.Location = new System.Drawing.Point(83, 250);
			this.cmbSource.Name = "cmbSource";
			this.cmbSource.Size = new System.Drawing.Size(224, 40);
			this.cmbSource.TabIndex = 2;
			this.cmbSource.Text = "Billing Values";
			this.cmbSource.SelectedIndexChanged += new System.EventHandler(this.cmbSource_SelectedIndexChanged);
			// 
			// lblSource
			// 
			this.lblSource.AutoSize = true;
			this.lblSource.Location = new System.Drawing.Point(20, 264);
			this.lblSource.Name = "lblSource";
			this.lblSource.Size = new System.Drawing.Size(43, 20);
			this.lblSource.TabIndex = 3;
			this.lblSource.Text = "USE";
			// 
			// cmbType
			// 
			this.cmbType.Items.AddRange(new object[] {
            "Account",
            "Name",
            "Map Lot",
            "Location",
            "Tran Code",
            "Land Code",
            "Building Code"});
			this.cmbType.Location = new System.Drawing.Point(407, 250);
			this.cmbType.Name = "cmbType";
			this.cmbType.Size = new System.Drawing.Size(232, 40);
			this.cmbType.Text = "Account";
			this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
			// 
			// lblType
			// 
			this.lblType.AutoSize = true;
			this.lblType.Location = new System.Drawing.Point(337, 264);
			this.lblType.Name = "lblType";
			this.lblType.Size = new System.Drawing.Size(52, 20);
			this.lblType.TabIndex = 1;
			this.lblType.Text = "TYPE";
			// 
			// cmbRange
			// 
			this.cmbRange.Items.AddRange(new object[] {
            "All",
            "Range"});
			this.cmbRange.Location = new System.Drawing.Point(124, 30);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.TabIndex = 27;
			this.cmbRange.Text = "All";
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
			// 
			// lblRange
			// 
			this.lblRange.AutoSize = true;
			this.lblRange.Location = new System.Drawing.Point(20, 44);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(67, 20);
			this.lblRange.TabIndex = 28;
			this.lblRange.Text = "RANGE";
			// 
			// framEnhancedBETE
			// 
			this.framEnhancedBETE.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.framEnhancedBETE.BackColor = System.Drawing.Color.White;
			this.framEnhancedBETE.Controls.Add(this.cmdEBOK);
			this.framEnhancedBETE.Controls.Add(this.GridEnhancedBETE);
			this.framEnhancedBETE.FormatCaption = false;
			this.framEnhancedBETE.Location = new System.Drawing.Point(30, 30);
			this.framEnhancedBETE.Name = "framEnhancedBETE";
			this.framEnhancedBETE.Size = new System.Drawing.Size(900, 575);
			this.framEnhancedBETE.TabIndex = 27;
			this.framEnhancedBETE.Text = "Enhanced BETE";
			this.framEnhancedBETE.Visible = false;
			// 
			// cmdEBOK
			// 
			this.cmdEBOK.Anchor = Wisej.Web.AnchorStyles.Bottom;
			this.cmdEBOK.AppearanceKey = "actionButton";
			this.cmdEBOK.ForeColor = System.Drawing.Color.White;
			this.cmdEBOK.Location = new System.Drawing.Point(416, 524);
			this.cmdEBOK.Name = "cmdEBOK";
			this.cmdEBOK.Size = new System.Drawing.Size(69, 40);
			this.cmdEBOK.TabIndex = 30;
			this.cmdEBOK.Text = "OK";
			this.cmdEBOK.Click += new System.EventHandler(this.cmdEBOK_Click);
			// 
			// GridEnhancedBETE
			// 
			this.GridEnhancedBETE.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridEnhancedBETE.Cols = 3;
			this.GridEnhancedBETE.ColumnHeadersVisible = false;
			this.GridEnhancedBETE.FixedRows = 0;
			this.GridEnhancedBETE.Location = new System.Drawing.Point(20, 30);
			this.GridEnhancedBETE.Name = "GridEnhancedBETE";
			this.GridEnhancedBETE.Rows = 16;
			this.GridEnhancedBETE.ShowFocusCell = false;
			this.GridEnhancedBETE.Size = new System.Drawing.Size(858, 485);
			this.GridEnhancedBETE.TabIndex = 28;
			this.GridEnhancedBETE.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridEnhancedBETE_ValidateEdit);
			this.GridEnhancedBETE.CurrentCellChanged += new System.EventHandler(this.GridEnhancedBETE_RowColChange);
			// 
			// chkUseEB
			// 
			this.chkUseEB.Location = new System.Drawing.Point(30, 30);
			this.chkUseEB.Name = "chkUseEB";
			this.chkUseEB.Size = new System.Drawing.Size(213, 23);
			this.chkUseEB.TabIndex = 31;
			this.chkUseEB.Text = "Use Enhanced BETE calculator";
			this.chkUseEB.Visible = false;
			this.chkUseEB.CheckedChanged += new System.EventHandler(this.chkUseEB_CheckedChanged);
			this.chkUseEB.Validating += new System.ComponentModel.CancelEventHandler(this.chkUseEB_Validating);
			// 
			// cmdShowEB
			// 
			this.cmdShowEB.AppearanceKey = "actionButton";
			this.cmdShowEB.Location = new System.Drawing.Point(328, 30);
			this.cmdShowEB.Name = "cmdShowEB";
			this.cmdShowEB.Size = new System.Drawing.Size(250, 40);
			this.cmdShowEB.TabIndex = 29;
			this.cmdShowEB.Text = "Enhanced BETE Worksheet";
			this.cmdShowEB.Visible = false;
			this.cmdShowEB.Click += new System.EventHandler(this.cmdShowEB_Click);
			// 
			// framInit
			// 
			this.framInit.BackColor = System.Drawing.Color.White;
			this.framInit.Controls.Add(this.cmbType);
			this.framInit.Controls.Add(this.lblType);
			this.framInit.Controls.Add(this.cmbSource);
			this.framInit.Controls.Add(this.lblSource);
			this.framInit.Controls.Add(this.cmdOk);
			this.framInit.Controls.Add(this.framRange);
			this.framInit.Controls.Add(this.Frame1);
			this.framInit.Controls.Add(this.Frame3);
			this.framInit.Location = new System.Drawing.Point(30, 90);
			this.framInit.Name = "framInit";
			this.framInit.Size = new System.Drawing.Size(648, 370);
			this.framInit.TabIndex = 2;
			// 
			// cmdOk
			// 
			this.cmdOk.AppearanceKey = "actionButton";
			this.cmdOk.ForeColor = System.Drawing.Color.White;
			this.cmdOk.Location = new System.Drawing.Point(296, 310);
			this.cmdOk.Name = "cmdOk";
			this.cmdOk.Size = new System.Drawing.Size(70, 40);
			this.cmdOk.TabIndex = 22;
			this.cmdOk.Text = "OK";
			this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
			// 
			// framRange
			// 
			this.framRange.AppearanceKey = "groupBoxNoBorders";
			this.framRange.Controls.Add(this.txtEnd);
			this.framRange.Controls.Add(this.txtStart);
			this.framRange.Controls.Add(this.Label2);
			this.framRange.Location = new System.Drawing.Point(288, 140);
			this.framRange.Name = "framRange";
			this.framRange.Size = new System.Drawing.Size(323, 90);
			this.framRange.TabIndex = 10;
			this.framRange.Visible = false;
			// 
			// txtEnd
			// 
			this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
			this.txtEnd.Location = new System.Drawing.Point(211, 30);
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Size = new System.Drawing.Size(96, 40);
			this.txtEnd.TabIndex = 12;
			// 
			// txtStart
			// 
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.Location = new System.Drawing.Point(20, 30);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(96, 40);
			this.txtStart.TabIndex = 11;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(150, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(35, 18);
			this.Label2.TabIndex = 13;
			this.Label2.Text = "TO";
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.t2kBegin);
			this.Frame1.Controls.Add(this.t2kEnd);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Location = new System.Drawing.Point(20, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(411, 90);
			this.Frame1.TabIndex = 6;
			this.Frame1.Text = "Municipal Fiscal Year";
			// 
			// t2kBegin
			// 
			this.t2kBegin.Location = new System.Drawing.Point(20, 30);
			this.t2kBegin.Mask = "##/##/####";
			this.t2kBegin.MaxLength = 10;
			this.t2kBegin.Name = "t2kBegin";
			this.t2kBegin.Size = new System.Drawing.Size(115, 22);
			this.t2kBegin.TabIndex = 7;
			// 
			// t2kEnd
			// 
			this.t2kEnd.Location = new System.Drawing.Point(276, 30);
			this.t2kEnd.Mask = "##/##/####";
			this.t2kEnd.MaxLength = 10;
			this.t2kEnd.Name = "t2kEnd";
			this.t2kEnd.Size = new System.Drawing.Size(115, 22);
			this.t2kEnd.TabIndex = 8;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(194, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(35, 18);
			this.Label1.TabIndex = 9;
			this.Label1.Text = "TO";
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.gridTownCode);
			this.Frame3.Controls.Add(this.cmbRange);
			this.Frame3.Controls.Add(this.lblRange);
			this.Frame3.Location = new System.Drawing.Point(20, 140);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(265, 90);
			this.Frame3.TabIndex = 3;
			this.Frame3.Text = "Records";
			// 
			// gridTownCode
			// 
			this.gridTownCode.Cols = 1;
			this.gridTownCode.ColumnHeadersVisible = false;
			this.gridTownCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.gridTownCode.ExtendLastCol = true;
			this.gridTownCode.FixedCols = 0;
			this.gridTownCode.FixedRows = 0;
			this.gridTownCode.Location = new System.Drawing.Point(9, 62);
			this.gridTownCode.Name = "gridTownCode";
			this.gridTownCode.ReadOnly = false;
			this.gridTownCode.RowHeadersVisible = false;
			this.gridTownCode.Rows = 1;
			this.gridTownCode.ShowFocusCell = false;
			this.gridTownCode.Size = new System.Drawing.Size(144, 19);
			this.gridTownCode.TabIndex = 26;
			this.gridTownCode.Visible = false;
			// 
			// GridForm1
			// 
			this.GridForm1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridForm1.AppearanceKey = "groupBoxNoBorders";
			this.GridForm1.Cols = 3;
			this.GridForm1.ColumnHeadersVisible = false;
			this.GridForm1.FixedRows = 0;
			this.GridForm1.Location = new System.Drawing.Point(30, 90);
			this.GridForm1.Name = "GridForm1";
			this.GridForm1.Rows = 17;
			this.GridForm1.ShowFocusCell = false;
			this.GridForm1.Size = new System.Drawing.Size(949, 393);
			this.GridForm1.TabIndex = 32;
			this.GridForm1.Visible = false;
			this.GridForm1.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridForm1_ValidateEdit);
			this.GridForm1.CurrentCellChanged += new System.EventHandler(this.GridForm1_RowColChange);
			// 
			// GridForm2
			// 
			this.GridForm2.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GridForm2.AppearanceKey = "groupBoxNoBorders";
			this.GridForm2.Cols = 4;
			this.GridForm2.ColumnHeadersVisible = false;
			this.GridForm2.FixedRows = 0;
			this.GridForm2.Location = new System.Drawing.Point(30, 503);
			this.GridForm2.Name = "GridForm2";
			this.GridForm2.Rows = 8;
			this.GridForm2.ShowFocusCell = false;
			this.GridForm2.Size = new System.Drawing.Size(949, 209);
			this.GridForm2.TabIndex = 1;
			this.GridForm2.Visible = false;
			this.GridForm2.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridForm2_ValidateEdit);
			this.GridForm2.CurrentCellChanged += new System.EventHandler(this.GridForm2_RowColChange);
			// 
			// mnuLoad
			// 
			this.mnuLoad.Index = -1;
			this.mnuLoad.Name = "mnuLoad";
			this.mnuLoad.Text = "Load";
			this.mnuLoad.Click += new System.EventHandler(this.mnuLoad_Click);
			// 
			// mnuSepar3
			// 
			this.mnuSepar3.Index = -1;
			this.mnuSepar3.Name = "mnuSepar3";
			this.mnuSepar3.Text = "-";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = -1;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Text = "Print";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnuPrintCertification
			// 
			this.mnuPrintCertification.Index = -1;
			this.mnuPrintCertification.Name = "mnuPrintCertification";
			this.mnuPrintCertification.Text = "Print Assessors Certification";
			this.mnuPrintCertification.Click += new System.EventHandler(this.mnuPrintCertification_Click);
			// 
			// mnuPrintBETEWorksheet
			// 
			this.mnuPrintBETEWorksheet.Index = -1;
			this.mnuPrintBETEWorksheet.Name = "mnuPrintBETEWorksheet";
			this.mnuPrintBETEWorksheet.Text = "Print BETE Worksheet";
			this.mnuPrintBETEWorksheet.Click += new System.EventHandler(this.mnuPrintBETEWorksheet_Click);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = -1;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = -1;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuSave.Text = "Save";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = -1;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = -1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = -1;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(509, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(91, 48);
			this.cmdSave.Text = "Save";
			this.cmdSave.Visible = false;
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// cmdPrintBETEWorksheet
			// 
			this.cmdPrintBETEWorksheet.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintBETEWorksheet.Location = new System.Drawing.Point(922, 29);
			this.cmdPrintBETEWorksheet.Name = "cmdPrintBETEWorksheet";
			this.cmdPrintBETEWorksheet.Size = new System.Drawing.Size(152, 24);
			this.cmdPrintBETEWorksheet.TabIndex = 1;
			this.cmdPrintBETEWorksheet.Text = "Print BETE Worksheet";
			this.cmdPrintBETEWorksheet.Visible = false;
			this.cmdPrintBETEWorksheet.Click += new System.EventHandler(this.mnuPrintBETEWorksheet_Click);
			// 
			// cmdPrintCertification
			// 
			this.cmdPrintCertification.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrintCertification.Location = new System.Drawing.Point(728, 29);
			this.cmdPrintCertification.Name = "cmdPrintCertification";
			this.cmdPrintCertification.Size = new System.Drawing.Size(188, 24);
			this.cmdPrintCertification.TabIndex = 2;
			this.cmdPrintCertification.Text = "Print Assessors Certification";
			this.cmdPrintCertification.Visible = false;
			this.cmdPrintCertification.Click += new System.EventHandler(this.mnuPrintCertification_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.Location = new System.Drawing.Point(676, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(46, 24);
			this.cmdPrint.TabIndex = 3;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Visible = false;
			this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// cmdLoad
			// 
			this.cmdLoad.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdLoad.Location = new System.Drawing.Point(624, 29);
			this.cmdLoad.Name = "cmdLoad";
			this.cmdLoad.Size = new System.Drawing.Size(46, 24);
			this.cmdLoad.TabIndex = 4;
			this.cmdLoad.Text = "Load";
			this.cmdLoad.Visible = false;
			this.cmdLoad.Click += new System.EventHandler(this.mnuLoad_Click);
			// 
			// frmTaxRateCalc
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1102, 688);
			this.KeyPreview = true;
			this.Name = "frmTaxRateCalc";
			this.ShowInTaskbar = false;
			this.Text = "Tax Rate Calculator";
			this.Load += new System.EventHandler(this.frmTaxRateCalc_Load);
			this.Resize += new System.EventHandler(this.frmTaxRateCalc_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTaxRateCalc_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.framEnhancedBETE)).EndInit();
			this.framEnhancedBETE.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdEBOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridEnhancedBETE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkUseEB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdShowEB)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framInit)).EndInit();
			this.framInit.ResumeLayout(false);
			this.framInit.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framRange)).EndInit();
			this.framRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.t2kBegin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.t2kEnd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			this.Frame3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridTownCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridForm1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridForm2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintBETEWorksheet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrintCertification)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoad)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		//private FCMenuStrip MainMenu1;
        private FCButton cmdPrintBETEWorksheet;
        private FCButton cmdPrintCertification;
        private FCButton cmdPrint;
        private FCButton cmdLoad;
    }
}
