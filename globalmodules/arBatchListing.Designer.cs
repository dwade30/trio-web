﻿namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class arBatchListing
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arBatchListing));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.fldTotalPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotalTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAcct = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldIntDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIntDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldName,
				this.fldPrincipal,
				this.fldTotal,
				this.fldInterest,
				this.fldCosts,
				this.fldAcct
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.4375F;
			this.fldName.MultiLine = false;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; white-space: nowrap; ddo-char-set: 0";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 2.5625F;
			// 
			// fldPrincipal
			// 
			this.fldPrincipal.Height = 0.1875F;
			this.fldPrincipal.Left = 3F;
			this.fldPrincipal.Name = "fldPrincipal";
			this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldPrincipal.Text = null;
			this.fldPrincipal.Top = 0F;
			this.fldPrincipal.Width = 1F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldTotal.Text = null;
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1.0625F;
			// 
			// fldInterest
			// 
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 4F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldInterest.Text = null;
			this.fldInterest.Top = 0F;
			this.fldInterest.Width = 1F;
			// 
			// fldCosts
			// 
			this.fldCosts.Height = 0.1875F;
			this.fldCosts.Left = 5F;
			this.fldCosts.Name = "fldCosts";
			this.fldCosts.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldCosts.Text = null;
			this.fldCosts.Top = 0F;
			this.fldCosts.Width = 1F;
			// 
			// fldAcct
			// 
			this.fldAcct.Height = 0.1875F;
			this.fldAcct.Left = 0F;
			this.fldAcct.Name = "fldAcct";
			this.fldAcct.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldAcct.Text = null;
			this.fldAcct.Top = 0F;
			this.fldAcct.Width = 0.4375F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			//
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalPrin,
				this.fldTotalTotal,
				this.fldTotalInt,
				this.fldTotalCost,
				this.lblTotalTitle,
				this.lnTotals
			});
			this.ReportFooter.Height = 0.5416667F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// fldTotalPrin
			// 
			this.fldTotalPrin.Height = 0.1875F;
			this.fldTotalPrin.Left = 3F;
			this.fldTotalPrin.Name = "fldTotalPrin";
			this.fldTotalPrin.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldTotalPrin.Text = null;
			this.fldTotalPrin.Top = 0.125F;
			this.fldTotalPrin.Width = 1F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 6F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldTotalTotal.Text = null;
			this.fldTotalTotal.Top = 0.125F;
			this.fldTotalTotal.Width = 1.0625F;
			// 
			// fldTotalInt
			// 
			this.fldTotalInt.Height = 0.1875F;
			this.fldTotalInt.Left = 4F;
			this.fldTotalInt.Name = "fldTotalInt";
			this.fldTotalInt.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldTotalInt.Text = null;
			this.fldTotalInt.Top = 0.125F;
			this.fldTotalInt.Width = 1F;
			// 
			// fldTotalCost
			// 
			this.fldTotalCost.Height = 0.1875F;
			this.fldTotalCost.Left = 5F;
			this.fldTotalCost.Name = "fldTotalCost";
			this.fldTotalCost.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldTotalCost.Text = null;
			this.fldTotalCost.Top = 0.125F;
			this.fldTotalCost.Width = 1F;
			// 
			// lblTotalTitle
			// 
			this.lblTotalTitle.Height = 0.1875F;
			this.lblTotalTitle.HyperLink = null;
			this.lblTotalTitle.Left = 0.8125F;
			this.lblTotalTitle.Name = "lblTotalTitle";
			this.lblTotalTitle.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblTotalTitle.Text = "Total:";
			this.lblTotalTitle.Top = 0.125F;
			this.lblTotalTitle.Width = 2.1875F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 3F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0F;
			this.lnTotals.Width = 4.0625F;
			this.lnTotals.X1 = 3F;
			this.lnTotals.X2 = 7.0625F;
			this.lnTotals.Y1 = 0F;
			this.lnTotals.Y2 = 0F;
			// 
			// PageHeader
			//
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPage,
				this.lblYear,
				this.lblDateHeader,
				this.lblName,
				this.lblPrincipal,
				this.lblInterest,
				this.lblCosts,
				this.lblTotal,
				this.Line1,
				this.fldYear,
				this.fldDate,
				this.lblAcct,
				this.Label1,
				this.fldIntDate
			});
			this.PageHeader.Height = 1.03125F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.375F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblHeader.Text = "Batch Listing";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.0625F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.75F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.25F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.8125F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.25F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.8125F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.25F;
			// 
			// lblYear
			// 
			this.lblYear.Height = 0.1875F;
			this.lblYear.HyperLink = null;
			this.lblYear.Left = 0.8125F;
			this.lblYear.Name = "lblYear";
			this.lblYear.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblYear.Text = "Year:";
			this.lblYear.Top = 0.4375F;
			this.lblYear.Width = 0.75F;
			// 
			// lblDateHeader
			// 
			this.lblDateHeader.Height = 0.1875F;
			this.lblDateHeader.HyperLink = null;
			this.lblDateHeader.Left = 2.5625F;
			this.lblDateHeader.Name = "lblDateHeader";
			this.lblDateHeader.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblDateHeader.Text = "Date:";
			this.lblDateHeader.Top = 0.4375F;
			this.lblDateHeader.Width = 0.8125F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.4375F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.8125F;
			this.lblName.Width = 2.5625F;
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 3F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblPrincipal.Text = "Principal";
			this.lblPrincipal.Top = 0.8125F;
			this.lblPrincipal.Width = 1F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 4F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 0.8125F;
			this.lblInterest.Width = 1F;
			// 
			// lblCosts
			// 
			this.lblCosts.Height = 0.1875F;
			this.lblCosts.HyperLink = null;
			this.lblCosts.Left = 5F;
			this.lblCosts.Name = "lblCosts";
			this.lblCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblCosts.Text = "Costs";
			this.lblCosts.Top = 0.8125F;
			this.lblCosts.Width = 1F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 6F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.8125F;
			this.lblTotal.Width = 1.0625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1F;
			this.Line1.Width = 7F;
			this.Line1.X1 = 7F;
			this.Line1.X2 = 0F;
			this.Line1.Y1 = 1F;
			this.Line1.Y2 = 1F;
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.1875F;
			this.fldYear.Left = 1.5625F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldYear.Text = null;
			this.fldYear.Top = 0.4375F;
			this.fldYear.Width = 0.75F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 3.375F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldDate.Text = null;
			this.fldDate.Top = 0.4375F;
			this.fldDate.Width = 1.125F;
			// 
			// lblAcct
			// 
			this.lblAcct.Height = 0.1875F;
			this.lblAcct.HyperLink = null;
			this.lblAcct.Left = 0F;
			this.lblAcct.Name = "lblAcct";
			this.lblAcct.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblAcct.Text = "Acct";
			this.lblAcct.Top = 0.8125F;
			this.lblAcct.Width = 0.4375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 4.625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.Label1.Text = "Interest Date:";
			this.Label1.Top = 0.4375F;
			this.Label1.Width = 1.1875F;
			// 
			// fldIntDate
			// 
			this.fldIntDate.Height = 0.1875F;
			this.fldIntDate.Left = 5.8125F;
			this.fldIntDate.Name = "fldIntDate";
			this.fldIntDate.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldIntDate.Text = null;
			this.fldIntDate.Top = 0.4375F;
			this.fldIntDate.Width = 1.125F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// arBatchListing
			//
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.0625F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldIntDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCost;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalTitle;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAcct;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldIntDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
