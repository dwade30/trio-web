//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;
using fecherFoundation;
using Scripting;
#if TWGNENTY
using TWGNENTY;
#endif

namespace Global
{
	/// <summary>
	/// Summary description for frmSetupSignatures.
	/// </summary>
	public partial class frmSetupSignatures : BaseForm
	{

		public frmSetupSignatures()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null && Wisej.Web.Application.OpenForms.Count == 0)
				_InstancePtr = this;
		}

		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupSignatures InstancePtr
		{
			get
			{
				if (_InstancePtr == null) // || _InstancePtr.IsDisposed
					_InstancePtr = new frmSetupSignatures();
				return _InstancePtr;
			}
		}
		protected static frmSetupSignatures _InstancePtr = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		


		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation

		// Written By
		// Corey Gray
		// Date
		// 4/11/2006


		// ********************************************************


		const int CNSTGRIDUSERCOLAUTOID = 0;
		const int CNSTGRIDUSERCOLNAME = 1;
		const int CNSTGRIDUSERCOLPASSWORD = 2;
		const int CNSTGRIDUSERCOLTYPE = 3;
		const int CNSTGRIDUSERCOLMODULE = 4;
		const int CNSTGRIDUSERCOLMODSPECIFIC = 5;
		const int CNSTGRIDUSERCOLMODSPECIFICID = 6;
		const int CNSTGRIDUSERCOLPICTURE = 7;

		private bool boolFullPrivileges;
		private string strNewPicture;

		public void Init(bool boolFullPermissions = false)
		{
			boolFullPrivileges = boolFullPermissions;
			this.Show(App.MainForm);
		}





		private void cmbType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//Application.DoEvents();
			ReShowUsers();
		}

		private void cmdBrowse_Click(object sender, System.EventArgs e)
		{
			try
			{	// On Error GoTo ErrorHandler
				string strFilename = "";


				App.MainForm.CommonDialog1.Filter = "Image Files|*.bmp;*.gif;*.jpg;*.sig";
                // MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                //- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
                App.MainForm.CommonDialog1.ShowSave();
				strFilename = App.MainForm.CommonDialog1.FileName;
				if (Strings.Trim(strFilename)!=string.Empty) {
					if (LoadSigPicture(ref strFilename)) {
						strNewPicture = strFilename;
					}
				}

				return;
			}
			catch
			{	// ErrorHandler:
				if (Information.Err().Number!=32755) {
					MessageBox.Show("Error Number "+Convert.ToString(Information.Err().Number)+" "+Information.Err().Description+"\n"+"In Browse_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			DisableSigFrame_2(true);

			txtPassword.Text = "";
			txtConfirmPassword.Text = "";
		}

		private void DisableSigFrame_2(bool boolDisable) { DisableSigFrame(ref boolDisable); }
		private void DisableSigFrame(ref bool boolDisable)
		{
			if (boolDisable) {
				framMain.Enabled = true;
				framSignature.Enabled = false;
				cmdOk.Enabled = false;
				cmdCancel.Enabled = false;
				cmdBrowse.Enabled = false;
				cmdClear.Enabled = false;
				mnuSaveContinue.Enabled = true;
				mnuSave.Enabled = true;
				mnuAddUser.Enabled = true;
				mnuDelete.Enabled = true;
			} else {
				framMain.Enabled = false;
				framSignature.Enabled = true;
				cmdOk.Enabled = true;
				cmdCancel.Enabled = true;
				cmdClear.Enabled = true;
				cmdBrowse.Enabled = true;
				mnuSaveContinue.Enabled = false;
				mnuSave.Enabled = false;
				mnuAddUser.Enabled = false;
				mnuDelete.Enabled = false;
			}
		}

		private void FillSigFrame(int lngRow)
		{
			FileSystemObject fso = new FileSystemObject();
			string strTemp;
			int lngID = 0;
			int lngType = 0;
			string strMod = "";

			strNewPicture = "";
			txtPassword.Text = "";
			txtConfirmPassword.Text = "";

			strTemp = Strings.Trim(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE));
			if (strTemp==string.Empty) {
				lngID = (int)Math.Round(Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLAUTOID)));
				if (lngID>0) {
					strMod = gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODULE);
					lngType = (int)Math.Round(Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLTYPE)));
					strTemp = modSignatureFile.GetSigFileName(ref lngID, ref strMod, ref lngType);
					if (strTemp!=string.Empty) {
						if (fso.FileExists(strTemp)) {
							if (!LoadSigPicture(ref strTemp)) {
								fso.DeleteFile(strTemp, true);
							}
						} else {
							imgSignature.Image = null;
						}
					} else {
						imgSignature.Image = null;
					}
				} else {
					imgSignature.Image = null;
				}
			} else {
				if (fso.FileExists(strTemp)) {
					if (!LoadSigPicture(ref strTemp)) {
						fso.DeleteFile(strTemp, true);
					}
				} else {
					imgSignature.Image = null;
				}
			}
		}

		private bool LoadSigPicture(ref string strFilename)
		{
			bool LoadSigPicture = false;
			try
			{	// On Error GoTo ErrorHandler
				double dblRatio;
				int lngWidth;
				int lngHeight;
				int lngMaxWidth;
				int lngMaxHeight;
				// vbPorter upgrade warning: lngNewHeight As int	OnWrite(double)
				int lngNewHeight;
				// vbPorter upgrade warning: lngNewWidth As int	OnWrite(double)
				int lngNewWidth = 0;

				LoadSigPicture = false;
				imgSignature.Image = FCUtils.LoadPicture(strFilename);
				lngMaxHeight = Shape1.Height;
				lngMaxWidth = Shape1.Width;

				dblRatio = imgSignature.Image.Height/imgSignature.Image.Width;

				lngNewHeight = Convert.ToInt32(dblRatio*lngMaxWidth);

				if (lngNewHeight>lngMaxHeight) {
					imgSignature.Height = lngMaxHeight;
					lngNewWidth = Convert.ToInt32(lngMaxHeight/dblRatio);
					imgSignature.Width = lngNewWidth;
				} else {
					imgSignature.Width = lngMaxWidth;
					imgSignature.Height = lngNewHeight;
				}
				LoadSigPicture = true;
				return LoadSigPicture;
			}
			catch
			{	// ErrorHandler:
				if (Information.Err().Number==481) {
					MessageBox.Show("You have selected an invalid image file for your signature.  Please try again.", "Invalid Signature File", MessageBoxButtons.OK, MessageBoxIcon.Information);
				} else {
					MessageBox.Show("Error Number "+Convert.ToString(Information.Err().Number)+" "+Information.Err().Description+"\n"+"In LoadSigPicture", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			return LoadSigPicture;
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			imgSignature.Image = null;
			strNewPicture = "Clear";
		}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			int lngRow;

			lngRow = gridUser.Row;

			if (Strings.Trim(txtPassword.Text)!=string.Empty) {
				if (Strings.Trim(txtConfirmPassword.Text)!=string.Empty) {
					if (txtPassword.Text!=txtConfirmPassword.Text) {
						MessageBox.Show("The new password and the confirmation do not match", "Bad Password", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					} else {
						gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPASSWORD, Strings.Trim(txtPassword.Text));
					}
				} else {
					MessageBox.Show("You must confirm the password to change it", "No Confirmation", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			if (strNewPicture=="Clear") {
				gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE, "");
				strNewPicture = "";
			} else if (strNewPicture!=string.Empty) {
				gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE, strNewPicture);
			}
			txtPassword.Text = "";
			txtConfirmPassword.Text = "";
			gridUser.RowData(lngRow, true);
			DisableSigFrame_2(true);
		}

		private void frmSetupSignatures_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = ((int)e.KeyData) / 0x10000;

			switch (KeyCode) {
				
				case Keys.Escape:
				{
					KeyCode = (Keys)0;
					mnuExit_Click();
					break;
				}
			} //end switch
		}

		private void frmSetupSignatures_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupSignatures properties;
			//frmSetupSignatures.FillStyle	= 0;
			//frmSetupSignatures.ScaleWidth	= 5880;
			//frmSetupSignatures.ScaleHeight	= 3810;
			//frmSetupSignatures.LinkTopic	= "Form2";
			//frmSetupSignatures.LockControls	= true;
			//frmSetupSignatures.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridUser();
			LoadGridUser();

			SetupcmbType();
			if (boolFullPrivileges) {
                gridUser.Editable = FCGrid.EditableSettings.flexEDKbd;
			}
		}



		private void ReShowUsers()
		{
			int lngRow;
			int lngType;
			int lngFirstVisible;

			lngType = cmbType.ItemData(cmbType.SelectedIndex);
			lngFirstVisible = 0;
			if (boolFullPrivileges) {
				switch (lngType) {
					
					case modSignatureFile.BUDGETARYSIG:
					case modSignatureFile.COLLECTORSIG:
					case modSignatureFile.TREASURERSIG:
					case modSignatureFile.UTILITYSIG:
					{
						mnuAddUser.Visible = false;
						mnuDelete.Visible = false;
						mnuSepar2.Visible = false;
						break;
					}
					default: {
						mnuAddUser.Visible = true;
						mnuDelete.Visible = true;
						mnuSepar2.Visible = true;
						break;
					}
				} //end switch
			} else {
				mnuAddUser.Visible = false;
				mnuDelete.Visible = false;
				mnuSepar2.Visible = false;
			}

			for(lngRow=1; lngRow<=gridUser.Rows-1; lngRow++) {
				if (Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLTYPE))==lngType) {
					gridUser.RowHidden(lngRow, false);
					if (lngFirstVisible<1) lngFirstVisible = lngRow;
				} else {
					gridUser.RowHidden(lngRow, true);
				}
			} // lngRow
			if (lngFirstVisible>0) {
				gridUser.Row = lngFirstVisible;
				FillSigFrame(lngFirstVisible);
			} else {
				gridUser.Row = 0;
				FillSigFrame(0);
			}
		}



		private void gridType_ComboCloseUp(int Row, int Col, ref bool FinishEdit)
		{
			//Application.DoEvents();
			ReShowUsers();
		}







		private void gridUser_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			gridUser.RowData(gridUser.Row, true);
		}

		private void gridUser_DblClick(object sender, System.EventArgs e)
		{
			int lngAutoID;
			int lngReturn = 0;

			if (gridUser.Row<1) return;

			lngAutoID = (int)Math.Round(Conversion.Val(gridUser.TextMatrix(gridUser.Row, CNSTGRIDUSERCOLAUTOID)));
			if (lngAutoID!=0) {
				// get password
				lngReturn = frmSignaturePassword.InstancePtr.Init((int)Math.Round(Conversion.Val(gridUser.TextMatrix(gridUser.Row, CNSTGRIDUSERCOLTYPE))), 0, false, lngAutoID);
				if (lngReturn!=lngAutoID) {
					return;
				}
			}
			DisableSigFrame_2(false);
		}


		// Private Sub gridUser_EnterCell()
		// Currently this screws up the double click event because that wont fire when in edit mode
		// If gridUser.Row > 0 Then
		// Select Case Val(gridUser.TextMatrix(gridUser.Row, CNSTGRIDUSERCOLTYPE))
		// Case CLERKSIG, PAYROLLSIG
		// If boolFullPrivileges Then
		// gridUser.EditCell
		// End If
		// Case Else
		// End Select
		// End If
		// End Sub

		private void gridUser_KeyDownEvent(object sender, KeyEventArgs e)
		{
			int lngRow;
			lngRow = gridUser.Row;
			switch (e.KeyCode) {
				
				case Keys.Delete:
				{
					DeleteUser();
					break;
				}
				case Keys.Insert:
				{
					break;
				}
				case Keys.F1:
				{
					if (e.Shift) {
						ResetPassword(lngRow);
					}
					break;
				}
			} //end switch
		}

		private void ResetPassword(int lngRow)
		{
			object lngResponse;
			clsDRWrapper rsSave = new clsDRWrapper();

			if (boolFullPrivileges && Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLAUTOID))==0) {
				// if it hasn't even been saved yet, don't ask for the one day code
				gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPASSWORD,"3333");
				gridUser.RowData(lngRow,true);
				MessageBox.Show("Password Cleared", "Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			lngResponse = 0;
			if (frmInput.InstancePtr.Init(ref lngResponse, "One Day Code", "Contact TRIO for the GN one day code to use this option", 1440, false, modGlobalConstants.InputDTypes.idtWholeNumber, "", true)) {
				if (modModDayCode.CheckWinModCode_6((int)lngResponse, "GN")) {
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPASSWORD, "3333");
					gridUser.RowData(lngRow, true);
					rsSave.Execute("update signatures set password = '����' where id = "+Convert.ToString(Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLAUTOID))), "SystemSettings");
				} else {
					MessageBox.Show("Code is not correct", "Invalid Code", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
		}

		private void gridUser_KeyDownEdit(object sender, KeyEventArgs e)
		{
			int lngRow;
			lngRow = gridUser.Row;
			switch (e.KeyCode) {
				
				case Keys.F1:
				{
					if (e.Shift) {
						ResetPassword(lngRow);
					}
					break;
				}
			} //end switch
		}

		private void gridUser_RowColChange(object sender, System.EventArgs e)
		{
			if (gridUser.Row>0) {
				FillSigFrame(gridUser.Row);
			}
		}

		private void mnuAddUser_Click(object sender, System.EventArgs e)
		{
			AddUser();
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			DeleteUser();
		}

		private void AddUser()
		{
			int lngRow = 0;
			int lngType = 0;
			string strMod = "";

			if (boolFullPrivileges) {
				if (mnuAddUser.Visible) {
					// lngType = Val(gridType.TextMatrix(0, 0))
					lngType = cmbType.ItemData(cmbType.SelectedIndex);
					switch (lngType) {
						
						case modSignatureFile.BUDGETARYSIG:
						{
							strMod = "BD";
							break;
						}
						case modSignatureFile.COLLECTORSIG:
						{
							strMod = "CL";
							break;
						}
						case modSignatureFile.TREASURERSIG:
						{
							strMod = "CL";
							break;
						}
						case modSignatureFile.CLERKSIG:
						{
							strMod = "CK";
							break;
						}
						case modSignatureFile.PAYROLLSIG:
						{
							strMod = "PY";
							break;
						}
						case modSignatureFile.UTILITYSIG:
						{
							strMod = "UT";
							break;
						}
					} //end switch
					gridUser.Rows += 1;
					lngRow = gridUser.Rows-1;
					gridUser.TopRow = lngRow;
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLAUTOID, Convert.ToString(0));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODSPECIFIC, "");
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODSPECIFICID, Convert.ToString(0));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODULE, strMod);
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLNAME, "New Signature");
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPASSWORD, "");
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLTYPE, Convert.ToString(lngType));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE, "");
					gridUser.RowData(lngRow, true);
				}
			}
		}

		private void DeleteUser()
		{
			if (boolFullPrivileges) {
				if (gridUser.Row<1) return;
				if (mnuDelete.Visible) {
					if (Conversion.Val(gridUser.TextMatrix(gridUser.Row, CNSTGRIDUSERCOLAUTOID))>0) {
						GridDelete.Rows += 1;
						GridDelete.TextMatrix(GridDelete.Rows-1, 0, Convert.ToString(Conversion.Val(gridUser.TextMatrix(gridUser.Row, CNSTGRIDUSERCOLAUTOID))));
					}
					gridUser.RemoveItem(gridUser.Row);
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}
		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}


		private void SetupcmbType()
		{
			try
			{	// On Error GoTo ErrorHandler
				// Dim strTemp As String
				// With gridType
				// .Rows = 1
				// strTemp = "#" & BUDGETARYSIG & ";Budgetary Checks|#" & CLERKSIG & ";Clerk Vitals|#" & PAYROLLSIG & ";Payroll Checks|#" & COLLECTORSIG & ";Tax Collector|"
				// strTemp = strTemp & "#" & TREASURERSIG & ";Tax Treasurer|#" & UTILITYSIG & ";Utility Lien Process"
				// .ColComboList(0) = strTemp
				// .TextMatrix(0, 0) = BUDGETARYSIG
				// ReShowUsers
				// End With

				cmbType.Clear();
				cmbType.AddItem("Budgetary Checks");
				cmbType.ItemData(cmbType.NewIndex, modSignatureFile.BUDGETARYSIG);
				cmbType.AddItem("Clerk Vitals");
				cmbType.ItemData(cmbType.NewIndex, modSignatureFile.CLERKSIG);
				cmbType.AddItem("Payroll Checks");
				cmbType.ItemData(cmbType.NewIndex, modSignatureFile.PAYROLLSIG);
				cmbType.AddItem("Tax Collector");
				cmbType.ItemData(cmbType.NewIndex, modSignatureFile.COLLECTORSIG);
				cmbType.AddItem("Tax Treasurer");
				cmbType.ItemData(cmbType.NewIndex, modSignatureFile.TREASURERSIG);
				cmbType.AddItem("Utility Lien Process");
				cmbType.ItemData(cmbType.NewIndex, modSignatureFile.UTILITYSIG);
				cmbType.SelectedIndex = 0;

				return;
			}
			catch
			{	// ErrorHandler:
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err().Number)+" "+Information.Err().Description+"\n"+"In SetupcmbType", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridUser()
		{

			gridUser.Cols = 8;
			gridUser.ColHidden(CNSTGRIDUSERCOLAUTOID, true);
			gridUser.ColHidden(CNSTGRIDUSERCOLPASSWORD, true);
			gridUser.ColHidden(CNSTGRIDUSERCOLTYPE, true);
			gridUser.ColHidden(CNSTGRIDUSERCOLMODULE, true);
			gridUser.ColHidden(CNSTGRIDUSERCOLMODSPECIFIC, true);
			gridUser.ColHidden(CNSTGRIDUSERCOLMODSPECIFICID, true);
			gridUser.ColHidden(CNSTGRIDUSERCOLPICTURE, true);
			gridUser.TextMatrix(0, CNSTGRIDUSERCOLNAME, "Name");

		}

		// Private Sub ResizeGridType()
		// Dim GridHeight As Long
		// With gridType
		// GridHeight = .RowHeight(0) + 50
		// .Height = GridHeight
		// End With
		// End Sub


		private void LoadGridUser()
		{
			try
			{	// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;


				gridUser.Rows = 1;
				rsLoad.OpenRecordset("Select * from signatures order by type", "SystemSettings");

				while (!rsLoad.EndOfFile()) {
					gridUser.Rows += 1;
					lngRow = gridUser.Rows-1;
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLAUTOID, Convert.ToString(rsLoad.Get_Fields_Int32("id")));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODSPECIFIC, Convert.ToString(rsLoad.Get_Fields_String("modspecific")));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODSPECIFICID, Convert.ToString(rsLoad.Get_Fields_Int32("modspecificid")));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODULE, Convert.ToString(rsLoad.Get_Fields_String("module")));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLNAME, Convert.ToString(rsLoad.Get_Fields_String("name")));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPASSWORD, modEncrypt.ToggleEncryptCode(rsLoad.Get_Fields_String("password")));
					// TODO: Check the table for the column [type] and replace with corresponding Get_Field method
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLTYPE, Convert.ToString(rsLoad.Get_Fields("type")));
					gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE, "");
					gridUser.RowData(lngRow, false);
					gridUser.RowHidden(lngRow, true);
					rsLoad.MoveNext();
				}

				return;
			}
			catch
			{	// ErrorHandler:
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err().Number)+" "+Information.Err().Description+"\n"+"In LoadGridUser", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			try
			{	// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngRow;
				int lngID = 0;
				int lngType = 0;
				string strMod = "";
				string strFilename = "";
				string strSrc = "";

				FileSystemObject fso = new FileSystemObject();

				SaveInfo = false;

				// handle deletions first
				for(lngRow=0; lngRow<=GridDelete.Rows-1; lngRow++) {
					modGlobalFunctions.AddCYAEntry_8("GN", "Deleted signature entry "+GridDelete.TextMatrix(lngRow, 0));
					rsSave.Execute("delete from signatures where id = "+Convert.ToString(Conversion.Val(GridDelete.TextMatrix(lngRow, 0))), "SystemSettings");
				} // lngRow
				GridDelete.Rows = 0;

				rsSave.OpenRecordset("select * from signatures", "SystemSettings");
				for(lngRow=1; lngRow<=gridUser.Rows-1; lngRow++) {
					lngID = (int)Math.Round(Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLAUTOID)));
					if (lngID==0 || Convert.ToBoolean(gridUser.RowData(lngRow))) {
						if (lngID==0) {
							rsSave.AddNew();
							// lngID = rsSave.Fields("id")
							gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLAUTOID, Convert.ToString(lngID));
							rsSave.Set_Fields("Type", Convert.ToString(Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLTYPE))));
							rsSave.Set_Fields("Module", gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODULE));
						} else {
							if (rsSave.FindFirstRecord("id", lngID)) {
								rsSave.Edit();
							}
						}
						if (Strings.Trim(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPASSWORD))!=string.Empty) {
							rsSave.Set_Fields("password", modEncrypt.ToggleEncryptCode(Strings.Trim(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPASSWORD))));
						}
						rsSave.Set_Fields("name", gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLNAME));
						rsSave.Set_Fields("modspecific", gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODSPECIFIC));
						rsSave.Set_Fields("modspecificid", Convert.ToString(Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODSPECIFICID))));
						rsSave.Update();
						lngID = Convert.ToInt32(rsSave.Get_Fields_Int32("id"));
						if (Strings.Trim(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE))!=string.Empty) {
							strSrc = Strings.Trim(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE));
							strMod = gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODULE);
							lngType = (int)Math.Round(Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLTYPE)));
							strFilename = modSignatureFile.GetSigFileName(ref lngID, ref strMod, ref lngType);
							if (fso.FileExists(strFilename)) {
								fso.DeleteFile(strFilename, true);
							}
							fso.CopyFile(strSrc, Environment.CurrentDirectory+"\\"+strFilename, true);
							gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLPICTURE, "");
						} else {
							lngType = (int)Math.Round(Conversion.Val(gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLTYPE)));
							strMod = gridUser.TextMatrix(lngRow, CNSTGRIDUSERCOLMODULE);
							strFilename = modSignatureFile.GetSigFileName(ref lngID, ref strMod, ref lngType);
							if (fso.FileExists(strFilename)) {
								fso.DeleteFile(strFilename, true);
							}
						}
						gridUser.RowData(lngRow, false);
					}
				} // lngRow

				SaveInfo = true;
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveInfo;
			}
			catch
			{	// ErrorHandler:
				MessageBox.Show("Error Number "+Convert.ToString(Information.Err().Number)+" "+Information.Err().Description+"\n"+"In SaveInfo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}


		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (SaveInfo()) {
				mnuExit_Click();
			}
		}

	}
}
