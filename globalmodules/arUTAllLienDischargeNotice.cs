﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using TWSharedLibrary.Data;

using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWUT0000
using modGlobal = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for arUTAllLienDischargeNotice.
	/// </summary>
	public partial class arUTAllLienDischargeNotice : BaseSectionReport
	{
		// nObj = 1
		//   0	arUTAllLienDischargeNotice	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/16/2004              *
		// *
		// MODIFIED BY    :               Melissa Lamprecht       *
		// LAST UPDATED   :               09/18/2007              *
		// ********************************************************
		clsDRWrapper rsLien = new clsDRWrapper();
		clsDRWrapper rsRateRec = new clsDRWrapper();
		string strMainText;
		string strTreasurerName = "";
		DateTime dtPaymentDate;
		string strOwnerName = "";
		// vbPorter upgrade warning: dtLienDate As DateTime	OnWriteFCConvert.ToInt16(
		DateTime dtLienDate;
		string strBook = "";
		string strPage = "";
		string strCounty = "";
		string strHisHer = "";
		string strMuniName = "";
		int lngLineLen;
		string strSignerDesignation = "";
		string strSignerName = "";
		DateTime dtCommissionExpiration;
		string strTitle = "";
		int lngCurrentLien;
		DateTime dtTreasSignDate;
		bool boolWater;
		string strWS = "";
		bool blnFirstRecord;
		DateTime dtAppearedDate;
		// MAL@20070918
		bool boolDefaultAppearedDate;
		// kk10292015 trouts-171  Make UT LDN process like CL
		public arUTAllLienDischargeNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Discharge Notice";
            this.ReportEnd += ArUTAllLienDischargeNotice_ReportEnd;
		}

        private void ArUTAllLienDischargeNotice_ReportEnd(object sender, EventArgs e)
        {
			rsLien.DisposeOf();
            rsRateRec.DisposeOf();

		}

        public static arUTAllLienDischargeNotice InstancePtr
		{
			get
			{
				return (arUTAllLienDischargeNotice)Sys.GetInstance(typeof(arUTAllLienDischargeNotice));
			}
		}

		protected arUTAllLienDischargeNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            rsLien.DisposeOf();
            rsRateRec.DisposeOf();
			base.Dispose(disposing);
		}

		private void SetStrings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strPaymentDate = "";
				string strFormatPaymentDate = "";
				string strFormatTreasSignDate = "";
				string strWSString = "";
				string strAppearedDate = "";
				lngLineLen = 35;
				// this is how many underscores are in the printed lines
				lblTownHeader.Text = strMuniName;
				fldMuni.Text = lblTownHeader.Text;
				if (boolWater)
				{
					// kk12162015 trouts-154  Change WATER to STORMWATER for Bangor
					if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
					{
						lblTitleBar.Text = "DISCHARGE OF MORTGAGE FOR STORMWATER CERTIFICATE";
						strWSString = "stormwater";
					}
					else
					{
						lblTitleBar.Text = "DISCHARGE OF MORTGAGE FOR WATER CERTIFICATE";
						strWSString = "water";
					}
					// kk 01232013 trout-817  Remmove legal ref from Discharge Notices
					// lblLegalDescription.Text = "Title 38, M.R.S.A. Section 1208"
					lblLegalDescription.Text = "";
					// kk12162015    strWSString = "water"
				}
				else
				{
					lblTitleBar.Text = "DISCHARGE OF MORTGAGE FOR SEWER CERTIFICATE";
					// lblLegalDescription.Text = "Title 38, M.R.S.A. Section 1208"
					lblLegalDescription.Text = "";
					strWSString = "sewer";
				}
				if (dtPaymentDate.ToOADate() != 0)
				{
					strPaymentDate = Strings.Format(dtPaymentDate, "MMMM dd, yyyy");
					strFormatPaymentDate = Strings.Format(dtPaymentDate, "MMMM dd, yyyy");
				}
				else
				{
					strPaymentDate = Strings.StrDup(15, "_");
					strFormatPaymentDate = Strings.StrDup(15, "_");
				}
				if (dtTreasSignDate.ToOADate() != 0)
				{
					strFormatTreasSignDate = Strings.Format(dtTreasSignDate, "MMMM dd, yyyy");
				}
				else
				{
					// if there is no treasurer signing date, then use the current date
					strFormatTreasSignDate = Strings.Format(DateTime.Today, "MMMM dd, yyyy");
				}
				// MAL@20070918
				if (dtAppearedDate.ToOADate() != 0)
				{
					strAppearedDate = Strings.Format(dtAppearedDate, "MMMM dd, yyyy");
				}
				else
				{
					if (boolDefaultAppearedDate)
					{
						// kk10292015 trouts-171  Make UT like CL
						// default this to the payment date
						strAppearedDate = Strings.Format(dtPaymentDate, "MMMM dd, yyyy");
					}
					else
					{
						// if the user does not want to default this to the payment date then leave a blank line for them to write it in
						strAppearedDate = Strings.StrDup(15, "_");
					}
				}
				strMainText = "\t" + "I, " + strTreasurerName + ", in my capacity as " + strTitle + " of the ";
				strMainText += strMuniName + ", hereby acknowledge that on " + strPaymentDate;
				strMainText += " I received full payment and satisfaction of the debt secured by the " + strWSString + " lien against property assessed to ";
				strMainText += strOwnerName + " created by the recording of a " + strWSString + " lien certificate dated ";
				if (dtLienDate.ToOADate() != 0)
				{
					strMainText += FCConvert.ToString(dtLienDate) + " in Book " + strBook + ", at Page " + strPage + " of the ";
				}
				else
				{
					strMainText += Strings.StrDup(15, "_") + " in Book " + strBook + ", at Page " + strPage + " of the ";
				}
				strMainText += strCounty + " County Registry of Deeds, and in consideration thereof I hereby discharge said " + strWSString + " lien.";
				fldMainText.Text = strMainText;
				fldTopDate.Text = "";
				// "Dated: " & Format(dtPaymentDate, "MMMM DD, YYYY")
				// fldSigLine.Text = String(lngLineLen, "_")
				fldSigTitle.Text = "";
				// "Treasurer"
				fldSigName.Text = strTreasurerName + ", " + strTitle;
				fldSigDate.Text = "Dated: " + strFormatTreasSignDate;
				// fldACKNOWLEDGEMENT = "State of Maine"
				fldACKNOWLEDGEMENT.Text = "ACKNOWLEDGEMENT";
				strMainText = strMuniName + "\r\n";
				strMainText += "State of Maine" + "\r\n" + strCounty + " County, ss." + "\r\n" + "\r\n";
				// strMainText = strMainText & "Personally appeared before me the above-named " & strTreasurerName        'MAL@20070918
				strMainText += "Personally appeared before me, on " + strAppearedDate + ", the above-named " + strTreasurerName;
				strMainText += ", who acknowledged the foregoing to be ";
				strMainText += strHisHer + " free act and deed in " + strHisHer + " capacity as " + strTitle + ".";
				fldBottomText.Text = strMainText;
				fldNotaryLine.Text = Strings.StrDup(lngLineLen, "_");
				if (strSignerName != "" && strSignerDesignation != "")
				{
					fldNotaryTitle.Text = strSignerName + ", " + strSignerDesignation;
					// "Notary Public / Attorney at Law"
				}
				else if (strSignerName != "")
				{
					fldNotaryTitle.Text = strSignerName;
				}
				else
				{
					fldNotaryTitle.Text = strSignerDesignation;
				}
				// fldNotaryPrintLine.Text = String(lngLineLen, "_")
				// fldNotaryPrintTitle.Text = strSignerName ' "Printed Name"
				// fldNotaryCommissionLine.Text = String(lngLineLen, "_")
				if (dtCommissionExpiration.ToOADate() != 0)
				{
					fldNotaryCommissionTitle.Text = "My commission expires: " + Strings.Format(dtCommissionExpiration, "MMMM dd, yyyy");
				}
				else
				{
					fldNotaryCommissionTitle.Text = "My commission expires: ";
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Setting Variables", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void Init(bool boolPassWater, string strPassTeasName, string strPassMuni, string strPassCounty, string strPassHisHer, string strPassSignerName, string strPassSignerDesignation, DateTime dtPassCommissionExpiration, string strPassTitle = "Treasurer", DateTime? dtPassTreasSignDateTemp = null, DateTime? dtPassAppearedDateTemp = null, bool boolPassDefaultAppearedDate = false, bool blnPrintArchive = false)
		{
			DateTime dtPassTreasSignDate = dtPassTreasSignDateTemp ?? DateTime.Now;
			DateTime dtPassAppearedDate = dtPassAppearedDateTemp ?? DateTime.Now;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will set all of the variables needed
				if (boolPassWater)
				{
					boolWater = true;
					strWS = "W";
				}
				else
				{
					boolWater = false;
					strWS = "S";
				}
				if (!blnPrintArchive)
				{
					rsLien.OpenRecordset("SELECT * FROM (SELECT LienKey, PrintedLDN, DatePaid FROM Lien INNER JOIN DischargeNeeded ON Lien.ID = DischargeNeeded.LienKey WHERE ISNULL(Printed,0) = 0) AS Temp INNER JOIN Bill ON Temp.LienKey = Bill." + strWS + "LienRecordNumber ORDER BY LienKey", modExtraModules.strUTDatabase);
				}
				else
				{
					rsLien.OpenRecordset("SELECT * FROM (SELECT LienKey, PrintedLDN, DatePaid FROM Lien INNER JOIN DischargeNeededArchive ON Lien.ID = DischargeNeededArchive.LienKey) AS Temp INNER JOIN Bill ON Temp.LienKey = Bill." + strWS + "LienRecordNumber ORDER BY LienKey", modExtraModules.strUTDatabase);
				}
				// Row - 1  'Treasurer Name
				strTreasurerName = strPassTeasName;
				// Row - 2  'Muni Name
				strMuniName = strPassMuni;
				// Row - 3  'County
				strCounty = strPassCounty;
				lblTownHeader.Text = strMuniName;
				boolDefaultAppearedDate = boolPassDefaultAppearedDate;
				// kk10292015 trouts-171 Make UT like CL
				// Row - 10 'His/Her
				strHisHer = Strings.LCase(strPassHisHer);
				if (Strings.Trim(strHisHer) == "")
				{
					strHisHer = "his/her";
				}
				// title
				strTitle = strPassTitle;
				// Row - 11 'Signer Name
				strSignerName = strPassSignerName;
				// Row - 12 'Signer's Designation
				strSignerDesignation = strPassSignerDesignation;
				// Row - 13 ' Commission expiration date
				dtCommissionExpiration = dtPassCommissionExpiration;
				// treasurer signing date
				dtTreasSignDate = dtPassTreasSignDate;
				// MAL@20070918
				// Appeared Date
				dtAppearedDate = dtPassAppearedDate;
				if (modGlobal.Statics.gdblLienSigAdjust != 0)
				{
					imgSig.Visible = true;
					imgSig.BringToFront();
					Line2.SendToBack();
					imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrTreasSigPath);
				}
				else
				{
					imgSig.Visible = false;
				}
				frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "", false, false);
				frmUTLienDischargeNotice.InstancePtr.Unload();
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				lngCurrentLien = FCConvert.ToInt32(rsLien.Get_Fields_Int32("LienKey"));
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				CheckNext:
				;
				rsLien.MoveNext();
				if (rsLien.EndOfFile() != true)
				{
					if (lngCurrentLien != FCConvert.ToInt32(rsLien.Get_Fields_Int32("LienKey")))
					{
						lngCurrentLien = FCConvert.ToInt32(rsLien.Get_Fields_Int32("LienKey"));
						eArgs.EOF = false;
					}
					else
					{
						goto CheckNext;
					}
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// SetVariables
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				blnFirstRecord = true;
				// kk05082014 trout-1082  Add print adjustments to UT
				// set the top and bottom margins
				// kk06122014 trout-10825  Conflicting Globals in CR - change gdblLDNAdjustment... to gdblUTLDNAdjustment...
				// kk02042016 trout-1197  New margins effective 10/2015 - 1.5" Top 1st page, 1.5" Bottom last page
				this.PageSettings.Margins.Top = FCConvert.ToSingle((1.5 * 1440) + (modUTCalculations.Statics.gdblUTLDNAdjustmentTop * 1440)) / 1440F;
				this.PageSettings.Margins.Bottom = FCConvert.ToSingle((1.5 * 1440) + (modUTCalculations.Statics.gdblUTLDNAdjustmentBottom * 1440)) / 1440F;
				this.PageSettings.Margins.Left = FCConvert.ToSingle((0.75 * 1440) + (modUTCalculations.Statics.gdblUTLDNAdjustmentSide * 1440)) / 1440F;
				this.PageSettings.Margins.Right = this.PageSettings.Margins.Left;
				this.PrintWidth = 8.5F - (this.PageSettings.Margins.Left + this.PageSettings.Margins.Right);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Report Start", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			clsDRWrapper rsUpd = new clsDRWrapper();
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will update all of the lien record that have just been printed
				rsLien.MoveFirst();
				while (!rsLien.EndOfFile())
				{
					// kgk  rsLien.Edit  'can't update join
					// rsLien.Fields("PrintedLDN") = True
					// rsLien.Update
					rsUpd.Execute("UPDATE Lien SET PrintedLDN = 1 WHERE ID = " + rsLien.Get_Fields_Int32("LienKey"), modExtraModules.strUTDatabase);
					rsLien.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Updating Lien Records", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			FillVariables();
			SetStrings();
		}

		private void FillVariables()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsInfo = new clsDRWrapper();
				clsDRWrapper rsREMaster = new clsDRWrapper();
				rsInfo.OpenRecordset("SELECT ActualAccountNumber, OName, OName2, Lien.RateKey, Lien.Book, Lien.Page, MapLot FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID WHERE Lien.ID = " + FCConvert.ToString(lngCurrentLien), modExtraModules.strUTDatabase);
				if (!rsInfo.EndOfFile())
				{
					// Row - 4  'Name1
					strOwnerName = FCConvert.ToString(rsInfo.Get_Fields_String("OName"));
					// Row - 5  'Name2
					if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("OName2"))) != "")
					{
						strOwnerName += " " + rsInfo.Get_Fields_String("OName2");
					}
					// Row - 6  'Payment Date
					dtPaymentDate = (DateTime)rsLien.Get_Fields_DateTime("DatePaid");
					rsRateRec.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsInfo.Get_Fields_Int32("RateKey"));
					if (!rsRateRec.EndOfFile())
					{
						// Row - 7  'Filing Date
						dtLienDate = (DateTime)rsRateRec.Get_Fields_DateTime("BillDate");
					}
					else
					{
						dtLienDate = DateTime.FromOADate(0);
					}
					// Row - 8  'Book
					// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
					strBook = FCConvert.ToString(rsInfo.Get_Fields("Book"));
					if (Strings.Trim(strBook) == "" || Conversion.Val(strBook) == 0)
					{
						// kk10022014 trout-1111  Add check for Book = 0
						strBook = Strings.StrDup(10, "_");
					}
					// Row - 9  'Page
					// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
					strPage = FCConvert.ToString(rsInfo.Get_Fields("Page"));
					if (Strings.Trim(strPage) == "" || Conversion.Val(strPage) == 0)
					{
						// kk10022014 trout-1111  Add check for Page = 0
						strPage = Strings.StrDup(10, "_");
					}
					if (FCConvert.ToString(rsInfo.Get_Fields_String("MapLot")) != "")
					{
						lblMapLot.Text = rsInfo.Get_Fields_String("MapLot");
					}
					else
					{
						rsREMaster.OpenRecordset("SELECT MapLot FROM Master WHERE AccountNumber = " + rsInfo.Get_Fields_Int32("ActualAccountNumber"), modExtraModules.strUTDatabase);
						if (!rsREMaster.EndOfFile())
						{
							lblMapLot.Text = rsREMaster.Get_Fields_String("MapLot");
						}
						else
						{
							lblMapLot.Text = "";
						}
					}
					fldAccount.Text = modGlobal.PadToString_8(rsInfo.Get_Fields_Int32("ActualAccountNumber"), 5);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Variables", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void arUTAllLienDischargeNotice_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arUTAllLienDischargeNotice.Caption	= "Lien Discharge Notice";
			//arUTAllLienDischargeNotice.Icon	= "arUTAllLienDischargeNotice.dsx":0000";
			//arUTAllLienDischargeNotice.Left	= 0;
			//arUTAllLienDischargeNotice.Top	= 0;
			//arUTAllLienDischargeNotice.Width	= 16110;
			//arUTAllLienDischargeNotice.Height	= 12030;
			//arUTAllLienDischargeNotice.StartUpPosition	= 3;
			//arUTAllLienDischargeNotice.SectionData	= "arUTAllLienDischargeNotice.dsx":058A;
			//End Unmaped Properties
		}
	}
}
