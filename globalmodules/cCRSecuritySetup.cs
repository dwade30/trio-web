﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cCRSecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cCRSecuritySetup() : base()
		{
			strThisModule = "CR";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem(" Entry into Cash Receipting", "", 1, false, ""));
			AddItem_2(CreateItem("Bank Maintenance", "", 22, false, ""));
			AddItem_2(CreateItem("Customize", "", 14, true, ""));
			AddItem_2(CreateItem("Customize", "Cash Drawer Codes", 18, false, ""));
			AddItem_2(CreateItem("Customize", "Change Comments", 19, false, ""));
			AddItem_2(CreateItem("Customize", "Change Teller Defaults", 21, false, ""));
			AddItem_2(CreateItem("Customize", "Password Protect Audit", 17, false, ""));
			AddItem_2(CreateItem("Customize", "Report Alignment", 20, false, ""));
			AddItem_2(CreateItem("Customize", "Reset Receipt Number", 15, false, ""));
			AddItem_2(CreateItem("Daily Receipt Audit", "", 4, true, ""));
			AddItem_2(CreateItem("Daily Receipt Audit", "Perform Daily Audit", 16, false, ""));
			AddItem_2(CreateItem("End of Year", "", 10, false, ""));
			AddItem_2(CreateItem("File Maintenance", "", 6, false, ""));
			AddItem_2(CreateItem("MVR3 Listing", "", 13, false, ""));
			AddItem_2(CreateItem("Open Cash Drawer", "", 9, false, ""));
			AddItem_2(CreateItem("Receipt Search Routines", "", 8, false, ""));
			AddItem_2(CreateItem("Receipt Type Listing", "", 12, false, ""));
			AddItem_2(CreateItem("Receipt Input", "", 2, false, ""));
			AddItem_2(CreateItem("Redisplay Last Receipt", "", 3, false, ""));
			AddItem_2(CreateItem("Remove Lien Maturity Fees", "", 23, false, ""));
			AddItem_2(CreateItem("Reprint Any Receipt", "", 11, false, ""));
			AddItem_2(CreateItem("Reprint Last Daily Audit", "", 5, false, ""));
			AddItem_2(CreateItem("Setup Receipt Types", "", 7, false, ""));
		}
	}
}
