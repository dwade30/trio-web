﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmPartyList.
	/// </summary>
	public partial class frmPartyList : BaseForm
	{
		public frmPartyList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPartyList InstancePtr
		{
			get
			{
				return (frmPartyList)Sys.GetInstance(typeof(frmPartyList));
			}
		}

		protected frmPartyList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private DialogResult intReturn;
		// VBto upgrade warning: varList As Collection	OnReadFCConvert.ToInt32(
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private FCCollection varList = new FCCollection();
		private FCCollection varList_AutoInitialized;

		private FCCollection varList
		{
			get
			{
				if (varList_AutoInitialized == null)
				{
					varList_AutoInitialized = new FCCollection();
				}
				return varList_AutoInitialized;
			}
			set
			{
				varList_AutoInitialized = value;
			}
		}

		private bool boolLoaded;
		const int IDCol = 0;
		const int NameCol = 1;
		const int AddressCol = 2;
		const int EmailCol = 3;

		private void cmdNo_Click(object sender, System.EventArgs e)
		{
			intReturn = DialogResult.No;
			Close();
		}

		private void cmdYes_Click(object sender, System.EventArgs e)
		{
			intReturn = DialogResult.Yes;
			Close();
		}

		private void frmPartyList_Activated(object sender, System.EventArgs e)
		{
			// If Not boolLoaded Then
			// LoadGrid
			// End If
			// boolLoaded = True
		}

		private void frmPartyList_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				Close();
			}
		}

		private void frmPartyList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPartyList.Icon	= "frmPartyList.frx":0000";
			//frmPartyList.FillStyle	= 0;
			//frmPartyList.ScaleWidth	= 9300;
			//frmPartyList.ScaleHeight	= 7980;
			//frmPartyList.LinkTopic	= "Form2";
			//frmPartyList.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//SearchGrid.BackColor	= "-2147483643";
			//			//SearchGrid.ForeColor	= "-2147483640";
			//SearchGrid.BorderStyle	= 1;
			//SearchGrid.FillStyle	= 0;
			//SearchGrid.Appearance	= 1;
			//SearchGrid.GridLines	= 1;
			//SearchGrid.WordWrap	= -1;
			//SearchGrid.ScrollBars	= 3;
			//SearchGrid.RightToLeft	= 0;
			//SearchGrid._cx	= 15796;
			//SearchGrid._cy	= 9499;
			//SearchGrid._ConvInfo	= 1;
			//SearchGrid.MousePointer	= 0;
			//SearchGrid.BackColorFixed	= -2147483633;
			//			//SearchGrid.ForeColorFixed	= -2147483630;
			//SearchGrid.BackColorSel	= -2147483635;
			//			//SearchGrid.ForeColorSel	= -2147483634;
			//SearchGrid.BackColorBkg	= -2147483636;
			//SearchGrid.BackColorAlternate	= -2147483643;
			//SearchGrid.GridColor	= -2147483633;
			//SearchGrid.GridColorFixed	= -2147483632;
			//SearchGrid.TreeColor	= -2147483632;
			//SearchGrid.FloodColor	= 192;
			//SearchGrid.SheetBorder	= -2147483642;
			//SearchGrid.FocusRect	= 1;
			//SearchGrid.HighLight	= 1;
			//SearchGrid.AllowSelection	= true;
			//SearchGrid.AllowBigSelection	= true;
			//SearchGrid.AllowUserResizing	= 0;
			//SearchGrid.SelectionMode	= 0;
			//SearchGrid.GridLinesFixed	= 2;
			//SearchGrid.GridLineWidth	= 1;
			//SearchGrid.RowHeightMin	= 0;
			//SearchGrid.RowHeightMax	= 0;
			//SearchGrid.ColWidthMin	= 0;
			//SearchGrid.ColWidthMax	= 0;
			//SearchGrid.ExtendLastCol	= true;
			//SearchGrid.FormatString	= "";
			//SearchGrid.ScrollTrack	= false;
			//SearchGrid.ScrollTips	= false;
			//SearchGrid.MergeCells	= 0;
			//SearchGrid.MergeCompare	= 0;
			//SearchGrid.AutoResize	= true;
			//SearchGrid.AutoSizeMode	= 0;
			//SearchGrid.AutoSearch	= 0;
			//SearchGrid.AutoSearchDelay	= 2;
			//SearchGrid.MultiTotals	= true;
			//SearchGrid.SubtotalPosition	= 1;
			//SearchGrid.OutlineBar	= 0;
			//SearchGrid.OutlineCol	= 0;
			//SearchGrid.Ellipsis	= 0;
			//SearchGrid.ExplorerBar	= 1;
			//SearchGrid.PicturesOver	= false;
			//SearchGrid.PictureType	= 0;
			//SearchGrid.TabBehavior	= 0;
			//SearchGrid.OwnerDraw	= 0;
			//SearchGrid.ShowComboButton	= true;
			//SearchGrid.TextStyle	= 0;
			//SearchGrid.TextStyleFixed	= 0;
			//SearchGrid.OleDragMode	= 0;
			//SearchGrid.OleDropMode	= 0;
			//SearchGrid.ComboSearch	= 3;
			//SearchGrid.AutoSizeMouse	= true;
			//SearchGrid.AllowUserFreezing	= 0;
			//SearchGrid.BackColorFrozen	= 0;
			//			//SearchGrid.ForeColorFrozen	= 0;
			//SearchGrid.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmPartyList.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetGridProperties();
			LoadGrid();
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Init(ref FCCollection varArray)
		{
			short Init = 0;
			intReturn = DialogResult.Yes;
			varList = varArray;
			Init = FCConvert.ToInt16(intReturn);
			this.Show(FormShowEnum.Modal);
			Init = FCConvert.ToInt16(intReturn);
			return Init;
		}

		private void ResizeGrid()
		{
			// .ColHidden(IDCol) = True
			SearchGrid.ColWidth(IDCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.07));
			SearchGrid.ColWidth(NameCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.35));
			SearchGrid.ColWidth(AddressCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.33));
			// .ColWidth(EmailCol) = .Width * 0.03
		}

		private void SetGridProperties()
		{
			SearchGrid.Cols = 4;
			// .ColHidden(IDCol) = True
			SearchGrid.ColWidth(IDCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.03));
			SearchGrid.ColWidth(NameCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.35));
			SearchGrid.ColWidth(AddressCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.35));
			SearchGrid.ColWidth(EmailCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.03));
			//SearchGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, SearchGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			SearchGrid.TextMatrix(0, NameCol, "Name");
			SearchGrid.TextMatrix(0, AddressCol, "Address");
			SearchGrid.TextMatrix(0, EmailCol, "E-Mail");
		}

		private void LoadGrid()
		{
			prgWait.Visible = true;
			lblWait.Visible = true;
			App.DoEvents();
			prgWait.Maximum = 100;
			prgWait.Value = 0;
			prgWait.Visible = true;
			// SearchGrid.Rows = UBound(varList) + 2
			SearchGrid.Rows = varList.Count + 1;
			SearchGrid.Redraw = false;
			SearchGrid.Visible = false;
			int lngOHeight;
			//FC:FINAL:AM: don't set the height; used anchoring instead
			//lngOHeight = SearchGrid.RowHeight(0);
			//SearchGrid.RowHeightMin = 3 * SearchGrid.RowHeight(0);
			//SearchGrid.RowHeight(0, lngOHeight);
			cParty tParty;
			cPartyController tPCont = new cPartyController();
			int x;
			// VBto upgrade warning: tAddr As cPartyAddress	OnWrite(Collection)
			cPartyAddress tAddr;
			int intPct = 0;
			// VBto upgrade warning: lngID As int	OnWrite(Collection)
			int lngID = 0;
			for (x = 1; x <= varList.Count; x++)
			{
				// For x = 0 To UBound(varList)
				lngID = varList[x];
				tParty = tPCont.GetParty(lngID);
				if (!(tParty == null))
				{
					SearchGrid.TextMatrix(x, IDCol, FCConvert.ToString(tParty.ID));
					SearchGrid.TextMatrix(x, NameCol, tParty.FullName);
					if (tParty.Addresses.Count > 0)
					{
						tAddr = tParty.Addresses[1];
						SearchGrid.TextMatrix(x, AddressCol, tAddr.GetFormattedAddress());
					}
					SearchGrid.TextMatrix(x, EmailCol, tParty.Email);
				}
				intPct = FCConvert.ToInt32(Conversion.Int(100 * (FCConvert.ToDouble(x) / FCConvert.ToDouble(varList.Count))));
				if (intPct > 100)
					intPct = 100;
				prgWait.Value = intPct;
				prgWait.Refresh();
				App.DoEvents();
			}
			// x
			SearchGrid.Redraw = true;
			SearchGrid.Visible = true;
			prgWait.Visible = false;
			lblWait.Visible = false;
		}

		private void frmPartyList_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}
	}
}
