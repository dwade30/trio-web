﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Drawing;

#if TWBD0000
using TWBD0000;

#endif
using fecherFoundation;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for rptSampleCheck.
	/// </summary>
	public partial class rptSampleCheck : BaseSectionReport
	{
		// nObj = 1
		//   0	rptSampleCheck	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		const int CheckUseCol = 2;
		const int CheckDescCol = 1;
		const int CheckXCol = 3;
		const int CheckYCol = 4;
		const int CheckTop = 4800;
		int intNumFields;

		public rptSampleCheck()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "SampleCheck";
		}

		public static rptSampleCheck InstancePtr
		{
			get
			{
				return (rptSampleCheck)Sys.GetInstance(typeof(rptSampleCheck));
			}
		}

		protected rptSampleCheck _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			int x;
			string strFont = "";
			string strTemp = "";
			clsReportPrinterFunctions clsPrt = new clsReportPrinterFunctions();
			GrapeCity.ActiveReports.SectionReportModel.TextBox ctl;
            using (clsDRWrapper rsFieldInfo = new clsDRWrapper())
            {
                GrapeCity.ActiveReports.SectionReportModel.Shape ctl2;
                //FC:FINAL:AM
                //this.Printer.PrintDialog();
                //FCCommonDialog dlg = new FCCommonDialog();
                //dlg.ShowPrinter();
                if (frmCreateCustomCheckFormat.InstancePtr.cmbCheckType.SelectedIndex == 1)
                {
                    // get a printer font
                    strFont = clsPrt.GetFont(FCGlobal.Printer.DeviceName, 10, "");
                }
                else
                {
                    strFont = "";
                }

                Line1.Y1 = CheckTop / 1440F;
                Line1.Y2 = CheckTop / 1440F;
                lblHelp.Top = CheckTop / 1440F - lblHelp.Height;
                // create the fields
                intNumFields = 0;
                for (x = 1; x <= frmCreateCustomCheckFormat.InstancePtr.vsCheckFields.Rows - 1; x++)
                {
                    if (FCConvert.CBool(frmCreateCustomCheckFormat.InstancePtr.vsCheckFields.TextMatrix(x, CheckUseCol))
                    )
                    {
                        intNumFields += 1;
                        strTemp = "CustomField" + FCConvert.ToString(intNumFields);
                        ctl = Detail.AddControlWithName<GrapeCity.ActiveReports.SectionReportModel.TextBox>(strTemp);
                        ctl.Top = FCConvert.ToSingle(
                            240 * Conversion.Val(
                                frmCreateCustomCheckFormat.InstancePtr.vsCheckFields
                                    .TextMatrix(x, CheckYCol)) + CheckTop) / 1440F;
                        ctl.Left = FCConvert.ToSingle(
                            144 * Conversion.Val(
                                frmCreateCustomCheckFormat.InstancePtr.vsCheckFields
                                    .TextMatrix(x, CheckXCol))) / 1440F;
                        ctl.WordWrap = false;
                        ctl.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                        rsFieldInfo.OpenRecordset("SELECT * FROM CustomCheckFields WHERE Description = '" +
                                                  frmCreateCustomCheckFormat.InstancePtr.vsCheckFields.TextMatrix(x,
                                                      CheckDescCol) + "'");
                        if (rsFieldInfo.EndOfFile() != true && rsFieldInfo.BeginningOfFile() != true)
                        {
                            if (Conversion.Val(rsFieldInfo.Get_Fields_Int16("FieldHeight")) > 0 &&
                                Conversion.Val(rsFieldInfo.Get_Fields_Int16("FieldWidth")) > 0)
                            {
                                ctl.Height = rsFieldInfo.Get_Fields_Int16("FieldHeight");
                                ctl.Width = rsFieldInfo.Get_Fields_Int16("FieldWidth");
                                Detail.Controls.Add(ctl2 = new GrapeCity.ActiveReports.SectionReportModel.Shape());
                                ctl2.Top = ctl.Top;
                                ctl2.Left = ctl.Left;
                                ctl2.Height = ctl.Height;
                                ctl2.Width = ctl.Width;
                            }
                            else
                            {
                                ctl.Width = 2880 / 1440f;
                                ctl.Height = 240 / 1440f;
                            }
                        }

                        //strTemp = "CustomField" + FCConvert.ToString(intNumFields);
                        //// ctl.Name = "HeaderRow" & CRow & "Col" & CCol
                        //ctl.Name = strTemp;
                        Fields.Add(ctl.Name);
                        // ctl.DataField = ctl.Name
                        if (strFont != string.Empty)
                        {
                            ctl.Font = new Font(strFont, ctl.Font.Size);
                        }

                        ctl.Text = frmCreateCustomCheckFormat.InstancePtr.vsCheckFields.TextMatrix(x, CheckDescCol);
                    }
                }

                // x
            }
        }

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// Unload Me
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// Dim x As Integer
			// 
			// For x = 1 To intNumFields
			// Next x
		}

		
	}
}
