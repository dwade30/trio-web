﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using System.Collections.Generic;
using TWSharedLibrary;

namespace Global
{
	public class cARPartyApp : SharedApplication.ICentralPartyApp
	{
		//=========================================================
		const string strModuleCode = "AR";

		public bool ReferencesParty(int lngID)
		{
			bool ICentralPartyApp_ReferencesParty = false;
			ICentralPartyApp_ReferencesParty = false;
			bool retAnswer;
			retAnswer = false;
			if (lngID > 0)
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select count(id)as theCount from customermaster where partyid = " + FCConvert.ToString(lngID), "AccountsReceivable");
				// TODO: Field [theCount] not found!! (maybe it is an alias?)
				if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
				{
					retAnswer = true;
				}
				ICentralPartyApp_ReferencesParty = retAnswer;
			}
			return ICentralPartyApp_ReferencesParty;
		}

		public List<object> AccountsReferencingParty(int lngID)
		{
			List<object> CentralPartyApp_AccountsReferencingParty = null;
			var theCollection = new List<object>();
			clsDRWrapper rsLoad = new clsDRWrapper();
			cCentralPartyReference pRef;

            try
            {
                if (lngID > 0)
                {
                    rsLoad.OpenRecordset("select * from customermaster where partyid = " + FCConvert.ToString(lngID), "AccountsReceivable");
                    while (!rsLoad.EndOfFile())
                    {
                        //Application.DoEvents();
                        pRef = new cCentralPartyReference();
                        pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id"));
                        pRef.Identifier = FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("CustomerID")));
                        pRef.AlternateIdentifier = "";
                        pRef.Description = "Customer";
                        pRef.ModuleCode = strModuleCode;
                        pRef.IdentifierForSort = Strings.Right(Strings.StrDup(12, "0") + FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Int32("CustomerID"))), 12);
                        theCollection.Add(pRef);
                        rsLoad.MoveNext();
                    }
                }
            }
            finally
            {
                rsLoad.DisposeOf();
            }
			CentralPartyApp_AccountsReferencingParty = theCollection;
			return CentralPartyApp_AccountsReferencingParty;
		}

		public string ModuleCode()
		{
			string ICentralPartyApp_ModuleCode = "";
			ICentralPartyApp_ModuleCode = strModuleCode;
			return ICentralPartyApp_ModuleCode;
		}

		public bool ChangePartyID(int lngOriginalPartyID, int lngNewPartyID)
		{
			bool ICentralPartyApp_ChangePartyID = false;
			clsDRWrapper rsSave = new clsDRWrapper();
			string strSQL;
			try
			{
				// On Error GoTo ErrorHandler
				strSQL = "Update Customermaster set partyid = " + FCConvert.ToString(lngNewPartyID) + " where partyid = " + FCConvert.ToString(lngOriginalPartyID);
				rsSave.Execute(strSQL, "AccountsReceivable");
				ICentralPartyApp_ChangePartyID = true;
				return ICentralPartyApp_ChangePartyID;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return ICentralPartyApp_ChangePartyID;
		}
	}
}
