﻿using fecherFoundation;
using Global;

namespace Global
{
    partial class frmAutoPay 
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
		{
			Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmbBatch = new fecherFoundation.FCComboBox();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.vsAutoPay = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileForcePrenote = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileProcPrenotes = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileForceClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSpacer = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPostPayments = new fecherFoundation.FCButton();
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.cmdSelectAll = new fecherFoundation.FCButton();
            this.cmdClearSelection = new fecherFoundation.FCButton();
            this.toolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsAutoPay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPostPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearSelection)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPostPayments);
            this.toolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsAutoPay);
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Controls.Add(this.cmbBatch);
            this.toolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClearSelection);
            this.TopPanel.Controls.Add(this.cmdSelectAll);
            this.TopPanel.Controls.Add(this.cmdPrintPreview);
            this.toolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrintPreview, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSelectAll, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClearSelection, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(242, 30);
            this.HeaderText.Text = "Auto-Pay Processing";
            this.toolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbBatch
            // 
            this.cmbBatch.Location = new System.Drawing.Point(136, 21);
            this.cmbBatch.Name = "cmbBatch";
            this.cmbBatch.Size = new System.Drawing.Size(176, 40);
            this.cmbBatch.TabIndex = 1;
            this.toolTip1.SetToolTip(this.cmbBatch, null);
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(73, 35);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(55, 17);
            this.fcLabel1.TabIndex = 2;
            this.fcLabel1.Text = "BATCH";
            this.toolTip1.SetToolTip(this.fcLabel1, null);
            // 
            // vsAutoPay
            // 
            this.vsAutoPay.Location = new System.Drawing.Point(73, 143);
            this.vsAutoPay.Name = "vsAutoPay";
            this.vsAutoPay.Rows = 50;
            this.vsAutoPay.Size = new System.Drawing.Size(696, 233);
            this.vsAutoPay.TabIndex = 188;
            this.toolTip1.SetToolTip(this.vsAutoPay, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileForcePrenote,
            this.mnuFileProcPrenotes,
            this.mnuFileForceClear,
            this.mnuSpacer,
            this.mnuProcessExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFileForcePrenote
            // 
            this.mnuFileForcePrenote.Index = 0;
            this.mnuFileForcePrenote.Name = "mnuFileForcePrenote";
            this.mnuFileForcePrenote.Text = "Force Prenote";
            // 
            // mnuFileProcPrenotes
            // 
            this.mnuFileProcPrenotes.Index = 1;
            this.mnuFileProcPrenotes.Name = "mnuFileProcPrenotes";
            this.mnuFileProcPrenotes.Text = "Process Prenotes";
            // 
            // mnuFileForceClear
            // 
            this.mnuFileForceClear.Index = 2;
            this.mnuFileForceClear.Name = "mnuFileForceClear";
            this.mnuFileForceClear.Text = "Force Clear";
            // 
            // mnuSpacer
            // 
            this.mnuSpacer.Index = 3;
            this.mnuSpacer.Name = "mnuSpacer";
            this.mnuSpacer.Text = "-";
            // 
            // mnuProcessExit
            // 
            this.mnuProcessExit.Index = 4;
            this.mnuProcessExit.Name = "mnuProcessExit";
            this.mnuProcessExit.Text = "Exit";
            this.mnuProcessExit.Click += new System.EventHandler(this.mnuProcessExit_Click);
            // 
            // cmdPostPayments
            // 
            this.cmdPostPayments.AppearanceKey = "acceptButton";
            this.cmdPostPayments.Location = new System.Drawing.Point(413, 30);
            this.cmdPostPayments.Name = "cmdPostPayments";
            this.cmdPostPayments.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPostPayments.Size = new System.Drawing.Size(188, 48);
            this.cmdPostPayments.TabIndex = 101;
            this.cmdPostPayments.Text = "Post Payments";
            this.toolTip1.SetToolTip(this.cmdPostPayments, null);
            this.cmdPostPayments.Click += new System.EventHandler(this.cmdPostPayments_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrintPreview.Location = new System.Drawing.Point(909, 31);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdPrintPreview.Size = new System.Drawing.Size(91, 24);
            this.cmdPrintPreview.TabIndex = 3;
            this.cmdPrintPreview.Text = "Print Preview";
            this.toolTip1.SetToolTip(this.cmdPrintPreview, null);
            this.cmdPrintPreview.Click += new System.EventHandler(this.cmdPrintPreview_Click);
            // 
            // cmdSelectAll
            // 
            this.cmdSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSelectAll.Location = new System.Drawing.Point(812, 31);
            this.cmdSelectAll.Name = "cmdSelectAll";
            this.cmdSelectAll.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdSelectAll.Size = new System.Drawing.Size(91, 24);
            this.cmdSelectAll.TabIndex = 4;
            this.cmdSelectAll.Text = "Select All";
            this.toolTip1.SetToolTip(this.cmdSelectAll, null);
            this.cmdSelectAll.Click += new System.EventHandler(this.cmdSelectAll_Click);
            // 
            // cmdClearSelection
            // 
            this.cmdClearSelection.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClearSelection.Location = new System.Drawing.Point(705, 31);
            this.cmdClearSelection.Name = "cmdClearSelection";
            this.cmdClearSelection.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdClearSelection.Size = new System.Drawing.Size(106, 24);
            this.cmdClearSelection.TabIndex = 5;
            this.cmdClearSelection.Text = "Clear Selection";
            this.toolTip1.SetToolTip(this.cmdClearSelection, null);
            this.cmdClearSelection.Click += new System.EventHandler(this.cmdClearSelection_Click);
            // 
            // frmAutoPay
            // 
            this.Name = "frmAutoPay";
            this.Text = "frmAutoPay";
            this.toolTip1.SetToolTip(this, null);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsAutoPay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPostPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearSelection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private fecherFoundation.FCComboBox cmbBatch;
        private fecherFoundation.FCLabel fcLabel1;
        public fecherFoundation.FCGrid vsAutoPay;
        private  fecherFoundation.FCToolStripMenuItem mnuProcess;
        private fecherFoundation.FCToolStripMenuItem mnuProcessExit;
        private fecherFoundation.FCToolStripMenuItem mnuFileForcePrenote;
        private fecherFoundation.FCToolStripMenuItem mnuFileProcPrenotes;
        private fecherFoundation.FCToolStripMenuItem mnuFileForceClear;
        public fecherFoundation.FCToolStripMenuItem mnuSpacer;
        public fecherFoundation.FCButton cmdPostPayments;
        private fecherFoundation.FCButton cmdPrintPreview;
        private fecherFoundation.FCButton cmdClearSelection;
        private fecherFoundation.FCButton cmdSelectAll;
        private Wisej.Web.ToolTip toolTip1;
    }
}