﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class sarBookPageRE : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/27/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/27/2005              *
		// ********************************************************
		int lngAcct;
		clsDRWrapper rsData = new clsDRWrapper();

		public sarBookPageRE()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarBookPageRE_ReportEnd;
		}

        private void SarBookPageRE_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        public static sarBookPageRE InstancePtr
		{
			get
			{
				return (sarBookPageRE)Sys.GetInstance(typeof(sarBookPageRE));
			}
		}

		protected sarBookPageRE _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			//FC:FINAL:SBE - #i341 - in original ColumnCount is not considered, and data is displayed only in one column.
			//this.Detail.ColumnCount = 2;
			lngAcct = FCConvert.ToInt32(this.UserData);
			strSQL = "SELECT * FROM BookPage WHERE Account = " + FCConvert.ToString(lngAcct);
			rsData.OpenRecordset(strSQL, modExtraModules.strREDatabase);
			if (rsData.EndOfFile())
			{
				fldBookPage.Text = "No Matches";
				lblHeader.Visible = false;
				GroupHeader1.Height = 0;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!rsData.EndOfFile())
				{
					// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
					fldBookPage.Text = "B" + FCConvert.ToString(rsData.Get_Fields("Book")) + " P" + FCConvert.ToString(rsData.Get_Fields("Page"));
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Book Page Report - RE");
			}
		}

		private void sarBookPageRE_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarBookPageRE.Caption	= "Account Detail";
			//sarBookPageRE.Icon	= "sarBookPageRE.dsx":0000";
			//sarBookPageRE.Left	= 0;
			//sarBookPageRE.Top	= 0;
			//sarBookPageRE.Width	= 11880;
			//sarBookPageRE.Height	= 8595;
			//sarBookPageRE.StartUpPosition	= 3;
			//sarBookPageRE.SectionData	= "sarBookPageRE.dsx":058A;
			//End Unmaped Properties
		}
	}
}
