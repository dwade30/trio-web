﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmGetInterestedParties.
	/// </summary>
	partial class frmGetInterestedParties : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAccount;
		public fecherFoundation.FCComboBox cmbSearchBy;
		public fecherFoundation.FCComboBox cmbContain;
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCLabel lblAccountNumber;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileReports;
		public fecherFoundation.FCToolStripMenuItem mnuFileIntPartyListing;
		public fecherFoundation.FCToolStripMenuItem mnuFileReportsREwithIP;
		public fecherFoundation.FCToolStripMenuItem mnuFileReportsUTwithIP;
		public fecherFoundation.FCToolStripMenuItem mnuFileLabels;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetInterestedParties));
			this.cmbAccount = new fecherFoundation.FCComboBox();
			this.cmbSearchBy = new fecherFoundation.FCComboBox();
			this.cmbContain = new fecherFoundation.FCComboBox();
			this.txtAccount = new fecherFoundation.FCTextBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.lblSearchBy = new fecherFoundation.FCLabel();
			this.lblContain = new fecherFoundation.FCLabel();
			this.lblAccountNumber = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileReports = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileIntPartyListing = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileReportsREwithIP = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileReportsUTwithIP = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileLabels = new fecherFoundation.FCToolStripMenuItem();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.btnProcess = new fecherFoundation.FCButton();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuGetAccount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 435);
			this.BottomPanel.Size = new System.Drawing.Size(509, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblAccount);
			this.ClientArea.Controls.Add(this.cmbAccount);
			this.ClientArea.Controls.Add(this.txtAccount);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.lblAccountNumber);
			this.ClientArea.Size = new System.Drawing.Size(509, 375);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(509, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(359, 35);
			this.HeaderText.Text = "Interested Parties Information";
			// 
			// cmbAccount
			// 
			this.cmbAccount.AutoSize = false;
			this.cmbAccount.FormattingEnabled = true;
			this.cmbAccount.Items.AddRange(new object[] {
				"Interested Party Number",
				"Real Estate Account",
				"Utility Account"
			});
			this.cmbAccount.Location = new System.Drawing.Point(203, 25);
			this.cmbAccount.Name = "cmbAccount";
			this.cmbAccount.Size = new System.Drawing.Size(220, 40);
			this.cmbAccount.TabIndex = 1;
			this.cmbAccount.Text = "Interested Party Number";
			this.cmbAccount.SelectedIndexChanged += new System.EventHandler(this.optAccount_Click);
			// 
			// cmbSearchBy
			// 
			this.cmbSearchBy.AutoSize = false;
			this.cmbSearchBy.FormattingEnabled = true;
			this.cmbSearchBy.Items.AddRange(new object[] {
				"Name"
			});
			this.cmbSearchBy.Location = new System.Drawing.Point(196, 25);
			this.cmbSearchBy.Name = "cmbSearchBy";
			this.cmbSearchBy.Size = new System.Drawing.Size(207, 40);
			this.cmbSearchBy.TabIndex = 1;
			this.cmbSearchBy.Text = "Name";
			// 
			// cmbContain
			// 
			this.cmbContain.AutoSize = false;
			this.cmbContain.FormattingEnabled = true;
			this.cmbContain.Items.AddRange(new object[] {
				"Contain Search",
				"Start With Search",
				"Ends With Search"
			});
			this.cmbContain.Location = new System.Drawing.Point(196, 80);
			this.cmbContain.Name = "cmbContain";
			this.cmbContain.Size = new System.Drawing.Size(207, 40);
			this.cmbContain.TabIndex = 3;
			this.cmbContain.Text = "Start With Search";
			// 
			// txtAccount
			// 
			this.txtAccount.AutoSize = false;
			this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
			this.txtAccount.Location = new System.Drawing.Point(203, 112);
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Size = new System.Drawing.Size(220, 40);
			this.txtAccount.TabIndex = 3;
			this.txtAccount.Enter += new System.EventHandler(this.txtAccount_Enter);
			// 
			// Frame2
			// 
			this.Frame2.Controls.Add(this.txtSearch);
			this.Frame2.Controls.Add(this.fcLabel1);
			this.Frame2.Controls.Add(this.lblSearchBy);
			this.Frame2.Controls.Add(this.lblContain);
			this.Frame2.Controls.Add(this.cmbContain);
			this.Frame2.Controls.Add(this.cmbSearchBy);
			this.Frame2.Location = new System.Drawing.Point(30, 166);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(433, 203);
			this.Frame2.TabIndex = 4;
			this.Frame2.Text = "SEARCH BY";
			this.Frame2.UseMnemonic = false;
			this.Frame2.Visible = false;
			// 
			// txtSearch
			// 
			this.txtSearch.AutoSize = false;
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch.Location = new System.Drawing.Point(196, 136);
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(207, 40);
			this.txtSearch.TabIndex = 5;
			// 
			// fcLabel1
			// 
			this.fcLabel1.AutoSize = true;
			this.fcLabel1.Location = new System.Drawing.Point(20, 150);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(174, 17);
			this.fcLabel1.TabIndex = 4;
			this.fcLabel1.Text = "CRITERIA TO SEARCH BY";
			this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblSearchBy
			// 
			this.lblSearchBy.AutoSize = true;
			this.lblSearchBy.Location = new System.Drawing.Point(20, 40);
			this.lblSearchBy.Name = "lblSearchBy";
			this.lblSearchBy.Size = new System.Drawing.Size(141, 17);
			this.lblSearchBy.TabIndex = 0;
			this.lblSearchBy.Text = "INTERESTED PARTY";
			this.lblSearchBy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblContain
			// 
			this.lblContain.AutoSize = true;
			this.lblContain.Location = new System.Drawing.Point(20, 94);
			this.lblContain.Name = "lblContain";
			this.lblContain.Size = new System.Drawing.Size(176, 17);
			this.lblContain.TabIndex = 2;
			this.lblContain.Text = "FIND ALL RECORDS THAT";
			this.lblContain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblAccountNumber
			// 
			this.lblAccountNumber.AutoSize = true;
			this.lblAccountNumber.Location = new System.Drawing.Point(30, 83);
			this.lblAccountNumber.Name = "lblAccountNumber";
			this.lblAccountNumber.Size = new System.Drawing.Size(456, 17);
			this.lblAccountNumber.TabIndex = 2;
			this.lblAccountNumber.Text = "ENTER THE INTERESTED PARTY AND PRESS \'ENTER\' TO CONTINUE";
			this.lblAccountNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileReports
			});
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.Text = "File";
			// 
			// mnuFileReports
			// 
			this.mnuFileReports.Index = 0;
			this.mnuFileReports.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileIntPartyListing,
				this.mnuFileReportsREwithIP,
				this.mnuFileReportsUTwithIP,
				this.mnuFileLabels
			});
			this.mnuFileReports.Text = "Reports & Lists";
			// 
			// mnuFileIntPartyListing
			// 
			this.mnuFileIntPartyListing.Index = 0;
			this.mnuFileIntPartyListing.Text = "Interested Party Master Listing";
			this.mnuFileIntPartyListing.Click += new System.EventHandler(this.mnuFileIntPartyListing_Click);
			// 
			// mnuFileReportsREwithIP
			// 
			this.mnuFileReportsREwithIP.Index = 1;
			this.mnuFileReportsREwithIP.Text = "RE Accounts w/Interested Parties";
			this.mnuFileReportsREwithIP.Click += new System.EventHandler(this.mnuFileReportsREwithIP_Click);
			// 
			// mnuFileReportsUTwithIP
			// 
			this.mnuFileReportsUTwithIP.Enabled = false;
			this.mnuFileReportsUTwithIP.Index = 2;
			this.mnuFileReportsUTwithIP.Text = "UT Accounts w/Interested Parties";
			this.mnuFileReportsUTwithIP.Click += new System.EventHandler(this.mnuFileReportsUTwithIP_Click);
			// 
			// mnuFileLabels
			// 
			this.mnuFileLabels.Index = 3;
			this.mnuFileLabels.Text = "Interested Party Labels";
			this.mnuFileLabels.Click += new System.EventHandler(this.mnuFileLabels_Click);
			// 
			// lblAccount
			// 
			this.lblAccount.AutoSize = true;
			this.lblAccount.Location = new System.Drawing.Point(30, 40);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(60, 17);
			this.lblAccount.TabIndex = 0;
			this.lblAccount.Text = "SELECT ";
			this.lblAccount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lblAccount.Click += new System.EventHandler(this.lblAccount_Click);
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(194, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(120, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Process";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// mnuExit
			// 
			this.mnuExit.Index = -1;
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// mnuSeperator
			// 
			this.mnuSeperator.Index = -1;
			this.mnuSeperator.Text = "-";
			// 
			// mnuGetAccount
			// 
			this.mnuGetAccount.Index = -1;
			this.mnuGetAccount.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuGetAccount.Text = "Process";
			this.mnuGetAccount.Click += new System.EventHandler(this.mnuGetAccount_Click);
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = -1;
			this.mnuFileSeperator2.Text = "-";
			// 
			// frmGetInterestedParties
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(509, 531);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmGetInterestedParties";
			this.ShowInTaskbar = false;
			this.Text = "Interested Parties Information";
			this.Load += new System.EventHandler(this.frmGetInterestedParties_Load);
			this.Activated += new System.EventHandler(this.frmGetInterestedParties_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetInterestedParties_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetInterestedParties_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCLabel lblContain;
		private FCLabel lblAccount;
		private FCLabel lblSearchBy;
		private FCLabel fcLabel1;
		private FCButton btnProcess;
		public FCToolStripMenuItem mnuExit;
		public FCToolStripMenuItem mnuSeperator;
		public FCToolStripMenuItem mnuGetAccount;
		public FCToolStripMenuItem mnuFileSeperator2;
	}
}
