﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptREMortgage : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Corey Grey              *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/15/2004              *
		// ********************************************************
		clsDRWrapper clsre = new clsDRWrapper();
		int lngLastAccount;
		double dblTotalSum;
		bool boolCL = false;
		bool boolBL = false;

		public rptREMortgage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Mortgage Holder List";
		}

		public static rptREMortgage InstancePtr
		{
			get
			{
				return (rptREMortgage)Sys.GetInstance(typeof(rptREMortgage));
			}
		}

		protected rptREMortgage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsre.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsre.EndOfFile();
			//Detail_Format();
		}

		public void Init(bool boolNameOrder)
		{
			string strSQL;
			string strOrder = "";
			string strDBName;
			string strPartyDBName;
			string strJoin;
			boolCL = modGlobalConstants.Statics.gboolCL;
			boolBL = modGlobalConstants.Statics.gboolBL;
			if (boolNameOrder)
			{
				strOrder = " ORDER BY rsname, Mparty.RSAccount ";
			}
			else
			{
				strOrder = " ORDER BY Mparty.RSAccount ";
			}
			lblTotalDue.Text = "Total Due as of " + Strings.Format(DateTime.Today, "MM/dd/yy");
			strDBName = clsre.Get_GetFullDBName("CentralData") + ".dbo.";
			strPartyDBName = clsre.Get_GetFullDBName("CentralParties") + ".dbo.";
			strJoin = GetMasterJoinForJoin();
			// Call clsre.OpenRecordset("select * from master inner join (" & strDBName & "mortgageassociation inner join " & strDBName & "mortgageholders on (mortgageassociation.mortgageholderid = mortgageholders.id)) on (master.rsaccount = mortgageassociation.account) where mortgageassociation.module = 'RE' and not (master.rsdeleted = 1) and master.rscard = 1" & strOrder, "twre0000.vb1")
			// strSQL = "select *,mortgageholders.id as mortid from master cross apply " & strPartyDBName & "getcentralpartyname(master.ownerpartyid) as p inner join (" & strDBName & "mortgageassociation inner join " & strDBName & "MortgageHolders on (mortgageassociation.mortgageholderid = mortgageholders.id)) on (master.rsaccount = mortgageassociation.account) where mortgageassociation.module = 'RE' and not (master.rsdeleted = 1) and master.rscard = 1" & strOrder
			strSQL = "select *,mortgageholders.id as mortid from " + strJoin + " inner join (" + strDBName + "mortgageassociation inner join " + strDBName + "mortgageholders on (mortgageassociation.mortgageholderid = mortgageholders.id)) on (mparty.rsaccount = mortgageassociation.account) where mortgageassociation.module = 'RE' and not (mparty.rsdeleted = 1) and mparty.rscard = 1" + strOrder;
			// Call clsre.OpenRecordset("select * from master inner join (" & strDBName & "mortgageassociation inner join " & strDBName & "mortgageholders on (mortgageassociation.mortgageholderid = mortgageholders.id)) on (master.rsaccount = mortgageassociation.account) where mortgageassociation.module = 'RE' and not (master.rsdeleted = 1) and master.rscard = 1" & strOrder, "twre0000.vb1")
			clsre.OpenRecordset(strSQL, "RealEstate");
			// Me.Show , MDIParent
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			lngLastAccount = 0;
			txtDate.Text = DateTime.Today.ToString();
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				double dblDue = 0;
				bool boolDup = false;
				if (!clsre.EndOfFile())
				{
					if (FCConvert.ToInt32(clsre.Get_Fields_Int32("rsaccount")) != lngLastAccount)
					{
						txtaccount.Text = FCConvert.ToString(clsre.Get_Fields_Int32("rsaccount"));
						//FC:FINAL:AM:#i56 - use correct field
						txtName.Text = FCConvert.ToString(clsre.Get_Fields_String("fullnamelf"));
						//txtName.Text = FCConvert.ToString(clsre.Get_Fields("RSName"));
						lngLastAccount = FCConvert.ToInt32(clsre.Get_Fields_Int32("rsaccount"));
						boolDup = false;
					}
					else
					{
						txtaccount.Text = "";
						txtName.Text = "";
						boolDup = true;
					}
					if (FCConvert.ToBoolean(clsre.Get_Fields_Boolean("escrowed")))
					{
						txtEscrow.Text = "Yes";
					}
					else
					{
						txtEscrow.Text = "No";
					}
					if (FCConvert.ToBoolean(clsre.Get_Fields_Boolean("receivebill")))
					{
						txtBill.Text = "Yes";
					}
					else
					{
						txtBill.Text = "No";
					}
					// TODO: Field [mortid] not found!! (maybe it is an alias?)
					txtHolderNumber.Text = FCConvert.ToString(clsre.Get_Fields("mortid"));
					txtHolderName.Text = FCConvert.ToString(clsre.Get_Fields_String("name"));
					if (!boolDup)
					{
						if (boolCL || boolBL)
						{
							dblDue = modCLCalculations.CalculateAccountTotal(lngLastAccount, true);
							fldTotalDue.Text = Strings.Format(dblDue, "$#,##0.00");
						}
						else
						{
							fldTotalDue.Text = "0.00";
						}
					}
					else
					{
						fldTotalDue.Text = "";
					}
					dblTotalSum += dblDue;
					clsre.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Detail Format");
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotalTotalDue.Text = Strings.Format(dblTotalSum, "$#,##0.00");
		}

		private string GetMasterJoinForJoin()
		{
			string GetMasterJoinForJoin = "";
			string strReturn;
			strReturn = "select master.*, cpo.FullNameLF as RSName,cpso.FullNameLF as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4, cpo.email as email from ";
			clsDRWrapper tLoad = new clsDRWrapper();
			string strTemp;
			strTemp = tLoad.Get_GetFullDBName("RealEstate");
			strReturn += strTemp + ".dbo.master left join ";
			strTemp = tLoad.Get_GetFullDBName("CentralParties");
			strTemp += ".dbo.PartyAndAddressView ";
			strReturn += strTemp + " as cpo on (master.ownerpartyid = cpo.ID) left join ";
			strReturn += strTemp + " as cpso on (master.SecOwnerPartyID = cpso.ID)";
			strReturn = " (" + strReturn + ") mparty ";
			GetMasterJoinForJoin = strReturn;
			tLoad.Dispose();
			return GetMasterJoinForJoin;
		}

		private void rptREMortgage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptREMortgage.Text	= "Mortgage Holder List";
			//rptREMortgage.Icon	= "rptREMortgage.dsx":0000";
			//rptREMortgage.Left	= 0;
			//rptREMortgage.Top	= 0;
			//rptREMortgage.Width	= 12960;
			//rptREMortgage.Height	= 7980;
			//rptREMortgage.StartUpPosition	= 3;
			//rptREMortgage.SectionData	= "rptREMortgage.dsx":058A;
			//End Unmaped Properties
		}
	}
}
