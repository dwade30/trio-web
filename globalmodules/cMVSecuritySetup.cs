﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cMVSecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cMVSecuritySetup() : base()
		{
			strThisModule = "MV";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem("Enter Motor Vehicle", "", 1, false, ""));
			AddItem_2(CreateItem("Exception Report Items", "", 12, false, ""));
			AddItem_2(CreateItem("File Maintenance", "", 35, true, ""));
			AddItem_2(CreateItem("File Maintenance", "Database Cleanup", 36, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Extract", 38, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Motor Vehicle Settings", 37, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Purge", 39, false, ""));
			AddItem_2(CreateItem("Fleet Master", "", 29, true, ""));
			AddItem_2(CreateItem("Fleet Master", "Add Fleet", 30, false, ""));
			AddItem_2(CreateItem("Fleet Master", "Add Group", 31, false, ""));
			AddItem_2(CreateItem("Fleet Master", "Delete Fleet / Group", 32, false, ""));
			AddItem_2(CreateItem("Inventory Maintenance", "", 8, true, ""));
			AddItem_2(CreateItem("Inventory Maintenance", "Add Inventory", 9, false, ""));
			AddItem_2(CreateItem("Inventory Maintenance", "Change Inventory Groups", 11, false, ""));
			AddItem_2(CreateItem("Inventory Maintenance", "Remove Inventory", 10, false, ""));
			AddItem_2(CreateItem("Inventory Status", "", 40, false, ""));
			AddItem_2(CreateItem("Process End of Period", "", 3, true, ""));
			AddItem_2(CreateItem("Process End of Period", "Close Period", 5, false, ""));
			AddItem_2(CreateItem("Process End of Period", "Run Interim Reports", 4, false, ""));
			AddItem_2(CreateItem("Process End of Period", "Run Verification Report", 6, false, ""));
			AddItem_2(CreateItem("Rapid Renewal", "", 41, true, ""));
			AddItem_2(CreateItem("Rapid Renewal", "Auto Post Journal", 49, false, ""));
			AddItem_2(CreateItem("Rapid Renewal", "Financial & Detail Processing", 43, false, ""));
			AddItem_2(CreateItem("Rapid Renewal", "Monthly Load", 42, false, ""));
			AddItem_2(CreateItem("Blue Book", "Create Opening Adjustments", 34, false, ""));
			AddItem_2(CreateItem("Reporting", "", 13, true, ""));
			AddItem_2(CreateItem("Reporting", "Fleet Registration List", 15, false, ""));
			AddItem_2(CreateItem("Reporting", "Reminders", 16, false, ""));
			AddItem_2(CreateItem("Reporting", "Teller Reports", 17, false, ""));
			AddItem_2(CreateItem("Reporting", "VIN List", 14, false, ""));
			AddItem_2(CreateItem("Reporting", "Audit Report", 48, false, ""));
			AddItem_2(CreateItem("Reporting", "Audit Archive Report", 47, false, ""));
			AddItem_2(CreateItem("Run BMV Update Disks", "", 7, false, ""));
			AddItem_2(CreateItem("Table Option Processing", "", 18, true, ""));
			AddItem_2(CreateItem("Table Option Processing", "Class Codes", 26, false, ""));
			AddItem_2(CreateItem("Table Option Processing", "Color Codes", 27, false, ""));
			AddItem_2(CreateItem("Table Option Processing", "County Codes", 21, false, ""));
			AddItem_2(CreateItem("Table Option Processing", "Default Values", 28, false, ""));
			AddItem_2(CreateItem("Table Option Processing", "Fees", 24, false, ""));
			AddItem_2(CreateItem("Table Option Processing", "Make Codes", 23, false, ""));
			AddItem_2(CreateItem("Table Option Processing", "Operator ID Codes", 25, false, ""));
			AddItem_2(CreateItem("Table Option Processing", "Reason Codes & Print Priorities", 22, false, ""));
			AddItem_2(CreateItem("Table Option Processing", "Residense & State Codes", 20, false, ""));
			AddItem_2(CreateItem("Table Option Processing", "Style Codes", 19, false, ""));
			AddItem_2(CreateItem("Teller Closeout", "", 45, false, ""));
			AddItem_2(CreateItem("Vehicle Update", "", 44, false, ""));
		}
	}
}
