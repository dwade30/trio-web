﻿namespace Global
{
	/// <summary>
	/// Summary description for arARDailyAudit.
	/// </summary>
	partial class arARDailyAudit
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arARDailyAudit));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPreview = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.sarDADetailOb = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarDACashOb = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarDANonCashOb = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.lblOutput = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.sarDATypeSummaryOB = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOutput)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.sarDADetailOb,
				this.sarDACashOb,
				this.sarDANonCashOb,
				this.lblOutput,
				this.sarDATypeSummaryOB
			});
			this.Detail.Height = 0.9375F;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.CanGrow = false;
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblPage,
				this.lblDate,
				this.lblTime,
				this.lblMuniName,
				this.lblPreview
			});
			this.GroupHeader1.Height = 0.5F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.3125F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "Daily Audit Report";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.46875F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.46875F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblPage.Text = "Page";
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.46875F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblDate.Text = "Date";
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'";
			this.lblTime.Text = "Time";
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.125F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'";
			this.lblMuniName.Text = "MuniName";
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.5625F;
			// 
			// lblPreview
			// 
			this.lblPreview.Height = 0.1875F;
			this.lblPreview.HyperLink = null;
			this.lblPreview.Left = 0F;
			this.lblPreview.Name = "lblPreview";
			this.lblPreview.Style = "font-family: \'Tahoma\'; text-align: center";
			this.lblPreview.Text = "PREVIEW";
			this.lblPreview.Top = 0.3125F;
			this.lblPreview.Visible = false;
			this.lblPreview.Width = 7.46875F;
			// 
			// sarDADetailOb
			// 
			this.sarDADetailOb.CloseBorder = false;
			this.sarDADetailOb.Height = 0.125F;
			this.sarDADetailOb.Left = 0F;
			this.sarDADetailOb.Name = "sarDADetailOb";
			this.sarDADetailOb.Report = null;
			this.sarDADetailOb.Top = 0.0625F;
			this.sarDADetailOb.Width = 6.9375F;
			// 
			// sarDACashOb
			// 
			this.sarDACashOb.CloseBorder = false;
			this.sarDACashOb.Height = 0.125F;
			this.sarDACashOb.Left = 0F;
			this.sarDACashOb.Name = "sarDACashOb";
			this.sarDACashOb.Report = null;
			this.sarDACashOb.Top = 0.25F;
			this.sarDACashOb.Width = 6.9375F;
			// 
			// sarDANonCashOb
			// 
			this.sarDANonCashOb.CloseBorder = false;
			this.sarDANonCashOb.Height = 0.125F;
			this.sarDANonCashOb.Left = 0F;
			this.sarDANonCashOb.Name = "sarDANonCashOb";
			this.sarDANonCashOb.Report = null;
			this.sarDANonCashOb.Top = 0.4375F;
			this.sarDANonCashOb.Width = 6.9375F;
			// 
			// lblOutput
			// 
			this.lblOutput.Height = 0.1875F;
			this.lblOutput.HyperLink = null;
			this.lblOutput.Left = 0F;
			this.lblOutput.Name = "lblOutput";
			this.lblOutput.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblOutput.Text = null;
			this.lblOutput.Top = 0.75F;
			this.lblOutput.Width = 7F;
			// 
			// sarDATypeSummaryOB
			// 
			this.sarDATypeSummaryOB.CloseBorder = false;
			this.sarDATypeSummaryOB.Height = 0.125F;
			this.sarDATypeSummaryOB.Left = 0F;
			this.sarDATypeSummaryOB.Name = "sarDATypeSummaryOB";
			this.sarDATypeSummaryOB.Report = null;
			this.sarDATypeSummaryOB.Top = 0.625F;
			this.sarDATypeSummaryOB.Width = 6.9375F;
			// 
			// arARDailyAudit
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.46875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOutput)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarDADetailOb;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarDACashOb;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarDANonCashOb;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOutput;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarDATypeSummaryOB;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPreview;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
