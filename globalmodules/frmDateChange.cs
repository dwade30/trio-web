﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;

#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmDateChange.
	/// </summary>
	public partial class frmDateChange : BaseForm
	{
		public frmDateChange()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmDateChange InstancePtr
		{
			get
			{
				return (frmDateChange)Sys.GetInstance(typeof(frmDateChange));
			}
		}

		protected frmDateChange _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/26/2006              *
		// ********************************************************
		DateTime dtNew;
		int intFormType;
		bool boolEffectChecked;

		public void Init(string strText, short intType, DateTime dtEffectiveDate)
		{
			// strText is the actual sting that will be shown above the date box
			// intType will tell me which place in my code called this form
			string strTemp = "";
			if (intType == 1)
			{
				chkEffect.Visible = true;
			}
			// get the default
			// GetKeyValues HKEY_CURRENT_USER, REGISTRYKEY & "CL\", "CLEffectRTD", strtemp
			// If strtemp <> "FALSE" Then
			// chkEffect.Value = vbChecked
			// Else
			// chkEffect.Value = vbUnchecked
			// End If
			lblInstructions.Text = strText;
			txtDate.Text = Strings.Format(dtEffectiveDate, "MM/dd/yyyy");
			intFormType = intType;
			this.Show(FormShowEnum.Modal, App.MainForm);
			// If boolEffectChecked Then
			// SaveKeyValue HKEY_CURRENT_USER, REGISTRYKEY & "CL\", "CLEffectRTD", "TRUE"
			// Else
			// SaveKeyValue HKEY_CURRENT_USER, REGISTRYKEY & "CL\", "CLEffectRTD", "FALSE"
			// End If
		}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intAns As short, int --> As DialogResult
			DialogResult intAns;
			DateTime datLowDate;
			DateTime datHighDate;
			switch (intFormType)
			{
				case 1:
					{
						// Effective Interest Date from frmRECLStatus
						if (Information.IsDate(txtDate.Text))
						{
							// MAL@20080304: Correct date functions
							// Tracker Reference: 12581
							// datLowDate = Month(Date) & "/" & Day(Date) & "/" & Year(Date) - 1
							// datHighDate = Month(Date) & "/" & Day(Date) & "/" & Year(Date) + 1
							datLowDate = DateAndTime.DateAdd("yyyy", -1, DateTime.Today);
							datHighDate = DateAndTime.DateAdd("yyyy", 1, DateTime.Today);
							if (DateAndTime.DateValue(txtDate.Text).ToOADate() < datLowDate.ToOADate() || DateAndTime.DateValue(txtDate.Text).ToOADate() > datHighDate.ToOADate())
							{
								intAns = MessageBox.Show("The date you are entering is different from today's date by more than a year.  Are you sure you wish to use this date?", "Change Date?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
								if (intAns == DialogResult.No)
								{
									goto UnloadTag;
								}
							}
							dtNew = DateAndTime.DateValue(txtDate.Text);
							boolEffectChecked = FCConvert.CBool(chkEffect.CheckState == Wisej.Web.CheckState.Checked);
							this.Hide();
							frmRECLStatus.InstancePtr.GetNewEffectiveInterestDate(ref dtNew, boolEffectChecked);
						}
						UnloadTag:
						;
						Close();
						break;
					}
			}
			//end switch
		}

		public void cmdOk_Click()
		{
			cmdOk_Click(cmdOk, new System.EventArgs());
		}

		private void cmdQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void frmDateChange_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
				return;
		}

		private void frmDateChange_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						Support.SendKeys("{tab}", false);
						break;
					}
			}
			//end switch
		}

		private void frmDateChange_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmDateChange_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDateChange.ScaleWidth	= 3015;
			//frmDateChange.ScaleHeight	= 2460;
			//frmDateChange.LinkTopic	= "Form1";
			//frmDateChange.LockControls	= -1  'True;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			if (modCLCalculations.Statics.gboolDefaultRTDToAutoChange)
			{
				chkEffect.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkEffect.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void txtDate_KeyDownEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Return:
					{
						// if the user hits the enter ID on the textbox when they are done editing the date
						cmdOk_Click();
						break;
					}
			}
			//end switch
		}

		private void txtDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(Strings.Left(txtDate.Text, 2)) > 12)
			{
				e.Cancel = true;
				MessageBox.Show("Please use the date format MM/dd/yyyy.", "Invalid Date Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
	}
}
