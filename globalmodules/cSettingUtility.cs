﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cSettingUtility
	{
		//=========================================================
		private cSettingsController setCont = new cSettingsController();

		public string GetSettingValue(string strSettingName, string strSettingType, string strOwnerType, string strOwner, string strDB, string strRegistryName, string strRegistrySubPath, string strDefaultValue)
		{
			string GetSettingValue = "";
			cSetting setVal;
			string strTemp = "";
			setVal = setCont.GetSetting(strSettingName, strSettingType, strOwnerType, strOwner, strDB);
			if (!(setVal == null))
			{
				GetSettingValue = setVal.SettingValue;
			}
			else if (strRegistryName != "")
			{
				strTemp = modRegistry.GetRegistryKey(strRegistryName, strRegistryName, strDefaultValue);
				setCont.SaveSetting(strTemp, strSettingName, strSettingType, strOwnerType, strOwner, strDB);
				GetSettingValue = strTemp;
			}
			else
			{
				setCont.SaveSetting(strDefaultValue, strSettingName, strSettingType, strOwnerType, strOwner, strDB);
				GetSettingValue = strDefaultValue;
			}
			return GetSettingValue;
		}
	}
}
