//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;
using fecherFoundation;

namespace Global
{
    /// <summary>
    /// Summary description for sarUTBookPageLN.
    /// </summary>
    public partial class sarUTBookPageLN : FCSectionReport
    {

        public sarUTBookPageLN()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            if (_InstancePtr == null)
                _InstancePtr = this;
            this.Name = "sarUTBookPageAccount";
        }

        public static sarUTBookPageLN InstancePtr
        {
            get
            {
                return (sarUTBookPageLN)Sys.GetInstance(typeof(sarUTBookPageLN));
            }
        }

        protected sarUTBookPageLN _InstancePtr = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                rsData.Dispose();
            }
            base.Dispose(disposing);
        }
        // nObj = 1
        //   0	sarUTBookPageLN	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               12/04/2006              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               12/04/2006              *
        // ********************************************************
        int lngAcct;
        clsDRWrapper rsData = new clsDRWrapper();
        string strWS;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
			eArgs.EOF = rsData.EndOfFile();
        }

        private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
        {
            switch (KeyCode)
            {

                case Keys.Escape:
                    {
                        this.Close();
                        break;
                    }
            } //end switch
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            string strSQL;
            string strQryFlds;

            //modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
            strWS = sarUTBookPageAccount.InstancePtr.strWS;

            this.Detail.ColumnCount = 2;
            lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
            strQryFlds = "Lien.ID AS LienKey, Lien.Book AS [Lien.Book], Page, DateCreated, BillNumber";

            strSQL = "SELECT " + strQryFlds + " FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID WHERE Lien.Book <> 0 AND ActualAccountNumber = " + FCConvert.ToString(lngAcct);
            rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
            if (rsData.EndOfFile())
            {
                fldBookPage.Text = ""; // "No Matches"
                lblHeader.Visible = false;
                GroupHeader1.Height = 0;
            }
        }

		private void Detail_Format(object sender, EventArgs e)
        {
            BindFields();
        }

        private void BindFields()
        {
            try
            {   // On Error GoTo ERROR_HANDLER
                string strBook = "";
                string strPage = "";
                int intLienKey = 0;

                if (!rsData.EndOfFile())
                {
                    // TODO: Field [Lien.Book] not found!! (maybe it is an alias?)
                    if (FCConvert.ToString(rsData.Get_Fields("Lien.Book")) != "")
                    {
                        // TODO: Field [Lien.Book] not found!! (maybe it is an alias?)
                        strBook = "B" + rsData.Get_Fields("Lien.Book");
                    }
                    else
                    {
                        strBook = "B        ";
                    }
                    // TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
                    if (FCConvert.ToString(rsData.Get_Fields("Page")) != "")
                    {
                        // TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
                        strPage = "P" + rsData.Get_Fields("Page");
                    }
                    else
                    {
                        strPage = "P        ";
                    }
                    fldBookPage.Text = strBook + " " + strPage;
                    fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy");
                    fldYear.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BillNumber"));
                    intLienKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("LienKey"));
                    rsData.MoveNext();
                    CheckNext:;
                    if (!rsData.EndOfFile())
                    {
                        if (FCConvert.ToInt32(rsData.Get_Fields_Int32("LienKey")) == intLienKey)
                        {
                            fldYear.Text = fldYear.Text + ", " + FCConvert.ToInt32(rsData.Get_Fields_Int32("BillNumber"));
                            rsData.MoveNext();
                            goto CheckNext;
                        }
                    }
                }
                return;
            }
            catch
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", "Error In Book Page Report - LN", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void sarUTBookPageLN_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //sarUTBookPageLN properties;
            //sarUTBookPageLN.Caption	= "Account Detail";
            //sarUTBookPageLN.Icon	= "sarUTBookPageLN.dsx":0000";
            //sarUTBookPageLN.Left	= 0;
            //sarUTBookPageLN.Top	= 0;
            //sarUTBookPageLN.Width	= 11880;
            //sarUTBookPageLN.Height	= 8595;
            //sarUTBookPageLN.StartUpPosition	= 3;
            //sarUTBookPageLN.SectionData	= "sarUTBookPageLN.dsx":058A;
            //End Unmaped Properties
        }
    }
}
