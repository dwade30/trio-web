﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Microsoft.VisualBasic;

namespace Global
{
	public class cBankService
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBankController bankCont = new cBankController();
		//private cSettingsController setCont = new cSettingsController();
		private cBankController bankCont_AutoInitialized;

		private cBankController bankCont
		{
			get
			{
				if (bankCont_AutoInitialized == null)
				{
					bankCont_AutoInitialized = new cBankController();
				}
				return bankCont_AutoInitialized;
			}
			set
			{
				bankCont_AutoInitialized = value;
			}
		}

		private cSettingsController setCont_AutoInitialized;

		private cSettingsController setCont
		{
			get
			{
				if (setCont_AutoInitialized == null)
				{
					setCont_AutoInitialized = new cSettingsController();
				}
				return setCont_AutoInitialized;
			}
			set
			{
				setCont_AutoInitialized = value;
			}
		}

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public int GetLastCheckNumberForBankCheckType(int lngBankID, string strCheckType)
		{
			int GetLastCheckNumberForBankCheckType = 0;
			ClearErrors();
			int lngCheckNumber;
			lngCheckNumber = 0;
			cBankCheckHistory bcHistory;
			bcHistory = bankCont.GetBankCheckHistoryByBankCheckType(lngBankID, strCheckType);
			if (bankCont.LastErrorNumber == 0)
			{
				if (!(bcHistory == null))
				{
					lngCheckNumber = bcHistory.LastCheckNumber;
				}
			}
			GetLastCheckNumberForBankCheckType = lngCheckNumber;
			return GetLastCheckNumberForBankCheckType;
			ErrorHandler:
			;
			lngLastError = Information.Err().Number;
			strLastError = Information.Err().Description;
			return GetLastCheckNumberForBankCheckType;
		}

		public int GetLastCheckNumberForBankForCorrectCheckType(int lngBankID, string strCheckType)
		{
			int GetLastCheckNumberForBankForCorrectCheckType = 0;
			string strTemp;
			string strType;
			strType = "";
			strTemp = setCont.GetSettingValue("UseBanksLastCheckNumber", "", "", "", "");
			if (Strings.LCase(strTemp) == "bank")
			{
				strType = "";
			}
			else if (Strings.LCase(strTemp) == "check type")
			{
				strType = strCheckType;
			}
			else
			{
				return GetLastCheckNumberForBankForCorrectCheckType;
			}
			GetLastCheckNumberForBankForCorrectCheckType = GetLastCheckNumberForBankCheckType(lngBankID, strType);
			return GetLastCheckNumberForBankForCorrectCheckType;
		}
		// vbPorter upgrade warning: strCheckType As Variant --> As string
		public void UpdateLastCheckNumberForBankForCorrectCheckType(int lngBankID, int lngCheckNumber, string strCheckType)
		{
			string strTemp;
			string strType;
			strType = "";
			strTemp = setCont.GetSettingValue("UseBanksLastCheckNumber", "", "", "", "");
			if (Strings.LCase(strTemp) == "bank")
			{
				strType = "";
			}
			else if (Strings.LCase(strTemp) == "check type")
			{
				strType = strCheckType;
			}
			else
			{
				return;
			}
			UpdateLastCheckNumberForBankCheckType(lngBankID, lngCheckNumber, strType);
		}
		// vbPorter upgrade warning: strCheckType As Variant --> As string
		public void UpdateLastCheckNumberForBankCheckType(int lngBankID, int lngCheckNumber, string strCheckType)
		{
			ClearErrors();
			cBankCheckHistory bcHistory;
			bcHistory = bankCont.GetBankCheckHistoryByBankCheckType(lngBankID, strCheckType);
			if (bankCont.LastErrorNumber == 0)
			{
				if (bcHistory == null)
				{
					bcHistory = new cBankCheckHistory();
					bcHistory.CheckType = strCheckType;
				}
				if (bcHistory.BankID < 1)
				{
					bcHistory.BankID = lngBankID;
				}
				bcHistory.LastCheckNumber = lngCheckNumber;
				bankCont.SaveBankCheckHistory(ref bcHistory);
				if (bankCont.LastErrorNumber != 0)
				{
					lngLastError = bankCont.LastErrorNumber;
					strLastError = bankCont.LastErrorMessage;
				}
			}
			else
			{
				lngLastError = bankCont.LastErrorNumber;
				strLastError = bankCont.LastErrorMessage;
			}
			return;
			ErrorHandler:
			;
			lngLastError = Information.Err().Number;
			strLastError = Information.Err().Description;
		}

		public void SetCurrentBank(int lngID)
		{
			bankCont.SetCurrentBank(lngID);
		}

		public void SetCurrentBankByBank(ref cBank bnk)
		{
			if (!(bnk == null))
			{
				bankCont.SetCurrentBank(bnk.ID);
			}
		}

		public cBank GetCurrentBank()
		{
			cBank GetCurrentBank = null;
			GetCurrentBank = bankCont.GetCurrentBank();
			return GetCurrentBank;
		}

		public int GetBankIDFromBankNumber(int lngBankNumber)
		{
			int GetBankIDFromBankNumber = 0;
			cBank bnk;
			int lngID;
			lngID = 0;
			bnk = bankCont.GetBankByNumber(lngBankNumber);
			if (!(bnk == null))
			{
				lngID = bnk.ID;
			}
			GetBankIDFromBankNumber = lngID;
			return GetBankIDFromBankNumber;
		}

		public cBank GetBankByNumber(int lngBankNumber)
		{
			cBank GetBankByNumber = null;
			GetBankByNumber = bankCont.GetBankByNumber(lngBankNumber);
			return GetBankByNumber;
		}

		public cGenericCollection GetBanks()
		{
			cGenericCollection GetBanks = null;
			GetBanks = bankCont.GetBanks();
			return GetBanks;
		}
	}
}
