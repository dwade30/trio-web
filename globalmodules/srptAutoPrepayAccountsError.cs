using System;
using Wisej.Web;
using fecherFoundation;
using modUTStatusPayments = Global.modUTFunctions;

namespace Global
{
    /// <summary>
    /// Summary description for srptAutoPrepayAccountsError.
    /// </summary>
    public partial class srptAutoPrepayAccountsError : FCSectionReport
    {

        public srptAutoPrepayAccountsError()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            if (_InstancePtr == null)
                _InstancePtr = this;
            this.Name = "Accounts with Credits - Unable to Create Auto Pre-Payment";
        }

        public static srptAutoPrepayAccountsError InstancePtr
        {
            get
            {
                return (srptAutoPrepayAccountsError)Sys.GetInstance(typeof(srptAutoPrepayAccountsError));
            }
        }

        protected srptAutoPrepayAccountsError _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                rsAccount.Dispose();
            }
            base.Dispose(disposing);
        }

        // nObj = 1
        //   0	srptAutoPrepayAccountsError	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Corey Gray              *
        // DATE           :                                       *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               12/15/2005              *
        // ********************************************************

        clsDRWrapper rsAccount = new clsDRWrapper();

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            //modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);
            txtDate.Text = DateTime.Today.ToShortDateString();
            txtMuniname.Text = modGlobalConstants.Statics.MuniName;

            rsAccount.OpenRecordset("SELECT * FROM tblAutoPrepayAccounts WHERE CreditCreated = 0 ORDER BY AccountKey", modExtraModules.strUTDatabase);

            if (!rsAccount.EndOfFile())
            {
                srptAutoPrepayAccounts.InstancePtr.txtTitle.Text = "Accounts with Credits - No Auto Pre-Payment Created";
                rptAutoPrepayAccounts.InstancePtr.PageBreak1.Enabled = true;
            }
            else
            {
                rptAutoPrepayAccounts.InstancePtr.PageBreak1.Enabled = false;
            }
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = rsAccount.EndOfFile();
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            try
            {   // On Error GoTo ERROR_HANDLER


                if (!rsAccount.EndOfFile())
                {

                    fldAccountNumber.Text = FCConvert.ToString(modUTStatusPayments.GetAccountNumber_2(rsAccount.Get_Fields_Int32("AccountKey")));
                    fldCreditAmount.Text = Strings.Format(rsAccount.Get_Fields_Double("CreditAmt"), "###0.00");

                    rsAccount.MoveNext();
                }


                return;
            }
            catch
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", "Error Detail Format", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void PageHeader_Format(object sender, EventArgs e)
        {
            txtPage.Text = "Page " + this.PageNumber;
        }

        private void srptAutoPrepayAccountsError_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //srptAutoPrepayAccountsError.Caption	= "Accounts with Credits - Unable to Create Auto Pre-Payment";
            //srptAutoPrepayAccountsError.Icon	= "srptAutoPrepayAccountsError.dsx":0000";
            //srptAutoPrepayAccountsError.Left	= 0;
            //srptAutoPrepayAccountsError.Top	= 0;
            //srptAutoPrepayAccountsError.Width	= 15240;
            //srptAutoPrepayAccountsError.Height	= 11115;
            //srptAutoPrepayAccountsError.StartUpPosition	= 3;
            //srptAutoPrepayAccountsError.SectionData	= "srptAutoPrepayAccountsError.dsx":058A;
            //End Unmaped Properties
        }
    }
}
