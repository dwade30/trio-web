﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
#if TWBL0000
using TWBL0000;


#elif TWRE0000
using TWRE0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for rptCertificateOfAssessment.
	/// </summary>
	public partial class rptCertificateOfAssessment : BaseSectionReport
	{
		public rptCertificateOfAssessment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Certificate of Assessment";
		}

		public static rptCertificateOfAssessment InstancePtr
		{
			get
			{
				return (rptCertificateOfAssessment)Sys.GetInstance(typeof(rptCertificateOfAssessment));
			}
		}

		protected rptCertificateOfAssessment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCertificateOfAssessment	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		private string strFiscalStart = string.Empty;
		private string strFiscalEnd = string.Empty;
		private double dblREValuation;
		// vbPorter upgrade warning: strFiscalEnding As object	OnWrite(string)	OnRead(string)
		public void Init(ref double dblREVal, ref string strFiscalBegin, ref string strFiscalEnding, bool modalDialog)
		{
			strFiscalStart = strFiscalBegin;
			dblREValuation = dblREVal;
			strFiscalEnd = strFiscalEnding;
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), false, false, "Pages", false, "", "TRIO Software", false, true, "AssessmentCertification", showModal: modalDialog);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: intStart As short --> As int	OnRead(string)
			int intStart = 0;
			// vbPorter upgrade warning: intEnd As short --> As int	OnRead(string)
			int intEnd = 0;
			string strMuni;
			clsDRWrapper rsLoad = new clsDRWrapper();
			int lngYear = 0;
			string strCounty = "";
			string strTaxCollector = "";
			string strTreasurer = "";
			double dblOther = 0;
			double dblSharing = 0;
			double dblOverlay = 0;
			double dblHomestead = 0;
			double dblCounty = 0;
			double dblMunicipal = 0;
			double dblTif = 0;
			double dblEducation = 0;
			double dblTotalDeductions = 0;
			double dblTotalAssessments = 0;
			string strCollectionDate = "";
			string strDueDate = "";
			double dblTaxRate = 0;
			string strInterestDate = "";
			double dblInterestRate = 0;
			double dblTotValuation = 0;
			string strduedate2 = "";
			string strInterestdate2 = "";
			string strCommitmentDate = "";
			double dblBETEReim = 0;
			strMuni = modGlobalConstants.Statics.MuniName;
			#if ModuleID_1
						            if (modGlobalVariables.Statics.CustomizedInfo.boolRegionalTown)
            {
                strMuni = modRegionalTown.GetTownKeyName_2(modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber);
                rsLoad.OpenRecordset("select * from taxratecalculation where isnull(townnumber,0) = " + modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber, modGlobalVariables.strREDatabase);
            }
            else
            {
                rsLoad.OpenRecordset("select * from taxratecalculation where isnull(townnumber,0) = 0", modGlobalVariables.strREDatabase);
            }

#else
			if (modRegionalTown.IsRegionalTown())
			{
				strMuni = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt16(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown));
				rsLoad.OpenRecordset("select * from taxratecalculation where isnull(townnumber,0) = " + FCConvert.ToString(modGlobalVariables.Statics.CustomizedInfo.intCurrentTown), modGlobalVariables.strREDatabase);
			}
			else
			{
				rsLoad.OpenRecordset("select * from taxratecalculation where isnull(townnumber,0) = 0", modGlobalVariables.strREDatabase);
			}
			#endif
			if (!rsLoad.EndOfFile())
			{
				#if ModuleID_3
				dblREValuation = Conversion.Val(rsLoad.Get_Fields_Double("REAmount")));
				#endif
				dblTotValuation = dblREValuation + Conversion.Val(rsLoad.Get_Fields_Int32("personalpropertyvaluation"));
				dblBETEReim = Conversion.Val(rsLoad.Get_Fields_Double("betereimbursement"));
				intStart = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("startpage"))));
				intEnd = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("endpage"))));
				// TODO: Check the table for the column [county] and replace with corresponding Get_Field method
				strCounty = FCConvert.ToString(rsLoad.Get_Fields("county"));
				strTaxCollector = FCConvert.ToString(rsLoad.Get_Fields_String("taxcollector"));
				strTreasurer = FCConvert.ToString(rsLoad.Get_Fields_String("treasurer"));
				lblMuniName.Text = strMuni;
				lblMunicipality2.Text = strMuni;
				lblCounty.Text = strCounty;
				txtCounty2.Text = strCounty;
				lblTaxCollector.Text = strTaxCollector;
				lblTaxCollector2.Text = strTaxCollector;
				dblCounty = Conversion.Val(rsLoad.Get_Fields_Double("countytax"));
				txtCountyTax.Text = Strings.Format(dblCounty, "#,###,###,##0.00");
				txtCountyTax2.Text = txtCountyTax.Text;
				dblMunicipal = Conversion.Val(rsLoad.Get_Fields_Double("municipalappropriation"));
				txtMunicipalAppropriation.Text = Strings.Format(dblMunicipal, "#,###,###,##0.00");
				txtMunicipalAppropriation2.Text = txtMunicipalAppropriation.Text;
				dblEducation = Conversion.Val(rsLoad.Get_Fields_Double("education"));
				txtSchoolAppropriation.Text = Strings.Format(dblEducation, "#,###,###,##0.00");
				txtSchoolAppropriation2.Text = txtSchoolAppropriation.Text;
				dblTif = Conversion.Val(rsLoad.Get_Fields_Double("tif"));
				txtTIF.Text = Strings.Format(dblTif, "#,###,###,##0.00");
				txtTif2.Text = txtTIF.Text;
				dblSharing = Conversion.Val(rsLoad.Get_Fields_Double("statesharing"));
				txtStateRevenue.Text = Strings.Format(dblSharing, "#,###,###,##0.00");
				txtStateRevenue2.Text = txtStateRevenue.Text;
				dblOther = Conversion.Val(rsLoad.Get_Fields_Double("other"));
				txtOtherRevenue.Text = Strings.Format(dblOther, "#,###,###,##0.00");
				txtOtherRevenue2.Text = txtOtherRevenue.Text;
				dblHomestead = Conversion.Val(rsLoad.Get_Fields_Double("homesteadreimbursement"));
				dblOverlay = Conversion.Val(rsLoad.Get_Fields_Double("overlay"));
				txtOverlay.Text = Strings.Format(dblOverlay, "#,###,###,##0.00");
				txtOverlay2.Text = txtOverlay.Text;
				txtHomestead.Text = Strings.Format(dblHomestead, "#,###,###,##0.00");
				txtBETE.Text = Strings.Format(dblBETEReim, "#,###,###,##0.00");
				txtBETE2.Text = txtBETE.Text;
				txtHomestead2.Text = txtHomestead.Text;
				dblTotalAssessments = dblOverlay + dblCounty + dblMunicipal + dblTif + dblEducation;
				txtTotalAssessments.Text = Strings.Format(dblTotalAssessments, "#,###,###,##0.00");
				txtTotalAssessments2.Text = txtTotalAssessments.Text;
				dblTotalDeductions = dblSharing + dblHomestead + dblOther + dblBETEReim;
				txtTotalDeductions.Text = Strings.Format(dblTotalDeductions, "#,###,###,##0.00");
				txtTotalDeductions2.Text = txtTotalDeductions.Text;
				txtNetAssessment.Text = Strings.Format(dblTotalAssessments - dblTotalDeductions, "#,###,###,##0.00");
				txtNetAssessment2.Text = txtNetAssessment.Text;
				strCollectionDate = "";
				if (Information.IsDate(rsLoad.Get_Fields("collectiondate")))
				{
					if (rsLoad.Get_Fields_DateTime("collectiondate").ToOADate() != 0)
					{
						strCollectionDate = Strings.Format(rsLoad.Get_Fields_DateTime("collectiondate"), "MM/dd/yyyy");
					}
				}
				if (Information.IsDate(rsLoad.Get_Fields("commitmentdate")))
				{
					if (rsLoad.Get_Fields_DateTime("commitmentdate").ToOADate() != 0)
					{
						strCommitmentDate = Strings.Format(rsLoad.Get_Fields_DateTime("commitmentdate"), "MM/dd/yyyy");
					}
				}
				if (strCommitmentDate == "")
					strCommitmentDate = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				lngYear = DateTime.Today.Year;
				if (Information.IsDate(strCommitmentDate))
				{
					lngYear = FCConvert.ToDateTime(strCommitmentDate).Year;
				}
				int intDateCount = 0;
				intDateCount = 0;
				if (Information.IsDate(rsLoad.Get_Fields("duedate3")))
				{
					if (rsLoad.Get_Fields_DateTime("duedate3").ToOADate() != 0)
					{
						intDateCount = 3;
						if (Information.IsDate(rsLoad.Get_Fields("duedate4")))
						{
							if (rsLoad.Get_Fields_DateTime("duedate4").ToOADate() != 0)
							{
								intDateCount = 4;
							}
						}
					}
				}
				// TODO: Check the table for the column [duedate] and replace with corresponding Get_Field method
				if (Information.IsDate(rsLoad.Get_Fields("duedate")))
				{
					// TODO: Check the table for the column [duedate] and replace with corresponding Get_Field method
					if (rsLoad.Get_Fields("duedate").ToOADate() != 0)
					{
						// TODO: Check the table for the column [duedate] and replace with corresponding Get_Field method
						strDueDate = Strings.Format(rsLoad.Get_Fields("duedate"), "MM/dd/yyyy");
						// If Month(strDueDate) > 4 Then
						// lngYear = Year(strDueDate)
						// Else
						// lngYear = Year(strDueDate) - 1
						// End If
					}
					if (Information.IsDate(rsLoad.Get_Fields("duedate2")))
					{
						if (rsLoad.Get_Fields_DateTime("duedate2").ToOADate() != 0)
						{
							if (intDateCount < 3)
							{
								strDueDate += " & " + Strings.Format(rsLoad.Get_Fields_DateTime("duedate2"), "MM/dd/yyyy");
							}
							else
							{
								strDueDate += ", " + Strings.Format(rsLoad.Get_Fields_DateTime("duedate2"), "MM/dd/yyyy");
							}
						}
					}
					if (intDateCount > 2)
					{
						if (intDateCount < 4)
						{
							strDueDate += " & " + Strings.Format(rsLoad.Get_Fields_DateTime("duedate3"), "MM/dd/yyyy");
						}
						else
						{
							strDueDate += ", " + Strings.Format(rsLoad.Get_Fields_DateTime("duedate3"), "MM/dd/yyyy");
						}
					}
					if (intDateCount > 3)
					{
						strDueDate += " & " + Strings.Format(rsLoad.Get_Fields_DateTime("duedate4"), "MM/dd/yyyy");
					}
				}
				int intInterestDateCount = 0;
				intInterestDateCount = 0;
				if (Information.IsDate(rsLoad.Get_Fields("interestdate3")))
				{
					if (rsLoad.Get_Fields_DateTime("interestdate3").ToOADate() != 0)
					{
						intInterestDateCount = 3;
						if (Information.IsDate(rsLoad.Get_Fields("interestdate4")))
						{
							if (rsLoad.Get_Fields_DateTime("interestdate4").ToOADate() != 0)
							{
								intInterestDateCount = 4;
							}
						}
					}
				}
				dblTaxRate = Conversion.Val(rsLoad.Get_Fields_Double("taxrate"));
				if (Information.IsDate(rsLoad.Get_Fields("interestdate")))
				{
					if (rsLoad.Get_Fields_DateTime("Interestdate").ToOADate() != 0)
					{
						strInterestDate = Strings.Format(rsLoad.Get_Fields_DateTime("interestdate"), "MM/dd/yyyy");
					}
					if (Information.IsDate(rsLoad.Get_Fields("interestdate2")))
					{
						if (rsLoad.Get_Fields_DateTime("interestdate2").ToOADate() != 0)
						{
							if (intInterestDateCount < 3)
							{
								strInterestDate += " & " + Strings.Format(rsLoad.Get_Fields_DateTime("interestdate2"), "MM/dd/yyyy");
							}
							else
							{
								strInterestDate += ", " + Strings.Format(rsLoad.Get_Fields_DateTime("interestdate2"), "MM/dd/yyyy");
							}
						}
					}
					if (intInterestDateCount > 2)
					{
						if (intInterestDateCount < 4)
						{
							strInterestDate += " & " + Strings.Format(rsLoad.Get_Fields_DateTime("interestdate3"), "MM/dd/yyyy");
						}
						else
						{
							strInterestDate += ", " + Strings.Format(rsLoad.Get_Fields_DateTime("interestdate3"), "MM/dd/yyyy");
						}
					}
					if (intInterestDateCount > 3)
					{
						strInterestDate += " & " + Strings.Format(rsLoad.Get_Fields_DateTime("interestdate4"), "MM/dd/yyyy");
					}
				}
				dblInterestRate = Conversion.Val(rsLoad.Get_Fields_Double("interestrate"));
			}
			// If IsDate(strFiscalEnd) Then
			// lngYear = Year(strFiscalEnd)
			// Else
			// lngYear = Year(Date)
			// End If
			RichEdit1.Font = new Font("Tahoma", RichEdit1.Font.Size);
			RichEdit1.Font = new Font(RichEdit1.Font.Name, 12);
			RichEdit2.Font = new Font("Tahoma", RichEdit2.Font.Size);
			RichEdit2.Font = new Font(RichEdit2.Font.Name, 12);
			RichEdit3.Font = new Font("Tahoma", RichEdit3.Font.Size);
			RichEdit3.Font = new Font(RichEdit3.Font.Name, 12);
			RichEdit4.Font = new Font("Tahoma", RichEdit4.Font.Size);
			RichEdit4.Font = new Font(RichEdit4.Font.Name, 12);
			RichEdit5.Font = new Font("Tahoma", RichEdit5.Font.Size);
			RichEdit5.Font = new Font(RichEdit5.Font.Name, 12);
			RichEdit6.Font = new Font("Tahoma", RichEdit6.Font.Size);
			RichEdit6.Font = new Font(RichEdit6.Font.Name, 12);
			RichEdit7.Font = new Font("Tahoma", RichEdit7.Font.Size);
			RichEdit7.Font = new Font(RichEdit7.Font.Name, 10);
			RichEdit8.Font = new Font("Tahoma", RichEdit8.Font.Size);
			RichEdit8.Font = new Font(RichEdit8.Font.Name, 10);
			// vbPorter upgrade warning: strStart As string	OnWrite(int, string)
			string strStart = "";
			// vbPorter upgrade warning: strEnd As string	OnWrite(int, string)
			string strEnd = "";
			if (intStart > 0)
			{
				strStart = FCConvert.ToString(intStart);
			}
			else
			{
				strStart = "   ";
			}
			if (intEnd > 0)
			{
				strEnd = FCConvert.ToString(intEnd);
			}
			else
			{
				strEnd = "   ";
			}
			RichEdit1.Text = "WE HEREBY CERTIFY, that the pages herein, numbered from " + strStart + " to " + strEnd + " inclusive, contain a list and valuation of Estates, ";
			RichEdit1.Text = RichEdit1.Text + "Real and Personal, liable to be taxed in the Municipality of " + strMuni + " for State, County, District, and Municipal Taxes ";
			RichEdit1.Text = RichEdit1.Text + "for the fiscal year " + strFiscalStart + " to " + strFiscalEnd + " as they existed on the first day of April " + FCConvert.ToString(lngYear) + ".";
			//FC:FINAL:CHN - issue #1400: Missing dates on report.
			// RichEdit2.Text = "IN WITNESS THEREOF, we have hereunto set our hands at " + strMuni + " this " + FCConvert.ToString(FCConvert.ToDateTime(strCommitmentDate).Day) + " day of " + Strings.Format(strCommitmentDate, "mmmm") + ", " + FCConvert.ToString(FCConvert.ToDateTime(strCommitmentDate).Year) + ".";
			RichEdit2.Text = "IN WITNESS THEREOF, we have hereunto set our hands at " + strMuni + " this " + FCConvert.ToString(FCConvert.ToDateTime(strCommitmentDate).Day) + " day of " + Strings.Format(strCommitmentDate, "MMMM") + ", " + FCConvert.ToString(FCConvert.ToDateTime(strCommitmentDate).Year) + ".";
			RichEdit3.Text = "You are to pay to " + strTreasurer + ", the Municipal Treasurer, or to any successor in office, the taxes herewith committed, paying on the last day of each month all money";
			RichEdit3.Text = RichEdit3.Text + " collected by you, and you are to complete and make an account of your collections of the whole sum on or before " + strCollectionDate + ".";
			RichEdit4.Text = "In case of the neglect of any person to pay the sum required by said list until after " + strDueDate + "; you will add interest to so much thereof as remains unpaid at the rate of ";
			RichEdit4.Text = RichEdit4.Text + Strings.Format(dblInterestRate, "0.00") + " percent per annum, commencing " + strInterestDate + " to the time of payment, and collect the same with the tax remaining unpaid.";
			RichEdit5.Text = "Given under our hands, as provided by a legal vote of the Municipality and Warrants received pursuant to the Laws of the State of Maine, this " + Strings.Format(strCommitmentDate, "MM/dd/yyyy") + ".";
			lblAssessorOf.Text = "Assessor(s) of: " + strMuni;
			RichEdit6.Text = "Herewith are committed to you true lists of the assessments of the Estates of the persons wherein named;";
			RichEdit6.Text = RichEdit6.Text + " you are to levy and collect the same, of each one their respective amount, therein set down, of the sum total of $" + txtNetAssessment.Text + " (being the amount ";
			RichEdit6.Text = RichEdit6.Text + "of the lists contained herein), according to the tenor of the foregoing warrant.";
			Label39.Text = "Given under our hands this " + Strings.Format(strCommitmentDate, "MM/dd/yyyy");
			RichEdit7.Text = "We hereby certify, that we have assessed a tax on the estate, real and personal liable to be taxed in the Municipality of " + strMuni + " for the fiscal year ";
			RichEdit7.Text = RichEdit7.Text + strFiscalStart + " to " + strFiscalEnd + ", at " + FCConvert.ToString(dblTaxRate * 1000) + " mils on the dollar, on a total taxable valuation of $";
			RichEdit7.Text = RichEdit7.Text + Strings.Format(dblTotValuation, "#,###,###,###,##0");
			RichEdit8.Text = "Lists of all the same we have committed to " + strTaxCollector + ", Tax Collector of said Municipality, with warrants in due form of law for collecting and paying ";
			RichEdit8.Text = RichEdit8.Text + "the same to " + strTreasurer + ", Municipal Treasurer of said Municipality, or the successor in office, on or before such date, or dates,";
			RichEdit8.Text = RichEdit8.Text + " as provided by legal vote of the Municipality and warrants received pursuant to the laws of the State of Maine. (Title 36 MRSA, section 712)";
			//FC:FINAL:DSE WordWrap is not functioning in RichTextBox
			RichEdit1.SetHtmlText(RichEdit1.Text);
			RichEdit2.SetHtmlText(RichEdit2.Text);
			RichEdit3.SetHtmlText(RichEdit3.Text);
			RichEdit4.SetHtmlText(RichEdit4.Text);
			RichEdit5.SetHtmlText(RichEdit5.Text);
			RichEdit6.SetHtmlText(RichEdit6.Text);
			RichEdit7.SetHtmlText(RichEdit7.Text);
			RichEdit8.SetHtmlText(RichEdit8.Text);
			Label40.Text = "Assessor(s) of: " + strMuni;
			Label76.Text = "Given under our hands this " + Strings.Format(strCommitmentDate, "MM/dd/yyyy");
			rsLoad.Dispose();
		}

	}
}
