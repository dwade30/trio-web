﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for frmEmailContacts.
	/// </summary>
	public partial class frmEmailContacts : BaseForm
	{
		public frmEmailContacts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEmailContacts InstancePtr
		{
			get
			{
				return (frmEmailContacts)Sys.GetInstance(typeof(frmEmailContacts));
			}
		}

		protected frmEmailContacts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int lngContactID;
		bool boolDataChanged;
		bool boolUnloadMe;
		//FC:FINAL:RPU:#1255 - Add a flag to retain if the form was opened from general entry
		public bool fromGeneralEntry = false;

		public void Init(bool boolModal, bool boolFromGroups = false, bool generalEntry = false)
		{
			boolUnloadMe = false;
			lngContactID = 0;
			if (boolFromGroups)
			{
				Toolbar1.Buttons[6 - 1].Visible = false;
				mnuGroups.Visible = false;
			}
			//FC:FINAL:RPU: #1255 - Add a flag to retain if the form was opened from general entry
			fromGeneralEntry = generalEntry;
			if (boolModal)
			{
				this.Show(FormShowEnum.Modal);
			}
			else
			{
				this.Show(App.MainForm);
			}
		}

		private void frmEmailContacts_Activated(object sender, System.EventArgs e)
		{
			if (boolUnloadMe)
			{
				Close();
				return;
			}
		}

		private void frmEmailContacts_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmEmailContacts_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEmailContacts.Icon	= "frmEmailContacts.frx":0000";
			//frmEmailContacts.FillStyle	= 0;
			//frmEmailContacts.ScaleWidth	= 9300;
			//frmEmailContacts.ScaleHeight	= 7695;
			//frmEmailContacts.LinkTopic	= "Form2";
			//frmEmailContacts.LockControls	= true;
			//frmEmailContacts.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//Toolbar1.ButtonWidth	= 820;
			//Toolbar1.ButtonHeight	= 794;
			//Buttons.NumButtons	= 6;
			//Images.NumListImages	= 6;
			//vsElasticLight1.OleObjectBlob	= "frmEmailContacts.frx":2F66";
			//End Unmaped Properties
			boolUnloadMe = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			txtEMail.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
			txtQuickname.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
			int lngID = 0;
			string strTemp = "";
			string strTemp2 = "";
			frmEmailChooseContactGroup.InstancePtr.Init(true, false, ref lngID, ref strTemp, false, false, ref strTemp2, true);
			if (lngID > 0)
			{
				LoadContact(lngID);
			}
			else if (lngID < 0)
			{
				// must not load
				boolUnloadMe = true;
			}
		}

		private void LoadContact(int lngID)
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsLoad = new clsDRWrapper();
				clsLoad.OpenRecordset("select * from emailcontacts where contactid = " + FCConvert.ToString(lngID), "SystemSettings");
				if (!clsLoad.EndOfFile())
				{
					lngContactID = lngID;
					boolDataChanged = false;
					txtEMail.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("email")));
					txtDescription.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("Description")));
					txtFirstName.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("firstname")));
					txtLastName.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("LastName")));
					txtQuickname.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("nickname")));
					txtCompany.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("Companyname")));
					txtPosition.Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("position")));
				}
				boolDataChanged = false;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In LoadContact", MsgBoxStyle.Critical, "Error");
			}
		}

		private void mnuAddToGroup_Click(object sender, System.EventArgs e)
		{
			AddToGroup();
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			DeleteContact();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuGroups_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:RPU:#1255: Show the form as modal if is opened from general entry
			////frmEmailGroups.InstancePtr.Init(0, false, false);
			frmEmailGroups.InstancePtr.Init(0, fromGeneralEntry, false);
		}

		private void mnuNewContact_Click(object sender, System.EventArgs e)
		{
			NewContact();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveContact();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveContact())
			{
				mnuExit_Click();
			}
		}

		private bool SaveContact()
		{
			bool SaveContact = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				SaveContact = false;
				if (Strings.Trim(txtQuickname.Text) == string.Empty)
				{
					FCMessageBox.Show("You must provide a quickname for the contact", MsgBoxStyle.Exclamation, "No Quickname");
					return SaveContact;
				}
				if (Strings.InStr(1, txtEMail.Text, "@", CompareConstants.vbTextCompare) <= 0)
				{
					FCMessageBox.Show("Not a valid E-mail address", MsgBoxStyle.Exclamation, "Invalid Address");
					return SaveContact;
				}
				rsSave.OpenRecordset("select * from emailcontacts where nickname = '" + Strings.Trim(txtQuickname.Text) + "' and not contactid = " + FCConvert.ToString(lngContactID), "SystemSettings");
				if (!rsSave.EndOfFile())
				{
					FCMessageBox.Show("This quickname is already used for another contact", MsgBoxStyle.Exclamation, "Invalid Quickname");
					return SaveContact;
				}
				rsSave.OpenRecordset("select * from emailgroups where nickname = '" + Strings.Trim(txtQuickname.Text) + "' ", "SystemSettings");
				if (!rsSave.EndOfFile())
				{
					FCMessageBox.Show("This quickname is already used for a contact group", MsgBoxStyle.Exclamation, "Invalid Quickname");
					return SaveContact;
				}
				rsSave.OpenRecordset("select * from emailcontacts where contactid = " + FCConvert.ToString(lngContactID), "SystemSettings");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					// lngContactID = rsSave.Get_Fields("contactid")
				}
				rsSave.Set_Fields("email", Strings.Trim(txtEMail.Text));
				rsSave.Set_Fields("description", Strings.Trim(txtDescription.Text));
				rsSave.Set_Fields("nickname", Strings.Trim(txtQuickname.Text));
				rsSave.Set_Fields("firstname", Strings.Trim(txtFirstName.Text));
				rsSave.Set_Fields("lastname", Strings.Trim(txtLastName.Text));
				rsSave.Set_Fields("companyname", Strings.Trim(txtCompany.Text));
				rsSave.Set_Fields("position", Strings.Trim(txtPosition.Text));
				rsSave.Update();
				lngContactID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("contactid"));
				SaveContact = true;
				boolDataChanged = false;
				FCMessageBox.Show("Save Successful", MsgBoxStyle.Information, "Saved");
				return SaveContact;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In SaveContact", MsgBoxStyle.Critical, "Error");
			}
			return SaveContact;
		}

		private void ClearTextBoxes()
		{
			txtCompany.Text = "";
			txtDescription.Text = "";
			txtEMail.Text = "";
			txtFirstName.Text = "";
			txtLastName.Text = "";
			txtPosition.Text = "";
			txtQuickname.Text = "";
		}

		private void DeleteContact()
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (lngContactID != 0)
				{
					if (FCMessageBox.Show("This will delete this contact and remove them from all groups" + "\r\n" + "Do you wish to continue?", MsgBoxStyle.Exclamation | MsgBoxStyle.YesNo, "Delete?") == DialogResult.Yes)
					{
						clsDRWrapper clsSave = new clsDRWrapper();
						clsSave.Execute("delete from emailassociations where contactid = " + FCConvert.ToString(lngContactID), "SystemSettings");
						clsSave.Execute("delete from emailcontacts where contactid = " + FCConvert.ToString(lngContactID), "SystemSettings");
						lngContactID = 0;
						boolDataChanged = false;
						ClearTextBoxes();
						FCMessageBox.Show("Contact Deleted", MsgBoxStyle.Information, "Deleted");
						return;
					}
				}
				else
				{
					ClearTextBoxes();
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In DeleteContact", MsgBoxStyle.Critical, "Error");
			}
		}

		private void mnuSearch_Click(object sender, System.EventArgs e)
		{
			FindContact();
		}

		private void FindContact()
		{
			try
			{
				// On Error GoTo ErrorHandler
				int lngID = 0;
				if (boolDataChanged)
				{
					if (FCMessageBox.Show("Data has been changed" + "\r\n" + "Do you want to save first?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Save?") == DialogResult.Yes)
					{
						if (!SaveContact())
						{
							return;
						}
					}
				}
				string temp = "";
				frmEmailChooseContactGroup.InstancePtr.Init(true, false, ref lngID, ref temp, false);
				if (lngID > 0)
				{
					LoadContact(lngID);
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In FindContact", MsgBoxStyle.Critical, "Error");
			}
		}

		private void AddToGroup()
		{
			try
			{
				// On Error GoTo ErrorHandler
				int lngID = 0;
				if (lngContactID == 0)
				{
					FCMessageBox.Show("Cannot add this contact to a group until it has been saved and created", MsgBoxStyle.Exclamation, "Save First");
					return;
				}
				string temp = "";
				frmEmailChooseContactGroup.InstancePtr.Init(false, true, ref lngID, ref temp, false);
				if (lngID > 0)
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from emailassociations where groupid = " + FCConvert.ToString(lngID) + " and contactid = " + FCConvert.ToString(lngContactID), "SystemSettings");
					if (rsSave.EndOfFile())
					{
						rsSave.AddNew();
						rsSave.Set_Fields("contactid", lngContactID);
						rsSave.Set_Fields("groupid", lngID);
						rsSave.Update();
						FCMessageBox.Show("Contact added to group", MsgBoxStyle.Information, "Added");
					}
					else
					{
						FCMessageBox.Show("This contact is already in that group", MsgBoxStyle.Information, "Already a Member");
					}
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In AddToGroup", MsgBoxStyle.Critical, "Error");
			}
		}

		private void Toolbar1_ButtonClick(object sender, fecherFoundation.FCToolBarButtonClickEventArgs e)
		{
			ToolBarButton Button = e.Button;
			int lngID/*unused?*/;
			if (Strings.UCase(Button.Name) == "BTNSAVE")
			{
				SaveContact();
			}
			else if (Strings.UCase(Button.Name) == "BTNNEW")
			{
				NewContact();
			}
			else if (Strings.UCase(Button.Name) == "BTNDELETE")
			{
				DeleteContact();
			}
			else if (Strings.UCase(Button.Name) == "BTNSEARCH")
			{
				FindContact();
			}
			else if (Strings.UCase(Button.Name) == "BTNADDTOGROUP")
			{
				AddToGroup();
			}
			else if (Strings.UCase(Button.Name) == "BTNEDITGROUPS")
			{
				//FC:FINAL:RPU:#1255: Show the form as modal if is opened from general entry
				//frmEmailGroups.InstancePtr.Init(0, false, false);
				frmEmailGroups.InstancePtr.Init(0, fromGeneralEntry, false);
			}
		}

		private void NewContact()
		{
			if (boolDataChanged)
			{
				if (FCMessageBox.Show("Save changes to current contact?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Save First?") == DialogResult.Yes)
				{
					if (!SaveContact())
					{
						return;
					}
				}
			}
			lngContactID = 0;
			ClearTextBoxes();
			boolDataChanged = false;
		}

		private void txtCompany_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtDescription_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtEMail_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtFirstName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtLastName_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtPosition_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtQuickname_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuSave_Click(sender, e);
		}

		private void cmdNewContact_Click(object sender, EventArgs e)
		{
			this.mnuNewContact_Click(sender, e);
		}

		private void cmdDelete_Click(object sender, EventArgs e)
		{
			this.mnuDelete_Click(sender, e);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			this.mnuSave_Click(sender, e);
		}
	}
}
