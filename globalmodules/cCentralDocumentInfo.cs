﻿using System;

namespace SharedApplication.CentralDocuments
{
	public class cCentralDocumentInfo
	{
		//=========================================================
		private int lngRecordID;
		private string strMediaType = string.Empty;
		private string strItemName = string.Empty;
		private string strItemDescription = string.Empty;
		private string strReferenceType = string.Empty;
		private int lngReferenceID;
		private string strAltReference = string.Empty;
		private string strDataGroup = string.Empty;
		private bool boolUpdated;
		private bool boolDeleted;
     
		public string DataGroup
		{
			set
			{
				strDataGroup = value;
				IsUpdated = true;
			}
			get
			{
				string DataGroup = "";
				DataGroup = strDataGroup;
				return DataGroup;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string MediaType
		{
			set
			{
				strMediaType = value;
				IsUpdated = true;
			}
			get
			{
				string MediaType = "";
				MediaType = strMediaType;
				return MediaType;
			}
		}

		public string ItemName
		{
			set
			{
				strItemName = value;
				IsUpdated = true;
			}
			get
			{
				string ItemName = "";
				ItemName = strItemName;
				return ItemName;
			}
		}

		public string ItemDescription
		{
			set
			{
				strItemDescription = value;
				IsUpdated = true;
			}
			get
			{
				string ItemDescription = "";
				ItemDescription = strItemDescription;
				return ItemDescription;
			}
		}

		public string ReferenceType
		{
			set
			{
				strReferenceType = value;
				IsUpdated = true;
			}
			get
			{
				string ReferenceType = "";
				ReferenceType = strReferenceType;
				return ReferenceType;
			}
		}

		public int ReferenceID
		{
			set
			{
				lngReferenceID = value;
				IsUpdated = true;
			}
			get
			{
				int ReferenceID = 0;
				ReferenceID = lngReferenceID;
				return ReferenceID;
			}
		}

		public string AltReference
		{
			set
			{
				strAltReference = value;
				IsUpdated = true;
			}
			get
			{
				string AltReference = "";
				AltReference = strAltReference;
				return AltReference;
			}
		}

		public cCentralDocumentInfo() : base()
		{
			strMediaType = "";
			strItemName = "";
			strItemDescription = "";
			strReferenceType = "";
			lngReferenceID = 0;
			strAltReference = "";
			lngRecordID = 0;
		}
	}
}
