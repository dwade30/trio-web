﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using Wisej.Web;
using Microsoft.VisualBasic;
using fecherFoundation;
using Global;

namespace TWPY0000
{
	public class clsShiftTypeList
	{
		//=========================================================
		private int intCurrentShiftType;
		private clsShiftType[] ShiftList = null;

		public int InsertShiftType(clsShiftType tShift)
		{
			int InsertShiftType = 0;
			int intReturn;
			// vbPorter upgrade warning: intindex As short	OnWriteFCConvert.ToInt32(
			int intindex;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				intReturn = -1;
				intindex = -1;
				InsertShiftType = -1;
				if (!FCUtils.IsEmpty(ShiftList))
				{
					intindex = Information.UBound(ShiftList, 1);
					if (intindex < 0)
					{
						intindex = 0;
					}
					Array.Resize(ref ShiftList, intindex + 1 + 1);
					intindex = Information.UBound(ShiftList, 1);
				}
				else
				{
					ShiftList = new clsShiftType[1 + 1];
					intindex = 0;
				}
				ShiftList[intindex] = tShift;
				intReturn = intindex;
				InsertShiftType = intindex;
				intCurrentShiftType = intindex;
				return InsertShiftType;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err().Number) + " " + "\r\n" + "In InsertShiftType", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return InsertShiftType;
		}

		public int AddShiftType(string strName, modSchedule.ScheduleShiftType intType, int lngCatID, int lngID, double dblLunch)
		{
			int AddShiftType = 0;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsShiftType tShift = new clsShiftType();
				int intReturn;
				intReturn = -1;
				AddShiftType = intReturn;
				tShift.OvertimePayCatID = lngCatID;
				tShift.ShiftID = lngID;
				tShift.ShiftName = strName;
				tShift.ShiftType = FCConvert.ToInt32(intType);
				tShift.LunchTime = dblLunch;
				tShift.Unused = false;
				intReturn = InsertShiftType(tShift);
				AddShiftType = intReturn;
				return AddShiftType;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err().Number) + " " + "\r\n" + "In AddShiftType", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddShiftType;
		}

		public void LoadTypes()
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				string strSQL;
				clsDRWrapper rsLoad = new clsDRWrapper();
				FCUtils.EraseSafe(ShiftList);
				strSQL = "select id from shifttypes order by name";
				rsLoad.OpenRecordset(strSQL, "twpy0000.vb1");
				clsShiftType tShift;
				while (!rsLoad.EndOfFile())
				{
					tShift = new clsShiftType();
					tShift.ShiftID = FCConvert.ToInt32(rsLoad.Get_Fields("id"));
					tShift.Unused = false;
					tShift.LoadType(rsLoad.Get_Fields_Int32("id"));
					InsertShiftType(tShift);
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err().Number) + " " + fecherFoundation.Information.Err().Description + "\r\n" + "In LoadTypes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public bool SaveShiftTypes()
		{
			bool SaveShiftTypes = false;
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				SaveShiftTypes = false;
				bool boolReturn;
				boolReturn = true;
				// vbPorter upgrade warning: X As short	OnWriteFCConvert.ToInt32(
				short X;
				if (!FCUtils.IsEmpty(ShiftList))
				{
					for (X = 0; X <= (Information.UBound(ShiftList, 1)); X++)
					{
						if (!(ShiftList[X] == null))
						{
							// If Not ShiftList(x).Unused Then
							boolReturn = boolReturn && ShiftList[X].SaveType();
							// End If
						}
					}
					// X
				}
				SaveShiftTypes = boolReturn;
				return SaveShiftTypes;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err().Number) + " " + fecherFoundation.Information.Err().Description + "\r\n" + "In SaveShiftTypes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveShiftTypes;
		}

		public clsShiftType GetCurrentShiftType()
		{
			clsShiftType GetCurrentShiftType = null;
			clsShiftType tShift;
			tShift = null;
			if (!FCUtils.IsEmpty(ShiftList))
			{
				if (intCurrentShiftType >= 0)
				{
					if (!(ShiftList[intCurrentShiftType] == null))
					{
						if (!ShiftList[intCurrentShiftType].Unused)
						{
							tShift = ShiftList[intCurrentShiftType];
						}
					}
				}
			}
			GetCurrentShiftType = tShift;
			return GetCurrentShiftType;
		}

		public clsShiftType GetShiftTypeByIndex(int intindex)
		{
			clsShiftType GetShiftTypeByIndex = null;
			clsShiftType tShift;
			tShift = null;
			if (!FCUtils.IsEmpty(ShiftList) && intindex >= 0)
			{
				if (!(ShiftList[intindex] == null))
				{
					if (!ShiftList[intindex].Unused)
					{
						intCurrentShiftType = intindex;
						tShift = ShiftList[intindex];
					}
				}
			}
			GetShiftTypeByIndex = tShift;
			return GetShiftTypeByIndex;
		}

		public clsShiftType GetShiftTypeByType(int lngType)
		{
			clsShiftType GetShiftTypeByType = null;
			clsShiftType tShift;
			tShift = null;
			if (!FCUtils.IsEmpty(ShiftList) && lngType >= 0)
			{
				MoveFirst();
				while (tShift == null && GetCurrentIndex() >= 0)
				{
					if (ShiftList[GetCurrentIndex()].ShiftID == lngType)
					{
						tShift = ShiftList[GetCurrentIndex()];
					}
					MoveNext();
				}
			}
			GetShiftTypeByType = tShift;
			return GetShiftTypeByType;
		}

		public int GetCurrentIndex()
		{
			int GetCurrentIndex = 0;
			GetCurrentIndex = intCurrentShiftType;
			return GetCurrentIndex;
		}

		public void MoveFirst()
		{
			if (!FCUtils.IsEmpty(ShiftList))
			{
				if (Information.UBound(ShiftList, 1) >= 0)
				{
					intCurrentShiftType = -1;
					MoveNext();
				}
				else
				{
					intCurrentShiftType = -1;
				}
			}
			else
			{
				intCurrentShiftType = -1;
			}
		}

		public int MoveNext()
		{
			int MoveNext = 0;
			int intReturn;
			intReturn = -1;
			MoveNext = -1;
			if (!FCUtils.IsEmpty(ShiftList))
			{
				// intCurrentWeek = intCurrentWeek + 1
				if (intCurrentShiftType > Information.UBound(ShiftList, 1))
				{
					intReturn = -1;
				}
				else
				{
					while (intCurrentShiftType <= Information.UBound(ShiftList, 1))
					{
						intCurrentShiftType += 1;
						if (intCurrentShiftType > Information.UBound(ShiftList, 1))
						{
							intReturn = -1;
							break;
						}
						else if (ShiftList[intCurrentShiftType] == null)
						{
						}
						else if (ShiftList[intCurrentShiftType].Unused)
						{
						}
						else
						{
							intReturn = intCurrentShiftType;
							break;
						}
					}
				}
				intCurrentShiftType = intReturn;
				MoveNext = intReturn;
			}
			else
			{
				intCurrentShiftType = -1;
				MoveNext = -1;
			}
			return MoveNext;
		}
	}
}
