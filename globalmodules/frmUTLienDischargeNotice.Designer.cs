﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmUTLienDischargeNotice.
	/// </summary>
	partial class frmUTLienDischargeNotice : BaseForm
	{
		public fecherFoundation.FCFrame fraVariables;
		public FCGrid vsData;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fraVariables = new fecherFoundation.FCFrame();
			this.vsData = new fecherFoundation.FCGrid();
			this.lblName = new fecherFoundation.FCLabel();
			this.lblYear = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFileSaveExit = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraVariables)).BeginInit();
			this.fraVariables.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSaveExit)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFileSaveExit);
			this.BottomPanel.Location = new System.Drawing.Point(0, 532);
			this.BottomPanel.Size = new System.Drawing.Size(456, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraVariables);
			this.ClientArea.Size = new System.Drawing.Size(476, 534);
			this.ClientArea.Controls.SetChildIndex(this.fraVariables, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(476, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(238, 28);
			this.HeaderText.Text = "Lien Discharge Notice";
			// 
			// fraVariables
			// 
			this.fraVariables.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.fraVariables.Controls.Add(this.vsData);
			this.fraVariables.Controls.Add(this.lblName);
			this.fraVariables.Controls.Add(this.lblYear);
			this.fraVariables.Location = new System.Drawing.Point(30, 30);
			this.fraVariables.Name = "fraVariables";
			this.fraVariables.Size = new System.Drawing.Size(420, 502);
			this.fraVariables.TabIndex = 0;
			this.fraVariables.Text = "Confirm Data";
			// 
			// vsData
			// 
			this.vsData.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsData.Cols = 3;
			this.vsData.ColumnHeadersVisible = false;
			this.vsData.FixedRows = 0;
			this.vsData.Location = new System.Drawing.Point(20, 90);
			this.vsData.Name = "vsData";
			this.vsData.Rows = 1;
			this.vsData.Size = new System.Drawing.Size(381, 392);
			this.vsData.TabIndex = 2;
			this.vsData.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsData_KeyPressEdit);
			this.vsData.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsData_BeforeEdit);
			this.vsData.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsData_ValidateEdit);
			this.vsData.CurrentCellChanged += new System.EventHandler(this.vsData_RowColChange);
			// 
			// lblName
			// 
			this.lblName.Location = new System.Drawing.Point(20, 60);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(373, 15);
			this.lblName.TabIndex = 1;
			this.lblName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblYear
			// 
			this.lblYear.Location = new System.Drawing.Point(20, 30);
			this.lblYear.Name = "lblYear";
			this.lblYear.Size = new System.Drawing.Size(130, 15);
			this.lblYear.TabIndex = 0;
			this.lblYear.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSaveExit,
            this.Seperator,
            this.mnuFileExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileSaveExit
			// 
			this.mnuFileSaveExit.Index = 0;
			this.mnuFileSaveExit.Name = "mnuFileSaveExit";
			this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSaveExit.Text = "Save & Continue";
			this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 2;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdFileSaveExit
			// 
			this.cmdFileSaveExit.AppearanceKey = "acceptButton";
			this.cmdFileSaveExit.Location = new System.Drawing.Point(162, 30);
			this.cmdFileSaveExit.Name = "cmdFileSaveExit";
			this.cmdFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileSaveExit.Size = new System.Drawing.Size(157, 48);
			this.cmdFileSaveExit.TabIndex = 0;
			this.cmdFileSaveExit.Text = "Save & Continue";
			this.cmdFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// frmUTLienDischargeNotice
			// 
			this.ClientSize = new System.Drawing.Size(476, 594);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmUTLienDischargeNotice";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Lien Discharge Notice";
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmUTLienDischargeNotice_Load);
			this.Activated += new System.EventHandler(this.frmUTLienDischargeNotice_Activated);
			this.Resize += new System.EventHandler(this.frmUTLienDischargeNotice_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUTLienDischargeNotice_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraVariables)).EndInit();
			this.fraVariables.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSaveExit)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdFileSaveExit;
	}
}
