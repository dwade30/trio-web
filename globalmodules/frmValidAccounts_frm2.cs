﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using Global;
using fecherFoundation;

namespace Global
{
	/// <summary>
	/// Summary description for frmLoadValidAccounts.
	/// </summary>
	public partial class frmLoadValidAccounts : BaseForm
	{
		public frmLoadValidAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLoadValidAccounts InstancePtr
		{
			get
			{
				return (frmLoadValidAccounts)Sys.GetInstance(typeof(frmLoadValidAccounts));
			}
		}

		protected frmLoadValidAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ************************************************
		// Property of TRIO Software Corporation          *
		// *
		// WRITTEN BY :           Dave Wade               *
		// DATE       :                                   *
		// *
		// MODIFIED BY:           Jim Bertolino           *
		// DATE       :           09/14/2006              *
		// *
		// ************************************************
		// Must have a global variable called gstrPassAccountFromForm
		// it is declared in monGlobalConstants
		// it will return the account in gstrPassAccountFromForm or through the init function
		// if it is cancelled or not account is selected then it will set the variable to ""
		clsDRWrapper rsDeptDiv = new clsDRWrapper();
		clsDRWrapper rsExpObj = new clsDRWrapper();
		clsDRWrapper rsRev = new clsDRWrapper();
		clsDRWrapper rsLedger = new clsDRWrapper();

		string strAccountNumber = "";
		string strDefaultAccount;
		int lngColAccount;
		int lngColDesc1;
		int lngColDesc2;
		int lngColDesc3;
		int lngColDesc4;
		int lngColDesc5;
		string strAcctTypeToShow;

		public string Init(string strPassDefaultAccount, string strAccounttTypeToShow = "")
		{
			string Init = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will show the form and return the account selected
				strDefaultAccount = strPassDefaultAccount;
				strAcctTypeToShow = strAccounttTypeToShow;
				this.Show(FormShowEnum.Modal, App.MainForm);
				Init = strAccountNumber;
				return Init;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Init", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			return Init;
		}
		
		private void frmLoadValidAccounts_Load(object sender, System.EventArgs e)
		{
            try
			{
				// On Error GoTo ERROR_HANDLER
				// col numbers
				lngColAccount = 0;
				lngColDesc1 = 1;
				lngColDesc2 = 2;
				lngColDesc3 = 3;
				lngColDesc4 = 4;
				lngColDesc5 = 5;
				modGlobalConstants.Statics.gstrPassAccountFromForm = "";
				strAccountNumber = "";
				rsDeptDiv.OpenRecordset("SELECT * FROM DeptDivTitles", "TWBD0000.vb1");
				rsExpObj.OpenRecordset("SELECT * FROM ExpObjTitles", "TWBD0000.vb1");
				rsLedger.OpenRecordset("SELECT * FROM LedgerTitles", "TWBD0000.vb1");
				rsRev.OpenRecordset("SELECT * FROM RevTitles", "TWBD0000.vb1");
                FormatGrid();
				
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				//FC:FINAL:AM: moved code from activate
				int intErr = 1;
				//FormatGrid();
				intErr = 2;
				vsAcct.TextMatrix(0, lngColAccount, "Account");
				intErr = 3;
				vsAcct.TextMatrix(0, lngColDesc1, "Dept/Fund");
				intErr = 4;
				vsAcct.TextMatrix(0, lngColDesc2, "Div/Acct/Prog");
				// set valid accounts flexgrid column headings
				intErr = 5;
				vsAcct.TextMatrix(0, lngColDesc3, "Rev/Exp/Fun");
				intErr = 6;
				vsAcct.TextMatrix(0, lngColDesc4, "Obj");
				intErr = 7;
				vsAcct.TextMatrix(0, lngColDesc5, "Cost Center");
				intErr = 8;
                vsAcct.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                GetData();
				intErr = 90;
				if (vsAcct.Enabled && vsAcct.Visible)
				{
                    vsAcct.Focus();
				}
				intErr = 100;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Form Load", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void frmLoadValidAccounts_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo ERROR_HANDLER
				// catches the escape and enter keys
				if (KeyAscii == Keys.Escape)
				{
					KeyAscii = (Keys)0;
					Close();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Form KeyPress", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
		}

		private void frmLoadValidAccounts_Resize(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				FormatGrid();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Form Resize", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void GetData()
		{
			int counter;
			string temp = "";
			clsDRWrapper rs = new clsDRWrapper();
			//! Load frmWait; // show the wait form
			frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Building Valid Accounts List";
			frmWait.InstancePtr.Show();
			frmWait.InstancePtr.Refresh();
			while (true)
			{
				try
				{
					// On Error GoTo ErrorHandler
					//ContinueLoadingData:;
					if (strAcctTypeToShow == "")
					{
						rs.OpenRecordset("SELECT * FROM AccountMaster WHERE Valid = 1 ORDER BY AccountType, FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField, FifthAccountField", "TWBD0000.vb1");
					}
					else
					{
						rs.OpenRecordset("SELECT * FROM AccountMaster WHERE AccountType = '" + strAcctTypeToShow + "' AND Valid = 1 ORDER BY AccountType, FirstAccountField, SecondAccountField, ThirdAccountField, FourthAccountField, FifthAccountField ", "TWBD0000.vb1");
					}
					if (!rs.EndOfFile())
					{
						rs.MoveLast();
						rs.MoveFirst();
						vsAcct.Rows = rs.RecordCount() + 1;
						vsAcct.Row = 1;
						if (vsAcct.Rows < 18)
						{
							vsAcct.Height = vsAcct.RowHeight(0) * vsAcct.Rows + 75;
						}
						for (counter = 1; counter <= rs.RecordCount(); counter++)
						{
							//Application.DoEvents();
							if (FCConvert.ToString(rs.Get_Fields_String("AccountType")) == "E")
							{
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								GetExp(rs.Get_Fields_String("Account"));
							}
							else if (rs.Get_Fields_String("AccountType") == "R")
							{
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								GetRev(rs.Get_Fields_String("Account"));
							}
							else if (rs.Get_Fields_String("AccountType") == "G")
							{
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								GetLedger(rs.Get_Fields_String("Account"));
                            }
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							vsAcct.TextMatrix(vsAcct.Row, lngColAccount, FCConvert.ToString(rs.Get_Fields("Account")));
							if (vsAcct.Row < vsAcct.Rows - 1)
							{
								vsAcct.Row += 1;
							}
							rs.MoveNext();
						}

                        //vsAcct.Col = 0;
                        //vsAcct.Row = 1;
						vsAcct.Select(1,0);
						FindMatchingAccount();
					}
					frmWait.InstancePtr.Unload();
					return;
				}
				catch (Exception ex)
				{
					// ErrorHandler:
					if (Information.Err(ex).Number == 401)
					{
						//FC:FINAL:SBE - changed to while statement
						//goto ContinueLoadingData;
					}
					else
					{
						MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Form Load", MessageBoxButtons.OK, MessageBoxIcon.Information);
						break;
					}
				}
			}
		}

		private void GetExp(string x)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rs2 = new clsDRWrapper();
				string Dept;
				string Div = "";
				string tempDiv;
				string tempObj;
				string Expense = "";
				string Object = "";
				string tempAccount;
				tempDiv = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
				tempObj = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)))));
				tempAccount = Strings.Right(x, x.Length - 2);
				Dept = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2)));
				tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Exp, 2)) + 1));
				if (!modAccountTitle.Statics.ExpDivFlag)
				{
					Div = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)));
					tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) + 1));
					Expense = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)));
					if (!modAccountTitle.Statics.ObjFlag)
					{
						tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) + 1));
						Object = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)));
					}
				}
				else
				{
					Expense = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)));
					if (!modAccountTitle.Statics.ObjFlag)
					{
						tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 5, 2)) + 1));
						Object = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Exp, 7, 2)));
					}
				}
				if (!modAccountTitle.Statics.ExpDivFlag)
				{
					rsDeptDiv.FindFirstRecord2("Department,Division", Dept + "," + tempDiv, ",");
					if (rsDeptDiv.NoMatch != true)
					{
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
						rsDeptDiv.FindFirstRecord2("Department,Division", Dept + "," + Div, ",");
						if (rsDeptDiv.NoMatch != true)
						{
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
							if (!modAccountTitle.Statics.ObjFlag)
							{
								rsExpObj.FindFirstRecord2("Expense,Object", Expense + "," + tempObj, ",");
								if (rsExpObj.NoMatch != true)
								{
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
									rsExpObj.FindFirstRecord2("Expense,Object", Expense + "," + Object, ",");
									if (rsExpObj.NoMatch != true)
									{
										vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
										return;
									}
									else
									{
										vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
										vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
										vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
										vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
										return;
									}
								}
								else
								{
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
									return;
								}
							}
							else
							{
								rsExpObj.FindFirstRecord2("Expense,Object", Expense + "," + tempObj, ",");
								if (rsExpObj.NoMatch != true)
								{
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
									return;
								}
								else
								{
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
									return;
								}
							}
						}
						else
						{
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
							return;
						}
					}
					else
					{
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
						return;
					}
				}
				else
				{
					rsDeptDiv.FindFirstRecord2("Department,Division", Dept + "," + tempDiv, ",");
					if (rsDeptDiv.NoMatch != true)
					{
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
						if (!modAccountTitle.Statics.ObjFlag)
						{
							rsExpObj.FindFirstRecord2("Expense,Object", Expense + "," + tempObj, ",");
							if (rsExpObj.NoMatch != true)
							{
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
								rsExpObj.FindFirstRecord2("Expense,Object", Expense + "," + Object, ",");
								if (rsExpObj.NoMatch != true)
								{
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
									return;
								}
								else
								{
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
									vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
									return;
								}
							}
							else
							{
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
								return;
							}
						}
						else
						{
							rsExpObj.FindFirstRecord2("Expense,Object", Expense + "," + tempObj, ",");
							if (rsExpObj.NoMatch != true)
							{
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, FCConvert.ToString(rsExpObj.Get_Fields_String("ShortDescription")));
								return;
							}
							else
							{
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
								return;
							}
						}
					}
					else
					{
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
						return;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Get EXP", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void GetRev(string x)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rs2 = new clsDRWrapper();
				string Dept;
				string Div = "";
				string tempExpDiv;
				string Revenue = "";
				string tempAccount;
				tempExpDiv = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)))));
				tempAccount = Strings.Right(x, x.Length - 2);
				Dept = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Rev, 2)));
				tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Rev, 2)) + 1));
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					Div = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)));
					tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)) + 1));
					Revenue = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)));
				}
				else
				{
					Revenue = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Rev, 5, 2)));
				}
				if (!modAccountTitle.Statics.RevDivFlag)
				{
					rsDeptDiv.FindFirstRecord2("Department,Division", Dept + "," + tempExpDiv, ",");
					if (rsDeptDiv.NoMatch != true)
					{
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
						rsDeptDiv.FindFirstRecord2("Department,Division", Dept + "," + Div, ",");
						if (rsDeptDiv.NoMatch != true)
						{
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
							rsRev.FindFirstRecord2("Department,Division,Revenue", Dept + "," + Div + "," + Revenue, ",");
							if (rsRev.NoMatch != true)
							{
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, FCConvert.ToString(rsRev.Get_Fields_String("ShortDescription")));
								return;
							}
							else
							{
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
								vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
								return;
							}
						}
						else
						{
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
							return;
						}
					}
					else
					{
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
						return;
					}
				}
				else
				{
					rsDeptDiv.FindFirstRecord2("Department,Division", Dept + "," + tempExpDiv, ",");
					if (rsDeptDiv.NoMatch != true)
					{
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, FCConvert.ToString(rsDeptDiv.Get_Fields_String("ShortDescription")));
						rsRev.FindFirstRecord2("Department,Revenue", Dept + "," + Revenue, ",");
						if (rsRev.NoMatch != true)
						{
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, FCConvert.ToString(rsRev.Get_Fields_String("ShortDescription")));
							return;
						}
						else
						{
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
							return;
						}
					}
					else
					{
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
						vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
						return;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Get EXP", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void GetLedger(string x)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rs2 = new clsDRWrapper();
				string Fund;
				string Acct;
				string tempAcct;
				string Year = "";
				string tempAccount;
				tempAcct = modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
				tempAccount = Strings.Right(x, x.Length - 2);
				Fund = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)));
				tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Left(modAccountTitle.Statics.Ledger, 2)) + 1));
				Acct = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)));
				if (!modAccountTitle.Statics.YearFlag)
				{
					tempAccount = Strings.Right(tempAccount, tempAccount.Length - (FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)) + 1));
					Year = Strings.Left(tempAccount, FCConvert.ToInt32(Strings.Mid(modAccountTitle.Statics.Ledger, 5, 2)));
				}
				rsLedger.FindFirstRecord2("Fund,Account", Fund + "," + tempAcct, ",");
				if (rsLedger.NoMatch != true)
				{
					vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, FCConvert.ToString(rsLedger.Get_Fields_String("ShortDescription")));
					if (!modAccountTitle.Statics.YearFlag)
					{
						rsLedger.FindFirstRecord2("Fund,Account,Year", Fund + "," + Acct + "," + Year, ",");
						if (rsLedger.NoMatch != true)
						{
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, FCConvert.ToString(rsLedger.Get_Fields_String("ShortDescription")));
							return;
						}
						else
						{
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
							return;
						}
					}
					else
					{
						rsLedger.FindFirstRecord2("Fund,Account", Fund + "," + Acct, ",");
						if (rsLedger.NoMatch != true)
						{
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, FCConvert.ToString(rsLedger.Get_Fields_String("ShortDescription")));
							return;
						}
						else
						{
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
							vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
							return;
						}
					}
				}
				else
				{
					vsAcct.TextMatrix(vsAcct.Row, lngColDesc4, "Undefined");
					vsAcct.TextMatrix(vsAcct.Row, lngColDesc3, "Undefined");
					vsAcct.TextMatrix(vsAcct.Row, lngColDesc2, "Undefined");
					vsAcct.TextMatrix(vsAcct.Row, lngColDesc1, "Undefined");
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Get Ledger", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
		
		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			vsAcct_DblClick(sender, e);
		}

		private void vsAcct_ClickEvent(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				vsAcct.Select(vsAcct.Row, 0, vsAcct.Row, vsAcct.Cols - 1);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "vsAcct_Click", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void vsAcct_DblClick(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strLabel;
				strLabel = vsAcct.TextMatrix(vsAcct.Row, lngColAccount);
				modGlobalConstants.Statics.gstrPassAccountFromForm = strLabel;
				strAccountNumber = strLabel;
				Close();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "vsAcct_DBLClick", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void vsAcct_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int counter;
				if (KeyCode == Keys.Up)
				{
					if (vsAcct.Row == 1)
					{
						KeyCode = 0;
					}
				}
				else if (KeyCode == Keys.Down)
				{
					if (vsAcct.Row == vsAcct.Rows - 1)
					{
						KeyCode = 0;
					}
				}
				else if (KeyCode == Keys.E)
				{
					KeyCode = 0;
					vsAcct.Row = 1;
					vsAcct.TopRow = 1;
				}
				else if (KeyCode == Keys.G)
				{
					KeyCode = 0;
					for (counter = 1; counter <= vsAcct.Rows - 1; counter++)
					{
						if (Strings.Left(vsAcct.TextMatrix(counter, lngColAccount), 1) == "G")
						{
							vsAcct.Row = counter;
							vsAcct.TopRow = counter;
							break;
						}
					}
				}
				else if (KeyCode == Keys.R)
				{
					KeyCode = 0;
					for (counter = 1; counter <= vsAcct.Rows - 1; counter++)
					{
						if (Strings.Left(vsAcct.TextMatrix(counter, lngColAccount), 1) == "R")
						{
							vsAcct.Row = counter;
							vsAcct.TopRow = counter;
							break;
						}
					}
				}
				else if (KeyCode == Keys.P)
				{
					KeyCode = 0;
					for (counter = 1; counter <= vsAcct.Rows - 1; counter++)
					{
						if (Strings.Left(vsAcct.TextMatrix(counter, lngColAccount), 1) == "P")
						{
							vsAcct.Row = counter;
							vsAcct.TopRow = counter;
							break;
						}
					}
				}
				else if (KeyCode == Keys.V)
				{
					KeyCode = 0;
					for (counter = 1; counter <= vsAcct.Rows - 1; counter++)
					{
						if (Strings.Left(vsAcct.TextMatrix(counter, lngColAccount), 1) == "V")
						{
							vsAcct.Row = counter;
							vsAcct.TopRow = counter;
							break;
						}
					}
				}
				else if (KeyCode == Keys.L)
				{
					KeyCode = 0;
					for (counter = 1; counter <= vsAcct.Rows - 1; counter++)
					{
						if (Strings.Left(vsAcct.TextMatrix(counter, lngColAccount), 1) == "L")
						{
							vsAcct.Row = counter;
							vsAcct.TopRow = counter;
							break;
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "vsAcct_KeyDown", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void vsAcct_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			string strLabel = "";
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 13)
			{
				// enter ID
				strLabel = vsAcct.TextMatrix(vsAcct.Row, lngColAccount);
				modGlobalConstants.Statics.gstrPassAccountFromForm = strLabel;
				strAccountNumber = strLabel;
				Close();
			}
		}

		private void FormatGrid()
		{
			int lngWid = 0;
			vsAcct.Cols = 6;
			lngWid = vsAcct.WidthOriginal;
			vsAcct.ColWidth(lngColAccount, FCConvert.ToInt32(lngWid * 0.240));
			vsAcct.ColWidth(lngColDesc1, FCConvert.ToInt32(lngWid * 0.145));
			vsAcct.ColWidth(lngColDesc2, FCConvert.ToInt32(lngWid * 0.145));
			vsAcct.ColWidth(lngColDesc3, FCConvert.ToInt32(lngWid * 0.145));
			vsAcct.ColWidth(lngColDesc4, FCConvert.ToInt32(lngWid * 0.145));
			vsAcct.ColWidth(lngColDesc5, FCConvert.ToInt32(lngWid * 0.145));
		}

		private void FindMatchingAccount()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will find an account similar to the one that is in the account box already
				int intLen;
				bool boolFound = false;
				int lngRow = 0;
				intLen = strDefaultAccount.Length;
				while (!(intLen <= 0 || boolFound))
				{
					lngRow = vsAcct.FindRow(Strings.Left(strDefaultAccount, intLen), 1, lngColAccount, true, false);
					if (lngRow > 0)
					{
						boolFound = true;
					}
					else
					{
						intLen -= 1;
					}
				}
				if (boolFound)
				{
					vsAcct.Select(lngRow, lngColAccount);
					vsAcct.TopRow = lngRow;
                }
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Default Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			vsAcct_DblClick(sender, e);
		}
	}
}
