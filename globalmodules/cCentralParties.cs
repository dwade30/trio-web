﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;
using fecherFoundation;

namespace Global
{
	public class cCentralParties
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cVersionInfo GetVersion()
		{
			cVersionInfo GetVersion = null;
			try
			{
				// On Error GoTo ErrorHandler
				ClearErrors();
				cVersionInfo tVer = new cVersionInfo();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from dbversion", "CentralParties");
				if (!rsLoad.EndOfFile())
				{
					tVer.Build = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Build"));
					tVer.Major = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Major"));
					tVer.Minor = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Minor"));
					tVer.Revision = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Revision"));
				}
				GetVersion = tVer;
				return GetVersion;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = Information.Err().Description;
				lngLastError = Information.Err().Number;
			}
			return GetVersion;
		}

		public void SetVersion(cVersionInfo nVersion)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (!(nVersion == null))
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from dbversion", "CentralParties");
					if (rsSave.EndOfFile())
					{
						rsSave.AddNew();
					}
					else
					{
						rsSave.Edit();
					}
					rsSave.Set_Fields("Major", nVersion.Major);
					rsSave.Set_Fields("Minor", nVersion.Minor);
					rsSave.Set_Fields("Revision", nVersion.Revision);
					rsSave.Set_Fields("Build", nVersion.Build);
					rsSave.Update();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = Information.Err().Description;
				lngLastError = Information.Err().Number;
			}
		}

		public bool CheckVersion()
		{
			bool CheckVersion = false;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cVersionInfo nVer = new cVersionInfo();
				cVersionInfo tVer = new cVersionInfo();
				cVersionInfo cVer;
				bool boolNeedUpdate;
				cVer = GetVersion();
				if (cVer == null)
				{
					cVer = new cVersionInfo();
				}
				nVer.Major = 1;

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					// If AddPartyAddressPhoneView Then
					if (UpdatePartyAddressPhoneView())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				//Application.DoEvents();
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 2;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					AddPartyMergedFlag();
					nVer.Copy(tVer);
					if (cVer.IsOlder(nVer))
					{
						SetVersion(nVer);
					}
				}
				//Application.DoEvents();
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 3;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					AddPartyAutoMergedFlag();
					nVer.Copy(tVer);
					if (cVer.IsOlder(nVer))
					{
						SetVersion(nVer);
					}
				}

				//Application.DoEvents();
				// kk01062015 trouts-119  Change module specific comments to use 2 char module id instead of module description/name
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 5;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					UpdateCommentProgModule();
					nVer.Copy(tVer);
					if (cVer.IsOlder(nVer))
					{
						SetVersion(nVer);
					}
				}
				//Application.DoEvents();
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 6;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					// the views added extra spaces to the names when doing last, first
					if (UpdatePartyAddressPhoneView() && UpdatePartyAddressView())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				//Application.DoEvents();
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 7;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (UpdatePartyAddressView())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(nVer);
						}
					}
					else
					{
						return false;
					}
				}

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 9;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (UpdatePartyNameView())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                CheckVersion = true;
				return CheckVersion;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = Information.Err().Description;
				lngLastError = Information.Err().Number;
			}
			return CheckVersion;
		}

		private bool UpdateCommentProgModule()
		{
            clsDRWrapper rsUpdate = new clsDRWrapper();
			string strSQL = "";
			string strModName = "";
			string strModID = "";
			int counter;
			// kk01062015 trouts-119  Change module specific comments to use 2 char module id instead of module description/name
			for (counter = 1; counter <= modCentralParties.gintNumberOfModuleTypes; counter++)
			{
				strModName = modCentralParties.GetModuleDescription(counter);
				strModID = modCentralParties.GetModuleCode(counter);
				strSQL = "UPDATE CentralComments SET ProgModule = '" + strModID + "' WHERE ProgModule = '" + strModName + "'";
				rsUpdate.Execute(strSQL, "CentralParties");
			}
			return true;
		}

		private bool UpdatePartyNameView()
		{
			
			clsDRWrapper rsUpdate = new clsDRWrapper();
			string strSQL = "";
			// kk12192014 trouts-115  Redefine the PartyNameView without using Parties.*
			rsUpdate.OpenRecordset("SELECT Table_Name FROM INFORMATION_SCHEMA.VIEWS WHERE Table_Name = 'PartyNameView' ", "CentralParties");
            if (rsUpdate.RecordCount() != 0)
            {
                strSQL = "DROP VIEW [dbo].[PartyNameView]";
                rsUpdate.Execute(strSQL, "CentralParties");
            }

            strSQL = "CREATE VIEW [dbo].[PartyNameView] AS " + "SELECT TOP (100) PERCENT ID, PartyGuid, PartyType, FirstName, MiddleName, LastName, Designation, Email, WebAddress, DateCreated, CreatedBy, " + "RTRIM(RTRIM(FirstName + ' ' + ISNULL(MiddleName, '')) + ' ' + RTRIM(ISNULL(LastName, '') + ' ' + ISNULL(Designation, ''))) AS FullName, " + "RTRIM(CASE WHEN ISNULL(LastName, '') = '' THEN '' ELSE RTRIM(LastName) + ', ' END + RTRIM(FirstName + ' ' + ISNULL(MiddleName, '')) + ' ' + ISNULL(Designation, '')) AS FullNameLF, " + "RTRIM(RTRIM(FirstName + ' ' + Left(MiddleName + ' ',1)) + ' ' + RTRIM(ISNULL(LastName, '') + ' ' + ISNULL(Designation, ''))) AS FullNameMiddleInitial, " + "RTRIM(CASE WHEN ISNULL(LastName, '') = '' THEN '' ELSE RTRIM(LastName) + ', ' END + RTRIM(FirstName + ' ' + Left(MiddleName + ' ',1)) + ' ' + ISNULL(Designation, '')) AS FullNameLFMiddleInitial " + "From Parties ORDER BY FullName";
				rsUpdate.Execute(strSQL, "CentralParties");
			
			return true;
		}

		private bool AddPartyAutoMergedFlag()
		{
			
			clsDRWrapper rsUpdate = new clsDRWrapper();
			rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Parties' AND COLUMN_NAME = 'AutoMerged'", "CentralParties");
			if (rsUpdate.RecordCount() == 0)
			{
				rsUpdate.Execute("ALTER TABLE Parties ADD AutoMerged bit NULL", "CentralParties");
			}
			return true;
		}

		private bool AddPartyMergedFlag()
		{
            clsDRWrapper rsUpdate = new clsDRWrapper();
			rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Parties' AND COLUMN_NAME = 'Merged'", "CentralParties");
			if (rsUpdate.RecordCount() == 0)
			{
				rsUpdate.Execute("ALTER TABLE Parties ADD Merged bit NULL", "CentralParties");
			}
			return true;
		}

		private bool AddPartyAddressPhoneView()
		{
			bool AddPartyAddressPhoneView = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL;
				string strprefix;
				strprefix = rsSave.CurrentPrefix;
				strSQL = "CREATE VIEW [PartyAddressAndPhoneView]" + "\r\n";
				strSQL += "AS " + "\r\n";
				strSQL += "SELECT     TOP (100) PERCENT Parties.ID, Parties.PartyGuid, Parties.PartyType, Parties.FirstName, Parties.MiddleName, Parties.LastName, " + "\r\n";
				strSQL += "                      Parties.Designation, Parties.Email, Parties.WebAddress, Parties.DateCreated, Parties.CreatedBy, Addresses.ID AS Expr1, " + "\r\n";
				strSQL += "                      Addresses.PartyID, Addresses.Address1, Addresses.Address2, Addresses.Address3, Addresses.City, Addresses.State, Addresses.Zip, " + "\r\n";
				strSQL += "                      Addresses.Country, Addresses.OverrideName, Addresses.AddressType, Addresses.Seasonal, Addresses.ProgModule, Addresses.Comment, " + "\r\n";
				strSQL += "                      Addresses.StartMonth, Addresses.StartDay, Addresses.EndMonth, Addresses.EndDay, Addresses.ModAccountID, " + "\r\n";
				strSQL += "                      RTRIM(RTRIM(Parties.FirstName + ' ' + ISNULL(Parties.MiddleName, '')) + ' ' + RTRIM(ISNULL(Parties.LastName, '') " + "\r\n";
				strSQL += "                      + ' ' + ISNULL(Parties.Designation, ''))) AS FullName, RTRIM(RTRIM(LTRIM(ISNULL(NULLIF (Parties.LastName + ',', ','), '') " + "\r\n";
				strSQL += "                      + ' ' + ISNULL(Parties.FirstName, '')) + ' ' + ISNULL(Parties.MiddleName, '')) + ' ' + ISNULL(Parties.Designation, '')) AS FullNameLF" + "\r\n";
				strSQL += "                      , CentralPhoneNumbers.PhoneNumber,CentralPhoneNumbers.Extension" + "\r\n";
				strSQL += "FROM         Parties left outer join" + "\r\n";
				strSQL += "CentralPhoneNumbers on parties.id = CentralPhoneNumbers.PartyID" + "\r\n";
				strSQL += " LEFT OUTER JOIN" + "\r\n";
				strSQL += "                      Addresses ON Parties.ID = Addresses.PartyID" + "\r\n";
				strSQL += "WHERE     ((Addresses.AddressType = 'Primary') OR" + "\r\n";
				strSQL += "                      (ISNULL(Addresses.PartyID, - 1) = - 1))" + "\r\n";
				strSQL += "           and" + "\r\n";
				strSQL += "           (CentralPhoneNumbers.PhoneOrder = 1 or isnull(CentralPhoneNumbers.ID,-1) = -1)" + "\r\n";
				strSQL += "           " + "\r\n";
				strSQL += "ORDER BY FullName";
				rsSave.Execute(strSQL, "CentralParties");
				AddPartyAddressPhoneView = true;
				return AddPartyAddressPhoneView;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddPartyAddressPhoneView;
		}

		private bool UpdatePartyAddressPhoneView()
		{
			bool UpdatePartyAddressPhoneView = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL;
				string strprefix;
				strprefix = rsSave.CurrentPrefix;
				rsSave.OpenRecordset("select * from sys.views where name = 'PartyAddressAndPhoneView'", "CentralParties");
				if (!rsSave.EndOfFile())
				{
					rsSave.Execute("drop view [dbo].[PartyAddressAndPhoneView]", "CentralParties");
				}
				strSQL = "CREATE VIEW [PartyAddressAndPhoneView]" + "\r\n";
				strSQL += "AS " + "\r\n";
				strSQL += "SELECT     TOP (100) PERCENT Parties.ID, Parties.PartyGuid, Parties.PartyType, Parties.FirstName, Parties.MiddleName, Parties.LastName, " + "\r\n";
				strSQL += "                      Parties.Designation, Parties.Email, Parties.WebAddress, Parties.DateCreated, Parties.CreatedBy, Addresses.ID AS Expr1, " + "\r\n";
				strSQL += "                      Addresses.PartyID, Addresses.Address1, Addresses.Address2, Addresses.Address3, Addresses.City, Addresses.State, Addresses.Zip, " + "\r\n";
				strSQL += "                      Addresses.Country, Addresses.OverrideName, Addresses.AddressType, Addresses.Seasonal, Addresses.ProgModule, Addresses.Comment, " + "\r\n";
				strSQL += "                      Addresses.StartMonth, Addresses.StartDay, Addresses.EndMonth, Addresses.EndDay, Addresses.ModAccountID, " + "\r\n";
				strSQL += "                      RTRIM(RTRIM(Parties.FirstName + ' ' + ISNULL(Parties.MiddleName, '')) + ' ' + RTRIM(ISNULL(Parties.LastName, '') " + "\r\n";
				strSQL += "                      + ' ' + ISNULL(Parties.Designation, ''))) AS FullName, RTRIM(RTRIM(LTRIM(ISNULL(NULLIF (Parties.LastName + ',', ','), '') " + "\r\n";
				strSQL += "                      + ' ' + ISNULL(Parties.FirstName, '')) + ' ' + ISNULL(Parties.MiddleName, '')) + ' ' + ISNULL(Parties.Designation, '')) AS FullNameLF" + "\r\n";
				strSQL += "                      , CentralPhoneNumbers.PhoneNumber,CentralPhoneNumbers.Extension" + "\r\n";
				strSQL += "FROM         Parties left outer join" + "\r\n";
				strSQL += "CentralPhoneNumbers on parties.id = CentralPhoneNumbers.PartyID" + "\r\n";
				strSQL += " LEFT OUTER JOIN" + "\r\n";
				strSQL += "                      Addresses ON Parties.ID = Addresses.PartyID" + "\r\n";
				strSQL += "WHERE     ((Addresses.AddressType = 'Primary') OR" + "\r\n";
				strSQL += "                      (ISNULL(Addresses.PartyID, - 1) = - 1))" + "\r\n";
				strSQL += "           and" + "\r\n";
				strSQL += "           (CentralPhoneNumbers.PhoneOrder = 1 or isnull(CentralPhoneNumbers.ID,-1) = -1)" + "\r\n";
				strSQL += "           " + "\r\n";
				strSQL += "ORDER BY FullName";
				rsSave.Execute(strSQL, "CentralParties");
				UpdatePartyAddressPhoneView = true;
				return UpdatePartyAddressPhoneView;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return UpdatePartyAddressPhoneView;
		}

		private bool UpdatePartyAddressView()
		{
            try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSQL;
				string strprefix;
				strprefix = rsSave.CurrentPrefix;
				rsSave.OpenRecordset("select * from sys.views where name = 'PartyAndAddressView'", "CentralParties");
				if (!rsSave.EndOfFile())
				{
					rsSave.Execute("drop view [PartyAndAddressView]", "CentralParties");
				}
				strSQL = "CREATE VIEW [PartyAndAddressView]" + "\r\n";
				strSQL += "AS " + "\r\n";
				strSQL += "SELECT     TOP (100) PERCENT dbo.Parties.ID, dbo.Parties.PartyGuid, dbo.Parties.PartyType, dbo.Parties.FirstName, dbo.Parties.MiddleName, dbo.Parties.LastName," + "\r\n";
				strSQL += "dbo.Parties.Designation, dbo.Parties.Email, dbo.Parties.WebAddress, dbo.Parties.DateCreated, dbo.Parties.CreatedBy, dbo.Addresses.ID AS Expr1," + "\r\n";
				strSQL += "dbo.Addresses.PartyID, dbo.Addresses.Address1, dbo.Addresses.Address2, dbo.Addresses.Address3, dbo.Addresses.City, dbo.Addresses.State, dbo.Addresses.Zip," + "\r\n";
				strSQL += "dbo.Addresses.Country, dbo.Addresses.OverrideName, dbo.Addresses.AddressType, dbo.Addresses.Seasonal, dbo.Addresses.ProgModule, dbo.Addresses.Comment," + "\r\n";
				strSQL += "dbo.Addresses.StartMonth, dbo.Addresses.StartDay, dbo.Addresses.EndMonth, dbo.Addresses.EndDay, dbo.Addresses.ModAccountID," + "\r\n";
				strSQL += "RTRIM(RTRIM(dbo.Parties.FirstName + ' ' + ISNULL(dbo.Parties.MiddleName, '')) + ' ' + RTRIM(ISNULL(dbo.Parties.LastName, '')" + "\r\n";
				strSQL += " + ' ' + ISNULL(dbo.Parties.Designation, ''))) AS FullName," + "\r\n";
				strSQL += "RTRIM(RTRIM(LTRIM(ISNULL(NULLIF(dbo.Parties.LastName + ',',', '),'') + ' ' + ISNULL(dbo.Parties.FirstName,'')) + ' ' + ISNULL(dbo.Parties.MiddleName, '')) + ' ' + ISNULL(dbo.Parties.Designation, '')) as FullNameLF" + "\r\n";
				strSQL += "FROM         dbo.Parties left JOIN" + "\r\n";
				strSQL += "dbo.Addresses ON dbo.Parties.ID = dbo.Addresses.PartyID" + "\r\n";
				strSQL += "WHERE     (dbo.Addresses.AddressType = 'Primary') or (ISNULL(dbo.addresses.PartyID,-1) = -1)" + "\r\n";
				strSQL += "ORDER BY FullName";
				rsSave.Execute(strSQL, "CentralParties");
				
				return true;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return false;
		}
	}
}
