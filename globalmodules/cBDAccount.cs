﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace Global
{
	public class cBDAccount
	{
		//=========================================================
		private int lngRecordID;
		private string strAccount = string.Empty;
		private string strAccountType = string.Empty;
		private string strGLAccountType = string.Empty;
		private bool boolValid;
		private double dblCurrentBudget;
		private string strFirstAccountField = string.Empty;
		private string strSecondAccountField = string.Empty;
		private string strThirdAccountField = string.Empty;
		private string strFourthAccountField = string.Empty;
		private string strFifthAccountField = string.Empty;
		private string strDescription = string.Empty;
		private string strShortDescription = string.Empty;
		private bool boolUpdated;
		private bool boolDeleted;

		public string AccountType
		{
			set
			{
				strAccountType = value;
				IsUpdated = true;
			}
			get
			{
				string AccountType = "";
				AccountType = strAccountType;
				return AccountType;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}

		public string FifthAccountField
		{
			set
			{
				strFifthAccountField = value;
				IsUpdated = true;
			}
			get
			{
				string FifthAccountField = "";
				FifthAccountField = strFifthAccountField;
				return FifthAccountField;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
				IsUpdated = true;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public string GLAccountType
		{
			set
			{
				strGLAccountType = value;
				IsUpdated = true;
			}
			get
			{
				string GLAccountType = "";
				GLAccountType = strGLAccountType;
				return GLAccountType;
			}
		}

		public bool IsValidAccount
		{
			set
			{
				boolValid = value;
				IsUpdated = true;
			}
			get
			{
				bool IsValidAccount = false;
				IsValidAccount = boolValid;
				return IsValidAccount;
			}
		}

		public double CurrentBudget
		{
			set
			{
				dblCurrentBudget = value;
				IsUpdated = true;
			}
			get
			{
				double CurrentBudget = 0;
				CurrentBudget = dblCurrentBudget;
				return CurrentBudget;
			}
		}

		public string FirstAccountField
		{
			set
			{
				strFirstAccountField = value;
				IsUpdated = true;
			}
			get
			{
				string FirstAccountField = "";
				FirstAccountField = strFirstAccountField;
				return FirstAccountField;
			}
		}

		public string SecondAccountField
		{
			set
			{
				strSecondAccountField = value;
				IsUpdated = true;
			}
			get
			{
				string SecondAccountField = "";
				SecondAccountField = strSecondAccountField;
				return SecondAccountField;
			}
		}

		public string ThirdAccountField
		{
			set
			{
				strThirdAccountField = value;
				IsUpdated = true;
			}
			get
			{
				string ThirdAccountField = "";
				ThirdAccountField = strThirdAccountField;
				return ThirdAccountField;
			}
		}

		public string FourthAccountField
		{
			set
			{
				strFourthAccountField = value;
				IsUpdated = true;
			}
			get
			{
				string FourthAccountField = "";
				FourthAccountField = strFourthAccountField;
				return FourthAccountField;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public bool IsExpense()
		{
			bool IsExpense = false;
			IsExpense = Strings.LCase(strAccountType) == "e";
			return IsExpense;
		}

		public bool IsRevenue()
		{
			bool IsRevenue = false;
			IsRevenue = Strings.LCase(strAccountType) == "r";
			return IsRevenue;
		}

		public bool IsLedger()
		{
			bool IsLedger = false;
			IsLedger = Strings.LCase(strAccountType) == "g";
			return IsLedger;
		}

		public bool IsAsset()
		{
			bool IsAsset = false;
			IsAsset = (Strings.LCase(GLAccountType) == "1" || Strings.LCase(GLAccountType) == "a");
			return IsAsset;
		}

		public bool IsLiability()
		{
			bool IsLiability = false;
			IsLiability = (Strings.LCase(GLAccountType) == "2" || Strings.LCase(GLAccountType) == "l");
			return IsLiability;
		}

		public bool IsFundBalance()
		{
			bool IsFundBalance = false;
			IsFundBalance = (Strings.LCase(GLAccountType) == "3" || Strings.LCase(GLAccountType) == "f");
			return IsFundBalance;
		}
	}
}
