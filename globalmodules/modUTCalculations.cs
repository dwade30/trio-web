﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary.Data;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWBD0000
using TWBD0000;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;
using modStatusPayments = TWUT0000.modUTUseCR;
#endif
namespace Global
{
	public class modUTCalculations
	{
		// kk02082016 trout-1197 Allow user to adjust side margins
		public static void UTSetup()
		{
			int intError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper WWK = new clsDRWrapper();
				clsDRWrapper WUT = new clsDRWrapper();
				clsDRWrapper Modules = new clsDRWrapper();
				string strTemp = "";
				intError = 2;

				WWK.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
				if (!WWK.EndOfFile())
				{
					intError = 4;
					modGlobal.Statics.gintBasis = WWK.Get_Fields_Int32("Basis");
					intError = 5;
					modExtraModules.Statics.dblDefaultInterestRate = Conversion.Val(WWK.Get_Fields_Double("TaxRate"));
					intError = 6;
					modExtraModules.Statics.dblOverPayRate = Conversion.Val(WWK.Get_Fields_Double("OverPayInterestRate"));
					// kk06042015 trout-1166  / 100
					intError = 16;
					intError = 19;
					if (FCUtils.IsNull(WWK.Get_Fields_String("Service")) == false)
					{
						modUTFunctions.Statics.TownService = FCConvert.ToString(WWK.Get_Fields_String("Service"));
						// water/sewer/both
					}
					else
					{
						modUTFunctions.Statics.TownService = "B";
					}
					if (Conversion.Val(WWK.Get_Fields_Int32("AutoPayOption")) == 0)
					{
						modUTFunctions.Statics.gboolAutoPayOldestBillFirst = true;
					}
					else
					{
						modUTFunctions.Statics.gboolAutoPayOldestBillFirst = false;
					}
					modUTFunctions.Statics.gboolPayWaterFirst = WWK.Get_Fields_Boolean("PayWaterFirst");
					intError = 20;
					modUTFunctions.Statics.gboolShowLastUTAccountInCR = FCConvert.CBool(WWK.Get_Fields_Boolean("ShowLastUTAccountInCR"));
					Statics.gboolDefaultUTRTDToAutoChange = WWK.Get_Fields_Boolean("DefaultUTRTDToAutoChange");
					intError = 21;
					modUTFunctions.Statics.gdblUTMuniTaxRate = Conversion.Val(WWK.Get_Fields_Double("TaxRate"));
					// get the town's tax rate
					intError = 22;
					if (FCConvert.ToString(WWK.Get_Fields_String("RegularIntMethod")) != "")
					{
						intError = 23;
						modExtraModules.Statics.gstrRegularIntMethod = FCConvert.ToString(WWK.Get_Fields_String("RegularIntMethod"));
						if (modExtraModules.Statics.gstrRegularIntMethod == "F")
						{
							intError = 24;
							modStatusPayments.Statics.gdblFlatInterestAmount = Conversion.Val(WWK.Get_Fields_Decimal("FlatInterestAmount"));
						}
					}
					else
					{
						modExtraModules.Statics.gstrRegularIntMethod = "P";
					}
					intError = 25;
					modExtraModules.Statics.gboolShowLienAmountOnBill = FCConvert.ToBoolean(WWK.Get_Fields_Boolean("ShowLienAmountOnBill"));
					intError = 26;
					Statics.gboolSplitBills = FCConvert.ToBoolean(WWK.Get_Fields_Boolean("SplitBilling"));
					intError = 27;
					if (Conversion.Val(WWK.Get_Fields_Int32("ReadingUnits")) > 0)
					{
						modExtraModules.Statics.glngTownReadingUnits = FCConvert.ToInt32(WWK.Get_Fields_Int32("ReadingUnits"));
					}
					else
					{
						modExtraModules.Statics.glngTownReadingUnits = 1;
					}
					intError = 28;
					Statics.gboolUseBookForLien = FCConvert.ToBoolean(WWK.Get_Fields_Boolean("UseBookForLien"));
					// TODO: Check the table for the column [AdjustmentLabelsDMH] and replace with corresponding Get_Field method
					if (WWK.Get_Fields_Double("AdjustmentLabelsDMH") != -100)
					{
						// TODO: Check the table for the column [AdjustmentLabelsDMH] and replace with corresponding Get_Field method
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMH", FCConvert.ToString(WWK.Get_Fields("AdjustmentLabelsDMH")));
						// TODO: Check the table for the column [AdjustmentLabelsDMV] and replace with corresponding Get_Field method
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMV", FCConvert.ToString(WWK.Get_Fields("AdjustmentLabelsDMV")));
						// TODO: Check the table for the column [AdjustmentLabelsDMH] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [AdjustmentLabelsDMV] and replace with corresponding Get_Field method
						modGlobalFunctions.AddCYAEntry_80("UT", "Setup registry entries for laser adjustments.", "DMH : " + FCConvert.ToString(WWK.Get_Fields("AdjustmentLabelsDMH")), "DMV : " + FCConvert.ToString(WWK.Get_Fields("AdjustmentLabelsDMV")));
						WWK.Edit();
						WWK.Set_Fields("AdjustmentLabelsDMH", -100);
						WWK.Set_Fields("AdjustmentLabelsDMV", -100);
						WWK.Update();
						WWK.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
						// gdblLabelsAdjustment = WWK.Get_Fields("AdjustmentLabels")
						// TODO: Check the table for the column [AdjustmentLabelsDMH] and replace with corresponding Get_Field method
						modGlobal.Statics.gdblLabelsAdjustmentDMH = Conversion.Val(WWK.Get_Fields("AdjustmentLabelsDMH"));
						// TODO: Check the table for the column [AdjustmentLabelsDMV] and replace with corresponding Get_Field method
						modGlobal.Statics.gdblLabelsAdjustmentDMV = Conversion.Val(WWK.Get_Fields("AdjustmentLabelsDMV"));
					}
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMH", ref strTemp);
					if (Conversion.Val(strTemp) == 0)
					{
						intError = 81;
						modGlobal.Statics.gdblLabelsAdjustmentDMH = 0;
					}
					else
					{
						intError = 92;
						modGlobal.Statics.gdblLabelsAdjustmentDMH = FCConvert.ToDouble(strTemp);
					}
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMV", ref strTemp);
					if (Conversion.Val(strTemp) == 0)
					{
						intError = 83;
						modGlobal.Statics.gdblLabelsAdjustmentDMV = 0;
					}
					else
					{
						intError = 94;
						modGlobal.Statics.gdblLabelsAdjustmentDMV = FCConvert.ToDouble(strTemp);
					}
					// TODO: Check the table for the column [AdjustmentCMFLaser] and replace with corresponding Get_Field method
					if (WWK.Get_Fields_Double("AdjustmentCMFLaser") != -100)
					{
						// TODO: Check the table for the column [AdjustmentCMFLaser] and replace with corresponding Get_Field method
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentCMFLaser", FCConvert.ToString(WWK.Get_Fields("AdjustmentCMFLaser")));
						// TODO: Check the table for the column [AdjustmentLabels] and replace with corresponding Get_Field method
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabels", FCConvert.ToString(WWK.Get_Fields("AdjustmentLabels")));
						// TODO: Check the table for the column [AdjustmentCMFLaser] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [AdjustmentLabels] and replace with corresponding Get_Field method
						modGlobalFunctions.AddCYAEntry_80("UT", "Setup registry entries for laser adjustments.", "CMF : " + FCConvert.ToString(WWK.Get_Fields("AdjustmentCMFLaser")), "Labels : " + FCConvert.ToString(WWK.Get_Fields("AdjustmentLabels")));
						WWK.Edit();
						WWK.Set_Fields("AdjustmentCMFLaser", -100);
						WWK.Set_Fields("AdjustmentLabels", -100);
						WWK.Update();
						WWK.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
					}
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentCMFLaser", ref strTemp);
					if (Conversion.Val(strTemp) == 0)
					{
						intError = 8;
						modGlobal.Statics.gdblCMFAdjustment = 0;
					}
					else
					{
						intError = 9;
						modGlobal.Statics.gdblCMFAdjustment = FCConvert.ToDouble(strTemp);
					}
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabels", ref strTemp);
					if (Conversion.Val(strTemp) == 0)
					{
						intError = 10;
						modGlobal.Statics.gdblLabelsAdjustment = 0;
					}
					else
					{
						intError = 11;
						modGlobal.Statics.gdblLabelsAdjustment = FCConvert.ToDouble(strTemp);
					}
					// kk05082014  trocl-1156/trout-1082   Add the Lien and LDN top and bottom margin adjustments
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLienAdjustmentTop", ref strTemp);
					if (Conversion.Val(strTemp) != 0)
					{
						modGlobal.Statics.gdblLienAdjustmentTop = FCConvert.ToDouble(strTemp);
					}
					else
					{
						modGlobal.Statics.gdblLienAdjustmentTop = 0;
					}
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLienAdjustmentBottom", ref strTemp);
					if (Conversion.Val(strTemp) != 0)
					{
						modGlobal.Statics.gdblLienAdjustmentBottom = FCConvert.ToDouble(strTemp);
					}
					else
					{
						modGlobal.Statics.gdblLienAdjustmentBottom = 0;
					}
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT", "gdblLienAdjustmentSide", ref strTemp);
					// kk02082016 trout-1197  Added side margin adjustment and removed the top right box
					if (Conversion.Val(strTemp) != 0)
					{
						modGlobal.Statics.gdblLienAdjustmentSide = FCConvert.ToDouble(strTemp);
					}
					else
					{
						modGlobal.Statics.gdblLienAdjustmentSide = 0;
					}
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLDNAdjustmentTop", ref strTemp);
					if (Conversion.Val(strTemp) != 0)
					{
						Statics.gdblUTLDNAdjustmentTop = FCConvert.ToDouble(strTemp);
						// kk06122014 trout-10825  Conflicting Globals in CR  - gdblLDNAdjustmentTop = CDbl(strTemp)
					}
					else
					{
						Statics.gdblUTLDNAdjustmentTop = 0;
					}
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLDNAdjustmentBottom", ref strTemp);
					if (Conversion.Val(strTemp) != 0)
					{
						Statics.gdblUTLDNAdjustmentBottom = FCConvert.ToDouble(strTemp);
						// kk06122014 trout-10825  Conflicting Globals in CR  - gdblLDNAdjustmentBottom = CDbl(strTemp)
					}
					else
					{
						Statics.gdblUTLDNAdjustmentBottom = 0;
					}
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT", "gdblLDNAdjustmentSide", ref strTemp);
					// kk02082016 trout-1197  Added side margin adjustment
					if (Conversion.Val(strTemp) != 0)
					{
						Statics.gdblUTLDNAdjustmentSide = FCConvert.ToDouble(strTemp);
					}
					else
					{
						Statics.gdblUTLDNAdjustmentSide = 0;
					}
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "SignatureAdjustLien", ref strTemp);
					// kk02012016 trout-1197  Digital signature Vertical Adjustment never worked
					if (Conversion.Val(strTemp) != 0)
					{
						modGlobal.Statics.gdblLienSigAdjust = FCConvert.ToDouble(strTemp);
					}
					else
					{
						modGlobal.Statics.gdblLienSigAdjust = 0;
					}
					modGlobalConstants.Statics.gblnAutoPrepayments = WWK.Get_Fields_Boolean("AutoPrepayments");
					if (WWK.IsFieldNull("AutoPaymentApplyTo"))
					{
						WWK.Edit();
						WWK.Set_Fields("AutoPaymentApplyTo", 0);
						WWK.Update();
						modGlobalConstants.Statics.gintApplyPrepayTo = 0;
					}
					else
					{
						modGlobalConstants.Statics.gintApplyPrepayTo = FCConvert.ToInt16(Conversion.Val(WWK.Get_Fields_Int32("AutoPaymentApplyTo")));
					}
					modGlobalConstants.Statics.gblnShowTaxAcquiredCaption = FCConvert.ToBoolean(WWK.Get_Fields_Boolean("ShowTaxAcquiredCaption"));
					modGlobalConstants.Statics.gboolIC = FCConvert.CBool(Conversion.Val(WWK.Get_Fields_Int32("IConnectUtilityID")) > 0);
					// kgk 02-28-2012  Moved from UT to GN database        'kgk trout-733 10-13-2011  Add Invoice Cloud
					// If WWK.DoesFieldExist("InvCldBillerGUID", "UtilityBilling", strUTDatabase) Then
					// gboolInvCld = CBool(WWK.Get_Fields("InvCldBillerGUID") <> "")
					// Else
					// gboolInvCld = False
					// End If
					modGlobalConstants.Statics.gblnAutoUpdateRETaxAcq = FCConvert.ToBoolean(WWK.Get_Fields_Boolean("UpdateRETaxAcquired"));
					modGlobalConstants.Statics.gblnCheckRETaxAcq = FCConvert.ToBoolean(WWK.Get_Fields_Boolean("CheckRETaxAcquired"));
					intError = 30;
				}
				else
				{
					modUTFunctions.Statics.TownService = "B";
					intError = 35;
					WWK.AddNew();
					WWK.Set_Fields("Service", "B");
					intError = 36;
					WWK.Set_Fields("RegularIntMethod", "P");
					modExtraModules.Statics.glngTownReadingUnits = 1;
					WWK.Update();
				}
				intError = 40;
				if (modUTFunctions.Statics.TownService == "S")
				{
					Statics.gboolShowOwner = FCConvert.ToBoolean(WWK.Get_Fields_Boolean("SBillToOwner"));
				}
				else if (modUTFunctions.Statics.TownService == "W")
				{
					Statics.gboolShowOwner = FCConvert.ToBoolean(WWK.Get_Fields_Boolean("WBillToOwner"));
				}
				else
				{
					Statics.gboolShowOwner = true;
				}
				intError = 45;
				// this will get the paths and passwords for the sig files
				// SetupSigInformation
				intError = 50;
                var utsCont = new cUTSettingsController();
                Statics.gUTSettings = utsCont.LoadSettings();
				return;
			}
			catch (Exception ex)
			{
				
				// Unload frmWait
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Utilities Setup Error - " + FCConvert.ToString(intError));
			}
		}

		public static double CalculateAccountUT(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXtraInt, bool boolWater)
		{
			double dblCurPrin = 0;
			double dblCurInt = 0;
			double dblCurCost = 0;
			bool boolShowError = false;
			bool boolUseTableName = false;
			DateTime dtLastInterestDate = DateTime.Now;
			double dblCurTax = 0;
			double dblPerDiem = 0;
			bool boolForcePerDiem = false;
			bool blnForceBillIntDate = false;
			return CalculateAccountUT(rsTemp, ref dtDate, ref dblXtraInt, boolWater, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, dtLastInterestDate, ref dblCurTax, ref dblPerDiem, ref boolForcePerDiem, ref blnForceBillIntDate);
		}

		public static double CalculateAccountUT(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXtraInt, bool boolWater,ref  double dblCurPrin,ref  double dblCurInt, ref double dblCurCost, bool boolShowError, bool boolUseTableName,ref DateTime dtLastInterestDate, ref double dblCurTax)
		{
			double dblPerDiem = 0;
			bool boolForcePerDiem = false;
			bool blnForceBillIntDate = false;
			return CalculateAccountUT(rsTemp, ref dtDate, ref dblXtraInt, boolWater, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, dtLastInterestDate, ref dblCurTax, ref dblPerDiem, ref boolForcePerDiem, ref blnForceBillIntDate);
		}

		internal static double CalculateAccountUT(clsDRWrapper rsTemp, ref DateTime dtDate, ref double dblXtraInt, bool boolWater, ref DateTime dtLastInterestDate, ref double dblPerDiem)
		{
			double dblCurPrin = 0;
			double dblCurInt = 0;
			double dblCurCost = 0;
			bool boolShowError = false;
			bool boolUseTableName = false;
			double dblCurTax = 0;
			bool boolForcePerDiem = false;
			bool blnForceBillIntDate = false;
			return CalculateAccountUT(rsTemp, ref dtDate, ref dblXtraInt, boolWater, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, dtLastInterestDate, ref dblCurTax, ref dblPerDiem, ref boolForcePerDiem, ref blnForceBillIntDate);
		}

		public static double CalculateAccountUT(clsDRWrapper rsTemp, ref DateTime dtDate, ref double dblXtraInt, bool boolWater, ref double dblCurPrin)
		{
			double dblCurInt = 0;
			double dblCurCost = 0;
			bool boolShowError = false;
			bool boolUseTableName = false;
			DateTime dtLastInterestDate = DateTime.Now;
			double dblCurTax = 0;
			double dblPerDiem = 0;
			bool boolForcePerDiem = false;
			bool blnForceBillIntDate = false;
			return CalculateAccountUT(rsTemp, ref dtDate, ref dblXtraInt, boolWater, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, dtLastInterestDate, ref dblCurTax, ref dblPerDiem, ref boolForcePerDiem, ref blnForceBillIntDate);
		}

		public static double CalculateAccountUT(clsDRWrapper rsTemp, ref DateTime dtDate, ref double dblXtraInt, bool boolWater, ref double dblCurInt, ref double dblCurCost)
		{
			double dblCurPrin = 0;
			bool boolShowError = false;
			bool boolUseTableName = false;
			DateTime dtLastInterestDate = DateTime.Now;
			double dblCurTax = 0;
			double dblPerDiem = 0;
			bool boolForcePerDiem = false;
			bool blnForceBillIntDate = false;
			return CalculateAccountUT(rsTemp, ref dtDate, ref dblXtraInt, boolWater, ref dblCurPrin, ref dblCurInt, ref dblCurCost, boolShowError, boolUseTableName, dtLastInterestDate, ref dblCurTax, ref dblPerDiem, ref boolForcePerDiem, ref blnForceBillIntDate);
		}
		// VBto upgrade warning: dblXtraInt As double	OnWrite(short, double, double)
		// VBto upgrade warning: dblCurInt As double	OnWrite(double, string, short)
		public static double CalculateAccountUT(clsDRWrapper rsTemp, ref DateTime dtDate, ref double dblXtraInt, bool boolWater, ref double dblCurPrin/* = 0 */, ref double dblCurInt/* = 0 */, ref double dblCurCost/* = 0 */, bool boolShowError/*= false*/, bool boolUseTableName/*= false*/, DateTime dtLastInterestDate/*= DateTime.Now*/, ref double dblCurTax/* = 0*/, ref double dblPerDiem/*= 0*/, ref bool boolForcePerDiem/*= false*/, ref bool blnForceBillIntDate/*= false*/)
		{
			double CalculateAccountUT = 0;
			// this will calculate the total amount owed on this person's bill at the payment date
			clsDRWrapper rsCalc = new clsDRWrapper();
			bool boolContinue;
			DateTime dtStartDate;
			DateTime dtDueDate;
			double dblPrinDuePerPeriod = 0;
			int intCT/*unused?*/;
			double dblIntRate = 0;
			DateTime dtIntPaidDate = DateTime.FromOADate(0);
			double dblPrinPaid = 0;
			double dblTaxPaid = 0;
			double dblIntPaid/*unused?*/;
			double dblCostPaid = 0;
			string strWS = "";
			int intDayAdded = 0;
			clsDRWrapper rsBill = new clsDRWrapper();
			if (boolWater)
			{
				strWS = "W";
			}
			else
			{
				strWS = "S";
			}
			boolContinue = true;
			// get the tax rate from the rate rec
			if (rsCalc.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsTemp.Get_Fields_Int32("BillingRateKey"), modExtraModules.strUTDatabase))
			{
				if (rsCalc.EndOfFile() != true && rsCalc.BeginningOfFile() != true)
				{
					if (FCUtils.IsNull(rsCalc.Get_Fields(strWS + "IntRate")) == false)
					{
						dblIntRate = Conversion.Val(rsCalc.Get_Fields(strWS + "IntRate"));
					}
					else
					{
						if (boolShowError)
							FCMessageBox.Show("Error retrieving Tax Rate Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
						boolContinue = false;
					}
				}
				else
				{
					if (boolShowError)
						FCMessageBox.Show("Error retrieving Rate Table Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
					boolContinue = false;
				}
			}
			else
			{
				if (boolShowError)
					FCMessageBox.Show("Error retrieving Rate Table Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
				boolContinue = false;
			}
			if (boolContinue)
			{
				//FC:FINAL:MSH - I.Issue #1002: rsTemp.Get_Fields(strWS + "IntPaidDate") can be equal to an empty string and 
				// will be throwed an exception
				if (rsTemp.Get_Fields_DateTime(strWS + "IntPaidDate").ToOADate() != DateTime.MinValue.ToOADate())
				{
					// kgk 08-06-2012
					dtIntPaidDate = rsTemp.Get_Fields_DateTime(strWS + "IntPaidDate");
				}
				// xxxx Debug.Assert rsTemp.Get_Fields(strWS & "IntPaidDate") <> ""
				if (blnForceBillIntDate)
				{
					rsBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsTemp.Get_Fields_Int32("AccountKey") + " ORDER BY BillDate DESC");
					if (rsBill.EndOfFile() != true && rsBill.BeginningOfFile() != true)
					{
						rsBill.MoveNext();
						if (rsBill.EndOfFile() != true)
						{
							dtIntPaidDate = rsBill.Get_Fields_DateTime("BillDate");
						}
					}
				}
				if (dtIntPaidDate.ToOADate() == 0)
				{
					// if the date has not been set then get the bill creation date from the rate record
					//FC:FINAL:MSH - I.Issue #1002: rsCalc.Get_Fields("IntStart") can be equal to an empty string and 
					// will be throwed an exception
					if (rsCalc.Get_Fields_DateTime("IntStart").ToOADate() != DateTime.MinValue.ToOADate())
					{
						dtIntPaidDate = DateAndTime.DateAdd("D", -1, rsCalc.Get_Fields_DateTime("IntStart"));
					}
				}
				// amount paid
				dblPrinPaid = rsTemp.Get_Fields_Double(strWS + "PrinPaid");
				dblTaxPaid = rsTemp.Get_Fields_Double(strWS + "TaxPaid");
				dblCostPaid = rsTemp.Get_Fields_Double(strWS + "CostPaid");
				dblIntPaid = rsTemp.Get_Fields_Double(strWS + "IntPaid");
				// this is the net principal
				dblCurPrin = rsTemp.Get_Fields_Double(strWS + "PrinOwed") - dblPrinPaid;
				// this is the net tax
				dblCurTax = rsTemp.Get_Fields_Double(strWS + "TaxOwed") - dblTaxPaid;
				// this is the net tax
				dblCurInt = rsTemp.Get_Fields_Double(strWS + "IntOwed");
				// - dblIntPaid
				// this is the net cost
				dblCurCost = rsTemp.Get_Fields_Double(strWS + "CostOwed") - rsTemp.Get_Fields_Double(strWS + "CostAdded") - dblCostPaid;
				// this is the amount to have interest calculated on
				dblPrinDuePerPeriod = dblCurPrin;
				// fill the interest dates
				// MAL@20081204: Remove -1 because IntStart should be calculated from that day
				// Tracker Reference: 16444
				// dtStartDate = DateAdd("D", -1, rsCalc.Get_Fields("IntStart"))
				dtStartDate = rsCalc.Get_Fields_DateTime("IntStart");
				// DJW@20090701: Added intDayAdded to handle adding a day of interest if we are dealing with intStart datae not interest paid through date
				if (dtIntPaidDate.ToOADate() > dtStartDate.ToOADate())
				{
					intDayAdded = 0;
					dtStartDate = dtIntPaidDate;
				}
				else
				{
					intDayAdded = 1;
				}
				// MAL@20080602: Changed to use the Interest Start Date
				// Tracker Reference: 13992
				// dtDueDate = rsCalc.Get_Fields("IntStart")
				// dtDueDate = rsCalc.Get_Fields("BillDate")
				// 
				// If dtIntPaidDate > dtDueDate Then
				// dtDueDate = dtIntPaidDate
				// End If
				// find out how much interest is due
				if (boolUseTableName)
				{
					dblCurInt += rsTemp.Get_Fields_Double(strWS + "IntAdded") * -1;
				}
				else
				{
					dblCurInt += rsTemp.Get_Fields_Double(strWS + "IntAdded") * -1;
				}
				// DJW@01102013 TROUT-845 Was gettign a fraction of a penny on this calculation even though the amoutn charged and paid looked the same
				dblCurInt = FCConvert.ToDouble(Strings.Format(dblCurInt - rsTemp.Get_Fields_Double(strWS + "IntPaid"), "0.00"));
				dblXtraInt = 0;
				// find out current interest
				// If dtDate < dtDueDate And dtDueDate > 0 Then                'this is the base scenerio...payment is before interest starts
				// DJW @ 07/01/2009 Changed Due Date to Start Date
				if (DateAndTime.DateDiff("D", dtDate, dtStartDate) > 0 && dtStartDate.ToOADate() > 0)
				{
					// this is the base scenerio...payment is before interest starts
					dblXtraInt = 0;
					dtLastInterestDate = dtStartDate;
				}
				else
				{
					// If dtDate < dtIntPaidDate Then                          'this is if interest has been charged to the period already
					if (DateAndTime.DateDiff("D", dtDate, dtIntPaidDate) >= 0)
					{
						// this is if interest has been charged to the period already
						dblXtraInt = 0;
						dtLastInterestDate = dtIntPaidDate;
					}
					else
					{
						dtLastInterestDate = dtStartDate;
						if (DateAndTime.DateDiff("D", dtStartDate, dtDate) >= 0)
						{
							// MAL@20081204: Added equal to 0 ; Tracker Reference: 16444
							if (dblPrinDuePerPeriod > 0)
							{
								if (modExtraModules.Statics.gstrRegularIntMethod == "P" || boolForcePerDiem)
								{
									// MAL@20081204: Add check for effective date = Interest Start Date
									// Tracker Reference: 16444
									if (DateAndTime.DateDiff("D", dtStartDate, dtDate) == 0)
									{
										dblXtraInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrinDuePerPeriod), dtStartDate, dtDate, dblIntRate, modGlobal.Statics.gintBasis, intDayAdded, modExtraModules.Statics.dblOverPayRate, ref dblPerDiem));
									}
									else
									{
										dblXtraInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrinDuePerPeriod), dtStartDate, dtDate, dblIntRate, modGlobal.Statics.gintBasis, intDayAdded, modExtraModules.Statics.dblOverPayRate, ref dblPerDiem));
									}
								}
								else if (modExtraModules.Statics.gstrRegularIntMethod == "F")
								{
									if (boolWater)
									{
										if (rsTemp.Get_Fields_Double("WIntAdded") == 0)
										{
											dblXtraInt = modStatusPayments.Statics.gdblFlatInterestAmount;
										}
									}
									else
									{
										if (rsTemp.Get_Fields_Double("SIntAdded") == 0)
										{
											dblXtraInt = modStatusPayments.Statics.gdblFlatInterestAmount;
										}
									}
								}
							}
							else
							{
								dblXtraInt = 0;
							}
						}
					}
				}
			}
			else
			{
				// This is for prepayment bills
				dblPrinPaid = rsTemp.Get_Fields_Double(strWS + "PrinPaid");
				dblCurPrin = rsTemp.Get_Fields_Double(strWS + "PrinOwed") - dblPrinPaid;
			}
			if (boolContinue)
			{
				dblCurInt += dblXtraInt;
				CalculateAccountUT = dblCurPrin + dblCurTax + dblCurInt + dblCurCost;
			}
			else
			{
				// reset values
				// dblCurPrin = 0
				dblCurInt = 0;
				dblCurCost = 0;
				dblCurTax = 0;
				CalculateAccountUT = dblCurPrin + dblCurTax + dblCurInt + dblCurCost;
			}
			rsCalc.Reset();
			return CalculateAccountUT;
		}
		// VBto upgrade warning: dblXtraInt As double	OnWrite(short, double, double)
		// VBto upgrade warning: dblCurPrin As object	OnWrite(double, short)
		// VBto upgrade warning: dblCurInt As object	OnWrite(short, double)
		// VBto upgrade warning: dblCurCost As object	OnWrite(short, double)
		public static double CalculateAccountUTLien(clsDRWrapper rsTemp, DateTime dtDate, ref double dblXtraInt, bool boolWater, double dblCurPrin = 0, double dblCurInt = 0, double dblCurCost = 0, bool boolShowError = false, bool boolUseTableName = false, DateTime? dtLastInterestDate = null, double dblCurTax = 0, double dblPerDiem = 0, double dblCurPLI = 0, bool blnForceBillIntDate = false)
		{
			if (dtLastInterestDate == null)
			{
				dtLastInterestDate = DateTime.FromOADate(0);
			}
			double CalculateAccountUTLien = 0;
			// this will calculate the total amount owed on this person's bill at the payment date
			// this is strictly used for Liened accounts, the recordset passed in should
			// be on the Lien Record that is to be calculated
			// rstemp is the lien record
			// dtdate is the payment date
			// dblXtraInt is the extra interest that is calculated and can be retrieved through this variabl
			clsDRWrapper rsCalc = new clsDRWrapper();
			bool boolContinue;
			DateTime dtStartDate;
			DateTime dtDueDate;
			double dblPrinDuePerPeriod = 0;
			int intCT/*unused?*/;
			double dblIntRate = 0;
			DateTime dtIntPaidDate = DateTime.FromOADate(0);
			double dblPrinPaid = 0;
			double dblXCostPLI = 0;
			string strWS = "";
			if (boolWater)
			{
				strWS = "W";
			}
			else
			{
				strWS = "S";
			}
			boolContinue = true;
			// get the tax rate from the rate rec
			if (rsCalc.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsTemp.Get_Fields_Int32("RateKey"), modExtraModules.strUTDatabase))
			{
				if (rsCalc.EndOfFile() != true && rsCalc.BeginningOfFile() != true)
				{
					if (FCUtils.IsNull(rsCalc.Get_Fields(strWS + "IntRate")) == false)
					{
						dblIntRate = Conversion.Val(rsCalc.Get_Fields(strWS + "IntRate"));
					}
					else
					{
						if (boolShowError)
							FCMessageBox.Show("Error retrieving Tax Rate Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
						boolContinue = false;
					}
				}
				else
				{
					if (boolShowError)
						FCMessageBox.Show("Error retrieving Rate Table Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
					boolContinue = false;
				}
			}
			else
			{
				if (boolShowError)
					FCMessageBox.Show("Error retrieving Rate Table Information.", MsgBoxStyle.OkOnly, "Rate Information Error");
				boolContinue = false;
			}
			if (boolContinue)
			{
				//FC:FINAL:MSH - issue #946: incorrect converting from DateTime to double
				if (rsTemp.Get_Fields_DateTime("IntPaidDate").ToOADate() != 0 && rsTemp.Get_Fields_DateTime("IntPaidDate").ToOADate() != 0)
				{
					dtIntPaidDate = rsTemp.Get_Fields_DateTime("IntPaidDate");
				}
				if (dtIntPaidDate.ToOADate() == 0 || blnForceBillIntDate)
				{
					// if the date has not been set then get the bill creation date from the rate record
					if (rsCalc.Get_Fields_DateTime("IntStart").ToOADate() != 0)
					{
						dtIntPaidDate = rsCalc.Get_Fields_DateTime("IntStart");
					}
				}
				// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblPrinDuePerPeriod = rsTemp.Get_Fields_Double("Principal");
				// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
				dblPrinPaid = rsTemp.Get_Fields_Double("PrinPaid");
				// tax due
				// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
				dblCurTax = rsTemp.Get_Fields_Double("Tax") - rsTemp.Get_Fields_Double("TaxPaid");
				// find out how much principal is due
				dblCurPrin = dblPrinDuePerPeriod;
				// this is the overall total
				dblCurPrin -= dblPrinPaid;
				// this is the breakdown
				if (dblPrinDuePerPeriod > dblPrinPaid)
				{
					// if period 1 due is more than the paid
					dblPrinDuePerPeriod -= dblPrinPaid;
					// then find out how much is left after paying as much as possible to Per 1
					dblPrinPaid = 0;
					// and set the paid = 0
				}
				else
				{
					dblPrinPaid -= dblPrinDuePerPeriod;
					// else subtract how much was due from per1 and find out how much is left over
					dblPrinDuePerPeriod = 0;
					// set how much is due from per 1 = 0
				}
				// fill the interest dates
				dtStartDate = rsCalc.Get_Fields_DateTime("IntStart");
				if (dtIntPaidDate.ToOADate() > dtStartDate.ToOADate())
				{
					dtStartDate = dtIntPaidDate;
				}
				// MAL@20080602: Changed End Date to the the Interest Start Date
				// Tracker Reference: 13992
				dtDueDate = rsCalc.Get_Fields_DateTime("IntStart");
				// dtDueDate = rsCalc.Get_Fields("End")
				if (dtIntPaidDate.ToOADate() > dtDueDate.ToOADate())
				{
					dtDueDate = dtIntPaidDate;
				}
				// find out how much interest is due
				System.Diagnostics.Debug.Assert(boolUseTableName == false);
				if (boolUseTableName)
				{
					dblCurInt = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("InterestCharged") * -1);
					// "LienRec.InterestCharged"
				}
				else
				{
					// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
					dblCurInt = rsTemp.Get_Fields_Double("IntAdded") * -1;
				}
				// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
				dblCurInt -= rsTemp.Get_Fields_Double("IntPaid");
				// + rsTemp.Get_Fields("PLIPaid"))     'MAL@20080911
				dblXtraInt = 0;
				// find out current interest
				if (DateAndTime.DateDiff("D", dtDate, dtDueDate) > 0 && dtDueDate.ToOADate() > 0)
				{
					// this is the base scenerio...payment is before interest starts
					dblXtraInt = 0;
					dtLastInterestDate = dtDueDate;
				}
				else
				{
					if (DateAndTime.DateDiff("D", dtDate, dtIntPaidDate) >= 0)
					{
						// this is if interest has been charged to the period already
						dblXtraInt = 0;
						dtLastInterestDate = dtIntPaidDate;
					}
					else
					{
						dtLastInterestDate = dtStartDate;
						if (dblPrinDuePerPeriod != 0)
						{
							// If gstrRegularIntMethod = "P" Then
							dblXtraInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrinDuePerPeriod), dtStartDate, dtDate, dblIntRate, modGlobal.Statics.gintBasis, 0, modExtraModules.Statics.dblOverPayRate, ref dblPerDiem));
							// ElseIf gstrRegularIntMethod = "F" Then
							// If rsTemp.Get_Fields("IntAdded") = 0 Then
							// dblXtraInt = gdblFlatInterestAmount
							// End If
							// End If
						}
					}
				}
			}
			if (boolContinue)
			{
				// MAL@20080911: Change to break out costs as expected in the Optional passed values
				// dblXCostPLI = rsTemp.Get_Fields("Interest") + (rsTemp.Get_Fields("Costs") - rsTemp.Get_Fields("MaturityFee")) - rsTemp.Get_Fields("CostPaid")  'this will add the Pre Lien Interest and Costs
				// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
				dblXCostPLI = rsTemp.Get_Fields_Double("Interest") - rsTemp.Get_Fields_Double("PLIPaid");
				// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
				dblCurCost = (rsTemp.Get_Fields_Double("Costs") - rsTemp.Get_Fields_Double("MaturityFee")) - rsTemp.Get_Fields_Double("CostPaid");
				dblCurInt += dblXtraInt;
				dblCurPLI = dblXCostPLI;
				CalculateAccountUTLien = modGlobal.Round(Conversion.Val(dblCurPrin) + dblCurTax + Conversion.Val(dblCurInt) + Conversion.Val(dblCurCost) + dblXCostPLI, 2);
			}
			else
			{
				// reset values
				dblCurPrin = 0;
				dblCurInt = 0;
				dblCurCost = 0;
				CalculateAccountUTLien = 0;
			}
			rsCalc.Reset();
			return CalculateAccountUTLien;
		}

		public static double CalculateAccountUTTotal(int lngAccountKey, bool boolWater)
		{
			double dblTotalCurrentInterest = 0;
			double dblTotalChargedInterest = 0;
			DateTime dtDate = DateTime.Now;
			bool boolGetOnlyLienAmount = false;
			int lngAsOfRateKey = 0;
			bool blnIsMHReport = false;
			double dblTotalCost = 0;
			double dblTotalPLI = 0;
			return CalculateAccountUTTotal(lngAccountKey,  boolWater, true, ref dblTotalCurrentInterest, ref dblTotalChargedInterest, ref dtDate, boolGetOnlyLienAmount, lngAsOfRateKey, blnIsMHReport, ref dblTotalCost, ref dblTotalPLI);
		}

		public static double CalculateAccountUTTotal(int lngAccountKey, bool boolWater, bool boolShowLienAmount)
		{
			double dblTotalCurrentInterest = 0;
			double dblTotalChargedInterest = 0;
			DateTime dtDate = DateTime.Now;
			bool boolGetOnlyLienAmount = false;
			int lngAsOfRateKey = 0;
			bool blnIsMHReport = false;
			double dblTotalCost = 0;
			double dblTotalPLI = 0;
			return CalculateAccountUTTotal(lngAccountKey, boolWater, boolShowLienAmount, ref dblTotalCurrentInterest, ref dblTotalChargedInterest, ref dtDate, boolGetOnlyLienAmount, lngAsOfRateKey, blnIsMHReport, ref dblTotalCost, ref dblTotalPLI);
		}

		public static double CalculateAccountUTTotal(int lngAccountKey, bool boolWater, bool boolShowLienAmount, bool boolGetOnlyLienAmount, bool blnIsMHReport)
		{
			double dblTotalCurrentInterest = 0;
			double dblTotalChargedInterest = 0;
			DateTime dtDate = DateTime.Now;
			int lngAsOfRateKey = 0;
			double dblTotalCost = 0;
			double dblTotalPLI = 0;
			return CalculateAccountUTTotal(lngAccountKey,  boolWater, true, ref dblTotalCurrentInterest, ref dblTotalChargedInterest, ref dtDate, boolGetOnlyLienAmount, lngAsOfRateKey, blnIsMHReport, ref dblTotalCost, ref dblTotalPLI);
		}

        public static double CalculateAccountUTTotal(int lngAccountKey, bool boolWater,
            bool boolShowLienAmount /*= true*/, ref double dblTotalCurrentInterest /*= 0*/,
            ref double dblTotalChargedInterest /*= 0*/, ref DateTime dtDate /*= DateTime.Now*/
            )
        {
            double tempTotalCost = 0;
            double tempTotalPLI = 0;
            return CalculateAccountUTTotal(lngAccountKey, boolWater, boolShowLienAmount, ref dblTotalCurrentInterest,
                ref dblTotalChargedInterest, ref dtDate, false, 0, false, ref tempTotalCost, ref tempTotalPLI);
        }

        public static double CalculateAccountUTTotal(int lngAccountKey,  bool boolWater, bool boolShowLienAmount/*= true*/, ref double dblTotalCurrentInterest/*= 0*/, ref double dblTotalChargedInterest/*= 0*/, ref DateTime dtDate/*= DateTime.Now*/, bool boolGetOnlyLienAmount/*= false*/, int lngAsOfRateKey/*= 0*/, bool blnIsMHReport/*= false*/, ref double dblTotalCost/*= 0*/, ref double dblTotalPLI/*= 0*/)
		{
			double CalculateAccountUTTotal = 0;
			clsDRWrapper rsCL = new clsDRWrapper();
			clsDRWrapper rsLien = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will calculate the total remaining balance and
                // return it as a double for the account and type passed in
                // if boolGetOnlyLienAmount = True then the Non Lien Amounts will not be shown
                // if boolShowLienAmount = True then the Lien Amounts will be shown
                // if lngAsOfRateKey is > 0 then this will calculate the total for all bills up to and including that rate ID
                string strSQL = "";
                double dblTotal = 0;
                double dblXtraInt = 0;
                string strWS = "";
                double dblCost = 0;
                strWS = boolWater ? "W" : "S";

                if (dtDate.ToOADate() == 0)
                {
                    dtDate = DateTime.Today;
                }

                // Corey 08/20/2007  Call 116996
                if (lngAsOfRateKey > 0)
                {
                    strSQL = "SELECT * FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " and billingratekey <= " + FCConvert.ToString(lngAsOfRateKey) + " AND (Service = 'B' OR Service = '" + strWS + "')";
                }
                else
                {
                    strSQL = "SELECT * FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND (Service = 'B' OR Service = '" + strWS + "')";
                }

                //if (Wisej.Web.Application.Session["lienData"] == null)
                //{
                //    // first time through, grab from db and put into cache
                    rsLien.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
                //    Wisej.Web.Application.Session["lienData"] = rsLien;
                //}
                //else
                //{
                //    // grab it from cache
                //    rsLien = Wisej.Web.Application.Session["lienData"];
                //}

                rsCL.OpenRecordset(strSQL, modExtraModules.strUTDatabase);

                if (!rsCL.EndOfFile())
                {
                    while (!rsCL.EndOfFile())
                    {
                        // calculate each year
                        var lienRecordNumber = rsCL.Get_Fields(strWS + "LienRecordNumber");

                        if (lienRecordNumber == 0)
                        {
                            // non-lien
                            if (!boolGetOnlyLienAmount)
                            {
                                dblXtraInt = 0;
                                dblTotal += CalculateAccountUT(rsCL, ref dtDate, ref dblXtraInt, boolWater, ref dblCost);

                                // kgk 11-11-2011  Why is this in the Current Interest parameter?  rsCL.Get_Fields(strWS & "CostOwed") - rsCL.Get_Fields(strWS & "CostAdded") - rsCL.Get_Fields(strWS & "CostPaid"))
                                dblTotalCurrentInterest += dblXtraInt;
                                dblTotalChargedInterest += Conversion.Val(rsCL.Get_Fields(strWS + "IntOwed")) - Conversion.Val(rsCL.Get_Fields(strWS + "IntAdded")) - Conversion.Val(rsCL.Get_Fields(strWS + "IntPaid"));
                                dblTotalCost += Conversion.Val(rsCL.Get_Fields(strWS + "CostOwed")) - Conversion.Val(rsCL.Get_Fields(strWS + "CostAdded")) - Conversion.Val(rsCL.Get_Fields(strWS + "CostPaid"));
                            }
                        }
                        else
                        {
                            if (boolShowLienAmount)
                            {
                                if (Conversion.Val(rsCL.Get_Fields(strWS + "CombinationLienKey")) == Conversion.Val(rsCL.Get_Fields_Int32("ID")))
                                {
                                    // lien
                                    rsLien.FindFirstRecord("ID", lienRecordNumber);

                                    if (!rsLien.NoMatch)
                                    {
                                        // calculate the lien record
                                        dblXtraInt = 0;
                                        dblTotal += CalculateAccountUTLien(rsLien, dtDate, ref dblXtraInt, boolWater, dblCost);
                                        dblTotalCurrentInterest += dblXtraInt;

                                        dblTotalChargedInterest += -Conversion.Val(rsLien.Get_Fields("IntAdded")) + Conversion.Val(rsLien.Get_Fields("Interest")) - Conversion.Val(rsLien.Get_Fields("IntPaid")) - Conversion.Val(rsLien.Get_Fields("PLIPaid"));

                                        dblTotalPLI += Conversion.Val(rsLien.Get_Fields("Interest")) - Conversion.Val(rsLien.Get_Fields("PLIPaid"));
                                    }
                                }
                            }
                        }

                        rsCL.MoveNext();
                    }
                }
                else
                {
                    // nothing found
                    dblTotal = 0;
                }

                CalculateAccountUTTotal = dblTotal;
                rsCL.Reset();
                rsLien.Reset();

                return CalculateAccountUTTotal;
            }
            catch (Exception ex)
            {

                CalculateAccountUTTotal = 0;
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Calculate Account Total Error");
                rsCL.Reset();
                rsLien.Reset();
            }
            finally
            {
                rsLien.DisposeOf();
                //Wisej.Web.Application.Session["lienData"] = null;
                rsCL.DisposeOf();
            }
			return CalculateAccountUTTotal;
		}

		public static double CalculateAccountUTTotal_1(int lngAccountKey, bool boolWater, ref DateTime dtDate, int lngAsOfRateKey)
		{
			double dblTotalCurrentInterest = 0;
			double dblTotalChargedInterest = 0;
			double dblTotalCost = 0;
			double dblTotalPLI = 0;
			return CalculateAccountUTTotal(lngAccountKey, boolWater, true, ref dblTotalCurrentInterest, ref dblTotalChargedInterest, ref dtDate, false, lngAsOfRateKey, false, ref dblTotalCost, ref dblTotalPLI);
		}

		public static double CalculateAccountUTTotal_2(int lngAccountKey, bool boolWater, bool boolShowLienAmount, ref DateTime dtDate)
		{
			double dblTotalCurrentInterest = 0;
			double dblTotalChargedInterest = 0;
			double dblTotalCost = 0;
			double dblTotalPLI = 0;
			return CalculateAccountUTTotal(lngAccountKey,  boolWater, boolShowLienAmount, ref dblTotalCurrentInterest, ref dblTotalChargedInterest, ref dtDate, false, 0, false, ref dblTotalCost, ref dblTotalPLI);
		}

		public static double CalculateAccountUTTotal_3(int lngAccountKey, bool boolWater, bool boolShowLienAmount, ref double dblTotalCurrentInterest, ref double dblTotalChargedInterest, ref DateTime dtDate)
		{
			double dblTotalCost = 0;
			double dblTotalPLI = 0;
			return CalculateAccountUTTotal(lngAccountKey, boolWater, boolShowLienAmount, ref dblTotalCurrentInterest, ref dblTotalChargedInterest, ref dtDate, false, 0, false, ref dblTotalCost, ref dblTotalPLI);
		}

		public static double CalculateAccountUTTotal_31(int lngAccountKey, bool boolWater, bool boolShowLienAmount, ref double dblTotalCurrentInterest, ref double dblTotalChargedInterest, DateTime dtDate)
		{
			return CalculateAccountUTTotal_3(lngAccountKey, boolWater, boolShowLienAmount, ref dblTotalCurrentInterest, ref dblTotalChargedInterest, ref dtDate);
		}

		public static double CalculateAccountUTTotal_32(int lngAccountKey, bool boolWater, bool boolShowLienAmount, DateTime dtDate)
		{
			double dblTotalCurrentInterest = 0;
			double dblTotalChargedInterest = 0;
			return CalculateAccountUTTotal_3(lngAccountKey, boolWater, boolShowLienAmount, ref dblTotalCurrentInterest, ref dblTotalChargedInterest, ref dtDate);
		}

		public static double CalculateAccountUTTotal_4(int lngAccountKey, bool boolWater, bool boolShowLienAmount, ref double dblTotalCurrentInterest, ref double dblTotalChargedInterest, ref DateTime dtDate, bool boolGetOnlyLienAmount, ref double dblTotalPLI)
		{
			double dblTotalCost = 0;
			return CalculateAccountUTTotal(lngAccountKey, boolWater, boolShowLienAmount, ref dblTotalCurrentInterest, ref dblTotalChargedInterest, ref dtDate, boolGetOnlyLienAmount, 0, false, ref dblTotalCost, ref dblTotalPLI);
		}

		public static double CalculateAccountUTTotal_5(int lngAccountKey, bool boolWater, bool boolShowLienAmount, DateTime dtDate, bool boolGetOnlyLienAmount)
		{
			double dblTotalCurrentInterest = 0;
			double dblTotalChargedInterest = 0;
			double dblTotalCost = 0;
			double dblTotalPLI = 0;
			return CalculateAccountUTTotal(lngAccountKey,  boolWater, boolShowLienAmount, ref dblTotalCurrentInterest, ref dblTotalChargedInterest, ref dtDate, boolGetOnlyLienAmount, 0, false, ref dblTotalCost, ref dblTotalPLI);
		}

		public static double CalculateAccountUTTotal_6(int lngAccountKey, bool boolWater, bool boolShowLienAmount, DateTime dtDate, bool boolGetOnlyLienAmount)
		{
			double dblTotalCurrentInterest = 0;
			double dblTotalChargedInterest = 0;
			double dblTotalCost = 0;
			double dblTotalPLI = 0;
			return CalculateAccountUTTotal(lngAccountKey,  boolWater, boolShowLienAmount, ref dblTotalCurrentInterest, ref dblTotalChargedInterest, ref dtDate, boolGetOnlyLienAmount, 0, false, ref dblTotalCost, ref dblTotalPLI);
		}

		public static string GetRateKeysByRange(ref DateTime dtStartDate, ref DateTime dtEndDate, bool boolBillingDate = false)
		{
			string GetRateKeysByRange = "";
			// returns a list of ratekeys that have an enddate (or billingdate depending on the boolean passed) between startdate and enddate
			string strReturn;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				GetRateKeysByRange = strReturn;
				if (!boolBillingDate)
				{
					strSQL = "Select * from ratekeys where ratetype = 'R' and end between '" + Strings.Format(dtStartDate, "MM/dd/yyyy") + "' and '" + Strings.Format(dtEndDate, "MM/dd/yyyy") + "'";
				}
				else
				{
					strSQL = "Select * from ratekeys where ratetype = 'R' and billdate between '" + Strings.Format(dtStartDate, "MM/dd/yyyy") + "' and '" + Strings.Format(dtEndDate, "MM/dd/yyyy") + "'";
				}
				clsLoad.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				while (!clsLoad.EndOfFile())
				{
					strReturn += FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")) + ",";
					clsLoad.MoveNext();
				}
				if (strReturn != string.Empty)
				{
					// strip the last comma off
					strReturn = Strings.Mid(strReturn, 1, strReturn.Length - 1);
				}
				GetRateKeysByRange = strReturn;
				return GetRateKeysByRange;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In GetRateKeysByEndDateRange", MsgBoxStyle.Critical, "Error");
			}
			return GetRateKeysByRange;
		}

		public static string GetBillsByRateKeys(ref string strRateKeyList, int lngAccountKey = 0)
		{
			string GetBillsByRateKeys = "";
			// gets a list of bills that use the list of ratekeys.  If account isn't specified, it gets all accounts bills
			string strReturn;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL = "";
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				GetBillsByRateKeys = strReturn;
				if (lngAccountKey > 0)
				{
					strSQL = "select ID from bill where accountkey = " + FCConvert.ToString(lngAccountKey) + " and billingratekey in (" + strRateKeyList + ")";
				}
				else
				{
					strSQL = "select ID from bill where billingratekey in (" + strRateKeyList + ")";
				}
				clsLoad.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				while (!clsLoad.EndOfFile())
				{
					strReturn += FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")) + ",";
					clsLoad.MoveNext();
				}
				if (strReturn != string.Empty)
				{
					// strip the last comma off
					strReturn = Strings.Mid(strReturn, 1, strReturn.Length - 1);
				}
				GetBillsByRateKeys = strReturn;
				return GetBillsByRateKeys;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In GetBillsByRateKeys", MsgBoxStyle.Critical, "Error");
			}
			return GetBillsByRateKeys;
		}
		// VBto upgrade warning: 'Return' As int	OnWrite(short, double)
		public static int GetConsumptionByBillKeys(ref string strBillList)
		{
			int GetConsumptionByBillKeys = 0;
			// Find breakdown records for these bills and adds total consumption
			double lngReturn;
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			string[] strAry = null;
			int x/*unused?*/;
			try
			{
				// On Error GoTo ErrorHandler
				GetConsumptionByBillKeys = 0;
				lngReturn = 0;
				// comma delimited list so split it up
				// strAry = Split(strBillList, ",", , CompareConstants.vbTextCompare)
				// do each bill separately because we can't add up consumptions. Multiple records for one meter
				// for a particular bill will each list the total consumption
				// For x = 0 To UBound(strAry)
				// DISTINCT taken out of this select by Corey 05/31/2006 because Enfield could not run sewer bills with this
				// strSQL = "select  billkey,meterkey,value from breakdown where type = 'C' and billkey = " & strAry(x) & " ORDER BY Service, Type, Description"
				// Call clsLoad.OpenRecordset(strSQL, strUTDatabase)
				// Do While Not clsLoad.EndOfFile
				// lngReturn = lngReturn + Val(clsLoad.Get_Fields("value"))
				// clsLoad.MoveNext
				// Loop
				// 
				// Next x
				strSQL = "select sum(consumption) as totcon from bill where ID in (" + strBillList + ")";
				clsLoad.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO: Field [totcon] not found!! (maybe it is an alias?)
					lngReturn = Conversion.Val(clsLoad.Get_Fields("totcon"));
				}
				GetConsumptionByBillKeys = FCConvert.ToInt32(lngReturn);
				return GetConsumptionByBillKeys;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In GetConsumptionByBillKeys", MsgBoxStyle.Critical, "Error");
			}
			return GetConsumptionByBillKeys;
		}

		public static int GetConsumptionByDateRange(ref DateTime dtStart, ref DateTime dtEnd, int lngAccountKey = 0)
		{
			int GetConsumptionByDateRange = 0;
			string strRateKeys;
			string strBills = "";
			int lngReturn;
			lngReturn = 0;
			GetConsumptionByDateRange = 0;
			strRateKeys = GetRateKeysByRange(ref dtStart, ref dtEnd);
			if (strRateKeys != string.Empty)
			{
				strBills = GetBillsByRateKeys(ref strRateKeys, lngAccountKey);
				if (strBills != string.Empty)
				{
					lngReturn = GetConsumptionByBillKeys(ref strBills);
				}
			}
			GetConsumptionByDateRange = lngReturn;
			return GetConsumptionByDateRange;
			ErrorHandler:
			;
			FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + "  " + Information.Err().Description + "\r\n" + "In GetConsumptionByDateRange", MsgBoxStyle.Critical, "Error");
			return GetConsumptionByDateRange;
		}

		public static string GetServiceRangeFromRateKeys(ref string strRateKeyList)
		{
			string GetServiceRangeFromRateKeys = "";
			// returns a comma delimited list of the earliest start date and the latest end date
			string strReturn;
			string strSQL = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				GetServiceRangeFromRateKeys = "";
				if (strRateKeyList != string.Empty)
				{
					strSQL = "Select min(start) as MinStart, max(end) as MaxStart from ratekeys where ID in (" + strRateKeyList + ")";
					clsLoad.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
					if (!clsLoad.EndOfFile())
					{
						// TODO: Field [Minstart] not found!! (maybe it is an alias?)
						// TODO: Field [maxstart] not found!! (maybe it is an alias?)
						strReturn = Strings.Format(clsLoad.Get_Fields("Minstart"), "MM/dd/yyyy") + "," + Strings.Format(clsLoad.Get_Fields("maxstart"), "MM/dd/yyyy");
					}
				}
				GetServiceRangeFromRateKeys = strReturn;
				return GetServiceRangeFromRateKeys;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In GetServiceRangeFromRateKeys", MsgBoxStyle.Critical, "Error");
			}
			return GetServiceRangeFromRateKeys;
		}

		public static string GetConsumptionAndAverageByDateRange(ref DateTime dtStart, ref DateTime dtEnd, int lngAccountKey = 0, double dblRateMult = 0)
		{
			string GetConsumptionAndAverageByDateRange = "";
			// returns a list of the consumption,average,and # service days
			string strRateKeys;
			string strBills = "";
			// VBto upgrade warning: lngReturn As int	OnWrite(int, double)
			double lngReturn;
			string strReturn;
			// VBto upgrade warning: dblAverage As double	OnWrite(short, string)
			double dblAverage;
			// VBto upgrade warning: strAry As string()	OnRead(DateTime)
			string[] strAry = null;
			// VBto upgrade warning: dtFirstDay As DateTime	OnWrite(string)
			DateTime dtFirstDay;
			// VBto upgrade warning: dtLastDay As DateTime	OnWrite(string)
			DateTime dtLastDay;
			// VBto upgrade warning: lngDays As int	OnWriteFCConvert.ToDouble(
			int lngDays = 0;
			dblAverage = 0;
			strReturn = "";
			lngReturn = 0;
			GetConsumptionAndAverageByDateRange = "";
			strRateKeys = GetRateKeysByRange(ref dtStart, ref dtEnd);
			if (strRateKeys != string.Empty)
			{
				strReturn = GetServiceRangeFromRateKeys(ref strRateKeys);
				if (strReturn != string.Empty)
				{
					strAry = Strings.Split(strReturn, ",", -1, CompareConstants.vbTextCompare);
					dtFirstDay = FCConvert.ToDateTime(strAry[0]);
					dtLastDay = FCConvert.ToDateTime(strAry[1]);
					lngDays = FCConvert.ToInt32(dtLastDay.ToOADate() - dtFirstDay.ToOADate());
					// Number of service days
				}
				strBills = GetBillsByRateKeys(ref strRateKeys, lngAccountKey);
				if (strBills != string.Empty)
				{
					lngReturn = GetConsumptionByBillKeys(ref strBills);
					if (dblRateMult > 0)
					{
						lngReturn *= dblRateMult;
					}
				}
				if (lngDays > 0)
				{
					dblAverage = FCConvert.ToDouble(Strings.Format(FCConvert.ToDouble(lngReturn) / lngDays, "0.00"));
				}
				strReturn = FCConvert.ToString(lngReturn) + "," + FCConvert.ToString(dblAverage) + "," + FCConvert.ToString(lngDays);
			}
			GetConsumptionAndAverageByDateRange = strReturn;
			return GetConsumptionAndAverageByDateRange;
			ErrorHandler:
			;
			FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err().Number) + "  " + Information.Err().Description + "\r\n" + "In GetConsumptionAndAverageByDateRange", MsgBoxStyle.Critical, "Error");
			return GetConsumptionAndAverageByDateRange;
		}

		public static void ClearCMFTable_Utility(string strType)
		{
			// MAL@20071022: Function to clear any records in the CMFNumbers table with matching type
			// Call Reference: 114824
			clsDRWrapper rsCMF = new clsDRWrapper();
			rsCMF.Execute("DELETE FROM CMFNumbers WHERE ProcessType = '" + strType + "'", modExtraModules.strUTDatabase);
		}

		public static void UTCalcSetup()
		{
			//kk03242014 trocl-816  Add this sub so we can set the basis and overpay rate so we can
			//calculate interest for group balance without running UTSetup
			//The globals are the same in CL and UT, but the values may be different
			try
			{
				clsDRWrapper WUT = new clsDRWrapper();
				//kk06042015 trout-1166  Customize saves these in UtilityBilling, not CollectionControl
				//                       There is no process that updates CollectionControl. Was this table linked to CL at one point?
				//    WUT.OpenRecordset "SELECT Basis, OverPayInterestRate FROM CollectionControl", strUTDatabase
				//    If Not WUT.EndOfFile Then
				//        gintBasis = WUT.Fields("Basis")
				//        dblOverPayRate = WUT.Fields("OverPayInterestRate") / 100
				//    Else
				//        MsgBox "An error occured while opening TWUT0000.vb1.  Please make sure that you are running from the correct directory.", vbOKOnly, "Open Database Error"
				//    End If
				WUT.OpenRecordset("SELECT RegularIntMethod, FlatInterestAmount, Basis, OverPayInterestRate FROM UtilityBilling", modExtraModules.strUTDatabase);
				if (!WUT.EndOfFile())
				{
					modGlobal.Statics.gintBasis = WUT.Get_Fields_Int32("Basis");
					modExtraModules.Statics.dblOverPayRate = WUT.Get_Fields_Double("OverPayInterestRate");
					//   / 100      'kk060421015 Change to store rate as demial value to be consistent -7 % => 0.07
				}
				if (WUT.Get_Fields_String("RegularIntMethod") != "")
				{
					modExtraModules.Statics.gstrRegularIntMethod = WUT.Get_Fields_String("RegularIntMethod");
					if (modExtraModules.Statics.gstrRegularIntMethod == "F")
					{
						modStatusPayments.Statics.gdblFlatInterestAmount = FCConvert.ToDouble(WUT.Get_Fields_Decimal("FlatInterestAmount"));
					}
					else
					{
						modExtraModules.Statics.gstrRegularIntMethod = "P";
					}
				}
				else
				{
					MessageBox.Show("An error occured while opening TWUT0000.vb1. Please make sure that you are running from the correct directory.", "Open Database Error", MessageBoxButtons.OK);
				}
				return;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + Information.Err(ex).Number + " - " + ex.GetBaseException().Message + ".", "Utilities Setup Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}
		}

		public class StaticVariables
		{
		
			public bool gboolShowMapLotInUTAudit;
			public bool gboolShowOwner;
			// If true, then show the owner name during the bill process
			public bool gboolSplitBills;
			public bool gboolUseBookForLien;
			public bool gboolDefaultUTRTDToAutoChange;
			// kk06122014 trout-1082  Change globals for LDN from gdblLDNAdjustment... to gdblUTLDNAdjustment... to fix conflict with CL in CR
			// If this is true then the checkbox on the effective date change screen will default to true
			public double gdblUTLDNAdjustmentTop;
			// adjusts the top margin of the Lien Discharge Notice
			public double gdblUTLDNAdjustmentBottom;
			// adjusts the bottom margin of the Lien Discharge Notice   'kk04222014  trocl-1156/trout-1082
			public double gdblUTLDNAdjustmentSide;
            public cUTSettings gUTSettings;
        }

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
