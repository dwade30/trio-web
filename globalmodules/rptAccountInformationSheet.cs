﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TWSharedLibrary;
#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWRE0000
using TWRE0000;
using modGlobal = TWRE0000.modGlobalRoutines;
using modStatusPayments = TWRE0000.modGlobalVariables;
using modExtraModules = TWRE0000.modGlobalVariables;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptAccountInformationSheet : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/18/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/27/2006              *
		// ********************************************************
		clsDRWrapper rsAcct = new clsDRWrapper();
		clsDRWrapper rsBill = new clsDRWrapper();
		clsDRWrapper rsLien = new clsDRWrapper();
		DateTime dtAsOfDate;
		int lngAcct;
		double dblPrin;
		double dblInt;
		double dblCost;
		double dblExtraIntCost;
		double dblPerDiem;
		double dblTotalAccountDue;
		bool boolFirstPass;
		bool boolUsePeriodTotalLine;
		int intLineNumber;
		modStatusPayments.BillPeriods AbatementPayments = new modStatusPayments.BillPeriods();
		const int CNSTLANDCATEGORYCROP = 7;
		const int CNSTLANDCATEGORYORCHARD = 8;
		const int CNSTLANDCATEGORYPASTURE = 9;

		public rptAccountInformationSheet()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
            this.ReportEnd += RptAccountInformationSheet_ReportEnd;
		}

        private void RptAccountInformationSheet_ReportEnd(object sender, EventArgs e)
        {
			rsAcct.DisposeOf();
            rsBill.DisposeOf();
            rsLien.DisposeOf();
		}

        public static rptAccountInformationSheet InstancePtr
		{
			get
			{
				return (rptAccountInformationSheet)Sys.GetInstance(typeof(rptAccountInformationSheet));
			}
		}

		protected rptAccountInformationSheet _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsLien?.Dispose();
				rsBill?.Dispose();
				rsAcct?.Dispose();
                rsLien = null;
                rsBill = null;
                rsAcct = null;
            }
            base.Dispose(disposing);
		}
		// VBto upgrade warning: lngAccountNumber As int	OnWriteFCConvert.ToDouble(
		public void Init(int lngAccountNumber, DateTime dtPassDate/*= DateTime.Now*/)
		{
			lngAcct = lngAccountNumber;
			if (dtPassDate.ToOADate() == 0)
			{
				dtAsOfDate = DateTime.Today;
			}
			else
			{
				dtAsOfDate = dtPassDate;
			}
			rsAcct.OpenRecordset("SELECT * FROM Master WHERE RSCard = 1 AND RSAccount = " + FCConvert.ToString(lngAcct), modExtraModules.strREDatabase);
			if (!modGlobalConstants.Statics.gboolBL && !modGlobalConstants.Statics.gboolCL)
			{
				FCMessageBox.Show("Error finding billing information", MsgBoxStyle.Critical, "Missing Billing Information");
				Cancel();
				return;
			}
			rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE BillingType = 'RE' AND Account = " + FCConvert.ToString(lngAcct) + " ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
			if (rsAcct.EndOfFile())
			{
				FCMessageBox.Show("Error finding account information.", MsgBoxStyle.Critical, "Missing Account Information");
				Cancel();
				// ElseIf rsBill.EndOfFile Then
				// MsgBox "Error finding billing information.", vbCritical, "Missing Billing Information"
				// Unload Me
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsBill.EndOfFile() || dblTotalAccountDue == 0;
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			SetHeaderString();
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			string strTemp = "";
			if (modRegionalTown.IsRegionalTown())
			{
				// TODO: Check the table for the column [trancode] and replace with corresponding Get_Field method
				strTemp = modRegionalTown.GetTownKeyName_2(FCConvert.ToInt32(rsBill.Get_Fields("trancode")));
			}
			else
			{
				strTemp = modGlobalConstants.Statics.MuniName;
			}
			if (Strings.UCase(modGlobalConstants.Statics.gstrCityTown) == "TOWN" || Strings.UCase(modGlobalConstants.Statics.gstrCityTown) == "CITY")
			{
				lblHeader.Text = modGlobalConstants.Statics.gstrCityTown + " of " + strTemp;
			}
			else
			{
				lblHeader.Text = strTemp;
			}
			lblHeader.Text = lblHeader.Text + "\r\n" + "Tax Information Sheet";
			lblHeader.Text = lblHeader.Text + "\r\n" + "As of: " + Strings.Format(dtAsOfDate, "MM/dd/yyyy");
			lblDateDisclaimer.Text = "All calculations are as of: " + Strings.Format(dtAsOfDate, "MM/dd/yyyy");
			boolFirstPass = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			// this will print the row from the grid
            using (clsDRWrapper rsCheckTC = new clsDRWrapper())
            {
                double dblTotalDue = 0;
                double dblCurrentInt = 0;
                double dblBillPrin = 0;
                double dblBillInt = 0;
                double dblBillCost = 0;
                double dblBillPLI = 0;
                double dblPD = 0;
                bool boolTransferPeriodAmounts = false;
                int intPerToUse = 0;
                double dblPeriodTotal = 0;
                double Tax1 = 0;
                double Tax2 = 0;
                double Tax3 = 0;
                double Tax4 = 0;
                double dblPerCost = 0;
                double dblPerInt = 0;
                bool blnNoCurrentInt = false;
                TRYAGAIN: ;
                if (!rsBill.EndOfFile())
                {
                    dblCurrentInt = 0;
                    dblBillPrin = 0;
                    dblBillInt = 0;
                    dblBillCost = 0;
                    dblBillPLI = 0;
                    dblTotalDue = 0;
                    dblPD = 0;
                    if (((rsBill.Get_Fields_Int32("LienRecordNumber"))) == 0)
                    {
                        // non lien
                        dblTotalDue =
                            modGlobal.Round(
                                modCLCalculations.CalculateAccountCL2(ref rsBill, lngAcct, dtAsOfDate,
                                    ref dblCurrentInt, ref dblBillPrin, ref dblBillInt, ref dblBillCost, ref dblPD), 2);
                        rsCheckTC.OpenRecordset(
                            "SELECT * FROM PaymentRec WHERE BillKey = " +
                            FCConvert.ToString(rsBill.Get_Fields_Int32("ID")) + " AND BillCode <> 'L' ORDER BY ID desc",
                            modExtraModules.strCLDatabase);
                        if (!rsCheckTC.EndOfFile())
                        {
                            if (rsCheckTC.Get_Fields_String("Code") == "U")
                            {
                                // if the last payment is a tax club payment them zero out the interest
                                dblBillInt -= dblCurrentInt;
                                // remove the current int from the charged int
                                dblCurrentInt = 0;
                            }
                        }
                    }
                    else
                    {
                        // lien
                        rsLien.OpenRecordset(
                            "SELECT * FROM LienRec WHERE ID = " +
                            FCConvert.ToString(rsBill.Get_Fields_Int32("LienRecordNumber")),
                            modExtraModules.strCLDatabase);
                        dblTotalDue = modCLCalculations.CalculateAccountCLLien(rsLien, dtAsOfDate, ref dblCurrentInt,
                            ref dblBillPrin, ref dblBillInt, ref dblBillCost, ref dblPD, ref dblBillPLI);
                    }

                    if (dblTotalDue == 0)
                    {
                        // move to the next bill because this does not have any amount due
                        rsBill.MoveNext();
                        goto TRYAGAIN;
                    }

                    boolTransferPeriodAmounts = false;
                    if (boolFirstPass && ((rsBill.Get_Fields_Int32("RateKey"))) != 0)
                    {
                        clsDRWrapper rsRate = new clsDRWrapper();
                        double dblPrincipalPaid = 0;
                        double dblPerPrin = 0;
                        intPerToUse = 1;
                        dblPrincipalPaid = Conversion.Val(rsBill.Get_Fields_Decimal("PrincipalPaid"));
                        rsRate.OpenRecordset(
                            "SELECT * FROM RateRec WHERE ID = " +
                            FCConvert.ToString(rsBill.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
                        if (!rsRate.EndOfFile())
                        {
                            boolFirstPass = false;
                            lblPeriodTitle.Visible = true;
                            if (((rsBill.Get_Fields_Int32("LienRecordNumber"))) == 0)
                            {
                                boolTransferPeriodAmounts = true;
                                // non lien
                                // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                CheckAbatements_2186(FCConvert.ToInt32(rsBill.Get_Fields("Account")),
                                    FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID")),
                                    Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue1")),
                                    Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue2")),
                                    Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue3")),
                                    Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue4")),
                                    FCConvert.ToInt32(rsBill.Get_Fields_Int32("RateKey")));
                                dblCurrentInt -= dblBillInt;
                                dblPD = 0;
                                dblTotalDue = modCLCalculations.CalculateAccountCL(ref rsBill, lngAcct, dtAsOfDate,
                                    ref dblCurrentInt, ref dblBillPrin, ref dblBillInt, ref dblBillCost, ref dblPD,
                                    ref AbatementPayments.Per1, ref AbatementPayments.Per2, ref AbatementPayments.Per3,
                                    ref AbatementPayments.Per4);
                                // ===================
                                // MAL@20080807: Changed to correctly check for tax club membership (not just last payment = U)
                                // Tracker Reference: 14579
                                // Check Tax Club
                                rsCheckTC.OpenRecordset(
                                    "SELECT * FROM TaxClub WHERE BillKey = " +
                                    FCConvert.ToString(rsBill.Get_Fields_Int32("ID")) + " AND [Year] = " +
                                    FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear")),
                                    modExtraModules.strCLDatabase);
                                if (!rsCheckTC.EndOfFile())
                                {
                                    blnNoCurrentInt = true;
                                }
                                else
                                {
                                    blnNoCurrentInt = false;
                                }

                                // Then Check Payments
                                rsCheckTC.OpenRecordset(
                                    "SELECT * FROM PaymentRec WHERE BillKey = " +
                                    FCConvert.ToString(rsBill.Get_Fields_Int32("ID")) +
                                    " AND BillCode <> 'L' ORDER BY ID desc", modExtraModules.strCLDatabase);
                                if (!rsCheckTC.EndOfFile())
                                {
                                    if (rsCheckTC.Get_Fields_String("Code") == "U" && blnNoCurrentInt)
                                    {
                                        // kk02052015 TROCL-1223  Add check for current Tax Club status
                                        // if the last payment is a tax club payment them zero out the interest
                                        dblBillInt -= dblCurrentInt;
                                        // remove the current int from the charged int
                                        dblCurrentInt = 0;
                                    }
                                }
                                else
                                {
                                    // No Payments Made but still member of tax club
                                    if (blnNoCurrentInt)
                                    {
                                        dblBillInt -= dblCurrentInt;
                                        // remove the current int from the charged int
                                        dblCurrentInt = 0;
                                    }
                                }

                                // ===================
                                Tax1 = Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue1")) - AbatementPayments.Per1;
                                // Tax1 = Tax1 - (rsBill.Fields("PrincipalPaid") + rsBill.Fields("InterestPaid") + rsBill.Fields("DemandFeesPaid"))
                                // Tax1 = Tax1 + (AbatementPayments.Per1 + AbatementPayments.Per2 + AbatementPayments.Per3 + AbatementPayments.Per4)
                                Tax2 = Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue2")) - AbatementPayments.Per2;
                                Tax3 = Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue3")) - AbatementPayments.Per3;
                                Tax4 = Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue4")) - AbatementPayments.Per4;
                                dblPrincipalPaid = Conversion.Val(rsBill.Get_Fields_Decimal("PrincipalPaid")) -
                                                   (AbatementPayments.Per1 + AbatementPayments.Per2 +
                                                    AbatementPayments.Per3 + AbatementPayments.Per4);
                                dblPerCost = Conversion.Val(rsBill.Get_Fields_Decimal("DemandFees")) +
                                             Conversion.Val(rsBill.Get_Fields_Decimal("DemandFeesPaid"));
                                dblPerInt = (FCConvert.ToDouble(rsBill.Get_Fields_Decimal("InterestCharged")) * -1) -
                                    Conversion.Val(rsBill.Get_Fields_Decimal("InterestPaid")) + dblCurrentInt;
                            }
                            else
                            {
                                // TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
                                Tax1 = Conversion.Val(rsLien.Get_Fields("Principal"));
                                // TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
                                // TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
                                dblPerCost = Conversion.Val(rsLien.Get_Fields("Costs")) -
                                             Conversion.Val(rsLien.Get_Fields_Decimal("CostsPaid")) -
                                             Conversion.Val(rsLien.Get_Fields("MaturityFee"));
                                // TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
                                dblPerInt = Conversion.Val(rsLien.Get_Fields("Interest")) -
                                            Conversion.Val(rsLien.Get_Fields_Decimal("InterestPaid")) -
                                            Conversion.Val(rsLien.Get_Fields_Decimal("InterestCharged"));
                                // ===================
                                // MAL@20080807: Changed to correctly check for tax club membership (not just last payment = U)
                                // Tracker Reference: 14579
                                // Check Tax Club
                                rsCheckTC.OpenRecordset(
                                    "SELECT * FROM TaxClub WHERE BillKey = " +
                                    FCConvert.ToString(rsBill.Get_Fields_Int32("ID")) + " AND [Year] = " +
                                    FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear")),
                                    modExtraModules.strCLDatabase);
                                if (!rsCheckTC.EndOfFile())
                                {
                                    blnNoCurrentInt = true;
                                }
                                else
                                {
                                    blnNoCurrentInt = false;
                                }

                                // Then Check Payments
                                rsCheckTC.OpenRecordset(
                                    "SELECT * FROM PaymentRec WHERE BillKey = " +
                                    FCConvert.ToString(rsLien.Get_Fields_Int32("ID")) +
                                    " AND BillCode = 'L' ORDER BY ID desc", modExtraModules.strCLDatabase);
                                if (!rsCheckTC.EndOfFile())
                                {
                                    if (rsCheckTC.Get_Fields_String("Code") == "U" && blnNoCurrentInt)
                                    {
                                        // kk02052015 TROCL-1223  Add check for current Tax Club status
                                        // if the last payment is a tax club payment them zero out the interest
                                        dblBillInt -= dblCurrentInt;
                                        // remove the current int from the charged int
                                        dblCurrentInt = 0;
                                    }
                                }
                                else
                                {
                                    // No Payments Made but still member of tax club
                                    if (blnNoCurrentInt)
                                    {
                                        dblBillInt -= dblCurrentInt;
                                        // remove the current int from the charged int
                                        dblCurrentInt = 0;
                                    }
                                }

                                lblPeriodTitle.Visible = false;
                                fldPeriodAmount1.Visible = false;
                                fldPeriodDate1.Visible = false;
                                fldPeriodAmount1Int.Visible = false;
                                fldPeriodAmount1Total.Visible = false;
                                fldPeriodAmount2.Visible = false;
                                fldPeriodDate2.Visible = false;
                                fldPeriodAmount2Total.Visible = false;
                                fldPeriodAmount3.Visible = false;
                                fldPeriodDate3.Visible = false;
                                fldPeriodAmount3Total.Visible = false;
                                fldPeriodAmount4.Visible = false;
                                fldPeriodDate4.Visible = false;
                                fldPeriodAmount4Total.Visible = false;
                                // fldPeriodAmount1Int.Text = Format(dblPerInt, "#,##0.00") 'Format(dblBillInt, "#,##0.00")
                                // fldPeriodAmount1Cost.Text = Format(dblPerCost, "#,##0.00") 'Format(dblCost, "#,##0.00")
                                // fldPeriodAmount1Total.Text = Format(Tax1 - rsLien.Fields("PrincipalPaid") + dblPerInt + dblPerCost, "#,##0.00")
                            }

                            lblPeriodTitle.Text =
                                modExtraModules.FormatYear(FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear"))) +
                                " Period Due";
                            dblPerPrin = Tax1;
                            if (dblPrincipalPaid >= dblPerPrin)
                            {
                                dblPrincipalPaid -= dblPerPrin;
                            }
                            else
                            {
                                intPerToUse = 2;
                                fldPeriodAmount1.Text = Strings.Format(dblPerPrin - dblPrincipalPaid, "#,##0.00");
                                dblPeriodTotal = dblPerPrin - dblPrincipalPaid;
                                fldPeriodDate1.Text =
                                    Strings.Format(rsRate.Get_Fields_DateTime("DueDate1"), "MM/dd/yyyy");
                                dblPrincipalPaid -= dblPerPrin;
                                if (dblPrincipalPaid < 0)
                                    dblPrincipalPaid = 0;
                            }

                            if (FCConvert.ToInt32(rsRate.Get_Fields_Int16("NumberOfPeriods")) > 1)
                            {
                                dblPerPrin = Tax2;
                                if (dblPrincipalPaid >= dblPerPrin)
                                {
                                    dblPrincipalPaid -= dblPerPrin;
                                }
                                else
                                {
                                    switch (intPerToUse)
                                    {
                                        case 1:
                                        {
                                            fldPeriodAmount1.Text = Strings.Format(dblPerPrin - dblPrincipalPaid,
                                                "#,##0.00");
                                            fldPeriodDate1.Text = Strings.Format(rsRate.Get_Fields_DateTime("DueDate2"),
                                                "MM/dd/yyyy");
                                            intPerToUse = 2;
                                            break;
                                        }
                                        default:
                                        {
                                            fldPeriodAmount2.Text = Strings.Format(dblPerPrin - dblPrincipalPaid,
                                                "#,##0.00");
                                            fldPeriodDate2.Text = Strings.Format(rsRate.Get_Fields_DateTime("DueDate2"),
                                                "MM/dd/yyyy");
                                            intPerToUse = 3;
                                            dblPrincipalPaid = 0;
                                            break;
                                        }
                                    }

                                    dblPeriodTotal += modGlobal.Round((dblPerPrin - dblPrincipalPaid), 2);
                                    dblPrincipalPaid -= dblPerPrin;
                                    if (dblPrincipalPaid < 0)
                                        dblPrincipalPaid = 0;
                                }

                                if (FCConvert.ToInt32(rsRate.Get_Fields_Int16("NumberOfPeriods")) > 2)
                                {
                                    dblPerPrin = Tax3;
                                    if (dblPrincipalPaid >= dblPerPrin)
                                    {
                                        dblPrincipalPaid -= dblPerPrin;
                                    }
                                    else
                                    {
                                        switch (intPerToUse)
                                        {
                                            case 1:
                                            {
                                                fldPeriodDate1.Text =
                                                    Strings.Format(rsRate.Get_Fields_DateTime("DueDate3"),
                                                        "MM/dd/yyyy");
                                                fldPeriodAmount1.Text = Strings.Format(dblPerPrin - dblPrincipalPaid,
                                                    "#,##0.00");
                                                intPerToUse = 2;
                                                break;
                                            }
                                            case 2:
                                            {
                                                fldPeriodDate2.Text =
                                                    Strings.Format(rsRate.Get_Fields_DateTime("DueDate3"),
                                                        "MM/dd/yyyy");
                                                fldPeriodAmount2.Text = Strings.Format(dblPerPrin - dblPrincipalPaid,
                                                    "#,##0.00");
                                                intPerToUse = 3;
                                                break;
                                            }
                                            default:
                                            {
                                                fldPeriodDate3.Text =
                                                    Strings.Format(rsRate.Get_Fields_DateTime("DueDate3"),
                                                        "MM/dd/yyyy");
                                                fldPeriodAmount3.Text = Strings.Format(dblPerPrin - dblPrincipalPaid,
                                                    "#,##0.00");
                                                dblPrincipalPaid = 0;
                                                intPerToUse = 4;
                                                break;
                                            }
                                        }

                                        //end switch
                                        dblPeriodTotal += (dblPerPrin - dblPrincipalPaid);
                                        dblPrincipalPaid -= dblPerPrin;
                                        if (dblPrincipalPaid < 0)
                                            dblPrincipalPaid = 0;
                                    }

                                    if (FCConvert.ToInt32(rsRate.Get_Fields_Int16("NumberOfPeriods")) > 3)
                                    {
                                        dblPerPrin = Tax4;
                                        if (dblPrincipalPaid >= dblPerPrin)
                                        {
                                            dblPrincipalPaid -= dblPerPrin;
                                        }
                                        else
                                        {
                                            switch (intPerToUse)
                                            {
                                                case 1:
                                                {
                                                    fldPeriodDate1.Text =
                                                        Strings.Format(rsRate.Get_Fields_DateTime("DueDate4"),
                                                            "MM/dd/yyyy");
                                                    fldPeriodAmount1.Text =
                                                        Strings.Format(dblPerPrin - dblPrincipalPaid, "#,##0.00");
                                                    intPerToUse = 2;
                                                    break;
                                                }
                                                case 2:
                                                {
                                                    fldPeriodDate2.Text =
                                                        Strings.Format(rsRate.Get_Fields_DateTime("DueDate4"),
                                                            "MM/dd/yyyy");
                                                    fldPeriodAmount2.Text =
                                                        Strings.Format(dblPerPrin - dblPrincipalPaid, "#,##0.00");
                                                    intPerToUse = 3;
                                                    break;
                                                }
                                                case 3:
                                                {
                                                    fldPeriodDate3.Text =
                                                        Strings.Format(rsRate.Get_Fields_DateTime("DueDate4"),
                                                            "MM/dd/yyyy");
                                                    fldPeriodAmount3.Text =
                                                        Strings.Format(dblPerPrin - dblPrincipalPaid, "#,##0.00");
                                                    intPerToUse = 4;
                                                    break;
                                                }
                                                case 4:
                                                {
                                                    fldPeriodDate4.Text =
                                                        Strings.Format(rsRate.Get_Fields_DateTime("DueDate4"),
                                                            "MM/dd/yyyy");
                                                    fldPeriodAmount4.Text =
                                                        Strings.Format(dblPerPrin - dblPrincipalPaid, "#,##0.00");
                                                    dblPrincipalPaid = 0;
                                                    intPerToUse = 5;
                                                    break;
                                                }
                                            }

                                            //end switch
                                            dblPeriodTotal += (dblPerPrin - dblPrincipalPaid);
                                        }
                                    }
                                    else
                                    {
                                        fldPeriodAmount4.Text = "";
                                        fldPeriodDate4.Text = "";
                                    }
                                }
                                else
                                {
                                    fldPeriodAmount3.Text = "";
                                    fldPeriodDate3.Text = "";
                                }
                            }
                            else
                            {
                                fldPeriodAmount2.Text = "";
                                fldPeriodDate2.Text = "";
                            }
                        }
                        else if (Conversion.Val(rsBill.Get_Fields_Decimal("PrincipalPaid")) != 0)
                        {
                            fldPeriodAmount1.Text = "";
                            fldPeriodDate1.Text = "";
                            lblPeriodTitle.Visible = false;
                        }
                        else
                        {
                            fldPeriodAmount1.Text = "";
                            fldPeriodDate1.Text = "";
                            lblPeriodTitle.Visible = false;
                        }
                    }

                    // Year
                    fldYear.Text =
                        modExtraModules.FormatYear(FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear")));
                    // per diem
                    fldPD.Text = Strings.Format(dblPD, "#,##0.0000");
                    dblPerDiem += dblPD;
                    // Principal
                    fldPrincipal.Text = Strings.Format(dblBillPrin, "#,##0.00");
                    if (((rsBill.Get_Fields_Int32("LienRecordNumber"))) == 0)
                    {
                        // non lien
                        // Total
                        fldTotal.Text = Strings.Format(dblBillPrin, "#,##0.00");
                        dblPrin += dblBillPrin;
                        // dblExtraIntCost = dblExtraIntCost + dblBillInt + dblBillCost '+ dblCurrentInt
                        dblInt += dblBillInt;
                        // + dblCurrentInt
                        dblCost += dblBillCost;
                        fldInterest.Text = Strings.Format(dblBillInt, "#,##0.00");
                        fldCosts.Text = Strings.Format(dblBillCost, "#,##0.00");
                        fldTotal.Text = Strings.Format(dblBillPrin + dblBillInt + dblBillCost, "#,##0.00");
                        if (boolTransferPeriodAmounts)
                        {
                            // fill the amounts in
                            fldPeriodAmount1Int.Text = Strings.Format(dblPerInt, "#,##0.00");
                            // Format(dblBillInt, "#,##0.00")
                            fldPeriodAmount1Cost.Text = Strings.Format(dblPerCost, "#,##0.00");
                            // Format(dblCost, "#,##0.00")
                            if (Conversion.Val(fldPeriodAmount1.Text) != 0)
                            {
                                fldPeriodAmount1Total.Text = Strings.Format(
                                    FCConvert.ToDouble(fldPeriodAmount1.Text) + dblBillInt + dblBillCost, "#,##0.00");
                            }
                            else
                            {
                                fldPeriodAmount1Total.Text = Strings.Format(dblBillInt + dblBillCost, "#,##0.00");
                            }

                            dblPeriodTotal += dblBillInt + dblCost;
                            switch (intPerToUse)
                            {
                                case 1:
                                {
                                    // this means the current bill had nothing due
                                    // so do not show anything
                                    boolUsePeriodTotalLine = false;
                                    break;
                                }
                                case 2:
                                {
                                    boolUsePeriodTotalLine = true;
                                    fldPeriodAmount2Total.Text = Strings.Format(dblPeriodTotal, "#,##0.00");
                                    break;
                                }
                                case 3:
                                {
                                    boolUsePeriodTotalLine = true;
                                    fldPeriodAmount2Total.Text =
                                        Strings.Format(FCConvert.ToDouble(fldPeriodAmount2.Text), "#,##0.00");
                                    fldPeriodAmount3Total.Text = Strings.Format(dblPeriodTotal, "#,##0.00");
                                    break;
                                }
                                case 4:
                                {
                                    boolUsePeriodTotalLine = true;
                                    fldPeriodAmount2Total.Text =
                                        Strings.Format(FCConvert.ToDouble(fldPeriodAmount2.Text), "#,##0.00");
                                    fldPeriodAmount3Total.Text =
                                        Strings.Format(FCConvert.ToDouble(fldPeriodAmount3.Text), "#,##0.00");
                                    fldPeriodAmount4Total.Text = Strings.Format(dblPeriodTotal, "#,##0.00");
                                    break;
                                }
                                default:
                                {
                                    boolUsePeriodTotalLine = true;
                                    fldPeriodAmount2Total.Text =
                                        Strings.Format(FCConvert.ToDouble(fldPeriodAmount2.Text), "#,##0.00");
                                    fldPeriodAmount3Total.Text =
                                        Strings.Format(FCConvert.ToDouble(fldPeriodAmount3.Text), "#,##0.00");
                                    fldPeriodAmount4Total.Text =
                                        Strings.Format(FCConvert.ToDouble(fldPeriodAmount4.Text), "#,##0.00");
                                    fldPeriodAmountTotal.Text = Strings.Format(dblPeriodTotal, "#,##0.00");
                                    break;
                                }
                            }

                            //end switch
                            intLineNumber = intPerToUse;
                        }
                    }
                    else
                    {
                        // Interest
                        fldInterest.Text = Strings.Format(dblBillInt + dblBillPLI, "#,##0.00");
                        // + dblCurrentInt + rsLien.Fields("Interest") - rsLien.Fields("PLIPaid") +
                        // Costs
                        fldCosts.Text = Strings.Format(dblBillCost, "#,##0.00");
                        // Total
                        fldTotal.Text = Strings.Format(dblBillPrin + dblBillInt + dblBillPLI + dblBillCost, "#,##0.00");
                        // + rsLien.Fields("Interest") - rsLien.Fields("PLIPaid")
                        dblPrin += dblBillPrin;
                        dblInt += dblBillInt + dblBillPLI;
                        // + dblCurrentInt + rsLien.Fields("Interest") - rsLien.Fields("PLIPaid")
                        dblCost += dblBillCost;
                        dblExtraIntCost = dblExtraIntCost;
                        // + dblCurrentInt
                    }

                    rsBill.MoveNext();
                }
                else
                {
                    Detail.Height = 0;
                    fldYear.Text = "";
                    fldPD.Text = "";
                    fldPrincipal.Text = "";
                    fldInterest.Text = "";
                    fldCosts.Text = "";
                    fldTotal.Text = "";
                    lnTotals.Visible = false;
                    fldYear.Visible = false;
                    fldPD.Visible = false;
                    fldPrincipal.Visible = false;
                    fldInterest.Visible = false;
                    fldCosts.Visible = false;
                    fldTotal.Visible = false;
                }
            }
        }

		private void HideFields(bool boolHide)
		{
			// this sub will either hide or show the fields depending if this account has any outstanding taxes
			fldYear.Visible = !boolHide;
			fldPD.Visible = !boolHide;
			fldPrincipal.Visible = !boolHide;
			fldInterest.Visible = !boolHide;
			fldCosts.Visible = !boolHide;
			fldTotal.Visible = !boolHide;
			lblYear.Visible = !boolHide;
			lblPD.Visible = !boolHide;
			lblPrincipal.Visible = !boolHide;
			lblInterest.Visible = !boolHide;
			lblCosts.Visible = !boolHide;
			lblTotal.Visible = !boolHide;
			lnTaxes.Visible = !boolHide;
			lnTotals.Visible = !boolHide;
			lnTotal.Visible = !boolHide;
			// lnOutStanding.Visible = Not boolHide
			fldTotalPD.Visible = !boolHide;
			fldTotalPrin.Visible = !boolHide;
			fldTotalInt.Visible = !boolHide;
			fldTotalCost.Visible = !boolHide;
			fldTotalTotal.Visible = !boolHide;
 
			if (boolHide)
			{
				lblOutstandingTaxes.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				lblOutstandingTaxes.Text = "There are no outstanding taxes.";
			}
		}

		private void SetHeaderString()
		{
			// this will set the correct header for this account
			// this will fill most of the report
            using (clsDRWrapper clsTemp = new clsDRWrapper())
            {
                // VBto upgrade warning: cParty As cParty	OnWrite(cParty)
                //cParty cParty = new cParty();
                string strOwner2;
                //cPartyController gCPCtlr = new cPartyController();			
                strOwner2 = rsAcct.Get_Fields_String("DeedName2");
                //cParty = gCPCtlr.GetParty(FCConvert.ToInt32(rsAcct.Get_Fields_Int32("OwnerPartyID")));
                lblName.Text = rsAcct.Get_Fields_String("Deedname1");
                // rsAcct.Fields("RSName")
                if (strOwner2 != "")
                {
                    // kgk 05012012  lblName.Text = lblName.Text & ", " & strOwner2   'rsAcct.Fields("RSSecOwner")
                    lblName.Text = lblName.Text + " & " + strOwner2;
                }

                lblLocation.Text = Strings.Trim(FCConvert.ToString(rsAcct.Get_Fields_String("RSLocNumAlph")) + " " +
                                                FCConvert.ToString(rsAcct.Get_Fields_String("RSLocStreet")));
                lblMapLot.Text = FCConvert.ToString(rsAcct.Get_Fields_String("RSMapLot"));
                lblBookPage.Text = modGlobalFunctions.GetCurrentBookPageString(lngAcct);
                lblTreeGrowth.Text = FindTreeGrowthAcres();
                clsTemp.OpenRecordset(
                    "Select sum(piacres) as acretotal,sum(lastlandval) as landtotal,sum(lastbldgval) as bldgtotal,sum(rlexemption) as exempttotal from master where rsaccount = " +
                    FCConvert.ToString(rsAcct.Get_Fields_Int32("rsaccount")), modExtraModules.strREDatabase);
                // TODO: Field [acretotal] not found!! (maybe it is an alias?)
                lblAcreage.Text = FCConvert.ToString(clsTemp.Get_Fields("acretotal"));
                // TODO: Field [LandTotal] not found!! (maybe it is an alias?)
                lblLandValue.Text = Strings.Format(Conversion.Val(clsTemp.Get_Fields("LandTotal")), "#,##0");
                // TODO: Field [BldgTotal] not found!! (maybe it is an alias?)
                lblBuildingValue.Text = Strings.Format(Conversion.Val(clsTemp.Get_Fields("BldgTotal")), "#,##0");
                // TODO: Field [ExemptTotal] not found!! (maybe it is an alias?)
                lblExemptValue.Text = Strings.Format(Conversion.Val(clsTemp.Get_Fields("ExemptTotal")), "#,##0");
                lblValualtionValue.Text =
                    Strings.Format(
                        Conversion.Val(clsTemp.Get_Fields("LandTotal")) +
                        Conversion.Val(clsTemp.Get_Fields("BldgTotal")) -
                        Conversion.Val(clsTemp.Get_Fields("exempttotal")), "#,##0");
                lblAccountNumber.Text = lngAcct.ToString();
                lblZoning.Text = FindZoning(FCConvert.ToInt32(Conversion.Val(rsAcct.Get_Fields_Int32("PIZone"))));
                lblSFLA.Text = FindSFLA();
                if (FCUtils.IsValidDateTime(rsAcct.Get_Fields_DateTime("SaleDate")))
                {
                    // And Val(rsAcct.Fields("PIValidity")) = 1
                    lblSaleDate.Text = Strings.Format(rsAcct.Get_Fields_DateTime("SaleDate"), "MM/dd/yyyy");
                    lblSalePrice.Text = Strings.Format(rsAcct.Get_Fields_Int32("PISalePrice"), "$#,##0");
                }
                else
                {
                    lblSaleDate.Text = "";
                    lblSalePrice.Text = "";
                }

                GetExemptValues();
                lblFarm.Text = GetFarmString();
                lblOpen.Text = GetOpenSpaceString();
                int lngLastSuppYear;
                int lngLastSuppID;
                int lngCYear;
                lngLastSuppYear = 0;
                lngLastSuppID = 0;
                lngCYear = 0;
                // this will find the last mill rate from the latest regular rate record
                if (!rsBill.EndOfFile())
                {
                    if (Strings.Right(FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear")), 1) != "1")
                    {
                        if (Conversion.Val(
                            Strings.Left(FCConvert.ToString(Conversion.Val(rsBill.Get_Fields_Int32("billingyear"))),
                                4)) > lngLastSuppYear)
                        {
                            lngLastSuppYear = FCConvert.ToInt32(Math.Round(
                                Conversion.Val(Strings.Left(FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear")),
                                    4))));
                            lngLastSuppID = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
                        }

                        while (!(Strings.Right(FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear")), 1) == "1"))
                        {
                            rsBill.MoveNext();
                            if (rsBill.EndOfFile())
                            {
                                if (lngLastSuppID == 0)
                                {
                                    //goto NoneFound;
                                    NoneFound();
                                    return;
                                    break;
                                    // MAL@20080128: Tracker Reference 12142
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }

                    if (lngLastSuppID > 0)
                    {
                        if (!rsBill.EndOfFile())
                        {
                            if (lngLastSuppYear >
                                Conversion.Val(Strings.Left(FCConvert.ToString(rsBill.Get_Fields_Int32("billingyear")),
                                    4)))
                            {
                                // If Not rsBill.FindFirstRecord("ID", lngLastSuppID) Then
                                if (!rsBill.FindFirst("id = " + FCConvert.ToString(lngLastSuppID)))
                                {
                                    //goto NoneFound;
                                    NoneFound();
                                    return;
                                }
                            }
                        }
                        else
                        {
                            // If Not rsBill.FindFirstRecord("ID", lngLastSuppID) Then
                            if (!rsBill.FindFirst("id = " + FCConvert.ToString(lngLastSuppID)))
                            {
                                //goto NoneFound;
                                NoneFound();
                                return;
                            }
                        }
                    }

                    lngCYear = FCConvert.ToInt32(Math.Round(
                        Conversion.Val(Strings.Left(FCConvert.ToString(rsBill.Get_Fields_Int32("Billingyear")), 4))));
                    lblMill.Text = FindLastMillRate_2(FCConvert.ToInt32(rsBill.Get_Fields_Int32("RateKey")));
                    lblLastTax.Text = Strings.Format(
                        Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue1")) +
                        Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue2")) +
                        Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue3")) +
                        Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue4")), "#,#00.00");
                    lblLastTaxTitle.Text = "Last Billed : " +
                                           modExtraModules.FormatYear(
                                               FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear")));
                    rsBill.MoveNext();
                    lngLastSuppYear = 0;
                    lngLastSuppID = 0;
                    if (!rsBill.EndOfFile())
                    {
                        if (Strings.Right(FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear")), 1) != "1")
                        {
                            if (Conversion.Val(Strings.Left(
                                    FCConvert.ToString(Conversion.Val(rsBill.Get_Fields_Int32("Billingyear"))), 4)) >
                                lngLastSuppYear &&
                                Conversion.Val(Strings.Left(
                                    FCConvert.ToString(Conversion.Val(rsBill.Get_Fields_Int32("Billingyear"))), 4)) <
                                lngCYear)
                            {
                                lngLastSuppYear = FCConvert.ToInt32(Math.Round(Conversion.Val(
                                    Strings.Left(FCConvert.ToString(rsBill.Get_Fields_Int32("billingyear")), 4))));
                                lngLastSuppID = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
                            }

                            while (!(
                                Strings.Right(FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear")), 1) == "1" &&
                                Conversion.Val(Strings.Left(
                                    FCConvert.ToString(Conversion.Val(rsBill.Get_Fields_Int32("Billingyear"))), 4)) <
                                lngCYear))
                            {
                                rsBill.MoveNext();
                                if (rsBill.EndOfFile())
                                {
                                    if (lngLastSuppID == 0)
                                    {
                                        lblPrevYearTax.Text = "";
                                        lblPrevYearTaxTitle.Text = "";
                                    }

                                    break;
                                    // kgk 03-11-11
                                }
                            }
                        }

                        if (lngLastSuppID > 0)
                        {
                            if (!rsBill.EndOfFile())
                            {
                                if (lngLastSuppYear >
                                    Conversion.Val(
                                        Strings.Left(FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear")), 4)))
                                {
                                    // If Not rsBill.FindFirstRecord("ID", lngLastSuppID) Then
                                    if (!rsBill.FindFirst("id = " + FCConvert.ToString(lngLastSuppID)))
                                    {
                                    }
                                }
                            }
                            else
                            {
                                // If Not rsBill.FindFirstRecord("ID", lngLastSuppID) Then
                                if (!rsBill.FindFirst("id = " + FCConvert.ToString(lngLastSuppID)))
                                {
                                }
                            }
                        }

                        if (rsBill.EndOfFile() != true)
                        {
                            lblPrevYearTax.Text = Strings.Format(
                                Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue1")) +
                                Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue2")) +
                                Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue3")) +
                                Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue4")), "#,#00.00");
                            lblPrevYearTaxTitle.Text =
                                "Previous Billed : " +
                                modExtraModules.FormatYear(FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear")));
                            lblPrevMill.Text =
                                FindLastMillRate_2(FCConvert.ToInt32(rsBill.Get_Fields_Int32("RateKey")));
                        }
                        else
                        {
                            lblPrevYearTax.Text = "";
                            lblPrevYearTaxTitle.Text = "";
                            lblPrevMill.Text = "";
                        }
                    }
                    else
                    {
                        lblPrevYearTax.Text = "";
                        lblPrevYearTaxTitle.Text = "";
                    }

                    rsBill.MoveFirst();
                    dblTotalAccountDue = modCLCalculations.CalculateAccountTotal(lngAcct, true);
                    // set to always be RE for now
                    if (dblTotalAccountDue == 0)
                    {
                        // there are no outstanding bills
                        HideFields(true);
                    }
                }
                else
                {
                    NoneFound:
                    //FC:FINAL:AM: moved to method
                    NoneFound();
                    //lblMill.Text = "Not Found";
                    //lblLastTaxTitle.Text = "Last Annual Billed Amount:";
                    //lblLastTax.Text = "Not Found";
                    //HideFields(true);
                }
            }
        }

		private void NoneFound()
		{
			lblMill.Text = "Not Found";
			lblLastTaxTitle.Text = "Last Annual Billed Amount:";
			lblLastTax.Text = "Not Found";
			HideFields(true);
		}

		private void GetExemptValues()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intEx1 = 0;
				int intEx2 = 0;
				int intEx3 = 0;
				string strEx1 = "";
				string strEx2 = "";
				string strEx3 = "";
				// set the exempt codes
				modGlobalFunctions.GetExemptCodesandDescriptions("RE", lngAcct, ref intEx1, ref strEx1, ref intEx2, ref strEx2, ref intEx3, ref strEx3);
				lblEx.Visible = false;
				if (intEx1 != 0)
				{
					fldEx1.Text = Strings.Format(intEx1, "00") + " - " + strEx1;
					lblEx.Visible = true;
				}
				else
				{
					fldEx1.Text = "";
				}
				if (intEx2 != 0)
				{
					fldEx2.Text = Strings.Format(intEx2, "00") + " - " + strEx2;
					lblEx.Visible = true;
				}
				else
				{
					fldEx2.Text = "";
				}
				if (intEx3 != 0)
				{
					fldEx3.Text = Strings.Format(intEx3, "00") + " - " + strEx3;
					lblEx.Visible = true;
				}
				else
				{
					fldEx3.Text = "";
				}
				if (((rsAcct.Get_Fields_Int32("ExemptVal1"))) != 0)
				{
					fldExVal1.Text = Strings.Format(rsAcct.Get_Fields_Int32("ExemptVal1"), "#,###");
				}
				else
				{
					fldExVal1.Text = "";
				}
				if (((rsAcct.Get_Fields_Int32("ExemptVal2"))) != 0)
				{
					fldExVal2.Text = Strings.Format(rsAcct.Get_Fields_Int32("ExemptVal2"), "#,###");
				}
				else
				{
					fldExVal2.Text = "";
				}
				if (((rsAcct.Get_Fields_Int32("ExemptVal3"))) != 0)
				{
					fldExVal3.Text = Strings.Format(rsAcct.Get_Fields_Int32("ExemptVal3"), "#,###");
				}
				else
				{
					fldExVal3.Text = "";
				}
				if (fldExVal1.Text == "" && fldExVal2.Text == "" && fldExVal3.Text == "")
				{
					fldExAmountTitle.Visible = false;
					lnExempt.Visible = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Finding Exempt Values");
			}
		}

		private string FindTreeGrowthAcres()
		{
			string FindTreeGrowthAcres = "";
            using (clsDRWrapper rsTree = new clsDRWrapper())
            {
                rsTree.OpenRecordset(
                    "SELECT SUM(RSSoft) AS Soft, SUM(RSMixed) AS Mixed, SUM(RSHard) AS Hard FROM Master WHERE RSAccount = " +
                    FCConvert.ToString(lngAcct), modExtraModules.strREDatabase);
                if (!rsTree.EndOfFile())
                {
                    // TODO: Field [Soft] not found!! (maybe it is an alias?)
                    // TODO: Field [Mixed] not found!! (maybe it is an alias?)
                    // TODO: Field [Hard] not found!! (maybe it is an alias?)
                    FindTreeGrowthAcres = "Soft : " + FCConvert.ToString(rsTree.Get_Fields("Soft")) + "   Mixed : " +
                                          FCConvert.ToString(rsTree.Get_Fields("Mixed")) + "   Hard : " +
                                          FCConvert.ToString(rsTree.Get_Fields("Hard"));
                }
                else
                {
                    FindTreeGrowthAcres = "";
                }
            }

            return FindTreeGrowthAcres;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotalPD.Text = Strings.Format(dblPerDiem, "#,##0.0000");
			fldTotalPrin.Text = Strings.Format(dblPrin, "#,##0.00");
			fldTotalInt.Text = Strings.Format(dblInt, "#,##0.00");
			fldTotalCost.Text = Strings.Format(dblCost, "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblPrin + dblInt + dblCost, "#,##0.00");
			if (boolUsePeriodTotalLine)
			{
				switch (intLineNumber)
				{
					case 1:
						{
							// this means the current bill had nothing due
							// so do not show anything
							lnPeriodTotal.Visible = false;
							break;
						}
					case 2:
						{
							lnPeriodTotal.Y1 = fldPeriodAmount2.Top;
							lnPeriodTotal.Y2 = fldPeriodAmount2.Top;
							break;
						}
					case 3:
						{
							lnPeriodTotal.Y1 = fldPeriodAmount3.Top;
							lnPeriodTotal.Y2 = fldPeriodAmount3.Top;
							break;
						}
					case 4:
						{
							lnPeriodTotal.Y1 = fldPeriodAmount4.Top;
							lnPeriodTotal.Y2 = fldPeriodAmount4.Top;
							break;
						}
					default:
						{
							lnPeriodTotal.Y1 = fldPeriodAmountTotal.Top;
							lnPeriodTotal.Y2 = fldPeriodAmountTotal.Top;
							break;
						}
				}
				//end switch
			}
			else
			{
				lnPeriodTotal.Visible = false;
			}
			// fldExtraIntCost.Visible = True
			// fldExtraIntCostTitle.Visible = True
			// fldTotalOutstanding.Visible = True
			// fldTotalOutstandingTitle.Visible = True
			// lnOutStanding.Visible = True
			// 
			// fldExtraIntCost.Text = Format(dblExtraIntCost, "#,##0.00")
			// fldTotalOutstanding.Text = Format(dblPrin + dblInt + dblCost + dblExtraIntCost, "#,##0.00")
			lblSignDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			// MAL@20080130: Add Tax Information Message
			// Call Reference: 111333
			lblTaxInfoMessage.Text = GetTaxInfoMessage();
		}

		private string FindLastMillRate_2(int lngRateKey)
		{
			return FindLastMillRate(ref lngRateKey);
		}

		private string FindLastMillRate(ref int lngRateKey)
		{
			string FindLastMillRate = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
                using (clsDRWrapper rsRate = new clsDRWrapper())
                {
                    rsRate.OpenRecordset(
                        "SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(lngRateKey) +
                        " ORDER BY [Year] desc, ID desc", modExtraModules.strCLDatabase);
                    if (!rsRate.EndOfFile())
                    {
                        FindLastMillRate = Strings.Format((rsRate.Get_Fields_Double("TaxRate")) * 1000, "0.000");
                        // & " mills"    'kk10282014 trocl-1209  Mill rate rounded off, change format from "0.00"
                    }
                    else
                    {
                        FindLastMillRate = "";
                    }
                }

                return FindLastMillRate;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Finding Mill Rate");
			}
			return FindLastMillRate;
		}

		private string GetFarmString()
		{
			string GetFarmString = "";
			try
			{
                // On Error GoTo ERROR_HANDLER
                using (clsDRWrapper rsF = new clsDRWrapper())
                {
                    // VBto upgrade warning: lngCode As int	OnWrite(string)
                    int lngCode = 0;
                    // VBto upgrade warning: strCodeArray As string()	OnReadFCConvert.ToInt32(
                    string[] strCodeArray = null;
                    string strCode;
                    string strA = "";
                    string strB = "";
                    double dblTotal = 0;
                    int intCT;
                    int intIndex;
                    rsF.OpenRecordset(
                        "select * from landtype where category = " + FCConvert.ToString(CNSTLANDCATEGORYCROP) +
                        " or category = " + FCConvert.ToString(CNSTLANDCATEGORYORCHARD) + " or category = " +
                        FCConvert.ToString(CNSTLANDCATEGORYPASTURE), modExtraModules.strREDatabase);
                    strCode = "";
                    while (!rsF.EndOfFile())
                    {
                        strCode += rsF.Get_Fields_String("code") + ",";
                        rsF.MoveNext();
                    }

                    // get rid of last comma
                    if (strCode != string.Empty)
                    {
                        strCode = Strings.Mid(strCode, 1, strCode.Length - 1);
                    }

                    if (Conversion.Val(strCode) > 0)
                    {
                        strCodeArray = Strings.Split(strCode, ",", -1, CompareConstants.vbTextCompare);
                    }

                    if (Conversion.Val(strCode) > 0)
                    {
                        rsF.OpenRecordset(
                            "SELECT * FROM Master WHERE RSAccount = " + FCConvert.ToString(lngAcct) +
                            " AND (PILand1Type IN (" + strCode + ") OR PILand2Type IN (" + strCode +
                            ") OR PILand3Type IN (" + strCode + ") OR PILand4Type IN (" + strCode +
                            ") OR PILand5Type IN (" + strCode + ") OR PILand6Type IN (" + strCode +
                            ") OR PILand7Type IN (" + strCode + "))", modExtraModules.strREDatabase);
                        if (!rsF.EndOfFile())
                        {
                            while (!rsF.EndOfFile())
                            {
                                for (intCT = 1; intCT <= 7; intCT++)
                                {
                                    for (intIndex = 0; intIndex <= Information.UBound(strCodeArray, 1); intIndex++)
                                    {
                                        lngCode = FCConvert.ToInt32(strCodeArray[intIndex]);
                                        // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                        if (((rsF.Get_Fields("PILand" + FCConvert.ToString(intCT) + "Type"))) ==
                                            lngCode)
                                        {
                                            // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                            strA = FCConvert.ToString(
                                                rsF.Get_Fields("PILand" + FCConvert.ToString(intCT) + "UnitsA"));
                                            // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                            strB = FCConvert.ToString(
                                                rsF.Get_Fields("PILand" + FCConvert.ToString(intCT) + "UnitsB"));
                                            dblTotal += Conversion.Val(strA + strB) / 100;
                                            break;
                                        }
                                    }
                                }

                                rsF.MoveNext();
                            }

                            GetFarmString = Strings.Format(dblTotal, "0.00") + " acres";
                        }
                        else
                        {
                            GetFarmString = "";
                        }
                    }
                    else
                    {
                        GetFarmString = "";
                    }
                }

                return GetFarmString;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				GetFarmString = "";
			}
			return GetFarmString;
		}

		private string GetOpenSpaceString()
		{
			string GetOpenSpaceString = "";
			try
			{
                // On Error GoTo ERROR_HANDLER
                using (clsDRWrapper rsF = new clsDRWrapper())
                {
                    // VBto upgrade warning: lngCode As int	OnWrite(string)
                    int lngCode = 0;
                    // VBto upgrade warning: strCodeArray As string()	OnReadFCConvert.ToInt32(
                    string[] strCodeArray = null;
                    string strCode = "";
                    string strA = "";
                    string strB = "";
                    double dblTotal = 0;
                    int intCT;
                    int intIndex;
                    // VBto upgrade warning: lngSQFT As int	OnWrite(int, double)
                    int lngSQFT = 0;
                    int lngSQFTtoACRE;
                    lngSQFTtoACRE = 43560;
                    // constant to find acres from square feet
                    rsF.OpenRecordset("SELECT * FROM Customize", modExtraModules.strREDatabase);
                    if (!rsF.EndOfFile())
                    {
                        strCode = FCConvert.ToString(rsF.Get_Fields_String("OpenCode"));
                        if (Conversion.Val(strCode) > 0)
                        {
                            strCodeArray = Strings.Split(strCode, ",", -1, CompareConstants.vbTextCompare);
                        }
                    }
                    else
                    {
                        GetOpenSpaceString = "";
                    }

                    if (Conversion.Val(strCode) > 0)
                    {
                        rsF.OpenRecordset(
                            "SELECT * FROM Master WHERE RSAccount = " + FCConvert.ToString(lngAcct) +
                            " AND (PILand1INFCode IN (" + strCode + ") OR PILand2INFCode IN (" + strCode +
                            ") OR PILand3INFCode IN (" + strCode + ") OR PILand4INFCode IN (" + strCode +
                            ") OR PILand5INFCode IN (" + strCode + ") OR PILand6INFCode IN (" + strCode +
                            ") OR PILand7INFCode IN (" + strCode + "))", modExtraModules.strREDatabase);
                        if (!rsF.EndOfFile())
                        {
                            while (!rsF.EndOfFile())
                            {
                                for (intCT = 1; intCT <= 7; intCT++)
                                {
                                    for (intIndex = 0; intIndex <= Information.UBound(strCodeArray, 1); intIndex++)
                                    {
                                        lngCode = FCConvert.ToInt32(strCodeArray[intIndex]);
                                        // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                        if (((rsF.Get_Fields("PILand" + FCConvert.ToString(intCT) + "INFCode"))) ==
                                            lngCode)
                                        {
                                            // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                            // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                            if (Conversion.Val(
                                                    rsF.Get_Fields("PILand" + FCConvert.ToString(intCT) + "Type")) >=
                                                21 && Conversion.Val(
                                                    rsF.Get_Fields("PILand" + FCConvert.ToString(intCT) + "Type")) <=
                                                46)
                                            {
                                                // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                                strA = FCConvert.ToString(
                                                    rsF.Get_Fields("PILand" + FCConvert.ToString(intCT) + "UnitsA"));
                                                // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                                strB = FCConvert.ToString(
                                                    rsF.Get_Fields("PILand" + FCConvert.ToString(intCT) + "UnitsB"));
                                                dblTotal += Conversion.Val(strA + strB) / 100;
                                                break;
                                            }
                                            // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                            // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                            else if (Conversion.Val(
                                                         rsF.Get_Fields(
                                                             "PILand" + FCConvert.ToString(intCT) + "Type")) >= 16 &&
                                                     Conversion.Val(
                                                         rsF.Get_Fields(
                                                             "PILand" + FCConvert.ToString(intCT) + "Type")) <= 20)
                                            {
                                                // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                                strA = FCConvert.ToString(
                                                    rsF.Get_Fields("PILand" + FCConvert.ToString(intCT) + "UnitsA"));
                                                // these are concatenated strAstrB   ie 30 40 = 3,040
                                                // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                                strB = FCConvert.ToString(
                                                    rsF.Get_Fields("PILand" + FCConvert.ToString(intCT) + "UnitsB"));
                                                lngSQFT = FCConvert.ToInt32(Math.Round(Conversion.Val(strA + strB)));
                                                dblTotal += (FCConvert.ToDouble(lngSQFT) / lngSQFTtoACRE);
                                                // square feet to acres
                                                break;
                                            }
                                            // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                            // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                            else if (Conversion.Val(
                                                         rsF.Get_Fields(
                                                             "PILand" + FCConvert.ToString(intCT) + "Type")) >= 11 &&
                                                     Conversion.Val(
                                                         rsF.Get_Fields(
                                                             "PILand" + FCConvert.ToString(intCT) + "Type")) <= 15)
                                            {
                                                // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                                strA = FCConvert.ToString(
                                                    rsF.Get_Fields("PILand" + FCConvert.ToString(intCT) + "UnitsA"));
                                                // this is a measurement strA X strB  ie 300 x 400 = 120,000
                                                // TODO: Field [PILand] not found!! (maybe it is an alias?)
                                                strB = FCConvert.ToString(
                                                    rsF.Get_Fields("PILand" + FCConvert.ToString(intCT) + "UnitsB"));
                                                lngSQFT = FCConvert.ToInt32(
                                                    Conversion.Val(strA) * Conversion.Val(strB));
                                                dblTotal += (FCConvert.ToDouble(lngSQFT) / lngSQFTtoACRE);
                                                // square feet to acres
                                                break;
                                            }
                                        }
                                    }
                                }

                                rsF.MoveNext();
                            }

                            GetOpenSpaceString = Strings.Format(dblTotal, "0.00") + " acres";
                        }
                        else
                        {
                            GetOpenSpaceString = "";
                        }
                    }
                    else
                    {
                        GetOpenSpaceString = "";
                    }
                }

                return GetOpenSpaceString;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				GetOpenSpaceString = "";
			}
			return GetOpenSpaceString;
		}

		private void CheckAbatements_2186(int lngA, int lngBK, double Tax1, double Tax2, double Tax3, double Tax4, int lngRK)
		{
			CheckAbatements(ref lngA, ref lngBK, ref Tax1, ref Tax2, ref Tax3, ref Tax4, ref lngRK);
		}

		private void CheckAbatements(ref int lngA, ref int lngBK, ref double Tax1, ref double Tax2, ref double Tax3, ref double Tax4, ref int lngRK)
		{
			clsDRWrapper rsAbate = new clsDRWrapper();
			clsDRWrapper rsRK = new clsDRWrapper();
			int lngNumPer;
			double dblAdded = 0;
			AbatementPayments.Per1 = 0;
			AbatementPayments.Per2 = 0;
			AbatementPayments.Per3 = 0;
			AbatementPayments.Per4 = 0;
			if (modStatusPayments.Statics.boolRE)
			{
				rsAbate.OpenRecordset("SELECT * FROM PaymentRec WHERE Code = 'A' AND BillCode = 'R' AND Account = " + FCConvert.ToString(lngA) + " AND BillKey = " + FCConvert.ToString(lngBK), modExtraModules.strCLDatabase);
			}
			else
			{
				rsAbate.OpenRecordset("SELECT * FROM PaymentRec WHERE Code = 'A' AND BillCode = 'P' AND Account = " + FCConvert.ToString(lngA) + " AND BillKey = " + FCConvert.ToString(lngBK), modExtraModules.strCLDatabase);
			}
			while (!rsAbate.EndOfFile())
			{
				// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
				dynamic VBtoVar = rsAbate.Get_Fields("Period");
				if (VBtoVar == 1)
				{
					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
					AbatementPayments.Per1 += Conversion.Val(rsAbate.Get_Fields("Principal"));
				}
				else if (VBtoVar == 2)
				{
					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
					AbatementPayments.Per2 += Conversion.Val(rsAbate.Get_Fields("Principal"));
				}
				else if (VBtoVar == 3)
				{
					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
					AbatementPayments.Per3 += Conversion.Val(rsAbate.Get_Fields("Principal"));
				}
				else if (VBtoVar == 4)
				{
					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
					AbatementPayments.Per4 += Conversion.Val(rsAbate.Get_Fields("Principal"));
				}
				else if (VBtoVar == "A")
				{
					// find out how many periods that this bill has
					rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(lngRK), modExtraModules.strCLDatabase);
					if (!rsRK.EndOfFile())
					{
						if (Conversion.Val(rsRK.Get_Fields_Int16("NumberOfPeriods")) != 0)
						{
							lngNumPer = FCConvert.ToInt32(Math.Round(Conversion.Val(rsRK.Get_Fields_Int16("NumberOfPeriods"))));
						}
						else
						{
							lngNumPer = 1;
						}
					}
					else
					{
						lngNumPer = 1;
					}
					switch (lngNumPer)
					{
						case 1:
							{
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AbatementPayments.Per1 += Conversion.Val(rsAbate.Get_Fields("Principal"));
								break;
							}
						case 2:
							{
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								if (Tax2 >= AbatementPayments.Per2 + modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 2, 2))
								{
									// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
									AbatementPayments.Per2 += modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 2, 2);
									// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
									dblAdded += modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 2, 2);
								}
								else
								{
									AbatementPayments.Per2 += Tax2;
									dblAdded += Tax2;
								}
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AbatementPayments.Per1 += modGlobal.Round(Conversion.Val(rsAbate.Get_Fields("Principal")) - dblAdded, 2);
								break;
							}
						case 3:
							{
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								if (Tax3 >= AbatementPayments.Per3 + modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 3, 2))
								{
									// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
									AbatementPayments.Per3 += modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 3, 2);
									// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
									dblAdded += modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 3, 2);
								}
								else
								{
									AbatementPayments.Per3 += Tax3;
									dblAdded += Tax3;
								}
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								if (Tax2 >= AbatementPayments.Per2 + modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 3, 2))
								{
									// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
									AbatementPayments.Per2 += modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 3, 2);
									// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
									dblAdded += modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 3, 2);
								}
								else
								{
									AbatementPayments.Per2 += Tax2;
									dblAdded += Tax2;
								}
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AbatementPayments.Per1 += modGlobal.Round(Conversion.Val(rsAbate.Get_Fields("Principal")) - dblAdded, 2);
								break;
							}
						case 4:
							{
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								if (Tax4 >= AbatementPayments.Per4 + modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 4, 2))
								{
									// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
									AbatementPayments.Per4 += modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 4, 2);
									// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
									dblAdded = modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 4, 2);
								}
								else
								{
									AbatementPayments.Per4 += Tax4;
									dblAdded = Tax4;
								}
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								if (Tax3 >= AbatementPayments.Per3 + modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 4, 2))
								{
									// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
									AbatementPayments.Per3 += modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 4, 2);
									// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
									dblAdded += modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 4, 2);
								}
								else
								{
									AbatementPayments.Per3 += Tax3;
									dblAdded += Tax3;
								}
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								if (Tax2 >= AbatementPayments.Per2 + modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 4, 2))
								{
									// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
									AbatementPayments.Per2 += modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 4, 2);
									// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
									dblAdded += modGlobal.Round(FCConvert.ToInt32(rsAbate.Get_Fields("Principal")) / 4, 2);
								}
								else
								{
									AbatementPayments.Per2 += Tax2;
									dblAdded += Tax2;
								}
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								AbatementPayments.Per1 += modGlobal.Round(Conversion.Val(rsAbate.Get_Fields("Principal")) - dblAdded, 2);
								break;
							}
					}
					//end switch
					// split the amount across the total amount of periods
				}
				rsAbate.MoveNext();
			}
            rsRK.Dispose();
            rsAbate.Dispose();
		}
		// VBto upgrade warning: lngZone As int	OnWriteFCConvert.ToDouble(
		private string FindZoning(int lngZone, int lngTown = 0)
		{
			string FindZoning = "";
			try
			{
                // On Error GoTo ERROR_HANDLER
                using (clsDRWrapper rsLoad = new clsDRWrapper())
                {
                    rsLoad.OpenRecordset(
                        "select * from zones where code = " + FCConvert.ToString(lngZone) + " and townnumber = " +
                        FCConvert.ToString(lngTown), modExtraModules.strREDatabase);
                    if (!rsLoad.EndOfFile())
                    {
                        FindZoning = FCConvert.ToString(lngZone) + " - " +
                                     FCConvert.ToString(rsLoad.Get_Fields_String("description"));
                    }
                    else
                    {
                        FindZoning = "";
                    }
                }

                return FindZoning;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Finding Zone Code");
			}
			return FindZoning;
		}
		// VBto upgrade warning: 'Return' As string	OnWrite(string, short)
		private string FindSFLA()
		{
			string FindSFLA = "";
			try
			{
                // On Error GoTo ERROR_HANDLER
                using (clsDRWrapper rsTemp = new clsDRWrapper())
                {
                    rsTemp.OpenRecordset(
                        "SELECT * FROM Dwelling WHERE RSCard = 1 AND RSAccount = " +
                        FCConvert.ToString(rsAcct.Get_Fields_Int32("RSAccount")), modExtraModules.strREDatabase);
                    if (!rsTemp.EndOfFile())
                    {
                        FindSFLA = FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("DISfla")));
                    }
                    else
                    {
                        FindSFLA = FCConvert.ToString(0);
                    }
                }

                return FindSFLA;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Finding SFLA Amount");
			}
			return FindSFLA;
		}

		private string GetTaxInfoMessage()
		{
			string GetTaxInfoMessage = "";
			try
			{
                // On Error GoTo ERROR_HANDLER
                using (clsDRWrapper rsTemp = new clsDRWrapper())
                {
                    rsTemp.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
                    if (!rsTemp.EndOfFile())
                    {
                        GetTaxInfoMessage = FCConvert.ToString(rsTemp.Get_Fields_String("TaxInfoSheetMessage"));
                    }
                }

                return GetTaxInfoMessage;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				// MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", vbCritical, "Error Finding Tax Message"
			}
			return GetTaxInfoMessage;
		}

		
	}
}
