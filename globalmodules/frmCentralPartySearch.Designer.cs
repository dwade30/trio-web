﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmCentralPartySearch.
	/// </summary>
	partial class frmCentralPartySearch : BaseForm
	{
		public fecherFoundation.FCButton cmdNewCustomer;
		public fecherFoundation.FCFrame fraWait;
		public Wisej.Web.ProgressBar prgWait;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCTextBox txtEmail;
		public fecherFoundation.FCTextBox txtAddress;
		public fecherFoundation.FCGrid SearchGrid;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCentralPartySearch));
            this.cmdNewCustomer = new fecherFoundation.FCButton();
            this.fraWait = new fecherFoundation.FCFrame();
            this.prgWait = new Wisej.Web.ProgressBar();
            this.Label4 = new fecherFoundation.FCLabel();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.txtName = new fecherFoundation.FCTextBox();
            this.txtEmail = new fecherFoundation.FCTextBox();
            this.txtAddress = new fecherFoundation.FCTextBox();
            this.SearchGrid = new fecherFoundation.FCGrid();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcess = new fecherFoundation.FCButton();
            this.tabControl1 = new Wisej.Web.TabControl();
            this.tabPage1 = new Wisej.Web.TabPage();
            this.tabPage2 = new Wisej.Web.TabPage();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.txtLastName = new fecherFoundation.FCTextBox();
            this.fcLabel2 = new fecherFoundation.FCLabel();
            this.txtFirstName = new fecherFoundation.FCTextBox();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.cmbSearchMethod = new fecherFoundation.FCComboBox();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewCustomer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWait)).BeginInit();
            this.fraWait.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 567);
            this.BottomPanel.Size = new System.Drawing.Size(978, 79);
            // 
            // ClientArea
            // 
            this.ClientArea.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.ClientArea.Controls.Add(this.tabControl1);
            this.ClientArea.Controls.Add(this.SearchGrid);
            this.ClientArea.Controls.Add(this.fraWait);
            this.ClientArea.Dock = Wisej.Web.DockStyle.None;
            this.ClientArea.Size = new System.Drawing.Size(998, 646);
            this.ClientArea.PanelCollapsed += new System.EventHandler(this.ClientArea_PanelCollapsed);
            this.ClientArea.Controls.SetChildIndex(this.fraWait, 0);
            this.ClientArea.Controls.SetChildIndex(this.SearchGrid, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.tabControl1, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdNewCustomer);
            this.TopPanel.Controls.Add(this.cmdSearch);
            this.TopPanel.Size = new System.Drawing.Size(998, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNewCustomer, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(227, 28);
            this.HeaderText.Text = "Central Party Search";
            // 
            // cmdNewCustomer
            // 
            this.cmdNewCustomer.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNewCustomer.Location = new System.Drawing.Point(877, 29);
            this.cmdNewCustomer.Name = "cmdNewCustomer";
            this.cmdNewCustomer.Size = new System.Drawing.Size(108, 24);
            this.cmdNewCustomer.TabIndex = 7;
            this.cmdNewCustomer.Text = "New Customer";
            this.cmdNewCustomer.Click += new System.EventHandler(this.cmdNewCustomer_Click);
            // 
            // fraWait
            // 
            this.fraWait.Controls.Add(this.prgWait);
            this.fraWait.Controls.Add(this.Label4);
            this.fraWait.Location = new System.Drawing.Point(186, 81);
            this.fraWait.Name = "fraWait";
            this.fraWait.Size = new System.Drawing.Size(242, 84);
            this.fraWait.TabIndex = 10;
            this.fraWait.UseMnemonic = false;
            this.fraWait.Visible = false;
            // 
            // prgWait
            // 
            this.prgWait.Location = new System.Drawing.Point(20, 49);
            this.prgWait.Name = "prgWait";
            this.prgWait.Size = new System.Drawing.Size(203, 18);
            this.prgWait.TabIndex = 1;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(65, 25);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(158, 20);
            this.Label4.TabIndex = 2;
            this.Label4.Text = "LOADING PARTIES";
            // 
            // cmdSearch
            // 
            this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSearch.ImageSource = "button-search";
            this.cmdSearch.Location = new System.Drawing.Point(776, 29);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(81, 24);
            this.cmdSearch.TabIndex = 6;
            this.cmdSearch.Text = "Search";
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.Location = new System.Drawing.Point(101, 9);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(298, 40);
            this.txtName.TabIndex = 2;
            this.txtName.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtName_KeyPress);
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmail.Location = new System.Drawing.Point(504, 9);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(429, 40);
            this.txtEmail.TabIndex = 3;
            this.txtEmail.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtEmail_KeyPress);
            // 
            // txtAddress
            // 
            this.txtAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress.Location = new System.Drawing.Point(100, 64);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(832, 40);
            this.txtAddress.TabIndex = 4;
            this.txtAddress.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAddress_KeyPress);
            // 
            // SearchGrid
            // 
            this.SearchGrid.Cols = 10;
            dataGridViewCellStyle1.WrapMode = Wisej.Web.DataGridViewTriState.True;
            this.SearchGrid.DefaultCellStyle = dataGridViewCellStyle1;
            this.SearchGrid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.SearchGrid.ExtendLastCol = true;
            this.SearchGrid.Location = new System.Drawing.Point(30, 164);
            this.SearchGrid.Name = "SearchGrid";
            this.SearchGrid.Rows = 1;
            this.SearchGrid.Size = new System.Drawing.Size(947, 403);
            this.SearchGrid.TabIndex = 8;
            this.SearchGrid.WordWrap = true;
            this.SearchGrid.DoubleClick += new System.EventHandler(this.SearchGrid_DblClick);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(430, 23);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(47, 15);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "E-MAIL";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(12, 78);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(67, 15);
            this.Label2.TabIndex = 4;
            this.Label2.Text = "ADDRESS";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(12, 23);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(42, 15);
            this.Label1.TabIndex = 9;
            this.Label1.Text = "NAME";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(507, 17);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Process";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(30, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.PageInsets = new Wisej.Web.Padding(1, 35, 1, 1);
            this.tabControl1.Size = new System.Drawing.Size(948, 154);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.Label1);
            this.tabPage1.Controls.Add(this.Label2);
            this.tabPage1.Controls.Add(this.txtName);
            this.tabPage1.Controls.Add(this.Label3);
            this.tabPage1.Controls.Add(this.txtEmail);
            this.tabPage1.Controls.Add(this.txtAddress);
            this.tabPage1.Location = new System.Drawing.Point(1, 35);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(946, 118);
            this.tabPage1.Text = "Simple";
            this.tabPage1.PanelCollapsed += new System.EventHandler(this.tabPage1_PanelCollapsed);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.fcLabel3);
            this.tabPage2.Controls.Add(this.txtLastName);
            this.tabPage2.Controls.Add(this.fcLabel2);
            this.tabPage2.Controls.Add(this.txtFirstName);
            this.tabPage2.Controls.Add(this.fcLabel1);
            this.tabPage2.Controls.Add(this.cmbSearchMethod);
            this.tabPage2.Location = new System.Drawing.Point(1, 35);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(946, 118);
            this.tabPage2.Text = "Advanced";
            // 
            // fcLabel3
            // 
            this.fcLabel3.AutoSize = true;
            this.fcLabel3.Location = new System.Drawing.Point(439, 77);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(78, 15);
            this.fcLabel3.TabIndex = 13;
            this.fcLabel3.Text = "LAST NAME";
            this.fcLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtLastName
            // 
            this.txtLastName.BackColor = System.Drawing.SystemColors.Window;
            this.txtLastName.Location = new System.Drawing.Point(528, 63);
            this.txtLastName.Name = "txtLastName";
            this.txtLastName.Size = new System.Drawing.Size(298, 40);
            this.txtLastName.TabIndex = 7;
            // 
            // fcLabel2
            // 
            this.fcLabel2.AutoSize = true;
            this.fcLabel2.Location = new System.Drawing.Point(17, 77);
            this.fcLabel2.Name = "fcLabel2";
            this.fcLabel2.Size = new System.Drawing.Size(83, 15);
            this.fcLabel2.TabIndex = 11;
            this.fcLabel2.Text = "FIRST NAME";
            this.fcLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFirstName
            // 
            this.txtFirstName.BackColor = System.Drawing.SystemColors.Window;
            this.txtFirstName.Location = new System.Drawing.Point(106, 63);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(302, 40);
            this.txtFirstName.TabIndex = 6;
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(17, 28);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(69, 15);
            this.fcLabel1.TabIndex = 1;
            this.fcLabel1.Text = "RECORDS";
            // 
            // cmbSearchMethod
            // 
            this.cmbSearchMethod.Location = new System.Drawing.Point(105, 12);
            this.cmbSearchMethod.Name = "cmbSearchMethod";
            this.cmbSearchMethod.Size = new System.Drawing.Size(303, 40);
            this.cmbSearchMethod.TabIndex = 5;
            // 
            // frmCentralPartySearch
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(998, 706);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmCentralPartySearch";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Central Party Search";
            this.Load += new System.EventHandler(this.frmCentralPartySearch_Load);
            this.Activated += new System.EventHandler(this.frmCentralPartySearch_Activated);
            this.Resize += new System.EventHandler(this.frmCentralPartySearch_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCentralPartySearch_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewCustomer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWait)).EndInit();
            this.fraWait.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcess;
        private TabControl tabControl1;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private FCLabel fcLabel1;
        private FCComboBox cmbSearchMethod;
        public FCLabel fcLabel3;
        public FCTextBox txtLastName;
        public FCLabel fcLabel2;
        public FCTextBox txtFirstName;
    }
}
