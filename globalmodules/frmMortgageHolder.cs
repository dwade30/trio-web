﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary.Data;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWBD0000
using TWBD0000;


#elif TWBL0000
using TWBL0000;
using modExtraModules = TWBL0000.modGlobalVariables;
using modGlobal = TWBL0000.modGlobalRoutines;
using modStatusPayments = TWBL0000.modGlobalVariables;


#elif TWRE0000
using TWRE0000;
using modStatusPayments = TWRE0000.modGlobalVariables;
using modGlobal = TWRE0000.modGlobalRoutines;
using modExtraModules = TWRE0000.modGlobalVariables;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;
using modStatusPayments = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmMortgageHolder.
	/// </summary>
	public partial class frmMortgageHolder : BaseForm
	{
		public frmMortgageHolder()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmMortgageHolder InstancePtr
		{
			get
			{
				return (frmMortgageHolder)Sys.GetInstance(typeof(frmMortgageHolder));
			}
		}

		protected frmMortgageHolder _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/21/2006              *
		// ********************************************************
		object obForm;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper clsMortgageHolder = new clsDRWrapper();
		//clsDRWrapper clsRealEstate = new clsDRWrapper();
		//clsDRWrapper clsPP = new clsDRWrapper();
		private clsDRWrapper clsMortgageHolder_AutoInitialized;

		private clsDRWrapper clsMortgageHolder
		{
			get
			{
				if (clsMortgageHolder_AutoInitialized == null)
				{
					clsMortgageHolder_AutoInitialized = new clsDRWrapper();
				}
				return clsMortgageHolder_AutoInitialized;
			}
			set
			{
				clsMortgageHolder_AutoInitialized = value;
			}
		}

		clsDRWrapper clsRealEstate_AutoInitialized;

		clsDRWrapper clsRealEstate
		{
			get
			{
				if (clsRealEstate_AutoInitialized == null)
				{
					clsRealEstate_AutoInitialized = new clsDRWrapper();
				}
				return clsRealEstate_AutoInitialized;
			}
			set
			{
				clsRealEstate_AutoInitialized = value;
			}
		}

		clsDRWrapper clsPP_AutoInitialized;

		clsDRWrapper clsPP
		{
			get
			{
				if (clsPP_AutoInitialized == null)
				{
					clsPP_AutoInitialized = new clsDRWrapper();
				}
				return clsPP_AutoInitialized;
			}
			set
			{
				clsPP_AutoInitialized = value;
			}
		}

		bool boolMortgageHolder;
		// if true this is mortgage holder if false this is re account
		bool boolGridEdited;
		bool boolAllowedtoChange;
		bool boolUseREAccount;
		bool boolDirty;
		string strMortHolderComboList = "";
		// these are col definitions
		int lngColAccount;
		int lngColName;
		int lngColModule;
		int lngColEscrow;
		int lngColReceieveBill;
		int lngColHidden;
		int lngColRecordKey;
		int lngColBookPage;
		int lngNewMHNumber;
		bool boolFromSelf;
		string strLastCell = "";

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			framHoldersList.Visible = false;
		}

		private void frmMortgageHolder_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (!boolMortgageHolder)
			{

			}
			else
			{
				// mortgage holder information
			}
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				if (framHoldersList.Visible)
				{
					framHoldersList.Visible = false;
				}
				else
				{
					mnuExit_Click();
				}
			}
			else if (KeyCode == Keys.F10)
			{
				// this will turn off the F10 ID
				KeyCode = 0;
			}
		}

		private void frmMortgageHolder_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				if (Grid.Visible)
				{
					ReturnFromSearch_2(0);
				}
				else
				{
					Close();
				}
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void frmMortgageHolder_Load(object sender, System.EventArgs e)
		{
			
		}

		private void frmMortgageHolder_Resize(object sender, System.EventArgs e)
		{
			FormatGrid();
			ResizeHelpGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			DialogResult intAns;
			if (boolDirty)
			{
				intAns = FCMessageBox.Show("Changes have been made, would you like to save them?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Data Changed");
				if (intAns == DialogResult.Yes)
				{
					if (SaveInformation())
					{
					}
					else
					{
						e.Cancel = true;
					}
				}
				else if (intAns == DialogResult.No)
				{
					// let the user exit
				}
				else if (intAns == DialogResult.Cancel)
				{
					// the user does not want to exit
					e.Cancel = true;
					return;
				}
			}
			if (!boolFromSelf)
			{
				// taken out for the ability to use in CR without the form frmGetMortgage
				// If frmGetMortgage.Enabled And frmGetMortgage.Visible Then
				// frmGetMortgage.SetFocus
				// Else
				App.MainForm.Show();
				// End If
			}
		}

		private void Grid_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{

		}

		private void Grid_DblClick(object sender, EventArgs e)
		{
			if (boolFromSelf)
				return;
			if (Grid.Col == lngColReceieveBill || Grid.Col == lngColEscrow)
			{

				boolGridEdited = true;

			}
		}

		private void grid_KeyDown(object sender, KeyEventArgs e)
		{

			int KeyCode = e.KeyValue;
			int VBtoVar = Grid.Col;
			if (VBtoVar == FCConvert.ToInt32(Keys.Delete))
			{
				KeyCode = 0;
				mnuDelete_Click();
			}
			else if (VBtoVar == lngColModule)
			{
				if (e.KeyCode == Keys.U)
				{
					if (Strings.Trim(Grid.TextMatrix(Grid.Row, lngColModule)) != "UT")
					{
						Grid_DblClick(Grid, EventArgs.Empty);
					}
				}
				else if (e.KeyCode == Keys.R)
				{
					if (Strings.Trim(Grid.TextMatrix(Grid.Row, lngColModule)) != "RE")
					{
						Grid_DblClick(Grid, EventArgs.Empty);
					}
				}
			}
			else if ((VBtoVar == lngColEscrow) || (VBtoVar == lngColReceieveBill))
			{

			}
		}

		private void Grid_KeyPressEdit(int Row, int Col, ref Keys KeyAscii)
		{
			if (Col == lngColAccount)
			{
				if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return) || (KeyAscii == Keys.Escape))
				{
					// let the numbers go through
				}
				else
				{
					KeyAscii = 0;
				}
			}
		}

		private void Grid_LostFocus()
		{
			if (Grid.Rows > 1)
			{
				Grid.Select(1, 0);
			}
			else
			{
				Grid.Select(0, 0);
			}
		}

		private void GRID_MouseDown(ref Keys Button, ref short Shift, ref float X, ref float Y)
		{
			if (boolAllowedtoChange == false || boolFromSelf)
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDNone;
				return;
			}
			if (Button == Keys.RButton)
			{
				// if its the right mouse button, then lets pop up the edit menu
				if ((Grid.MouseRow > 0) && (Grid.MouseRow < Grid.Rows))
				{
					Grid.Row = Grid.MouseRow;
				}
				this.PopupMenu(mnuPop);
			}
		}

        private void Grid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
        }

        //FC:FINAL:SBE - #3831 - move code from CellFormatting to CellValueChange. CellFormatting is triggered multiple times, and MessageBox doesn't stop new trigger of cell formatting event
        private void Grid_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            int lngMR;
            int lngMC;
            clsDRWrapper rsMH = new clsDRWrapper();
            string strMHAddress = "";
            lngMR = Grid.GetFlexRowIndex(e.RowIndex);
            lngMC = Grid.GetFlexColIndex(e.ColumnIndex);
            // MAL@20081211: Add check for hover over same cell to avoid unnecessary refresh
            // Tracker Reference: 16047
            if (Grid.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                DataGridViewCell cell = Grid[e.ColumnIndex, e.RowIndex];
                if (strLastCell == FCConvert.ToString(lngMR) + "," + FCConvert.ToString(lngMC))
                {
                    // We've already done this for this cell, don't refresh
                }
                else
                {
                    cell.ToolTipText = "";
                    strLastCell = FCConvert.ToString(lngMR) + "," + FCConvert.ToString(lngMC);
                    if (lngMR > 0)
                    {
                        if (!boolMortgageHolder)
                        {
                            if (lngMR > 0 && lngMC == lngColBookPage)
                            {
                                cell.ToolTipText = Grid.TextMatrix(lngMR, lngColBookPage);
                            }
                            else if (lngColName == lngMC)
                            {
                                if (Conversion.Val(Grid.TextMatrix(lngMR, lngColAccount)) != 0)
                                {
                                    rsMH.OpenRecordset("SELECT * FROM MortgageHolders WHERE ID = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngMR, lngColAccount))), "CentralData");
                                    if (!rsMH.EndOfFile())
                                    {
                                        strMHAddress = FCConvert.ToString(rsMH.Get_Fields_String("Address1")) + " " + Strings.Trim(FCConvert.ToString(rsMH.Get_Fields_String("Address2")));
                                        if (Strings.Trim(strMHAddress) != "")
                                        {
                                            strMHAddress = Strings.Trim(strMHAddress) + ", " + FCConvert.ToString(rsMH.Get_Fields_String("City"));
                                        }
                                        else
                                        {
                                            strMHAddress = FCConvert.ToString(rsMH.Get_Fields_String("City"));
                                        }
                                        cell.ToolTipText = strMHAddress;
                                    }
                                    else
                                    {
                                        cell.ToolTipText = "";
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (lngMR > 0 && lngMC == lngColBookPage)
                            {
                                cell.ToolTipText = Grid.TextMatrix(lngMR, lngColBookPage);
                            }
                            else if (lngColName == lngMC)
                            {
                                if (Conversion.Val(Grid.TextMatrix(lngMR, lngColAccount)) != 0)
                                {
                                    rsMH.OpenRecordset("SELECT * FROM Master WHERE RSAccount = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngMR, lngColAccount))), modExtraModules.strREDatabase);
                                    // rsMH.InsertName "OwnerPartyID", "Own1", False, True, True, "", False, "", True, ""
                                    if (!rsMH.EndOfFile())
                                    {

                                        strMHAddress = rsMH.Get_Fields_String("DeedName1");
                                        // TODO: Field [Own1Address1] not found!! (maybe it is an alias?)
                                        // TODO: Field [Own1Address2] not found!! (maybe it is an alias?)
                                        strMHAddress += " " + FCConvert.ToString(rsMH.Get_Fields("Own1Address1")) + " " + Strings.Trim(FCConvert.ToString(rsMH.Get_Fields("Own1Address2")));
                                        if (Strings.Trim(strMHAddress) != "")
                                        {
                                            // TODO: Field [Own1City] not found!! (maybe it is an alias?)
                                            strMHAddress = Strings.Trim(strMHAddress) + ", " + FCConvert.ToString(rsMH.Get_Fields("Own1City"));
                                        }
                                        else
                                        {
                                            // TODO: Field [Own1City] not found!! (maybe it is an alias?)
                                            strMHAddress = FCConvert.ToString(rsMH.Get_Fields("Own1City"));
                                        }
                                        cell.ToolTipText = strMHAddress;
                                    }
                                    else
                                    {
                                        cell.ToolTipText = "";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void Grid_RowColChange(object sender, EventArgs e)
		{
			if (boolFromSelf)
				return;
			if (Grid.Row == Grid.Rows - 1 && Grid.Col == Grid.Cols - 1)
			{
				Grid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				Grid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
			if (!boolAllowedtoChange)
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDNone;
				return;
			}
			if (Grid.Row > 0)
			{
				if (Grid.Col == lngColAccount)
				{
					//ToolTip1.SetToolTip(Grid, "");
					Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					Grid.EditCell();
				}
				else if (Grid.Col == lngColName)
				{
					//ToolTip1.SetToolTip(Grid, "");
					Grid.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				else if (Grid.Col == lngColEscrow || Grid.Col == lngColReceieveBill)
				{
					//ToolTip1.SetToolTip(Grid, "");
					Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				else if (Grid.Col == lngColModule)
				{
					if (modGlobalConstants.Statics.gboolUT && (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL))
					{
                        // Grid.ToolTipText = "Double click to change from UT to RE."
                        //ToolTip1.SetToolTip(Grid, "Select RE from the List to Change from UT");
                        Grid[Grid.Col, Grid.Row - 1].ToolTipText = "Select RE from the List to Change from UT";
                    }
					// MAL@20071109
					// Grid.Editable = flexEDNone
					Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
                    Grid.EditCell();
                    // Grid.ComboList = "1;RE|2;UT"
                    // Grid.ShowComboButton = True
                }
				else if (Grid.Col == lngColBookPage)
				{
					//ToolTip1.SetToolTip(Grid, "");
					Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				else
				{
					//ToolTip1.SetToolTip(Grid, "");
					Grid.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			else
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDNone;
				//ToolTip1.SetToolTip(Grid, "");
			}
		}

		private void Grid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (Grid.CurrentCell.IsInEditMode)
			{
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = Grid.GetFlexRowIndex(e.RowIndex);
				int col = Grid.GetFlexColIndex(e.ColumnIndex);
				if (row < 1 || row >= Grid.Rows)
					return;
				boolDirty = true;
				boolGridEdited = true;
				if (col == lngColAccount)
				{
					// no other columns should be editable
					// get the account name??
					if (Strings.Trim(Grid.TextMatrix(row, lngColModule)) == "")
					{
						// get a default setting
						if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
						{
							if (GetAccountName(FCConvert.ToInt32(Conversion.Val(Grid.EditText)), row, boolMortgageHolder, false, true))
							{
								// the name has been added to the grid
							}
							else
							{
								// this account could not be found
								if (!boolMortgageHolder)
								{
									FCMessageBox.Show("This holder could not be found in the system.", MsgBoxStyle.Information, "Invalid Holder");
								}
								else
								{
									FCMessageBox.Show("This account could not be found in the system, or has been deleted.", MsgBoxStyle.Information, "Missing Account");
								}
								e.Cancel = true;
							}
						}
						else if (modGlobalConstants.Statics.gboolUT)
						{
							if (GetAccountName(FCConvert.ToInt32(Conversion.Val(Grid.EditText)), row, boolMortgageHolder, true))
							{
								// the name has been added to the grid
							}
							else
							{
								// this account could not be found
								if (boolMortgageHolder)
								{
									FCMessageBox.Show("This holder could not be found in the system.", MsgBoxStyle.Information, "Invalid Holder");
								}
								else
								{
									FCMessageBox.Show("This account could not be found in the system.", MsgBoxStyle.Information, "Missing Account");
								}
								e.Cancel = true;
							}
						}
					}
					else
					{
						if (Grid.TextMatrix(row, lngColModule) != "UT")
						{
							if (GetAccountName(FCConvert.ToInt32(Conversion.Val(Grid.EditText)), row, boolMortgageHolder, false, modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL))
							{
								// the name has been added to the grid
							}
							else
							{
								// this account could not be found
								FCMessageBox.Show("This account could not be found in the system, or has been deleted.", MsgBoxStyle.Information, "Missing Account");
								e.Cancel = true;
							}
						}
						else
						{
							if (GetAccountName(FCConvert.ToInt32(Conversion.Val(Grid.EditText)), row, boolMortgageHolder, true))
							{
								// the name has been added to the grid
							}
							else
							{
								// this account could not be found
								FCMessageBox.Show("This account could not be found in the system.", MsgBoxStyle.Information, "Missing Account");
								e.Cancel = true;
							}
						}
					}
				}
			}
		}

		private void HelpGrid_AfterSort(int Col, ref short Order)
		{
			HelpGrid.AddItem("\t" + "Add New Mortgage Holder", 1);
		}

		private void HelpGrid_BeforeSort(int Col, ref short Order)
		{
			HelpGrid.RemoveItem(1);
		}

		private void HelpGrid_DblClick(object sender, EventArgs e)
		{
			if (HelpGrid.Row < 1)
				return;
			if (HelpGrid.Row > HelpGrid.Rows - 1)
				return;
			if (HelpGrid.Row == 1)
			{
				if (CreateNewMortHolder())
				{
					Grid.AddItem("");
					Grid.TextMatrix(Grid.Rows - 1, lngColAccount, HelpGrid.TextMatrix(HelpGrid.Rows - 1, 0));
					Grid.TextMatrix(Grid.Rows - 1, lngColName, HelpGrid.TextMatrix(HelpGrid.Rows - 1, 1));
					Grid.TextMatrix(Grid.Rows - 1, lngColEscrow, FCConvert.ToString(false));
					Grid.TextMatrix(Grid.Rows - 1, lngColReceieveBill, FCConvert.ToString(false));
					Grid.TextMatrix(Grid.Rows - 1, lngColRecordKey, HelpGrid.TextMatrix(HelpGrid.Rows - 1, 0));
					Grid.TextMatrix(Grid.Rows - 1, lngColHidden, FCConvert.ToString(-1));
				}
			}
			else
			{
				Grid.AddItem("");
				Grid.TextMatrix(Grid.Rows - 1, lngColAccount, HelpGrid.TextMatrix(HelpGrid.Row, 0));
				Grid.TextMatrix(Grid.Rows - 1, lngColName, HelpGrid.TextMatrix(HelpGrid.Row, 1));
				Grid.TextMatrix(Grid.Rows - 1, lngColEscrow, FCConvert.ToString(false));
				Grid.TextMatrix(Grid.Rows - 1, lngColReceieveBill, FCConvert.ToString(false));
				Grid.TextMatrix(Grid.Rows - 1, lngColRecordKey, HelpGrid.TextMatrix(HelpGrid.Row, 0));
				Grid.TextMatrix(Grid.Rows - 1, lngColHidden, FCConvert.ToString(-1));
			}
			boolDirty = true;
			AdjustGridHeight();
			boolGridEdited = true;
			framHoldersList.Visible = false;
			Grid.Focus();
		}

		private bool CreateNewMortHolder()
		{
			bool CreateNewMortHolder = false;
			// VBto upgrade warning: frmObj As object --> As frmMortgageHolder
			frmMortgageHolder frmObj = null;
			int lngNMort;
			clsDRWrapper clsLoad = new clsDRWrapper();
			CreateNewMortHolder = false;
			frmObj = new frmMortgageHolder();
			lngNMort = frmObj.Init(0, 1, true, true);
			frmObj = null;
			if (lngNMort > 0)
			{
				CreateNewMortHolder = true;
				HelpGrid.AddItem(FCConvert.ToString(lngNMort));
				clsLoad.OpenRecordset("select * from mortgageholders where ID = " + FCConvert.ToString(lngNMort), "CentralData");
				if (!clsLoad.EndOfFile())
				{
					HelpGrid.TextMatrix(HelpGrid.Rows - 1, 1, FCConvert.ToString(clsLoad.Get_Fields_String("Name")));
				}
			}
			return CreateNewMortHolder;
		}
		// VBto upgrade warning: lngMortID As int	OnWriteFCConvert.ToDouble(
		private void EditMortHolder_2(int lngMortID)
		{
			EditMortHolder(ref lngMortID);
		}

		private void EditMortHolder(ref int lngMortID)
		{
			// VBto upgrade warning: frmObj As object --> As frmMortgageHolder
			frmMortgageHolder frmObj = null;
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngNMort;
			frmObj = new frmMortgageHolder();
			lngNMort = frmObj.Init(lngMortID, 1, true, false);
			if (lngNMort > 0)
			{
				// in case the name was edited, must reload it
				clsLoad.OpenRecordset("select * from mortgageholders where ID = " + FCConvert.ToString(lngMortID), "CentralData");
				if (!clsLoad.EndOfFile())
				{
					HelpGrid.TextMatrix(HelpGrid.Row, 1, FCConvert.ToString(clsLoad.Get_Fields_String("Name")));
				}
			}
		}

		private void HelpGrid_MouseDown(ref Keys Button, ref short Shift, ref float X, ref float Y)
		{
			if (Button == Keys.RButton)
			{
				// if its the right mouse button, then lets pop up the edit menu
				if ((HelpGrid.MouseRow > 1) && (HelpGrid.MouseRow < HelpGrid.Rows))
				{
					HelpGrid.Row = HelpGrid.MouseRow;
				}
				this.PopupMenu(mnuHelpPop);
			}
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			Grid.AddItem("");
			Grid.TextMatrix(Grid.Rows - 1, lngColEscrow, FCConvert.ToString(false));
			Grid.TextMatrix(Grid.Rows - 1, lngColReceieveBill, FCConvert.ToString(false));
			if (boolMortgageHolder)
			{
				//FC:FINAL:MSH - issue #1717: remove changing cells BackColor
				//Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Rows - 1, lngColName, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
				//Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Grid.Rows - 1, lngColBookPage, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			cmdFileDeleteRow.Enabled = true;
			AdjustGridHeight();
		}

		public void mnuAdd_Click()
		{
			mnuAdd_Click(mnuAdd, new System.EventArgs());
		}

		private void mnuChangeNumber_Click(object sender, System.EventArgs e)
		{
			// get number then update display and recordset
			clsDRWrapper clsSave = new clsDRWrapper();
			object lngReturn = 0;
			TRYAGAIN:
			;
			//FC:FINAL:DDU: #i129 fixed form dimension and text and pass input return by reference. Account no. get's value
			if (frmInput.InstancePtr.Init(ref lngReturn, "New Number", "Enter the new Mortgage Holder Number", FCConvert.ToInt32(frmInput.InstancePtr.WidthOriginal / 2F), false, modGlobalConstants.InputDTypes.idtString, "", false))
			{
				clsSave.OpenRecordset("select * from mortgageholders where ID = " + FCConvert.ToString(lngReturn), "CentralData");
				if (!clsSave.EndOfFile())
				{
					FCMessageBox.Show("That number is already in use.", MsgBoxStyle.Exclamation, "Already Exists");
					goto TRYAGAIN;
				}
				else
				{
					lblAccount.Text = FCConvert.ToString(lngReturn);
					clsMortgageHolder.Set_Fields("ID", lngReturn);
				}
			}
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			string strTemp = "";
			// check to make sure that there is a valid record to remove
			if (Grid.Row < 1)
				return;
			if (Grid.Row > Grid.Rows - 1)
				return;
			// send a different message depending on which screen is showing
			if (boolMortgageHolder)
			{
				strTemp = "Are you sure that you would like to remove account " + Grid.TextMatrix(Grid.Row, lngColAccount) + " from this mortgage holder?";
			}
			else
			{
				strTemp = "Are you sure that you would like to remove mortgageholder " + Grid.TextMatrix(Grid.Row, lngColAccount) + " from this account?";
			}
			if (FCMessageBox.Show(strTemp, MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Remove Record") == DialogResult.Yes)
			{
				Grid.RemoveItem(Grid.Row);
				boolGridEdited = true;
			}
			else
			{
				// do nothing
			}
			if (Grid.Rows <= 1)
				cmdFileDeleteRow.Enabled = false;
			AdjustGridHeight();
		}

		public void mnuDelete_Click()
		{
			mnuDelete_Click(mnuDelete, new System.EventArgs());
		}

		private void mnuDeleteMort_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsSave = new clsDRWrapper();
				int lngMortgageHolderNum;
				if (!boolMortgageHolder || !boolAllowedtoChange)
				{
					mnuDeleteMort.Visible = false;
					mnuSepar3.Visible = false;
					return;
				}
				if (Conversion.Val(clsMortgageHolder.Get_Fields_Int32("ID")) == 0)
				{
					FCMessageBox.Show("Mortgage Holder has never been saved." + "\r\n" + "Nothing to delete.", MsgBoxStyle.Information, "Nothing to Delete");
					return;
				}
				lngMortgageHolderNum = FCConvert.ToInt32(Math.Round(Conversion.Val(clsMortgageHolder.Get_Fields_Int32("ID"))));
				if (FCMessageBox.Show("This will permanently delete this mortgage holder." + "\r\n" + "Are you sure you want to delete this record?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Delete Mortgage Holder") == DialogResult.Yes)
				{
					// check to see if this mortgage holder is still linked to mortgages
					clsSave.OpenRecordset("select * from mortgageassociation where MORTGAGEHOLDERID = " + FCConvert.ToString(lngMortgageHolderNum), "CentralData");
					if (!clsSave.EndOfFile())
					{
						if (FCMessageBox.Show("There are accounts associated with this mortgage holder." + "\r\n" + "Do you still want to delete it?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Associated Accounts") == DialogResult.Yes)
						{
							modGlobalFunctions.AddCYAEntry_26("GN", "Deleted mortgage holder " + FCConvert.ToString(lngMortgageHolderNum) + " " + FCConvert.ToString(clsMortgageHolder.Get_Fields_String("Name")), "Accounts Still Associated");
							clsMortgageHolder.Delete();
							clsMortgageHolder.Update();
							while (!clsSave.EndOfFile())
							{
								// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
								modGlobalFunctions.AddCYAEntry_26("GN", "Deleted Mort Assocation due to deleted holder", "Acct " + FCConvert.ToString(clsSave.Get_Fields_String("module")) + " " + FCConvert.ToString(clsSave.Get_Fields("account")));
								clsSave.Delete();
								clsSave.Update();
								clsSave.MoveNext();
							}
							FCMessageBox.Show("Mortgage holder deleted", MsgBoxStyle.Information, "Deleted");
							boolDirty = false;
							Close();
							return;
						}
						else
						{
							return;
						}
					}
					else
					{
						modGlobalFunctions.AddCYAEntry_26("GN", "Deleted mortgage holder " + FCConvert.ToString(lngMortgageHolderNum) + " " + FCConvert.ToString(clsMortgageHolder.Get_Fields_String("Name")), "Accounts Still Associated");
						clsMortgageHolder.Delete();
						clsMortgageHolder.Update();
						FCMessageBox.Show("Mortgage holder deleted", MsgBoxStyle.Information, "Deleted");
						boolDirty = false;
						Close();
						return;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In mnuDeleteMort", MsgBoxStyle.Critical, "Error");
			}
		}

		public void mnuExit_Click()
		{
            this.Unload();
		}

		private void FormatGrid(bool boolReset = false)
		{
			int lngWidth = 0;
			if (boolReset)
			{
				Grid.Cols = 8;
				Grid.Rows = 1;
				//PPJ:FINAL:MHO:IIT#89 - move .ColDataType() to the FormatGrid so that cells are formated before inserting values
				Grid.ColDataType(lngColEscrow, FCGrid.DataTypeSettings.flexDTBoolean);
				Grid.ColDataType(lngColReceieveBill, FCGrid.DataTypeSettings.flexDTBoolean);
				Grid.TextMatrix(0, lngColEscrow, "Escrowed");
				Grid.ColAlignment(lngColEscrow, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				Grid.TextMatrix(0, lngColReceieveBill, "Get Bill");
				Grid.ColAlignment(lngColReceieveBill, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				Grid.TextMatrix(0, lngColModule, "Type");
				Grid.ColAlignment(lngColModule, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				Grid.TextMatrix(0, lngColBookPage, "Book Page");
				Grid.ColAlignment(lngColBookPage, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}
			if (!boolMortgageHolder)
			{
				lngWidth = Grid.WidthOriginal;
				Grid.ColWidth(lngColAccount, FCConvert.ToInt32(lngWidth * 0.08));
				// account
				Grid.ColWidth(lngColName, FCConvert.ToInt32(lngWidth * 0.5));
				// name
				Grid.ColWidth(lngColEscrow, FCConvert.ToInt32(lngWidth * 0.1));
				// escrowed
				Grid.ColWidth(lngColReceieveBill, FCConvert.ToInt32(lngWidth * 0.1));
				// Get Bill
				Grid.ColWidth(lngColHidden, 0);
				Grid.ColWidth(lngColModule, 0);
				Grid.ColWidth(lngColRecordKey, 0);
                Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                Grid.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                if (boolUseREAccount)
				{
					Grid.ColWidth(lngColBookPage, FCConvert.ToInt32(lngWidth * 0.12));
					Grid.ColWidth(lngColName, FCConvert.ToInt32(lngWidth * 0.48));
				}
				else
				{
					Grid.ColWidth(lngColBookPage, 0);
				}
			}
			else
			{
				lngWidth = Grid.WidthOriginal;
				Grid.ColWidth(lngColAccount, FCConvert.ToInt32(lngWidth * 0.08));
				// account
				Grid.ColWidth(lngColName, FCConvert.ToInt32(lngWidth * 0.48));
				// name
				Grid.ColWidth(lngColEscrow, FCConvert.ToInt32(lngWidth * 0.1));
				// escrowed
				Grid.ColWidth(lngColReceieveBill, FCConvert.ToInt32(lngWidth * 0.1));
				// Get Bill
				Grid.ColWidth(lngColHidden, 0);
				Grid.ColWidth(lngColModule, FCConvert.ToInt32(lngWidth * 0.1));
				Grid.ColWidth(lngColRecordKey, 0);
				Grid.ColWidth(lngColBookPage, FCConvert.ToInt32(lngWidth * 0.12));
			}
			AdjustGridHeight();
		}

		private void AdjustGridHeight()
		{

		}

		private void SetupHelpGrid()
		{
			HelpGrid.Cols = 2;
			HelpGrid.TextMatrix(0, 0, "Number");
			HelpGrid.TextMatrix(0, 1, "Mortgage Holder");
		}

		private void ResizeHelpGrid()
		{
			int GridWidth = 0;
			GridWidth = HelpGrid.WidthOriginal;
			HelpGrid.ColWidth(0, FCConvert.ToInt32(0.1 * GridWidth));
			HelpGrid.ColWidth(1, FCConvert.ToInt32(0.84 * GridWidth));
			HelpGrid.ExtendLastCol = true;
			HelpGrid.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
		}

		private void FillHelpGrid()
		{
			clsDRWrapper clsMort = new clsDRWrapper();
			try
			{
				// On Error GoTo ERROR_HANDLER
				HelpGrid.AddItem("" + "\t" + "Add New Mortgage Holder");
				clsMort.OpenRecordset("SELECT * FROM MortgageHolders ORDER BY ID", "CentralData");
				while (!clsMort.EndOfFile())
				{
					HelpGrid.AddItem(FCConvert.ToString(clsMort.Get_Fields_Int32("ID")) + "\t" + FCConvert.ToString(clsMort.Get_Fields_String("Name")));
					clsMort.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "In Fill Help Grid");
			}
		}

		private void FillGrid()
		{
			clsDRWrapper clsMortAssoc = new clsDRWrapper();
			clsDRWrapper rsBook = new clsDRWrapper();
			string strYesNo = "";
			string strYesNo2 = "";
			int lngAcct/*unused?*/;
			bool boolYesNo = false;
			bool boolYesNo2 = false;
			// fills the grid with mortgage information
			boolDirty = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				SetupGrid();
				if (boolMortgageHolder)
				{
					// this is RE/BL/CL/UT Mortgage Holder
					// this part will show a mortgage holder at the top and the mortgages held at the bottom
					// get mortgageassociation info
					if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
					{
						clsMortAssoc.OpenRecordset("SELECT * FROM " + clsMortAssoc.Get_GetFullDBName("CentralData") + ".dbo.MortgageAssociation INNER JOIN MASTER ON (MortgageAssociation.Account = MASTER.RSAccount) WHERE mortgageassociation.mortgageholderid = " + FCConvert.ToString(clsMortgageHolder.Get_Fields_Int32("ID")) + " AND Master.RSCard = 1 AND MortgageAssociation.Module = 'RE' ORDER BY MortgageAssociation.Account", modExtraModules.strREDatabase);
					}
					else
					{
						clsMortAssoc.OpenRecordset("SELECT * FROM MortgageAssociation WHERE MortgageHolderID = " + FCConvert.ToString(clsMortgageHolder.Get_Fields_Int32("ID")) + " AND Module = 'RE' ORDER BY Account", "CentralData");
					}
					while (!clsMortAssoc.EndOfFile())
					{
						// set the strings equal to the boolean values
						if (FCConvert.ToBoolean(clsMortAssoc.Get_Fields_Boolean("Escrowed")))
						{
							boolYesNo = true;
						}
						else
						{
							boolYesNo = false;
						}
						if (FCConvert.ToBoolean(clsMortAssoc.Get_Fields_Boolean("Receivebill")))
						{
							boolYesNo2 = true;
						}
						else
						{
							boolYesNo2 = false;
						}
						if (boolUseREAccount)
						{
							if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
							{
								Grid.AddItem("");
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								Grid.TextMatrix(Grid.Rows - 1, lngColAccount, FCConvert.ToString(clsMortAssoc.Get_Fields("Account")));
								Grid.TextMatrix(Grid.Rows - 1, lngColModule, "RE");
								// TODO: Field [Own1FullName] not found!! (maybe it is an alias?)
                                if (!string.IsNullOrWhiteSpace(clsMortAssoc.Get_Fields_String("DeedName2")))
                                {
                                    Grid.TextMatrix(Grid.Rows - 1, lngColName, clsMortAssoc.Get_Fields_String("DeedName1") + " & " + clsMortAssoc.Get_Fields_String("DeedName2"));
                                }
                                else
                                {
                                    Grid.TextMatrix(Grid.Rows - 1, lngColName, clsMortAssoc.Get_Fields_String("DeedName1"));
                                }
								Grid.TextMatrix(Grid.Rows - 1, lngColEscrow, FCConvert.ToString(boolYesNo));
								Grid.TextMatrix(Grid.Rows - 1, lngColReceieveBill, FCConvert.ToString(boolYesNo2));
								Grid.TextMatrix(Grid.Rows - 1, lngColBookPage, Strings.Trim(FCConvert.ToString(clsMortAssoc.Get_Fields_String("bookpage"))));
							}
							else
							{
								Grid.AddItem("");
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								Grid.TextMatrix(Grid.Rows - 1, lngColAccount, FCConvert.ToString(clsMortAssoc.Get_Fields("Account")));
								Grid.TextMatrix(Grid.Rows - 1, lngColModule, "");
								Grid.TextMatrix(Grid.Rows - 1, lngColName, "");
								Grid.TextMatrix(Grid.Rows - 1, lngColEscrow, FCConvert.ToString(boolYesNo));
								Grid.TextMatrix(Grid.Rows - 1, lngColReceieveBill, FCConvert.ToString(boolYesNo2));
								Grid.TextMatrix(Grid.Rows - 1, lngColBookPage, Strings.Trim(FCConvert.ToString(clsMortAssoc.Get_Fields_String("bookpage"))));
							}
						}
						clsMortAssoc.MoveNext();
					}
					if (Strings.UCase(App.EXEName) == "TWUT0000")
					{
						// this is UT
						clsMortAssoc.OpenRecordset("SELECT * FROM " + clsMortAssoc.Get_GetFullDBName("CentralData") + ".dbo.MortgageAssociation INNER JOIN Master ON (MortgageAssociation.Account = Master.AccountNumber) WHERE MortgageAssociation.MortgageHolderID = " + FCConvert.ToString(clsMortgageHolder.Get_Fields_Int32("ID")) + " AND MortgageAssociation.Module = 'UT' ORDER BY MortgageAssociation.Account", modExtraModules.strUTDatabase);
						// trouts-226 kjr 04.12.17 chg MortAssoc to MortgageAssociation
						while (!clsMortAssoc.EndOfFile())
						{
							// set the strings equal to the boolean values
							if (FCConvert.ToBoolean(clsMortAssoc.Get_Fields_Boolean("Escrowed")))
							{
								boolYesNo = true;
							}
							else
							{
								boolYesNo = false;
							}
							if (FCConvert.ToBoolean(clsMortAssoc.Get_Fields_Boolean("Receivebill")))
							{
								boolYesNo2 = true;
							}
							else
							{
								boolYesNo2 = false;
							}
							Grid.AddItem("");
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							Grid.TextMatrix(Grid.Rows - 1, lngColAccount, FCConvert.ToString(clsMortAssoc.Get_Fields("Account")));
							Grid.TextMatrix(Grid.Rows - 1, lngColModule, "UT");
							// TODO: Field [OwnerName] not found!! (maybe it is an alias?)
							Grid.TextMatrix(Grid.Rows - 1, lngColName, FCConvert.ToString(clsMortAssoc.Get_Fields("DeedName1")));
							Grid.TextMatrix(Grid.Rows - 1, lngColEscrow, FCConvert.ToString(boolYesNo));
							Grid.TextMatrix(Grid.Rows - 1, lngColReceieveBill, FCConvert.ToString(boolYesNo2));
							// TODO: Field [MortgageAssociation.BookPage] not found!! (maybe it is an alias?)
							Grid.TextMatrix(Grid.Rows - 1, lngColBookPage, FCConvert.ToString(clsMortAssoc.Get_Fields("MortgageAssociation.BookPage")));
							clsMortAssoc.MoveNext();
						}
					}
					if (Grid.Rows > 1)
					{
						//FC:FINAL:MSH - issue #1717: remove changing cells BackColor
						//Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, lngColName, Grid.Rows - 1, lngColName, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, lngColBookPage, Grid.Rows - 1, lngColBookPage) = TRIOCOLORGRAYEDOUTTEXTBOX
					}
				}
				else
				{
					if (!boolUseREAccount && modGlobalConstants.Statics.gboolUT)
					{
						// And Not optMod(0).Value Then
						// get the mortgage info for this account
						// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						clsMortAssoc.OpenRecordset("SELECT * FROM Mortgageassociation INNER JOIN Mortgageholders on (Mortgageassociation.MORTGAGEHOLDERID = Mortgageholders.ID) WHERE Mortgageassociation.Account = " + FCConvert.ToString(clsRealEstate.Get_Fields("AccountNumber")) + " AND Mortgageassociation.Module = 'UT' ORDER BY Mortgageassociation.Mortgageholderid", "CentralData");
					}
					else
					{
						// get the mortgage info for this account
						clsMortAssoc.OpenRecordset("SELECT * FROM Mortgageassociation INNER JOIN Mortgageholders on (Mortgageassociation.MORTGAGEHOLDERID = Mortgageholders.ID) WHERE Mortgageassociation.Account = " + FCConvert.ToString(clsRealEstate.Get_Fields_Int32("RSAccount")) + " AND Mortgageassociation.Module = 'RE' ORDER BY Mortgageassociation.Mortgageholderid", "CentralData");
					}
					while (!clsMortAssoc.EndOfFile())
					{
						// set the strings equal to the boolean values
						if (FCConvert.ToBoolean(clsMortAssoc.Get_Fields_Boolean("escrowed")))
						{
							boolYesNo = true;
						}
						else
						{
							boolYesNo = false;
						}
						if (FCConvert.ToBoolean(clsMortAssoc.Get_Fields_Boolean("receivebill")))
						{
							boolYesNo2 = true;
						}
						else
						{
							boolYesNo2 = false;
						}
						Grid.AddItem("");
						// .TextMatrix(.Rows - 1, lngColAccount) = clsMortAssoc.Get_Fields("ID")
						Grid.TextMatrix(Grid.Rows - 1, lngColAccount, FCConvert.ToString(clsMortAssoc.Get_Fields_Int32("MortgageHolderID")));
						// .TextMatrix(.rows - 1, lngColModule) = ""
						Grid.TextMatrix(Grid.Rows - 1, lngColEscrow, FCConvert.ToString(boolYesNo));
						Grid.TextMatrix(Grid.Rows - 1, lngColReceieveBill, FCConvert.ToString(boolYesNo2));
						Grid.TextMatrix(Grid.Rows - 1, lngColBookPage, FCConvert.ToString(clsMortAssoc.Get_Fields_String("bookpage")));
						if (!boolUseREAccount && modGlobalConstants.Statics.gboolUT)
						{
							Grid.TextMatrix(Grid.Rows - 1, lngColName, FCConvert.ToString(clsMortAssoc.Get_Fields_String("Name")));
							// .TextMatrix(.rows - 1, lngColBookPage) = clsMortAssoc.Get_Fields("BookPage")
						}
						else
						{
							Grid.TextMatrix(Grid.Rows - 1, lngColName, FCConvert.ToString(clsMortAssoc.Get_Fields_String("Name")));
							// .TextMatrix(.rows - 1, lngColBookPage) = PadStringWithSpaces(clsMortAssoc.Get_Fields("Book"), 5) & clsMortAssoc.Get_Fields("Page")
						}
						clsMortAssoc.MoveNext();
					}
					if (Grid.Rows > 1)
					{
						//FC:FINAL:MSH - issue #1717: remove changing cells BackColor
						//Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, lngColName, Grid.Rows - 1, lngColName, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						// Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, lngColBookPage, Grid.Rows - 1, lngColBookPage) = TRIOCOLORGRAYEDOUTTEXTBOX
					}
				}
				if (Grid.Rows > 1)
				{
					Grid.Row = 1;
					Grid.Col = lngColModule;
				}
				boolDirty = false;
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "In Fill Grid");
			}
		}

		private void mnuFileAdd_Click(object sender, System.EventArgs e)
		{
			mnuAdd_Click();
		}

		private void cmdFileDeleteRow_Click(object sender, System.EventArgs e)
		{
			mnuDelete_Click();
		}

		private void mnuFilePrintMortHolderInfo_Click(object sender, System.EventArgs e)
		{
			rptMHWithREAccounts.InstancePtr.Init(false, FCConvert.ToString(Conversion.Val(lblAccount.Text)), FCConvert.ToString(Conversion.Val(lblAccount.Text)));
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (boolAllowedtoChange == false)
					return;
				if (SaveInformation())
				{
					Close();
				}
				else
				{
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Information, "Save Error");
			}
		}

		private void mnuHelpPopEdit_Click(object sender, System.EventArgs e)
		{
			if (HelpGrid.Row > 1)
			{
				EditMortHolder_2(FCConvert.ToInt32(Conversion.Val(HelpGrid.TextMatrix(HelpGrid.Row, 0))));
			}
		}

		private void mnuMortgageHolderLabel_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(lblAccount.Text) > 0)
			{
				#if TWBL0000||TWRE0000
								                frmCustomLabels.InstancePtr.Init(FCConvert.ToInt32(lblAccount.Text), 0);
#else
				frmCustomLabels.InstancePtr.Init(lblAccount.Text, 0);
				#endif
			}
			else
			{
				FCMessageBox.Show("This record must be saved before a label can be printed", MsgBoxStyle.Exclamation, "Save First");
			}
		}

		private void mnuMortHolderList_Click(object sender, System.EventArgs e)
		{
			if (boolMortgageHolder)
				return;
			// If UCase(frmMortgageHolder.ActiveControl.Name) = "GRID" Then
			framHoldersList.Visible = true;
			// End If
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				SaveInformation();
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Information, "Save Error");
			}
		}

		private void FillInfo()
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			cParty cCP;
			try
			{
				// On Error GoTo ERROR_HANDLER
				cPartyController tPC = new cPartyController();
				cParty tP = new cParty();
                cPartyAddress tAddress = new cPartyAddress();

                txtAddress1.Text = "";
                txtAddress2.Text = "";
                txtAddress3.Text = "";
                txtCity.Text = "";
                txtState.Text = "";
                txtZip.Text = "";
                lblLocationLabel.Visible = false;
                lblLocation.Visible = false;

				if (boolMortgageHolder)
				{
					// Mort Holder
					fraMortContact.Visible = true;
					cmdMortHolderList.Visible = false;
					if (Strings.UCase(Strings.Mid(App.EXEName, 3, 2)) == "RE" || Strings.UCase(Strings.Mid(App.EXEName, 3, 2)) == "BL")
					{
						mnuMortgageHolderLabel.Visible = true;
					}
					else
					{
						mnuMortgageHolderLabel.Visible = false;
					}
					txtName.Text = FCConvert.ToString(clsMortgageHolder.Get_Fields_String("name"));
					txtAddress1.Text = FCConvert.ToString(clsMortgageHolder.Get_Fields_String("address1"));
					txtAddress2.Text = FCConvert.ToString(clsMortgageHolder.Get_Fields_String("address2"));
					txtAddress3.Text = FCConvert.ToString(clsMortgageHolder.Get_Fields_String("address3"));
					txtCity.Text = FCConvert.ToString(clsMortgageHolder.Get_Fields_String("city"));
					txtState.Text = FCConvert.ToString(clsMortgageHolder.Get_Fields_String("state"));
					txtZip.Text = FCConvert.ToString(clsMortgageHolder.Get_Fields_String("zip"));
					txtZip4.Text = FCConvert.ToString(clsMortgageHolder.Get_Fields_String("zip4"));
					lblAccount.Text = FCConvert.ToString(clsMortgageHolder.Get_Fields_Int32("ID"));
					txtContact.Text = FCConvert.ToString(clsMortgageHolder.Get_Fields_String("Contact"));
					txtPhone.Text = FCConvert.ToString(clsMortgageHolder.Get_Fields_String("Phone"));
					if (clsMortgageHolder.Get_Fields_Boolean("ReceiveCopies") == true)
					{
						chkRcvCopies.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkRcvCopies.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else if (boolUseREAccount)
				{
					// RE Account
					mnuMortgageHolderLabel.Visible = false;
					fraMortContact.Visible = false;
					cmdMortHolderList.Visible = true;
					txtName.Text = "";	
                    txtName.Text = clsRealEstate.Get_Fields_String("DeedName1");

                    tP = tPC.GetParty(clsRealEstate.Get_Fields_Int32("OwnerPartyID"));
                    if (tP != null)
                    {
                        tAddress = tP.GetPrimaryAddress();
                        if (tAddress != null)
                        {
                            txtAddress1.Text = tAddress.Address1;
                            txtAddress2.Text = tAddress.Address2;
                            txtAddress3.Text = tAddress.Address3;
                            txtCity.Text = tAddress.City;
                            txtState.Text = tAddress.State;
                            txtZip.Text = tAddress.Zip;
                        }
                    }
                    // TODO: Field [Own1Address1] not found!! (maybe it is an alias?)
                    lblAccount.Text = FCConvert.ToString(clsRealEstate.Get_Fields_Int32("rsaccount"));
                    lblLocationLabel.Visible = true;
                    lblLocation.Visible = true;
                    lblLocationLabel.Text = clsRealEstate.Get_Fields_String(("rslocapt")) + clsRealEstate.Get_Fields_String("rslocnumalph") + " " + clsRealEstate.Get_Fields_String("rslocstreet");
                }
                else
				{
					// UT Account
					fraMortContact.Visible = false;
					cmdMortHolderList.Visible = true;
					mnuMortgageHolderLabel.Visible = false;
					txtName.Text = clsRealEstate.Get_Fields_String("DeedName1");
                    tP = tPC.GetParty(clsRealEstate.Get_Fields_Int32("OwnerPartyID"));
                    if (tP != null)
                    {
                        tAddress = tP.GetPrimaryAddress();
                        if (tAddress != null)
                        {
                            txtAddress1.Text = tAddress.Address1;
                            txtAddress2.Text = tAddress.Address2;
                            txtAddress3.Text = tAddress.Address3;
                            txtCity.Text = tAddress.City;
                            txtState.Text = tAddress.State;
                            txtZip.Text = tAddress.Zip;
                        }
                    }
                    lblAccount.Text = FCConvert.ToString(clsRealEstate.Get_Fields("AccountNumber"));
                    lblLocationLabel.Visible = true;
                    lblLocation.Visible = true;
                    lblLocationLabel.Text = clsRealEstate.Get_Fields_Int32(("streetnumber")) + " " + clsRealEstate.Get_Fields_String("StreetName");

                }
                return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "In Fill Info");
			}
		}

		private void SetupGrid()
		{
			// this will create a list of MH for the grid dropdown
			if (!boolMortgageHolder)
			{
				clsDRWrapper rsMH = new clsDRWrapper();
				strMortHolderComboList = "";
				rsMH.OpenRecordset("SELECT * FROM MortgageHolders", "CentralData");
				while (!rsMH.EndOfFile())
				{
					// strMortHolderComboList = strMortHolderComboList & "#" & PadToString(rsMH.Get_Fields("ID"), 4) & ";" & PadToString(rsMH.Get_Fields("ID"), 4) & vbTab & rsMH.Get_Fields("Name") & "|"
					strMortHolderComboList += "#" + FCConvert.ToString(rsMH.Get_Fields_Int32("ID")) + ";" + FCConvert.ToString(rsMH.Get_Fields_Int32("ID")) + "\t" + FCConvert.ToString(rsMH.Get_Fields_String("Name")) + "|";
					rsMH.MoveNext();
				}
				// this will take the last pipe off the end of the string
				if (strMortHolderComboList.Length > 0)
				{
					strMortHolderComboList = Strings.Left(strMortHolderComboList, strMortHolderComboList.Length - 1);
				}
				Grid.ColComboList(lngColAccount, strMortHolderComboList);
			}
			else
			{
				Grid.ColComboList(lngColAccount, "");
			}
            //FC:FINAL:AM:#3418 - define combolist without numbers; they are not used
			//Grid.ColComboList(lngColModule, "0;RE|1;UT");
            Grid.ColComboList(lngColModule, "RE|UT");
            //FC:FINAL:MSH - issue #1717: change alignment of 'Number' and 'Mortgage Holder' columns
            Grid.ColAlignment(lngColModule, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            Grid.ColAlignment(lngColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(lngColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}
		// VBto upgrade warning: lngMortgageHolder As int	OnWrite(int, string, double)
		public int Init(int lngMortgageHolder, short intTypeofAccount, bool boolREAccounts = true, bool boolRecursive = false)
		{
			int Init = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// boolshowmortgageholder is true if showing a mortgage holder and false if showing a real estate account
				lngColAccount = 2;
				lngColName = 3;
				lngColModule = 1;
				lngColEscrow = 6;
				lngColReceieveBill = 7;
				lngColHidden = 5;
				lngColRecordKey = 0;
				lngColBookPage = 4;
				boolAllowedtoChange = true;
				boolDirty = false;
				boolUseREAccount = boolREAccounts;
				boolFromSelf = boolRecursive;
				mnuFilePrintMortHolderInfo.Visible = false;
				switch (intTypeofAccount)
				{
					case 1:
						{
							// Mort
							boolMortgageHolder = true;
							lblAccountType.Text = "Mortgage Holder:";
							this.HeaderText.Text = "Mortgage Holder";
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "MHLastMHNumber", modGlobal.PadToString(lngMortgageHolder, 6));
							mnuFilePrintMortHolderInfo.Visible = true;
							break;
						}
					case 2:
						{
							// RE
							boolMortgageHolder = false;
							lblAccountType.Text = "Real Estate Account:";
							this.HeaderText.Text = "Real Estate Account";
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "MHLastRENumber", modGlobal.PadToString(lngMortgageHolder, 6));
							break;
						}
					case 3:
						{
							// UT
							boolMortgageHolder = false;
							lblAccountType.Text = "Utility Account:";
							this.HeaderText.Text = "Utility Account";
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "MHLastUTNumber", modGlobal.PadToString(lngMortgageHolder, 6));
							break;
						}
				}
                //FC:FINAL:AM: moved code from load
                clsDRWrapper rsMH = new clsDRWrapper();
                boolGridEdited = false;
                SetupHelpGrid();
                FillHelpGrid();
                FormatGrid(true);
                this.lblAccount.Left = this.HeaderText.Right + 20;
				//end switch
				if (!boolMortgageHolder || boolRecursive)
				{
					mnuDeleteMort.Visible = false;
					mnuSepar3.Visible = false;
				}
				else
				{
					mnuDeleteMort.Visible = true;
					mnuSepar3.Visible = true;
				}
				if (boolMortgageHolder)
				{
					lblMortgageHolder.Text = "Mortgages Held";
					Grid.TextMatrix(0, lngColAccount, "Account");
					Grid.TextMatrix(0, lngColName, "Name");
					Grid.TextMatrix(0, lngColEscrow, "Escrowed");
					Grid.TextMatrix(0, lngColReceieveBill, "Receive Bill");
					//FC:FINAL:CHN: Allow to input numbers at Grid columns on client side (javascript).
					Grid.EditingControlShowing -= Grid_EditingControlShowing;
					Grid.EditingControlShowing += Grid_EditingControlShowing;
				}
				else
				{
					lblMortgageHolder.Text = "Mortgage Holders";
					Grid.TextMatrix(0, lngColAccount, "Number");
					Grid.TextMatrix(0, lngColName, "Mortgage Holder");
					Grid.TextMatrix(0, lngColEscrow, "Escrowed");
					Grid.TextMatrix(0, lngColReceieveBill, "Receive Bill");
				}

				mnuChangeNumber.Enabled = false;
				mnuChangeNumber.Visible = false;
                if (boolMortgageHolder)
				{
					clsMortgageHolder.OpenRecordset("SELECT * FROM MortgageHolders WHERE ID = " + FCConvert.ToString(lngMortgageHolder), "CentralData");
					if (lngMortgageHolder == 0)
					{
						// this is to be added
						clsMortgageHolder.AddNew();
                        //FC:FINAL:CHN - issue #1576: Not show not enabled menu item at menu (redesign).
                        mnuChangeNumber.Enabled = true;
						mnuChangeNumber.Visible = true;
                        chkRcvCopies.CheckState = Wisej.Web.CheckState.Checked;
						SetupGrid();
					}
					else
					{
						if (!clsMortgageHolder.EndOfFile())
						{
							// already exists so just edit it
							clsMortgageHolder.Edit();
							FillGrid();
							FillInfo();
						}
						else
						{
							// program sent an id that is invalid. This should never be able to happen
							FCMessageBox.Show("Mortgage Holder number " + FCConvert.ToString(lngMortgageHolder) + " doesn't exist.", MsgBoxStyle.Exclamation, "Not Found");
							Close();
							return Init;
						}
					}
					lblAccount.Text = FCConvert.ToString(clsMortgageHolder.Get_Fields_Int32("ID"));
					// MAL@20080723: Add Receive Copies Option to Mortgage Holders
					// Tracker Reference: 13813
					chkRcvCopies.Visible = true;
				}
				else
				{
					// disable the textboxes.  They can't edit realestate from here
					mnuChangeNumber.Visible = false;
					txtName.Enabled = false;
					txtAddress1.Enabled = false;
					txtAddress2.Enabled = false;
					txtAddress3.Enabled = false;
					txtCity.Enabled = false;
					txtState.Enabled = false;
					txtZip.Enabled = false;
					txtZip4.Enabled = false;
					if (boolUseREAccount)
					{
						clsRealEstate.OpenRecordset("SELECT * FROM Master WHERE RSAccount = " + FCConvert.ToString(lngMortgageHolder) + " AND RSCard = 1", modExtraModules.strREDatabase);
						// clsRealEstate.InsertName "OwnerPartyID,SecOwnerPartyID", "Own1,Own2", False, True, True, "", False, "", True, ""
						if (!clsRealEstate.EndOfFile())
						{
							FillGrid();
							FillInfo();
						}
						else
						{
							FCMessageBox.Show("Real Estate account " + FCConvert.ToString(lngMortgageHolder) + " doesn't exist.", MsgBoxStyle.Exclamation, "Not Found");
							Close();
							return Init;
						}
					}
					else
					{
						// UT
						clsRealEstate.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + FCConvert.ToString(lngMortgageHolder), modExtraModules.strUTDatabase);
						// clsRealEstate.InsertName "OwnerPartyID", "Own1", False, True, True, "", False, "", True, ""
						if (!clsRealEstate.EndOfFile())
						{
							FillGrid();
							FillInfo();
						}
						else
						{
							FCMessageBox.Show("Utility account " + FCConvert.ToString(lngMortgageHolder) + " doesn't exist.", MsgBoxStyle.Exclamation, "Not Found");
							Close();
							return Init;
						}
					}
					// MAL@20080723
					chkRcvCopies.Visible = false;
				}
				//FC:FINAL:AM:#i361 - hide the menu if all menu items are hidden
				if (!this.mnuChangeNumber.Visible && !this.mnuMortgageHolderLabel.Visible && !this.mnuDeleteMort.Visible && !this.mnuFilePrintMortHolderInfo.Visible)
				{
					this.Menu = null;
				}
				if (boolRecursive)
				{
					Init = 0;
					// GRID.Visible = False       'Jim B 10/22/2003 Took this out so that the grid would be visible to show the lists of mortgages held
					Grid.Editable = FCGrid.EditableSettings.flexEDNone;
					if (Grid.Rows > 1)
					{
						//FC:FINAL:MSH - issue #1717: remove changing cells BackColor
						//Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, Grid.Rows - 1, lngColReceieveBill, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
					}
					this.Show(FormShowEnum.Modal);
					Init = lngNewMHNumber;
				}
				boolDirty = false;
				return Init;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Init Error");
			}
			return Init;
		}

		private void txtAddress1_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtAddress2_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtAddress3_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtCity_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtCity_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii >= 65 && KeyAscii <= 90) || (KeyAscii >= 96 && KeyAscii <= 122) || (KeyAscii == 8) || (KeyAscii == 45) || (KeyAscii == 46) || (KeyAscii == 32))
			{
				// allow all of these characters
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtContact_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtName_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtPhone_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtState_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtState_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{

		}

		private void txtZip_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtZip_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtZip4_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtZip4_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			e.KeyChar = Strings.Chr(KeyAscii);
		}
		// VBto upgrade warning: lngAcct As int	OnWriteFCConvert.ToDouble(
		private bool GetAccountName(int lngAcct, int lngRW, bool boolMH, bool boolUT = false, bool boolOverRideRE = false)
		{
			bool GetAccountNameRet = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will query the RE database and get the name from the account
				// If there is no RE database, then it will check the last bill in the collections file
				// if there are no matching accounts, then nothing will be displayed and the account number will not be validated
				clsDRWrapper rsREAcct = new clsDRWrapper();
				clsDRWrapper rsBook = new clsDRWrapper();
				string strName = "";
				cPartyController tPartCont = new cPartyController();
				cParty tParty;
				if (lngAcct > 0)
				{
					if (boolMH)
					{
						if ((modStatusPayments.Statics.boolRE && !boolUT) || boolOverRideRE)
						{
							// does the user have RE if so, then get the name from there
							rsREAcct.OpenRecordset("SELECT rsaccount,ownerpartyid,secownerpartyid,deedname1,deedname2 FROM Master WHERE RSAccount = " + FCConvert.ToString(lngAcct), modExtraModules.strREDatabase);
							// rsdeleted <> 1 and
							// rsREAcct.InsertName "OwnerPartyID,SecOwnerPartyID", "Own1,Own2", False, True, True, "", False, "", True, ""
							if (!rsREAcct.EndOfFile())
                            {
                                strName = rsREAcct.Get_Fields_String("DeedName1");
                                if (!String.IsNullOrWhiteSpace(rsREAcct.Get_Fields_String("DeedName2")))
                                {
                                    strName += " & " + rsREAcct.Get_Fields_String("DeedName2");
                                }
								Grid.TextMatrix(lngRW, lngColName, strName);
								rsBook.OpenRecordset("SELECT * FROM BookPage WHERE Account = " + FCConvert.ToString(lngAcct) + " AND Card = 1 AND [Current] = 1 ORDER BY Line", modExtraModules.strREDatabase);
								if (rsBook.EndOfFile())
								{
									Grid.TextMatrix(lngRW, lngColBookPage, "");
								}
								else
								{
									// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
									// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
									Grid.TextMatrix(lngRW, lngColBookPage, modGlobalFunctions.PadStringWithSpaces(FCConvert.ToString(rsBook.Get_Fields("Book")), 5) + FCConvert.ToString(rsBook.Get_Fields("Page")));
								}
								GetAccountNameRet = true;
							}
							else
							{
								// no account in RE master table
								Grid.TextMatrix(lngRW, lngColName, "");
								GetAccountNameRet = false;
							}
						}
						else if (boolUT && modGlobalConstants.Statics.gboolUT)
						{
							rsREAcct.OpenRecordset("SELECT ownerpartyid,deedname1,reaccount FROM Master WHERE AccountNumber = " + FCConvert.ToString(lngAcct), modExtraModules.strUTDatabase);
							// rsREAcct.InsertName "OwnerPartyID", "Own1", False, True, True, "", False, "", True, ""
							if (!rsREAcct.EndOfFile())
							{
								// If rsREAcct.EndOfFile <> True And rsREAcct.BeginningOfFile <> True Then
								if (((rsREAcct.Get_Fields_Int32("REAccount"))) == 0 || !(modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL))
								{
                                    Grid.TextMatrix(lngRW, lngColName, rsREAcct.Get_Fields_String("DeedName1"));                                    
									GetAccountNameRet = true;
								}
								else if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
								{
									// this needs to call this function for its real estate account
									// tell the user that this is a linked account
									FCMessageBox.Show("This Utility Account is linked to Real Estate account #" + FCConvert.ToString(rsREAcct.Get_Fields_Int32("REAccount")) + ".", MsgBoxStyle.Information, "Linked Account");
									Grid.TextMatrix(lngRW, lngColAccount, FCConvert.ToString(rsREAcct.Get_Fields_Int32("REAccount")));
									Grid.EditText = FCConvert.ToString(rsREAcct.Get_Fields_Int32("REAccount"));
									GetAccountNameRet = GetAccountName(FCConvert.ToInt32(rsREAcct.Get_Fields_Int32("REAccount")), lngRW, true, false, true);
									Grid.TextMatrix(lngRW, lngColModule, "RE");
									// kgk 12-08-2011 trout-785
									return GetAccountNameRet;
								}
							}
							else
							{
								// no account in UT Master table
								Grid.TextMatrix(lngRW, lngColName, "");
								GetAccountNameRet = false;
							}
							// Grid.TextMatrix(lngRW, lngColModule) = "UT"
						}
						else if (modGlobalConstants.Statics.gboolCL)
						{
							// else get the name from the last CL Bill
							rsREAcct.OpenRecordset("SELECT name1,name2 FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
							if (rsREAcct.EndOfFile() != true && rsREAcct.BeginningOfFile() != true)
							{
								if (Strings.Trim(FCConvert.ToString(rsREAcct.Get_Fields_String("Name2"))) != "")
								{
									Grid.TextMatrix(lngRW, lngColName, FCConvert.ToString(rsREAcct.Get_Fields_String("Name1")));
								}
								else
								{
									Grid.TextMatrix(lngRW, lngColName, FCConvert.ToString(rsREAcct.Get_Fields_String("Name1")) + " & " + FCConvert.ToString(rsREAcct.Get_Fields_String("Name2")));
								}
								GetAccountNameRet = true;
								// Grid.TextMatrix(lngRW, lngColModule) = "RE"
							}
							else
							{
								// no account in BillingMaster
								Grid.TextMatrix(lngRW, lngColName, "");
								GetAccountNameRet = false;
							}
						}
					}
					else
					{
						// get the mortgage holder information
						rsREAcct.OpenRecordset("SELECT * FROM MortgageHolders WHERE ID = " + FCConvert.ToString(lngAcct), "CentralData");
						if (rsREAcct.EndOfFile() != true && rsREAcct.BeginningOfFile() != true)
						{
							Grid.TextMatrix(lngRW, lngColName, FCConvert.ToString(rsREAcct.Get_Fields_String("name")));
							GetAccountNameRet = true;
						}
						else
						{
							// no account in mortgageholder table
							// Grid.TextMatrix(lngRW, lngColName) = ""
							GetAccountNameRet = false;
						}
					}
				}
				else
				{
					if (boolMortgageHolder)
					{
						if (Grid.Col == 0)
						{
							// If Grid.TextMatrix(lngRW, lngColModule) = "RE" And gboolUT Then
							// Grid.TextMatrix(lngRW, lngColModule) = "UT"
							// Else
							// Grid.TextMatrix(lngRW, lngColModule) = "RE"
							// End If
						}
					}
					else
					{
					}
					GetAccountNameRet = true;
				}
				return GetAccountNameRet;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Account Name");
			}
			return GetAccountNameRet;
		}

		private bool CheckForMultiples()
		{
			bool CheckForMultiples = false;
			// this function will check to make sure that there is only one of each account/mortgage holder
			// it will return true if there are no multiples and false otherwise
			int lngRow/*unused?*/;
			int lngAcct/*unused?*/;
			int lngRWCT;
			int lngMatchRow = 0;
			BeginAgain:
			;
			CheckForMultiples = true;
			if (Grid.Rows > 1)
			{
				for (lngRWCT = 1; lngRWCT <= Grid.Rows - 1; lngRWCT++)
				{
					lngMatchRow = Grid.FindRow(Grid.TextMatrix(lngRWCT, lngColAccount), lngRWCT + 1, lngColAccount);
					if (lngMatchRow >= 0)
					{
						if (boolMortgageHolder)
						{
							// also check the module
							if (Grid.TextMatrix(lngRWCT, lngColModule) == Grid.TextMatrix(lngMatchRow, lngColModule))
							{
								if (Grid.TextMatrix(lngRWCT, lngColBookPage) == Grid.TextMatrix(lngMatchRow, lngColBookPage))
								{
									Grid.RemoveItem(lngMatchRow);
									goto BeginAgain;
								}
							}
						}
						else
						{
							if (Grid.TextMatrix(lngRWCT, lngColBookPage) == Grid.TextMatrix(lngMatchRow, lngColBookPage))
							{
								CheckForMultiples = false;
								break;
							}
						}
					}
					else
					{
					}
				}
			}
			else
			{
				CheckForMultiples = true;
			}
			return CheckForMultiples;
		}

		private bool SaveInformation()
		{
			bool SaveInformation = false;
			try
			{
				clsDRWrapper clsSave = new clsDRWrapper();
				// used to save to database
				clsDRWrapper clsTemp = new clsDRWrapper();
				// used for various things
				clsDRWrapper clsUT = new clsDRWrapper();
				// used for saving UT
				int X;
				// loop counter
				string strEscrow = "";
				string strGetBill = "";
				string strMod = "";
				bool boolNoMultiples;
				string strGetBookPage = "";
				string strType = "";
				// select the first row/col of the grid
				Grid.Select(0, 0);
				boolNoMultiples = CheckForMultiples();
				if (boolNoMultiples)
				{
					if (boolMortgageHolder)
					{
						// save the mortgage holder information first
						clsMortgageHolder.Set_Fields("Name", Strings.Trim(txtName.Text));
						clsMortgageHolder.Set_Fields("Address1", Strings.Trim(txtAddress1.Text));
						clsMortgageHolder.Set_Fields("Address2", Strings.Trim(txtAddress2.Text));
						clsMortgageHolder.Set_Fields("Address3", Strings.Trim(txtAddress3.Text));
						clsMortgageHolder.Set_Fields("City", Strings.Trim(txtCity.Text));
						clsMortgageHolder.Set_Fields("State", Strings.Trim(txtState.Text));
						clsMortgageHolder.Set_Fields("Zip", Strings.Trim(txtZip.Text));
						clsMortgageHolder.Set_Fields("Zip4", Strings.Trim(txtZip4.Text));
						clsMortgageHolder.Set_Fields("Contact", Strings.Trim(txtContact.Text));
						clsMortgageHolder.Set_Fields("Phone", Strings.Trim(txtPhone.Text));
						// MAL@20080723: Add Receive Copies option
						// Tracker Reference: 13813
						clsMortgageHolder.Set_Fields("ReceiveCopies", FCConvert.CBool(chkRcvCopies.CheckState == Wisej.Web.CheckState.Checked));
                        // lngNewMHNumber = clsMortgageHolder.Get_Fields("id")
                        //FC:FINAL:CHN - issue #1576: Not show not enabled menu item at menu (redesign).
                        mnuChangeNumber.Enabled = false;
						mnuChangeNumber.Visible = false;
                        clsMortgageHolder.Update();
						lngNewMHNumber = FCConvert.ToInt32(clsMortgageHolder.Get_Fields_Int32("id"));
						lblAccount.Text = FCConvert.ToString(lngNewMHNumber);
						clsMortgageHolder.MoveFirst();
						clsMortgageHolder.Edit();
						if (boolGridEdited)
						{
							// now save the association stuff in the grid
							clsSave.Execute("DELETE FROM MortgageAssociation WHERE MortgageHolderID = " + FCConvert.ToString(clsMortgageHolder.Get_Fields_Int32("ID")), "CentralData");
							// do this only if there is a change
							if (Grid.Rows > 1)
							{
								// If gboolRE Or gboolBL Then
								// clsTemp.OpenRecordset "SELECT RSAccount FROM Master WHERE RSCard = 1", strREDatabase 'AND rsdeleted <> 1
								// End If
								if (modGlobalConstants.Statics.gboolUT)
								{
									clsUT.OpenRecordset("SELECT * FROM Master WHERE ISNULL(Deleted,0) = 0", modExtraModules.strUTDatabase);
								}
								for (X = 1; X <= Grid.Rows - 1; X++)
								{
									if (Grid.TextMatrix(X, lngColModule) != "UT")
									{
										// This will save RE accounts
										if (Conversion.Val(Grid.TextMatrix(X, lngColAccount)) > 0)
										{
											clsTemp.OpenRecordset("SELECT RSAccount FROM Master WHERE RSCard = 1 and rsaccount = " + Grid.TextMatrix(X, lngColAccount), modExtraModules.strREDatabase);
											// clsTemp.FindFirstRecord "RSAccount", Grid.TextMatrix(x, lngColAccount)
											// clsTemp.FindFirstRecord "Rsaccount", Val(Grid.TextMatrix(x, lngColAccount))
											if (clsTemp.EndOfFile())
											{
												FCMessageBox.Show("Real Estate Account " + Grid.TextMatrix(X, lngColAccount) + " doesn't exist or has been deleted." + "\r\n" + "This line will not be saved.", MsgBoxStyle.Exclamation, "Invalid Account");
											}
											else
											{
												if (FCConvert.CBool(Strings.UCase(Grid.TextMatrix(X, lngColEscrow))) == true)
												{
													strEscrow = "1";
												}
												else
												{
													strEscrow = "0";
												}
												if (FCConvert.CBool(Strings.UCase(Grid.TextMatrix(X, lngColReceieveBill))) == true)
												{
													strGetBill = "1";
												}
												else
												{
													strGetBill = "0";
												}
												clsTemp.Execute("INSERT INTO MortgageAssociation (MortgageHolderID,Account,BookPage,Escrowed,Receivebill,Module) VALUES (" + FCConvert.ToString(clsMortgageHolder.Get_Fields_Int32("ID")) + "," + Grid.TextMatrix(X, lngColAccount) + ",'" + Grid.TextMatrix(X, lngColBookPage) + "'," + strEscrow + "," + strGetBill + ",'RE')", "CentralData");
											}
										}
									}
									else
									{
										// This will save UT accounts
										if (Conversion.Val(Grid.TextMatrix(X, lngColAccount)) > 0)
										{
											// clsUT.FindFirstRecord "AccountNumber", Grid.TextMatrix(x, lngColAccount)
											clsUT.FindFirst("AccountNumber = " + Grid.TextMatrix(X, lngColAccount));
											if (clsUT.NoMatch)
											{
												FCMessageBox.Show("Utility Account " + Grid.TextMatrix(X, lngColAccount) + " doesn't exist or has been deleted." + "\r\n" + "This line will not be saved.", MsgBoxStyle.Exclamation, "Invalid Account");
											}
											else
											{
												//FC:FINAL:AM #1054 - bug in original code; use the correct column                                                
												//if (FCConvert.CBool(Strings.UCase(Grid.TextMatrix(X, 2))) == true)
												if (FCConvert.CBool(Strings.UCase(Grid.TextMatrix(X, lngColEscrow))) == true)
												{
													strEscrow = "1";
												}
												else
												{
													strEscrow = "0";
												}
												//FC:FINAL:AM #1054 - bug in original code; use the correct column  
												//if (FCConvert.CBool(Strings.UCase(Grid.TextMatrix(X, 3))) == true)
												if (FCConvert.CBool(Strings.UCase(Grid.TextMatrix(X, lngColReceieveBill))) == true)
												{
													strGetBill = "1";
												}
												else
												{
													strGetBill = "0";
												}
												clsUT.Execute("INSERT INTO MortgageAssociation (MortgageHolderID,Account,Escrowed,Receivebill,Module) VALUES (" + FCConvert.ToString(clsMortgageHolder.Get_Fields_Int32("ID")) + "," + Grid.TextMatrix(X, lngColAccount) + "," + strEscrow + "," + strGetBill + ",'UT')", "CentralData");
											}
										}
									}
								}
								// X
							}
						}
						SaveInformation = true;
						boolDirty = false;
						FCMessageBox.Show("Mortgage holder #" + lblAccount.Text + " has been successfully saved.", MsgBoxStyle.Information, "Save Mortgage Holder");
					}
					else
					{
						if (boolGridEdited)
						{
							if (boolUseREAccount)
							{
								clsSave.Execute("DELETE FROM MortgageAssociation WHERE Account = " + FCConvert.ToString(clsRealEstate.Get_Fields_Int32("RSAccount")) + " AND Module = 'RE'", "CentralData");
								strMod = "RE";
							}
							else
							{
								// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
								clsSave.Execute("DELETE FROM MortgageAssociation WHERE Account = " + FCConvert.ToString(clsRealEstate.Get_Fields("AccountNumber")) + " AND Module = 'UT'", "CentralData");
								strMod = "UT";
							}
							if (Grid.Rows > 1)
							{
								clsTemp.OpenRecordset("SELECT * FROM MortgageHolders ORDER BY ID", "CentralData");
								for (X = 1; X <= Grid.Rows - 1; X++)
								{
									// make sure the mortgage number is valid
									if (Strings.Trim(Grid.TextMatrix(X, lngColAccount)) != string.Empty)
									{
										// Call clsTemp.FindFirstRecord("id", Grid.TextMatrix(x, lngColAccount))
										clsTemp.FindFirst("id = " + Grid.TextMatrix(X, lngColAccount));
										if (clsTemp.NoMatch)
										{
											FCMessageBox.Show("Mortgage Holder number " + Grid.TextMatrix(X, lngColAccount) + " doesn't exist." + "\r\n" + "This line will not be saved.", MsgBoxStyle.Exclamation, "Invalid Holder");
										}
										else
										{
											if (FCConvert.CBool(Strings.UCase(Grid.TextMatrix(X, lngColEscrow))) == true)
											{
												strEscrow = "1";
											}
											else
											{
												strEscrow = "0";
											}
											if (FCConvert.CBool(Strings.UCase(Grid.TextMatrix(X, lngColReceieveBill))) == true)
											{
												strGetBill = "1";
											}
											else
											{
												strGetBill = "0";
											}
											strGetBookPage = Strings.Trim(Grid.TextMatrix(X, lngColBookPage));
											if (boolUseREAccount)
											{
												clsTemp.Execute("INSERT INTO MortgageAssociation (MortgageHolderID,Account,Escrowed,Receivebill,MODULE,bookpage) VALUES (" + Grid.TextMatrix(X, lngColAccount) + "," + FCConvert.ToString(clsRealEstate.Get_Fields_Int32("RSAccount")) + "," + strEscrow + "," + strGetBill + ",'" + strMod + "','" + strGetBookPage + "')", "CentralData");
											}
											else
											{
												// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
												clsTemp.Execute("INSERT INTO MortgageAssociation (MortgageHolderID,Account,Escrowed,Receivebill,MODULE) VALUES (" + Grid.TextMatrix(X, lngColAccount) + "," + FCConvert.ToString(clsRealEstate.Get_Fields("AccountNumber")) + "," + strEscrow + "," + strGetBill + ",'" + strMod + "')", "CentralData");
											}
										}
									}
								}
								// X
							}
						}
						SaveInformation = true;
						boolDirty = false;
						FCMessageBox.Show("Account saved successfully.", MsgBoxStyle.Information, "Save Account");
					}
				}
				else
				{
					SaveInformation = false;
					if (boolMortgageHolder)
					{
						if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
						{
							FCMessageBox.Show("Multiple instances of the same account with the same book and page are present.  Please fix this before saving.", MsgBoxStyle.Exclamation, "Save Unsuccessful");
						}
						else
						{
							FCMessageBox.Show("Multiple instances of the same account are present.  Please fix this before saving.", MsgBoxStyle.Exclamation, "Save Unsuccessful");
						}
					}
					else
					{
						if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
						{
							FCMessageBox.Show("Multiple instances of the same mortgage holder with the same book and page are present.  Please fix this before saving.", MsgBoxStyle.Exclamation, "Save Unsuccessful");
						}
						else
						{
							FCMessageBox.Show("Multiple instances of the same mortgage holder are present.  Please fix this before saving.", MsgBoxStyle.Exclamation, "Save Unsuccessful");
						}
					}
				}
				return SaveInformation;
			}
			catch (Exception ex)
			{
				SaveInformation = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Save Error");
			}
			return SaveInformation;
		}

		private void ReturnFromSearch_2(int lngAcct)
		{
			ReturnFromSearch(ref lngAcct);
		}

		private void ReturnFromSearch(ref int lngAcct)
		{
			framHoldersList.Visible = true;
			//mnuFileSaveExit.Enabled = true;
			if (lngAcct == 0)
			{
				// If vsAccounts.Visible And vsAccounts.Enabled Then
				// vsAccounts.SetFocus
				// Else
				// 
				// End If
			}
			else
			{
				// LoadAccountInformation lngAcct, 0
				// SetGridHeight
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuSave_Click(sender, e);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			this.mnuSave_Click(sender, e);
		}

		private void cmdMortHolderList_Click(object sender, EventArgs e)
		{
			this.mnuMortHolderList_Click(sender, e);
		}

		private void cmdFileAdd_Click(object sender, EventArgs e)
		{
			this.mnuFileAdd_Click(sender, e);
		}
 
		//FC:FINAL:CHN: Allow numbers only to input at Account column cells (javascript).
		private void Grid_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				if (Grid.Col == 2)
				{
					var box = e.Control as TextBox;
					if (box != null)
					{
                        box.AllowOnlyNumericInput();
                        box.RemoveAlphaCharactersOnValueChange();
					}
				}
			}
		}
	}
}
