﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TWCL0000;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptREIntParty : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Corey Grey              *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :               Melissa Lamprecht       *
		// LAST UPDATED   :               12/31/2007              *
		// ********************************************************
		clsDRWrapper clsre = new clsDRWrapper();
		int lngLastAccount;
		double dblTotalSum;

		public rptREIntParty()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Interested Party List";
		}

		public static rptREIntParty InstancePtr
		{
			get
			{
				return (rptREIntParty)Sys.GetInstance(typeof(rptREIntParty));
			}
		}

		protected rptREIntParty _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsre?.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsre.EndOfFile();
			//Detail_Format();
		}

		public void Init(bool boolNameOrder)
		{
			string strSQL = "";
			// Dim strOrder            As String    'kgk 04042012  Not used here
			string strCurrentModule;
			// If boolNameOrder Then
			// strOrder = " ORDER BY Master.RSName, Master.RSAccount "
			// Else
			// strOrder = " ORDER BY Master.RSAccount "
			// End If
			strCurrentModule = Strings.Mid(modGlobal.DEFAULTDATABASE, 3, 2);
			lblTotalDue.Text = "Total Due as of " + Strings.Format(DateTime.Today, "MM/dd/yy");
			// clsre.OpenRecordset "SELECT * FROM Master INNER JOIN Owners ON Master.RSAccount = Owners.Account WHERE RSDeleted = False AND Owners.Module = '" & strCurrentModule & "'", strREDatabase
			clsre.OpenRecordset("SELECT * FROM Master INNER JOIN Owners ON Master.RSAccount = Owners.Account WHERE RSDeleted = 0 AND Owners.ModuleCode = 'RE' and associd = 0", modExtraModules.strREDatabase);
			clsre.InsertName("OwnerPartyID,SecOwnerPartyID", "Own1,Own2", false, true, true, "", false, "", true, "");
			// Me.Show , MDIParent
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			lngLastAccount = 0;
			txtDate.Text = DateTime.Today.ToString();
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				double dblDue = 0;
				bool boolDup = false;
				if (!clsre.EndOfFile())
				{
					if (Conversion.Val(clsre.Get_Fields_Int32("RSAccount")) != lngLastAccount)
					{
						txtaccount.Text = FCConvert.ToString(clsre.Get_Fields_Int32("RSAccount"));
						txtName.Text = clsre.Get_Fields("DeedName1");
						lngLastAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsre.Get_Fields_Int32("RSAccount"))));
						boolDup = false;
					}
					else
					{
						txtaccount.Text = "";
						txtName.Text = "";
						boolDup = true;
					}
					if (FCConvert.ToBoolean(clsre.Get_Fields_Boolean("ReceiveCopies")))
					{
						txtCopies.Text = "Yes";
					}
					else
					{
						txtCopies.Text = "No";
					}
					// TODO: Field [Owners.AutoID] not found!! (maybe it is an alias?)
					txtIPNumber.Text = FCConvert.ToString(clsre.Get_Fields("Owners.AutoID"));
					txtIPName.Text = clsre.Get_Fields_String("Name");
					if (!boolDup)
					{
						if (modGlobalConstants.Statics.gboolCL || modGlobalConstants.Statics.gboolBL)
						{
							dblDue = modCLCalculations.CalculateAccountTotal(lngLastAccount, true);
							fldTotalDue.Text = Strings.Format(dblDue, "$#,##0.00");
						}
						else
						{
							fldTotalDue.Text = "0.00";
						}
					}
					else
					{
						fldTotalDue.Text = "";
					}
					dblTotalSum += dblDue;
					clsre.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Detail Format");
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotalTotalDue.Text = Strings.Format(dblTotalSum, "$#,##0.00");
		}

		
	}
}
