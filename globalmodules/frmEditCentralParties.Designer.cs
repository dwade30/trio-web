﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmEditCentralParties.
	/// </summary>
	partial class frmEditCentralParties : BaseForm
	{
		public fecherFoundation.FCFrame fraWait;
		public fecherFoundation.FCLabel Label25;
		public fecherFoundation.FCGrid fgrdSavedContactPhones;
		public fecherFoundation.FCFrame fraComment;
		public fecherFoundation.FCRichTextBox txtCommentComment;
		public fecherFoundation.FCButton cmdCancelComment;
		public fecherFoundation.FCButton cmdOKComment;
		public fecherFoundation.FCComboBox cboCommentModule;
		public fecherFoundation.FCLabel Label23;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel lblCommentDateLastModified;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCLabel lblCreatedBy;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCFrame fraContact;
		public fecherFoundation.FCButton cmdCancelContact;
		public fecherFoundation.FCButton cmdOKContact;
		public fecherFoundation.FCTextBox txtContactEmail;
		public fecherFoundation.FCTextBox txtContactDescription;
		public fecherFoundation.FCTextBox txtContactName;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCGrid fgrdContactPhone;
		public fecherFoundation.FCRichTextBox txtContactComments;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCFrame fraAddress;
		public fecherFoundation.FCTextBox txtCountry;
		public fecherFoundation.FCRichTextBox txtComments;
		public fecherFoundation.FCFrame fraOverrideCriteria;
		public fecherFoundation.FCCheckBox chkSeasonal;
		public fecherFoundation.FCFrame fraSeasonal;
		public fecherFoundation.FCComboBox cboEndDay;
		public fecherFoundation.FCComboBox cboEndMonth;
		public fecherFoundation.FCComboBox cboStartDay;
		public fecherFoundation.FCComboBox cboStartMonth;
		public fecherFoundation.FCLabel lblDateRangeTo;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCComboBox cboAccount;
		public fecherFoundation.FCComboBox cboModule;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCComboBox cboState;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtAddress3;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtOverrideName;
		public fecherFoundation.FCComboBox cboAddressType;
		public fecherFoundation.FCButton cmdCancelAddress;
		public fecherFoundation.FCButton cmdOKAddress;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCTabControl tabDetails;
		public fecherFoundation.FCTabPage tabDetails_Comments;
		public fecherFoundation.FCGrid fgrdAddress;
		public fecherFoundation.FCButton cmdNewAddress;
		public fecherFoundation.FCButton cmdDeleteAddress;
		public fecherFoundation.FCTabPage tabDetails_Address;
		public fecherFoundation.FCGrid fgrdContacts;
		public fecherFoundation.FCButton cmdNewContact;
		public fecherFoundation.FCButton cmdDeleteContact;
		public fecherFoundation.FCTabPage tabDetails_Contacts;
		public fecherFoundation.FCGrid fgrdComments;
		public fecherFoundation.FCButton cmdNewComment;
		public fecherFoundation.FCButton cmdDeleteComment;
		public fecherFoundation.FCTabPage tabDetails_Page4;
		public fecherFoundation.FCGrid gridReferences;
		public fecherFoundation.FCTextBox txtWebAddress;
		public fecherFoundation.FCTextBox txtEmail;
		public fecherFoundation.FCFrame fraPhone;
		public fecherFoundation.FCGrid fgrdPhone;
		public fecherFoundation.FCTextBox txtCompanyName;
		public fecherFoundation.FCTextBox txtLast;
		public fecherFoundation.FCTextBox txtMI;
		public fecherFoundation.FCTextBox txtDesignation;
		public fecherFoundation.FCTextBox txtFirst;
		public fecherFoundation.FCComboBox cboType;
		public fecherFoundation.FCLabel lblPartyCreatedBy;
		public fecherFoundation.FCLabel lblGuid;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel lblCompanyName;
		public fecherFoundation.FCLabel lblFirstName;
		public fecherFoundation.FCLabel lblMI;
		public fecherFoundation.FCLabel lblLast;
		public fecherFoundation.FCLabel lblDesignation;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblCreatedDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCHeader lblParyNumber;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileSearch;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditCentralParties));
            this.fraWait = new fecherFoundation.FCFrame();
            this.Label25 = new fecherFoundation.FCLabel();
            this.fgrdSavedContactPhones = new fecherFoundation.FCGrid();
            this.fraComment = new fecherFoundation.FCFrame();
            this.Label22 = new fecherFoundation.FCLabel();
            this.Label20 = new fecherFoundation.FCLabel();
            this.txtCommentComment = new fecherFoundation.FCRichTextBox();
            this.cmdCancelComment = new fecherFoundation.FCButton();
            this.cmdOKComment = new fecherFoundation.FCButton();
            this.cboCommentModule = new fecherFoundation.FCComboBox();
            this.Label23 = new fecherFoundation.FCLabel();
            this.Label21 = new fecherFoundation.FCLabel();
            this.lblCommentDateLastModified = new fecherFoundation.FCLabel();
            this.lblCreatedBy = new fecherFoundation.FCLabel();
            this.fraContact = new fecherFoundation.FCFrame();
            this.txtContactEmail = new fecherFoundation.FCTextBox();
            this.txtContactDescription = new fecherFoundation.FCTextBox();
            this.txtContactName = new fecherFoundation.FCTextBox();
            this.cmdCancelContact = new fecherFoundation.FCButton();
            this.txtContactComments = new fecherFoundation.FCRichTextBox();
            this.cmdOKContact = new fecherFoundation.FCButton();
            this.Label19 = new fecherFoundation.FCLabel();
            this.Label18 = new fecherFoundation.FCLabel();
            this.Label17 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.fgrdContactPhone = new fecherFoundation.FCGrid();
            this.cmdDeleteContact = new fecherFoundation.FCButton();
            this.cmdNewContact = new fecherFoundation.FCButton();
            this.tabDetails_Page4 = new fecherFoundation.FCTabPage();
            this.gridReferences = new fecherFoundation.FCGrid();
            this.fraAddress = new fecherFoundation.FCFrame();
            this.txtCountry = new fecherFoundation.FCTextBox();
            this.txtComments = new fecherFoundation.FCRichTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.cboState = new fecherFoundation.FCComboBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtAddress3 = new fecherFoundation.FCTextBox();
            this.txtAddress2 = new fecherFoundation.FCTextBox();
            this.txtAddress1 = new fecherFoundation.FCTextBox();
            this.txtOverrideName = new fecherFoundation.FCTextBox();
            this.cboAddressType = new fecherFoundation.FCComboBox();
            this.cmdCancelAddress = new fecherFoundation.FCButton();
            this.cmdOKAddress = new fecherFoundation.FCButton();
            this.Label24 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.fraOverrideCriteria = new fecherFoundation.FCFrame();
            this.chkSeasonal = new fecherFoundation.FCCheckBox();
            this.cboAccount = new fecherFoundation.FCComboBox();
            this.fraSeasonal = new fecherFoundation.FCFrame();
            this.cboEndDay = new fecherFoundation.FCComboBox();
            this.cboEndMonth = new fecherFoundation.FCComboBox();
            this.cboStartDay = new fecherFoundation.FCComboBox();
            this.cboStartMonth = new fecherFoundation.FCComboBox();
            this.lblDateRangeTo = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.cboModule = new fecherFoundation.FCComboBox();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.tabDetails = new fecherFoundation.FCTabControl();
            this.tabDetails_Address = new fecherFoundation.FCTabPage();
            this.fgrdAddress = new fecherFoundation.FCGrid();
            this.cmdDeleteAddress = new fecherFoundation.FCButton();
            this.cmdNewAddress = new fecherFoundation.FCButton();
            this.tabDetails_Contacts = new fecherFoundation.FCTabPage();
            this.fgrdContacts = new fecherFoundation.FCGrid();
            this.tabDetails_Comments = new fecherFoundation.FCTabPage();
            this.fgrdComments = new fecherFoundation.FCGrid();
            this.cmdDeleteComment = new fecherFoundation.FCButton();
            this.cmdNewComment = new fecherFoundation.FCButton();
            this.txtWebAddress = new fecherFoundation.FCTextBox();
            this.txtEmail = new fecherFoundation.FCTextBox();
            this.fraPhone = new fecherFoundation.FCFrame();
            this.fgrdPhone = new fecherFoundation.FCGrid();
            this.txtCompanyName = new fecherFoundation.FCTextBox();
            this.txtLast = new fecherFoundation.FCTextBox();
            this.txtMI = new fecherFoundation.FCTextBox();
            this.txtDesignation = new fecherFoundation.FCTextBox();
            this.txtFirst = new fecherFoundation.FCTextBox();
            this.cboType = new fecherFoundation.FCComboBox();
            this.lblPartyCreatedBy = new fecherFoundation.FCLabel();
            this.lblGuid = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.lblCompanyName = new fecherFoundation.FCLabel();
            this.lblFirstName = new fecherFoundation.FCLabel();
            this.lblMI = new fecherFoundation.FCLabel();
            this.lblLast = new fecherFoundation.FCLabel();
            this.lblDesignation = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblCreatedDate = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblParyNumber = new fecherFoundation.FCHeader();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSearch = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip();
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdFileNew = new fecherFoundation.FCButton();
            this.cmdFileDelete = new fecherFoundation.FCButton();
            this.cmdPrevious = new fecherFoundation.FCButton();
            this.cmdFileNext = new fecherFoundation.FCButton();
            this.cmdFileSave = new fecherFoundation.FCButton();
            this.mnuSep = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSep2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileNext = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrevious = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSep3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileNew = new fecherFoundation.FCToolStripMenuItem();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.fraWait.SuspendLayout();
            this.fraComment.SuspendLayout();
            this.fraContact.SuspendLayout();
            this.Frame2.SuspendLayout();
            this.tabDetails_Page4.SuspendLayout();
            this.fraAddress.SuspendLayout();
            this.fraOverrideCriteria.SuspendLayout();
            this.fraSeasonal.SuspendLayout();
            this.tabDetails.SuspendLayout();
            this.tabDetails_Address.SuspendLayout();
            this.tabDetails_Contacts.SuspendLayout();
            this.tabDetails_Comments.SuspendLayout();
            this.fraPhone.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 655);
            this.BottomPanel.Size = new System.Drawing.Size(978, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.tabDetails);
            this.ClientArea.Controls.Add(this.fraContact);
            this.ClientArea.Controls.Add(this.fraAddress);
            this.ClientArea.Controls.Add(this.fraComment);
            this.ClientArea.Controls.Add(this.txtWebAddress);
            this.ClientArea.Controls.Add(this.txtEmail);
            this.ClientArea.Controls.Add(this.txtLast);
            this.ClientArea.Controls.Add(this.txtMI);
            this.ClientArea.Controls.Add(this.txtDesignation);
            this.ClientArea.Controls.Add(this.txtFirst);
            this.ClientArea.Controls.Add(this.cboType);
            this.ClientArea.Controls.Add(this.lblPartyCreatedBy);
            this.ClientArea.Controls.Add(this.lblGuid);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.lblCompanyName);
            this.ClientArea.Controls.Add(this.lblFirstName);
            this.ClientArea.Controls.Add(this.lblMI);
            this.ClientArea.Controls.Add(this.lblLast);
            this.ClientArea.Controls.Add(this.lblDesignation);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.lblCreatedDate);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.fraPhone);
            this.ClientArea.Controls.Add(this.fraWait);
            this.ClientArea.Controls.Add(this.txtCompanyName);
            this.ClientArea.Size = new System.Drawing.Size(998, 628);
            this.ClientArea.Controls.SetChildIndex(this.txtCompanyName, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraWait, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraPhone, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCreatedDate, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label3, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblDesignation, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblLast, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblMI, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblFirstName, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCompanyName, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label5, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblGuid, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPartyCreatedBy, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboType, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtFirst, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtDesignation, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtMI, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtLast, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtEmail, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtWebAddress, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraComment, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraAddress, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraContact, 0);
            this.ClientArea.Controls.SetChildIndex(this.tabDetails, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileSave);
            this.TopPanel.Controls.Add(this.cmdFileNext);
            this.TopPanel.Controls.Add(this.cmdPrevious);
            this.TopPanel.Controls.Add(this.cmdFileDelete);
            this.TopPanel.Controls.Add(this.cmdFileNew);
            this.TopPanel.Controls.Add(this.lblParyNumber);
            this.TopPanel.Size = new System.Drawing.Size(998, 60);
            this.TopPanel.Controls.SetChildIndex(this.lblParyNumber, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrevious, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileNext, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileSave, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(26, 26);
            this.HeaderText.Size = new System.Drawing.Size(80, 28);
            this.HeaderText.Text = "Party #";
            // 
            // fraWait
            // 
            this.fraWait.Controls.Add(this.Label25);
            this.fraWait.Location = new System.Drawing.Point(413, 197);
            this.fraWait.Name = "fraWait";
            this.fraWait.Size = new System.Drawing.Size(310, 138);
            this.fraWait.TabIndex = 20;
            this.fraWait.UseMnemonic = false;
            this.fraWait.Visible = false;
            // 
            // Label25
            // 
            this.Label25.Location = new System.Drawing.Point(1, 70);
            this.Label25.Name = "Label25";
            this.Label25.Size = new System.Drawing.Size(322, 85);
            this.Label25.TabIndex = 0;
            this.Label25.Text = "CHECKING FOR DUPLICATES...";
            // 
            // fgrdSavedContactPhones
            // 
            this.fgrdSavedContactPhones.Cols = 6;
            this.fgrdSavedContactPhones.ExtendLastCol = true;
            this.fgrdSavedContactPhones.Location = new System.Drawing.Point(20, 20);
            this.fgrdSavedContactPhones.Name = "fgrdSavedContactPhones";
            this.fgrdSavedContactPhones.Rows = 50;
            this.fgrdSavedContactPhones.Size = new System.Drawing.Size(893, 190);
            this.fgrdSavedContactPhones.TabIndex = 0;
            this.fgrdSavedContactPhones.Visible = false;
            // 
            // fraComment
            // 
            this.fraComment.BackColor = System.Drawing.Color.White;
            this.fraComment.Controls.Add(this.Label22);
            this.fraComment.Controls.Add(this.Label20);
            this.fraComment.Controls.Add(this.txtCommentComment);
            this.fraComment.Controls.Add(this.cmdCancelComment);
            this.fraComment.Controls.Add(this.cmdOKComment);
            this.fraComment.Controls.Add(this.cboCommentModule);
            this.fraComment.Controls.Add(this.Label23);
            this.fraComment.Controls.Add(this.Label21);
            this.fraComment.Controls.Add(this.lblCommentDateLastModified);
            this.fraComment.Controls.Add(this.lblCreatedBy);
            this.fraComment.Location = new System.Drawing.Point(30, 285);
            this.fraComment.Name = "fraComment";
            this.fraComment.Size = new System.Drawing.Size(572, 251);
            this.fraComment.TabIndex = 25;
            this.fraComment.Text = "Comment";
            this.fraComment.UseMnemonic = false;
            this.fraComment.Visible = false;
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.BackColor = System.Drawing.Color.Transparent;
            this.Label22.Location = new System.Drawing.Point(20, 62);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(104, 15);
            this.Label22.TabIndex = 2;
            this.Label22.Text = "LAST MODIFIED";
            this.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label20
            // 
            this.Label20.AutoSize = true;
            this.Label20.BackColor = System.Drawing.Color.Transparent;
            this.Label20.Location = new System.Drawing.Point(20, 32);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(87, 15);
            this.Label20.TabIndex = 0;
            this.Label20.Text = "CREATED BY";
            this.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCommentComment
            // 
            this.txtCommentComment.Location = new System.Drawing.Point(143, 143);
            this.txtCommentComment.Name = "txtCommentComment";
            this.txtCommentComment.Size = new System.Drawing.Size(400, 40);
            this.txtCommentComment.TabIndex = 7;
            // 
            // cmdCancelComment
            // 
            this.cmdCancelComment.AppearanceKey = "actionButton";
            this.cmdCancelComment.Location = new System.Drawing.Point(295, 201);
            this.cmdCancelComment.Name = "cmdCancelComment";
            this.cmdCancelComment.Size = new System.Drawing.Size(74, 40);
            this.cmdCancelComment.TabIndex = 85;
            this.cmdCancelComment.Text = "Cancel";
            this.cmdCancelComment.Click += new System.EventHandler(this.cmdCancelComment_Click);
            // 
            // cmdOKComment
            // 
            this.cmdOKComment.AppearanceKey = "actionButton";
            this.cmdOKComment.Location = new System.Drawing.Point(203, 201);
            this.cmdOKComment.Name = "cmdOKComment";
            this.cmdOKComment.Size = new System.Drawing.Size(64, 40);
            this.cmdOKComment.TabIndex = 84;
            this.cmdOKComment.Text = "Ok";
            this.cmdOKComment.Click += new System.EventHandler(this.cmdOKComment_Click);
            // 
            // cboCommentModule
            // 
            this.cboCommentModule.BackColor = System.Drawing.SystemColors.Window;
            this.cboCommentModule.Items.AddRange(new object[] {
            "Company",
            "Individual"});
            this.cboCommentModule.Location = new System.Drawing.Point(143, 90);
            this.cboCommentModule.Name = "cboCommentModule";
            this.cboCommentModule.Size = new System.Drawing.Size(238, 40);
            this.cboCommentModule.TabIndex = 5;
            // 
            // Label23
            // 
            this.Label23.AutoSize = true;
            this.Label23.BackColor = System.Drawing.Color.Transparent;
            this.Label23.Location = new System.Drawing.Point(20, 157);
            this.Label23.Name = "Label23";
            this.Label23.Size = new System.Drawing.Size(80, 15);
            this.Label23.TabIndex = 6;
            this.Label23.Text = "COMMENTS";
            this.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.BackColor = System.Drawing.Color.Transparent;
            this.Label21.Location = new System.Drawing.Point(20, 104);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(60, 15);
            this.Label21.TabIndex = 4;
            this.Label21.Text = "MODULE";
            this.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCommentDateLastModified
            // 
            this.lblCommentDateLastModified.BackColor = System.Drawing.Color.Transparent;
            this.lblCommentDateLastModified.Location = new System.Drawing.Point(143, 62);
            this.lblCommentDateLastModified.Name = "lblCommentDateLastModified";
            this.lblCommentDateLastModified.Size = new System.Drawing.Size(234, 15);
            this.lblCommentDateLastModified.TabIndex = 3;
            // 
            // lblCreatedBy
            // 
            this.lblCreatedBy.BackColor = System.Drawing.Color.Transparent;
            this.lblCreatedBy.Location = new System.Drawing.Point(143, 32);
            this.lblCreatedBy.Name = "lblCreatedBy";
            this.lblCreatedBy.Size = new System.Drawing.Size(234, 15);
            this.lblCreatedBy.TabIndex = 1;
            // 
            // fraContact
            // 
            this.fraContact.BackColor = System.Drawing.Color.White;
            this.fraContact.Controls.Add(this.txtContactEmail);
            this.fraContact.Controls.Add(this.txtContactDescription);
            this.fraContact.Controls.Add(this.txtContactName);
            this.fraContact.Controls.Add(this.cmdCancelContact);
            this.fraContact.Controls.Add(this.txtContactComments);
            this.fraContact.Controls.Add(this.cmdOKContact);
            this.fraContact.Controls.Add(this.Label19);
            this.fraContact.Controls.Add(this.Label18);
            this.fraContact.Controls.Add(this.Label17);
            this.fraContact.Controls.Add(this.Label16);
            this.fraContact.Controls.Add(this.Frame2);
            this.fraContact.Location = new System.Drawing.Point(30, 284);
            this.fraContact.Name = "fraContact";
            this.fraContact.Size = new System.Drawing.Size(790, 367);
            this.fraContact.TabIndex = 23;
            this.fraContact.Text = "Contact";
            this.fraContact.UseMnemonic = false;
            this.fraContact.Visible = false;
            // 
            // txtContactEmail
            // 
            this.txtContactEmail.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactEmail.Location = new System.Drawing.Point(147, 130);
            this.txtContactEmail.Name = "txtContactEmail";
            this.txtContactEmail.Size = new System.Drawing.Size(220, 40);
            this.txtContactEmail.TabIndex = 5;
            this.txtContactEmail.Text = "Text1";
            // 
            // txtContactDescription
            // 
            this.txtContactDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactDescription.Location = new System.Drawing.Point(147, 80);
            this.txtContactDescription.Name = "txtContactDescription";
            this.txtContactDescription.Size = new System.Drawing.Size(220, 40);
            this.txtContactDescription.TabIndex = 3;
            this.txtContactDescription.Text = "Text1";
            // 
            // txtContactName
            // 
            this.txtContactName.BackColor = System.Drawing.SystemColors.Window;
            this.txtContactName.Location = new System.Drawing.Point(147, 30);
            this.txtContactName.Name = "txtContactName";
            this.txtContactName.Size = new System.Drawing.Size(220, 40);
            this.txtContactName.TabIndex = 1;
            this.txtContactName.Text = "Text1";
            // 
            // cmdCancelContact
            // 
            this.cmdCancelContact.AppearanceKey = "actionButton";
            this.cmdCancelContact.Location = new System.Drawing.Point(402, 310);
            this.cmdCancelContact.Name = "cmdCancelContact";
            this.cmdCancelContact.Size = new System.Drawing.Size(75, 40);
            this.cmdCancelContact.TabIndex = 10;
            this.cmdCancelContact.Text = "Cancel";
            this.cmdCancelContact.Click += new System.EventHandler(this.cmdCancelContact_Click);
            // 
            // txtContactComments
            // 
            this.txtContactComments.Location = new System.Drawing.Point(147, 247);
            this.txtContactComments.Name = "txtContactComments";
            this.txtContactComments.Size = new System.Drawing.Size(609, 40);
            this.txtContactComments.TabIndex = 7;
            // 
            // cmdOKContact
            // 
            this.cmdOKContact.AppearanceKey = "actionButton";
            this.cmdOKContact.Location = new System.Drawing.Point(313, 310);
            this.cmdOKContact.Name = "cmdOKContact";
            this.cmdOKContact.Size = new System.Drawing.Size(74, 40);
            this.cmdOKContact.TabIndex = 9;
            this.cmdOKContact.Text = "Ok";
            this.cmdOKContact.Click += new System.EventHandler(this.cmdOKContact_Click);
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.BackColor = System.Drawing.Color.Transparent;
            this.Label19.Location = new System.Drawing.Point(20, 261);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(80, 15);
            this.Label19.TabIndex = 6;
            this.Label19.Text = "COMMENTS";
            this.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label18
            // 
            this.Label18.AutoSize = true;
            this.Label18.BackColor = System.Drawing.Color.Transparent;
            this.Label18.Location = new System.Drawing.Point(20, 144);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(47, 15);
            this.Label18.TabIndex = 4;
            this.Label18.Text = "E-MAIL";
            this.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.BackColor = System.Drawing.Color.Transparent;
            this.Label17.Location = new System.Drawing.Point(20, 94);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(93, 15);
            this.Label17.TabIndex = 2;
            this.Label17.Text = "DESCRIPTION";
            this.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.BackColor = System.Drawing.Color.Transparent;
            this.Label16.Location = new System.Drawing.Point(20, 44);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(42, 15);
            this.Label16.TabIndex = 0;
            this.Label16.Text = "NAME";
            this.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Frame2
            // 
            this.Frame2.AppearanceKey = "groupBoxNoBorders";
            this.Frame2.BackColor = System.Drawing.Color.Transparent;
            this.Frame2.Controls.Add(this.fgrdContactPhone);
            this.Frame2.Location = new System.Drawing.Point(387, 30);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(384, 207);
            this.Frame2.TabIndex = 8;
            this.Frame2.Text = "Phone Numbers";
            this.Frame2.UseMnemonic = false;
            // 
            // fgrdContactPhone
            // 
            this.fgrdContactPhone.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fgrdContactPhone.BackColor = System.Drawing.Color.Transparent;
            this.fgrdContactPhone.Cols = 5;
            this.fgrdContactPhone.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.fgrdContactPhone.ExtendLastCol = true;
            this.fgrdContactPhone.Location = new System.Drawing.Point(0, 30);
            this.fgrdContactPhone.Name = "fgrdContactPhone";
            this.fgrdContactPhone.ReadOnly = false;
            this.fgrdContactPhone.Rows = 50;
            this.fgrdContactPhone.Size = new System.Drawing.Size(369, 157);
            this.fgrdContactPhone.StandardTab = false;
            this.fgrdContactPhone.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.fgrdContactPhone.TabIndex = 0;
            this.fgrdContactPhone.KeyDown += new Wisej.Web.KeyEventHandler(this.fgrdContactPhone_KeyDown);
            // 
            // cmdDeleteContact
            // 
            this.cmdDeleteContact.AppearanceKey = "actionButton";
            this.cmdDeleteContact.Location = new System.Drawing.Point(471, 221);
            this.cmdDeleteContact.Name = "cmdDeleteContact";
            this.cmdDeleteContact.Size = new System.Drawing.Size(75, 40);
            this.cmdDeleteContact.TabIndex = 2;
            this.cmdDeleteContact.Text = "Delete";
            this.cmdDeleteContact.Click += new System.EventHandler(this.cmdDeleteContact_Click);
            // 
            // cmdNewContact
            // 
            this.cmdNewContact.AppearanceKey = "actionButton";
            this.cmdNewContact.Location = new System.Drawing.Point(386, 221);
            this.cmdNewContact.Name = "cmdNewContact";
            this.cmdNewContact.Size = new System.Drawing.Size(62, 40);
            this.cmdNewContact.TabIndex = 1;
            this.cmdNewContact.Text = "New";
            this.cmdNewContact.Click += new System.EventHandler(this.cmdNewContact_Click);
            // 
            // tabDetails_Page4
            // 
            this.tabDetails_Page4.Controls.Add(this.gridReferences);
            this.tabDetails_Page4.Location = new System.Drawing.Point(1, 35);
            this.tabDetails_Page4.Name = "tabDetails_Page4";
            this.tabDetails_Page4.Size = new System.Drawing.Size(933, 288);
            this.tabDetails_Page4.Text = "References";
            // 
            // gridReferences
            // 
            this.gridReferences.ExtendLastCol = true;
            this.gridReferences.Location = new System.Drawing.Point(20, 20);
            this.gridReferences.Name = "gridReferences";
            this.gridReferences.Size = new System.Drawing.Size(892, 235);
            this.gridReferences.TabIndex = 95;
            // 
            // fraAddress
            // 
            this.fraAddress.BackColor = System.Drawing.Color.White;
            this.fraAddress.Controls.Add(this.txtCountry);
            this.fraAddress.Controls.Add(this.txtComments);
            this.fraAddress.Controls.Add(this.txtZip);
            this.fraAddress.Controls.Add(this.cboState);
            this.fraAddress.Controls.Add(this.txtCity);
            this.fraAddress.Controls.Add(this.txtAddress3);
            this.fraAddress.Controls.Add(this.txtAddress2);
            this.fraAddress.Controls.Add(this.txtAddress1);
            this.fraAddress.Controls.Add(this.txtOverrideName);
            this.fraAddress.Controls.Add(this.cboAddressType);
            this.fraAddress.Controls.Add(this.cmdCancelAddress);
            this.fraAddress.Controls.Add(this.cmdOKAddress);
            this.fraAddress.Controls.Add(this.Label24);
            this.fraAddress.Controls.Add(this.Label15);
            this.fraAddress.Controls.Add(this.Label11);
            this.fraAddress.Controls.Add(this.Label10);
            this.fraAddress.Controls.Add(this.Label9);
            this.fraAddress.Controls.Add(this.Label8);
            this.fraAddress.Controls.Add(this.Label7);
            this.fraAddress.Controls.Add(this.Label6);
            this.fraAddress.Controls.Add(this.fraOverrideCriteria);
            this.fraAddress.Location = new System.Drawing.Point(30, 15);
            this.fraAddress.Name = "fraAddress";
            this.fraAddress.Size = new System.Drawing.Size(759, 515);
            this.fraAddress.TabIndex = 24;
            this.fraAddress.Text = "Address";
            this.fraAddress.UseMnemonic = false;
            this.fraAddress.Visible = false;
            // 
            // txtCountry
            // 
            this.txtCountry.BackColor = System.Drawing.SystemColors.Window;
            this.txtCountry.Location = new System.Drawing.Point(147, 330);
            this.txtCountry.Name = "txtCountry";
            this.txtCountry.Size = new System.Drawing.Size(414, 40);
            this.txtCountry.TabIndex = 15;
            this.txtCountry.Text = "txtCountry";
            // 
            // txtComments
            // 
            this.txtComments.Location = new System.Drawing.Point(147, 382);
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(591, 40);
            this.txtComments.TabIndex = 18;
            // 
            // txtZip
            // 
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.Location = new System.Drawing.Point(419, 279);
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(142, 40);
            this.txtZip.TabIndex = 13;
            this.txtZip.Text = "Text1";
            // 
            // cboState
            // 
            this.cboState.BackColor = System.Drawing.SystemColors.Window;
            this.cboState.Items.AddRange(new object[] {
            "Individual",
            "Company"});
            this.cboState.Location = new System.Drawing.Point(329, 279);
            this.cboState.Name = "cboState";
            this.cboState.Size = new System.Drawing.Size(80, 40);
            this.cboState.TabIndex = 12;
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.Location = new System.Drawing.Point(147, 279);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(171, 40);
            this.txtCity.TabIndex = 11;
            this.txtCity.Text = "Text1";
            // 
            // txtAddress3
            // 
            this.txtAddress3.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress3.Location = new System.Drawing.Point(147, 230);
            this.txtAddress3.Name = "txtAddress3";
            this.txtAddress3.Size = new System.Drawing.Size(414, 40);
            this.txtAddress3.TabIndex = 9;
            this.txtAddress3.Text = "Text1";
            // 
            // txtAddress2
            // 
            this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress2.Location = new System.Drawing.Point(147, 180);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(414, 40);
            this.txtAddress2.TabIndex = 7;
            this.txtAddress2.Text = "Text1";
            // 
            // txtAddress1
            // 
            this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress1.Location = new System.Drawing.Point(147, 130);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(414, 40);
            this.txtAddress1.TabIndex = 5;
            this.txtAddress1.Text = "Text1";
            // 
            // txtOverrideName
            // 
            this.txtOverrideName.BackColor = System.Drawing.SystemColors.Window;
            this.txtOverrideName.Location = new System.Drawing.Point(147, 80);
            this.txtOverrideName.Name = "txtOverrideName";
            this.txtOverrideName.Size = new System.Drawing.Size(414, 40);
            this.txtOverrideName.TabIndex = 3;
            this.txtOverrideName.Text = "Text1";
            // 
            // cboAddressType
            // 
            this.cboAddressType.BackColor = System.Drawing.SystemColors.Window;
            this.cboAddressType.Items.AddRange(new object[] {
            "Individual",
            "Company"});
            this.cboAddressType.Location = new System.Drawing.Point(147, 30);
            this.cboAddressType.Name = "cboAddressType";
            this.cboAddressType.Size = new System.Drawing.Size(414, 40);
            this.cboAddressType.TabIndex = 1;
            this.cboAddressType.SelectedIndexChanged += new System.EventHandler(this.cboAddressType_SelectedIndexChanged);
            // 
            // cmdCancelAddress
            // 
            this.cmdCancelAddress.AppearanceKey = "actionButton";
            this.cmdCancelAddress.Location = new System.Drawing.Point(389, 445);
            this.cmdCancelAddress.Name = "cmdCancelAddress";
            this.cmdCancelAddress.Size = new System.Drawing.Size(75, 40);
            this.cmdCancelAddress.TabIndex = 20;
            this.cmdCancelAddress.Text = "Cancel";
            this.cmdCancelAddress.Click += new System.EventHandler(this.cmdCancelAddress_Click);
            // 
            // cmdOKAddress
            // 
            this.cmdOKAddress.AppearanceKey = "actionButton";
            this.cmdOKAddress.Location = new System.Drawing.Point(292, 445);
            this.cmdOKAddress.Name = "cmdOKAddress";
            this.cmdOKAddress.Size = new System.Drawing.Size(75, 40);
            this.cmdOKAddress.TabIndex = 19;
            this.cmdOKAddress.Text = "Ok";
            this.cmdOKAddress.Click += new System.EventHandler(this.cmdOKAddress_Click);
            // 
            // Label24
            // 
            this.Label24.AutoSize = true;
            this.Label24.BackColor = System.Drawing.Color.Transparent;
            this.Label24.Location = new System.Drawing.Point(20, 344);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(68, 15);
            this.Label24.TabIndex = 14;
            this.Label24.Text = "COUNTRY";
            this.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.BackColor = System.Drawing.Color.Transparent;
            this.Label15.Location = new System.Drawing.Point(20, 395);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(80, 15);
            this.Label15.TabIndex = 17;
            this.Label15.Text = "COMMENTS";
            this.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.BackColor = System.Drawing.Color.Transparent;
            this.Label11.Location = new System.Drawing.Point(20, 293);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(52, 15);
            this.Label11.TabIndex = 10;
            this.Label11.Text = "C / S / Z";
            this.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.BackColor = System.Drawing.Color.Transparent;
            this.Label10.Location = new System.Drawing.Point(20, 244);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(78, 15);
            this.Label10.TabIndex = 8;
            this.Label10.Text = "ADDRESS 3";
            this.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.BackColor = System.Drawing.Color.Transparent;
            this.Label9.Location = new System.Drawing.Point(20, 194);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(78, 15);
            this.Label9.TabIndex = 6;
            this.Label9.Text = "ADDRESS 2";
            this.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.BackColor = System.Drawing.Color.Transparent;
            this.Label8.Location = new System.Drawing.Point(20, 144);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(78, 15);
            this.Label8.TabIndex = 4;
            this.Label8.Text = "ADDRESS 1";
            this.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.BackColor = System.Drawing.Color.Transparent;
            this.Label7.Location = new System.Drawing.Point(20, 94);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(93, 15);
            this.Label7.TabIndex = 2;
            this.Label7.Text = "DESCRIPTION";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.BackColor = System.Drawing.Color.Transparent;
            this.Label6.Location = new System.Drawing.Point(20, 44);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(38, 15);
            this.Label6.TabIndex = 0;
            this.Label6.Text = "TYPE";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraOverrideCriteria
            // 
            this.fraOverrideCriteria.BackColor = System.Drawing.Color.Transparent;
            this.fraOverrideCriteria.Controls.Add(this.chkSeasonal);
            this.fraOverrideCriteria.Controls.Add(this.cboAccount);
            this.fraOverrideCriteria.Controls.Add(this.fraSeasonal);
            this.fraOverrideCriteria.Controls.Add(this.cboModule);
            this.fraOverrideCriteria.Controls.Add(this.Label13);
            this.fraOverrideCriteria.Controls.Add(this.Label12);
            this.fraOverrideCriteria.Location = new System.Drawing.Point(20, 215);
            this.fraOverrideCriteria.Name = "fraOverrideCriteria";
            this.fraOverrideCriteria.Size = new System.Drawing.Size(718, 189);
            this.fraOverrideCriteria.TabIndex = 16;
            this.fraOverrideCriteria.Text = "Use This Address Only For";
            this.fraOverrideCriteria.UseMnemonic = false;
            this.fraOverrideCriteria.Visible = false;
            // 
            // chkSeasonal
            // 
            this.chkSeasonal.BackColor = System.Drawing.Color.Transparent;
            this.chkSeasonal.Location = new System.Drawing.Point(20, 84);
            this.chkSeasonal.Name = "chkSeasonal";
            this.chkSeasonal.Size = new System.Drawing.Size(86, 22);
            this.chkSeasonal.TabIndex = 4;
            this.chkSeasonal.Text = "Seasonal";
            this.chkSeasonal.Click += new System.EventHandler(this.chkSeasonal_Click);
            // 
            // cboAccount
            // 
            this.cboAccount.BackColor = System.Drawing.SystemColors.Window;
            this.cboAccount.Items.AddRange(new object[] {
            "Individual",
            "Company"});
            this.cboAccount.Location = new System.Drawing.Point(434, 30);
            this.cboAccount.Name = "cboAccount";
            this.cboAccount.Size = new System.Drawing.Size(167, 40);
            this.cboAccount.TabIndex = 3;
            // 
            // fraSeasonal
            // 
            this.fraSeasonal.AppearanceKey = "groupBoxNoBorders";
            this.fraSeasonal.BackColor = System.Drawing.Color.Transparent;
            this.fraSeasonal.Controls.Add(this.cboEndDay);
            this.fraSeasonal.Controls.Add(this.cboEndMonth);
            this.fraSeasonal.Controls.Add(this.cboStartDay);
            this.fraSeasonal.Controls.Add(this.cboStartMonth);
            this.fraSeasonal.Controls.Add(this.lblDateRangeTo);
            this.fraSeasonal.Controls.Add(this.Label14);
            this.fraSeasonal.Enabled = false;
            this.fraSeasonal.Location = new System.Drawing.Point(142, 84);
            this.fraSeasonal.Name = "fraSeasonal";
            this.fraSeasonal.Size = new System.Drawing.Size(459, 104);
            this.fraSeasonal.TabIndex = 5;
            this.fraSeasonal.UseMnemonic = false;
            // 
            // cboEndDay
            // 
            this.cboEndDay.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndDay.Items.AddRange(new object[] {
            "Individual",
            "Company"});
            this.cboEndDay.Location = new System.Drawing.Point(265, 50);
            this.cboEndDay.Name = "cboEndDay";
            this.cboEndDay.Size = new System.Drawing.Size(65, 40);
            this.cboEndDay.TabIndex = 5;
            // 
            // cboEndMonth
            // 
            this.cboEndMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboEndMonth.Items.AddRange(new object[] {
            "Individual",
            "Company"});
            this.cboEndMonth.Location = new System.Drawing.Point(125, 50);
            this.cboEndMonth.Name = "cboEndMonth";
            this.cboEndMonth.Size = new System.Drawing.Size(120, 40);
            this.cboEndMonth.TabIndex = 4;
            this.cboEndMonth.SelectedIndexChanged += new System.EventHandler(this.cboEndMonth_SelectedIndexChanged);
            // 
            // cboStartDay
            // 
            this.cboStartDay.BackColor = System.Drawing.SystemColors.Window;
            this.cboStartDay.Items.AddRange(new object[] {
            "Individual",
            "Company"});
            this.cboStartDay.Location = new System.Drawing.Point(265, 3);
            this.cboStartDay.Name = "cboStartDay";
            this.cboStartDay.Size = new System.Drawing.Size(65, 40);
            this.cboStartDay.TabIndex = 2;
            // 
            // cboStartMonth
            // 
            this.cboStartMonth.BackColor = System.Drawing.SystemColors.Window;
            this.cboStartMonth.Items.AddRange(new object[] {
            "Individual",
            "Company"});
            this.cboStartMonth.Location = new System.Drawing.Point(125, 3);
            this.cboStartMonth.Name = "cboStartMonth";
            this.cboStartMonth.Size = new System.Drawing.Size(120, 40);
            this.cboStartMonth.TabIndex = 1;
            this.cboStartMonth.SelectedIndexChanged += new System.EventHandler(this.cboStartMonth_SelectedIndexChanged);
            // 
            // lblDateRangeTo
            // 
            this.lblDateRangeTo.AutoSize = true;
            this.lblDateRangeTo.BackColor = System.Drawing.Color.Transparent;
            this.lblDateRangeTo.Location = new System.Drawing.Point(2, 54);
            this.lblDateRangeTo.Name = "lblDateRangeTo";
            this.lblDateRangeTo.Size = new System.Drawing.Size(22, 15);
            this.lblDateRangeTo.TabIndex = 3;
            this.lblDateRangeTo.Text = "TO";
            this.lblDateRangeTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.BackColor = System.Drawing.Color.Transparent;
            this.Label14.Location = new System.Drawing.Point(2, 7);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(89, 15);
            this.Label14.TabIndex = 0;
            this.Label14.Text = "DATE RANGE";
            this.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cboModule
            // 
            this.cboModule.BackColor = System.Drawing.SystemColors.Window;
            this.cboModule.Items.AddRange(new object[] {
            "Company",
            "Individual"});
            this.cboModule.Location = new System.Drawing.Point(124, 30);
            this.cboModule.Name = "cboModule";
            this.cboModule.Size = new System.Drawing.Size(167, 40);
            this.cboModule.Sorted = true;
            this.cboModule.TabIndex = 1;
            this.cboModule.SelectedIndexChanged += new System.EventHandler(this.cboModule_SelectedIndexChanged);
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.BackColor = System.Drawing.Color.Transparent;
            this.Label13.Location = new System.Drawing.Point(330, 44);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(68, 15);
            this.Label13.TabIndex = 2;
            this.Label13.Text = "ACCOUNT";
            this.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.BackColor = System.Drawing.Color.Transparent;
            this.Label12.Location = new System.Drawing.Point(20, 44);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(60, 15);
            this.Label12.TabIndex = 0;
            this.Label12.Text = "MODULE";
            this.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tabDetails
            // 
            this.tabDetails.Controls.Add(this.tabDetails_Address);
            this.tabDetails.Controls.Add(this.tabDetails_Contacts);
            this.tabDetails.Controls.Add(this.tabDetails_Comments);
            this.tabDetails.Controls.Add(this.tabDetails_Page4);
            this.tabDetails.Location = new System.Drawing.Point(30, 331);
            this.tabDetails.Name = "tabDetails";
            this.tabDetails.PageInsets = new Wisej.Web.Padding(1, 35, 1, 1);
            this.tabDetails.SelectedIndex = 0;
            this.tabDetails.Size = new System.Drawing.Size(935, 324);
            this.tabDetails.TabIndex = 22;
            this.tabDetails.Text = "Address";
            // 
            // tabDetails_Address
            // 
            this.tabDetails_Address.Controls.Add(this.fgrdAddress);
            this.tabDetails_Address.Controls.Add(this.cmdDeleteAddress);
            this.tabDetails_Address.Controls.Add(this.cmdNewAddress);
            this.tabDetails_Address.Location = new System.Drawing.Point(1, 35);
            this.tabDetails_Address.Name = "tabDetails_Address";
            this.tabDetails_Address.Size = new System.Drawing.Size(933, 288);
            this.tabDetails_Address.Text = "Address";
            // 
            // fgrdAddress
            // 
            this.fgrdAddress.Cols = 21;
            this.fgrdAddress.ExtendLastCol = true;
            this.fgrdAddress.Location = new System.Drawing.Point(20, 20);
            this.fgrdAddress.Name = "fgrdAddress";
            this.fgrdAddress.Rows = 50;
            this.fgrdAddress.Size = new System.Drawing.Size(893, 190);
            this.fgrdAddress.TabIndex = 0;
            this.fgrdAddress.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.fgrdAddress_ValidateEdit);
            this.fgrdAddress.DoubleClick += new System.EventHandler(this.fgrdAddress_DblClick);
            // 
            // cmdDeleteAddress
            // 
            this.cmdDeleteAddress.AppearanceKey = "actionButton";
            this.cmdDeleteAddress.Location = new System.Drawing.Point(469, 220);
            this.cmdDeleteAddress.Name = "cmdDeleteAddress";
            this.cmdDeleteAddress.Size = new System.Drawing.Size(73, 40);
            this.cmdDeleteAddress.TabIndex = 2;
            this.cmdDeleteAddress.Text = "Delete";
            this.cmdDeleteAddress.Click += new System.EventHandler(this.cmdDeleteAddress_Click);
            // 
            // cmdNewAddress
            // 
            this.cmdNewAddress.AppearanceKey = "actionButton";
            this.cmdNewAddress.Location = new System.Drawing.Point(390, 220);
            this.cmdNewAddress.Name = "cmdNewAddress";
            this.cmdNewAddress.Size = new System.Drawing.Size(59, 40);
            this.cmdNewAddress.TabIndex = 1;
            this.cmdNewAddress.Text = "New";
            this.cmdNewAddress.Click += new System.EventHandler(this.cmdNewAddress_Click);
            // 
            // tabDetails_Contacts
            // 
            this.tabDetails_Contacts.Controls.Add(this.cmdDeleteContact);
            this.tabDetails_Contacts.Controls.Add(this.cmdNewContact);
            this.tabDetails_Contacts.Controls.Add(this.fgrdContacts);
            this.tabDetails_Contacts.Controls.Add(this.fgrdSavedContactPhones);
            this.tabDetails_Contacts.Location = new System.Drawing.Point(1, 35);
            this.tabDetails_Contacts.Name = "tabDetails_Contacts";
            this.tabDetails_Contacts.Size = new System.Drawing.Size(933, 288);
            this.tabDetails_Contacts.Text = "Contacts";
            // 
            // fgrdContacts
            // 
            this.fgrdContacts.Cols = 7;
            this.fgrdContacts.ExtendLastCol = true;
            this.fgrdContacts.Location = new System.Drawing.Point(20, 20);
            this.fgrdContacts.Name = "fgrdContacts";
            this.fgrdContacts.Rows = 50;
            this.fgrdContacts.Size = new System.Drawing.Size(893, 190);
            this.fgrdContacts.TabIndex = 0;
            this.fgrdContacts.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.fgrdContacts_ValidateEdit);
            this.fgrdContacts.DoubleClick += new System.EventHandler(this.fgrdContacts_DblClick);
            // 
            // tabDetails_Comments
            // 
            this.tabDetails_Comments.Controls.Add(this.fgrdComments);
            this.tabDetails_Comments.Controls.Add(this.cmdDeleteComment);
            this.tabDetails_Comments.Controls.Add(this.cmdNewComment);
            this.tabDetails_Comments.Location = new System.Drawing.Point(1, 35);
            this.tabDetails_Comments.Name = "tabDetails_Comments";
            this.tabDetails_Comments.Size = new System.Drawing.Size(933, 288);
            this.tabDetails_Comments.Text = "Comments";
            // 
            // fgrdComments
            // 
            this.fgrdComments.Cols = 6;
            this.fgrdComments.ExtendLastCol = true;
            this.fgrdComments.Location = new System.Drawing.Point(20, 20);
            this.fgrdComments.Name = "fgrdComments";
            this.fgrdComments.Rows = 50;
            this.fgrdComments.Size = new System.Drawing.Size(893, 190);
            this.fgrdComments.TabIndex = 0;
            this.fgrdComments.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.fgrdComments_ValidateEdit);
            this.fgrdComments.DoubleClick += new System.EventHandler(this.fgrdComments_DblClick);
            // 
            // cmdDeleteComment
            // 
            this.cmdDeleteComment.AppearanceKey = "actionButton";
            this.cmdDeleteComment.Location = new System.Drawing.Point(471, 222);
            this.cmdDeleteComment.Name = "cmdDeleteComment";
            this.cmdDeleteComment.Size = new System.Drawing.Size(72, 40);
            this.cmdDeleteComment.TabIndex = 2;
            this.cmdDeleteComment.Text = "Delete";
            this.cmdDeleteComment.Click += new System.EventHandler(this.cmdDeleteComment_Click);
            // 
            // cmdNewComment
            // 
            this.cmdNewComment.AppearanceKey = "actionButton";
            this.cmdNewComment.Location = new System.Drawing.Point(389, 222);
            this.cmdNewComment.Name = "cmdNewComment";
            this.cmdNewComment.Size = new System.Drawing.Size(63, 40);
            this.cmdNewComment.TabIndex = 1;
            this.cmdNewComment.Text = "New";
            this.cmdNewComment.Click += new System.EventHandler(this.cmdNewComment_Click);
            // 
            // txtWebAddress
            // 
            this.txtWebAddress.BackColor = System.Drawing.SystemColors.Window;
            this.txtWebAddress.Location = new System.Drawing.Point(164, 213);
            this.txtWebAddress.Name = "txtWebAddress";
            this.txtWebAddress.Size = new System.Drawing.Size(329, 40);
            this.txtWebAddress.TabIndex = 19;
            this.txtWebAddress.TextChanged += new System.EventHandler(this.txtWebAddress_TextChanged);
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmail.Location = new System.Drawing.Point(164, 163);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(329, 40);
            this.txtEmail.TabIndex = 17;
            this.txtEmail.TextChanged += new System.EventHandler(this.txtEmail_TextChanged);
            // 
            // fraPhone
            // 
            this.fraPhone.AppearanceKey = "groupBoxNoBorders";
            this.fraPhone.Controls.Add(this.fgrdPhone);
            this.fraPhone.Location = new System.Drawing.Point(523, 164);
            this.fraPhone.Name = "fraPhone";
            this.fraPhone.Size = new System.Drawing.Size(442, 165);
            this.fraPhone.TabIndex = 21;
            this.fraPhone.Text = "Phone Numbers";
            this.fraPhone.UseMnemonic = false;
            // 
            // fgrdPhone
            // 
            this.fgrdPhone.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fgrdPhone.Cols = 5;
            this.fgrdPhone.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.fgrdPhone.ExtendLastCol = true;
            this.fgrdPhone.Location = new System.Drawing.Point(0, 30);
            this.fgrdPhone.Name = "fgrdPhone";
            this.fgrdPhone.ReadOnly = false;
            this.fgrdPhone.Rows = 3;
            this.fgrdPhone.Size = new System.Drawing.Size(436, 118);
            this.fgrdPhone.StandardTab = false;
            this.fgrdPhone.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.fgrdPhone.TabIndex = 0;
            this.fgrdPhone.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.fgrdPhone_ValidateEdit);
            this.fgrdPhone.KeyDown += new Wisej.Web.KeyEventHandler(this.fgrdPhone_KeyDown);
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.BackColor = System.Drawing.SystemColors.Window;
            this.txtCompanyName.Location = new System.Drawing.Point(244, 109);
            this.txtCompanyName.MaxLength = 255;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(452, 40);
            this.txtCompanyName.TabIndex = 15;
            this.txtCompanyName.Visible = false;
            this.txtCompanyName.TextChanged += new System.EventHandler(this.txtCompanyName_TextChanged);
            // 
            // txtLast
            // 
            this.txtLast.BackColor = System.Drawing.SystemColors.Window;
            this.txtLast.Location = new System.Drawing.Point(521, 109);
            this.txtLast.Name = "txtLast";
            this.txtLast.Size = new System.Drawing.Size(268, 40);
            this.txtLast.TabIndex = 11;
            this.txtLast.TextChanged += new System.EventHandler(this.txtLast_TextChanged);
            // 
            // txtMI
            // 
            this.txtMI.BackColor = System.Drawing.SystemColors.Window;
            this.txtMI.Location = new System.Drawing.Point(269, 109);
            this.txtMI.Name = "txtMI";
            this.txtMI.Size = new System.Drawing.Size(226, 40);
            this.txtMI.TabIndex = 9;
            this.txtMI.Text = "Text1";
            this.txtMI.TextChanged += new System.EventHandler(this.txtMI_TextChanged);
            // 
            // txtDesignation
            // 
            this.txtDesignation.BackColor = System.Drawing.SystemColors.Window;
            this.txtDesignation.Location = new System.Drawing.Point(812, 109);
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.Size = new System.Drawing.Size(144, 40);
            this.txtDesignation.TabIndex = 13;
            this.txtDesignation.Text = "Text1";
            this.ToolTip1.SetToolTip(this.txtDesignation, "Sr., Jr., 4th etc.");
            this.txtDesignation.TextChanged += new System.EventHandler(this.txtDesignation_TextChanged);
            // 
            // txtFirst
            // 
            this.txtFirst.BackColor = System.Drawing.SystemColors.Window;
            this.txtFirst.Location = new System.Drawing.Point(30, 109);
            this.txtFirst.MaxLength = 255;
            this.txtFirst.Name = "txtFirst";
            this.txtFirst.Size = new System.Drawing.Size(222, 40);
            this.txtFirst.TabIndex = 7;
            this.txtFirst.Text = "Text1";
            this.txtFirst.TextChanged += new System.EventHandler(this.txtFirst_TextChanged);
            // 
            // cboType
            // 
            this.cboType.BackColor = System.Drawing.SystemColors.Window;
            this.cboType.Items.AddRange(new object[] {
            "Individual",
            "Company"});
            this.cboType.Location = new System.Drawing.Point(105, 30);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(292, 40);
            this.cboType.TabIndex = 5;
            this.cboType.SelectedIndexChanged += new System.EventHandler(this.cboType_SelectedIndexChanged);
            // 
            // lblPartyCreatedBy
            // 
            this.lblPartyCreatedBy.Location = new System.Drawing.Point(386, 15);
            this.lblPartyCreatedBy.Name = "lblPartyCreatedBy";
            this.lblPartyCreatedBy.Size = new System.Drawing.Size(62, 13);
            this.lblPartyCreatedBy.TabIndex = 92;
            this.lblPartyCreatedBy.Text = "LABEL25";
            this.lblPartyCreatedBy.Visible = false;
            // 
            // lblGuid
            // 
            this.lblGuid.Location = new System.Drawing.Point(468, 15);
            this.lblGuid.Name = "lblGuid";
            this.lblGuid.Size = new System.Drawing.Size(64, 16);
            this.lblGuid.TabIndex = 91;
            this.lblGuid.Text = "LABEL25";
            this.lblGuid.Visible = false;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(30, 227);
            this.Label5.Name = "Label5";
            this.Label5.TabIndex = 18;
            this.Label5.Text = "WEB ADDRESS";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(30, 177);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(47, 15);
            this.Label4.TabIndex = 16;
            this.Label4.Text = "E-MAIL";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.AutoSize = true;
            this.lblCompanyName.Location = new System.Drawing.Point(30, 123);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(185, 15);
            this.lblCompanyName.TabIndex = 14;
            this.lblCompanyName.Text = "COMPANY OR GROUP NAME";
            this.lblCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCompanyName.Visible = false;
            // 
            // lblFirstName
            // 
            this.lblFirstName.AutoSize = true;
            this.lblFirstName.Location = new System.Drawing.Point(30, 88);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(42, 15);
            this.lblFirstName.TabIndex = 6;
            this.lblFirstName.Text = "FIRST";
            this.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMI
            // 
            this.lblMI.AutoSize = true;
            this.lblMI.Location = new System.Drawing.Point(269, 88);
            this.lblMI.Name = "lblMI";
            this.lblMI.Size = new System.Drawing.Size(53, 15);
            this.lblMI.TabIndex = 8;
            this.lblMI.Text = "MIDDLE";
            this.lblMI.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLast
            // 
            this.lblLast.AutoSize = true;
            this.lblLast.Location = new System.Drawing.Point(521, 88);
            this.lblLast.Name = "lblLast";
            this.lblLast.Size = new System.Drawing.Size(37, 15);
            this.lblLast.TabIndex = 10;
            this.lblLast.Text = "LAST";
            this.lblLast.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDesignation
            // 
            this.lblDesignation.AutoSize = true;
            this.lblDesignation.Location = new System.Drawing.Point(812, 88);
            this.lblDesignation.Name = "lblDesignation";
            this.lblDesignation.Size = new System.Drawing.Size(94, 15);
            this.lblDesignation.TabIndex = 12;
            this.lblDesignation.Text = "DESIGNATION";
            this.lblDesignation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(30, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(38, 15);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "TYPE";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCreatedDate
            // 
            this.lblCreatedDate.Location = new System.Drawing.Point(294, 15);
            this.lblCreatedDate.Name = "lblCreatedDate";
            this.lblCreatedDate.Size = new System.Drawing.Size(72, 20);
            this.lblCreatedDate.TabIndex = 3;
            this.lblCreatedDate.Visible = false;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(171, 15);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(104, 15);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "DATE CREATED";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label2.Visible = false;
            // 
            // lblParyNumber
            // 
            this.lblParyNumber.Location = new System.Drawing.Point(122, 26);
            this.lblParyNumber.Name = "lblParyNumber";
            this.lblParyNumber.Size = new System.Drawing.Size(111, 28);
            this.lblParyNumber.TabIndex = 1;
            this.lblParyNumber.Text = "00000";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSearch});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFileSearch
            // 
            this.mnuFileSearch.Index = 0;
            this.mnuFileSearch.Name = "mnuFileSearch";
            this.mnuFileSearch.Shortcut = Wisej.Web.Shortcut.CtrlS;
            this.mnuFileSearch.Text = "Search";
            this.mnuFileSearch.Visible = false;
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(454, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(91, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Save";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdFileNew
            // 
            this.cmdFileNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileNew.Location = new System.Drawing.Point(689, 29);
            this.cmdFileNew.Name = "cmdFileNew";
            this.cmdFileNew.Shortcut = Wisej.Web.Shortcut.CtrlShiftN;
            this.cmdFileNew.Size = new System.Drawing.Size(42, 24);
            this.cmdFileNew.TabIndex = 54;
            this.cmdFileNew.Text = "New";
            this.cmdFileNew.Click += new System.EventHandler(this.cmdFileNew_Click);
            // 
            // cmdFileDelete
            // 
            this.cmdFileDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileDelete.Location = new System.Drawing.Point(734, 29);
            this.cmdFileDelete.Name = "cmdFileDelete";
            this.cmdFileDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
            this.cmdFileDelete.Size = new System.Drawing.Size(54, 24);
            this.cmdFileDelete.TabIndex = 55;
            this.cmdFileDelete.Text = "Delete";
            this.cmdFileDelete.Visible = false;
            this.cmdFileDelete.Click += new System.EventHandler(this.cmdFileDelete_Click);
            // 
            // cmdPrevious
            // 
            this.cmdPrevious.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrevious.Location = new System.Drawing.Point(791, 29);
            this.cmdPrevious.Name = "cmdPrevious";
            this.cmdPrevious.Shortcut = Wisej.Web.Shortcut.F7;
            this.cmdPrevious.Size = new System.Drawing.Size(66, 24);
            this.cmdPrevious.TabIndex = 56;
            this.cmdPrevious.Text = "Previous";
            this.cmdPrevious.Click += new System.EventHandler(this.cmdPrevious_Click);
            // 
            // cmdFileNext
            // 
            this.cmdFileNext.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileNext.Location = new System.Drawing.Point(860, 29);
            this.cmdFileNext.Name = "cmdFileNext";
            this.cmdFileNext.Shortcut = Wisej.Web.Shortcut.F8;
            this.cmdFileNext.Size = new System.Drawing.Size(42, 24);
            this.cmdFileNext.TabIndex = 57;
            this.cmdFileNext.Text = "Next";
            this.cmdFileNext.Click += new System.EventHandler(this.cmdFileNext_Click);
            // 
            // cmdFileSave
            // 
            this.cmdFileSave.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileSave.Location = new System.Drawing.Point(906, 29);
            this.cmdFileSave.Name = "cmdFileSave";
            this.cmdFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdFileSave.Size = new System.Drawing.Size(44, 24);
            this.cmdFileSave.TabIndex = 58;
            this.cmdFileSave.Text = "Save";
            this.cmdFileSave.Click += new System.EventHandler(this.cmdFileSave_Click);
            // 
            // mnuSep
            // 
            this.mnuSep.Index = -1;
            this.mnuSep.Name = "mnuSep";
            this.mnuSep.Text = "-";
            this.mnuSep.Visible = false;
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = -1;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = -1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = -1;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = -1;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuSep2
            // 
            this.mnuSep2.Index = -1;
            this.mnuSep2.Name = "mnuSep2";
            this.mnuSep2.Text = "-";
            // 
            // mnuFileNext
            // 
            this.mnuFileNext.Index = -1;
            this.mnuFileNext.Name = "mnuFileNext";
            this.mnuFileNext.Shortcut = Wisej.Web.Shortcut.F8;
            this.mnuFileNext.Text = "Next";
            this.mnuFileNext.Click += new System.EventHandler(this.mnuFileNext_Click);
            // 
            // mnuPrevious
            // 
            this.mnuPrevious.Index = -1;
            this.mnuPrevious.Name = "mnuPrevious";
            this.mnuPrevious.Shortcut = Wisej.Web.Shortcut.F7;
            this.mnuPrevious.Text = "Previous";
            this.mnuPrevious.Click += new System.EventHandler(this.mnuPrevious_Click);
            // 
            // mnuSep3
            // 
            this.mnuSep3.Index = -1;
            this.mnuSep3.Name = "mnuSep3";
            this.mnuSep3.Text = "-";
            // 
            // mnuFileDelete
            // 
            this.mnuFileDelete.Enabled = false;
            this.mnuFileDelete.Index = -1;
            this.mnuFileDelete.Name = "mnuFileDelete";
            this.mnuFileDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
            this.mnuFileDelete.Text = "Delete";
            this.mnuFileDelete.Visible = false;
            this.mnuFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
            // 
            // mnuFileNew
            // 
            this.mnuFileNew.Index = -1;
            this.mnuFileNew.Name = "mnuFileNew";
            this.mnuFileNew.Shortcut = Wisej.Web.Shortcut.CtrlShiftN;
            this.mnuFileNew.Text = "New";
            this.mnuFileNew.Click += new System.EventHandler(this.mnuFileNew_Click);
            // 
            // frmEditCentralParties
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(998, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmEditCentralParties";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Central Parties";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmEditCentralParties_Load);
            this.Activated += new System.EventHandler(this.frmEditCentralParties_Activated);
            this.Resize += new System.EventHandler(this.frmEditCentralParties_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEditCentralParties_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.fraWait.ResumeLayout(false);
            this.fraComment.ResumeLayout(false);
            this.fraComment.PerformLayout();
            this.fraContact.ResumeLayout(false);
            this.fraContact.PerformLayout();
            this.Frame2.ResumeLayout(false);
            this.tabDetails_Page4.ResumeLayout(false);
            this.fraAddress.ResumeLayout(false);
            this.fraAddress.PerformLayout();
            this.fraOverrideCriteria.ResumeLayout(false);
            this.fraOverrideCriteria.PerformLayout();
            this.fraSeasonal.ResumeLayout(false);
            this.fraSeasonal.PerformLayout();
            this.tabDetails.ResumeLayout(false);
            this.tabDetails_Address.ResumeLayout(false);
            this.tabDetails_Contacts.ResumeLayout(false);
            this.tabDetails_Comments.ResumeLayout(false);
            this.fraPhone.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
		public FCButton cmdFileNew;
		public FCButton cmdFileDelete;
		public FCButton cmdPrevious;
		public FCButton cmdFileNext;
		public FCButton cmdFileSave;
		public FCToolStripMenuItem mnuSep;
		public FCToolStripMenuItem mnuProcessQuit;
		public FCToolStripMenuItem Seperator;
		public FCToolStripMenuItem mnuProcessSave;
		public FCToolStripMenuItem mnuFileSave;
		public FCToolStripMenuItem mnuSep2;
		public FCToolStripMenuItem mnuFileNext;
		public FCToolStripMenuItem mnuPrevious;
		public FCToolStripMenuItem mnuSep3;
		public FCToolStripMenuItem mnuFileDelete;
		public FCToolStripMenuItem mnuFileNew;
	}
}
