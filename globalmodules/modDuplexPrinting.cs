﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using GrapeCity.ActiveReports;
using System.Collections.Generic;

#if TWRE0000
using TWRE0000;
#endif
namespace Global
{
	public class modDuplexPrinting
	{
		//public static bool willPrintDuplex(string strPrinter)
		//{
		//	return false;
		//}

		public static void DuplexPrintReport(GrapeCity.ActiveReports.SectionReport rptObj, string strPrinter = "", bool boolForcePrint = false, bool blnLandscape = false, List<string> batchReports = null)
		{

            rptObj.Run();

            rptObj.ExportToPDFAndPrint(batchReports);

        }

  //      public static string GetPrinterManually(int lngMaxPages = 1, int intStartPage = 1, int intEndPage = 0, int intNumCopies = 1, bool boolDisablePageNums = false, int intOrientation = -1)
		//{
		//	string GetPrinterManually = "";
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		if (!boolDisablePageNums)
		//		{
		//			App.MainForm.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.PrinterConstants.cdlPDPageNums) + FCConvert.ToInt32(FCCommonDialog.PrinterConstants.cdlPDUseDevModeCopies);
		//			// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
		//		}
		//		else
		//		{
		//			App.MainForm.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.PrinterConstants.cdlPDPageNums) + FCConvert.ToInt32(FCCommonDialog.PrinterConstants.cdlPDUseDevModeCopies) + FCConvert.ToInt32(FCCommonDialog.PrinterConstants.cdlPDNoPageNums);
		//			// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
		//		}
		//		App.MainForm.CommonDialog1.Max = lngMaxPages;
		//		App.MainForm.CommonDialog1.FromPage = 1;
		//		App.MainForm.CommonDialog1.ToPage = lngMaxPages;
		//		App.MainForm.CommonDialog1.CancelError = true;
		//		App.MainForm.CommonDialog1.ShowPrinter();
		//		GetPrinterManually = FCGlobal.Printer.DeviceName;
		//		intStartPage = App.MainForm.CommonDialog1.FromPage;
		//		intEndPage = App.MainForm.CommonDialog1.ToPage;
		//		intNumCopies = App.MainForm.CommonDialog1.PrinterSettings.Copies;
		//		intOrientation = FCConvert.ToInt32(App.MainForm.CommonDialog1.Orientation);
		//		return GetPrinterManually;
		//	}
		//	catch (Exception ex)
		//	{
				
		//		GetPrinterManually = "";
		//	}
		//	return GetPrinterManually;
		//}
	}
}
