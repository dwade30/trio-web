﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using System.Runtime.InteropServices;
using fecherFoundation;
using Microsoft.VisualBasic;


namespace TWPY0000
{
	public class cBrowseForFolder
	{
		//=========================================================
		const short BIF_STATUSTEXT = 0x4;
		const short BIF_RETURNONLYFSDIRS = 1;
		const short BIF_DONTGOBELOWDOMAIN = 2;
		const short MAX_PATH = 260;
		const short WM_USER = 0x400;
		const short BFFM_INITIALIZED = 1;
		const short BFFM_SELCHANGED = 2;
		const short BFFM_SETSTATUSTEXT = (WM_USER + 100);
		const short BFFM_SETSELECTION = (WM_USER + 102);

		[DllImport("user32", EntryPoint = "SendMessageA")]
		private static extern int SendMessage(int hWnd, int wMsg, int wParam, string lParam);

		[DllImport("shell32")]
		private static extern int SHBrowseForFolder(ref BrowseInfo lpbi);

		[DllImport("shell32")]
		private static extern int SHGetPathFromIDList(int pidList, IntPtr lpBuffer);

		private int SHGetPathFromIDListWrp(int pidList, ref string lpBuffer)
		{
			int ret;
			IntPtr plpBuffer = FCUtils.GetByteFromString(lpBuffer);
			ret = SHGetPathFromIDList(pidList, plpBuffer);
			FCUtils.GetStringFromByte(ref lpBuffer, plpBuffer);
			return ret;
		}

		[DllImport("kernel32", EntryPoint = "lstrcatA")]
		private static extern int lstrcat(IntPtr lpString1, string lpString2);

		private int lstrcatWrp(ref string lpString1, string lpString2)
		{
			int ret;
			IntPtr plpString1 = FCUtils.GetByteFromString(lpString1);
			ret = lstrcat(plpString1, lpString2);
			FCUtils.GetStringFromByte(ref lpString1, plpString1);
			return ret;
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct BrowseInfo
		{
			public int hWndOwner;
			public int pIDLRoot;
			public int pszDisplayName;
			public int lpszTitle;
			public int ulFlags;
			public int lpfnCallback;
			public int lParam;
			public int iImage;
		};

		private string m_CurrentDirectory = string.Empty;
		private string strTitle = string.Empty;
		private string strDirectory = string.Empty;

		public string Directory
		{
			set
			{
				strDirectory = value;
			}
			get
			{
				string Directory = "";
				Directory = strDirectory;
				return Directory;
			}
		}

		public string Title
		{
			set
			{
				strTitle = value;
			}
			get
			{
				string Title = "";
				Title = strTitle;
				return Title;
			}
		}

		public void ShowDialog()
		{
			int lpIDList;
			string szTitle;
			string sBuffer = "";
			BrowseInfo tBrowseInfo = new BrowseInfo();
			m_CurrentDirectory = strDirectory + "\0";
			szTitle = strTitle;
			// .hWndOwner = owner.hWnd
			tBrowseInfo.lpszTitle = lstrcatWrp(ref szTitle, "");
			tBrowseInfo.ulFlags = BIF_RETURNONLYFSDIRS + BIF_DONTGOBELOWDOMAIN + BIF_STATUSTEXT;
			// .lpfnCallback = GetAddressofFunction(AddressOf BrowseCallbackProc)  'get address of function.
			lpIDList = SHBrowseForFolder(ref tBrowseInfo);
			if (FCConvert.ToBoolean((lpIDList)))
			{
				sBuffer = Strings.Space(MAX_PATH);
				SHGetPathFromIDListWrp(lpIDList, ref sBuffer);
				//FC:FINAL:DSE:#i2317 String does not contain the '\0' terminal character
				//sBuffer = Strings.Left(sBuffer, Strings.InStr(sBuffer, "\0", CompareConstants.vbBinaryCompare) - 1);
				strDirectory = sBuffer;
			}
			else
			{
				strDirectory = "";
			}
		}

		private int BrowseCallbackProc(int hWnd, int uMsg, int lp, int pData)
		{
			int BrowseCallbackProc = 0;
			int lpIDList;
			int ret = 0;
			string sBuffer = "";
			/*? On Error Resume Next  */// Sugested by MS to prevent an error from
			// propagating back into the calling process.
			switch (uMsg)
			{
				case BFFM_INITIALIZED:
					{
						if (m_CurrentDirectory != "")
						{
							SendMessage(hWnd, BFFM_SETSELECTION, 1, m_CurrentDirectory);
						}
						break;
					}
				case BFFM_SELCHANGED:
					{
						sBuffer = Strings.Space(MAX_PATH);
						ret = SHGetPathFromIDListWrp(lp, ref sBuffer);
						if (ret == 1)
						{
							SendMessage(hWnd, BFFM_SETSTATUSTEXT, 0, sBuffer);
						}
						break;
					}
			}
			//end switch
			BrowseCallbackProc = 0;
			return BrowseCallbackProc;
		}

		private int GetAddressofFunction(ref int add)
		{
			int GetAddressofFunction = 0;
			GetAddressofFunction = add;
			return GetAddressofFunction;
		}
	}
}
