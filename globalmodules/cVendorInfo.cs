﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cVendorInfo
	{
		//=========================================================
		private int lngRecordID;
		private int lngVendorNumber;
		private string strCheckName = string.Empty;
		private string strCheckAddress1 = string.Empty;
		private string strCheckAddress2 = string.Empty;
		private string strCheckAddress3 = string.Empty;
		private string strCheckAddress4 = string.Empty;
		private string strEmail = string.Empty;
		private double dblInvoiceAmount;
		private string strInvoiceDescription = string.Empty;

		public string Description
		{
			set
			{
				strInvoiceDescription = value;
			}
			get
			{
				string Description = "";
				Description = strInvoiceDescription;
				return Description;
			}
		}

		public double Amount
		{
			set
			{
				dblInvoiceAmount = value;
			}
			get
			{
				double Amount = 0;
				Amount = dblInvoiceAmount;
				return Amount;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int VendorNumber
		{
			set
			{
				lngVendorNumber = value;
			}
			get
			{
				int VendorNumber = 0;
				VendorNumber = lngVendorNumber;
				return VendorNumber;
			}
		}

		public string CheckName
		{
			set
			{
				strCheckName = value;
			}
			get
			{
				string CheckName = "";
				CheckName = strCheckName;
				return CheckName;
			}
		}

		public string CheckAddress1
		{
			set
			{
				strCheckAddress1 = value;
			}
			get
			{
				string CheckAddress1 = "";
				CheckAddress1 = strCheckAddress1;
				return CheckAddress1;
			}
		}

		public string CheckAddress2
		{
			set
			{
				strCheckAddress2 = value;
			}
			get
			{
				string CheckAddress2 = "";
				CheckAddress2 = strCheckAddress2;
				return CheckAddress2;
			}
		}

		public string CheckAddress3
		{
			set
			{
				strCheckAddress3 = value;
			}
			get
			{
				string CheckAddress3 = "";
				CheckAddress3 = strCheckAddress3;
				return CheckAddress3;
			}
		}

		public string CheckAddress4
		{
			set
			{
				strCheckAddress4 = value;
			}
			get
			{
				string CheckAddress4 = "";
				CheckAddress4 = strCheckAddress4;
				return CheckAddress4;
			}
		}

		public string EMail
		{
			set
			{
				strEmail = value;
			}
			get
			{
				string EMail = "";
				EMail = strEmail;
				return EMail;
			}
		}
	}
}
