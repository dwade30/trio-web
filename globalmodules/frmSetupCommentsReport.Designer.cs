﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmSetupCommentsReport.
	/// </summary>
	partial class frmSetupCommentsReport : BaseForm
	{
		public fecherFoundation.FCFrame fraDateRange;
		public Global.T2KDateBox txtEndDate;
		public Global.T2KDateBox txtStartDate;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame fraAcctRange;
		public fecherFoundation.FCTextBox txtEndAcct;
		public fecherFoundation.FCTextBox txtStartAcct;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblInstr2;
		public fecherFoundation.FCLabel lblInstr;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fraDateRange = new fecherFoundation.FCFrame();
			this.txtEndDate = new Global.T2KDateBox();
			this.txtStartDate = new Global.T2KDateBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.fraAcctRange = new fecherFoundation.FCFrame();
			this.txtEndAcct = new fecherFoundation.FCTextBox();
			this.txtStartAcct = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblInstr2 = new fecherFoundation.FCLabel();
			this.lblInstr = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cdmSaveContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
			this.fraDateRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAcctRange)).BeginInit();
			this.fraAcctRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cdmSaveContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cdmSaveContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 371);
			this.BottomPanel.Size = new System.Drawing.Size(534, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraDateRange);
			this.ClientArea.Controls.Add(this.fraAcctRange);
			this.ClientArea.Controls.Add(this.lblInstr2);
			this.ClientArea.Controls.Add(this.lblInstr);
			this.ClientArea.Size = new System.Drawing.Size(534, 311);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(534, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(269, 30);
			this.HeaderText.Text = "Print Comments Report";
			// 
			// fraDateRange
			// 
			this.fraDateRange.Controls.Add(this.txtEndDate);
			this.fraDateRange.Controls.Add(this.txtStartDate);
			this.fraDateRange.Controls.Add(this.Label2);
			this.fraDateRange.Location = new System.Drawing.Point(30, 197);
			this.fraDateRange.Name = "fraDateRange";
			this.fraDateRange.Size = new System.Drawing.Size(476, 90);
			this.fraDateRange.TabIndex = 3;
			this.fraDateRange.Text = "Dates";
			// 
			// txtEndDate
			// 
			this.txtEndDate.Location = new System.Drawing.Point(279, 30);
			this.txtEndDate.Mask = "##/##/####";
			this.txtEndDate.Name = "txtEndDate";
			this.txtEndDate.Size = new System.Drawing.Size(177, 40);
			this.txtEndDate.TabIndex = 2;
			this.txtEndDate.Text = "  /  /";
			// 
			// txtStartDate
			// 
			this.txtStartDate.Location = new System.Drawing.Point(20, 30);
			this.txtStartDate.Mask = "##/##/####";
			this.txtStartDate.Name = "txtStartDate";
			this.txtStartDate.Size = new System.Drawing.Size(177, 40);
			this.txtStartDate.TabIndex = 0;
			this.txtStartDate.Text = "  /  /";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(224, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(29, 19);
			this.Label2.TabIndex = 1;
			this.Label2.Text = "TO";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// fraAcctRange
			// 
			this.fraAcctRange.Controls.Add(this.txtEndAcct);
			this.fraAcctRange.Controls.Add(this.txtStartAcct);
			this.fraAcctRange.Controls.Add(this.Label1);
			this.fraAcctRange.Location = new System.Drawing.Point(30, 90);
			this.fraAcctRange.Name = "fraAcctRange";
			this.fraAcctRange.Size = new System.Drawing.Size(476, 90);
			this.fraAcctRange.TabIndex = 2;
			this.fraAcctRange.Text = "Accounts";
			// 
			// txtEndAcct
			// 
			this.txtEndAcct.AutoSize = false;
			this.txtEndAcct.BackColor = System.Drawing.SystemColors.Window;
			this.txtEndAcct.LinkItem = null;
			this.txtEndAcct.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEndAcct.LinkTopic = null;
			this.txtEndAcct.Location = new System.Drawing.Point(279, 30);
			this.txtEndAcct.MaxLength = 10;
			this.txtEndAcct.Name = "txtEndAcct";
			this.txtEndAcct.Size = new System.Drawing.Size(177, 40);
			this.txtEndAcct.TabIndex = 2;
			this.txtEndAcct.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtEndAcct_KeyPress);
			// 
			// txtStartAcct
			// 
			this.txtStartAcct.AutoSize = false;
			this.txtStartAcct.BackColor = System.Drawing.SystemColors.Window;
			this.txtStartAcct.LinkItem = null;
			this.txtStartAcct.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStartAcct.LinkTopic = null;
			this.txtStartAcct.Location = new System.Drawing.Point(20, 30);
			this.txtStartAcct.MaxLength = 10;
			this.txtStartAcct.Name = "txtStartAcct";
			this.txtStartAcct.Size = new System.Drawing.Size(177, 40);
			this.txtStartAcct.TabIndex = 0;
			this.txtStartAcct.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtStartAcct_KeyPress);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(224, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(29, 19);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "TO";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblInstr2
			// 
			this.lblInstr2.Location = new System.Drawing.Point(30, 57);
			this.lblInstr2.Name = "lblInstr2";
			this.lblInstr2.Size = new System.Drawing.Size(201, 19);
			this.lblInstr2.TabIndex = 1;
			this.lblInstr2.Text = "F12 TO CONTINUE";
			this.lblInstr2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblInstr
			// 
			this.lblInstr.Location = new System.Drawing.Point(30, 30);
			this.lblInstr.Name = "lblInstr";
			this.lblInstr.Size = new System.Drawing.Size(430, 19);
			this.lblInstr.TabIndex = 0;
			this.lblInstr.Text = "ENTER RANGE TO REPORT OR LEAVE BLANK FOR ALL COMMENTS";
			this.lblInstr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveContinue,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cdmSaveContinue
			// 
			this.cdmSaveContinue.AppearanceKey = "acceptButton";
			this.cdmSaveContinue.Location = new System.Drawing.Point(178, 33);
			this.cdmSaveContinue.Name = "cdmSaveContinue";
			this.cdmSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cdmSaveContinue.Size = new System.Drawing.Size(175, 48);
			this.cdmSaveContinue.TabIndex = 0;
			this.cdmSaveContinue.Text = "Save & Continue";
			this.cdmSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// frmSetupCommentsReport
			// 
			this.ClientSize = new System.Drawing.Size(534, 479);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSetupCommentsReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Print Comments Report";
			this.Load += new System.EventHandler(this.frmSetupCommentsReport_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSetupCommentsReport_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
			this.fraDateRange.ResumeLayout(false);
			this.fraDateRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAcctRange)).EndInit();
			this.fraAcctRange.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cdmSaveContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cdmSaveContinue;
	}
}
