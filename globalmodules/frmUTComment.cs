﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmUTComment.
	/// </summary>
	public partial class frmUTComment : BaseForm
	{
		public frmUTComment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUTComment InstancePtr
		{
			get
			{
				return (frmUTComment)Sys.GetInstance(typeof(frmUTComment));
			}
		}

		protected frmUTComment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/26/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/28/2005              *
		// ********************************************************
		int lngAcct;
		int lngAcctKey;
		clsDRWrapper rsData = new clsDRWrapper();
		string strCommentText;

		public string Init(ref int lngPassAcctKey)
		{
			string Init = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				lngAcctKey = lngPassAcctKey;
				rsData.OpenRecordset("SELECT * FROM CommentRec WHERE AccountKey = " + FCConvert.ToString(lngAcctKey), modExtraModules.strUTDatabase);
				if (!rsData.EndOfFile())
				{
					mnuFilePrint.Enabled = true;
					txtComment.Text = FCConvert.ToString(rsData.Get_Fields_String("Comment"));
				}
				else
				{
					mnuFilePrint.Enabled = false;
					txtComment.Text = "";
				}
				strCommentText = txtComment.Text;
				// initialize the text
				this.Text = "Comment for Account " + FCConvert.ToString(modUTFunctions.GetAccountNumber(ref lngAcctKey));
				this.Show(FormShowEnum.Modal, App.MainForm);
				Init = strCommentText;
				return Init;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Intializing Comment Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Init;
		}

		private void frmUTComment_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmUTComment_Load(object sender, System.EventArgs e)
		{
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// set the back color of any labels or textboxes
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			rptCommentsReport.InstancePtr.Init(this.Modal, lngAcctKey);
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			strCommentText = txtComment.Text;
			rsData.OpenRecordset("SELECT * FROM CommentRec WHERE AccountKey = " + FCConvert.ToString(lngAcctKey), modExtraModules.strUTDatabase);
			if (!rsData.EndOfFile())
			{
				rsData.Edit();
			}
			else
			{
				rsData.AddNew();
			}
			rsData.Set_Fields("AccountKey", lngAcctKey);
			rsData.Set_Fields("Comment", strCommentText);
			rsData.Set_Fields("Updated", Strings.Format(DateTime.Now, "MM/dd/yyyy"));
			// kk07282016 trout-1131 Add ability to filter by Date
			rsData.Update();
			if (Strings.Trim(strCommentText) != "")
			{
				mnuFilePrint.Enabled = true;
			}
			else
			{
				mnuFilePrint.Enabled = false;
			}
            //FC:FINAL:BSE #3782 add save message 
            MessageBox.Show("Comment Saved", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

		public void mnuFileSave_Click()
		{
			mnuFileSave_Click(mnuFileSave, new System.EventArgs());
		}

		private void mnuFileSaveandExit_Click(object sender, System.EventArgs e)
		{
			// kk01192017 trout-1252  Update this so comment isn't automatically updated when we exit
			mnuFileSave_Click();
            // strCommentText = txtComment.Text
			Close();
		}
	}
}
