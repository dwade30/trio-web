//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWAR0000
using TWAR0000;


#elif TWBD0000
using TWBD0000;


#elif TWCR0000
using TWCR0000;


#elif TWFA0000
using TWFA0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmSelectPostingJournal.
	/// </summary>
	partial class frmSelectPostingJournal : BaseForm
	{
		public fecherFoundation.FCComboBox cboJournals;
		public fecherFoundation.FCButton CancelButton;
		public fecherFoundation.FCButton OKButton;
		public fecherFoundation.FCLabel Label1;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cboJournals = new fecherFoundation.FCComboBox();
            this.CancelButton = new fecherFoundation.FCButton();
            this.OKButton = new fecherFoundation.FCButton();
            this.Label1 = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CancelButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OKButton)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 245);
            this.BottomPanel.Size = new System.Drawing.Size(415, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cboJournals);
            this.ClientArea.Controls.Add(this.CancelButton);
            this.ClientArea.Controls.Add(this.OKButton);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(415, 185);
            this.ClientArea.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(415, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(201, 30);
            this.HeaderText.Text = "Journal Selection";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cboJournals
            // 
            this.cboJournals.BackColor = System.Drawing.SystemColors.Window;
            this.cboJournals.Location = new System.Drawing.Point(30, 70);
            this.cboJournals.Name = "cboJournals";
            this.cboJournals.Size = new System.Drawing.Size(289, 40);
            this.cboJournals.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cboJournals, null);
            this.cboJournals.SelectedIndexChanged += new System.EventHandler(this.cboJournals_SelectedIndexChanged);
            // 
            // CancelButton
            // 
            this.CancelButton.AppearanceKey = "actionButton";
            this.CancelButton.ForeColor = System.Drawing.Color.White;
            this.CancelButton.Location = new System.Drawing.Point(133, 130);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(85, 40);
            this.CancelButton.TabIndex = 3;
            this.CancelButton.Text = "Cancel";
            this.ToolTip1.SetToolTip(this.CancelButton, null);
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // OKButton
            // 
            this.OKButton.AppearanceKey = "actionButton";
            this.OKButton.ForeColor = System.Drawing.Color.White;
            this.OKButton.Location = new System.Drawing.Point(30, 129);
            this.OKButton.Name = "OKButton";
            this.OKButton.Size = new System.Drawing.Size(86, 40);
            this.OKButton.TabIndex = 2;
            this.OKButton.Text = "OK";
            this.ToolTip1.SetToolTip(this.OKButton, null);
            this.OKButton.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(383, 23);
            this.Label1.Text = "PLEASE SELECT A JOURNAL AND CLICK THE OK BUTTON";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // frmSelectPostingJournal
            // 
            this.AcceptButton = this.OKButton;
            this.ClientSize = new System.Drawing.Size(415, 353);
            this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSelectPostingJournal";
            this.ShowInTaskbar = false;
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Journal Selection";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmSelectPostingJournal_Load);
            this.Activated += new System.EventHandler(this.frmSelectPostingJournal_Activated);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CancelButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OKButton)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}