﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using fecherFoundation.Extensions;
using System;
using System.Collections.Generic;
using TWSharedLibrary;
using TWSharedLibrary.Data;
using Wisej.Web;

namespace Global
{
    public class cBDAccountController
    {
        //=========================================================
        private string strLastError = "";
        private int lngLastError;
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cALFRange alfRangeSetup = new cALFRange();
        private cALFRange alfRangeSetup_AutoInitialized;

        private cALFRange alfRangeSetup
        {
            get
            {
                if (alfRangeSetup_AutoInitialized == null)
                {
                    alfRangeSetup_AutoInitialized = new cALFRange();
                }
                return alfRangeSetup_AutoInitialized;
            }
            set
            {
                alfRangeSetup_AutoInitialized = value;
            }
        }

        private Dictionary<object, object> dictExpenses = new Dictionary<object, object>();
        private Dictionary<object, object> dictDepartments = new Dictionary<object, object>();
        private Dictionary<object, object> dictRevenues = new Dictionary<object, object>();
        private Dictionary<object, object> dictFunds = new Dictionary<object, object>();

        public string LastErrorMessage
        {
            get
            {
                string LastErrorMessage = "";
                LastErrorMessage = strLastError;
                return LastErrorMessage;
            }
        }

        public int LastErrorNumber
        {
            get
            {
                int LastErrorNumber = 0;
                LastErrorNumber = lngLastError;
                return LastErrorNumber;
            }
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As bool
        public bool HadError
        {
            get
            {
                bool HadError = false;
                HadError = lngLastError != 0;
                return HadError;
            }
        }

        public void ClearErrors()
        {
            strLastError = "";
            lngLastError = 0;
        }

        private void SetError(int lngErrorNumber, string strErrorMessage)
        {
            lngLastError = lngErrorNumber;
            strLastError = strErrorMessage;
        }

        public cFund GetFund(int lngID)
        {
            cFund GetFund = null;
            ClearErrors();
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                cFund retFund = null;
                rsLoad.OpenRecordset("select * from ledgerTitles where id = " + FCConvert.ToString(lngID), "Budgetary");

                if (!rsLoad.EndOfFile())
                {
                    retFund = new cFund();
                    FillFund(ref retFund, ref rsLoad);
                }

                GetFund = retFund;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetFund;
        }

        private void FillFund(ref cFund theFund, ref clsDRWrapper rs)
        {
            try
            {
                // On Error GoTo ErrorHandler
                if (!rs.EndOfFile())
                {
                    theFund.CashFund = rs.Get_Fields_String("cashfund");
                    theFund.Description = rs.Get_Fields_String("LongDescription");
                    // TODO: Check the table for the column [fund] and replace with corresponding Get_Field method
                    theFund.Fund = rs.Get_Fields_String("fund");
                    theFund.GLAccountType = rs.Get_Fields_String("glaccounttype");
                    theFund.ShortDescription = rs.Get_Fields_String("ShortDescription");
                    theFund.ID = rs.Get_Fields_Int32("ID");
                    theFund.IsUpdated = false;
                }
                return;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        public cFund GetFundByFund(string strFund)
        {
            cFund GetFundByFund = null;
            ClearErrors();
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                cFund retFund = null;
                rsLoad.OpenRecordset("select * from ledgerTitles where fund = '" + strFund + "' and account = '000'", "Budgetary");

                if (!rsLoad.EndOfFile())
                {
                    retFund = new cFund();
                    FillFund(ref retFund, ref rsLoad);
                }

                GetFundByFund = retFund;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetFundByFund;
        }

        public cGenericCollection GetFunds()
        {
            cGenericCollection GetFunds = null;
            ClearErrors();
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                cGenericCollection retColl = new cGenericCollection();
                cFund retFund;
                rsLoad.OpenRecordset("select * from LedgerTitles where convert(int,isnull(account,'0')) = 0 order by fund", "Budgetary");

                while (!rsLoad.EndOfFile())
                {
                    ////////Application.DoEvents();
                    retFund = new cFund();
                    FillFund(ref retFund, ref rsLoad);
                    retColl.AddItem(retFund);
                    rsLoad.MoveNext();
                }

                GetFunds = retColl;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetFunds;
        }

        public Dictionary<object, object> GetFundsAsDictionary()
        {
            Dictionary<object, object> GetFundsAsDictionary = new Dictionary<object, object>();
            ClearErrors();
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                Dictionary<object, object> retDict = new Dictionary<object, object>();
                cFund retFund;
                rsLoad.OpenRecordset("select * from LedgerTitles where convert(int,isnull(account,'0')) = 0 order by fund", "Budgetary");

                while (!rsLoad.EndOfFile())
                {
                    ////////Application.DoEvents();
                    retFund = new cFund();
                    FillFund(ref retFund, ref rsLoad);
                    retDict.Add(retFund.Fund, retFund);
                    rsLoad.MoveNext();
                }

                GetFundsAsDictionary = retDict;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetFundsAsDictionary;
        }

        private void LoadFundsDictionary()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            dictFunds.Clear();
            cFund retFund;
            rsLoad.OpenRecordset("Select * from ledgertitles where convert(int,isnull(account,'0')) = 0 order by fund", "Budgetary");
            while (!rsLoad.EndOfFile())
            {
                ////////Application.DoEvents();
                retFund = new cFund();
                FillFund(ref retFund, ref rsLoad);
                LoadAccountsForFund(ref retFund);
                dictFunds.Add(retFund.Fund, retFund);
                rsLoad.MoveNext();
            }
            rsLoad.DisposeOf();
        }

        private void LoadAccountsForFund(ref cFund retFund)
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            cBDAccountTitle acctTitle;
            string strAcct = "";
            rsLoad.OpenRecordset("Select * from ledgertitles where fund = '" + retFund.Fund + "' and convert(int,isnull(account,'0')) > 0 order by account, isnull([year],'')", "Budgetary");
            while (!rsLoad.EndOfFile())
            {
                ////////Application.DoEvents();
                acctTitle = new cBDAccountTitle();
                FillLedgerTitle(ref acctTitle, ref rsLoad);
                // TODO: Check the table for the column [account] and replace with corresponding Get_Field method
                // TODO: Check the table for the column [year] and replace with corresponding Get_Field method
                strAcct = rsLoad.Get_Fields_String("account") + "-" + rsLoad.Get_Fields_String("year");
                if (!retFund.AccountTitles.ContainsKey(strAcct))
                {
                    retFund.AccountTitles.Add(strAcct, acctTitle);
                }
                rsLoad.MoveNext();
            }
            rsLoad.DisposeOf();
        }

        public cGenericCollection GetFundsByFundRange(string strStartFund, string strEndFund)
        {
            cGenericCollection GetFundsByFundRange = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cGenericCollection retColl = new cGenericCollection();
                cFund retFund;
                string strSql;
                strSql = "select * from LedgerTitles where cast(isnull(account,'0') as int) = 0 ";
                if (strEndFund != "")
                {
                    strSql += " and fund between '" + strStartFund + "' and '" + strEndFund + "' ";
                }
                else if (strStartFund != "")
                {
                    strSql += " and fund >= '" + strStartFund + "' ";
                }
                strSql += " order by fund";
                rsLoad.OpenRecordset(strSql, "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    ////////Application.DoEvents();
                    retFund = new cFund();
                    FillFund(ref retFund, ref rsLoad);
                    retColl.AddItem(retFund);
                    rsLoad.MoveNext();
                }
                rsLoad.DisposeOf();
                GetFundsByFundRange = retColl;
                return GetFundsByFundRange;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetFundsByFundRange;
        }

        public cGenericCollection GetLedgerTitles()
        {
            cGenericCollection GetLedgerTitles = null;
            ClearErrors();
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                cGenericCollection retColl = new cGenericCollection();
                cBDAccountTitle retTitle;
                string strSql;
                strSql = "select * from LedgerTitles order by fund,account,[year] ";
                rsLoad.OpenRecordset(strSql, "Budgetary");

                while (!rsLoad.EndOfFile())
                {
                    retTitle = new cBDAccountTitle();
                    FillLedgerTitle(ref retTitle, ref rsLoad);
                    retColl.AddItem(retTitle);
                    rsLoad.MoveNext();
                }

                GetLedgerTitles = retColl;

                return GetLedgerTitles;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetLedgerTitles;
        }

        public cGenericCollection GetAssetLedgerTitlesByFund(string strFund)
        {
            cGenericCollection GetAssetLedgerTitlesByFund = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cGenericCollection retColl = new cGenericCollection();
                cBDAccountTitle retTitle;
                string strSql;
                strSql = "select * from LedgerTitles where cast(isnull(account,'0') as int) > 0 ";
                strSql += " and fund = '" + strFund + "' ";
                strSql += " and glaccounttype = '1' ";
                strSql += " order by account, [year]";
                rsLoad.OpenRecordset(strSql, "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    retTitle = new cBDAccountTitle();
                    FillLedgerTitle(ref retTitle, ref rsLoad);
                    retColl.AddItem(retTitle);
                    rsLoad.MoveNext();
                }
                rsLoad.DisposeOf();
                GetAssetLedgerTitlesByFund = retColl;
                return GetAssetLedgerTitlesByFund;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetAssetLedgerTitlesByFund;
        }

        public cGenericCollection GetLiabilityLedgerTitlesByFund(string strFund)
        {
            cGenericCollection GetLiabilityLedgerTitlesByFund = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cGenericCollection retColl = new cGenericCollection();
                cBDAccountTitle retTitle;
                string strSql;
                strSql = "select * from LedgerTitles where cast(isnull(account,'0') as int) > 0 ";
                strSql += " and fund = '" + strFund + "' ";
                strSql += " and glaccounttype = '2' ";
                strSql += " order by account, [year]";
                rsLoad.OpenRecordset(strSql, "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    retTitle = new cBDAccountTitle();
                    FillLedgerTitle(ref retTitle, ref rsLoad);
                    retColl.AddItem(retTitle);
                    rsLoad.MoveNext();
                }
                rsLoad.DisposeOf();
                GetLiabilityLedgerTitlesByFund = retColl;
                return GetLiabilityLedgerTitlesByFund;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetLiabilityLedgerTitlesByFund;
        }

        public cGenericCollection GetFundBalanceLedgerTitlesByFund(string strFund)
        {
            cGenericCollection GetFundBalanceLedgerTitlesByFund = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cGenericCollection retColl = new cGenericCollection();
                cBDAccountTitle retTitle;
                string strSql;
                strSql = "select * from LedgerTitles where cast(isnull(account,'0') as int) > 0 ";
                strSql += " and fund = '" + strFund + "' ";
                strSql += " and glaccounttype = '3' ";
                strSql += " order by account, [year]";
                rsLoad.OpenRecordset(strSql, "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    retTitle = new cBDAccountTitle();
                    FillLedgerTitle(ref retTitle, ref rsLoad);
                    retColl.AddItem(retTitle);
                    rsLoad.MoveNext();
                }
                rsLoad.DisposeOf();
                GetFundBalanceLedgerTitlesByFund = retColl;
                return GetFundBalanceLedgerTitlesByFund;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetFundBalanceLedgerTitlesByFund;
        }

        private void FillLedgerTitle(ref cBDAccountTitle aTitle, ref clsDRWrapper rs)
        {
            try
            {
                // On Error GoTo ErrorHandler
                if (!rs.EndOfFile())
                {
                    aTitle.CashFund = rs.Get_Fields_String("cashfund");
                    aTitle.Description = rs.Get_Fields_String("LongDescription");
                    // TODO: Check the table for the column [fund] and replace with corresponding Get_Field method
                    aTitle.Fund = rs.Get_Fields_String("fund");
                    aTitle.GLAccountType = rs.Get_Fields_String("glaccounttype");
                    aTitle.ShortDescription = rs.Get_Fields_String("ShortDescription");
                    // TODO: Check the table for the column [account] and replace with corresponding Get_Field method
                    aTitle.Account = rs.Get_Fields_String("account");
                    // TODO: Check the table for the column [year] and replace with corresponding Get_Field method
                    aTitle.Suffix = rs.Get_Fields_String("year");
                    aTitle.ID = rs.Get_Fields_Int32("ID");
                    aTitle.IsUpdated = false;
                }
                return;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        public cGenericCollection GetGLAccounts()
        {
            cGenericCollection GetGLAccounts = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cGenericCollection retColl = new cGenericCollection();
                cBDAccount gAccount;
                string strSql;
                strSql = "select * from AccountMaster where accounttype = 'G' order by account";
                rsLoad.OpenRecordset(strSql, "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    gAccount = new cBDAccount();
                    FillAccount(ref gAccount, ref rsLoad);
                    retColl.AddItem(gAccount);
                    rsLoad.MoveNext();
                }
                rsLoad.DisposeOf();
                GetGLAccounts = retColl;
                return GetGLAccounts;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetGLAccounts;
        }

        public cBDAccount GetAccountByAccount(string strAccount)
        {
            cBDAccount GetAccountByAccount = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cBDAccount Acct = null;
                rsLoad.OpenRecordset("Select * from accountmaster where account = '" + strAccount + "'", "Budgetary");
                if (!rsLoad.EndOfFile())
                {
                    Acct = new cBDAccount();
                    FillAccount(ref Acct, ref rsLoad);
                }
                rsLoad.DisposeOf();
                GetAccountByAccount = Acct;
                return GetAccountByAccount;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetAccountByAccount;
        }

        public cBDAccount GetFullAccountByAccount(string strAccount)
        {
            cBDAccount GetFullAccountByAccount = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cBDAccount Acct = null;
                rsLoad.OpenRecordset("Select * from accountmaster where account = '" + strAccount + "'", "Budgetary");
                if (!rsLoad.EndOfFile())
                {
                    Acct = new cBDAccount();
                    FillAccount(ref Acct, ref rsLoad);
                    cDescriptionItem descItem;
                    descItem = GetAccountDescriptionByAccount(strAccount);
                    Acct.ShortDescription = descItem.ShortDescription;
                    Acct.Description = descItem.Description;
                }
                rsLoad.DisposeOf();
                GetFullAccountByAccount = Acct;
                return GetFullAccountByAccount;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetFullAccountByAccount;
        }

        public cGenericCollection GetAccounts()
        {
            cGenericCollection GetAccounts = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cGenericCollection retColl = new cGenericCollection();
                cBDAccount Acct;
                rsLoad.OpenRecordset("select * from accountmaster order by Account", "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    Acct = new cBDAccount();
                    FillAccount(ref Acct, ref rsLoad);
                    retColl.AddItem(Acct);
                    rsLoad.MoveNext();
                }
                rsLoad.DisposeOf();
                GetAccounts = retColl;
                return GetAccounts;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetAccounts;
        }

        public Dictionary<object, object> GetAccountsDictionary(string strArchive = "")
        {
            Dictionary<object, object> GetAccountsDictionary = new Dictionary<object, object>();
            ClearErrors();
            cBDAccount Acct = null;
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                Dictionary<object, object> retDict = new Dictionary<object, object>();
                if (strArchive != "")
                {
                    rsLoad.GroupName = strArchive;
                }
                rsLoad.OpenRecordset("select * from accountmaster order by Account", "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    Acct = new cBDAccount();
                    FillAccount(ref Acct, ref rsLoad);
                    retDict.Add(Acct.Account, Acct);
                    rsLoad.MoveNext();
                }
                rsLoad.DisposeOf();
                GetAccountsDictionary = retDict;
                return GetAccountsDictionary;
            }
            catch (Exception ex)
            {
                if (!(Acct == null))
                {
                    SetError(Information.Err(ex).Number, ex.GetBaseException().Message + "\r\n" + "Current account: " + Acct.Account);
                }
                else
                {
                    SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                }
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetAccountsDictionary;
        }

        public Dictionary<object, object> GetFullAccountsDictionary()
        {
            Dictionary<object, object> GetFullAccountsDictionary = new Dictionary<object, object>();
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                Dictionary<object, object> retDict = new Dictionary<object, object>();
                cBDAccount Acct;
                cDescriptionItem descItem;
                rsLoad.OpenRecordset("select * from accountmaster order by Account", "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    ////////Application.DoEvents();
                    Acct = new cBDAccount();
                    FillAccount(ref Acct, ref rsLoad);
                    modNewAccountBox.Statics.strAccount = Acct.Account;
                    descItem = GetAccountDescriptionByAccount(modNewAccountBox.Statics.strAccount);
                    Acct.ShortDescription = descItem.ShortDescription;
                    Acct.Description = descItem.Description;
                    retDict.Add(Acct.Account, Acct);
                    rsLoad.MoveNext();
                }
                rsLoad.DisposeOf();
                GetFullAccountsDictionary = retDict;
                return GetFullAccountsDictionary;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetFullAccountsDictionary;
        }

        public cGenericCollection GetGLAccountsByFund(string strFund)
        {
            cGenericCollection GetGLAccountsByFund = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cGenericCollection retColl = new cGenericCollection();
                cBDAccount gAccount;
                string strSql;
                strSql = "select * from AccountMaster where accounttype = 'G' and firstaccountfield = '" + strFund + "' order by account";
                rsLoad.OpenRecordset(strSql, "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    gAccount = new cBDAccount();
                    FillAccount(ref gAccount, ref rsLoad);
                    retColl.AddItem(gAccount);
                    rsLoad.MoveNext();
                }
                rsLoad.DisposeOf();
                GetGLAccountsByFund = retColl;
                return GetGLAccountsByFund;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetGLAccountsByFund;
        }

        private void SetAssetsRange(ref string strLow, ref string strHigh)
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                string strSql;
                strSql = "select * from LedgerRanges where recordname = 'A'";
                rsLoad.OpenRecordset(strSql, "Budgetary");
                if (!rsLoad.EndOfFile())
                {
                    // TODO: Check the table for the column [Low] and replace with corresponding Get_Field method
                    strLow = FCConvert.ToString(rsLoad.Get_Fields("Low"));
                    // TODO: Check the table for the column [High] and replace with corresponding Get_Field method
                    strHigh = FCConvert.ToString(rsLoad.Get_Fields("High"));
                }
                else
                {
                    strLow = "";
                    strHigh = "";
                }
                return;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private void SetLiabilitiesRange(ref string strLow, ref string strHigh)
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                string strSql;
                strSql = "select * from LedgerRanges where recordname = 'L'";
                rsLoad.OpenRecordset(strSql, "Budgetary");
                if (!rsLoad.EndOfFile())
                {
                    // TODO: Check the table for the column [Low] and replace with corresponding Get_Field method
                    strLow = FCConvert.ToString(rsLoad.Get_Fields("Low"));
                    // TODO: Check the table for the column [High] and replace with corresponding Get_Field method
                    strHigh = FCConvert.ToString(rsLoad.Get_Fields("High"));
                }
                else
                {
                    strLow = "";
                    strHigh = "";
                }
                return;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private void SetFundBalancesRange(ref string strLow, ref string strHigh)
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                string strSql;
                strSql = "select * from LedgerRanges where recordname = 'F'";
                rsLoad.OpenRecordset(strSql, "Budgetary");
                if (!rsLoad.EndOfFile())
                {
                    // TODO: Check the table for the column [Low] and replace with corresponding Get_Field method
                    strLow = FCConvert.ToString(rsLoad.Get_Fields("Low"));
                    // TODO: Check the table for the column [High] and replace with corresponding Get_Field method
                    strHigh = FCConvert.ToString(rsLoad.Get_Fields("High"));
                }
                else
                {
                    strLow = "";
                    strHigh = "";
                }
                return;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        public cGenericCollection GetGLAssetsByFund(string strFund)
        {
            cGenericCollection GetGLAssetsByFund = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cGenericCollection retColl = new cGenericCollection();
                cBDAccount gAccount;
                string strSql;
                // Dim strStartAcct As String
                // Dim strEndAcct As String
                // Call SetAssetsRange(strStartAcct, strEndAcct)
                strSql = "select * from AccountMaster where accounttype = 'G' and firstaccountfield = '" + strFund + "' ";
                strSql += " and glaccounttype = '1'";
                strSql += " order by account";
                rsLoad.OpenRecordset(strSql, "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    gAccount = new cBDAccount();
                    FillAccount(ref gAccount, ref rsLoad);
                    retColl.AddItem(gAccount);
                    rsLoad.MoveNext();
                }
                rsLoad.DisposeOf();
                GetGLAssetsByFund = retColl;
                return GetGLAssetsByFund;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetGLAssetsByFund;
        }

        public cGenericCollection GetGLLiabilitiesByFund(string strFund)
        {
            cGenericCollection GetGLLiabilitiesByFund = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cGenericCollection retColl = new cGenericCollection();
                cBDAccount gAccount;
                string strSql;
                // Dim strStartAcct As String
                // Dim strEndAcct As String
                // Call SetLiabilitiesRange(strStartAcct, strEndAcct)
                strSql = "select * from AccountMaster where accounttype = 'G' and firstaccountfield = '" + strFund + "' ";
                strSql += " and GLAccounttype = '2' ";
                strSql += " order by account";
                rsLoad.OpenRecordset(strSql, "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    gAccount = new cBDAccount();
                    FillAccount(ref gAccount, ref rsLoad);
                    retColl.AddItem(gAccount);
                    rsLoad.MoveNext();
                }
                rsLoad.DisposeOf();
                GetGLLiabilitiesByFund = retColl;
                return GetGLLiabilitiesByFund;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetGLLiabilitiesByFund;
        }

        public cGenericCollection GetGLFundBalancesByFund(string strFund)
        {
            cGenericCollection GetGLFundBalancesByFund = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cGenericCollection retColl = new cGenericCollection();
                cBDAccount gAccount;
                string strSql;
                // Dim strStartAcct As String
                // Dim strEndAcct As String
                // Call SetFundBalancesRange(strStartAcct, strEndAcct)
                strSql = "select * from AccountMaster where accounttype = 'G' and firstaccountfield = '" + strFund + "' ";
                strSql += " and GLAccounttype = '3' ";
                strSql += " order by account";
                rsLoad.OpenRecordset(strSql, "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    gAccount = new cBDAccount();
                    FillAccount(ref gAccount, ref rsLoad);
                    retColl.AddItem(gAccount);
                    rsLoad.MoveNext();
                }
                rsLoad.DisposeOf();
                GetGLFundBalancesByFund = retColl;
                return GetGLFundBalancesByFund;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetGLFundBalancesByFund;
        }

        private void FillAccount(ref cBDAccount theAccount, ref clsDRWrapper rs)
        {
            try
            {
                // On Error GoTo ErrorHandler
                if (!(theAccount == null))
                {
                    if (!(rs == null))
                    {
                        // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                        theAccount.Account = rs.Get_Fields_String("Account");
                        theAccount.AccountType = rs.Get_Fields_String("accounttype");
                        // TODO: Check the table for the column [CurrentBudget] and replace with corresponding Get_Field method
                        theAccount.CurrentBudget = Conversion.Val(rs.Get_Fields("CurrentBudget"));
                        theAccount.FirstAccountField = rs.Get_Fields_String("FirstAccountField");
                        theAccount.SecondAccountField = rs.Get_Fields_String("SecondAccountField");
                        theAccount.ThirdAccountField = rs.Get_Fields_String("ThirdAccountField");
                        theAccount.FourthAccountField = rs.Get_Fields_String("FourthAccountField");
                        theAccount.FifthAccountField = rs.Get_Fields_String("FifthAccountField");
                        theAccount.GLAccountType = rs.Get_Fields_String("glaccounttype");
                        if (!modBudgetaryAccounting.Statics.gboolPUCChartOfAccounts && Strings.LCase(theAccount.AccountType) == "g")
                        {
                            theAccount.GLAccountType = alfRangeSetup.GetALFByAccountSegment(theAccount.SecondAccountField);
                        }
                        theAccount.ID = rs.Get_Fields_Int32("ID");
                        theAccount.IsValidAccount = rs.Get_Fields_Boolean("Valid");
                        theAccount.IsUpdated = false;
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        public cGenericCollection GetDepartmentTitles()
        {
            cGenericCollection GetDepartmentTitles = null;
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                cGenericCollection retColl = new cGenericCollection();
                clsDRWrapper rs = new clsDRWrapper();
                cDeptDivTitle dd;
                rs.OpenRecordset("select * from deptdivtitles where cast(isnull(division,'0') as int) = 0 order by department", "Budgetary");
                while (!rs.EndOfFile())
                {
                    dd = new cDeptDivTitle();
                    FillDeptDivTitle(ref dd, ref rs);
                    retColl.AddItem(dd);
                    rs.MoveNext();
                }
                GetDepartmentTitles = retColl;
                return GetDepartmentTitles;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetDepartmentTitles;
        }

        public cGenericCollection GetExpenseObjectTitles()
        {
            cGenericCollection GetExpenseObjectTitles = null;
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                cGenericCollection retColl = new cGenericCollection();
                clsDRWrapper rs = new clsDRWrapper();
                cExpenseObjectTitle eo;
                rs.OpenRecordset("select * from expobjtitles order by expense, [object]", "Budgetary");
                while (!rs.EndOfFile())
                {
                    eo = new cExpenseObjectTitle();
                    FillExpenseObject(ref eo, ref rs);
                    retColl.AddItem(eo);
                    rs.MoveNext();
                }
                GetExpenseObjectTitles = retColl;
                return GetExpenseObjectTitles;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetExpenseObjectTitles;
        }

        private void FillExpenseObject(ref cExpenseObjectTitle eo, ref clsDRWrapper rs)
        {
            eo.BreakdownCode = FCConvert.ToInt32(Conversion.Val(rs.Get_Fields_Int32("BreakdownCode")));
            eo.Expense = rs.Get_Fields_String("Expense");
            eo.ExpenseObject = rs.Get_Fields_String("Object");
            eo.ID = rs.Get_Fields_Int32("ID");
            eo.LongDescription = rs.Get_Fields_String("LongDescription");
            eo.ShortDescription = rs.Get_Fields_String("ShortDescription");
            // TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
            eo.Tax = rs.Get_Fields_String("Tax");
            eo.IsUpdated = false;
        }

        private void FillRevenueTitleObject(ref cRevTitle rTitle, ref clsDRWrapper rs)
        {
            rTitle.CloseoutAccount = rs.Get_Fields_String("CloseoutAccount");
            rTitle.Department = rs.Get_Fields_String("Department");
            rTitle.Division = rs.Get_Fields_String("Division");
            rTitle.ID = rs.Get_Fields_Int32("ID");
            rTitle.LongDescription = rs.Get_Fields_String("LongDescription");
            rTitle.Revenue = rs.Get_Fields_String("Revenue");
            rTitle.ShortDescription = rs.Get_Fields_String("ShortDescription");
            rTitle.IsUpdated = false;
        }

        public cGenericCollection GetRevenueTitles()
        {
            cGenericCollection GetRevenueTitles = null;
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                cGenericCollection collTitles = new cGenericCollection();
                clsDRWrapper rs = new clsDRWrapper();
                cRevTitle rTitle;
                rs.OpenRecordset("select * from RevTitles order by department,division,revenue", "Budgetary");
                while (!rs.EndOfFile())
                {
                    rTitle = new cRevTitle();
                    FillRevenueTitleObject(ref rTitle, ref rs);
                    collTitles.AddItem(rTitle);
                    rs.MoveNext();
                }
                rs.DisposeOf();
                GetRevenueTitles = collTitles;
                return GetRevenueTitles;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetRevenueTitles;
        }

        public cGenericCollection GetRevenueAccountTitlesByFund(string strFund)
        {
            cGenericCollection GetRevenueAccountTitlesByFund = null;
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                cGenericCollection collTitles = new cGenericCollection();
                bool boolUseDivision = false;
                cGenericCollection collDepartmentTitles;
                cGenericCollection collDDs;
                cDeptDivTitle dd;
                clsDRWrapper rs = new clsDRWrapper();
                string strShortDescription;
                string strLongDescription;
                string strLastDepartment = "";
                cDeptDivTitle currDept = null;
                cDeptDivTitle CurrDivision = null;
                cRevTitle currRevTitle;
                string strSql = "";
                if (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Rev, 3, 2)) > 0)
                {
                    boolUseDivision = true;
                }
                if (boolUseDivision)
                {
                    collDepartmentTitles = GetDepartmentTitles();
                }
                collDDs = GetDeptDivs();
                if (HadError)
                {
                    return GetRevenueAccountTitlesByFund;
                }
                collDDs.MoveFirst();
                strShortDescription = "";
                strLongDescription = "";
                while (collDDs.IsCurrent())
                {
                    dd = (cDeptDivTitle)collDDs.GetCurrentItem();
                    if (Conversion.Val(strFund) == Conversion.Val(dd.Fund))
                    {
                        if (Conversion.Val(currDept.Division) == 0)
                        {
                            currDept = dd;
                        }
                        else
                        {
                            CurrDivision = dd;
                        }
                        if (!boolUseDivision || Conversion.Val(dd.Division) > 0)
                        {
                            if (!boolUseDivision)
                            {
                                strSql = "Select * from revtitles where department = '" + currDept.Department + "' order by revenue";
                            }
                            else if (Conversion.Val(dd.Division) > 0)
                            {
                                strSql = "select * from revtitles where department = '" + dd.Department + "' and Division = '" + dd.Division + "' order by revenue";
                            }
                            rs.OpenRecordset(strSql, "Budgetary");
                            while (!rs.EndOfFile())
                            {
                                currRevTitle = new cRevTitle();
                                FillRevenueTitleObject(ref currRevTitle, ref rs);
                                strShortDescription = currDept.ShortDescription;
                                strLongDescription = currDept.LongDescription;
                                if (boolUseDivision)
                                {
                                    strShortDescription += " / " + CurrDivision.ShortDescription;
                                    strLongDescription += " / " + CurrDivision.LongDescription;
                                }
                                strShortDescription += " - ";
                                strLongDescription += " - ";
                                strShortDescription += currRevTitle.ShortDescription;
                                strLongDescription += currRevTitle.LongDescription;
                                currRevTitle.ShortDescription = strShortDescription;
                                currRevTitle.LongDescription = strLongDescription;
                                collTitles.AddItem(currRevTitle);
                                rs.MoveNext();
                            }
                        }
                    }
                    collDDs.MoveNext();
                }
                GetRevenueAccountTitlesByFund = collTitles;
                return GetRevenueAccountTitlesByFund;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetRevenueAccountTitlesByFund;
        }
        // Public Function GetExpenseAccountsByDepartment(ByVal strDept As String) As cGenericCollection
        // On Error GoTo ErrorHandler
        // Dim strSQL As String
        // ClearErrors
        // strSQL = "Select * from accountmaster where AccountType = 'E' and firstaccountfield = '" & strDept & "' "
        // Exit Function
        // ErrorHandler:
        // Call SetError(Err.Number, Err.Description)
        // End Function
        public cGenericCollection GetExpenseAccountsByFund(string strFund)
        {
            cGenericCollection GetExpenseAccountsByFund = null;
            // first get all departments in this fund
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                cGenericCollection collAccounts = new cGenericCollection();
                cGenericCollection collDDs;
                cBDAccount Acct;
                cDeptDivTitle dd;
                clsDRWrapper rs = new clsDRWrapper();
                string strSql = "";
                bool boolUseDivision = false;
                string strExpense = "";
                string strObject = "";
                cGenericCollection collDepartmentTitles = null;
                cGenericCollection collExpObs;
                string strLastDepartment;
                cDeptDivTitle deptTitle;
                cDeptDivTitle currDept = null;
                string strShortDescription = "";
                string strLongDescription = "";
                cExpenseObjectTitle currEO = null;
                strLastDepartment = "";
                if (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) > 0)
                {
                    boolUseDivision = true;
                }
                if (boolUseDivision)
                {
                    collDepartmentTitles = GetDepartmentTitles();
                }
                collExpObs = GetExpenseObjectTitles();
                collDDs = GetDeptDivsByFund(strFund, !boolUseDivision);
                if (HadError)
                {
                    return GetExpenseAccountsByFund;
                }
                collDDs.MoveFirst();
                while (collDDs.IsCurrent())
                {
                    dd = (cDeptDivTitle)collDDs.GetCurrentItem();
                    if (!boolUseDivision || Conversion.Val(dd.Division) > 0)
                    {
                        strSql = "Select * from accountmaster where AccountType = 'E' and firstaccountfield = '" + dd.Department + "' ";
                        // If Val(dd.Division) > 0 Then
                        if (boolUseDivision)
                        {
                            strSql += " and secondaccountfield = '" + dd.Division + "' ";
                        }
                        strSql += " order by account";
                        rs.OpenRecordset(strSql, "Budgetary");
                        while (!rs.EndOfFile())
                        {
                            Acct = new cBDAccount();
                            FillAccount(ref Acct, ref rs);
                            strShortDescription = "";
                            strLongDescription = "";
                            if (!boolUseDivision)
                            {
                                strShortDescription = dd.ShortDescription + " - ";
                                strLongDescription = dd.LongDescription + " - ";
                            }
                            else
                            {
                                if (strLastDepartment != dd.Department)
                                {
                                    collDepartmentTitles.MoveFirst();
                                    while (collDepartmentTitles.IsCurrent())
                                    {
                                        currDept = (cDeptDivTitle)collDepartmentTitles.GetCurrentItem();
                                        if (currDept.Department == dd.Department)
                                        {
                                            break;
                                        }
                                        currDept = null;
                                        collDepartmentTitles.MoveNext();
                                    }
                                    strLastDepartment = dd.Department;
                                }
                                if (!(currDept == null))
                                {
                                    strShortDescription = currDept.ShortDescription + " / " + dd.ShortDescription + " - ";
                                    strLongDescription = currDept.LongDescription + " / " + dd.LongDescription + " - ";
                                }
                                else
                                {
                                    strShortDescription = dd.ShortDescription + " - ";
                                    strLongDescription = dd.LongDescription + " - ";
                                }
                            }
                            strExpense = modBudgetaryAccounting.GetExpense(Acct.Account);
                            strObject = modBudgetaryAccounting.GetObject(Acct.Account);
                            collExpObs.MoveFirst();
                            while (collExpObs.IsCurrent())
                            {
                                currEO = (cExpenseObjectTitle)collExpObs.GetCurrentItem();
                                if (currEO.Expense == strExpense)
                                {
                                    if (Conversion.Val(currEO.ExpenseObject) == 0 && currEO.ExpenseObject != strObject)
                                    {
                                        strShortDescription += currEO.ShortDescription + " / ";
                                        strLongDescription += currEO.LongDescription + " / ";
                                    }
                                    if (currEO.ExpenseObject == strObject)
                                    {
                                        break;
                                    }
                                }
                                currEO = null;
                                collExpObs.MoveNext();
                            }
                            if (!(currEO == null))
                            {
                                strShortDescription += currEO.ShortDescription;
                                strLongDescription += currEO.LongDescription;
                            }
                            Acct.ShortDescription = strShortDescription;
                            Acct.Description = strLongDescription;
                            collAccounts.AddItem(Acct);
                            rs.MoveNext();
                        }
                        rs.DisposeOf();
                    }
                    collDDs.MoveNext();
                }
                GetExpenseAccountsByFund = collAccounts;
                return GetExpenseAccountsByFund;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetExpenseAccountsByFund;
        }

        public cGenericCollection GetDeptDivsByFund(string strFund, bool boolDepartmentOnly)
        {
            cGenericCollection GetDeptDivsByFund = null;
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rs = new clsDRWrapper();
                cDeptDivTitle dd;
                cGenericCollection collDDs = new cGenericCollection();
                rs.OpenRecordset("select * from deptdivtitles where fund = '" + strFund + "' order by department , division", "Budgetary");
                while (!rs.EndOfFile())
                {
                    if (!boolDepartmentOnly || Conversion.Val(rs.Get_Fields_String("division")) == 0)
                    {
                        dd = new cDeptDivTitle();
                        FillDeptDivTitle(ref dd, ref rs);
                        collDDs.AddItem(dd);
                    }
                    rs.MoveNext();
                }
                rs.DisposeOf();
                GetDeptDivsByFund = collDDs;
                return GetDeptDivsByFund;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetDeptDivsByFund;
        }

        public cGenericCollection GetDeptDivs()
        {
            cGenericCollection GetDeptDivs = null;
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rs = new clsDRWrapper();
                cDeptDivTitle dd;
                cGenericCollection collDDs = new cGenericCollection();
                rs.OpenRecordset("select * from deptdivtitles order by department , division", "Budgetary");
                while (!rs.EndOfFile())
                {
                    dd = new cDeptDivTitle();
                    FillDeptDivTitle(ref dd, ref rs);
                    collDDs.AddItem(dd);
                    rs.MoveNext();
                }
                GetDeptDivs = collDDs;
                return GetDeptDivs;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetDeptDivs;
        }

        public cGenericCollection GetDeptDivsByRange(string strStartDept, string strEndDept, bool boolDepartmentOnly)
        {
            cGenericCollection GetDeptDivsByRange = null;
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rs = new clsDRWrapper();
                cDeptDivTitle dd;
                cGenericCollection collDDs = new cGenericCollection();
                rs.OpenRecordset("select * from deptdivtitles where department between '" + strStartDept + "' and '" + strEndDept + "' order by department, division", "Budgetary");
                while (!rs.EndOfFile())
                {
                    if (!boolDepartmentOnly || Conversion.Val(rs.Get_Fields_String("division")) == 0)
                    {
                        dd = new cDeptDivTitle();
                        FillDeptDivTitle(ref dd, ref rs);
                        collDDs.AddItem(dd);
                    }
                    rs.MoveNext();
                }
                GetDeptDivsByRange = collDDs;
                return GetDeptDivsByRange;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetDeptDivsByRange;
        }

        private void FillDeptDivTitle(ref cDeptDivTitle dd, ref clsDRWrapper rs)
        {
            dd.BreakdownCode = rs.Get_Fields_Int32("BreakdownCode");
            dd.CloseoutAccount = rs.Get_Fields_String("CloseoutAccount");
            dd.Department = rs.Get_Fields_String("Department");
            dd.Division = rs.Get_Fields_String("division");
            // TODO: Check the table for the column [fund] and replace with corresponding Get_Field method
            dd.Fund = rs.Get_Fields_String("fund");
            dd.ID = rs.Get_Fields_Int32("id");
            dd.LongDescription = rs.Get_Fields_String("LongDescription");
            dd.ShortDescription = rs.Get_Fields_String("ShortDescription");
            dd.IsUpdated = false;
        }

        public cALFRange GetALFRanges()
        {
            cALFRange GetALFRanges = null;
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                clsDRWrapper rs = new clsDRWrapper();
                cALFRange alfRange = new cALFRange();
                rs.OpenRecordset("select * from ledgerranges", "Budgetary");
                while (!rs.EndOfFile())
                {
                    var recName = Strings.LCase(FCConvert.ToString(rs.Get_Fields_String("recordname")));

                    switch (recName)
                    {
                        case "a":
                            // TODO: Check the table for the column [Low] and replace with corresponding Get_Field method
                            alfRange.AssetLowRange = FCConvert.ToString(rs.Get_Fields("Low"));
                            // TODO: Check the table for the column [high] and replace with corresponding Get_Field method
                            alfRange.AssetHighRange = FCConvert.ToString(rs.Get_Fields("high"));

                            break;
                        case "l":
                            // TODO: Check the table for the column [Low] and replace with corresponding Get_Field method
                            alfRange.LiabilityLowRange = FCConvert.ToString(rs.Get_Fields("Low"));
                            // TODO: Check the table for the column [high] and replace with corresponding Get_Field method
                            alfRange.LiabilityHighRange = FCConvert.ToString(rs.Get_Fields("high"));

                            break;
                        case "f":
                            // TODO: Check the table for the column [Low] and replace with corresponding Get_Field method
                            alfRange.FundBalanceLowRange = FCConvert.ToString(rs.Get_Fields("Low"));
                            // TODO: Check the table for the column [high] and replace with corresponding Get_Field method
                            alfRange.FundBalanceHighRange = FCConvert.ToString(rs.Get_Fields("high"));

                            break;
                    }
                    rs.MoveNext();
                }
                rs.DisposeOf();
                GetALFRanges = alfRange;
                return GetALFRanges;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetALFRanges;
        }

        public cBDAccountController() : base()
        {
            alfRangeSetup = GetALFRanges();
            //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
            if (alfRangeSetup_AutoInitialized == null)
            {
                alfRangeSetup = new cALFRange();
            }
        }

        public cDescriptionItem GetAccountDescriptionByAccount(string strAccount)
        {
            cDescriptionItem GetAccountDescriptionByAccount = null;
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                clsDRWrapper rsLoad = new clsDRWrapper();
                cDescriptionItem descItem = new cDescriptionItem();
                cDescriptionItem tempDesc;
                string strDescription = "";
                string strDept = "";
                string strDiv = "";
                bool boolUseDivision = false;
                string strExpense = "";
                string strObject = "";
                string strRevenue = "";
                bool boolRevUseDivision;
                string strFund = "";
                string strAccountSegment = "";
                string strSuffix = "";
                boolRevUseDivision = !modAccountTitle.Statics.RevDivFlag;
                rsLoad.OpenRecordset("Select account,accounttype,firstaccountfield,secondaccountfield,thirdaccountfield,fourthaccountfield from accountmaster where account = '" + strAccount + "'", "Budgetary");
                if (!rsLoad.EndOfFile())
                {
                    var accountType = Strings.LCase(FCConvert.ToString(rsLoad.Get_Fields_String("AccountType")));

                    switch (accountType)
                    {
                        case "e":
                        {
                            if (Conversion.Val(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2)) > 0)
                            {
                                boolUseDivision = true;
                            }
                            strExpense = modBudgetaryAccounting.GetExpense(strAccount);
                            strObject = modBudgetaryAccounting.GetObject(strAccount);
                            strDept = FCConvert.ToString(rsLoad.Get_Fields_String("FirstAccountField"));
                            strDiv = boolUseDivision == true ? FCConvert.ToString(rsLoad.Get_Fields_String("SecondAccountField")) : "";
                            tempDesc = GetDeptDivDescription(strDept, strDiv);
                            descItem.ShortDescription = tempDesc.ShortDescription;
                            descItem.Description = tempDesc.Description;
                            tempDesc = GetExpenseObjectDescription(strExpense, strObject);
                            descItem.ShortDescription = descItem.ShortDescription + " - " + tempDesc.ShortDescription;
                            descItem.Description = descItem.Description + " - " + tempDesc.Description;

                            break;
                        }
                        case "r":
                        {
                            strDept = FCConvert.ToString(rsLoad.Get_Fields_String("firstaccountfield"));
                            strDiv = boolRevUseDivision ? FCConvert.ToString(rsLoad.Get_Fields_String("SecondAccountField")) : "";
                            tempDesc = GetDeptDivDescription(strDept, strDiv);
                            descItem.ShortDescription = tempDesc.ShortDescription;
                            descItem.Description = tempDesc.Description;
                            strRevenue = modBudgetaryAccounting.GetRevenue(strAccount);
                            tempDesc = GetRevenueDescription(strDept, strDiv, strRevenue);
                            descItem.ShortDescription = descItem.ShortDescription + " - " + tempDesc.ShortDescription;
                            descItem.Description = descItem.Description + " - " + tempDesc.Description;

                            break;
                        }
                        case "g":
                            strFund = FCConvert.ToString(rsLoad.Get_Fields_String("FirstAccountField"));
                            strAccountSegment = FCConvert.ToString(rsLoad.Get_Fields_String("SecondAccountField"));
                            strSuffix = FCConvert.ToString(rsLoad.Get_Fields_String("ThirdAccountField"));
                            tempDesc = GetLedgerDescription(strFund, strAccountSegment, strSuffix);
                            descItem.ShortDescription = tempDesc.ShortDescription;
                            descItem.Description = tempDesc.Description;

                            break;
                    }
                }
                GetAccountDescriptionByAccount = descItem;
                return GetAccountDescriptionByAccount;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetAccountDescriptionByAccount;
        }

        private cDescriptionItem GetLedgerDescription(string strFund, string strAccountSegment, string strSuffix)
        {
            cDescriptionItem GetLedgerDescription = null;
            cDescriptionItem descItem = new cDescriptionItem();
            if (strFund != "")
            {
                if (dictFunds.Count < 1)
                {
                    LoadFundsDictionary();
                }
                if (dictFunds.ContainsKey(strFund))
                {
                    // vbPorter upgrade warning: theFund As cFund	OnWrite(object)
                    cFund theFund;
                    theFund = (cFund)dictFunds[strFund];
                    string strTemp = "";
                    strTemp = strAccountSegment + "-" + strSuffix;
                    if (theFund.AccountTitles.ContainsKey(strTemp))
                    {
                        // vbPorter upgrade warning: acctTitle As cBDAccountTitle	OnWrite(object)
                        cBDAccountTitle acctTitle;
                        acctTitle = (cBDAccountTitle)theFund.AccountTitles[strTemp];
                        descItem.ShortDescription = acctTitle.ShortDescription;
                        descItem.Description = acctTitle.Description;
                    }
                }
            }
            GetLedgerDescription = descItem;
            return GetLedgerDescription;
        }

        private void LoadRevenuesDictionary()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            cRevTitle revItem;
            string strKey = "";
            dictRevenues.Clear();
            rsLoad.OpenRecordset("Select * from revtitles order by department,[division],revenue", "Budgetary");
            while (!rsLoad.EndOfFile())
            {
                ////////Application.DoEvents();
                revItem = new cRevTitle();
                FillRevenueTitleObject(ref revItem, ref rsLoad);
                strKey = revItem.Department + "-";
                if (!modAccountTitle.Statics.RevDivFlag)
                {
                    strKey += revItem.Division + "-";
                }
                strKey += revItem.Revenue;
                dictRevenues.Add(strKey, revItem);
                rsLoad.MoveNext();
            }
            rsLoad.DisposeOf();
        }

        private void LoadDepartmentDictionary()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            string strLastDepartment = "";
            cBDDepartment Dept;
            dictDepartments.Clear();
            rsLoad.OpenRecordset("select * from deptdivtitles where convert(int,isnull(division,'0')) = 0 order by department", "Budgetary");
            while (!rsLoad.EndOfFile())
            {
                ////////Application.DoEvents();
                Dept = new cBDDepartment();
                FillDepartment(ref Dept, ref rsLoad);
                LoadDivisions(ref Dept);
                dictDepartments.Add(Dept.Department, Dept);
                rsLoad.MoveNext();
            }
            rsLoad.DisposeOf();
        }

        private void LoadExpenseDictionary()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            cBDExpense expen;
            dictExpenses.Clear();
            rsLoad.OpenRecordset("Select * from expobjtitles where convert(int,isnull([object],'0')) = 0 order by expense", "Budgetary");
            while (!rsLoad.EndOfFile())
            {
                ////////Application.DoEvents();
                expen = new cBDExpense();
                FillExpense(ref expen, ref rsLoad);
                LoadExpObjects(ref expen);
                dictExpenses.Add(expen.Expense, expen);
                rsLoad.MoveNext();
            }
            rsLoad.DisposeOf();
        }

        private void FillExpense(ref cBDExpense expen, ref clsDRWrapper rsLoad)
        {
            if (expen != null && rsLoad != null)
            {
                expen.ID = rsLoad.Get_Fields_Int32("ID");
                expen.BreakdownCode = FCConvert.ToInt16(Conversion.Val(rsLoad.Get_Fields_Int32("BreakdownCode")));
                expen.Description = rsLoad.Get_Fields_String("LongDescription");
                expen.Expense = rsLoad.Get_Fields_String("Expense");
                expen.ShortDescription = rsLoad.Get_Fields_String("ShortDescription");
            }
        }

        private void LoadExpObjects(ref cBDExpense expen)
        {
            if (!(expen == null))
            {
                clsDRWrapper rsLoad = new clsDRWrapper();
                cBDExpenseObject expOb;
                expen.ExpenseObjects.Clear();
                rsLoad.OpenRecordset("Select * from expobjtitles where expense = '" + expen.Expense + "' and convert(int,isnull([object],'0')) > 0 order by [object]", "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    ////////Application.DoEvents();
                    expOb = new cBDExpenseObject();
                    FillExpObject(ref expOb, ref rsLoad);
                    expen.ExpenseObjects.Add(expOb.ExpObject, expOb);
                    rsLoad.MoveNext();
                }
                rsLoad.DisposeOf();
            }
        }

        private void FillExpObject(ref cBDExpenseObject expOb, ref clsDRWrapper rsLoad)
        {
            if (expOb != null && rsLoad != null)
            {
                expOb.Description = rsLoad.Get_Fields_String("LongDescription");
                expOb.Expense = rsLoad.Get_Fields_String("Expense");
                expOb.ExpObject = rsLoad.Get_Fields_String("Object");
                expOb.ID = rsLoad.Get_Fields_Int32("ID");
                expOb.ShortDescription = rsLoad.Get_Fields_String("ShortDescription");
            }
        }

        private void LoadDivisions(ref cBDDepartment Dept)
        {
            if (Dept != null)
            {
                clsDRWrapper rsLoad = new clsDRWrapper();
                cBDDivision divOb;
                Dept.Divisions.Clear();
                rsLoad.OpenRecordset("select * from deptdivtitles where department = '" + Dept.Department + "' and convert(int,isnull([division],'0') )> 0 order by [division]", "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    ////////Application.DoEvents();
                    divOb = new cBDDivision();
                    FillDivision(ref divOb, ref rsLoad);
                    Dept.Divisions.Add(divOb.Division, divOb);
                    rsLoad.MoveNext();
                }
                rsLoad.DisposeOf();
            }
        }

        private void FillDivision(ref cBDDivision divOb, ref clsDRWrapper rsLoad)
        {
            if (divOb != null && rsLoad != null)
            {
                divOb.ID = rsLoad.Get_Fields_Int32("ID");
                divOb.Department = rsLoad.Get_Fields_String("Department");
                divOb.Division = rsLoad.Get_Fields_String("division");
                divOb.Description = rsLoad.Get_Fields_String("LongDescription");
                // TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
                divOb.Fund = rsLoad.Get_Fields_String("Fund");
                divOb.ShortDescription = rsLoad.Get_Fields_String("ShortDescription");
            }
        }

        private void FillDepartment(ref cBDDepartment Dept, ref clsDRWrapper rsLoad)
        {
            if (Dept != null && rsLoad != null)
            {
                Dept.ID = rsLoad.Get_Fields_Int32("ID");
                Dept.Description = rsLoad.Get_Fields_String("LongDescription");
                Dept.ShortDescription = rsLoad.Get_Fields_String("ShortDescription");
                // TODO: Check the table for the column [FUnd] and replace with corresponding Get_Field method
                Dept.Fund = rsLoad.Get_Fields_String("FUnd");
                Dept.Department = rsLoad.Get_Fields_String("Department");
            }
        }

        private cDescriptionItem GetRevenueDescription(string strDept, string strDiv, string strRevenue)
        {
            cDescriptionItem GetRevenueDescription = null;
            cDescriptionItem descItem = new cDescriptionItem();
            string strShort = "";
            string strLong = "";
            // vbPorter upgrade warning: revTitle As cRevTitle	OnWrite(object)
            cRevTitle revTitle;
            string strTemp = "";
            if (strDept != "")
            {
                if (strRevenue != "")
                {
                    if (dictRevenues.Count < 1)
                    {
                        LoadRevenuesDictionary();
                    }
                    if (strDiv != "")
                    {
                        strTemp = strDept + "-" + strDiv + "-" + strRevenue;
                    }
                    else
                    {
                        strTemp = strDept + "-" + strRevenue;
                    }
                    if (dictRevenues.ContainsKey(strTemp))
                    {
                        revTitle = (cRevTitle)dictRevenues[strTemp];
                        descItem.ShortDescription = revTitle.ShortDescription;
                        descItem.Description = revTitle.LongDescription;
                    }
                }
            }
            GetRevenueDescription = descItem;
            return GetRevenueDescription;
        }

        private cDescriptionItem GetDeptDivDescription(string strDept, string strDiv)
        {
            cDescriptionItem GetDeptDivDescription = null;
            cDescriptionItem descItem = new cDescriptionItem();
            string strShort = "";
            string strLong = "";
            // vbPorter upgrade warning: Dept As cBDDepartment	OnWrite(object)
            cBDDepartment Dept;
            // vbPorter upgrade warning: divOb As cBDDivision	OnWrite(object)
            cBDDivision divOb;
            if (strDept != "")
            {
                if (dictDepartments.Count < 1)
                {
                    LoadDepartmentDictionary();
                }
                if (dictDepartments.ContainsKey(strDept))
                {
                    Dept = (cBDDepartment)dictDepartments[strDept];
                    strShort = Dept.ShortDescription;
                    strLong = Dept.Description;
                    if (Dept.Divisions.ContainsKey(strDiv))
                    {
                        divOb = (cBDDivision)Dept.Divisions[strDiv];
                        strShort += " / " + divOb.ShortDescription;
                        strLong += " / " + divOb.Description;
                    }
                }
            }
            descItem.ShortDescription = strShort;
            descItem.Description = strLong;
            GetDeptDivDescription = descItem;
            return GetDeptDivDescription;
        }

        private cDescriptionItem GetExpenseObjectDescription(string strExpense, string strObject)
        {
            cDescriptionItem GetExpenseObjectDescription = null;
            cDescriptionItem descItem = new cDescriptionItem();
            string strShort = "";
            string strLong = "";
            // vbPorter upgrade warning: ExpObject As cBDExpenseObject	OnWrite(object)
            cBDExpenseObject ExpObject;
            // vbPorter upgrade warning: Expense As cBDExpense	OnWrite(object)
            cBDExpense Expense;
            if (strExpense != "")
            {
                if (dictExpenses.Count < 1)
                {
                    LoadExpenseDictionary();
                }
                if (dictExpenses.ContainsKey(strExpense))
                {
                    Expense = (cBDExpense)dictExpenses[strExpense];
                    strShort = Expense.ShortDescription;
                    strLong = Expense.Description;
                    if (Expense.ExpenseObjects.ContainsKey(strObject))
                    {
                        ExpObject = (cBDExpenseObject)Expense.ExpenseObjects[strObject];
                        strShort += " / " + ExpObject.ShortDescription;
                        strLong += " / " + ExpObject.Description;
                    }
                }
            }
            descItem.ShortDescription = strShort;
            descItem.Description = strLong;
            GetExpenseObjectDescription = descItem;
            return GetExpenseObjectDescription;
        }

        public cStandardAccounts GetStandardAccounts()
        {
            cStandardAccounts GetStandardAccounts = null;
            clsDRWrapper rsLoad = new clsDRWrapper();
            cStandardAccounts sa = new cStandardAccounts();
            rsLoad.OpenRecordset("select * from standardaccounts order by code", "Budgetary");
            while (!rsLoad.EndOfFile())
            {
                var code = Strings.LCase(rsLoad.Get_Fields_String("Code"));

                var account = FCConvert.ToString(rsLoad.Get_Fields("account"));

                switch (code)
                {
                    case "ue":
                        sa.UnliquidatedEncumbrance = account;

                        break;
                    case "eo":
                        sa.EncumbranceOffset = account;

                        break;
                    case "ec":
                        sa.ExpenseControl = account;

                        break;
                    case "fb":
                        sa.FundBalance = account;

                        break;
                    case "rc":
                        sa.RevenueControl = account;

                        break;
                    case "cm":
                        sa.MiscCash = account;

                        break;
                }
                rsLoad.MoveNext();
            }
            rsLoad.DisposeOf();
            GetStandardAccounts = sa;
            return GetStandardAccounts;
        }

        public cGenericCollection GetGLAccountsByAccountObject(string strAcct)
        {
            cGenericCollection GetGLAccountsByAccountObject = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsLoad = new clsDRWrapper();
                cGenericCollection retColl = new cGenericCollection();
                cBDAccount gAccount;
                string strSql;
                strSql = "select * from AccountMaster where accounttype = 'G' and secondaccountfield = '" + strAcct + "' order by account";
                rsLoad.OpenRecordset(strSql, "Budgetary");
                while (!rsLoad.EndOfFile())
                {
                    gAccount = new cBDAccount();
                    FillAccount(ref gAccount, ref rsLoad);
                    retColl.AddItem(gAccount);
                    rsLoad.MoveNext();
                }
                GetGLAccountsByAccountObject = retColl;
                return GetGLAccountsByAccountObject;
            }
            catch (Exception ex)
            {
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetGLAccountsByAccountObject;
        }

        public string GetFundForAccount(string strAccount)
        {
            string GetFundForAccount = "";
            string strFund = "";
            if (strAccount.Length > 3)
            {
                string strTemp = "";
                string[] strArray = null;
                string strDept = "";
                strTemp = Strings.Mid(strAccount, 3);
                strArray = Strings.Split(strTemp, "-", -1, CompareConstants.vbBinaryCompare);
                if (Strings.LCase(Strings.Left(strAccount + " ", 1)) == "e")
                {
                    strFund = GetFundForDepartment(strArray[0]);
                }
                else if (Strings.LCase(Strings.Left(strAccount + " ", 1)) == "r")
                {
                    strFund = GetFundForDepartment(strArray[0]);
                }
                else
                {
                    strFund = strArray[0];
                }
            }
            GetFundForAccount = strFund;
            return GetFundForAccount;
        }

        public string GetFundForDepartment(string strDepartment)
        {
            string GetFundForDepartment = "";
            string strFund = "";
            if (strDepartment != "")
            {
                if (dictDepartments.Count < 1)
                {
                    LoadDepartmentDictionary();
                }
                if (dictDepartments.ContainsKey(strDepartment))
                {
                    // vbPorter upgrade warning: Dept As cBDDepartment	OnWrite(object)
                    cBDDepartment Dept;
                    Dept = (cBDDepartment)dictDepartments[strDepartment];
                    strFund = Dept.Fund;
                }
            }
            GetFundForDepartment = strFund;
            return GetFundForDepartment;
        }
    }
}
