﻿using fecherFoundation;

namespace Global
{
	public class cPartyAddress
	{
		//=========================================================
		private int lngID;
		private int lngPartyID;
		private string strAddress1 = "";
		private string strAddress2 = "";
		private string strAddress3 = "";
		private string strCity = "";
		private string strState = "";
		private string strZip = "";
		private string strCountry = "";
		private string strOverrideName = "";
		private string strAddressType = "";
		private bool boolSeasonal;
		private string strProgModule = "";
		private string strComment = "";
		private int intStartMonth;
		private int intStartDay;
		private int intEndMonth;
		private int intEndDay;
		private int lngModAccountID;

		public string GetFormattedAddress()
		{
			string GetFormattedAddress = "";
			string strReturn = "";
			string strCRLF = "";
			string strComma = "";
			//FC:FINAL:MSH - issue #905: replace "\r\n" for correct formatting string in labels
			if (Strings.Trim(strAddress1) != "")
			{
				strReturn = strCRLF + strAddress1;
				strCRLF = "\r\n";
			}
			if (Strings.Trim(strAddress2) != "")
			{
				strReturn += strCRLF + strAddress2;
				strCRLF = "\r\n";
			}
			if (Strings.Trim(strAddress3) != "")
			{
				strReturn += strCRLF + strAddress3;
				strCRLF = "\r\n";
			}
			if (strCity != "" || strState != "" || strZip != "")
			{
				if (strCity != "")
					strComma = ",";
				strReturn += strCRLF + Strings.Trim(Strings.Trim(strCity + strComma + " " + strState) + " " + strZip);
				strCRLF = "\r\n";
			}
			if (strCountry != "")
			{
				if (Strings.LCase(Strings.Trim(strCountry)) != "united states")
				{
					strReturn += strCRLF + strCountry;
					strCRLF = "\r\n";
				}
			}
			GetFormattedAddress = strReturn;
			return GetFormattedAddress;
		}

		public int ID
		{
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
			set
			{
				lngID = value;
			}
		}

		public int PartyID
		{
			get
			{
				int PartyID = 0;
				PartyID = lngPartyID;
				return PartyID;
			}
			set
			{
				lngPartyID = value;
			}
		}

		public string Address1
		{
			get
			{
				string Address1 = "";
				Address1 = strAddress1;
				return Address1;
			}
			set
			{
				strAddress1 = value;
			}
		}

		public string Address2
		{
			get
			{
				string Address2 = "";
				Address2 = strAddress2;
				return Address2;
			}
			set
			{
				strAddress2 = value;
			}
		}

		public string Address3
		{
			get
			{
				string Address3 = "";
				Address3 = strAddress3;
				return Address3;
			}
			set
			{
				strAddress3 = value;
			}
		}

		public string City
		{
			get
			{
				string City = "";
				City = strCity;
				return City;
			}
			set
			{
				strCity = value;
			}
		}

		public string State
		{
			get
			{
				string State = "";
				State = strState;
				return State;
			}
			set
			{
				strState = value;
			}
		}

		public string Zip
		{
			get
			{
				string Zip = "";
				Zip = strZip;
				return Zip;
			}
			set
			{
				strZip = value;
			}
		}

		public string Country
		{
			get
			{
				string Country = "";
				Country = strCountry;
				return Country;
			}
			set
			{
				strCountry = value;
			}
		}

		public string OverrideName
		{
			get
			{
				string OverrideName = "";
				OverrideName = strOverrideName;
				return OverrideName;
			}
			set
			{
				strOverrideName = value;
			}
		}

		public string AddressType
		{
			get
			{
				string AddressType = "";
				AddressType = strAddressType;
				return AddressType;
			}
			set
			{
				strAddressType = value;
			}
		}

		public bool Seasonal
		{
			get
			{
				bool Seasonal = false;
				Seasonal = boolSeasonal;
				return Seasonal;
			}
			set
			{
				boolSeasonal = value;
			}
		}

		public string ProgModule
		{
			get
			{
				string ProgModule = "";
				ProgModule = strProgModule;
				return ProgModule;
			}
			set
			{
				strProgModule = value;
			}
		}

		public string Comment
		{
			get
			{
				string Comment = "";
				Comment = strComment;
				return Comment;
			}
			set
			{
				strComment = value;
			}
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public int StartMonth
		{
			get
			{
				return intStartMonth;
			}
			set
			{
				intStartMonth = value;
			}
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public int StartDay
		{
			get
			{
				return intStartDay;
			}
			set
			{
				intStartDay = value;
			}
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public int EndMonth
		{
			get
			{
				return intEndMonth;
			}
			set
			{
				intEndMonth = value;
			}
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public int EndDay
		{
			get
			{
				return intEndDay;
			}
			set
			{
				intEndDay = value;
			}
		}

		public int ModAccountID
		{
			get
			{
				int ModAccountID = 0;
				ModAccountID = lngModAccountID;
				return ModAccountID;
			}
			set
			{
				lngModAccountID = value;
			}
		}

		public bool IsSameAddress(ref cPartyAddress tAddress)
		{
			bool IsSameAddress = false;
			if (!(tAddress == null))
			{
				if (Strings.LCase(Address1) == Strings.LCase(tAddress.Address1))
				{
					if (Strings.LCase(Address2) == Strings.LCase(tAddress.Address2))
					{
						if (Strings.LCase(Address3) == Strings.LCase(tAddress.Address3))
						{
							if (Strings.LCase(City) == Strings.LCase(tAddress.City))
							{
								if (Strings.LCase(State) == Strings.LCase(tAddress.State))
								{
									if (Strings.LCase(Zip) == Strings.LCase(tAddress.Zip))
									{
										IsSameAddress = true;
										return IsSameAddress;
									}
								}
							}
						}
					}
				}
			}
			IsSameAddress = false;
			return IsSameAddress;
		}
	}
}
