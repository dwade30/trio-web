﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cBankCheckHistory
	{
		//=========================================================
		private int lngRecordID;
		private int lngBankID;
		private int lngCheckNumber;
		private string strCheckType = string.Empty;
		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int BankID
		{
			set
			{
				lngBankID = value;
				IsUpdated = true;
			}
			get
			{
				int BankID = 0;
				BankID = lngBankID;
				return BankID;
			}
		}

		public string CheckType
		{
			set
			{
				strCheckType = value;
				IsUpdated = true;
			}
			get
			{
				string CheckType = "";
				CheckType = strCheckType;
				return CheckType;
			}
		}

		public int LastCheckNumber
		{
			set
			{
				lngCheckNumber = value;
				IsUpdated = true;
			}
			get
			{
				int LastCheckNumber = 0;
				LastCheckNumber = lngCheckNumber;
				return LastCheckNumber;
			}
		}
	}
}
