﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

#if TWCR0000
using TWCR0000;


#elif TWUT0000
using modGlobal = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for sarUTDANonCash.
	/// </summary>
	public partial class sarUTDANonCash : FCSectionReport
	{
		// nObj = 1
		//   0	sarUTDANonCash	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/06/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/02/2004              *
		// ********************************************************
		bool boolWide;
		float lngWidth;
		string strSQL = "";
		clsDRWrapper rsData = new clsDRWrapper();
		int lngYear;
		int lngRowHeight;
		int intType;
		double dblPrinSum;
		double dblTaxSum;
		double dblIntSum;
		double dblCostSum;

		public sarUTDANonCash()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarUTDANonCash_ReportEnd;
		}

        private void SarUTDANonCash_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        public static sarUTDANonCash InstancePtr
		{
			get
			{
				return (sarUTDANonCash)Sys.GetInstance(typeof(sarUTDANonCash));
			}
		}

		protected sarUTDANonCash _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
				BindFields();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQLAddition = "";
			lngRowHeight = 270;
			// intType = rptUTDailyAuditMaster.intType
			intType = FCConvert.ToInt16(this.UserData);
			switch (intType)
			{
				case 0:
					{
						strSQLAddition = "AND Service <> 'S' ";
						break;
					}
				case 1:
					{
						strSQLAddition = "AND Service <> 'W' ";
						break;
					}
				case 2:
					{
						strSQLAddition = "";
						break;
					}
			}
			//end switch
			if (modGlobalConstants.Statics.gboolCR)
			{
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(Tax) AS TTax, SUM(CurrentInterest + PreLienInterest) AS Interest, SUM(LienCost) AS Cost, BillNumber, Code FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND (CashDrawer = 'N' AND GeneralLedger = 'N') AND ReceiptNumber > 0 GROUP BY BillNumber, Code";
			}
			else
			{
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(Tax) AS TTax, SUM(CurrentInterest + PreLienInterest) AS Interest, SUM(LienCost) AS Cost, BillNumber, Code FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND (CashDrawer = 'N' AND GeneralLedger = 'N') GROUP BY BillNumber, Code";
			}
			rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			frmWait.InstancePtr.IncrementProgress();
			if (rsData.EndOfFile() != true)
			{
				SetupFields();
			}
			else
			{
				EndReport();
			}
		}

		private void SetupFields()
		{
			// this will adjust the fields to thier correct
			boolWide = rptUTDailyAuditMaster.InstancePtr.boolLandscape;
			lngWidth = rptUTDailyAuditMaster.InstancePtr.lngWidth;
			// set the full page objects
			this.PrintWidth = lngWidth;
			// Me.Detail.Width = lngWidth
			lblHeader.Width = lngWidth - 1 / 1440F;
			lnHeader.X2 = lngWidth - 1 / 1440F;
			// header labels/lines
			if (!boolWide)
			{
				lblYear.Left = 0;
				lblType.Left = 1080 / 1440F;
				lblPrincipal.Left = 3060 / 1440F;
				lblTax.Left = 4680 / 1440F;
				lblInterest.Left = 6210 / 1440F;
				lblCosts.Left = 7740 / 1440F;
				lblTotal.Left = 9270 / 1440F;
			}
			else
			{
				lblYear.Left = 0;
				lblType.Left = 1440 / 1440F;
				lblPrincipal.Left = 3870 / 1440F;
				lblTax.Left = 6030 / 1440F;
				lblInterest.Left = 8100 / 1440F;
				lblCosts.Left = 10080 / 1440F;
				lblTotal.Left = 11880 / 1440F;
			}
			// Lefts
			// detail section
			fldYear.Left = lblYear.Left;
			fldType.Left = lblType.Left;
			fldPrincipal.Left = lblPrincipal.Left;
			fldTax.Left = lblTax.Left;
			fldInterest.Left = lblInterest.Left;
			fldCosts.Left = lblCosts.Left;
			fldTotal.Left = lblTotal.Left;
			// footer labels/fields
			lblTotals.Left = lblType.Left;
			fldTotalPrin.Left = lblPrincipal.Left;
			fldTotalTax.Left = lblTax.Left;
			fldTotalInt.Left = lblInterest.Left;
			fldTotalCosts.Left = lblCosts.Left;
			fldTotalTotal.Left = lblTotal.Left;
			// Widths
			// detail section
			fldYear.Width = lblYear.Width;
			fldType.Width = lblType.Width;
			fldPrincipal.Width = lblPrincipal.Width;
			fldTax.Width = lblTax.Width;
			fldInterest.Width = lblInterest.Width;
			fldCosts.Width = lblCosts.Width;
			fldTotal.Width = lblTotal.Width;
			// footer labels/fields
			lblTotals.Width = lblType.Width;
			fldTotalPrin.Width = lblPrincipal.Width;
			fldTotalTax.Width = lblTax.Width;
			fldTotalInt.Width = lblInterest.Width;
			fldTotalCosts.Width = lblCosts.Width;
			fldTotalTotal.Width = lblTotal.Width;
			lnTotals.X1 = lblPrincipal.Left;
			lnTotals.X2 = fldTotalTotal.Left + fldTotalTotal.Width;
		}

		private void BindFields()
		{
			// this will fill the fields with the correct data
			frmWait.InstancePtr.IncrementProgress();
			fldType.Text = rsData.Get_Fields_String("Code");
			fldYear.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BillNumber"));
			fldPrincipal.Text = Strings.Format(rsData.Get_Fields_Double("Prin"), "#,##0.00");
			dblPrinSum += FCConvert.ToDouble(rsData.Get_Fields_Double("Prin"));
			// TODO: Field [TTax] not found!! (maybe it is an alias?)
			fldTax.Text = Strings.Format(rsData.Get_Fields("TTax"), "#,##0.00");
			// TODO: Field [TTax] not found!! (maybe it is an alias?)
			dblTaxSum += FCConvert.ToDouble(rsData.Get_Fields("TTax"));
			// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
			fldInterest.Text = Strings.Format(rsData.Get_Fields("Interest"), "#,##0.00");
			// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
			dblIntSum += FCConvert.ToDouble(rsData.Get_Fields("Interest"));
			// TODO: Check the table for the column [Cost] and replace with corresponding Get_Field method
			fldCosts.Text = Strings.Format(rsData.Get_Fields("Cost"), "#,##0.00");
			// TODO: Check the table for the column [Cost] and replace with corresponding Get_Field method
			dblCostSum += FCConvert.ToDouble(rsData.Get_Fields("Cost"));
			// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
			// TODO: Field [TTax] not found!! (maybe it is an alias?)
			// TODO: Check the table for the column [Cost] and replace with corresponding Get_Field method
			fldTotal.Text = Strings.Format(rsData.Get_Fields("Prin") + rsData.Get_Fields("Interest") + rsData.Get_Fields("TTax") + rsData.Get_Fields("Cost"), "#,##0.00");
			// move to the next record
			rsData.MoveNext();
		}

		private void EndReport()
		{
			// this will turn all of the labels/fields to visible = false and set the heights to 0
			lnHeader.Visible = false;
			lblHeader.Visible = false;
			lnTotals.Visible = false;
			lblPrincipal.Visible = false;
			lblTax.Visible = false;
			lblInterest.Visible = false;
			lblCosts.Visible = false;
			lblTotal.Visible = false;
			fldTotalPrin.Visible = false;
			fldTotalTax.Visible = false;
			fldTotalInt.Visible = false;
			fldTotalCosts.Visible = false;
			fldTotalTotal.Visible = false;
			lblTotals.Visible = false;
			lblType.Visible = false;
			lblYear.Visible = false;
			fldYear.Visible = false;
			ReportFooter.Height = 0;
			ReportHeader.Height = 0;
			Detail.Height = 0;
			lnHeader.Visible = false;
			lnTotals.Visible = false;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalPrin.Text = Strings.Format(dblPrinSum, "#,##0.00");
			fldTotalTax.Text = Strings.Format(dblTaxSum, "#,##0.00");
			fldTotalInt.Text = Strings.Format(dblIntSum, "#,##0.00");
			fldTotalCosts.Text = Strings.Format(dblCostSum, "#,##0.00");
			// calculate the total line
			fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldTotalPrin.Text) + FCConvert.ToDouble(fldTotalTax.Text) + FCConvert.ToDouble(fldTotalInt.Text) + FCConvert.ToDouble(fldTotalCosts.Text), "#,##0.00");
		}

		private void sarUTDANonCash_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarUTDANonCash.Caption	= "Daily Audit NonCash";
			//sarUTDANonCash.Icon	= "sarUTDANonCash.dsx":0000";
			//sarUTDANonCash.Left	= 0;
			//sarUTDANonCash.Top	= 0;
			//sarUTDANonCash.Width	= 11880;
			//sarUTDANonCash.Height	= 8490;
			//sarUTDANonCash.StartUpPosition	= 3;
			//sarUTDANonCash.SectionData	= "sarUTDANonCash.dsx":058A;
			//End Unmaped Properties
		}
	}
}
