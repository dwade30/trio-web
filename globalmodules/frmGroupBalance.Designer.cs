//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWPP0000
using TWPP0000;
using modGlobalVariables = TWPP0000.modGlobal;


#elif TWBL0000
using TWBL0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmGroupBalance.
	/// </summary>
	partial class frmGroupBalance : BaseForm
	{
		public fecherFoundation.FCTextBox txtTotalBalance;
		public FCGrid GridBalance;
		public fecherFoundation.FCFrame framGroup;
		public fecherFoundation.FCLabel lblZip4;
		public fecherFoundation.FCLabel lblZip;
		public fecherFoundation.FCLabel lblState;
		public fecherFoundation.FCLabel lblCity;
		public fecherFoundation.FCLabel lblStreetName;
		public fecherFoundation.FCLabel lblStreetLetter;
		public fecherFoundation.FCLabel lblStreetNumber;
		public fecherFoundation.FCLabel lblAddress2;
		public fecherFoundation.FCLabel lblAddress1;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private FCButton cmdPrint;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGroupBalance));
            this.txtTotalBalance = new fecherFoundation.FCTextBox();
            this.GridBalance = new fecherFoundation.FCGrid();
            this.framGroup = new fecherFoundation.FCFrame();
            this.lblZip4 = new fecherFoundation.FCLabel();
            this.lblZip = new fecherFoundation.FCLabel();
            this.lblState = new fecherFoundation.FCLabel();
            this.lblCity = new fecherFoundation.FCLabel();
            this.lblStreetName = new fecherFoundation.FCLabel();
            this.lblStreetLetter = new fecherFoundation.FCLabel();
            this.lblStreetNumber = new fecherFoundation.FCLabel();
            this.lblAddress2 = new fecherFoundation.FCLabel();
            this.lblAddress1 = new fecherFoundation.FCLabel();
            this.lblName = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framGroup)).BeginInit();
            this.framGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 625);
            this.BottomPanel.Size = new System.Drawing.Size(805, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtTotalBalance);
            this.ClientArea.Controls.Add(this.GridBalance);
            this.ClientArea.Controls.Add(this.framGroup);
            this.ClientArea.Controls.Add(this.Label9);
            this.ClientArea.Size = new System.Drawing.Size(825, 628);
            this.ClientArea.TabIndex = 0;
            this.ClientArea.Controls.SetChildIndex(this.Label9, 0);
            this.ClientArea.Controls.SetChildIndex(this.framGroup, 0);
            this.ClientArea.Controls.SetChildIndex(this.GridBalance, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtTotalBalance, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(825, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(164, 28);
            this.HeaderText.Text = "Group Balance";
            // 
            // txtTotalBalance
            // 
            this.txtTotalBalance.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotalBalance.Location = new System.Drawing.Point(284, 278);
            this.txtTotalBalance.LockedOriginal = true;
            this.txtTotalBalance.Name = "txtTotalBalance";
            this.txtTotalBalance.ReadOnly = true;
            this.txtTotalBalance.Size = new System.Drawing.Size(106, 40);
            this.txtTotalBalance.TabIndex = 2;
            this.txtTotalBalance.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // GridBalance
            // 
            this.GridBalance.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.GridBalance.Cols = 4;
            this.GridBalance.ExtendLastCol = true;
            this.GridBalance.FixedCols = 0;
            this.GridBalance.Location = new System.Drawing.Point(30, 341);
            this.GridBalance.Name = "GridBalance";
            this.GridBalance.RowHeadersVisible = false;
            this.GridBalance.Rows = 1;
            this.GridBalance.Size = new System.Drawing.Size(765, 284);
            this.GridBalance.TabIndex = 3;
            // 
            // framGroup
            // 
            this.framGroup.Controls.Add(this.lblZip4);
            this.framGroup.Controls.Add(this.lblZip);
            this.framGroup.Controls.Add(this.lblState);
            this.framGroup.Controls.Add(this.lblCity);
            this.framGroup.Controls.Add(this.lblStreetName);
            this.framGroup.Controls.Add(this.lblStreetLetter);
            this.framGroup.Controls.Add(this.lblStreetNumber);
            this.framGroup.Controls.Add(this.lblAddress2);
            this.framGroup.Controls.Add(this.lblAddress1);
            this.framGroup.Controls.Add(this.lblName);
            this.framGroup.Controls.Add(this.Label8);
            this.framGroup.Controls.Add(this.Label7);
            this.framGroup.Controls.Add(this.Label6);
            this.framGroup.Controls.Add(this.Label5);
            this.framGroup.Controls.Add(this.Label4);
            this.framGroup.Controls.Add(this.Label3);
            this.framGroup.Controls.Add(this.Label2);
            this.framGroup.Controls.Add(this.Label1);
            this.framGroup.Location = new System.Drawing.Point(30, 30);
            this.framGroup.Name = "framGroup";
            this.framGroup.Size = new System.Drawing.Size(765, 221);
            this.framGroup.TabIndex = 0;
            this.framGroup.Text = "Group";
            // 
            // lblZip4
            // 
            this.lblZip4.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblZip4.Location = new System.Drawing.Point(683, 190);
            this.lblZip4.Name = "lblZip4";
            this.lblZip4.Size = new System.Drawing.Size(72, 20);
            this.lblZip4.TabIndex = 17;
            this.lblZip4.Text = "LABEL10";
            // 
            // lblZip
            // 
            this.lblZip.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblZip.Location = new System.Drawing.Point(566, 190);
            this.lblZip.Name = "lblZip";
            this.lblZip.Size = new System.Drawing.Size(82, 20);
            this.lblZip.TabIndex = 16;
            this.lblZip.Text = "LABEL10";
            // 
            // lblState
            // 
            this.lblState.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblState.Location = new System.Drawing.Point(440, 190);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(66, 20);
            this.lblState.TabIndex = 13;
            this.lblState.Text = "LABEL10";
            // 
            // lblCity
            // 
            this.lblCity.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblCity.Location = new System.Drawing.Point(151, 190);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(167, 20);
            this.lblCity.TabIndex = 11;
            this.lblCity.Text = "LABEL10";
            // 
            // lblStreetName
            // 
            this.lblStreetName.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblStreetName.Location = new System.Drawing.Point(344, 150);
            this.lblStreetName.Name = "lblStreetName";
            this.lblStreetName.Size = new System.Drawing.Size(209, 20);
            this.lblStreetName.TabIndex = 9;
            this.lblStreetName.Text = "LABEL10";
            // 
            // lblStreetLetter
            // 
            this.lblStreetLetter.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblStreetLetter.Location = new System.Drawing.Point(236, 150);
            this.lblStreetLetter.Name = "lblStreetLetter";
            this.lblStreetLetter.Size = new System.Drawing.Size(87, 20);
            this.lblStreetLetter.TabIndex = 8;
            this.lblStreetLetter.Text = "LABEL10";
            // 
            // lblStreetNumber
            // 
            this.lblStreetNumber.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblStreetNumber.Location = new System.Drawing.Point(151, 150);
            this.lblStreetNumber.Name = "lblStreetNumber";
            this.lblStreetNumber.Size = new System.Drawing.Size(65, 20);
            this.lblStreetNumber.TabIndex = 7;
            this.lblStreetNumber.Text = "LABEL10";
            // 
            // lblAddress2
            // 
            this.lblAddress2.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblAddress2.Location = new System.Drawing.Point(151, 110);
            this.lblAddress2.Name = "lblAddress2";
            this.lblAddress2.Size = new System.Drawing.Size(266, 20);
            this.lblAddress2.TabIndex = 5;
            this.lblAddress2.Text = "LABEL10";
            // 
            // lblAddress1
            // 
            this.lblAddress1.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblAddress1.Location = new System.Drawing.Point(151, 70);
            this.lblAddress1.Name = "lblAddress1";
            this.lblAddress1.Size = new System.Drawing.Size(266, 20);
            this.lblAddress1.TabIndex = 3;
            this.lblAddress1.Text = "LABEL10";
            // 
            // lblName
            // 
            this.lblName.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblName.Location = new System.Drawing.Point(151, 30);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(266, 20);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "LABEL10";
            // 
            // Label8
            // 
            this.Label8.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label8.Location = new System.Drawing.Point(20, 150);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(103, 20);
            this.Label8.TabIndex = 6;
            this.Label8.Text = "ST. ADDRESS";
            // 
            // Label7
            // 
            this.Label7.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label7.Location = new System.Drawing.Point(658, 190);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(10, 13);
            this.Label7.TabIndex = 15;
            this.Label7.Text = "-";
            // 
            // Label6
            // 
            this.Label6.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label6.Location = new System.Drawing.Point(534, 190);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(26, 20);
            this.Label6.TabIndex = 14;
            this.Label6.Text = "ZIP";
            // 
            // Label5
            // 
            this.Label5.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label5.Location = new System.Drawing.Point(363, 190);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(50, 20);
            this.Label5.TabIndex = 12;
            this.Label5.Text = "STATE";
            // 
            // Label4
            // 
            this.Label4.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label4.Location = new System.Drawing.Point(20, 190);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(83, 20);
            this.Label4.TabIndex = 10;
            this.Label4.Text = "CITY";
            // 
            // Label3
            // 
            this.Label3.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label3.Location = new System.Drawing.Point(20, 110);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(77, 20);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "ADDRESS 2";
            // 
            // Label2
            // 
            this.Label2.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label2.Location = new System.Drawing.Point(20, 70);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(89, 20);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "ADDRESS 1";
            // 
            // Label1
            // 
            this.Label1.Font = new System.Drawing.Font("default", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(71, 20);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "NAME";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(30, 292);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(198, 21);
            this.Label9.TabIndex = 1;
            this.Label9.Text = "TOTAL OUTSTANDING BALANCE";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPrint,
            this.mnuSepar1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 0;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrint.Text = "Print Preview";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = 1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(347, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(117, 48);
            this.cmdPrint.TabIndex = 0;
            this.cmdPrint.Text = "Print Preview";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // frmGroupBalance
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(825, 688);
            this.FillColor = 0;
            this.Name = "frmGroupBalance";
            this.Text = "Group Balance";
            this.Load += new System.EventHandler(this.frmGroupBalance_Load);
            this.Resize += new System.EventHandler(this.frmGroupBalance_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framGroup)).EndInit();
            this.framGroup.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion
	}
}