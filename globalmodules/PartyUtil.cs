using fecherFoundation;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;

namespace Global
{
    //[ComClass(PartyUtil.ClassId, PartyUtil.InterfaceId, PartyUtil.EventsId)]
    public class PartyUtil
    {

        #region "COM GUIDs"
        // These  GUIDs provide the COM identity for this class 
        // and its COM interfaces. If you change them, existing 
        // clients will no longer be able to access the class.
        public const string ClassId = "18d10381-ec7d-4f07-96f2-5d23524c0e3c";
        public const string InterfaceId = "242f28aa-7a46-426a-8b9e-16cebb20e050";
        #endregion
        public const string EventsId = "b4c87871-8d03-447b-b353-7c85b6f258db";

        // A creatable COM class must have a Public Sub New() 
        // with no parameters, otherwise, the class will not be 
        // registered in the COM registry and cannot be created 
        // via CreateObject.


        private ICentralPartyCondenseInterface tCP = null;
        private InteropServiceLocator tSLoc;
        private int intLastErrorCode = 0;
        private string strLastErrorDescription = "";
        public event EventHandler UpdateMessage;

        public event EventHandler CondensedPartiesHintsCompleted;

        public event EventHandler CondensedPartiesHintsCancelled;


        private enum PartyTypes
        {
            Individual = 0,
            BusinessOrInstitution = 1
        }
        public int ErrorNumber
        {
            get { return intLastErrorCode; }
            set { intLastErrorCode = value; }
        }

        public string ErrorDescription
        {
            get { return strLastErrorDescription; }
            set { strLastErrorDescription = value; }
        }

        public void ClearErrors()
        {
            intLastErrorCode = 0;
            strLastErrorDescription = "";
        }


        public void StopProcess()
        {
            if (tCP != null)
            {
                tCP.StopProcess();

            }
        }
        public bool CombineParties(int KeepPartyID, int MergePartyID)
        {
            if (tCP != null)
            {
                return tCP.CombineParties(KeepPartyID, MergePartyID);
            }
            else
            {
                return false;
            }
        }
        public bool GetCondensedPartiesHintsAsynchronously(int intMaxNumGroups)
        {
            return tCP.GetCondensedPartiesHintsAsynchronously(intMaxNumGroups);
        }
        public object getCondensedPartiesHintsInRangeAsynchronously(int intMaxNumGroups, string strStart, string strEnd)
        {
            return tCP.GetCondensedPartiesHintsInRangeAsynchronously(intMaxNumGroups, strStart, strEnd);
        }

        public bool GetCondensedPartiesHintsFromListAsynchronously([MarshalAs(UnmanagedType.SafeArray)]
ref System.Array arSrcIDs)
        {
            //If Not System.Diagnostics.EventLog.SourceExists("PartyUtil Test") Then
            //    System.Diagnostics.EventLog.CreateEventSource("PartyUtil Test")
            //End If
            // System.Diagnostics.EventLog.WriteEntry("PartyUtil Test", "Started Condense Procedure", EventLogEntryType.Information)
            ClearErrors();
            if (tCP != null)
            {
                if (arSrcIDs != null)
                {
                    if (arSrcIDs.Length > 0)
                    {
                        System.Collections.ObjectModel.Collection<int> srcColl = new System.Collections.ObjectModel.Collection<int>();
                        foreach (int intID in arSrcIDs)
                        {
                            srcColl.Add(intID);
                        }

                        return tCP.GetCondensedPartiesHintsFromListAsynchronously(ref srcColl);
                    }
                    else
                    {
                        ErrorDescription = "No parties in list";
                        ErrorNumber = -1;
                        return false;
                    }
                }
                else
                {
                    ErrorDescription = "Empty parties list";
                    ErrorNumber = -1;
                    return false;
                }
            }
            else
            {
                ErrorDescription = "Party merge interface is null";
                ErrorNumber = -1;
                return false;
            }
        }
        [return: MarshalAs(UnmanagedType.SafeArray)]
        public int[] AccountsReferencingParty(int PartyID, string strMod)
        {
            if (tCP != null)
            {
                return tCP.AccountsReferencingParty(PartyID, strMod).ToArray();
            }
            else
            {
                return new int[] {

            };
            }
        }
        public bool IsPartyReferenced(int PartyID)
        {
            if (tCP != null)
            {
                return tCP.IsPartyReferenced(PartyID);
            }
            else
            {
                return false;
            }
        }
        public string ConnectionString
        {
            get
            {
                if (tCP != null)
                {
                    return tCP.ConnectionString;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (tCP != null)
                {
                    tCP.ConnectionString = value;
                }
            }
        }
        public PartyUtil()
        {
            //base.New();
            tSLoc = new InteropServiceLocator();
            tCP = tSLoc.GetService<ICentralPartyCondenseInterface>();

        }

        private void  // ERROR: Handles clauses are not supported in C#
    tCP_CondensedPartiesHintsCancelled()
        {
            if (CondensedPartiesHintsCancelled != null)
            {
                CondensedPartiesHintsCancelled(this, EventArgs.Empty);
            }
        }


        private void  // ERROR: Handles clauses are not supported in C#
    tCP_CondensedPartiesHintsCompleted(System.Collections.ObjectModel.Collection<System.Collections.ObjectModel.Collection<int>> HintsCollection)
        {
            if (CondensedPartiesHintsCompleted != null)
            {
                CondensedPartiesHintsCompleted(this, EventArgs.Empty);
            }
            if (HintsCollection != null)
            {
                if (HintsCollection.Count > 0)
                {
                    IPartyGUIInterface tFrm = null;
                    tFrm = tSLoc.GetService<IPartyGUIInterface>();
                    if (tFrm != null)
                    {
                        tFrm.ShowCentralPartyCondenseView(ref HintsCollection, ConnectionString);
                    }
                }
                else
                {
                    //MsgBox("No duplicates found", MsgBoxStyle.OkOnly, "No Duplicates");
                }
            }
            else
            {
                //MsgBox("No duplicates found", MsgBoxStyle.OkOnly, "No Duplicates");
            }
        }

        private void  // ERROR: Handles clauses are not supported in C#
    tCP_UpdateMessage(double dblProgress, string strMessage)
        {
            //FC:TODO:JEI
            if (UpdateMessage != null)
            {
                //UpdateMessage(dblProgress, strMessage);
            }
        }

        [return: MarshalAs(UnmanagedType.SafeArray)]
        public int[] GetPartyHintsByParty(string strFirstName, string strMiddleName, string strLastName, string strDesig, int intPartyType)
        {
            ClearErrors();
            int[] returnArray;
            if (tCP != null)
            {
                try
                {
                    List<int> tcoll = tCP.GetPartyHintsByParty(strFirstName, strMiddleName, strLastName, strDesig, intPartyType);
                    if (tcoll != null)
                    {
                        returnArray = tcoll.ToArray();
                    }
                    else
                    {
                        returnArray = new int[] {

                    };
                    }
                }
                catch (Exception ex)
                {
                    returnArray = new int[] {

                };
                }
            }
            else
            {
                returnArray = new int[] {

            };
            }
            return returnArray;
        }

        private static bool IsLikelyNotPerson(string[] strName)
        {
            try
            {
                for (int x = 0; x <= strName.GetUpperBound(0); x++)
                {
                    switch (strName[x].ToString().Replace(".", "").ToUpper())
                    {
                        case "TRUSTEE":
                        case "TRUSTEES":
                            return true;
                        case "INC":
                        case "CORP":
                        case "COMPANY":
                        case "AT":
                        case "LLC":
                        case "ASSN":
                        case "LIMITED":
                            return true;
                        case "PARTNERS":
                        case "OWNERS":
                        case "ASSOC":
                        case "PROPERTIES":
                        case "APARTMENTS":
                        case "UTILITY":
                        case "UTILITIES":
                        case "UTIL":
                            return true;
                        case "ASSOCIATES":
                        case "ASSOCIATION":
                        case "CONSERVANCY":
                        case "PRESERVE":
                        case "ASSOCIATE":
                        case "REVOCABLE":
                            return true;
                        case "CORPORATION":
                        case "HOMEOWNERS":
                        case "ESTATE":
                        case "ESTATES":
                        case "BANK":
                        case "PRIVATE":
                        case "REALTY":
                        case "CREDIT":
                        case "UNION":
                            return true;
                        case "MAINE":
                        case "BANGOR":
                        case "TRUST":
                        case "INVESTMENTS":
                        case "PORTLAND":
                        case "PUBLIC":
                        case "STATE":
                            return true;
                        case "TOWN":
                        case "CITY":
                        //case "COMPANY":
                        case "BUSINESS":
                        case "INCORPORATED":
                        case "HYDRO":
                        case "ELECTRIC":
                        case "LIBRARY":
                            return true;
                        case "UNITED":
                        case "US":
                        case "GOVERNMENT":
                        case "GOV":
                        case "GOVT":
                        case "FEDERAL":
                            return true;
                        case "MUNICIPAL":
                        case "MUNICIPALITY":
                        case "COUNTY":
                        case "AUTO":
                        case "BAKERY":
                            return true;
                        case "ISLAND":
                        case "NATIONAL":
                        case "ACADIA":
                        case "FUEL":
                            //case "PRESERVE":
                            return true;
                        case "PRESERVATION":
                        case "VOLUNTEER":
                        case "COLLEGE":
                        case "UNIVERSITY":
                        case "RAILROAD":
                            return true;
                        case "TR":
                        case "REPAIR":
                        case "VILLAGE":
                            return true;
                        case "CHURCH":
                        case "CONGREGATION":
                        case "MINISTRY":
                            return true;
                        case "PARSONAGE":
                        case "EST":
                        case "ESQ":
                            return true;
                        case "IRREVOCABLE":
                        case "LIVING":
                        case "MANAGE":
                        case "SCHOOL":
                            return true;
                        case "PROPERTY":
                        case "TAX":
                        case "GOSPEL":
                        case "ASSEMBLY":
                            return true;
                        case "HEIRS":
                        case "FARM":
                        case "FARMS":
                        case "ENTERPRISE":
                            return true;
                        case "PO":
                        case "BOX":
                        case "SYSTEM":
                        //case "ASSOC":
                        case "GROUP":
                            return true;
                        case "SATELLITE":
                        case "LEASING":
                        case "EQUIPMENT":
                        case "INSTITUTION":
                        case "FINANCIAL":
                            return true;
                        case "SERVICE":
                        case "PEPSI":
                        case "DIRECTV":
                        case "TV":
                            //case "ELECTRIC":
                            return true;
                        case "AROOSTOOK":
                        case "NETWORK":
                        case "MICMACS":
                        case "PARTNERSHIP":
                            return true;
                        case "PLACE":
                        case "POTATO":
                        case "BROTHERS":
                        case "CABLE":
                        case "LMT":
                        case "ET":
                            return true;
                        case "HARDWARE":
                        case "HERMITAGE":
                        case "MECHANICAL":
                            return true;
                        case "SCIENTIFIC":
                        case "FAMILY":
                        case "PRACTICE":
                        case "OF":
                        case "SONS":
                        case "SON":
                        case "#":
                        case "%":
                            return true;
                        case "C/O":
                        case "C/0":
                        case "CO":
                            return true;
                        case "AL":
                        case "ALS":
                            if (x > 0)
                            {
                                if (strName[x - 1].ToString().ToUpper() == "ET")
                                {
                                    return true;
                                }
                            }
                            break;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool IsDesignation(string strNamePart)
        {
            try
            {
                if (string.IsNullOrEmpty(strNamePart))
                {
                    return false;
                }
                if (strNamePart.ToString().Trim() == string.Empty)
                {
                    return false;
                }
                switch (strNamePart.ToString().ToUpper())
                {
                    case "SR":
                    case "JR":
                    case "II":
                    case "III":
                    case "IV":
                    case "VI":
                    case "VII":
                    case "VIII":
                    case "IX":
                        return true;
                    case "I":
                    case "V":
                    case "X":
                        return true;
                    case "FIRST":
                    case "SECOND":
                    case "THIRD":
                    case "FOURTH":
                    case "FIFTH":
                    case "SIXTH":
                    case "SEVENTH":
                    case "EIGHTH":
                    case "NINTH":
                    case "TENTH":
                        return true;
                    case "1ST":
                    case "2ND":
                    case "3RD":
                    case "4TH":
                    case "5TH":
                    case "6TH":
                    case "7TH":
                    case "8TH":
                    case "9TH":
                    case "10TH":
                        return true;
                }
                if (Information.IsNumeric(strNamePart))
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool ParsePartialName(string strOriginal, ref string strFName, ref string strMName, ref string strDName, ref string strLName, ref int intPType, bool boolLast = false)
        {
            string strString = "";
            string[] strAry;
            bool boolParse = false;
            int intLimit = 0;
            int x = 0;
            bool boolReturn = false;
            intPType = FCConvert.ToInt32(PartyTypes.Individual);
            try
            {
                strFName = "";
                strMName = "";
                strDName = "";

                strString = strOriginal.ToString().Trim();
                strString = strString.ToString().Replace(".", "");
                if (!boolLast)
                {
                    if (strString.IndexOf(",") >= 0)
                    {
                        strFName = strOriginal;
                        return boolReturn;
                    }
                }
                else
                {
                    if (strString.IndexOf(",") >= 0)
                    {
                        strAry = strString.Split(',');
                        strLName = strAry[0];
                        strString = Strings.Trim(strAry[1]);
                    }
                }
                boolParse = true;
                strAry = strString.Split(' ');
                if (strAry.Length > 3 || strAry.Length == 1)
                {
                    strFName = strString;
                    strMName = "";
                    strDName = "";
                }
                else
                {
                    intLimit = strAry.GetUpperBound(0);
                    if (intLimit < 0)
                        return boolReturn;
                    switch (strAry.Last().ToString().ToUpper())
                    {
                        case "I":
                        case "V":
                        case "X":
                            if (strAry.GetUpperBound(0) == 2 & strOriginal.IndexOf("&") < 0)
                            {
                                strDName = strAry.Last();
                                intLimit = strAry.GetUpperBound(0) - 1;
                            }
                            break;
                        default:
                            if (IsDesignation(strAry.Last().ToString().ToUpper()))
                            {
                                strDName = strAry.Last();
                                intLimit = strAry.GetUpperBound(0) - 1;
                            }
                            break;
                    }
                    if (IsLikelyNotPerson(strAry))
                    {
                        boolParse = false;
                        intPType = FCConvert.ToInt32(PartyTypes.BusinessOrInstitution);
                    }

                    if (boolParse)
                    {
                        if (strString.IndexOf("&") >= 0)
                        {
                            strFName = "";
                            for (x = 0; x <= intLimit; x++)
                            {
                                strFName = strFName + strAry[x] + " ";
                            }
                            strFName = strFName.Trim();
                            strMName = "";
                        }
                        else if (intLimit <= 1)
                        {
                            strFName = strAry[0];
                            if (intLimit > 0)
                            {
                                strMName = strAry[1];
                            }
                        }
                        else
                        {
                            strFName = "";
                            for (x = 0; x <= intLimit; x++)
                            {
                                strFName = strFName + strAry[x] + " ";
                            }
                            strFName = strFName.ToString().Trim();
                            strMName = "";
                        }
                    }
                }
                boolReturn = true;
                return boolReturn;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [return: MarshalAs(UnmanagedType.SafeArray)]
        public UtilParty[] SplitName(string strName, bool boolAllowMultiples = true)
        {

            try
            {
                List<UtilParty> tColl = new List<UtilParty>();
                if (strName == null)
                {
                    return tColl.ToArray();
                }
                string strF = "";
                string strL = "";
                string strM = "";
                string strD = "";
                string strsecL = "";
                string strOtherHalf = "";
                bool boolParse = false;
                int intindex = 0;
                int x = 0;
                string[] strAry;
                int intLimit = 0;
                string strTemp = "";

                string strFirst = "";
                string strMiddle = "";
                string strLast = "";
                string strDesig = "";
                string strSecFirst = "";
                string strSecMiddle = "";
                string strSecLast = "";
                string strSecDesig = "";
                int intPtype1 = FCConvert.ToInt32(PartyTypes.Individual);
                int intPtype2 = FCConvert.ToInt32(PartyTypes.Individual);
                if (strName != "")
                {
                    if (strName[0] == '&' & strName.Length > 1)
                    {
                        strName = strName.Substring(1);
                    }
                }
                strName = strName.Replace("  ", " ").Trim();
                strName = strName.Replace("  ", " ").Trim();
                strName = strName.Replace("  ", " ").Trim();
                intindex = strName.IndexOf(",");
                if (intindex > 0)
                {
                    strL = strName.ToString().Substring(0, intindex);
                    strAry = strL.Split(' ');
                    if (strAry.GetUpperBound(0) > 0)
                    {
                        if (IsDesignation(strAry.Last()))
                        {
                            strDesig = strAry.Last();
                            strL = "";
                            foreach (var strTempItem in strAry.Take(strAry.Length - 1))
                            {
                                strL = strL + " " + strTempItem;
                            }
                            strL = strL.Trim();
                        }
                    }
                    if (intindex < strName.Length - 1)
                    {
                        strOtherHalf = strName.ToString().Substring(intindex + 1);
                        bool boolAnd = false;
                        string strAnd = "";
                        strOtherHalf = strOtherHalf.Replace("  ", " ").Trim();
                        strOtherHalf = strOtherHalf.Replace("  ", " ").Trim();
                        strOtherHalf = strOtherHalf.Replace("  ", " ").Trim();
                        intindex = strOtherHalf.IndexOf("&");
                        if (intindex < 0)
                        {
                            intindex = strOtherHalf.IndexOf(" and ", System.StringComparison.CurrentCultureIgnoreCase);
                            if (intindex >= 0)
                            {
                                strOtherHalf = strOtherHalf.Replace(" and ", " & ");
                                strOtherHalf = strOtherHalf.Replace(" AND ", " & ");
                                strOtherHalf = strOtherHalf.Replace(" And ", " & ");
                                strOtherHalf = strOtherHalf.Replace(" ANd ", " & ");
                                strOtherHalf = strOtherHalf.Replace(" aND ", " & ");
                                boolAnd = true;
                                strAnd = strOtherHalf;
                            }
                        }
                        if (intindex >= 0)
                        {
                            strAry = strOtherHalf.ToString().Split('&');
                            if (strAry.Length < 3)
                            {
                                strAry[1] = Strings.Trim(strAry[1]);
                                strsecL = "";
                                if (ParsePartialName(strAry[1], ref strF, ref strM, ref strD, ref strsecL, ref intPtype2, true))
                                {
                                    strSecFirst = strF;
                                    strSecMiddle = strM;
                                    if (strsecL == "")
                                    {
                                        strSecLast = strL;
                                    }
                                    else
                                    {
                                        strSecLast = strsecL;
                                    }
                                    strSecDesig = strD;
                                    strAry[0] = Strings.Trim(strAry[0]);
                                    if (ParsePartialName(strAry[0], ref strF, ref strM, ref strD, ref strsecL, ref intPtype1))
                                    {
                                    }
                                    else
                                    {
                                        strSecFirst = "";
                                        strSecMiddle = "";
                                        strSecLast = "";
                                        strSecDesig = "";
                                    }
                                }
                                else
                                {
                                    boolParse = false;
                                    if (boolAnd)
                                        strOtherHalf = strAnd;
                                    strF = strOtherHalf;
                                    strM = "";
                                    strD = "";
                                }
                            }
                            else
                            {
                                strL = "";
                                strDesig = "";
                                strF = strName;
                                boolParse = false;
                            }
                        }
                        else
                        {
                            if (boolAnd)
                                strOtherHalf = strAnd;
                            if (ParsePartialName(strOtherHalf, ref strF, ref strM, ref strD, ref strsecL, ref intPtype1))
                            {
                                if (boolAnd & strOtherHalf == strF)
                                {
                                    strOtherHalf = strAnd;
                                }
                                boolParse = true;
                            }
                            else
                            {
                                boolParse = false;
                                strF = strName;
                                strM = "";
                                strD = "";
                                strL = "";
                            }
                        }
                    }
                    else
                    {
                        strF = strName;
                        strL = "";
                        strM = "";
                        strD = "";
                    }
                }
                else
                {
                    strF = strName;
                    if (IsLikelyNotPerson(strName.Split(' ')))
                    {
                        intPtype1 = FCConvert.ToInt32(PartyTypes.BusinessOrInstitution);
                    }
                }
                strFirst = strF;
                strLast = strL;
                if (strD != "")
                {
                    strDesig = strD;
                }
                strMiddle = strM;

                UtilParty newPart = null;
                if (strFirst != "" || strLast != "")
                {
                    newPart = new UtilParty();
                    if (boolAllowMultiples || (strSecFirst == "" & strSecLast == ""))
                    {
                        newPart.Designation = strDesig;
                        newPart.FirstName = strFirst;
                        newPart.LastName = strLast;
                        newPart.MiddleName = strMiddle;
                    }
                    else
                    {
                        newPart.Designation = "";
                        newPart.LastName = "";
                        newPart.MiddleName = "";
                        newPart.FirstName = strName;
                    }
                    newPart.PartyGuid = System.Guid.NewGuid().ToString();
                    newPart.PartyType = intPtype1;
                    // newPart.Title = ""
                    //newPart.Gender = ""
                    //newPart.Title = ""
                    tColl.Add(newPart);
                }
                if (strSecFirst != "" || strSecLast != "")
                {
                    if (boolAllowMultiples || (strFirst == "" & strLast == ""))
                    {
                        newPart = new UtilParty();
                        newPart.PartyGuid = System.Guid.NewGuid().ToString();
                        newPart.PartyType = intPtype2;
                        //newPart.Title = ""
                        //newPart.Gender = ""
                        newPart.Designation = strSecDesig;
                        newPart.FirstName = strSecFirst;
                        newPart.LastName = strSecLast;
                        newPart.MiddleName = strSecMiddle;
                        tColl.Add(newPart);
                    }
                }

                return tColl.ToArray();
            }
            catch (Exception ex)
            {
                return new UtilParty[] {

            };
            }
        }

        public UtilParty SplitCorrectlyOrderedName(string strName)
        {
            try
            {
                if (string.IsNullOrEmpty(strName))
                    return null;
                UtilParty tReturn = new UtilParty();
                tReturn.PartyType = FCConvert.ToInt32(PartyTypes.Individual);
                tReturn.PartyGuid = System.Guid.NewGuid().ToString();
                //tReturn.Title = ""
                //tReturn.Gender = ""
                tReturn.Email = "";
                tReturn.FirstName = "";
                tReturn.Designation = "";
                tReturn.LastName = "";
                tReturn.MiddleName = "";

                string[] strAry;

                if (strName.ToString().Trim() != "")
                {
                    strAry = strName.ToString().Trim().Split(' ');
                    if (strAry != null)
                    {
                        if (strAry.Length > 0)
                        {
                            switch (strAry.Length)
                            {
                                case 1:
                                    tReturn.FirstName = strName.ToString().Trim();
                                    tReturn.PartyType = FCConvert.ToInt32(PartyTypes.BusinessOrInstitution);
                                    break;
                                case 2:
                                    if (!IsLikelyNotPerson(strAry))
                                    {
                                        tReturn.FirstName = strAry[0].ToString();
                                        tReturn.LastName = strAry[1].ToString();
                                    }
                                    else
                                    {
                                        tReturn.FirstName = strName.ToString().Trim();
                                        tReturn.PartyType = FCConvert.ToInt32(PartyTypes.BusinessOrInstitution);
                                    }
                                    break;
                                case 3:
                                    if (!IsLikelyNotPerson(strAry))
                                    {
                                        if (!IsDesignation(strAry.Last()))
                                        {
                                            tReturn.FirstName = strAry[0].ToString();
                                            tReturn.MiddleName = strAry[1].ToString();
                                            tReturn.LastName = strAry[2].ToString();
                                        }
                                        else
                                        {
                                            tReturn.FirstName = strAry[0].ToString();
                                            tReturn.LastName = strAry[1].ToString();
                                            tReturn.Designation = strAry[2].ToString();
                                        }
                                    }
                                    else
                                    {
                                        tReturn.FirstName = strName.ToString().Trim();
                                        tReturn.PartyType = FCConvert.ToInt32(PartyTypes.BusinessOrInstitution);
                                    }
                                    break;
                                case 4:
                                    if (!IsLikelyNotPerson(strAry))
                                    {
                                        if (IsDesignation(strAry.Last()))
                                        {
                                            if (strAry[2].ToString().ToLower() != "the")
                                            {
                                                tReturn.FirstName = strAry[0].ToString();
                                                tReturn.MiddleName = strAry[1].ToString();
                                                tReturn.LastName = strAry[2].ToString();
                                                tReturn.Designation = strAry[3].ToString();
                                            }
                                            else
                                            {
                                                tReturn.FirstName = strAry[0].ToString();
                                                tReturn.LastName = strAry[1].ToString();
                                                tReturn.Designation = strAry[3].ToString();
                                            }
                                        }
                                        else
                                        {
                                            tReturn.FirstName = strName.ToString().Trim();
                                        }
                                    }
                                    else
                                    {
                                        tReturn.FirstName = strName.ToString().Trim();
                                        if (IsLikelyNotPerson(strAry))
                                        {
                                            tReturn.PartyType = FCConvert.ToInt32(PartyTypes.BusinessOrInstitution);
                                        }
                                    }
                                    break;
                                case 5:
                                    if (!IsLikelyNotPerson(strAry))
                                    {
                                        if (IsDesignation(strAry.Last()))
                                        {
                                            if (strAry[3].ToString().ToLower() == "the")
                                            {
                                                tReturn.FirstName = strAry[0].ToString();
                                                tReturn.MiddleName = strAry[1].ToString();
                                                tReturn.LastName = strAry[2].ToString();
                                                tReturn.Designation = strAry.Last().ToString();
                                            }
                                            else
                                            {
                                                tReturn.FirstName = strName.ToString().Trim();
                                            }
                                        }
                                        else
                                        {
                                            tReturn.FirstName = strName.ToString().Trim();
                                        }
                                    }
                                    else
                                    {
                                        tReturn.FirstName = strName.ToString().Trim();
                                        tReturn.PartyType = FCConvert.ToInt32(PartyTypes.BusinessOrInstitution);
                                    }
                                    break;
                                default:
                                    tReturn.FirstName = strName.ToString().Trim();
                                    tReturn.PartyType = FCConvert.ToInt32(PartyTypes.BusinessOrInstitution);
                                    break;
                            }
                        }
                        else
                        {
                            tReturn.FirstName = strName.ToString().Trim();
                        }
                    }
                }
                else
                {
                    return null;
                }
                return tReturn;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }


    public class UtilParty
    {
        private int intID = 0;
        public int ID
        {
            get { return intID; }
            set { intID = value; }
        }
        private string strPartyGuid = "";
        public string PartyGuid
        {
            get { return strPartyGuid; }
            set { strPartyGuid = value; }
        }

        private int intPartyType = 0;
        public int PartyType
        {
            get { return intPartyType; }
            set { intPartyType = value; }
        }

        private string strFirstName = "";
        public string FirstName
        {
            get { return strFirstName; }
            set { strFirstName = value; }
        }
        private string strMiddleName = "";
        public string MiddleName
        {
            get { return strMiddleName; }
            set { strMiddleName = value; }
        }
        private string strLastName = "";
        public string LastName
        {
            get { return strLastName; }
            set { strLastName = value; }
        }
        private string strDesignation = "";
        public string Designation
        {
            get { return strDesignation; }
            set { strDesignation = value; }
        }
        private string strEmail = "";
        public string Email
        {
            get { return strEmail; }
            set { strEmail = value; }
        }
        private string strWebAddress = "";
        public string WebAddress
        {
            get { return strWebAddress; }
            set { strWebAddress = value; }
        }
        private Nullable<System.DateTime> dtDateCreated;
        public Nullable<System.DateTime> DateCreated
        {
            get { return dtDateCreated; }
            set
            {
                if (value != null)
                {
                    if (!Convert.IsDBNull(value))
                    {
                        dtDateCreated = value;
                    }
                }
            }
        }
        private string strCreatedBy = "";
        public string CreatedBy
        {
            get { return strCreatedBy; }
            set { strCreatedBy = value; }
        }
    }

}
