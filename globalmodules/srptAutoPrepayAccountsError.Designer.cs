namespace Global
{
    /// <summary>
    /// Summary description for srptAutoPrepayAccountsErrors.
    /// </summary>
    partial class srptAutoPrepayAccountsError
    {

        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptAutoPrepayAccountsError));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCreditAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCreditAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            //
            // 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
            this.Detail.CanGrow = false;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAccountNumber,
            this.fldCreditAmount});
            this.Detail.Height = 0.3020833F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            // 
            // PageHeader
            //
            // 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.txtTitle,
            this.txtMuniname,
            this.txtDate,
            this.txtPage,
            this.txtTime,
            this.Label1,
            this.Label2});
            this.PageHeader.Height = 0.7916667F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // txtTitle
            // 
            this.txtTitle.Height = 0.25F;
            this.txtTitle.Left = 0F;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.txtTitle.Text = "Accounts with Credits - No Auto Pre-Payment Created";
            this.txtTitle.Top = 0F;
            this.txtTitle.Width = 7F;
            // 
            // txtMuniname
            // 
            this.txtMuniname.Height = 0.19F;
            this.txtMuniname.Left = 0F;
            this.txtMuniname.MultiLine = false;
            this.txtMuniname.Name = "txtMuniname";
            this.txtMuniname.Style = "font-family: \'Tahoma\'";
            this.txtMuniname.Text = null;
            this.txtMuniname.Top = 0F;
            this.txtMuniname.Width = 1.5F;
            // 
            // txtDate
            // 
            this.txtDate.Height = 0.1875F;
            this.txtDate.Left = 5.9375F;
            this.txtDate.MultiLine = false;
            this.txtDate.Name = "txtDate";
            this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtDate.Text = null;
            this.txtDate.Top = 0F;
            this.txtDate.Width = 1.0625F;
            // 
            // txtPage
            // 
            this.txtPage.Height = 0.125F;
            this.txtPage.Left = 6F;
            this.txtPage.MultiLine = false;
            this.txtPage.Name = "txtPage";
            this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
            this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
            this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
            this.txtPage.Text = null;
            this.txtPage.Top = 0.1875F;
            this.txtPage.Width = 1F;
            // 
            // txtTime
            // 
            this.txtTime.Height = 0.19F;
            this.txtTime.Left = 0F;
            this.txtTime.MultiLine = false;
            this.txtTime.Name = "txtTime";
            this.txtTime.Style = "font-family: \'Tahoma\'";
            this.txtTime.Text = null;
            this.txtTime.Top = 0.15625F;
            this.txtTime.Width = 1.5625F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.3125F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 2F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
            this.Label1.Text = "Account Number";
            this.Label1.Top = 0.5F;
            this.Label1.Width = 0.9375F;
            // 
            // Label2
            // 
            this.Label2.Height = 0.3125F;
            this.Label2.HyperLink = null;
            this.Label2.Left = 3.4375F;
            this.Label2.Name = "Label2";
            this.Label2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: center";
            this.Label2.Text = "Credit Amount";
            this.Label2.Top = 0.5F;
            this.Label2.Width = 0.8125F;
            // 
            // fldAccountNumber
            // 
            this.fldAccountNumber.Height = 0.1875F;
            this.fldAccountNumber.Left = 2F;
            this.fldAccountNumber.Name = "fldAccountNumber";
            this.fldAccountNumber.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center";
            this.fldAccountNumber.Text = null;
            this.fldAccountNumber.Top = 0.0625F;
            this.fldAccountNumber.Width = 0.9375F;
            // 
            // fldCreditAmount
            // 
            this.fldCreditAmount.Height = 0.1875F;
            this.fldCreditAmount.Left = 3.4375F;
            this.fldCreditAmount.Name = "fldCreditAmount";
            this.fldCreditAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center";
            this.fldCreditAmount.Text = null;
            this.fldCreditAmount.Top = 0.0625F;
            this.fldCreditAmount.Width = 0.8125F;
            // 
            // srptAutoPrepayAccountsErrors
            //
            // 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.5F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Portrait;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.052083F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCreditAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountNumber;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCreditAmount;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
        private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
    }
}