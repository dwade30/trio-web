﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWGNENTY
using TWGNENTY;
using modExtraModules = TWGNENTY.modGNBas;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmShowCYA.
	/// </summary>
	public partial class frmShowCYA : BaseForm
	{
		public frmShowCYA()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmShowCYA InstancePtr
		{
			get
			{
				return (frmShowCYA)Sys.GetInstance(typeof(frmShowCYA));
			}
		}

		protected frmShowCYA _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               11/05/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/05/2003              *
		// ********************************************************
		bool boolLoaded;
		int lngColDate;
		int lngColDescription1;
		int lngColDescription2;
		int lngColDescription3;
		int lngColDescription4;
		int lngColDescription5;
		int lngColTime;
		int lngColUser;
		int lngColKey;
		int lngColModule;

		private void Format_Grid(bool boolReset)
		{
			FormatGrid(boolReset);
		}

		private void FormatGrid(bool boolReset = false)
		{
			int lngWid = 0;
			lngWid = vsCYA.WidthOriginal;
			if (boolReset)
			{
				vsCYA.Rows = 1;
				vsCYA.Cols = 10;
			}
			vsCYA.ColWidth(lngColDate, FCConvert.ToInt32(lngWid * 0.12));
			vsCYA.ColWidth(lngColDescription1, FCConvert.ToInt32(lngWid * 0.12));
			vsCYA.ColWidth(lngColDescription2, FCConvert.ToInt32(lngWid * 0.12));
			vsCYA.ColWidth(lngColDescription2, FCConvert.ToInt32(lngWid * 0.12));
			vsCYA.ColWidth(lngColDescription2, FCConvert.ToInt32(lngWid * 0.12));
			vsCYA.ColWidth(lngColDescription2, FCConvert.ToInt32(lngWid * 0.12));
			vsCYA.ColWidth(lngColTime, FCConvert.ToInt32(lngWid * 0.12));
			vsCYA.ColWidth(lngColUser, FCConvert.ToInt32(lngWid * 0.1));
			vsCYA.ColWidth(lngColKey, 0);
			vsCYA.ColWidth(lngColModule, FCConvert.ToInt32(lngWid * 0.05));
			vsCYA.TextMatrix(0, lngColDate, "Date");
			vsCYA.TextMatrix(0, lngColDescription1, "Desc 1");
			vsCYA.TextMatrix(0, lngColDescription2, "Desc 2");
			vsCYA.TextMatrix(0, lngColDescription3, "Desc 3");
			vsCYA.TextMatrix(0, lngColDescription4, "Desc 4");
			vsCYA.TextMatrix(0, lngColDescription5, "Desc 5");
			vsCYA.TextMatrix(0, lngColTime, "Time");
			vsCYA.TextMatrix(0, lngColUser, "User");
			vsCYA.TextMatrix(0, lngColKey, "ID");
			vsCYA.TextMatrix(0, lngColModule, "Mod");
			// set the grid height
			if ((vsCYA.Rows * vsCYA.RowHeight(0)) + 80 > ((frmShowCYA.InstancePtr.Height - vsCYA.Top) * 0.9))
			{
				vsCYA.Height = FCConvert.ToInt32(((frmShowCYA.InstancePtr.Height - vsCYA.Top) * 0.9));
				// .ScrollBars = flexScrollBarBoth
			}
			else
			{
				vsCYA.Height = (vsCYA.Rows * vsCYA.RowHeight(0)) + 80;
				// .ScrollBars = flexScrollBarHorizontal
			}
		}

		private void FillCombo()
		{
			// fill the combo box
			cmbMod.AddItem("All");
			cmbMod.AddItem("AR - Account Recievable");
			cmbMod.AddItem("BD - Budgetary");
			cmbMod.AddItem("CE - Code Enforcement");
			cmbMod.AddItem("CL - Tax Collections");
			cmbMod.AddItem("CK - Clerk");
			cmbMod.AddItem("CR - Cash Receipting");
			cmbMod.AddItem("FA - Fixed Assests");
			cmbMod.AddItem("GN - General Entry");
			cmbMod.AddItem("MV - Motor Vehicle");
			cmbMod.AddItem("PP - Personal Property");
			cmbMod.AddItem("PY - Payroll");
			cmbMod.AddItem("RE - Real Estate");
			cmbMod.AddItem("UT - Utility Billing");
			cmbMod.AddItem("VR - Voter Registration");
		}

		private void cmbMod_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// when a mod is selected, then show the accounts
			Format_Grid(true);
			ShowCYAEntries_2(cmbMod.Items[cmbMod.SelectedIndex].ToString());
			FormatGrid();
		}

		private void cmbMod_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbMod.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
			// this will make the dropdown area bigger
		}

		private void cmbMod_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						// this will dropdown the combobox
						KeyCode = (Keys)0;
						if (modAPIsConst.SendMessageByNum(cmbMod.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbMod.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
						}
						break;
					}
			}
			//end switch
		}

		private void frmShowCYA_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				FillCombo();
			}
			else
			{
			}
		}

		private void frmShowCYA_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmShowCYA.ScaleWidth	= 9300;
			//frmShowCYA.ScaleHeight	= 7845;
			//frmShowCYA.LinkTopic	= "Form1";
			//frmShowCYA.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			Format_Grid(true);
			lblInstructions.Text = "Select the TRIO module that you wish to view.";
			// set the col values
			lngColDate = 0;
			lngColDescription1 = 4;
			lngColDescription2 = 5;
			lngColDescription3 = 6;
			lngColDescription4 = 7;
			lngColDescription5 = 8;
			lngColTime = 1;
			lngColUser = 2;
			lngColKey = 3;
			lngColModule = 9;
		}

		private void frmShowCYA_Resize(object sender, System.EventArgs e)
		{
			Format_Grid(false);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void ShowCYAEntries_2(string strMod)
		{
			ShowCYAEntries(ref strMod);
		}

		private void ShowCYAEntries(ref string strMod)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with the entries from the CYA Table
				bool boolEnd = false;
				string strSQL = "";
				clsDRWrapper rsData = new clsDRWrapper();
				if (Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))) == "ALL")
				{
					// show all entries from all modules
					boolEnd = true;
					// this routine will call itself for each module
					ShowCYAEntries_2("AR");
					ShowCYAEntries_2("BD");
					ShowCYAEntries_2("CE");
					ShowCYAEntries_2("CL");
					ShowCYAEntries_2("CK");
					ShowCYAEntries_2("CR");
					ShowCYAEntries_2("E9");
					ShowCYAEntries_2("FA");
					ShowCYAEntries_2("GN");
					ShowCYAEntries_2("MV");
					ShowCYAEntries_2("PP");
					ShowCYAEntries_2("PY");
					ShowCYAEntries_2("RE");
					ShowCYAEntries_2("UT");
					ShowCYAEntries_2("VR");
				}
				else if (Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))) == "AR")
				{
					boolEnd = true;
					// this will not allow the rest to run
				}
				else if (Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))) == "BD")
				{
					rsData.OpenRecordset("SELECT * FROM CYA", modExtraModules.strBDDatabase, 0, false, 0, false, string.Empty, false);
				}
				else if (Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))) == "CE")
				{
					boolEnd = true;
					// this will not allow the rest to run
				}
				else if (Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))) == "CL")
				{
					rsData.OpenRecordset("SELECT * FROM CYA", modExtraModules.strCLDatabase, 0, false, 0, false, string.Empty, false);
				}
				else if (Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))) == "CK")
				{
					rsData.OpenRecordset("SELECT * FROM CYA", modExtraModules.strCKDatabase, 0, false, 0, false, string.Empty, false);
				}
				else if (Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))) == "CR")
				{
					rsData.OpenRecordset("SELECT * FROM CYA", modExtraModules.strCRDatabase, 0, false, 0, false, string.Empty, false);
				}
                else if (Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))) == "FA")
				{
					rsData.OpenRecordset("SELECT * FROM CYA", modExtraModules.strFADatabase, 0, false, 0, false, string.Empty, false);
				}
				else if (Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))) == "GN")
				{
					rsData.OpenRecordset("SELECT * FROM CYA", "SystemSettings", 0, false, 0, false, string.Empty, false);
				}
				else if (Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))) == "MV")
				{
					rsData.OpenRecordset("SELECT * FROM CYA", modExtraModules.strMVDatabase, 0, false, 0, false, string.Empty, false);
				}
				else if (Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))) == "PP")
				{
					rsData.OpenRecordset("SELECT * FROM CYA", modExtraModules.strPPDatabase, 0, false, 0, false, string.Empty, false);
				}
				else if (Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))) == "PY")
				{
					rsData.OpenRecordset("SELECT * FROM CYA", modExtraModules.strPYDatabase, 0, false, 0, false, string.Empty, false);
				}
				else if (Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))) == "RE")
				{
					rsData.OpenRecordset("SELECT * FROM CYA", modExtraModules.strREDatabase, 0, false, 0, false, string.Empty, false);
				}
				else if (Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))) == "UT")
				{
					rsData.OpenRecordset("SELECT * FROM CYA", modExtraModules.strUTDatabase, 0, false, 0, false, string.Empty, false);
				}
                if (!boolEnd)
				{
					// if this is not the main call for all mods then show the entries
					while (!rsData.EndOfFile())
					{
						vsCYA.AddItem("");
						// TODO: Field [Date] not found!! (maybe it is an alias?)
						vsCYA.TextMatrix(vsCYA.Rows - 1, lngColDate, Strings.Format(rsData.Get_Fields("Date"), "MM/dd/yyyy"));
						vsCYA.TextMatrix(vsCYA.Rows - 1, lngColDescription1, FCConvert.ToString(rsData.Get_Fields_String("Description1")));
						vsCYA.TextMatrix(vsCYA.Rows - 1, lngColDescription2, FCConvert.ToString(rsData.Get_Fields_String("Description2")));
						vsCYA.TextMatrix(vsCYA.Rows - 1, lngColDescription3, FCConvert.ToString(rsData.Get_Fields_String("Description3")));
						vsCYA.TextMatrix(vsCYA.Rows - 1, lngColDescription4, FCConvert.ToString(rsData.Get_Fields_String("Description4")));
						vsCYA.TextMatrix(vsCYA.Rows - 1, lngColDescription5, FCConvert.ToString(rsData.Get_Fields_String("Description5")));
						vsCYA.TextMatrix(vsCYA.Rows - 1, lngColKey, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
						vsCYA.TextMatrix(vsCYA.Rows - 1, lngColModule, Strings.UCase(Strings.Trim(Strings.Left(strMod, 3))));
						// TODO: Field [Time] not found!! (maybe it is an alias?)
						vsCYA.TextMatrix(vsCYA.Rows - 1, lngColTime, Strings.Format(rsData.Get_Fields("Time"), "hh:mm tt"));
						// TODO: Check the table for the column [UserID] and replace with corresponding Get_Field method
						vsCYA.TextMatrix(vsCYA.Rows - 1, lngColUser, FCConvert.ToString(rsData.Get_Fields("UserID")));
						rsData.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Showing " + strMod + " Records.", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Grid_RowHeightChanged(object sender, DataGridViewRowEventArgs e)
		{
			FCGrid grid = sender as FCGrid;
			vsCYA_AfterUserResize(grid.GetFlexRowIndex(e.RowIndex), -1);
		}

		private void Grid_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
		{
			FCGrid grid = sender as FCGrid;
			vsCYA_AfterUserResize(-1, grid.GetFlexColIndex(e.Column.Index));
		}

		private void vsCYA_AfterUserResize(int row, int col)
		{
			int x;
			int lngWidth = 0;
			vsCYA.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollBoth;
			if ((vsCYA.Rows * vsCYA.RowHeight(0)) + 80 > ((frmShowCYA.InstancePtr.Height - vsCYA.Top) * 0.9))
			{
				vsCYA.Height = FCConvert.ToInt32(((frmShowCYA.InstancePtr.Height - vsCYA.Top) * 0.9));
				// .ScrollBars = flexScrollBarBoth
			}
			else
			{
				vsCYA.Height = (vsCYA.Rows * vsCYA.RowHeight(0)) + 80;
				// when cols are expanded the horizontal scroll bar covers the last row
				// and the vertical scroll bar isn't enabled so size the grid to
				// include the scroll bar
				lngWidth = 0;
				for (x = 0; x <= vsCYA.Cols - 1; x++)
				{
					lngWidth += vsCYA.ColWidth(x);
				}
				// x
				if (lngWidth > vsCYA.Width)
					vsCYA.Height += vsCYA.RowHeight(0);
				// .ScrollBars = flexScrollBarHorizontal
			}
		}
	}
}
