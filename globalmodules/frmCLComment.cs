﻿using System;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using SharedApplication;
using SharedApplication.TaxCollections.Comment;
using SharedApplication.TaxCollections.Models;
using SharedDataAccess.TaxCollections;
using SharedDataAccess.UtilityBilling;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmCLComment.
	/// </summary>
	public partial class frmCLComment : BaseForm, IModalView<ICollectionsCommentViewModel>
    {
        private TaxCollectionsContext clContext;

		public frmCLComment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmCLComment(ICollectionsCommentViewModel viewModel, TaxCollectionsContext clContext) : this()
        {
            this.ViewModel = viewModel;
            this.clContext = clContext;
        }

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmCLComment InstancePtr
		{
			get
			{
				return (frmCLComment)Sys.GetInstance(typeof(frmCLComment));
			}
		}

		protected frmCLComment _InstancePtr = null;

		int lngAcct;
		string strCommentText;


 
		private void frmCLComment_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void frmCLComment_Load(object sender, System.EventArgs e)
		{

			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// set the back color of any labels or textboxes
            txtComment.Text = ViewModel.Comment;
            strCommentText = ViewModel.Comment;
            lngAcct = ViewModel.Account;
        }

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileSaveandExit_Click(object sender, System.EventArgs e)
		{
			strCommentText = txtComment.Text;
			Close();
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSaveandExit_Click(sender, e);
		}

        public ICollectionsCommentViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            this.Show(FormShowEnum.Modal);            
            ViewModel.Comment = strCommentText;
            ViewModel.Save();
        }
    }
}
