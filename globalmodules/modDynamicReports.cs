﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using fecherFoundation;

namespace Global
{
	public class modDynamicReports
	{
		//=========================================================
		// vbPorter upgrade warning: intReportType As short	OnWriteFCConvert.ToInt32(
		public static string ParseDynamicDocumentString_18(string strModDB, string strOriginal, short intReportType, dynamic clsDReport)
		{
			return ParseDynamicDocumentString(ref strModDB, ref strOriginal, ref intReportType, ref clsDReport);
		}

		public static string ParseDynamicDocumentString(ref string strModDB, ref string strOriginal, ref short intReportType, ref dynamic clsDReport)
		{
			string ParseDynamicDocumentString = "";
			// this will replace all of the variables with the information needed
			string strBuildString;
			// this is the string that will be built and returned at the end
			string strTemp = "";
			// this is a temporary sting that will be used to store and transfer string segments
			int lngNextVariable;
			// this is the position of the beginning of the next variable (0 = no more variables)
			int lngEndOfLastVariable;
			// this is the position of the end of the last variable
			string strTag = "";
			try
			{
				ParseDynamicDocumentString = "";
				lngEndOfLastVariable = 0;
				lngNextVariable = 1;
				strBuildString = "";
				if (strOriginal.Length < 1)
					return ParseDynamicDocumentString;
				// priming read
				if (Strings.InStr(1, strOriginal, "<", CompareConstants.vbBinaryCompare) > 0)
				{
					lngNextVariable = Strings.InStr(lngEndOfLastVariable + 1, strOriginal, "<", CompareConstants.vbBinaryCompare);
					// do until there are no more variables left
					do
					{
						// add the string from lngEndOfLastVariable - lngNextVariable to the BuildString
						strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1, lngNextVariable - lngEndOfLastVariable - 1);
						// set the end pointer
						if (Strings.InStr(lngNextVariable, strOriginal, ">", CompareConstants.vbBinaryCompare) > 0)
						{
							lngEndOfLastVariable = Strings.InStr(lngNextVariable, strOriginal, ">", CompareConstants.vbBinaryCompare);
							// replace the variable
							strTag = Strings.Mid(strOriginal, lngNextVariable + 1, lngEndOfLastVariable - lngNextVariable - 1);
							if (Strings.UCase(strTag) == "DATE")
							{
								strBuildString += Strings.Format(DateTime.Today, "MM/dd/yyyy");
							}
							else if (Strings.UCase(strTag) == "TIME")
							{
								strBuildString += Strings.Format(DateTime.Now, "h:mm tt");
							}
							else if (Strings.UCase(strTag) == "YEAR")
							{
								strBuildString += FCConvert.ToString(DateTime.Today.Year);
							}
							else if (Strings.UCase(strTag) == "BUSNAME")
							{
								strBuildString += modGlobalConstants.Statics.MuniName;
							}
							else if (Strings.UCase(strTag) == "CRLF")
							{
								// ignore
							}
							else
							{
								if (intReportType == modCustomBill.CNSTDYNAMICREPORTTYPECUSTOMBILL)
								{
									strBuildString += clsDReport.GetDataByCode(GetCodeFromDynamicTag(ref strTag, ref intReportType, ref strModDB));
								}
								else
								{
									strBuildString += clsDReport.GetDataByCode(GetCodeFromDynamicTag(ref strTag, ref intReportType, ref strModDB), intReportType.ToString());
								}
							}
							// check for another variable
							lngNextVariable = Strings.InStr(lngEndOfLastVariable, strOriginal, "<", CompareConstants.vbBinaryCompare);
						}
						else
						{
							lngNextVariable = 0;
						}
					}
					while (!(lngNextVariable == 0));
				}
				// take the last of the string and add it to the end
				if (lngEndOfLastVariable < strOriginal.Length)
				{
					strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1);
				}
				ParseDynamicDocumentString = strBuildString;
				return ParseDynamicDocumentString;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ParseDynamicDocumentString", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return ParseDynamicDocumentString;
			}
		}

		private static int GetCodeFromDynamicTag(ref string strTag, ref short intReportType, ref string strModDB)
		{
			int GetCodeFromDynamicTag = 0;
			clsDRWrapper clsLoad = new clsDRWrapper();
			GetCodeFromDynamicTag = 0;
			clsLoad.OpenRecordset("select * from dynamicreportcodes where reporttype = " + FCConvert.ToString(intReportType) + " and tag = '<" + strTag + ">'", strModDB);
			if (!clsLoad.EndOfFile())
			{
				GetCodeFromDynamicTag = clsLoad.Get_Fields_Int32("code");
			}
			return GetCodeFromDynamicTag;
		}
	}
}
