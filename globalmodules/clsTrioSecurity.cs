﻿using fecherFoundation;
using System;
using System.Linq;
using SharedApplication;
using SharedApplication.ClientSettings.Models;
using SharedApplication.SystemSettings.Models;
using TWSharedLibrary;
using TWSharedLibrary.Data;

namespace Global
{
    public class clsTrioSecurity
    {
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Corey Gray              *
        // Date           :                                       *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // Last Updated   :               01/14/2003              *
        // ********************************************************
        private string strSecurityDB = string.Empty;
        private string strAppName = string.Empty;
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private clsDRWrapper dcSecurity = new clsDRWrapper();
        private clsDRWrapper dcSecurity_AutoInitialized;

        private clsDRWrapper dcSecurity
        {
            get => dcSecurity_AutoInitialized ?? (dcSecurity_AutoInitialized = new clsDRWrapper());
            set => dcSecurity_AutoInitialized = value;
        }

        int intUserID;
        private bool boolDefPermission;
        private bool boolUseSec;
        private bool boolGlobalUseSec;
        private string strComputerName = string.Empty;
        private string strUserName = string.Empty;
        private bool boolDontLogActivity;
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cSecurityUser userOb = new cSecurityUser();
        private cSecurityUser userOb_AutoInitialized;

        private cSecurityUser userOb
        {
            get => userOb_AutoInitialized ?? (userOb_AutoInitialized = new cSecurityUser());
            set => userOb_AutoInitialized = value;
        }

        public clsTrioSecurity() : base()
        {
            // just sets up a default database name
            strSecurityDB = "SystemSettings";
        }

        public string GetNetworkUserName()
        {
            string networkUserName = string.Empty;
            // returns the user name logged onto this computer
            //GetNetworkUserName = strUserName;
            return networkUserName;
        }


        public void Init(string strModuleName, string strDBName = "SystemSettings", object strID = null, object intUser = null, bool boolDontLog = false)
        {
            // strAName is the application name.  If strdbname isnt sent it DefaultInfo to systemsettings
            // This loads the security permissions for the current user into rssecurity
            // In some cases you might not want the permissions for the person who logged in
            // In that situation you can provide the operatorid or the username or id number and then set intuser  to show which
            // of the three you sent
            clsDRWrapper dcTemp = new clsDRWrapper();
            string strReturn = string.Empty;
            string strField = string.Empty;
            clsDRWrapper clsTemp = new clsDRWrapper();
            string strName;
            int lngNameLen;

            try
            {
                // On Error GoTo ErrorHandler
                boolDontLogActivity = boolDontLog;
                lngNameLen = 1024;
                strName = Strings.StrDup(lngNameLen, " ");
                modAPIsConst.GetComputerNameWrp(ref strName, ref lngNameLen);

                if (Strings.Trim(strName).Length > 0)
                {
                    strComputerName = Strings.Replace(Strings.Trim(strName), FCConvert.ToString(Convert.ToChar(0)), "", 1, -1, CompareConstants.vbBinaryCompare);
                }

                lngNameLen = 1024;
                strName = Strings.StrDup(lngNameLen, " ");
                modRegistry.GetUserNameWrp(ref strName, ref lngNameLen);

                if (Strings.Trim(strName).Length > 0)
                {
                    strUserName = Strings.Replace(Strings.Trim(strName), FCConvert.ToString(Convert.ToChar(0)), "", 1, -1, CompareConstants.vbBinaryCompare);
                }

                strSecurityDB = !Information.IsNothing(strDBName) ? strDBName : "SystemSettings";
                strAppName = strModuleName;
                clsTemp.OpenRecordset("select * from globalvariables", strSecurityDB);
                boolUseSec = true;
                boolGlobalUseSec = boolUseSec;
                boolDefPermission = false;

                if (Information.IsNothing(strID))
                {
                    modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "securityid", ref strReturn);
                    intUserID = FCConvert.ToInt32(Math.Round(Conversion.Val(strReturn + "")));

                    if (intUserID == -1)
                    {
                        DontUseSecurity();
                        SaveActivity(strModuleName);

                        return;
                    }
                }
                else
                {
                    if (FCConvert.ToString(strID) == string.Empty)
                    {
                        modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "securityid", ref strReturn);
                        intUserID = FCConvert.ToInt32(Math.Round(Conversion.Val(strReturn + "")));

                        if (intUserID == -1)
                        {
                            DontUseSecurity();
                            SaveActivity(strModuleName);

                            return;
                        }
                    }
                    else
                    {
                        User user;
                        switch (FCConvert.ToInt32(intUser))
                        {
                            case 1:
                                intUserID = FCConvert.ToInt32(Math.Round(Conversion.Val(strID)));

                                break;

                            case 2:
	                            dcTemp.OpenRecordset("select * from Users where ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' and UserName = " + FCConvert.ToString(strID), "ClientSettings");
	                            if (dcTemp.BeginningOfFile() != true && dcTemp.EndOfFile() != true)
	                            {
		                            intUserID = dcTemp.Get_Fields_Int32("ID");
	                            }
	                            else
	                            {
		                            intUserID = -1;
                                }
	                            
                                break;

                            case 3:
	                            dcTemp.OpenRecordset("select * from Users where ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' and OPID = " + FCConvert.ToString(strID), "ClientSettings");
	                            if (dcTemp.BeginningOfFile() != true && dcTemp.EndOfFile() != true)
	                            {
		                            intUserID = dcTemp.Get_Fields_Int32("ID");
	                            }
	                            else
	                            {
		                            intUserID = -1;
	                            }

                                break;
                        }
                    }
                }

                cLogin tLogin = new cLogin();

                if (intUserID > 0)
                {
                    userOb = tLogin.GetUserByID(intUserID);
                }
                else
                {
                    userOb.ID = intUserID;

                    if (intUserID == -1)
                    {
                        userOb.User = "SuperUser";
                        userOb.OpID = "SuperUser";
                        userOb.UserName = "SuperUser";
                        userOb.Frequency = 0;
                        userOb.UseSecurity = false;
                    }
                }

                SaveActivity(strModuleName);

                if (intUserID == -1)
                {
                    DontUseSecurity();

                    return;
                }

                if (boolUseSec)
                {
                    dcSecurity.OpenRecordset("select * from permissionstable where userid = " + FCConvert.ToString(intUserID) + " and modulename = '" + strAppName + "'", strSecurityDB);
                    boolUseSec = true;
                    boolDefPermission = false;
                }
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                if (Information.Err(ex).Number == 0)
                {
                    boolDefPermission = false;
                }
                else
                {
                    FCMessageBox.Show(" Error number "
                                      + FCConvert.ToString(Information.Err(ex)
                                                                      .Number)
                                      + "  "
                                      + ex.GetBaseException()
                                          .Message
                                      + " occured in security init function", MsgBoxStyle.Critical, "Error");
                }
            }
            finally
            {
                dcTemp.DisposeOf();
                clsTemp.DisposeOf();
            }
        }

        private void SaveActivity(string strMod)
        {
            // saves the module,user and time that they logged in
            clsDRWrapper clsSave = new clsDRWrapper();
            string strSQL = string.Empty;
            int intTries/*unused?*/;
            try
            {
                // On Error GoTo ErrorHandler
                if (boolDontLogActivity)
                    return;
                AddActivityEntry("Start Up");
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In SaveActivitiy", MsgBoxStyle.Critical, "Error");
            }
        }

        public void AddActivityEntry(string strDescription, string strText1 = "", string strMod = "", string strDBName = "CentralData")
        {
            // saves module,user, time etc
            clsDRWrapper clsSave = new clsDRWrapper();
            string strSQL;
            int intTries;
            string strMd = string.Empty;
            try
            {
                // On Error GoTo ErrorHandler
                if (strMod == string.Empty)
                {
                    strMd = strAppName;
                }
                else
                {
                    strMd = strMod;
                }
                intTries = 0;
            Retry:
                ;
                strSQL = "Insert into activitylog (module,[user],networkusername,computername,activitydate,activitytime,description,text1) values (";
                strSQL += "'" + strMd + "'";
                strSQL += "," + FCConvert.ToString(intUserID);
                strSQL += ",'" + GetNetworkUserName() + "'";
                strSQL += ",'" + GetNameOfComputer() + "'";
                strSQL += ",'" + Strings.Format(DateTime.Today, "MM/dd/yyyy") + "'";
                strSQL += ",'" + FCConvert.ToString(DateTime.Now) + "'";
                strSQL += ",'" + strDescription + "'";
                strSQL += ",'" + strText1 + "'";
                strSQL += ")";
                clsSave.Execute(strSQL, strDBName, false);
                clsSave.OpenRecordset("select count(id) as thecount from activitylog", strDBName);
                clsSave.MoveLast();
                clsSave.MoveFirst();
                if (!clsSave.EndOfFile())
                {
                    // TODO: Field [THECOUNT] not found!! (maybe it is an alias?)
                    if (Conversion.Val(clsSave.Get_Fields("THECOUNT")) > 50000)
                    {
                        clsSave.OpenRecordset("select top 1 * from activitylog order by activityTIME", strDBName);
                        clsSave.Delete();
                        clsSave.Update();
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In SaveActivity", MsgBoxStyle.Critical, "Error");
            }
        }

        public string CheckOtherPermissions(int lngCode, string strModuleName = "", short intUserIDNumber = 0)
        {
            string CheckOtherPermissions = string.Empty;
            clsDRWrapper clsLoad = new clsDRWrapper();
            int intUserIDToUse = 0;
            string strModToUse = string.Empty;
            if (!boolGlobalUseSec)
            {
                // noone uses security
                CheckOtherPermissions = "F";
                return CheckOtherPermissions;
            }
            if (intUserIDNumber == 0)
            {
                intUserIDToUse = intUserID;
                if (!boolUseSec)
                {
                    // no security for this person
                    CheckOtherPermissions = "F";
                    return CheckOtherPermissions;
                }
            }
            else
            {
                intUserIDToUse = intUserIDNumber;
            }
            if (intUserIDToUse == -1)
            {
                // super user
                CheckOtherPermissions = "F";
                return CheckOtherPermissions;
            }
            if (strModuleName == string.Empty)
            {
                strModToUse = strAppName;
            }
            else
            {
                strModToUse = strModuleName;
            }
            if (intUserIDToUse == intUserID && strModToUse == strAppName)
            {
                CheckOtherPermissions = FCConvert.ToString(Check_Permissions(lngCode));
                return CheckOtherPermissions;
            }
            
            clsLoad.OpenRecordset("select * from permissionstable where userid = " + FCConvert.ToString(intUserIDToUse) + " and modulename = '" + strModToUse + "' and functionid = " + FCConvert.ToString(lngCode), strSecurityDB);
            if (!clsLoad.EndOfFile())
            {
                CheckOtherPermissions = FCConvert.ToString(clsLoad.Get_Fields_String("permission"));
            }
            else
            {
                CheckOtherPermissions = boolDefPermission ? "F" : "N";
            }
            return CheckOtherPermissions;
        }
        // VBto upgrade warning: 'Return' As object	OnWrite(string, object)
        public string Check_Permissions(int lngCode)
        {
            string Check_Permissions = "";
            // inputs the code representing a function
            bool boolReturn;
            try
            {
                // On Error GoTo ErrorHandler
                if (!boolUseSec)
                {
                    Check_Permissions = "F";
                    return Check_Permissions;
                }
                else if (lngCode == 0)
                {
	                Check_Permissions = "F";
	                return Check_Permissions;
                }
                if (dcSecurity.EndOfFile() && dcSecurity.BeginningOfFile())
                {
                    FCMessageBox.Show("No permissions are defined for user " + FCConvert.ToString(Get_UsersUserID()), MsgBoxStyle.Exclamation, null);
                    return Check_Permissions;
                }
                boolReturn = dcSecurity.FindFirstRecord("FunctionID", lngCode);
                if (boolReturn)
                {
                    Check_Permissions = dcSecurity.Get_Fields_String("Permission");
                }
                else
                {
                    // The function permission is missing from the database for this user
                    // send a 0 back as an error signal
                    // MsgBox "Error.  Security Permissions for code " & lngCode & vbNewLine & "for user " & intUserID & " is missing." & vbNewLine & "Permission for this function cannot be given."
                    // Check_Permissions = "0"
                    dcSecurity.AddNew();
                    dcSecurity.SetData("functionid", lngCode);
                    if (boolDefPermission)
                    {
                        dcSecurity.Set_Fields("permission", "F");
                    }
                    else
                    {
                        dcSecurity.Set_Fields("Permission", "N");
                    }
                    dcSecurity.SetData("modulename", strAppName);
                    dcSecurity.SetData("userid", intUserID);
                    dcSecurity.Update();
                    if (boolDefPermission)
                    {
                        Check_Permissions = "F";
                    }
                    else
                    {
                        Check_Permissions = "N";
                    }
                }
                return Check_Permissions;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error. Security Permissions for code " + FCConvert.ToString(lngCode) + "\r\n" + "for user " + FCConvert.ToString(intUserID) + " is missing." + "\r\n" + "Permission for this function cannot be given.", MsgBoxStyle.Critical, null);
                Check_Permissions = "0";
            }
            return Check_Permissions;
        }

        public void Get_Children(ref clsDRWrapper Children, ref int lngParent)
        {
            // takes a dataconnection and a parent code as input
            // Then it gets all records from the database with intparent as a parent
            Children.OpenRecordset("Select * from IDTable where modulename = '" + strAppName + "' and functionid = " + FCConvert.ToString(lngParent), strSecurityDB);
        }
        // VBto upgrade warning: 'Return' As Variant --> As short	OnWriteFCConvert.ToInt32(
        public short Get_UserID()
        {
            short Get_UserID = 0;
            Get_UserID = FCConvert.ToInt16(intUserID);
            return Get_UserID;
        }
        // VBto upgrade warning: 'Return' As object	OnWrite(string, object)
        public string Get_UserName()
        {
            string Get_UserName = null;
            clsDRWrapper dcTemp = new clsDRWrapper();
            if (intUserID == -1)
            {
                Get_UserName = "SuperUser";
                return Get_UserName;
            }
            dcTemp.OpenRecordset("select * from Users where ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' and id = " + FCConvert.ToString(intUserID), "ClientSettings");
            if (!dcTemp.EndOfFile())
            {
                Get_UserName = dcTemp.Get_Fields_String("username");
            }
            else
            {
                Get_UserName = string.Empty;
            }
            return Get_UserName;
        }
        // VBto upgrade warning: 'Return' As object	OnWrite(string, object)
        public string Get_OpID()
        {
            string Get_OpID = null;
            clsDRWrapper dcTemp = new clsDRWrapper();
            if (intUserID == -1)
            {
                Get_OpID = "SuperUser";
                return Get_OpID;
            }
            dcTemp.OpenRecordset("select * from Users where ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' and id = " + FCConvert.ToString(intUserID), "ClientSettings");
            if (!dcTemp.EndOfFile())
            {
                Get_OpID = dcTemp.Get_Fields_String("opid");
            }
            else
            {
                Get_OpID = string.Empty;
            }
            return Get_OpID;
        }
        // VBto upgrade warning: 'Return' As object	OnWrite(string, object)
        public string Get_UsersUserID()
        {
            //var repo = new SharedDataAccess.Repository<Security>(new SystemSettingsContext(null));
            string Get_UsersUserID = string.Empty;
            clsDRWrapper dcTemp = new clsDRWrapper();
            if (intUserID == -1)
            {
                Get_UsersUserID = "SuperUser";
                return Get_UsersUserID;
            }
            dcTemp.OpenRecordset("select * from Users where ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' and id = " + FCConvert.ToString(intUserID), "ClientSettings");
            if (!dcTemp.EndOfFile())
            {
                // TODO: Check the table for the column [userid] and replace with corresponding Get_Field method
                Get_UsersUserID = dcTemp.Get_Fields("userid");
            }
            else
            {
                Get_UsersUserID = string.Empty;
            }
            return Get_UsersUserID;
        }
        // VBto upgrade warning: 'Return' As Variant --> As bool
        public bool Using_Security()
        {
            bool Using_Security = false;
            Using_Security = boolUseSec;
            return Using_Security;
        }

        public void DontUseSecurity()
        {
            boolUseSec = false;
        }

        public string GetNameOfComputer()
        {
            string GetNameOfComputer = string.Empty;
            // returns the name of the computer as seen by the network
            GetNameOfComputer = strComputerName;
            return GetNameOfComputer;
        }
    }
}
