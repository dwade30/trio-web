﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using System.Collections.Generic;
using TWSharedLibrary;

namespace Global
{
	public class cCKPartyApp : SharedApplication.ICentralPartyApp
	{
		//=========================================================
		const string strModuleCode = "CK";

		public bool ReferencesParty(int lngID)
		{
			bool ICentralPartyApp_ReferencesParty = false;
			ICentralPartyApp_ReferencesParty = false;
			bool retAnswer;
			retAnswer = false;
			if (lngID > 0)
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select count(id)as theCount from DogOwner where partyid = " + FCConvert.ToString(lngID), "Clerk");
				// TODO: Field [theCount] not found!! (maybe it is an alias?)
				if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
				{
					retAnswer = true;
				}
				ICentralPartyApp_ReferencesParty = retAnswer;
			}
			return ICentralPartyApp_ReferencesParty;
		}

		public List<object> AccountsReferencingParty(int lngID)
		{
            List<object> ICentralPartyApp_AccountsReferencingParty = null;
			var theCollection = new List<object>();
			clsDRWrapper rsLoad = new clsDRWrapper();
			cCentralPartyReference pRef;
			if (lngID > 0)
			{
				rsLoad.OpenRecordset("select  * from DogOwner where partyid = " + FCConvert.ToString(lngID), "Clerk");
				while (!rsLoad.EndOfFile())
				{
					//Application.DoEvents();
					pRef = new cCentralPartyReference();
					pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					pRef.AlternateIdentifier = "";
					pRef.Description = "Dog Owner";
					pRef.Identifier = FCConvert.ToString(rsLoad.Get_Fields_Int32("id"));
					pRef.ModuleCode = strModuleCode;
					pRef.IdentifierForSort = Strings.Right(Strings.StrDup(12, "0") + rsLoad.Get_Fields_Int32("ID"), 12);
					theCollection.Add(pRef);
					rsLoad.MoveNext();
				}
			}
			ICentralPartyApp_AccountsReferencingParty = theCollection;
			return ICentralPartyApp_AccountsReferencingParty;
		}

		public string ModuleCode()
		{
			string ICentralPartyApp_ModuleCode = "";
			ICentralPartyApp_ModuleCode = strModuleCode;
			return ICentralPartyApp_ModuleCode;
		}

		public bool ChangePartyID(int lngOriginalPartyID, int lngNewPartyID)
		{
			bool ICentralPartyApp_ChangePartyID = false;
			clsDRWrapper rsSave = new clsDRWrapper();
			string strSQL;
			try
			{
				// On Error GoTo ErrorHandler
				strSQL = "Update DogOwner set partyid = " + FCConvert.ToString(lngNewPartyID) + " where partyid = " + FCConvert.ToString(lngOriginalPartyID);
				rsSave.Execute(strSQL, "Clerk");
				ICentralPartyApp_ChangePartyID = true;
				return ICentralPartyApp_ChangePartyID;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return ICentralPartyApp_ChangePartyID;
		}
	}
}
