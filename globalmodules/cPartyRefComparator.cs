﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace Global
{
	public class cPartyRefComparator : cIComparator
	{
		//=========================================================
		// vbPorter upgrade warning: compareA As object	OnRead(cCentralPartyReference)
		// vbPorter upgrade warning: compareB As object	OnRead(cCentralPartyReference)
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Compare(ref object compareA, ref object compareB)
		{
			short cIComparator_Compare = 0;
			cCentralPartyReference tempA;
			cCentralPartyReference tempB;
			tempA = (cCentralPartyReference)compareA;
			tempB = (cCentralPartyReference)compareB;
			int intReturn;
			intReturn = 0;
			if (!(tempA == null))
			{
				if (!(tempB == null))
				{
					if (fecherFoundation.Strings.CompareString(tempA.ModuleCode, tempB.ModuleCode, true) < 0)
					{
						intReturn = -1;
					}
					else if (fecherFoundation.Strings.CompareString(tempA.ModuleCode, tempB.ModuleCode, true) > 0)
					{
						intReturn = 1;
					}
					else
					{
						if (fecherFoundation.Strings.CompareString(tempA.Description, tempB.Description, true) < 0)
						{
							intReturn = -1;
						}
						else if (fecherFoundation.Strings.CompareString(tempA.Description, tempB.Description, true) > 0)
						{
							intReturn = 1;
						}
						else
						{
							if (fecherFoundation.Strings.CompareString(tempA.IdentifierForSort, tempB.IdentifierForSort, true) < 0)
							{
								intReturn = -1;
							}
							else if (fecherFoundation.Strings.CompareString(tempA.IdentifierForSort, tempB.IdentifierForSort, false) > 0)
							{
								intReturn = 1;
							}
						}
					}
				}
			}
			cIComparator_Compare = FCConvert.ToInt16(intReturn);
			return cIComparator_Compare;
		}
	}
}
