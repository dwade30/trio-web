//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using fecherFoundation;

namespace Global
{
    public class cRealEstateDB
    {

        //=========================================================

        private string strLastError = "";
        private int lngLastError;

        public string LastErrorMessage
        {
            get
            {
                string LastErrorMessage = "";
                LastErrorMessage = strLastError;
                return LastErrorMessage;
            }
        }

        public int LastErrorNumber
        {
            get
            {
                int LastErrorNumber = 0;
                LastErrorNumber = lngLastError;
                return LastErrorNumber;
            }
        }

        public void ClearErrors()
        {
            strLastError = "";
            lngLastError = 0;
        }

        public cVersionInfo GetVersion()
        {
            cVersionInfo GetVersion = null;
            try
            {   // On Error GoTo ErrorHandler
                ClearErrors();
                cVersionInfo tVer = new cVersionInfo();
                clsDRWrapper rsLoad = new clsDRWrapper();
                rsLoad.OpenRecordset("select * from dbversion", "RealEstate");
                if (!rsLoad.EndOfFile())
                {
                    tVer.Build = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Build"));
                    tVer.Major = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Major"));
                    tVer.Minor = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Minor"));
                    tVer.Revision = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Revision"));
                }
                GetVersion = tVer;
                return GetVersion;
            }
            catch
            {   // ErrorHandler:
                strLastError = Information.Err().Description;
                lngLastError = Information.Err().Number;
            }
            return GetVersion;
        }


        public void SetVersion(ref cVersionInfo nVersion)
        {
            try
            {   // On Error GoTo ErrorHandler
                if (!(nVersion == null))
                {
                    clsDRWrapper rsSave = new clsDRWrapper();
                    rsSave.OpenRecordset("select * from dbversion", "RealEstate");
                    if (rsSave.EndOfFile())
                    {
                        rsSave.AddNew();
                    }
                    else
                    {
                        rsSave.Edit();
                    }
                    rsSave.Set_Fields("Major", nVersion.Major);
                    rsSave.Set_Fields("Minor", nVersion.Minor);
                    rsSave.Set_Fields("Revision", nVersion.Revision);
                    rsSave.Set_Fields("Build", nVersion.Build);
                    rsSave.Update();
                }
                return;
            }
            catch
            {   // ErrorHandler:
                strLastError = Information.Err().Description;
                lngLastError = Information.Err().Number;
            }
        }

        public bool CheckVersion()
        {
            bool CheckVersion = false;
            ClearErrors();
            try
            {   // On Error GoTo ErrorHandler
                cVersionInfo nVer = new cVersionInfo();
                cVersionInfo tVer = new cVersionInfo();
                cVersionInfo cVer;
                bool boolNeedUpdate;
                clsDRWrapper rsTest = new clsDRWrapper();
                if (!rsTest.DBExists("RealEstate"))
                {
                    return CheckVersion;
                }

                cVer = GetVersion();
                if (cVer == null)
                {
                    cVer = new cVersionInfo();
                }
                nVer.Major = 1; // default to 1.0.0.0

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 0;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (MakeIndexes())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (MakeMoreIndexes())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 2;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddIMPBField())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 3;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AlterBookPageFields())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 5;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddDocumentType())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 6;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddWidthExponentFields())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 7;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddDeedNames())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 8;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddDeedNamesToPending())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 10;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddAccountPartyAddressView())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 11;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AlterTreeGrowthFields())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 12;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddDocumentIdToPictures())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 13;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
	                if (AddMovedToClientSettingsFieldToReportTitlesTable())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(ref nVer);
		                }
	                }
	                else
	                {
		                return false;
	                }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 14;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (ChangeWidowerCategories())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                CheckVersion = true;
                return CheckVersion;
            }
            catch
            {   // ErrorHandler:
                strLastError = Information.Err().Description;
                lngLastError = Information.Err().Number;
            }
            return CheckVersion;
        }

        private bool AddMovedToClientSettingsFieldToReportTitlesTable()
        {
	        clsDRWrapper rsUpdate = new clsDRWrapper();
	        string strsql = "";

	        try
	        {
		        if (!FieldExists("ReportTitles", "MovedToClientSettings", "RealEstate"))
		        {
			        strsql = "Alter Table ReportTitles Add MovedToClientSettings bit null";
			        rsUpdate.Execute(strsql, "RealEstate");
			        rsUpdate.Execute("Update ReportTitles set MovedToClientSettings = 0", "RealEstate");
		        }
 
		        return true;
	        }
	        catch (Exception e)
	        {
		        return false;
	        }
        }

        private bool AddDocumentType()
        {
            bool AddDocumentType = false;
            try
            {   // On Error GoTo ErrorHandler

                AddDocumentType = true;
                return AddDocumentType;
            }
            catch
            {   // ErrorHandler:
                strLastError = Information.Err().Description;
                lngLastError = Information.Err().Number;
            }
            return AddDocumentType;
        }

        private string GetDataType(string strTableName, string strFieldName, string strDB)
        {
            string GetDataType = "";
            string strSQL;
            clsDRWrapper rsLoad = new clsDRWrapper();
            strSQL = "select data_type from information_schema.columns where table_name = '" + strTableName + "' and column_name = '" + strFieldName + "'";
            rsLoad.OpenRecordset(strSQL, strDB);
            if (!rsLoad.EndOfFile())
            {
                // TODO: Field [data_type] not found!! (maybe it is an alias?)
                GetDataType = FCConvert.ToString(rsLoad.Get_Fields("data_type"));
            }
            return GetDataType;
        }

        private bool AlterBookPageFields()
        {
            bool AlterBookPageFields = false;
            try
            {   // On Error GoTo ErrorHandler
                string strSQL = "";
                bool boolBook = false;
                bool boolPage = false;
                bool boolSRBook = false;
                bool boolSRPage = false;
                bool boolPendingBook = false;
                bool boolPendingPage = false;
                string strType = "";

                if (GetDataType("BookPage", "Book", "RealEstate") == "int")
                {
                    boolBook = true;
                }
                if (GetDataType("BookPage", "Page", "RealEstate") == "int")
                {
                    boolPage = true;
                }
                if (GetDataType("SRBookPage", "Book", "RealEstate") == "int")
                {
                    boolSRBook = true;
                }
                if (GetDataType("SRBookPage", "Page", "RealEstate") == "int")
                {
                    boolSRPage = true;
                }
                if (GetDataType("Pending", "Book", "RealEstate") == "int")
                {
                    boolPendingBook = true;
                }
                if (GetDataType("Pending", "Page", "RealEstate") == "int")
                {
                    boolPendingPage = true;
                }

                clsDRWrapper rsSave = new clsDRWrapper();
                if (boolBook)
                {
                    strSQL = "Alter table bookpage alter column Book varchar(255) null";
                    rsSave.Execute(strSQL, "RealEstate");
                }
                if (boolPage)
                {
                    strSQL = "Alter table bookpage alter column Page varchar(255) null";
                    rsSave.Execute(strSQL, "RealEstate");
                }
                if (boolSRBook)
                {
                    strSQL = "Alter table SRbookpage alter column Book varchar(255) null";
                    rsSave.Execute(strSQL, "RealEstate");
                }
                if (boolSRPage)
                {
                    strSQL = "Alter table SRbookpage alter column Page varchar(255) null";
                    rsSave.Execute(strSQL, "RealEstate");
                }
                if (boolPendingBook)
                {
                    strSQL = "Alter table Pending alter column Book varchar(255) null";
                    rsSave.Execute(strSQL, "RealEstate");
                }
                if (boolPendingPage)
                {
                    strSQL = "Alter table Pending alter column Page varchar(255) null";
                    rsSave.Execute(strSQL, "RealEstate");
                }
                AlterBookPageFields = true;
                return AlterBookPageFields;
            }
            catch
            {   // ErrorHandler:

            }
            return AlterBookPageFields;
        }

        private bool AlterTreeGrowthFields()
        {
            try
            {
                var boolSoft = false;
                var boolMixed = false;
                var boolHard = false;
                var boolOther = false;
                var strSQL = "";
                var rsSave = new clsDRWrapper();
                if (GetDataType("Master", "rsSoft", "RealEstate") == "int")
                {
                    boolSoft = true;
                }
                if (GetDataType("Master", "rsMixed", "RealEstate") == "int")
                {
                    boolMixed = true;
                }
                if (GetDataType("Master", "rsHard", "RealEstate") == "int")
                {
                    boolHard = true;
                }
                if (GetDataType("Master", "rsOther", "RealEstate") == "int")
                {
                    boolOther = true;
                }
                if (boolSoft)
                {
                    strSQL = "Alter table Master alter column RSSoft float null";
                    rsSave.Execute(strSQL, "RealEstate");
                }
                if (boolMixed)
                {
                    strSQL = "Alter table Master alter column RSMixed float null";
                    rsSave.Execute(strSQL, "RealEstate");
                }
                if (boolHard)
                {
                    strSQL = "Alter table Master alter column RSHard float null";
                    rsSave.Execute(strSQL, "RealEstate");
                }
                if (boolOther)
                {
                    strSQL = "Alter table Master alter column RSOther float null";
                    rsSave.Execute(strSQL, "RealEstate");
                }
                return true;
            }
            catch
            {
                return false;
            }

        }

        private bool AddIMPBField()
        {
            bool AddIMPBField = false;
            try
            {   // On Error GoTo ErrorHandler
                clsDRWrapper rsSave = new clsDRWrapper();
                string strSQL;
                strSQL = "Select column_name from information_schema.columns where column_name = 'IMPBTrackingNumber' and table_name = 'CMFNumbers'";
                rsSave.OpenRecordset(strSQL, "RealEstate");
                if (rsSave.EndOfFile())
                {
                    strSQL = "alter table.[dbo].[CMFNumbers] add [IMPBTrackingNumber] nvarchar(50) NULL";
                    rsSave.Execute(strSQL, "RealEstate", false);
                }
                AddIMPBField = true;
                return AddIMPBField;
            }
            catch
            {   // ErrorHandler:
            }
            return AddIMPBField;
        }

        private bool AddSettingsTable()
        {
            bool AddSettingsTable = false;
            cSettingsController contSet = new cSettingsController();
            AddSettingsTable = contSet.AddSettingsTable("PersonalProperty");
            return AddSettingsTable;
        }


        private bool MakeIndexes()
        {
            bool MakeIndexes = false;
            try
            {   // On Error GoTo ErrorHandler
                MakeIndex("RealEstate", "Master", "ix_AccountCard", "RSAccount,RSCard");
                MakeIndex("RealEstate", "Dwelling", "ix_AccountCard", "RSAccount,RSCard");
                MakeIndex("RealEstate", "Commercial", "ix_AccountCard", "RSAccount,RSCard");
                MakeIndex("RealEstate", "Outbuilding", "ix_AccountCard", "RSAccount,RSCard");
                MakeIndex("RealEstate", "BookPage", "ix_Account", "Account");
                MakeIndex("RealEstate", "CommercialCost", "ix_RecordNumber", "CRecordNumber");
                MakeIndex("RealEstate", "CommRec", "ix_CRecordNumber", "CRecordNumber");
                MakeIndex("RealEstate", "CostRecord", "ix_CRecordNumber", "CRecordNumber");
                MakeIndex("RealEstate", "DwellingExterior", "ix_Code", "Code");
                MakeIndex("RealEstate", "DwellingHeat", "ix_Code", "Code");
                MakeIndex("RealEstate", "DwellingStyle", "ix_Code", "Code");
                MakeIndex("RealEstate", "EconomicCode", "ix_Code", "Code");
                MakeIndex("RealEstate", "ExemptCode", "ix_Code", "Code");
                MakeIndex("RealEstate", "ExpenseTable", "ix_Code", "Code");
                MakeIndex("RealEstate", "LandSchedule", "ix_Schedule", "Schedule");
                MakeIndex("RealEstate", "LandSchedule", "ix_ScheduleCode", "Schedule,Code");
                MakeIndex("RealEstate", "LandTRecord", "ix_LRecordNumber", "LRecordNumber");
                MakeIndex("RealEstate", "LandType", "ix_Code", "Code");
                MakeIndex("RealEstate", "MapLot", "ix_Account", "Account");
                MakeIndex("RealEstate", "MVRReport", "ix_Code", "Code");
                MakeIndex("RealEstate", "Neighborhood", "ix_Code", "Code");
                MakeIndex("RealEstate", "OccupancyCodes", "ix_Code", "Code");
                MakeIndex("RealEstate", "OutbuildingSQFTFactors", "ix_Code", "Code");
                MakeIndex("RealEstate", "Owners", "ix_Account", "Account");
                MakeIndex("RealEstate", "PictureRecord", "ix_AccountCardPic", "MRAccountNumber,RSCard,PicNum");
                MakeIndex("RealEstate", "PreviousAssessment", "ix_AccountCard", "Account,Card");
                MakeIndex("RealEstate", "tblBldgCode", "ix_Code", "Code");
                MakeIndex("RealEstate", "tblLandCode", "ix_Code", "Code");
                MakeIndex("RealEstate", "tblTranCode", "ix_Code", "Code");
                MakeIndex("RealEstate", "Zones", "ix_Code", "Code");
                MakeIndex("RealEstate", "ZoneSchedule", "ix_ZoneNeighborhood", "Zone,Neighborhood");
                MakeIndexes = true;
                return MakeIndexes;
            }
            catch
            {   // ErrorHandler:
                MessageBox.Show("Error " + FCConvert.ToString(Information.Err().Number) + "  " + Information.Err().Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                MakeIndexes = false;
            }
            return MakeIndexes;
        }
        private bool MakeMoreIndexes()
        {
            bool MakeMoreIndexes = false;
            try
            {   // On Error GoTo ErrorHandler
                MakeIndex("RealEstate", "SummRecord", "ix_AccountCard", "SACCT,CardNumber");
                MakeMoreIndexes = true;
                return MakeMoreIndexes;
            }
            catch
            {   // ErrorHandler:
                MessageBox.Show("Error " + FCConvert.ToString(Information.Err().Number) + "  " + Information.Err().Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                MakeMoreIndexes = false;
            }
            return MakeMoreIndexes;
        }
        private void MakeIndex(string strDBName, string strTableName, string strName, string strFields)
        {
            string strSQL = "";
            clsDRWrapper rsSave = new clsDRWrapper();
            rsSave.OpenRecordset(GetFindIndexSQL(strTableName, strName), strDBName);
            if (rsSave.EndOfFile())
            {
                rsSave.Execute(GetAddIndexSQL(strTableName, strName, strFields), strDBName);
            }
        }

        private string GetFindIndexSQL(string strTableName, string strName)
        {
            string GetFindIndexSQL = "";
            string strSQL;

            strSQL = "Select * from sys.indexes where object_id = OBJECT_ID(N'[dbo].[" + strTableName + "]') and name =N'" + strName + "'";
            GetFindIndexSQL = strSQL;
            return GetFindIndexSQL;
        }

        private string GetAddIndexSQL(string strTableName, string strName, string strFields)
        {
            string GetAddIndexSQL = "";
            string[] strAry = null;
            strAry = Strings.Split(strFields, ",", -1, CompareConstants.vbBinaryCompare);
            string strSQL;
            strSQL = "CREATE NONCLUSTERED INDEX [" + strName + "] on [dbo].[" + strTableName + "] (";
            int x;
            for (x = 0; x <= Information.UBound(strAry, 1); x++)
            {
                if (x > 0)
                {
                    strSQL += ",[" + strAry[x] + "] ASC";
                }
                else
                {
                    strSQL += "[" + strAry[x] + "] ASC";
                }
            } // x
            strSQL += ") WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]";
            GetAddIndexSQL = strSQL;
            return GetAddIndexSQL;
        }

        private bool CreateCommercialCostTables()
        {
            bool CreateCommercialCostTables = false;
            try
            {   // On Error GoTo ErrorHandler
                clsDRWrapper rsSave = new clsDRWrapper();
                string strSQL;

                strSQL = GetTableCreationHeaderSQL("CommercialOccupancyCodes");
                strSQL += "[Name] [nvarchar] (255) NULL,";
                strSQL += "[Code] [nvarchar] (255) NULL,";
                strSQL += "[SectionID] [int] NULL,";
                strSQL += "[OccupancyCodeNumber] [int] NULL,";
                strSQL += GetTablePKSql("CommercialOccupancyCodes");
                rsSave.Execute(strSQL, "RealEstate", false);

                strSQL = GetTableCreationHeaderSQL("CommercialSections");
                strSQL += "[Name] [nvarchar] (255) NULL,";
                strSQL += "[Description] [nvarchar] (255) NULL,";
                strSQL += GetTablePKSql("CommercialSections");
                rsSave.Execute(strSQL, "RealEstate", false);

                strSQL = GetTableCreationHeaderSQL("StoryHeightMultipliers");
                strSQL += "[SectionID] [int] NULL,";
                strSQL += "[WallHeight] [float]  NULL,";
                strSQL += "[Multiplier] [float]  NULL,";
                strSQL += GetTablePKSql("StoryHeightMultipliers");
                rsSave.Execute(strSQL, "RealEstate", false);

                strSQL = GetTableCreationHeaderSQL("AreaPerimeterMultipliers");
                strSQL += "[SectionID] [int] NULL,";
                strSQL += "[FloorArea] [float] NULL,";
                strSQL += "[Perimeter] [float]  NULL,";
                strSQL += "[Multiplier] [float]  NULL,";
                strSQL += GetTablePKSql("AreaPerimeterMultipliers");
                rsSave.Execute(strSQL, "RealEstate", false);

                strSQL = GetTableCreationHeaderSQL("CommercialHeatingCooling");
                strSQL += "[ShortDescription] [nvarchar] (255) NULL,";
                strSQL += "[FullDescription] [nvarchar] (255) NULL,";
                strSQL += "[CodeNumber] [int] NULL,";
                strSQL += GetTablePKSql("CommercialHeatingCooling");
                rsSave.Execute(strSQL, "RealEstate", false);

                strSQL = GetTableCreationHeaderSQL("CommercialHeatingCoolingCosts");
                strSQL += "[SectionID] [int] NULL,";
                strSQL += "[CommercialHeatingCoolingID] [int] NULL,";
                strSQL += "[MildClimateCost] [float] NULL,";
                strSQL += "[ModerateClimateCost] [float] NULL,";
                strSQL += "[ExtremeClimateCost] [float] NULL,";
                strSQL += GetTablePKSql("CommercialHeatingCollingCosts");
                rsSave.Execute(strSQL, "RealEstate", false);

                strSQL = GetTableCreationHeaderSQL("CommercialExteriorWalls");
                strSQL += "[FullDescription] [nvarchar] (255) NULL,";
                strSQL += "[ShortDescription] [nvarchar] (255) NULL,";
                strSQL += "[CodeNumber] [int] NULL,";
                strSQL += GetTablePKSql("CommercialExteriorWalls");
                rsSave.Execute(strSQL, "RealEstate", false);

                strSQL = GetTableCreationHeaderSQL("CommercialLocations");
                strSQL += "[State] [nvarchar] (255) NULL,";
                strSQL += "[Location] [nvarchar] (255) NULL,";
                strSQL += GetTablePKSql("CommercialLocations");
                rsSave.Execute(strSQL, "RealEstate", false);

                strSQL = GetTableCreationHeaderSQL("CommercialLocationMultipliers");
                strSQL += "[CommercialLocationID] [int] NULL,";
                strSQL += "[ClassCategory] [int] NULL,";
                strSQL += "[Multiplier] [float] NULL,";
                strSQL += GetTablePKSql("CommercialLocationMultipliers");
                rsSave.Execute(strSQL, "RealEstate", false);

                strSQL = GetTableCreationHeaderSQL("CommercialOccupancyCosts");
                strSQL += "[OccupancyCodeID] [int] NULL,";
                strSQL += "[ConstructionClassID] [int] NULL,";
                strSQL += "[QualityCode] [int] NULL,";
                strSQL += "[SqftCost] [float] NULL,";
                strSQL += "[Life] [int] NULL,";
                strSQL += GetTablePKSql("CommercialOccupancyCosts");
                rsSave.Execute(strSQL, "RealEstate", false);

                strSQL = GetTableCreationHeaderSQL("CommercialConditions");
                strSQL += "[Description] [nvarchar] (255) NULL,";
                strSQL += "[CodeNumber] [int] NULL,";
                strSQL += "[High] [float] NULL,";
                strSQL += "[Low] [float] NULL,";
                strSQL += "[Exponent] [float] NULL,";
                strSQL += GetTablePKSql("CommercialConditions");
                rsSave.Execute(strSQL, "RealEstate", false);

                strSQL = GetTableCreationHeaderSQL("CommercialIncomeRates");
                strSQL += "[OccupancyCodeNumber] [int] NULL,";
                strSQL += "[Rate] [float] NULL,";
                strSQL += GetTablePKSql("CommercialIncomeRates");
                rsSave.Execute(strSQL, "RealEstate", false);

                strSQL = GetTableCreationHeaderSQL("CommercialConstructionClasses");
                strSQL += "[Name] [nvarchar] (255) NULL,";
                strSQL += "[ClassCategory] [int] NULL,";
                strSQL += "[CodeNumber] [int] NULL,";
                strSQL += GetTablePKSql("CommercialConstructionClasses");
                rsSave.Execute(strSQL, "RealEstate", false);

                strSQL = GetTableCreationHeaderSQL("CommercialTrends");
                strSQL += "[SectionID] [int] NULL,";
                strSQL += "[ConstructionClassID] [int] NULL,";
                strSQL += "[Multiplier] [float] NULL,";
                strSQL += GetTablePKSql("CommercialTrends");
                rsSave.Execute(strSQL, "RealEstate", false);

                CreateCommercialCostTables = true;
                return CreateCommercialCostTables;
            }
            catch
            {   // ErrorHandler:
            }
            return CreateCommercialCostTables;
        }

        private string GetTableCreationHeaderSQL(string strTableName)
        {
            string GetTableCreationHeaderSQL = "";
            string strSQL;
            strSQL = "if not exists (select * from information_schema.tables where table_name = N'" + strTableName + "') " + "\r\n";
            strSQL += "Create Table [dbo].[" + strTableName + "] (";
            strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
            GetTableCreationHeaderSQL = strSQL;
            return GetTableCreationHeaderSQL;
        }

        private string GetTablePKSql(string strTableName)
        {
            string GetTablePKSql = "";
            string strSQL;
            strSQL = " Constraint [PK_" + strTableName + "] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
            GetTablePKSql = strSQL;
            return GetTablePKSql;
        }

        private bool FieldExists(string strTable, string strField, string strDB)
        {
            bool FieldExists = false;
            try
            {   // On Error GoTo ErrorHandler
                string strSQL;
                clsDRWrapper rsLoad = new clsDRWrapper();
                strSQL = "Select column_name from information_schema.columns where column_name = '" + strField + "' and table_name = '" + strTable + "'";
                rsLoad.OpenRecordset(strSQL, strDB);
                if (!rsLoad.EndOfFile())
                {
                    FieldExists = true;
                }
                return FieldExists;
            }
            catch
            {   // ErrorHandler:

            }
            return FieldExists;
        }

        private bool AddWidthExponentFields()
        {
            bool AddWidthExponentFields = false;
            try
            {   // On Error GoTo ErrorHandler
                string strSQL = "";
                bool boolCreate1 = false;
                bool boolCreate2 = false;
                clsDRWrapper rsSave = new clsDRWrapper();
                if (!FieldExists("LandSchedule", "WidthExponent1", "RealEstate"))
                {
                    boolCreate1 = true;
                }
                if (!FieldExists("LandSchedule", "WidthExponent2", "RealEstate"))
                {
                    boolCreate2 = true;
                }
                if (boolCreate1)
                {
                    strSQL = "alter table LandSchedule Add WidthExponent1 [float] NULL";
                    rsSave.Execute(strSQL, "RealEstate");
                    strSQL = "update LandSchedule set WidthExponent1 = .5";
                    rsSave.Execute(strSQL, "RealEstate");
                }
                if (boolCreate2)
                {
                    strSQL = "alter table LandSchedule Add WidthExponent2 [float]  NULL";
                    rsSave.Execute(strSQL, "RealEstate");
                    strSQL = "update LandSchedule set WidthExponent2 = .5";
                    rsSave.Execute(strSQL, "RealEstate");
                }
                AddWidthExponentFields = true;
                return AddWidthExponentFields;
            }
            catch
            {   // ErrorHandler:
            }
            return AddWidthExponentFields;
        }

        private bool AddDeedNames()
        {
            var boolAddDeedName1 = false;
            var boolAddDeedName2 = false;            
            try
            {
                if (!FieldExists("Master","DeedName1","RealEstate"))
                {
                    boolAddDeedName1 = true;
                }

                if (!FieldExists("Master", "DeedName2", "RealEstate"))
                {
                    boolAddDeedName2 = true;
                }

                if (boolAddDeedName1 || boolAddDeedName2)
                {
                    var rsSave = new clsDRWrapper();
                    if (boolAddDeedName1)
                    {
                        rsSave.Execute("alter table Master add DeedName1 [nvarchar] (255) NULL", "RealEstate");
                    }

                    if (boolAddDeedName2)
                    {
                        rsSave.Execute("alter table Master add DeedName2 [nvarchar] (255) NULL", "RealEstate");
                    }
                }
                    return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddDeedNamesToPending()
        {
            try
            {
                var boolAddDeedName1 = false;
                var boolAddDeedName2 = false;
                if (!FieldExists("Pending", "DeedName1", "RealEstate"))
                {
                    boolAddDeedName1 = true;
                }

                if (!FieldExists("Pending", "DeedName2", "RealEstate"))
                {
                    boolAddDeedName2 = true;
                }

                if (boolAddDeedName1 || boolAddDeedName2)
                {
                    var rsSave = new clsDRWrapper();
                    if (boolAddDeedName1)
                    {
                        rsSave.Execute("alter table Pending add DeedName1 [nvarchar] (255) NULL", "RealEstate");
                    }

                    if (boolAddDeedName2)
                    {
                        rsSave.Execute("alter table Pending add DeedName2 [nvarchar] (255) NULL", "RealEstate");
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool ConvertPartiesToDeedNames()
        {
            try
            {
                var strSQL = "select * from master where isnull(deedname1,'') = '' and ownerpartyid > 0";
                var rsSave = new clsDRWrapper();
                var rsLoad = new clsDRWrapper();
                rsSave.OpenRecordset(strSQL, "RealEstate");
                while (!rsSave.EndOfFile())
                {
                    rsLoad.OpenRecordset("select * from parties where id = " + rsSave.Get_Fields_Int32("ownerpartyid"),
                        "CentralParties");
                    if (!rsLoad.EndOfFile())
                    {
                        rsSave.Edit();
                        var lastName = rsLoad.Get_Fields_String("lastname").Trim();
                        var firstName = rsLoad.Get_Fields_String("FirstName").Trim();
                        var middleName = rsLoad.Get_Fields_String("middlename").Trim();
                        var designation = rsLoad.Get_Fields_String("Designation").Trim();
                        var fullName = "";
                        if (rsLoad.Get_Fields_String("lastname").Trim() != "")
                        {
                            fullName = ((lastName + ", " + firstName + " " + middleName).Trim() + " " + designation)
                                .Trim();
                        }
                        else
                        {
                            fullName = ((firstName + " " + middleName).Trim() + " " + designation).Trim();
                        }
                        rsSave.Set_Fields("DeedName1",fullName);
                        rsSave.Update();
                    }

                    if (rsSave.Get_Fields_Int32("secownerpartyid") > 0)
                    {
                        rsLoad.OpenRecordset(
                            "select * from parties where id = " + rsSave.Get_Fields_Int32("Secownerpartyid"),
                            "CentralParties");
                        if (!rsLoad.EndOfFile())
                        {
                            rsSave.Edit();
                            var lastName = rsLoad.Get_Fields_String("lastname").Trim();
                            var firstName = rsLoad.Get_Fields_String("FirstName").Trim();
                            var middleName = rsLoad.Get_Fields_String("middlename").Trim();
                            var designation = rsLoad.Get_Fields_String("Designation").Trim();
                            var fullName = "";
                            if (rsLoad.Get_Fields_String("lastname").Trim() != "")
                            {
                                fullName = ((lastName + ", " + firstName + " " + middleName).Trim() + " " + designation)
                                    .Trim();
                            }
                            else
                            {
                                fullName = ((firstName + " " + middleName).Trim() + " " + designation).Trim();
                            }
                            rsSave.Set_Fields("DeedName2", fullName);
                            rsSave.Update();
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool AddAccountPartyAddressView()
        {
            clsDRWrapper rsUpdate = new clsDRWrapper();

            try
            {
                rsUpdate.OpenRecordset("select* FROM sys.views where name = 'AccountPartyAddressView'", "RealEstate");
                if (!rsUpdate.EndOfFile())
                {
                    rsUpdate.Execute("Drop View [dbo].[AccountPartyAddressView]", "RealEstate");
                }
                var sql = "CREATE VIEW [dbo].[AccountPartyAddressView] as " 
                 + "select tbl2.*, Parties.PartyGuid as SecOwnerGuid, Parties.PartyType as SecOwnerPartyType, Parties.FirstName as SecOwnerFirstName, Parties.MiddleName as SecOwnerMiddleName, Parties.LastName as SecOwnerLastName, Parties.Designation as SecOwnerDesignation " 
                 + "from (select tbl1.*, Addresses.Address1,Addresses.Address2,Addresses.Address3,Addresses.City, Addresses.State,Addresses.Zip,Addresses.AddressType  from "
                 + "(select Master.*"
                 + ", parties.PartyGuid as OwnerGuid, parties.PartyType as OwnerPartyType, parties.FirstName as OwnerFirstName, parties.MiddleName as OwnerMiddleName, parties.LastName as OwnerLastName, parties.Designation as OwnerDesignation, parties.Email as OwnerEmail"
                 + " from dbo.Master Inner join "
                 + rsUpdate.Get_GetFullDBName("CentralParties") 
                 + ".dbo.Parties on Master.OwnerPartyID = Parties.id) tbl1"
                 + " left join " + rsUpdate.Get_GetFullDBName("CentralParties") + ".dbo.Addresses"
                 + " on tbl1.OwnerPartyID = Addresses.PartyID "
                 + " where AddressType = 'Primary') tbl2 "
                 + " left join " + rsUpdate.Get_GetFullDBName("CentralParties") 
                 + ".dbo.Parties on tbl2.SecOwnerPartyID = Parties.ID";
                
                rsUpdate.Execute(sql,"RealEstate");
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddDocumentIdToPictures()
        {
            clsDRWrapper rsUpdate = new clsDRWrapper();
            try
            {
                if (!FieldExists("PictureRecord", "DocumentIdentifier", "RealEstate"))
                {
                    rsUpdate.Execute("alter table PictureRecord add DocumentIdentifier UniqueIdentifier Not NULL Default NewID()", "RealEstate");
                    rsUpdate.Execute("Update PictureRecord set DocumentIdentifier = (SELECT CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER))" , "RealEstate");
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool ChangeWidowerCategories()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                rsSave.Execute("Update ExemptCode set Category = 44 where  Category = 40 or Category = 39", "RealEstate");
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
