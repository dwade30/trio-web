﻿namespace Global
{
	/// <summary>
	/// Summary description for rptCertificateOfAssessment.
	/// </summary>
	partial class rptCertificateOfAssessment
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCertificateOfAssessment));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.RichEdit1 = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.RichEdit2 = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCounty = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTaxCollector = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCountyTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMunicipalAppropriation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTIF = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSchoolAppropriation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOverlay = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAssessments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label30 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label31 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStateRevenue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHomestead = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherRevenue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalDeductions = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label32 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label33 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNetAssessment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageBreak1 = new GrapeCity.ActiveReports.SectionReportModel.PageBreak();
			this.RichEdit3 = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.RichEdit4 = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.RichEdit5 = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line9 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line10 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAssessorOf = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label34 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTaxCollector2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label36 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMunicipality2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label38 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.RichEdit6 = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.Label39 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line11 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line12 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line13 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line14 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line15 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label40 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label41 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageBreak2 = new GrapeCity.ActiveReports.SectionReportModel.PageBreak();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label45 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCounty2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label47 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label49 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.RichEdit7 = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.Label50 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label51 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label52 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label53 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label54 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label55 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label56 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label57 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label58 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label59 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label60 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label61 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label62 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label63 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtCountyTax2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMunicipalAppropriation2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTif2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtSchoolAppropriation2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOverlay2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalAssessments2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label64 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label65 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label66 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label67 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label68 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label69 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label70 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label71 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label72 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtStateRevenue2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtHomestead2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtOtherRevenue2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTotalDeductions2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label73 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label74 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNetAssessment2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.RichEdit8 = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
			this.Line16 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line17 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line18 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line19 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line20 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label75 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label76 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label77 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label78 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label79 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBETE = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label80 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label81 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtBETE2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCounty)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxCollector)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountyTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipalAppropriation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTIF)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSchoolAppropriation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverlay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateRevenue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHomestead)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherRevenue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDeductions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetAssessment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessorOf)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxCollector2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMunicipality2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCounty2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountyTax2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipalAppropriation2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTif2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSchoolAppropriation2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverlay2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessments2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label65)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateRevenue2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHomestead2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherRevenue2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDeductions2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetAssessment2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETE2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Field1,
				this.RichEdit1,
				this.RichEdit2,
				this.Line1,
				this.Line2,
				this.Line3,
				this.Line4,
				this.Line5,
				this.Label1,
				this.Field2,
				this.Label2,
				this.Label3,
				this.lblMuniName,
				this.Label5,
				this.lblCounty,
				this.Label6,
				this.lblTaxCollector,
				this.Label7,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Label18,
				this.Label19,
				this.Label20,
				this.Label21,
				this.Label22,
				this.txtCountyTax,
				this.txtMunicipalAppropriation,
				this.txtTIF,
				this.txtSchoolAppropriation,
				this.txtOverlay,
				this.txtTotalAssessments,
				this.Label23,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Label29,
				this.Label30,
				this.Label31,
				this.txtStateRevenue,
				this.txtHomestead,
				this.txtOtherRevenue,
				this.txtTotalDeductions,
				this.Label32,
				this.Label33,
				this.txtNetAssessment,
				this.PageBreak1,
				this.RichEdit3,
				this.RichEdit4,
				this.RichEdit5,
				this.Line6,
				this.Line7,
				this.Line8,
				this.Line9,
				this.Line10,
				this.lblAssessorOf,
				this.Field3,
				this.Label34,
				this.lblTaxCollector2,
				this.Label36,
				this.lblMunicipality2,
				this.Label38,
				this.RichEdit6,
				this.Label39,
				this.Line11,
				this.Line12,
				this.Line13,
				this.Line14,
				this.Line15,
				this.Label40,
				this.Label41,
				this.PageBreak2,
				this.Field4,
				this.Label45,
				this.txtCounty2,
				this.Label47,
				this.Label49,
				this.RichEdit7,
				this.Label50,
				this.Label51,
				this.Label52,
				this.Label53,
				this.Label54,
				this.Label55,
				this.Label56,
				this.Label57,
				this.Label58,
				this.Label59,
				this.Label60,
				this.Label61,
				this.Label62,
				this.Label63,
				this.txtCountyTax2,
				this.txtMunicipalAppropriation2,
				this.txtTif2,
				this.txtSchoolAppropriation2,
				this.txtOverlay2,
				this.txtTotalAssessments2,
				this.Label64,
				this.Label65,
				this.Label66,
				this.Label67,
				this.Label68,
				this.Label69,
				this.Label70,
				this.Label71,
				this.Label72,
				this.txtStateRevenue2,
				this.txtHomestead2,
				this.txtOtherRevenue2,
				this.txtTotalDeductions2,
				this.Label73,
				this.Label74,
				this.txtNetAssessment2,
				this.RichEdit8,
				this.Line16,
				this.Line17,
				this.Line18,
				this.Line19,
				this.Line20,
				this.Label75,
				this.Label76,
				this.Label77,
				this.Label78,
				this.Label79,
				this.txtBETE,
				this.Label80,
				this.Label81,
				this.txtBETE2
			});
			this.Detail.Height = 25.42708F;
			this.Detail.Name = "Detail";
			// 
			// Field1
			// 
			this.Field1.Height = 0.21875F;
            this.Field1.Left = 0.75F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Field1.Text = "ASSESSORS\' CERTIFICATION OF ASSESSMENT";
			this.Field1.Top = 0.0625F;
			this.Field1.Width = 5.5F;
			// 
			// RichEdit1
			// 
			//this.RichEdit1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.RichEdit1.Height = 0.6979167F;
			this.RichEdit1.Left = 0.03125F;
			this.RichEdit1.Name = "RichEdit1";
			this.RichEdit1.RTF = resources.GetString("RichEdit1.RTF");
			this.RichEdit1.Top = 0.3333333F;
			this.RichEdit1.Width = 6.833333F;
			// 
			// RichEdit2
			// 
			//this.RichEdit2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.RichEdit2.Height = 0.4479167F;
			this.RichEdit2.Left = 0F;
			this.RichEdit2.Name = "RichEdit2";
			this.RichEdit2.RTF = resources.GetString("RichEdit2.RTF");
			this.RichEdit2.Top = 1.125F;
			this.RichEdit2.Width = 6.833333F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.04166667F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.9375F;
			this.Line1.Width = 3.697917F;
			this.Line1.X1 = 0.04166667F;
			this.Line1.X2 = 3.739583F;
			this.Line1.Y1 = 1.9375F;
			this.Line1.Y2 = 1.9375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.0625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 2.1875F;
			this.Line2.Width = 3.697917F;
			this.Line2.X1 = 0.0625F;
			this.Line2.X2 = 3.760417F;
			this.Line2.Y1 = 2.1875F;
			this.Line2.Y2 = 2.1875F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 0.0625F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 2.447917F;
			this.Line3.Width = 3.697917F;
			this.Line3.X1 = 0.0625F;
			this.Line3.X2 = 3.760417F;
			this.Line3.Y1 = 2.447917F;
			this.Line3.Y2 = 2.447917F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 0.0625F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 2.697917F;
			this.Line4.Width = 3.697917F;
			this.Line4.X1 = 0.0625F;
			this.Line4.X2 = 3.760417F;
			this.Line4.Y1 = 2.697917F;
			this.Line4.Y2 = 2.697917F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 0.0625F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 2.947917F;
			this.Line5.Width = 3.697917F;
			this.Line5.X1 = 0.0625F;
			this.Line5.X2 = 3.760417F;
			this.Line5.Y1 = 2.947917F;
			this.Line5.Y2 = 2.947917F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.2083333F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 4.34375F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 12pt";
			this.Label1.Text = "Municipal Assessor(s)";
			this.Label1.Top = 1.729167F;
			this.Label1.Width = 1.927083F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.21875F;
            this.Field2.Left = 0.75F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Field2.Text = "MUNICIPAL TAX ASSESSMENT WARRANT";
			this.Field2.Top = 3.1875F;
			this.Field2.Width = 5.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.2395833F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.01041667F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "";
			this.Label2.Text = "State of Maine";
			this.Label2.Top = 3.4375F;
			this.Label2.Width = 1.395833F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.2395833F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.25F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "";
			this.Label3.Text = "Municipality";
			this.Label3.Top = 3.4375F;
			this.Label3.Width = 1.09375F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.2395833F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 2.375F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 3.4375F;
			this.lblMuniName.Width = 2.21875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.2395833F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "";
			this.Label5.Text = "County";
			this.Label5.Top = 3.4375F;
			this.Label5.Width = 0.59375F;
			// 
			// lblCounty
			// 
			this.lblCounty.Height = 0.2395833F;
			this.lblCounty.HyperLink = null;
			this.lblCounty.Left = 5.25F;
			this.lblCounty.Name = "lblCounty";
			this.lblCounty.Style = "";
			this.lblCounty.Text = null;
			this.lblCounty.Top = 3.4375F;
			this.lblCounty.Width = 1.65625F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.2395833F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 0.01041667F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "";
			this.Label6.Text = "To";
			this.Label6.Top = 3.677083F;
			this.Label6.Width = 0.3333333F;
			// 
			// lblTaxCollector
			// 
			this.lblTaxCollector.Height = 0.2395833F;
			this.lblTaxCollector.HyperLink = null;
			this.lblTaxCollector.Left = 0.3854167F;
			this.lblTaxCollector.Name = "lblTaxCollector";
			this.lblTaxCollector.Style = "";
			this.lblTaxCollector.Text = null;
			this.lblTaxCollector.Top = 3.677083F;
			this.lblTaxCollector.Width = 2.645833F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.2395833F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 3.125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "";
			this.Label7.Text = ", Tax Collector";
			this.Label7.Top = 3.677083F;
			this.Label7.Width = 2.458333F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.6770833F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.01041667F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "";
			this.Label8.Text = "In the name of the State of Maine you are hereby required to collect of each pers" + "on named in the list herewith committed to you the amount set down on said list " + "as payable by that person.";
			this.Label8.Top = 3.979167F;
			this.Label8.Width = 6.895833F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1770833F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.02083333F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-size: 10pt; font-weight: bold; text-decoration: underline";
			this.Label9.Text = "Assessments:";
			this.Label9.Top = 4.729167F;
			this.Label9.Width = 1.53125F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 0.3541667F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-size: 10pt";
			this.Label10.Text = "County Tax";
			this.Label10.Top = 5.010417F;
			this.Label10.Width = 1.822917F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 0.02083333F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-size: 10pt; text-align: right";
			this.Label11.Text = "1.";
			this.Label11.Top = 5.010417F;
			this.Label11.Width = 0.2604167F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 0.3541667F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-size: 10pt";
			this.Label12.Text = "Municipal Appropriation";
			this.Label12.Top = 5.260417F;
			this.Label12.Width = 1.822917F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 0.02083333F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-size: 10pt; text-align: right";
			this.Label13.Text = "2.";
			this.Label13.Top = 5.260417F;
			this.Label13.Width = 0.2604167F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 0.3541667F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-size: 10pt";
			this.Label14.Text = "TIF Financing Plan Amount";
			this.Label14.Top = 5.510417F;
			this.Label14.Width = 1.822917F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 0.02083333F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-size: 10pt; text-align: right";
			this.Label15.Text = "3.";
			this.Label15.Top = 5.510417F;
			this.Label15.Width = 0.2604167F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.3541667F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-size: 10pt";
			this.Label16.Text = "Local Educational Appropriation";
			this.Label16.Top = 5.760417F;
			this.Label16.Width = 2.260417F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 0.02083333F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-size: 10pt; text-align: right";
			this.Label17.Text = "4.";
			this.Label17.Top = 5.760417F;
			this.Label17.Width = 0.2604167F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 0.3541667F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-size: 10pt";
			this.Label18.Text = "Overlay (Not to Exceed 5% of \"Net To Be";
			this.Label18.Top = 6.010417F;
			this.Label18.Width = 2.9375F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.1875F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 0.02083333F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-size: 10pt; text-align: right";
			this.Label19.Text = "5.";
			this.Label19.Top = 6.010417F;
			this.Label19.Width = 0.2604167F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.3541667F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-size: 10pt; font-weight: bold";
			this.Label20.Text = "Total Assessments";
			this.Label20.Top = 6.447917F;
			this.Label20.Width = 1.822917F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 0.02083333F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-size: 10pt; text-align: right";
			this.Label21.Text = "6.";
			this.Label21.Top = 6.447917F;
			this.Label21.Width = 0.2604167F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 0.3541667F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-size: 10pt";
			this.Label22.Text = "Raised\" (see tax rate calculation #16)";
			this.Label22.Top = 6.197917F;
			this.Label22.Width = 2.447917F;
			// 
			// txtCountyTax
			// 
			this.txtCountyTax.Height = 0.19F;
			this.txtCountyTax.Left = 3.46875F;
			this.txtCountyTax.Name = "txtCountyTax";
			this.txtCountyTax.Style = "font-size: 10pt; text-align: right";
			this.txtCountyTax.Text = "Field3";
			this.txtCountyTax.Top = 5.020833F;
			this.txtCountyTax.Width = 1.40625F;
			// 
			// txtMunicipalAppropriation
			// 
			this.txtMunicipalAppropriation.Height = 0.19F;
			this.txtMunicipalAppropriation.Left = 3.46875F;
			this.txtMunicipalAppropriation.Name = "txtMunicipalAppropriation";
			this.txtMunicipalAppropriation.Style = "font-size: 10pt; text-align: right";
			this.txtMunicipalAppropriation.Text = "Field3";
			this.txtMunicipalAppropriation.Top = 5.270833F;
			this.txtMunicipalAppropriation.Width = 1.40625F;
			// 
			// txtTIF
			// 
			this.txtTIF.Height = 0.19F;
			this.txtTIF.Left = 3.46875F;
			this.txtTIF.Name = "txtTIF";
			this.txtTIF.Style = "font-size: 10pt; text-align: right";
			this.txtTIF.Text = "Field3";
			this.txtTIF.Top = 5.520833F;
			this.txtTIF.Width = 1.40625F;
			// 
			// txtSchoolAppropriation
			// 
			this.txtSchoolAppropriation.Height = 0.19F;
			this.txtSchoolAppropriation.Left = 3.46875F;
			this.txtSchoolAppropriation.Name = "txtSchoolAppropriation";
			this.txtSchoolAppropriation.Style = "font-size: 10pt; text-align: right";
			this.txtSchoolAppropriation.Text = "Field3";
			this.txtSchoolAppropriation.Top = 5.770833F;
			this.txtSchoolAppropriation.Width = 1.40625F;
			// 
			// txtOverlay
			// 
			this.txtOverlay.Height = 0.19F;
			this.txtOverlay.Left = 3.46875F;
			this.txtOverlay.Name = "txtOverlay";
			this.txtOverlay.Style = "font-size: 10pt; text-align: right";
			this.txtOverlay.Text = "Field3";
			this.txtOverlay.Top = 6.208333F;
			this.txtOverlay.Width = 1.40625F;
			// 
			// txtTotalAssessments
			// 
			this.txtTotalAssessments.Height = 0.19F;
			this.txtTotalAssessments.Left = 5.03125F;
			this.txtTotalAssessments.Name = "txtTotalAssessments";
			this.txtTotalAssessments.Style = "font-size: 10pt; text-align: right";
			this.txtTotalAssessments.Text = "Field3";
			this.txtTotalAssessments.Top = 6.458333F;
			this.txtTotalAssessments.Width = 1.40625F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1770833F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 0.02083333F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-size: 10pt; font-weight: bold; text-decoration: underline";
			this.Label23.Text = "Deductions:";
			this.Label23.Top = 6.854167F;
			this.Label23.Width = 1.53125F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 0.3541667F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-size: 10pt";
			this.Label24.Text = "State Municipal Revenue Sharing";
			this.Label24.Top = 7.135417F;
			this.Label24.Width = 2.510417F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 0.02083333F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-size: 10pt; text-align: right";
			this.Label25.Text = "7.";
			this.Label25.Top = 7.135417F;
			this.Label25.Width = 0.2604167F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 0.3541667F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-size: 10pt";
			this.Label26.Text = "Homestead Reimbursement";
			this.Label26.Top = 7.385417F;
			this.Label26.Width = 1.947917F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 0.02083333F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-size: 10pt; text-align: right";
			this.Label27.Text = "8.";
			this.Label27.Top = 7.385417F;
			this.Label27.Width = 0.2604167F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 0.3541667F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-size: 10pt";
			this.Label28.Text = "Other Revenue";
			this.Label28.Top = 7.885417F;
			this.Label28.Width = 1.822917F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 0.02083333F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-size: 10pt; text-align: right";
			this.Label29.Text = "10.";
			this.Label29.Top = 7.885417F;
			this.Label29.Width = 0.2604167F;
			// 
			// Label30
			// 
			this.Label30.Height = 0.1875F;
			this.Label30.HyperLink = null;
			this.Label30.Left = 0.3541667F;
			this.Label30.Name = "Label30";
			this.Label30.Style = "font-size: 10pt; font-weight: bold";
			this.Label30.Text = "Total Deductions";
			this.Label30.Top = 8.135417F;
			this.Label30.Width = 2.260417F;
			// 
			// Label31
			// 
			this.Label31.Height = 0.1875F;
			this.Label31.HyperLink = null;
			this.Label31.Left = 0.02083333F;
			this.Label31.Name = "Label31";
			this.Label31.Style = "font-size: 10pt; text-align: right";
			this.Label31.Text = "11.";
			this.Label31.Top = 8.135417F;
			this.Label31.Width = 0.2604167F;
			// 
			// txtStateRevenue
			// 
			this.txtStateRevenue.Height = 0.19F;
			this.txtStateRevenue.Left = 3.46875F;
			this.txtStateRevenue.Name = "txtStateRevenue";
			this.txtStateRevenue.Style = "font-size: 10pt; text-align: right";
			this.txtStateRevenue.Text = "Field3";
			this.txtStateRevenue.Top = 7.145833F;
			this.txtStateRevenue.Width = 1.40625F;
			// 
			// txtHomestead
			// 
			this.txtHomestead.Height = 0.19F;
			this.txtHomestead.Left = 3.46875F;
			this.txtHomestead.Name = "txtHomestead";
			this.txtHomestead.Style = "font-size: 10pt; text-align: right";
			this.txtHomestead.Text = "Field3";
			this.txtHomestead.Top = 7.395833F;
			this.txtHomestead.Width = 1.40625F;
			// 
			// txtOtherRevenue
			// 
			this.txtOtherRevenue.Height = 0.19F;
			this.txtOtherRevenue.Left = 3.46875F;
			this.txtOtherRevenue.Name = "txtOtherRevenue";
			this.txtOtherRevenue.Style = "font-size: 10pt; text-align: right";
			this.txtOtherRevenue.Text = "Field3";
			this.txtOtherRevenue.Top = 7.895833F;
			this.txtOtherRevenue.Width = 1.40625F;
			// 
			// txtTotalDeductions
			// 
			this.txtTotalDeductions.Height = 0.19F;
			this.txtTotalDeductions.Left = 5.03125F;
			this.txtTotalDeductions.Name = "txtTotalDeductions";
			this.txtTotalDeductions.Style = "font-size: 10pt; text-align: right";
			this.txtTotalDeductions.Text = "Field3";
			this.txtTotalDeductions.Top = 8.145833F;
			this.txtTotalDeductions.Width = 1.40625F;
			// 
			// Label32
			// 
			this.Label32.Height = 0.1875F;
			this.Label32.HyperLink = null;
			this.Label32.Left = 0.3541667F;
			this.Label32.Name = "Label32";
			this.Label32.Style = "font-size: 10pt; font-weight: bold; text-decoration: underline";
			this.Label32.Text = "Net Assessment for Commitment";
			this.Label32.Top = 8.385417F;
			this.Label32.Width = 2.635417F;
			// 
			// Label33
			// 
			this.Label33.Height = 0.1875F;
			this.Label33.HyperLink = null;
			this.Label33.Left = 0.02083333F;
			this.Label33.Name = "Label33";
			this.Label33.Style = "font-size: 10pt; text-align: right";
			this.Label33.Text = "12.";
			this.Label33.Top = 8.385417F;
			this.Label33.Width = 0.2604167F;
			// 
			// txtNetAssessment
			// 
			this.txtNetAssessment.Height = 0.19F;
			this.txtNetAssessment.Left = 5.03125F;
			this.txtNetAssessment.Name = "txtNetAssessment";
			this.txtNetAssessment.Style = "font-size: 10pt; text-align: right";
			this.txtNetAssessment.Text = "Field3";
			this.txtNetAssessment.Top = 8.395833F;
			this.txtNetAssessment.Width = 1.40625F;
			// 
			// PageBreak1
			// 
			this.PageBreak1.Height = 0.05555556F;
			this.PageBreak1.Left = 0F;
			this.PageBreak1.Name = "PageBreak1";
			this.PageBreak1.Size = new System.Drawing.SizeF(6.96875F, 0.05555556F);
			this.PageBreak1.Top = 8.604167F;
			this.PageBreak1.Width = 6.96875F;
			// 
			// RichEdit3
			// 
			//this.RichEdit3.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.RichEdit3.Height = 0.6979167F;
			this.RichEdit3.Left = 0F;
			this.RichEdit3.Name = "RichEdit3";
			this.RichEdit3.RTF = resources.GetString("RichEdit3.RTF");
			this.RichEdit3.Top = 8.75F;
			this.RichEdit3.Width = 6.833333F;
			// 
			// RichEdit4
			// 
			//this.RichEdit4.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.RichEdit4.Height = 0.8229167F;
			this.RichEdit4.Left = 0F;
			this.RichEdit4.Name = "RichEdit4";
			this.RichEdit4.RTF = resources.GetString("RichEdit4.RTF");
			this.RichEdit4.Top = 9.635417F;
			this.RichEdit4.Width = 6.833333F;
			// 
			// RichEdit5
			// 
			//this.RichEdit5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.RichEdit5.Height = 0.4479167F;
			this.RichEdit5.Left = 0F;
			this.RichEdit5.Name = "RichEdit5";
			this.RichEdit5.RTF = resources.GetString("RichEdit5.RTF");
			this.RichEdit5.Top = 10.64583F;
			this.RichEdit5.Width = 6.833333F;
			// 
			// Line6
			// 
			this.Line6.Height = 0F;
			this.Line6.Left = 0.04166667F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 11.4375F;
			this.Line6.Width = 3.697917F;
			this.Line6.X1 = 0.04166667F;
			this.Line6.X2 = 3.739583F;
			this.Line6.Y1 = 11.4375F;
			this.Line6.Y2 = 11.4375F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 0.0625F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 11.6875F;
			this.Line7.Width = 3.697917F;
			this.Line7.X1 = 0.0625F;
			this.Line7.X2 = 3.760417F;
			this.Line7.Y1 = 11.6875F;
			this.Line7.Y2 = 11.6875F;
			// 
			// Line8
			// 
			this.Line8.Height = 0F;
			this.Line8.Left = 0.0625F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 11.94792F;
			this.Line8.Width = 3.697917F;
			this.Line8.X1 = 0.0625F;
			this.Line8.X2 = 3.760417F;
			this.Line8.Y1 = 11.94792F;
			this.Line8.Y2 = 11.94792F;
			// 
			// Line9
			// 
			this.Line9.Height = 0F;
			this.Line9.Left = 0.0625F;
			this.Line9.LineWeight = 1F;
			this.Line9.Name = "Line9";
			this.Line9.Top = 12.19792F;
			this.Line9.Width = 3.697917F;
			this.Line9.X1 = 0.0625F;
			this.Line9.X2 = 3.760417F;
			this.Line9.Y1 = 12.19792F;
			this.Line9.Y2 = 12.19792F;
			// 
			// Line10
			// 
			this.Line10.Height = 0F;
			this.Line10.Left = 0.0625F;
			this.Line10.LineWeight = 1F;
			this.Line10.Name = "Line10";
			this.Line10.Top = 12.44792F;
			this.Line10.Width = 3.697917F;
			this.Line10.X1 = 0.0625F;
			this.Line10.X2 = 3.760417F;
			this.Line10.Y1 = 12.44792F;
			this.Line10.Y2 = 12.44792F;
			// 
			// lblAssessorOf
			// 
			this.lblAssessorOf.Height = 0.2083333F;
			this.lblAssessorOf.HyperLink = null;
			this.lblAssessorOf.Left = 3.78125F;
			this.lblAssessorOf.Name = "lblAssessorOf";
			this.lblAssessorOf.Style = "font-size: 12pt";
			this.lblAssessorOf.Text = "Assessors(s) of:";
			this.lblAssessorOf.Top = 11.22917F;
			this.lblAssessorOf.Width = 3.114583F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.21875F;
            this.Field3.Left = 0.75F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Field3.Text = "CERTIFICATE OF COMMITMENT";
			this.Field3.Top = 12.8125F;
			this.Field3.Width = 5.5F;
			// 
			// Label34
			// 
			this.Label34.Height = 0.2395833F;
			this.Label34.HyperLink = null;
			this.Label34.Left = 0.01041667F;
			this.Label34.Name = "Label34";
			this.Label34.Style = "";
			this.Label34.Text = "To";
			this.Label34.Top = 13.11458F;
			this.Label34.Width = 0.3333333F;
			// 
			// lblTaxCollector2
			// 
			this.lblTaxCollector2.Height = 0.2395833F;
			this.lblTaxCollector2.HyperLink = null;
			this.lblTaxCollector2.Left = 0.3854167F;
			this.lblTaxCollector2.Name = "lblTaxCollector2";
			this.lblTaxCollector2.Style = "";
			this.lblTaxCollector2.Text = null;
			this.lblTaxCollector2.Top = 13.11458F;
			this.lblTaxCollector2.Width = 2.645833F;
			// 
			// Label36
			// 
			this.Label36.Height = 0.2395833F;
			this.Label36.HyperLink = null;
			this.Label36.Left = 3.125F;
			this.Label36.Name = "Label36";
			this.Label36.Style = "";
			this.Label36.Text = "the Collector of the Municipality of";
			this.Label36.Top = 13.11458F;
			this.Label36.Width = 2.833333F;
			// 
			// lblMunicipality2
			// 
			this.lblMunicipality2.Height = 0.2395833F;
			this.lblMunicipality2.HyperLink = null;
			this.lblMunicipality2.Left = 0.01041667F;
			this.lblMunicipality2.Name = "lblMunicipality2";
			this.lblMunicipality2.Style = "";
			this.lblMunicipality2.Text = null;
			this.lblMunicipality2.Top = 13.36458F;
			this.lblMunicipality2.Width = 2.645833F;
			// 
			// Label38
			// 
			this.Label38.Height = 0.2395833F;
			this.Label38.HyperLink = null;
			this.Label38.Left = 2.75F;
			this.Label38.Name = "Label38";
			this.Label38.Style = "";
			this.Label38.Text = ", aforesaid.";
			this.Label38.Top = 13.36458F;
			this.Label38.Width = 2.833333F;
			// 
			// RichEdit6
			// 
			//this.RichEdit6.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.RichEdit6.Height = 0.4479167F;
			this.RichEdit6.Left = 0F;
			this.RichEdit6.Name = "RichEdit6";
			this.RichEdit6.RTF = resources.GetString("RichEdit6.RTF");
			this.RichEdit6.Top = 13.8125F;
			this.RichEdit6.Width = 6.833333F;
			// 
			// Label39
			// 
			this.Label39.Height = 0.2395833F;
			this.Label39.HyperLink = null;
			this.Label39.Left = 0F;
			this.Label39.Name = "Label39";
			this.Label39.Style = "";
			this.Label39.Text = "Given under our hands this";
			this.Label39.Top = 14.44792F;
			this.Label39.Width = 6.895833F;
			// 
			// Line11
			// 
			this.Line11.Height = 0F;
			this.Line11.Left = 0.04166667F;
			this.Line11.LineWeight = 1F;
			this.Line11.Name = "Line11";
			this.Line11.Top = 15.0625F;
			this.Line11.Width = 3.697917F;
			this.Line11.X1 = 0.04166667F;
			this.Line11.X2 = 3.739583F;
			this.Line11.Y1 = 15.0625F;
			this.Line11.Y2 = 15.0625F;
			// 
			// Line12
			// 
			this.Line12.Height = 0F;
			this.Line12.Left = 0.0625F;
			this.Line12.LineWeight = 1F;
			this.Line12.Name = "Line12";
			this.Line12.Top = 15.3125F;
			this.Line12.Width = 3.697917F;
			this.Line12.X1 = 0.0625F;
			this.Line12.X2 = 3.760417F;
			this.Line12.Y1 = 15.3125F;
			this.Line12.Y2 = 15.3125F;
			// 
			// Line13
			// 
			this.Line13.Height = 0F;
			this.Line13.Left = 0.0625F;
			this.Line13.LineWeight = 1F;
			this.Line13.Name = "Line13";
			this.Line13.Top = 15.57292F;
			this.Line13.Width = 3.697917F;
			this.Line13.X1 = 0.0625F;
			this.Line13.X2 = 3.760417F;
			this.Line13.Y1 = 15.57292F;
			this.Line13.Y2 = 15.57292F;
			// 
			// Line14
			// 
			this.Line14.Height = 0F;
			this.Line14.Left = 0.0625F;
			this.Line14.LineWeight = 1F;
			this.Line14.Name = "Line14";
			this.Line14.Top = 15.82292F;
			this.Line14.Width = 3.697917F;
			this.Line14.X1 = 0.0625F;
			this.Line14.X2 = 3.760417F;
			this.Line14.Y1 = 15.82292F;
			this.Line14.Y2 = 15.82292F;
			// 
			// Line15
			// 
			this.Line15.Height = 0F;
			this.Line15.Left = 0.0625F;
			this.Line15.LineWeight = 1F;
			this.Line15.Name = "Line15";
			this.Line15.Top = 16.07292F;
			this.Line15.Width = 3.697917F;
			this.Line15.X1 = 0.0625F;
			this.Line15.X2 = 3.760417F;
			this.Line15.Y1 = 16.07292F;
			this.Line15.Y2 = 16.07292F;
			// 
			// Label40
			// 
			this.Label40.Height = 0.2083333F;
			this.Label40.HyperLink = null;
			this.Label40.Left = 3.78125F;
			this.Label40.Name = "Label40";
			this.Label40.Style = "font-size: 12pt";
			this.Label40.Text = "Assessors(s) of:";
			this.Label40.Top = 14.85417F;
			this.Label40.Width = 3.114583F;
			// 
			// Label41
			// 
			this.Label41.Height = 0.2395833F;
			this.Label41.HyperLink = null;
			this.Label41.Left = 0F;
			this.Label41.Name = "Label41";
			this.Label41.Style = "";
			this.Label41.Text = "Complete in Duplicate. File original with Tax Collector. File copy in Valuation B" + "ook";
			this.Label41.Top = 16.3125F;
			this.Label41.Width = 6.895833F;
			// 
			// PageBreak2
			// 
			this.PageBreak2.Height = 0.05555556F;
			this.PageBreak2.Left = 0F;
			this.PageBreak2.Name = "PageBreak2";
			this.PageBreak2.Size = new System.Drawing.SizeF(6.96875F, 0.05555556F);
			this.PageBreak2.Top = 16.69792F;
			this.PageBreak2.Width = 6.96875F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 0.75F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-size: 10pt; font-weight: bold; text-align: center";
			this.Field4.Text = "CERTIFICATE OF ASSESSMENT TO BE RETURNED TO MUNICIPAL TREASURER";
			this.Field4.Top = 16.9375F;
			this.Field4.Width = 5.5F;
			// 
			// Label45
			// 
			this.Label45.Height = 0.2395833F;
			this.Label45.HyperLink = null;
			this.Label45.Left = 0F;
			this.Label45.Name = "Label45";
			this.Label45.Style = "font-size: 10pt";
			this.Label45.Text = "County";
			this.Label45.Top = 17.375F;
			this.Label45.Width = 0.59375F;
			// 
			// txtCounty2
			// 
			this.txtCounty2.Height = 0.2395833F;
			this.txtCounty2.HyperLink = null;
			this.txtCounty2.Left = 0.625F;
			this.txtCounty2.Name = "txtCounty2";
			this.txtCounty2.Style = "font-size: 10pt";
			this.txtCounty2.Text = null;
			this.txtCounty2.Top = 17.375F;
			this.txtCounty2.Width = 1.65625F;
			// 
			// Label47
			// 
			this.Label47.Height = 0.2395833F;
			this.Label47.HyperLink = null;
			this.Label47.Left = 2.760417F;
			this.Label47.Name = "Label47";
			this.Label47.Style = "font-size: 10pt; text-align: center";
			this.Label47.Text = "STATE OF MAINE";
			this.Label47.Top = 17.11458F;
			this.Label47.Width = 1.520833F;
			// 
			// Label49
			// 
			this.Label49.Height = 0.2395833F;
			this.Label49.HyperLink = null;
			this.Label49.Left = 2.1875F;
			this.Label49.Name = "Label49";
			this.Label49.Style = "font-size: 10pt";
			this.Label49.Text = ", ss.";
			this.Label49.Top = 17.375F;
			this.Label49.Width = 0.4583333F;
			// 
			// RichEdit7
			// 
			//this.RichEdit7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.RichEdit7.Height = 0.5729167F;
			this.RichEdit7.Left = 0F;
			this.RichEdit7.Name = "RichEdit7";
			this.RichEdit7.RTF = resources.GetString("RichEdit7.RTF");
			this.RichEdit7.Top = 17.8125F;
			this.RichEdit7.Width = 6.833333F;
			// 
			// Label50
			// 
			this.Label50.Height = 0.1770833F;
			this.Label50.HyperLink = null;
			this.Label50.Left = 0.02083333F;
			this.Label50.Name = "Label50";
			this.Label50.Style = "font-size: 10pt; font-weight: bold; text-decoration: underline";
			this.Label50.Text = "Assessments:";
			this.Label50.Top = 18.54167F;
			this.Label50.Width = 1.53125F;
			// 
			// Label51
			// 
			this.Label51.Height = 0.1875F;
			this.Label51.HyperLink = null;
			this.Label51.Left = 0.3541667F;
			this.Label51.Name = "Label51";
			this.Label51.Style = "font-size: 10pt";
			this.Label51.Text = "County Tax";
			this.Label51.Top = 18.82292F;
			this.Label51.Width = 1.822917F;
			// 
			// Label52
			// 
			this.Label52.Height = 0.1875F;
			this.Label52.HyperLink = null;
			this.Label52.Left = 0.02083333F;
			this.Label52.Name = "Label52";
			this.Label52.Style = "font-size: 10pt; text-align: right";
			this.Label52.Text = "1.";
			this.Label52.Top = 18.82292F;
			this.Label52.Width = 0.2604167F;
			// 
			// Label53
			// 
			this.Label53.Height = 0.1875F;
			this.Label53.HyperLink = null;
			this.Label53.Left = 0.3541667F;
			this.Label53.Name = "Label53";
			this.Label53.Style = "font-size: 10pt";
			this.Label53.Text = "Municipal Appropriation";
			this.Label53.Top = 19.07292F;
			this.Label53.Width = 1.822917F;
			// 
			// Label54
			// 
			this.Label54.Height = 0.1875F;
			this.Label54.HyperLink = null;
			this.Label54.Left = 0.02083333F;
			this.Label54.Name = "Label54";
			this.Label54.Style = "font-size: 10pt; text-align: right";
			this.Label54.Text = "2.";
			this.Label54.Top = 19.07292F;
			this.Label54.Width = 0.2604167F;
			// 
			// Label55
			// 
			this.Label55.Height = 0.1875F;
			this.Label55.HyperLink = null;
			this.Label55.Left = 0.3541667F;
			this.Label55.Name = "Label55";
			this.Label55.Style = "font-size: 10pt";
			this.Label55.Text = "TIF Financing Plan Amount";
			this.Label55.Top = 19.32292F;
			this.Label55.Width = 1.822917F;
			// 
			// Label56
			// 
			this.Label56.Height = 0.1875F;
			this.Label56.HyperLink = null;
			this.Label56.Left = 0.02083333F;
			this.Label56.Name = "Label56";
			this.Label56.Style = "font-size: 10pt; text-align: right";
			this.Label56.Text = "3.";
			this.Label56.Top = 19.32292F;
			this.Label56.Width = 0.2604167F;
			// 
			// Label57
			// 
			this.Label57.Height = 0.1875F;
			this.Label57.HyperLink = null;
			this.Label57.Left = 0.3541667F;
			this.Label57.Name = "Label57";
			this.Label57.Style = "font-size: 10pt";
			this.Label57.Text = "Local Educational Appropriation";
			this.Label57.Top = 19.57292F;
			this.Label57.Width = 2.260417F;
			// 
			// Label58
			// 
			this.Label58.Height = 0.1875F;
			this.Label58.HyperLink = null;
			this.Label58.Left = 0.02083333F;
			this.Label58.Name = "Label58";
			this.Label58.Style = "font-size: 10pt; text-align: right";
			this.Label58.Text = "4.";
			this.Label58.Top = 19.57292F;
			this.Label58.Width = 0.2604167F;
			// 
			// Label59
			// 
			this.Label59.Height = 0.1875F;
			this.Label59.HyperLink = null;
			this.Label59.Left = 0.3541667F;
			this.Label59.Name = "Label59";
			this.Label59.Style = "font-size: 10pt";
			this.Label59.Text = "Overlay (Not to Exceed 5% of \"Net To Be";
			this.Label59.Top = 19.82292F;
			this.Label59.Width = 2.625F;
			// 
			// Label60
			// 
			this.Label60.Height = 0.1875F;
			this.Label60.HyperLink = null;
			this.Label60.Left = 0.02083333F;
			this.Label60.Name = "Label60";
			this.Label60.Style = "font-size: 10pt; text-align: right";
			this.Label60.Text = "5.";
			this.Label60.Top = 19.82292F;
			this.Label60.Width = 0.2604167F;
			// 
			// Label61
			// 
			this.Label61.Height = 0.1875F;
			this.Label61.HyperLink = null;
			this.Label61.Left = 0.3541667F;
			this.Label61.Name = "Label61";
			this.Label61.Style = "font-size: 10pt; font-weight: bold";
			this.Label61.Text = "Total Assessments";
			this.Label61.Top = 20.26042F;
			this.Label61.Width = 1.822917F;
			// 
			// Label62
			// 
			this.Label62.Height = 0.1875F;
			this.Label62.HyperLink = null;
			this.Label62.Left = 0.02083333F;
			this.Label62.Name = "Label62";
			this.Label62.Style = "font-size: 10pt; text-align: right";
			this.Label62.Text = "6.";
			this.Label62.Top = 20.26042F;
			this.Label62.Width = 0.2604167F;
			// 
			// Label63
			// 
			this.Label63.Height = 0.1875F;
			this.Label63.HyperLink = null;
			this.Label63.Left = 0.3541667F;
			this.Label63.Name = "Label63";
			this.Label63.Style = "font-size: 10pt";
			this.Label63.Text = "Raised\" (see tax rate calculation #16)";
			this.Label63.Top = 20.01042F;
			this.Label63.Width = 2.635417F;
			// 
			// txtCountyTax2
			// 
			this.txtCountyTax2.Height = 0.19F;
			this.txtCountyTax2.Left = 3.46875F;
			this.txtCountyTax2.Name = "txtCountyTax2";
			this.txtCountyTax2.Style = "font-size: 10pt; text-align: right";
			this.txtCountyTax2.Text = "Field3";
			this.txtCountyTax2.Top = 18.83333F;
			this.txtCountyTax2.Width = 1.40625F;
			// 
			// txtMunicipalAppropriation2
			// 
			this.txtMunicipalAppropriation2.Height = 0.19F;
			this.txtMunicipalAppropriation2.Left = 3.46875F;
			this.txtMunicipalAppropriation2.Name = "txtMunicipalAppropriation2";
			this.txtMunicipalAppropriation2.Style = "font-size: 10pt; text-align: right";
			this.txtMunicipalAppropriation2.Text = "Field3";
			this.txtMunicipalAppropriation2.Top = 19.08333F;
			this.txtMunicipalAppropriation2.Width = 1.40625F;
			// 
			// txtTif2
			// 
			this.txtTif2.Height = 0.19F;
			this.txtTif2.Left = 3.46875F;
			this.txtTif2.Name = "txtTif2";
			this.txtTif2.Style = "font-size: 10pt; text-align: right";
			this.txtTif2.Text = "Field3";
			this.txtTif2.Top = 19.33333F;
			this.txtTif2.Width = 1.40625F;
			// 
			// txtSchoolAppropriation2
			// 
			this.txtSchoolAppropriation2.Height = 0.19F;
			this.txtSchoolAppropriation2.Left = 3.46875F;
			this.txtSchoolAppropriation2.Name = "txtSchoolAppropriation2";
			this.txtSchoolAppropriation2.Style = "font-size: 10pt; text-align: right";
			this.txtSchoolAppropriation2.Text = "Field3";
			this.txtSchoolAppropriation2.Top = 19.58333F;
			this.txtSchoolAppropriation2.Width = 1.40625F;
			// 
			// txtOverlay2
			// 
			this.txtOverlay2.Height = 0.19F;
			this.txtOverlay2.Left = 3.46875F;
			this.txtOverlay2.Name = "txtOverlay2";
			this.txtOverlay2.Style = "font-size: 10pt; text-align: right";
			this.txtOverlay2.Text = "Field3";
			this.txtOverlay2.Top = 20.02083F;
			this.txtOverlay2.Width = 1.40625F;
			// 
			// txtTotalAssessments2
			// 
			this.txtTotalAssessments2.Height = 0.19F;
			this.txtTotalAssessments2.Left = 4.96875F;
			this.txtTotalAssessments2.Name = "txtTotalAssessments2";
			this.txtTotalAssessments2.Style = "font-size: 10pt; text-align: right";
			this.txtTotalAssessments2.Text = "Field3";
			this.txtTotalAssessments2.Top = 20.27083F;
			this.txtTotalAssessments2.Width = 1.40625F;
			// 
			// Label64
			// 
			this.Label64.Height = 0.1770833F;
			this.Label64.HyperLink = null;
			this.Label64.Left = 0.02083333F;
			this.Label64.Name = "Label64";
			this.Label64.Style = "font-size: 10pt; font-weight: bold; text-decoration: underline";
			this.Label64.Text = "Deductions:";
			this.Label64.Top = 20.60417F;
			this.Label64.Width = 1.53125F;
			// 
			// Label65
			// 
			this.Label65.Height = 0.1875F;
			this.Label65.HyperLink = null;
			this.Label65.Left = 0.3541667F;
			this.Label65.Name = "Label65";
			this.Label65.Style = "font-size: 10pt";
			this.Label65.Text = "State Municipal Revenue Sharing";
			this.Label65.Top = 20.88542F;
			this.Label65.Width = 2.510417F;
			// 
			// Label66
			// 
			this.Label66.Height = 0.1875F;
			this.Label66.HyperLink = null;
			this.Label66.Left = 0.02083333F;
			this.Label66.Name = "Label66";
			this.Label66.Style = "font-size: 10pt; text-align: right";
			this.Label66.Text = "7.";
			this.Label66.Top = 20.88542F;
			this.Label66.Width = 0.2604167F;
			// 
			// Label67
			// 
			this.Label67.Height = 0.1875F;
			this.Label67.HyperLink = null;
			this.Label67.Left = 0.3541667F;
			this.Label67.Name = "Label67";
			this.Label67.Style = "font-size: 10pt";
			this.Label67.Text = "Homestead Reimbursement";
			this.Label67.Top = 21.13542F;
			this.Label67.Width = 1.947917F;
			// 
			// Label68
			// 
			this.Label68.Height = 0.1875F;
			this.Label68.HyperLink = null;
			this.Label68.Left = 0.02083333F;
			this.Label68.Name = "Label68";
			this.Label68.Style = "font-size: 10pt; text-align: right";
			this.Label68.Text = "8.";
			this.Label68.Top = 21.13542F;
			this.Label68.Width = 0.2604167F;
			// 
			// Label69
			// 
			this.Label69.Height = 0.1875F;
			this.Label69.HyperLink = null;
			this.Label69.Left = 0.3541667F;
			this.Label69.Name = "Label69";
			this.Label69.Style = "font-size: 10pt";
			this.Label69.Text = "Other Revenue";
			this.Label69.Top = 21.63542F;
			this.Label69.Width = 1.822917F;
			// 
			// Label70
			// 
			this.Label70.Height = 0.1875F;
			this.Label70.HyperLink = null;
			this.Label70.Left = 0.02083333F;
			this.Label70.Name = "Label70";
			this.Label70.Style = "font-size: 10pt; text-align: right";
			this.Label70.Text = "10.";
			this.Label70.Top = 21.63542F;
			this.Label70.Width = 0.2604167F;
			// 
			// Label71
			// 
			this.Label71.Height = 0.1875F;
			this.Label71.HyperLink = null;
			this.Label71.Left = 0.3541667F;
			this.Label71.Name = "Label71";
			this.Label71.Style = "font-size: 10pt; font-weight: bold";
			this.Label71.Text = "Total Deductions";
			this.Label71.Top = 21.88542F;
			this.Label71.Width = 2.260417F;
			// 
			// Label72
			// 
			this.Label72.Height = 0.1875F;
			this.Label72.HyperLink = null;
			this.Label72.Left = 0.02083333F;
			this.Label72.Name = "Label72";
			this.Label72.Style = "font-size: 10pt; text-align: right";
			this.Label72.Text = "11.";
			this.Label72.Top = 21.88542F;
			this.Label72.Width = 0.2604167F;
			// 
			// txtStateRevenue2
			// 
			this.txtStateRevenue2.Height = 0.19F;
			this.txtStateRevenue2.Left = 3.46875F;
			this.txtStateRevenue2.Name = "txtStateRevenue2";
			this.txtStateRevenue2.Style = "font-size: 10pt; text-align: right";
			this.txtStateRevenue2.Text = "Field3";
			this.txtStateRevenue2.Top = 20.89583F;
			this.txtStateRevenue2.Width = 1.40625F;
			// 
			// txtHomestead2
			// 
			this.txtHomestead2.Height = 0.19F;
			this.txtHomestead2.Left = 3.46875F;
			this.txtHomestead2.Name = "txtHomestead2";
			this.txtHomestead2.Style = "font-size: 10pt; text-align: right";
			this.txtHomestead2.Text = "Field3";
			this.txtHomestead2.Top = 21.14583F;
			this.txtHomestead2.Width = 1.40625F;
			// 
			// txtOtherRevenue2
			// 
			this.txtOtherRevenue2.Height = 0.19F;
			this.txtOtherRevenue2.Left = 3.46875F;
			this.txtOtherRevenue2.Name = "txtOtherRevenue2";
			this.txtOtherRevenue2.Style = "font-size: 10pt; text-align: right";
			this.txtOtherRevenue2.Text = "Field3";
			this.txtOtherRevenue2.Top = 21.64583F;
			this.txtOtherRevenue2.Width = 1.40625F;
			// 
			// txtTotalDeductions2
			// 
			this.txtTotalDeductions2.Height = 0.19F;
			this.txtTotalDeductions2.Left = 4.96875F;
			this.txtTotalDeductions2.Name = "txtTotalDeductions2";
			this.txtTotalDeductions2.Style = "font-size: 10pt; text-align: right";
			this.txtTotalDeductions2.Text = "Field3";
			this.txtTotalDeductions2.Top = 21.89583F;
			this.txtTotalDeductions2.Width = 1.40625F;
			// 
			// Label73
			// 
			this.Label73.Height = 0.1875F;
			this.Label73.HyperLink = null;
			this.Label73.Left = 0.3541667F;
			this.Label73.Name = "Label73";
			this.Label73.Style = "font-size: 10pt; font-weight: bold; text-decoration: underline";
			this.Label73.Text = "Net Assessment for Commitment";
			this.Label73.Top = 22.13542F;
			this.Label73.Width = 2.635417F;
			// 
			// Label74
			// 
			this.Label74.Height = 0.1875F;
			this.Label74.HyperLink = null;
			this.Label74.Left = 0.02083333F;
			this.Label74.Name = "Label74";
			this.Label74.Style = "font-size: 10pt; text-align: right";
			this.Label74.Text = "12.";
			this.Label74.Top = 22.13542F;
			this.Label74.Width = 0.2604167F;
			// 
			// txtNetAssessment2
			// 
			this.txtNetAssessment2.Height = 0.19F;
			this.txtNetAssessment2.Left = 4.96875F;
			this.txtNetAssessment2.Name = "txtNetAssessment2";
			this.txtNetAssessment2.Style = "font-size: 10pt; text-align: right";
			this.txtNetAssessment2.Text = "Field3";
			this.txtNetAssessment2.Top = 22.14583F;
			this.txtNetAssessment2.Width = 1.40625F;
			// 
			// RichEdit8
			// 
			//this.RichEdit8.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.RichEdit8.Height = 0.5729167F;
			this.RichEdit8.Left = 0F;
			this.RichEdit8.Name = "RichEdit8";
			this.RichEdit8.RTF = resources.GetString("RichEdit8.RTF");
			this.RichEdit8.Top = 22.44792F;
			this.RichEdit8.Width = 6.833333F;
			// 
			// Line16
			// 
			this.Line16.Height = 0F;
			this.Line16.Left = 0.04166667F;
			this.Line16.LineWeight = 1F;
			this.Line16.Name = "Line16";
			this.Line16.Top = 23.8125F;
			this.Line16.Width = 3.697917F;
			this.Line16.X1 = 0.04166667F;
			this.Line16.X2 = 3.739583F;
			this.Line16.Y1 = 23.8125F;
			this.Line16.Y2 = 23.8125F;
			// 
			// Line17
			// 
			this.Line17.Height = 0F;
			this.Line17.Left = 0.0625F;
			this.Line17.LineWeight = 1F;
			this.Line17.Name = "Line17";
			this.Line17.Top = 24.0625F;
			this.Line17.Width = 3.697917F;
			this.Line17.X1 = 0.0625F;
			this.Line17.X2 = 3.760417F;
			this.Line17.Y1 = 24.0625F;
			this.Line17.Y2 = 24.0625F;
			// 
			// Line18
			// 
			this.Line18.Height = 0F;
			this.Line18.Left = 0.0625F;
			this.Line18.LineWeight = 1F;
			this.Line18.Name = "Line18";
			this.Line18.Top = 24.32292F;
			this.Line18.Width = 3.697917F;
			this.Line18.X1 = 0.0625F;
			this.Line18.X2 = 3.760417F;
			this.Line18.Y1 = 24.32292F;
			this.Line18.Y2 = 24.32292F;
			// 
			// Line19
			// 
			this.Line19.Height = 0F;
			this.Line19.Left = 0.0625F;
			this.Line19.LineWeight = 1F;
			this.Line19.Name = "Line19";
			this.Line19.Top = 24.57292F;
			this.Line19.Width = 3.697917F;
			this.Line19.X1 = 0.0625F;
			this.Line19.X2 = 3.760417F;
			this.Line19.Y1 = 24.57292F;
			this.Line19.Y2 = 24.57292F;
			// 
			// Line20
			// 
			this.Line20.Height = 0F;
			this.Line20.Left = 0.0625F;
			this.Line20.LineWeight = 1F;
			this.Line20.Name = "Line20";
			this.Line20.Top = 24.82292F;
			this.Line20.Width = 3.697917F;
			this.Line20.X1 = 0.0625F;
			this.Line20.X2 = 3.760417F;
			this.Line20.Y1 = 24.82292F;
			this.Line20.Y2 = 24.82292F;
			// 
			// Label75
			// 
			this.Label75.Height = 0.2083333F;
			this.Label75.HyperLink = null;
			this.Label75.Left = 3.78125F;
			this.Label75.Name = "Label75";
			this.Label75.Style = "font-size: 10pt";
			this.Label75.Text = "Municipal Assessor(s)";
			this.Label75.Top = 23.60417F;
			this.Label75.Width = 3.114583F;
			// 
			// Label76
			// 
			this.Label76.Height = 0.2395833F;
			this.Label76.HyperLink = null;
			this.Label76.Left = 0F;
			this.Label76.Name = "Label76";
			this.Label76.Style = "font-size: 10pt";
			this.Label76.Text = "Given under our hands this";
			this.Label76.Top = 23.26042F;
			this.Label76.Width = 6.895833F;
			// 
			// Label77
			// 
			this.Label77.Height = 0.2395833F;
			this.Label77.HyperLink = null;
			this.Label77.Left = 0F;
			this.Label77.Name = "Label77";
			this.Label77.Style = "font-size: 10pt";
			this.Label77.Text = "Complete in Duplicate. File original with Tax Collector. File copy in Valuation B" + "ook";
			this.Label77.Top = 25.125F;
			this.Label77.Width = 6.895833F;
			// 
			// Label78
			// 
			this.Label78.Height = 0.1875F;
			this.Label78.HyperLink = null;
			this.Label78.Left = 0.3541667F;
			this.Label78.Name = "Label78";
			this.Label78.Style = "font-size: 10pt";
			this.Label78.Text = "BETE Reimbursement";
			this.Label78.Top = 7.635417F;
			this.Label78.Width = 1.947917F;
			// 
			// Label79
			// 
			this.Label79.Height = 0.1875F;
			this.Label79.HyperLink = null;
			this.Label79.Left = 0.02083333F;
			this.Label79.Name = "Label79";
			this.Label79.Style = "font-size: 10pt; text-align: right";
			this.Label79.Text = "9.";
			this.Label79.Top = 7.635417F;
			this.Label79.Width = 0.2604167F;
			// 
			// txtBETE
			// 
			this.txtBETE.Height = 0.19F;
			this.txtBETE.Left = 3.46875F;
			this.txtBETE.Name = "txtBETE";
			this.txtBETE.Style = "font-size: 10pt; text-align: right";
			this.txtBETE.Text = "Field3";
			this.txtBETE.Top = 7.645833F;
			this.txtBETE.Width = 1.40625F;
			// 
			// Label80
			// 
			this.Label80.Height = 0.1875F;
			this.Label80.HyperLink = null;
			this.Label80.Left = 0.3541667F;
			this.Label80.Name = "Label80";
			this.Label80.Style = "font-size: 10pt";
			this.Label80.Text = "BETE Reimbursement";
			this.Label80.Top = 21.38542F;
			this.Label80.Width = 1.947917F;
			// 
			// Label81
			// 
			this.Label81.Height = 0.1875F;
			this.Label81.HyperLink = null;
			this.Label81.Left = 0.02083333F;
			this.Label81.Name = "Label81";
			this.Label81.Style = "font-size: 10pt; text-align: right";
			this.Label81.Text = "9.";
			this.Label81.Top = 21.38542F;
			this.Label81.Width = 0.2604167F;
			// 
			// txtBETE2
			// 
			this.txtBETE2.Height = 0.19F;
			this.txtBETE2.Left = 3.46875F;
			this.txtBETE2.Name = "txtBETE2";
			this.txtBETE2.Style = "font-size: 10pt; text-align: right";
			this.txtBETE2.Text = "Field3";
			this.txtBETE2.Top = 21.39583F;
			this.txtBETE2.Width = 1.40625F;
			// 
			// rptCertificateOfAssessment
			//
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.75F;
			this.PageSettings.Margins.Right = 0.75F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.96875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCounty)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxCollector)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountyTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipalAppropriation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTIF)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSchoolAppropriation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverlay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateRevenue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHomestead)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherRevenue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDeductions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetAssessment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAssessorOf)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxCollector2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMunicipality2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCounty2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label50)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label51)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label52)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label53)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label54)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label55)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label56)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label57)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label58)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label59)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label60)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label61)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label62)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label63)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtCountyTax2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMunicipalAppropriation2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTif2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSchoolAppropriation2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverlay2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalAssessments2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label64)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label65)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label66)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label67)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label68)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label69)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label70)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label71)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label72)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtStateRevenue2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtHomestead2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOtherRevenue2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDeductions2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label73)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label74)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNetAssessment2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label75)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label76)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label77)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label78)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label79)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label80)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label81)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBETE2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox RichEdit1;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox RichEdit2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCounty;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxCollector;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCountyTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipalAppropriation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTIF;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSchoolAppropriation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverlay;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAssessments;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label30;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateRevenue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHomestead;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherRevenue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalDeductions;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label32;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNetAssessment;
		private GrapeCity.ActiveReports.SectionReportModel.PageBreak PageBreak1;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox RichEdit3;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox RichEdit4;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox RichEdit5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line9;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAssessorOf;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label34;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxCollector2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label36;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMunicipality2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label38;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox RichEdit6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label39;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line11;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line13;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label40;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label41;
		private GrapeCity.ActiveReports.SectionReportModel.PageBreak PageBreak2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label45;
		private GrapeCity.ActiveReports.SectionReportModel.Label txtCounty2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label47;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label49;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox RichEdit7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label50;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label51;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label52;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label53;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label54;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label55;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label56;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label57;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label58;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label59;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label60;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label61;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label62;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label63;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtCountyTax2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMunicipalAppropriation2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTif2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtSchoolAppropriation2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOverlay2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalAssessments2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label64;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label65;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label66;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label67;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label68;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label69;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label70;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label71;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label72;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtStateRevenue2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtHomestead2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtOtherRevenue2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalDeductions2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label73;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label74;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNetAssessment2;
		private GrapeCity.ActiveReports.SectionReportModel.RichTextBox RichEdit8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line16;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line18;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label75;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label76;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label77;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label78;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label79;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETE;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label80;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label81;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtBETE2;
	}
}
