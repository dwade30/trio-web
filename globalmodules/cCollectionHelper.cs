﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Wisej.Web;

namespace Global
{
	public class cCollectionHelper
	{
		//=========================================================
		// vbPorter upgrade warning: comparer As cIComparator	OnWrite(cPartyRefComparator)
		public FCCollection Sort(FCCollection srcCollection, cIComparator comparer)
		{
			FCCollection Sort = null;
			int[] Index = null;
			// - "AutoDim"
			if (srcCollection == null)
			{
				Sort = null;
				return Sort;
			}
			if (comparer == null)
			{
				Sort = null;
				return Sort;
			}
			int n;
			n = srcCollection.Count;
			if (n == 0)
			{
				Sort = new FCCollection();
				return Sort;
			}
			Index = new int[n - 1 + 1];
			int i, m;
			for (i = 0; i <= n - 1; i++)
			{
				Index[i] = i + 1;
			}
			for (i = (n / 2) - 1; i >= 0; i--)
			{
				MakeHeap(ref srcCollection, ref Index, i, n, ref comparer);
			}
			for (m = n; m >= 2; m--)
			{
				Exchange(ref Index, 0, m - 1);
				MakeHeap(ref srcCollection, ref Index, 0, m - 1, ref comparer);
			}
			FCCollection returnColl = new FCCollection();
			for (i = 0; i <= n - 1; i++)
			{
				returnColl.Add(srcCollection[Index[i]]);
			}
			Sort = returnColl;
			return Sort;
		}

		private void Exchange(ref int[] Index, int i, int j)
		{
			int temp;
			temp = Index[i];
			Index[i] = Index[j];
			Index[j] = temp;
		}

		private void MakeHeap(ref FCCollection c, ref int[] Index, int i1, int n, ref cIComparator comparer)
		{
			int nDiv2;
			nDiv2 = n / 2;
			int i;
			i = i1;
			int k = 0;
			object item1, item2;
			while (i < nDiv2)
			{
				k = 2 * i + 1;
				if (k + 1 < n)
				{
					//FC:FINAL:MSH - Issue #622: Method doesn't work without 'ref' keyword and  
					// property or indexer can't pe passed as 'ref' params, so we must take needed items as object and add 'ref' keyword
					//if (comparer.Compare(c[Index[k]], c[Index[k + 1]]) < 0)
					item1 = c[Index[k]] as object;
					item2 = c[Index[k + 1]] as object;
					if (comparer.Compare(ref item1, ref item2) < 0)
					{
						k += 1;
					}
				}
				// If c[Index(i]) >= c[Index(k]) Then Exit Do
				//FC:FINAL:MSH - Issue #622: Method doesn't work without 'ref' keyword and  
				// property or indexer can't pe passed as 'ref' params, so we must take needed items as object and add 'ref' keyword
				//if (comparer.Compare(c[Index[i]], c[Index[k]]) >= 0) break;
				item1 = c[Index[i]] as object;
				item2 = c[Index[k]] as object;
				if (comparer.Compare(ref item1, ref item2) >= 0)
					break;
				Exchange(ref Index, i, k);
				i = k;
			}
		}

		public void Concatenate(ref FCCollection destinationCollection, ref FCCollection collectionToAdd)
		{
			if (!(destinationCollection == null))
			{
				if (!(collectionToAdd == null))
				{
					int x;
					for (x = 1; x <= collectionToAdd.Count; x++)
					{
						//////Application.DoEvents();
						destinationCollection.Add(collectionToAdd[x]);
					}
				}
			}
		}
	}
}
