﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cBDAccountTitle
	{
		//=========================================================
		private int lngRecordID;
		private string strFund = string.Empty;
		private string strShortDescription = string.Empty;
		private string strDescription = string.Empty;
		private string strCashFund = string.Empty;
		private bool boolUseDueToDueFrom;
		private string strGLAccountType = string.Empty;
		private string strAccount = string.Empty;
		private string strSuffix = string.Empty;
		private bool boolUpdated;
		private bool boolDeleted;

		public string Account
		{
			set
			{
				strAccount = value;
				IsUpdated = true;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public string Suffix
		{
			set
			{
				strSuffix = value;
				IsUpdated = true;
			}
			get
			{
				string Suffix = "";
				Suffix = strSuffix;
				return Suffix;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
				IsUpdated = true;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
				IsUpdated = true;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
				IsUpdated = true;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string CashFund
		{
			set
			{
				strCashFund = value;
				IsUpdated = true;
			}
			get
			{
				string CashFund = "";
				CashFund = strCashFund;
				return CashFund;
			}
		}

		public bool UseDueToDueFrom
		{
			set
			{
				boolUseDueToDueFrom = value;
				IsUpdated = true;
			}
			get
			{
				bool UseDueToDueFrom = false;
				UseDueToDueFrom = boolUseDueToDueFrom;
				return UseDueToDueFrom;
			}
		}

		public string GLAccountType
		{
			set
			{
				strGLAccountType = value;
				IsUpdated = true;
			}
			get
			{
				string GLAccountType = "";
				GLAccountType = strGLAccountType;
				return GLAccountType;
			}
		}
	}
}
