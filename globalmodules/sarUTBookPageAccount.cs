//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;
using fecherFoundation;

#if TWUT0000
using TWUT0000;
#endif

namespace Global
{
    /// <summary>
    /// Summary description for arUTBookPageAccount.
    /// </summary>
    public partial class sarUTBookPageAccount : FCSectionReport
    {

        public sarUTBookPageAccount()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            if (_InstancePtr == null)
                _InstancePtr = this;
            this.Name = "sarUTBookPageAccount";
        }

        public static sarUTBookPageAccount InstancePtr
        {
            get
            {
                return (sarUTBookPageAccount)Sys.GetInstance(typeof(sarUTBookPageAccount));
            }
        }

        protected sarUTBookPageAccount _InstancePtr = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                rsData.Dispose();
            }
            base.Dispose(disposing);
        }

        // nObj = 1
        //   0	arUTBookPageAccount	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               12/04/2006              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               01/16/2007              *
        // ********************************************************
        clsDRWrapper rsData = new clsDRWrapper();
        // Public lngBill1                 As Long
        // Public lngBill2                 As Long
        public int lngAcct1;
        public int lngAcct2;
        public bool boolWater;
        public string strWS = "";

        // vbPorter upgrade warning: lngFirstAccount As int	OnWriteFCConvert.ToDouble(
        // vbPorter upgrade warning: lngLastAccount As int	OnWriteFCConvert.ToDouble(
        public void Init(bool boolPassWater, int lngFirstAccount, int lngLastAccount, int lngFirstBill = 0, int lngLastBill = 0, int lngEligible = 0)
        {
            try
            {   // On Error GoTo ERROR_HANDLER
                string strSQL;
                string strEligible = "";

                boolWater = boolPassWater;
                if (boolWater)
                {
                    strWS = "W";
                }
                else
                {
                    strWS = "S";
                }

                if (lngEligible != 0)
                {
                    strEligible = " AND (" + strWS + "LienProcessStatus = 3 OR " + strWS + "LienProcessStatus = 2) AND (" + strWS + "PrinOwed) > " + strWS + "PrinPaid ";
                }

                strSQL = "SELECT DISTINCT AccountNumber, OwnerName FROM (SELECT Master.ID, AccountNumber, pOwn.FullNameLF AS OwnerName FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = Master.OwnerPartyID ) AS qMstr INNER JOIN Bill ON qMstr.ID = Bill.AccountKey WHERE AccountNumber >= " + FCConvert.ToString(lngFirstAccount) + " AND AccountNumber <= " + FCConvert.ToString(lngLastAccount) + " AND " + strWS + "CombinationLienKey = Bill.ID " + strEligible + " ORDER BY AccountNumber";
                // strSQL = "SELECT DISTINCT AccountNumber, OName, BillNumber FROM Master INNER JOIN Bill ON Master.Key = Bill.AccountKey WHERE AccountNumber >= " & lngFirstAccount & " AND AccountNumber <= " & lngLastAccount & " AND " & strWS & "CombinationLienKey = Bill " & strEligible & " ORDER BY AccountNumber"
                // strSQL = "SELECT RSAccount, RSName FROM Master WHERE RSCard = 1 AND RSAccount >= " & lngFirstAccount & " AND RSAccount <= " & lngLastAccount & " ORDER BY RSAccount"

                rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                if (!rsData.EndOfFile())
                {
                    this.Name = "Book Page Report";
                    // lngBill1 = lngFirstBill
                    // lngBill2 = lngLastBill
                    lngAcct1 = lngFirstAccount;
                    lngAcct2 = lngLastAccount;
                    SetReportHeader();
                    frmReportViewer.InstancePtr.Init(this);
                }
                else
                {
                    MessageBox.Show("No matching accounts.", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                    return;
                }
                return;
            }
            catch
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", "Error Initializing Book Page Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = rsData.EndOfFile();
        }

        private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
        {
            switch (KeyCode)
            {

                case Keys.Escape:
                    {
                        this.Close();
                        break;
                    }
            } //end switch
        }

        private void ActiveReport_ReportEnd(object sender, EventArgs e)
        {
            frmWait.InstancePtr.Unload();
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            //modGlobalFunctions.SetFixedSize(this, TWUT0000.MDIParent.InstancePtr.GRID.Text);

            lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
            lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
            lblMuniName.Text = modGlobalConstants.Statics.MuniName;

            frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Book and Page", true, rsData.RecordCount(), true);
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            BindFields();
        }

        private void PageHeader_Format(object sender, EventArgs e)
        {
            lblPage.Text = "Page " + this.PageNumber;
        }

        private void BindFields()
        {
            try
            {   // On Error GoTo ERROR_HANDLER
                int lngAcct = 0;
                if (!rsData.EndOfFile())
                {
                    frmWait.InstancePtr.IncrementProgress();

                    // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                    lngAcct = FCConvert.ToInt32(rsData.Get_Fields("AccountNumber"));

                    Line1.Visible = !(Conversion.Val(lblAccount.Text) == lngAcct);
                    lblAccount.Visible = Line1.Visible;
                    sarBookPageREob.Visible = Line1.Visible;
                    sarBookPageMHob.Visible = Line1.Visible;

                    lblAccount.Text = lngAcct.ToString();

                    // TODO: Field [OwnerName] not found!! (maybe it is an alias?)
                    lblName.Text = FCConvert.ToString(rsData.Get_Fields("OwnerName"));
                    if (Strings.Trim(lblName.Text) == "")
                    {
                        lblName.Text = "No Name on Bill";
                    }
                    // lblBill.Caption = rsData.Fields("BillNumber")

                    // This will set all of the sub reports
                    if (Line1.Visible)
                    {
                        sarBookPageREob.Report = new sarBookPageUT();
                        sarBookPageREob.Report.UserData = lngAcct;
                        sarBookPageMHob.Report = new sarUTBookPageMH();
                        sarBookPageMHob.Report.UserData = lngAcct;
                    }
                    sarBookPageLNob.Report = new sarUTBookPageLN();
                    sarBookPageLNob.Report.UserData = lngAcct;
                    sarBookPageLDNob.Report = new sarUTBookPageLDN();
                    sarBookPageLDNob.Report.UserData = lngAcct;

                    rsData.MoveNext();
                }
                return;
            }
            catch (Exception ex)
            {   
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Book Page Report - Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void SetReportHeader()
        {
            string strYear = "";
            string strAcct = "";

            // set the report type caption
            if (lngAcct1 == lngAcct2)
            {
                strAcct = "Account : " + FCConvert.ToString(lngAcct1);
            }
            else
            {
                strAcct = "Account : " + FCConvert.ToString(lngAcct1) + " to " + FCConvert.ToString(lngAcct2);
            }

            // If lngBill1 <> 0 Then
            // If lngBill1 = lngBill2 Then
            // strYear = "Bill : " & lngBill1
            // Else
            // strYear = "Bill : " & lngBill1 & " to " & lngBill2
            // End If
            // Else
            // strYear = ""
            // End If

            lblReportType.Text = Strings.Trim(strAcct + "  " + strYear);
        }

        private void arUTBookPageAccount_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //arUTBookPageAccount properties;
            //arUTBookPageAccount.Caption	= "Book Page Report";
            //arUTBookPageAccount.Icon	= "sarUTBookPageAccount.dsx":0000";
            //arUTBookPageAccount.Left	= 0;
            //arUTBookPageAccount.Top	= 0;
            //arUTBookPageAccount.Width	= 11880;
            //arUTBookPageAccount.Height	= 8595;
            //arUTBookPageAccount.StartUpPosition	= 3;
            //arUTBookPageAccount.SectionData	= "sarUTBookPageAccount.dsx":058A;
            //End Unmaped Properties
        }
    }
}
