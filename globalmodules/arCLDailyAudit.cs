﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TWSharedLibrary;
#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arCLDailyAudit : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/26/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/30/2005              *
		// ********************************************************
		public bool boolWide;
		// True if this report is in Wide Format
		public float lngWidth;
		// This is the Print Width of the report
		bool boolDone;
		public string strType = "";
		bool boolShowRE;
		// Show RE Records?
		public bool boolOrderByReceipt;
		// order by receipt
		bool boolCloseOutAudit;
		// Actually close out the payments
		public bool boolLandscape;
		// is the report to be printed landscape?
		public string strOrder = "";

		public arCLDailyAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Daily Audit Report";
		}

		public static arCLDailyAudit InstancePtr
		{
			get
			{
				return (arCLDailyAudit)Sys.GetInstance(typeof(arCLDailyAudit));
			}
		}

		protected arCLDailyAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// this should let the detail section fire once
			if (boolDone)
			{
				eArgs.EOF = boolDone;
			}
			else
			{
				eArgs.EOF = boolDone;
				boolDone = true;
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsOrder = new clsDRWrapper();
			string strCLReportType;
			StartUp();
			if (rsOrder.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase, 0, false, 0, false, "", true, "", true))
			{
				if (rsOrder.EndOfFile() != true)
				{
					if (FCConvert.ToBoolean(rsOrder.Get_Fields_Boolean("AuditSeqReceipt")))
					{
						boolOrderByReceipt = true;
					}
					else
					{
						boolOrderByReceipt = false;
					}
				}
				else
				{
					boolOrderByReceipt = true;
				}
			}
			else
			{
				FCMessageBox.Show("An error has occured while determining your default Audit sort order.  This will not affect the data.  The order has been set to Receipt Number.", MsgBoxStyle.Information, "Collections Table Error");
				boolOrderByReceipt = true;
			}
			strCLReportType = FCConvert.ToString(this.UserData);
			if (boolOrderByReceipt)
			{
				strOrder = " ORDER BY ReceiptNumber";
			}
			else
			{
				strOrder = "";
			}
			if (boolShowRE)
			{
				rsOrder.OpenRecordset("SELECT * FROM PaymentRec WHERE BillCode <> 'P' AND ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut), modExtraModules.strCLDatabase);
			}
			else
			{
				rsOrder.OpenRecordset("SELECT * FROM PaymentRec WHERE BillCode = 'P' AND ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut), modExtraModules.strCLDatabase);
			}
			if (rsOrder.EndOfFile())
			{
				lblOutput.Visible = true;
				lblOutput.Text = "There are no collection records to process at this time.";
			}
			else
			{
				lblOutput.Visible = false;
				sarDADetailOb.Visible = true;
				sarDACashOb.Visible = true;
				sarDANonCashOb.Visible = true;
				sarDATypeSummaryOB.Visible = true;
				// set the subreports
				sarDADetailOb.Report = new sarDADetail();
				sarDACashOb.Report = new sarDACash();
				sarDANonCashOb.Report = new sarDANonCash();
				sarDATypeSummaryOB.Report = new sarDATypeSummary();
				sarDACashOb.Report.UserData = strCLReportType;
				sarDANonCashOb.Report.UserData = strCLReportType;
				sarDATypeSummaryOB.Report.UserData = strCLReportType;
			}
			rsOrder.Dispose();
		}

		private void SetupFields()
		{
			// find out which way the user wants to print
			boolWide = boolLandscape;
			if (boolWide)
			{
				// wide format
				lngWidth = 15000 / 1440f;
			}
			else
			{
				// narrow format
				//FC:FINAL:KS: #i241:total column missing
				//lngWidth = FCConvert.ToInt32((11400 / 1440f);
				lngWidth = 11520 / 1440f;
			}
			this.PrintWidth = lngWidth;
			lblPreview.Width = lngWidth;
			sarDADetailOb.Width = lngWidth;
			sarDACashOb.Width = lngWidth;
			sarDANonCashOb.Width = lngWidth;
			//FC:FINAL:KS: #i241:total column missing
			//sarDATypeSummaryOB.Width = lngWidth - 1;
			sarDATypeSummaryOB.Width = lngWidth;
			sarDADetailOb.Left = 0;
			sarDACashOb.Left = 0;
			sarDANonCashOb.Left = 0;
			//sarDATypeSummary.InstancePtr.Left = 0;
			lblOutput.Width = lngWidth;
			lblHeader.Width = lngWidth;
			lblDate.Left = lngWidth - lblDate.Width;
			lblPage.Left = lngWidth - lblPage.Width;
		}

		private void StartUp()
		{
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = Strings.Trim(modGlobalConstants.Statics.MuniName);
			// these are passed from rptCLDailyAuditMaster
			boolCloseOutAudit = rptCLDailyAuditMaster.InstancePtr.boolCloseOutAudit;
			if (boolCloseOutAudit)
			{
				lblPreview.Visible = false;
			}
			else
			{
				if (rptCLDailyAuditMaster.InstancePtr.boolRecreation)
				{
					lblPreview.Text = "REPRINT";
				}
				else
				{
					lblPreview.Text = "PREVIEW";
				}
				lblPreview.Visible = true;
			}
			boolShowRE = rptCLDailyAuditMaster.InstancePtr.boolShowRE;
			boolLandscape = rptCLDailyAuditMaster.InstancePtr.boolLandscape;
			if (boolShowRE)
			{
				strType = "AND (BillCode = 'R' OR BillCode = 'L') ";
				lblHeader.Text = "Real Estate Daily Audit Report";
			}
			else
			{
				strType = "AND BillCode = 'P' ";
				lblHeader.Text = "Personal Property Daily Audit Report";
			}
			// SetFixedSizeReport Me, MDIParent.Grid
			SetupFields();
			frmWait.InstancePtr.IncrementProgress();
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + rptCLDailyAuditMaster.InstancePtr.PageNumber;
		}

		private void arCLDailyAudit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arCLDailyAudit.Text	= "Daily Audit Report";
			//arCLDailyAudit.Icon	= "arCLDailyAudit.dsx":0000";
			//arCLDailyAudit.Left	= 0;
			//arCLDailyAudit.Top	= 0;
			//arCLDailyAudit.Width	= 11880;
			//arCLDailyAudit.Height	= 8415;
			//arCLDailyAudit.StartUpPosition	= 3;
			//arCLDailyAudit.SectionData	= "arCLDailyAudit.dsx":058A;
			//End Unmaped Properties
		}
	}
}
