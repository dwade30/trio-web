﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cBDExpenseObject
	{
		//=========================================================
		private int lngRecordNumber;
		private string strExpense = string.Empty;
		private string strShortDescription = string.Empty;
		private string strLongDescription = string.Empty;
		private string strExpenseObject = string.Empty;

		public string ExpObject
		{
			set
			{
				strExpenseObject = value;
			}
			get
			{
				string ExpObject = "";
				ExpObject = strExpenseObject;
				return ExpObject;
			}
		}

		public int ID
		{
			set
			{
				lngRecordNumber = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordNumber;
				return ID;
			}
		}

		public string Expense
		{
			set
			{
				strExpense = value;
			}
			get
			{
				string Expense = "";
				Expense = strExpense;
				return Expense;
			}
		}

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}

		public string Description
		{
			set
			{
				strLongDescription = value;
			}
			get
			{
				string Description = "";
				Description = strLongDescription;
				return Description;
			}
		}
	}
}
