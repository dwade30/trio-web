﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Microsoft.ApplicationInsights.Web;
using TWSharedLibrary;
using Wisej.Web;

#if TWBD0000
using TWBD0000;
#endif
namespace Global
{
	public class cJournalController
	{
		//=========================================================
		private string strLastError = "";
		// vbPorter upgrade warning: lngLastError As int	OnWrite(short, string)
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cJournal GetJournal(int lngID)
		{
			cJournal GetJournal = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cJournal journ = null;
				rsLoad.OpenRecordset("select * from JournalMaster where id = " + FCConvert.ToString(lngID), "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					journ = new cJournal();
					// TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					journ.BankNumber = FCConvert.ToInt32(rsLoad.Get_Fields("BankNumber"));
					journ.CheckDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("CheckDate"));
					journ.CRPeriodCloseoutKey = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("CRPeriodCloseoutKey"))));
					journ.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
					journ.IsAutomaticJournal = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutomaticJournal"));
					journ.IsCreditMemo = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CreditMemo"));
					journ.IsCreditMemoCorrection = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CreditMemoCorrection"));
					// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					journ.JournalNumber = FCConvert.ToInt32(rsLoad.Get_Fields("JournalNumber"));
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					journ.JournalType = FCConvert.ToString(rsLoad.Get_Fields("Type"));
					journ.OOBPostingDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("OOBPostingDate"));
					journ.OOBPOstingOpID = FCConvert.ToString(rsLoad.Get_Fields_String("OOBPostingOpID"));
					journ.OOBPostingTime = FCConvert.ToString(rsLoad.Get_Fields_DateTime("OOBPostingTime"));
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					journ.Period = FCConvert.ToInt16(rsLoad.Get_Fields("Period"));
					journ.PostedOOB = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("PostedOOB"));
					journ.Status = FCConvert.ToString(rsLoad.Get_Fields_String("Status"));
					journ.StatusChangeDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatusChangeDate"));
					journ.StatusChangeOpID = FCConvert.ToString(rsLoad.Get_Fields_String("StatusChangeOpID"));
					journ.StatusChangeTime = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatusChangeTime"));
					journ.TotalAmount = FCConvert.ToDouble(rsLoad.Get_Fields_Decimal("TotalAmount"));
					journ.TotalCreditMemo = FCConvert.ToDouble(rsLoad.Get_Fields_Decimal("TotalCreditMemo"));
					journ.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					journ.IsUpdated = false;
				}
				GetJournal = journ;
				return GetJournal;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetJournal;
		}

		public void SaveJournal(ref cJournal journ)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (!(journ == null))
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from JournalMaster where id = " + journ.ID, "Budgetary");
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						if (journ.ID > 0)
						{
							lngLastError = 9999;
							strLastError = "Journal record not found";
							return;
						}
						rsSave.AddNew();
					}
					rsSave.Set_Fields("BankNumber", journ.BankNumber);
					if (Information.IsDate(journ.CheckDate))
					{
						rsSave.Set_Fields("CheckDate", journ.CheckDate);
					}
					rsSave.Set_Fields("crperiodcloseoutkey", journ.CRPeriodCloseoutKey);
					rsSave.Set_Fields("Description", journ.Description);
					rsSave.Set_Fields("AutomaticJournal", journ.IsAutomaticJournal);
					rsSave.Set_Fields("CreditMemo", journ.IsCreditMemo);
					rsSave.Set_Fields("CreditMemoCorrection", journ.IsCreditMemoCorrection);
					rsSave.Set_Fields("JournalNumber", journ.JournalNumber);
					rsSave.Set_Fields("Type", journ.JournalType);
					if (Information.IsDate(journ.OOBPostingDate))
					{
						rsSave.Set_Fields("OOBPostingDate", journ.OOBPostingDate);
						rsSave.Set_Fields("OOBPostingTime", "12/30/1899" + journ.OOBPostingTime);
					}
					rsSave.Set_Fields("OOBPostingOpID", journ.OOBPOstingOpID);
					rsSave.Set_Fields("Period", journ.Period);
					rsSave.Set_Fields("PostedOOB", journ.PostedOOB);
					rsSave.Set_Fields("Status", journ.Status);
					rsSave.Set_Fields("StatusChangeDate", journ.StatusChangeDate);
					rsSave.Set_Fields("StatusChangeOpID", journ.StatusChangeOpID);
					rsSave.Set_Fields("StatusChangeTime", "12/30/1899 " + journ.StatusChangeTime);
					rsSave.Set_Fields("Totalamount", journ.TotalAmount);
					rsSave.Set_Fields("TotalCreditMemo", journ.TotalCreditMemo);
					rsSave.Update();
					journ.ID = rsSave.Get_Fields_Int32("ID");
					journ.IsUpdated = false;
					if (!(journ.JournalEntries == null))
					{
						SaveJournalEntries(journ.JournalEntries);
					}
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
		}

		public void SaveJournalEntry(ref cJournalEntry jEntry)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (jEntry.IsDeleted)
				{
					DeleteJournalEntry(jEntry.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("Select * from journalentries where id = " + jEntry.ID, "Budgetary");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (jEntry.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Journal entry record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("Account", jEntry.Account);
				rsSave.Set_Fields("Amount", jEntry.Amount);
				rsSave.Set_Fields("BankNumber", jEntry.BankNumber);
				rsSave.Set_Fields("CarryForward", jEntry.CarryForward);
				rsSave.Set_Fields("CheckNumber", jEntry.CheckNumber);
				rsSave.Set_Fields("1099", jEntry.Code1099);
				rsSave.Set_Fields("Description", jEntry.Description);
				if (Information.IsDate(jEntry.JournalEntriesDate))
				{
					rsSave.Set_Fields("JournalEntriesDate", jEntry.JournalEntriesDate);
				}
				rsSave.Set_Fields("JournalNumber", jEntry.JournalNumber);
				rsSave.Set_Fields("Type", jEntry.JournalType);
				rsSave.Set_Fields("Period", jEntry.Period);
				rsSave.Set_Fields("PO", jEntry.PO);
				if (Information.IsDate(jEntry.PostedDate))
				{
					rsSave.Set_Fields("PostedDate", jEntry.PostedDate);
				}
				rsSave.Set_Fields("project", jEntry.Project);
				rsSave.Set_Fields("RCB", jEntry.RCB);
				rsSave.Set_Fields("Status", jEntry.Status);
				rsSave.Set_Fields("VendorNumber", jEntry.VendorNumber);
				rsSave.Set_Fields("WarrantNumber", jEntry.WarrantNumber);
				rsSave.Update();
				jEntry.ID = rsSave.Get_Fields_Int32("ID");
				jEntry.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
		}

		public cJournalEntry GetJournalEntry(int lngID)
		{
			cJournalEntry GetJournalEntry = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cJournalEntry jEntry = null;
				rsLoad.OpenRecordset("select * from JournalEntries where id = " + FCConvert.ToString(lngID), "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					jEntry = new cJournalEntry();
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					jEntry.Account = FCConvert.ToString(rsLoad.Get_Fields("Account"));
					// TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
					//FC:FINAL:MSH - can't implicitly convert decimal to double(same with issue #625)
					jEntry.Amount = rsLoad.Get_Fields_Double("Amount");
					// TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					jEntry.BankNumber = FCConvert.ToInt32(rsLoad.Get_Fields("BankNumber"));
					// TODO: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
					jEntry.CarryForward = FCConvert.ToBoolean(rsLoad.Get_Fields("CarryForward"));
					// TODO: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
					jEntry.CheckNumber = FCConvert.ToString(rsLoad.Get_Fields("CheckNumber"));
					// TODO: Check the table for the column [1099] and replace with corresponding Get_Field method
					jEntry.Code1099 = FCConvert.ToString(rsLoad.Get_Fields("1099"));
					jEntry.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
					if (Information.IsDate(rsLoad.Get_Fields("JournalEntriesDate")))
					{
						jEntry.JournalEntriesDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("JournalEntriesDate"));
					}
					// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					jEntry.JournalNumber = FCConvert.ToInt32(rsLoad.Get_Fields("JournalNumber"));
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					jEntry.JournalType = FCConvert.ToString(rsLoad.Get_Fields("Type"));
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					jEntry.Period = FCConvert.ToInt32(rsLoad.Get_Fields("Period"));
					// TODO: Check the table for the column [PO] and replace with corresponding Get_Field method
					jEntry.PO = FCConvert.ToString(rsLoad.Get_Fields("PO"));
					if (Information.IsDate(rsLoad.Get_Fields("PostedDate")))
					{
						jEntry.PostedDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("PostedDate"));
					}
					jEntry.Project = FCConvert.ToString(rsLoad.Get_Fields_String("Project"));
					jEntry.RCB = FCConvert.ToString(rsLoad.Get_Fields_String("RCB"));
					jEntry.Status = FCConvert.ToString(rsLoad.Get_Fields_String("Status"));
					jEntry.VendorNumber = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("VendorNumber"));
					jEntry.WarrantNumber = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("WarrantNumber"));
					jEntry.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					jEntry.IsUpdated = false;
				}
				GetJournalEntry = jEntry;
				return GetJournalEntry;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetJournalEntry;
		}

		private void FillJournalEntriesByNumber(int lngJournalNumber, cGenericCollection gColl, string strArchive = "")
		{
			ClearErrors();
				clsDRWrapper rsLoad = new clsDRWrapper();

                try
                {
                    // On Error GoTo ErrorHandler
                    cJournalEntry jEntry;
                    DateTime date;

                    if (strArchive != "")
                    {
                        rsLoad.GroupName = strArchive;
                    }

                    rsLoad.OpenRecordset("select * from JournalEntries where journalnumber = " + FCConvert.ToString(lngJournalNumber), "Budgetary");

                    while (!rsLoad.EndOfFile())
                    {
                        ////////Application.DoEvents();
                        jEntry = new cJournalEntry();

                        // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                        jEntry.Account = FCConvert.ToString(rsLoad.Get_Fields("Account"));

                        // TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
                        jEntry.Amount = FCConvert.ToDouble(rsLoad.Get_Fields("Amount"));

                        // TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
                        jEntry.BankNumber = FCConvert.ToInt32(rsLoad.Get_Fields("BankNumber"));

                        // TODO: Check the table for the column [CarryForward] and replace with corresponding Get_Field method
                        jEntry.CarryForward = FCConvert.ToBoolean(rsLoad.Get_Fields("CarryForward"));

                        // TODO: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                        jEntry.CheckNumber = FCConvert.ToString(rsLoad.Get_Fields("CheckNumber"));

                        // TODO: Check the table for the column [1099] and replace with corresponding Get_Field method
                        jEntry.Code1099 = FCConvert.ToString(rsLoad.Get_Fields("1099"));
                        jEntry.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));

                        if (Information.IsDate(rsLoad.Get_Fields("JournalEntriesDate"), out date))
                        {
                            jEntry.JournalEntriesDate = FCConvert.ToString(date);
                        }

                        // TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                        jEntry.JournalNumber = FCConvert.ToInt32(rsLoad.Get_Fields("JournalNumber"));

                        // TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
                        jEntry.JournalType = FCConvert.ToString(rsLoad.Get_Fields("Type"));

                        // TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
                        jEntry.Period = FCConvert.ToInt16(rsLoad.Get_Fields("Period"));

                        // TODO: Check the table for the column [PO] and replace with corresponding Get_Field method
                        jEntry.PO = FCConvert.ToString(rsLoad.Get_Fields("PO"));

                        if (Information.IsDate(rsLoad.Get_Fields("PostedDate"), out date))
                        {
                            jEntry.PostedDate = FCConvert.ToString(date);
                        }

                        jEntry.Project = FCConvert.ToString(rsLoad.Get_Fields_String("Project"));
                        jEntry.RCB = FCConvert.ToString(rsLoad.Get_Fields_String("RCB"));
                        jEntry.Status = FCConvert.ToString(rsLoad.Get_Fields_String("Status"));
                        jEntry.VendorNumber = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("VendorNumber"));
                        jEntry.WarrantNumber = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("WarrantNumber"));
                        jEntry.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
                        jEntry.IsUpdated = false;
                        gColl.AddItem(jEntry);
                        rsLoad.MoveNext();
                    }

                    return;
                }
                catch (Exception ex)
                {
				    StaticSettings.GlobalTelemetryService.TrackException(ex);
				    lngLastError = Information.Err(ex).Number;
                    strLastError = ex.GetBaseException().Message;
                }
                finally
                {
                    rsLoad.DisposeOf();
                }
		}

		public void SaveJournalEntries(cGenericCollection gColl)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (!(gColl == null))
				{
					gColl.MoveFirst();
					cJournalEntry jEntry;
					while (gColl.IsCurrent())
					{
						////////Application.DoEvents();
						jEntry = (cJournalEntry)gColl.GetCurrentItem();
						SaveJournalEntry(ref jEntry);
						if (jEntry.IsDeleted)
						{
							gColl.RemoveCurrent();
						}
						gColl.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
		}

		public void DeleteJournalEntry(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from journalentries where id = " + FCConvert.ToString(lngID), "Budgetary");
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
		}

		public cGenericCollection GetJournalEntriesByNumber(int lngJournalNumber)
		{
			cGenericCollection GetJournalEntriesByNumber = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				FillJournalEntriesByNumber(lngJournalNumber, gColl);
				GetJournalEntriesByNumber = gColl;
				return GetJournalEntriesByNumber;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetJournalEntriesByNumber;
		}

		private void FillJournal(ref cJournal journ, ref clsDRWrapper rsLoad)
		{
			// TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
			journ.BankNumber = FCConvert.ToInt32(rsLoad.Get_Fields("BankNumber"));
			journ.CheckDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("CheckDate"));
			journ.CRPeriodCloseoutKey = FCConvert.ToInt32(Conversion.Val(rsLoad.Get_Fields_Int32("CRPeriodCloseoutKey")));
			journ.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
			journ.IsAutomaticJournal = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutomaticJournal"));
			journ.IsCreditMemo = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CreditMemo"));
			journ.IsCreditMemoCorrection = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CreditMemoCorrection"));
			// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
			journ.JournalNumber = FCConvert.ToInt32(rsLoad.Get_Fields("JournalNumber"));
			// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
			journ.JournalType = FCConvert.ToString(rsLoad.Get_Fields("Type"));
			journ.OOBPostingDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("OOBPostingDate"));
			journ.OOBPOstingOpID = FCConvert.ToString(rsLoad.Get_Fields_String("OOBPostingOpID"));
			journ.OOBPostingTime = FCConvert.ToString(rsLoad.Get_Fields_DateTime("OOBPostingTime"));
			// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
			journ.Period = FCConvert.ToInt16(rsLoad.Get_Fields("Period"));
			journ.PostedOOB = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("PostedOOB"));
			journ.Status = FCConvert.ToString(rsLoad.Get_Fields_String("Status"));
			journ.StatusChangeDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatusChangeDate"));
			journ.StatusChangeOpID = FCConvert.ToString(rsLoad.Get_Fields_String("StatusChangeOpID"));
			journ.StatusChangeTime = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatusChangeTime"));
			journ.TotalAmount = FCConvert.ToDouble(rsLoad.Get_Fields_Decimal("TotalAmount"));
			journ.TotalCreditMemo = FCConvert.ToDouble(rsLoad.Get_Fields_Decimal("TotalCreditMemo"));
			journ.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
			journ.IsUpdated = false;
		}

		public cJournal GetJournalByNumber(int lngNumber)
		{
			cJournal GetJournalByNumber = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cJournal journ = null;
				rsLoad.OpenRecordset("select * from JournalMaster where journalnumber = " + FCConvert.ToString(lngNumber), "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					journ = new cJournal();
					// TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					journ.BankNumber = FCConvert.ToInt32(rsLoad.Get_Fields("BankNumber"));
					journ.CheckDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("CheckDate"));
					journ.CRPeriodCloseoutKey = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("CRPeriodCloseoutKey"))));
					journ.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
					journ.IsAutomaticJournal = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutomaticJournal"));
					journ.IsCreditMemo = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CreditMemo"));
					journ.IsCreditMemoCorrection = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CreditMemoCorrection"));
					// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					journ.JournalNumber = FCConvert.ToInt32(rsLoad.Get_Fields("JournalNumber"));
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					journ.JournalType = FCConvert.ToString(rsLoad.Get_Fields("Type"));
					journ.OOBPostingDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("OOBPostingDate"));
					journ.OOBPOstingOpID = FCConvert.ToString(rsLoad.Get_Fields_String("OOBPostingOpID"));
					journ.OOBPostingTime = FCConvert.ToString(rsLoad.Get_Fields_DateTime("OOBPostingTime"));
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					journ.Period = FCConvert.ToInt16(rsLoad.Get_Fields("Period"));
					journ.PostedOOB = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("PostedOOB"));
					journ.Status = FCConvert.ToString(rsLoad.Get_Fields_String("Status"));
					journ.StatusChangeDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatusChangeDate"));
					journ.StatusChangeOpID = FCConvert.ToString(rsLoad.Get_Fields_String("StatusChangeOpID"));
					journ.StatusChangeTime = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatusChangeTime"));
					journ.TotalAmount = Conversion.Val(rsLoad.Get_Fields_Decimal("TotalAmount"));
					journ.TotalCreditMemo = Conversion.Val(rsLoad.Get_Fields_Decimal("TotalCreditMemo"));
					journ.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					journ.IsUpdated = false;
				}
				GetJournalByNumber = journ;
				return GetJournalByNumber;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetJournalByNumber;
		}

		public cJournal GetFullJournal(int lngID)
		{
			cJournal GetFullJournal = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cJournal journ;
				journ = GetJournal(lngID);
				if (!(journ == null))
				{
					FillJournalEntriesByNumber(journ.JournalNumber, journ.JournalEntries);
				}
				GetFullJournal = journ;
				return GetFullJournal;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetFullJournal;
		}

		public cJournal GetFullJournalByNumber(int lngJournalNumber)
		{
			cJournal GetFullJournalByNumber = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cJournal journ;
				journ = GetJournalByNumber(lngJournalNumber);
				if (!(journ == null))
				{
					FillJournalEntriesByNumber(lngJournalNumber, journ.JournalEntries);
				}
				GetFullJournalByNumber = journ;
				return GetFullJournalByNumber;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetFullJournalByNumber;
		}

		public cGenericCollection GetFullJournalsByPeriod(int intStartPeriod, int intEndPeriod, bool boolIncludePending, string strArchive = "")
		{
			cGenericCollection GetFullJournalsByPeriod = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				cJournal journ;
				string strSql = "";
				clsDRWrapper rs = new clsDRWrapper();
				if (strArchive != "")
				{
					rs.GroupName = strArchive;
				}
				if (intStartPeriod <= intEndPeriod)
				{
					strSql = "Select * from journalmaster where period between " + FCConvert.ToString(intStartPeriod) + " and " + FCConvert.ToString(intEndPeriod);
				}
				else
				{
					strSql = "Select * from journalmaster where (period >= " + FCConvert.ToString(intStartPeriod) + " or period <= " + FCConvert.ToString(intEndPeriod) + ") ";
				}
				if (!boolIncludePending)
				{
					strSql += " and status = 'P' ";
				}
				else
				{
					strSql += " and status <> 'D' ";
				}
				strSql += " order by period, journalnumber";
				rs.OpenRecordset(strSql, "Budgetary");
				while (!rs.EndOfFile())
				{
					////////Application.DoEvents();
					journ = new cJournal();
					FillJournal(ref journ, ref rs);
					FillJournalEntriesByNumber(journ.JournalNumber, journ.JournalEntries, strArchive);
					gColl.AddItem(journ);
					rs.MoveNext();
				}
                rs.DisposeOf();
				GetFullJournalsByPeriod = gColl;
				return GetFullJournalsByPeriod;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetFullJournalsByPeriod;
		}

		public cGenericCollection GetJournals()
		{
			cGenericCollection GetJournals = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cJournal journ;
				rsLoad.OpenRecordset("select * from JournalMaster order by JournalNumber", "Budgetary");
				while (!rsLoad.EndOfFile())
				{
					////////Application.DoEvents();
					journ = new cJournal();
					// TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					journ.BankNumber = FCConvert.ToInt32(rsLoad.Get_Fields("BankNumber"));
					journ.CheckDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("CheckDate"));
					journ.CRPeriodCloseoutKey = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("CRPeriodCloseoutKey"))));
					journ.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
					journ.IsAutomaticJournal = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutomaticJournal"));
					journ.IsCreditMemo = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CreditMemo"));
					journ.IsCreditMemoCorrection = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CreditMemoCorrection"));
					// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					journ.JournalNumber = FCConvert.ToInt32(rsLoad.Get_Fields("JournalNumber"));
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					journ.JournalType = FCConvert.ToString(rsLoad.Get_Fields("Type"));
					journ.OOBPostingDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("OOBPostingDate"));
					journ.OOBPOstingOpID = FCConvert.ToString(rsLoad.Get_Fields_String("OOBPostingOpID"));
					journ.OOBPostingTime = FCConvert.ToString(rsLoad.Get_Fields_DateTime("OOBPostingTime"));
					// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
					journ.Period = FCConvert.ToInt16(rsLoad.Get_Fields("Period"));
					journ.PostedOOB = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("PostedOOB"));
					journ.Status = FCConvert.ToString(rsLoad.Get_Fields_String("Status"));
					journ.StatusChangeDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatusChangeDate"));
					journ.StatusChangeOpID = FCConvert.ToString(rsLoad.Get_Fields_String("StatusChangeOpID"));
					journ.StatusChangeTime = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatusChangeTime"));
					journ.TotalAmount = Conversion.Val(rsLoad.Get_Fields_Decimal("TotalAmount"));
					journ.TotalCreditMemo = Conversion.Val(rsLoad.Get_Fields_Decimal("TotalCreditMemo"));
					journ.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					journ.IsUpdated = false;
					gColl.AddItem(journ);
					rsLoad.MoveNext();
				}
                rsLoad.DisposeOf();
				GetJournals = gColl;
				return GetJournals;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetJournals;
		}

		public cGenericCollection GetUnpostedJournals()
		{
			cGenericCollection GetUnpostedJournals = null;
			GetUnpostedJournals = GetPostedUnpostedJournals(false);
			return GetUnpostedJournals;
		}

		public cGenericCollection GetPostedJournals()
		{
			cGenericCollection GetPostedJournals = null;
			GetPostedJournals = GetPostedUnpostedJournals(true);
			return GetPostedJournals;
		}

		private cGenericCollection GetPostedUnpostedJournals(bool boolPosted)
		{
			cGenericCollection GetPostedUnpostedJournals = null;
			ClearErrors();
				clsDRWrapper rsLoad = new clsDRWrapper();

                try
                {
                    // On Error GoTo ErrorHandler
                    cGenericCollection gColl = new cGenericCollection();
                    cJournal journ;

                    if (!boolPosted)
                    {
                        rsLoad.OpenRecordset("select * from JournalMaster where status <> 'P'  and status <> 'D' order by JournalNumber", "Budgetary");
                    }
                    else
                    {
                        rsLoad.OpenRecordset("select * from JournalMaster where status = 'P' order by JournalNumber desc", "Budgetary");
                    }

                    while (!rsLoad.EndOfFile())
                    {
                        ////////Application.DoEvents();
                        journ = new cJournal();

                        // TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
                        journ.BankNumber = FCConvert.ToInt32(rsLoad.Get_Fields("BankNumber"));
                        journ.CheckDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("CheckDate"));
                        journ.CRPeriodCloseoutKey = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("CRPeriodCloseoutKey"))));
                        journ.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
                        journ.IsAutomaticJournal = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("AutomaticJournal"));
                        journ.IsCreditMemo = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CreditMemo"));
                        journ.IsCreditMemoCorrection = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CreditMemoCorrection"));
						
                        // TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                        journ.JournalNumber = FCConvert.ToInt32(rsLoad.Get_Fields("JournalNumber"));

                        // TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
                        journ.JournalType = FCConvert.ToString(rsLoad.Get_Fields("Type"));
                        journ.OOBPostingDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("OOBPostingDate"));
                        journ.OOBPOstingOpID = FCConvert.ToString(rsLoad.Get_Fields_String("OOBPostingOpID"));
                        journ.OOBPostingTime = FCConvert.ToString(rsLoad.Get_Fields_DateTime("OOBPostingTime"));

                        // TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
                        journ.Period = FCConvert.ToInt16(rsLoad.Get_Fields("Period"));
                        journ.PostedOOB = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("PostedOOB"));
                        journ.Status = FCConvert.ToString(rsLoad.Get_Fields_String("Status"));
                        journ.StatusChangeDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatusChangeDate"));
                        journ.StatusChangeOpID = FCConvert.ToString(rsLoad.Get_Fields_String("StatusChangeOpID"));
                        journ.StatusChangeTime = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatusChangeTime"));
                        journ.TotalAmount = Conversion.Val(rsLoad.Get_Fields_Decimal("TotalAmount"));
                        journ.TotalCreditMemo = Conversion.Val(rsLoad.Get_Fields_Decimal("TotalCreditMemo"));
                        journ.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
                        journ.IsUpdated = false;
                        gColl.AddItem(journ);
                        rsLoad.MoveNext();
                    }

                    GetPostedUnpostedJournals = gColl;

                    return GetPostedUnpostedJournals;
                }
                catch (Exception ex)
                {
				    StaticSettings.GlobalTelemetryService.TrackException(ex);
				    lngLastError = Information.Err(ex).Number;
                    strLastError = ex.GetBaseException().Message;
                }
                finally
                {
                    rsLoad.DisposeOf();
                }
			return GetPostedUnpostedJournals;
		}

		public int GetJournalEntryCount(int lngJournalNumber)
		{
			int GetJournalEntryCount = 0;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngReturn;
				lngReturn = 0;
				rsLoad.OpenRecordset("Select count(ID) as entriescount from journalentries where journalnumber = " + FCConvert.ToString(lngJournalNumber), "Budgetary");
				// TODO: Field [entriescount] not found!! (maybe it is an alias?)
				lngReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("entriescount"))));
				GetJournalEntryCount = lngReturn;
				return GetJournalEntryCount;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetJournalEntryCount;
		}

		public double GetJournalEntriesAmount(int lngJournalNumber)
		{
			double GetJournalEntriesAmount = 0;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				int dblReturn;
				dblReturn = 0;
				rsLoad.OpenRecordset("Select sum(amount) as journalamount from journalentries where journalnumber = " + FCConvert.ToString(lngJournalNumber), "Budgetary");
				// TODO: Field [journalamount] not found!! (maybe it is an alias?)
				dblReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("journalamount"))));
				GetJournalEntriesAmount = dblReturn;
				return GetJournalEntriesAmount;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetJournalEntriesAmount;
		}

		public bool IsJournalOutOfBalance(int lngJournalNumber, string journalType)
		{

			ClearErrors();
			clsDRWrapper rsLoad = new clsDRWrapper();
			try
			{
				if (journalType.ToLower() == "gj" || journalType.ToLower() == "py" || journalType.ToLower() == "cw")
				{
					rsLoad.OpenRecordset("Select sum(amount) as journalamount from journalentries where journalnumber = " + FCConvert.ToString(lngJournalNumber), "Budgetary");
					if (FCConvert.ToDouble(rsLoad.Get_Fields("journalamount")) != 0)
					{
						if (journalType.ToLower() == "gj" || journalType.ToLower() == "py")
						{
							rsLoad.OpenRecordset("Select * from journalentries where journalnumber = " + FCConvert.ToString(lngJournalNumber) + " AND RCB = 'E'", "Budgetary");
							if (!rsLoad.EndOfFile() && !rsLoad.BeginningOfFile())
							{
								return false;
							}
							else
							{
								return true;
							}
						}
						else
						{
							return true;
						}
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}

			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			finally
			{
				rsLoad.DisposeOf();
			}

			return false;
		}

		public string GetPostedDate(int lngJournalNumber)
		{
			string GetPostedDate = "";
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				string strReturn;
				strReturn = "";
				rsLoad.OpenRecordset("Select top 1 posteddate from journalentries where journalnumber = " + FCConvert.ToString(lngJournalNumber), "Budgetary");
				if (Information.IsDate(rsLoad.Get_Fields("posteddate")))
				{
					strReturn = FCConvert.ToString(rsLoad.Get_Fields_DateTime("PostedDate"));
				}
				GetPostedDate = strReturn;
				return GetPostedDate;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetPostedDate;
		}

		public void SetJournalToDeleted(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.Execute("update journalmaster set status = 'D', statuschangedate = '" + DateTime.Today.ToShortDateString() + "', statuschangetime = '" + DateTime.Today.ToShortTimeString() + "', StatusChangeOpID = '" + modBudgetaryAccounting.Statics.User + "' where id = " + FCConvert.ToString(lngID) + " and status = 'E'", "Budgetary");
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
		}

		public void DeleteJournalDetailsByJournalNumber(int lngJournalNumber)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.Execute("Delete from JournalEntries where id = " + FCConvert.ToString(lngJournalNumber), "Budgetary");
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
		}

		public int CreateJournal(string strDescription, string strType, int intPeriod)
		{
			int CreateJournal = 0;
			try
			{
				// On Error GoTo ErrorHandler
				ClearErrors();
				int lngJournal;
				int lngID;
				lngJournal = 0;
				lngID = 0;
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					lngLastError = FCConvert.ToInt32("-9000");
					strLastError = "Unable to lock journal table to obtain a new journal number";
				}
				else
				{
					clsDRWrapper rsLoad = new clsDRWrapper();
					rsLoad.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC", "Budgetary");
					if (!rsLoad.EndOfFile())
					{
						// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						lngJournal = FCConvert.ToInt32(rsLoad.Get_Fields("JournalNumber")) + 1;
					}
					else
					{
						lngJournal = 1;
					}
					rsLoad.AddNew();
					rsLoad.Set_Fields("JournalNumber", lngJournal);
					rsLoad.Set_Fields("Status", "E");
					rsLoad.Set_Fields("StatusChangeTime", "12/30/1899 " + DateTime.Today.ToShortTimeString());
					rsLoad.Set_Fields("StatusChangeDate", DateTime.Today);
					rsLoad.Set_Fields("Description", strDescription);
					rsLoad.Set_Fields("Type", strType);
					rsLoad.Set_Fields("Period", intPeriod);
					rsLoad.Update();
					lngID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					modBudgetaryAccounting.UnlockJournal();
				}
				CreateJournal = lngID;
				return CreateJournal;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return CreateJournal;
		}

		public void UpdateJournalStatus(int lngJournalNumber, string strorigStatus, string strNewStatus)
		{
			try
			{
				// On Error GoTo ErrorHandler
				ClearErrors();
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.Execute("Update journalmaster set status = '" + strNewStatus + "' where status = '" + strorigStatus + "' and JournalNumber = " + FCConvert.ToString(lngJournalNumber), "Budgetary");
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
		}

		public cGenericCollection GetJournalBriefInfosByPeriodRange(int intStartPeriod, int intEndPeriod, bool boolPosted, bool boolUnposted)
		{
			cGenericCollection GetJournalBriefInfosByPeriodRange = null;
			try
			{
				// On Error GoTo ErrorHandler
				ClearErrors();
				string strWhere = "";
				string strSql;
				if (intEndPeriod >= intStartPeriod)
				{
					strWhere = " where Period between " + FCConvert.ToString(intStartPeriod) + "  and " + FCConvert.ToString(intEndPeriod);
				}
				else
				{
					strWhere = "where (period >= " + FCConvert.ToString(intStartPeriod) + " or period <= " + FCConvert.ToString(intEndPeriod) + ") ";
				}
				if (boolPosted && !boolUnposted)
				{
					strWhere += " and  status = 'P' ";
				}
				else if (boolUnposted && !boolPosted)
				{
					strWhere += " and status <> 'P' and status <> 'D' ";
				}
				else
				{
					strWhere += " and status <> 'D' ";
				}
				strSql = "select ID,JournalNumber, [Type], period from journalmaster " + strWhere + " order by journalnumber";
				clsDRWrapper rs = new clsDRWrapper();
				rs.OpenRecordset(strSql, "Budgetary");
				cGenericCollection jList = new cGenericCollection();
				cJournalBriefInfo jInfo;
				while (!rs.EndOfFile())
				{
					////////Application.DoEvents();
					jInfo = new cJournalBriefInfo();
					jInfo.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					// TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
					jInfo.JournalNumber = FCConvert.ToInt32(rs.Get_Fields("JournalNumber"));
					// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
					jInfo.JournalType = FCConvert.ToString(rs.Get_Fields("Type"));
					// TODO: Check the table for the column [period] and replace with corresponding Get_Field method
					jInfo.Period = FCConvert.ToInt16(rs.Get_Fields("period"));
					jList.AddItem(jInfo);
					rs.MoveNext();
				}
				GetJournalBriefInfosByPeriodRange = jList;
				return GetJournalBriefInfosByPeriodRange;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetJournalBriefInfosByPeriodRange;
		}
	}
}
