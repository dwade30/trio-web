﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class modCustomChecks
	{
		public static void AddFieldToCustomCheckTable(ref string strDesc, ref string strControlName, ref string strCheckType, ref bool blnInclude, ref double dblXDefault, ref double dblYDefault, short intWidth = -1, short intHeight = -1, short intFormatID = 1)
		{
			clsDRWrapper rsCustomCheck = new clsDRWrapper();
			rsCustomCheck.OpenRecordset("SELECT * FROM CustomCheckFields WHERE Description = '" + strDesc + "'");
			if (rsCustomCheck.EndOfFile() != true && rsCustomCheck.BeginningOfFile() != true)
			{
				// Matthew Change on 5/6/04
				// rsCustomCheck.Edit
			}
			else
			{
				rsCustomCheck.AddNew();
				rsCustomCheck.Set_Fields("Description", strDesc);
				rsCustomCheck.Set_Fields("ControlName", strControlName);
				rsCustomCheck.Set_Fields("CheckType", strCheckType);
				rsCustomCheck.Set_Fields("Include", blnInclude);
				rsCustomCheck.Set_Fields("Xposition", dblXDefault);
				rsCustomCheck.Set_Fields("DefaultXposition", dblXDefault);
				rsCustomCheck.Set_Fields("Yposition", dblYDefault);
				rsCustomCheck.Set_Fields("DefaultYposition", dblYDefault);
				rsCustomCheck.Set_Fields("FieldWidth", intWidth);
				rsCustomCheck.Set_Fields("FieldHeight", intHeight);
				rsCustomCheck.Set_Fields("FormatID", intFormatID);
				rsCustomCheck.Update();
			}
		}

		public class StaticVariables
		{
			//=========================================================
			public int intCustomCheckFormatToUse;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
