﻿using fecherFoundation;
using fecherFoundation.Extensions;

namespace Global
{
	/// <summary>
	/// Summary description for frmPrintDuplex.
	/// </summary>
	partial class frmPrintDuplex : BaseForm
	{
		public fecherFoundation.FCComboBox cmbDuplex;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrintDuplex));
			this.cmbDuplex = new fecherFoundation.FCComboBox();
			this.lblDuplex = new fecherFoundation.FCLabel();
			//this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.fcLabel1 = new fecherFoundation.FCLabel();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 176);
			this.BottomPanel.Size = new System.Drawing.Size(501, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fcLabel1);
			this.ClientArea.Controls.Add(this.lblDuplex);
			this.ClientArea.Controls.Add(this.cmbDuplex);
			this.ClientArea.Size = new System.Drawing.Size(501, 116);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(501, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(67, 35);
			this.HeaderText.Text = "Print";
			// 
			// cmbDuplex
			// 
			this.cmbDuplex.AutoSize = false;
			this.cmbDuplex.FormattingEnabled = true;
			this.cmbDuplex.Items.AddRange(new object[] {
				"None (Print pages on separate pieces of paper)",
				"Duplex (Printer can print on both sides of paper)",
				"After flipping, top page is the last page",
				"After flipping, top page is the first page"
			});
			this.cmbDuplex.Location = new System.Drawing.Point(200, 30);
			this.cmbDuplex.Name = "cmbDuplex";
			this.cmbDuplex.Size = new System.Drawing.Size(263, 40);
			this.cmbDuplex.TabIndex = 1;
			this.cmbDuplex.Text = "None (Print pages on separate pieces of paper)";
			this.cmbDuplex.SelectedIndexChanged += new System.EventHandler(this.cmbDuplex_SelectedIndexChanged);
			// 
			// lblDuplex
			// 
			this.lblDuplex.AutoSize = true;
			this.lblDuplex.Location = new System.Drawing.Point(30, 44);
			this.lblDuplex.Name = "lblDuplex";
			this.lblDuplex.Size = new System.Drawing.Size(119, 16);
			this.lblDuplex.TabIndex = 0;
			this.lblDuplex.Text = "DUPLEX PRINTING";
			// 
			// MainMenu1
			// 
			//this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
			//this.mnuFile});
			//this.MainMenu1.Visible = true;
			//
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.Text = "File";
			// 
			// fcLabel1
			// 
			this.fcLabel1.AutoSize = true;
			this.fcLabel1.Location = new System.Drawing.Point(30, 90);
			this.fcLabel1.Name = "fcLabel1";
			this.fcLabel1.Size = new System.Drawing.Size(409, 16);
			this.fcLabel1.TabIndex = 2;
			this.fcLabel1.Text = "MANUAL (PRINT ALL FIRST SIDE PAGES THEN ALL SECOND SIDES)";
			this.fcLabel1.Click += new System.EventHandler(this.fcLabel1_Click);
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(167, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(144, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Save & Continue";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmPrintDuplex
			// 
			//this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(501, 284);
			this.FillColor = 0;
			//this.Menu = this.MainMenu1;
			this.Name = "frmPrintDuplex";
			this.Text = "Print";
			this.Load += new System.EventHandler(this.frmPrintDuplex_Load);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCLabel lblDuplex;
		private FCLabel fcLabel1;
		private FCButton btnProcess;
	}
}
