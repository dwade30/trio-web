﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Microsoft.VisualBasic;
using fecherFoundation;

namespace Global
{
	public class cBankController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cBank GetBank(int lngID)
		{
			cBank GetBank = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cBank bk = new cBank();
				rsLoad.OpenRecordset("select * from Banks where id = " + FCConvert.ToString(lngID), "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					bk = new cBank();
					bk.AccountType = FCConvert.ToString(rsLoad.Get_Fields_String("AccountType"));
					// TODO: Check the table for the column [Balance] and replace with corresponding Get_Field method
					bk.Balance = Conversion.Val(rsLoad.Get_Fields("Balance"));
					bk.BankAccountNumber = FCConvert.ToString(rsLoad.Get_Fields_String("BankAccountNumber"));
					// TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					bk.BankNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("BankNumber"))));
					bk.CashAccount = FCConvert.ToString(rsLoad.Get_Fields_String("CashAccount"));
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					bk.Fund = FCConvert.ToString(rsLoad.Get_Fields("Fund"));
					bk.IsCurrentBank = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CurrentBank"));
					bk.Name = FCConvert.ToString(rsLoad.Get_Fields_String("Name"));
					bk.RoutingNumber = FCConvert.ToString(rsLoad.Get_Fields_String("RoutingNumber"));
					bk.StatementDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatementDate"));
					bk.TownCashAccount = FCConvert.ToString(rsLoad.Get_Fields_String("TownCashAccount"));
					bk.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					bk.IsUpdated = false;
				}
				GetBank = bk;
				return GetBank;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetBank;
		}

		public cBank GetBankByNumber(int lngNumber)
		{
			cBank GetBankByNumber = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cBank bk = new cBank();
				rsLoad.OpenRecordset("select * from Banks where BankNumber = " + FCConvert.ToString(lngNumber), "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					bk = new cBank();
					bk.AccountType = FCConvert.ToString(rsLoad.Get_Fields_String("AccountType"));
					// TODO: Check the table for the column [Balance] and replace with corresponding Get_Field method
					bk.Balance = Conversion.Val(rsLoad.Get_Fields("Balance"));
					bk.BankAccountNumber = FCConvert.ToString(rsLoad.Get_Fields_String("BankAccountNumber"));
					// TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					bk.BankNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("BankNumber"))));
					bk.CashAccount = FCConvert.ToString(rsLoad.Get_Fields_String("CashAccount"));
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					bk.Fund = FCConvert.ToString(rsLoad.Get_Fields("Fund"));
					bk.IsCurrentBank = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CurrentBank"));
					bk.Name = FCConvert.ToString(rsLoad.Get_Fields_String("Name"));
					bk.RoutingNumber = FCConvert.ToString(rsLoad.Get_Fields_String("RoutingNumber"));
					bk.StatementDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatementDate"));
					bk.TownCashAccount = FCConvert.ToString(rsLoad.Get_Fields_String("TownCashAccount"));
					bk.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					bk.IsUpdated = false;
				}
				GetBankByNumber = bk;
				return GetBankByNumber;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetBankByNumber;
		}

		public cBank GetCurrentBank()
		{
			cBank GetCurrentBank = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				cBank bk = new cBank();
				rsLoad.OpenRecordset("select * from Banks where CurrentBank = 1 and rtrim(Name) <> '' ", "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					bk = new cBank();
					bk.AccountType = FCConvert.ToString(rsLoad.Get_Fields_String("AccountType"));
					// TODO: Check the table for the column [Balance] and replace with corresponding Get_Field method
					bk.Balance = Conversion.Val(rsLoad.Get_Fields("Balance"));
					bk.BankAccountNumber = FCConvert.ToString(rsLoad.Get_Fields_String("BankAccountNumber"));
					// TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					bk.BankNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("BankNumber"))));
					bk.CashAccount = FCConvert.ToString(rsLoad.Get_Fields_String("CashAccount"));
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					bk.Fund = FCConvert.ToString(rsLoad.Get_Fields("Fund"));
					bk.IsCurrentBank = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CurrentBank"));
					bk.Name = FCConvert.ToString(rsLoad.Get_Fields_String("Name"));
					bk.RoutingNumber = FCConvert.ToString(rsLoad.Get_Fields_String("RoutingNumber"));
					bk.StatementDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatementDate"));
					bk.TownCashAccount = FCConvert.ToString(rsLoad.Get_Fields_String("TownCashAccount"));
					bk.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					bk.IsUpdated = false;
				}
				GetCurrentBank = bk;
				return GetCurrentBank;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetCurrentBank;
		}

		public void SetCurrentBank(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rssave = new clsDRWrapper();
				rssave.Execute("update banks set currentbank = 0", "Budgetary");
				rssave.Execute("Update banks set currentbank = 1 where id = " + FCConvert.ToString(lngID), "Budgetary");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public cGenericCollection GetBanks()
		{
			cGenericCollection GetBanks = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cGenericCollection gColl = new cGenericCollection();
				clsDRWrapper rsLoad = new clsDRWrapper();
				cBank bk;
				rsLoad.OpenRecordset("select * from banks where isnull(Name,'') <> '' order by banknumber", "Budgetary");
				while (!rsLoad.EndOfFile())
				{
					bk = new cBank();
					bk.AccountType = FCConvert.ToString(rsLoad.Get_Fields_String("AccountType"));
					// TODO: Check the table for the column [Balance] and replace with corresponding Get_Field method
					bk.Balance = Conversion.Val(rsLoad.Get_Fields("Balance"));
					bk.BankAccountNumber = FCConvert.ToString(rsLoad.Get_Fields_String("BankAccountNumber"));
					// TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
					bk.BankNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields("BankNumber"))));
					bk.CashAccount = FCConvert.ToString(rsLoad.Get_Fields_String("CashAccount"));
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					bk.Fund = FCConvert.ToString(rsLoad.Get_Fields("Fund"));
					bk.IsCurrentBank = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("CurrentBank"));
					bk.Name = FCConvert.ToString(rsLoad.Get_Fields_String("Name"));
					bk.RoutingNumber = FCConvert.ToString(rsLoad.Get_Fields_String("RoutingNumber"));
					bk.StatementDate = FCConvert.ToString(rsLoad.Get_Fields_DateTime("StatementDate"));
					bk.TownCashAccount = FCConvert.ToString(rsLoad.Get_Fields_String("TownCashAccount"));
					bk.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					bk.IsUpdated = false;
					gColl.AddItem(bk);
					rsLoad.MoveNext();
				}
				GetBanks = gColl;
				return GetBanks;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetBanks;
		}

		public cBankCheckHistory GetBankCheckHistoryByBankCheckType(int lngBankID, string strCheckType)
		{
			cBankCheckHistory GetBankCheckHistoryByBankCheckType = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				//FC:FINAL:MSH - issue #767: initialized object not equal to null and next work will be incorrect
				//cBankCheckHistory bcHistory = new cBankCheckHistory();
				cBankCheckHistory bcHistory = null;
				rsLoad.OpenRecordset("select * from BankCheckHistory where Bankid = " + FCConvert.ToString(lngBankID) + " and CheckType = '" + strCheckType + "'", "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					bcHistory = new cBankCheckHistory();
					bcHistory.BankID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("BankID"));
					bcHistory.LastCheckNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("LastCheckNumber"))));
					bcHistory.CheckType = FCConvert.ToString(rsLoad.Get_Fields_String("CheckType"));
					bcHistory.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					bcHistory.IsUpdated = false;
				}
				GetBankCheckHistoryByBankCheckType = bcHistory;
				return GetBankCheckHistoryByBankCheckType;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
			return GetBankCheckHistoryByBankCheckType;
		}

		public void SaveBankCheckHistory(ref cBankCheckHistory bcHistory)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (bcHistory.IsDeleted)
				{
					DeleteBankCheckHistory(bcHistory.ID);
					return;
				}
				clsDRWrapper rssave = new clsDRWrapper();
				rssave.OpenRecordset("select * from BankCheckHistory where id = " + bcHistory.ID, "Budgetary");
				if (!rssave.EndOfFile())
				{
					rssave.Edit();
				}
				else
				{
					if (bcHistory.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Bank check history record not found";
						return;
					}
					rssave.AddNew();
				}
				rssave.Set_Fields("BankID", bcHistory.BankID);
				rssave.Set_Fields("LastCheckNumber", bcHistory.LastCheckNumber);
				rssave.Set_Fields("CheckType", bcHistory.CheckType);
				rssave.Update();
				bcHistory.ID = rssave.Get_Fields_Int32("ID");
				bcHistory.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}

		public void DeleteBankCheckHistory(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from BankCheckHistory where id = " + FCConvert.ToString(lngID), "Budgetary");
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = Information.Err(ex).Number;
				strLastError = Information.Err(ex).Description;
			}
		}
	}
}
