﻿using System.Data.Common;
using System.Transactions;
using System.Runtime.InteropServices;
using System.Data;
using System;
using fecherFoundation;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Data.Odbc;
using System.Collections.Generic;
using System.Web.Security.AntiXss;
using TWSharedLibrary;

namespace Global
{
	public class DataReader : IDisposable
	{
		#region "COM GUIDs"
		// These  GUIDs provide the COM identity for this class
		// and its COM interfaces. If you change them, existing
		// clients will no longer be able to access the class.
		public const string ClassId = "e030c140-4fc6-466b-88d8-efe166393f78";
		public const string InterfaceId = "55996827-3b70-45dd-b643-8ee356650dc2";
		#endregion

		public const string EventsId = "4fc11e52-ddcc-4776-84d9-66784ef78b5c";
		// A creatable COM class must have a Public Sub New()
		// with no parameters, otherwise, the class will not be
		// registered in the COM registry and cannot be created
		// via CreateObject.
		public DataReader() : base()
		{
			if (Statics.ConnectionPools == null)
			{
				Statics.ConnectionPools = new ConnectionPool();
			}
			strMegaGroup = Statics.ConnectionPools.DefaultMegaGroup;
			strGroup = Statics.ConnectionPools.DefaultGroup;
			this.DefaultGroupChanged += DataReader_DefaultGroupChanged;
			this.DefaultMegaGroupChanged += DataReader_DefaultMegaGroupChanged;
		}

		private void DataReader_DefaultMegaGroupChanged(object sender, string e)
		{
			strMegaGroup = e;
		}

		private void DataReader_DefaultGroupChanged(object sender, string e)
		{
			strGroup = e;
		}

		private event EventHandler<string> DefaultGroupChanged;
		private event EventHandler<string> DefaultMegaGroupChanged;

		private System.Data.DataTable TheDataTable = null;
		private string strCurrentTableName = "";
		private int lngCurrentRow = -1;
		private string curCon = "";
		private DbConnection RecCon = null;
		private string strCurSelect = "";
		private System.Data.Common.DataAdapter TheDataAdapter = null;
		private string strInstanceDefaultDatabase = "";
		private System.Transactions.DependentTransaction DependentScope = null;
		private System.Transactions.TransactionScope tranScope = null;
		private bool boolNoMatch = true;
		private CommittableTransaction theTransaction = null;
		private System.Transactions.Transaction oldAmbient = null;
		private int intLastErrorCode = 0;
		private string strLastErrorDescription = "";
		private string strMegaGroup = "";
		private string strGroup = "Live";
		private DbCommand TheStoredProcedure = null;
		private bool boolAddedDefaults = false;
		private bool boolJustDeleted = false;

		public void Dispose()
		{
			if (TheDataTable != null)
			{
				TheDataTable.Dispose();
				TheDataTable = null;
			}
			if (TheDataAdapter != null)
			{
				TheDataAdapter.Dispose();
				TheDataAdapter = null;
			}
			if (TheStoredProcedure != null)
			{
				TheStoredProcedure.Dispose();
				TheStoredProcedure = null;
			}
			if (RecCon != null)
			{
				RecCon.Dispose();
				RecCon = null;
			}
			oldAmbient = null;
			if (DependentScope != null)
			{
				DependentScope.Dispose();
				DependentScope = null;
			}
			if (theTransaction != null)
			{
				theTransaction.Dispose();
				theTransaction = null;
			}
			if (tranScope != null)
			{
				tranScope.Dispose();
				tranScope = null;
			}
		}

		public bool ContainsField(string fieldName)
		{
			return TheDataTable.Columns.Contains(fieldName);
		}

		public void ReOrder(string strOrderBy)
		{
			try
			{
				ReOrderAndFilter("", strOrderBy);
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		public void ReOrderAndFilter(string strFilter, string strOrderBy)
		{
			try
			{
				if (TheDataTable != null)
				{
					if (TheDataTable.Rows.Count > 0)
					{
						if (strFilter.Trim() != "" || strOrderBy.Trim() != "")
						{
							if (strFilter.Trim() != "")
							{
								TheDataTable.DefaultView.RowFilter = strFilter;
							}
							if (strOrderBy.Trim() != "")
							{
								TheDataTable.DefaultView.Sort = strOrderBy;
							}
							DataTable tTab = TheDataTable.DefaultView.ToTable();
							if (tTab != null)
							{
								tTab.PrimaryKey = TheDataTable.PrimaryKey;
								TheDataTable = tTab;
								lngCurrentRow = -1;
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		public void Filter(string strFilter)
		{
			try
			{
				ReOrderAndFilter(strFilter, "");
			}
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
		}

		private class cAddress
		{
			public long ID
			{
				get
				{
					return m_ID;
				}
				set
				{
					m_ID = value;
				}
			}

			private long m_ID;

			public long PartyID
			{
				get
				{
					return m_PartyID;
				}
				set
				{
					m_PartyID = value;
				}
			}

			private long m_PartyID;

			public string Address1
			{
				get
				{
					return m_Address1;
				}
				set
				{
					m_Address1 = value;
				}
			}

			private string m_Address1 = string.Empty;

			public string Address2
			{
				get
				{
					return m_Address2;
				}
				set
				{
					m_Address2 = value;
				}
			}

			private string m_Address2 = string.Empty;

			public string Address3
			{
				get
				{
					return m_Address3;
				}
				set
				{
					m_Address3 = value;
				}
			}

			private string m_Address3 = string.Empty;

			public string City
			{
				get
				{
					return m_City;
				}
				set
				{
					m_City = value;
				}
			}

			private string m_City = string.Empty;

			public string State
			{
				get
				{
					return m_State;
				}
				set
				{
					m_State = value;
				}
			}

			private string m_State = string.Empty;

			public string Zip
			{
				get
				{
					return m_Zip;
				}
				set
				{
					m_Zip = value;
				}
			}

			private string m_Zip = string.Empty;

			public string Country
			{
				get
				{
					return m_Country;
				}
				set
				{
					m_Country = value;
				}
			}

			private string m_Country = string.Empty;

			public string OverrideName
			{
				get
				{
					return m_OverrideName;
				}
				set
				{
					m_OverrideName = value;
				}
			}

			private string m_OverrideName = string.Empty;

			public string AddressType
			{
				get
				{
					return m_AddressType;
				}
				set
				{
					m_AddressType = value;
				}
			}

			private string m_AddressType = string.Empty;

			public bool Seasonal
			{
				get
				{
					return m_Seasonal;
				}
				set
				{
					m_Seasonal = value;
				}
			}

			private bool m_Seasonal;

			public string ProgModule
			{
				get
				{
					return m_ProgModule;
				}
				set
				{
					m_ProgModule = value;
				}
			}

			private string m_ProgModule = string.Empty;

			public string Comment
			{
				get
				{
					return m_Comment;
				}
				set
				{
					m_Comment = value;
				}
			}

			private string m_Comment = string.Empty;

			public int StartMonth
			{
				get
				{
					return m_StartMonth;
				}
				set
				{
					m_StartMonth = value;
				}
			}

			private int m_StartMonth;

			public int StartDay
			{
				get
				{
					return m_StartDay;
				}
				set
				{
					m_StartDay = value;
				}
			}

			private int m_StartDay;

			public int EndMonth
			{
				get
				{
					return m_EndMonth;
				}
				set
				{
					m_EndMonth = value;
				}
			}

			private int m_EndMonth;

			public int EndDay
			{
				get
				{
					return m_EndDay;
				}
				set
				{
					m_EndDay = value;
				}
			}

			private int m_EndDay;

			public long ModAccountID
			{
				get
				{
					return m_ModAccountID;
				}
				set
				{
					m_ModAccountID = value;
				}
			}

			private long m_ModAccountID;
		}

		private cAddress GetAddress(DataRow[] tRows, string strProgModule = "", int lngModAccountID = 0, string strDate = "")
		{
			cAddress tAddress = null;
			int intCurDay;
			int intCurMonth;
            if (strDate == "")
            {
                //FC:FINAL:AM: performance
                //strDate = string.Format(System.DateTime.Today.ToString(), "MM/dd/yyyy");
                intCurDay = DateTime.Today.Day;
                intCurMonth = DateTime.Today.Month;
            }
            else
            {
                intCurDay = FCConvert.ToDateTime(strDate).Day;
                intCurMonth = FCConvert.ToDateTime(strDate).Month;
            }
			if (tRows.Length > 0)
			{
				//DataRow tRow;
				bool boolSoFar;
				bool boolBetterMatch;
				foreach (var tRow in tRows)
				{
					boolSoFar = true;
					if (strProgModule.ToLower() != tRow["ProgModule"].ToString().ToLower() & tRow["ProgModule"].ToString().Trim() != "")
					{
						boolSoFar = false;
					}
					if (lngModAccountID != Conversion.Val(tRow["ModAccountID"].ToString()) & Conversion.Val(tRow["ModAccountID"].ToString()) != 0)
					{
						boolSoFar = false;
					}
					if (boolSoFar)
					{
						if (!Convert.IsDBNull(tRow["Seasonal"]))
						{
							if ((bool)tRow["Seasonal"])
							{
								if (Conversion.Val(tRow["StartMonth"].ToString()) <= Conversion.Val(tRow["EndMonth"].ToString()))
								{
									if (intCurMonth >= Conversion.Val(tRow["StartMonth"].ToString()) & intCurMonth <= Conversion.Val(tRow["EndMonth"].ToString()))
									{
										if (intCurDay >= Conversion.Val(tRow["StartDay"].ToString()) || intCurMonth > Conversion.Val(tRow["StartMonth"].ToString()))
										{
											if (!(intCurDay <= Conversion.Val(tRow["EndDay"].ToString()) || intCurMonth < Conversion.Val(tRow["EndMonth"].ToString())))
											{
												boolSoFar = false;
											}
										}
										else
										{
											boolSoFar = false;
										}
									}
									else
									{
										boolSoFar = false;
									}
								}
								else
								{
									if (intCurMonth >= FCConvert.ToInt32(tRow["StartMonth"]) || intCurMonth <= FCConvert.ToInt32(tRow["EndMonth"]))
									{
										if (intCurDay >= FCConvert.ToInt32(tRow["StartDay"]) || intCurMonth != FCConvert.ToInt32(tRow["StartMonth"]))
										{
											if (!(intCurDay <= FCConvert.ToInt32(tRow["EndDay"]) || intCurMonth != FCConvert.ToInt32(tRow["EndMonth"])))
											{
												boolSoFar = false;
											}
										}
										else
										{
											boolSoFar = false;
										}
									}
									else
									{
										boolSoFar = false;
									}
								}
							}
						}
					}
					if (boolSoFar)
					{
						if (tAddress == null)
						{
							cAddress tAdd = new cAddress();
							tAdd.Address1 = tRow["Address1"].ToString();
							tAdd.Address2 = tRow["Address2"].ToString();
							tAdd.Address3 = tRow["Address3"].ToString();
							tAdd.AddressType = tRow["AddressType"].ToString();
							tAdd.City = tRow["City"].ToString();
							tAdd.Comment = tRow["Comment"].ToString();
							tAdd.Country = tRow["Country"].ToString();
							tAdd.EndDay = FCConvert.ToInt32(Conversion.Val(tRow["endday"].ToString()));
							tAdd.EndMonth = FCConvert.ToInt32(Conversion.Val(tRow["endmonth"].ToString()));
							tAdd.ID = (long)Conversion.Val(tRow["id"]);
							tAdd.ModAccountID = (long)Conversion.Val(tRow["modaccountid"].ToString());
							tAdd.OverrideName = tRow["overridename"].ToString();
							tAdd.PartyID = (long)Conversion.Val(tRow["partyid"]);
							tAdd.ProgModule = tRow["progmodule"].ToString();
							if (!Convert.IsDBNull(tRow["seasonal"]))
							{
								tAdd.Seasonal = (bool)tRow["seasonal"];
							}
							else
							{
								tAdd.Seasonal = false;
							}
							tAdd.StartDay = FCConvert.ToInt32(Conversion.Val(tRow["startday"].ToString()));
							tAdd.StartMonth = FCConvert.ToInt32(Conversion.Val(tRow["Startmonth"].ToString()));
							tAdd.State = tRow["state"].ToString();
							tAdd.Zip = tRow["zip"].ToString();
							tAddress = tAdd;
						}
						else
						{
							//compare for best match
							cAddress tAdd = new cAddress();
							tAdd.Address1 = tRow["Address1"].ToString();
							tAdd.Address2 = tRow["Address2"].ToString();
							tAdd.Address3 = tRow["Address3"].ToString();
							tAdd.AddressType = tRow["AddressType"].ToString();
							tAdd.City = tRow["City"].ToString();
							tAdd.Comment = tRow["Comment"].ToString();
							tAdd.Country = tRow["Country"].ToString();
							tAdd.EndDay = FCConvert.ToInt32(Conversion.Val(tRow["endday"].ToString()));
							tAdd.EndMonth = FCConvert.ToInt32(Conversion.Val(tRow["endmonth"].ToString()));
							tAdd.ID = (long)Conversion.Val(tRow["id"]);
							tAdd.ModAccountID = (long)Conversion.Val(tRow["modaccountid"].ToString());
							tAdd.OverrideName = tRow["overridename"].ToString();
							tAdd.PartyID = (long)Conversion.Val(tRow["partyid"]);
							tAdd.ProgModule = tRow["progmodule"].ToString();
							if (!Convert.IsDBNull(tRow["seasonal"]))
							{
								tAdd.Seasonal = (bool)tRow["seasonal"];
							}
							else
							{
								tAdd.Seasonal = false;
							}
							tAdd.StartDay = FCConvert.ToInt32(Conversion.Val(tRow["startday"].ToString()));
							tAdd.StartMonth = FCConvert.ToInt32(Conversion.Val(tRow["Startmonth"].ToString()));
							tAdd.State = tRow["state"].ToString();
							tAdd.Zip = tRow["zip"].ToString();
							boolBetterMatch = true;
							if (lngModAccountID != 0)
							{
								if (tAddress.ModAccountID == lngModAccountID & tAdd.ModAccountID != lngModAccountID)
								{
									boolBetterMatch = false;
								}
							}
							if (strProgModule != "")
							{
								if (tAddress.ProgModule.ToLower() == strProgModule.ToLower() & tAdd.ProgModule.ToLower() != strProgModule.ToLower())
								{
									boolBetterMatch = false;
								}
							}
							if (boolBetterMatch)
							{
								if (tAddress.Seasonal & !tAdd.Seasonal)
								{
									boolBetterMatch = false;
								}
							}
							if (boolBetterMatch)
							{
								tAddress = tAdd;
							}
						}
					}
				}
			}
			return tAddress;
		}

		public void InsertAddress(string strIDFields, string strPrefixes, string strProgModule = "", bool boolUseIDAsModAccountId = false, string strReOrderBy = "")
		{
			ClearErrors();
			try
			{
				if (TheDataTable != null)
				{
					if (TheDataTable.Rows.Count > 0)
					{
						DataReader tRead = new DataReader();
						DataReader tempRead = new DataReader();
						string[] strAry;
						string[] strPrefixAry;
						string strIDField;
						string strPrefix;
						strAry = Strings.Split(strIDFields, ",");
						strPrefixAry = Strings.Split(strPrefixes, ",");
						tRead.GroupName = this.GroupName;
						tRead.MegaGroup = this.MegaGroup;
						tempRead.GroupName = this.GroupName;
						tempRead.MegaGroup = this.MegaGroup;
						int intIndex;
						for (intIndex = 0; intIndex <= Information.UBound(strAry); intIndex++)
						{
							strIDField = strAry[intIndex];
							//FC:FINAL:DDU:#2325 - fix error when strPrefixAry is null
							strPrefix = strPrefixAry.Length > intIndex ? strPrefixAry[intIndex] : "";
							if (tRead.OpenRecordset("select * from parties order by id", "CentralParties"))
							{
								//tempRead.OpenRecordset("select * from addresses where partyid = " & tRow["id"], "CentralParties")
								tempRead.OpenRecordset("select * from addresses order by id", "CentralParties");
								string strFieldName = "";
								DataColumn tCol = null;
								strFieldName = strPrefix + "Address1";
								if (!TheDataTable.Columns.Contains(strFieldName))
								{
									tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
									tCol.AllowDBNull = true;
									tCol.ExtendedProperties.Add("unmappedcolumn", true);
								}
								strFieldName = strPrefix + "Address2";
								if (!TheDataTable.Columns.Contains(strFieldName))
								{
									tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
									tCol.AllowDBNull = true;
									tCol.ExtendedProperties.Add("unmappedcolumn", true);
								}
								strFieldName = strPrefix + "Address3";
								if (!TheDataTable.Columns.Contains(strFieldName))
								{
									tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
									tCol.AllowDBNull = true;
									tCol.ExtendedProperties.Add("unmappedcolumn", true);
								}
								strFieldName = strPrefix + "City";
								if (!TheDataTable.Columns.Contains(strFieldName))
								{
									tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
									tCol.AllowDBNull = true;
									tCol.ExtendedProperties.Add("unmappedcolumn", true);
								}
								strFieldName = strPrefix + "State";
								if (!TheDataTable.Columns.Contains(strFieldName))
								{
									tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
									tCol.AllowDBNull = true;
									tCol.ExtendedProperties.Add("unmappedcolumn", true);
								}
								strFieldName = strPrefix + "Zip";
								if (!TheDataTable.Columns.Contains(strFieldName))
								{
									tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
									tCol.AllowDBNull = true;
									tCol.ExtendedProperties.Add("unmappedcolumn", true);
								}
								DataRow[] tRows = null;
								DataRow tRow = null;
								DataRow cRow = null;
								string strTemp = "";
								for (int x = 0; x <= TheDataTable.Rows.Count - 1; x++)
								{
									if (TheDataTable.Rows[x][strIDField].ToString().Length > 0)
									{
										tRows = tRead.FindFirstRecord("id = " + TheDataTable.Rows[x][strIDField]);
										if (tRows != null)
										{
											if (tRows.Length > 0)
											{
												cRow = TheDataTable.Rows[x];
												tRow = tRows[0];
												cAddress tAdd = null;
												if (!boolUseIDAsModAccountId)
												{
													tAdd = GetAddress(tempRead.FindFirstRecord("partyid =" + tRow["id"]), strProgModule);
												}
												else
												{
													tAdd = GetAddress(tempRead.FindFirstRecord("partyid =" + tRow["id"]), strProgModule, FCConvert.ToInt32(tRow["id"]));
												}
												if (tAdd != null)
												{
													strFieldName = strPrefix + "Address1";
													cRow[strFieldName] = tAdd.Address1;
													strFieldName = strPrefix + "Address2";
													cRow[strFieldName] = tAdd.Address2;
													strFieldName = strPrefix + "Address3";
													cRow[strFieldName] = tAdd.Address3;
													strFieldName = strPrefix + "City";
													cRow[strFieldName] = tAdd.City;
													strFieldName = strPrefix + "State";
													cRow[strFieldName] = tAdd.State;
													strFieldName = strPrefix + "Zip";
													cRow[strFieldName] = tAdd.Zip;
												}
											}
										}
									}
								}
							}
						}
						if (strReOrderBy != "")
						{
							ReOrder(strReOrderBy);
						}
					}
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				this.ErrorDescription = ex.Message;
				this.ErrorNumber = -1;
			}
		}

		public void InsertName(string strIDFields, string strPrefixes, bool boolIncludeEmail = false, bool boolNameAsFull = false, bool boolIncludeAddress = false, string strProgModule = "", bool boolUseIDAsModAccountId = false, string strReOrderBy = "", bool boolLastFirst = false, string strFilter = "", bool boolFullNameBoth = false)
		{
			ClearErrors();
			try
			{
				if (TheDataTable != null)
				{
                    if (TheDataTable.Rows.Count > 0)
                    {
                        DataReader tRead = new DataReader();
                        DataReader tempRead = new DataReader();
                        string[] strAry;
                        string[] strPrefixAry;
                        string strIDField;
                        string strPrefix;
                        strAry = Strings.Split(strIDFields, ",");
                        //FC:FINAL:JTA - #471 - use string.Split instead of Strings.Split. Previous fix in Information.Strings class was incorrect.
                        //strPrefixAry = Strings.Split(strPrefixes, ",");
                        strPrefixAry = strPrefixes.Split(',');
                        tRead.GroupName = this.GroupName;
                        tRead.MegaGroup = this.MegaGroup;
                        tempRead.GroupName = this.GroupName;
                        tempRead.MegaGroup = this.MegaGroup;
                        int intIndex;
                        for (intIndex = 0; intIndex <= Information.UBound(strAry); intIndex++)
                        {
                            strIDField = strAry[intIndex];
                            strPrefix = strPrefixAry[intIndex];
                            //FC:FINAL:JEI:IT500: Performance
                            if (true)//tRead.OpenRecordset("select * from parties order by id", "CentralParties"))
                            {
                                //tempRead.OpenRecordset("select * from addresses where partyid = " & tRow["id"], "CentralParties")
                                if (boolIncludeAddress)
                                {
                                    //FC:FINAL:JEI:IT500: Performance
                                    //tempRead.OpenRecordset("select * from addresses order by id", "CentralParties");
                                }
                                string strFieldName = "";
                                DataColumn tCol = null;
                                if (boolIncludeEmail)
                                {
                                    strFieldName = strPrefix + "EMail";
                                    if (!TheDataTable.Columns.Contains(strFieldName))
                                    {
                                        tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
                                        tCol.AllowDBNull = true;
                                        tCol.ExtendedProperties.Add("unmappedcolumn", true);
                                    }
                                }
                                if (!boolNameAsFull & !boolFullNameBoth)
                                {
                                    strFieldName = strPrefix + "FirstName";
                                    if (!TheDataTable.Columns.Contains(strFieldName))
                                    {
                                        tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
                                        tCol.AllowDBNull = true;
                                        tCol.ExtendedProperties.Add("unmappedcolumn", true);
                                    }
                                    strFieldName = strPrefix + "MiddleName";
                                    if (!TheDataTable.Columns.Contains(strFieldName))
                                    {
                                        tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
                                        tCol.AllowDBNull = true;
                                        tCol.ExtendedProperties.Add("unmappedcolumn", true);
                                    }
                                    strFieldName = strPrefix + "LastName";
                                    if (!TheDataTable.Columns.Contains(strFieldName))
                                    {
                                        tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
                                        tCol.AllowDBNull = true;
                                        tCol.ExtendedProperties.Add("unmappedcolumn", true);
                                    }
                                    strFieldName = strPrefix + "Designation";
                                    if (!TheDataTable.Columns.Contains(strFieldName))
                                    {
                                        tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
                                        tCol.AllowDBNull = true;
                                        tCol.ExtendedProperties.Add("unmappedcolumn", true);
                                    }
                                }
                                else
                                {
                                    strFieldName = strPrefix + "FullName";
                                    if (!TheDataTable.Columns.Contains(strFieldName))
                                    {
                                        tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
                                        tCol.AllowDBNull = true;
                                        tCol.ExtendedProperties.Add("unmappedcolumn", true);
                                    }
                                    if (boolFullNameBoth)
                                    {
                                        strFieldName = strPrefix + "FullNameLastFirst";
                                        if (!TheDataTable.Columns.Contains(strFieldName))
                                        {
                                            tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
                                            tCol.AllowDBNull = true;
                                            tCol.ExtendedProperties.Add("unmappedcolumn", true);
                                        }
                                    }
                                }
                                if (boolIncludeAddress)
                                {
                                    strFieldName = strPrefix + "Address1";
                                    if (!TheDataTable.Columns.Contains(strFieldName))
                                    {
                                        tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
                                        tCol.AllowDBNull = true;
                                        tCol.ExtendedProperties.Add("unmappedcolumn", true);
                                    }
                                    strFieldName = strPrefix + "Address2";
                                    if (!TheDataTable.Columns.Contains(strFieldName))
                                    {
                                        tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
                                        tCol.AllowDBNull = true;
                                        tCol.ExtendedProperties.Add("unmappedcolumn", true);
                                    }
                                    strFieldName = strPrefix + "Address3";
                                    if (!TheDataTable.Columns.Contains(strFieldName))
                                    {
                                        tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
                                        tCol.AllowDBNull = true;
                                        tCol.ExtendedProperties.Add("unmappedcolumn", true);
                                    }
                                    strFieldName = strPrefix + "City";
                                    if (!TheDataTable.Columns.Contains(strFieldName))
                                    {
                                        tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
                                        tCol.AllowDBNull = true;
                                        tCol.ExtendedProperties.Add("unmappedcolumn", true);
                                    }
                                    strFieldName = strPrefix + "State";
                                    if (!TheDataTable.Columns.Contains(strFieldName))
                                    {
                                        tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
                                        tCol.AllowDBNull = true;
                                        tCol.ExtendedProperties.Add("unmappedcolumn", true);
                                    }
                                    strFieldName = strPrefix + "Zip";
                                    if (!TheDataTable.Columns.Contains(strFieldName))
                                    {
                                        tCol = TheDataTable.Columns.Add(strFieldName, typeof(string));
                                        tCol.AllowDBNull = true;
                                        tCol.ExtendedProperties.Add("unmappedcolumn", true);
                                    }
                                }
                                //DataRow[] tRows = null;
                                DataRow tRow = null;
                                DataRow cRow = null;
                                string strTemp = "";
                                for (int x = 0; x <= TheDataTable.Rows.Count - 1; x++)
                                {
                                    if (TheDataTable.Columns[strIDField] != null && TheDataTable.Rows[x][strIDField].ToString().Length > 0)
                                    {
                                        //FC:FINAL:JEI:IT500: Performance
                                        //if (tRead.OpenRecordset("select * from parties where id = " + TheDataTable.Rows[x][strIDField] + " order by id", "CentralParties"))
                                        if (tRead.OpenRecordset("select FirstName, MiddleName, LastName, Designation, EMail, PartyType, ID from parties where id = " + TheDataTable.Rows[x][strIDField], "CentralParties"))
                                        {
                                            //FC:FINAL:AM:#2020 - there is only one record in tRead
                                            //tRows = tRead.FindFirstRecord("id = " + TheDataTable.Rows[x][strIDField]);
                                            //if (tRows != null)
                                            if (tRead.MoveNext())
                                            {
                                                //if (tRows.Length > 0)
                                                {
                                                    cRow = TheDataTable.Rows[x];
                                                    //tRow = tRows[0];
                                                    tRow = tRead.TheDataTable.Rows[0];
                                                    if (boolIncludeEmail)
                                                    {
                                                        strFieldName = strPrefix + "EMail";
                                                        cRow[strFieldName] = tRow["email"];
                                                    }
                                                    if (!boolNameAsFull & !boolFullNameBoth)
                                                    {
                                                        strFieldName = strPrefix + "FirstName";
                                                        cRow[strFieldName] = tRow["FirstName"];
                                                        strFieldName = strPrefix + "MiddleName";
                                                        cRow[strFieldName] = tRow["MiddleName"];
                                                        strFieldName = strPrefix + "LastName";
                                                        cRow[strFieldName] = tRow["LastName"];
                                                        strFieldName = strPrefix + "Designation";
                                                        cRow[strFieldName] = tRow["Designation"];
                                                    }
                                                    else
                                                    {
                                                        if (!boolFullNameBoth)
                                                        {
                                                            strFieldName = strPrefix + "FullName";
                                                            if (!boolLastFirst || Conversion.Val(tRow["partytype"].ToString()) == 1)
                                                            {
                                                                strTemp = Strings.Trim(Strings.Trim(Strings.Trim(tRow["FirstName"] + " " + tRow["MiddleName"]) + " " + tRow["LastName"]) + " " + tRow["Designation"]);
                                                            }
                                                            else
                                                            {
                                                                if (Strings.Trim(tRow["lastname"].ToString()) != "")
                                                                {
                                                                    strTemp = Strings.Trim(Strings.Trim(Strings.Trim(tRow["LastName"] + ", " + tRow["Firstname"]) + " " + tRow["Middlename"]) + " " + tRow["Designation"]);
                                                                }
                                                                else
                                                                {
                                                                    strTemp = Strings.Trim(Strings.Trim(tRow["Firstname"] + " " + tRow["Middlename"]) + " " + tRow["Designation"]);
                                                                }
                                                            }
                                                            cRow[strFieldName] = strTemp;
                                                        }
                                                        else
                                                        {
                                                            strFieldName = strPrefix + "FullName";
                                                            strTemp = Strings.Trim(Strings.Trim(Strings.Trim(tRow["FirstName"] + " " + tRow["MiddleName"]) + " " + tRow["LastName"]) + " " + tRow["Designation"]);
                                                            cRow[strFieldName] = strTemp;
                                                            strFieldName = strPrefix + "FullNameLastFirst";
                                                            if (Conversion.Val(tRow["partytype"].ToString()) == 1)
                                                            {
                                                                strTemp = Strings.Trim(Strings.Trim(Strings.Trim(tRow["FirstName"] + " " + tRow["MiddleName"]) + " " + tRow["LastName"]) + " " + tRow["Designation"]);
                                                            }
                                                            else
                                                            {
                                                                if (Strings.Trim(tRow["lastname"].ToString()) != "")
                                                                {
                                                                    strTemp = Strings.Trim(Strings.Trim(Strings.Trim(tRow["LastName"] + ", " + tRow["Firstname"]) + " " + tRow["Middlename"]) + " " + tRow["Designation"]);
                                                                }
                                                                else
                                                                {
                                                                    strTemp = Strings.Trim(Strings.Trim(tRow["Firstname"] + " " + tRow["Middlename"]) + " " + tRow["Designation"]);
                                                                }
                                                            }
                                                            cRow[strFieldName] = strTemp;
                                                        }
                                                    }
                                                    if (boolIncludeAddress)
                                                    {
                                                        //FC:FINAL:JEI:IT500: Performance
                                                        if (tempRead.OpenRecordset("select * from addresses where partyid = " + tRow["id"], "CentralParties"))
                                                        {
                                                            cAddress tAdd = null;
                                                            //FC:FINAL:AM:#2020 - there is only one record in tempRead
                                                            if (tempRead.MoveNext())
                                                            {
                                                                DataRow[] tempRow = new DataRow[1];
                                                                tempRow[0] = tempRead.TheDataTable.Rows[0];
                                                                if (!boolUseIDAsModAccountId)
                                                                {
                                                                    //tAdd = GetAddress(tempRead.FindFirstRecord("partyid =" + tRow["id"]), strProgModule);
                                                                    tAdd = GetAddress(tempRow, strProgModule);
                                                                }
                                                                else
                                                                {
                                                                    //tAdd = GetAddress(tempRead.FindFirstRecord("partyid =" + tRow["id"]), strProgModule, FCConvert.ToInt32(tRow["id"]));
                                                                    tAdd = GetAddress(tempRow, strProgModule, FCConvert.ToInt32(tRow["id"]));
                                                                }
                                                                if (tAdd != null)
                                                                {
                                                                    strFieldName = strPrefix + "Address1";
                                                                    cRow[strFieldName] = tAdd.Address1;
                                                                    strFieldName = strPrefix + "Address2";
                                                                    cRow[strFieldName] = tAdd.Address2;
                                                                    strFieldName = strPrefix + "Address3";
                                                                    cRow[strFieldName] = tAdd.Address3;
                                                                    strFieldName = strPrefix + "City";
                                                                    cRow[strFieldName] = tAdd.City;
                                                                    strFieldName = strPrefix + "State";
                                                                    cRow[strFieldName] = tAdd.State;
                                                                    strFieldName = strPrefix + "Zip";
                                                                    cRow[strFieldName] = tAdd.Zip;
                                                                }
                                                            }
														}
													}
												}
											}
										}
									}
								}
							}
						}
						if (strReOrderBy != "" || strFilter != "")
						{
							ReOrderAndFilter(strFilter, strReOrderBy);
						}
					}
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				this.ErrorDescription = ex.Message;
				this.ErrorNumber = -1;
			}
		}

		public string GetFullDBName(string strDB)
		{
			ConnectionInfo tInfo = null;
			tInfo = Statics.ConnectionPools.GetConnectionInformation(strDB, strGroup, strMegaGroup);
			if (tInfo != null)
			{
				return tInfo.InitialCatalog.ToString();
			}
			else
			{
				return "";
			}
		}

		public string ConnectionInformation(string strDB)
		{
			if (strDB == "")
			{
				strDB = strInstanceDefaultDatabase;
				if (strDB == "")
				{
					strDB = Statics.strDefaultDatabaseAlias;
				}
			}
			return Statics.ConnectionPools.GetConnectionString(strDB, strGroup, strMegaGroup);
		}

		public string[] ConnectionNames
		{
			get
			{
				if (strGroup != "")
				{
					return Statics.ConnectionPools.GetConnectionNames(strGroup, strMegaGroup);
				}
				else
				{
					return new string[] {

					};
				}
			}
		}

		public int MegaGroupsCount()
		{
			if (ConnectionPool.Statics.MegaGroups != null)
			{
				return ConnectionPool.Statics.MegaGroups.Count;
			}
			else
			{
				return 0;
			}
		}

		public ConnectionMegaGroup AvailableMegaGroups(int intIndex)
		{
			if (ConnectionPool.Statics.MegaGroups != null)
			{
				if (ConnectionPool.Statics.MegaGroups.Count > intIndex)
				{
					return ConnectionPool.Statics.MegaGroups[intIndex];
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}

		public string GroupName
		{
			get
			{
				return strGroup;
			}
			set
			{
				strGroup = value;
			}
		}

		public string MegaGroup
		{
			get
			{
				return strMegaGroup;
			}
			set
			{
				strMegaGroup = value;
			}
		}

		public string DefaultMegaGroup
		{
			get
			{
				return Statics.ConnectionPools.DefaultMegaGroup;
			}
			set
			{
				bool boolRaiseEvent = false;
				if (Statics.ConnectionPools.DefaultMegaGroup != value)
				{
					boolRaiseEvent = true;
				}
				Statics.ConnectionPools.DefaultMegaGroup = value;
				strMegaGroup = value;
				if (boolRaiseEvent)
				{
					if (DefaultMegaGroupChanged != null)
					{
						DefaultMegaGroupChanged(this, value);
					}
				}
			}
		}

		public string DefaultGroup
		{
			get
			{
				return Statics.ConnectionPools.DefaultGroup;
			}
			set
			{
				bool boolRaiseEvent = false;
				if (Statics.ConnectionPools.DefaultGroup != value)
				{
					boolRaiseEvent = true;
                    System.Web.Security.AntiXss.AntiXssEncoder.UrlEncode("asfsad");
                }
				Statics.ConnectionPools.DefaultGroup = value;
				strGroup = value;
				if (boolRaiseEvent)
				{
					if (DefaultGroupChanged != null)
					{
						DefaultGroupChanged(this, value);
					}
				}
			}
		}
		
		public bool SetStoredProcedure(string strProcedureName, string strConnectionName)
		{
			if (strConnectionName == "")
			{
				strConnectionName = Statics.strDefaultDatabaseAlias;
			}
			ConnectionInfo tInfo = null;
			tInfo = Statics.ConnectionPools.GetConnectionInformationByName(strConnectionName, strMegaGroup, strGroup);
			if (TheStoredProcedure != null)
			{
				TheStoredProcedure.Dispose();
				TheStoredProcedure = null;
			}
			try
			{
				if (tInfo != null)
				{
					curCon = strConnectionName;
					switch (tInfo.ConnectionType)
					{
						case dbConnectionTypes.SQLServer:
							SqlConnection tCon = Statics.ConnectionPools.GetConnectionByName(strConnectionName, strMegaGroup, strGroup) as SqlConnection;
							if (tCon != null)
							{
								tCon.Open();
								SqlCommand tCMD__1 = new SqlCommand(strProcedureName, tCon);
								tCMD__1.CommandType = CommandType.StoredProcedure;
								SqlCommandBuilder.DeriveParameters(tCMD__1);
								TheStoredProcedure = tCMD__1;
								tCon.Close();
								return true;
							}
							else
							{
								return false;
							}
						case dbConnectionTypes.OLEDB:
							OleDbConnection tCon1 = Statics.ConnectionPools.GetConnectionByName(strConnectionName, strMegaGroup, strGroup) as OleDbConnection;
							if (tCon1 != null)
							{
								tCon1.Open();
								OleDbCommand tCmd__2 = new OleDbCommand(strProcedureName, tCon1);
								tCmd__2.CommandType = CommandType.StoredProcedure;
								OleDbCommandBuilder.DeriveParameters(tCmd__2);
								TheStoredProcedure = tCmd__2;
								tCon1.Close();
								return true;
							}
							else
							{
								return false;
							}
							break;
						case dbConnectionTypes.ODBC:
							OdbcConnection tCon2 = Statics.ConnectionPools.GetConnectionByName(strConnectionName, strMegaGroup, strGroup) as OdbcConnection;
							if (tCon2 != null)
							{
								tCon2.Open();
								OdbcCommand tCmd__2 = new OdbcCommand(strProcedureName, tCon2);
								tCmd__2.CommandType = CommandType.StoredProcedure;
								OdbcCommandBuilder.DeriveParameters(tCmd__2);
								TheStoredProcedure = tCmd__2;
								tCon2.Close();
							}
							else
							{
								return false;
							}
							break;
						default:
							return false;
					}
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return false;
			}
			return false;
		}

		public string GetStoredProcedureParameters()
		{
			if (TheStoredProcedure != null)
			{
				if (TheStoredProcedure.Parameters.Count > 0)
				{
					System.Text.StringBuilder strReturn = new System.Text.StringBuilder();
					string strComma = "";
					foreach (DbParameter tParm in TheStoredProcedure.Parameters)
					{
						strReturn.Append(strComma + tParm.ParameterName);
						strComma = ",";
					}
					return strReturn.ToString();
				}
				else
				{
					return "";
				}
			}
			else
			{
				return "";
			}
		}

		public int ExecuteStoredProcedure(bool boolReturnTable, bool boolAddMissingSchema = false)
		{
			int intReturn;
			try
			{
				if (TheStoredProcedure != null)
				{
					if (TheStoredProcedure.Connection != null)
					{
						if (TheStoredProcedure.Connection.State != ConnectionState.Open)
						{
							TheStoredProcedure.Connection.Open();
						}
						if (!boolReturnTable)
						{
							return TheStoredProcedure.ExecuteNonQuery();
						}
						else
						{
							if (TheDataTable == null)
							{
								TheDataTable = new System.Data.DataTable();
							}
							if (TheDataAdapter != null)
							{
								TheDataAdapter.Dispose();
								TheDataAdapter = null;
							}
							RecCon = TheStoredProcedure.Connection;
							TheDataTable.Rows.Clear();
							if (TheStoredProcedure.Connection.GetType().Equals(typeof(SqlConnection)))
							{
								SqlDataAdapter tAdapter = new SqlDataAdapter(TheStoredProcedure as SqlCommand);
								TheDataAdapter = tAdapter;
								if (boolAddMissingSchema)
								{
									TheDataAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
								}
								else
								{
									TheDataAdapter.MissingSchemaAction = MissingSchemaAction.Add;
								}
								intReturn = tAdapter.Fill(TheDataTable);
								MoveFirst();
								return intReturn;
							}
							else if (TheStoredProcedure.Connection.GetType().Equals(typeof(OleDbConnection)))
							{
								OleDbDataAdapter tAdapter = new OleDbDataAdapter(TheStoredProcedure as OleDbCommand);
								TheDataAdapter = tAdapter;
								if (boolAddMissingSchema)
								{
									TheDataAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
								}
								else
								{
									TheDataAdapter.MissingSchemaAction = MissingSchemaAction.Add;
								}
								intReturn = tAdapter.Fill(TheDataTable);
								MoveFirst();
								return intReturn;
							}
							else if (TheStoredProcedure.Connection.GetType().Equals(typeof(OdbcConnection)))
							{
								OdbcDataAdapter tAdapter = new OdbcDataAdapter(TheStoredProcedure as OdbcCommand);
								TheDataAdapter = tAdapter;
								if (boolAddMissingSchema)
								{
									TheDataAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
								}
								else
								{
									TheDataAdapter.MissingSchemaAction = MissingSchemaAction.Add;
								}
								intReturn = tAdapter.Fill(TheDataTable);
								MoveFirst();
								return intReturn;
							}
							return -1;
						}
					}
					else
					{
						return -1;
					}
				}
				else
				{
					return -1;
				}
			}
			catch (DbException dex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(dex);
				intLastErrorCode = dex.ErrorCode;
				strLastErrorDescription = dex.Message.ToString();
				return -1;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message.ToString();
				return -1;
			}
		}

		public void CreateStoredProcedure(string strProcName, string strSQL, string strDatabase = "")
		{
		}

		public bool AddTableField(string strFieldName, int enumFieldType, int intSize = 0)
		{
			return true;
		}

		public int FieldsCount
		{
			get
			{
				if (TheDataTable != null)
				{
					return TheDataTable.Columns.Count;
				}
				else
				{
					return 0;
				}
			}
		}

		public string FieldsIndexName(int intIndex)
		{
			if (TheDataTable != null)
			{
				if (intIndex >= 0)
				{
					if (intIndex < TheDataTable.Columns.Count)
					{
						return TheDataTable.Columns[intIndex].ColumnName.ToString();
					}
					else
					{
						return "";
					}
				}
				else
				{
					return "";
				}
			}
			else
			{
				return "";
			}
		}

		public void SetFieldsIndexValue(int intIndex, object value)
		{
			SetField(TheDataTable.Columns[intIndex].ColumnName.ToString(), value);
		}

		public object FieldsIndexValue(int intIndex)
		{
			return Fields(TheDataTable.Columns[intIndex].ColumnName.ToString());
		}

		public int FieldsIndexDataType(int intIndex)
		{
			return FCConvert.ToInt32(System.Type.GetTypeCode(TheDataTable.Columns[intIndex].DataType));
		}

		public int FieldsDataType(string strFieldName)
		{
			return FCConvert.ToInt32(System.Type.GetTypeCode(TheDataTable.Columns[strFieldName].DataType));
		}

		public string DefaultDB
		{
			get
			{
				return strInstanceDefaultDatabase;
			}
			set
			{
				strInstanceDefaultDatabase = value;
			}
		}

		public string SharedDefaultDB
		{
			get
			{
				return Statics.strDefaultDatabaseAlias;
			}
			set
			{
				Statics.strDefaultDatabaseAlias = value;
			}
		}

		public bool NoMatch
		{
			get
			{
				return boolNoMatch;
			}
		}

		public int ErrorNumber
		{
			get
			{
				return intLastErrorCode;
			}
			set
			{
				intLastErrorCode = value;
			}
		}

		public string ErrorDescription
		{
			get
			{
				return strLastErrorDescription;
			}
			set
			{
				strLastErrorDescription = value;
			}
		}

		public void ClearErrors()
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
		}

		private void ClearState()
		{
			boolJustDeleted = false;
			ClearErrors();
		}

		public void CancelUpdate()
		{
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			if (TheDataTable != null)
			{
				if (lngCurrentRow >= 0 & TheDataTable.Rows.Count > lngCurrentRow)
				{
					TheDataTable.Rows[lngCurrentRow].RejectChanges();
				}
			}
		}

		public int RecordCount()
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			if (TheDataTable != null)
			{
				return TheDataTable.Rows.Count;
			}
			else
			{
				return 0;
			}
		}

		public int AbsolutePosition()
		{
			return lngCurrentRow;
		}

		public bool AddConnection(string strConnectionString, string strName = "", int Connectiontype = (int)(dbConnectionTypes.SQLServer))
		{
			//, Optional ByVal ConnectionSubType As Integer = dbConnectionSubTypes.MSACCESS) As Boolean
			//Dim boolSuccess As Boolean
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			//boolSuccess = False
			//Dim tcon As DbConnection
			//tcon = Nothing
			//Try
			switch ((dbConnectionTypes)Connectiontype)
			{
				case dbConnectionTypes.SQLServer:
					//tcon = ConnectionPools.GetConnection(strConnectionString, dbConnectionTypes.SQLServer, strName)
					return Statics.ConnectionPools.AddConnection(strConnectionString, dbConnectionTypes.SQLServer, strName);
				case dbConnectionTypes.ODBC:
					//tcon = ConnectionPools.GetConnection(strConnectionString, dbConnectionTypes.ODBC, strName)
					//, ConnectionSubType)
					return Statics.ConnectionPools.AddConnection(strConnectionString, dbConnectionTypes.ODBC, strName);
				case dbConnectionTypes.OLEDB:
					//tcon = ConnectionPools.GetConnection(strConnectionString, dbConnectionTypes.OLEDB, strName)
					return Statics.ConnectionPools.AddConnection(strConnectionString, dbConnectionTypes.OLEDB, strName);
				default:
					return false;
			}
			//If Not IsNothing(tcon) Then boolSuccess = True
			//Catch dex As DbException
			//    intLastErrorCode = dex.ErrorCode
			//    strLastErrorDescription = dex.Message
			//Catch ex As Exception
			//    intLastErrorCode = 1
			//    strLastErrorDescription = ex.Message
			//End Try
			//Return boolSuccess
		}
		//Public Function AddConnection(ByVal strConnectionString As String, Optional ByVal strName As String = "", Optional ByVal Connectiontype As dbConnectionTypes = dbConnectionTypes.SQLServer) As Boolean
		//    Dim boolSuccess As Boolean
		//    intLastErrorCode = 0
		//    boolSuccess = False
		//    Dim tcon As DbConnection
		//    tcon = Nothing
		//    Try
		//        Select Case Connectiontype
		//            Case dbConnectionTypes.SQLServer
		//                tcon = ConnectionPools.GetConnection(strConnectionString, dbConnectionTypes.SQLServer, strName)
		//            Case dbConnectionTypes.ODBC
		//                tcon = ConnectionPools.GetConnection(strConnectionString, dbConnectionTypes.ODBC, strName)
		//            Case dbConnectionTypes.OLEDB
		//                tcon = ConnectionPools.GetConnection(strConnectionString, dbConnectionTypes.OLEDB, strName)
		//        End Select
		//        If Not IsNothing(tcon) Then boolSuccess = True
		//    Catch dex As DbException
		//        intLastErrorCode = dex.ErrorCode
		//    Catch ex As Exception
		//    End Try
		//    Return boolSuccess
		//End Function
		//***********************************************************************************
		/// <summary>
		/// OpenRecordset is intended for backward compatibility. Do not use this function
		/// Only the first 2 parameters are used.  The rest are only intended to allow old
		/// code to compile
		/// </summary>
		/// <param name="strSelect">The select statement</param>
		/// <param name="strDBName">Was the file name.  Now is the nickname for a connection</param>
		/// <param name="intType">Not used</param>
		/// <param name="boolReadOnly">Not used</param>
		/// <param name="LockEdit">Not used</param>
		/// <param name="boolOption">Not used</param>
		/// <param name="strPWD">Not used</param>
		/// <param name="boolShowError">Not used</param>
		/// <param name="ConnectionOverride">Not used</param>
		/// <returns>returns true if successful, false otherwise</returns>
		/// <remarks>For backwards compatibility. Not to be used in new code</remarks>
		public bool OpenRecordset(string strSelect, string strDBName = "", int intType = 0, bool boolReadOnly = false, int LockEdit = 0, bool boolOption = false, string strPWD = "", bool boolShowError = true, string ConnectionOverride = "", bool boolAutoCreateCommands = true)
		{
			boolAddedDefaults = false;
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			bool returnValue = false;
			if (String.IsNullOrEmpty(strDBName))
			{
				strDBName = strInstanceDefaultDatabase;
				if (strDBName == "")
				{
					strDBName = Statics.strDefaultDatabaseAlias;
				}
			}
			strCurSelect = strSelect;
			//strCurSelect.Replace(" false", " 0")
			//strCurSelect.Replace(" False", " 0")
			//strCurSelect.Replace(" FALSE", " 0")
			//strCurSelect.Replace(" true", " 1")
			//strCurSelect.Replace(" True", " 1")
			//strCurSelect.Replace(" TRUE", " 1")
			if (TheDataTable != null)
			{
				TheDataTable.Rows.Clear();
				TheDataTable.Dispose();
				TheDataTable = null;
			}
			if (OpenDataTable(strCurSelect, strDBName, string.Empty, boolAutoCreateCommands))
			{
				returnValue = true;
			}
			return returnValue;
		}

		public object FieldValue(string strFieldName, int lngRow)
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			object ReturnValue;
			bool boolFail = false;
			try
			{
				if (TheDataTable != null)
				{
					if (lngRow >= 0 & TheDataTable.Rows.Count > 0)
					{
						if (TheDataTable.Rows[lngRow][strFieldName] != null)
						{
							//FC:FINAL:AM:#2020 - remove extra checks
                            //if (TheDataTable.Rows[lngRow][strFieldName] != null)
							//{
								//if (!Convert.IsDBNull(TheDataTable.Rows[lngRow][strFieldName]))
								//{
									if (!DBNull.Value.Equals(TheDataTable.Rows[lngRow][strFieldName]))
									{
										ReturnValue = TheDataTable.Rows[lngRow][strFieldName];
									}
									else
									{
										ReturnValue = "";
										boolFail = true;
									}
							//	}
							//	else
							//	{
							//		ReturnValue = "";
							//		boolFail = true;
							//	}
							//}
							//else
							//{
							//	ReturnValue = "";
							//	boolFail = true;
							//}
						}
						else
						{
							ReturnValue = "";
							boolFail = true;
						}
					}
					else
					{
						ReturnValue = "";
						boolFail = true;
					}
				}
				else
				{
					ReturnValue = "";
					boolFail = true;
				}
			}
			catch (DbException dex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(dex);
				intLastErrorCode = dex.ErrorCode;
				strLastErrorDescription = dex.Message;
				ReturnValue = "";
				boolFail = true;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				ReturnValue = "";
				boolFail = true;
			}
			if (boolFail)
			{
				if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(bool)))
				{
					ReturnValue = false;
				}
				else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(int)))
				{
					ReturnValue = 0;
				}
				else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(double)))
				{
					ReturnValue = 0;
				}
			}
			return (ReturnValue);
		}

		public void SetParameterValue(string strParameterName, object value)
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			if (TheStoredProcedure == null)
				return;
			if (TheStoredProcedure.Parameters[strParameterName] == null)
				return;
			try
			{
				if (value != null)
				{
					if (!Convert.IsDBNull(value))
					{
						object thevalue;
						if (value.GetType().ToString() != "System.__ComObject" && value.GetType().ToString().ToLower() != "combobox")
						{
							thevalue = value;
						}
						else
						{
							thevalue = value.ToString();
						}
						if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(System.DateTime)))
						{
							if (Information.IsDate(thevalue))
							{
								TheStoredProcedure.Parameters[strParameterName].Value = FCConvert.ToDateTime(thevalue.ToString());
							}
							else
							{
								TheStoredProcedure.Parameters[strParameterName].Value = DBNull.Value;
							}
						}
						else if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(int)))
						{
							TheStoredProcedure.Parameters[strParameterName].Value = FCConvert.ToInt32(Conversion.Val(thevalue));
						}
						else if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(Int16)))
						{
							TheStoredProcedure.Parameters[strParameterName].Value = (Int16)Conversion.Val(thevalue);
						}
						else if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(Int32)))
						{
							TheStoredProcedure.Parameters[strParameterName].Value = (Int32)Conversion.Val(thevalue);
						}
						else if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(Int64)))
						{
							TheStoredProcedure.Parameters[strParameterName].Value = (Int64)Conversion.Val(thevalue);
						}
						else if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(double)))
						{
							TheStoredProcedure.Parameters[strParameterName].Value = FCConvert.ToDouble(Conversion.Val(thevalue));
						}
						else if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(decimal)))
						{
							TheStoredProcedure.Parameters[strParameterName].Value = FCConvert.ToDecimal(Conversion.Val(thevalue));
						}
						else if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(string)))
						{
							TheStoredProcedure.Parameters[strParameterName].Value = thevalue.ToString();
						}
						else
						{
							TheStoredProcedure.Parameters[strParameterName].Value = thevalue;
						}
					}
					else
					{
						TheStoredProcedure.Parameters[strParameterName].Value = value;
					}
				}
				else
				{
					if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(System.DateTime)))
					{
						TheStoredProcedure.Parameters[strParameterName].Value = DBNull.Value;
					}
					else if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(int)))
					{
						TheStoredProcedure.Parameters[strParameterName].Value = 0;
					}
					else if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(Int16)))
					{
						TheStoredProcedure.Parameters[strParameterName].Value = 0;
					}
					else if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(Int32)))
					{
						TheStoredProcedure.Parameters[strParameterName].Value = 0;
					}
					else if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(Int64)))
					{
						TheStoredProcedure.Parameters[strParameterName].Value = 0;
					}
					else if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(double)))
					{
						TheStoredProcedure.Parameters[strParameterName].Value = 0;
					}
					else if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(decimal)))
					{
						TheStoredProcedure.Parameters[strParameterName].Value = 0;
					}
					else if (TheStoredProcedure.Parameters[strParameterName].DbType.Equals(typeof(string)))
					{
						TheStoredProcedure.Parameters[strParameterName].Value = "";
					}
					else
					{
						TheStoredProcedure.Parameters[strParameterName].Value = "";
					}
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				FCMessageBox.Show(ex.Message.ToString() + Constants.vbNewLine + "In ParameterValue", MsgBoxStyle.Critical, "Error");
			}
		}

		public object ParameterValue(string strParametername)
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			object ReturnValue;
			bool boolFail = false;
			try
			{
				if (TheStoredProcedure != null)
				{
					if (TheStoredProcedure.Parameters[strParametername] != null)
					{
						if (!Convert.IsDBNull(TheStoredProcedure.Parameters[strParametername].Value))
						{
							return TheStoredProcedure.Parameters[strParametername].Value;
						}
						else
						{
							switch (TheStoredProcedure.Parameters[strParametername].DbType)
							{
								case DbType.Boolean:
									return false;
								case DbType.Int16:
								case DbType.Int32:
								case DbType.Int64:
								case DbType.Double:
								case DbType.Decimal:
									return 0;
							//Case DbType.Date, DbType.DateTime, DbType.DateTime2
							//    Return DBNull.Value
								default:
									return "";
							}
						}
					}
					else
					{
						return "";
					}
				}
				else
				{
					return "";
				}
			}
			catch (DbException dex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(dex);
				intLastErrorCode = dex.ErrorCode;
				strLastErrorDescription = dex.Message;
				ReturnValue = "";
				boolFail = true;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				ReturnValue = "";
				boolFail = true;
			}
			return ReturnValue;
		}

		public void SetField(string strFieldName, object value, bool boolSilent = false)
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			try
			{
				if (value != null)
				{
					if (!Convert.IsDBNull(value))
					{
						object thevalue;
						if (value.GetType().ToString() != "System.__ComObject" & value.GetType().ToString().ToLower() != "combobox")
						{
							thevalue = value;
						}
						else
						{
							thevalue = value.ToString();
						}
						if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(System.DateTime)))
						{
                            DateTime date;
                            if (Information.IsDate(thevalue, out date))
							{
								TheDataTable.Rows[lngCurrentRow][strFieldName] = date;
							}
							else
							{
								TheDataTable.Rows[lngCurrentRow][strFieldName] = DBNull.Value;
							}
						}
						else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(int)))
						{
							TheDataTable.Rows[lngCurrentRow][strFieldName] = FCConvert.ToInt32(Conversion.Val(thevalue));
						}
						else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(double)))
						{
							TheDataTable.Rows[lngCurrentRow][strFieldName] = Conversion.Val(thevalue);
						}
						else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(decimal)))
						{
							TheDataTable.Rows[lngCurrentRow][strFieldName] = Conversion.Val(thevalue);
						}
						else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(string)))
						{
							TheDataTable.Rows[lngCurrentRow][strFieldName] = thevalue.ToString();
						}
						else
						{
							TheDataTable.Rows[lngCurrentRow][strFieldName] = thevalue;
						}
					}
					else
					{
						TheDataTable.Rows[lngCurrentRow][strFieldName] = value;
					}
				}
				else
				{
					if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(System.DateTime)))
					{
						TheDataTable.Rows[lngCurrentRow][strFieldName] = DBNull.Value;
					}
					else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(int)))
					{
						TheDataTable.Rows[lngCurrentRow][strFieldName] = 0;
					}
					else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(double)))
					{
						TheDataTable.Rows[lngCurrentRow][strFieldName] = 0;
					}
					else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(decimal)))
					{
						TheDataTable.Rows[lngCurrentRow][strFieldName] = 0;
					}
					else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(bool)))
					{
						TheDataTable.Rows[lngCurrentRow][strFieldName] = false;
					}
					else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(string)))
					{
						TheDataTable.Rows[lngCurrentRow][strFieldName] = "";
					}
					else
					{
						TheDataTable.Rows[lngCurrentRow][strFieldName] = "";
					}
				}
			}
			catch (DbException dex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(dex);
				intLastErrorCode = dex.ErrorCode;
				strLastErrorDescription = dex.Message;
				if (!boolSilent)
				{
					FCMessageBox.Show(dex.Message.ToString() + Constants.vbNewLine + "In DataReader.Fields", MsgBoxStyle.Critical, "Error");
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				if (!boolSilent)
				{
					FCMessageBox.Show(ex.Message.ToString() + Constants.vbNewLine + "In DataReader.Fields", MsgBoxStyle.Critical, "Error");
				}
			}
		}

		public object Fields(string strFieldName)
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			object ReturnValue;
			bool boolFail = false;
			try
			{
				//FC:FINAl:SBE - check if column exist to avoid exception (performance improovment)
				//if (TheDataTable != null)
				if (TheDataTable != null && TheDataTable.Columns[strFieldName] != null)
				{
					if (lngCurrentRow >= 0 & TheDataTable.Rows.Count > 0)
					{
						if (TheDataTable.Rows[lngCurrentRow][strFieldName] != null)
						{
							if (TheDataTable.Rows[lngCurrentRow][strFieldName] != null)
							{
								if (!Convert.IsDBNull(TheDataTable.Rows[lngCurrentRow][strFieldName]))
								{
									if (!DBNull.Value.Equals(TheDataTable.Rows[lngCurrentRow][strFieldName]))
									{
										ReturnValue = TheDataTable.Rows[lngCurrentRow][strFieldName];
									}
									else
									{
										ReturnValue = "";
										boolFail = true;
									}
								}
								else
								{
									ReturnValue = "";
									boolFail = true;
								}
							}
							else
							{
								ReturnValue = "";
								boolFail = true;
							}
						}
						else
						{
							ReturnValue = "";
							boolFail = true;
						}
					}
					else
					{
						ReturnValue = "";
						boolFail = true;
					}
				}
				else
				{
					ReturnValue = "";
					boolFail = true;
					//FC:FINAL:JEI if TheDataTable == null we have to return
					return ReturnValue;
				}
			}
			catch (DbException dex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(dex);
				intLastErrorCode = dex.ErrorCode;
				strLastErrorDescription = dex.Message;
				ReturnValue = "";
				boolFail = true;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				ReturnValue = "";
				boolFail = true;
			}
			try
			{
				//FC:FINAL:SBE - check if column exist to avoid exception (performance improovment)
				//if (boolFail)
				if (boolFail && TheDataTable.Columns[strFieldName] != null)
				{
					if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(bool)))
					{
						ReturnValue = false;
					}
					else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(int)))
					{
						ReturnValue = 0;
					}
					else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(double)))
					{
						ReturnValue = 0;
					}
					else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(decimal)))
					{
						ReturnValue = 0;
					}
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return (ReturnValue);
		}
		//Set(ByVal value As Object)
		//    intLastErrorCode = 0
		//    Try
		//        //MsgBox(GetType(ValueType).ToString())
		//        If Not Convert.IsDBNull(value) Then
		//            If TheDataTable.Columns[strFieldName].DataType.Equals(GetType(Date)) Then
		//                If Information.IsDate(value) Then
		//                    TheDataTable.Rows[lngCurrentRow][strFieldName] = CType(value, Date)
		//                Else
		//                    TheDataTable.Rows[lngCurrentRow][strFieldName] = DBNull.Value
		//                End If
		//            ElseIf TheDataTable.Columns[strFieldName].DataType.Equals(GetType(Integer)) Then
		//                TheDataTable.Rows[lngCurrentRow][strFieldName] = CType(Conversion.Val(value), Integer)
		//            ElseIf TheDataTable.Columns[strFieldName].DataType.Equals(GetTypeFCConvert.ToDouble() Then
		//                TheDataTable.Rows[lngCurrentRow][strFieldName] = Conversion.Val(value)
		//            ElseIf TheDataTable.Columns[strFieldName].DataType.Equals(GetType(String)) Then
		//                //MsgBox(value.ToString())
		//                TheDataTable.Rows[lngCurrentRow][strFieldName] = value.ToString
		//            Else
		//                TheDataTable.Rows[lngCurrentRow][strFieldName] = value
		//            End If
		//        Else
		//            TheDataTable.Rows[lngCurrentRow][strFieldName] = value
		//        End If
		//    Catch dex As DbException
		//        intLastErrorCode = dex.ErrorCode
		//        //MsgBox(dex.Message.ToString()& vbNewLine & "In DataReader.Fields", MsgBoxStyle.Critical, "Error")
		//    Catch ex As Exception
		//        //MsgBox(ex.Message.ToString()& vbNewLine & "In DataReader.Fields", MsgBoxStyle.Critical, "Error")
		//    End Try
		//End Set
		public bool Execute(string strStatement, string strConnectionName = "", bool boolShowError = true)
		{
			bool boolReturn;
			DbConnection tcon = null;
			boolReturn = false;
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			//strStatement.Replace(" false", " 0")
			//strStatement.Replace(" False", " 0")
			//strStatement.Replace(" FALSE", " 0")
			//strStatement.Replace(" true", " 1")
			//strStatement.Replace(" True", " 1")
			//strStatement.Replace(" TRUE", " 1")
			try
			{
				if (strConnectionName != "")
				{
					tcon = Statics.ConnectionPools.GetConnectionByName(strConnectionName, strMegaGroup, strGroup);
					if (tcon == null)
					{
						return false;
					}
				}
				else
				{
					//only works if DefaultDatabase is setup
					if (strInstanceDefaultDatabase != "")
					{
						tcon = Statics.ConnectionPools.GetConnectionByName(strInstanceDefaultDatabase, strMegaGroup, strGroup);
					}
					else
					{
						tcon = Statics.ConnectionPools.GetConnectionByName(Statics.strDefaultDatabaseAlias, strMegaGroup, strGroup);
					}
					if (tcon == null)
					{
						return false;
					}
				}
				if (tcon is SqlConnection)
				{
					SqlCommand tComm = new SqlCommand(strStatement, tcon as SqlConnection);
					if (tcon.State != ConnectionState.Open)
					{
						tcon.Open();
					}
					tComm.ExecuteNonQuery();
					tComm.Dispose();
					boolReturn = true;
				}
				else if (tcon is System.Data.Odbc.OdbcConnection)
				{
					OdbcCommand tComm = new OdbcCommand(strStatement, tcon as OdbcConnection);
					if (tcon.State != ConnectionState.Open)
					{
						tcon.Open();
					}
					tComm.ExecuteNonQuery();
					tComm.Dispose();
					boolReturn = true;
				}
				else if (tcon is System.Data.OleDb.OleDbConnection)
				{
					OleDbCommand tComm = new OleDbCommand(strStatement, tcon as OleDbConnection);
					if (tcon.State != ConnectionState.Open)
					{
						tcon.Open();
					}
					tComm.ExecuteNonQuery();
					tComm.Dispose();
					boolReturn = true;
				}
			}
			catch (DbException dex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(dex);
				intLastErrorCode = dex.ErrorCode;
				strLastErrorDescription = dex.Message;
				if (boolShowError)
				{
					FCMessageBox.Show(dex.Message.ToString(), MsgBoxStyle.Critical, "Error");
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				if (boolShowError)
				{
					FCMessageBox.Show(ex.Message.ToString(), MsgBoxStyle.Critical, "Error");
				}
			}
			finally
			{
				if (tcon != null)
				{
					if (tcon.State == ConnectionState.Open)
					{
						tcon.Close();
					}
					tcon.Dispose();
				}
			}
			return boolReturn;
		}

		public bool UpdateDatabaseTable(string strTableName, string strDatabaseName, string strDatabasepassword = "")
		{
			return true;
		}

		public void MoveFirst()
		{
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			try
			{
				if (TheDataTable != null)
				{
					if (TheDataTable.Rows.Count > 0)
					{
						lngCurrentRow = 0;
					}
					else
					{
						lngCurrentRow = -1;
					}
				}
				else
				{
					lngCurrentRow = -1;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngCurrentRow = -1;
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
			}
		}

		public void MoveLast()
		{
			// intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			try
			{
				if (TheDataTable != null)
				{
					if (TheDataTable.Rows.Count > 0)
					{
						lngCurrentRow = TheDataTable.Rows.Count - 1;
					}
					else
					{
						lngCurrentRow = -1;
					}
				}
				else
				{
					lngCurrentRow = -1;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = -1;
				strLastErrorDescription = ex.Message;
				lngCurrentRow = -1;
			}
		}

		public bool MoveNext()
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			if (boolJustDeleted)
			{
				boolJustDeleted = false;
				if (lngCurrentRow >= 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			try
			{
				if (TheDataTable != null)
				{
					if (lngCurrentRow + 1 <= TheDataTable.Rows.Count)
					{
						lngCurrentRow = lngCurrentRow + 1;
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					lngCurrentRow = -1;
					return false;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				lngCurrentRow = -1;
				return false;
			}
		}

		public bool FindLastRecord(string strFieldName, object obFindValue)
		{
			int x;
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			try
			{
				if (TheDataTable != null)
				{
					for (x = TheDataTable.Rows.Count - 1; x >= 0; x += -1)
					{
						if (TheDataTable.Rows[x].RowState != DataRowState.Deleted)
						{
							if (!Convert.IsDBNull(TheDataTable.Rows[x][strFieldName]))
							{
								if (TheDataTable.Rows[x][strFieldName] == obFindValue)
								{
									lngCurrentRow = x;
									boolNoMatch = false;
									return true;
								}
							}
						}
					}
				}
				else
				{
					boolNoMatch = true;
					return false;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				boolNoMatch = true;
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				return false;
			}
			return false;
		}

		public bool FindPreviousRecord(string strFieldName, object obFindValue)
		{
			int x;
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			try
			{
				if (TheDataTable != null)
				{
					for (x = lngCurrentRow - 1; x >= 0; x += -1)
					{
						if (TheDataTable.Rows[x].RowState != DataRowState.Deleted)
						{
							if (!Convert.IsDBNull(TheDataTable.Rows[x][strFieldName]))
							{
								if (TheDataTable.Rows[x][strFieldName] == obFindValue)
								{
									lngCurrentRow = x;
									boolNoMatch = false;
									return true;
								}
							}
						}
					}
				}
				else
				{
					boolNoMatch = true;
					return false;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				boolNoMatch = true;
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				return false;
			}
			return false;
		}

		public bool FindPreviousRecord(ref System.Array arFieldNames, ref System.Array arFindValues)
		{
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			if (arFieldNames == null || arFindValues == null)
			{
				boolNoMatch = true;
				return false;
			}
			try
			{
				if (TheDataTable != null)
				{
					int x;
					int y = 0;
					DataRow tRow = null;
					bool boolMatch = false;
					for (x = lngCurrentRow - 1; x >= 0; x += -1)
					{
						tRow = TheDataTable.Rows[x];
						if (tRow.RowState != DataRowState.Deleted)
						{
							boolMatch = true;
							for (y = 0; y <= arFieldNames.Length - 1; y++)
							{
								if (!Convert.IsDBNull(tRow[arFieldNames.GetValue(y).ToString()]))
								{
									if (tRow[arFieldNames.GetValue(y).ToString()] != arFindValues.GetValue(y))
									{
										//lngCurrentRow = x
										//boolNoMatch = False
										//Return True
										boolMatch = false;
										break;
									}
								}
								else
								{
									boolMatch = false;
									break;
								}
							}
							if (boolMatch)
							{
								lngCurrentRow = x;
								boolNoMatch = false;
								return true;
							}
						}
					}
					boolNoMatch = true;
					return false;
				}
				else
				{
					boolNoMatch = true;
					return false;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				boolNoMatch = true;
				return false;
			}
		}

		public bool FindFirstRecord(string strFieldName, object obFindValue, string strOperator = "=")
		{
			int x;
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			strOperator = strOperator.ToString().Trim();
			if (TheDataTable != null)
			{
				switch (strOperator)
				{
					case "<":
						for (x = 0; x <= TheDataTable.Rows.Count - 1; x++)
						{
							if (TheDataTable.Rows[x].RowState != DataRowState.Deleted)
							{
								if (!Convert.IsDBNull(TheDataTable.Rows[x][strFieldName]))
								{
									if (FCConvert.ToInt32(TheDataTable.Rows[x][strFieldName]) < FCConvert.ToInt32(obFindValue))
									{
										lngCurrentRow = x;
										boolNoMatch = false;
										return true;
									}
								}
							}
						}
						break;
					case "<=":
						for (x = 0; x <= TheDataTable.Rows.Count - 1; x++)
						{
							if (TheDataTable.Rows[x].RowState != DataRowState.Deleted)
							{
								if (!Convert.IsDBNull(TheDataTable.Rows[x][strFieldName]))
								{
									if (FCConvert.ToInt32(TheDataTable.Rows[x][strFieldName]) <= FCConvert.ToInt32(obFindValue))
									{
										lngCurrentRow = x;
										boolNoMatch = false;
										return true;
									}
								}
							}
						}
						break;
					case ">=":
						for (x = 0; x <= TheDataTable.Rows.Count - 1; x++)
						{
							if (TheDataTable.Rows[x].RowState != DataRowState.Deleted)
							{
								if (!Convert.IsDBNull(TheDataTable.Rows[x][strFieldName]))
								{
									if (FCConvert.ToInt32(TheDataTable.Rows[x][strFieldName]) >= FCConvert.ToInt32(obFindValue))
									{
										lngCurrentRow = x;
										boolNoMatch = false;
										return true;
									}
								}
							}
						}
						break;
					case ">":
						for (x = 0; x <= TheDataTable.Rows.Count - 1; x++)
						{
							if (TheDataTable.Rows[x].RowState != DataRowState.Deleted)
							{
								if (!Convert.IsDBNull(TheDataTable.Rows[x][strFieldName]))
								{
									if (FCConvert.ToInt32(TheDataTable.Rows[x][strFieldName]) > FCConvert.ToInt32(obFindValue))
									{
										lngCurrentRow = x;
										boolNoMatch = false;
										return true;
									}
								}
							}
						}
						break;
					case "<>":
						if (obFindValue != null)
						{
							string strFindValue = obFindValue.ToString();
							for (x = 0; x <= TheDataTable.Rows.Count - 1; x++)
							{
								if (TheDataTable.Rows[x].RowState != DataRowState.Deleted)
								{
									if (!Convert.IsDBNull(TheDataTable.Rows[x][strFieldName]))
									{
										if (TheDataTable.Rows[x][strFieldName].ToString() != strFindValue)
										{
											lngCurrentRow = x;
											boolNoMatch = false;
											return true;
										}
									}
								}
							}
						}
						break;
					default:
						if (obFindValue != null)
						{
							string strFindValue = obFindValue.ToString();
                            int intFindValue = 0;
                            bool numericSearch = false;
                            //FC:FINAL:AM:#2188 - if the db column is int compare the values as numeric; e.g. 2 will be equal to "02"
                            if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(int)) && int.TryParse(strFindValue, out intFindValue))
                            {
                                numericSearch = true;                       
                            }
							for (x = 0; x <= TheDataTable.Rows.Count - 1; x++)
							{
								if (TheDataTable.Rows[x].RowState != DataRowState.Deleted)
								{
									if (!Convert.IsDBNull(TheDataTable.Rows[x][strFieldName]))
									{
										if (!numericSearch)
										{
											if (TheDataTable.Rows[x][strFieldName].ToString() == strFindValue)
											{
												lngCurrentRow = x;
												boolNoMatch = false;
												return true;
											}
										}
										else if ((int)TheDataTable.Rows[x][strFieldName] == intFindValue)
										{
											lngCurrentRow = x;
											boolNoMatch = false;
											return true;
										}
									}
								}
							}
						}
						break;
				}
				boolNoMatch = true;
				return false;
			}
			else
			{
				boolNoMatch = true;
				return false;
			}
		}

		public bool FindNextRecord(string strFieldName, object obFindValue)
		{
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			try
			{
				if (TheDataTable != null && obFindValue != null)
				{
					string strFindValue = obFindValue.ToString();
					int x;
					for (x = lngCurrentRow + 1; x <= TheDataTable.Rows.Count - 1; x++)
					{
						if (TheDataTable.Rows[x].RowState != DataRowState.Deleted)
						{
							if (!Convert.IsDBNull(TheDataTable.Rows[x][strFieldName]))
							{
								if (TheDataTable.Rows[x][strFieldName].ToString() == strFindValue)
								{
									lngCurrentRow = x;
									boolNoMatch = false;
									return true;
								}
							}
						}
					}
					boolNoMatch = true;
					return false;
				}
				else
				{
					boolNoMatch = true;
					return false;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				boolNoMatch = true;
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
			}
			return false;
		}

		public bool FindFirstRecord(string strFieldNames, string strFindValues, string strDelimiter, string strOperator = "=")
		{
			string[] arFieldNames;
			string[] arFindValues;
			string[] arOperators;
			char[] arDelimiter = new char[1];
			string tFieldName = "";
			int tIndex;
			string tKeyWord = "";
			string strOperatorList = "";
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			arDelimiter[0] = Convert.ToChar(strDelimiter);
			arFieldNames = strFieldNames.Split(arDelimiter);
			arFindValues = strFindValues.Split(arDelimiter);
			if (arFieldNames == null || arFindValues == null)
			{
				boolNoMatch = true;
				return false;
			}
			if (strOperator != "")
			{
				arOperators = strOperator.Split(arDelimiter);
			}
			else
			{
				strOperator = "=";
				arOperators = strOperator.Split(arDelimiter);
			}
			if (Information.UBound(arOperators) < Information.UBound(arFieldNames))
			{
				Array.Resize(ref arOperators, arFieldNames.Length + 1);
			}
			int x;
			for (x = 0; x <= Information.UBound(arOperators); x++)
			{
				if (arOperators[x] != null)
				{
					if (arOperators[x].ToString().Trim() == "")
					{
						arOperators[x] = "=";
					}
					else
					{
						arOperators[x] = arOperators[x].ToString().Trim();
					}
				}
				else
				{
					arOperators[x] = "=";
				}
			}
			try
			{
				if (TheDataTable != null)
				{
					int y = 0;
					DataRow tRow = null;
					for (x = 0; x <= TheDataTable.Rows.Count - 1; x++)
					{
						tRow = TheDataTable.Rows[x];
						if (tRow.RowState != DataRowState.Deleted)
						{
							bool breakFory = false;
							for (y = 0; y <= arFieldNames.Length - 1; y++)
							{
								tFieldName = arFieldNames[y].ToString().Trim();
								if (!tFieldName.Contains("("))
								{
									switch (arOperators[y].ToString())
									{
										case "<":
											if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
											{
												if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) < FCConvert.ToInt32(arFindValues[y]))
												{
													if (y == arFieldNames.Length - 1)
													{
														lngCurrentRow = x;
														boolNoMatch = false;
														return true;
													}
												}
												else
												{
													breakFory = true;
													goto EXITFOR;
													//break; Was : Exit For
												}
											}
											else
											{
												breakFory = true;
												goto EXITFOR;
												//break; Was : Exit For
											}
											break;
										case "<=":
											if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
											{
												if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) <= FCConvert.ToInt32(arFindValues[y]))
												{
													if (y == arFieldNames.Length - 1)
													{
														lngCurrentRow = x;
														boolNoMatch = false;
														return true;
													}
												}
												else
												{
													breakFory = true;
													goto EXITFOR;
													//break; Was : Exit For
												}
											}
											else
											{
												breakFory = true;
												goto EXITFOR;
												//break; Was : Exit For
											}
											break;
										case ">":
											if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
											{
												if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) > FCConvert.ToInt32(arFindValues[y]))
												{
													if (y == arFieldNames.Length - 1)
													{
														lngCurrentRow = x;
														boolNoMatch = false;
														return true;
													}
												}
												else
												{
													breakFory = true;
													goto EXITFOR;
													//break; Was : Exit For
												}
											}
											else
											{
												breakFory = true;
												goto EXITFOR;
												//break; Was : Exit For
											}
											break;
										case ">=":
											if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
											{
												if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) >= FCConvert.ToInt32(arFindValues[y]))
												{
													if (y == arFieldNames.Length - 1)
													{
														lngCurrentRow = x;
														boolNoMatch = false;
														return true;
													}
												}
												else
												{
													breakFory = true;
													goto EXITFOR;
													//break; Was : Exit For
												}
											}
											else
											{
												breakFory = true;
												goto EXITFOR;
												//break; Was : Exit For
											}
											break;
										case "<>":
											if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
											{
												if ((string)FieldValue(tFieldName.ToString(), x).ToString() != arFindValues[y])
												{
													if (y == arFieldNames.Length - 1)
													{
														lngCurrentRow = x;
														boolNoMatch = false;
														return true;
													}
												}
												else
												{
													breakFory = true;
													goto EXITFOR;
													//break; Was : Exit For
												}
											}
											else
											{
												breakFory = true;
												goto EXITFOR;
												//break; Was : Exit For
											}
											break;
										default:
											if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
											{
												if ((string)FieldValue(tFieldName.ToString(), x).ToString() == arFindValues[y])
												{
													if (y == arFieldNames.Length - 1)
													{
														lngCurrentRow = x;
														boolNoMatch = false;
														return true;
													}
												}
												else
												{
													breakFory = true;
													goto EXITFOR;
													//break; Was : Exit For
												}
											}
											else
											{
												breakFory = true;
												goto EXITFOR;
												//break; Was : Exit For
											}
											break;
									}
								}
								else
								{
									tIndex = tFieldName.IndexOf("(");
									tKeyWord = tFieldName.Substring(0, tIndex + 1);
									tFieldName = tFieldName.Substring(tIndex);
									tFieldName.Replace("(", "");
									tFieldName.Replace(")", "");
									tFieldName.Trim();
									switch (Strings.LCase(tKeyWord))
									{
										case "val":
											switch (arOperators[y].ToString())
											{
												case "<":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (Conversion.Val(FieldValue(tFieldName.ToString(), x)) < Conversion.Val(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case "<=":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (Conversion.Val(FieldValue(tFieldName.ToString(), x)) <= Conversion.Val(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case ">":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (Conversion.Val(FieldValue(tFieldName.ToString(), x)) > Conversion.Val(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case ">=":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (Conversion.Val(FieldValue(tFieldName.ToString(), x)) >= Conversion.Val(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case "<>":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (Conversion.Val(FieldValue(tFieldName.ToString(), x)) != Conversion.Val(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												default:
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (Conversion.Val(FieldValue(tFieldName.ToString(), x)) == Conversion.Val(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
											}
											break;
										case "trim":
											switch (arOperators[y].ToString())
											{
												case "<":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x).ToString().Trim()) < FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case "<=":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x).ToString().Trim()) <= FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case ">":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x).ToString().Trim()) > FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case ">=":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x).ToString().Trim()) >= FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case "<>":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FieldValue(tFieldName.ToString(), x).ToString().Trim() != arFindValues[y])
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												default:
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FieldValue(tFieldName.ToString(), x).ToString().Trim() == arFindValues[y])
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
											}
											break;
										default:
											switch (arOperators[y].ToString())
											{
												case "<":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) < FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case "<=":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) <= FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case ">":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) > FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case ">=":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) >= FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case "<>":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if ((string)FieldValue(tFieldName.ToString(), x) != arFindValues[y])
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												default:
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if ((string)FieldValue(tFieldName.ToString(), x) == arFindValues[y])
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
											}
											break;
									}
								}
								EXITFOR:
								if (breakFory)
								{
									break;
								}
							}
						}
					}
					boolNoMatch = true;
					return false;
				}
				else
				{
					boolNoMatch = true;
					return false;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				boolNoMatch = true;
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				return false;
			}
		}

		public bool FindNextRecord(System.Array arFieldNames, System.Array arFindValues)
		{
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			if (arFieldNames == null || arFindValues == null)
			{
				boolNoMatch = true;
				return false;
			}
			try
			{
				if (TheDataTable != null)
				{
					int x;
					int y = 0;
					bool boolMatch = false;
					DataRow tRow = null;
					for (x = lngCurrentRow + 1; x <= TheDataTable.Rows.Count - 1; x++)
					{
						tRow = TheDataTable.Rows[x];
						if (tRow.RowState != DataRowState.Deleted)
						{
							boolMatch = true;
							for (y = 0; y <= arFieldNames.Length - 1; y++)
							{
								if (!Convert.IsDBNull(tRow[arFieldNames.GetValue(y).ToString()]))
								{
									//FC:FINAL:MSH - i.issue #1098: comparing object values will return incorrect result of comparing
									//if (tRow[arFieldNames.GetValue(y).ToString()] != arFindValues.GetValue(y))
									if (string.Compare(tRow[arFieldNames.GetValue(y).ToString()].ToString(), arFindValues.GetValue(y).ToString()) != 0)
									{
										//lngCurrentRow = x
										//boolNoMatch = False
										//Return True
										boolMatch = false;
										break;
									}
								}
								else
								{
									boolMatch = false;
									break;
								}
							}
							if (boolMatch)
							{
								lngCurrentRow = x;
								boolNoMatch = false;
								return true;
							}
						}
					}
					boolNoMatch = true;
					return false;
				}
				else
				{
					boolNoMatch = true;
					return false;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				boolNoMatch = true;
				return false;
			}
		}

		public bool FindNextRecord(string strFieldNames, string strFindValues, string strDelimiter, string strOperator = "=")
		{
			string[] arFieldNames;
			string[] arFindValues;
			string[] arOperators;
			char[] arDelimiter = new char[1];
			string tFieldName = "";
			int tIndex;
			string tKeyWord = "";
			string strOperatorList = "";
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			arDelimiter[0] = Convert.ToChar(strDelimiter);
			arFieldNames = strFieldNames.Split(arDelimiter);
			arFindValues = strFindValues.Split(arDelimiter);
			if (arFieldNames == null || arFindValues == null)
			{
				boolNoMatch = true;
				return false;
			}
			if (strOperator != "")
			{
				arOperators = strOperator.Split(arDelimiter);
			}
			else
			{
				strOperator = "=";
				arOperators = strOperator.Split(arDelimiter);
			}
			if (Information.UBound(arOperators) < Information.UBound(arFieldNames))
			{
				Array.Resize(ref arOperators, arFieldNames.Length + 1);
			}
			int x;
			for (x = 0; x <= Information.UBound(arOperators); x++)
			{
				if (arOperators[x] != null)
				{
					if (arOperators[x].ToString().Trim() == "")
					{
						arOperators[x] = "=";
					}
					else
					{
						arOperators[x] = arOperators[x].ToString().Trim();
					}
				}
				else
				{
					arOperators[x] = "=";
				}
			}
			try
			{
				if (TheDataTable != null)
				{
					int y = 0;
					DataRow tRow = null;
					for (x = lngCurrentRow + 1; x <= TheDataTable.Rows.Count - 1; x++)
					{
						tRow = TheDataTable.Rows[x];
						if (tRow.RowState != DataRowState.Deleted)
						{
							bool breakFory = false;
							for (y = 0; y <= arFieldNames.Length - 1; y++)
							{
								tFieldName = arFieldNames[y].ToString().Trim();
								if (!tFieldName.Contains("("))
								{
									switch (arOperators[y].ToString())
									{
										case "<":
											if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
											{
												if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) < FCConvert.ToInt32(arFindValues[y]))
												{
													if (y == arFieldNames.Length - 1)
													{
														lngCurrentRow = x;
														boolNoMatch = false;
														return true;
													}
												}
												else
												{
													breakFory = true;
													goto EXITFOR;
													//break; Was : Exit For
												}
											}
											else
											{
												breakFory = true;
												goto EXITFOR;
												//break; Was : Exit For
											}
											break;
										case "<=":
											if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
											{
												if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) <= FCConvert.ToInt32(arFindValues[y]))
												{
													if (y == arFieldNames.Length - 1)
													{
														lngCurrentRow = x;
														boolNoMatch = false;
														return true;
													}
												}
												else
												{
													breakFory = true;
													goto EXITFOR;
													//break; Was : Exit For
												}
											}
											else
											{
												breakFory = true;
												goto EXITFOR;
												//break; Was : Exit For
											}
											break;
										case ">":
											if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
											{
												if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) > FCConvert.ToInt32(arFindValues[y]))
												{
													if (y == arFieldNames.Length - 1)
													{
														lngCurrentRow = x;
														boolNoMatch = false;
														return true;
													}
												}
												else
												{
													breakFory = true;
													goto EXITFOR;
													//break; Was : Exit For
												}
											}
											else
											{
												breakFory = true;
												goto EXITFOR;
												//break; Was : Exit For
											}
											break;
										case ">=":
											if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
											{
												if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) >= FCConvert.ToInt32(arFindValues[y]))
												{
													if (y == arFieldNames.Length - 1)
													{
														lngCurrentRow = x;
														boolNoMatch = false;
														return true;
													}
												}
												else
												{
													breakFory = true;
													goto EXITFOR;
													//break; Was : Exit For
												}
											}
											else
											{
												breakFory = true;
												goto EXITFOR;
												//break; Was : Exit For
											}
											break;
										case "<>":
											if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
											{
												if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) != FCConvert.ToInt32(arFindValues[y]))
												{
													if (y == arFieldNames.Length - 1)
													{
														lngCurrentRow = x;
														boolNoMatch = false;
														return true;
													}
												}
												else
												{
													breakFory = true;
													goto EXITFOR;
													//break; Was : Exit For
												}
											}
											else
											{
												breakFory = true;
												goto EXITFOR;
												//break; Was : Exit For
											}
											break;
										default:
											if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
											{
												if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) == FCConvert.ToInt32(arFindValues[y]))
												{
													if (y == arFieldNames.Length - 1)
													{
														lngCurrentRow = x;
														boolNoMatch = false;
														return true;
													}
												}
												else
												{
													breakFory = true;
													goto EXITFOR;
													//break; Was : Exit For
												}
											}
											else
											{
												breakFory = true;
												goto EXITFOR;
												//break; Was : Exit For
											}
											break;
									}
								}
								else
								{
									tIndex = tFieldName.IndexOf("(");
									tKeyWord = tFieldName.Substring(0, tIndex + 1);
									tFieldName = tFieldName.Substring(tIndex);
									tFieldName.Replace("(", "");
									tFieldName.Replace(")", "");
									tFieldName.Trim();
									switch (Strings.LCase(tKeyWord))
									{
										case "val":
											switch (arOperators[y].ToString())
											{
												case "<":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (Conversion.Val(FieldValue(tFieldName.ToString(), x)) < Conversion.Val(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case "<=":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (Conversion.Val(FieldValue(tFieldName.ToString(), x)) <= Conversion.Val(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case ">":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (Conversion.Val(FieldValue(tFieldName.ToString(), x)) > Conversion.Val(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case ">=":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (Conversion.Val(FieldValue(tFieldName.ToString(), x)) >= Conversion.Val(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case "<>":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (Conversion.Val(FieldValue(tFieldName.ToString(), x)) != Conversion.Val(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												default:
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (Conversion.Val(FieldValue(tFieldName.ToString(), x)) == Conversion.Val(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
											}
											break;
										case "trim":
											switch (arOperators[y].ToString())
											{
												case "<":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x).ToString().Trim()) < FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case "<=":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x).ToString().Trim()) <= FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case ">":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x).ToString().Trim()) > FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case ">=":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x).ToString().Trim()) >= FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case "<>":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FieldValue(tFieldName.ToString(), x).ToString().Trim() != arFindValues[y])
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												default:
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FieldValue(tFieldName.ToString(), x).ToString().Trim() == arFindValues[y])
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
											}
											break;
										default:
											switch (arOperators[y].ToString())
											{
												case "<":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) < FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case "<=":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) <= FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case ">":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) > FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case ">=":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FCConvert.ToInt32(FieldValue(tFieldName.ToString(), x)) >= FCConvert.ToInt32(arFindValues[y]))
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												case "<>":
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FieldValue(tFieldName.ToString(), x).ToString() != arFindValues[y])
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
												default:
													if (!Convert.IsDBNull(FieldValue(tFieldName.ToString(), x)))
													{
														if (FieldValue(tFieldName.ToString(), x).ToString() == arFindValues[y])
														{
															if (y == arFieldNames.Length - 1)
															{
																lngCurrentRow = x;
																boolNoMatch = false;
																return true;
															}
														}
														else
														{
															breakFory = true;
															goto EXITFOR;
															//break; Was : Exit For
														}
													}
													else
													{
														breakFory = true;
														goto EXITFOR;
														//break; Was : Exit For
													}
													break;
											}
											break;
									}
								}
								EXITFOR:
								if (breakFory)
								{
									break;
								}
							}
						}
					}
					boolNoMatch = true;
					return false;
				}
				else
				{
					boolNoMatch = true;
					return false;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				boolNoMatch = true;
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				return false;
			}
		}

		public bool FindFirst(string strFindSQL)
		{
			string strSQL;
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			strSQL = strFindSQL;
			boolNoMatch = true;
			try
			{
				if (TheDataTable != null)
				{
					System.Data.DataRow FindRow = null;
					try
					{
						//FC:COMP:JEI
						//FindRow = TheDataTable.Select(strSQL).FirstOrDefault();
						DataRow[] rows = TheDataTable.Select(strSQL);
						if (rows != null && rows.Length > 0)
						{
							FindRow = rows[0];
						}
						if (FindRow != null)
						{
							lngCurrentRow = TheDataTable.Rows.IndexOf(FindRow);
							boolNoMatch = false;
							return true;
						}
						else
						{
							return false;
						}
					}
					catch (Exception ex)
					{
                        StaticSettings.GlobalTelemetryService.TrackException(ex);
						return false;
					}
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				return false;
			}
		}

		public System.Data.DataRow[] FindFirstRecord(string strFindSQL)
		{
			string strSQL;
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			strSQL = strFindSQL;
			boolNoMatch = true;
			try
			{
				if (TheDataTable != null)
				{
					System.Data.DataRow[] FindRowCollection = null;
					try
					{
						FindRowCollection = TheDataTable.Select(strSQL);
						if (FindRowCollection != null)
						{
							if (Information.UBound(FindRowCollection) >= 0)
							{
								boolNoMatch = false;
								return FindRowCollection;
							}
							else
							{
								return FindRowCollection;
							}
						}
						else
						{
							return FindRowCollection;
						}
					}
					catch (Exception ex)
					{
                        StaticSettings.GlobalTelemetryService.TrackException(ex);
						return null;
					}
				}
				else
				{
					return null;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				return null;
			}
		}

		public bool MovePrevious()
		{
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			try
			{
				if (lngCurrentRow >= 0)
				{
					lngCurrentRow = lngCurrentRow - 1;
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				return false;
			}
		}

		public bool EndOfFile()
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			//EndOfFile = false;
			if (TheDataTable != null)
			{
				if (lngCurrentRow < TheDataTable.Rows.Count & TheDataTable.Rows.Count > 0)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				return true;
			}
		}

		public bool IsLastRecord()
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			if (TheDataTable != null)
			{
				if (lngCurrentRow == TheDataTable.Rows.Count - 1)
				{
					return true;
				}
			}
			else
			{
				return false;
			}
			return false;
		}

		public bool IsFirstRecord()
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			if (TheDataTable != null)
			{
				if (lngCurrentRow == 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}

		public bool BeginningOfFile()
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			//BeginningOfFile = false;
			if (TheDataTable != null)
			{
				if (lngCurrentRow < 0)
				{
					return true;
				}
				else if (TheDataTable.Rows.Count == 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return true;
			}
		}

		public bool BeginingOfFile()
		{
			return BeginningOfFile();
		}

		private void DetermineDefaults()
		{
			try
			{
				foreach (System.Data.DataColumn col in TheDataTable.Columns)
				{
					//skip the id field
					if (!col.Unique & !col.AutoIncrement)
					{
						if (col.DataType.Equals(typeof(string)))
						{
						}
						else if (col.DataType.Equals(typeof(int)))
						{
						}
						//FC:COMP:JEI
						//switch (col.DataType)
						//{
						//    case typeof(string):
						//        col.DefaultValue = "";
						//    case typeof(int):
						//    case typeofFCConvert.ToDouble(:
						//    case typeof(short):
						//    case typeof(decimal):
						//        col.DefaultValue = 0;
						//    case typeof(bool):
						//        col.DefaultValue = false;
						//    case typeof(System.DateTime):
						//}
						if (col.DataType == typeof(string))
						{
							col.DefaultValue = "";
						}
						else if (col.DataType == typeof(int) || col.DataType == typeof(double) || col.DataType == typeof(short) || col.DataType == typeof(decimal))
						{
							col.DefaultValue = 0;
						}
						else if (col.DataType == typeof(bool))
						{
							col.DefaultValue = false;
						}
					}
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			boolAddedDefaults = true;
		}
		//Public Sub SetDefaultValue(ByVal strFieldName As String, ByVal value As Object)
		//    intLastErrorCode = 0
		//    strLastErrorDescription = ""
		//    Try
		//        If Not value Is Nothing Then
		//            If Not Convert.IsDBNull(value) Then
		//                Dim thevalue As Object
		//                If Not value.GetType().ToString()= "System.__ComObject" And Not value.GetType().ToString().ToLower()= "combobox" Then
		//                    thevalue = value
		//                Else
		//                    thevalue = value.text
		//                End If
		//                If TheDataTable.Columns[strFieldName].DataType.Equals(GetType(Date)) Then
		//                    If Information.IsDate(thevalue) Then
		//                        TheDataTable.Columns[strFieldName].DefaultValue = CType(thevalue, Date)
		//                    Else
		//                        TheDataTable.Columns[strFieldName].DefaultValue = DBNull.Value
		//                    End If
		//                ElseIf TheDataTable.Columns[strFieldName].DataType.Equals(GetType(Integer)) Then
		//                    TheDataTable.Columns[strFieldName].DefaultValue = CType(Conversion.Val(thevalue), Integer)
		//                ElseIf TheDataTable.Columns[strFieldName].DataType.Equals(GetTypeFCConvert.ToDouble() Then
		//                    TheDataTable.Columns[strFieldName].DefaultValue = Conversion.Val(thevalue)
		//                ElseIf TheDataTable.Columns[strFieldName].DataType.Equals(GetTypeFCConvert.ToDecimal() Then
		//                    TheDataTable.Columns[strFieldName].DefaultValue = Conversion.Val(thevalue)
		//                ElseIf TheDataTable.Columns[strFieldName].DataType.Equals(GetType(String)) Then
		//                    TheDataTable.Columns[strFieldName].DefaultValue = thevalue.ToString
		//                Else
		//                    TheDataTable.Columns[strFieldName].DefaultValue = thevalue
		//                End If
		//            End If
		//        End If
		//    Catch dex As DbException
		//        intLastErrorCode = dex.ErrorCode
		//        strLastErrorDescription = dex.Message
		//    Catch ex As Exception
		//        intLastErrorCode = 1
		//        strLastErrorDescription = ex.Message
		//    End Try
		//End Sub
		public bool AddNew(bool boolDontAddDefaults = false)
		{
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			try
			{
				if (!boolDontAddDefaults & !boolAddedDefaults)
				{
					DetermineDefaults();
				}
				TheDataTable.Rows.Add(TheDataTable.NewRow());
				lngCurrentRow = TheDataTable.Rows.Count - 1;
				return true;
			}
			catch (DbException dex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(dex);
				intLastErrorCode = dex.ErrorCode;
				strLastErrorDescription = dex.Message;
				FCMessageBox.Show("Error " + dex.Message.ToString() + Constants.vbNewLine + "In AddNew", MsgBoxStyle.Critical, "Error");
				return false;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
				FCMessageBox.Show("Error " + ex.Message.ToString() + Constants.vbNewLine + "In AddNew", MsgBoxStyle.Critical, "Error");
				return false;
			}
		}

		public bool Edit()
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			ClearState();
			return true;
		}
		// Public Function UpdateWithStoredProcedures(ByVal strUpdateProcedure As String, ByVal strInsertProcedure As String, ByVal strDeleteProcedure As String) As Boolean
		//intLastErrorCode = 0
		//strLastErrorDescription = ""
		//If lngCurrentRow >= 0 Then
		//    If Not TheDataTable Is Nothing Then
		//        Dim tcon As DbConnection
		//        Dim tStr As String = ""
		//        Try
		//            'tcon = ConnectionPools.GetConnectionByName(curCon)
		//            tcon = RecCon
		//            If Not tcon Is Nothing Then
		//                Dim tRows(-1) As System.Data.DataRow
		//                'If Not boolUpdateAllRows Then
		//                ReDim tRows(0)
		//                tRows(0) = TheDataTable.Rows[lngCurrentRow]
		//                If TypeOf tcon Is SqlConnection Then
		//                    Dim tb As SqlCommandBuilder = Nothing
		//                    If strUpdateProcedure <> "" Then
		//                        If CType(TheDataAdapter, SqlDataAdapter).UpdateCommand Is Nothing Then
		//                            CType(TheDataAdapter, SqlDataAdapter).UpdateCommand = tcon.CreateCommand
		//                        End If
		//                        CType(TheDataAdapter, SqlDataAdapter).UpdateCommand.CommandText = strUpdateProcedure
		//                        CType(TheDataAdapter, SqlDataAdapter).UpdateCommand.CommandType = CommandType.StoredProcedure
		//                        SqlCommandBuilder.DeriveParameters(CType(TheDataAdapter, SqlDataAdapter).UpdateCommand)
		//                        For Each tParm As SqlParameter In CType(TheDataAdapter, SqlDataAdapter).UpdateCommand.Parameters
		//                        Next tParm
		//                    Else
		//                        If CType(TheDataAdapter, SqlDataAdapter).UpdateCommand Is Nothing Then
		//                            'CType(TheDataAdapter, SqlDataAdapter).UpdateCommand = tcon.CreateCommand
		//                            If tb Is Nothing Then
		//                                tb = New SqlCommandBuilder(TheDataAdapter)
		//                            End If
		//                            If Not tb.GetUpdateCommand Is Nothing Then
		//                                CType(TheDataAdapter, SqlDataAdapter).UpdateCommand = tb.GetUpdateCommand
		//                            End If
		//                        End If
		//                    End If
		//                    If strInsertCommandText <> "" Then
		//                        If CType(TheDataAdapter, SqlDataAdapter).InsertCommand Is Nothing Then
		//                            CType(TheDataAdapter, SqlDataAdapter).InsertCommand = tcon.CreateCommand
		//                        End If
		//                        CType(TheDataAdapter, SqlDataAdapter).InsertCommand.CommandText = strInsertCommandText
		//                    Else
		//                        If CType(TheDataAdapter, SqlDataAdapter).InsertCommand Is Nothing Then
		//                            CType(TheDataAdapter, SqlDataAdapter).InsertCommand = tcon.CreateCommand
		//                            If tb Is Nothing Then
		//                                tb = New SqlCommandBuilder(TheDataAdapter)
		//                            End If
		//                            If Not tb.GetInsertCommand Is Nothing Then
		//                                CType(TheDataAdapter, SqlDataAdapter).InsertCommand = tb.GetInsertCommand
		//                            End If
		//                        End If
		//                    End If
		//                    CType(TheDataAdapter, SqlDataAdapter).InsertCommand.UpdatedRowSource = UpdateRowSource.Both
		//                    'If strIdentityColumnToFill <> "" Then
		//                    '    If Not CType(TheDataAdapter, SqlDataAdapter).InsertCommand.CommandText.Contains("Select " & strIdentityColumnToFill & " from " & TheDataTable.TableName.ToString()& " where " & strIdentityColumnToFill & " = SCOPE_IDENTITY()") Then
		//                    '        CType(TheDataAdapter, SqlDataAdapter).InsertCommand.UpdatedRowSource = UpdateRowSource.Both
		//                    '        CType(TheDataAdapter, SqlDataAdapter).InsertCommand.CommandText = CType(TheDataAdapter, SqlDataAdapter).InsertCommand.CommandText & "; Select " & strIdentityColumnToFill & " from " & TheDataTable.TableName.ToString()& " where autoid = SCOPE_IDENTITY()"
		//                    '    End If
		//                    'End If
		//                    If strDeleteCommandText <> "" Then
		//                        If CType(TheDataAdapter, SqlDataAdapter).DeleteCommand Is Nothing Then
		//                            CType(TheDataAdapter, SqlDataAdapter).DeleteCommand = tcon.CreateCommand
		//                        End If
		//                        CType(TheDataAdapter, SqlDataAdapter).DeleteCommand.CommandText = strDeleteCommandText
		//                    Else
		//                        If CType(TheDataAdapter, SqlDataAdapter).DeleteCommand Is Nothing Then
		//                            ' CType(TheDataAdapter, SqlDataAdapter).DeleteCommand = tcon.CreateCommand
		//                            If tb Is Nothing Then
		//                                tb = New SqlCommandBuilder(TheDataAdapter)
		//                            End If
		//                            If Not tb.GetDeleteCommand Is Nothing Then
		//                                CType(TheDataAdapter, SqlDataAdapter).DeleteCommand = tb.GetDeleteCommand
		//                            End If
		//                        End If
		//                    End If
		//                    CType(TheDataAdapter, SqlDataAdapter).Update(tRows)
		//                    If Not tb Is Nothing Then
		//                        tb.Dispose()
		//                    End If
		//                ElseIf TypeOf tcon Is System.Data.Odbc.OdbcConnection Then
		//                    CType(TheDataAdapter, Odbc.OdbcDataAdapter).Update(tRows)
		//                ElseIf TypeOf tcon Is System.Data.OleDb.OleDbConnection Then
		//                    Dim tb As OleDb.OleDbCommandBuilder = Nothing
		//                    Try
		//                        If strUpdateCommandText <> "" Then
		//                            If CType(TheDataAdapter, OleDb.OleDbDataAdapter).UpdateCommand Is Nothing Then
		//                                CType(TheDataAdapter, OleDb.OleDbDataAdapter).UpdateCommand = tcon.CreateCommand
		//                            End If
		//                            CType(TheDataAdapter, OleDb.OleDbDataAdapter).UpdateCommand.CommandText = strUpdateCommandText
		//                        Else
		//                            If CType(TheDataAdapter, OleDb.OleDbDataAdapter).UpdateCommand Is Nothing Then
		//                                CType(TheDataAdapter, OleDb.OleDbDataAdapter).UpdateCommand = tcon.CreateCommand
		//                                If tb Is Nothing Then
		//                                    tb = New OleDb.OleDbCommandBuilder(TheDataAdapter)
		//                                End If
		//                                If Not tb.GetUpdateCommand Is Nothing Then
		//                                    CType(TheDataAdapter, OleDb.OleDbDataAdapter).UpdateCommand = tb.GetUpdateCommand
		//                                End If
		//                            End If
		//                        End If
		//                        CType(TheDataAdapter, OleDb.OleDbDataAdapter).UpdateCommand.UpdatedRowSource = UpdateRowSource.Both
		//                    Catch ex As Exception
		//                    End Try
		//                    Try
		//                        If strInsertCommandText <> "" Then
		//                            If Not CType(TheDataAdapter, OleDb.OleDbDataAdapter).InsertCommand Is Nothing Then
		//                                CType(TheDataAdapter, OleDb.OleDbDataAdapter).InsertCommand.Dispose()
		//                                CType(TheDataAdapter, OleDb.OleDbDataAdapter).InsertCommand = Nothing
		//                            End If
		//                            CType(TheDataAdapter, OleDb.OleDbDataAdapter).InsertCommand = tcon.CreateCommand
		//                            CType(TheDataAdapter, OleDb.OleDbDataAdapter).InsertCommand.CommandText = strInsertCommandText
		//                        Else
		//                            If CType(TheDataAdapter, OleDb.OleDbDataAdapter).InsertCommand Is Nothing Then
		//                                CType(TheDataAdapter, OleDb.OleDbDataAdapter).InsertCommand = tcon.CreateCommand
		//                                If tb Is Nothing Then
		//                                    tb = New OleDb.OleDbCommandBuilder(TheDataAdapter)
		//                                End If
		//                                If Not tb.GetInsertCommand Is Nothing Then
		//                                    CType(TheDataAdapter, OleDb.OleDbDataAdapter).InsertCommand = tb.GetInsertCommand
		//                                End If
		//                            End If
		//                        End If
		//                        CType(TheDataAdapter, OleDb.OleDbDataAdapter).InsertCommand.UpdatedRowSource = UpdateRowSource.Both
		//                    Catch ex As Exception
		//                    End Try
		//                    Try
		//                        If strDeleteCommandText <> "" Then
		//                            If CType(TheDataAdapter, OleDb.OleDbDataAdapter).DeleteCommand Is Nothing Then
		//                                CType(TheDataAdapter, OleDb.OleDbDataAdapter).DeleteCommand = tcon.CreateCommand
		//                            End If
		//                            CType(TheDataAdapter, OleDb.OleDbDataAdapter).DeleteCommand.CommandText = strDeleteCommandText
		//                        Else
		//                            If CType(TheDataAdapter, OleDb.OleDbDataAdapter).DeleteCommand Is Nothing Then
		//                                CType(TheDataAdapter, OleDb.OleDbDataAdapter).DeleteCommand = tcon.CreateCommand
		//                                If tb Is Nothing Then
		//                                    tb = New OleDb.OleDbCommandBuilder(TheDataAdapter)
		//                                End If
		//                                If Not tb.GetDeleteCommand Is Nothing Then
		//                                    CType(TheDataAdapter, OleDb.OleDbDataAdapter).DeleteCommand = tb.GetDeleteCommand
		//                                End If
		//                            End If
		//                        End If
		//                    Catch ex As Exception
		//                    End Try
		//                    CType(TheDataAdapter, OleDb.OleDbDataAdapter).Update(tRows)
		//                    'If strIdentityColumnToFill <> "" Then
		//                    '    Using tAIDCom As New OleDb.OleDbCommand("Select @@Identity", tcon)
		//                    '        If Not tcon.State = ConnectionState.Open Then
		//                    '            tcon.Open()
		//                    '        End If
		//                    '        TheDataTable.Rows[lngCurrentRow].Item(strIdentityColumnToFill) = CInt(tAIDCom.ExecuteScalar)
		//                    '    End Using
		//                    'End If
		//                    If Not tb Is Nothing Then
		//                        tb.Dispose()
		//                    End If
		//                Else
		//                    Return False
		//                End If
		//                TheDataTable.AcceptChanges()
		//                Return True
		//            Else
		//                Return False
		//            End If
		//        Catch dex As DbException
		//            intLastErrorCode = dex.ErrorCode
		//            strLastErrorDescription = dex.Message
		//            //MsgBox(dex.Message & vbNewLine & " in Update", MsgBoxStyle.Critical, "Error")
		//            Return False
		//        Catch ex As Exception
		//            intLastErrorCode = 1
		//            strLastErrorDescription = ex.Message
		//            //MsgBox(ex.Message & vbNewLine & " in Update", MsgBoxStyle.Critical, "Error")
		//            Return False
		//        End Try
		//    End If
		//Else
		//    Return False
		//End If
		// End Function
		public bool Update(bool boolUpdateAllRows = false, string strUpdateCommandText = "", string strInsertCommandText = "", string strDeleteCommandText = "")
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			if (lngCurrentRow >= 0)
			{
				if (TheDataTable != null)
				{
					DbConnection tcon;
					try
					{
						//tcon = ConnectionPools.GetConnectionByName(curCon)
						tcon = RecCon;
						if (tcon != null)
						{
							System.Data.DataRow[] tRows = new System.Data.DataRow[1];
							//If Not boolUpdateAllRows Then
							tRows[0] = TheDataTable.Rows[lngCurrentRow];
							//Else
							//    TheDataTable.Rows.CopyTo(tRows, 0)
							//    'tRows(0).SetModified()
							//End If
							//TheDataAdapter.AcceptChangesDuringUpdate = True
							if (tcon is SqlConnection)
							{
								SqlCommand tUCmd = null;
								SqlCommand tICmd = null;
								SqlCommand tDCmd = null;
								if (OmitNullsOnInsert || (((SqlDataAdapter)TheDataAdapter).UpdateCommand == null) || (((SqlDataAdapter)TheDataAdapter).InsertCommand == null) || (((SqlDataAdapter)TheDataAdapter).DeleteCommand == null))
								{
									tUCmd = ((SqlConnection)tcon).CreateCommand();
									tICmd = ((SqlConnection)tcon).CreateCommand();
									tDCmd = ((SqlConnection)tcon).CreateCommand();
									if (strUpdateCommandText == "" || strInsertCommandText == "" || strDeleteCommandText == "" || OmitNullsOnInsert)
									{
										CreateSQLCommands(TheDataTable.TableName, tUCmd, tICmd, tDCmd, OmitNullsOnInsert);
									}
									if (strUpdateCommandText != "")
									{
										tUCmd.CommandText = strUpdateCommandText;
										tUCmd.Parameters.Clear();
									}
									if (strInsertCommandText != "")
									{
										tICmd.CommandText = strInsertCommandText;
										tICmd.Parameters.Clear();
									}
									if (strDeleteCommandText != "")
									{
										tDCmd.CommandText = strDeleteCommandText;
										tDCmd.Parameters.Clear();
									}
									((SqlDataAdapter)TheDataAdapter).UpdateCommand = tUCmd;
									((SqlDataAdapter)TheDataAdapter).InsertCommand = tICmd;
									((SqlDataAdapter)TheDataAdapter).DeleteCommand = tDCmd;
									// CType(TheDataAdapter, SqlDataAdapter).InsertCommand.UpdatedRowSource = UpdateRowSource.Both
									((SqlDataAdapter)TheDataAdapter).InsertCommand.UpdatedRowSource = UpdateRowSource.OutputParameters;
								}
								//Dim tb As SqlCommandBuilder = Nothing
								//If strUpdateCommandText <> "" Then
								//    If CType(TheDataAdapter, SqlDataAdapter).UpdateCommand Is Nothing Then
								//        CType(TheDataAdapter, SqlDataAdapter).UpdateCommand = tcon.CreateCommand
								//    End If
								//    CType(TheDataAdapter, SqlDataAdapter).UpdateCommand.CommandText = strUpdateCommandText
								//Else
								//    If CType(TheDataAdapter, SqlDataAdapter).UpdateCommand Is Nothing Then
								//        'CType(TheDataAdapter, SqlDataAdapter).UpdateCommand = tcon.CreateCommand
								//        If tb Is Nothing Then
								//            tb = New SqlCommandBuilder(TheDataAdapter)
								//        End If
								//        If Not tb.GetUpdateCommand Is Nothing Then
								//            CType(TheDataAdapter, SqlDataAdapter).UpdateCommand = tb.GetUpdateCommand
								//        End If
								//    End If
								//End If
								//If strInsertCommandText <> "" Then
								//    If CType(TheDataAdapter, SqlDataAdapter).InsertCommand Is Nothing Then
								//        CType(TheDataAdapter, SqlDataAdapter).InsertCommand = tcon.CreateCommand
								//    End If
								//    CType(TheDataAdapter, SqlDataAdapter).InsertCommand.CommandText = strInsertCommandText
								//Else
								//    If CType(TheDataAdapter, SqlDataAdapter).InsertCommand Is Nothing Then
								//        CType(TheDataAdapter, SqlDataAdapter).InsertCommand = tcon.CreateCommand
								//        If tb Is Nothing Then
								//            tb = New SqlCommandBuilder(TheDataAdapter)
								//        End If
								//        If Not tb.GetInsertCommand Is Nothing Then
								//            CType(TheDataAdapter, SqlDataAdapter).InsertCommand = tb.GetInsertCommand
								//        End If
								//    End If
								//End If
								//CType(TheDataAdapter, SqlDataAdapter).InsertCommand.UpdatedRowSource = UpdateRowSource.Both
								//'If strIdentityColumnToFill <> "" Then
								//'    If Not CType(TheDataAdapter, SqlDataAdapter).InsertCommand.CommandText.Contains("Select " & strIdentityColumnToFill & " from " & TheDataTable.TableName.ToString()& " where " & strIdentityColumnToFill & " = SCOPE_IDENTITY()") Then
								//'        CType(TheDataAdapter, SqlDataAdapter).InsertCommand.UpdatedRowSource = UpdateRowSource.Both
								//'        CType(TheDataAdapter, SqlDataAdapter).InsertCommand.CommandText = CType(TheDataAdapter, SqlDataAdapter).InsertCommand.CommandText & "; Select " & strIdentityColumnToFill & " from " & TheDataTable.TableName.ToString()& " where autoid = SCOPE_IDENTITY()"
								//'    End If
								//'End If
								//If strDeleteCommandText <> "" Then
								//    If CType(TheDataAdapter, SqlDataAdapter).DeleteCommand Is Nothing Then
								//        CType(TheDataAdapter, SqlDataAdapter).DeleteCommand = tcon.CreateCommand
								//    End If
								//    CType(TheDataAdapter, SqlDataAdapter).DeleteCommand.CommandText = strDeleteCommandText
								//Else
								//    If CType(TheDataAdapter, SqlDataAdapter).DeleteCommand Is Nothing Then
								//        ' CType(TheDataAdapter, SqlDataAdapter).DeleteCommand = tcon.CreateCommand
								//        If tb Is Nothing Then
								//            tb = New SqlCommandBuilder(TheDataAdapter)
								//        End If
								//        If Not tb.GetDeleteCommand Is Nothing Then
								//            CType(TheDataAdapter, SqlDataAdapter).DeleteCommand = tb.GetDeleteCommand
								//        End If
								//    End If
								//End If
								if (!boolUpdateAllRows)
								{
									((SqlDataAdapter)TheDataAdapter).Update(tRows);
									//CType(TheDataAdapter, SqlDataAdapter).Update({TheDataTable.Rows[lngCurrentRow]})
								}
								else
								{
									((SqlDataAdapter)TheDataAdapter).Update(TheDataTable);
								}
								//If Not tb Is Nothing Then
								//    tb.Dispose()
								//End If
							}
							else if (tcon is System.Data.Odbc.OdbcConnection)
							{
								System.Data.Odbc.OdbcCommand tUCmd = null;
								System.Data.Odbc.OdbcCommand tICmd = null;
								System.Data.Odbc.OdbcCommand tDCmd = null;
								if ((((OdbcDataAdapter)TheDataAdapter).UpdateCommand == null) || (((OdbcDataAdapter)TheDataAdapter).InsertCommand == null) || (((OdbcDataAdapter)TheDataAdapter).DeleteCommand == null))
								{
									tUCmd = ((OdbcConnection)tcon).CreateCommand();
									tICmd = ((OdbcConnection)tcon).CreateCommand();
									tDCmd = ((OdbcConnection)tcon).CreateCommand();
									if (strUpdateCommandText == "" || strInsertCommandText == "" || strDeleteCommandText == "")
									{
										CreateODBCCommands(TheDataTable.TableName, tUCmd, tICmd, tDCmd);
									}
									if (strUpdateCommandText != "")
									{
										tUCmd.CommandText = strUpdateCommandText;
										tUCmd.Parameters.Clear();
									}
									if (strInsertCommandText != "")
									{
										tICmd.CommandText = strInsertCommandText;
										tICmd.Parameters.Clear();
									}
									if (strDeleteCommandText != "")
									{
										tDCmd.CommandText = strDeleteCommandText;
										tDCmd.Parameters.Clear();
									}
									((OdbcDataAdapter)TheDataAdapter).UpdateCommand = tUCmd;
									((OdbcDataAdapter)TheDataAdapter).InsertCommand = tICmd;
									((OdbcDataAdapter)TheDataAdapter).DeleteCommand = tDCmd;
									((OdbcDataAdapter)TheDataAdapter).InsertCommand.UpdatedRowSource = UpdateRowSource.Both;
								}
								((OdbcDataAdapter)TheDataAdapter).Update(tRows);
								//CType(TheDataAdapter, Odbc.OdbcDataAdapter).Update({TheDataTable.Rows[lngCurrentRow]})                           
							}
							else if (tcon is System.Data.OleDb.OleDbConnection)
							{
								OleDbCommandBuilder tb = null;
								try
								{
									if (strUpdateCommandText != "")
									{
										if (((OleDbDataAdapter)TheDataAdapter).UpdateCommand == null)
										{
											((OleDbDataAdapter)TheDataAdapter).UpdateCommand = tcon.CreateCommand() as OleDbCommand;
										}
										((OleDbDataAdapter)TheDataAdapter).UpdateCommand.CommandText = strUpdateCommandText;
									}
									else
									{
										if (((OleDbDataAdapter)TheDataAdapter).UpdateCommand == null)
										{
											((OleDbDataAdapter)TheDataAdapter).UpdateCommand = tcon.CreateCommand() as OleDbCommand;
											if (tb == null)
											{
												tb = new OleDbCommandBuilder(TheDataAdapter as OleDbDataAdapter);
											}
											if (tb.GetUpdateCommand() != null)
											{
												((OleDbDataAdapter)TheDataAdapter).UpdateCommand = tb.GetUpdateCommand() as OleDbCommand;
											}
										}
									}
									((OleDbDataAdapter)TheDataAdapter).UpdateCommand.UpdatedRowSource = UpdateRowSource.Both;
								}
								catch (Exception ex)
								{
                                    StaticSettings.GlobalTelemetryService.TrackException(ex);
								}
								try
								{
									if (strInsertCommandText != "")
									{
										if (((OleDbDataAdapter)TheDataAdapter).InsertCommand != null)
										{
											((OleDbDataAdapter)TheDataAdapter).InsertCommand.Dispose();
											((OleDbDataAdapter)TheDataAdapter).InsertCommand = null;
										}
										((OleDbDataAdapter)TheDataAdapter).InsertCommand = tcon.CreateCommand() as OleDbCommand;
										((OleDbDataAdapter)TheDataAdapter).InsertCommand.CommandText = strInsertCommandText;
									}
									else
									{
										if (((OleDbDataAdapter)TheDataAdapter).InsertCommand == null)
										{
											((OleDbDataAdapter)TheDataAdapter).InsertCommand = tcon.CreateCommand() as OleDbCommand;
											if (tb == null)
											{
												tb = new OleDbCommandBuilder(TheDataAdapter as OleDbDataAdapter);
											}
											if (tb.GetInsertCommand() != null)
											{
												((OleDbDataAdapter)TheDataAdapter).InsertCommand = tb.GetInsertCommand();
											}
										}
									}
									((OleDbDataAdapter)TheDataAdapter).InsertCommand.UpdatedRowSource = UpdateRowSource.Both;
								}
								catch (Exception ex)
								{
                                    StaticSettings.GlobalTelemetryService.TrackException(ex);
								}
								try
								{
									if (strDeleteCommandText != "")
									{
										if (((OleDbDataAdapter)TheDataAdapter).DeleteCommand == null)
										{
											((OleDbDataAdapter)TheDataAdapter).DeleteCommand = tcon.CreateCommand() as OleDbCommand;
										}
										((OleDbDataAdapter)TheDataAdapter).DeleteCommand.CommandText = strDeleteCommandText;
									}
									else
									{
										if (((OleDbDataAdapter)TheDataAdapter).DeleteCommand == null)
										{
											((OleDbDataAdapter)TheDataAdapter).DeleteCommand = tcon.CreateCommand() as OleDbCommand;
											if (tb == null)
											{
												tb = new OleDbCommandBuilder(TheDataAdapter as OleDbDataAdapter);
											}
											if (tb.GetDeleteCommand() != null)
											{
												((OleDbDataAdapter)TheDataAdapter).DeleteCommand = tb.GetDeleteCommand();
											}
										}
									}
								}
								catch (Exception ex)
								{
                                    StaticSettings.GlobalTelemetryService.TrackException(ex);
								}
								((OleDbDataAdapter)TheDataAdapter).Update(tRows);
								//If strIdentityColumnToFill <> "" Then
								//    Using tAIDCom As New OleDb.OleDbCommand("Select @@Identity", tcon)
								//        If Not tcon.State = ConnectionState.Open Then
								//            tcon.Open()
								//        End If
								//        TheDataTable.Rows[lngCurrentRow].Item(strIdentityColumnToFill) = CInt(tAIDCom.ExecuteScalar)
								//    End Using
								//End If
								if (tb != null)
								{
									tb.Dispose();
								}
							}
							else
							{
								return false;
							}
							TheDataTable.AcceptChanges();
							return true;
						}
						else
						{
							return false;
						}
					}
					catch (DbException dex)
					{
                        StaticSettings.GlobalTelemetryService.TrackException(dex);
						intLastErrorCode = dex.ErrorCode;
						strLastErrorDescription = dex.Message;
						FCMessageBox.Show(dex.Message + Constants.vbNewLine + " in Update", MsgBoxStyle.Critical, "Error");
						return false;
					}
					catch (Exception ex)
					{
                        StaticSettings.GlobalTelemetryService.TrackException(ex);
						intLastErrorCode = 1;
						strLastErrorDescription = ex.Message;
						FCMessageBox.Show(ex.Message + Constants.vbNewLine + " in Update", MsgBoxStyle.Critical, "Error");
						return false;
					}
				}
			}
			else
			{
				return false;
			}
			return false;
		}

		public DbConnection GetConnection(string strAlias)
		{
			DbConnection tCon = null;
			//Dim tNode As ConnectionPool.ConnectionNode = Nothing
			ConnectionInfo tInfo = null;
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			//tNode = ConnectionPools.GetConnectionInformationByName(strAlias)
			tInfo = Statics.ConnectionPools.GetConnectionInformationByName(strAlias, strMegaGroup, strGroup);
			//If Not tNode Is Nothing Then
			//    tCon = tNode.GetConnection
			//End If
			if (tInfo != null)
			{
				tCon = tInfo.GetConnection();
			}
			return tCon;
		}

		public bool OpenDataTable(string strSelect, string strConnectionName, string strTableName = "", bool boolAutoCreateCommands = true, bool boolSuppressMessages = false, bool boolDontLoadSchema = false)
		{
			bool boolSuccess;
			//Dim tcon As DbConnection = Nothing
			//Dim tNode As ConnectionPool.ConnectionNode = Nothing
			ConnectionInfo tInfo = null;
			bool boolAddMissingSchema = true;
			boolAddedDefaults = false;
			boolSuccess = false;
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			if (TheDataTable == null)
			{
				TheDataTable = new System.Data.DataTable();
			}
			boolAddMissingSchema = boolAddMissingSchema & (!boolDontLoadSchema);
			tInfo = Statics.ConnectionPools.GetConnectionInformationByName(strConnectionName, strMegaGroup, strGroup);
			//tNode = ConnectionPools.GetConnectionInformationByName(strConnectionName)
			if (tInfo != null)
			{
				if (RecCon != null)
				{
					if (RecCon.State == ConnectionState.Open)
					{
						RecCon.Close();
						RecCon.Dispose();
						RecCon = null;
					}
				}
				RecCon = tInfo.GetConnection();
				//tcon = tNode.GetConnection
			}
			if (RecCon != null)
			{
				try
				{
					if (TheDataAdapter != null)
					{
						TheDataAdapter.Dispose();
						TheDataAdapter = null;
					}
					if (tInfo.ConnectionType == dbConnectionTypes.SQLServer)
					{
						SqlDataAdapter tAdapter = new SqlDataAdapter(strSelect, (SqlConnection)RecCon);
						//If boolAutoCreateCommands Then
						//    Dim tb As New SqlCommandBuilder(tAdapter)
						//    If Not tb.GetUpdateCommand Is Nothing Then
						//        tAdapter.UpdateCommand = tb.GetUpdateCommand
						//    End If
						//    If Not tb.GetInsertCommand Is Nothing Then
						//        tAdapter.InsertCommand = tb.GetInsertCommand
						//    End If
						//    If Not tb.GetDeleteCommand Is Nothing Then
						//        tAdapter.DeleteCommand = tb.GetDeleteCommand
						//    End If
						//End If
						TheDataAdapter = tAdapter;
						if (Strings.InStr(Strings.LCase(strSelect), "inner join ") > 0)
						{
							boolAddMissingSchema = false;
						}
						else if (Strings.InStr(Strings.LCase(strSelect), " union all ") > 0)
						{
							boolAddMissingSchema = false;
						}
						else if (Strings.InStr(Strings.LCase(strSelect), "left join ") > 0)
						{
							boolAddMissingSchema = false;
						}
						if (boolAddMissingSchema)
						{
							TheDataAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
						}
						else
						{
							TheDataAdapter.MissingSchemaAction = MissingSchemaAction.Add;
						}
						tAdapter.Fill(TheDataTable);
						curCon = strConnectionName;
						strCurSelect = strSelect;
					}
					else if (tInfo.ConnectionType == dbConnectionTypes.ODBC)
					{
						OdbcDataAdapter tAdapter = new OdbcDataAdapter(strSelect, (OdbcConnection)RecCon);
						//If boolAutoCreateCommands Then
						//    Dim tb As New Odbc.OdbcCommandBuilder(tAdapter)
						//    If Not tb.GetUpdateCommand Is Nothing Then
						//        tAdapter.UpdateCommand = tb.GetUpdateCommand
						//    End If
						//    If Not tb.GetInsertCommand Is Nothing Then
						//        tAdapter.InsertCommand = tb.GetInsertCommand
						//    End If
						//    If Not tb.GetDeleteCommand Is Nothing Then
						//        tAdapter.DeleteCommand = tb.GetDeleteCommand
						//    End If
						//End If
						TheDataAdapter = tAdapter;
						if (Strings.InStr(Strings.LCase(strSelect), "inner join ") > 0)
						{
							boolAddMissingSchema = false;
						}
						else if (Strings.InStr(Strings.LCase(strSelect), " union all ") > 0)
						{
							boolAddMissingSchema = false;
						}
						else if (Strings.InStr(Strings.LCase(strSelect), "left join ") > 0)
						{
							boolAddMissingSchema = false;
						}
						if (boolAddMissingSchema)
						{
							TheDataAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
						}
						else
						{
							TheDataAdapter.MissingSchemaAction = MissingSchemaAction.Add;
						}
						tAdapter.Fill(TheDataTable);
						curCon = strConnectionName;
						strCurSelect = strSelect;
					}
					else if (tInfo.ConnectionType == dbConnectionTypes.OLEDB)
					{
						OleDbDataAdapter tAdapter = new OleDbDataAdapter(strSelect, (OleDbConnection)RecCon);
						//If boolAutoCreateCommands Then
						//    Dim tb As New OleDb.OleDbCommandBuilder(tAdapter)
						//    If Not tb.GetUpdateCommand Is Nothing Then
						//        tAdapter.UpdateCommand = tb.GetUpdateCommand
						//    End If
						//    If Not tb.GetInsertCommand Is Nothing Then
						//        tAdapter.InsertCommand = tb.GetInsertCommand
						//    End If
						//    If Not tb.GetDeleteCommand Is Nothing Then
						//        tAdapter.DeleteCommand = tb.GetDeleteCommand
						//    End If
						//End If
						TheDataAdapter = tAdapter;
						tAdapter.Fill(TheDataTable);
						curCon = strConnectionName;
						strCurSelect = strSelect;
					}
					else
					{
						curCon = "";
						strCurSelect = "";
						//break; // TODO: might not be correct. Was : Exit Try
					}
					//'create commands based on datatable
					//If strTableName <> String.Empty Then
					//    CreateCommands(strTableName, tInfo)
					//Else
					//    CreateCommands(TheDataTable.TableName, tInfo)
					//End If
					boolSuccess = true;
					lngCurrentRow = 0;
				}
				catch (DbException dex)
				{
                    StaticSettings.GlobalTelemetryService.TrackException(dex);
					intLastErrorCode = dex.ErrorCode;
					strLastErrorDescription = dex.Message;
					boolSuccess = false;
					curCon = "";
					if (!boolSuppressMessages)
					{
						FCMessageBox.Show("Could not fill data table" + Constants.vbNewLine + dex.Message, MsgBoxStyle.Critical, "Error");
					}
				}
				catch (Exception ex)
				{
                    StaticSettings.GlobalTelemetryService.TrackException(ex);
					intLastErrorCode = 1;
					strLastErrorDescription = ex.Message;
					boolSuccess = false;
					curCon = "";
					if (!boolSuppressMessages)
					{
						FCMessageBox.Show("Could not fill data table" + Constants.vbNewLine + ex.Message, MsgBoxStyle.Critical, "Error");
					}
				}
				
			}
			return boolSuccess;
		}

		public string GetParameterSymbol(int tType)
		{
			//TODO: determine actual characters used for non-sql server databases
			switch ((dbConnectionTypes)tType)
			{
				case dbConnectionTypes.SQLServer:
					return "@";
				case dbConnectionTypes.OLEDB:
					return "?";
				case dbConnectionTypes.ODBC:
					return "@";
				default:
					return "@";
			}
		}

		public string GetParameterSymbol(dbConnectionTypes tType)
		{
			//TODO: determine actual characters used for non-sql server databases
			switch (tType)
			{
				case dbConnectionTypes.SQLServer:
					return "@";
				case dbConnectionTypes.OLEDB:
					return "?";
				case dbConnectionTypes.ODBC:
					return "@";
				default:
					return "@";
			}
		}
		//Public Function GetCommand(ByVal tNode As Spectrum.Data.ConnectionPool.ConnectionNode) As DbCommand
		//    Select Case tNode.TypeOfConnection
		//        Case dbConnectionTypes.SQLServer
		//        Case dbConnectionTypes.ODBC
		//        Case dbConnectionTypes.OLEDB
		//        Case Else
		//    End Select
		//End Function
		public void CreateODBCCommands(string strTableName, OdbcCommand tCMD, OdbcCommand tICmd, OdbcCommand tDCmd)
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			try
			{
				if (TheDataTable != null)
				{
					if (TheDataTable.PrimaryKey != null)
					{
						if (!(TheDataTable.PrimaryKey.Length <= 0))
						{
							System.Text.StringBuilder strSQL = new System.Text.StringBuilder();
							System.Text.StringBuilder strISQL = new System.Text.StringBuilder();
							System.Text.StringBuilder strIValueSQL = new System.Text.StringBuilder();
							System.Text.StringBuilder strDSQL = new System.Text.StringBuilder();
							int x;
							bool boolFound = false;
							string strComma = "";
							string strAnd = " ";
							dbConnectionTypes tType = dbConnectionTypes.ODBC;
							string strParameterSymbol = "";
							OdbcParameter tParm = null;
							OdbcParameter tIParm = null;
							OdbcParameter tDParm = null;
							strParameterSymbol = GetParameterSymbol(tType);
							strSQL.Append("Update " + strTableName + " set ");
							strISQL.Append("Insert into [" + strTableName + "] (");
							strIValueSQL.Append(") Values (");
							strDSQL.Append("Delete from " + strTableName + " where ");
							if (tCMD != null)
							{
								tCMD.Parameters.Clear();
							}
							if (tICmd != null)
							{
								tICmd.Parameters.Clear();
							}
							if (tDCmd != null)
							{
								tDCmd.Parameters.Clear();
							}
							foreach (System.Data.DataColumn tCol in TheDataTable.Columns)
							{
								boolFound = false;
								for (x = 0; x <= TheDataTable.PrimaryKey.Length - 1; x++)
								{
									if (tCol.ColumnName.ToString().ToLower() == TheDataTable.PrimaryKey[x].ColumnName.ToString().ToLower())
									{
										boolFound = true;
										break;
									}
								}
								if (!boolFound)
								{
									strSQL.Append(strComma + "[" + tCol.ColumnName.ToString() + "] = " + strParameterSymbol + tCol.ColumnName.ToString());
									strISQL.Append(strComma + "[" + tCol.ColumnName.ToString() + "]");
									strIValueSQL.Append(strComma + "@" + tCol.ColumnName.ToString());
									strComma = ",";
									tParm = tCMD.CreateParameter();
									tParm.ParameterName = "@" + tCol.ColumnName.ToString();
									tParm.SourceColumn = tCol.ColumnName.ToString();
									if (tCol.DataType.Equals(typeof(bool)))
									{
										tParm.DbType = DbType.Boolean;
									}
									else if (tCol.DataType.Equals(typeof(Int16)))
									{
										tParm.DbType = DbType.Int16;
									}
									else if (tCol.DataType.Equals(typeof(int)))
									{
										tParm.DbType = DbType.Int32;
									}
									else if (tCol.DataType.Equals(typeof(Int32)))
									{
										tParm.DbType = DbType.Int32;
									}
									else if (tCol.DataType.Equals(typeof(Int64)))
									{
										tParm.DbType = DbType.Int64;
									}
									else if (tCol.DataType.Equals(typeof(string)))
									{
										tParm.DbType = DbType.String;
									}
									else if (tCol.DataType.Equals(typeof(System.DateTime)))
									{
										tParm.DbType = DbType.Date;
									}
									else if (tCol.DataType.Equals(typeof(double)))
									{
										tParm.DbType = DbType.Double;
									}
									else if (tCol.DataType.Equals(typeof(decimal)))
									{
										tParm.DbType = DbType.Decimal;
									}
									else if (tCol.DataType.Equals(typeof(Guid)))
									{
										tParm.DbType = DbType.Guid;
									}
									else if (tCol.DataType.Equals(typeof(byte)))
									{
										tParm.DbType = DbType.Byte;
									}
									else if (tCol.DataType.Equals(typeof(sbyte)))
									{
										tParm.DbType = DbType.SByte;
									}
									if (tICmd != null)
									{
										tIParm = tICmd.CreateParameter();
										tIParm.ParameterName = tParm.ParameterName;
										tIParm.DbType = tParm.DbType;
										tIParm.SourceColumn = tParm.SourceColumn;
										tICmd.Parameters.Add(tIParm);
									}
									tCMD.Parameters.Add(tParm);
								}
								else
								{
									if (tICmd != null)
									{
										tIParm = tICmd.CreateParameter();
										tIParm.ParameterName = "@" + tCol.ColumnName.ToString();
										if (tCol.DataType.Equals(typeof(Int16)))
										{
											tIParm.DbType = DbType.Int16;
										}
										else if (tCol.DataType.Equals(typeof(int)))
										{
											tIParm.DbType = DbType.Int32;
										}
										else if (tCol.DataType.Equals(typeof(Int32)))
										{
											tIParm.DbType = DbType.Int32;
										}
										else if (tCol.DataType.Equals(typeof(Int64)))
										{
											tIParm.DbType = DbType.Int64;
										}
										else if (tCol.DataType.Equals(typeof(Guid)))
										{
											tIParm.DbType = DbType.Guid;
										}
										tIParm.SourceColumn = tCol.ColumnName.ToString();
										tIParm.Direction = ParameterDirection.Output;
										tICmd.Parameters.Add(tIParm);
									}
								}
							}
							if (TheDataTable.PrimaryKey != null)
							{
								if (TheDataTable.PrimaryKey.Length > 0)
								{
									strSQL.Append(" where ");
									for (x = 0; x <= TheDataTable.PrimaryKey.Length - 1; x++)
									{
										strSQL.Append(strAnd + "[" + TheDataTable.PrimaryKey[x].ColumnName.ToString() + "] = " + strParameterSymbol + TheDataTable.PrimaryKey[x].ColumnName.ToString());
										//strISQL.Append(strAnd & "[" & TheDataTable.PrimaryKey[x].ColumnName.ToString()& "] = @" & TheDataTable.PrimaryKey[x].ColumnName.ToString())
										strDSQL.Append(strAnd + "[" + TheDataTable.PrimaryKey[x].ColumnName.ToString() + "] = @" + TheDataTable.PrimaryKey[x].ColumnName.ToString());
										strAnd = " and ";
										tParm = tCMD.CreateParameter();
										tParm.ParameterName = "@" + TheDataTable.PrimaryKey[x].ColumnName.ToString();
										tParm.SourceColumn = TheDataTable.PrimaryKey[x].ColumnName.ToString();
										if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(bool)))
										{
											tParm.DbType = DbType.Boolean;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(Int16)))
										{
											tParm.DbType = DbType.Int16;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(int)))
										{
											tParm.DbType = DbType.Int32;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(Int32)))
										{
											tParm.DbType = DbType.Int32;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(Int64)))
										{
											tParm.DbType = DbType.Int64;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(string)))
										{
											tParm.DbType = DbType.String;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(System.DateTime)))
										{
											tParm.DbType = DbType.Date;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(double)))
										{
											tParm.DbType = DbType.Double;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(decimal)))
										{
											tParm.DbType = DbType.Decimal;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(Guid)))
										{
											tParm.DbType = DbType.Guid;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(byte)))
										{
											tParm.DbType = DbType.Byte;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(sbyte)))
										{
											tParm.DbType = DbType.SByte;
										}
										//If Not tICmd Is Nothing Then
										//    tIParm = tICmd.CreateParameter
										//    tIParm.ParameterName = tParm.ParameterName
										//    tIParm.SourceColumn = tParm.SourceColumn
										//    tIParm.DbType = tParm.DbType
										//    tIParm.Direction = ParameterDirection.Output
										//    tICmd.Parameters.Add(tIParm)
										//End If
										if (tDCmd != null)
										{
											tDParm = tDCmd.CreateParameter();
											tDParm.ParameterName = tParm.ParameterName;
											tDParm.SourceColumn = tParm.SourceColumn;
											tDParm.DbType = tParm.DbType;
											tDCmd.Parameters.Add(tDParm);
										}
										tCMD.Parameters.Add(tParm);
									}
								}
							}
							tCMD.CommandText = strSQL.ToString();
							if (tICmd != null)
							{
								strISQL.Append(strIValueSQL.ToString() + ")");
								strISQL.Append(" set @" + TheDataTable.PrimaryKey[0].ColumnName.ToString() + " = SCOPE_IDENTITY()");
								tICmd.CommandText = strISQL.ToString();
							}
							if (tDCmd != null)
							{
								tDCmd.CommandText = strDSQL.ToString();
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
			}
		}

		public void CreateSQLCommands(string strTableName, SqlCommand tCMD, SqlCommand tICmd, SqlCommand tDCmd, bool boolOmitNulls = false)
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			try
			{
				if (TheDataTable != null)
				{
					if (TheDataTable.PrimaryKey != null)
					{
						if (!(TheDataTable.PrimaryKey.Length <= 0))
						{
							System.Text.StringBuilder strSQL = new System.Text.StringBuilder();
							System.Text.StringBuilder strISQL = new System.Text.StringBuilder();
							System.Text.StringBuilder strIValueSQL = new System.Text.StringBuilder();
							System.Text.StringBuilder strDSQL = new System.Text.StringBuilder();
							int x;
							bool boolFound = false;
							string strComma = "";
							string strInsertComma = "";
							string strAnd = " ";
							dbConnectionTypes tType = dbConnectionTypes.SQLServer;
							string strParameterSymbol = "";
							SqlParameter tParm = null;
							SqlParameter tIParm = null;
							SqlParameter tDParm = null;
							bool boolTemp = false;
							strParameterSymbol = GetParameterSymbol(tType);
							//If Not strTableName.Contains("[") Then
							strSQL.Append("Update [" + strTableName + "] set ");
							strISQL.Append("Insert into [" + strTableName + "] (");
							strIValueSQL.Append(") Values (");
							strDSQL.Append("Delete from [" + strTableName + "] where ");
							//Else
							//    strSQL.Append("Update " & strTableName & " set ")
							//    strISQL.Append("Insert into [" & strTableName & "] (")
							//    strIValueSQL.Append(") Values (")
							//    strDSQL.Append("Delete from " & strTableName & " where ")
							//End If
							if (tCMD != null)
							{
								tCMD.Parameters.Clear();
							}
							if (tICmd != null)
							{
								tICmd.Parameters.Clear();
							}
							if (tDCmd != null)
							{
								tDCmd.Parameters.Clear();
							}
							foreach (System.Data.DataColumn tCol in TheDataTable.Columns)
							{
								if (!tCol.ExtendedProperties.ContainsKey("unmappedcolumn"))
								{
									boolFound = false;
									for (x = 0; x <= TheDataTable.PrimaryKey.Length - 1; x++)
									{
										if (tCol.ColumnName.ToString().ToLower() == TheDataTable.PrimaryKey[x].ColumnName.ToString().ToLower())
										{
											boolFound = true;
											break;
										}
									}
									if (!boolFound)
									{
										strSQL.Append(strComma + "[" + tCol.ColumnName.ToString() + "] = " + strParameterSymbol + tCol.ColumnName.ToString());
										boolTemp = false;
										if (boolOmitNulls)
										{
											if (lngCurrentRow >= 0)
											{
												if (TheDataTable.Rows[lngCurrentRow].RowState != DataRowState.Deleted)
												{
													boolTemp = TheDataTable.Rows[lngCurrentRow].IsNull(tCol);
												}
											}
										}
										if (!boolOmitNulls || !boolTemp)
										{
											strISQL.Append(strInsertComma + "[" + tCol.ColumnName.ToString() + "]");
											strIValueSQL.Append(strInsertComma + "@" + tCol.ColumnName.ToString());
											strInsertComma = ",";
										}
										strComma = ",";
										tParm = tCMD.CreateParameter();
										tParm.ParameterName = "@" + tCol.ColumnName.ToString();
										tParm.SourceColumn = tCol.ColumnName.ToString();
										if (tCol.DataType.Equals(typeof(bool)))
										{
											tParm.DbType = DbType.Boolean;
										}
										else if (tCol.DataType.Equals(typeof(Int16)))
										{
											tParm.DbType = DbType.Int16;
										}
										else if (tCol.DataType.Equals(typeof(int)))
										{
											tParm.DbType = DbType.Int32;
										}
										else if (tCol.DataType.Equals(typeof(Int32)))
										{
											tParm.DbType = DbType.Int32;
										}
										else if (tCol.DataType.Equals(typeof(Int64)))
										{
											tParm.DbType = DbType.Int64;
										}
										else if (tCol.DataType.Equals(typeof(string)))
										{
											tParm.DbType = DbType.String;
											tParm.Size = tCol.MaxLength;
										}
										else if (tCol.DataType.Equals(typeof(System.DateTime)))
										{
											tParm.DbType = DbType.Date;
										}
										else if (tCol.DataType.Equals(typeof(double)))
										{
											tParm.DbType = DbType.Double;
										}
										else if (tCol.DataType.Equals(typeof(decimal)))
										{
											tParm.DbType = DbType.Decimal;
										}
										else if (tCol.DataType.Equals(typeof(Guid)))
										{
											tParm.DbType = DbType.Guid;
										}
										else if (tCol.DataType.Equals(typeof(byte)))
										{
											tParm.DbType = DbType.Byte;
										}
										else if (tCol.DataType.Equals(typeof(sbyte)))
										{
											tParm.DbType = DbType.SByte;
										}
										else if (tCol.DataType.IsArray)
										{
											if (tCol.DataType.GetElementType() == typeof(byte))
											{
												tParm.DbType = DbType.Binary;
											}
										}
										if (tICmd != null & (!boolOmitNulls || !boolTemp))
										{
											tIParm = tICmd.CreateParameter();
											tIParm.ParameterName = tParm.ParameterName;
											tIParm.DbType = tParm.DbType;
											tIParm.SourceColumn = tParm.SourceColumn;
											tIParm.Size = tParm.Size;
											tICmd.Parameters.Add(tIParm);
										}
										tCMD.Parameters.Add(tParm);
									}
									else
									{
										if (tICmd != null)
										{
											tIParm = tICmd.CreateParameter();
											tIParm.ParameterName = "@" + tCol.ColumnName.ToString();
											if (tCol.DataType.Equals(typeof(Int16)))
											{
												tIParm.DbType = DbType.Int16;
											}
											else if (tCol.DataType.Equals(typeof(int)))
											{
												tIParm.DbType = DbType.Int32;
											}
											else if (tCol.DataType.Equals(typeof(Int32)))
											{
												tIParm.DbType = DbType.Int32;
											}
											else if (tCol.DataType.Equals(typeof(Int64)))
											{
												tIParm.DbType = DbType.Int64;
											}
											else if (tCol.DataType.Equals(typeof(Guid)))
											{
												tIParm.DbType = DbType.Guid;
											}
											tIParm.SourceColumn = tCol.ColumnName.ToString();
											tIParm.Direction = ParameterDirection.Output;
											tICmd.Parameters.Add(tIParm);
										}
									}
								}
							}
							if (TheDataTable.PrimaryKey != null)
							{
								if (TheDataTable.PrimaryKey.Length > 0)
								{
									strSQL.Append(" where ");
									for (x = 0; x <= TheDataTable.PrimaryKey.Length - 1; x++)
									{
										strSQL.Append(strAnd + "[" + TheDataTable.PrimaryKey[x].ColumnName.ToString() + "] = " + strParameterSymbol + TheDataTable.PrimaryKey[x].ColumnName.ToString());
										//strISQL.Append(strAnd & "[" & TheDataTable.PrimaryKey[x].ColumnName.ToString()& "] = @" & TheDataTable.PrimaryKey[x].ColumnName.ToString())
										strDSQL.Append(strAnd + "[" + TheDataTable.PrimaryKey[x].ColumnName.ToString() + "] = @" + TheDataTable.PrimaryKey[x].ColumnName.ToString());
										strAnd = " and ";
										tParm = tCMD.CreateParameter();
										tParm.ParameterName = "@" + TheDataTable.PrimaryKey[x].ColumnName.ToString();
										tParm.SourceColumn = TheDataTable.PrimaryKey[x].ColumnName.ToString();
										if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(bool)))
										{
											tParm.DbType = DbType.Boolean;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(Int16)))
										{
											tParm.DbType = DbType.Int16;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(int)))
										{
											tParm.DbType = DbType.Int32;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(Int32)))
										{
											tParm.DbType = DbType.Int32;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(Int64)))
										{
											tParm.DbType = DbType.Int64;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(string)))
										{
											tParm.DbType = DbType.String;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(System.DateTime)))
										{
											tParm.DbType = DbType.Date;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(double)))
										{
											tParm.DbType = DbType.Double;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(decimal)))
										{
											tParm.DbType = DbType.Decimal;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(Guid)))
										{
											tParm.DbType = DbType.Guid;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(byte)))
										{
											tParm.DbType = DbType.Byte;
										}
										else if (TheDataTable.PrimaryKey[x].DataType.Equals(typeof(sbyte)))
										{
											tParm.DbType = DbType.SByte;
										}
										//If Not tICmd Is Nothing Then
										//    tIParm = tICmd.CreateParameter
										//    tIParm.ParameterName = tParm.ParameterName
										//    tIParm.SourceColumn = tParm.SourceColumn
										//    tIParm.DbType = tParm.DbType
										//    tIParm.Direction = ParameterDirection.Output
										//    tICmd.Parameters.Add(tIParm)
										//End If
										if (tDCmd != null)
										{
											tDParm = tDCmd.CreateParameter();
											tDParm.ParameterName = tParm.ParameterName;
											tDParm.SourceColumn = tParm.SourceColumn;
											tDParm.DbType = tParm.DbType;
											tDCmd.Parameters.Add(tDParm);
										}
										tCMD.Parameters.Add(tParm);
									}
								}
							}
							tCMD.CommandText = strSQL.ToString();
							if (tICmd != null)
							{
								strISQL.Append(strIValueSQL.ToString() + ")");
								strISQL.Append(" set @" + TheDataTable.PrimaryKey[0].ColumnName.ToString() + " = SCOPE_IDENTITY()");
								tICmd.CommandText = strISQL.ToString();
							}
							if (tDCmd != null)
							{
								tDCmd.CommandText = strDSQL.ToString();
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
			}
		}
		//Public Function ConvertTypeToDBType(tType As System.Type) As DbType
		//    If tType.Equals(GetType(Boolean)) Then
		//        Return DbType.Boolean
		//    ElseIf tType.Equals(GetType(Int16)) Then
		//        Return DbType.Int16
		//    ElseIf tType.Equals(GetType(Integer)) Then
		//        Return DbType.Int32
		//    ElseIf tType.Equals(GetType(Int32)) Then
		//        Return DbType.Int32
		//    ElseIf tType.Equals(GetType(Int64)) Then
		//        Return DbType.Int64
		//    ElseIf tType.Equals(GetType(String)) Then
		//        Return DbType.String
		//    ElseIf tType.Equals(GetType(Date)) Then
		//        Return DbType.Date
		//    ElseIf tType.Equals(GetTypeFCConvert.ToDouble() Then
		//        Return DbType.Double
		//    ElseIf tType.Equals(GetTypeFCConvert.ToDecimal() Then
		//        Return DbType.Decimal
		//    ElseIf tType.Equals(GetType(Guid)) Then
		//        Return DbType.Guid
		//    ElseIf tType.Equals(GetType(Byte)) Then
		//        Return DbType.Byte
		//    ElseIf tType.Equals(GetType(SByte)) Then
		//        Return DbType.SByte
		//    End If
		//    Return DbType.Object
		//End Function
		public object GetData(string strFieldName)
		{
			//  intLastErrorCode = 0
			return Fields(strFieldName);
			//If Not TheDataTable Is Nothing Then
			//    If Not lngCurrentRow < 0 Then
			//        Return TheDataTable.Rows[lngCurrentRow][strFieldName]
			//        Return
			//    Else
			//        Return Nothing
			//    End If
			//Else
			//    Return Nothing
			//End If
		}

		public void SetData(string strFieldName, object obValue)
		{
			//intLastErrorCode = 0
			//If Not TheDataTable Is Nothing Then
			//    TheDataTable.Rows[lngCurrentRow][strFieldName] = obValue
			//End If
			SetField(strFieldName, obValue);
		}

		public object GetFirstRecord()
		{
			//intLastErrorCode = 0
			//If Not TheDataTable.Rows[lngCurrentRow].Item(0) Is Nothing Then
			//    Return TheDataTable.Rows[lngCurrentRow].Item(0)
			//Else
			//    Return ""
			//End If
			return FieldsIndexValue(0);
		}

		public string Name()
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			return strCurSelect;
		}

		public bool IsntAnything()
		{
			if (TheDataTable != null)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		public void Reset()
		{
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			lngCurrentRow = -1;
			//If Not TheDataTable Is Nothing Then
			//    Try
			//        lngCurrentRow = -1
			//        TheDataTable.Rows.Clear()
			//        TheDataTable.Dispose()
			//        TheDataTable = Nothing
			//    Catch ex As Exception
			//    End Try
			//End If
		}

		public System.Data.DataTable GetDataTable()
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			return TheDataTable;
		}

		public int BookMark()
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			return lngCurrentRow;
		}

		public void SetRow(int intRow)
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			boolJustDeleted = false;
			if (intRow < TheDataTable.Rows.Count)
			{
				lngCurrentRow = intRow;
			}
		}

		public bool BeginTrans()
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			tranScope = new System.Transactions.TransactionScope();
			//oldAmbient = Transaction.Current
			//Dim cTrans As CommittableTransaction = New CommittableTransaction
			//Transaction.Current = cTrans
			//Try
			//    cTrans.BeginCommit(Nothing, Nothing)
			//Catch ex As Exception
			//    intLastErrorCode = 1
			//    strLastErrorDescription = ex.Message
			//    Return False
			//Finally
			//    Transaction.Current = oldAmbient
			//End Try
			return false;
		}

		public void EndTrans()
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			//theTransaction.Commit()
			if (tranScope != null)
			{
				tranScope.Complete();
				tranScope.Dispose();
				tranScope = null;
			}
		}

		public void Rollback()
		{
			//intLastErrorCode = 0
			//strLastErrorDescription = ""
			ClearState();
			//theTransaction.Rollback()
			if (tranScope != null)
			{
				tranScope.Dispose();
				tranScope = null;
			}
		}

		private void OnCommitted(IAsyncResult aResult)
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			CommittableTransaction cTran;
			cTran = (CommittableTransaction)aResult;
			try
			{
				using (cTran)
				{
					cTran.EndCommit(aResult);
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		public void Delete()
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			try
			{
				if (TheDataTable != null)
				{
					if (lngCurrentRow < TheDataTable.Rows.Count & lngCurrentRow >= 0)
					{
						TheDataTable.Rows[lngCurrentRow].Delete();
						boolJustDeleted = true;
						if (lngCurrentRow >= TheDataTable.Rows.Count)
						{
							lngCurrentRow = -1;
						}
					}
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
			}
		}

		public bool DoesFieldExist(string strFieldName, string strTableName, string strDatabaseName = "")
		{
			return false;
		}

		public bool TableExists(string strTableName, string strConnectionName)
		{
			try
			{
				if (Strings.Trim(strTableName) != "" & Strings.Trim(strConnectionName) != "")
				{
					return OpenDataTable("select top 1 * from " + strTableName, strConnectionName, "", false, true);
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return false;
			}
		}

		public string TableToJSON()
		{
			try
			{
				TableItem tTab = TableToTableItem();
				if (tTab != null)
				{
					System.Web.Script.Serialization.JavaScriptSerializer tSer = new System.Web.Script.Serialization.JavaScriptSerializer();
					return tSer.Serialize(tTab);
				}
				else
				{
					return "";
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return "";
			}
		}

		public TableItem TableToTableItem()
		{
			try
			{
				ClearErrors();
				TableItem tTab = new TableItem();
				RecordItem tRec;
				tTab.TableName = TheDataTable.TableName;
				foreach (DataRow tRow in TheDataTable.Rows)
				{
					tRec = new RecordItem();
					foreach (DataColumn tcol in TheDataTable.Columns)
					{
						tRec.Fields.Add(tcol.ColumnName, tRow[tcol.ColumnName].ToString());
					}
					tTab.Records.Add(tRec);
				}
				return tTab;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				strLastErrorDescription = ex.Message;
				return null;
			}
		}

		private string RowToJSON(DataRow tRow)
		{
			try
			{
				if (tRow != null)
				{
					RecordItem tRec = new RecordItem();
					foreach (DataColumn tCol in tRow.Table.Columns)
					{
						tRec.Fields.Add(tCol.ColumnName, tRow[tCol.ColumnName].ToString());
					}
					System.Web.Script.Serialization.JavaScriptSerializer tSer = new System.Web.Script.Serialization.JavaScriptSerializer();
					return tSer.Serialize(tRec);
				}
				else
				{
					return "";
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return "";
			}
		}

		public string CurrentRecordToJSON()
		{
			try
			{
				if (TheDataTable != null)
				{
					if (lngCurrentRow >= 0 & TheDataTable.Rows.Count > 0)
					{
						return RowToJSON(TheDataTable.Rows[lngCurrentRow]);
					}
				}
				return "";
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return "";
			}
		}

		public bool JSONToTable(bool boolAsNew, string strJSON, string strIDField)
		{
			try
			{
				if (strJSON != "")
				{
					TableItem tTab;
					GenericJSONTableSerializer tSer = new GenericJSONTableSerializer();
					tTab = tSer.GetTableItemFromJSON(strJSON);
					if (tTab != null)
					{
						return TableItemToTable(boolAsNew, tTab, strIDField);
					}
				}
				else
				{
					return false;
				}
				return false;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return false;
			}
		}

		public bool JSONCollectionToTable(bool boolAsNew, string strJSON, string strIDField)
		{
			try
			{
				if (strJSON != "")
				{
					TableCollection tColl;
					GenericJSONTableSerializer tSer = new GenericJSONTableSerializer();
					tColl = tSer.GetCollectionFromJSON(strJSON);
					if (tColl != null)
					{
						if (tColl.TableCount() > 0)
						{
							TableItem tTableItem;
							tTableItem = tColl.TableItems[0];
							return TableItemToTable(boolAsNew, tTableItem, strIDField);
						}
					}
				}
				else
				{
					return false;
				}
				return false;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return false;
			}
		}

		public bool TableItemToTable(bool boolAsNew, TableItem tTableItem, string strIDField)
		{
			try
			{
				if (tTableItem != null)
				{
					bool boolFound = false;
					DataRow tRow = null;
					string strVal = "";
					string strField = "";
					foreach (RecordItem tRec in tTableItem.Records)
					{
						boolFound = false;
						if (!boolAsNew)
						{
							//try to find it
							foreach (DataRow tRow1 in TheDataTable.Rows)
							{
								if (tRow1[strIDField].ToString() == tRec.Fields[strIDField])
								{
									boolFound = true;
									break;
								}
							}
						}
						if (!boolFound)
						{
							tRow = TheDataTable.NewRow();
						}
						//FC:COMP:JEI
						//for (int x = 0; x <= tRec.Fields.Count - 1; x++)
						//{
						//    strVal = tRec.Fields.ElementAt(x).Value;
						//    strField = tRec.Fields.ElementAt(x).Key;
						//    if (strField.ToLower() != strIDField.ToLower())
						//    {
						//        if (strVal.ToLower() != "null")
						//        {
						//            tRow[strField] = strVal;
						//        }
						//        else
						//        {
						//            tRow[strField] = DBNull.Value;
						//        }
						//    }
						//}
						foreach (KeyValuePair<string, string> item in tRec.Fields)
						{
							strVal = item.Value;
							strField = item.Key;
							if (strField.ToLower() != strIDField.ToLower())
							{
								if (strVal.ToLower() != "null")
								{
									tRow[strField] = strVal;
								}
								else
								{
									tRow[strField] = DBNull.Value;
								}
							}
						}
						if (!boolFound)
						{
							TheDataTable.Rows.Add(tRow);
						}
					}
					return true;
				}
				return false;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return false;
			}
		}

		public bool RecordItemToRow(bool boolAsNew, ref RecordItem tRec, string strIDField)
		{
			try
			{
				bool boolFound;
				DataRow tRow = null;
				string strVal;
				string strField;
				if (tRec != null)
				{
					boolFound = false;
					if (!boolAsNew)
					{
						//try to find it
						foreach (DataRow tRowItem in TheDataTable.Rows)
						{
							if (tRowItem[strIDField].ToString() == tRec.Fields[strIDField])
							{
								boolFound = true;
								break;
							}
						}
					}
					if (!boolFound)
					{
						tRow = TheDataTable.NewRow();
					}
					//FC:COMP:JEI
					//for (int x = 0; x <= tRec.Fields.Count - 1; x++) {
					//    strVal = tRec.Fields.ElementAt(x).Value;
					//    strField = tRec.Fields.ElementAt(x).Key;
					//    if (strField.ToLower()!= strIDField.ToLower()) {
					//        if (strVal.ToLower()!= "null") {
					//            tRow[strField] = strVal;
					//        } else {
					//            tRow[strField] = DBNull.Value;
					//        }
					//    }
					//}
					foreach (KeyValuePair<string, string> item in tRec.Fields)
					{
						strVal = item.Value;
						strField = item.Key;
						if (strField.ToLower() != strIDField.ToLower())
						{
							if (strVal.ToLower() != "null")
							{
								tRow[strField] = strVal;
							}
							else
							{
								tRow[strField] = DBNull.Value;
							}
						}
					}
					if (!boolFound)
					{
						TheDataTable.Rows.Add(tRow);
					}
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return false;
			}
		}

		public string GUIDToString(ref object tGuid)
		{
			//If Not tGuid Is Nothing Then
			return ((Guid)tGuid).ToString();
			//End If
		}

		public object StringToGUID(string strGUID)
		{
			if (strGUID != "")
			{
				Guid tGuid = new Guid(strGUID);
				return tGuid;
			}
			else
			{
				return null;
			}
		}

		private bool _OmitNullsOnInsert = false;

		public bool OmitNullsOnInsert
		{
			get
			{
				return _OmitNullsOnInsert;
			}
			set
			{
				_OmitNullsOnInsert = value;
			}
		}

		public object GetDefaultValue(string strFieldName)
		{
			return TheDataTable.Columns[strFieldName].DefaultValue;
		}

		public void SetDefaultValue(string strFieldName, object value)
		{
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			try
			{
				if (value != null)
				{
					if (!Convert.IsDBNull(value))
					{
						object thevalue;
						if (value.GetType().ToString() != "System.__ComObject" && value.GetType().ToString().ToLower() != "combobox")
						{
							thevalue = value;
						}
						else
						{
							thevalue = value.ToString();
						}
						if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(System.DateTime)))
						{
							if (Information.IsDate(thevalue))
							{
								TheDataTable.Columns[strFieldName].DefaultValue = FCConvert.ToDateTime(thevalue.ToString());
							}
							else
							{
								TheDataTable.Columns[strFieldName].DefaultValue = DBNull.Value;
							}
						}
						else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(int)))
						{
							TheDataTable.Columns[strFieldName].DefaultValue = FCConvert.ToInt32(Conversion.Val(thevalue));
						}
						else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(double)))
						{
							TheDataTable.Columns[strFieldName].DefaultValue = Conversion.Val(thevalue);
						}
						else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(decimal)))
						{
							TheDataTable.Columns[strFieldName].DefaultValue = Conversion.Val(thevalue);
						}
						else if (TheDataTable.Columns[strFieldName].DataType.Equals(typeof(string)))
						{
							TheDataTable.Columns[strFieldName].DefaultValue = thevalue.ToString();
						}
						else
						{
							TheDataTable.Columns[strFieldName].DefaultValue = thevalue;
						}
					}
				}
			}
			catch (DbException dex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(dex);
				intLastErrorCode = dex.ErrorCode;
				strLastErrorDescription = dex.Message;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 1;
				strLastErrorDescription = ex.Message;
			}
		}

		public void BulkCopy(string tableName, string strConnectionName, bool boolAutoMapColumns)
		{
			string connString;
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			try
			{
				connString = Statics.ConnectionPools.GetConnectionString(strConnectionName, strGroup, strMegaGroup);
				using (System.Data.SqlClient.SqlBulkCopy bulkCpy = new System.Data.SqlClient.SqlBulkCopy(connString))
				{
					System.Data.SqlClient.SqlBulkCopy bulkCpyRef = bulkCpy;
					if (boolAutoMapColumns)
					{
						AutoMapBulkCopyColumns(ref bulkCpyRef, false);
					}
					bulkCpyRef.BulkCopyTimeout = 0;
					bulkCpyRef.DestinationTableName = tableName;
					bulkCpyRef.WriteToServer(TheDataTable);
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 9999;
				strLastErrorDescription = ex.Message;
			}
		}

		public void BulkCopyWithID(string tableName, string strConnectionName, bool boolAutoMapColumns)
		{
			string connString;
			intLastErrorCode = 0;
			strLastErrorDescription = "";
			try
			{
				connString = Statics.ConnectionPools.GetConnectionString(strConnectionName, strGroup, strMegaGroup);
				using (System.Data.SqlClient.SqlBulkCopy bulkCpy = new System.Data.SqlClient.SqlBulkCopy(connString, System.Data.SqlClient.SqlBulkCopyOptions.KeepIdentity))
				{
					System.Data.SqlClient.SqlBulkCopy bulkCpyRef = bulkCpy;
					if (boolAutoMapColumns)
					{
						AutoMapBulkCopyColumns(ref bulkCpyRef, true);
					}
					bulkCpyRef.BulkCopyTimeout = 0;
					bulkCpyRef.DestinationTableName = tableName;
					bulkCpyRef.WriteToServer(TheDataTable);
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				intLastErrorCode = 9999;
				strLastErrorDescription = ex.Message;
			}
		}

		private void AutoMapBulkCopyColumns(ref System.Data.SqlClient.SqlBulkCopy bulkCpy, bool boolMapID)
		{
			if (bulkCpy != null)
			{
				foreach (DataColumn col in TheDataTable.Columns)
				{
					if (boolMapID || col.ColumnName.ToLower() != "id")
					{
						bulkCpy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
					}
				}
			}
		}

		public class StaticVariables
		{
			public ConnectionPool ConnectionPools = null;
			//[ComClass(DataReader.ClassId, DataReader.InterfaceId, DataReader.EventsId)]
			public string strDefaultDatabaseAlias = "DefaultDatabase";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
	//[ComClass(DBManipulator.ClassId, DBManipulator.InterfaceId, DBManipulator.EventsId)]
	public class DBManipulator
	{
		#region "COM GUIDs"
		// These  GUIDs provide the COM identity for this class
		// and its COM interfaces. If you change them, existing
		// clients will no longer be able to access the class.
		public const string ClassId = "c2efa256-a993-4391-8cf2-c73add41c7e0";
		public const string InterfaceId = "a01ede67-197b-4db4-a38d-3b99bb7a8cc1";
		#endregion

		public const string EventsId = "5567e639-890e-4836-ac81-d5a2341dae0a";
		// A creatable COM class must have a Public Sub New()
		// with no parameters, otherwise, the class will not be
		// registered in the COM registry and cannot be created
		// via CreateObject.
		public DBManipulator()
		{
			//base.New();
		}

		public enum CoType
		{
			addfield = 0,
			addtable = 1
		}

		public bool TableExists(string strTableName, string strDBAlias)
		{
			//If strTableName.ToString().Trim() = "" Then Return False
			//If strDBAlias.ToString().Trim() = "" Then Return False
			//Dim tCon As DbConnection = Nothing
			//Dim tbl As DataTable = Nothing
			//Try
			//    Dim tDB As New clsDataConnectionDLL.clsDataConnection
			//    Dim tPool As New ConnectionPool
			//    'dim strRestrictions() as String = {Nothing,Nothing,"Tables"}
			//    tCon = tPool.GetConnectionByName(strDBAlias)
			//    If Not tCon Is Nothing Then
			//        If tCon.State <> ConnectionState.Open Then
			//            tCon.Open()
			//        End If
			//        tbl = tCon.GetSchema("Tables")
			//        If Not tbl Is Nothing Then
			//            For Each tRow As DataRow In tbl.Rows
			//                If tRow["TABLE_Name"].ToString().ToLower()= strTableName.ToString().ToLower()Then
			//                    Return True
			//                End If
			//            Next tRow
			//            tbl.Dispose()
			//        End If
			//    End If
			//    'Dim tCon As SqlConnection = CType(tDB., SqlConnection)
			//    'tCon = db.Connection
			//    'tCon.open()
			//    'tbl = tCon.GetSchema("Tables", strRestrictions)
			//    'tCon.Close()
			//    'If Not tbl Is Nothing Then
			//    '    For Each tRow As DataRow In tbl.Rows
			//    '        If tRow["TABLE_NAME"].ToString().ToLower()= strTable.ToString().ToLower()Then
			//    '            tbl.Dispose()
			//    '            Return True
			//    '        End If
			//    '    Next tRow
			//    '    tbl.Dispose()
			//    Return False
			//    'Else
			//    'Return False
			//    'End If
			//Catch ex As Exception
			//Finally
			//    If Not tbl Is Nothing Then
			//        tbl.Dispose()
			//        tbl = Nothing
			//    End If
			//    If Not tCon Is Nothing Then
			//        tCon.Close()
			//        tCon.Dispose()
			//        tCon = Nothing
			//    End If
			//End Try
			return false;
		}
	}
}
