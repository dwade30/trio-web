﻿//Fecher vbPorter - Version 1.0.0.27
using System.Runtime.InteropServices;
using fecherFoundation;

namespace Global
{
	public class cSaveFileDialog
	{
		//=========================================================
		[DllImport("comdlg32.dll", EntryPoint = "GetSaveFileNameA")]
		private static extern int GetSaveFileName(ref OPENFILENAME pOpenfilename);

		[StructLayout(LayoutKind.Sequential)]
		private struct OPENFILENAME
		{
			public int lStructSize;
			public int hwndOwner;
			public int hInstance;
			public string lpstrFilter;
			public string lpstrCustomFilter;
			public int nMaxCustFilter;
			public int nFilterIndex;
			public string lpstrFile;
			public int nMaxFile;
			public string lpstrFileTitle;
			public int nMaxFileTitle;
			public string lpstrInitialDir;
			public string lpstrTitle;
			public int flags;
			public short nFileOffset;
			public short nFileExtension;
			public string lpstrDefExt;
			public int lCustData;
			public int lpfnHook;
			public string lpTemplateName;
		};

		private string strTitle = string.Empty;
		private string strInitialDir = string.Empty;
		private string strFilter = string.Empty;
		private int lngFlags;
		private int intFilterIndex;
		private string strFileName = string.Empty;
		// Private strFileNames() As String
		private bool boolMultiselect;
		private string strDefaultExtension = string.Empty;

		public string DefaultExtension
		{
			set
			{
				strDefaultExtension = value;
			}
			get
			{
				string DefaultExtension = "";
				DefaultExtension = strDefaultExtension;
				return DefaultExtension;
			}
		}

		public int Options
		{
			set
			{
				lngFlags = value;
			}
			get
			{
				int Options = 0;
				Options = lngFlags;
				return Options;
			}
		}

		public bool MultiSelect
		{
			set
			{
				boolMultiselect = value;
			}
			get
			{
				bool MultiSelect = false;
				MultiSelect = boolMultiselect;
				return MultiSelect;
			}
		}

		public string Title
		{
			set
			{
				strTitle = value;
			}
			get
			{
				string Title = "";
				Title = strTitle;
				return Title;
			}
		}

		public string InitialDirectory
		{
			set
			{
				strInitialDir = value;
			}
			get
			{
				string InitialDirectory = "";
				InitialDirectory = strInitialDir;
				return InitialDirectory;
			}
		}

		public string Filter
		{
			set
			{
				strFilter = value;
			}
			get
			{
				string Filter = "";
				Filter = strFilter;
				return Filter;
			}
		}

		public short FilterIndex
		{
			set
			{
				value = FCConvert.ToInt16(intFilterIndex);
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short FilterIndex = 0;
				FilterIndex = FCConvert.ToInt16(intFilterIndex);
				return FilterIndex;
			}
		}

		public string FileName
		{
			set
			{
				strFileName = value;
			}
			get
			{
				string FileName = "";
				FileName = strFileName;
				return FileName;
			}
		}
		// Public Property Get FileNames() As String()
		// FileNames = strFileNames
		// End Property
		public int ShowDialog()
		{
			int ShowDialog = 0;
			OPENFILENAME OFName = new OPENFILENAME();
			OFName.lStructSize = Marshal.SizeOf(OFName);
			//FC:TODO:AM
			//OFName.hInstance = Support.GetHInstance();
			OFName.flags = lngFlags;
			OFName.lpstrFile = Strings.Space(254);
			if (strFileName != "")
			{
				OFName.lpstrFile = strFileName + Strings.Space(254 - strFileName.Length);
			}
			else
			{
				OFName.lpstrFile = Strings.Space(254);
			}
			if (strFilter != "")
			{
				// sFilter = "Batch Files (*.bat)" & Chr(0) & "*.BAT" & Chr(0)
				OFName.lpstrFilter = strFilter;
				if (intFilterIndex > 0)
				{
					OFName.nFilterIndex = intFilterIndex;
				}
			}
			OFName.lpstrDefExt = strDefaultExtension;
			OFName.nMaxFile = 255;
			OFName.lpstrFileTitle = Strings.Space(254);
			OFName.nMaxFileTitle = 255;
			OFName.lpstrInitialDir = strInitialDir;
			OFName.lpstrTitle = strTitle;
			int lngReturn;
			lngReturn = GetSaveFileName(ref OFName);
			if (lngReturn > 0)
			{
				strFileName = OFName.lpstrFile;
			}
			ShowDialog = lngReturn;
			return ShowDialog;
		}
	}
}
