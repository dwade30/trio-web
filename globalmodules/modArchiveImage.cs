﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Wisej.Core;

namespace Global
{
	public class modArchiveImage
	{
		//=========================================================
		//public static void ShowArchiveImage(ref string strArchive)
		//{
		//	if (strArchive != "")
		//	{
		//		if (App.MainForm.WindowState != FormWindowState.Minimized)
		//		{
		//			App.MainForm.picArchive.Width = App.MainForm.Width - App.MainForm.GRID.Width;
		//			if (App.MainForm.picArchive.Width > App.MainForm.picArchive.Height)
		//			{
		//				App.MainForm.imgArchive.Width = App.MainForm.picArchive.Height;
		//				App.MainForm.imgArchive.Height = FCConvert.ToInt32(App.MainForm.picArchive.Height / 3.0);
		//			}
		//			else
		//			{
		//				App.MainForm.imgArchive.Width = App.MainForm.picArchive.Width;
		//				App.MainForm.imgArchive.Height = FCConvert.ToInt32(App.MainForm.picArchive.Height / 3.0);
		//			}
		//			if (App.MainForm.imgArchive.Width < App.MainForm.picArchive.Width)
		//			{
		//				App.MainForm.imgArchive.Left = FCConvert.ToInt32(((App.MainForm.picArchive.Width - App.MainForm.imgArchive.Width) / 2.0));
		//			}
		//			App.MainForm.imgArchive.Top = FCConvert.ToInt32(App.MainForm.picArchive.Height / 3.0);
		//			App.MainForm.picArchive.BackColor = System.Drawing.Color.White;
		//		}
		//		App.MainForm.imgArchive.Visible = true;
		//	}
		//	else
		//	{
		//		App.MainForm.picArchive.Visible = false;
		//		App.MainForm.imgArchive.Visible = false;
		//	}
		//}
		public static void ShowArchiveImage(ref string strArchive)
		{
			//FC:FINAL:CHN - issue #1516: Add showing archive image after running archive.
			dynamic MainForm = App.MainForm;
			var picArchive = (FCPictureBox)MainForm.PicArchive;
			var imgArchive = (FCPictureBox)MainForm.imgArchive;
			if (strArchive != "")
			{
				// TODO: check sizes
				if (MainForm.WindowState != FormWindowState.Minimized)
				{
					// picArchive.Width = MainForm.Width- App.MainForm.GRID.Width
					picArchive.Width = MainForm.Width - picArchive.Left;
					if (picArchive.Width > picArchive.Height)
					{
						imgArchive.Width = picArchive.Height;
						imgArchive.Height = FCConvert.ToInt32(picArchive.Height / 3.0);
					}
					else
					{
						imgArchive.Width = picArchive.Width;
						imgArchive.Height = FCConvert.ToInt32(picArchive.Height / 3.0);
					}
					if (imgArchive.Width < picArchive.Width)
					{
						imgArchive.Left = FCConvert.ToInt32(((picArchive.Width - imgArchive.Width) / 2.0));
					}
					imgArchive.Top = FCConvert.ToInt32(picArchive.Height / 3.0);
					// picArchive.BackColor = System.Drawing.Color.White;
				}
				picArchive.Visible = true;
				imgArchive.Visible = true;
			}
			else
			{
				picArchive.Visible = false;
				imgArchive.Visible = false;
			}
		}
	}
}
