﻿using Wisej.Web;
using fecherFoundation;
using Wisej.Core;

namespace Global
{
	public class modBlockEntry
	{
		public static void WriteYY(bool boolUseRegistry = false)
		{
			if (Strings.UCase(App.EXEName) == "TWGNENTY")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "GN", boolUseRegistry);
			}
			else if (Strings.UCase(App.EXEName) == "TWMV0000")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "MV", boolUseRegistry);
			}
			else if (Strings.UCase(App.EXEName) == "TWBD0000")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "BD", boolUseRegistry);
			}
			else if (Strings.UCase(App.EXEName) == "TWRE0000")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "RE", boolUseRegistry);
			}
			else if (Strings.UCase(App.EXEName) == "TWRB0000")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "RB", boolUseRegistry);
			}
			else if (Strings.UCase(App.EXEName) == "TWPP0000")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "PP", boolUseRegistry);
			}
			else if (Strings.UCase(App.EXEName) == "TWE90000")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "E9", boolUseRegistry);
			}
			else if (Strings.UCase(App.EXEName) == "TWCK0000")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "CK", boolUseRegistry);
			}
			else if (Strings.UCase(App.EXEName) == "TWPY0000")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "PY", boolUseRegistry);
			}
			else if (Strings.UCase(App.EXEName) == "TWCR0000")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "CR", boolUseRegistry);
			}
			else if (Strings.UCase(App.EXEName) == "TWCL0000")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "CL", boolUseRegistry);
			}
			else if (Strings.UCase(App.EXEName) == "TWUT0000")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "UT", boolUseRegistry);
			}
			else if (Strings.UCase(App.EXEName) == "TWVR0000")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "VR", boolUseRegistry);
			}
			else if (Strings.UCase(App.EXEName) == "TWCE0000")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "CE", boolUseRegistry);
			}
			else if (Strings.UCase(App.EXEName) == "TWFA0000")
			{
				modReplaceWorkFiles.EntryFlagFile(true, "FA", boolUseRegistry);
			}
			else
			{
				modReplaceWorkFiles.EntryFlagFile(true, "YY", boolUseRegistry);
			}
		}

		
	}
}
