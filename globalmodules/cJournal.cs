﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace Global
{
	public class cJournal
	{
		//=========================================================
		private int lngRecordID;
		private int lngJournalNumber;
		private string strJournalDescription = string.Empty;
		private string strJournalType = string.Empty;
		private string strJournalStatus = "";
		private int intJournalPeriod;
		private bool boolPostedOOB;
		private string strOOBPostingOpID = string.Empty;
		private string strOOBPostingDate = "";
		private string strOOBPostingTime = string.Empty;
		private string strStatusChangeDate = "";
		private string strStatusChangeTime = string.Empty;
		private string strStatusChangeOpID = string.Empty;
		private string strCheckDate = "";
		private bool boolAutomaticJournal;
		private bool boolCreditMemo;
		private bool boolCreditMemoCorrection;
		private int lngBankNumber;
		private double dblTotalAmount;
		private double dblTotalCreditMemo;
		private int lngCRPeriodCloseoutKey;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collJournalEntries = new cGenericCollection();
		private cGenericCollection collJournalEntries_AutoInitialized;

		private cGenericCollection collJournalEntries
		{
			get
			{
				if (collJournalEntries_AutoInitialized == null)
				{
					collJournalEntries_AutoInitialized = new cGenericCollection();
				}
				return collJournalEntries_AutoInitialized;
			}
		}

		private bool boolUpdated;
		private bool boolDeleted;

		public bool IsPosted
		{
			get
			{
				bool IsPosted = false;
				IsPosted = Strings.LCase(strJournalStatus) == "p";
				return IsPosted;
			}
		}

		public cGenericCollection JournalEntries
		{
			get
			{
				cGenericCollection JournalEntries = null;
				JournalEntries = collJournalEntries;
				return JournalEntries;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int JournalNumber
		{
			set
			{
				lngJournalNumber = value;
				IsUpdated = true;
			}
			get
			{
				int JournalNumber = 0;
				JournalNumber = lngJournalNumber;
				return JournalNumber;
			}
		}

		public string Description
		{
			set
			{
				strJournalDescription = value;
				IsUpdated = true;
			}
			get
			{
				string Description = "";
				Description = strJournalDescription;
				return Description;
			}
		}

		public string JournalType
		{
			set
			{
				strJournalType = value;
				IsUpdated = true;
			}
			get
			{
				string JournalType = "";
				JournalType = strJournalType;
				return JournalType;
			}
		}

		public string Status
		{
			set
			{
				strJournalStatus = value;
				IsUpdated = true;
			}
			get
			{
				string Status = "";
				Status = strJournalStatus;
				return Status;
			}
		}

		public short Period
		{
			set
			{
				intJournalPeriod = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short Period = 0;
				Period = FCConvert.ToInt16(intJournalPeriod);
				return Period;
			}
		}

		public bool PostedOOB
		{
			set
			{
				boolPostedOOB = value;
				IsUpdated = true;
			}
			get
			{
				bool PostedOOB = false;
				PostedOOB = boolPostedOOB;
				return PostedOOB;
			}
		}

		public string OOBPOstingOpID
		{
			set
			{
				strOOBPostingOpID = value;
				IsUpdated = true;
			}
			get
			{
				string OOBPOstingOpID = "";
				OOBPOstingOpID = strOOBPostingOpID;
				return OOBPOstingOpID;
			}
		}

		public string OOBPostingDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strOOBPostingDate = value;
				}
				else
				{
					strOOBPostingDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string OOBPostingDate = "";
				OOBPostingDate = strOOBPostingDate;
				return OOBPostingDate;
			}
		}

		public string OOBPostingTime
		{
			set
			{
				strOOBPostingTime = value;
				IsUpdated = true;
			}
			get
			{
				string OOBPostingTime = "";
				OOBPostingTime = strOOBPostingTime;
				return OOBPostingTime;
			}
		}

		public string StatusChangeDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strStatusChangeDate = value;
				}
				else
				{
					strStatusChangeDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string StatusChangeDate = "";
				StatusChangeDate = strStatusChangeDate;
				return StatusChangeDate;
			}
		}

		public string StatusChangeTime
		{
			set
			{
				strStatusChangeTime = value;
				IsUpdated = true;
			}
			get
			{
				string StatusChangeTime = "";
				StatusChangeTime = strStatusChangeTime;
				return StatusChangeTime;
			}
		}

		public string StatusChangeOpID
		{
			set
			{
				strStatusChangeOpID = value;
				IsUpdated = true;
			}
			get
			{
				string StatusChangeOpID = "";
				StatusChangeOpID = strStatusChangeOpID;
				return StatusChangeOpID;
			}
		}

		public string CheckDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strCheckDate = value;
				}
				else
				{
					strCheckDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string CheckDate = "";
				CheckDate = strCheckDate;
				return CheckDate;
			}
		}

		public bool IsAutomaticJournal
		{
			set
			{
				boolAutomaticJournal = value;
				IsUpdated = true;
			}
			get
			{
				bool IsAutomaticJournal = false;
				IsAutomaticJournal = boolAutomaticJournal;
				return IsAutomaticJournal;
			}
		}

		public bool IsCreditMemo
		{
			set
			{
				boolCreditMemo = value;
				IsUpdated = true;
			}
			get
			{
				bool IsCreditMemo = false;
				IsCreditMemo = boolCreditMemo;
				return IsCreditMemo;
			}
		}

		public bool IsCreditMemoCorrection
		{
			set
			{
				boolCreditMemoCorrection = value;
				IsUpdated = true;
			}
			get
			{
				bool IsCreditMemoCorrection = false;
				IsCreditMemoCorrection = boolCreditMemoCorrection;
				return IsCreditMemoCorrection;
			}
		}

		public int BankNumber
		{
			set
			{
				lngBankNumber = value;
				IsUpdated = true;
			}
			get
			{
				int BankNumber = 0;
				BankNumber = lngBankNumber;
				return BankNumber;
			}
		}

		public double TotalAmount
		{
			set
			{
				dblTotalAmount = value;
				IsUpdated = true;
			}
			get
			{
				double TotalAmount = 0;
				TotalAmount = dblTotalAmount;
				return TotalAmount;
			}
		}

		public double TotalCreditMemo
		{
			set
			{
				dblTotalCreditMemo = value;
				IsUpdated = true;
			}
			get
			{
				double TotalCreditMemo = 0;
				TotalCreditMemo = dblTotalCreditMemo;
				return TotalCreditMemo;
			}
		}

		public int CRPeriodCloseoutKey
		{
			set
			{
				lngCRPeriodCloseoutKey = value;
				IsUpdated = true;
			}
			get
			{
				int CRPeriodCloseoutKey = 0;
				CRPeriodCloseoutKey = lngCRPeriodCloseoutKey;
				return CRPeriodCloseoutKey;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}
	}
}
