﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using System.Collections.Generic;
using TWSharedLibrary;

namespace Global
{
	public class cCEPartyApp : SharedApplication.ICentralPartyApp
	{
		//=========================================================
		const string strModuleCode = "CE";

		public bool ReferencesParty(int lngID)
		{
			bool ICentralPartyApp_ReferencesParty = false;
			ICentralPartyApp_ReferencesParty = false;
			bool retAnswer;
			retAnswer = false;
			if (lngID > 0)
			{
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select count(id)as theCount from cemaster where owner1id = " + FCConvert.ToString(lngID) + " or owner2id = " + FCConvert.ToString(lngID), "CodeEnforcement");
				// TODO: Field [theCount] not found!! (maybe it is an alias?)
				if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
				{
					retAnswer = true;
				}
				ICentralPartyApp_ReferencesParty = retAnswer;
			}
			return ICentralPartyApp_ReferencesParty;
		}

		public List<object> AccountsReferencingParty(int lngID)
		{
			List<object> CentralPartyApp_AccountsReferencingParty = null;
			var theCollection = new List<object>();
			clsDRWrapper rsLoad = new clsDRWrapper();
			cCentralPartyReference pRef;
			if (lngID > 0)
			{
				rsLoad.OpenRecordset("select  id, CEAccount,Maplot from cemaster where owner1id = " + FCConvert.ToString(lngID) + " or owner2id = " + FCConvert.ToString(lngID) + " order by ceaccount", "CodeEnforcement");
				while (!rsLoad.EndOfFile())
				{
					//Application.DoEvents();
					pRef = new cCentralPartyReference();
					pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					pRef.ModuleCode = strModuleCode;
					// TODO: Field [CEaccount] not found!! (maybe it is an alias?)
					pRef.Identifier = FCConvert.ToString(rsLoad.Get_Fields("CEaccount"));
					// TODO: Field [CEaccount] not found!! (maybe it is an alias?)
					pRef.IdentifierForSort = Strings.Right(Strings.StrDup(12, "0") + rsLoad.Get_Fields("CEaccount"), 12);
					pRef.AlternateIdentifier = FCConvert.ToString(rsLoad.Get_Fields_String("Maplot"));
					pRef.Description = "Code Enforcement Account";
					theCollection.Add(pRef);
					rsLoad.MoveNext();
				}
			}
			CentralPartyApp_AccountsReferencingParty = theCollection;
			return CentralPartyApp_AccountsReferencingParty;
		}

		public string ModuleCode()
		{
			string ICentralPartyApp_ModuleCode = "";
			ICentralPartyApp_ModuleCode = strModuleCode;
			return ICentralPartyApp_ModuleCode;
		}

		public bool ChangePartyID(int lngOriginalPartyID, int lngNewPartyID)
		{
			bool ICentralPartyApp_ChangePartyID = false;
			clsDRWrapper rsSave = new clsDRWrapper();
			string strSQL;
			try
			{
				// On Error GoTo ErrorHandler
				strSQL = "Update cemaster set owner1id = " + FCConvert.ToString(lngNewPartyID) + " where owner1id = " + FCConvert.ToString(lngOriginalPartyID);
				rsSave.Execute(strSQL, "CodeEnforcement");
				strSQL = "Update CEMaster set Owner2ID = " + FCConvert.ToString(lngNewPartyID) + " where owner2id = " + FCConvert.ToString(lngOriginalPartyID);
				rsSave.Execute(strSQL, "CodeEnforcement");
				ICentralPartyApp_ChangePartyID = true;
				return ICentralPartyApp_ChangePartyID;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return ICentralPartyApp_ChangePartyID;
		}
	}
}
