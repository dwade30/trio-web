﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmListChoiceWithIDs.
	/// </summary>
	public partial class frmListChoiceWithIDs : BaseForm
	{
		public frmListChoiceWithIDs()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmListChoiceWithIDs InstancePtr
		{
			get
			{
				return (frmListChoiceWithIDs)Sys.GetInstance(typeof(frmListChoiceWithIDs));
			}
		}

		protected frmListChoiceWithIDs _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 11/03/2004
		// Inputs a list of items to list in a grid with check boxes
		// you specify whether to allow multiple selections
		// and if a selection must be made or not
		// optionally you can specify which items to default to true
		// a return of nullstring indicates no choices
		// a return of -1 means it was cancelled
		// ********************************************************
		string strReturn;
		bool boolForceSelection;
		bool boolAllowMultiple;

		public string Init(ref string strList, string strTitle = "", string strMessage = "", bool boolAllowMultiSelect = false, string strDefaultList = "", bool boolMustSelect = false)
		{
			string Init = "";
			// takes a | delimited list of items to choose from, ids are separated by ;
			// so 1;First Choice|2;Second Choice
			// the default list is | delimited and is a list of items to be set to true
			// lists passed to and from init are not zero based
			// the return value is a list of the selected items
			string[] strAry = null;
			string[] strAry2 = null;
			int x;
			try
			{
				// On Error GoTo ErrorHandler
				strReturn = "";
				boolForceSelection = boolMustSelect;
				boolAllowMultiple = boolAllowMultiSelect;
				SetupGrid();
				strAry = Strings.Split(strList, "|", -1, CompareConstants.vbTextCompare);
				for (x = 0; x <= Information.UBound(strAry, 1); x++)
				{
					Grid.Rows += 1;
					strAry2 = Strings.Split(strAry[x], ";", -1, CompareConstants.vbTextCompare);
					Grid.TextMatrix(Grid.Rows - 1, 1, strAry2[1]);
					Grid.RowData(Grid.Rows - 1, Conversion.Val(strAry2[0]));
					Grid.TextMatrix(Grid.Rows - 1, 0, FCConvert.ToString(false));
				}
				// x
				if (Strings.Trim(strDefaultList) != string.Empty)
				{
					strAry = Strings.Split(strDefaultList, "|", -1, CompareConstants.vbTextCompare);
					for (x = 0; x <= Information.UBound(strAry, 1); x++)
					{
						Grid.TextMatrix(FCConvert.ToInt32(Conversion.Val(strAry[x]) - 1), 0, FCConvert.ToString(true));
					}
					// x
				}
				ResizeGrid();
				this.Text = strTitle;
				//this.HeaderText.Text = strTitle;
				lblMessage.Text = strMessage;
				this.Show(FormShowEnum.Modal);
				Init = strReturn;
				return Init;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In Init", MsgBoxStyle.Critical, "Error");
			}
			return Init;
		}

		private void frmListChoiceWithIDs_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmListChoiceWithIDs_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmListChoiceWithIDs.Icon	= "frmListChoiceWithIDs.frx":0000";
			//frmListChoiceWithIDs.FillStyle	= 0;
			//frmListChoiceWithIDs.ScaleWidth	= 5880;
			//frmListChoiceWithIDs.ScaleHeight	= 4125;
			//frmListChoiceWithIDs.LinkTopic	= "Form2";
			//frmListChoiceWithIDs.LockControls	= true;
			//frmListChoiceWithIDs.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//Grid.BackColor	= "-2147483643";
			//			//Grid.ForeColor	= "-2147483640";
			//Grid.BorderStyle	= 1;
			//Grid.FillStyle	= 0;
			//Grid.Appearance	= 1;
			//Grid.GridLines	= 1;
			//Grid.WordWrap	= 0;
			//Grid.ScrollBars	= 2;
			//Grid.RightToLeft	= 0;
			//Grid._cx	= 8281;
			//Grid._cy	= 4895;
			//Grid._ConvInfo	= 1;
			//Grid.MousePointer	= 0;
			//Grid.BackColorFixed	= -2147483633;
			//			//Grid.ForeColorFixed	= -2147483630;
			//Grid.BackColorSel	= -2147483635;
			//			//Grid.ForeColorSel	= -2147483634;
			//Grid.BackColorBkg	= -2147483636;
			//Grid.BackColorAlternate	= -2147483643;
			//Grid.GridColor	= -2147483633;
			//Grid.GridColorFixed	= -2147483632;
			//Grid.TreeColor	= -2147483632;
			//Grid.FloodColor	= 192;
			//Grid.SheetBorder	= -2147483642;
			//Grid.FocusRect	= 0;
			//Grid.HighLight	= 0;
			//Grid.AllowSelection	= false;
			//Grid.AllowBigSelection	= false;
			//Grid.AllowUserResizing	= 0;
			//Grid.SelectionMode	= 1;
			//Grid.GridLinesFixed	= 2;
			//Grid.GridLineWidth	= 1;
			//Grid.Rows	= 0;
			//Grid.Cols	= 2;
			//Grid.FixedRows	= 0;
			//Grid.FixedCols	= 0;
			//Grid.RowHeightMin	= 0;
			//Grid.RowHeightMax	= 0;
			//Grid.ColWidthMin	= 0;
			//Grid.ColWidthMax	= 0;
			//Grid.ExtendLastCol	= true;
			//Grid.FormatString	= "";
			//Grid.ScrollTrack	= true;
			//Grid.ScrollTips	= false;
			//Grid.MergeCells	= 0;
			//Grid.MergeCompare	= 0;
			//Grid.AutoResize	= true;
			//Grid.AutoSizeMode	= 0;
			//Grid.AutoSearch	= 0;
			//Grid.AutoSearchDelay	= 2;
			//Grid.MultiTotals	= true;
			//Grid.SubtotalPosition	= 1;
			//Grid.OutlineBar	= 0;
			//Grid.OutlineCol	= 0;
			//Grid.Ellipsis	= 0;
			//Grid.ExplorerBar	= 0;
			//Grid.PicturesOver	= false;
			//Grid.PictureType	= 0;
			//Grid.TabBehavior	= 0;
			//Grid.OwnerDraw	= 0;
			//Grid.Editable	= 0;
			//Grid.ShowComboButton	= true;
			//Grid.TextStyle	= 0;
			//Grid.TextStyleFixed	= 0;
			//Grid.OleDragMode	= 0;
			//Grid.OleDropMode	= 0;
			//Grid.ComboSearch	= 3;
			//Grid.AutoSizeMouse	= true;
			//Grid.FrozenRows	= 0;
			//Grid.FrozenCols	= 0;
			//Grid.AllowUserFreezing	= 0;
			//Grid.BackColorFrozen	= 0;
			//			//Grid.ForeColorFrozen	= 0;
			//Grid.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmListChoiceWithIDs.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void SetupGrid()
		{
			Grid.Cols = 2;
			Grid.Rows = 0;
			Grid.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		}

		private void frmListChoiceWithIDs_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			int x;
			if (!boolAllowMultiple)
			{
				if (FCConvert.CBool(Grid.TextMatrix(Grid.Row, 0)))
				{
					for (x = 0; x <= Grid.Rows - 1; x++)
					{
						if (Grid.TextMatrix(x, 0) != string.Empty)
						{
							if (FCConvert.CBool(Grid.TextMatrix(x, 0)) && x != Grid.Row)
							{
								Grid.TextMatrix(x, 0, FCConvert.ToString(false));
							}
						}
					}
					// x
				}
			}
		}

		private void Grid_RowColChange(object sender, EventArgs e)
		{
			if (Grid.Col == 0)
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			int x;
			string strTemp;
			bool boolHasATrue;
			Grid.Row = -1;
			App.DoEvents();
			strTemp = "";
			boolHasATrue = false;
			for (x = 0; x <= Grid.Rows - 1; x++)
			{
				if (FCConvert.CBool(Grid.TextMatrix(x, 0)))
				{
					// strTemp = strTemp & x + 1 & ","
					strTemp += FCConvert.ToString(Grid.RowData(x)) + ",";
					boolHasATrue = true;
				}
			}
			// x
			if (boolForceSelection && !boolHasATrue)
			{
				FCMessageBox.Show("You must make a selection before continuing", MsgBoxStyle.Exclamation, "No Selection Made");
				return;
			}
			if (strTemp != string.Empty)
			{
				strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
			}
			strReturn = strTemp;
			Close();
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(0, FCConvert.ToInt32(0.1 * GridWidth));
			Grid.ColWidth(1, FCConvert.ToInt32(0.85 * GridWidth));
			//FC:FINAL:AM: don't set the height; used anchoring instead
			//if (Grid.Rows > 10)
			//{
			//	Grid.Height = (Grid.RowHeight(0) * 10) + 60;
			//}
			//else if (Grid.Rows > 0)
			//{
			//	Grid.Height = (Grid.RowHeight(0) * Grid.Rows) + 60;
			//}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuSaveContinue_Click(sender, e);
		}
	}
}
