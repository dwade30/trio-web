﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmRECLComment.
	/// </summary>
	partial class frmRECLComment : BaseForm
	{
		public fecherFoundation.FCComboBox cmbCommentType;
		public fecherFoundation.FCTextBox txtExistingComments;
		public fecherFoundation.FCRichTextBox rtbText;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCLabel lblExisting;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRECLComment));
            this.cmbCommentType = new fecherFoundation.FCComboBox();
            this.txtExistingComments = new fecherFoundation.FCTextBox();
            this.rtbText = new fecherFoundation.FCRichTextBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.lblExisting = new fecherFoundation.FCLabel();
            this.lblCommentType = new fecherFoundation.FCLabel();
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdClear = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtbText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 463);
            this.BottomPanel.Size = new System.Drawing.Size(871, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblCommentType);
            this.ClientArea.Controls.Add(this.cmbCommentType);
            this.ClientArea.Controls.Add(this.txtExistingComments);
            this.ClientArea.Controls.Add(this.rtbText);
            this.ClientArea.Controls.Add(this.lblExisting);
            this.ClientArea.Size = new System.Drawing.Size(891, 597);
            this.ClientArea.Controls.SetChildIndex(this.lblExisting, 0);
            this.ClientArea.Controls.SetChildIndex(this.rtbText, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtExistingComments, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbCommentType, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblCommentType, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Size = new System.Drawing.Size(891, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(376, 28);
            this.HeaderText.Text = "Real Estate / Collections Comment";
            // 
            // cmbCommentType
            // 
            this.cmbCommentType.Items.AddRange(new object[] {
            "RE",
            "CL"});
            this.cmbCommentType.Location = new System.Drawing.Point(188, 247);
            this.cmbCommentType.Name = "cmbCommentType";
            this.cmbCommentType.Size = new System.Drawing.Size(160, 40);
            this.cmbCommentType.TabIndex = 0;
            this.cmbCommentType.SelectedIndexChanged += new System.EventHandler(this.optCommentType_Click);
            // 
            // txtExistingComments
            // 
            this.txtExistingComments.AcceptsReturn = true;
            this.txtExistingComments.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtExistingComments.BackColor = System.Drawing.SystemColors.Window;
            this.txtExistingComments.Location = new System.Drawing.Point(30, 345);
            this.txtExistingComments.LockedOriginal = true;
            this.txtExistingComments.Multiline = true;
            this.txtExistingComments.Name = "txtExistingComments";
            this.txtExistingComments.ReadOnly = true;
            this.txtExistingComments.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.txtExistingComments.Size = new System.Drawing.Size(855, 118);
            this.txtExistingComments.TabIndex = 2;
            // 
            // rtbText
            // 
            this.rtbText.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.rtbText.Location = new System.Drawing.Point(30, 30);
            this.rtbText.Name = "rtbText";
            this.rtbText.Size = new System.Drawing.Size(855, 189);
            this.rtbText.TabIndex = 0;
            this.rtbText.TabStop = false;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // lblExisting
            // 
            this.lblExisting.AutoSize = true;
            this.lblExisting.BackColor = System.Drawing.Color.Transparent;
            this.lblExisting.Location = new System.Drawing.Point(30, 310);
            this.lblExisting.Name = "lblExisting";
            this.lblExisting.Size = new System.Drawing.Size(151, 15);
            this.lblExisting.TabIndex = 3;
            this.lblExisting.Text = "PREVIOUS COMMENTS";
            // 
            // lblCommentType
            // 
            this.lblCommentType.AutoSize = true;
            this.lblCommentType.Location = new System.Drawing.Point(30, 261);
            this.lblCommentType.Name = "lblCommentType";
            this.lblCommentType.Size = new System.Drawing.Size(109, 15);
            this.lblCommentType.TabIndex = 4;
            this.lblCommentType.Text = "COMMENT TYPE";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(382, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Save";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.Location = new System.Drawing.Point(807, 28);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(49, 24);
            this.cmdClear.TabIndex = 61;
            this.cmdClear.Text = "Clear";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.Location = new System.Drawing.Point(760, 28);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(41, 24);
            this.cmdPrint.TabIndex = 62;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // frmRECLComment
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(891, 657);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmRECLComment";
            this.Text = "Real Estate / Collections Comment";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmRECLComment_Load);
            this.Resize += new System.EventHandler(this.frmRECLComment_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRECLComment_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtbText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCLabel lblCommentType;
		private FCButton btnProcess;
		public FCButton cmdClear;
		public FCButton cmdPrint;
	}
}
