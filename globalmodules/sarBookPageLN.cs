﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class sarBookPageLN : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/27/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/28/2005              *
		// ********************************************************
		int lngAcct;
		clsDRWrapper rsData = new clsDRWrapper();

		public sarBookPageLN()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarBookPageLN_ReportEnd;
		}

        private void SarBookPageLN_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        public static sarBookPageLN InstancePtr
		{
			get
			{
				return (sarBookPageLN)Sys.GetInstance(typeof(sarBookPageLN));
			}
		}

		protected sarBookPageLN _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			////modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			this.Detail.ColumnCount = 2;
			lngAcct = FCConvert.ToInt32(this.UserData);
			strSQL = "SELECT * FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE Book <> '' AND Account = " + FCConvert.ToString(lngAcct);
			rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (rsData.EndOfFile())
			{
				fldBookPage.Text = "";
				// "No Matches"
				lblHeader.Visible = false;
				GroupHeader1.Height = 0;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strBook = "";
				string strPage = "";
				if (!rsData.EndOfFile())
				{
					// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsData.Get_Fields("Book")) != "")
					{
						// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
						strBook = "B" + FCConvert.ToString(rsData.Get_Fields("Book"));
					}
					else
					{
						strBook = "B        ";
					}
					// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsData.Get_Fields("Page")) != "")
					{
						// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
						strPage = "P" + FCConvert.ToString(rsData.Get_Fields("Page"));
					}
					else
					{
						strPage = "P        ";
					}
					fldBookPage.Text = strBook + " " + strPage;
					fldYear.Text = FormatBillingYear_2(FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")));
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Book Page Report - LN");
			}
		}

		private string FormatBillingYear_2(string strYear)
		{
			return FormatBillingYear(ref strYear);
		}

		private string FormatBillingYear(ref string strYear)
		{
			string FormatBillingYear = "";
			if (Strings.InStr(1, strYear, "-") > 0)
			{
				if (Strings.InStr(1, strYear, "*") > 0)
				{
					FormatBillingYear = Strings.Left(strYear, 4) + FCConvert.ToString(Conversion.Val(Strings.Right(strYear, 2)));
				}
				else
				{
					FormatBillingYear = Strings.Left(strYear, 4) + Strings.Right(strYear, 1);
				}
			}
			else
			{
				if (Strings.InStr(1, strYear, "*") > 0)
				{
					FormatBillingYear = Strings.Left(strYear, 4) + "-" + FCConvert.ToString(Conversion.Val(Strings.Right(strYear, 2)));
				}
				else
				{
					FormatBillingYear = Strings.Left(strYear, 4) + "-" + Strings.Right(strYear, 1);
				}
			}
			return FormatBillingYear;
		}

		private void sarBookPageLN_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarBookPageLN.Caption	= "Account Detail";
			//sarBookPageLN.Icon	= "sarBookPageLN.dsx":0000";
			//sarBookPageLN.Left	= 0;
			//sarBookPageLN.Top	= 0;
			//sarBookPageLN.Width	= 11880;
			//sarBookPageLN.Height	= 8595;
			//sarBookPageLN.StartUpPosition	= 3;
			//sarBookPageLN.SectionData	= "sarBookPageLN.dsx":058A;
			//End Unmaped Properties
		}
	}
}
