﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;
#if TWCL0000
using TWCL0000;
using TWCL0000.Properties;

#elif TWBD0000
using TWBD0000;


#elif TWCR0000
using TWCR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmRECLStatus.
	/// </summary>
	public partial class frmRECLStatus : BaseForm
	{
		public frmRECLStatus()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label1 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.cmdDisc = new System.Collections.Generic.List<fecherFoundation.FCButton>();
			this.cmdDisc.AddControlArrayElement(this.cmdDisc_0, 0);
			this.cmdDisc.AddControlArrayElement(this.cmdDisc_1, 1);
			this.cmdDisc.AddControlArrayElement(this.cmdDisc_2, 2);
			this.cmdDisc.AddControlArrayElement(this.cmdDisc_3, 3);
			this.cmdWOProcess = new System.Collections.Generic.List<fecherFoundation.FCButton>();
			this.cmdWOProcess.AddControlArrayElement(this.cmdWOProcess_0, 0);
			this.cmdWOProcess.AddControlArrayElement(this.cmdWOProcess_1, 1);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:AM: moved code from Load
			lngColYear = 1;
			lngColDate = 2;
			lngColReference = 3;
			lngColPeriod = 4;
			lngColCode = 5;
			lngColPrincipal = 6;
			lngColInterest = 7;
			lngColCosts = 8;
			lngColTotalAmount = 9;
			lngColGridCode = 10;
			lngColPaymentID = 11;
			lngColCHGINTNumber = 12;
			lngColPerDiem = 13;
			lngColComment = 14;
			lngColPending = 15;
			boolOngoingReversal = false;
			boolNoCurrentInt = false;
			lngNoCurrentIntYear = 0;
			boolAcctDetailShown = false;
			modStatusPayments.Statics.EffectiveDate = DateTime.Now;
			//FC:FINAL:AM:#i173 - set the column width
			txtAcctNumber.ColWidth(0, txtAcctNumber.WidthOriginal);
			clsAcct.GRID7Light = txtAcctNumber;
			clsAcct.DefaultAccountType = "G";
			clsAcct.ToolTipText = "Hit Shift-F2 to get a valid accounts list.";
			// set the default databases
			rsCL.DefaultDB = modExtraModules.strCLDatabase;
			rsLR.DefaultDB = modExtraModules.strCLDatabase;
			// set the strings to show
			strStatusString = "Go To Status";
			strPaymentString = "Go To Payments/Adj";
			strSupplemntalBillString = "Original";
			// reset all variables and arrays
			Reset_Sum();
			modStatusPayments.Statics.boolMultiLine = false;
			modStatusPayments.Statics.boolUseOldCHGINTDate = false;
			FCUtils.EraseSafe(modStatusPayments.Statics.PaymentArray);
			FCUtils.EraseSafe(modStatusPayments.Statics.dblCurrentInt);
			FCUtils.EraseSafe(modStatusPayments.Statics.dateOldDate);
			mnuFilePrintBPReport.Visible = modStatusPayments.Statics.boolRE;
			//FC:FINAL:AM:#4 - decrease the height of rows
			vsRateInfo.RowHeight(-1, 400);
			vsPeriod.RowHeight(-1, 550);
			vsPeriod.CellBorderStyle = DataGridViewCellBorderStyle.None;
            txtInterest.AllowOnlyNumericInput(true);
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRECLStatus InstancePtr
		{
			get
			{
				return (frmRECLStatus)Sys.GetInstance(typeof(frmRECLStatus));
			}
		}

		protected frmRECLStatus _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/07/2006              *
		// ********************************************************
		clsDRWrapper rsCL = new clsDRWrapper();
		// Collections Recordset
		clsDRWrapper rsLR = new clsDRWrapper();
		// Lien Record Recordset
		int CurrentAccount;
		// Holds the Account Number of the Current Account
		string CurrentOwner = "";
		string strSQL = "";
		// Temporary Holding Variable for SQL Statements
		string DataName = "";
		int holdyear;
		public bool FormLoaded;
		// true if form is loaded so that it can be activated many times without resetting the grid
		public bool FormReset;
		// if the effective date gets changed, then this gets set to true to allow the form to recreate the grid
		decimal curCosts;
		// VBto upgrade warning: curCurrentInterest As double	OnWrite(short, double)	OnReadFCConvert.ToDouble(
		decimal curCurrentInterest;
		decimal curPreLienInterest;
		decimal curPrincipalDue;
		// VBto upgrade warning: bcode As FixedString	OnWrite(string)
		string bcode = "";
		// Billing Code
		char[] bl = new char[1];
		int intNumberOfPeriods;
		// VBto upgrade warning: OriginalBillDate As DateTime	OnWrite(DateTime, string)
		DateTime OriginalBillDate;
		int CurRow;
		// This holds the current line number of the flexgrid
		int MaxRows;
		// Holds the max rows in the grid
		bool CollapseFlag;
		// Tells the collapse routine to fire or not
		int ParentLine;
		// Holds the line number of the last header line (used in swapping and holding totals)
		// VBto upgrade warning: sumPrin As double	OnWrite(double, short)
		double sumPrin;
		// these sums will keep track of the account totals for the bottom subtotal line
		// VBto upgrade warning: sumInt As double	OnWrite(double, short)
		double sumInt;
		// VBto upgrade warning: sumCost As double	OnWrite(double, short)
		double sumCost;
		// VBto upgrade warning: sumTotal As double	OnWrite(double, short)
		double sumTotal;
		bool boolPayment;
		// true if this is the payment screen false if status
		bool boolDisableInsert;
		// kk03132015 trocrs-36  Add option to disable automatically filling in pmt amount
		bool boolRefKeystroke;
		// this is true if a keystroke is used to get into cmbPeriod
		bool boolNegPayments;
		string strStatusString = "";
		// this just holds the string for the menu options
		string strPaymentString = "";
		// this just holds the string for the menu options
		int lngGroupNumber;
		int lngGroupID;
		string strGroupComment = "";
		public bool boolUnloadOK;
		// this is to check to see if the user really wants to leave the screen with pending activity
		public double dblTotalPerDiem;
		// this is the sum of each year per diem to be shown with the rate info
		bool boolPop;
		// this is true when there is a priority comment on this account
		string strNoteText = "";
		// this is where the note text will be stored
		int intNotePriority;
		string strMailingAddress1 = "";
		// this will store the mailing address from RE
		string strMailingAddress2 = "";
		string strSupplemntalBillString;
		// this holds the string that I use to ID the parent row from others during the info swap
		string strCurrentBookPage = "";
		public bool boolNoCurrentInt;
		// this will be set by PaymentRecords if there is a tax club so that current interest is no calculated
		public int lngNoCurrentIntYear;
		public bool boolCollapsedInputBoxes;
		public string strSearchSQL = "";
		// if this string is not "" then this will have an sql statement that has the list of accounts searched for
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private clsDRWrapper rsSearchString = new clsDRWrapper();
		private clsDRWrapper rsSearchString_AutoInitialized;

		private clsDRWrapper rsSearchString
		{
			get
			{
				if (rsSearchString_AutoInitialized == null)
				{
					rsSearchString_AutoInitialized = new clsDRWrapper();
				}
				return rsSearchString_AutoInitialized;
			}
			set
			{
				rsSearchString_AutoInitialized = value;
			}
		}

		double dblAssocAccountTotal;
		// this will be filled with the amount that the associated account has due for all years
		public string strPassSortOrder = "";
		// this will pass the sort order for the list of accounts
		string[] strPeriodToolTip = new string[5 + 1];
		// this will hold the period grid's tool tips
		public bool boolReprintReceipt;
		// this is set to true when choosing an old payment to reprint the receipt for
		public bool boolDoNotResetRTD;
		// If this is true then it will not reset the RTD to the EID
		public bool boolChangingCode;
		public int lngCLResCode;
		public bool boolLoading;
		public bool boolOngoingReversal;
		// if this is true then there is already a reversal on this screen and should not be treated as a true reversal
		clsGridAccount clsAcct = new clsGridAccount();
		bool boolDeleted;
		bool boolDoNotContinueSave;
		bool boolAcctDetailShown;
		// VBto upgrade warning: dblLastTCInt As double	OnWrite(short, double)
		double dblLastTCInt;
		bool blnDiscountCredit;
		// If True, Discount amount results in credit(pre-payment) on account
		int lngNewBillKey;
		// MAL@20071005
		bool blnIsCollapsed;
		// MAL@20071115
		bool blnInProgress;
		// MAL@20071115
		int lngCurrentGridRow;
		// MAL@20071115
		double dblPrin;
		double dblInt;
		double dblPLI;
		double dblCosts;
		clsDRWrapper rsDisc = new clsDRWrapper();
		int intYear;
		int LRN;
		double dblOrigPrin;
		double dblPrinPaid;
		// VBto upgrade warning: dblTemp As double	OnWriteFCConvert.ToDouble(
		double[] dblTemp = new double[4 + 1];
		bool blnHasCurrInt;
		double dblCurrInt;
		double dblTempTotal;
		int lngAccount;
		int intCharCount;
		bool boolTaxAcquired;
		// kgk 06-10-11 trocr-641
		public int lngColYear;
		public int lngColDate;
		public int lngColReference;
		public int lngColPeriod;
		public int lngColCode;
		public int lngColPrincipal;
		public int lngColInterest;
		public int lngColCosts;
		public int lngColTotalAmount;
		public int lngColGridCode;
		public int lngColPaymentID;
		public int lngColCHGINTNumber;
		public int lngColPerDiem;
		public int lngColComment;
		public int lngColPending;
		public string strPrinAcct = "";
		public string strIntAcct = "";
		public string strPLIAcct = "";
		public string strCostAcct = "";
		public bool blnIsWriteOff;
		public bool blnIsRefund;
		// MAL@2008013
		public int OwnerPartyID;
		// CR Central Party Interface
		//private void cmbCode_SelectedIndexChanged(object sender, System.EventArgs e)
		//{
		//	bool boolLien = false;
		//	string strTemp;
		//	bool boolAffectTC = false;
		//	int lngCT;
		//	blnIsWriteOff = false;
		//	// Reset Value
		//	for (lngCT = 0; lngCT <= Information.UBound(modStatusPayments.Statics.gboolCLNoIntFromTaxClub, 1) - 1; lngCT++)
		//	{
		//		if (modStatusPayments.Statics.gboolCLNoIntFromTaxClub[lngCT])
		//		{
		//			boolAffectTC = true;
		//		}
		//	}
		//	//FC:FINAL:DDU #i114 prevent comboboxes to select indexes when they are empty
		//	if (cmbCode.Items.Count != 0)
		//	{
		//		strTemp = Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1);
		//	}
		//	else
		//	{
		//		strTemp = "";
		//	}
		//	if (!boolChangingCode)
		//	{
		//		if (boolAffectTC && strTemp != "U" && strTemp != "A" && !boolLoading)
		//		{
		//			DialogResult messageBoxResult = FCMessageBox.Show("Some years are not being affected by interest because of a Tax Club payment, changing the code could cause interest to be charged to this account.  Are you sure that you would like to change the code?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Tax Club");
		//			if (messageBoxResult == DialogResult.Yes)
		//			{
		//			}
		//			else if (messageBoxResult == DialogResult.Cancel)
		//			{
		//				for (lngCT = 0; lngCT <= cmbCode.Items.Count - 1; lngCT++)
		//				{
		//					// Doevents
		//					if (Strings.Left(cmbCode.Items[lngCT].ToString(), 1) == "U")
		//					{
		//						cmbCode.SelectedIndex = lngCT;
		//						break;
		//					}
		//				}
		//				return;
		//			}
		//		}
		//	}
		//	else
		//	{
		//		boolChangingCode = false;
		//	}
		//	if (modStatusPayments.YearCodeValidate(boolLien))
		//	{
		//		if ((strTemp == "A") || (strTemp == "R"))
		//		{
		//			// Abatement
		//			// set the Affect Cash and the Affect Cash Drawer to False and get the Abatement Account Number to put into the Account Box
		//			txtCash.Text = "N";
		//			txtCD.Text = "N";
		//			if (modStatusPayments.Statics.boolRE)
		//			{
		//				// If this is a lien then use the lien abatement account
		//				if (!boolLien)
		//				{
		//					txtAcctNumber.TextMatrix(0, 0, GetCLAccount("A", 90, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(cmbYear.Items[cmbYear.SelectedIndex].ToString(), 3, 2))))));
		//				}
		//				else
		//				{
		//					txtAcctNumber.TextMatrix(0, 0, GetCLAccount("A", 91, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(cmbYear.Items[cmbYear.SelectedIndex].ToString(), 3, 2))))));
		//				}
		//			}
		//			else
		//			{
		//				txtAcctNumber.TextMatrix(0, 0, GetCLAccount("A", 92, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(cmbYear.Items[cmbYear.SelectedIndex].ToString(), 3, 2))))));
		//			}
		//			// set the boxes
		//			InputBoxAdjustment_2(false);
		//		}
		//		else if (strTemp == "S")
		//		{
		//			// Supplemental
		//			// set the Affect Cash and the Affect Cash Drawer to False and get the Supplemental Account Number to put into the Account Box
		//			cmbPeriod.SelectedIndex = 4;
		//			txtCash.Text = "N";
		//			txtCD.Text = "N";
		//			txtAcctNumber.TextMatrix(0, 0, GetCLAccount("S", 90));
		//			// set the boxes
		//			InputBoxAdjustment_2(true);
		//		}
		//		else if (strTemp == "D")
		//		{
		//			// Discount
		//			if (modStatusPayments.Statics.boolRE)
		//			{
		//				if (Strings.Left(cmbPeriod.Text, 1) == "A")
		//				{
		//					// set the Affect Cash and the Affect Cash Drawer to False and get the Discount Account Number to put into the Account Box
		//					ShowDiscountFrame(true);
		//					// txtCash.Text = "N"
		//					// txtCD.Text = "N"
		//					txtAcctNumber.TextMatrix(0, 0, GetCLAccount("D", 90));
		//				}
		//				else
		//				{
		//					txtCash.Text = "N";
		//					txtCD.Text = "N";
		//					txtAcctNumber.TextMatrix(0, 0, GetCLAccount("D", 90));
		//				}
		//			}
		//			else
		//			{
		//				if (Strings.Left(cmbPeriod.Text, 1) == "A")
		//				{
		//					// set the Affect Cash and the Affect Cash Drawer to False and get the Discount Account Number to put into the Account Box
		//					ShowDiscountFrame(true);
		//					// txtCash.Text = "N"
		//					// txtCD.Text = "N"
		//					txtAcctNumber.TextMatrix(0, 0, GetCLAccount("D", 92));
		//				}
		//				else
		//				{
		//					txtCash.Text = "N";
		//					txtCD.Text = "N";
		//					txtAcctNumber.TextMatrix(0, 0, GetCLAccount("D", 92));
		//				}
		//			}
		//			// set the boxes
		//			InputBoxAdjustment_2(true);
		//		}
		//		else if (strTemp == "P")
		//		{
		//			// Payment
		//			// If Left$(cmbPeriod.List(cmbPeriod.ListIndex), 1) <> "A" Then
		//			cmbPeriod.SelectedIndex = 4;
		//			txtCash.Text = "Y";
		//			txtCD.Text = "Y";
		//			txtAcctNumber.TextMatrix(0, 0, "");
		//			txtAcctNumber.Enabled = false;
		//			// end if
		//			// set the boxes
		//			InputBoxAdjustment_2(true);
		//		}
		//		else if (strTemp == "U")
		//		{
		//			// Tax Club
		//			txtCash.Text = "Y";
		//			txtCD.Text = "Y";
		//			txtAcctNumber.TextMatrix(0, 0, "");
		//			txtAcctNumber.Enabled = false;
		//			// set the boxes
		//			InputBoxAdjustment_2(false);
		//		}
		//		else if (strTemp == "Y")
		//		{
		//			// Prepayment
		//			frmRECLStatus.InstancePtr.boolChangingCode = true;
		//			if (Strings.Right(cmbYear.Items[cmbYear.SelectedIndex].ToString(), 1) != "*")
		//			{
		//				// MAL@20071115
		//				// cmbCode.ListIndex = 3
		//				cmbPeriod.SelectedIndex = 3;
		//			}
		//			else
		//			{
		//				cmbPeriod.SelectedIndex = 4;
		//				txtCash.Text = "Y";
		//				txtCD.Text = "Y";
		//				txtAcctNumber.TextMatrix(0, 0, "");
		//				txtAcctNumber.Enabled = false;
		//			}
		//			frmRECLStatus.InstancePtr.boolChangingCode = false;
		//			// set the boxes
		//			InputBoxAdjustment_2(true);
		//		}
		//		else if (strTemp == "C")
		//		{
		//			// set the boxes
		//			txtCash.Text = "Y";
		//			txtCD.Text = "Y";
		//			txtAcctNumber.TextMatrix(0, 0, "");
		//			txtAcctNumber.Enabled = false;
		//			cmbPeriod.SelectedIndex = 4;
		//			InputBoxAdjustment_2(false);
		//			// MAL@20071010: Added support for write-off payment type
		//			// MAL@20071105: Change the wording per support's request
		//		}
		//		else if (strTemp == "N")
		//		{
		//			blnIsWriteOff = true;
		//			if (!WriteoffPaymentExists())
		//			{
		//				if (modStatusPayments.Statics.boolRE)
		//				{
		//					if (Strings.Left(cmbPeriod.Text, 1) == "A")
		//					{
		//						ShowWriteoffFrame(true);
		//						if (!boolLien)
		//						{
		//							if (!boolTaxAcquired)
		//							{
		//								// kgk 06-10-11 trocl-641  Use tax acquired accounts
		//								GetAllCLAccounts(90, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//							}
		//							else
		//							{
		//								GetAllCLAccounts(890, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//							}
		//						}
		//						else
		//						{
		//							if (!boolTaxAcquired)
		//							{
		//								// kgk 06-10-11 trocl-641  Use tax acquired accounts
		//								GetAllCLAccounts(91, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//							}
		//							else
		//							{
		//								GetAllCLAccounts(891, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//							}
		//						}
		//						txtCash.Text = "N";
		//						txtCD.Text = "N";
		//					}
		//					else
		//					{
		//						txtCash.Text = "N";
		//						txtCD.Text = "N";
		//						if (!boolLien)
		//						{
		//							if (!boolTaxAcquired)
		//							{
		//								// kgk 06-10-11 trocl-641  Use tax acquired accounts
		//								GetAllCLAccounts(90, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//							}
		//							else
		//							{
		//								GetAllCLAccounts(890, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//							}
		//						}
		//						else
		//						{
		//							if (!boolTaxAcquired)
		//							{
		//								// kgk 06-10-11 trocl-641  Use tax acquired accounts
		//								GetAllCLAccounts(91, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//							}
		//							else
		//							{
		//								GetAllCLAccounts(891, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//							}
		//						}
		//					}
		//				}
		//				else
		//				{
		//					if (Strings.Left(cmbPeriod.Text, 1) == "A")
		//					{
		//						ShowWriteoffFrame(true);
		//						GetAllCLAccounts(92, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//						txtCash.Text = "N";
		//						txtCD.Text = "N";
		//					}
		//					else
		//					{
		//						txtCash.Text = "N";
		//						txtCD.Text = "N";
		//						GetAllCLAccounts(92, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//					}
		//				}
		//			}
		//			else
		//			{
		//				FCMessageBox.Show("Payments already exist from this year.", MsgBoxStyle.Exclamation | MsgBoxStyle.OkOnly, "Payment Exists");
		//				// MAL@20071115
		//				// If gboolCR Then
		//				// cmbCode.ListIndex = 4
		//				cmbCode.SelectedIndex = modStatusPayments.GetPaymentCodeListIndex("P");
		//				// Else
		//				// cmbCode.ListIndex = 3
		//				// End If
		//			}
		//			// MAL@20080130: New refund code
		//			// Call Reference: 115825
		//		}
		//		else if (strTemp == "F")
		//		{
		//			blnIsRefund = true;
		//			if (ValidateRefundPayment())
		//			{
		//				// Everything is okay, continue
		//				if (modStatusPayments.Statics.boolRE)
		//				{
		//					if (Strings.Left(cmbPeriod.Text, 1) == "A")
		//					{
		//						if (!boolLien)
		//						{
		//							GetAllCLAccounts(90, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//						}
		//						else
		//						{
		//							GetAllCLAccounts(91, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//						}
		//						txtCash.Text = "N";
		//						txtCD.Text = "N";
		//					}
		//					else
		//					{
		//						txtCash.Text = "N";
		//						txtCD.Text = "N";
		//						if (!boolLien)
		//						{
		//							GetAllCLAccounts(90, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//						}
		//						else
		//						{
		//							GetAllCLAccounts(91, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//						}
		//					}
		//				}
		//				else
		//				{
		//					if (Strings.Left(cmbPeriod.Text, 1) == "A")
		//					{
		//						GetAllCLAccounts(92, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//						txtCash.Text = "N";
		//						txtCD.Text = "N";
		//					}
		//					else
		//					{
		//						txtCash.Text = "N";
		//						txtCD.Text = "N";
		//						GetAllCLAccounts(92, FCConvert.ToInt16(Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//					}
		//				}
		//				txtInterest.Text = FCConvert.ToString(dblPrin);
		//				txtAcctNumber.Text = strPrinAcct;
		//				txtReference.Text = "Refund";
		//			}
		//			else
		//			{
		//				blnIsRefund = false;
		//			}
		//		}
		//		else
		//		{
		//			cmbPeriod.SelectedIndex = 4;
		//			modStatusPayments.Statics.boolMultiLine = false;
		//			modStatusPayments.Statics.boolUseOldCHGINTDate = false;
		//			txtCash.Text = "Y";
		//			txtCD.Text = "Y";
		//			txtAcctNumber.TextMatrix(0, 0, "");
		//			txtAcctNumber.Enabled = false;
		//			// set the boxes
		//			InputBoxAdjustment_2(true);
		//		}
		//	}
		//	else
		//	{
		//		frmRECLStatus.InstancePtr.boolChangingCode = true;
		//		// MAL@20071113: Changed to accomodate the new "N" option
		//		cmbCode.SelectedIndex = modStatusPayments.GetPaymentCodeListIndex("P");
		//		// If gboolCR Then
		//		// cmbCode.ListIndex = 4
		//		// Else
		//		// cmbCode.ListIndex = 3
		//		// End If
		//		frmRECLStatus.InstancePtr.boolChangingCode = false;
		//	}
		//	CheckCD();
		//	CheckCash();
		//}

		private void cmbCode_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbCode.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cmbCode_Enter(object sender, System.EventArgs e)
		{
			boolRefKeystroke = true;
		}

		private void cmbCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo ERROR_HANDLER
				// left = 37
				// right = 39
				if (KeyCode == (Keys)37)
				{
					boolRefKeystroke = true;
					cmbPeriod.Focus();
					KeyCode = 0;
				}
				else if (KeyCode == (Keys)39)
				{
					if (txtCD.Visible && txtCD.Enabled)
					{
						txtCD.Focus();
					}
					KeyCode = 0;
				}
				else if (KeyCode == Keys.Space)
				{
					if (modAPIsConst.SendMessageByNum(cmbCode.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
					{
						modAPIsConst.SendMessageByNum(cmbCode.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
						KeyCode = 0;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In cmbCode KeyDown");
			}
		}

		private void cmbPeriod_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbCode.SelectedIndex != -1 && Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A")
			{
				// let this pass
			}
			else
			{
				//FC:FINAL:DDU #i114 prevent comboboxes to select indexes when they are empty
				if (cmbPeriod.Items.Count != 0)
				{
					cmbPeriod.SelectedIndex = 4;
				}
			}
		}

		private void cmbPeriod_DropDown(object sender, System.EventArgs e)
		{
			boolRefKeystroke = true;
		}

		private void cmbPeriod_Enter(object sender, System.EventArgs e)
		{
			if (!boolRefKeystroke)
			{
				// if a ID was not pressed to get here then send it to the txtInterest field
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
				else
				{
					txtInterest.Focus();
				}
			}
		}

		private void cmbPeriod_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// left = 37
			// right = 39
			if (FCConvert.ToInt32(KeyCode) == 37)
			{
				txtReference.Focus();
				KeyCode = 0;
			}
			else if (FCConvert.ToInt32(KeyCode) == 39)
			{
				cmbCode.Focus();
				KeyCode = 0;
			}
			else if (KeyCode == Keys.Space)
			{
				if (modAPIsConst.SendMessageByNum(cmbPeriod.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
				{
					modAPIsConst.SendMessageByNum(cmbPeriod.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
					KeyCode = 0;
				}
			}
		}

		private void cmbPeriod_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A")
			{
				// let this pass
			}
			else
			{
				cmbPeriod.SelectedIndex = 4;
			}
		}

		//private void cmbYear_SelectedIndexChanged(object sender, System.EventArgs e)
		//{
		//	// this sub will show the totals line above the payment boxes with period information
		//	clsDRWrapper rsYear = new clsDRWrapper();
		//	clsDRWrapper rsRate = new clsDRWrapper();
		//	int TotLine = 0;
		//	string strSQL = "";
		//	int intYear = 0;
		//	int LRN/*unused?*/;
		//	// VBto upgrade warning: Tax1 As double	OnWrite(double, double, short)
		//	decimal Tax1;
		//	// VBto upgrade warning: Tax2 As double	OnWrite(double, double, short)
		//	decimal Tax2;
		//	// VBto upgrade warning: Tax3 As double	OnWrite(double, double, short)
		//	decimal Tax3;
		//	// VBto upgrade warning: Tax4 As double	OnWrite(double, double, short)
		//	decimal Tax4;
		//	bool boolLien/*unused?*/;
		//	bool blnTC;
		//	//PPJ:FINAL:MHO:IIT #64 - return if SelectedIndex = -1 to avoid exceptions as no item is selected
		//	if (cmbYear.SelectedIndex == -1)
		//	{
		//		return;
		//	}
		//	if (cmbYear.Items[cmbYear.SelectedIndex].ToString() == "Auto")
		//	{
		//		// gboolCurrentBillInTaxClub = False
		//		strPeriodToolTip[1] = "";
		//		strPeriodToolTip[2] = "";
		//		strPeriodToolTip[3] = "";
		//		strPeriodToolTip[4] = "";
		//		if (modStatusPayments.YearCodeValidate())
		//		{
		//			frmRECLStatus.InstancePtr.boolChangingCode = true;
		//			// MAL@20071106: Changed ListIndex to accomodate new code
		//			cmbCode.SelectedIndex = modStatusPayments.GetPaymentCodeListIndex("P");
		//			frmRECLStatus.InstancePtr.boolChangingCode = false;
		//			vsPeriod.TextMatrix(0, 0, "Total Due:");
		//			vsPeriod.TextMatrix(0, 1, Strings.Format(GRID.TextMatrix(GRID.Rows - 1, lngColTotalAmount), "#,##0.00"));
		//			vsPeriod.TextMatrix(0, 2, "");
		//			vsPeriod.TextMatrix(0, 3, "");
		//			vsPeriod.TextMatrix(0, 4, "");
		//		}
		//		else
		//		{
		//			if (GRID.Row == GRID.Rows - 1)
		//			{
		//				// this is the last line, so leave the "Auto" in the combo
		//			}
		//			else
		//			{
		//				// cmbYear.ListIndex = -1
		//				frmRECLStatus.InstancePtr.boolChangingCode = true;
		//				// MAL@20071115
		//				cmbCode.SelectedIndex = modStatusPayments.GetPaymentCodeListIndex("P");
		//				// cmbCode.ListIndex = 3   'Change this to a payment
		//				frmRECLStatus.InstancePtr.boolChangingCode = false;
		//			}
		//		}
		//	}
		//	else
		//	{
		//		intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(cmbYear.Items[cmbYear.SelectedIndex].ToString()))));
		//		blnTC = CheckTaxClubPayments(intYear);
		//		TotLine = (NextSpacerLine(GRID.FindRow(modExtraModules.FormatYear(intYear.ToString()), 1, lngColYear)) - 1);
		//		if (TotLine > 0)
		//		{
		//			if (modStatusPayments.Statics.boolRE)
		//			{
		//				strSQL = "SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'RE'";
		//			}
		//			else
		//			{
		//				strSQL = "SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'PP'";
		//			}
		//			rsYear.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
		//			if (rsYear.EndOfFile() != true)
		//			{
		//				rsRate.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsYear.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
		//				if (!rsRate.EndOfFile())
		//				{
		//					if (rsRate.Get_Fields_DateTime("DueDate1") is DateTime)
		//					{
		//						strPeriodToolTip[1] = Strings.Format(rsRate.Get_Fields_DateTime("DueDate1"), "MM/dd/yyyy");
		//					}
		//					else
		//					{
		//						strPeriodToolTip[1] = "";
		//					}
		//					if (rsRate.Get_Fields_DateTime("DueDate2") is DateTime)
		//					{
		//						strPeriodToolTip[2] = Strings.Format(rsRate.Get_Fields_DateTime("DueDate2"), "MM/dd/yyyy");
		//					}
		//					else
		//					{
		//						strPeriodToolTip[2] = "";
		//					}
		//					if (rsRate.Get_Fields_DateTime("DueDate3") is DateTime)
		//					{
		//						strPeriodToolTip[3] = Strings.Format(rsRate.Get_Fields_DateTime("DueDate3"), "MM/dd/yyyy");
		//					}
		//					else
		//					{
		//						strPeriodToolTip[3] = "";
		//					}
		//					if (rsRate.Get_Fields_DateTime("DueDate4") is DateTime)
		//					{
		//						strPeriodToolTip[4] = Strings.Format(rsRate.Get_Fields_DateTime("DueDate4"), "MM/dd/yyyy");
		//					}
		//					else
		//					{
		//						strPeriodToolTip[4] = "";
		//					}
		//				}
		//				if (FCConvert.ToInt32(rsYear.Get_Fields_Int32("LienRecordNumber")) != 0)
		//				{
		//					// it is a lien
		//					if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A")
		//					{
		//						// if this is an abatement then make sure the correct abatement account is used
		//						txtAcctNumber.TextMatrix(0, 0, GetCLAccount("A", 91, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(cmbYear.Items[cmbYear.SelectedIndex].ToString(), 3, 2))))));
		//					}
		//					Tax1 = FCConvert.ToDecimal(GRID.TextMatrix(TotLine, lngColTotalAmount));
		//					vsPeriod.TextMatrix(0, 0, "Period Due:");
		//					vsPeriod.TextMatrix(0, 1, "1) " + Strings.Format(Tax1, "#,##0.00"));
		//					vsPeriod.TextMatrix(0, 2, "2) 0.00");
		//					vsPeriod.TextMatrix(0, 3, "3) 0.00");
		//					vsPeriod.TextMatrix(0, 4, "4) 0.00");
		//				}
		//				else
		//				{
		//					// it is NOT a lien
		//					if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A")
		//					{
		//						// if this is an abatement then make sure the correct abatement account is used
		//						if (modStatusPayments.Statics.boolRE)
		//						{
		//							txtAcctNumber.TextMatrix(0, 0, GetCLAccount("A", 90, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(cmbYear.Items[cmbYear.SelectedIndex].ToString(), 3, 2))))));
		//						}
		//						else
		//						{
		//							txtAcctNumber.TextMatrix(0, 0, GetCLAccount("A", 92, FCConvert.ToInt16(FCConvert.ToString(Conversion.Val(Strings.Mid(cmbYear.Items[cmbYear.SelectedIndex].ToString(), 3, 2))))));
		//						}
		//					}
		//					// MAL@20080808: Changed to see if current year is in Tax Club (support for multiple TC years)
		//					// Tracker Reference: 14033
		//					if (modStatusPayments.Statics.boolRE)
		//					{
		//						if (IsInTaxClub(FCConvert.ToInt32(rsYear.Get_Fields_Int32("BillingYear")), StaticSettings.TaxCollectionValues.LastAccountRE))
		//						{
		//							// If rsYear.Get_Fields("BillingYear") = lngNoCurrentIntYear Then
		//							// MAL@20071115
		//							// cmbCode.ListIndex = 5
		//							cmbCode.SelectedIndex = modStatusPayments.GetPaymentCodeListIndex("U");
		//						}
		//						else if (cmbCode.SelectedIndex == 5)
		//						{
		//						}
		//						else if (cmbCode.SelectedIndex == modStatusPayments.GetPaymentCodeListIndex("U"))
		//						{
		//							// cmbCode.ListIndex = 3
		//							cmbCode.SelectedIndex = modStatusPayments.GetPaymentCodeListIndex("P");
		//						}
		//					}
		//					else
		//					{
		//						if (IsInTaxClub(FCConvert.ToInt32(rsYear.Get_Fields_Int32("BillingYear")), StaticSettings.TaxCollectionValues.LastAccountPP))
		//						{
		//							// MAL@20071115
		//							// cmbCode.ListIndex = 5
		//							cmbCode.SelectedIndex = modStatusPayments.GetPaymentCodeListIndex("U");
		//						}
		//						else if (cmbCode.SelectedIndex == 5)
		//						{
		//						}
		//						else if (cmbCode.SelectedIndex == modStatusPayments.GetPaymentCodeListIndex("U"))
		//						{
		//							// cmbCode.ListIndex = 3
		//							cmbCode.SelectedIndex = modStatusPayments.GetPaymentCodeListIndex("P");
		//						}
		//					}
		//					// 
		//					// 
		//					Tax1 = FCConvert.ToDecimal(Conversion.Val(rsYear.Get_Fields_Decimal("TaxDue1")) - (Conversion.Val(rsYear.Get_Fields_Decimal("InterestCharged")) - modStatusPayments.Statics.dblCurrentInt[(intYear) - modExtraModules.DEFAULTSUBTRACTIONVALUE]) + Conversion.Val(rsYear.Get_Fields_Decimal("DemandFees")));
		//					Tax1 -= FCConvert.ToDecimal(modStatusPayments.Statics.AbatePaymentsArray[rsYear.Get_Fields_Int32("BillingYear") % 1000].Per1);
		//					Tax1 -= FCConvert.ToDecimal((Conversion.Val(rsYear.Get_Fields_Decimal("PrincipalPaid")) + Conversion.Val(rsYear.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsYear.Get_Fields_Decimal("DemandFeesPaid"))));
		//					Tax1 += FCConvert.ToDecimal((modStatusPayments.Statics.AbatePaymentsArray[rsYear.Get_Fields_Int32("BillingYear") % 1000].Per1 + modStatusPayments.Statics.AbatePaymentsArray[rsYear.Get_Fields_Int32("BillingYear") % 1000].Per2 + modStatusPayments.Statics.AbatePaymentsArray[rsYear.Get_Fields_Int32("BillingYear") % 1000].Per3 + modStatusPayments.Statics.AbatePaymentsArray[rsYear.Get_Fields_Int32("BillingYear") % 1000].Per4));
		//					Tax2 = FCConvert.ToDecimal(Conversion.Val(rsYear.Get_Fields_Decimal("TaxDue2")) - modStatusPayments.Statics.AbatePaymentsArray[rsYear.Get_Fields_Int32("BillingYear") % 1000].Per2);
		//					Tax3 = FCConvert.ToDecimal(Conversion.Val(rsYear.Get_Fields_Decimal("TaxDue3")) - modStatusPayments.Statics.AbatePaymentsArray[rsYear.Get_Fields_Int32("BillingYear") % 1000].Per3);
		//					Tax4 = FCConvert.ToDecimal(Conversion.Val(rsYear.Get_Fields_Decimal("TaxDue4")) - modStatusPayments.Statics.AbatePaymentsArray[rsYear.Get_Fields_Int32("BillingYear") % 1000].Per4);
		//					if (Tax1 < 0)
		//					{
		//						// this needs to change, needs to set the tax values earlier
		//						Tax2 += Tax1;
		//						Tax1 = 0;
		//						if (Tax2 < 0)
		//						{
		//							Tax3 += Tax2;
		//							Tax2 = 0;
		//							if (Tax3 < 0)
		//							{
		//								Tax4 += Tax3;
		//								Tax3 = 0;
		//								if (Tax4 < 0)
		//								{
		//									Tax1 = Tax4;
		//									Tax4 = 0;
		//								}
		//							}
		//						}
		//					}
		//					vsPeriod.TextMatrix(0, 0, "Period Due:");
		//					vsPeriod.TextMatrix(0, 1, "1) " + Strings.Format(Tax1, "#,##0.00"));
		//					// + (CCur(Grid.TextMatrix(TotLine, 7)) + CCur(Grid.TextMatrix(TotLine, 8))), "#,##0.00")
		//					vsPeriod.TextMatrix(0, 2, "2) " + Strings.Format(Tax2, "#,##0.00"));
		//					vsPeriod.TextMatrix(0, 3, "3) " + Strings.Format(Tax3, "#,##0.00"));
		//					vsPeriod.TextMatrix(0, 4, "4) " + Strings.Format(Tax4, "#,##0.00"));
		//				}
		//			}
		//			else
		//			{
		//				strPeriodToolTip[1] = "";
		//				strPeriodToolTip[2] = "";
		//				strPeriodToolTip[3] = "";
		//				strPeriodToolTip[4] = "";
		//			}
		//		}
		//		else
		//		{
		//			strPeriodToolTip[1] = "";
		//			strPeriodToolTip[2] = "";
		//			strPeriodToolTip[3] = "";
		//			strPeriodToolTip[4] = "";
		//		}
		//	}
		//}

		private void cmbYear_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbYear.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void cmbYear_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (FCConvert.ToInt32(KeyCode) == 37)
			{
				// left = 37
				KeyCode = 0;
			}
			else if (FCConvert.ToInt32(KeyCode) == 39)
			{
				// right = 39
				if (txtTransactionDate.Visible && txtTransactionDate.Enabled)
				{
					txtTransactionDate.Focus();
				}
				KeyCode = 0;
			}
			else if (KeyCode == Keys.Return)
			{
				// if they hit enter, move them to the next field
			}
			else if (KeyCode == Keys.Space)
			{
				if (modAPIsConst.SendMessageByNum(cmbYear.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
				{
					modAPIsConst.SendMessageByNum(cmbYear.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
					KeyCode = 0;
				}
			}
		}

		//public void cmdDisc_Click(int Index, object sender, System.EventArgs e)
		//{
		//	switch (Index)
		//	{
		//		case 0:
		//			{
		//				// Yes
		//				// continue processing
		//				// add this payment to the PaymentArray and to the Bottom List
		//				AddDiscountToList();
		//				// hide discount fram and show grid, mnuoptions, payment frame
		//				fraDiscount.Visible = false;
		//				GRID.Visible = true;
		//				fraPayment.Visible = true;
		//				mnuPayment.Visible = true;
		//				cmdPaymentPreview.Visible = true;
		//				break;
		//			}
		//		case 1:
		//			{
		//				// No
		//				// let the user adjust the amount to be paid and recalculate
		//				lblDiscountInstructions.Text = "Please enter the correct amount in the space provided below and press Enter.  When it is correct press Yes.";
		//				txtDiscPrin.ReadOnly = false;
		//				txtDiscDisc.ReadOnly = false;
		//				txtDiscPrin.SelectionStart = 0;
		//				txtDiscPrin.SelectionLength = txtDiscPrin.Text.Length;
		//				txtDiscPrin.Focus();
		//				break;
		//			}
		//		case 2:
		//			{
		//				// Cancel
		//				// exit window with no recourse setting the cmbCode to "P"
		//				fraDiscount.Visible = false;
		//				GRID.Visible = true;
		//				fraPayment.Visible = true;
		//				mnuPayment.Visible = true;
		//				cmdPaymentPreview.Visible = true;
		//				frmRECLStatus.InstancePtr.boolChangingCode = true;
		//				// MAL@20071115
		//				// cmbCode.ListIndex = 3
		//				cmbCode.SelectedIndex = modStatusPayments.GetPaymentCodeListIndex("P");
		//				frmRECLStatus.InstancePtr.boolChangingCode = false;
		//				break;
		//			}
		//		case 3:
		//			{
		//				// Cancel
		//				lblOriginalTax_DblClick();
		//				break;
		//			}
		//	}
		//	//end switch
		//}

		//public void cmdDisc_Click(short Index)
		//{
		//	cmdDisc_Click(Index, cmdDisc[Index], new System.EventArgs());
		//}

		//private void cmdDisc_Click(object sender, System.EventArgs e)
		//{
		//	int index = cmdDisc.IndexOf((FCButton)sender);
		//	cmdDisc_Click(index, sender, e);
		//}

		//private void cmdNoteCancel_Click(object sender, System.EventArgs e)
		//{
		//	// Hide the Note frame
		//	fraEditNote.Visible = false;
		//	GRID.Visible = true;
		//	if (modExtraModules.IsThisCR() && modGlobalConstants.Statics.gboolCR)
		//	{
		//		fraPayment.Visible = true;
		//		mnuPayment.Visible = true;
		//		cmdPaymentPreview.Visible = true;
		//		btnProcess.Enabled = true;
		//		btnProcess.Visible = true;
		//		btnSaveExit.Enabled = true;
		//		btnSaveExit.Visible = true;
		//	}
		//	else if (boolPayment)
		//	{
		//		fraPayment.Visible = true;
		//		mnuPayment.Visible = true;
		//		cmdPaymentPreview.Visible = true;
		//		btnProcess.Enabled = true;
		//		btnProcess.Visible = true;
		//		btnSaveExit.Enabled = true;
		//		btnSaveExit.Visible = true;
		//	}
		//}

		private void cmdNoteProcess_Click(object sender, System.EventArgs e)
		{
			EditNote();
			fraEditNote.Visible = false;
			GRID.Visible = true;
			if (modExtraModules.IsThisCR() && modGlobalConstants.Statics.gboolCR)
			{
				fraPayment.Visible = true;
				mnuPayment.Visible = true;
				cmdPaymentPreview.Visible = true;
				btnProcess.Enabled = true;
				btnProcess.Visible = true;
				btnSaveExit.Enabled = true;
				btnSaveExit.Visible = true;
			}
			else if (boolPayment)
			{
				fraPayment.Visible = true;
				mnuPayment.Visible = true;
				cmdPaymentPreview.Visible = true;
				btnProcess.Enabled = true;
				btnProcess.Visible = true;
				btnSaveExit.Enabled = true;
				btnSaveExit.Visible = true;
			}
		}

		private void cmdRIClose_Click(object sender, System.EventArgs e)
		{
			fraRateInfo.Visible = false;
			mnuFilePrintRateInfo.Visible = fraRateInfo.Visible;
		}

		public void cmdRIClose_Click()
		{
			cmdRIClose_Click(cmdRIClose, new System.EventArgs());
		}

		//private void cmdWOProcess_Click(int Index, object sender, System.EventArgs e)
		//{
		//	switch (Index)
		//	{
		//		case 0:
		//			{
		//				// Process
		//				// continue processing
		//				// add this payment to the PaymentArray and to the Bottom List
		//				AddWriteOfftoList();
		//				// hide writeoff frame and show grid, mnuoptions, payment frame
		//				fraWriteOff.Visible = false;
		//				GRID.Visible = true;
		//				fraPayment.Visible = true;
		//				mnuPayment.Visible = true;
		//				cmdPaymentPreview.Visible = true;
		//				break;
		//			}
		//		case 1:
		//			{
		//				// Cancel
		//				// exit window with no recourse setting the cmbCode to "P"
		//				fraWriteOff.Visible = false;
		//				GRID.Visible = true;
		//				fraPayment.Visible = true;
		//				mnuPayment.Visible = true;
		//				cmdPaymentPreview.Visible = true;
		//				frmRECLStatus.InstancePtr.boolChangingCode = true;
		//				// MAL@20071115
		//				// cmbCode.ListIndex = 3
		//				cmbCode.SelectedIndex = modStatusPayments.GetPaymentCodeListIndex("P");
		//				frmRECLStatus.InstancePtr.boolChangingCode = false;
		//				break;
		//			}
		//	}
		//	//end switch
		//	// Reactivate F11 and F12
		//	btnProcess.Enabled = true;
		//	btnSaveExit.Enabled = true;
		//}

		//private void cmdWOProcess_Click(object sender, System.EventArgs e)
		//{
		//	int index = cmdWOProcess.IndexOf((FCButton)sender);
		//	cmdWOProcess_Click(index, sender, e);
		//}

		//private void frmRECLStatus_Activated(object sender, System.EventArgs e)
		//{
		//	bool boolContinue = false;
		//	int lngAcct = 0;
		//	int intCT;
		//	int lngDefYear = 0;
		//	//FC:FINAL:DDU:#i142 moved payment checker where textbox is not empty
		//	//boolPayment = (frmCLGetAccount.InstancePtr.txtHold.Text == "P");
		//	clsDRWrapper rsDefaults = new clsDRWrapper();
		//	mnuFileREComment.Visible = modStatusPayments.Statics.boolRE;
		//	mnuProcess.Enabled = false;
		//	// mnuPayment.Visible = False
		//	if (!modExtraModules.IsThisCR())
		//	{
		//		if (modGlobalConstants.Statics.gboolCR)
		//		{
		//			mnuProcessGoTo.Enabled = false;
		//			mnuProcessGoTo.Visible = false;
		//			cmdFileOptionsPrint.Visible = false;
		//			cmdFileOptionsPrint.Enabled = false;
		//		}
		//		else
		//		{
		//			if (boolPayment)
		//			{
		//				cmdFileOptionsPrint.Visible = true;
		//				cmdFileOptionsPrint.Enabled = true;
		//			}
		//			else
		//			{
		//				cmdFileOptionsPrint.Visible = false;
		//				cmdFileOptionsPrint.Enabled = false;
		//			}
		//		}
		//		if (FormReset)
		//		{
		//			frmRECLStatus.InstancePtr.Text = "Real Estate Account Status as of " + Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy");
		//			RemoveSavedPayments();
		//			Reset_Sum();
		//		}
		//		else
		//		{
		//			modStatusPayments.Statics.EffectiveDate = FCConvert.ToDateTime(Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy"));
		//			if (modStatusPayments.Statics.boolRE)
		//			{
		//				frmRECLStatus.InstancePtr.Text = "Real Estate Account Status as of " + Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy");
		//			}
		//			else
		//			{
		//				frmRECLStatus.InstancePtr.Text = "Personal Property Account Status as of " + Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy");
		//			}
		//		}
		//	}
		//	else
		//	{
		//		mnuProcessGoTo.Enabled = false;
		//		mnuProcessGoTo.Visible = false;
		//		cmdFileOptionsPrint.Visible = false;
		//		cmdFileOptionsPrint.Enabled = false;
		//		if (FormReset)
		//		{
		//			if (modStatusPayments.Statics.boolRE)
		//			{
		//				frmRECLStatus.InstancePtr.Text = "Real Estate Account Status as of " + Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy");
		//			}
		//			else
		//			{
		//				frmRECLStatus.InstancePtr.Text = "Personal Property Account Status as of " + Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy");
		//			}
		//			RemoveSavedPayments();
		//			Reset_Sum();
		//		}
		//		else
		//		{
		//			if (!FormLoaded)
		//			{
		//				// kk03262015 trocrs-21 Handle lose/get focus without resetting date
		//				modStatusPayments.Statics.EffectiveDate = FCConvert.ToDateTime(Strings.Format(DateTime.Now, "MM/dd/yyyy"));
		//			}
		//			else
		//			{
		//				modStatusPayments.Statics.EffectiveDate = FCConvert.ToDateTime(Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy"));
		//			}
		//			if (modStatusPayments.Statics.boolRE)
		//			{
		//				frmRECLStatus.InstancePtr.Text = "Real Estate Account Status as of " + Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy");
		//			}
		//			else
		//			{
		//				frmRECLStatus.InstancePtr.Text = "Personal Property Account Status as of " + Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy");
		//			}
		//		}
		//	}
		//	if (!FormLoaded)
		//	{
		//		// setup the recordset with the account list from the search screen in it
		//		if (strSearchSQL != "")
		//		{
		//			mnuFileNextAccount.Visible = true;
		//			mnuFilePreviousAccount.Visible = true;
		//		}
		//		else
		//		{
		//			mnuFileNextAccount.Visible = false;
		//			mnuFilePreviousAccount.Visible = false;
		//		}
		//	}
		//	if (FormLoaded && !FormReset)
		//	{
		//	}
		//	else
		//	{
		//		FormLoaded = true;
		//		FormReset = false;
		//		CollapseFlag = true;
		//		boolLoading = true;
		//		FCUtils.EraseSafe(modStatusPayments.Statics.intLDNAnswer);
		//		// reset the answer for the lien discharge notice
		//		// reset all of the arrays in case the form load did not fire
		//		// rsCL.Reset
		//		// rsLR.Reset
		//		if (FormReset)
		//		{
		//			FCUtils.EraseSafe(modStatusPayments.Statics.PaymentArray);
		//		}
		//		FCUtils.EraseSafe(modStatusPayments.Statics.dblCurrentInt);
		//		FCUtils.EraseSafe(modStatusPayments.Statics.dateOldDate);
		//		GRID.Rows = 1;
		//		// Doevents
		//		boolContinue = StartUp();
		//		// this start the computation
		//		if (modStatusPayments.Statics.boolRE)
		//		{
		//			lngAcct = StaticSettings.TaxCollectionValues.LastAccountRE;
		//			mnuFileTaxInfoSheet.Visible = true;
		//		}
		//		else
		//		{
		//			lngAcct = StaticSettings.TaxCollectionValues.LastAccountPP;
		//			mnuFileTaxInfoSheet.Visible = false;
		//		}
		//		// check for a note
		//		strNoteText = Strings.Trim(GetAccountNote(ref lngAcct, ref boolPop));
		//		if (boolContinue)
		//		{
		//			GRID.Outline(1);
		//			// collapses the grid to level zero (one)
		//			GRID.Visible = true;
		//			CollapseFlag = false;
		//		}
		//		else
		//		{
		//			if (FormReset)
		//			{
		//				FCMessageBox.Show("Error resetting this screen.", MsgBoxStyle.Exclamation, "Error On Reset");
		//			}
		//			else
		//			{
		//				// account does not exist so go back to selection screen
		//				frmCLGetAccount.InstancePtr.Show(App.MainForm);
		//				if (boolPayment)
		//				{
		//					frmCLGetAccount.InstancePtr.txtHold.Text = "P";
		//				}
		//				else
		//				{
		//					frmCLGetAccount.InstancePtr.txtHold.Text = "S";
		//				}
		//			}
		//			Close();
		//			return;
		//		}
		//		if (boolPayment)
		//		{
		//			modStatusPayments.FillPaymentComboBoxes();
		//			modStatusPayments.ResetPaymentFrame();
		//			fraPayment.Visible = true;
		//			//fraPayment.Left = FCConvert.ToInt32((this.Width - fraPayment.Width) / 3.0);
		//			fraStatusLabels.Visible = false;
		//			fraPPStatusLabels.Visible = false;
		//			Format_PaymentGrid();
		//			if (!modStatusPayments.FillPendingTransactions())
		//			{
		//				frmWait.InstancePtr.Unload();
		//				Close();
		//			}
		//			modStatusPayments.CheckAllPending();
		//			modStatusPayments.Format_vsPeriod();
		//			if (txtReference.Enabled && txtReference.Visible)
		//			{
		//				txtReference.Focus();
		//			}
		//			btnProcess.Enabled = true;
		//			btnSaveExit.Enabled = true;
		//			// check for tax club payments
		//			// if there are outstanding payments, then show the tax club label to click
		//			// MAL@20080812: Made adjustments to accomodate multiple tax clubs
		//			// Tracker Reference: 14033
		//			if (modStatusPayments.Statics.boolRE)
		//			{
		//				lngDefYear = modStatusPayments.FindDefaultYear( StaticSettings.TaxCollectionValues.LastAccountRE);
		//			}
		//			else
		//			{
		//				lngDefYear = modStatusPayments.FindDefaultYear( StaticSettings.TaxCollectionValues.LastAccountPP);
		//			}
		//			if (CheckTaxClubPayments(lngDefYear))
		//			{
		//				// If boolRE Then
		//				// lngDefYear = FindDefaultYear(CurrentAccountRE)
		//				// Else
		//				// lngDefYear = FindDefaultYear(CurrentAccountPP)
		//				// End If
		//				if (modExtraModules.FormatYear(lngDefYear.ToString()) != modExtraModules.FormatYear(FCConvert.ToString(lblTaxClub.Tag)))
		//				{
		//					// if there is a different year than the TC year then set the year to Auto
		//					cmbYear.SelectedIndex = cmbYear.Items.Count - 1;
		//				}
		//				else
		//				{
		//					// else set it to the TC year
		//					for (intCT = 0; intCT <= cmbYear.Items.Count - 1; intCT++)
		//					{
		//						if (cmbYear.Items[intCT].ToString().Replace("*", "") == modExtraModules.FormatYear(FCConvert.ToString(lblTaxClub.Tag)))
		//						{
		//							cmbYear.SelectedIndex = intCT;
		//							break;
		//						}
		//					}
		//				}
		//			}
		//			else if (modStatusPayments.Statics.gboolDefaultPaymentsToAuto)
		//			{
		//				// this sets the year to Auto
		//				cmbYear.SelectedIndex = cmbYear.Items.Count - 1;
		//			}
		//			else
		//			{
		//				if (modStatusPayments.Statics.boolRE)
		//				{
		//					lngDefYear = modStatusPayments.FindDefaultYear( StaticSettings.TaxCollectionValues.LastAccountRE);
		//				}
		//				else
		//				{
		//					lngDefYear = modStatusPayments.FindDefaultYear( StaticSettings.TaxCollectionValues.LastAccountPP);
		//				}
		//				// else set it to the TC year
		//				for (intCT = 0; intCT <= cmbYear.Items.Count - 1; intCT++)
		//				{
		//					if (cmbYear.Items[intCT].ToString().Replace("*", "") == modExtraModules.FormatYear(lngDefYear.ToString()))
		//					{
		//						cmbYear.SelectedIndex = intCT;
		//						break;
		//					}
		//				}
		//			}
		//			if (NegativeBillValues())
		//			{
		//				//FC:FINAL:AM:#i315 - show the message in a timer
		//				Timer timer = new Timer();
		//				timer.Tick += (s, a) =>
		//				{
		//					timer.Stop();
		//					FCMessageBox.Show("You have some years with negative balances.  Press F11 to apply the appropriate entries.", MsgBoxStyle.Information, "Negative Bill Balances");
		//				};
		//				timer.Start();
		//			}
		//		}
		//		else
		//		{
		//			fraPayment.Visible = false;
		//			if (modStatusPayments.Statics.boolRE)
		//			{
		//				fraStatusLabels.Visible = true;
		//			}
		//			else
		//			{
		//				fraPPStatusLabels.Visible = true;
		//			}
		//			GRID.Visible = true;
		//			if (GRID.Enabled && GRID.Visible)
		//			{
		//				GRID.Focus();
		//			}
		//		}
		//	}
		//	if (boolPayment)
		//	{
		//		// kk03132105 trocrs-36  Option to disable automatically filling in payment amount on Insert or Double Click
		//		rsDefaults.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
		//		if (!rsDefaults.EndOfFile())
		//		{
		//			/*? On Error Resume Next  */
		//			boolDisableInsert = FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("DisableAutoPmtFill"));
		//			/*? On Error GoTo 0 */
		//		}
		//		mnuPayment.Visible = true;
		//		cmdPaymentPreview.Visible = true;
		//		mnuProcessGoTo.Text = strStatusString;
		//		vsPayments.Visible = true;
		//		lblPaymentInfo.Visible = true;
		//	}
		//	else
		//	{
		//		lblPaymentInfo.Visible = false;
		//		mnuPayment.Visible = false;
		//		cmdPaymentPreview.Visible = false;
		//		mnuProcessGoTo.Text = strPaymentString;
		//	}
		//	boolLoading = false;
		//	blnInProgress = false;
		//	blnIsCollapsed = true;
		//	mnuProcess.Enabled = true;
		//	frmWait.InstancePtr.Unload();
		//	if (strNoteText != "")
		//	{
		//		// if there is a comment
		//		if (boolPop)
		//		{
		//			SHOWAGAIN:
		//			;
		//			//FC:FINAL:AM:#61 - show the message in a timer
		//			Timer timer = new Timer();
		//			timer.Tick += (s, a) =>
		//			{
		//				timer.Stop();
		//				if (FCMessageBox.Show("Account " + FCConvert.ToString(lngAcct) + " note." + "\r\n" + strNoteText, MsgBoxStyle.OkOnly | MsgBoxStyle.Information | MsgBoxStyle.DefaultButton2, "Account Note") != DialogResult.OK)
		//				{
		//					// GoTo SHOWAGAIN
		//				}
		//			};
		//			timer.Start();
		//			boolPop = false;
		//		}
		//		imgNote.Visible = true;
		//		mnuFileEditNote.Visible = true;
		//		// mnuFilePriority.Visible = True
		//	}
		//	else
		//	{
		//		imgNote.Visible = false;
		//		// mnuFilePriority.Visible = False
		//	}
		//	boolLoading = false;
		//	//FC:FINAL:DDU:#1942 - align cols
		//	vsPayments.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//	vsPayments.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//	vsPayments.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//	vsPayments.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//	vsPayments.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//	vsPayments.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//	vsPayments.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
		//	vsPayments.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
		//	vsPayments.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
		//	vsPayments.ColAlignment(12, FCGrid.AlignmentSettings.flexAlignRightCenter);
		//}

		//public void Form_Activate()
		//{
		//	frmRECLStatus_Activated(this, new System.EventArgs());
		//}

		//private void frmRECLStatus_Enter(object sender, System.EventArgs e)
		//{
		//	//FC:FINAL:RPU:#27 fraRateInfo should be closed only from close button 
		//	//if (fraRateInfo.Visible == true)
		//	//fraRateInfo.Visible = false;
		//	mnuFilePrintRateInfo.Visible = fraRateInfo.Visible;
		//}

		//private void frmRECLStatus_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		//{
		//	Keys KeyCode = e.KeyCode;
		//	int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
		//	if (KeyCode == Keys.Return)
		//	{
		//		// SendKeys "{tab}"
		//	}
		//	else if (KeyCode == Keys.Escape)
		//	{
		//		if (vsPreview.Visible == true)
		//		{
		//			// close the preview frame
		//			vsPreview.Visible = false;
		//			GRID.Visible = true;
		//			cmdPaymentPreview.Text = "Preview";
		//		}
		//		else if (fraRateInfo.Visible == true)
		//		{
		//			// close the rate info frame
		//			KeyCode = 0;
		//			cmdRIClose_Click();
		//		}
		//		else
		//		{
		//			// If vsPayments.rows <= 1 Then
		//			// Exit
		//			KeyCode = 0;
		//			mnuProcessExit_Click();
		//			// End If
		//		}
		//	}
		//	else if (KeyCode == Keys.F10)
		//	{
		//		KeyCode = 0;
		//	}
		//	else if (KeyCode == Keys.Insert)
		//	{
		//		KeyCode = 0;
		//		if (boolDisableInsert)
		//		{
		//			// kk03132015  trocrs-36  Add option to disable automatically filling in payment amount
		//			return;
		//		}
		//		if (boolPayment)
		//		{
		//			// create a payment line for the year or all years if it is on auto
		//			string strPer = "";
		//			string strCode = "";
		//			double dbl1 = 0;
		//			double dbl2 = 0;
		//			double dbl3 = 0;
		//			double dbl4 = 0;
		//			double dblOwe1;
		//			double dblOwe2;
		//			double dblOwe3;
		//			double dblOwe4;
		//			dblOwe1 = 0;
		//			dblOwe2 = 0;
		//			dblOwe3 = 0;
		//			dblOwe4 = 0;
		//			strPer = Strings.Left(cmbPeriod.Items[cmbPeriod.SelectedIndex].ToString(), 1);
		//			strCode = Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1);
		//			if (strCode == "A")
		//			{
		//				// MsgBox "Please enter an abatement value manually.", MsgBoxStyle.Exclamation, "Abatement Insert"
		//				// do not insert anything for an abatement
		//				// Exit Sub
		//				// Dave took out the revesal = true argument in the createoppisitionline function
		//				modStatusPayments.CreateOppositionLine(GRID.FindRow("=", GRID.FindRow(cmbYear.Items[cmbYear.SelectedIndex].ToString(), 0, lngColYear, false, false), lngColGridCode, false, false));
		//			}
		//			else
		//			{
		//				if (cmbYear.Items[cmbYear.SelectedIndex].ToString() == "Auto")
		//				{
		//					modStatusPayments.CreateOppositionLine(GRID.Rows - 1);
		//				}
		//				else
		//				{
		//					// If Left(strPer, 1) = "A" Then
		//					// CreateOppositionLine (NextSpacerLine(Grid.FindRow(cmbYear.List(cmbYear.ListIndex), 1, 1)) - 1)
		//					// Else
		//					// then take the info from the period grid
		//					if (vsPeriod.TextMatrix(0, FCConvert.ToInt32(Conversion.Val(strPer))) != "")
		//					{
		//						if (strPer == "A")
		//						{
		//							if (vsPeriod.TextMatrix(0, 1) == "")
		//								return;
		//							if (vsPeriod.TextMatrix(0, 1) != "")
		//								dblOwe1 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 1), vsPeriod.TextMatrix(0, 1).Length - 2));
		//							if (vsPeriod.TextMatrix(0, 2) != "")
		//								dblOwe2 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 2), vsPeriod.TextMatrix(0, 2).Length - 2));
		//							if (vsPeriod.TextMatrix(0, 3) != "")
		//								dblOwe3 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 3), vsPeriod.TextMatrix(0, 3).Length - 2));
		//							if (vsPeriod.TextMatrix(0, 4) != "")
		//								dblOwe4 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 4), vsPeriod.TextMatrix(0, 4).Length - 2));
		//							if (strPeriodToolTip[1] != "")
		//							{
		//								// If CDate(strPeriodToolTip(1)) < EffectiveDate Then
		//								if (vsPeriod.TextMatrix(0, 1) != "")
		//									dbl1 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 1), vsPeriod.TextMatrix(0, 1).Length - 2));
		//								// End If
		//								if (DateAndTime.DateValue(strPeriodToolTip[1]).ToOADate() < modStatusPayments.Statics.EffectiveDate.ToOADate())
		//								{
		//									if (strPeriodToolTip[2] != "")
		//									{
		//										// MATTHEW 11/12/2004 YORK
		//										// If CDate(strPeriodToolTip(2)) < EffectiveDate Then
		//										if (DateAndTime.DateValue(strPeriodToolTip[2]).ToOADate() < modStatusPayments.Statics.EffectiveDate.ToOADate() || dbl1 == 0)
		//										{
		//											if (vsPeriod.TextMatrix(0, 2) != "")
		//												dbl2 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 2), vsPeriod.TextMatrix(0, 2).Length - 2));
		//											if (DateAndTime.DateValue(strPeriodToolTip[2]).ToOADate() >= modStatusPayments.Statics.EffectiveDate.ToOADate() && dbl1 == 0)
		//											{
		//												if (vsPeriod.TextMatrix(0, 3) != "")
		//													dbl3 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 3), vsPeriod.TextMatrix(0, 3).Length - 2));
		//											}
		//										}
		//										// COREY 01/09/2009
		//										if (strPeriodToolTip[3] != "")
		//										{
		//											if (DateAndTime.DateValue(strPeriodToolTip[3]).ToOADate() < modStatusPayments.Statics.EffectiveDate.ToOADate() || (dbl2 == 0 && dbl1 == 0))
		//											{
		//												if (vsPeriod.TextMatrix(0, 3) != "")
		//													dbl3 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 3), vsPeriod.TextMatrix(0, 3).Length - 2));
		//												if (DateAndTime.DateValue(strPeriodToolTip[3]).ToOADate() >= modStatusPayments.Statics.EffectiveDate.ToOADate() && (dbl2 == 0 && dbl1 == 0))
		//												{
		//													if (vsPeriod.TextMatrix(0, 4) != "")
		//														dbl4 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 4), vsPeriod.TextMatrix(0, 4).Length - 2));
		//												}
		//											}
		//											if (strPeriodToolTip[4] != "")
		//											{
		//												if (DateAndTime.DateValue(strPeriodToolTip[4]).ToOADate() < modStatusPayments.Statics.EffectiveDate.ToOADate() || (dbl3 == 0 && dbl2 == 0 && dbl1 == 0))
		//												{
		//													if (vsPeriod.TextMatrix(0, 4) != "")
		//														dbl4 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 4), vsPeriod.TextMatrix(0, 4).Length - 2));
		//												}
		//											}
		//										}
		//									}
		//								}
		//							}
		//							// Select Case Val(strPer)
		//							// Case 1 To 4
		//							// CreatePeriodOppositionLine dbl1, dbl2, dbl3, dbl4, Val(strPer), FormatYear(cmbYear.List(cmbYear.ListIndex))
		//							// Case Else
		//							// End Select
		//							CreatePeriodOppositionLine(dbl1, dbl2, dbl3, dbl4, 0, FCConvert.ToInt16(modExtraModules.FormatYear(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//						}
		//						else
		//						{
		//							if (vsPeriod.TextMatrix(0, 1) == "")
		//								return;
		//							if (vsPeriod.TextMatrix(0, 1) != "")
		//								dbl1 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 1), vsPeriod.TextMatrix(0, 1).Length - 2));
		//							if (vsPeriod.TextMatrix(0, 2) != "")
		//								dbl2 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 2), vsPeriod.TextMatrix(0, 2).Length - 2));
		//							if (vsPeriod.TextMatrix(0, 3) != "")
		//								dbl3 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 3), vsPeriod.TextMatrix(0, 3).Length - 2));
		//							if (vsPeriod.TextMatrix(0, 4) != "")
		//								dbl4 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 4), vsPeriod.TextMatrix(0, 4).Length - 2));
		//							if (Conversion.Val(strPer) >= 1 && Conversion.Val(strPer) <= 4)
		//							{
		//								CreatePeriodOppositionLine(dbl1, dbl2, dbl3, dbl4, FCConvert.ToInt16(Conversion.Val(strPer)), FCConvert.ToInt16(modExtraModules.FormatYear(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//							}
		//							else
		//							{
		//							}
		//						}
		//					}
		//					else
		//					{
		//						txtInterest.Text = "0.00";
		//						txtPrincipal.Text = "0.00";
		//						txtCosts.Text = "0.00";
		//					}
		//					// End If
		//				}
		//			}
		//		}
		//	}
		//}

		private void frmRECLStatus_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// set the back color of any labels or textboxes
			modGlobalFunctions.SetTRIOColors(this);

			lblGrpInfo_Pmt.Text = "G";

#if TWCL0000
            //FC:FINAL:PB - issue #2089: replace .image with .imagesource (new icon)
            //this.imgNote.Image = global::TWCL0000.Properties.Resources.imgNote;
            this.imgNote.ImageSource = "imgnote?color=#707884";
#endif

            boolPayment = (frmCLGetAccount.InstancePtr.txtHold.Text == "P");
			boolUnloadOK = false;
		}

		public void SetToolTip()
		{
			// this will set all of the strings of the tool tips except ones that are in grid cells
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			bool boolContinue = false;
			if (vsPayments.Rows > 1)
			{
				if (!boolUnloadOK)
				{
					boolContinue = true;
					// kk03032014  trocrs-26  Double check for pending payments so we don't delete saved payments
					if (modExtraModules.IsThisCR())
					{
						boolContinue = !PendingSaved();
					}
					if (boolContinue)
					{
						if (FCMessageBox.Show("You have pending payments, would you like to exit without saving them?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Exit Payment Screen") == DialogResult.Yes)
						{
							// erase the pending transactions
							while (!(vsPayments.Rows <= 1))
							{
								DeletePaymentRow(1);
							}
							FCUtils.EraseSafe(modStatusPayments.Statics.PaymentArray);
						}
						else
						{
							e.Cancel = true;
							return;
						}
					}
				}
			}
			FormLoaded = false;
			if (!modExtraModules.IsThisCR())
			{
				App.MainForm.Show();
			}
			boolDoNotResetRTD = false;
			FCUtils.EraseSafe(modStatusPayments.Statics.gboolCLNoIntFromTaxClub);
			FCUtils.EraseSafe(modStatusPayments.Statics.gboolCLSetIntPaidDateBack);
			FCUtils.EraseSafe(modStatusPayments.Statics.gdtCLOldDateForTaxClub);
		}

		private bool PendingSaved()
		{
			bool PendingSaved = false;
			// this function will check the pending payments to see if there are any that have not been saved yet
			// if there are any pending payments that are not saved, then a true will be returned otherwise false will be
			// kk03032014  trocrs-26  Update this function to check the status of the payment record so we aren't deleting
			// payments that have been saved in another CR session
			int intCT/*unused?*/;
			clsDRWrapper rsPend = new clsDRWrapper();
			PendingSaved = true;
			// For intCT = 1 To vsPayments.rows - 1
			// If Val(vsPayments.TextMatrix(intCT, 10)) = 0 Then
			// PendingSaved = False
			// Exit For
			// End If
			// Next
			if (modStatusPayments.Statics.boolRE)
			{
				rsPend.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountPayments + ") AS qCAP WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND ReceiptNumber = -1 AND Code <> 'I' AND BillCode <> 'P' ORDER BY RecordedTransactionDate desc", modExtraModules.strCLDatabase);
			}
			else
			{
				rsPend.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountPayments + ") AS qCAP WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND ReceiptNumber = -1 AND Code <> 'I' AND BillCode = 'P' ORDER BY RecordedTransactionDate desc", modExtraModules.strCLDatabase);
			}
			PendingSaved = rsPend.EndOfFile();
			return PendingSaved;
		}

		private void frmRECLStatus_Resize(object sender, System.EventArgs e)
		{
			//FC:HACK:AM: the size can get negative
			if (frmRECLStatus.InstancePtr.ClientArea.Height > 0)
			{
				// when the form gets resized, this will adjust the grid size and the rows/columns
				Format_Grid();
				AutoSize_GRID();
				if (fraRateInfo.Visible == true)
				{
					frmRECLStatus.InstancePtr.fraRateInfo.Left = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Width - frmRECLStatus.InstancePtr.fraRateInfo.Width) / 2.0);
					//frmRECLStatus.InstancePtr.fraRateInfo.Top = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Height - frmRECLStatus.InstancePtr.fraRateInfo.Height) / 2.0);
				}
				mnuFilePrintRateInfo.Visible = fraRateInfo.Visible;
				// Tracker Reference: 11187
				if (fraEditNote.Visible == true)
				{
					fraEditNote.Left = FCConvert.ToInt32((this.Width - fraEditNote.Width) / 2.5);
					fraEditNote.Top = FCConvert.ToInt32((this.Height - fraEditNote.Height) / 2.5);
				}
				// MAL@20071011: Adjusted TA label
				if (lblTaxAcquired.Visible == true)
				{
					lblTaxAcquired.Top = 0;
				}
				//FC:FINAL:DDU:#1942 - align cols
				vsPayments.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsPayments.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsPayments.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsPayments.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				vsPayments.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				vsPayments.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				vsPayments.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsPayments.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsPayments.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsPayments.ColAlignment(12, FCGrid.AlignmentSettings.flexAlignRightCenter);
			}
		}

		//private bool StartUp()
		//{
		//	bool StartUp = false;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// VBto upgrade warning: bl As FixedString	OnWrite(string)
		//		char[] bl = new char[1];
		//		int blnumber = 0;
		//		int rc;
		//		bool boolLienDischarged = false;
		//		clsDRWrapper rsDischarge = new clsDRWrapper();
		//		int intCT;
		//		clsDRWrapper rsTemp = new clsDRWrapper();
		//		// reset the totals
		//		dblTotalPerDiem = 0;
		//		FCUtils.EraseSafe(modStatusPayments.Statics.gboolCLNoIntFromTaxClub);
		//		FCUtils.EraseSafe(modStatusPayments.Statics.gboolCLSetIntPaidDateBack);
		//		FCUtils.EraseSafe(modStatusPayments.Statics.gdtCLOldDateForTaxClub);
		//		dblLastTCInt = 0;
		//		boolOngoingReversal = false;
		//		modStatusPayments.Statics.gboolCheckedForReversals = false;
		//		boolNoCurrentInt = false;
		//		// format the flexgrid
		//		Format_Grid();
		//		//FC:FINAL:AM: add the expand button
		//		GRID.AddExpandButton();
		//		GRID.RowHeadersWidth = 10;
		//		GRID.Redraw = false;
		//		// turn this on when ready
		//		GRID.Visible = false;
		//		CurRow = 0;
		//		// start on line 1
		//		// opens the collections database to the correct billing master record
		//		if (modStatusPayments.Statics.boolRE)
		//		{
		//			rsCL.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingType = 'RE' ORDER BY BillingYear Desc", modExtraModules.strCLDatabase);
		//			mnuFileMortgageHolderInfo.Visible = true;
		//		}
		//		else
		//		{
		//			rsCL.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingType = 'PP' ORDER BY BillingYear Desc", modExtraModules.strCLDatabase);
		//			mnuFileMortgageHolderInfo.Visible = false;
		//		}
		//		rc = rsCL.RecordCount();
		//		if (rc != 0)
		//		{
		//			// check for a comment
		//			//					lblAcctComment.ForeColor = Color.Red;
		//			ToolTip1.SetToolTip(lblAcctComment, "This account has a comment.");

		//			// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
		//			if (Strings.Trim(modStatusPayments.GetAccountComment_2(FCConvert.ToInt32(rsCL.Get_Fields("Account")), modStatusPayments.Statics.boolRE)) != "")
		//			{
		//				lblAcctComment.Visible = true;
		//			}
		//			else
		//			{
		//				lblAcctComment.Visible = false;
		//			}
		//			if (modStatusPayments.Statics.boolRE)
		//			{
		//				FillInREInfo();
		//				lngGroupNumber = modGlobalFunctions.GetGroupNumber_6(StaticSettings.TaxCollectionValues.LastAccountRE, "RE");
		//			}
		//			else
		//			{
		//				FillInPPInfo();
		//				lngGroupNumber = modGlobalFunctions.GetGroupNumber_6(StaticSettings.TaxCollectionValues.LastAccountPP, "PP");
		//			}
		//			if (lngGroupNumber != 0)
		//			{
		//				mnuFilePrintGroupInfo.Visible = true;
		//			}
		//			else
		//			{
		//				mnuFilePrintGroupInfo.Visible = false;
		//			}
		//			// checks for a lien record
		//			if (FCConvert.ToString(rsCL.Get_Fields_String("WhetherBilledBefore")) == "L" && ((rsCL.Get_Fields_Int32("LienRecordNumber"))) != 0)
		//			{
		//				// if this is a bill created by a lien
		//				strSQL = "SELECT * FROM LIENREC WHERE ID = " + FCConvert.ToString(rsCL.Get_Fields_Int32("LienRecordNumber"));
		//				rsLR.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
		//				bl = "L".ToCharArray();
		//				bcode = "LIEN";
		//				blnumber = FCConvert.ToInt32(rsLR.Get_Fields_Int32("ID"));
		//			}
		//			else if (rsCL.Get_Fields_String("WhetherBilledBefore") == "A")
		//			{
		//				// this bill is an (A)ctual bill
		//				rsLR.Reset();
		//				bl = "R".ToCharArray();
		//				bcode = "BILL";
		//				blnumber = FCConvert.ToInt32(rsCL.Get_Fields_Int32("ID"));
		//				// BillKey
		//			}
		//			// fill the main value labels
		//			FillMainLabels();
		//			if (boolDeleted)
		//			{
		//				this.Text = "DELETED " + this.Text;
		//			}
		//			// clear the abatement array
		//			for (intCT = 0; intCT <= Information.UBound(modStatusPayments.Statics.AbatePaymentsArray, 1) - 1; intCT++)
		//			{
		//				modStatusPayments.Statics.AbatePaymentsArray[intCT].Per1 = 0;
		//				modStatusPayments.Statics.AbatePaymentsArray[intCT].Per2 = 0;
		//				modStatusPayments.Statics.AbatePaymentsArray[intCT].Per3 = 0;
		//				modStatusPayments.Statics.AbatePaymentsArray[intCT].Per4 = 0;
		//			}
		//			boolLienDischarged = false;
		//			FillLabels_2(FCConvert.ToInt32(rsCL.Get_Fields_Int32("ID")));
		//			// this will fill the labels at the top of the screen with account info
		//			// this will only put the latest billing info in
		//			while (!(rsCL.EndOfFile() == true))
		//			{
		//				// check all records from the same account
		//				// checks for a lien record
		//				if (rsCL.Get_Fields_Int32("LienRecordNumber") != 0)
		//				{
		//					strSQL = "SELECT * FROM LIENREC WHERE ID = " + FCConvert.ToString(rsCL.Get_Fields_Int32("LienRecordNumber"));
		//					rsLR.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
		//					if (!boolLienDischarged)
		//					{
		//						// only check it if this has not been found yet
		//						rsDischarge.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + FCConvert.ToString(rsCL.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
		//						// & " AND Printed = False" 'take this out so that a town can print it over and over in needed
		//						if (!rsDischarge.EndOfFile())
		//						{
		//							boolLienDischarged = true;
		//						}
		//					}
		//					if (rsLR.EndOfFile())
		//					{
		//						// if there is no record then only use the regular regord
		//						rsLR.Reset();
		//						bl = "R".ToCharArray();
		//						bcode = "BILL";
		//						blnumber = FCConvert.ToInt32(rsCL.Get_Fields_Int32("ID"));
		//					}
		//					else
		//					{
		//						bl = "L".ToCharArray();
		//						bcode = "LIEN";
		//						blnumber = FCConvert.ToInt32(rsLR.Get_Fields_Int32("ID"));
		//					}
		//				}
		//				else
		//				{
		//					rsLR.Reset();
		//					bl = "R".ToCharArray();
		//					bcode = "BILL";
		//					blnumber = FCConvert.ToInt32(rsCL.Get_Fields_Int32("ID"));
		//				}
		//				// creates the header line for this bill record
		//				CreateMasterLine(ref blnumber);
		//				// calculates the principal and stores it in curPrincipal Due
		//				curPrincipalDue = modStatusPayments.DeterminePrincipal(ref rsCL, ref rsLR, ref bcode, ref intNumberOfPeriods, ref modStatusPayments.Statics.EffectiveDate);
		//				// fills in the rest of the payments
		//				FillGrid(ref blnumber);
		//				rsCL.MoveNext();
		//			}
		//			mnuFileDischarge.Visible = boolLienDischarged;
		//			mnuFilePrintAltLDN.Visible = boolLienDischarged;
		//			// this will adjust the initial height
		//			Adjust_GRID_Height((rc + 2) * GRID.RowHeight(0));
		//			GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngColReference, GRID.Rows - 1, lngColReference, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//			Grid_RowColChange(GRID, EventArgs.Empty);
		//			GRID_Coloring(1);
		//			GRID.Select(0, lngColYear, 0, lngColGridCode);
		//			GRID.CellBorder(Color.Black, 0, 0, 0, 1, 0, 1);
		//			Create_Account_Totals();
  //                  //FC:FINAL:SBE - redesign grid level tree task
  //                  modColorScheme.ColorGrid(GRID);
		//			GRID.Redraw = true;
		//			// turn this on when ready
		//			GRID.Refresh();
		//			frmRECLStatus.InstancePtr.Refresh();
		//			strGroupComment = modGlobalFunctions.ShowGroupMessage(ref lngGroupNumber);
		//			// lngGroupNumber was set before with the getgroupnumber function
		//			if (Strings.Trim(strGroupComment) != "")
		//				FCMessageBox.Show(strGroupComment, MsgBoxStyle.Information, "Group " + FCConvert.ToString(lngGroupNumber) + " Message");
		//			StartUp = true;
		//		}
		//		else
		//		{
		//			// if this account does not exist then go back to the get account screen
		//			StartUp = false;
		//		}
		//		return StartUp;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Startup");
		//	}
		//	return StartUp;
		//}

		//private void CreateMasterLine(ref int BNum)
		//{
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// this creates the first line for each year
		//		clsDRWrapper rsRK = new clsDRWrapper();
		//		int lngRK;
		//		lngRK = FCConvert.ToInt32(rsCL.Get_Fields_Int32("Ratekey"));
		//		if (lngRK == 0)
		//		{
		//			if (rsLR.Name() != "")
		//			{
		//				if (!rsLR.EndOfFile())
		//				{
		//					lngRK = FCConvert.ToInt32(rsLR.Get_Fields_Int32("RateKey"));
		//				}
		//				else
		//				{
		//					lngRK = 0;
		//				}
		//			}
		//		}
		//		if (lngRK != 0)
		//		{
		//			rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(lngRK), modExtraModules.strCLDatabase);
		//			if (rsRK.EndOfFile() || rsRK.BeginningOfFile())
		//			{
		//				OriginalBillDate = DateTime.Today;
		//				FCMessageBox.Show("No rate record has been defined for the billing year " + modExtraModules.FormatYear(FCConvert.ToString(rsCL.Get_Fields_Int32("BillingYear"))) + ".", MsgBoxStyle.Information, "Rate Record Error");
		//			}
		//			else
		//			{
		//				rsRK.MoveFirst();
		//				OriginalBillDate = FCConvert.ToDateTime(Strings.Format(rsRK.Get_Fields_DateTime("BillingDate"), "MM/dd/yy"));
		//			}
		//		}
		//		CurRow += 1;
		//		Add(CurRow, 0);
		//		GetDataFromRateTable();
		//		// interest
		//		GRID.TextMatrix(CurRow, lngColYear, modExtraModules.FormatYear(FCConvert.ToString(rsCL.Get_Fields_Int32("BillingYear"))));
		//		if (lngRK == 0)
		//		{
		//			GRID.TextMatrix(CurRow, lngColDate, "No RK");
		//		}
		//		else
		//		{
		//			GRID.TextMatrix(CurRow, lngColDate, Strings.Format(OriginalBillDate, "MM/dd/yy"));
		//		}
		//		GRID.TextMatrix(CurRow, lngColPending, " ");
		//		ParentLine = CurRow;
		//		// this sets the parent line so that I can easily get back to it from the child rows
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Master Line");
		//	}
		//}

		//private void FillGrid(ref int BNum)
		//{
		//	// VB6 Bad Scope Dim:
		//	double dblPerDiem = 0;
		//	// this sub will fill the subordinate rows with the billing/payment/lien information
		//	DateTime dateTemp = DateTime.FromOADate(0);
		//	bool boolTemp = false;
		//	string strName = "";
		//	int intCT/*unused?*/;
		//	holdyear = 0;
		//	curCurrentInterest = 0;
		//	if (FCConvert.ToInt32(rsCL.Get_Fields_Int32("BillingYear")) != 0)
		//	{
		//		if (FCConvert.ToInt32(rsCL.Get_Fields_Int32("BillingYear")) == holdyear)
		//		{
		//			holdyear = FCConvert.ToInt32(rsCL.Get_Fields_Int32("BillingYear"));
		//		}
		//		else
		//		{
		//			// this is the subordinate rows
		//			// check for a different owner and make a seperate row to show the old owner name
		//			if (Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("Name2"))) != "")
		//			{
		//				strName = Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("Name1")) + " & " + FCConvert.ToString(rsCL.Get_Fields_String("Name2")));
		//				// fill in the labels with the record information
		//			}
		//			else
		//			{
		//				strName = Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("Name1")));
		//			}
		//			if (Strings.Trim(strName) != Strings.Trim(lblOwnersName.Text))
		//			{
		//				CurRow += 1;
		//				Add(CurRow, 1);
		//				GRID.TextMatrix(CurRow, lngColReference, "Billed To:");
		//				GRID.MergeRow(CurRow, true, lngColCode, lngColCosts);
		//				//FC:FINAL:AM: merge the cells
		//				GRID.TextMatrix(CurRow, lngColCode, strName);
		//				GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, CurRow, lngColCode, CurRow, lngColCosts, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//				GRID.TextMatrix(CurRow, lngColPending, " ");
		//			}
		//			// Original Bill Line in grid
		//			holdyear = FCConvert.ToInt32(rsCL.Get_Fields_Int32("BillingYear"));
		//			if (holdyear % 10 == 1)
		//			{
		//				GRID.TextMatrix(ParentLine, lngColReference, "Original");
		//			}
		//			else
		//			{
		//				GRID.TextMatrix(ParentLine, lngColReference, strSupplemntalBillString);
		//			}
		//			GRID.TextMatrix(ParentLine, lngColPrincipal, FCConvert.ToDouble(rsCL.Get_Fields_Decimal("TaxDue1") + rsCL.Get_Fields_Decimal("TaxDue2") + rsCL.Get_Fields_Decimal("TaxDue3") + rsCL.Get_Fields_Decimal("TaxDue4")));
		//			// curPrincipalDue
		//			GRID.TextMatrix(ParentLine, lngColTotalAmount, (Conversion.Val(GRID.TextMatrix(ParentLine, lngColCosts)) + Conversion.Val(GRID.TextMatrix(ParentLine, lngColInterest)) + Conversion.Val(GRID.TextMatrix(ParentLine, lngColPrincipal))));
		//			GRID.TextMatrix(ParentLine, lngColGridCode, "+");
		//		}
		//		if (bcode == "LIEN")
		//		{
		//			// if this record is a lien, then show all of the Pre Lien payments first without showing the Lien line or the Lien payments
		//			if (PaymentRecords(FCConvert.ToInt32(rsCL.Get_Fields_Int32("ID")), false))
		//			{
		//				boolTemp = true;
		//			}
		//			else
		//			{
		//				if (boolNoCurrentInt)
		//				{
		//					// this means that the last payment was a "U" so no interest should accrue but that is why the function is coming out false
		//					boolTemp = true;
		//				}
		//				else
		//				{
		//					FCMessageBox.Show("An error has occured for BillingYear " + FCConvert.ToString(rsCL.Get_Fields_Int32("BillingYear")) + ".", MsgBoxStyle.Information, "Payment Record Error");
		//				}
		//			}
		//		}
		//		if (PaymentRecords(BNum, boolTemp))
		//		{
		//			// this writes all of the payment records
		//			// current interest line (if Needed)
		//			if (!boolNoCurrentInt)
		//			{
		//				boolNoCurrentInt = false;
		//				curCurrentInterest = modStatusPayments.DetermineInterest(ref rsCL, ref rsLR, ref bcode, ref curCosts, ref curPreLienInterest, ref curPrincipalDue, ref dateTemp, ref dblPerDiem);
		//			}
		//			else
		//			{
		//				if (dblLastTCInt == 0)
		//				{
		//					dblLastTCInt = FCConvert.ToDouble(modStatusPayments.DetermineInterest(ref rsCL, ref rsLR, ref bcode, ref curCosts, ref curPreLienInterest, ref curPrincipalDue, ref dateTemp, ref dblPerDiem));
		//				}
		//			}
		//			// rscl is a record from the BillingMaster table
		//			// rslien is a record from the LienRec table if needed
		//			GRID.TextMatrix(ParentLine, lngColPerDiem, FCConvert.ToString(dblPerDiem));
		//			dblTotalPerDiem += dblPerDiem;
		//			dblPerDiem = 0;
		//			modStatusPayments.Statics.dateOldDate[(holdyear) - modExtraModules.DEFAULTSUBTRACTIONVALUE] = dateTemp;
		//			// holds that last Interest Date in the grid so that the last payment can be reversed
		//		}
		//		else
		//		{
		//			// current interest line (if Needed)
		//			if (!boolNoCurrentInt)
		//			{
		//				boolNoCurrentInt = false;
		//				curCurrentInterest = modStatusPayments.DetermineInterest(ref rsCL, ref rsLR, ref bcode, ref curCosts, ref curPreLienInterest, ref curPrincipalDue, ref dateTemp, ref dblPerDiem);
		//			}
		//			else
		//			{
		//				if (dblLastTCInt == 0)
		//				{
		//					dblLastTCInt = FCConvert.ToDouble(modStatusPayments.DetermineInterest(ref rsCL, ref rsLR, ref bcode, ref curCosts, ref curPreLienInterest, ref curPrincipalDue, ref dateTemp, ref dblPerDiem));
		//				}
		//			}
		//			GRID.TextMatrix(ParentLine, lngColPerDiem, FCConvert.ToString(dblPerDiem));
		//			dblTotalPerDiem += dblPerDiem;
		//			dblPerDiem = 0;
		//		}
		//		modStatusPayments.Statics.dblCurrentInt[(holdyear) - modExtraModules.DEFAULTSUBTRACTIONVALUE] = FCConvert.ToDouble(curCurrentInterest);
		//		// this holds the current interest totals for later use
		//		if (curCurrentInterest != 0)
		//		{
		//			CurRow += 1;
		//			Add(CurRow, 1);
		//			curCurrentInterest *= -1;
		//			GRID.TextMatrix(CurRow, lngColYear, "");
		//			GRID.TextMatrix(CurRow, lngColDate, "");
		//			GRID.TextMatrix(CurRow, lngColPending, " ");
		//			if (curCurrentInterest < 0)
		//			{
		//				GRID.TextMatrix(CurRow, lngColReference, "CURINT");
		//			}
		//			else
		//			{
		//				GRID.TextMatrix(CurRow, lngColReference, "EARNINT");
		//			}
		//			GRID.TextMatrix(CurRow, lngColPrincipal, 0);
		//			GRID.TextMatrix(CurRow, lngColInterest, curCurrentInterest);
		//			GRID.TextMatrix(CurRow, lngColCosts, 0);
		//			GRID.TextMatrix(CurRow, lngColTotalAmount, (FCConvert.ToDouble(GRID.TextMatrix(CurRow, lngColCosts)) + FCConvert.ToDouble(GRID.TextMatrix(CurRow, lngColInterest)) + FCConvert.ToDouble(GRID.TextMatrix(CurRow, lngColPrincipal))));
		//			GRID.TextMatrix(CurRow, lngColGridCode, "-");
		//			// this is used to sum the cols - is for an increase in the bill amount...it is adding to the bill
		//		}
		//		// subtotals line - this will be shown on the master line when this line is not shown
		//		CurRow += 1;
		//		Add(CurRow, 1);
		//		// total line
		//		GRID.TextMatrix(CurRow, lngColPending, " ");
		//		Add(CurRow + 1, 1);
		//		// and spacer line to hold hidden values
		//		GRID.TextMatrix(CurRow + 1, lngColPending, " ");
		//		// subtotal format, bold and top border
		//		GRID.TextMatrix(CurRow, lngColGridCode, "=");
		//		GRID.TextMatrix(CurRow, lngColReference, "Total");
  //              //FC:FINAL:AM:#2091 - color the subtotals
  //              GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, CurRow, lngColReference + 1, CurRow, GRID.Cols - 1, Color.FromArgb(5, 204, 71));
		//		if (bcode == "LIEN")
		//		{
		//			GRID.TextMatrix(CurRow + 1, lngColDate, GRID.TextMatrix(CurRow + 1, lngColDate) + "*");
		//		}
		//		int l = 0;
		//		int sumRow;
		//		int j;
		//		double sum = 0;
		//		l = LastParentRow(CurRow);
		//		for (j = lngColPrincipal; j <= lngColTotalAmount; j++)
		//		{
		//			sum = 0;
		//			for (sumRow = l; sumRow <= CurRow - 1; sumRow++)
		//			{
		//				if (GRID.TextMatrix(sumRow, lngColGridCode) == "+")
		//				{
		//					sum = modGlobal.Round(sum + modGlobal.Round(Conversion.Val(GRID.TextMatrix(sumRow, j)), 2), 2);
		//				}
		//				else if (GRID.TextMatrix(sumRow, lngColGridCode) == "-")
		//				{
		//					sum = modGlobal.Round(sum - modGlobal.Round(Conversion.Val(GRID.TextMatrix(sumRow, j)), 2), 2);
		//				}
		//				else if (FCConvert.ToBoolean(GRID.TextMatrix(sumRow, lngColGridCode)) == true)
		//				{
		//					sum = modGlobal.Round(Conversion.Val(GRID.TextMatrix(sumRow, j)), 2);
		//				}
		//			}
		//			//bool Match/*unused?*/;
		//			GRID.TextMatrix(CurRow, j, Strings.Format(sum, "#,##0.00"));
		//			GRID.TextMatrix(CurRow + 1, j, Strings.Format(sum, "#,##0.00"));
		//		}
		//		// spacer line
		//		CurRow += 1;
		//		// puts the totals in the header line to start
		//		if (GRID.TextMatrix(ParentLine, lngColReference) == "Original" || GRID.TextMatrix(ParentLine, lngColReference) == strSupplemntalBillString)
		//		{
		//			SwapRows_8(ParentLine, NextSpacerLine(ParentLine));
		//		}
		//	}
		//	//FC:FINAL:SBE - #147 - pending flag should be bigger and bolder (requested by client)
		//	GRID.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 1, lngColPending, GRID.Rows - 1, lngColPending, 24F);
		//	GRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 1, lngColPending, GRID.Rows - 1, lngColPending, true);
		//	GRID.Select(0, 0);
		//}

		//private bool CompareCol(ref double GRID_Total, ref short GRID_Col, ref short yr)
		//{
		//	bool CompareCol = false;
		//	// this checks to see if the grid totals are the same as the recordset totals
		//	double dblTemp = 0;
		//	clsDRWrapper rsLTemp;
		//	clsDRWrapper rsTemp;
		//	rsTemp = new clsDRWrapper();
		//	if (modStatusPayments.Statics.boolRE)
		//	{
		//		rsTemp.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingYear = " + FCConvert.ToString(yr) + " BillingType = 'RE'", modExtraModules.strCLDatabase);
		//	}
		//	else
		//	{
		//		rsTemp.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingYear = " + FCConvert.ToString(yr) + " BillingType = 'PP'", modExtraModules.strCLDatabase);
		//	}
		//	if (((rsTemp.Get_Fields_Int32("LienRecordNumber"))) == 0)
		//	{
		//		// these are not liened
		//		switch (GRID_Col)
		//		{
		//			case 6:
		//				{
		//					// principal line
		//					dblTemp = modGlobal.Round((Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4"))) - Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid")), 2);
		//					break;
		//				}
		//			case 7:
		//				{
		//					// interest
		//					dblTemp = modGlobal.Round(FCConvert.ToDouble((-1 * rsTemp.Get_Fields_Decimal("InterestCharged")) - rsTemp.Get_Fields_Decimal("InterestPaid") - curCurrentInterest), 2);
		//					break;
		//				}
		//			case 8:
		//				{
		//					// costs
		//					dblTemp = modGlobal.Round(Conversion.Val(rsTemp.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsTemp.Get_Fields_Decimal("DemandFeesPaid")), 2);
		//					break;
		//				}
		//			case 9:
		//				{
		//					// totals
		//					dblTemp = modGlobal.Round(FCConvert.ToDouble(((rsTemp.Get_Fields_Decimal("TaxDue1") + rsTemp.Get_Fields_Decimal("TaxDue2") + rsTemp.Get_Fields_Decimal("TaxDue3") + rsTemp.Get_Fields_Decimal("TaxDue4")) - rsTemp.Get_Fields_Decimal("PrincipalPaid")) + (-1 * (rsTemp.Get_Fields_Decimal("InterestCharged")) - rsTemp.Get_Fields_Decimal("InterestPaid") - curCurrentInterest) + (rsTemp.Get_Fields_Decimal("DemandFees") - rsTemp.Get_Fields_Decimal("DemandFeesPaid"))), 2);
		//					break;
		//				}
		//		}
		//		//end switch
		//		// If yr = 1996 Then Stop
		//		if (Conversion.Val(GRID_Total) == Conversion.Val(dblTemp))
		//		{
		//			CompareCol = true;
		//		}
		//	}
		//	else
		//	{
		//		rsLTemp = new clsDRWrapper();
		//		rsLTemp.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsTemp.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
		//		// these are liened
		//		switch (GRID_Col)
		//		{
		//			case 6:
		//				{
		//					// principal line
		//					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//					dblTemp = modGlobal.Round(Conversion.Val(rsLTemp.Get_Fields("Principal")) - Conversion.Val(rsLTemp.Get_Fields_Decimal("PrincipalPaid")), 2);
		//					break;
		//				}
		//			case 7:
		//				{
		//					// interest
		//					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//					// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//					dblTemp = modGlobal.Round(FCConvert.ToDouble(Conversion.Val(rsLTemp.Get_Fields("Interest")) - Conversion.Val(rsLTemp.Get_Fields_Decimal("InterestCharged")) - Conversion.Val(rsLTemp.Get_Fields_Decimal("InterestPaid")) - Conversion.Val(rsLTemp.Get_Fields("PLIPaid")) - curCurrentInterest), 2);
		//					break;
		//				}
		//			case 8:
		//				{
		//					// costs
		//					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//					// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//					dblTemp = modGlobal.Round(Conversion.Val(rsLTemp.Get_Fields("Costs")) - Conversion.Val(rsLTemp.Get_Fields("MaturityFee")) - Conversion.Val(rsLTemp.Get_Fields_Decimal("CostsPaid")), 2);
		//					break;
		//				}
		//			case 9:
		//				{
		//					// totals
		//					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//					// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//					// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//					dblTemp = modGlobal.Round(FCConvert.ToDouble((Conversion.Val(rsLTemp.Get_Fields("Principal")) + Conversion.Val(rsLTemp.Get_Fields("Interest")) - (Conversion.Val(rsLTemp.Get_Fields_Decimal("InterestCharged")) + Conversion.Val(rsLTemp.Get_Fields("MaturityFee"))) + Conversion.Val(rsLTemp.Get_Fields("Costs"))) - (Conversion.Val(rsLTemp.Get_Fields_Decimal("PrincipalPaid")) + Conversion.Val(rsLTemp.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsLTemp.Get_Fields("PLIPaid")) + Conversion.Val(rsLTemp.Get_Fields_Decimal("CostsPaid"))) - curCurrentInterest), 2);
		//					break;
		//				}
		//		}
		//		//end switch
		//		// If yr = 1996 Then Stop
		//		if (Conversion.Val(GRID_Total) == Conversion.Val(dblTemp))
		//		{
		//			CompareCol = true;
		//		}
		//	}
		//	return CompareCol;
		//}

		//private void FillLabels_2(int BK)
		//{
		//	FillLabels(ref BK);
		//}

		private void FillLabels(ref int BK)
		{
			// pass in the bill ID and the information is set
			// VBto upgrade warning: curHold As double	OnWriteFCConvert.ToDouble(
			decimal curHold;
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp = "";
			int lngAcct/*unused?*/;
			// sets the current information at the top of the screen
			if (modStatusPayments.Statics.boolRE)
			{
				strTemp = "SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE ID = " + FCConvert.ToString(BK) + " AND BillingType = 'RE' ORDER BY BillingYear Desc";
			}
			else
			{
				strTemp = "SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE ID = " + FCConvert.ToString(BK) + " AND BillingType = 'PP' ORDER BY BillingYear Desc";
			}
			rsTemp.OpenRecordset(strTemp, modExtraModules.strCLDatabase);
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				if (modStatusPayments.Statics.boolRE)
				{
					lblLandValue2.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("LandValue")), "#,##0");
					lblBuildingValue2.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("BuildingValue")), "#,##0");
					lblExemptValue2.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("ExemptValue")), "#,##0");
					lblYear.Text = FCConvert.ToString(Conversion.Val(modExtraModules.FormatYear(FCConvert.ToString(rsTemp.Get_Fields_Int32("BillingYear")))));
					curHold = FCConvert.ToDecimal(Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4")));
					lblTotalValue2.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("LandValue")) + Conversion.Val(rsTemp.Get_Fields_Int32("BuildingValue")) - Conversion.Val(rsTemp.Get_Fields_Int32("ExemptValue")), "#,##0");
					lblTotalValue2.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("LandValue")) + Conversion.Val(rsTemp.Get_Fields_Int32("BuildingValue")) - Conversion.Val(rsTemp.Get_Fields_Int32("ExemptValue")), "#,##0");
				}
				else
				{
					vsPPCategory.TextMatrix(0, 2, FCConvert.ToString(Conversion.Val(modExtraModules.FormatYear(FCConvert.ToString(rsTemp.Get_Fields_Int32("BillingYear"))))));
					vsPPCategory.TextMatrix(1, 2, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category1"))), "#,##0"));
					vsPPCategory.TextMatrix(2, 2, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category2"))), "#,##0"));
					vsPPCategory.TextMatrix(3, 2, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category3"))), "#,##0"));
					vsPPCategory.TextMatrix(4, 2, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category4"))), "#,##0"));
					vsPPCategory.TextMatrix(5, 2, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category5"))), "#,##0"));
					vsPPCategory.TextMatrix(6, 2, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category6"))), "#,##0"));
					vsPPCategory.TextMatrix(7, 2, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category7"))), "#,##0"));
					vsPPCategory.TextMatrix(8, 2, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category8"))), "#,##0"));
					vsPPCategory.TextMatrix(9, 2, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category9"))), "#,##0"));
					vsPPCategory.TextMatrix(10, 2, Strings.Format(FCConvert.ToString(Conversion.Val(Conversion.Val(rsTemp.Get_Fields_Int32("Category1")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category2")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category3")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category4")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category5")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category6")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category7")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category8")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category9")))), "#,##0.00"));
					curHold = FCConvert.ToDecimal(Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4")));
				}
			}
			else
			{
				// Stop
			}
		}

		private void GetDataFromRateTable()
		{
			clsDRWrapper rsRT = new clsDRWrapper();
			if (bcode == "LIEN")
			{
				strSQL = "SELECT * FROM RATEREC WHERE ID = " + FCConvert.ToString(rsLR.Get_Fields_Int32("Ratekey"));
			}
			else
			{
				strSQL = "SELECT * FROM RATEREC WHERE ID = " + FCConvert.ToString(rsCL.Get_Fields_Int32("Ratekey"));
			}
			// If rsCl.fields("BillingYear") = 1996 Then Stop
			rsRT.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (rsRT.EndOfFile() != true && rsRT.BeginningOfFile() != true)
			{
				if (FCUtils.IsNull(rsRT.Get_Fields("NumberOfPeriods")) == false)
					intNumberOfPeriods = FCConvert.ToInt32(rsRT.Get_Fields_Int16("NumberOfPeriods"));
				if (FCUtils.IsEmptyDateTime(rsRT.Get_Fields_DateTime("InterestStartDate1")) == false)
					modStatusPayments.Statics.strInterestDate1 = FCConvert.ToString(rsRT.Get_Fields_DateTime("InterestStartDate1"));
				if (FCUtils.IsEmptyDateTime(rsRT.Get_Fields_DateTime("InterestStartDate2")) == false)
					modStatusPayments.Statics.strInterestDate2 = FCConvert.ToString(rsRT.Get_Fields_DateTime("InterestStartDate2"));
				if (FCUtils.IsEmptyDateTime(rsRT.Get_Fields_DateTime("InterestStartDate3")) == false)
					modStatusPayments.Statics.strInterestDate3 = FCConvert.ToString(rsRT.Get_Fields_DateTime("InterestStartDate3"));
				if (FCUtils.IsEmptyDateTime(rsRT.Get_Fields_DateTime("InterestStartDate4")) == false)
					modStatusPayments.Statics.strInterestDate4 = FCConvert.ToString(rsRT.Get_Fields_DateTime("InterestStartDate4"));
				if (FCUtils.IsNull(rsRT.Get_Fields_Double("InterestRate")) == false)
				{
					modStatusPayments.Statics.dblInterestRate = Conversion.Val(rsRT.Get_Fields_Double("InterestRate"));
				}
			}
			else
			{
				modStatusPayments.Statics.strInterestDate1 = FCConvert.ToString(FCConvert.ToDateTime(0));
				modStatusPayments.Statics.strInterestDate2 = FCConvert.ToString(FCConvert.ToDateTime(0));
				modStatusPayments.Statics.strInterestDate3 = FCConvert.ToString(FCConvert.ToDateTime(0));
				modStatusPayments.Statics.strInterestDate4 = FCConvert.ToString(FCConvert.ToDateTime(0));
			}
			if (modStatusPayments.Statics.dblInterestRate == 0)
				modStatusPayments.Statics.dblInterestRate = modExtraModules.Statics.dblDefaultInterestRate;
		}

		private void Format_Grid()
		{
			// this sets the size of the grid control and the titles line
			int wid/*unused?*/;
			GRID.Cols = 16;
			GRID.Width = FCConvert.ToInt32(frmRECLStatus.InstancePtr.Width * 0.95);
			//GRID.Left = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Width - GRID.Width) / 5.0);
			GRID.Left = 30;
			//FC:FINAL:SBE - #118 - align payments grid with main grid, and change grid style
			this.vsPayments.Width = GRID.Width;
			this.txtComments.Width = (this.vsPayments.Left + this.vsPayments.Width) - this.txtComments.Left;
            //this.GRID.Columns[lngColTotalAmount - 1].DefaultCellStyle.ForeColor = Color.FromArgb(5, 204, 71);
			this.GRID.Columns[lngColTotalAmount - 1].DefaultCellStyle.Font = new Font("default", 16F, FontStyle.Bold, GraphicsUnit.Pixel);
			if (this.fraStatusLabels.Visible)
			{
				this.fraStatusLabels.Width = (this.GRID.Left + this.GRID.Width) - this.fraStatusLabels.Left;
			}
			if (boolPayment)
			{
				GRID.Height = FCConvert.ToInt32(frmRECLStatus.InstancePtr.ClientArea.Height * 0.5);
				GRID.TopOriginal = 250;
			}
			else
			{
				if (modStatusPayments.Statics.boolRE)
				{
					fraStatusLabels.Visible = true;
					GRID.Top = fraStatusLabels.Top + fraStatusLabels.Height;
				}
				else
				{
					fraPPStatusLabels.Visible = true;
					GRID.Top = fraPPStatusLabels.Top + fraPPStatusLabels.Height;
				}
				GRID.Height = FCConvert.ToInt32(Math.Abs(frmRECLStatus.InstancePtr.ClientArea.Height - GRID.Top - (frmRECLStatus.InstancePtr.ClientArea.Height * 0.09)));
			}
			GRID.Top += 30;
			GRID.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
			// column headers
			GRID.TextMatrix(0, lngColYear, "Year");
			// Billing Year
			GRID.TextMatrix(0, lngColDate, "Date");
			// Recorded Date
			GRID.TextMatrix(0, lngColReference, "Ref");
			// Reference
			GRID.TextMatrix(0, lngColPeriod, "P");
			// Period
			GRID.TextMatrix(0, lngColCode, "C");
			// Code
			GRID.TextMatrix(0, lngColPrincipal, "Principal");
			// Principal
			GRID.TextMatrix(0, lngColInterest, "Interest");
			// Interest
			GRID.TextMatrix(0, lngColCosts, "Costs");
			// Costs
			GRID.TextMatrix(0, lngColTotalAmount, "Total");
			// Total
			// 10                 '+ = Bill, - = Payment line, = = Subtotal line
			// 11                 'Payment ID
			// 12                 'CHGINT #
			// 13                 'Per Diem
			// 14                 'Pending *
			AutoSize_GRID();
			GRID.ColFormat(lngColDate, "MM/dd/yy");
			GRID.ColFormat(lngColPrincipal, "#,##0.00");
			GRID.ColFormat(lngColInterest, "#,##0.00");
			GRID.ColFormat(lngColCosts, "#,##0.00");
			GRID.ColFormat(lngColTotalAmount, "#,##0.00");
			GRID.ColAlignment(lngColPrincipal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GRID.ColAlignment(lngColInterest, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GRID.ColAlignment(lngColCosts, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GRID.ColAlignment(lngColTotalAmount, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GRID.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GRID.ColAlignment(14, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngColPrincipal, 0, lngColTotalAmount, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngColPeriod, 0, lngColCode, FCGrid.AlignmentSettings.flexAlignRightCenter);
			GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngColDate, 0, lngColDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			//FC:FINAL:AM: adjust the payment frame location
			fraPayment.Top = GRID.Bottom + 20;
			//FC:FINAL:DSE:#390 Adjust heights, so that controls would not be taller than parents
			fraPayment.Height = ClientArea.Height - fraPayment.Top - 10;
			vsPayments.Height = fraPayment.Height - vsPayments.Top;
			//FC:FINAL:DDU:#1937 - fix overlap problem
			fraPayment.Width = vsPayments.Width + vsPayments.Left + 10; ;
			vsPeriod.Width = lblTotalPendingDue.Left - 20 - vsPeriod.Left;
            //FC:FINAL:AM:#2045 - set the tooltips on the header
            GRID.Columns[lngColDate - GRID.FixedCols].HeaderCell.ToolTipText = "Recorded Date";
            string strTemp = "";
            strTemp += " 3 - 30 Day Notice Fee" + ", ";
            strTemp += " A - Abatement" + ", ";
            strTemp += " C - Correction" + ", ";
            strTemp += " D - Discount" + ", ";
            strTemp += " I - Interest" + ", ";
            strTemp += " P - Payment" + ", ";
            strTemp += " R - Refunded Abatement" + ", ";
            strTemp += " S - Supplemental" + ", ";
            strTemp += " U - Tax Club" + ", ";
            strTemp += " Y - PrePayment";
            GRID.Columns[lngColCode - GRID.FixedCols].HeaderCell.ToolTipText = strTemp;
        }

		private void Add(int Row, int lvl)
		{
			// this sub adds a row in the grid at position = row and .rowoutlinelevel = lvl
			// set the row as a group
			if (MaxRows == 0)
			{
				MaxRows = 2;
			}
			GRID.AddItem("", Row);
			// increments the number of rows
			MaxRows = GRID.Rows - 1;
			lvl += 1;
			if (lvl == 1)
			{
                //GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngColYear, Row, GRID.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
                // &H80000018
                GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, lngColTotalAmount, Color.FromArgb(5, 204, 71));
			}
			else if (lvl == 2)
			{
				GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, Row, lngColReference, Row, GRID.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
				//GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngColYear, Row, GRID.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
				GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngColYear, Row, GRID.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249)));
                // &H80000016
                //				GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, lngColYear, Row, 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                // &H80000016
			}
			else
			{
				//FC:FINAL:AM: don't set the backcolor
				//GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngColYear, Row, GRID.Cols - 1, System.Drawing.Color.White);
			}
			// set the indentation level of the group
			GRID.RowOutlineLevel(Row, FCConvert.ToInt16(lvl));
			if (lvl <= 1)
				GRID.IsSubtotal(Row, true);
		}

		private void AutoSize_GRID()
		{
			// sets the width of each column in order to fit in the grid without a scrollbar
			// the total if added should be slightly less then 1
			int lngTemp/*unused?*/;
			int I/*unused?*/;
			int wid = 0;
			wid = GRID.WidthOriginal;
			GRID.ColWidth(lngColYear, FCConvert.ToInt32(wid * 0.09));
			GRID.ColWidth(lngColDate, FCConvert.ToInt32(wid * 0.1));
			GRID.ColWidth(lngColReference, FCConvert.ToInt32(wid * 0.098));
			GRID.ColWidth(lngColPeriod, FCConvert.ToInt32(wid * 0.04));
			GRID.ColWidth(lngColCode, FCConvert.ToInt32(wid * 0.04));
			GRID.ColWidth(lngColPrincipal, FCConvert.ToInt32(wid * 0.14));
			GRID.ColWidth(lngColInterest, FCConvert.ToInt32(wid * 0.14));
			GRID.ColWidth(lngColCosts, FCConvert.ToInt32(wid * 0.13));
			GRID.ColWidth(lngColTotalAmount, FCConvert.ToInt32(wid * 0.15));
			GRID.ColWidth(lngColGridCode, 0);
			// Row code
			GRID.ColWidth(lngColPaymentID, 0);
			// Payment ID
			GRID.ColWidth(lngColCHGINTNumber, 0);
			// CHGINT Number
			GRID.ColWidth(lngColPerDiem, 0);
			// Per Diem in Master Line
			GRID.ColWidth(lngColComment, 0);
			// Comments for this payment
			//FC:FINAL:RPU #i12: Adjust width for colPending
			GRID.ColWidth(lngColPending, FCConvert.ToInt32(wid * 0.025));
			// * Col
			// .ColWidth(0) = .Width * 0.03
			// .ColWidth(1) = .Width * 0.072
			// .ColWidth(2) = .Width * 0.085
			// .ColWidth(3) = .Width * 0.11
			// .ColWidth(4) = .Width * 0.04
			// .ColWidth(5) = .Width * 0.045
			// .ColWidth(6) = .Width * 0.147
			// .ColWidth(7) = .Width * 0.145
			// .ColWidth(8) = .Width * 0.13
			// .ColWidth(9) = .Width * 0.15
			// .ColWidth(10) = 0   'Row Code
			// .ColWidth(11) = 0   'PaymentKey
			// .ColWidth(12) = 0   'CGHINT Number
			// .ColWidth(13) = 0   'Per Diem in master line
			// .ColWidth(14) = .Width * 0.005
		}
		//private void GRID_AfterCollapse(int Row, int State)
		//{
		//if (!blnInProgress)
		//{
		//if (Row == -1)
		//{
		//	// 0/1 buttons used
		//	switch (State)
		//	{
		//	// New state of node(s)
		//		case 0:
		//			{
		//				blnIsCollapsed = false;
		//				break;
		//			}
		//		case 1:
		//		case 2:
		//			{
		//				blnIsCollapsed = true;
		//				break;
		//			}
		//	}
		//	//end switch
		//}
		//else
		//{
		// Specific Node selected
		//switch (State)
		//{
		// New state of node(s)
		//	case 0:
		//		{
		//			blnIsCollapsed = true;
		//			break;
		//		}
		//	case 1:
		//	case 2:
		//		{
		//			blnIsCollapsed = false;
		//			break;
		//		}
		//}
		//end switch
		//		}
		//		lngCurrentGridRow = this.GRID.GetFlexRowIndex(e.RowIndex);
		//		GRID_Collapsed();
		//	}
		//}
		private void GRID_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			blnIsCollapsed = false;
			lngCurrentGridRow = this.GRID.GetFlexRowIndex(e.RowIndex);
			GRID_Collapsed();
		}

		private void GRID_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			blnIsCollapsed = true;
			lngCurrentGridRow = this.GRID.GetFlexRowIndex(e.RowIndex);
			GRID_Collapsed();
		}

		private void Grid_Click(object sender, EventArgs e)
		{
			// this will set the year information to the year being selected
			int cRow;
			mnuFileReprintReceipt.Visible = false;
			if (GRID.Col == lngColYear && GRID.RowOutlineLevel(GRID.Row) == 1)
			{
				for (cRow = 0; cRow <= cmbYear.Items.Count - 1; cRow++)
				{
					if (Strings.Trim(cmbYear.Items[cRow].ToString().Replace("*", "")) == Strings.Trim(GRID.TextMatrix(GRID.Row, GRID.Col)))
					{
						cmbYear.SelectedIndex = cRow;
						break;
					}
				}
			}
			else if (GRID.RowOutlineLevel(GRID.Row) == 2 && !modGlobalConstants.Statics.gboolCR)
			{
				// this is a payment line
				if (Conversion.Val(GRID.TextMatrix(GRID.Row, 11)) != 0 && Strings.Trim(GRID.TextMatrix(GRID.Row, lngColCode)) != "I")
				{
					mnuFileReprintReceipt.Visible = true;
				}
			}
		}

		//private void GRID_Collapsed()
		//{
		//	// this sub occurs when a gridline is collapsed or expanded
		//	// it will change the data in the main rows depending upon its status
		//	int temp/*unused?*/;
		//	int counter;
		//	int rows = 0;
		//	int height1 = 0;
		//	bool DeptFlag/*unused?*/;
		//	bool DivisionFlag/*unused?*/;
		//	int cRow;
		//	int lngTempRow;
		//	// this will set the year information to the year being selected
		//	//lngTempRow = GRID.Row;
		//	lngTempRow = lngCurrentGridRow;
		//	if (GRID.RowOutlineLevel(lngTempRow) == 1)
		//	{
		//		for (cRow = 0; cRow <= cmbYear.Items.Count - 1; cRow++)
		//		{
		//			// set the combo box
		//			if (Strings.Trim(cmbYear.Items[cRow].ToString()) == Strings.Trim(GRID.TextMatrix(lngTempRow, lngColYear)))
		//			{
		//				cmbYear.SelectedIndex = cRow;
		//				break;
		//			}
		//		}
		//	}
		//	if (CollapseFlag == false)
		//	{
		//		blnInProgress = true;
		//		// Loop through all rows
		//		if (lngCurrentGridRow == -1)
		//		{
		//			for (counter = 1; counter <= GRID.Rows - 1; counter++)
		//			{
		//				// MAL@20071115: Rework the way this works to avoid inconsistency
		//				if (!blnIsCollapsed)
		//				{
		//					// Needs to be collapsed
		//					rows += 1;
		//					if (GRID.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineExpanded)
		//					{
		//						GRID.IsCollapsed(counter, FCGrid.CollapsedSettings.flexOutlineCollapsed);
		//					}
		//					if (GRID.RowOutlineLevel(counter) == 1)
		//					{
		//						if (GRID.TextMatrix(counter, lngColReference) == "Original" || GRID.TextMatrix(counter, lngColReference) == strSupplemntalBillString)
		//						{
		//							SwapRows_8(counter, NextSpacerLine(counter));
		//						}
		//					}
		//				}
		//				else
		//				{
		//					rows += 1;
		//					if (GRID.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
		//					{
		//						GRID.IsCollapsed(counter, FCGrid.CollapsedSettings.flexOutlineExpanded);
		//					}
		//					if (GRID.RowOutlineLevel(counter) == 1)
		//					{
		//						if (GRID.TextMatrix(counter, lngColReference) != "Original" || GRID.TextMatrix(counter, lngColReference) != strSupplemntalBillString)
		//						{
		//							SwapRows_8(counter, NextSpacerLine(counter));
		//						}
		//					}
		//				}
		//			}
		//		}
		//		else
		//		{
		//			for (counter = 1; counter <= GRID.Rows - 1; counter++)
		//			{
		//				// MAL@20071115: Rework the way this works to avoid inconsistency
		//				if (!blnIsCollapsed && counter == lngCurrentGridRow)
		//				{
		//					// Needs to be collapsed
		//					rows += 1;
		//					if (GRID.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineExpanded)
		//					{
		//						GRID.IsCollapsed(counter, FCGrid.CollapsedSettings.flexOutlineCollapsed);
		//					}
		//					if (GRID.RowOutlineLevel(counter) == 1)
		//					{
		//						if (GRID.TextMatrix(counter, lngColReference) == "Original" || GRID.TextMatrix(counter, lngColReference) == strSupplemntalBillString)
		//						{
		//							SwapRows_8(counter, NextSpacerLine(counter));
		//						}
		//					}
		//				}
		//				else
		//				{
		//					if (counter == lngCurrentGridRow)
		//					{
		//						rows += 1;
		//						if (GRID.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
		//						{
		//							GRID.IsCollapsed(counter, FCGrid.CollapsedSettings.flexOutlineExpanded);
		//						}
		//						if (GRID.RowOutlineLevel(counter) == 1)
		//						{
		//							if (GRID.TextMatrix(counter, lngColReference) != "Original" || GRID.TextMatrix(counter, lngColReference) != strSupplemntalBillString)
		//							{
		//								SwapRows_8(counter, NextSpacerLine(counter));
		//							}
		//						}
		//					}
		//					else
		//					{
		//						rows += 1;
		//					}
		//				}
		//			}
		//		}
		//		height1 = (rows + 1) * GRID.RowHeight(0);
		//		Adjust_GRID_Height(height1);
		//		blnInProgress = false;
		//		lngCurrentGridRow = 0;
		//	}
			
		//}

		//private void Adjust_GRID_Height(int ht)
		//{
		//}

		//private void Grid_DblClick(object sender, EventArgs e)
		//{
		//	int cRow = 0;
		//	cRow = GRID.Row;
		//	if (cRow > 0)
		//	{
		//		if (GRID.RowOutlineLevel(cRow) == 1)
		//		{
		//			if (GRID.Col == lngColTotalAmount || GRID.Col == lngColPerDiem)
		//			{
		//				// do nothing
		//			}
		//			else if (GRID.Col == lngColYear)
		//			{
		//				// set the year
		//				for (cRow = 0; cRow <= cmbYear.Items.Count - 1; cRow++)
		//				{
		//					if (Strings.Trim(cmbYear.Items[cRow].ToString()) == Strings.Trim(GRID.TextMatrix(GRID.Row, GRID.Col)))
		//					{
		//						cmbYear.SelectedIndex = cRow;
		//						break;
		//					}
		//				}
		//			}
		//			// expand the rows
		//			if (GRID.IsCollapsed(GRID.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
		//			{
		//				GRID.IsCollapsed(GRID.Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
		//			}
		//			else
		//			{
		//				GRID.IsCollapsed(GRID.Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
		//			}
		//		}
		//		else if (GRID.RowOutlineLevel(cRow) == 2 && fraPayment.Visible == true && (GRID.TextMatrix(cRow, lngColReference) != "CHGINT" && GRID.TextMatrix(cRow, lngColReference) != "EARNINT" && GRID.TextMatrix(cRow, lngColReference) != "CNVRSN" && GRID.TextMatrix(cRow, lngColReference) != "INTEREST"))
		//		{
		//			// reverse the payment on this line
		//			if (GRID.TextMatrix(cRow, lngColReference) != "Billed To:" && Strings.Trim(GRID.TextMatrix(cRow, lngColCode)) != "I")
		//			{
		//				// make sure it is not an interest payment and not a name row
		//				if (Strings.Trim(GRID.TextMatrix(cRow, lngColReference)) != "Total" && Strings.Trim(GRID.TextMatrix(cRow, lngColReference)) != "CURINT" && Strings.Trim(GRID.TextMatrix(cRow, lngColReference)) != "Liened")
		//				{
		//					// And Not boolOngoingReversal Then
		//					if (Strings.Trim(GRID.TextMatrix(cRow, lngColCode)) == "A" || Strings.Trim(GRID.TextMatrix(cRow, lngColCode)) == "R")
		//					{
		//						FCMessageBox.Show("Please use a correction or negative abatement to manually reverse an abatement.", MsgBoxStyle.Exclamation, "Abatement Reversal");
		//					}
		//					else if (Strings.Trim(GRID.TextMatrix(cRow, lngColCode)) != "D")
		//					{
		//						if (FCMessageBox.Show("Are you sure that you would like to reverse this payment?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Reverse Payment") == DialogResult.Yes)
		//						{
		//							modStatusPayments.CreateOppositionLine(cRow, true);
		//						}
		//					}
		//					else
		//					{
		//						// reversing a discount
		//						if (FCMessageBox.Show("Are you sure that you would like to reverse this discount?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Reverse Payment") == DialogResult.Yes)
		//						{
		//							modStatusPayments.CreateOppositionLine(cRow, true);
		//						}
		//					}
		//				}
		//				else if (Strings.Trim(GRID.TextMatrix(cRow, lngColReference)) == "Liened")
		//				{
		//					if (boolDisableInsert)
		//					{
		//						// kk03132015  trocrs-36  Add option to disable automatically filling in payment amount
		//						return;
		//					}
		//					modStatusPayments.CreateOppositionLine(cRow, true);
		//					txtReference.Text = "";
		//				}
		//				else
		//				{
		//					if (boolDisableInsert)
		//					{
		//						// kk03132015  trocrs-36  Add option to disable automatically filling in payment amount
		//						return;
		//					}
		//					modStatusPayments.CreateOppositionLine(cRow);
		//					txtReference.Text = "";
		//				}
		//			}
		//		}
		//		else if (GRID.RowOutlineLevel(cRow) == 0)
		//		{
		//			// this is the total account line
		//			if (boolDisableInsert)
		//			{
		//				// kk03132015  trocrs-36  Add option to disable automatically filling in payment amount
		//				return;
		//			}
		//			modStatusPayments.CreateOppositionLine(cRow);
		//		}
		//	}
		//}

		//private void GRID_GotFocus()
		//{
		//	if (fraRateInfo.Visible == true)
		//		fraRateInfo.Visible = false;
		//	mnuFilePrintRateInfo.Visible = fraRateInfo.Visible;
		//}

		//private void GRID_KeyPress(object sender, KeyPressEventArgs e)
		//{
		//	int KeyAscii = Strings.Asc(e.KeyChar);
		//	if (KeyAscii == 32)
		//	{
		//		// spacebar
		//		KeyAscii = 0;
		//		Grid_DblClick(GRID, EventArgs.Empty);
		//	}
		//	else if (KeyAscii == 13 && GRID.RowOutlineLevel(GRID.Row) == 1)
		//	{
		//		KeyAscii = 0;
		//		Grid_Click(GRID, EventArgs.Empty);
		//	}
		//}

		private void GRID_MouseDown(object sender, MouseEventArgs e)
		{
			// catch the right click to show the rate information about that year
			int lngMRow;
			lngMRow = GRID.MouseRow;
			if (e.Button == MouseButtons.Right && lngMRow > 0)
			{
				// right click
				if (GRID.RowOutlineLevel(lngMRow) == 1)
				{
					GRID.Select(lngMRow, lngColYear);
					// this will force the information to be correct by selecting the correct row before looking at the labels
					if (modStatusPayments.Statics.boolRE)
					{
						rsCL.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingYear = " + modExtraModules.FormatYear(GRID.TextMatrix(lngMRow, lngColYear)) + "  AND BillingType = 'RE'", modExtraModules.strCLDatabase);
					}
					else
					{
						rsCL.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingYear = " + modExtraModules.FormatYear(GRID.TextMatrix(lngMRow, lngColYear)) + "  AND BillingType = 'PP'", modExtraModules.strCLDatabase);
					}
					if (rsCL.EndOfFile() != true && rsCL.BeginningOfFile() != true)
					{
						ShowRateInfo(ref lngMRow, ref lngColYear, ref rsCL);
					}
				}
				else if (GRID.RowOutlineLevel(lngMRow) == 2)
				{
					ShowPaymentInfo(lngMRow, GRID.MouseCol);
				}
				mnuFilePrintRateInfo.Visible = fraRateInfo.Visible;
			}
		}

        //FC:FINAL:AM:#2047 - set the tooltip on each cell
        //private void GRID_MouseMove(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //	// as the mouse moves over the grid, the tooltip will change depending on which cell the pointer is currently in
        //	string strTemp = "";
        //	int lngMC;
        //	int lngMR;
        //	lngMR = GRID.MouseRow;
        //	lngMC = GRID.MouseCol;
        //	if (lngMR == 0)
        //	{
        //		if (lngMC == lngColYear)
        //		{
        //			// Year
        //			strTemp = "";
        //			ToolTip1.SetToolTip(GRID, strTemp);
        //		}
        //		else if (lngMR == lngColDate)
        //		{
        //			// Date
        //			strTemp = "Recorded Date";
        //			ToolTip1.SetToolTip(GRID, strTemp);
        //		}
        //		else if (lngMR == lngColReference)
        //		{
        //			// Reference
        //			strTemp = "";
        //			ToolTip1.SetToolTip(GRID, strTemp);
        //		}
        //		else if (lngMR == lngColPeriod)
        //		{
        //			// Period
        //			strTemp = "";
        //			ToolTip1.SetToolTip(GRID, strTemp);
        //		}
        //		else if (lngMR == lngColCode)
        //		{
        //			// Code
        //			strTemp += " 3 - 30 Day Notice Fee" + ", ";
        //			strTemp += " A - Abatement" + ", ";
        //			strTemp += " C - Correction" + ", ";
        //			strTemp += " D - Discount" + ", ";
        //			strTemp += " I - Interest" + ", ";
        //			strTemp += " P - Payment" + ", ";
        //			strTemp += " R - Refunded Abatement" + ", ";
        //			strTemp += " S - Supplemental" + ", ";
        //			strTemp += " U - Tax Club" + ", ";
        //			strTemp += " Y - PrePayment";
        //			ToolTip1.SetToolTip(GRID, strTemp);
        //		}
        //		else if (lngMR == lngColPrincipal)
        //		{
        //			// Prin
        //			strTemp = "";
        //			ToolTip1.SetToolTip(GRID, strTemp);
        //		}
        //		else if (lngMR == lngColInterest)
        //		{
        //			// Int
        //			strTemp = "";
        //			ToolTip1.SetToolTip(GRID, strTemp);
        //		}
        //		else if (lngMR == lngColCosts)
        //		{
        //			// Costs
        //			strTemp = "";
        //			ToolTip1.SetToolTip(GRID, strTemp);
        //		}
        //		else if (lngMR == lngColTotalAmount)
        //		{
        //			// Total
        //			strTemp = "";
        //			ToolTip1.SetToolTip(GRID, strTemp);
        //		}
        //		else
        //		{
        //			strTemp = "";
        //			ToolTip1.SetToolTip(GRID, strTemp);
        //		}
        //	}
        //	else
        //	{
        //		if (lngMR > 0)
        //		{
        //			if (GRID.TextMatrix(lngMR, lngColGridCode) != "" && GRID.TextMatrix(lngMR, lngColGridCode) != "=")
        //			{
        //				if (GRID.TextMatrix(lngMR, lngColReference) != "CURINT" && GRID.TextMatrix(lngMR, lngColReference) != "EARNINT")
        //				{
        //					if (GRID.RowOutlineLevel(lngMR) == 1 && lngMC == 1)
        //					{
        //						ToolTip1.SetToolTip(GRID, "Right-Click for Rate information and Period Totals.");
        //					}
        //					else if (GRID.RowOutlineLevel(lngMR) == 2)
        //					{
        //						ToolTip1.SetToolTip(GRID, "Right-Click for Payment information.");
        //					}
        //					else
        //					{
        //						ToolTip1.SetToolTip(GRID, "");
        //					}
        //				}
        //				else
        //				{
        //					ToolTip1.SetToolTip(GRID, "");
        //				}
        //			}
        //			else
        //			{
        //				ToolTip1.SetToolTip(GRID, "");
        //			}
        //		}
        //		else
        //		{
        //			ToolTip1.SetToolTip(GRID, "");
        //		}
        //	}
        //}

        private void GRID_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            string strTemp = "";
            int lngMC;
            int lngMR;
            lngMR = GRID.GetFlexRowIndex(e.RowIndex);
            lngMC = GRID.GetFlexColIndex(e.ColumnIndex);

            if (GRID.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                DataGridViewCell cell = GRID[e.ColumnIndex, e.RowIndex];
                if (GRID.TextMatrix(lngMR, lngColGridCode) != "" && GRID.TextMatrix(lngMR, lngColGridCode) != "=")
                {
                    if (GRID.TextMatrix(lngMR, lngColReference) != "CURINT" && GRID.TextMatrix(lngMR, lngColReference) != "EARNINT")
                    {
                        if (GRID.RowOutlineLevel(lngMR) == 1 && lngMC == 1)
                        {
                            cell.ToolTipText = "Right-Click for Rate information and Period Totals.";
                        }
                        else if (GRID.RowOutlineLevel(lngMR) == 2)
                        {
                            cell.ToolTipText = "Right-Click for Payment information.";
                        }
                        else
                        {
                            cell.ToolTipText = "";
                        }
                    }
                    else
                    {
                        cell.ToolTipText = "";
                    }
                }
                else
                {
                    cell.ToolTipText = "";
                }
            }
        }

  //      private void Grid_RowColChange(object sender, EventArgs e)
		//{
		//	// this will change the labels depending apon which line is click
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	if (GRID.RowOutlineLevel(GRID.Row) == 1)
		//	{
		//		if (modStatusPayments.Statics.boolRE)
		//		{
		//			rsTemp.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE BillingYear = " + modExtraModules.FormatYear(GRID.TextMatrix(GRID.Row, lngColYear)) + " AND Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingType = 'RE'", modExtraModules.strCLDatabase);
		//		}
		//		else
		//		{
		//			rsTemp.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE BillingYear = " + modExtraModules.FormatYear(GRID.TextMatrix(GRID.Row, lngColYear)) + " AND Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingType = 'PP'", modExtraModules.strCLDatabase);
		//		}
		//		if (rsTemp.EndOfFile() || rsTemp.BeginningOfFile())
		//		{
		//		}
		//		else
		//		{
		//			rsTemp.MoveFirst();
		//			FillLabels_2(FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID")));
		//		}
		//	}
		//}
		// VBto upgrade warning: CurRow As Variant --> As int
		public int LastParentRow(int CurRow)
		{
			int LastParentRow = 0;
			int l;
			int curLvl;
			l = CurRow;
			curLvl = GRID.RowOutlineLevel(l);
			l -= 1;
			if (l > 0)
			{
				while (GRID.RowOutlineLevel(l) > 1)
				{
					l -= 1;
				}
				LastParentRow = l;
			}
			return LastParentRow;
		}

		private void FillMainLabels()
		{
			// from the bill record then from the RE account
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp = "";
			string strTemp2 = "";
			int intCT;
			
			// sets the current information at the top of the screen
			if (modStatusPayments.Statics.boolRE)
			{
				// Real Estate - This is the primary association that indicates that RE & PP where billed together
				rsTemp.OpenRecordset("SELECT * FROM ModuleAssociation WHERE Module = 'PP' AND PrimaryAssociate = 1 AND REMasterAcct = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE), "CentralData");
				if (!rsTemp.EndOfFile())
				{
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					dblAssocAccountTotal = modCLCalculations.CalculateAccountTotal(FCConvert.ToInt32(rsTemp.Get_Fields("Account")), false);
					if (boolPayment)
					{
						// lblAssociatedAccount.Top = lblPaymentInfo.Top
						// lblAssociatedAccount.Left = imgNote.Left + imgNote.Width + 70
						lblAssocAcct_Pmt.Text = "PP";
						lblAssocAcct_Pmt.Visible = true;
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						ToolTip1.SetToolTip(lblAssocAcct_Pmt, "Associated PP Account: " + FCConvert.ToString(rsTemp.Get_Fields("Account")) + "  Amount Outstanding: " + Strings.Format(dblAssocAccountTotal, "$#,##0.00"));
					}
					else
					{
						// lblAssociatedAccount.Top = imgNote.Top + imgNote.Height + 70
						// lblAssociatedAccount.Left = imgNote.Left
						lblAssocAcct.Text = "PP";
						lblAssocAcct.Visible = true;
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						ToolTip1.SetToolTip(lblAssocAcct, "Associated PP Account: " + FCConvert.ToString(rsTemp.Get_Fields("Account")) + "  Amount Outstanding: " + Strings.Format(dblAssocAccountTotal, "$#,##0.00"));
					}
				}
				else
				{
					lblAssocAcct_Pmt.Visible = false;
					ToolTip1.SetToolTip(lblAssocAcct_Pmt, "");
					lblAssocAcct.Visible = false;
					ToolTip1.SetToolTip(lblAssocAcct, "");
				}
				
				strTemp = "SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE ID = " + FCConvert.ToString(rsCL.Get_Fields_Int32("ID")) + " AND BillingType = 'RE' ORDER BY BillingYear Desc";
				lblLand.Text = "Land";
				rsTemp.OpenRecordset("SELECT SUM(CAST(LastLandVal AS BIGINT)) AS Land, SUM(CAST(LastBLDGVal AS BIGINT)) AS Building, SUM(CAST(RLExemption AS BIGINT)) AS Exempt FROM Master WHERE RSAccount = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " GROUP BY RSAccount", modExtraModules.strREDatabase);
				if (!rsTemp.EndOfFile())
				{
					lblLandValue.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("Land")), "#,##0");
					lblBuildingValue.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("Building")), "#,##0");
					lblExemptValue.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("Exempt")), "#,##0");
					lblTotalValue.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("Land")) + Conversion.Val(rsTemp.Get_Fields_Int32("Building")) - Conversion.Val(rsTemp.Get_Fields_Int32("Exempt")), "#,##0");
				}
				else
				{
					rsTemp.OpenRecordset(strTemp, modExtraModules.strCLDatabase);
					if (!(rsTemp.EndOfFile()) && !(rsTemp.BeginningOfFile()))
					{
						lblLandValue.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("LandValue")), "#,###");
						lblBuildingValue.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("BuildingValue")), "#,###");
						lblExemptValue.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("ExemptValue")), "#,###");
						lblTotalValue.Text = Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("LandValue")) + Conversion.Val(rsTemp.Get_Fields_Int32("BuildingValue")) - Conversion.Val(rsTemp.Get_Fields_Int32("ExemptValue")), "#,###");
					}
				}
				// RE Account Info				
				strTemp = "SELECT RSREF1, RITranCode, TaxAcquired, RSDeleted, Address1, Address2, City, State, Zip FROM Master INNER JOIN " + modGlobal.Statics.strDbCP + "Addresses ON Master.OwnerPartyID = Addresses.PartyID WHERE RSACCOUNT = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND RSCard = 1";
				if (rsTemp.OpenRecordset(strTemp, modExtraModules.strREDatabase))
				{
					if (!rsTemp.EndOfFile())
					{
						lblReference1.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSREF1")) + " ");
						// FIX THIS!!!  this should be from the BookPage Field --> Corey needs to write a converter for RE to put the Ref field in Bookpage
						strMailingAddress1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address1"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address2")));
						strMailingAddress2 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("City"))) + ", " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("State"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Zip")));
						if (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("TaxAcquired")))
						{
							lblTaxAcquired.Visible = true;
							boolTaxAcquired = true;
						}
						else
						{
							lblTaxAcquired.Visible = false;
							boolTaxAcquired = false;
						}
						// MAL@20071011: Correct placement for label
						// Program Bug Reference: 6287
						// If boolPayment Then
						lblTaxAcquired.Top = 0;
						// Else
						// lblTaxAcquired.Top = 420
						// End If
						boolDeleted = FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("RSDeleted"));
						lngCLResCode = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields_Int32("RITranCode"))));
						// for the residence differences
					}
				}
				strTemp = "SELECT SUM(PIACRES) as Acres FROM Master WHERE RSAccount = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE);
				if (rsTemp.OpenRecordset(strTemp, modExtraModules.strREDatabase))
				{
					if (!(rsTemp.EndOfFile()) && !(rsTemp.BeginningOfFile()))
					{
						lblAcreage.Text = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_Double("Acres")) + " ");
					}
				}
				strTemp = "SELECT * FROM BookPage WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND Card = 1 ORDER BY Line desc";
				if (rsTemp.OpenRecordset(strTemp, modExtraModules.strREDatabase))
				{
					if (rsTemp.EndOfFile())
					{
						strCurrentBookPage = "Not Found";
					}
					else
					{
						// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
						strCurrentBookPage = "B" + FCConvert.ToString(rsTemp.Get_Fields("Book")) + " P" + FCConvert.ToString(rsTemp.Get_Fields("Page"));
					}
				}
			}
			else
			{
				// Personal Property
				strCurrentBookPage = "";
				lblTaxAcquired.Visible = false;
				boolTaxAcquired = false;
				// format the grid
				vsPPCategory.Rows = 11;
				vsPPCategory.RowHeight(-1, 300);
				// format the size of columns ect
				vsPPCategory.ColWidth(0, FCConvert.ToInt32(vsPPCategory.WidthOriginal * 0.41));
				vsPPCategory.ColWidth(1, FCConvert.ToInt32(vsPPCategory.WidthOriginal * 0.28));
				vsPPCategory.ColWidth(2, FCConvert.ToInt32(vsPPCategory.WidthOriginal * 0.28));
				vsPPCategory.ExtendLastCol = true;
				vsPPCategory.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsPPCategory.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsPPCategory.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
				vsPPCategory.BorderStyle = BorderStyle.None;
				//vsPPCategory.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, vsPPCategory.Rows - 1, vsPPCategory.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				// headings
				vsPPCategory.TextMatrix(0, 0, "Category");
				vsPPCategory.TextMatrix(0, 1, "Current");
				vsPPCategory.TextMatrix(0, 2, "Year");
				// category 1-9
				// load the category names
				rsTemp.OpenRecordset("SELECT * FROM RatioTrends", modExtraModules.strPPDatabase);
				if (!rsTemp.EndOfFile())
				{
					for (intCT = 1; intCT <= 9; intCT++)
					{
						rsTemp.FindFirstRecord("Type", intCT);
						if (!rsTemp.NoMatch)
						{
							vsPPCategory.TextMatrix(intCT, 0, FCConvert.ToString(rsTemp.Get_Fields_String("Description")));
						}
						else
						{
							vsPPCategory.TextMatrix(intCT, 0, "Category " + FCConvert.ToString(intCT));
						}
					}
				}
				else
				{
					vsPPCategory.TextMatrix(1, 0, "Category 1");
					vsPPCategory.TextMatrix(2, 0, "Category 2");
					vsPPCategory.TextMatrix(3, 0, "Category 3");
					vsPPCategory.TextMatrix(4, 0, "Category 4");
					vsPPCategory.TextMatrix(5, 0, "Category 5");
					vsPPCategory.TextMatrix(6, 0, "Category 6");
					vsPPCategory.TextMatrix(7, 0, "Category 7");
					vsPPCategory.TextMatrix(8, 0, "Category 8");
					vsPPCategory.TextMatrix(9, 0, "Category 9");
				}
				// total line
				vsPPCategory.TextMatrix(10, 0, "Total");
				// make some of the lines bold
				vsPPCategory.Select(0, 1, 0, 2);
				vsPPCategory.CellBorder(Color.Black, 0, 0, 0, 1, 0, 1);
				vsPPCategory.Select(9, 1, 9, 2);
				vsPPCategory.CellBorder(Color.Black, 0, 0, 0, 1, 0, 1);
				rsTemp.OpenRecordset("SELECT * FROM ModuleAssociation WHERE Module = 'PP' AND PrimaryAssociate = 1 AND Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP), "CentralData");
				if (!rsTemp.EndOfFile())
				{
					dblAssocAccountTotal = modCLCalculations.CalculateAccountTotal(FCConvert.ToInt32(rsTemp.Get_Fields_Int32("REMasterAcct")), true);
					if (boolPayment)
					{
						// kgk lblAssocAcct.Top = lblPaymentInfo.Top
						// kgk lblAssocAcct.Left = imgNote.Left + imgNote.Width + 70
						lblAssocAcct_Pmt.Text = "RE";
						lblAssocAcct_Pmt.Visible = true;
						ToolTip1.SetToolTip(lblAssocAcct_Pmt, "Associated RE Account: " + FCConvert.ToString(rsTemp.Get_Fields_Int32("REMasterAcct")) + "  Amount Outstanding: " + Strings.Format(dblAssocAccountTotal, "$#,##0.00"));
					}
					else
					{
						// kgk lblAssocAcct.Top = imgNote.Top + imgNote.Height + 70
						// kgk lblAssocAcct.Left = imgNote.Left
						lblAssocAcct.Text = "RE";
						lblAssocAcct.Visible = true;
						ToolTip1.SetToolTip(lblAssocAcct, "Associated RE Account: " + FCConvert.ToString(rsTemp.Get_Fields_Int32("REMasterAcct")) + "  Amount Outstanding: " + Strings.Format(dblAssocAccountTotal, "$#,##0.00"));
					}
				}
				else
				{
					lblAssocAcct_Pmt.Visible = false;
					ToolTip1.SetToolTip(lblAssocAcct_Pmt, "");
					lblAssocAcct.Visible = false;
					ToolTip1.SetToolTip(lblAssocAcct, "");
				}
				
				strTemp = "SELECT * FROM PPValuations WHERE ValueKey = " + FCConvert.ToString(rsCL.Get_Fields("Account"));
				// Personal Property Info
				rsTemp.OpenRecordset(strTemp, modExtraModules.strPPDatabase);
				if (!(rsTemp.EndOfFile()) && !(rsTemp.BeginningOfFile()))
				{
					vsPPCategory.TextMatrix(1, 1, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category1"))), "#,##0.00"));
					vsPPCategory.TextMatrix(2, 1, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category2"))), "#,##0.00"));
					vsPPCategory.TextMatrix(3, 1, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category3"))), "#,##0.00"));
					vsPPCategory.TextMatrix(4, 1, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category4"))), "#,##0.00"));
					vsPPCategory.TextMatrix(5, 1, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category5"))), "#,##0.00"));
					vsPPCategory.TextMatrix(6, 1, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category6"))), "#,##0.00"));
					vsPPCategory.TextMatrix(7, 1, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category7"))), "#,##0.00"));
					vsPPCategory.TextMatrix(8, 1, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category8"))), "#,##0.00"));
					vsPPCategory.TextMatrix(9, 1, Strings.Format(FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("Category9"))), "#,##0.00"));
					vsPPCategory.TextMatrix(10, 1, Strings.Format(Conversion.Val(rsTemp.Get_Fields_Int32("Category1")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category2")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category3")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category4")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category5")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category6")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category7")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category8")) + Conversion.Val(rsTemp.Get_Fields_Int32("Category9")), "#,##0.00"));
				}
				// PP Account Info
				strTemp = "SELECT pOwn.Address1, pOwn.Address2, pOwn.City, pOwn.State, pOwn.Zip, TranCode, Deleted " + "FROM PPMaster INNER JOIN " + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn ON PPMaster.PartyID = pOwn.ID " + "WHERE ACCOUNT = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP);
				rsTemp.OpenRecordset(strTemp, modExtraModules.strPPDatabase);
				if (!(rsTemp.EndOfFile()) && !(rsTemp.BeginningOfFile()))
				{
					strMailingAddress1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address1")));
					if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address2"))) != "")
					{
						strMailingAddress1 += "\r\n" + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address2")));
					}
					// kk strMailingAddress2 = Trim(rsTemp.Get_Fields("Own1City")) & ", " & Trim(rsTemp.Get_Fields("Own1State")) & " " & Trim(rsTemp.Get_Fields("Own1Zip"))
					strMailingAddress2 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("City"))) + ", " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("State"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Zip")));
					boolDeleted = rsTemp.Get_Fields_Boolean("Deleted");
					// TODO: Check the table for the column [TranCode] and replace with corresponding Get_Field method
					lngCLResCode = rsTemp.Get_Fields("TranCode");
				}
				// set the mailing address
				lblPPAddress.Text = strMailingAddress1 + "\r\n" + strMailingAddress2;
			}
			//kk03212014 trocl-816  Change group info display
			FillGroupInfo();
		}

		private void SwapRows_8(int x, int y)
		{
			SwapRows(ref x, ref y);
		}

		private void SwapRows(ref int x, ref int y)
		{
			// this function will switch the two rows values in the grid
			int intCol;
			string strTemp = "";
			for (intCol = 2; intCol <= GRID.Cols - 1; intCol++)
			{
				strTemp = GRID.TextMatrix(x, intCol);
				GRID.TextMatrix(x, intCol, GRID.TextMatrix(y, intCol));
				GRID.TextMatrix(y, intCol, strTemp);
			}
		}

		private int NextSpacerLine(int x)
		{
			int NextSpacerLine = 0;
			// finds the next spacer line after the row number passed in by finding the next total line
			NextSpacerLine = GRID.FindRow("=", x, lngColGridCode) + 1;
			return NextSpacerLine;
		}

		private void GRID_Coloring(int l)
		{
			// this sub will recursively call itself in order to color all of the total lines and spacer lines
			int lngTemp;
			lngTemp = NextSpacerLine(l);
			if (lngTemp > 0)
			{
				GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngTemp, 0, lngTemp, GRID.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249)));
				// &H80000016 'this will hide the text that is living inside of the spacer row
				GRID.Select(lngTemp - 1, lngColPrincipal, lngTemp - 1, lngColGridCode);
				// this will color the totals lines
				GRID.CellBorder(Color.Blue, 0, 1, 0, 0, 0, 0);
				GRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngTemp - 1, 1, lngTemp - 1, GRID.Cols - 1, true);
				sumPrin += FCConvert.ToDouble(GRID.TextMatrix(lngTemp - 1, lngColPrincipal));
				sumInt += FCConvert.ToDouble(GRID.TextMatrix(lngTemp - 1, lngColInterest));
				sumCost += FCConvert.ToDouble(GRID.TextMatrix(lngTemp - 1, lngColCosts));
				sumTotal += FCConvert.ToDouble(GRID.TextMatrix(lngTemp - 1, lngColTotalAmount));
				GRID_Coloring(lngTemp + 1);
				// try again recursively
			}
			else
			{
				GRID.Select(0, lngColYear);
			}
		}

		private bool PaymentRecords(int BNumber, bool boolUseLien = true)
		{
			bool PaymentRecords = false;
			// VB6 Bad Scope Dim:
			int I;
			// this function will create rows subordinate to the master line for each bill record
			// and in the grid fill them with each payment made against this account
			// this function will return True if Interest Should be calculated for it
			clsDRWrapper rsRK = new clsDRWrapper();
			clsDRWrapper rsPy = new clsDRWrapper();
			// Payroll Recordset
			clsDRWrapper rsTC = new clsDRWrapper();
			double dblAdded = 0;
			int lngCount = 0;
			DateTime dtLastCHGINTForTC = DateTime.FromOADate(0);
			DateTime dtIntDate;
			if (modStatusPayments.Statics.boolRE)
			{
				strSQL = "SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountPayments + ") AS qCAP WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillKey = " + FCConvert.ToString(BNumber) + " AND BillCode = 'R' AND ReceiptNumber <> -1 ORDER BY RecordedTransactionDate, ActualSystemDate, ReceiptNumber, ID ";
			}
			else
			{
				strSQL = "SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountPayments + ") AS qCAP WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillKey = " + FCConvert.ToString(BNumber) + " AND BillCode = 'P' AND ReceiptNumber <> -1 ORDER BY RecordedTransactionDate, ActualSystemDate, ReceiptNumber, ID ";
			}
			modStatusPayments.Statics.gboolCLNoIntFromTaxClub[rsCL.Get_Fields_Int32("BillingYear") - modExtraModules.DEFAULTSUBTRACTIONVALUE] = false;
			if (!boolUseLien)
			{
				// MAL@20080723: Add check for matching year for Tax Club
				// Tracker Reference: 14033
				rsTC.OpenRecordset("SELECT * FROM TaxClub WHERE BillKey = " + FCConvert.ToString(BNumber) + " AND [Year] = " + FCConvert.ToString(rsCL.Get_Fields_Int32("BillingYear")), modExtraModules.strCLDatabase);
				if (!rsTC.EndOfFile())
				{
					boolNoCurrentInt = true;
					// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
					lngNoCurrentIntYear = FCConvert.ToInt32(rsTC.Get_Fields("Year"));
				}
				else
				{
					boolNoCurrentInt = false;
				}
				// payment lines --- all that apply
				rsPy.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				lngCount = rsPy.RecordCount();
				if (lngCount != 0)
				{
					rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsCL.Get_Fields_Int32("Ratekey")), modExtraModules.strCLDatabase);
					if (!rsRK.EndOfFile())
					{
						dtLastCHGINTForTC = (DateTime)rsRK.Get_Fields_DateTime("BillingDate");
					}
					for (I = 1; I <= lngCount; I++)
					{
						if (FCConvert.ToDouble(Strings.Trim(FCConvert.ToString(rsPy.Get_Fields_Int32("ReceiptNumber")) + " ")) != -1)
						{
							CurRow += 1;
							Add(CurRow, 1);
							GRID.TextMatrix(CurRow, lngColDate, Strings.Trim(FCConvert.ToString(rsPy.Get_Fields_DateTime("RecordedTransactionDate")) + " "));
							GRID.TextMatrix(CurRow, lngColReference, Strings.Trim(FCConvert.ToString(rsPy.Get_Fields_String("Reference")) + " "));
							GRID.TextMatrix(CurRow, lngColPeriod, Strings.Trim(FCConvert.ToString(rsPy.Get_Fields("Period")) + " "));
							dblAdded = 0;
							if (rsPy.Get_Fields_String("Code") == "A")
							{
								// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
								dynamic VBtoVar = rsPy.Get_Fields("Period");
								if (VBtoVar == "1")
								{
									modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per1 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per1 + Conversion.Val(rsPy.Get_Fields("Principal"));
								}
								else if (VBtoVar == "2")
								{
									modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2 + Conversion.Val(rsPy.Get_Fields("Principal"));
								}
								else if (VBtoVar == "3")
								{
									modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per3 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per3 + Conversion.Val(rsPy.Get_Fields("Principal"));
								}
								else if (VBtoVar == "4")
								{
									modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per4 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per4 + Conversion.Val(rsPy.Get_Fields("Principal"));
								}
								else if (VBtoVar == "A")
								{
									if (!rsRK.EndOfFile())
									{
										// find out how many periods that this bill has
										if (rsRK.Get_Fields_Int16("NumberOfPeriods") == 1)
										{
											// only one period, so put it all into per 1
											// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
											// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
											// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
											modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per1 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per1 + Conversion.Val(rsPy.Get_Fields("Principal"));
										}
										else if (rsRK.Get_Fields_Int16("NumberOfPeriods") == 2)
										{
											// two periods so make sure that not too much has been put into period 2, once it is full then put the rest into period one
											// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
											// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
											if (Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue2")) >= modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2 + modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 2, 2))
											{
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
												modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2 + modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 2, 2);
												// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
												dblAdded += modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 2, 2);
											}
											else
											{
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												dblAdded += (Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue2")) - modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2);
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2 = Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue2"));
											}
											// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
											// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
											// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
											modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per1 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per1 + modGlobal.Round(Conversion.Val(rsPy.Get_Fields("Principal")) - dblAdded, 2);
											// AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per1 = AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per1 + Round(rsPy.Get_Fields("Principal") / 2, 2)
											// AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per2 = AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per2 + Round(rsPy.Get_Fields("Principal") / 2, 2)
										}
										else if (rsRK.Get_Fields_Int16("NumberOfPeriods") == 3)
										{
											// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
											// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
											if (Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue3")) >= modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per3 + modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 3, 2))
											{
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
												modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per3 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per3 + modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 3, 2);
												// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
												dblAdded += modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 3, 2);
											}
											else
											{
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												dblAdded += (Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue3")) - modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per3);
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per3 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per3 + Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue3"));
											}
											// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
											// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
											if (Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue2")) >= modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2 + modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 3, 2))
											{
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
												modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2 + modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 3, 2);
												// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
												dblAdded += modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 3, 2);
											}
											else
											{
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												dblAdded += (Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue2")) - modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2);
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2 = Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue2"));
											}
											// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
											// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
											// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
											modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per1 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per1 + modGlobal.Round(Conversion.Val(rsPy.Get_Fields("Principal")) - dblAdded, 2);
											// AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per1 = AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per1 + Round(rsPy.Get_Fields("Principal") / 3, 2)
											// AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per2 = AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per2 + Round(rsPy.Get_Fields("Principal") / 3, 2)
											// AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per3 = AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per3 + Round(rsPy.Get_Fields("Principal") / 3, 2)
										}
										else if (rsRK.Get_Fields_Int16("NumberOfPeriods") == 4)
										{
											// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
											// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
											if (Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue4")) >= modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per4 + modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 4, 2))
											{
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
												modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per4 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per4 + modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 4, 2);
												// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
												dblAdded = modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 4, 2);
											}
											else
											{
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												dblAdded += (Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue4")) - modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per4);
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per4 = Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue4"));
											}
											// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
											// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
											if (Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue3")) >= modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per3 + modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 4, 2))
											{
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
												modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per3 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per3 + modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 4, 2);
												// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
												dblAdded += modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 4, 2);
											}
											else
											{
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												dblAdded += (Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue3")) - modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per3);
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per3 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per3 + Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue3"));
											}
											// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
											// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
											if (Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue2")) >= modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2 + modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 4, 2))
											{
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
												modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2 + modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 4, 2);
												// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
												dblAdded += modGlobal.Round(FCConvert.ToInt32(rsPy.Get_Fields("Principal")) / 4, 2);
											}
											else
											{
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												dblAdded += (Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue2")) - modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2);
												// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
												modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per2 = Conversion.Val(rsCL.Get_Fields_Decimal("TaxDue2"));
											}
											// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
											// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
											// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
											modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per1 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per1 + modGlobal.Round(Conversion.Val(rsPy.Get_Fields("Principal")) - dblAdded, 2);
											// this way did not work because of rounding issues
											// AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per4 = AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per4 + Round(rsPy.Get_Fields("Principal") / 4, 2)
											// AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per3 = AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per3 + Round(rsPy.Get_Fields("Principal") / 4, 2)
											// AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per2 = AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per2 + Round(rsPy.Get_Fields("Principal") / 4, 2)
											// AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per1 = AbatePaymentsArray(rsPy.Get_Fields("Year") Mod 1000).Per1 + Round(rsPy.Get_Fields("Principal") / 4, 2)
										}
									}
									else
									{
										// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
										// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
										// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
										modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per1 = modStatusPayments.Statics.AbatePaymentsArray[rsPy.Get_Fields("Year") % 1000].Per1 + Conversion.Val(rsPy.Get_Fields("Principal"));
									}
									// split the amount across the total amount of periods
								}
							}
							GRID.TextMatrix(CurRow, lngColCode, Strings.Trim(rsPy.Get_Fields_String("Code") + " "));
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							GRID.TextMatrix(CurRow, lngColPrincipal, rsPy.Get_Fields("Principal"));
							GRID.TextMatrix(CurRow, lngColInterest, (rsPy.Get_Fields_Decimal("PreLienInterest") + rsPy.Get_Fields_Decimal("CurrentInterest")));
							GRID.TextMatrix(CurRow, lngColCosts, Conversion.Val(rsPy.Get_Fields_Decimal("LienCost")));
							GRID.TextMatrix(CurRow, lngColTotalAmount, (FCConvert.ToDouble(GRID.TextMatrix(CurRow, lngColCosts)) + FCConvert.ToDouble(GRID.TextMatrix(CurRow, lngColInterest)) + FCConvert.ToDouble(GRID.TextMatrix(CurRow, lngColPrincipal))));
							GRID.TextMatrix(CurRow, lngColGridCode, "-");
							GRID.TextMatrix(CurRow, lngColPaymentID, FCConvert.ToString(rsPy.Get_Fields_Int32("ID")));
							GRID.TextMatrix(CurRow, lngColCHGINTNumber, FCConvert.ToString(rsPy.Get_Fields_Int32("CHGINTNumber")));
							GRID.TextMatrix(CurRow, lngColComment, FCConvert.ToString(rsPy.Get_Fields_String("Comments")));
						}
						string VBtoVar1 = rsPy.Get_Fields_String("Code");
						if (VBtoVar1 == "I")
						{
							if (!rsPy.IsFieldNull("EffectiveInterestDate"))
							{
								dtLastCHGINTForTC = (DateTime)rsPy.Get_Fields_DateTime("EffectiveInterestDate");
							}
							PaymentRecords = true;
						}
						else if (VBtoVar1 == "U")
						{
							// this will check to see if a tax club payment was last, if so
							// Dave 7/30/07 Changing Tax CLubs to go by whether or not a tax club exists not on if last payemtn is a type U
							if (boolNoCurrentInt)
							{
								// They have a tax club - so no interest
								PaymentRecords = false;
								// do not calculate interest on this account
								boolNoCurrentInt = true;
								modStatusPayments.Statics.gboolCLNoIntFromTaxClub[rsCL.Get_Fields_Int32("BillingYear") - modExtraModules.DEFAULTSUBTRACTIONVALUE] = true;
								modStatusPayments.Statics.gdtCLOldDateForTaxClub[rsCL.Get_Fields_Int32("BillingYear") - modExtraModules.DEFAULTSUBTRACTIONVALUE] = dtLastCHGINTForTC;
							}
							else
							{
								modStatusPayments.Statics.gboolCLNoIntFromTaxClub[rsCL.Get_Fields_Int32("BillingYear") - modExtraModules.DEFAULTSUBTRACTIONVALUE] = false;
								PaymentRecords = true;
								// boolNoCurrentInt = False
							}
						}
						else
						{
							if (boolNoCurrentInt && rsPy.Get_Fields_String("Code") != "A" && rsPy.Get_Fields_String("Code") != "R")
							{
								// kk09302014 trocl-892  Added Checks for Abatement/Refunded Abatement
								modStatusPayments.Statics.gboolCLNoIntFromTaxClub[rsCL.Get_Fields_Int32("BillingYear") - modExtraModules.DEFAULTSUBTRACTIONVALUE] = false;
								// boolNoCurrentInt = False
							}
							PaymentRecords = true;
						}
						rsPy.MoveNext();
					}
				}
				else
				{
					// no payments...
					PaymentRecords = true;
				}
			}
			if (modStatusPayments.Statics.gboolCLNoIntFromTaxClub[FCConvert.ToInt16(rsCL.Get_Fields_Int32("BillingYear")) - modExtraModules.DEFAULTSUBTRACTIONVALUE])
			{
				if (bcode == "LIEN" && boolUseLien)
				{
					dtIntDate = (DateTime)rsLR.Get_Fields_DateTime("InterestAppliedThroughDate");
				}
				else
				{
					dtIntDate = (DateTime)rsCL.Get_Fields_DateTime("InterestAppliedThroughDate");
				}
				if (DateAndTime.DateDiff("D", dtIntDate, modStatusPayments.Statics.gdtCLOldDateForTaxClub[FCConvert.ToInt16(rsCL.Get_Fields_Int32("BillingYear")) - modExtraModules.DEFAULTSUBTRACTIONVALUE]) < 0)
				{
					modStatusPayments.Statics.gdtCLOldDateForTaxClub[rsCL.Get_Fields_Int32("BillingYear") - modExtraModules.DEFAULTSUBTRACTIONVALUE] = dtIntDate;
				}
			}
			if (bcode == "LIEN" && boolUseLien)
			{
				// if this is liened then show the lien line
				CurRow += 1;
				Add(CurRow, 1);
				rsRK.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsLR.Get_Fields_Int32("Ratekey")), modExtraModules.strCLDatabase);
				if (rsRK.EndOfFile() || rsRK.BeginningOfFile())
				{
				}
				else
				{
					rsRK.MoveFirst();
					GRID.TextMatrix(CurRow, lngColDate, Strings.Format(rsRK.Get_Fields_DateTime("BillingDate"), "MM/dd/yy"));
				}
				GRID.TextMatrix(CurRow, lngColReference, "Liened");
				GRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, CurRow, lngColReference, true);
				GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, CurRow, lngColReference, Color.Red);
				GRID.Select(CurRow, lngColPrincipal, CurRow, lngColTotalAmount);
				GRID.CellBorder(Color.Black, 0, 1, 0, 0, 0, 0);
				// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
				GRID.TextMatrix(CurRow, lngColPrincipal, Conversion.Val(rsLR.Get_Fields("Principal")));
				// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
				GRID.TextMatrix(CurRow, lngColInterest, Conversion.Val(rsLR.Get_Fields("Interest")));
				// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
				GRID.TextMatrix(CurRow, lngColCosts, Conversion.Val(rsLR.Get_Fields("Costs")));
				// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
				GRID.TextMatrix(CurRow, lngColTotalAmount, (Conversion.Val(rsLR.Get_Fields("Principal")) + Conversion.Val(rsLR.Get_Fields("Interest")) + Conversion.Val(rsLR.Get_Fields("Costs"))));
				GRID.TextMatrix(CurRow, lngColGridCode, "-1");
				GRID.TextMatrix(CurRow, lngColPaymentID, FCConvert.ToString(Conversion.Val(rsLR.Get_Fields_Int32("ID"))));
				// and show the lien payments on subsequent lines
				strSQL = "SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountPayments + ") AS qCAP WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillKey = " + FCConvert.ToString(BNumber) + " AND BillCode = 'L' ORDER BY  RecordedTransactionDate, ID";
				rsPy.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
				lngCount = rsPy.RecordCount();
				// clear the year of Pre Lien abatement info
				modStatusPayments.Statics.AbatePaymentsArray[rsCL.Get_Fields_Int32("BillingYear") % 1000].Per1 = 0;
				modStatusPayments.Statics.AbatePaymentsArray[rsCL.Get_Fields_Int32("BillingYear") % 1000].Per2 = 0;
				modStatusPayments.Statics.AbatePaymentsArray[rsCL.Get_Fields_Int32("BillingYear") % 1000].Per3 = 0;
				modStatusPayments.Statics.AbatePaymentsArray[rsCL.Get_Fields_Int32("BillingYear") % 1000].Per4 = 0;
				if (lngCount != 0)
				{
					for (I = 1; I <= lngCount; I++)
					{
						if (FCConvert.ToDouble(Strings.Trim(FCConvert.ToString(rsPy.Get_Fields_Int32("ReceiptNumber")) + " ")) != -1)
						{
							CurRow += 1;
							Add(CurRow, 1);
							// If rsPy.Get_Fields("EffectiveInterestDate") <> 0 Then
							// .TextMatrix(CurRow, 2) = rsPy.Get_Fields("EffectiveInterestDate")
							// Else
							GRID.TextMatrix(CurRow, lngColDate, Strings.Trim(FCConvert.ToString(rsPy.Get_Fields_DateTime("RecordedTransactionDate")) + " "));
							// End If
							// .TextMatrix(CurRow, 2) = Trim$(rsPy.Get_Fields("RecordedTransactionDate") & " ")
							GRID.TextMatrix(CurRow, lngColReference, Strings.Trim(FCConvert.ToString(rsPy.Get_Fields_String("Reference")) + " "));
							// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
							GRID.TextMatrix(CurRow, lngColPeriod, Strings.Trim(FCConvert.ToString(rsPy.Get_Fields("Period")) + " "));
							GRID.TextMatrix(CurRow, lngColCode, Strings.Trim(rsPy.Get_Fields_String("Code") + " "));
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							GRID.TextMatrix(CurRow, lngColPrincipal, Strings.Trim(FCConvert.ToString(rsPy.Get_Fields("Principal")) + " "));
							GRID.TextMatrix(CurRow, lngColInterest, modGlobal.Round(Conversion.Val(rsPy.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPy.Get_Fields_Decimal("CurrentInterest")), 2));
							GRID.TextMatrix(CurRow, lngColCosts, Conversion.Val(rsPy.Get_Fields_Decimal("LienCost")));
							GRID.TextMatrix(CurRow, lngColTotalAmount, (FCConvert.ToDouble(GRID.TextMatrix(CurRow, lngColCosts)) + FCConvert.ToDouble(GRID.TextMatrix(CurRow, lngColInterest)) + FCConvert.ToDouble(GRID.TextMatrix(CurRow, lngColPrincipal))));
							GRID.TextMatrix(CurRow, lngColGridCode, "-");
							// this is used to sum the cols - is for a payment...it is subtracting from the bill
							GRID.TextMatrix(CurRow, lngColPaymentID, FCConvert.ToString(rsPy.Get_Fields_Int32("ID")));
							GRID.TextMatrix(CurRow, lngColCHGINTNumber, FCConvert.ToString(rsPy.Get_Fields_Int32("CHGINTNumber")));
							GRID.TextMatrix(CurRow, lngColComment, FCConvert.ToString(rsPy.Get_Fields_String("Comments")));
						}
						// kk10152015 trocl-1271
						// Dave 7/30/07 Changing Tax CLubs to go by whether or not a tax club exists not on if last payemtn is a type U
						// MAL@20080718: Change back to take last payment into consideration for interest
						// Tracker Reference: 14579
						// If rsPy.Get_Fields("Code") = "U" And boolNoCurrentInt Then  'this will check to see if a tax club payment was last, if so
						// PaymentRecords = True          'do not calculate interest on this account
						// boolNoCurrentInt = True
						// Else
						// PaymentRecords = True
						// boolNoCurrentInt = False
						// End If
						string VBtoVar2 = rsPy.Get_Fields_String("Code");
						if (VBtoVar2 == "I")
						{
							if (!rsPy.IsFieldNull("EffectiveInterestDate"))
							{
								dtLastCHGINTForTC = (DateTime)rsPy.Get_Fields_DateTime("EffectiveInterestDate");
							}
							PaymentRecords = true;
						}
						else if (VBtoVar2 == "U")
						{
							// Tax CLubs to go by whether or not a tax club exists not on if last payemtn is a type U
							if (boolNoCurrentInt)
							{
								// They have a tax club - so no interest
								PaymentRecords = false;
								// do not calculate interest on this account
								boolNoCurrentInt = true;
								modStatusPayments.Statics.gboolCLNoIntFromTaxClub[rsCL.Get_Fields_Int32("BillingYear") - modExtraModules.DEFAULTSUBTRACTIONVALUE] = true;
								modStatusPayments.Statics.gdtCLOldDateForTaxClub[rsCL.Get_Fields_Int32("BillingYear") - modExtraModules.DEFAULTSUBTRACTIONVALUE] = dtLastCHGINTForTC;
							}
							else
							{
								modStatusPayments.Statics.gboolCLNoIntFromTaxClub[rsCL.Get_Fields_Int32("BillingYear") - modExtraModules.DEFAULTSUBTRACTIONVALUE] = false;
								PaymentRecords = true;
							}
						}
						else
						{
							if (boolNoCurrentInt && rsPy.Get_Fields_String("Code") != "A" && rsPy.Get_Fields_String("Code") != "R")
							{
								// kk09302014 trocl-892  Added Checks for Abatement/Refunded Abatement
								modStatusPayments.Statics.gboolCLNoIntFromTaxClub[rsCL.Get_Fields_Int32("BillingYear") - modExtraModules.DEFAULTSUBTRACTIONVALUE] = false;
							}
							PaymentRecords = true;
						}
						rsPy.MoveNext();
					}
				}
			}
			return PaymentRecords;
		}

		private void imgNote_DoubleClick(object sender, System.EventArgs e)
		{
			clsDRWrapper rsNote = new clsDRWrapper();
			// MAL@20071213: Change to integrate the add/edit note and note priority steps into one
			// Tracker Reference: 11187
			rsNote.DefaultDB = modExtraModules.strCLDatabase;
			if (modStatusPayments.Statics.boolRE)
			{
				rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE));
			}
			else
			{
				rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP));
			}
			if (!rsNote.EndOfFile())
			{
				if (FCConvert.ToInt32(rsNote.Get_Fields_Int32("Priority")) > 0)
				{
					boolPop = true;
				}
				else
				{
					boolPop = false;
				}
			}
			// align the frame
			fraEditNote.Left = FCConvert.ToInt32((this.Width - fraEditNote.Width) / 2.5);
			fraEditNote.Top = FCConvert.ToInt32((this.Height - fraEditNote.Height) / 2.5);
			// Show the Frame
			fraDiscount.Visible = false;
			GRID.Visible = false;
			fraPayment.Visible = false;
			mnuPayment.Visible = false;
			cmdPaymentPreview.Visible = false;
			btnProcess.Visible = false;
			btnSaveExit.Visible = false;
			fraEditNote.Visible = true;
            // Set the Text
			txtNote.Text = strNoteText;
			if (boolPop)
			{
				chkPriority.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkPriority.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			// EditNote
            txtNote.Focus();
        }

		private void EditNote()
		{
			// Tracker Reference: 11187
			string strTemp;
			clsDRWrapper rsNote;
			int lngAcctNum = 0;
			string strType = "";
			if (modStatusPayments.Statics.boolRE)
			{
				lngAcctNum = StaticSettings.TaxCollectionValues.LastAccountRE;
				strType = "RE";
			}
			else
			{
				lngAcctNum = StaticSettings.TaxCollectionValues.LastAccountPP;
				strType = "PP";
			}
			strTemp = txtNote.Text;
			rsNote = new clsDRWrapper();
			rsNote.DefaultDB = modExtraModules.strCLDatabase;
			if (strTemp == "" && strNoteText != "")
			{
				if (FCMessageBox.Show("Are you sure that you would like to delete this note?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Delete Note") == DialogResult.Yes)
				{
					if (modStatusPayments.Statics.boolRE)
					{
						rsNote.Execute("DELETE FROM Comments WHERE Account = " + FCConvert.ToString(lngAcctNum) + " AND Type ='RE'", modExtraModules.strCLDatabase);
					}
					else
					{
						rsNote.Execute("DELETE FROM Comments WHERE Account = " + FCConvert.ToString(lngAcctNum) + " AND Type ='PP'", modExtraModules.strCLDatabase);
					}
					strNoteText = Strings.Trim(strTemp);
					imgNote.Visible = false;
				}
			}
			else
			{
				// save the next note text
				strNoteText = Strings.Trim(strTemp);
				if (modStatusPayments.Statics.boolRE)
				{
					rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(lngAcctNum) + " AND Type ='RE'");
				}
				else
				{
					rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(lngAcctNum) + " AND Type ='PP'");
				}
				if (rsNote.RecordCount() > 0)
				{
					rsNote.Edit();
				}
				else
				{
					rsNote.AddNew();
				}
				rsNote.Set_Fields("Account", lngAcctNum);
				rsNote.Set_Fields("Comment", strNoteText);
				rsNote.Set_Fields("Type", strType);
				if (FCConvert.CBool(chkPriority.CheckState == Wisej.Web.CheckState.Checked))
				{
					rsNote.Set_Fields("Priority", 1);
					boolPop = true;
				}
				else
				{
					rsNote.Set_Fields("Priority", 0);
					boolPop = false;
				}
				// MAL@20080429: Add support for pop-up reminder in RE
				// Tracker Reference: 12741
				if (strType == "RE")
				{
					rsNote.Set_Fields("ShowInRE", FCConvert.CBool(chkShowInRE.CheckState == Wisej.Web.CheckState.Checked));
				}
				else
				{
					rsNote.Set_Fields("ShowInRE", false);
				}
				rsNote.Update();
				imgNote.Visible = true;
			}
		}

		private void lblAcctComment_Click(object sender, System.EventArgs e)
		{
			mnuFileComment_Click();
		}

		private void lblAcctComment_DoubleClick(object sender, System.EventArgs e)
		{
			mnuFileComment_Click();
		}

		private void lblGrpInfo_Click(object sender, System.EventArgs e)
		{
			//kk03202014 trocl-816  Change the Group info display - show the form from the Globals
			frmGroupBalance.InstancePtr.Init(lngGroupID);
		}

		private void lblGrpInfo_Pmt_Click(object sender, System.EventArgs e)
		{
			//kk03202014 trocl-816  Change the Group info display - show the form from the Globals
			frmGroupBalance.InstancePtr.Init(lngGroupID);
		}

		private void lblOriginalTax_DoubleClick(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// when the user double clicks on this, the original tax will be discounted and the rest of the tax owed will be put in the tax field
				double dblNewDisc = 0;
				double dblNewPrin;
				// MAL@20071003: Added support for discounts on bills where amount due is less than discount amount
				// Call Reference: 117012
				// MAL@20080402: Add support for pre-payment discounts (if tax has not been billed)
				// Tracker Reference: 12996
				if (FCConvert.ToDouble(lblOriginalTax.Text) == 0)
				{
					dblNewDisc = 0;
				}
				else
				{
					dblNewDisc = modCLDiscount.CalculateDiscount(FCConvert.ToDouble(lblOriginalTax.Text));
					// MAL@20080422: Don't allow creation of reversal of credit balance
					if (dblNewDisc <= 0)
					{
						dblNewDisc = 0;
					}
				}
				dblNewPrin = FCConvert.ToDouble(lblOriginalTax.Text) - FCConvert.ToDouble(Conversion.Val(lblOriginalTax.Tag)) - dblNewDisc;
				txtDiscDisc.Text = Strings.Format(dblNewDisc, "#,##0.00");
				if (dblNewPrin > 0)
				{
					txtDiscPrin.Text = Strings.Format(dblNewPrin, "#,##0.00");
					blnDiscountCredit = false;
					// MAL@20080401: Add support for pre-payment discount
					// Tracker Reference: 12996
				}
				else if (FCConvert.ToDouble(lblOriginalTax.Text) == 0)
				{
					txtDiscPrin.Text = Strings.Format(0, "#,##0.00");
					blnDiscountCredit = false;
				}
				else
				{
					// txtDiscPrin.Text = Format(lblDiscTotal.Caption, "#,##0.00")     'Get Total Principal Due
					txtDiscPrin.Text = Strings.Format(0, "#,##0.00");
					blnDiscountCredit = true;
				}
				// MAL@20080429: Make sure that total gets recalculated as well
				lblDiscTotal.Text = Strings.Format(FCConvert.ToDouble(txtDiscPrin.Text) + FCConvert.ToDouble(txtDiscDisc.Text), "#,##0.00");
				if (cmdDisc[0].Visible && cmdDisc[0].Enabled)
				{
					cmdDisc[0].Focus();
				}
				// Dim dblNewDisc              As Double
				// Dim dblNewPrin              As Double
				// 
				// dblNewDisc = CalculateDiscount(CDbl(lblOriginalTax.Caption))
				// dblNewPrin = CDbl(lblOriginalTax.Caption) - CDbl(lblOriginalTax.Tag) - dblNewDisc
				// 
				// txtDiscDisc.Text = Format(dblNewDisc, "#,##0.00")
				// txtDiscPrin.Text = Format(dblNewPrin, "#,##0.00")
				// 
				// If cmdDisc(0).Visible And cmdDisc(0).Enabled Then
				// cmdDisc(0).SetFocus
				// End If
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Calculating Original Tax Discount");
			}
		}

		public void lblOriginalTax_DblClick()
		{
			lblOriginalTax_DoubleClick(lblOriginalTax, new System.EventArgs());
		}

		private void lblOriginalTaxLabel_DoubleClick(object sender, System.EventArgs e)
		{
			lblOriginalTax_DblClick();
		}

		private void lblTaxClub_DoubleClick(object sender, System.EventArgs e)
		{
			// This will create a tax club payment for this account
			CreateTaxClubPayment();
		}

		private void mnuFileAccountInfo_Click(object sender, System.EventArgs e)
		{
			ShowAccountInfo();
		}

		private void mnuFileComment_Click(object sender, System.EventArgs e)
		{
			int lngAcctNum = 0;
			if (modStatusPayments.Statics.boolRE)
			{
				//frmCLComment.InstancePtr.Init( modStatusPayments.Statics.boolRE, StaticSettings.TaxCollectionValues.LastAccountRE);
				lngAcctNum = StaticSettings.TaxCollectionValues.LastAccountRE;
			}
			else
			{
				//frmCLComment.InstancePtr.Init( modStatusPayments.Statics.boolRE,  StaticSettings.TaxCollectionValues.LastAccountPP);
				lngAcctNum = StaticSettings.TaxCollectionValues.LastAccountPP;
			}
			// check for a comment
			//			lblAcctComment.ForeColor = Color.Red;
			ToolTip1.SetToolTip(lblAcctComment, "This account has a comment.");
			if (Strings.Trim(modStatusPayments.GetAccountComment(ref lngAcctNum, ref modStatusPayments.Statics.boolRE)) != "")
			{
				lblAcctComment.Visible = true;
			}
			else
			{
				lblAcctComment.Visible = false;
			}
		}

		public void mnuFileComment_Click()
		{
			mnuFileComment_Click(mnuFileComment, new System.EventArgs());
		}

		private void mnuFileEditNote_Click(object sender, System.EventArgs e)
		{
			bool blnShowRE = false;
			clsDRWrapper rsNote = new clsDRWrapper();
			rsNote.DefaultDB = modExtraModules.strCLDatabase;
			if (modStatusPayments.Statics.boolRE)
			{
				rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE));
			}
			else
			{
				rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP));
			}
			if (!rsNote.EndOfFile())
			{
				if (FCConvert.ToInt32(rsNote.Get_Fields_Int32("Priority")) > 0)
				{
					boolPop = true;
				}
				else
				{
					boolPop = false;
				}
				blnShowRE = FCConvert.ToBoolean(rsNote.Get_Fields_Boolean("ShowInRE"));
				// MAL@20080429 ; Tracker Reference: 12741
			}
			// If rsNote.EndOfFile Then        'if there is not a note for this account, add one and then edit it
			// rsNote.AddNew
			// If boolRE Then
			// rsNote.Get_Fields("Account") = CurrentAccountRE
			// rsNote.Get_Fields("Type") = "RE"
			// Else
			// rsNote.Get_Fields("Account") = CurrentAccountPP
			// rsNote.Get_Fields("Type") = "PP"
			// End If
			// rsNote.Update
			// End If
			// End If
			// MAL@20071213: Change to integrate the add/edit note and note priority steps into one
			// Tracker Reference: 11187
			// align the frame
			fraEditNote.Left = FCConvert.ToInt32((this.Width - fraEditNote.Width) / 2.5);
			fraEditNote.Top = FCConvert.ToInt32((this.Height - fraEditNote.Height) / 2.5);
			// Show the Frame
			fraDiscount.Visible = false;
			GRID.Visible = false;
			fraPayment.Visible = false;
			mnuPayment.Visible = false;
			cmdPaymentPreview.Visible = false;
			//cmdPaymentSave.Visible = false;
			btnProcess.Visible = false;
			btnSaveExit.Visible = false;
			fraEditNote.Visible = true;
            imgNote.Visible = true;
			// Set the Text
			txtNote.Text = strNoteText;
			// MAL@20080509: Add check for character limit
			// Tracker Reference: 12741
			intCharCount = txtNote.Text.Length;
			lblRemainChars.Text = FCConvert.ToString((255 - intCharCount));
			if (FCConvert.ToInt16(FCConvert.ToDouble(lblRemainChars.Text)) <= 0)
			{
				lblRemainChars.Text = FCConvert.ToString(0);
			}
			if (boolPop)
			{
				chkPriority.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkPriority.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			// MAL@20080429: Add support for showing pop-up reminder in RE
			// Tracker Reference: 12741
			if (blnShowRE && modStatusPayments.Statics.boolRE)
			{
				chkShowInRE.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkShowInRE.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (!modStatusPayments.Statics.boolRE)
			{
				chkShowInRE.Visible = false;
			}
			else
			{
				chkShowInRE.Visible = true;
			}
			// EditNote
		}

		private void mnuFileDischarge_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDischarge = new clsDRWrapper();
				clsDRWrapper rsBill = new clsDRWrapper();
				int lngLienKey = 0;
				int lngRow;
				// VBto upgrade warning: lngYear As int	OnWrite(string)
				int lngYear = 0;
				lngRow = GRID.Row;
				if (lngRow > 0)
				{
					if (lngRow < GRID.Rows)
					{
						lngRow += 1;
					}
					lngYear = FCConvert.ToInt32(modExtraModules.FormatYear(GRID.TextMatrix(LastParentRow(lngRow), lngColYear)));
					if (lngYear > 19000)
					{
						rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingYear = " + FCConvert.ToString(lngYear) + " AND BillingType = 'RE'", modExtraModules.strCLDatabase);
						if (!rsBill.EndOfFile())
						{
							lngLienKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("LienRecordNumber"));
						}
						else
						{
							lngLienKey = 0;
						}
					}
					else
					{
						lngLienKey = 0;
					}
				}
				else
				{
					lngLienKey = 0;
				}
				if (lngLienKey > 0)
				{
					rsDischarge.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + FCConvert.ToString(rsBill.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
					// & " AND Printed = False"
					if (!rsDischarge.EndOfFile())
					{
						// not sure about the date...maybe have to look the record first
						frmLienDischargeNotice.InstancePtr.Init(rsDischarge.Get_Fields_DateTime("Datepaid"), lngLienKey, lngYear.ToString());
					}
					else
					{
						if (FCMessageBox.Show("No Discharge Notice has been created for this lien record.  Would you like to create one?", MsgBoxStyle.YesNo, "No Discharge") == DialogResult.Yes)
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							modGlobalFunctions.AddCYAEntry_242("CL", "Add Discharge Notice", "Acct : " + FCConvert.ToString(rsBill.Get_Fields("Account")), "Lien ID : " + FCConvert.ToString(lngLienKey), "Year : " + FCConvert.ToString(rsBill.Get_Fields_Int32("BillingYear")));
							rsDischarge.AddNew();
							rsDischarge.Set_Fields("UpdatedDate", DateTime.Today);
							rsDischarge.Set_Fields("Datepaid", DateTime.Today);
							rsDischarge.Set_Fields("Teller", modGlobalConstants.Statics.gstrUserID);
							rsDischarge.Set_Fields("LienKey", lngLienKey);
							rsDischarge.Update();
							frmLienDischargeNotice.InstancePtr.Init(DateTime.Today, lngLienKey, lngYear.ToString());
						}
						else
						{
							// do nothing
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Lien Discharge Notice Error");
			}
		}

		private void mnuFileInterestedParty_Click(object sender, System.EventArgs e)
		{
			frmInterestedParties.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(lblAccount.Text)), "CL", 0, true, 2);
		}

		private void mnuFileMortgageHolderInfo_Click(object sender, System.EventArgs e)
		{
			frmMortgageHolder.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(lblAccount.Text)), 2);
			frmMortgageHolder.InstancePtr.Show(App.MainForm);
		}

		private void mnuFileNextAccount_Click(object sender, System.EventArgs e)
		{
			// this will reset the form to the next account from the search grid
			if (Information.UBound(modStatusPayments.Statics.arrSearchList, 1) > 1)
			{
				modStatusPayments.Statics.lngSearchListPointer = (modStatusPayments.Statics.lngSearchListPointer + 1) % (Information.UBound(modStatusPayments.Statics.arrSearchList, 1) + 1);
				SetToNewAccount(ref modStatusPayments.Statics.arrSearchList[modStatusPayments.Statics.lngSearchListPointer]);
			}
			// If strSearchSQL <> "" Then
			// rsSearchString.MoveNext
			// If Not rsSearchString.EndOfFile Then
			// SetToNewAccount rsSearchString.Get_Fields("Account")
			// Else
			// rsSearchString.MoveFirst    'start from the first again
			// If Not rsSearchString.EndOfFile Then
			// SetToNewAccount rsSearchString.Get_Fields("Account")
			// End If
			// End If
			// End If
		}

		//private void mnuFileOptionLoadValidAccount_Click(object sender, System.EventArgs e)
		//{
		//	if (modGlobalConstants.Statics.gboolBD)
		//	{
		//		// txtAcctNumber.Text = frmLoadValidAccounts.Init(txtAcctNumber.Text)
		//		txtAcctNumber.TextMatrix(0, 0, frmLoadValidAccounts.InstancePtr.Init(txtAcctNumber.Text));
		//		txtAcctNumber.EditText = txtAcctNumber.TextMatrix(0, 0);
		//	}
		//}

		//private void mnuFileOptionsBlankBill_Click(object sender, System.EventArgs e)
		//{
		//	int lngYear;
		//	int lngBK = 0;
		//	// this will show the year selction choice and allow the user to add a blank billing record
		//	lngYear = frmPrePayYear.InstancePtr.Init(1);
		//	if (lngYear > 20000)
		//	{
		//		if (modStatusPayments.Statics.boolRE)
		//		{
		//			lngBK = modStatusPayments.CreateBlankCLBill_18(lngYear, StaticSettings.TaxCollectionValues.LastAccountRE, "RE");
		//		}
		//		else
		//		{
		//			lngBK = modStatusPayments.CreateBlankCLBill_18(lngYear, StaticSettings.TaxCollectionValues.LastAccountPP, "PP");
		//		}
		//		if (lngBK > 0)
		//		{
		//			// a bill has been created, now tell the user and refresh the screen
		//			if (modStatusPayments.Statics.boolRE)
		//			{
		//				FCMessageBox.Show("A bill has been created for Real Estate account " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + ".", MsgBoxStyle.Information, "Bill Record Created");
		//			}
		//			else
		//			{
		//				FCMessageBox.Show("A bill has been created for Personal Property account " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + ".", MsgBoxStyle.Information, "Bill Record Created");
		//			}
		//			// refresh the screen
		//			FormReset = true;
		//			Form_Activate();
		//			// Doevents
		//			FormReset = false;
		//		}
		//	}
		//}

		//private void mnuFileOptionsPrint_Click(object sender, System.EventArgs e)
		//{
		//	// Print the receipt...only from CL
		//	if (!modGlobalConstants.Statics.gboolCR)
		//	{
		//		modStatusPayments.Statics.gboolUsingCLReceipt = true;
		//		// set this variable so the payments know to print a receipt
		//		mnuPaymentSaveExit_Click();
		//		// call the save and exit function
		//		modStatusPayments.Statics.gboolUsingCLReceipt = false;
		//		// unset the variable
		//	}
		//}

		//private void mnuFilePreviousAccount_Click(object sender, System.EventArgs e)
		//{
		//	// this will reset the form to the previous account from the search grid
		//	if (Information.UBound(modStatusPayments.Statics.arrSearchList, 1) > 1)
		//	{
		//		if (modStatusPayments.Statics.lngSearchListPointer == 0)
		//		{
		//			modStatusPayments.Statics.lngSearchListPointer = Information.UBound(modStatusPayments.Statics.arrSearchList, 1) - 1;
		//		}
		//		else
		//		{
		//			modStatusPayments.Statics.lngSearchListPointer -= 1;
		//		}
		//		SetToNewAccount(ref modStatusPayments.Statics.arrSearchList[modStatusPayments.Statics.lngSearchListPointer]);
		//	}
		//}

		//private void mnuFilePrint_Click(object sender, System.EventArgs e)
		//{
		//	// this will show the summary grid in a report format, only showing the rows that are open
		//	// at the time the option is choosen
		//	frmReportViewer.InstancePtr.Init(arAccountDetail.InstancePtr);
		//	// .Show , MDIParent
		//	boolAcctDetailShown = true;
		//}

		//private void mnuFilePrintAltLDN_Click(object sender, System.EventArgs e)
		//{
		//	frmLienDischargeNoticeBP.InstancePtr.Init(DateTime.Today, StaticSettings.TaxCollectionValues.LastAccountRE);
		//}

		//private void mnuFilePrintBPReport_Click(object sender, System.EventArgs e)
		//{
		//	arBookPageAccount.InstancePtr.Init(StaticSettings.TaxCollectionValues.LastAccountRE, StaticSettings.TaxCollectionValues.LastAccountRE);
		//}

		//private void mnuFilePrintGroupInfo_Click(object sender, System.EventArgs e)
		//{
		//	// This will show a report with the group information
		//	rptGroupInformation.InstancePtr.Init(lngGroupNumber);
		//}

		//private void mnuFilePrintRateInfo_Click(object sender, System.EventArgs e)
		//{
		//	arCLInformationScreen.InstancePtr.Init(vsRateInfo.Rows, vsRateInfo.Cols);
		//	frmReportViewer.InstancePtr.Init(arCLInformationScreen.InstancePtr);
		//}

		//private void mnuFilePriority_Click(object sender, System.EventArgs e)
		//{
		//	DialogResult lngAnswer;
		//	clsDRWrapper rsNote = new clsDRWrapper();
		//	// this will allow the user to set the priority of the note to pop up when the account is entered
		//	rsNote.DefaultDB = modExtraModules.strCLDatabase;
		//	lngAnswer = FCMessageBox.Show("Would you like this note to pop up when then account is accessed.", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Note Priority");
		//	if (lngAnswer == DialogResult.Yes)
		//	{
		//		if (modStatusPayments.Statics.boolRE)
		//		{
		//			rsNote.Execute("UPDATE Comments SET Priority = 1 WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + "  AND Type = 'RE'", modExtraModules.strCLDatabase);
		//		}
		//		else
		//		{
		//			rsNote.Execute("UPDATE Comments SET Priority = 1 WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + "  AND Type = 'PP'", modExtraModules.strCLDatabase);
		//		}
		//	}
		//	else if (lngAnswer == DialogResult.No)
		//	{
		//		if (modStatusPayments.Statics.boolRE)
		//		{
		//			rsNote.Execute("UPDATE Comments SET Priority = 0 WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + "  AND Type = 'RE'", modExtraModules.strCLDatabase);
		//		}
		//		else
		//		{
		//			rsNote.Execute("UPDATE Comments SET Priority = 0 WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + "  AND Type = 'PP'", modExtraModules.strCLDatabase);
		//		}
		//	}
		//	else
		//	{
		//		// do nothing
		//	}
		//}

		private void mnuFileREComment_Click(object sender, System.EventArgs e)
		{
			if (modStatusPayments.Statics.boolRE)
			{
				frmRECLComment.InstancePtr.Init(ref StaticSettings.TaxCollectionValues.LastAccountRE);
			}
		}

		private void mnuFileReprintReceipt_Click(object sender, System.EventArgs e)
		{
			int lngGridRow;
			clsDRWrapper rsReprint = new clsDRWrapper();
			// this needs to call the CL receipt
			lngGridRow = GRID.Row;
			rsReprint.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + GRID.TextMatrix(lngGridRow, lngColPaymentID), modExtraModules.strCLDatabase);
			if (!rsReprint.EndOfFile())
			{
				boolReprintReceipt = true;
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				modStatusPayments.Statics.ReprintCLReceipt.Account = FCConvert.ToInt32(rsReprint.Get_Fields("Account"));
				modStatusPayments.Statics.ReprintCLReceipt.ActualSystemDate = (DateTime)rsReprint.Get_Fields_DateTime("ActualSystemDate");
				modStatusPayments.Statics.ReprintCLReceipt.BillCode = FCConvert.ToString(rsReprint.Get_Fields_String("BillCode"));
				modStatusPayments.Statics.ReprintCLReceipt.BillKey = FCConvert.ToInt32(rsReprint.Get_Fields_Int32("BillKey"));
				modStatusPayments.Statics.ReprintCLReceipt.BudgetaryAccountNumber = FCConvert.ToString(rsReprint.Get_Fields_String("BudgetaryAccountNumber"));
				modStatusPayments.Statics.ReprintCLReceipt.CashDrawer = FCConvert.ToString(rsReprint.Get_Fields_String("CashDrawer"));
				modStatusPayments.Statics.ReprintCLReceipt.CHGINTDate = (DateTime)rsReprint.Get_Fields_DateTime("CHGINTDate");
				modStatusPayments.Statics.ReprintCLReceipt.CHGINTNumber = FCConvert.ToInt32(rsReprint.Get_Fields_Int32("CHGINTNumber"));
				modStatusPayments.Statics.ReprintCLReceipt.Code = rsReprint.Get_Fields_String("Code");
				modStatusPayments.Statics.ReprintCLReceipt.Comments = FCConvert.ToString(rsReprint.Get_Fields_String("Comments"));
				modStatusPayments.Statics.ReprintCLReceipt.CurrentInterest = FCConvert.ToDecimal(rsReprint.Get_Fields_Decimal("CurrentInterest"));
				modStatusPayments.Statics.ReprintCLReceipt.EffectiveInterestDate = (DateTime)rsReprint.Get_Fields_DateTime("EffectiveInterestDate");
				modStatusPayments.Statics.ReprintCLReceipt.GeneralLedger = FCConvert.ToString(rsReprint.Get_Fields_String("GeneralLedger"));
				modStatusPayments.Statics.ReprintCLReceipt.ID = FCConvert.ToInt32(rsReprint.Get_Fields_Int32("ID"));
				modStatusPayments.Statics.ReprintCLReceipt.LienCost = FCConvert.ToDecimal(rsReprint.Get_Fields_Decimal("LienCost"));
				modStatusPayments.Statics.ReprintCLReceipt.PaidBy = FCConvert.ToString(rsReprint.Get_Fields_String("PaidBy"));
				// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
				modStatusPayments.Statics.ReprintCLReceipt.Period = FCConvert.ToString(rsReprint.Get_Fields("Period"));
				modStatusPayments.Statics.ReprintCLReceipt.PreLienInterest = FCConvert.ToDecimal(rsReprint.Get_Fields_Decimal("PreLienInterest"));
				// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
				modStatusPayments.Statics.ReprintCLReceipt.Principal = FCConvert.ToDecimal(rsReprint.Get_Fields("Principal"));
				modStatusPayments.Statics.ReprintCLReceipt.ReceiptNumber = FCConvert.ToString(rsReprint.Get_Fields_Int32("ReceiptNumber"));
				modStatusPayments.Statics.ReprintCLReceipt.RecordedTransactionDate = (DateTime)rsReprint.Get_Fields_DateTime("RecordedTransactionDate");
				modStatusPayments.Statics.ReprintCLReceipt.Reference = FCConvert.ToString(rsReprint.Get_Fields_String("Reference"));
				modStatusPayments.Statics.ReprintCLReceipt.ReversalID = 0;
				// rsReprint.Get_Fields("ReversalID")
				modStatusPayments.Statics.ReprintCLReceipt.Teller = FCConvert.ToString(rsReprint.Get_Fields_String("Teller"));
				modStatusPayments.Statics.ReprintCLReceipt.TransNumber = FCConvert.ToString(rsReprint.Get_Fields_String("TransNumber"));
				// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
				modStatusPayments.Statics.ReprintCLReceipt.Year = FCConvert.ToInt32(rsReprint.Get_Fields("Year"));
				if (modExtraModules.IsThisCR())
				{
					// TODO: Check the table for the column [ResCode] and replace with corresponding Get_Field method
					modStatusPayments.Statics.ReprintCLReceipt.ResCode = FCConvert.ToInt32(rsReprint.Get_Fields("ResCode"));
				}
				// Doevents
				modGlobal.PrintCLReceipt();
				// Doevents
				boolReprintReceipt = false;
			}
		}

		private void mnuFileTaxInfoSheet_Click(object sender, System.EventArgs e)
		{
			rptAccountInformationSheet.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(lblAccount.Text)), modStatusPayments.Statics.EffectiveDate);
		}

		//private void mnuPaymentClearList_Click(object sender, System.EventArgs e)
		//{
		//	int lngCT;
		//	for (lngCT = vsPayments.Rows - 1; lngCT >= 1; lngCT--)
		//	{
		//		if (lngCT >= vsPayments.Rows)
		//		{
		//			// a secondary check, so that if a CHGINT line is removed while removing the others, then the removal will restart at the last row
		//			if (vsPayments.Rows > 1)
		//			{
		//				lngCT = vsPayments.Rows - 1;
		//			}
		//			else
		//			{
		//				break;
		//			}
		//		}
		//		if (vsPayments.TextMatrix(lngCT, 2) != "CHGINT")
		//		{
		//			// do not try and remove the CHGINT lines because they will be deleted by their respective payments
		//			// check to see if any of the payments have already been saved in the database
		//			if (lngCT <= vsPayments.Rows - 1)
		//			{
		//				DeletePaymentRow(lngCT);
		//				// this will remove it from the CR Summary screen too
		//			}
		//		}
		//	}
		//	// this will double check for CHGINT payments that may still be in the list
		//	if (vsPayments.Rows > 0)
		//	{
		//		for (lngCT = vsPayments.Rows - 1; lngCT >= 1; lngCT--)
		//		{
		//			if (lngCT < vsPayments.Rows - 1)
		//			{
		//				// check to see if any of the payments have already been saved in the database
		//				DeletePaymentRow(lngCT);
		//				// this will remove it from the CR Summary screen too
		//			}
		//		}
		//	}
		//	vsPayments.Rows = 1;
		//	FCUtils.EraseSafe(modStatusPayments.Statics.PaymentArray);
		//	modStatusPayments.CheckAllPending();
		//}

		//private void mnuPaymentClearPayment_Click(object sender, System.EventArgs e)
		//{
		//	modStatusPayments.ResetPaymentFrame();
		//}

		//private void mnuPaymentPreview_Click(object sender, System.EventArgs e)
		//{
		//	string strTemp = "";
		//	// this sub will show the user a preview of the account and year taking into account all
		//	// payments that are saved in the payment grid
		//	if (cmdPaymentPreview.Text != "ExitPreview")
		//	{
		//		if (GRID.RowOutlineLevel(GRID.Row) == 1)
		//		{
		//			strTemp = GRID.TextMatrix(GRID.Row, lngColYear);
		//		}
		//		else
		//		{
		//			strTemp = GRID.TextMatrix(LastParentRow(GRID.Row), lngColYear);
		//		}
		//		if (Conversion.Val(txtPrincipal.Text) != 0 || Conversion.Val(txtInterest.Text) != 0 || Conversion.Val(txtCosts.Text) != 0)
		//		{
		//			FCMessageBox.Show("There is a pending payment that has not been saved.  If you would like to save it, please hit F11 and then run the preview.", MsgBoxStyle.Exclamation, "Pending Payment");
		//			return;
		//		}
		//		if (Conversion.Val(strTemp) != 0)
		//		{
		//			modStatusPayments.CreatePreview_2(FCConvert.ToInt16(modExtraModules.FormatYear(strTemp)));
		//		}
		//	}
		//	else
		//	{
		//		cmdPaymentPreview.Text = "Preview";
		//		GRID.Visible = true;
		//		vsPreview.Visible = false;
		//		vsPreview.Rows = 1;
		//	}
		//}

		//private void mnuPaymentSave_Click(object sender, System.EventArgs e)
		//{
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// this will add the current payment that is being entered into the payment frame
		//		// into the listbox below the boxes and into the payment array
		//		int I;
		//		int intYear/*unused?*/;
		//		int lngDefYear/*unused?*/;
		//		if (txtCash.Text == "N" && txtCD.Text == "N")
		//		{
		//			txtAcctNumber_Validate_2(false);
		//		}
		//		if (!boolPayment || vsPreview.Visible || boolDoNotContinueSave)
		//		{
		//			return;
		//		}
		//		// this is the regular payment
		//		int intParrIndex;
		//		intParrIndex = -1;
		//		// this is the default, if this is changed then it is the index number of the corresponding payment
		//		if (txtPrincipal.Text == "")
		//			txtPrincipal.Text = "0.00";
		//		if (txtInterest.Text == "")
		//			txtInterest.Text = "0.00";
		//		if (txtCosts.Text == "")
		//			txtCosts.Text = "0.00";
		//		if (FCConvert.ToDouble(txtPrincipal.Text) != 0 || FCConvert.ToDouble(txtInterest.Text) != 0 || FCConvert.ToDouble(txtCosts.Text) != 0)
		//		{
		//			if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A")
		//			{
		//				// MAL@20080325: Calculate interest without having to tab out of Principal field
		//				// Tracker Reference: 12776
		//				if (Conversion.Val(txtInterest.Text) == 0)
		//				{
		//					txtInterest.Text = Strings.Format(CalculateAbatementInterestByPayments_2(FCConvert.ToDouble(txtPrincipal.Text)), "#,##0.00");
		//					// , dblMaxInt
		//				}
		//				// abatement = decrease the bill amount (opposite of Supplemental)
		//				intParrIndex = modStatusPayments.AddPaymentToList(vsPayments.Rows);
		//				modStatusPayments.AddCHGINTToList(vsPayments.Rows);
		//				modStatusPayments.ResetPaymentFrame();
		//			}
		//			else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R")
		//			{
		//				// MAL@20080325: Calculate interest without having to tab out of Principal field
		//				// Tracker Reference: 12776
		//				if (Conversion.Val(txtInterest.Text) == 0)
		//				{
		//					txtInterest.Text = Strings.Format(CalculateAbatementInterestByPayments_2(FCConvert.ToDouble(txtPrincipal.Text)), "#,##0.00");
		//					// , dblMaxInt
		//				}
		//				// refunded abatement = decrease the bill amount and give the cash to the person
		//				intParrIndex = modStatusPayments.AddPaymentToList(vsPayments.Rows);
		//				modStatusPayments.AddCHGINTToList(vsPayments.Rows);
		//				AddRefundedAbatementLine(intParrIndex);
		//				frmRECLStatus.InstancePtr.boolChangingCode = true;
		//				// set the combo box back to payment
		//				for (I = 0; I <= cmbCode.Items.Count - 1; I++)
		//				{
		//					if (Strings.Left(cmbCode.Items[I].ToString(), 1) == "P")
		//					{
		//						cmbCode.SelectedIndex = I;
		//						break;
		//					}
		//				}
		//				frmRECLStatus.InstancePtr.boolChangingCode = false;
		//			}
		//			else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "D")
		//			{
		//				// discount...this works without hitting F11
		//			}
		//			else if ((Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "P") || (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "C"))
		//			{
		//				// check to see if there are any years before the one being paid
		//				if (cmbYear.Items[cmbYear.SelectedIndex].ToString() != "Auto")
		//				{
		//					if (PaymentForDefaultYear())
		//					{
		//						// this is ok
		//					}
		//					else
		//					{
		//						// not paying the default year
		//						if (FCMessageBox.Show("You are not paying the default year.  Would you like to continue with this transaction?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Default Year") == DialogResult.Yes)
		//						{
		//							// they said yes, so allow them by
		//						}
		//						else
		//						{
		//							// they said not, exit this function
		//							return;
		//						}
		//					}
		//				}
		//				if (modStatusPayments.PaymentGreaterThanOwed())
		//				{
		//					// this function figures out if the payment entered is greater than the years bill
		//					if (FCMessageBox.Show("The payment entered is greater than owed, would you like to continue?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Payment Amount") == DialogResult.Yes)
		//					{
		//						// yes
		//						if (cmbYear.Items[cmbYear.SelectedIndex].ToString() == "Auto")
		//						{
		//							// since it is an overpayment
		//							CheckForLastYearOverpayment();
		//						}
		//						intParrIndex = modStatusPayments.AddPaymentToList(vsPayments.Rows, FCConvert.CBool(Strings.UCase(frmRECLStatus.InstancePtr.txtReference.Text) == "REVERS") || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "C", true);
		//						// CBool(dblCurrentInt((FormatYear(cmbYear.List(cmbYear.ListIndex))) - DEFAULTSUBTRACTIONVALUE) <> 0)
		//					}
		//					else
		//					{
		//						// no
		//					}
		//				}
		//				else
		//				{
		//					// payment is not greater than owed
		//					intParrIndex = modStatusPayments.AddPaymentToList(vsPayments.Rows, FCConvert.CBool(Strings.UCase(frmRECLStatus.InstancePtr.txtReference.Text) == "REVERS") || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "C");
		//				}
		//			}
		//			else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "S")
		//			{
		//				// supplemental bill = increase the amount of the bill (Opposite of Abatement)
		//				intParrIndex = modStatusPayments.AddPaymentToList(vsPayments.Rows);
		//			}
		//			else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "U")
		//			{
		//				// tax club = can pay in installments like a christmas club
		//				// when tax club is entered, there is no interest charged and the interest paid through date it not affected
		//				intParrIndex = modStatusPayments.AddPaymentToList(vsPayments.Rows, false, true);
		//			}
		//			else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "Y")
		//			{
		//				// prepayment
		//				// create a new Billing Year with a zero balance if it is after this year
		//				// create a payment record that will credit the account in the principal column
		//				intParrIndex = modStatusPayments.AddPaymentToList(vsPayments.Rows);
		//				modStatusPayments.AddCHGINTToList(vsPayments.Rows);
		//			}
		//			else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "N")
		//			{
		//				// Writeoff (Non-Budgetary)
		//			}
		//			else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "F")
		//			{
		//				// Overpayment Refund
		//				intParrIndex = modStatusPayments.AddPaymentToList(vsPayments.Rows);
		//			}
		//			else
		//			{
		//				FCMessageBox.Show("No Code entered!", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "ERROR");
		//			}
		//		}
		//		else
		//		{
		//			if (vsPayments.Rows == 1)
		//			{
		//				if (NegativeBillValues())
		//				{
		//					if (FCMessageBox.Show("Apply the negative account values to bills with outstanding balances?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Negative Account Balances") == DialogResult.Yes)
		//					{
		//						AffectAllNegativeBillValues();
		//						modStatusPayments.CheckAllPending();
		//					}
		//				}
		//				else
		//				{
		//					FCMessageBox.Show("There are no values in the payment fields.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "ERROR");
		//				}
		//			}
		//		}
		//		if (intParrIndex == -1)
		//		{
		//			return;
		//		}
		//		// reset payment boxes
		//		txtTransactionDate.Text = Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy");
		//		txtReference.Text = "";
		//		txtPrincipal.Text = Strings.Format(0, "#,##0.00");
		//		txtInterest.Text = Strings.Format(0, "#,##0.00");
		//		txtCosts.Text = Strings.Format(0, "#,##0.00");
		//		txtComments.Text = "";
		//		// this is the CHGINT Payment Record
		//		if (modStatusPayments.Statics.lngCHGINT > 0)
		//		{
		//			// if this is a payment reverse that has interest charged
		//			modStatusPayments.AddCHGINTToList(vsPayments.Rows, 0, false, intParrIndex);
		//			// then add the reverse of the interest charged to the payment grid
		//		}
		//		else if (modStatusPayments.Statics.lngCHGINT < 0)
		//		{
		//			// If Not boolOngoingReversal And lngReverseCHGINTNumber <> 0 Then
		//			modStatusPayments.AddCHGINTToList(vsPayments.Rows, modStatusPayments.Statics.PaymentArray[intParrIndex].ReversalID, false, intParrIndex);
		//			// then add the reverse of the interest charged to the payment grid
		//			// Else
		//			// boolMultiLine = False
		//			// End If
		//		}
		//		modStatusPayments.CheckAllPending();
		//		// this will reset the asteriks on the right side of the form whenever another payment is added
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving Payment");
		//	}
		//}

		//public void mnuPaymentSave_Click()
		//{
		//	mnuPaymentSave_Click(btnProcess, new System.EventArgs());
		//}

		//private void mnuPaymentSaveExit_Click(object sender, System.EventArgs e)
		//{
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		clsDRWrapper rsTemp = new clsDRWrapper();
		//		mnuPaymentSave_Click();
		//		if (txtCash.Text == "N" && txtCD.Text == "N")
		//		{
		//			txtAcctNumber_Validate_2(false);
		//		}
		//		// this sub will create all of the payment records that are saved in the
		//		if (!boolPayment || vsPreview.Visible || fraDiscount.Visible || boolDoNotContinueSave)
		//		{
		//			return;
		//		}
		//		// Open "SavingPayment.txt" For Output As #1
		//		// Write #1, Format(Time, "hh:mm:ss") & " - Start"
		//		// payment list
		//		bool boolDone = false;
		//		clsDRWrapper rsCreatePy = new clsDRWrapper();
		//		int ArrayIndex = 0;
		//		DateTime dateTemp = DateTime.FromOADate(0);
		//		// DJW@11/17/2010 Changed array size from 1000 to 20000
		//		bool[] boolIntChargedToday = new bool[20000 + 1];
		//		// this will not let interest get charged more than once a day
		//		bool boolSaveForCR;
		//		bool boolDoNotRecalc = false;
		//		int lngCurrentYear = 0;
		//		DialogResult intAns;
		//		boolSaveForCR = modExtraModules.IsThisCR();
		//		// if negative this is variable, lngCHGINT, is the charged interest line that is getting reversed
		//		if (modStatusPayments.Statics.lngCHGINT > 0 || modStatusPayments.Statics.boolMultiLine || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A")
		//		{
		//			// this is a multiline so this option is not available
		//			FCMessageBox.Show("This is a multiline payment or abatement.  Save the payments first. <F11>", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Save");
		//		}
		//		else
		//		{
		//			// normal payment
		//			boolDone = true;
		//			if (txtCash.Text == "N" && txtCD.Text == "N" && Strings.InStr(1, "_", txtAcctNumber.TextMatrix(0, 0)) != 0)
		//			{
		//				if (!blnIsWriteOff)
		//				{
		//					FCMessageBox.Show("Please enter an account number unless this is going to affect cash.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Account Number Missing");
		//					return;
		//				}
		//			}
		//			if (Strings.Trim(txtPrincipal.Text) == "")
		//				txtPrincipal.Text = "0.00";
		//			if (Strings.Trim(txtCosts.Text) == "")
		//				txtCosts.Text = "0.00";
		//			if (Strings.Trim(txtInterest.Text) == "")
		//				txtInterest.Text = "0.00";
		//			// *********************  this will just make sure that the user wants to go ahead with this action
		//			if (FCConvert.ToDouble(txtPrincipal.Text) != 0 || FCConvert.ToDouble(txtInterest.Text) != 0 || FCConvert.ToDouble(txtCosts.Text) != 0)
		//			{
		//				// Write #1, Format(Time, "hh:mm:ss") & " - Get Amounts"
		//				if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A")
		//				{
		//					boolDoNotRecalc = false;
		//				}
		//				else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R")
		//				{
		//					boolDoNotRecalc = false;
		//				}
		//				else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "D")
		//				{
		//					// discount...this works without hitting F9
		//					boolDoNotRecalc = false;
		//				}
		//				else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "P")
		//				{
		//					// Write #1, Format(Time, "hh:mm:ss") & " - Payment Start"
		//					boolDoNotRecalc = false;
		//					// check to see if there are any years before the one being paid
		//					if (cmbYear.Items[cmbYear.SelectedIndex].ToString() != "Auto")
		//					{
		//						if (PaymentForDefaultYear())
		//						{
		//							// this is ok
		//						}
		//						else
		//						{
		//							// not paying the default year
		//							if (FCMessageBox.Show("You are not paying the default year.  Would you like to continue with this transaction?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Default Year") == DialogResult.Yes)
		//							{
		//								// they said yes, so allow them by
		//							}
		//							else
		//							{
		//								// they said not, exit this function
		//								return;
		//							}
		//						}
		//					}
		//					// Write #1, Format(Time, "hh:mm:ss") & " - Payment Greater Than Owed"
		//					if (modStatusPayments.PaymentGreaterThanOwed())
		//					{
		//						// this function figures out if the payment entered is greater than the years bill
		//						if (FCMessageBox.Show("The payment entered is greater than owed, would you like to continue?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Payment Amount") == DialogResult.Yes)
		//						{
		//							// yes
		//							// keep rockin
		//							if (cmbYear.Items[cmbYear.SelectedIndex].ToString() == "Auto")
		//							{
		//								// since it is an overpayment
		//								CheckForLastYearOverpayment();
		//							}
		//						}
		//						else
		//						{
		//							// no
		//							// get out of this function
		//							return;
		//						}
		//					}
		//					else
		//					{
		//						// payment is not greater than owed
		//						// keep rockin
		//					}
		//				}
		//				else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "C")
		//				{
		//					// Write #1, Format(Time, "hh:mm:ss") & " - Correction Start"
		//					boolDoNotRecalc = true;
		//					// check to see if there are any years before the one being paid
		//					if (cmbYear.Items[cmbYear.SelectedIndex].ToString() != "Auto")
		//					{
		//						if (PaymentForDefaultYear())
		//						{
		//							// this is ok
		//						}
		//						else
		//						{
		//							// not paying the default year
		//							if (FCMessageBox.Show("You are not paying the default year.  Would you like to continue with this transaction?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Default Year") == DialogResult.Yes)
		//							{
		//								// they said yes, so allow them by
		//							}
		//							else
		//							{
		//								// they said not, exit this function
		//								return;
		//							}
		//						}
		//					}
		//					// Write #1, Format(Time, "hh:mm:ss") & " - Correction Payment Greater Than Owed"
		//					if (modStatusPayments.PaymentGreaterThanOwed())
		//					{
		//						// this function figures out if the payment entered is greater than the years bill
		//						if (FCMessageBox.Show("The payment entered is greater than owed, would you like to continue?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Payment Amount") == DialogResult.Yes)
		//						{
		//							// yes
		//							// keep rockin
		//						}
		//						else
		//						{
		//							// no
		//							// get out of this function
		//							return;
		//						}
		//					}
		//					else
		//					{
		//						// payment is not greater than owed
		//						// keep rockin
		//					}
		//				}
		//				else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "S")
		//				{
		//					boolDoNotRecalc = false;
		//				}
		//				else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "U")
		//				{
		//					boolDoNotRecalc = false;
		//					clsDRWrapper rsTC = new clsDRWrapper();
		//					if (Strings.UCase(cmbYear.Items[cmbYear.SelectedIndex].ToString()) == "AUTO")
		//					{
		//						// this is not a valid payment
		//						txtPrincipal.Text = "0.00";
		//						txtInterest.Text = "0.00";
		//						txtCosts.Text = "0.00";
		//					}
		//					else if (Strings.Right(cmbYear.Items[cmbYear.SelectedIndex].ToString(), 1) == "*")
		//					{
		//						// prepayment year
		//						// kk08122014 trocls-49  This is just pulling out the Year without the bill number  lngCurrentYear = val(FormatYear(Left(cmbYear.List(cmbYear.ListIndex), Len(cmbYear.List(cmbYear.ListIndex)) - 1)))
		//						lngCurrentYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(cmbYear.Items[cmbYear.SelectedIndex].ToString()))));
		//						if (modStatusPayments.Statics.boolRE)
		//						{
		//							// check to see if there is an updated tax club record
		//							rsTC.OpenRecordset("SELECT * FROM TaxClub WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND Type = 'RE' AND TotalPayments >= TotalPaid", modExtraModules.strCLDatabase);
		//							rsCL.FindFirstRecord2("BillingYear,Account,BillingType", FCConvert.ToString(lngCurrentYear) + "," + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + ",RE", ",");
		//							// kk08122014 trocls-49  ' "BillingYear, Account, BillingType", lngCurrentYear & ", " & CurrentAccountRE & ", 'RE'", ","
		//						}
		//						else
		//						{
		//							rsTC.OpenRecordset("SELECT * FROM TaxClub WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND Type = 'PP' AND TotalPayments >= TotalPaid", modExtraModules.strCLDatabase);
		//							rsCL.FindFirstRecord2("BillingYear,Account,BillingType", FCConvert.ToString(lngCurrentYear) + "," + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + ",PP", ",");
		//							// kk08122014 trocls-49  '   "BillingYear, Account, BillingType", lngCurrentYear & ", " & CurrentAccountRE & ", 'PP'", ","
		//						}
		//						if (rsCL.NoMatch)
		//						{
		//							// create a blank bill
		//							intAns = FCMessageBox.Show("There are no bill records for this account.  Is this a prepayment?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Prepayment");
		//							if (intAns == DialogResult.Yes)
		//							{
		//								if (modStatusPayments.Statics.boolRE)
		//								{
		//									modStatusPayments.CreateBlankCLBill_18(lngCurrentYear, StaticSettings.TaxCollectionValues.LastAccountRE, "RE");
		//								}
		//								else
		//								{
		//									modStatusPayments.CreateBlankCLBill_18(lngCurrentYear, StaticSettings.TaxCollectionValues.LastAccountPP, "PP");
		//								}
		//							}
		//						}
		//						else
		//						{
		//							// rock on
		//						}
		//					}
		//					else
		//					{
		//						// regular year
		//						lngCurrentYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(cmbYear.Items[cmbYear.SelectedIndex].ToString()))));
		//						if (modStatusPayments.Statics.boolRE)
		//						{
		//							// check to see if there is an updated tax club record
		//							rsTC.OpenRecordset("SELECT * FROM TaxClub WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND Type = 'RE' AND TotalPayments >= TotalPaid", modExtraModules.strCLDatabase);
		//							rsCL.FindFirstRecord2("RateKey,Account,BillingType", "0," + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + ",RE", ",", "<>,=,=");
		//							// kk08122014 trocls-49   ' ",'RE'"
		//						}
		//						else
		//						{
		//							rsTC.OpenRecordset("SELECT * FROM TaxClub WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND Type = 'PP' AND TotalPayments >= TotalPaid", modExtraModules.strCLDatabase);
		//							rsCL.FindFirstRecord2("RateKey,Account,BillingType", "0," + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + ",PP", ",", "<>,=,=");
		//							// kk08122014 trocls-49   ' ",'PP'"
		//						}
		//					}
		//					if (rsTC.EndOfFile())
		//					{
		//						// there is no tax club for this bill
		//						if (rsCL.NoMatch)
		//						{
		//							// no bill record
		//						}
		//						else
		//						{
		//						}
		//					}
		//					else
		//					{
		//						if (((rsTC.Get_Fields_Int32("BillKey"))) == 0)
		//						{
		//							// update this taxclub record
		//							if (rsCL.NoMatch)
		//							{
		//							}
		//							else
		//							{
		//								rsTC.Edit();
		//								rsTC.Set_Fields("BillKey", rsCL.Get_Fields_Int32("ID"));
		//								// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
		//								if (((rsTC.Get_Fields("Year"))) == 0)
		//								{
		//									rsTC.Set_Fields("Year", rsCL.Get_Fields_Int32("BillingYear"));
		//								}
		//								rsTC.Set_Fields("lastpaiddate", modStatusPayments.Statics.EffectiveDate);
		//								rsTC.Update();
		//							}
		//						}
		//					}
		//				}
		//				else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "Y")
		//				{
		//					boolDoNotRecalc = false;
		//				}
		//				else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "N")
		//				{
		//					// Write-Off
		//					boolDoNotRecalc = false;
		//				}
		//				else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "F")
		//				{
		//					// Overpayment Refund
		//					boolDoNotRecalc = false;
		//				}
		//				else
		//				{
		//					boolDoNotRecalc = false;
		//					FCMessageBox.Show("No Code entered!", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "ERROR");
		//					if (cmbCode.Visible && cmbCode.Enabled)
		//					{
		//						cmbCode.Focus();
		//					}
		//					return;
		//				}
		//			}
		//			// *********************
		//			// Write #1, Format(Time, "hh:mm:ss") & " - Push Last Payment Into List"
		//			// first push the last payment into the list if it has any values in it
		//			if (FCConvert.ToDouble(txtPrincipal.Text) != 0 || FCConvert.ToDouble(txtCosts.Text) != 0 || FCConvert.ToDouble(txtInterest.Text) != 0)
		//			{
		//				// maybe prompt if there are values...
		//				// Doevents
		//				if (modStatusPayments.AddPaymentToList(vsPayments.Rows, boolDoNotRecalc) == -1)
		//				{
		//					return;
		//				}
		//			}
		//			// create the payment records
		//			// rsCreatePy.BeginTrans
		//			int lngCounter;
		//			for (lngCounter = 1; lngCounter <= vsPayments.Rows - 1; lngCounter++)
		//			{
		//				// Write #1, Format(Time, "hh:mm:ss") & " - Start Counter - " & lngCounter
		//				// ShowInterestPaidThroughDate 4 & " - " & lngCounter
		//				// get the recordset ready
		//				rsCreatePy.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountPayments + ") AS qCAP WHERE Account = 0", modExtraModules.strCLDatabase);
		//				if (Conversion.Val(vsPayments.TextMatrix(lngCounter, 10)) == 0)
		//				{
		//					// if this is 0 then there is no record created for it in the database
		//					ArrayIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(lngCounter, 9))));
		//					// this takes the array index from the last part of the list text
		//					if (ArrayIndex < 0)
		//					{
		//						// Write #1, Format(Time, "hh:mm:ss") & " - Payment Already Created"
		//						// check to see if it has already been taken care of in CreatePaymentRecord
		//						// if is has, then skip this part because you don't want to create it again
		//						if (modStatusPayments.Statics.PaymentArray[Math.Abs(FCConvert.ToInt16(ArrayIndex))].ReversalID != -1)
		//						{
		//							// this is a CHGINT line
		//							if (!modStatusPayments.CreateCHGINTPaymentRecord(FCConvert.ToInt16(ArrayIndex * -1), rsCreatePy))
		//							{
		//								boolDone = false;
		//								break;
		//							}
		//						}
		//					}
		//					else
		//					{
		//						// Write #1, Format(Time, "hh:mm:ss") & " - Create Payment Start"
		//						// regular payment line
		//						if (!modStatusPayments.CreatePaymentRecord(ArrayIndex, ref rsCreatePy, ref lngCounter))
		//						{
		//							boolDone = false;
		//							break;
		//						}
		//					}
		//					ArrayIndex = Math.Abs(FCConvert.ToInt16(ArrayIndex));
		//					// if this is not a reversal of CHGINT and there is some current interest then create a CHGINT line
		//					if (modStatusPayments.Statics.lngCHGINT == 0 && modStatusPayments.Statics.PaymentArray[ArrayIndex].ReversalID == 0 && modStatusPayments.Statics.dblCurrentInt[modStatusPayments.Statics.PaymentArray[ArrayIndex].Year - modExtraModules.DEFAULTSUBTRACTIONVALUE] != 0 && !boolIntChargedToday[modStatusPayments.Statics.PaymentArray[ArrayIndex].Year - modExtraModules.DEFAULTSUBTRACTIONVALUE] && modStatusPayments.Statics.PaymentArray[ArrayIndex].Code != "U")
		//					{
		//						// Write #1, Format(Time, "hh:mm:ss") & " - Create Payment CGHINT"
		//						// needs to add the Old Effective Interest Date to the CHGINTDate and the billkey to the field of the last payment
		//						int lngTemp = 0;
		//						int lngID = 0;
		//						int lngBK = 0;
		//						rsCreatePy.MoveFirst();
		//						lngID = FCConvert.ToInt32(rsCreatePy.Get_Fields_Int32("ID"));
		//						vsPayments.TextMatrix(lngCounter, 10, FCConvert.ToString(lngID));
		//						// Write #1, Format(Time, "hh:mm:ss") & " - Create Payment CGHINT - 1"
		//						lngTemp = modStatusPayments.CreateCHGINTLine(ArrayIndex, ref dateTemp);
		//						// Write #1, Format(Time, "hh:mm:ss") & " - Create Payment CGHINT - 2"
		//						lngBK = FCConvert.ToInt32(rsCreatePy.Get_Fields_Int32("BillKey"));
		//						rsCreatePy.Edit();
		//						// False
		//						rsCreatePy.Set_Fields("CHGINTNumber", lngTemp);
		//						rsCreatePy.Set_Fields("CHGINTDate", modStatusPayments.Statics.EffectiveDate);
		//						rsCreatePy.Update();
		//						if (!boolSaveForCR)
		//						{
		//							// set the interest PTD for non reversals with interest
		//							rsCreatePy.Execute("UPDATE BillingMaster SET InterestAppliedThroughDate = '" + Strings.Format(modStatusPayments.Statics.PaymentArray[ArrayIndex].CHGINTDate, "MM/dd/yyyy") + "' WHERE ID = " + FCConvert.ToString(lngBK), modExtraModules.strCLDatabase);
		//						}
		//						boolIntChargedToday[modStatusPayments.Statics.PaymentArray[ArrayIndex].Year - modExtraModules.DEFAULTSUBTRACTIONVALUE] = true;
		//					}
		//					else if (modStatusPayments.Statics.lngCHGINT == 0 && modStatusPayments.Statics.PaymentArray[ArrayIndex].ReversalID > -1 && !boolSaveForCR)
		//					{
		//						// this is a reversal
		//						rsCreatePy.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(modStatusPayments.Statics.PaymentArray[ArrayIndex].ID));
		//						if (!rsCreatePy.EndOfFile())
		//						{
		//							rsTemp.OpenRecordset("SELECT * FROM BillingMaster WHERE ID = " + FCConvert.ToString(modStatusPayments.Statics.PaymentArray[ArrayIndex].BillKey), modExtraModules.strCLDatabase);
		//							if (!rsTemp.EndOfFile())
		//							{
		//								rsTemp.Edit();
		//								if (rsCreatePy.Get_Fields_String("Code") != "A")
		//								{
		//									if (FCConvert.ToInt32(rsCreatePy.Get_Fields_Int32("CHGINTNumber")) == 0)
		//									{
		//										// this is when there is no applied interest line
		//										if (FCConvert.ToBoolean(rsCreatePy.Get_Fields_Boolean("Reversal")))
		//										{
		//											// if this is set to a reversal, then use the SetEIDate
		//											if (rsCreatePy.Get_Fields_DateTime("SetEIDate") is DateTime)
		//											{
		//												rsTemp.Set_Fields("InterestAppliedThroughDate", rsCreatePy.Get_Fields_DateTime("SetEIDate"));
		//											}
		//											else
		//											{
		//												if (((rsCreatePy.Get_Fields_Decimal("CurrentInterest"))) != 0 && DateAndTime.DateDiff("D", (DateTime)rsTemp.Get_Fields_DateTime("InterestAppliedThroughDate"), (DateTime)rsCreatePy.Get_Fields_DateTime("EffectiveInterestDate")) > 0)
		//												{
		//													rsTemp.Set_Fields("InterestAppliedThroughDate", rsCreatePy.Get_Fields_DateTime("EffectiveInterestDate"));
		//												}
		//											}
		//										}
		//										else if (FCConvert.ToInt32(Conversion.Val(rsCreatePy.Get_Fields_DateTime("CHGINTDate"))) == 0)
		//										{
		//											// if there is no CHGINT then find out it this is a reversal or not
		//											// If rsCPy.Get_Fields("CurrentInterest") = 0 And DateDiff("D", rsTemp.Get_Fields("InterestAppliedThroughDate"), rsCPy.Get_Fields("EffectiveInterestDate")) > 0 Then
		//											rsTemp.Set_Fields("InterestAppliedThroughDate", rsCreatePy.Get_Fields_DateTime("EffectiveInterestDate"));
		//											// End If
		//										}
		//										else
		//										{
		//											// If rsCPy.Get_Fields("CurrentInterest") = 0 And DateDiff("D", rsTemp.Get_Fields("InterestAppliedThroughDate"), rsCPy.Get_Fields("CHGINTDate")) > 0 Then
		//											rsCreatePy.Set_Fields("InterestAppliedThroughDate", rsCreatePy.Get_Fields_DateTime("CHGINTDate"));
		//											// End If
		//										}
		//									}
		//									else if (rsCreatePy.Get_Fields_Boolean("Reversal"))
		//									{
		//										// if this is set to a reversal, then use the CGHINTDate
		//										rsTemp.Set_Fields("InterestAppliedThroughDate", rsCreatePy.Get_Fields_DateTime("CHGINTDate"));
		//									}
		//								}
		//								rsTemp.Update();
		//							}
		//						}
		//					}
		//				}
		//			}
		//			// Write #1, Format(Time, "hh:mm:ss") & " - Clean Up"
		//			if (boolDone)
		//			{
		//				boolUnloadOK = true;
		//				if (boolSaveForCR)
		//				{
		//					// if this is in CR then send the information back into the Receipt Input Screen
		//					this.Hide();
		//					modUseCR.FillCLInfo();
		//					FormLoaded = false;
		//					App.DoEvents();
		//					// If Not boolAcctDetailShown Then
		//					frmReceiptInput.InstancePtr.Show(App.MainForm);
		//					// End If
		//				}
		//				else
		//				{
		//					if (modStatusPayments.Statics.gboolUsingCLReceipt)
		//					{
		//						modGlobal.PrintCLReceipt();
		//						// this will print the receipt from the payment array
		//					}
		//					Close();
		//				}
		//				boolUnloadOK = false;
		//			}
		//			else
		//			{
		//				FCMessageBox.Show("The transactions caused an error and have not been applied.", MsgBoxStyle.Critical, "ERROR");
		//				// rsCreatePy.RollBack
		//			}
		//		}
		//		// Write #1, Format(Time, "hh:mm:ss") & " - End Payment Save"
		//		// Close #1
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
		//		// ERROR_HANDLER:
		//		// Close #1
		//		FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Saving/Exit Payment");
		//	}
		//}

		//public void mnuPaymentSaveExit_Click()
		//{
		//	mnuPaymentSaveExit_Click(null, new System.EventArgs());
		//}

		//private void mnuProcessChangeAccount_Click(object sender, System.EventArgs e)
		//{
		//	// this sub will allow the user to go back to frmCLGetAccount and enter another account number
		//	bool boolGoBack = false;
		//	if (vsPayments.Rows > 1)
		//	{
		//		if (FCMessageBox.Show("Are you sure that you would like to leave this screen?  Any payments that you have added this session will be lost unless saved.", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Change Account") == DialogResult.Yes)
		//		{
		//			boolGoBack = true;
		//		}
		//		else
		//		{
		//			boolGoBack = false;
		//		}
		//	}
		//	else
		//	{
		//		boolGoBack = true;
		//	}
		//	if (boolGoBack)
		//	{
		//		Close();
		//		// Doevents
		//		//! Load frmCLGetAccount;
		//		if (boolPayment)
		//		{
		//			frmCLGetAccount.InstancePtr.txtHold.Text = "P";
		//		}
		//		else
		//		{
		//			frmCLGetAccount.InstancePtr.txtHold.Text = "S";
		//		}
		//		frmCLGetAccount.InstancePtr.Show(App.MainForm);
		//		frmCLGetAccount.InstancePtr.Focus();
		//	}
		//}

		//private void mnuProcessEffective_Click(object sender, System.EventArgs e)
		//{
		//	// This sub will set the Effective Interest Date and reset the grid
		//	string strDate = "";
		//	// strDate = InputBox("Please enter the new Effective Interest Date.  (MM/dd/yyyy)", "Effective Interest Date", Format(EffectiveDate, "MM/dd/yyyy"))
		//	ShowEffectiveInterestDateForm();
		//}



		public void GetNewEffectiveInterestDate(ref DateTime dtDate, bool boolChangeRTD = true)
		{
			// this sub will change the effective interest date and reset the grid
			// boolDoNothing is in case the user cancels, it is defaulted to update the form
			// this is a valid date
			if (modStatusPayments.Statics.EffectiveDate.ToOADate() != dtDate.ToOADate())
			{
				boolDoNotResetRTD = !boolChangeRTD;
				modStatusPayments.Statics.EffectiveDate = dtDate;
				frmRECLStatus.InstancePtr.Text = "Real Estate Account Status as of " + Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy");
				FormReset = true;
				this.Refresh();
				App.DoEvents();
				FormReset = false;
			}
		}

		//private void Reset_GRID()
		//{
		//	FormReset = true;
		//	GRID.Rows = 1;
		//	if (fraPayment.Visible == true)
		//	{
		//		FCUtils.EraseSafe(modStatusPayments.Statics.PaymentArray);
		//		vsPayments.Rows = 1;
		//	}
		//	Form_Activate();
		//}

		//private void Create_Account_Totals()
		//{
		//	int lngCounter = 0;
		//	// add a account totals row
		//	lngCounter = GRID.Rows;
		//	Add(lngCounter, -1);
		//	//FC:FINAL:AM:#i178 - merge the cells
		//	//GRID.MergeRow(lngCounter, true);
		//	GRID.MergeRow(lngCounter, true, lngColYear, lngColCode);
		//	GRID.TextMatrix(lngCounter, lngColYear, "Account Totals as of " + Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy"));
		//	//GRID.TextMatrix(lngCounter, lngColDate, GRID.TextMatrix(lngCounter, lngColYear));
		//	//GRID.TextMatrix(lngCounter, lngColReference, GRID.TextMatrix(lngCounter, lngColYear));
		//	//GRID.TextMatrix(lngCounter, lngColPeriod, GRID.TextMatrix(lngCounter, lngColYear));
		//	//GRID.TextMatrix(lngCounter, lngColCode, GRID.TextMatrix(lngCounter, lngColYear));
		//	GRID.TextMatrix(lngCounter, lngColPrincipal, Strings.Format(sumPrin, "#,##0.00"));
		//	GRID.TextMatrix(lngCounter, lngColInterest, Strings.Format(sumInt, "#,##0.00"));
		//	GRID.TextMatrix(lngCounter, lngColCosts, Strings.Format(sumCost, "#,##0.00"));
		//	if (modGlobal.Round((sumPrin + sumInt + sumCost), 2) == modGlobal.Round(sumTotal, 2))
		//	{
		//		GRID.TextMatrix(lngCounter, lngColTotalAmount, Strings.Format(sumTotal, "#,##0.00"));
		//	}
		//	else
		//	{
		//		GRID.TextMatrix(lngCounter, lngColTotalAmount, "ERROR(" + FCConvert.ToString(sumTotal) + ")");
		//	}
		//	//GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngCounter, 1, lngCounter, GRID.Cols - 1, 0xFF8080);
		//	// light blue  '&H8000& pea green        '&H407000 dark green
		//	//			GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngCounter, 1, lngCounter, GRID.Cols - 1, System.Drawing.Color.White);
		//	GRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngCounter, 1, lngCounter, GRID.Cols - 1, true);
		//	//FC:FINAL:SBE - #118 - apply style to total line
		//	GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngCounter, lngColCode + 1, lngCounter, GRID.Cols - 1, Color.FromArgb(5, 204, 71));
		//}

		private void Reset_Sum()
		{
			// this just resets the sums in order to process another account or another effective date
			sumPrin = 0;
			sumInt = 0;
			sumCost = 0;
			sumTotal = 0;
		}

		private void mnuProcessExit_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:RPU:#i122:Close the form
			//-frmRECLStatus.Close();
			frmRECLStatus.InstancePtr.Close();
		}

		public void mnuProcessExit_Click()
		{
			//mnuProcessExit_Click(mnuProcessExit, new System.EventArgs());
		}

		private void Format_PaymentGrid()
		{
			int wid = 0;
			vsPayments.Cols = 13;
			wid = vsPayments.WidthOriginal;
			vsPayments.ColWidth(0, FCConvert.ToInt32(wid * 0.078));
			// Year
			vsPayments.ColWidth(1, FCConvert.ToInt32(wid * 0.115));
			// Date
			vsPayments.ColWidth(2, FCConvert.ToInt32(wid * 0.117));
			// Reference
			vsPayments.ColWidth(3, FCConvert.ToInt32(wid * 0.05));
			// Period
			vsPayments.ColWidth(4, FCConvert.ToInt32(wid * 0.05));
			// Code
			vsPayments.ColWidth(5, FCConvert.ToInt32(wid * 0.06));
			// CD / Affect Cash
			vsPayments.ColWidth(6, FCConvert.ToInt32(wid * 0.135));
			// Principal
			vsPayments.ColWidth(7, FCConvert.ToInt32(wid * 0.13));
			// Interest
			vsPayments.ColWidth(8, FCConvert.ToInt32(wid * 0.12));
			// Costs
			vsPayments.ColWidth(9, 0);
			// Array Index
			vsPayments.ColWidth(10, 0);
			// ID Number from autonumberfield in DB
			vsPayments.ColWidth(11, 0);
			// CHGINT Number
			vsPayments.ColWidth(12, FCConvert.ToInt32(wid * 0.145));
			// Total
			//vsPayments.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vsPayments.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vsPayments.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vsPayments.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vsPayments.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//FC:FINAL:RPU:#24 align the rest of columns text
			//vsPayments.ColAlignment(6, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vsPayments.ColAlignment(7, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vsPayments.ColAlignment(8, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vsPayments.ColAlignment(12, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vsPayments.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsPayments.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsPayments.ExtendLastCol = true;
			vsPayments.TextMatrix(0, 0, "Year");
			vsPayments.TextMatrix(0, 1, "Date");
			vsPayments.TextMatrix(0, 2, "Reference");
			vsPayments.TextMatrix(0, 3, "Per");
			vsPayments.TextMatrix(0, 4, "Code");
			vsPayments.TextMatrix(0, 5, "Cash");
			vsPayments.TextMatrix(0, 6, "Principal");
			vsPayments.TextMatrix(0, 7, "Interest");
			vsPayments.TextMatrix(0, 8, "Costs");
			vsPayments.TextMatrix(0, 12, "Total");
		}

		private void mnuProcessGoTo_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:KS: #227: menus overlapped
			this.fraEditNote.Visible = false;
			if (!modExtraModules.IsThisCR())
			{
				if (boolPayment)
				{
					GoToStatus();
				}
				else
				{
					GoToPayment();
				}
			}
			//if (boolPayment)
			//{
			//	lblAcctComment.Left = 200;
			//	lblAcctComment.Top = 0;
			//}
			//else
			//{
			//	lblAcctComment.Left = 0;
			//	lblAcctComment.Top = 660;
			//}
		}

		private void txtAcctNumber_GotFocus(object sender, EventArgs e)
		{
			if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R" || txtAcctNumber.Enabled == false)
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
		}

		private void txtAcctNumber_KeyDown(object sender, KeyEventArgs e)
		{
			// left = 37
			// right = 39
			switch (FCConvert.ToInt32(e.KeyCode))
			{
				case 37:
					{
						//if (txtAcctNumber.SelStart == 0)
						{
							if (txtCash.Enabled == true)
							{
								if (txtCash.Visible && txtCash.Enabled)
								{
									txtCash.Focus();
								}
							}
							else
							{
								if (txtCD.Visible && txtCD.Enabled)
								{
									txtCD.Focus();
								}
							}
							//KeyCode = 0;
						}
						break;
					}
				case 39:
					{
						//if (txtAcctNumber.SelStart == txtAcctNumber.Text.Length - 1)
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
							//KeyCode = 0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtAcctNumber_Validate_2(bool Cancel)
		{
			txtAcctNumber_Validate(ref Cancel);
		}

		private void txtAcctNumber_Validate(ref bool Cancel)
		{
			// validation of the BD account number
			string strAcct = "";
			if (txtCash.Enabled == true)
			{
				// If txtAcctNumber.Enabled = True Then
				// strAcct = txtacctnumber.TextMatrix(0,0)
				// If AccountValidate(strAcct) Then
				// Cancel = False
				// Else
				// Cancel = True
				// MsgBox "Please enter a valid account number.", MsgBoxStyle.OkOnly |MsgBoxStyle.Information, "Invalid Account"
				// End If
				// End If
			}
			else
			{
				txtAcctNumber.Enabled = false;
			}
			clsDRWrapper rsF = new clsDRWrapper();
			boolDoNotContinueSave = false;
			if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gboolBD)
			{
				// check the fun and make sure that it matches the current type's fund
				if (Strings.InStr(1, txtAcctNumber.Text, "_") == 0 && txtAcctNumber.Text.Length > 2)
				{
					if (Strings.Left(txtAcctNumber.Text, 1) != "M")
					{
						if (modGlobal.Statics.gboolMultipleTowns)
						{
							rsF.OpenRecordset("SELECT Account1 FROM Type WHERE TypeCode = 90 AND TownKey = " + FCConvert.ToString(lngCLResCode), modExtraModules.strCRDatabase);
						}
						else
						{
							rsF.OpenRecordset("SELECT Account1 FROM Type WHERE TypeCode = 90", modExtraModules.strCRDatabase);
						}
						if (!rsF.EndOfFile())
						{
							if (modBudgetaryAccounting.GetFundFromAccount(txtAcctNumber.Text) != modBudgetaryAccounting.GetFundFromAccount(FCConvert.ToString(rsF.Get_Fields_String("Account1"))))
							{
								FCMessageBox.Show("This account's fund does not match the fund for this receipt type.", MsgBoxStyle.Exclamation, "Invalid Fund");
								boolDoNotContinueSave = true;
							}
						}
					}
				}
			}
		}

		private void txtAcctNumber_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (txtAcctNumber.CurrentCell.IsInEditMode)
			{
				clsDRWrapper rsF = new clsDRWrapper();
				boolDoNotContinueSave = false;
				if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gboolBD)
				{
					// check the fun and make sure that it matches the current type's fund
					if (Strings.InStr(1, txtAcctNumber.EditText, "_") == 0 && txtAcctNumber.EditText.Length > 2)
					{
						if (Strings.Left(txtAcctNumber.EditText, 1) != "M")
						{
							if (modGlobal.Statics.gboolMultipleTowns)
							{
								rsF.OpenRecordset("SELECT Account1 FROM Type WHERE TypeCode = 90 AND TownKey = " + FCConvert.ToString(lngCLResCode), modExtraModules.strCRDatabase);
							}
							else
							{
								rsF.OpenRecordset("SELECT Account1 FROM Type WHERE TypeCode = 90", modExtraModules.strCRDatabase);
							}
							if (!rsF.EndOfFile())
							{
								if (modBudgetaryAccounting.GetFundFromAccount(txtAcctNumber.EditText) != modBudgetaryAccounting.GetFundFromAccount(FCConvert.ToString(rsF.Get_Fields_String("Account1"))))
								{
									FCMessageBox.Show("This account's fund does not match the fund for this receipt type.", MsgBoxStyle.Exclamation, "Invalid Fund");
									boolDoNotContinueSave = true;
								}
							}
						}
					}
				}
			}
		}

		private void txtCash_TextChanged(object sender, System.EventArgs e)
		{
			if (txtCash.Text == "N")
			{
				boolDoNotContinueSave = false;
			}
		}

		private void txtCash_DoubleClick(object sender, System.EventArgs e)
		{
			if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R")
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
			else
			{
				if (txtCash.Text == "N")
				{
					txtCash.Text = "Y";
					boolDoNotContinueSave = false;
				}
				else
				{
					txtCash.Text = "N";
				}
				CheckCash();
			}
		}

		public void txtCash_DblClick()
		{
			txtCash_DoubleClick(txtCash, new System.EventArgs());
		}

		private void txtCash_Enter(object sender, System.EventArgs e)
		{
			if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R")
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
		}

		private void txtCash_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// left = 37
			// right = 39
			if (FCConvert.ToInt32(KeyCode) == 37)
			{
				if (txtCD.Visible && txtCD.Enabled)
				{
					txtCD.Focus();
				}
				KeyCode = 0;
			}
			else if (FCConvert.ToInt32(KeyCode) == 39)
			{
				if (txtCash.Text == "N")
				{
					if (txtAcctNumber.Visible && txtPrincipal.Enabled)
					{
						txtAcctNumber.Focus();
					}
				}
				else
				{
					if (txtPrincipal.Visible && txtPrincipal.Enabled)
					{
						txtPrincipal.Focus();
					}
				}
				KeyCode = 0;
			}
			else if (KeyCode == Keys.Space)
			{
				txtCash_DblClick();
			}
			else if (KeyCode == Keys.N)
			{
				txtCash.Text = "N";
			}
			else if (KeyCode == Keys.Y)
			{
				txtCash.Text = "Y";
			}
			else
			{
				KeyCode = 0;
			}
			CheckCash();
		}

		private void txtCash_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.N) || (KeyAscii == Keys.Y) || (KeyAscii == Keys.Space) || (FCConvert.ToInt32(KeyAscii) == 8))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCD_TextChanged(object sender, System.EventArgs e)
		{
			if (txtCD.Text == "N")
			{
				boolDoNotContinueSave = false;
			}
		}

		private void txtCD_DoubleClick(object sender, System.EventArgs e)
		{
			if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R")
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
			else
			{
				if (txtCD.Text == "N")
				{
					txtCD.Text = "Y";
					boolDoNotContinueSave = false;
				}
				else
				{
					txtCD.Text = "N";
				}
				CheckCD();
			}
		}

		public void txtCD_DblClick()
		{
			txtCD_DoubleClick(txtCD, new System.EventArgs());
		}

		private void txtCD_Enter(object sender, System.EventArgs e)
		{
			if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R")
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
		}

		private void txtCD_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (FCConvert.ToInt32(KeyCode) == 37)
			{
				// left = 37
				cmbCode.Focus();
				KeyCode = 0;
			}
			else if (FCConvert.ToInt32(KeyCode) == 39)
			{
				// right = 39
				if (txtCD.Text == "N")
				{
					if (txtCash.Visible && txtCash.Enabled)
					{
						txtCash.Focus();
					}
				}
				else
				{
					if (txtPrincipal.Visible && txtPrincipal.Enabled)
					{
						txtPrincipal.Focus();
					}
				}
				KeyCode = 0;
			}
			else if (KeyCode == Keys.Space)
			{
				txtCD_DblClick();
			}
			else if (KeyCode == Keys.N)
			{
				txtCD.Text = "N";
			}
			else if (KeyCode == Keys.Y)
			{
				txtCD.Text = "Y";
			}
			else
			{
				KeyCode = 0;
			}
			CheckCD();
		}

		private void txtCD_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.N) || (KeyAscii == Keys.Y) || (KeyAscii == Keys.Space) || (FCConvert.ToInt32(KeyAscii) == 8))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtComments_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int KeyCode = FCConvert.ToInt32(e.KeyCode);
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == 37)
			{
				// left = 37
				if (txtComments.SelectionStart == 0)
				{
					if (txtCosts.Visible && txtCosts.Enabled)
					{
						txtCosts.Focus();
					}
					KeyCode = 0;
				}
			}
			else if (KeyCode == 39)
			{
				// right = 39
				if (txtComments.SelectionStart == txtComments.Text.Length)
				{
					vsPayments.Focus();
					KeyCode = 0;
				}
			}
		}

		private void txtCosts_Enter(object sender, System.EventArgs e)
		{
			txtCosts.SelectionStart = 0;
			txtCosts.SelectionLength = txtCosts.Text.Length;
		}

		private void txtCosts_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int KeyCode = FCConvert.ToInt32(e.KeyCode);
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == 37)
			{
				// left = 37
				if (txtCosts.SelectionStart == 0)
				{
					txtInterest.Focus();
					KeyCode = 0;
				}
			}
			else if (KeyCode == 39)
			{
				// right = 39
				if (txtCosts.SelectionStart == txtCosts.Text.Length)
				{
					txtComments.Focus();
					KeyCode = 0;
				}
			}
		}

		private void txtCosts_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			// If Left(cmbCode.List(cmbCode.ListIndex), 1) = "R" Then
			// KeyAscii = 0
			// Else
			if ((KeyAscii == 8) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else if (KeyAscii == 46)
			{
				// decimal point
				lngDecPlace = Strings.InStr(1, txtCosts.Text, ".");
				if (lngDecPlace != 0)
				{
					if (txtCosts.SelectionStart < lngDecPlace && txtCosts.SelectionLength + txtCosts.SelectionStart >= lngDecPlace)
					{
						// if the decimal place is being highlighted and will be written over, then allow it
					}
					else
					{
						// if there is already a decimal point then stop others
						KeyAscii = 0;
					}
				}
			}
			else if (KeyAscii == 45)
			{
				// minus
				KeyAscii = 0;
				if (Conversion.Val(txtCosts.Text) == 0)
				{
					txtCosts.Text = "0.00";
				}
				txtCosts.Text = Strings.Format(Math.Abs(FCConvert.ToDouble(txtCosts.Text)) * -1, "#,##0.00");
				txtCosts.SelectionStart = 0;
				txtCosts.SelectionLength = txtCosts.Text.Length;
			}
			else if (KeyAscii == 43)
			{
				// plus
				KeyAscii = 0;
				if (Conversion.Val(txtCosts.Text) == 0)
				{
					txtCosts.Text = "0.00";
				}
				txtCosts.Text = Strings.Format(Math.Abs(FCConvert.ToDouble(txtCosts.Text)), "#,##0.00");
				txtCosts.SelectionStart = 0;
				txtCosts.SelectionLength = txtCosts.Text.Length;
			}
			else if ((KeyAscii == 99) || (KeyAscii == 67))
			{
				// c, C - this will clear the box
				KeyAscii = 0;
				txtCosts.Text = "0.00";
				txtCosts.SelectionStart = 0;
				txtCosts.SelectionLength = 4;
			}
			else
			{
				KeyAscii = 0;
			}
			// End If
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtCosts_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (txtCosts.Text != "")
				{
					if (FCConvert.ToDouble(txtCosts.Text) > 9999.99)
					{
						FCMessageBox.Show("This field cannot contain a value exceeding $9,999.99.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Input Error");
						txtCosts.Text = Strings.Format(9999.99, "#,##0.00");
					}
					else if (FCConvert.ToDouble(txtCosts.Text) < -9999.99)
					{
						FCMessageBox.Show("This field cannot contain a value exceeding $-9,999.99.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Input Error");
						txtCosts.Text = Strings.Format(-9999.99, "#,##0.00");
					}
					else
					{
						txtCosts.Text = Strings.Format(FCConvert.ToDouble(txtCosts.Text), "#,##0.00");
					}
				}
				else
				{
					txtCosts.Text = "0.00";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				e.Cancel = true;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Invalid Data");
			}
		}

		private void txtDiscDisc_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
                //FC:FINAL:SBE - global fix - validate event should not be triggered twice (issue #4605)
                //txtDiscPrin_Validate(false);
                Support.SendKeys("{TAB}");
            }
            else if ((FCConvert.ToInt32(KeyAscii) >= 48 && FCConvert.ToInt32(KeyAscii) <= 57) || (FCConvert.ToInt32(KeyAscii) == 46) || (FCConvert.ToInt32(KeyAscii) == 8))
			{
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDiscDisc_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will check for the amount to be more than it should be
				e.Cancel = true;
				if ((FCConvert.ToDouble(txtDiscDisc.Text) + FCConvert.ToDouble(txtDiscPrin.Text)) > FCConvert.ToDouble(lblDiscHiddenTotal.Text) && FCConvert.ToDouble(lblDiscHiddenTotal.Text) > 0)
				{
					DialogResult messageBoxResult = FCMessageBox.Show("The amount entered is greater than the amount of principal owed for this year.  Do you wish to continue?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Validation");
					if (messageBoxResult == DialogResult.Yes)
					{
						// continue
						e.Cancel = false;
					}
					else if (messageBoxResult == DialogResult.No)
					{
						// Cancel = False
					}
					else if (messageBoxResult == DialogResult.Cancel)
					{
						ShowDiscountFrame(false);
					}
				}
				else
				{
					e.Cancel = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				e.Cancel = true;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Invalid Data");
			}
		}

		private void txtDiscPrin_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
                //FC:FINAL:SBE - global fix - validate event should not be triggered twice (issue #4605)
                //txtDiscPrin_Validate(false);
                Support.SendKeys("{TAB}");
            }
            else if ((FCConvert.ToInt32(KeyAscii) >= 48 && FCConvert.ToInt32(KeyAscii) <= 57) || (FCConvert.ToInt32(KeyAscii) == 46) || (FCConvert.ToInt32(KeyAscii) == 8))
			{
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDiscPrin_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// checks for a positive value in principal
			// VBto upgrade warning: dblPrin As double	OnWrite(short, double, double)
			double dblPrin = 0;
			double dblDisc = 0;
			bool boolGo;
			boolGo = true;
			if (txtDiscPrin.ReadOnly != true)
			{
				if (Conversion.Val(txtDiscPrin.Text) == 0)
				{
					dblPrin = 0;
					txtDiscPrin.Text = "0.00";
				}
				else
				{
					dblPrin = modGlobal.Round(FCConvert.ToDouble(txtDiscPrin.Text), 2);
				}
				// MAL@20080402: Add support for pre-payment discounts (no tax billed)
				// Tracker Reference: 12996
				if (FCConvert.ToDouble(lblOriginalTax.Text) == 0 && dblPrin > 0)
				{
					dblDisc = modCLDiscount.CalculateDiscount(dblPrin, 0, true);
				}
				else if (FCConvert.ToDouble(lblOriginalTax.Text) > 0)
				{
					dblDisc = modCLDiscount.CalculateDiscount(FCConvert.ToDouble(lblOriginalTax.Text), dblPrin, true);
				}
				else
				{
					dblDisc = 0;
				}
				if (Conversion.Val(txtDiscPrin.Text) > 0)
				{
					if ((dblDisc + dblPrin) > FCConvert.ToDouble(lblDiscHiddenTotal.Text) && FCConvert.ToDouble(lblDiscHiddenTotal.Text) > 0)
					{
						// If MsgBox("The amount entered is greater than the amount of principal owed for this year.  Do you wish to continue?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Validation") <> 7 Then
						// boolGo = False
						// Else
						// ShowDiscountFrame
						// End If
						DialogResult messageBoxResult = FCMessageBox.Show("The amount entered is greater than the amount of principal owed for this year.  Do you wish to continue?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Validation");
						if (messageBoxResult == DialogResult.Yes)
						{
							// continue
							boolGo = false;
						}
						else if (messageBoxResult == DialogResult.No)
						{
							e.Cancel = false;
						}
						else if (messageBoxResult == DialogResult.Cancel)
						{
							ShowDiscountFrame(false);
						}
					}
					// If boolGo Then
					// dblPrin = CCur(txtDiscPrin.Text)
					// dblDisc = CalculateDiscount(0, dblPrin)
					// txtDiscDisc.Text = Format(Round(dblDisc, 2), "#,##0.00")
					// lblDiscTotal = Format(Round(dblDisc + dblPrin, 2), "#,##0.00")
					// End If
				}
				else
				{
					// Cancel = True
				}
				// MAL@20080429: Move this section to always update the fields
				// Tracker Reference: 12996
				if (boolGo)
				{
					dblPrin = FCConvert.ToDouble(txtDiscPrin.Text);
					// dblDisc = CalculateDiscount(0, dblPrin)
					txtDiscDisc.Text = Strings.Format(modGlobal.Round(dblDisc, 2), "#,##0.00");
					lblDiscTotal.Text = Strings.Format(modGlobal.Round(dblDisc + dblPrin, 2), "#,##0.00");
				}
			}
		}

		public void txtDiscPrin_Validate(bool Cancel)
		{
			txtDiscPrin_Validating(txtDiscPrin, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtInterest_Enter(object sender, System.EventArgs e)
		{
			txtInterest.SelectionStart = 0;
			txtInterest.SelectionLength = txtInterest.Text.Length;
		}

		private void txtInterest_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int KeyCode = FCConvert.ToInt32(e.KeyCode);
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == 37)
			{
				// left = 37
				if (txtInterest.SelectionStart == 0)
				{
					if (txtPrincipal.Visible && txtPrincipal.Enabled)
					{
						txtPrincipal.Focus();
					}
					KeyCode = 0;
				}
			}
			else if (KeyCode == 39)
			{
				// right = 39
				if (txtInterest.SelectionStart == txtInterest.Text.Length)
				{
					if (txtCosts.Visible && txtCosts.Enabled)
					{
						txtCosts.Focus();
					}
					KeyCode = 0;
				}
			}
		}

		private void txtInterest_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A")
			{
				switch (KeyAscii)
				{
					case 99:
					case 67:
					case 8:
						{
							// c, C, 0 - this will clear the box
							KeyAscii = 0;
							txtInterest.Text = "0.00";
							txtInterest.SelectionStart = 0;
							txtInterest.SelectionLength = 4;
							break;
						}
				}
				//end switch
			}
			else
			{
				if ((KeyAscii == 8) || (KeyAscii >= 48 && KeyAscii <= 57))
				{
					// do nothing
				}
				else if (KeyAscii == 46)
				{
					// decimal point
					lngDecPlace = Strings.InStr(1, txtInterest.Text, ".");
					if (lngDecPlace != 0)
					{
						if (txtInterest.SelectionStart < lngDecPlace && txtInterest.SelectionLength + txtInterest.SelectionStart >= lngDecPlace)
						{
							// if the decimal place is being highlighted and will be written over, then allow it
						}
						else
						{
							// if there is already a decimal point then stop others
							KeyAscii = 0;
						}
					}
				}
				else if (KeyAscii == 45)
				{
					// minus
					KeyAscii = 0;
					if (Conversion.Val(txtInterest.Text) == 0)
					{
						txtInterest.Text = "0.00";
					}
					//FC:FINAL:SBE - #57 - remove the '-' sign before converting text value
					txtInterest.Text = txtInterest.Text.Replace('-', '\0');
					txtInterest.Text = Strings.Format(Math.Abs(FCConvert.ToDouble(txtInterest.Text)) * -1, "#,##0.00");
					txtInterest.SelectionStart = 0;
					txtInterest.SelectionLength = txtInterest.Text.Length;
				}
				else if (KeyAscii == 43)
				{
					// plus
					KeyAscii = 0;
					if (Conversion.Val(txtInterest.Text) == 0)
					{
						txtInterest.Text = "0.00";
					}
					//FC:FINAL:SBE - #57 - remove the '+' sign before converting text value
					txtInterest.Text = txtInterest.Text.Replace('+', '\0');
					txtInterest.Text = Strings.Format(Math.Abs(FCConvert.ToDouble(txtInterest.Text)), "#,##0.00");
					txtInterest.SelectionStart = 0;
					txtInterest.SelectionLength = txtInterest.Text.Length;
				}
				else if ((KeyAscii == 99) || (KeyAscii == 67))
				{
					// c, C - this will clear the box
					KeyAscii = 0;
					txtInterest.Text = "0.00";
					txtInterest.SelectionStart = 0;
					txtInterest.SelectionLength = 4;
				}
				else
				{
					KeyAscii = 0;
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtInterest_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (txtInterest.Text != "")
				{
					if (FCConvert.ToDouble(txtInterest.Text) > 9999999.99)
					{
						FCMessageBox.Show("This field cannot contain a value exceeding $9,999,999.99.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Input Error");
						txtInterest.Text = Strings.Format(9999999.99, "#,##0.00");
					}
					else if (FCConvert.ToDouble(txtInterest.Text) < -9999999.99)
					{
						FCMessageBox.Show("This field cannot contain a value exceeding $-9,999,999.99.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Input Error");
						txtInterest.Text = Strings.Format(-9999999.99, "#,##0.00");
					}
					else
					{
						txtInterest.Text = Strings.Format(FCConvert.ToDouble(txtInterest.Text), "#,##0.00");
					}
				}
				else
				{
					txtInterest.Text = "0.00";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				e.Cancel = true;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Invalid Data");
			}
		}

		private void txtNote_TextChanged(object sender, System.EventArgs e)
		{
			intCharCount = txtNote.Text.Length;
			lblRemainChars.Text = FCConvert.ToString((255 - intCharCount));
			if (FCConvert.ToInt16(FCConvert.ToDouble(lblRemainChars.Text)) <= 0)
			{
				lblRemainChars.Text = FCConvert.ToString(0);
			}
		}

		private void txtNote_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			intCharCount = txtNote.Text.Length;
			lblRemainChars.Text = FCConvert.ToString((255 - intCharCount));
			if (FCConvert.ToInt16(FCConvert.ToDouble(lblRemainChars.Text)) <= 0)
			{
				lblRemainChars.Text = FCConvert.ToString(0);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtPrincipal_Enter(object sender, System.EventArgs e)
		{
			txtPrincipal.SelectionStart = 0;
			txtPrincipal.SelectionLength = txtPrincipal.Text.Length;
		}

		private void txtPrincipal_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int KeyCode = FCConvert.ToInt32(e.KeyCode);
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == 37)
			{
				// left = 37
				if (txtPrincipal.SelectionStart == 0)
				{
					if (txtCD.Text == "N")
					{
						if (txtCash.Text == "N")
						{
							txtAcctNumber.Focus();
						}
						else
						{
							txtAcctNumber.Enabled = false;
							if (txtCash.Visible && txtCash.Enabled)
							{
								txtCash.Focus();
							}
						}
					}
					else
					{
						if (txtCD.Visible && txtCD.Enabled)
						{
							txtCD.Focus();
						}
					}
					KeyCode = 0;
				}
			}
			else if (KeyCode == 39)
			{
				// right = 39
				if (txtPrincipal.SelectionStart == txtPrincipal.Text.Length)
				{
					txtInterest.Focus();
					KeyCode = 0;
				}
			}
		}

		private void txtPrincipal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if ((KeyAscii == 8) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else if (KeyAscii == 46)
			{
				// decimal point
				lngDecPlace = Strings.InStr(1, txtPrincipal.Text, ".");
				if (lngDecPlace != 0)
				{
					if (txtPrincipal.SelectionStart < lngDecPlace && txtPrincipal.SelectionLength + txtPrincipal.SelectionStart >= lngDecPlace)
					{
						// if the decimal place is being highlighted and will be written over, then allow it
					}
					else
					{
						// if there is already a decimal point then stop others
						KeyAscii = 0;
					}
				}
			}
			else if (KeyAscii == 45)
			{
				// minus
				KeyAscii = 0;
				if (Conversion.Val(txtPrincipal.Text) == 0)
				{
					txtPrincipal.Text = "0.00";
				}
				txtPrincipal.Text = Strings.Format(Math.Abs(FCConvert.ToDouble(txtPrincipal.Text)) * -1, "#,##0.00");
				txtPrincipal.SelectionStart = 0;
				txtPrincipal.SelectionLength = txtPrincipal.Text.Length;
			}
			else if (KeyAscii == 43)
			{
				// plus
				KeyAscii = 0;
				if (Conversion.Val(txtPrincipal.Text) == 0)
				{
					txtPrincipal.Text = "0.00";
				}
				txtPrincipal.Text = Strings.Format(Math.Abs(FCConvert.ToDouble(txtPrincipal.Text)), "#,##0.00");
				txtPrincipal.SelectionStart = 0;
				txtPrincipal.SelectionLength = txtPrincipal.Text.Length;
			}
			else if ((KeyAscii == 99) || (KeyAscii == 67))
			{
				// c, C - this will clear the box
				KeyAscii = 0;
				txtPrincipal.Text = "0.00";
				txtPrincipal.SelectionStart = 0;
				txtPrincipal.SelectionLength = 4;
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtPrincipal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				double dblMaxInt/*unused?*/;
				// this is validaiting the amount entered
				if (txtPrincipal.Text != "")
				{
					if (FCConvert.ToDouble(txtPrincipal.Text) > 99999999.99)
					{
						FCMessageBox.Show("This field cannot contain a value exceeding $99,999,999.99.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Input Error");
						txtPrincipal.Text = Strings.Format(99999999.99, "#,##0.00");
					}
					else if (FCConvert.ToDouble(txtPrincipal.Text) < -99999999.99)
					{
						FCMessageBox.Show("This field cannot contain a value exceeding $-99,999,999.99.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Input Error");
						txtPrincipal.Text = Strings.Format(-99999999.99, "#,##0.00");
					}
					else
					{
						txtPrincipal.Text = Strings.Format(FCConvert.ToDouble(txtPrincipal.Text), "#,##0.00");
					}
				}
				else
				{
					txtPrincipal.Text = "0.00";
				}
				// this will check to see if this payment is an abatement, if so then it will calculate the interest
				// automatically and the user can either keep it or zero the interest out
				if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A")
				{
					// calculate the interest
					txtInterest.Text = Strings.Format(CalculateAbatementInterestByPayments_2(FCConvert.ToDouble(txtPrincipal.Text)), "#,##0.00");
					// , dblMaxInt
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				e.Cancel = true;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Invalid Data");
			}
		}

		private void txtReference_Enter(object sender, System.EventArgs e)
		{
			boolRefKeystroke = false;
		}

		private void txtReference_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			bool boolTest = false;
			if (FCConvert.ToInt32(KeyCode) == 37)
			{
				// left = 37
				if (txtReference.SelectionStart == 0)
				{
					if (txtTransactionDate.Visible && txtTransactionDate.Enabled)
					{
						txtTransactionDate.Focus();
					}
					KeyCode = 0;
				}
			}
			else if (FCConvert.ToInt32(KeyCode) == 39)
			{
				// right = 39
				if (txtReference.SelectionStart == txtReference.Text.Length)
				{
					boolRefKeystroke = true;
					cmbPeriod.Focus();
					KeyCode = 0;
				}
			}
			else if (KeyCode == Keys.Return)
			{
				boolTest = false;
				txtReference_Validate(ref boolTest);
				if (boolTest)
				{
				}
				else
				{
					if (txtPrincipal.Visible && txtPrincipal.Enabled)
					{
						txtPrincipal.Focus();
					}
					else
					{
						txtInterest.Focus();
					}
					KeyCode = 0;
				}
			}
		}

		private void txtReference_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// catches reserved words
			if ((Strings.UCase(Strings.Trim(txtReference.Text)) == "CHGINT") || (Strings.UCase(Strings.Trim(txtReference)) == "CNVRSN") || (Strings.UCase(Strings.Trim(txtReference.Text)) == "REVRSE"))
			{
				FCMessageBox.Show(Strings.UCase(Strings.Trim(txtReference.Text)) + " is a reserved reference string.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "ERROR");
				e.Cancel = true;
			}
		}

		public void txtReference_Validate(ref bool Cancel)
		{
			txtReference_Validating(txtReference, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtTotalPendingDue_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			//FC:FINAL:SBE - #i263 - changing KeyChar on server side is not working in WiseJ. Set ReadOnly property in designer.cs will have the same result
			//http://lab.iceteagroup.com/Gemini/project/FC/13/item/8423
			//int KeyAscii = Strings.Asc(e.KeyChar);
			//// this will not allow any input to this text box
			//KeyAscii = 0;
			//e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtTransactionDate_KeyDown(ref Keys KeyCode, ref short Shift)
		{
			if (FCConvert.ToInt32(KeyCode) == 37)
			{
				// left = 37
				if (txtTransactionDate.SelStart == 0)
				{
					cmbYear.Focus();
					KeyCode = 0;
				}
			}
			else if (FCConvert.ToInt32(KeyCode) == 39)
			{
				// right = 39
				if (txtTransactionDate.SelStart == txtTransactionDate.Text.Length)
				{
					txtReference.Focus();
					KeyCode = 0;
				}
			}
			else if (KeyCode == Keys.Return)
			{
				if (txtReference.Visible && txtReference.Enabled)
				{
					txtReference.Focus();
				}
				else
				{
					Support.SendKeys("{tab}", false);
				}
			}
			else if ((KeyCode == Keys.Back) || (KeyCode == Keys.Delete) || (FCConvert.ToInt32(KeyCode) >= 48 && FCConvert.ToInt32(KeyCode) <= 57) || (FCConvert.ToInt32(KeyCode) == 191) || (FCConvert.ToInt32(KeyCode) == 111) || (FCConvert.ToInt32(KeyCode) >= 96 && FCConvert.ToInt32(KeyCode) <= 105))
			{
				// backspace, delete, 0-9, "/", numberpad "/", 0-9 all can pass
			}
			else
			{
				KeyCode = 0;
			}
		}

		private void txtTransactionDate_Validate(ref bool Cancel)
		{
			// If txtTransactionDate = "" Then txtTransactionDate = EffectiveDate
			// If Not IsDate(txtTransactionDate.Text) Then
			// MsgBox "Please enter a valid date.", MsgBoxStyle.OkOnly |MsgBoxStyle.Information, "Incorrect Date Format"
			// txtTransactionDate.SelStart = 0
			// txtTransactionDate.SelLength = Len(txtTransactionDate.Text)
			// Cancel = True
			// Else
			// txtTransactionDate.Text = Format(txtTransactionDate.Text, "MM/dd/yyyy")
			// End If
			if (Conversion.Val(Strings.Left(txtTransactionDate.Text, 2)) > 12)
			{
				Cancel = true;
				FCMessageBox.Show("Please use the date format MM/dd/yyyy.", MsgBoxStyle.Information, "Invalid Date Format");
			}
		}

		private void txtWOCosts_TextChanged(object sender, System.EventArgs e)
		{
			UpdateWOTotals();
		}

		private void txtWOCosts_Enter(object sender, System.EventArgs e)
		{
			txtWOCosts.SelectionStart = 0;
			txtWOCosts.SelectionLength = txtWOCosts.Text.Length;
		}

		private void txtWOCosts_Leave(object sender, System.EventArgs e)
		{
			UpdateTextFormats();
		}

		private void txtWOCurInterest_TextChanged(object sender, System.EventArgs e)
		{
			UpdateWOTotals();
		}

		private void txtWOCurInterest_Enter(object sender, System.EventArgs e)
		{
			txtWOCurInterest.SelectionStart = 0;
			txtWOCurInterest.SelectionLength = txtWOCurInterest.Text.Length;
		}

		private void txtWOCurInterest_Leave(object sender, System.EventArgs e)
		{
			UpdateTextFormats();
		}

		private void txtWOPLI_TextChanged(object sender, System.EventArgs e)
		{
			UpdateWOTotals();
		}

		private void txtWOPLI_Enter(object sender, System.EventArgs e)
		{
			txtWOPLI.SelectionStart = 0;
			txtWOPLI.SelectionLength = txtWOPLI.Text.Length;
		}

		private void txtWOPLI_Leave(object sender, System.EventArgs e)
		{
			UpdateTextFormats();
		}

		private void txtWOPrincipal_TextChanged(object sender, System.EventArgs e)
		{
			UpdateWOTotals();
		}

		private void txtWOPrincipal_Enter(object sender, System.EventArgs e)
		{
			txtWOPrincipal.SelectionStart = 0;
			txtWOPrincipal.SelectionLength = txtWOPrincipal.Text.Length;
		}

		private void txtWOPrincipal_Leave(object sender, System.EventArgs e)
		{
			UpdateTextFormats();
		}

		//private void vsPayments_KeyDown(object sender, KeyEventArgs e)
		//{
		//	int l;
		//	int KeyCode = e.KeyValue;
		//	if (e.KeyCode == Keys.Delete)
		//	{
		//		// If vsPayments.TextMatrix(vsPayments.Row, 2) <> "CHGINT" And vsPayments.TextMatrix(vsPayments.Row, 2) <> "EARNINT" Then
		//		if (FCMessageBox.Show("Are you sure that you would like to delete this payment?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Delete Payment Record") == DialogResult.Yes)
		//		{
		//			if (vsPayments.TextMatrix(vsPayments.Row, 2) == "AUTO" || vsPayments.TextMatrix(vsPayments.Row, 2) == "OVERPAY" || vsPayments.TextMatrix(vsPayments.Row, 2) == "PREPAY-A")
		//			{
		//				// if this is an automatic payment, then delete all the automatic payments
		//				for (l = vsPayments.Rows - 1; l >= 1; l--)
		//				{
		//					// If vsPayments.TextMatrix(l, 2) = "AUTO" Or vsPayments.TextMatrix(l, 2) = "OVERPAY" Or vsPayments.TextMatrix(vsPayments.Row, 2) = "PREPAY-A" Then
		//					if (vsPayments.TextMatrix(l, 2) == "AUTO" || vsPayments.TextMatrix(l, 2) == "OVERPAY" || l == vsPayments.Row)
		//					{
		//						DeletePaymentRow(l);
		//					}
		//				}
		//			}
		//			else if (vsPayments.Row != 0)
		//			{
		//				DeletePaymentRow(vsPayments.Row);
		//			}
		//			modStatusPayments.CheckAllPending();
		//			// this resets all of the pending markers
		//		}
		//		// End If
		//	}
		//	KeyCode = 0;
		//}

		private void GoToStatus()
		{
			fraPayment.Visible = false;
			if (modStatusPayments.Statics.boolRE)
			{
				fraStatusLabels.Visible = true;
				GRID.Top = fraStatusLabels.Top + fraStatusLabels.Height;
			}
			else
			{
				fraPPStatusLabels.Visible = true;
				GRID.Top = fraPPStatusLabels.Top + fraPPStatusLabels.Height;
			}
			GRID.Height = FCConvert.ToInt32(frmRECLStatus.InstancePtr.ClientArea.Height - GRID.Top - (frmRECLStatus.InstancePtr.ClientArea.Height * 0.09));
			mnuPayment.Visible = false;
			cmdPaymentPreview.Visible = false;
			mnuProcessGoTo.Text = strPaymentString;
			GRID.Focus();
			btnProcess.Enabled = false;
			btnSaveExit.Enabled = false;
			cmdFileOptionsPrint.Visible = false;
			cmdFileOptionsPrint.Enabled = false;
			boolPayment = false;
		}

		//private void GoToPayment()
		//{
		//	fraStatusLabels.Visible = false;
		//	fraPPStatusLabels.Visible = false;
		//	fraPayment.Visible = true;
		//	mnuPayment.Visible = true;
		//	cmdPaymentPreview.Visible = true;
		//	vsPayments.Visible = true;
		//	mnuProcessGoTo.Text = strStatusString;
		//	// Doevents
		//	GRID.Top = 50;
		//	GRID.Height = FCConvert.ToInt32(frmRECLStatus.InstancePtr.ClientArea.Height * 0.50);
		//	//fraPayment.Left = FCConvert.ToInt32((this.Width - fraPayment.Width) / 3.0);
		//	fraPayment.Top = GRID.Bottom + 20;
		//	frmRECLStatus.InstancePtr.Refresh();
		//	modStatusPayments.FillPaymentComboBoxes();
		//	modStatusPayments.ResetPaymentFrame();
		//	Format_PaymentGrid();
		//	modStatusPayments.FillPendingTransactions();
		//	modStatusPayments.Format_vsPeriod();
		//	txtReference.Focus();
		//	btnProcess.Enabled = true;
		//	btnSaveExit.Enabled = true;
		//	cmdFileOptionsPrint.Visible = true;
		//	cmdFileOptionsPrint.Enabled = true;
		//	boolPayment = true;
		//}

		//private bool NegativeBillValues()
		//{
		//	bool NegativeBillValues = false;
		//	// this will return true if any year has negative values
		//	int lngRow;
		//	//FC:FINAL:DDU:#i142 set to 1 for findRow method
		//	lngRow = 1;
		//	do
		//	{
		//		lngRow = GRID.FindRow("=", lngRow, lngColGridCode) + 1;
		//		if (lngRow > 0)
		//		{
		//			if (FCConvert.ToDouble(GRID.TextMatrix(lngRow - 1, lngColTotalAmount)) < 0 && LastParentRow(lngRow - 1) > 1)
		//				NegativeBillValues = true;
		//		}
		//	}
		//	while (!(NegativeBillValues == true || GRID.FindRow("=", lngRow, lngColGridCode) == -1));
		//	return NegativeBillValues;
		//}

		//private void AffectAllNegativeBillValues()
		//{
		//	// this will create a payment against the negative value to zero it out and also
		//	// an equal amount against any newer bills, leaving the extra in the last year
		//	int lngRow;
		//	double dblNegTotal = 0;
		//	// VBto upgrade warning: strYear As Variant --> As string
		//	string strYear = "";
		//	// collapse all of the grid
		//	modGlobalFunctions.CollapseRows(0, GRID, 1);
		//	// start at the oldest year and check for negative balances
		//	for (lngRow = GRID.Rows - 2; lngRow >= 2; lngRow--)
		//	{
		//		// kk10172014 trocl-1150 Change from "To 1" to "To 2" since we don't care if there's already a credit on the most recent bill
		//		if (GRID.RowOutlineLevel(lngRow) == 1)
		//		{
		//			// check for the master row
		//			if (Conversion.Val(GRID.TextMatrix(lngRow, lngColTotalAmount)) != 0)
		//			{
		//				// make sure that the row is not text
		//				if (FCConvert.ToDouble(GRID.TextMatrix(lngRow, lngColTotalAmount)) < 0)
		//				{
		//					// check for a value that below zero
		//					// dblNegTotal = dblNegTotal + CDbl(GRID.TextMatrix(lngRow, 9))
		//					// payment to zero out the year
		//					strYear = modExtraModules.FormatYear(GRID.TextMatrix(lngRow, lngColYear));
		//					if (Conversion.Val(strYear) <= Conversion.Val(modGlobal.Statics.gstrLastYearBilled))
		//					{
		//						dblNegTotal += AffectBillYear_8(true, FCConvert.ToInt16(FCConvert.ToDouble(strYear)), lngRow);
		//					}
		//				}
		//				else if (dblNegTotal < 0)
		//				{

		//				}
		//			}
		//		}
		//	}
		//	// kk09192014 trocl-1150  Combine multiple credit balances into one credit then apply it forward, otherwise multiple AUTO payments are detected
		//	if (dblNegTotal < 0)
		//	{
		//		ApplyCreditForward(ref dblNegTotal);
		//	}
		//}

		//private double AffectBillYear_8(bool boolNegative, short intYear, int lngRow)
		//{
		//	return AffectBillYear(ref boolNegative, ref intYear, ref lngRow);
		//}

		//private double AffectBillYear(ref bool boolNegative, ref short intYear, ref int lngRow)
		//{
		//	double AffectBillYear = 0;
		//	// kk09192014 trocl-1150  Change this function to just add payments to offset the credit balance on the current year
		//	// this function will create a bill record to zero out the payments are best as possible
		//	// it will use as much as needed by the year to be payed off, or as much as it has in dblNegValue
		//	clsDRWrapper rs = new clsDRWrapper();
		//	clsDRWrapper rsLien = new clsDRWrapper();
		//	string strSQL = "";
		//	double dblPrinNeed = 0;
		//	double dblIntNeed = 0;
		//	double dblCostNeed = 0;
		//	double dblPrin/*unused?*/;
		//	double dblInt/*unused?*/;
		//	double dblCost/*unused?*/;
		//	double dblNegValue;
		//	int I;
		//	int lngRWCT = 0;
		//	AffectBillYear = FCConvert.ToDouble(GRID.TextMatrix(lngRow, lngColTotalAmount));
		//	dblNegValue = FCConvert.ToDouble(GRID.TextMatrix(lngRow, lngColTotalAmount));
		//	if (boolNegative)
		//	{
		//		if (modStatusPayments.Statics.boolRE)
		//		{
		//			strSQL = "SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'RE'";
		//		}
		//		else
		//		{
		//			strSQL = "SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'PP'";
		//		}
		//		rs.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
		//		if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
		//		{
		//			txtReference.Text = "AUTO";
		//			for (I = 0; I <= cmbYear.Items.Count - 1; I++)
		//			{
		//				if (Conversion.Val(modExtraModules.FormatYear(cmbYear.Items[I].ToString())) == intYear)
		//				{
		//					cmbYear.SelectedIndex = I;
		//					break;
		//				}
		//			}
		//			frmRECLStatus.InstancePtr.boolChangingCode = true;
		//			cmbCode.SelectedIndex = 1;
		//			cmbPeriod.SelectedIndex = 4;
		//			for (I = 0; I <= cmbCode.Items.Count - 1; I++)
		//			{
		//				if (Strings.Left(cmbCode.Items[I].ToString(), 1) == "P")
		//				{
		//					cmbCode.SelectedIndex = I;
		//					break;
		//				}
		//			}
		//			frmRECLStatus.InstancePtr.boolChangingCode = false;
		//			if (((rs.Get_Fields_Int32("LienRecordNumber"))) == 0)
		//			{
		//				// regular bill record
		//				dblPrinNeed = Conversion.Val(rs.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rs.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rs.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rs.Get_Fields_Decimal("TaxDue4")) - Conversion.Val(rs.Get_Fields_Decimal("PrincipalPaid"));
		//				// MAL@20080318: Correct the way this handles overpayments of Interest
		//				// Tracker Reference: 12808
		//				if (rs.Get_Fields_Decimal("InterestPaid") > (rs.Get_Fields_Decimal("InterestCharged") * -1))
		//				{
		//					// Overpayment exists
		//					dblIntNeed = (Conversion.Val(rs.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rs.Get_Fields_Decimal("InterestCharged")) + modStatusPayments.Statics.dblCurrentInt[(intYear) - modExtraModules.DEFAULTSUBTRACTIONVALUE]) * -1;
		//				}
		//				else
		//				{
		//					dblIntNeed = Conversion.Val(rs.Get_Fields_Decimal("InterestCharged")) + modStatusPayments.Statics.dblCurrentInt[(intYear) - modExtraModules.DEFAULTSUBTRACTIONVALUE] + Conversion.Val(rs.Get_Fields_Decimal("InterestPaid"));
		//				}
		//				dblCostNeed = Conversion.Val(rs.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rs.Get_Fields_Decimal("DemandFeesPaid"));
		//				modStatusPayments.Statics.lngForceBillKey = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
		//				modStatusPayments.Statics.boolForceIsLiened = false;
		//			}
		//			else
		//			{
		//				// lien record
		//				strSQL = "SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rs.Get_Fields_Int32("LienRecordNumber"));
		//				rsLien.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
		//				// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//				dblPrinNeed = Conversion.Val(rsLien.Get_Fields("Principal")) - Conversion.Val(rsLien.Get_Fields_Decimal("PrincipalPaid"));
		//				// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//				// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//				dblIntNeed = FCConvert.ToDouble((rsLien.Get_Fields_Decimal("InterestCharged") * -1)) + modStatusPayments.Statics.dblCurrentInt[(intYear) - modExtraModules.DEFAULTSUBTRACTIONVALUE] - Conversion.Val(rsLien.Get_Fields_Decimal("InterestPaid")) - Conversion.Val(rsLien.Get_Fields("PLIPaid")) + Conversion.Val(rsLien.Get_Fields("Interest"));
		//				// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//				// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//				dblCostNeed = Conversion.Val(rsLien.Get_Fields("Costs")) - Conversion.Val(rsLien.Get_Fields("MaturityFee")) - Conversion.Val(rsLien.Get_Fields_Decimal("CostsPaid"));
		//				modStatusPayments.Statics.lngForceBillKey = FCConvert.ToInt32(rs.Get_Fields_Int32("LienRecordNumber"));
		//				modStatusPayments.Statics.boolForceIsLiened = true;
		//			}
		//			// find pending activity and adjust the need accordingly      12/04/2003
		//			lngRWCT = vsPayments.Rows - 1;
		//			while (!(lngRWCT == 0))
		//			{
		//				if (FCConvert.ToInt32(FCConvert.ToDouble(modExtraModules.FormatYear(vsPayments.TextMatrix(lngRWCT, 0)))) == ((rs.Get_Fields_Int32("BillingYear"))))
		//				{
		//					// adjust the need
		//					dblPrinNeed -= FCConvert.ToDouble(vsPayments.TextMatrix(lngRWCT, 6));
		//					dblIntNeed -= FCConvert.ToDouble(vsPayments.TextMatrix(lngRWCT, 7));
		//					dblCostNeed -= FCConvert.ToDouble(vsPayments.TextMatrix(lngRWCT, 8));
		//				}
		//				else
		//				{
		//					// let it ride
		//				}
		//				lngRWCT -= 1;
		//			}
		//			if (dblPrinNeed + dblIntNeed + dblCostNeed >= 0)
		//			{
		//				return AffectBillYear;
		//			}
		//			if (dblIntNeed > dblNegValue)
		//			{
		//				txtInterest.Text = Strings.Format(dblIntNeed, "#,##0.00");
		//				dblNegValue -= dblIntNeed;
		//			}
		//			else
		//			{
		//				txtInterest.Text = Strings.Format(dblNegValue, "#,##0.00");
		//				dblNegValue = 0;
		//			}
		//			if (dblCostNeed > dblNegValue)
		//			{
		//				txtCosts.Text = Strings.Format(dblCostNeed, "#,##0.00");
		//				dblNegValue -= dblCostNeed;
		//			}
		//			else
		//			{
		//				txtCosts.Text = Strings.Format(dblNegValue, "#,##0.00");
		//				dblNegValue = 0;
		//			}
		//			if (dblPrinNeed > dblNegValue)
		//			{
		//				txtPrincipal.Text = Strings.Format(dblPrinNeed, "#,##0.00");
		//				dblNegValue -= dblPrinNeed;
		//			}
		//			else
		//			{
		//				txtPrincipal.Text = Strings.Format(dblNegValue, "#,##0.00");
		//				dblNegValue = 0;
		//			}
		//			modStatusPayments.Statics.lngForceBillingYearInAuto = FCConvert.ToInt32(rs.Get_Fields_Int32("BillingYear"));
		//			modStatusPayments.AddPaymentToList(vsPayments.Rows, true, true);					
		//		}
		//	}
		//	return AffectBillYear;
		//}

		//private void ApplyCreditForward(ref double dblCreditBal)
		//{
		//	// kk09192014 trocl-1150  Apply Credit Balance to newer bills
		//	// reset all the forced variables
		//	modStatusPayments.Statics.lngForceBillingYearInAuto = 0;
		//	modStatusPayments.Statics.lngForceBillKey = 0;
		//	modStatusPayments.Statics.boolForceIsLiened = false;
		//	cmbYear.SelectedIndex = cmbYear.Items.Count - 1;
		//	// this will set the year AUTO
		//	// this gets set to auto so that the normal payment process is applied
		//	txtPrincipal.Text = Strings.Format(dblCreditBal * -1, "#,##0.00");
		//	modStatusPayments.AddPaymentToList(vsPayments.Rows);
		//}

		private void DeletePaymentRow(int Row)
		{
			bool boolTemp;
			int intArrIndex;
			int I;
			int lngIDNumber;
			int l;
			string strCode;
			lngIDNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(Row, 10))));
			intArrIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(Row, 9))));
			strCode = vsPayments.TextMatrix(Row, 4);
			boolTemp = EraseCHGINTPaymentLine(ref Row);
			if (intArrIndex > 0)
			{
				modStatusPayments.ResetPayment(intArrIndex, boolTemp);
				// this will delete if from the database if it is already stored and clear the payment array
			}
			if (lngIDNumber != 0)
			{
				for (l = 1; l <= vsPayments.Rows - 1; l++)
				{
					if (Conversion.Val(vsPayments.TextMatrix(l, 10)) == lngIDNumber)
					{
						if (l != 0)
						{
							vsPayments.RemoveItem(l);
							// remove the line from the grid
						}
						break;
					}
				}
			}
			else
			{
				if (vsPayments.Rows > 2)
				{
					// MAL@20081113: Add check for abatement payment with two rows that need deleting
					// Tracker Reference: 16063
					if (strCode == "R")
					{
						for (I = 0; I <= vsPayments.Rows - 1; I++)
						{
							lngIDNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(I, 10))));
							if (modExtraModules.IsThisCR() && lngIDNumber > 0)
							{
								modUseCR.DeleteCLPaymentFromShoppingList(ref lngIDNumber);
							}
						}
						// I
						vsPayments.Rows = 1;
						FCUtils.EraseSafe(modStatusPayments.Statics.PaymentArray);
					}
					else
					{
						// more than one payment showing
						vsPayments.RemoveItem(Row);
					}
				}
				else
				{
					vsPayments.Rows = 1;
					// clear the grid out
					FCUtils.EraseSafe(modStatusPayments.Statics.PaymentArray);
					// erase all the payments
				}
			}
			// check for CR
			if (modExtraModules.IsThisCR() && lngIDNumber > 0)
			{
				// this will delete the payment from the shopping list
				modUseCR.DeleteCLPaymentFromShoppingList(ref lngIDNumber);
			}
		}

		private bool EraseCHGINTPaymentLine(ref int Row)
		{
			bool EraseCHGINTPaymentLine = false;
			// this will check to see if there is a CHGINT line associated with the payment that is being
			// erased and find out if there is any other payments that it should be associated with instead of being erased
			// and will associate it, if needed
			int lngIndex;
			clsDRWrapper rsCHGINT = new clsDRWrapper();
			clsDRWrapper rsTemp = new clsDRWrapper();
			int lngID;
			int lngCHGINTNumber = 0;
			int I;
			bool boolEraseCHGINT/*unused?*/;
			int lngDeleteRow;
			int lngNew = 0;
			int lngCHGINTRow = 0;
			string strCode = "";
			lngDeleteRow = Row;
			// find out if the payment being deleted has a CHGINT or EARNINT line with it
			lngIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(lngDeleteRow, 9))));
			// this will find the payment array index of this payment
			lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(lngDeleteRow, 10))));
			if (lngIndex > 0 && lngIndex < modStatusPayments.MAX_PAYMENTS && lngID > 0)
			{
				if (modStatusPayments.Statics.boolRE)
				{
					rsCHGINT.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountPayments + ") AS qCAP WHERE ID = " + FCConvert.ToString(lngID) + " AND BillCode <> 'P'", modExtraModules.strCLDatabase);
				}
				else
				{
					rsCHGINT.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountPayments + ") AS qCAP WHERE ID = " + FCConvert.ToString(lngID) + " AND BillCode = 'P'", modExtraModules.strCLDatabase);
				}
				if (rsCHGINT.EndOfFile() != true && rsCHGINT.BeginningOfFile() != true)
				{
					if (((rsCHGINT.Get_Fields_Int32("CHGINTNumber"))) != 0)
					{
						lngCHGINTNumber = FCConvert.ToInt32(rsCHGINT.Get_Fields_Int32("CHGINTNumber"));
					}
				}
				else if (modStatusPayments.Statics.PaymentArray[lngIndex].CHGINTNumber != 0)
				{
					lngCHGINTNumber = modStatusPayments.Statics.PaymentArray[lngIndex].CHGINTNumber;
				}
				lngNew = 0;
				// default to true then attempt to prove otherwise
				if (lngCHGINTNumber != 0)
				{
					// if there is a CHGINT or EARNINT line
					// find out if there is another line that would require a CHGINT/EARNINT line
					for (I = 1; I <= vsPayments.Rows - 1; I++)
					{
						if (!rsCHGINT.EndOfFile())
						{
							// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
							if (Conversion.Val(Strings.Right(vsPayments.TextMatrix(I, 0), 4)) == Conversion.Val(rsCHGINT.Get_Fields("Year")))
							{
								// check for the same year
								if (vsPayments.TextMatrix(I, 2) != "CHGINT" && vsPayments.TextMatrix(I, 2) != "EARNINT" && Conversion.Val(vsPayments.TextMatrix(I, 10)) != lngID && modStatusPayments.Statics.PaymentArray[FCConvert.ToInt32(vsPayments.TextMatrix(I, 9))].EffectiveInterestDate.ToOADate() == Conversion.Val(rsCHGINT.Get_Fields_DateTime("EffectiveInterestDate")))
								{
									// check to see if it is a CHGINT or EARNINT and has the same effective interest date
									lngNew = I;
									// this will give the row of the first payment during the same year
									break;
								}
							}
						}
					}
					// find the row that the CHGINT line is stored in the grid
					for (I = 1; I <= vsPayments.Rows - 1; I++)
					{
						if (Conversion.Val(vsPayments.TextMatrix(I, 10)) == lngCHGINTNumber)
						{
							lngCHGINTRow = I;
							break;
						}
					}
					// delete the CHGINT or EARNINT line
					if (lngNew == 0)
					{
						// if there is no line to hook the CHGINT or EARNINT to
						// erase the line and the corresponding record
						rsCHGINT.Execute("DELETE FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngCHGINTNumber), modExtraModules.strCLDatabase);
						if (lngCHGINTRow > 0)
						{
							vsPayments.RemoveItem(lngCHGINTRow);
						}
						EraseCHGINTPaymentLine = true;
					}
					else
					{
						// change the CHGINT of the next payment made for that same account/year/?EDate
						if (Conversion.Val(vsPayments.TextMatrix(lngNew, 10)) == 0)
						{
							EraseCHGINTPaymentLine = false;
							modStatusPayments.Statics.PaymentArray[FCConvert.ToInt32(vsPayments.TextMatrix(lngNew, 9))].CHGINTNumber = lngCHGINTNumber;
							vsPayments.TextMatrix(lngNew, 11, FCConvert.ToString(lngCHGINTNumber));
							if (FCConvert.ToDouble(vsPayments.TextMatrix(lngNew, 10)) != 0)
							{
								// if the payment line has been saved in the database then
								if (modStatusPayments.Statics.boolRE)
								{
									rsCHGINT.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountPayments + ") AS qCAP WHERE ID = " + vsPayments.TextMatrix(lngNew, 10) + " AND BillCode <> 'P'", modExtraModules.strCLDatabase);
								}
								else
								{
									rsCHGINT.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountPayments + ") AS qCAP WHERE ID = " + vsPayments.TextMatrix(lngNew, 10) + " AND BillCode = 'P'", modExtraModules.strCLDatabase);
								}
								if (rsCHGINT.EndOfFile() != true && rsCHGINT.BeginningOfFile() != true)
								{
									rsCHGINT.Edit();
									rsCHGINT.Set_Fields("CHGINTNumber", lngCHGINTNumber);
									rsCHGINT.Update(false);
								}
							}
						}
						else
						{
							// erase the payment line and the corresponding record
							if (modStatusPayments.Statics.boolRE)
							{
								rsCHGINT.Execute("DELETE FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngCHGINTNumber) + " BillCode <> 'P'", modExtraModules.strCLDatabase);
							}
							else
							{
								rsCHGINT.Execute("DELETE FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngCHGINTNumber) + " BillCode = 'P'", modExtraModules.strCLDatabase);
							}
							if (lngCHGINTRow > 0)
							{
								vsPayments.RemoveItem(lngCHGINTRow);
							}
							EraseCHGINTPaymentLine = true;
						}
					}
				}
			}
			else
			{
				if (!modExtraModules.IsThisCR() && modStatusPayments.Statics.PaymentArray[Math.Abs(lngIndex)].CHGINTNumber != 0)
				{
					lngCHGINTNumber = modStatusPayments.Statics.PaymentArray[Math.Abs(lngIndex)].CHGINTNumber;
					if (modStatusPayments.Statics.boolRE)
					{
						rsCHGINT.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngCHGINTNumber) + " AND BillCode <> 'P'", modExtraModules.strCLDatabase);
					}
					else
					{
						rsCHGINT.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngCHGINTNumber) + " AND BillCode = 'P'", modExtraModules.strCLDatabase);
					}
					// this will have to reverse the change in the lien/bill record
					if (modStatusPayments.Statics.PaymentArray[lngIndex].BillCode == "L")
					{
						// adjust the lien record
						strCode = "RE";
						rsTemp.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(modStatusPayments.Statics.PaymentArray[lngIndex].BillKey), modExtraModules.strCLDatabase);
						if (rsTemp.RecordCount() != 0 && rsCHGINT.RecordCount() != 0)
						{
							rsTemp.Edit();
							rsTemp.Set_Fields("InterestCharged", (Conversion.Val(rsTemp.Get_Fields_Decimal("InterestCharged")) - Conversion.Val(rsCHGINT.Get_Fields_Decimal("CurrentInterest"))));
							rsTemp.Update();
						}
					}
					else
					{
						// adjust the bill record
						if (modStatusPayments.Statics.boolRE)
						{
							strCode = "RE";
							rsTemp.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE ID = " + FCConvert.ToString(modStatusPayments.Statics.PaymentArray[lngIndex].BillKey) + " AND BillingType = 'RE'", modExtraModules.strCLDatabase);
						}
						else
						{
							strCode = "PP";
							rsTemp.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE ID = " + FCConvert.ToString(modStatusPayments.Statics.PaymentArray[lngIndex].BillKey) + " AND BillingType = 'PP'", modExtraModules.strCLDatabase);
						}
						if (rsTemp.RecordCount() != 0 && rsCHGINT.RecordCount() != 0)
						{
							rsTemp.Edit();
							rsTemp.Set_Fields("InterestCharged", (Conversion.Val(rsTemp.Get_Fields_Decimal("InterestCharged")) - Conversion.Val(rsCHGINT.Get_Fields_Decimal("CurrentInterest"))));
							rsTemp.Update();
						}
					}
				}
				if (lngCHGINTRow > 0)
				{
					vsPayments.RemoveItem(lngCHGINTRow);
				}
				else
				{
					if (lngIndex > 0 && lngIndex < modStatusPayments.MAX_PAYMENTS)
					{
						if (modStatusPayments.Statics.PaymentArray[lngIndex].CHGINTNumber != 0)
						{
							lngID = modStatusPayments.Statics.PaymentArray[lngIndex].CHGINTNumber;
							rsCHGINT.Execute("DELETE FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngID), modExtraModules.strCLDatabase);
						}
					}
				}
			}
			return EraseCHGINTPaymentLine;
		}

		//private void vsPeriod_DblClick(object sender, EventArgs e)
		//{
		//	double dbl1 = 0;
		//	double dbl2 = 0;
		//	double dbl3 = 0;
		//	double dbl4 = 0;
		//	if (boolDisableInsert)
		//	{
		//		// kk03132015  trocrs-36  Add option to disable automatically filling in payment amount
		//		return;
		//	}
		//	if (cmbYear.Items[cmbYear.SelectedIndex].ToString() == "Auto")
		//	{
		//		modStatusPayments.CreateOppositionLine(GRID.Rows - 1);
		//		// create an opposition line for the whole account
		//	}
		//	else
		//	{
		//		if (vsPeriod.TextMatrix(0, 1) == "")
		//			return;
		//		if (vsPeriod.TextMatrix(0, 1) != "")
		//			dbl1 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 1), vsPeriod.TextMatrix(0, 1).Length - 2));
		//		if (vsPeriod.TextMatrix(0, 2) != "")
		//			dbl2 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 2), vsPeriod.TextMatrix(0, 2).Length - 2));
		//		if (vsPeriod.TextMatrix(0, 3) != "")
		//			dbl3 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 3), vsPeriod.TextMatrix(0, 3).Length - 2));
		//		if (vsPeriod.TextMatrix(0, 4) != "")
		//			dbl4 = FCConvert.ToDouble(Strings.Right(vsPeriod.TextMatrix(0, 4), vsPeriod.TextMatrix(0, 4).Length - 2));
		//		int VBtoVar = vsPeriod.Col;
		//		if (VBtoVar >= 1 && VBtoVar <= 4)
		//		{
		//			CreatePeriodOppositionLine(dbl1, dbl2, dbl3, dbl4, vsPeriod.Col, FCConvert.ToInt16(modExtraModules.FormatYear(cmbYear.Items[cmbYear.SelectedIndex].ToString())));
		//		}
		//		else
		//		{
		//		}
		//	}
		//}

        //private void vsPeriod_MouseMove(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //	int lngMR;
        //	int lngMC;
        //	lngMR = vsPeriod.MouseRow;
        //	lngMC = vsPeriod.MouseCol;
        //	if (lngMC > 0 && lngMC < 5)
        //	{
        //		// kk05122015  trocrs-39  Catch out of bounds
        //		ToolTip1.SetToolTip(vsPeriod, strPeriodToolTip[lngMC]);
        //	}
        //	else
        //	{
        //		ToolTip1.SetToolTip(vsPeriod, "");
        //	}
        //}

    
        private void vsPeriod_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int lngMR;
            int lngMC;
            lngMR = vsPeriod.GetFlexRowIndex(e.RowIndex);
            lngMC = vsPeriod.GetFlexColIndex(e.ColumnIndex);
            if (vsPeriod.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                DataGridViewCell cell = vsPeriod[e.ColumnIndex, e.RowIndex];
                if (lngMC > 0 && lngMC < 5)
                {
                    // kk05122015  trocrs-39  Catch out of bounds
                    cell.ToolTipText = strPeriodToolTip[lngMC];
                }
                else
                {
                    cell.ToolTipText = "";
                }
            }
        }

        //      private void vsPPCategory_MouseMove(object sender, DataGridViewCellMouseEventArgs e)
        //{
        //	int lngMR;
        //	int lngMC;
        //	lngMR = vsPPCategory.MouseRow;
        //	lngMC = vsPPCategory.MouseCol;
        //	if (lngMR > 0 && lngMR < 10 && lngMC == 0)
        //	{
        //		// if this is a category code
        //		ToolTip1.SetToolTip(vsPPCategory, vsPPCategory.TextMatrix(lngMR, lngMC));
        //	}
        //	else
        //	{
        //		ToolTip1.SetToolTip(vsPPCategory, "");
        //	}
        //}

        private void vsPPCategory_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
			int lngMR;
			int lngMC;
			lngMR = vsPPCategory.GetFlexRowIndex(e.RowIndex);
			lngMC = vsPPCategory.GetFlexColIndex(e.ColumnIndex);
            if (vsPPCategory.IsValidCell(e.ColumnIndex, e.RowIndex))
               {
                DataGridViewCell cell = vsPPCategory[e.ColumnIndex, e.RowIndex];
                if (lngMR > 0 && lngMR < 10 && lngMC == 0)
                {
                    // if this is a category code
                    cell.ToolTipText = vsPPCategory.TextMatrix(lngMR, lngMC);
                }
                else
                {
                    cell.ToolTipText = "";
                }
            }
		}
		private void vsPreview_Click(object sender, EventArgs e)
		{
			if (vsPreview.Visible)
			{
				cmdPaymentPreview.Text = "Preview";
				GRID.Visible = true;
				vsPreview.Visible = false;
				vsPreview.Rows = 1;
			}
		}

		private void vsRateInfo_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// if this is the applied through date, then format the edit mask
			if (modStatusPayments.Statics.boolRE)
			{
				switch (vsRateInfo.Row)
				{
					case 11:
						{
							if (vsRateInfo.Col == 4)
							{
								vsRateInfo.EditMask = "##/##/####";
							}
							else
							{
								vsRateInfo.EditMask = "";
							}
							break;
						}
					default:
						{
							vsRateInfo.EditMask = "";
							break;
						}
				}
				//end switch
			}
			else
			{
				switch (vsRateInfo.Row)
				{
					case 13:
						{
							if (vsRateInfo.Col == 5)
							{
								vsRateInfo.EditMask = "##/##/####";
							}
							else
							{
								vsRateInfo.EditMask = "";
							}
							break;
						}
					default:
						{
							vsRateInfo.EditMask = "";
							break;
						}
				}
				//end switch
			}
		}

		private void vsRateInfo_DblClick(object sender, EventArgs e)
		{
			double dbl1 = 0;
			double dbl2 = 0;
			double dbl3 = 0;
			double dbl4 = 0;
			if (boolPayment)
			{
				if (vsRateInfo.TextMatrix(vsRateInfo.Row, 1) == "")
					return;
				if (vsRateInfo.TextMatrix(15, 1) != "")
					dbl1 = FCConvert.ToDouble(vsRateInfo.TextMatrix(15, 1));
				if (vsRateInfo.TextMatrix(16, 1) != "")
					dbl2 = FCConvert.ToDouble(vsRateInfo.TextMatrix(16, 1));
				if (vsRateInfo.TextMatrix(17, 1) != "")
					dbl3 = FCConvert.ToDouble(vsRateInfo.TextMatrix(17, 1));
				if (vsRateInfo.TextMatrix(18, 1) != "")
					dbl4 = FCConvert.ToDouble(vsRateInfo.TextMatrix(18, 1));
				int VBtoVar = vsRateInfo.Row;
				if (VBtoVar >= 15 && VBtoVar <= 18)
				{
					CreatePeriodOppositionLine(dbl1, dbl2, dbl3, dbl4, FCConvert.ToInt16(vsRateInfo.Row - 14), FCConvert.ToInt16(Conversion.Val(modExtraModules.FormatYear(Strings.Right(fraRateInfo.Text, 6)))));
				}
				else
				{
				}
			}
		}
		// VBto upgrade warning: intPer As short	OnWrite(int, double)
		// VBto upgrade warning: intYear As short	OnWrite(string, double)
		private void CreatePeriodOppositionLine(double dblPer1, double dblPer2, double dblPer3, double dblPer4, int intPer, int intYear)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will create an opposition line for the period clicked on
				int intCT;
				double dblP;
				double dblI = 0;
				double dblC = 0;
				double dblTemp = 0;
				clsDRWrapper rsTemp = new clsDRWrapper();
				bool boolCombine;
				boolCombine = !txtPrincipal.Visible;
				// find what is needed for the year chosen
				if (modStatusPayments.Statics.boolRE)
				{
					rsTemp.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'RE'", modExtraModules.strCLDatabase);
				}
				else
				{
					rsTemp.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'PP'", modExtraModules.strCLDatabase);
				}
				if (rsTemp.EndOfFile() != true)
				{
					if (((rsTemp.Get_Fields_Int32("LienRecordNumber"))) == 0)
					{
						dblP = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4")) - Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid"));
						dblI = modStatusPayments.Statics.dblCurrentInt[(intYear) - modExtraModules.DEFAULTSUBTRACTIONVALUE] - Conversion.Val(rsTemp.Get_Fields_Decimal("InterestCharged")) - Conversion.Val(rsTemp.Get_Fields_Decimal("InterestPaid"));
						dblC = Conversion.Val(rsTemp.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsTemp.Get_Fields_Decimal("DemandFeesPaid"));
					}
					else
					{
						rsTemp.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsTemp.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
						if (rsTemp.EndOfFile() != true)
						{
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							dblP = Conversion.Val(rsTemp.Get_Fields("Principal")) - Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid"));
							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
							dblI = modStatusPayments.Statics.dblCurrentInt[(intYear) - modExtraModules.DEFAULTSUBTRACTIONVALUE] + Conversion.Val(rsTemp.Get_Fields("Interest")) - Conversion.Val(rsTemp.Get_Fields_Decimal("InterestCharged")) - Conversion.Val(rsTemp.Get_Fields_Decimal("InterestPaid")) - Conversion.Val(rsTemp.Get_Fields("PLIPaid"));
							// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
							dblC = Conversion.Val(rsTemp.Get_Fields("Costs")) - Conversion.Val(rsTemp.Get_Fields("MaturityFee")) - Conversion.Val(rsTemp.Get_Fields_Decimal("CostsPaid"));
						}
					}
				}
				else
				{
					// this is a lien
					dblP = 0;
					dblI = dblPer1 + dblPer2 + dblPer3 + dblPer4;
					dblC = 0;
				}
				if (Strings.Left(cmbCode.Text, 1) == "A")
				{
					if (dblC != 0 || dblI != 0)
					{
						FCMessageBox.Show("Interest and/or costs are due on this bill. Please manually enter the amount to abate.", MsgBoxStyle.Information, "Manually Abate");
						return;
					}
				}
				// set the year
				for (intCT = 0; intCT <= cmbYear.Items.Count - 1; intCT++)
				{
					if (Conversion.Val(modExtraModules.FormatYear(cmbYear.Items[intCT].ToString())) == intYear)
					{
						cmbYear.SelectedIndex = intCT;
						break;
					}
				}
				if (intPer == 4 || intPer == 0)
				{
					// check the other periods to find out if any have balances
					if (dblPer1 > 0 || dblPer2 > 0 || dblPer3 > 0)
					{
						// if they do, then show a message to either include them or not create the line
						if (intPer == 0)
						{
							dblTemp = dblPer1 + dblPer2 + dblPer3 + dblPer4;
						}
						else
						{
							if (FCMessageBox.Show("There are outstanding balances on previous periods, you must pay the earliest bill first.  Would you like to include the previous amounts?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Prior Balances") == DialogResult.Yes)
							{
								dblTemp = dblPer1 + dblPer2 + dblPer3 + dblPer4;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						dblTemp = dblPer4;
					}
				}
				else if (intPer == 3)
				{
					// check the other periods to find out if any have balances
					if (dblPer1 > 0 || dblPer2 > 0)
					{
						// if they do, then show a message to either include them or not create the line
						if (FCMessageBox.Show("There are outstanding balances on previous periods, you must pay the most recent first.  Would you like to include the previous amounts?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Prior Balances") == DialogResult.Yes)
						{
							dblTemp = dblPer1 + dblPer2 + dblPer3;
						}
						else
						{
							return;
						}
					}
					else
					{
						dblTemp = dblPer3;
					}
				}
				else if (intPer == 2)
				{
					// check the other periods to find out if any have balances
					if (dblPer1 > 0)
					{
						// if they do, then show a message to either include them or not create the line
						if (FCMessageBox.Show("There are outstanding balances for previous periods." + "\r\n" + "This payment will include these amounts." + "\r\n" + "Do you wish to continue?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Previous Period Balances") == DialogResult.Yes)
						{
							dblTemp = dblPer2 + dblPer1;
						}
						else
						{
							return;
						}
					}
					else
					{
						dblTemp = dblPer2;
					}
				}
				else if (intPer == 1)
				{
					dblTemp = dblPer1;
				}
				// spread the payment out
				if (dblI >= dblTemp || boolCombine)
				{
					// all used in interest
					txtInterest.Text = Strings.Format(dblTemp, "#,##0.00");
					txtPrincipal.Text = "0.00";
					txtCosts.Text = "0.00";
				}
				else
				{
					txtInterest.Text = Strings.Format(dblI, "#,##0.00");
					dblTemp -= dblI;
					if (dblC >= dblTemp)
					{
						txtCosts.Text = Strings.Format(dblTemp, "#,##0.00");
						txtPrincipal.Text = "0.00";
					}
					else
					{
						txtCosts.Text = Strings.Format(dblC, "#,##0.00");
						dblTemp -= dblC;
						txtPrincipal.Text = Strings.Format(dblTemp, "#,##0.00");
					}
				}
				if (fraRateInfo.Visible == true)
					fraRateInfo.Visible = false;
				mnuFilePrintRateInfo.Visible = fraRateInfo.Visible;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Period Opposition Error");
			}
		}
		// VBto upgrade warning: intYear As short	OnWrite(string)
		public string GetCLAccount(string strType, int lngType, short intYear = 0)
		{
			string GetCLAccount = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return the correct Account number to use int he Account Box
				clsDRWrapper rsBD = new clsDRWrapper();
				if (modGlobalConstants.Statics.gboolCR)
				{
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						rsBD.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngType) + " AND TownKey = " + FCConvert.ToString(lngCLResCode), modExtraModules.strCRDatabase);
					}
					else
					{
						rsBD.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngType), modExtraModules.strCRDatabase);
					}
					if (!rsBD.EndOfFile())
					{
						switch (lngType)
						{
							case 90:
								{
									if (strType == "A")
									{
										if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account6"))))
										{
											GetCLAccount = FCConvert.ToString(rsBD.Get_Fields_String("Account6"));
											if (Strings.Left(GetCLAccount, 1) == "G")
											{
												if (FCConvert.ToBoolean(rsBD.Get_Fields_Boolean("Year6")))
												{
													GetCLAccount = Strings.Left(FCConvert.ToString(rsBD.Get_Fields_String("Account6")), FCConvert.ToString(rsBD.Get_Fields_String("Account6")).Length - 2) + modGlobal.PadToString(intYear, 2);
												}
											}
										}
										else
										{
											GetCLAccount = "";
										}
									}
									else if (strType == "D")
									{
										if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account3"))))
										{
											GetCLAccount = FCConvert.ToString(rsBD.Get_Fields_String("Account3"));
										}
										else
										{
											GetCLAccount = "";
										}
									}
									else if (strType == "S")
									{
										if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account5"))))
										{
											GetCLAccount = FCConvert.ToString(rsBD.Get_Fields_String("Account5"));
										}
										else
										{
											GetCLAccount = "";
										}
									}
									break;
								}
							case 91:
								{
									if (strType == "A")
									{
										if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account6"))))
										{
											GetCLAccount = FCConvert.ToString(rsBD.Get_Fields_String("Account6"));
											if (Strings.Left(GetCLAccount, 1) == "G")
											{
												if (FCConvert.ToBoolean(rsBD.Get_Fields_Boolean("Year6")))
												{
													GetCLAccount = Strings.Left(FCConvert.ToString(rsBD.Get_Fields_String("Account6")), FCConvert.ToString(rsBD.Get_Fields_String("Account6")).Length - 2) + modGlobal.PadToString(intYear, 2);
												}
											}
										}
										else
										{
											GetCLAccount = "";
										}
									}
									else if (strType == "D")
									{
										// If AccountValidate(rsBD.Get_Fields("Account3")) Then
										// GetCLAccount = rsBD.Get_Fields("Account3")
										// Else
										GetCLAccount = "";
										// End If
									}
									else if (strType == "S")
									{
										if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account5"))))
										{
											GetCLAccount = FCConvert.ToString(rsBD.Get_Fields_String("Account5"));
										}
										else
										{
											GetCLAccount = "";
										}
									}
									break;
								}
							case 92:
								{
									if (strType == "A")
									{
										if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account6"))))
										{
											GetCLAccount = FCConvert.ToString(rsBD.Get_Fields_String("Account6"));
											if (Strings.Left(GetCLAccount, 1) == "G")
											{
												if (FCConvert.ToBoolean(rsBD.Get_Fields_Boolean("Year6")))
												{
													GetCLAccount = Strings.Left(FCConvert.ToString(rsBD.Get_Fields_String("Account6")), FCConvert.ToString(rsBD.Get_Fields_String("Account6")).Length - 2) + modGlobal.PadToString(intYear, 2);
												}
											}
										}
										else
										{
											GetCLAccount = "";
										}
									}
									else if (strType == "D")
									{
										if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account3"))))
										{
											GetCLAccount = FCConvert.ToString(rsBD.Get_Fields_String("Account3"));
										}
										else
										{
											GetCLAccount = "";
										}
									}
									else if (strType == "S")
									{
										if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account5"))))
										{
											GetCLAccount = FCConvert.ToString(rsBD.Get_Fields_String("Account5"));
										}
										else
										{
											GetCLAccount = "";
										}
									}
									break;
								}
						}
						//end switch
						if (GetCLAccount == "")
						{
							FCMessageBox.Show("Error in type " + FCConvert.ToString(lngType) + ".  Please make sure that the account is setup.", MsgBoxStyle.Exclamation, "Account Setup Error");
							if (strType == "A")
							{
								GetCLAccount = "M ABATEMENT";
							}
							else if (strType == "D")
							{
								if (lngType == 91)
								{
									FCMessageBox.Show("You cannot discount a lien.", MsgBoxStyle.Exclamation, "Invalid Entry");
								}
								else
								{
									GetCLAccount = "M DISCOUNT";
								}
							}
							else if (strType == "S")
							{
								GetCLAccount = "M SUPPLEMENT";
							}
						}
					}
					else
					{
						// RE Payment
						if (strType == "A")
						{
							GetCLAccount = "M ABATEMENT";
						}
						else if (strType == "D")
						{
							GetCLAccount = "M DISCOUNT";
						}
						else if (strType == "S")
						{
							GetCLAccount = "M SUPPLEMENT";
						}
					}
				}
				else
				{
					// RE Payment
					if (strType == "A")
					{
						GetCLAccount = "M ABATEMENT";
					}
					else if (strType == "D")
					{
						GetCLAccount = "M DISCOUNT";
					}
					else if (strType == "S")
					{
						GetCLAccount = "M SUPPLEMENT";
					}
				}
				return GetCLAccount;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				GetCLAccount = "";
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Account Information Error");
			}
			return GetCLAccount;
		}

		private void FillInREInfo()
		{
			clsDRWrapper rsMainInfo = new clsDRWrapper();
			string strBP = "";
			rsMainInfo.OpenRecordset("SELECT OwnerPartyID, RSMapLot, RSLocNumAlph, RSLocApt, RSLocStreet, DeedName1 AS Name1, DeedName2 AS Name2 FROM Master WHERE RSAccount = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND RSCard = 1", modExtraModules.strREDatabase);
			if (rsMainInfo.EndOfFile() != true)
			{
				if (Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields_String("Name2"))) != "")
				{
					lblOwnersName.Text = Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields_String("Name1"))) + " & " + Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields_String("Name2")));
					// fill in the labels with the record information
				}
				else
				{
					lblOwnersName.Text = Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields_String("Name1")));
				}
				OwnerPartyID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMainInfo.Get_Fields_Int32("OwnerPartyID"))));
				CurrentOwner = Strings.Trim(lblOwnersName.Text);
				lblOwnersName.Visible = true;
				lblOwnersName.BackColor = System.Drawing.Color.White;
				lblMapLot.Text = Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields_String("RSMapLot")));
				lblMapLot.Visible = true;
				lblMapLot.BackColor = System.Drawing.Color.White;
				lblLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields_String("RSLocNumAlph")) + " ") + " " + Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields_String("RSLocApt")) + " ") + " " + Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields_String("RSLocStreet")) + " "));
				if (Conversion.Val(rsMainInfo.Get_Fields_String("RSLocNumAlph")) == 0 && Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields_String("RSLocStreet"))) == "")
					lblLocation.Text = "";
				lblLocation.Visible = true;
				lblLocation.BackColor = System.Drawing.Color.White;
				lblAccount.BackColor = System.Drawing.Color.White;
				lblAccount.Visible = true;
				lblPPAccount.Visible = false;
				lblAccount.Text = Strings.Format(StaticSettings.TaxCollectionValues.LastAccountRE, "000000");
				lblPPAccount.Text = Strings.Format(StaticSettings.TaxCollectionValues.LastAccountPP, "000000");
				// this label will display the account information when the form is in Payment Mode
				//FC:FINAL:AM: remove labels
				//lblPaymentInfo.Text = "Account: " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + "     Name: " + lblOwnersName.Text + "       Location: " + lblLocation.Text;
				lblPaymentInfo.Text = lblOwnersName.Text + " " + lblLocation.Text;
			}
			else
			{
				if (Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("Name2"))) != "")
				{
					lblOwnersName.Text = Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("Name1")) + " & " + FCConvert.ToString(rsCL.Get_Fields_String("Name2")));
					// fill in the labels with the record information
				}
				else
				{
					lblOwnersName.Text = Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("Name1")));
				}
				OwnerPartyID = 0;
				CurrentOwner = Strings.Trim(lblOwnersName.Text);
				lblOwnersName.Visible = true;
				lblOwnersName.BackColor = System.Drawing.Color.White;
				lblMapLot.Text = Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("MapLot")));
				lblMapLot.Visible = true;
				lblMapLot.BackColor = System.Drawing.Color.White;
				lblLocation.Text = Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_Int32("StreetNumber")) + " ") + " " + Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("StreetName")) + " ");
				if (Conversion.Val(lblLocation.Text) == 0)
					lblLocation.Text = "";
				lblLocation.Visible = true;
				lblLocation.BackColor = System.Drawing.Color.White;
				lblAccount.BackColor = System.Drawing.Color.White;
				lblAccount.Visible = true;
				lblPPAccount.Visible = false;
				lblAccount.Text = Strings.Format(StaticSettings.TaxCollectionValues.LastAccountRE, "000000");
				lblPPAccount.Text = Strings.Format(StaticSettings.TaxCollectionValues.LastAccountPP, "000000");
				// this label will display the account information when the form is in Payment Mode
				//FC:FINAL:AM: remove labels
				//lblPaymentInfo.Text = "Account: " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + "     Name: " + lblOwnersName.Text + "       Location: " + lblLocation.Text;
				lblPaymentInfo.Text = lblOwnersName.Text + " " + lblLocation.Text;
			}
			rsMainInfo.OpenRecordset("SELECT * FROM BookPage WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND Card = 1 AND [Current] = 1 ORDER BY Line", modExtraModules.strREDatabase);
			while (!rsMainInfo.EndOfFile())
			{
				// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
				strBP += "B" + FCConvert.ToString(rsMainInfo.Get_Fields("Book")) + "P" + FCConvert.ToString(rsMainInfo.Get_Fields("Page")) + ", ";
				rsMainInfo.MoveNext();
			}
			if (Strings.Trim(strBP) != "")
			{
				strBP = Strings.Left(strBP, strBP.Length - 2);
				// cut off the last comma
			}
			lblBookPage.Text = strBP;
			rsMainInfo.Reset();
		}

		private void FillInPPInfo()
		{
			clsDRWrapper rsMainInfo = new clsDRWrapper();
			rsMainInfo.OpenRecordset("SELECT PPMaster.*, pOwn.FullNameLF AS Name " + "FROM PPMaster INNER JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pOwn ON PPMaster.PartyID = pOwn.ID " + "WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP), modExtraModules.strPPDatabase);
			if (rsMainInfo.EndOfFile() != true)
			{
				lblOwnersName.Text = Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields_String("Name")));
				OwnerPartyID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMainInfo.Get_Fields_Int32("PartyID"))));
				CurrentOwner = Strings.Trim(lblOwnersName.Text);
				lblOwnersName.Visible = true;
				lblOwnersName.BackColor = System.Drawing.Color.White;
				lblMapLot.Text = "";
				lblMapLot.Visible = false;
				lblMapLot.BackColor = System.Drawing.Color.White;
				lblLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields_Int32("StreetNumber")) + " ") + " " + Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields_String("Street")) + " "));
				if (Conversion.Val(lblLocation.Text) == 0)
					lblLocation.Text = "";
				lblLocation.Visible = true;
				// lblLocation.BackColor = System.Drawing.Color.White
				// lblAccount.BackColor = System.Drawing.Color.White
				//lblAccount.Visible = true;
				lblPPAccount.Text = Strings.Format(StaticSettings.TaxCollectionValues.LastAccountPP, "000000");
				// this label will display the account information when the form is in Payment Mode
				//FC:FINAL:AM:
				//lblPaymentInfo.Text = "Account: " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + "     Name: " + lblOwnersName.Text + "       Location: " + lblLocation.Text;
				lblPaymentInfo.Text = lblOwnersName.Text + " " + lblLocation.Text;
				lblPPOwnersName.Text = FCConvert.ToString(rsMainInfo.Get_Fields_String("Name"));
				// kk 120912  .Get_Fields("Own1FullName")
			}
			else
			{
				if (Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("Name2"))) != "")
				{
					lblOwnersName.Text = Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("Name1")) + " & " + FCConvert.ToString(rsCL.Get_Fields_String("Name2")));
					// fill in the labels with the record information
				}
				else
				{
					lblOwnersName.Text = Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("Name1")));
				}
				OwnerPartyID = 0;
				CurrentOwner = Strings.Trim(lblOwnersName.Text);
				lblOwnersName.Visible = true;
				lblOwnersName.BackColor = System.Drawing.Color.White;
				lblMapLot.Text = "";
				lblMapLot.Visible = false;
				lblMapLot.BackColor = System.Drawing.Color.White;
				lblLocation.Text = Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_Int32("StreetNumber")) + " ") + " " + Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("StreetName")) + " ");
				if (Conversion.Val(lblLocation.Text) == 0)
					lblLocation.Text = "";
				lblLocation.Visible = true;
				lblLocation.BackColor = System.Drawing.Color.White;
				lblAccount.BackColor = System.Drawing.Color.White;
				lblAccount.Visible = true;
				lblAccount.Text = Strings.Format(StaticSettings.TaxCollectionValues.LastAccountPP, "000000");
				lblPPAccount.Text = Strings.Format(StaticSettings.TaxCollectionValues.LastAccountPP, "000000");
				// this label will display the account information when the form is in Payment Mode
				//FC:FINAL:DDU:#1068 - remove labels
				//lblPaymentInfo.Text = "Account: " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + "     Name: " + lblOwnersName.Text + "       Location: " + lblLocation.Text;
				lblPaymentInfo.Text = lblOwnersName.Text + " " + lblLocation.Text;
			}
		}

		private void ShowRateInfo(ref int cRow, ref int cCol, ref clsDRWrapper rsCL)
		{
			int lngErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will show a box with the rate information in it
				// as soon as the box loses focus, I will make it disappear
				clsDRWrapper rsRI = new clsDRWrapper();
				clsDRWrapper rsLR = new clsDRWrapper();
				int intYear = 0;
				int I;
				int RK;
				int LRN;
				// VBto upgrade warning: dtInterestPDThroughDate As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dtInterestPDThroughDate = DateTime.FromOADate(0);
				double dblTotalAbate = 0;
				lngErrCode = 1;
				RK = rsCL.Get_Fields_Int32("RateKey");
				lngErrCode = 2;
				LRN = rsCL.Get_Fields_Int32("LienRecordNumber");
				lngErrCode = 3;
				if (LRN != 0)
				{
					rsLR.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(LRN), modExtraModules.strCLDatabase);
					lngErrCode = 4;
					RK = FCConvert.ToInt32(rsLR.Get_Fields_Int32("RateKey"));
				}
				lngErrCode = 5;
				rsRI.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(RK), modExtraModules.strCLDatabase);
				// format grid
				if (modStatusPayments.Statics.boolRE)
				{
					frmRECLStatus.InstancePtr.vsRateInfo.Cols = 1;
					frmRECLStatus.InstancePtr.vsRateInfo.Cols = 5;
				}
				else
				{
					frmRECLStatus.InstancePtr.vsRateInfo.Cols = 1;
					frmRECLStatus.InstancePtr.vsRateInfo.Cols = 6;
				}
				if (LRN != 0)
				{
					frmRECLStatus.InstancePtr.vsRateInfo.Rows = 22;
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(20, 3, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(20, 4, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(21, 3, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(21, 4, "");
				}
				else
				{
					frmRECLStatus.InstancePtr.vsRateInfo.Rows = 20;
				}
				frmRECLStatus.InstancePtr.vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//				fraRateInfo.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(frmRECLStatus.InstancePtr.GRID.TextMatrix(cRow, cCol), 4) + Strings.Right(frmRECLStatus.InstancePtr.GRID.TextMatrix(cRow, cCol), 1))));
				frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(0, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.24));
				frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(1, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.19));
				frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(2, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.04));
				if (modStatusPayments.Statics.boolRE)
				{
					frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(3, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.23));
					frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(4, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.28));
					frmRECLStatus.InstancePtr.vsRateInfo.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
				}
				else
				{
					frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(3, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.17));
					frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(4, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.18));
					frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(5, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.17));
					frmRECLStatus.InstancePtr.vsRateInfo.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
				}
				frmRECLStatus.InstancePtr.vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
				//frmRECLStatus.InstancePtr.vsRateInfo.Height = frmRECLStatus.InstancePtr.vsRateInfo.RowHeight(0) * frmRECLStatus.InstancePtr.vsRateInfo.Rows;
				frmRECLStatus.InstancePtr.vsRateInfo.Select(18, 0, 18, 1);
				frmRECLStatus.InstancePtr.vsRateInfo.CellBorder(ColorTranslator.FromOle(0), 0, 0, 0, 0, 0, 0);
				frmRECLStatus.InstancePtr.vsRateInfo.Select(15, 0, 15, 1);
				frmRECLStatus.InstancePtr.vsRateInfo.CellBorder(ColorTranslator.FromOle(0), 0, 2, 0, 0, 0, 1);
				frmRECLStatus.InstancePtr.vsRateInfo.Select(0, 0);
				frmRECLStatus.InstancePtr.vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
				// show the first set of information (rate information and period info)--------------
				frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 0, "RateKey");
				frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 0, "Tax Rate");
				frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 0, "Number of Periods");
				frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 0, "Rate Type");
				frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 0, "");
				// "Creation Date"
				frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 0, "Interest Per 1");
				frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 0, "Due Date Per 1");
				frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 0, "Interest Rate");
				frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 0, "Period 1 Due");
				if (LRN != 0)
				{
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 0, "");
				}
				else
				{
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 0, "Bill ID");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 0, "Interest Per 2");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 0, "Interest Per 3");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 0, "Interest Per 4");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 0, "Due Date Per 2");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 0, "Due Date Per 3");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 0, "Due Date Per 4");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 0, "Period 2 Due");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 0, "Period 3 Due");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 0, "Period 4 Due");
				}
				frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 0, "Per Diem");
				if (rsRI.BeginningOfFile() != true && rsRI.EndOfFile() != true)
				{
					lngErrCode = 6;
					// Select Case rsRI.Get_Fields("RateType")
					// Case "L"
					// .TextMatrix(0, 1) = rsRI.Get_Fields("RateKey") & " - Lien"
					// Case "S"
					// .TextMatrix(0, 1) = rsRI.Get_Fields("RateKey") & " - Supp"
					// Case Else
					// .TextMatrix(0, 1) = rsRI.Get_Fields("RateKey") & " - Reg"
					// End Select
					// 
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 1, FCConvert.ToString(rsRI.Get_Fields_Int32("ID")));
					lngErrCode = 7;
					if (LRN == 0)
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 1, Strings.Format(rsRI.Get_Fields_Double("TaxRate") * 1000, "#0.000"));
						// kk10282014 trocl-1209  Mill rate rounded off, change format from "0.00"
					}
					else
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 1, "N/A");
					}
					lngErrCode = 8;
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 1, FCConvert.ToString(rsRI.Get_Fields_Int16("NumberofPeriods")));
					lngErrCode = 9;
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 1, FCConvert.ToString(rsRI.Get_Fields_String("RateType")));
					lngErrCode = 10;
					if (LRN == 0)
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1, FCConvert.ToString(rsCL.Get_Fields_Int32("ID")));
					}
					else
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1, "");
					}
					lngErrCode = 11;
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 1, "");
					// rsRI.Get_Fields("CreationDate")
					lngErrCode = 12;
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 1, Strings.Format(rsRI.Get_Fields_DateTime("InterestStartDate1"), "MM/dd/yyyy"));
					lngErrCode = 13;
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 1, Strings.Format(rsRI.Get_Fields_DateTime("InterestStartDate2"), "MM/dd/yyyy"));
					lngErrCode = 14;
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 1, Strings.Format(rsRI.Get_Fields_DateTime("InterestStartDate3"), "MM/dd/yyyy"));
					lngErrCode = 15;
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 1, Strings.Format(rsRI.Get_Fields_DateTime("InterestStartDate4"), "MM/dd/yyyy"));
					lngErrCode = 16;
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 1, Strings.Format(rsRI.Get_Fields_DateTime("DueDate1"), "MM/dd/yyyy"));
					lngErrCode = 17;
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 1, Strings.Format(rsRI.Get_Fields_DateTime("DueDate2"), "MM/dd/yyyy"));
					lngErrCode = 18;
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 1, Strings.Format(rsRI.Get_Fields_DateTime("DueDate3"), "MM/dd/yyyy"));
					lngErrCode = 19;
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1, Strings.Format(rsRI.Get_Fields_DateTime("DueDate4"), "MM/dd/yyyy"));
					lngErrCode = 20;
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 1, Strings.Format(rsRI.Get_Fields_Double("InterestRate"), "#0.00%"));
					lngErrCode = 21;
					if (LRN == 0)
					{
						// this is not a lien
						lngErrCode = 22;
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1, FCConvert.ToString(modGlobal.Round((FCConvert.ToDouble(rsCL.Get_Fields_Decimal("TaxDue1")) - modStatusPayments.Statics.AbatePaymentsArray[FCUtils.iMod(rsCL.Get_Fields_Int32("BillingYear"), 1000)].Per1) - (FCConvert.ToDouble(rsCL.Get_Fields_Decimal("InterestCharged")) - modStatusPayments.Statics.dblCurrentInt[(intYear) - modExtraModules.DEFAULTSUBTRACTIONVALUE]) + FCConvert.ToDouble(rsCL.Get_Fields_Decimal("DemandFees")), 2)));
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1, FCConvert.ToString(modGlobal.Round(FCConvert.ToDouble(rsCL.Get_Fields_Decimal("TaxDue2")) - modStatusPayments.Statics.AbatePaymentsArray[FCUtils.iMod(rsCL.Get_Fields_Int32("BillingYear"), 1000)].Per2, 2)));
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 1, FCConvert.ToString(modGlobal.Round(FCConvert.ToDouble(rsCL.Get_Fields_Decimal("TaxDue3")) - modStatusPayments.Statics.AbatePaymentsArray[FCUtils.iMod(rsCL.Get_Fields_Int32("BillingYear"), 1000)].Per3, 2)));
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 1, FCConvert.ToString(modGlobal.Round(FCConvert.ToDouble(rsCL.Get_Fields_Decimal("TaxDue4")) - modStatusPayments.Statics.AbatePaymentsArray[FCUtils.iMod(rsCL.Get_Fields_Int32("BillingYear"), 1000)].Per4, 2)));
						if (rsCL.Get_Fields_DateTime("InterestAppliedThroughDate") is DateTime)
						{
							dtInterestPDThroughDate = rsCL.Get_Fields_DateTime("InterestAppliedThroughDate");
						}
						else
						{
							dtInterestPDThroughDate = DateTime.FromOADate(0);
						}
						// this will find the total abatement amount to take out of the payment totals
						dblTotalAbate = modStatusPayments.Statics.AbatePaymentsArray[FCUtils.iMod(rsCL.Get_Fields_Int32("BillingYear"), 1000)].Per1 + modStatusPayments.Statics.AbatePaymentsArray[FCUtils.iMod(rsCL.Get_Fields_Int32("BillingYear"), 1000)].Per2 + modStatusPayments.Statics.AbatePaymentsArray[FCUtils.iMod(rsCL.Get_Fields_Int32("BillingYear"), 1000)].Per3 + modStatusPayments.Statics.AbatePaymentsArray[FCUtils.iMod(rsCL.Get_Fields_Int32("BillingYear"), 1000)].Per4;
						// VBto upgrade warning: totPaid As double	OnWrite(double, double)
						double totPaid = 0;
						// need to use the lien costs paid even though it is not a lien because of demand fees
						totPaid = FCConvert.ToDouble(rsCL.Get_Fields_Decimal("PrincipalPaid")) + FCConvert.ToDouble(rsCL.Get_Fields_Decimal("InterestPaid")) + FCConvert.ToDouble(rsCL.Get_Fields_Decimal("DemandFeesPaid")) - dblTotalAbate;
						if (totPaid > FCConvert.ToDouble(frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1)))
						{
							totPaid -= FCConvert.ToDouble(frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1));
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1, "");
							if (totPaid > FCConvert.ToDouble(frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1)))
							{
								totPaid -= FCConvert.ToDouble(frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1));
								frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1, "");
								if (totPaid > FCConvert.ToDouble(frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 1)))
								{
									totPaid -= FCConvert.ToDouble(frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 1));
									frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 1, "");
									frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 1, FCConvert.ToString(FCConvert.ToDouble(frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 1)) - totPaid));
								}
								else
								{
									frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 1, FCConvert.ToString(FCConvert.ToDouble(frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 1)) - totPaid));
								}
							}
							else
							{
								frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1, FCConvert.ToString(FCConvert.ToDouble(frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1)) - totPaid));
							}
						}
						else
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1, FCConvert.ToString(FCConvert.ToDouble(frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1)) - totPaid));
						}
					}
					else
					{
						// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1, FCConvert.ToString((Conversion.Val(rsLR.Get_Fields("Principal")) - Conversion.Val(rsLR.Get_Fields_Decimal("PrincipalPaid"))) + (Conversion.Val(rsLR.Get_Fields("Interest")) - Conversion.Val(rsLR.Get_Fields_Decimal("InterestCharged")) - Conversion.Val(rsLR.Get_Fields_Decimal("InterestPaid")) - Conversion.Val(rsLR.Get_Fields("PLIPaid"))) + (Conversion.Val(rsLR.Get_Fields("Costs")) - Conversion.Val(rsLR.Get_Fields("MaturityFee")) - Conversion.Val(rsLR.Get_Fields_Decimal("CostsPaid"))) + modStatusPayments.Statics.dblCurrentInt[(intYear) - modExtraModules.DEFAULTSUBTRACTIONVALUE]));
						dtInterestPDThroughDate = (DateTime)rsLR.Get_Fields_DateTime("InterestAppliedThroughDate");
					}
					if (frmRECLStatus.InstancePtr.GRID.IsCollapsed(cRow) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						int l = 0;
						l = frmRECLStatus.InstancePtr.GRID.FindRow("=", cRow, lngColGridCode) + 1;
						if (l != -1)
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1, Strings.Format(frmRECLStatus.InstancePtr.GRID.TextMatrix(l, lngColPerDiem), "#,##0.0000"));
						}
					}
					else
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1, Strings.Format(frmRECLStatus.InstancePtr.GRID.TextMatrix(cRow, lngColPerDiem), "#,##0.0000"));
					}
					// this checks to see if the dates are 0 and replaces them with 0
					for (I = 5; I <= frmRECLStatus.InstancePtr.vsRateInfo.Rows - 1; I++)
					{
						//FC:FINAL:DSE:#881 Use correct date string for empty date
						//if (frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(I, 1) == "12:00:00 AM")
						if (frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(I, 1) == Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(I, 1, "");
						if (Strings.Right(frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(I, 1), 1) == "%")
						{
							if (Conversion.Val(Strings.Left(frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(I, 1), frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(I, 1).Length - 1)) == 0)
							{
								frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(I, 1, "");
							}
						}
						else if (Conversion.Val(frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(I, 1)) == 0)
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(I, 1, "");
						}
						else
						{
							if (I >= 15 && I <= 18)
							{
								frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(I, 1, Strings.Format(FCConvert.ToDouble(frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(I, 1)), "#,##0.00"));
							}
						}
					}
				}
				else
				{
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 0, "");
				}
				// show the second set of information (valuations)-------------------- Col #2
				if (modStatusPayments.Statics.boolRE)
				{
					// RE
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 3, modExtraModules.FormatYear(intYear.ToString()) + " Information");
					frmRECLStatus.InstancePtr.vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 3, true);
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 3, "Land Value");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 3, "Building Value");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 3, "Exempt Value");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 3, "Net Value");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 3, "Current Information");
					frmRECLStatus.InstancePtr.vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 5, 3, true);
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 3, "Land Value");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 3, "Building Value");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 3, "Exempt Value");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 3, "Net Value");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 3, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 3, "Interest Paid Date");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 3, "Total Per Diem");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 3, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 3, "Billing Date");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 3, "Creation Date");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 3, "Commitment Date");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 3, "");
					// "Account Group"
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 3, "Maplot");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 3, "Book Page");
					// "Mailing Address"
					//FC:FINAL:AM:#4 - merge the cells
					//FC:TEMP:DSE Merged cells do not display well Merged cells do not display well
					//this.vsRateInfo.MergeRow(0, true, 3, 4);
					//frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 4, frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 3));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 4, frmRECLStatus.InstancePtr.lblLandValue2.Text);
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 4, frmRECLStatus.InstancePtr.lblBuildingValue2.Text);
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 4, frmRECLStatus.InstancePtr.lblExemptValue2.Text);
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 4, frmRECLStatus.InstancePtr.lblTotalValue2.Text);
					//FC:FINAL:AM:#4 - merge the cells
					//FC:TEMP:DSE Merged cells do not display well
					//this.vsRateInfo.MergeRow(5, true, 3, 4);
					//frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 4, frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 3));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 4, frmRECLStatus.InstancePtr.lblLandValue.Text);
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 4, frmRECLStatus.InstancePtr.lblBuildingValue.Text);
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 4, frmRECLStatus.InstancePtr.lblExemptValue.Text);
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 4, frmRECLStatus.InstancePtr.lblTotalValue.Text);
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 4, "");
					if (dtInterestPDThroughDate.ToOADate() != 0)
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 4, Strings.Format(dtInterestPDThroughDate, "MM/dd/yyyy"));
					}
					else
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 4, "");
					}
					// .TextMatrix(12, 4) = frmRECLStatus.lblMapLot.Caption
					// .TextMatrix(13, 4) = frmRECLStatus.lblReference1.Caption
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 4, Strings.Format(dblTotalPerDiem, "#,##0.0000"));
					if (rsRI.BeginningOfFile() != true && rsRI.EndOfFile() != true)
					{
						// this will show the acreage and book page from the bill record
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 4, rsRI.Get_Fields_DateTime("BillingDate").ToString("d"));
						// rsCL.Get_Fields("Acres")  'frmRECLStatus.lblAcreage.Caption
						if (rsRI.Get_Fields_DateTime("CreationDate") is DateTime)
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 4, rsRI.Get_Fields_DateTime("CreationDate").ToString("d"));
							// rsCL.Get_Fields("Acres")  'frmRECLStatus.lblAcreage.Caption
						}
						else
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 4, "");
						}
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 4, rsRI.Get_Fields_DateTime("CommitmentDate").ToString("d"));
						// rsCL.Get_Fields("BookPage") ' strCurrentBookPage
					}
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 4, "");
					// lngGroupNumber
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 4, Strings.Trim(rsCL.Get_Fields_String("MapLot") + " "));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 4, Strings.Trim(rsCL.Get_Fields_String("BookPage") + " "));
					if (LRN > 0)
					{
						// .rows = .rows + 1
						// .Height = .RowHeight(.rows - 1) + .RowHeight
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(20, 3, "Lien Book Page");
						if (!rsLR.IsFieldNull("book"))
						{
							// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(20, 4, "B " + FCConvert.ToString(rsLR.Get_Fields("Book")) + " P " + FCConvert.ToString(rsLR.Get_Fields("Page")));
						}
						clsDRWrapper rsTemp = new clsDRWrapper();
						rsTemp.OpenRecordset("select * from dischargeneeded where lienkey = " + FCConvert.ToString(rsLR.Get_Fields_Int32("ID")), "twcl0000.vb1");
						if (!rsTemp.EndOfFile())
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(21, 3, "Lien Discharge Book Page");
							if (!rsTemp.IsFieldNull("book"))
							{
								// TODO: Check the table for the column [book] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
								frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(21, 4, "B " + FCConvert.ToString(rsTemp.Get_Fields("book")) + " P " + FCConvert.ToString(rsTemp.Get_Fields("Page")));
							}
						}
					}
				}
				else
				{
					// PP
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 5, modExtraModules.FormatYear(intYear.ToString()));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 4, "Current");
					frmRECLStatus.InstancePtr.vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 4, 0, 5, true);
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 3, "Cat 1");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 3, "Cat 2");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 3, "Cat 3");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 3, "Cat 4");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 3, "Cat 5");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 3, "Cat 6");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 3, "Cat 7");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 3, "Cat 8");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 3, "Cat 9");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 3, "Total");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 3, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 3, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 3, "Interest Paid Date");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 3, "Total Per Diem");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 3, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 3, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 3, "Account Group");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 3, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 3, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 4, vsPPCategory.TextMatrix(1, 1));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 4, vsPPCategory.TextMatrix(2, 1));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 4, vsPPCategory.TextMatrix(3, 1));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 4, vsPPCategory.TextMatrix(4, 1));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 4, vsPPCategory.TextMatrix(5, 1));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 4, vsPPCategory.TextMatrix(6, 1));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 4, vsPPCategory.TextMatrix(7, 1));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 4, vsPPCategory.TextMatrix(8, 1));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 4, vsPPCategory.TextMatrix(9, 1));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 4, vsPPCategory.TextMatrix(10, 1));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 4, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 4, "");
					if (dtInterestPDThroughDate.ToOADate() != 0)
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 5, Strings.Format(dtInterestPDThroughDate, "MM/dd/yyyy"));
					}
					else
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 5, "");
					}
					//FC:FINAL:AM: merge the cells
					//FC:TEMP:DSE Merged cells do not display well
					//this.vsRateInfo.MergeRow(14, true, 3, 4);
					//this.vsRateInfo.MergeRow(17, true, 3, 4);
					//frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 4, "Total Per Diem");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 4, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 4, "");
					//frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 4, "Account Group");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 4, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 4, "");
					// extra column values
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 5, vsPPCategory.TextMatrix(1, 2));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 5, vsPPCategory.TextMatrix(2, 2));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 5, vsPPCategory.TextMatrix(3, 2));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 5, vsPPCategory.TextMatrix(4, 2));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 5, vsPPCategory.TextMatrix(5, 2));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 5, vsPPCategory.TextMatrix(6, 2));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 5, vsPPCategory.TextMatrix(7, 2));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 5, vsPPCategory.TextMatrix(8, 2));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 5, vsPPCategory.TextMatrix(9, 2));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 5, vsPPCategory.TextMatrix(10, 2));
					// double this up so that it will merge
					//FC:FINAL:AM:#4 - merge the cells
					//FC:TEMP:DSE Merged cells do not display well
					//this.vsRateInfo.MergeRow(11, true, 4, 5);
					//this.vsRateInfo.MergeRow(12, true, 4, 5);
					//frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 5, frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 4));
					//frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 5, frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 4));
					// .TextMatrix(13, 4) = .TextMatrix(13, 3)
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 5, Strings.Format(dblTotalPerDiem, "#,##0.0000"));
					//FC:TEMP:DSE Merged cells do not display well
					//this.vsRateInfo.MergeRow(15, true, 4, 5);
					//this.vsRateInfo.MergeRow(16, true, 4, 5);
					//this.vsRateInfo.MergeRow(18, true, 4, 5);
					//this.vsRateInfo.MergeRow(19, true, 4, 5);
					//frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 5, frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 4));
					//frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 5, frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 4));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 5, FCConvert.ToString(lngGroupNumber));
					//frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 5, frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 4));
					//frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 5, frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 4));
				}
				if (modStatusPayments.Statics.boolRE)
				{
					//FC:TEMP:DSE Merged cells do not display well
					//frmRECLStatus.InstancePtr.vsRateInfo.MergeRow(0, true);
					//frmRECLStatus.InstancePtr.vsRateInfo.MergeRow(5, true);
					frmRECLStatus.InstancePtr.vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, 0, 4, FCGrid.AlignmentSettings.flexAlignCenterCenter);
					frmRECLStatus.InstancePtr.vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 5, 3, 5, 4, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				}
				else
				{
					//FC:TEMP:DSE Merged cells do not display well
					//frmRECLStatus.InstancePtr.vsRateInfo.MergeRow(13, true);
					//frmRECLStatus.InstancePtr.vsRateInfo.MergeRow(14, true);
					//frmRECLStatus.InstancePtr.vsRateInfo.MergeRow(17, true);
				}
				// show the frame-----------------------------------------------------
				frmRECLStatus.InstancePtr.fraRateInfo.Text = "Rate Information - " + modExtraModules.FormatYear(intYear.ToString());
				frmRECLStatus.InstancePtr.fraRateInfo.Left = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Width - frmRECLStatus.InstancePtr.fraRateInfo.Width) / 2.0);
				//frmRECLStatus.InstancePtr.fraRateInfo.Top = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Height - frmRECLStatus.InstancePtr.fraRateInfo.Height) / 2.0);
				//FC:FINAL:DDU:#i170 bring to front the close buton
				//frmRECLStatus.InstancePtr.cmdRIClose.Top = frmRECLStatus.InstancePtr.fraRateInfo.Height - cmdRIClose.Height - 2;
				frmRECLStatus.InstancePtr.cmdRIClose.Left = FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.Width / 2.0F);
				frmRECLStatus.InstancePtr.cmdRIClose.Focus();
				frmRECLStatus.InstancePtr.fraRateInfo.Visible = true;
				frmRECLStatus.InstancePtr.vsRateInfo.SendToBack();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Rate Information Error" + " - " + FCConvert.ToString(lngErrCode));
			}
		}

		private void ShowPaymentInfo(int cRow, int cCol)
		{
			int lngErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will show a box with the rate information in it
				// as soon as the box loses focus, I will make it disappear
				clsDRWrapper rsRI = new clsDRWrapper();
				clsDRWrapper rsLR = new clsDRWrapper();
				int intYear/*unused?*/;
				int I/*unused?*/;
				int RK/*unused?*/;
				int LRN/*unused?*/;
				lngErrCode = 1;
				if (Conversion.Val(frmRECLStatus.InstancePtr.GRID.TextMatrix(cRow, lngColPaymentID)) > 0 && Conversion.Val(frmRECLStatus.InstancePtr.GRID.TextMatrix(cRow, lngColGridCode)) >= 0)
				{
					// payment line
					rsRI.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountPayments + ") AS qCAP WHERE ID = " + frmRECLStatus.InstancePtr.GRID.TextMatrix(cRow, lngColPaymentID), modExtraModules.strCLDatabase);
					// format grid
					frmRECLStatus.InstancePtr.vsRateInfo.Cols = 1;
					frmRECLStatus.InstancePtr.vsRateInfo.Cols = 5;
					frmRECLStatus.InstancePtr.vsRateInfo.Rows = 20;
					frmRECLStatus.InstancePtr.vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
					frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(0, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.3));
					frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(1, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.19));
					frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(2, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.01));
					frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(3, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.3));
					frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(4, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.19));
					frmRECLStatus.InstancePtr.vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
					//frmRECLStatus.InstancePtr.vsRateInfo.Height = frmRECLStatus.InstancePtr.vsRateInfo.RowHeight(0) * frmRECLStatus.InstancePtr.vsRateInfo.Rows;
					// this will hide the border of the other information on the shared grid
					frmRECLStatus.InstancePtr.vsRateInfo.Select(19, 0, 19, 1);
					frmRECLStatus.InstancePtr.vsRateInfo.CellBorder(ColorTranslator.FromOle(0), 0, 0, 0, 0, 0, 0);
					frmRECLStatus.InstancePtr.vsRateInfo.Select(15, 0, 15, 1);
					frmRECLStatus.InstancePtr.vsRateInfo.CellBorder(ColorTranslator.FromOle(0), 0, 0, 0, 0, 0, 0);
					frmRECLStatus.InstancePtr.vsRateInfo.Select(0, 0);
					frmRECLStatus.InstancePtr.vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 19, 1, 19, 4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					frmRECLStatus.InstancePtr.vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
					// show the first set of information (rate information and period info)--------------
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 0, "Account");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 0, "Tax Year");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 0, "Bill ID");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 0, "Receipt Number");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 0, "Recorded Transaction");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 0, "Effective Interest Date");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 0, "Actual Transaction Date");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 0, "Teller ID");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 0, "Cash Drawer");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 0, "Affect Cash");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 0, "BD Account");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 3, "ID");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 3, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 3, "Reference");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 3, "Paid By");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 3, "Daily Close Out");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 3, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 3, "Principal");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 3, "Interest");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 3, "Pre Lien Interest");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 3, "Lien Costs");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 3, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 3, "Total");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 0, "Comments");
					lngErrCode = 1;
					if (rsRI.BeginningOfFile() != true && rsRI.EndOfFile() != true)
					{
						lngErrCode = 2;
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 1, FCConvert.ToString(rsRI.Get_Fields("Account")));
						lngErrCode = 3;
						// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 1, modExtraModules.FormatYear(FCConvert.ToString(rsRI.Get_Fields("Year"))));
						lngErrCode = 4;
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 1, FCConvert.ToString(rsRI.Get_Fields_Int32("BillKey")));
						lngErrCode = 5;
						// check to see if this is a payment from the DOS system
						lngErrCode = 6;
						if (FCConvert.ToBoolean(rsRI.Get_Fields_Boolean("DOSReceiptNumber")))
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1, FCConvert.ToString(rsRI.Get_Fields_Int32("ReceiptNumber")));
						}
						else
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1, GetActualReceiptNumber_8(FCConvert.ToInt32(rsRI.Get_Fields_Int32("ReceiptNumber")), rsRI.Get_Fields_DateTime("ActualSystemDate")));
						}
						lngErrCode = 7;
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 1, "");
						lngErrCode = 8;
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 1, Strings.Format(rsRI.Get_Fields_DateTime("RecordedTransactionDate"), "MM/dd/yyyy"));
						lngErrCode = 9;
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 1, Strings.Format(rsRI.Get_Fields_DateTime("EffectiveInterestDate"), "MM/dd/yyyy"));
						lngErrCode = 10;
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 1, Strings.Format(rsRI.Get_Fields_DateTime("ActualSystemDate"), "MM/dd/yyyy"));
						lngErrCode = 11;
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 1, FCConvert.ToString(rsRI.Get_Fields_String("Teller")));
						lngErrCode = 12;
						if (FCConvert.ToString(rsRI.Get_Fields_String("CashDrawer")) == "Y")
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 1, "YES");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1, "YES");
						}
						else
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 1, "NO");
							if (FCConvert.ToString(rsRI.Get_Fields_String("GeneralLedger")) == "Y")
							{
								frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1, "YES");
							}
							else
							{
								frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1, "NO");
								frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 1, FCConvert.ToString(rsRI.Get_Fields_String("BudgetaryAccountNumber")));
							}
						}
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 4, FCConvert.ToString(rsRI.Get_Fields_Int32("ID")));
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 4, FCConvert.ToString(rsRI.Get_Fields_String("Reference")));
						lngErrCode = 13;
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 4, FCConvert.ToString(rsRI.Get_Fields_String("PaidBy")));
						lngErrCode = 14;
						if (FCConvert.ToBoolean(rsRI.Get_Fields_Int32("DailyCloseOut")))
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 4, "True");
						}
						else
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 4, "False");
						}
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 4, "");
						lngErrCode = 15;
						// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 4, Strings.Format(rsRI.Get_Fields("Principal"), "#,##0.00"));
						lngErrCode = 16;
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 4, Strings.Format(rsRI.Get_Fields_Decimal("CurrentInterest"), "#,##0.00"));
						lngErrCode = 17;
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 4, Strings.Format(rsRI.Get_Fields_Decimal("PreLienInterest"), "#,##0.00"));
						lngErrCode = 18;
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 4, Strings.Format(rsRI.Get_Fields_Decimal("LienCost"), "#,##0.00"));
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 4, "");
						// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 4, Strings.Format(Conversion.Val(rsRI.Get_Fields("Principal")) + Conversion.Val(rsRI.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsRI.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsRI.Get_Fields_Decimal("LienCost")), "#,##0.00"));
						lngErrCode = 19;
						//FC:FINAL:AM: merge the cells
						this.vsRateInfo.MergeRow(19, true, 1, 4);
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1, FCConvert.ToString(rsRI.Get_Fields_String("Comments")));
						//frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 2, frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1));
						//frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 3, frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1));
						//frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 4, frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1));
						//frmRECLStatus.InstancePtr.vsRateInfo.MergeRow(19, true);
					}
					else
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 0, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 0, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 0, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 0, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 0, "");
					}
					// show the second set of information (valuations)--------------------
					if (modStatusPayments.Statics.boolRE)
					{
						// RE
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 0, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 0, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 0, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 0, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 3, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 3, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 3, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 3, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 3, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 3, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 3, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 4, "");
					}
					else
					{
						// PP
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 0, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 0, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 0, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 0, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 3, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 3, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 3, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 3, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 3, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 3, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 3, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 4, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 4, "");
					}
					// show the frame-----------------------------------------------------
					frmRECLStatus.InstancePtr.fraRateInfo.Text = "Payment Information";
					frmRECLStatus.InstancePtr.fraRateInfo.Left = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Width - frmRECLStatus.InstancePtr.fraRateInfo.Width) / 2.0);
					//frmRECLStatus.InstancePtr.fraRateInfo.Top = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Height - frmRECLStatus.InstancePtr.fraRateInfo.Height) / 2.0);
					//FC:FINAL:DDU:#i170 bring to front the close buton
					//frmRECLStatus.InstancePtr.cmdRIClose.Top = frmRECLStatus.InstancePtr.fraRateInfo.Height - cmdRIClose.Height - 2;
					frmRECLStatus.InstancePtr.cmdRIClose.Left = FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.Width / 2.0F);
					frmRECLStatus.InstancePtr.cmdRIClose.Focus();
					frmRECLStatus.InstancePtr.fraRateInfo.Visible = true;
					frmRECLStatus.InstancePtr.vsRateInfo.SendToBack();
				}
				else
				{
					// check to see if this is a lien record
					if (FCConvert.ToBoolean(frmRECLStatus.InstancePtr.GRID.TextMatrix(cRow, lngColGridCode)) == true && Conversion.Val(GRID.TextMatrix(cRow, lngColPaymentID)) > 0)
					{
						// show the liened record info
						rsLR.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + GRID.TextMatrix(cRow, lngColPaymentID), modExtraModules.strCLDatabase);
						if (rsLR.EndOfFile() != true && rsLR.BeginningOfFile() != true)
						{
							// format grid
							frmRECLStatus.InstancePtr.vsRateInfo.Cols = 1;
							frmRECLStatus.InstancePtr.vsRateInfo.Cols = 5;
							frmRECLStatus.InstancePtr.vsRateInfo.Rows = 1;
							frmRECLStatus.InstancePtr.vsRateInfo.Rows = 19;
							frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(0, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.4));
							frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(1, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.585));
							frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(2, frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0);
							frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(3, frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0);
							frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(4, frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0);
							frmRECLStatus.InstancePtr.vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
							//frmRECLStatus.InstancePtr.vsRateInfo.Height = frmRECLStatus.InstancePtr.vsRateInfo.RowHeight(0) * frmRECLStatus.InstancePtr.vsRateInfo.Rows;
							// .Select 10, 0, 10, 1
							// .CellBorder 0, 0, 0, 0, 0, 0, 0
							// .Select 15, 0, 15, 1
							// .CellBorder 0, 0, 0, 0, 0, 0, 0
							// .Select 19, 0, 19, 1
							frmRECLStatus.InstancePtr.vsRateInfo.CellBorder(ColorTranslator.FromOle(0), 0, 1, 0, 0, 0, 1);
							frmRECLStatus.InstancePtr.vsRateInfo.Select(15, 0, 15, 1);
							frmRECLStatus.InstancePtr.vsRateInfo.CellBorder(ColorTranslator.FromOle(0), 0, 0, 0, 0, 0, 0);
							frmRECLStatus.InstancePtr.vsRateInfo.Select(0, 0);
							frmRECLStatus.InstancePtr.vsRateInfo.ExtendLastCol = true;
							frmRECLStatus.InstancePtr.vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, frmRECLStatus.InstancePtr.vsRateInfo.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
							frmRECLStatus.InstancePtr.vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
							// show the first set of information (rate information and period info)--------------
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 0, "Lien Record Number");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 0, "Rate Record");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 0, "Status");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 0, "Date Created");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 0, "Interest Applied Through");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 0, "Book / Page");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 0, "Principal");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 0, "Interest");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 0, "Costs");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 0, "Interest Charged");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 0, "Maturity Fee");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 0, "Lien Total");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 0, "");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 0, "Principal Paid");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 0, "Interest Paid");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 0, "Costs Paid");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 0, "Total Paid");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 0, "");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 0, "Total Due");
							// fill in the actual data
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 1, FCConvert.ToString(rsLR.Get_Fields_Int32("ID")));
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 1, FCConvert.ToString(rsLR.Get_Fields_Int32("RateKey")));
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 1, FCConvert.ToString(rsLR.Get_Fields_String("Status")));
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 1, Strings.Format(rsLR.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy"));
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1, Strings.Format(rsLR.Get_Fields_DateTime("InterestAppliedThroughDate"), "MM/dd/yyyy"));
							if (!rsLR.IsFieldNull("Book") || !rsLR.IsFieldNull("Page"))
							{
								// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
								frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 1, "B" + FCConvert.ToString(rsLR.Get_Fields("Book")) + "P" + FCConvert.ToString(rsLR.Get_Fields("Page")));
							}
							else
							{
								frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 1, "");
							}
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 1, Strings.Format(rsLR.Get_Fields("Principal"), "#,##0.00"));
							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 1, Strings.Format(rsLR.Get_Fields("Interest"), "#,##0.00"));
							// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 1, Strings.Format(rsLR.Get_Fields("Costs"), "#,##0.00"));
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 1, Strings.Format(rsLR.Get_Fields_Decimal("InterestCharged") * -1, "#,##0.00"));
							// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 1, Strings.Format(rsLR.Get_Fields("MaturityFee") * -1, "#,##0.00"));
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 1, Strings.Format(Conversion.Val(rsLR.Get_Fields("Principal")) + Conversion.Val(rsLR.Get_Fields("Interest")) + Conversion.Val(rsLR.Get_Fields("Costs")) - (Conversion.Val(rsLR.Get_Fields_Decimal("InterestCharged")) + Conversion.Val(rsLR.Get_Fields("MaturityFee"))), "#,##0.00"));
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 1, "");
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1, Strings.Format(rsLR.Get_Fields_Decimal("PrincipalPaid"), "#,##0.00"));
							// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 1, Strings.Format(Conversion.Val(rsLR.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsLR.Get_Fields("PLIPaid")), "#,##0.00"));
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1, Strings.Format(rsLR.Get_Fields_Decimal("CostsPaid"), "#,##0.00"));
							// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1, Strings.Format(Conversion.Val(rsLR.Get_Fields_Decimal("PrincipalPaid")) + Conversion.Val(rsLR.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsLR.Get_Fields("PLIPaid")) + Conversion.Val(rsLR.Get_Fields_Decimal("CostsPaid")), "#,##0.00"));
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 1, "");
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 1, Strings.Format((Conversion.Val(rsLR.Get_Fields("Principal")) + Conversion.Val(rsLR.Get_Fields("Interest")) + Conversion.Val(rsLR.Get_Fields("Costs")) - (Conversion.Val(rsLR.Get_Fields_Decimal("InterestCharged")) + Conversion.Val(rsLR.Get_Fields("MaturityFee")))) - (Conversion.Val(rsLR.Get_Fields_Decimal("PrincipalPaid")) + Conversion.Val(rsLR.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsLR.Get_Fields("PLIPaid")) + Conversion.Val(rsLR.Get_Fields_Decimal("CostsPaid"))), "#,##0.00"));
							// show the frame-----------------------------------------------------
							frmRECLStatus.InstancePtr.fraRateInfo.Text = "Lien Information";
							frmRECLStatus.InstancePtr.fraRateInfo.Left = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Width - frmRECLStatus.InstancePtr.fraRateInfo.Width) / 2.0);
							//frmRECLStatus.InstancePtr.fraRateInfo.Top = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Height - frmRECLStatus.InstancePtr.fraRateInfo.Height) / 2.0);
							//FC:FINAL:DDU:#i170 bring to front the close buton
							//frmRECLStatus.InstancePtr.cmdRIClose.Top = frmRECLStatus.InstancePtr.fraRateInfo.Height - cmdRIClose.Height - 2;
							frmRECLStatus.InstancePtr.cmdRIClose.Left = FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.Width / 2.0F);
							frmRECLStatus.InstancePtr.cmdRIClose.Focus();
							frmRECLStatus.InstancePtr.fraRateInfo.Visible = true;
							frmRECLStatus.InstancePtr.vsRateInfo.SendToBack();
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Rate Information Error" + " - " + FCConvert.ToString(lngErrCode));
			}
		}

		private void ShowAccountInfo()
		{
			int lngErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will show a frame with a lot of the account information that you would find in RE
				// as soon as the box loses focus, I will make it disappear
				clsDRWrapper rsAccountInfo = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				clsDRWrapper clsTemp = new clsDRWrapper();
				string strBook = "";
				int lngSFLA = 0;
				int intEx1 = 0;
				int intEx2 = 0;
				int intEx3 = 0;
				string strEx1 = "";
				string strEx2 = "";
				string strEx3 = "";
				lngErrCode = 1;
				if (modStatusPayments.Statics.boolRE)
				{
					// find the lngSQFA
					rsTemp.OpenRecordset("SELECT * FROM DWelling WHERE RSCard = 1 AND RSAccount = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE), modExtraModules.strREDatabase);
					if (!rsTemp.EndOfFile())
					{
						lngSFLA = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields_Int32("DISfla"))));
					}
					else
					{
						lngSFLA = 0;
					}
					rsAccountInfo.OpenRecordset("SELECT DeedName1 AS Name, DeedName2 AS Name2, pOwn1.Address1, pOwn1.Address2, pOwn1.City, pOwn1.State, pOwn1.Zip, " + "RSLocNumAlph, RSLocApt, RSLocStreet, RSMapLot, RSAccount, TaxAcquired, TADate, RSRef1, RITranCode " + "FROM Master INNER JOIN " + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn1 ON Master.OwnerPartyID = pOwn1.ID " + "LEFT JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pOwn2 ON Master.SecOwnerPartyID = pOwn2.ID " + "WHERE RSAccount = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND RSCard = 1", modExtraModules.strREDatabase);
					// format grid
					frmRECLStatus.InstancePtr.vsRateInfo.Cols = 1;
					frmRECLStatus.InstancePtr.vsRateInfo.Cols = 2;
					frmRECLStatus.InstancePtr.vsRateInfo.Rows = 20;
					frmRECLStatus.InstancePtr.vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
					frmRECLStatus.InstancePtr.vsRateInfo.ExtendLastCol = true;
					frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(0, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.35));
					frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(1, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.55));
					frmRECLStatus.InstancePtr.vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
					//frmRECLStatus.InstancePtr.vsRateInfo.Height = frmRECLStatus.InstancePtr.vsRateInfo.RowHeight(0) * frmRECLStatus.InstancePtr.vsRateInfo.Rows;
					// this will hide the border of the other information on the shared grid
					frmRECLStatus.InstancePtr.vsRateInfo.Select(19, 0, 19, 1);
					frmRECLStatus.InstancePtr.vsRateInfo.CellBorder(ColorTranslator.FromOle(0), 0, 0, 0, 0, 0, 0);
					frmRECLStatus.InstancePtr.vsRateInfo.Select(15, 0, 15, 1);
					frmRECLStatus.InstancePtr.vsRateInfo.CellBorder(ColorTranslator.FromOle(0), 0, 0, 0, 0, 0, 0);
					frmRECLStatus.InstancePtr.vsRateInfo.Select(0, 0);
					frmRECLStatus.InstancePtr.vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, frmRECLStatus.InstancePtr.vsRateInfo.Rows - 1, 0, frmRECLStatus.InstancePtr.vsRateInfo.Rows - 1, frmRECLStatus.InstancePtr.vsRateInfo.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					frmRECLStatus.InstancePtr.vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
					// show the first set of information (rate information and period info)--------------
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 0, "Current Owner:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 0, "Second Owner:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 0, "Location:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 0, "Mailing Address:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 0, "Map Lot:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 0, "Book Page(s):");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 0, "Acres:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 0, "Tax Acquired:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 0, "Associated PP Account:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 0, "Outstanding PP Amount:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 0, "Account Group:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 0, "Reference:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 0, "Last Calculated SFLA:");
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 0, "Tran Code");
					}
					else
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 0, "");
					}
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 0, "Exempt Codes:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Name"))));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Name2"))));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("RSLOCNUMALPH")) + " " + FCConvert.ToString(rsAccountInfo.Get_Fields_String("RSLOCAPT")) + " " + FCConvert.ToString(rsAccountInfo.Get_Fields_String("RSLOCSTREET"))));
					// location
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Address1"))));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Address2"))));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 1, Strings.Trim(Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("City"))) + ", " + FCConvert.ToString(rsAccountInfo.Get_Fields_String("State")) + " " + FCConvert.ToString(rsAccountInfo.Get_Fields_String("Zip"))));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 1, "");
					// blank line
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("RSMapLot"))));
					rsTemp.OpenRecordset("SELECT * FROM BookPage WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND [Current] = 1 ORDER BY Line desc", modExtraModules.strREDatabase);
					strBook = "";
					while (!rsTemp.EndOfFile())
					{
						// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
						strBook += "B" + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Book"))) + "P" + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Page")));
						rsTemp.MoveNext();
					}
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 1, strBook);
					clsTemp.OpenRecordset("SELECT SUM(PIACRES) AS AcreTotal,SUM(LastLandVal) AS LandTotal,SUM(LastBldgVal) AS BldgTotal,SUM(RLExemption) AS ExemptTotal FROM Master WHERE RSAccount = " + FCConvert.ToString(rsAccountInfo.Get_Fields_Int32("RSAccount")), modExtraModules.strREDatabase);
					if (!clsTemp.EndOfFile())
					{
						// TODO: Field [AcreTotal] not found!! (maybe it is an alias?)
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 1, FCConvert.ToString(Conversion.Val(clsTemp.Get_Fields("AcreTotal"))));
					}
					else
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 1, FCConvert.ToString(rsAccountInfo.Get_Fields_Double("PIAcres")));
					}
					if (FCConvert.ToBoolean(rsAccountInfo.Get_Fields_Boolean("TaxAcquired")))
					{
						if (Information.IsDate(rsAccountInfo.Get_Fields("TADate")) && rsAccountInfo.Get_Fields("TADate").ToOADate() > 0)
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 1, Strings.Format(rsAccountInfo.Get_Fields_DateTime("TADate"), "MM/dd/yyyy"));
						}
						else
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 1, "TRUE");
						}
					}
					else
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 1, "FALSE");
					}
					rsTemp.OpenRecordset("SELECT * FROM ModuleAssociation WHERE Module = 'PP' AND PrimaryAssociate = 1 AND REMasterAcct = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE), "CentralData");
					if (!rsTemp.EndOfFile())
					{
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 1, FCConvert.ToString(rsTemp.Get_Fields("Account")));
					}
					else
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 1, "NONE");
					}
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 1, Strings.Format(dblAssocAccountTotal, "$#,##0.00"));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1, FCConvert.ToString(lngGroupNumber));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("RSRef1"))));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1, FCConvert.ToString(lngSFLA));
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1, FCConvert.ToString(rsAccountInfo.Get_Fields_Int32("RITranCode")));
					}
					else
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1, "");
					}
					// Get exempt codes
					modGlobalFunctions.GetExemptCodesandDescriptions("RE",  StaticSettings.TaxCollectionValues.LastAccountRE, ref intEx1, ref strEx1, ref intEx2, ref strEx2, ref intEx3, ref strEx3);
					if (intEx1 != 0)
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 1, Strings.Format(intEx1, "00") + " - " + strEx1);
					}
					if (intEx2 != 0)
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 1, Strings.Format(intEx2, "00") + " - " + strEx2);
					}
					if (intEx3 != 0)
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1, Strings.Format(intEx3, "00") + " - " + strEx3);
					}
					lngErrCode = 1;
					// show the frame-----------------------------------------------------
					frmRECLStatus.InstancePtr.fraRateInfo.Text = "Real Estate Account Information";
					frmRECLStatus.InstancePtr.fraRateInfo.Left = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Width - frmRECLStatus.InstancePtr.fraRateInfo.Width) / 2.0);
					//frmRECLStatus.InstancePtr.fraRateInfo.Top = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Height - frmRECLStatus.InstancePtr.fraRateInfo.Height) / 2.0);
					//frmRECLStatus.InstancePtr.cmdRIClose.Top = frmRECLStatus.InstancePtr.fraRateInfo.Height - cmdRIClose.Height - 2;
					//FC:FINAL:DDU:#i162 bring to front the close buton
					frmRECLStatus.InstancePtr.cmdRIClose.Left = FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.Width / 2.0F);
					frmRECLStatus.InstancePtr.cmdRIClose.Focus();
					frmRECLStatus.InstancePtr.fraRateInfo.Visible = true;
					frmRECLStatus.InstancePtr.vsRateInfo.SendToBack();
				}
				else
				{
					// Personal Property Account
					rsAccountInfo.OpenRecordset("SELECT pOwn1.FullNameLF AS Name, pOwn1.Address1, pOwn1.Address2, pOwn1.City, pOwn1.State, pOwn1.Zip, " + "StreetNumber, Street, Open1, Open2, TranCode " + "FROM PPMaster INNER JOIN " + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn1 ON PPMaster.PartyID = pOwn1.ID " + "WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP), modExtraModules.strPPDatabase);
					// format grid
					frmRECLStatus.InstancePtr.vsRateInfo.Cols = 1;
					frmRECLStatus.InstancePtr.vsRateInfo.Cols = 2;
					frmRECLStatus.InstancePtr.vsRateInfo.Rows = 20;
					frmRECLStatus.InstancePtr.vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
					frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(0, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.35));
					frmRECLStatus.InstancePtr.vsRateInfo.ColWidth(1, FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.55));
					frmRECLStatus.InstancePtr.vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
					frmRECLStatus.InstancePtr.vsRateInfo.ExtendLastCol = true;
					//frmRECLStatus.InstancePtr.vsRateInfo.Height = frmRECLStatus.InstancePtr.vsRateInfo.RowHeight(0) * frmRECLStatus.InstancePtr.vsRateInfo.Rows;
					// this will hide the border of the other information on the shared grid
					frmRECLStatus.InstancePtr.vsRateInfo.Select(19, 0, 19, 1);
					frmRECLStatus.InstancePtr.vsRateInfo.CellBorder(ColorTranslator.FromOle(0), 0, 0, 0, 0, 0, 0);
					frmRECLStatus.InstancePtr.vsRateInfo.Select(15, 0, 15, 1);
					frmRECLStatus.InstancePtr.vsRateInfo.CellBorder(ColorTranslator.FromOle(0), 0, 0, 0, 0, 0, 0);
					frmRECLStatus.InstancePtr.vsRateInfo.Select(0, 0);
					frmRECLStatus.InstancePtr.vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, frmRECLStatus.InstancePtr.vsRateInfo.Rows - 1, 1, frmRECLStatus.InstancePtr.vsRateInfo.Rows - 1, frmRECLStatus.InstancePtr.vsRateInfo.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					frmRECLStatus.InstancePtr.vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 0, "Current Owner:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 0, "Location:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 0, "Mailing Address:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 0, "Open 1:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 0, "Open 2:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 0, "Associated RE Account:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 0, "Outstanding RE Amount:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 0, "Account Group:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 0, "");
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 0, "Tran Code:");
					}
					else
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 0, "");
					}
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 0, "Exempt Codes:");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 0, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 0, "");
					if (!rsAccountInfo.EndOfFile())
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Name"))));
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_Int32("StreetNumber")) + " " + FCConvert.ToString(rsAccountInfo.Get_Fields_String("Street"))));
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Address1"))));
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Address2"))));
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 1, Strings.Trim(Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("City"))) + ", " + FCConvert.ToString(rsAccountInfo.Get_Fields_String("State")) + " " + FCConvert.ToString(rsAccountInfo.Get_Fields_String("Zip"))));
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 1, "");
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Open1"))));
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Open2"))));
						if (modGlobal.Statics.gboolMultipleTowns)
						{
							// TODO: Check the table for the column [TranCode] and replace with corresponding Get_Field method
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1, FCConvert.ToString(rsAccountInfo.Get_Fields("TranCode")));
						}
						else
						{
							frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1, "");
						}
					}
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 1, "");
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 1, "");
					rsTemp.OpenRecordset("SELECT * FROM ModuleAssociation WHERE Module = 'PP' AND PrimaryAssociate = 1 AND Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP), "CentralData");
					if (!rsTemp.EndOfFile())
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 1, FCConvert.ToString(rsTemp.Get_Fields_Int32("REMasterAcct")));
					}
					else
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 1, "NONE");
					}
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1, Strings.Format(dblAssocAccountTotal, "$#,##0.00"));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 1, FCConvert.ToString(lngGroupNumber));
					frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1, "");
					// Get exempt codes
					modGlobalFunctions.GetExemptCodesandDescriptions("PP",  StaticSettings.TaxCollectionValues.LastAccountPP, ref intEx1, ref strEx1, ref intEx2, ref strEx2, ref intEx3, ref strEx3);
					if (intEx1 != 0)
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 1, Strings.Format(intEx1, "00") + " - " + strEx1);
					}
					if (intEx2 != 0)
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 1, Strings.Format(intEx2, "00") + " - " + strEx2);
					}
					if (intEx3 != 0)
					{
						frmRECLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1, Strings.Format(intEx3, "00") + " - " + strEx3);
					}
					// show the frame-----------------------------------------------------
					frmRECLStatus.InstancePtr.fraRateInfo.Text = "Personal Property Account Information";
					frmRECLStatus.InstancePtr.fraRateInfo.Left = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Width - frmRECLStatus.InstancePtr.fraRateInfo.Width) / 2.0);
					//frmRECLStatus.InstancePtr.fraRateInfo.Top = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Height - frmRECLStatus.InstancePtr.fraRateInfo.Height) / 2.0);
					//FC:FINAL:DDU:#i162 bring to front the close buton
					//frmRECLStatus.InstancePtr.cmdRIClose.Top = frmRECLStatus.InstancePtr.fraRateInfo.Height - cmdRIClose.Height - 2;
					frmRECLStatus.InstancePtr.cmdRIClose.Left = FCConvert.ToInt32(frmRECLStatus.InstancePtr.vsRateInfo.Width / 2.0F);
					frmRECLStatus.InstancePtr.cmdRIClose.Focus();
					frmRECLStatus.InstancePtr.fraRateInfo.Visible = true;
					frmRECLStatus.InstancePtr.vsRateInfo.SendToBack();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Rate Information Error" + " - " + FCConvert.ToString(lngErrCode));
			}
		}
		// VBto upgrade warning: 'Return' As string	OnWrite(string, int)
		private string GetActualReceiptNumber_8(int lngRN, DateTime dtDate)
		{
			return GetActualReceiptNumber(ref lngRN, ref dtDate);
		}

		private string GetActualReceiptNumber(ref int lngRN, ref DateTime dtDate)
		{
			string GetActualReceiptNumber = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will accept a long of a receipt number ID in the CR table
				// if the receipt does not exist or any other error occurs, the function will
				// return 0, otherwise it returns the receipt number shown on the receipt
				clsDRWrapper rsRN = new clsDRWrapper();
				if (lngRN > 0)
				{
					if (modGlobalConstants.Statics.gboolCR)
					{
						// kgk 09-18-2012 trocr-357   rsRN.OpenRecordset "SELECT * FROM Receipt WHERE ReceiptKey = " & lngRN, strCRDatabase
						rsRN.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND ReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00:00 AM' AND ReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59:59 PM'", modExtraModules.strCRDatabase);
						if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
						{
							GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
						}
						else
						{
							rsRN.OpenRecordset("SELECT * FROM LastYearReceipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND LastYearReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00:00 AM' AND LastYearReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59:59 PM'", modExtraModules.strCRDatabase);
							if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
							{
								GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
							}
							else
							{
								GetActualReceiptNumber = FCConvert.ToString(lngRN) + "*";
							}
						}
					}
					else
					{
						GetActualReceiptNumber = FCConvert.ToString(lngRN);
					}
				}
				else
				{
					GetActualReceiptNumber = FCConvert.ToString(0);
				}
				return GetActualReceiptNumber;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Returning Receipt Number");
			}
			return GetActualReceiptNumber;
		}

		private void CheckCD()
		{
			if (txtCD.Text == "N")
			{
				txtCash.Enabled = true;
				txtCash.TabStop = true;
			}
			else
			{
				if (txtCash.Text == "N")
					txtCash.Text = "Y";
				txtCash.TabStop = false;
				txtCash.Enabled = false;
				if (cmbCode.Items.Count != 0)
				{
					if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) != "D")
					{
						txtAcctNumber.TextMatrix(0, 0, "");
					}
				}
				txtAcctNumber.Enabled = false;
				txtAcctNumber.TabStop = false;
			}
		}

		private void CheckCash()
		{
			if (txtCD.Text == "N")
			{
				if (txtCash.Text == "N")
				{
					txtAcctNumber.Enabled = true;
					txtAcctNumber.TabStop = true;
				}
				else
				{
					txtAcctNumber.TextMatrix(0, 0, "");
					txtAcctNumber.Enabled = false;
					txtAcctNumber.TabStop = false;
				}
			}
			else
			{
				if (txtCash.Text == "N")
					txtCash.Text = "Y";
			}
		}

		private double CalculateAbatementInterest(ref double dblPrin, double dblMaxInterest = 0)
		{
			double CalculateAbatementInterest = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will find and return the current accounts abatement interest for that year
				int intYear = 0;
				// VBto upgrade warning: dblInt As double	OnWrite(double, short)
				double dblInt = 0;
				double dblPayTotal/*unused?*/;
				double dblTotalAbate;
				double dblRate = 0;
				string strTemp;
				string strType = "";
				clsDRWrapper rsBill = new clsDRWrapper();
				clsDRWrapper rsPay = new clsDRWrapper();
				clsDRWrapper rsRate = new clsDRWrapper();
				clsDRWrapper rsL = new clsDRWrapper();
				int lngAcct = 0;
				DateTime dtIntStartDate;
				int intPeriod;
				dblTotalAbate = dblPrin;
				strTemp = cmbYear.Items[cmbYear.SelectedIndex].ToString();
				if (Conversion.Val(strTemp) != 0)
				{
					intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(strTemp))));
					// make sure that the year is valid
					intPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbPeriod.Items[cmbPeriod.SelectedIndex].ToString())));
					// need to calculate the amount of interest for just the abatement amount
					// start at the last payment and work your way back until the bill
					if (modStatusPayments.Statics.boolRE)
					{
						// get the correct account
						lngAcct = StaticSettings.TaxCollectionValues.LastAccountRE;
						strType = "RE";
					}
					else
					{
						lngAcct = StaticSettings.TaxCollectionValues.LastAccountPP;
						strType = "PP";
					}
					strTemp = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = '" + strType + "'";
					rsBill.OpenRecordset(strTemp, modExtraModules.strCLDatabase);
					if (rsBill.EndOfFile() != true && rsBill.BeginningOfFile() != true)
					{
						// get the interest rate for this bill
						if (((rsBill.Get_Fields_Int32("LienRecordNumber"))) != 0)
						{
							rsL.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsBill.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
							if (!rsL.EndOfFile())
							{
								rsRate.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
								if (rsRate.EndOfFile())
								{
								}
								if (FCConvert.ToBoolean(rsRate.Get_Fields_Boolean("UseRKRateForAbate")))
								{
									dblRate = Conversion.Val(rsRate.Get_Fields_Double("AbatementInterestRate"));
									// kk09302014 trocl-1197    ' / 10000    'preset this fields
									rsRate.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsL.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
									if (rsRate.Get_Fields_Double("TaxRate") >= 0)
									{
										dblRate = Conversion.Val(rsRate.Get_Fields_Double("InterestRate"));
									}
								}
								else
								{
									if (Conversion.Val(rsRate.Get_Fields_Double("AbatementInterestRate")) > 0)
									{
										dblRate = Conversion.Val(rsRate.Get_Fields_Double("AbatementInterestRate"));
										// kk09302014 trocl-1197    ' / 10000
									}
									else
									{
										dblRate = 0;
									}
								}
							}
							else
							{
								dblRate = modStatusPayments.Statics.dblInterestRate;
							}
						}
						else
						{
							rsRate.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsBill.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
							if (rsRate.Get_Fields_Double("TaxRate") >= 0)
							{
								dblRate = Conversion.Val(rsRate.Get_Fields_Double("InterestRate"));
							}
							else
							{
								dblRate = modStatusPayments.Statics.dblInterestRate;
							}
						}
						// 
						// this will calculate all of the interest that needs to be abated from the beginning
						switch (intPeriod)
						{
							case 0:
								{
									// all periods
									if (FCConvert.ToString(rsRate.Get_Fields_Int16("NumberofPeriods")) == "1")
									{
										if (Conversion.Val(rsRate.Get_Fields_DateTime("InterestStartDate1")) < modStatusPayments.Statics.EffectiveDate.ToOADate())
										{
											dblInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin), DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("InterestStartDate1")), modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
										}
										else
										{
											dblInt = 0;
										}
									}
									else if (FCConvert.ToString(rsRate.Get_Fields_Int16("NumberofPeriods")) == "2")
									{
										if (Conversion.Val(rsRate.Get_Fields_DateTime("InterestStartDate1")) < modStatusPayments.Statics.EffectiveDate.ToOADate())
										{
											dblInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin / 2), DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("InterestStartDate1")), modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
											if (Conversion.Val(rsRate.Get_Fields_DateTime("InterestStartDate2")) < modStatusPayments.Statics.EffectiveDate.ToOADate())
											{
												dblInt += FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin / 2), DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("InterestStartDate2")), modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
											}
											else
											{
												dblInt = 0;
											}
										}
										else
										{
											dblInt = 0;
										}
									}
									else if (FCConvert.ToString(rsRate.Get_Fields_Int16("NumberofPeriods")) == "3")
									{
										if (Conversion.Val(rsRate.Get_Fields_DateTime("InterestStartDate1")) < modStatusPayments.Statics.EffectiveDate.ToOADate())
										{
											dblInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin / 3), DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("InterestStartDate1")), modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
											if (Conversion.Val(rsRate.Get_Fields_DateTime("InterestStartDate2")) < modStatusPayments.Statics.EffectiveDate.ToOADate())
											{
												dblInt += FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin / 3), DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("InterestStartDate2")), modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
												if (Conversion.Val(rsRate.Get_Fields_DateTime("InterestStartDate3")) < modStatusPayments.Statics.EffectiveDate.ToOADate())
												{
													dblInt += FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin / 3), DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("InterestStartDate3")), modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
												}
												else
												{
													dblInt = 0;
												}
											}
											else
											{
												dblInt = 0;
											}
										}
										else
										{
											dblInt = 0;
										}
									}
									else if (FCConvert.ToString(rsRate.Get_Fields_Int16("NumberofPeriods")) == "4")
									{
										if (Conversion.Val(rsRate.Get_Fields_DateTime("InterestStartDate1")) < modStatusPayments.Statics.EffectiveDate.ToOADate())
										{
											dblInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin / 4), DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("InterestStartDate1")), modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
											if (Conversion.Val(rsRate.Get_Fields_DateTime("InterestStartDate2")) < modStatusPayments.Statics.EffectiveDate.ToOADate())
											{
												dblInt += FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin / 4), DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("InterestStartDate2")), modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
												if (Conversion.Val(rsRate.Get_Fields_DateTime("InterestStartDate3")) < modStatusPayments.Statics.EffectiveDate.ToOADate())
												{
													dblInt += FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin / 4), DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("InterestStartDate3")), modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
													if (Conversion.Val(rsRate.Get_Fields_DateTime("InterestStartDate4")) < modStatusPayments.Statics.EffectiveDate.ToOADate())
													{
														dblInt += FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin / 4), DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("InterestStartDate3")), modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
													}
													else
													{
														dblInt = 0;
													}
												}
												else
												{
													dblInt = 0;
												}
											}
											else
											{
												dblInt = 0;
											}
										}
										else
										{
											dblInt = 0;
										}
									}
									break;
								}
							case 1:
								{
									dblInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin), DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("InterestStartDate1")), modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
									break;
								}
							case 2:
								{
									dblInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin), DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("InterestStartDate2")), modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
									break;
								}
							case 3:
								{
									dblInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin), DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("InterestStartDate3")), modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
									break;
								}
							case 4:
								{
									dblInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin), DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("InterestStartDate4")), modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
									break;
								}
						}
						//end switch
						CalculateAbatementInterest += dblInt;
						if (dblInt > dblMaxInterest && dblMaxInterest > 0)
						{
							CalculateAbatementInterest = dblMaxInterest;
						}
						else
						{
							// keep going
						}
						// If dblTotalAbate > 0 Then
						// If dblTotalAbate <= ((rsBill.Get_Fields("TaxDue1") + rsBill.Get_Fields("TaxDue2") + rsBill.Get_Fields("TaxDue3") + rsBill.Get_Fields("TaxDue4")) - rsBill.Get_Fields("PrincipalPaid")) Then
						// calculate the rest of the abatement
						// this is to abate all of the interest that has been accumulated from the beginning of the bill until now on what has not been paid
						// dblInt = CalculateInterest(dblTotalAbate, rsRate.Get_Fields("InterestStartDate1"), EffectiveDate, dblRate, gintBasis)
						// Else
						// dblInt = CalculateInterest((rsBill.Get_Fields("TaxDue1") + rsBill.Get_Fields("TaxDue2") + rsBill.Get_Fields("TaxDue3") + rsBill.Get_Fields("TaxDue4")) - rsBill.Get_Fields("PrincipalPaid"), rsRate.Get_Fields("InterestStartDate1"), EffectiveDate, dblRate, gintBasis)
						// End If
						// CalculateAbatementInterest = CalculateAbatementInterest + dblInt
						// End If
					}
					else
					{
						CalculateAbatementInterest = 0;
					}
				}
				else
				{
					// invalid year
					CalculateAbatementInterest = 0;
				}
				return CalculateAbatementInterest;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Calculate Abatement Interest");
			}
			return CalculateAbatementInterest;
		}
		// NEW CODE
		public double CalculateAbatementInterestByPayments_2(double dblTotalAbate)
		{
			return CalculateAbatementInterestByPayments(ref dblTotalAbate);
		}

		public double CalculateAbatementInterestByPayments(ref double dblTotalAbate)
		{
			double CalculateAbatementInterestByPayments = 0;
			// , Optional dblMaxInterest As Double
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will find and return the current accounts abatement interest for that year
				double dblTotalInt = 0;
				// Accumulator for total interest
				// VBto upgrade warning: dblPaymentInt As double	OnWriteFCConvert.ToDouble(
				double dblPaymentInt = 0;
				// Interest for one payment amount
				double dblPayTotal = 0;
				// Payment total that has been used towards the abatement total
				double dblPaymentAmount = 0;
				// Amountof the next payment
				double dblRate = 0;
				// Interest Rate
				double dblBillDue = 0;
				// This is the amount of the bill that is due
				string strTemp;
				// RE or PP abatement
				string strType = "";
				clsDRWrapper rsBill = new clsDRWrapper();
				clsDRWrapper rsPay = new clsDRWrapper();
				clsDRWrapper rsRate = new clsDRWrapper();
				clsDRWrapper rsL = new clsDRWrapper();
				int lngAcct = 0;
				DateTime dtIntStartDate;
				int intPeriod/*unused?*/;
				bool boolLien = false;
				int lngYear = 0;
				int lngRK = 0;
				strTemp = modExtraModules.FormatYear(cmbYear.Items[cmbYear.SelectedIndex].ToString());
				if (Conversion.Val(strTemp) != 0)
				{
					lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
					// intPeriod = Val(cmbPeriod.List(cmbPeriod.ListIndex))
					// need to calculate the amount of interest for just the abatement amount
					// start at the last payment and work your way back until the bill
					if (modStatusPayments.Statics.boolRE)
					{
						// get the correct account
						lngAcct = StaticSettings.TaxCollectionValues.LastAccountRE;
						strType = "RE";
					}
					else
					{
						lngAcct = StaticSettings.TaxCollectionValues.LastAccountPP;
						strType = "PP";
					}
					rsRate.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
					if (rsRate.EndOfFile())
					{
						return CalculateAbatementInterestByPayments;
					}
					strTemp = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " AND BillingYear = " + FCConvert.ToString(lngYear) + " AND BillingType = '" + strType + "'";
					rsBill.OpenRecordset(strTemp, modExtraModules.strCLDatabase);
					if (rsBill.EndOfFile() != true && rsBill.BeginningOfFile() != true)
					{
						// find the rate information
						if (FCConvert.ToBoolean(rsRate.Get_Fields_Boolean("UseRKRateForAbate")))
						{
							dblRate = modGlobal.Round(Conversion.Val(rsRate.Get_Fields_Double("AbatementInterestRate")), 6);
							// preset this fields         'kk09252014 trocl-1197   / 10000
							if (((rsBill.Get_Fields_Int32("LienRecordNumber"))) != 0)
							{
								rsL.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsBill.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
								if (!rsL.EndOfFile())
								{
									lngRK = FCConvert.ToInt32(rsL.Get_Fields_Int32("RateKey"));
								}
								else
								{
									lngRK = FCConvert.ToInt32(rsBill.Get_Fields_Int32("RateKey"));
								}
							}
							else
							{
								lngRK = FCConvert.ToInt32(rsBill.Get_Fields_Int32("RateKey"));
							}
							rsRate.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(lngRK), modExtraModules.strCLDatabase);
							if (rsRate.Get_Fields_Double("TaxRate") >= 0)
							{
								dblRate = modGlobal.Round(Conversion.Val(rsRate.Get_Fields_Double("InterestRate")), 6);
							}
						}
						else
						{
							if (Conversion.Val(rsRate.Get_Fields_Double("AbatementInterestRate")) > 0)
							{
								dblRate = modGlobal.Round(Conversion.Val(rsRate.Get_Fields_Double("AbatementInterestRate")), 6);
								// kk09252014 trocl-1197   / 10000
							}
							else
							{
								dblRate = 0;
							}
						}
						// get the interest rate for this bill
						if (((rsBill.Get_Fields_Int32("LienRecordNumber"))) != 0)
						{
							boolLien = true;
						}
						else
						{
							boolLien = false;
						}
						dblBillDue = Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsBill.Get_Fields_Decimal("TaxDue4")) - Conversion.Val(rsBill.Get_Fields_Decimal("PrincipalPaid")) - Conversion.Val(rsBill.Get_Fields_Decimal("InterestCharged")) - Conversion.Val(rsBill.Get_Fields_Decimal("InterestPaid")) - Conversion.Val(rsBill.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsBill.Get_Fields_Decimal("DemandFeesPaid"));
						if (dblBillDue > dblTotalAbate)
						{
							// no amount needs to be calculated
							CalculateAbatementInterestByPayments = 0;
							return CalculateAbatementInterestByPayments;
						}
						else
						{
							dblTotalAbate = modGlobal.Round(dblTotalAbate - dblBillDue, 2);
						}
						// check all of the payments and calculate the interest owed on each one
						if (!boolLien)
						{
							strTemp = "SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsBill.Get_Fields_Int32("ID")) + " AND BillCode = '" + Strings.Left(strType, 1) + "' AND Reference <> 'CHGINT' ORDER BY EffectiveInterestDate desc, ID desc";
						}
						else
						{
							strTemp = "SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(rsBill.Get_Fields_Int32("ID")) + " AND BillCode = 'L' AND Reference <> 'CHGINT' ORDER BY EffectiveInterestDate desc, ID desc";
						}
						rsPay.OpenRecordset(strTemp, modExtraModules.strCLDatabase);
						while (!(dblTotalAbate - dblPayTotal <= 0 || rsPay.EndOfFile()))
						{
							dtIntStartDate = (DateTime)rsPay.Get_Fields_DateTime("EffectiveInterestDate");
							// the payments effective interest date
							if (dtIntStartDate.ToOADate() > 0)
							{
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								dblPaymentAmount = Conversion.Val(rsPay.Get_Fields("Principal")) + Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("LienCost"));
								if (dblTotalAbate - dblPayTotal < dblPaymentAmount)
								{
									dblPaymentAmount = dblTotalAbate - dblPayTotal;
								}
								// calculate interest
								dblPaymentInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPaymentAmount), dtIntStartDate, modStatusPayments.Statics.EffectiveDate, dblRate, modGlobal.Statics.gintBasis));
								dblTotalInt += dblPaymentInt;
								dblPayTotal += dblPaymentAmount;
							}
							rsPay.MoveNext();
						}
						CalculateAbatementInterestByPayments = dblTotalInt;
					}
					else
					{
						CalculateAbatementInterestByPayments = 0;
					}
				}
				else
				{
					// invalid year
					CalculateAbatementInterestByPayments = 0;
				}
				return CalculateAbatementInterestByPayments;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Calculate Abatement Interest");
			}
			return CalculateAbatementInterestByPayments;
		}
		// VBto upgrade warning: 'Return' As DateTime	OnWrite(DateTime, short)
		private DateTime FindInterestStartDate(ref int lngAC, ref short intYR, string strType = "RE", short intPer = 1)
		{
			DateTime FindInterestStartDate = System.DateTime.Now;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will find and return the latest interest date for this account and year
				clsDRWrapper rsDate = new clsDRWrapper();
				string strSQL;
				DateTime dtIntStartDate;
				DateTime dtPerDate;
				strSQL = "WHERE Account = " + FCConvert.ToString(lngAC) + " AND BillingType = '" + strType + "' AND BillingYear = " + FCConvert.ToString(intYR);
				// get all of the payments
				rsDate.OpenRecordset("SELECT * FROM BillingMaster " + strSQL, modExtraModules.strCLDatabase);
				if (rsDate.EndOfFile() != true && rsDate.BeginningOfFile() != true)
				{
					// find the interest paid through date
					if (((rsDate.Get_Fields_DateTime("InterestAppliedThroughDate")).ToOADate()) != 0)
					{
						dtIntStartDate = (DateTime)rsDate.Get_Fields_DateTime("InterestAppliedThroughDate");
					}
					else
					{
						dtIntStartDate = modStatusPayments.Statics.EffectiveDate;
					}
					// find the interest start date for the period that is passed in
					rsDate.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsDate.Get_Fields_Int32("RateKey")), modExtraModules.strCLDatabase);
					if (rsDate.EndOfFile() != true && rsDate.BeginningOfFile() != true)
					{
						dtPerDate = rsDate.Get_Fields_DateTime("InterestStartDate" + FCConvert.ToString(intPer));
					}
					else
					{
						dtPerDate = modStatusPayments.Statics.EffectiveDate;
					}
					if (DateAndTime.DateDiff("D", dtIntStartDate, dtPerDate) < 0)
					{
						// find out which is the latest
						FindInterestStartDate = dtIntStartDate;
					}
					else
					{
						FindInterestStartDate = dtPerDate;
					}
				}
				else
				{
					FindInterestStartDate = DateTime.FromOADate(0);
				}
				return FindInterestStartDate;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Find Interest Start Date");
			}
			return FindInterestStartDate;
		}
		// VBto upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		private void AddRefundedAbatementLine(int intIndex)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will add a payment to the payment grid that will not be added to and cash or go to budgetary
				// this is the refund line and will have a code of 'R' and have the exact values of the abatement
				int intRefundIndex;
				int lngRow;
				// find the next available index for the payment array
				for (intRefundIndex = 1; intRefundIndex <= modStatusPayments.MAX_PAYMENTS; intRefundIndex++)
				{
					if (modStatusPayments.Statics.PaymentArray[intRefundIndex].Year == 0)
						break;
				}
				if (intRefundIndex > modStatusPayments.MAX_PAYMENTS)
				{
					FCMessageBox.Show("Exceeded the maximum amount of payments. (" + FCConvert.ToString(modStatusPayments.MAX_PAYMENTS) + ")", MsgBoxStyle.Critical, "Refunded Abatement Error");
					return;
					// exit w/o adding refund
				}
				// start by setting all of the defaults
				modStatusPayments.Statics.PaymentArray[intRefundIndex] = modStatusPayments.Statics.PaymentArray[intIndex];
				// set the Budgetary account number to an account that everyone will know that does not get passed through the accounting mod
				if (modExtraModules.IsThisCR())
				{
					modStatusPayments.Statics.PaymentArray[intRefundIndex].BudgetaryAccountNumber = GetRefundedAbabtementAccount_8(modStatusPayments.Statics.PaymentArray[intRefundIndex].BillCode == "L", Strings.Format((modStatusPayments.Statics.PaymentArray[intRefundIndex].Year / 10.0) - 2000, "00"));
				}
				else
				{
					modStatusPayments.Statics.PaymentArray[intRefundIndex].BudgetaryAccountNumber = "M NO ENTRY";
				}
				// force codes
				modStatusPayments.Statics.PaymentArray[intRefundIndex].Code = "R";
				modStatusPayments.Statics.PaymentArray[intRefundIndex].Period = "1";
				modStatusPayments.Statics.PaymentArray[intRefundIndex].CashDrawer = "N";
				modStatusPayments.Statics.PaymentArray[intRefundIndex].GeneralLedger = "N";
				// reverse all of the values
				modStatusPayments.Statics.PaymentArray[intRefundIndex].LienCost = modStatusPayments.Statics.PaymentArray[intIndex].LienCost * -1;
				modStatusPayments.Statics.PaymentArray[intRefundIndex].CurrentInterest = modStatusPayments.Statics.PaymentArray[intIndex].CurrentInterest * -1;
				modStatusPayments.Statics.PaymentArray[intRefundIndex].PreLienInterest = modStatusPayments.Statics.PaymentArray[intIndex].PreLienInterest * -1;
				modStatusPayments.Statics.PaymentArray[intRefundIndex].Principal = modStatusPayments.Statics.PaymentArray[intIndex].Principal * -1;
				// add the payment to the payment list
				lngRow = vsPayments.Rows;
				vsPayments.AddItem("");
				// Year
				vsPayments.TextMatrix(lngRow, 0, modExtraModules.FormatYear(modStatusPayments.Statics.PaymentArray[intRefundIndex].Year.ToString()));
				// Date
				vsPayments.TextMatrix(lngRow, 1, Strings.Format(modStatusPayments.Statics.PaymentArray[intRefundIndex].RecordedTransactionDate, "MM/dd/yyyy"));
				// Ref
				vsPayments.TextMatrix(lngRow, 2, modStatusPayments.Statics.PaymentArray[intRefundIndex].Reference);
				// Per
				vsPayments.TextMatrix(lngRow, 3, modStatusPayments.Statics.PaymentArray[intRefundIndex].Period);
				// Code
				vsPayments.TextMatrix(lngRow, 4, modStatusPayments.Statics.PaymentArray[intRefundIndex].Code);
				// Cash
				vsPayments.TextMatrix(lngRow, 5, modStatusPayments.Statics.PaymentArray[intRefundIndex].CashDrawer + "   " + modStatusPayments.Statics.PaymentArray[intRefundIndex].GeneralLedger);
				// Prin
				vsPayments.TextMatrix(lngRow, 6, Strings.Format(modStatusPayments.Statics.PaymentArray[intRefundIndex].Principal, "#,##0.00"));
				// Int
				vsPayments.TextMatrix(lngRow, 7, Strings.Format(modStatusPayments.Statics.PaymentArray[intRefundIndex].CurrentInterest + modStatusPayments.Statics.PaymentArray[intRefundIndex].PreLienInterest, "#,##0.00"));
				// Costs
				vsPayments.TextMatrix(lngRow, 8, Strings.Format(modStatusPayments.Statics.PaymentArray[intRefundIndex].LienCost, "#,##0.00"));
				// Array Index
				vsPayments.TextMatrix(lngRow, 9, FCConvert.ToString(intRefundIndex));
				// ID Number from autonumberfield in DB
				vsPayments.TextMatrix(lngRow, 10, FCConvert.ToString(0));
				// CHGINT Number
				vsPayments.TextMatrix(lngRow, 11, FCConvert.ToString(0));
				// Total
				vsPayments.TextMatrix(lngRow, 12, Strings.Format(modStatusPayments.Statics.PaymentArray[intRefundIndex].CurrentInterest + modStatusPayments.Statics.PaymentArray[intRefundIndex].PreLienInterest + modStatusPayments.Statics.PaymentArray[intRefundIndex].Principal + modStatusPayments.Statics.PaymentArray[intRefundIndex].LienCost, "#,##0.00"));
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Refunded Abatement Error");
			}
		}

		//private void ShowDiscountFrame(bool boolCheckInt = false)
		//{
		//	// this sub will show the Discount Frame with calculated figures, users can change the principal payment
		//	double dblTotal = 0;
		//	// the total payment needed
		//	double dblDisc = 0;
		//	// total discount
		//	double dblDiscPayment/*unused?*/;
		//	// how much is due after discount is taken
		//	clsDRWrapper rsDisc = new clsDRWrapper();
		//	int intYear = 0;
		//	int LRN = 0;
		//	double dblOrigPrin = 0;
		//	double dblPrinPaid = 0;
		//	// VBto upgrade warning: dblTemp As double	OnWriteFCConvert.ToDouble(
		//	double[] dblTemp = new double[4 + 1];
		//	RETRYDISCOUNT:
		//	;
		//	if (Strings.Trim(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 1)) != "")
		//	{
		//		if (Conversion.Val(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 1), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 1).Length - 2)) != 0)
		//		{
		//			dblTemp[1] = FCConvert.ToDouble(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 1), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 1).Length - 2));
		//		}
		//	}
		//	if (Strings.Trim(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 2)) != "")
		//	{
		//		if (Conversion.Val(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 2), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 2).Length - 2)) != 0)
		//		{
		//			dblTemp[2] = FCConvert.ToDouble(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 2), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 2).Length - 2));
		//		}
		//	}
		//	if (Strings.Trim(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 3)) != "")
		//	{
		//		if (Conversion.Val(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 3), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 3).Length - 2)) != 0)
		//		{
		//			dblTemp[3] = FCConvert.ToDouble(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 3), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 3).Length - 2));
		//		}
		//	}
		//	if (Strings.Trim(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 4)) != "")
		//	{
		//		if (Conversion.Val(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 4), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 4).Length - 2)) != 0)
		//		{
		//			dblTemp[4] = FCConvert.ToDouble(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 4), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 4).Length - 2));
		//		}
		//	}
		//	// do not allow a discount if there is a negative value owed for that year and not auto in year
		//	// find which year is being discounted
		//	intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(Strings.Left(frmRECLStatus.InstancePtr.cmbYear.Items[frmRECLStatus.InstancePtr.cmbYear.SelectedIndex].ToString(), 6)))));
		//	// find the total due for this year
		//	if (modStatusPayments.Statics.boolRE)
		//	{
		//		rsDisc.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'RE'", modExtraModules.strCLDatabase);
		//	}
		//	else
		//	{
		//		rsDisc.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'PP'", modExtraModules.strCLDatabase);
		//	}
		//	if (rsDisc.RecordCount() != 0)
		//	{
		//		if (((rsDisc.Get_Fields_Int32("LienRecordNumber"))) == 0)
		//		{
		//			// regular billing
		//			if (rsDisc.Get_Fields_Decimal("InterestCharged") < 0 && boolCheckInt)
		//			{
		//				if (FCMessageBox.Show("There is interest on this account.  Are you sure that you would like to continue?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Account Accruing Interest") != DialogResult.Yes)
		//				{
		//					return;
		//				}
		//			}
		//			dblTotal = (Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue1")) + Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue2")) + Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue3")) + Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue4"))) - Conversion.Val(rsDisc.Get_Fields_Decimal("PrincipalPaid"));
		//			dblOrigPrin = Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue1")) + Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue2")) + Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue3")) + Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue4"));
		//			dblPrinPaid = Conversion.Val(rsDisc.Get_Fields_Decimal("PrincipalPaid"));
		//		}
		//		else
		//		{
		//			// lien
		//			LRN = FCConvert.ToInt32(rsDisc.Get_Fields_Int32("LienRecordNumber"));
		//			if (rsDisc.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(LRN), modExtraModules.strCLDatabase))
		//			{
		//				if (rsDisc.RecordCount() != 0)
		//				{
		//					if (rsDisc.Get_Fields_Decimal("InterestCharged") < 0 && boolCheckInt)
		//					{
		//						if (FCMessageBox.Show("There is interest on this account.  Are you sure that you would like to continue?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Account Accruing Interest") != DialogResult.Yes)
		//						{
		//							return;
		//						}
		//					}
		//					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//					dblTotal = Conversion.Val(rsDisc.Get_Fields("Principal")) - Conversion.Val(rsDisc.Get_Fields_Decimal("PrincipalPaid"));
		//					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//					dblOrigPrin = Conversion.Val(rsDisc.Get_Fields("Principal"));
		//					dblPrinPaid = Conversion.Val(rsDisc.Get_Fields_Decimal("PrincipalPaid"));
		//				}
		//				else
		//				{
		//					// lien record not found
		//					FCMessageBox.Show("Lien record number, " + FCConvert.ToString(LRN) + ", not found.", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "ERROR");
		//				}
		//			}
		//			else
		//			{
		//				FCMessageBox.Show("Error opening table LienRec.", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "ERROR");
		//			}
		//		}
		//		// calculate discount
		//		// MAL@20080402: Add support for pre-payment discounts (tax has not been billed yet)
		//		// Tracker Reference: 12996
		//		if (dblTotal == 0)
		//		{
		//			dblDisc = 0;
		//		}
		//		else
		//		{
		//			dblDisc = modCLDiscount.CalculateDiscount(dblTotal);
		//		}
		//		// MAL@20080422: Don't show credit balances (set to zero)
		//		// Tracker Reference: 12996
		//		if (dblDisc <= 0)
		//		{
		//			dblDisc = 0;
		//		}
		//		if (dblTotal <= 0)
		//		{
		//			dblTotal = 0;
		//		}
		//		// show discount and user contribution
		//		frmRECLStatus.InstancePtr.txtDiscDisc.Text = Strings.Format(dblDisc, "#,##0.00");
		//		frmRECLStatus.InstancePtr.txtDiscPrin.Text = Strings.Format(dblTotal - dblDisc, "#,##0.00");
		//		frmRECLStatus.InstancePtr.lblDiscTotal.Text = Strings.Format(dblTotal, "#,##0.00");
		//		frmRECLStatus.InstancePtr.lblDiscHiddenTotal.Text = FCConvert.ToString(dblTotal);
		//		frmRECLStatus.InstancePtr.lblOriginalTax.Text = Strings.Format(dblOrigPrin, "#,##0.00");
		//		frmRECLStatus.InstancePtr.lblOriginalTax.Tag = Strings.Format(dblPrinPaid, "#,##0.00");
		//		// set the instructions
		//		frmRECLStatus.InstancePtr.lblDiscountInstructions.Text = "Is this principal payment and discount correct?";
		//		// lock textboxes
		//		frmRECLStatus.InstancePtr.txtDiscPrin.ReadOnly = true;
		//		frmRECLStatus.InstancePtr.txtDiscDisc.ReadOnly = true;
		//		// align the frame
		//		frmRECLStatus.InstancePtr.fraDiscount.Left = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Width - frmRECLStatus.InstancePtr.fraDiscount.Width) / 2.0);
		//		frmRECLStatus.InstancePtr.fraDiscount.Top = FCConvert.ToInt32((frmRECLStatus.InstancePtr.ClientArea.Height - frmRECLStatus.InstancePtr.fraDiscount.Height) / 2.0);
		//		// show the frame
		//		frmRECLStatus.InstancePtr.fraDiscount.Visible = true;
		//		frmRECLStatus.InstancePtr.GRID.Visible = false;
		//		frmRECLStatus.InstancePtr.fraPayment.Visible = false;
		//		frmRECLStatus.InstancePtr.mnuPayment.Visible = false;
		//		//frmRECLStatus.InstancePtr.mnuPaymentPreview.Visible = false;
		//	}
		//	else
		//	{
		//		// no current record
		//		DialogResult messageBoxResult = FCMessageBox.Show("There is no bill for year " + modExtraModules.FormatYear(FCConvert.ToString(intYear)) + ".  Would you like to create it?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Discount");
		//		if (messageBoxResult == DialogResult.Yes)
		//		{
		//			// create a bill for this year
		//			if (modStatusPayments.Statics.boolRE)
		//			{
		//				modStatusPayments.CreateBlankCLBill_20(intYear, StaticSettings.TaxCollectionValues.LastAccountRE, "RE");
		//			}
		//			else
		//			{
		//				modStatusPayments.CreateBlankCLBill_20(intYear, StaticSettings.TaxCollectionValues.LastAccountPP, "PP");
		//			}
		//			// goto the discount screen
		//			goto RETRYDISCOUNT;
		//		}
		//		else if ((messageBoxResult == DialogResult.No) || (messageBoxResult == DialogResult.Cancel))
		//		{
		//			frmRECLStatus.InstancePtr.boolChangingCode = true;
		//			// MAL@20071115: Changed to use new function to get the correct ListIndex
		//			// frmRECLStatus.cmbCode.ListIndex = 3     'set the code box back to a payment
		//			frmRECLStatus.InstancePtr.cmbCode.SelectedIndex = modStatusPayments.GetPaymentCodeListIndex("P");
		//			frmRECLStatus.InstancePtr.boolChangingCode = false;
		//		}
		//	}
		//}

		//private void AddDiscountToList()
		//{
		//	double dblPrinNeed;
		//	double dblIntNeed = 0;
		//	double dblCostNeed = 0;
		//	int intYear;
		//	int intCT;
		//	int intCT2;
		//	bool boolIsValidDisc;
		//	DialogResult intTemp = 0;
		//	int lngAcct = 0;
		//	// MAL@20071005
		//	string PayCode = "";
		//	string strTemp = "";
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	clsDRWrapper rsLienRec = new clsDRWrapper();
		//	intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(Strings.Left(frmRECLStatus.InstancePtr.cmbYear.Items[frmRECLStatus.InstancePtr.cmbYear.SelectedIndex].ToString(), 6)))));
		//	if (modStatusPayments.Statics.boolRE)
		//	{
		//		strTemp = "SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'RE'";
		//		lngAcct = StaticSettings.TaxCollectionValues.LastAccountRE;
		//	}
		//	else
		//	{
		//		strTemp = "SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'PP'";
		//		lngAcct = StaticSettings.TaxCollectionValues.LastAccountPP;
		//	}
		//	rsTemp.OpenRecordset(strTemp, modExtraModules.strCLDatabase);
		//	if (rsTemp.RecordCount() != 0)
		//	{
		//	}
		//	else
		//	{
		//		FCMessageBox.Show("There is no bill created for " + modExtraModules.FormatYear(intYear.ToString()) + ".", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "ERROR");
		//		return;
		//	}
		//	for (intCT = 1; intCT <= modStatusPayments.MAX_PAYMENTS; intCT++)
		//	{
		//		// this finds a place in the array
		//		if (modStatusPayments.Statics.PaymentArray[intCT].Year == 0)
		//		{
		//			// to store the new payment record
		//			break;
		//		}
		//	}
		//	//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
		//	modStatusPayments.Statics.PaymentArray[intCT] = new modStatusPayments.Payment(0);
		//	// get the next year and its total due values
		//	if (((rsTemp.Get_Fields_Int32("LienRecordNumber"))) == 0)
		//	{
		//		dblPrinNeed = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4")) - Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid"));
		//		dblIntNeed = (Conversion.Val(rsTemp.Get_Fields_Decimal("InterestCharged")) + Conversion.Val(rsTemp.Get_Fields_Decimal("InterestPaid"))) * -1;
		//		dblCostNeed = Conversion.Val(rsTemp.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsTemp.Get_Fields_Decimal("DemandFeesPaid"));
		//		if (modStatusPayments.Statics.boolRE)
		//		{
		//			PayCode = "R";
		//		}
		//		else
		//		{
		//			PayCode = "P";
		//		}
		//	}
		//	else
		//	{
		//		PayCode = "L";
		//		rsLienRec.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsTemp.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
		//		if (rsLienRec.RecordCount() != 0)
		//		{
		//			// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//			dblPrinNeed = Conversion.Val(rsLienRec.Get_Fields("Principal")) - Conversion.Val(rsLienRec.Get_Fields_Decimal("PrincipalPaid"));
		//			// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//			// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//			dblIntNeed = Conversion.Val(rsLienRec.Get_Fields("Interest")) - Conversion.Val(rsLienRec.Get_Fields_Decimal("InterestCharged")) - Conversion.Val(rsLienRec.Get_Fields_Decimal("InterestPaid")) - Conversion.Val(rsLienRec.Get_Fields("PLIPaid")) + modStatusPayments.Statics.dblCurrentInt[(intYear) - modExtraModules.DEFAULTSUBTRACTIONVALUE];
		//			// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//			// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//			dblCostNeed = Conversion.Val(rsLienRec.Get_Fields("Costs")) - Conversion.Val(rsLienRec.Get_Fields("MaturityFee")) - Conversion.Val(rsLienRec.Get_Fields_Decimal("CostsPaid"));
		//		}
		//	}
		//	boolIsValidDisc = true;
		//	// kk02042015 trocl-1225  Change for readability    'intTemp = 6
		//	if (dblIntNeed > 0 || dblCostNeed > 0)
		//	{
		//		intTemp = FCMessageBox.Show("There is outstanding Interest and/or Costs, are you sure that this is a valid discount?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Interest and Cost Warning");
		//		if (intTemp != DialogResult.Yes)
		//		{
		//			// kk02042015 trocl-1225  Add for readability
		//			boolIsValidDisc = false;
		//		}
		//	}
		//	// If CCur(txtDiscPrin.Text) = 0 Then
		//	if (FCConvert.ToDouble(txtDiscPrin.Text) == 0 && !blnDiscountCredit)
		//	{
		//		if (FCMessageBox.Show("There is no principal associated with this discount.  Are you sure that you would like to proceed?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "No Principal") == DialogResult.Yes)
		//		{
		//			// keep going
		//		}
		//		else
		//		{
		//			FCMessageBox.Show("No discount amount added.", MsgBoxStyle.Exclamation, "No Amount");
		//			modGlobalFunctions.AddCYAEntry_8("CL", "Add Discount with no Principal");
		//			return;
		//		}
		//	}
		//	// kk02042015 trocl-1225  Change for readability
		//	// Select Case intTemp
		//	// Case 6      'Yes
		//	if (boolIsValidDisc)
		//	{
		//		if (intCT < modStatusPayments.MAX_PAYMENTS)
		//		{
		//			if (FCConvert.ToDouble(txtDiscPrin.Text) != 0)
		//			{
		//				// this adds it to the array of payments
		//				if (modStatusPayments.Statics.boolRE)
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].Account = StaticSettings.TaxCollectionValues.LastAccountRE;
		//					modGlobalFunctions.AddCYAEntry_8("CL", "Override of discount amount. RE Account: " + FCConvert.ToString(modStatusPayments.Statics.PaymentArray[intCT].Account));
		//				}
		//				else
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].Account = StaticSettings.TaxCollectionValues.LastAccountPP;
		//					modGlobalFunctions.AddCYAEntry_8("CL", "Override of discount amount. PP Account: " + FCConvert.ToString(modStatusPayments.Statics.PaymentArray[intCT].Account));
		//				}
		//				modStatusPayments.Statics.PaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//				modStatusPayments.Statics.PaymentArray[intCT].BillCode = PayCode;
		//				if (PayCode == "L")
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
		//				}
		//				else
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
		//				}
		//				modStatusPayments.Statics.PaymentArray[intCT].CashDrawer = txtCD.Text;
		//				// allow the user to set it to yesterday
		//				if (((rsTemp.Get_Fields_Decimal("TaxDue1"))) != 0)
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].Code = "P";
		//				}
		//				else
		//				{
		//					// This will show as a prepayment if the billing year does not have any taxes applied to it
		//					modStatusPayments.Statics.PaymentArray[intCT].Code = "Y";
		//				}
		//				modStatusPayments.Statics.PaymentArray[intCT].ResCode = lngCLResCode;
		//				modStatusPayments.Statics.PaymentArray[intCT].Comments = frmRECLStatus.InstancePtr.txtComments.Text;
		//				modStatusPayments.Statics.PaymentArray[intCT].EffectiveInterestDate = modStatusPayments.Statics.EffectiveDate;
		//				modStatusPayments.Statics.PaymentArray[intCT].GeneralLedger = "Y";
		//				// cannot write this off
		//				modStatusPayments.Statics.PaymentArray[intCT].Period = Strings.Left(cmbPeriod.Items[cmbPeriod.SelectedIndex].ToString(), 1);
		//				modStatusPayments.Statics.PaymentArray[intCT].PreLienInterest = 0;
		//				if (blnDiscountCredit)
		//				{
		//					// MAL@20071005: credit exists
		//					modStatusPayments.Statics.PaymentArray[intCT].Principal = 0;
		//				}
		//				else
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].Principal = FCConvert.ToDecimal(txtDiscPrin.Text);
		//				}
		//				modStatusPayments.Statics.PaymentArray[intCT].CurrentInterest = 0;
		//				modStatusPayments.Statics.PaymentArray[intCT].LienCost = 0;
		//				modStatusPayments.Statics.PaymentArray[intCT].ReceiptNumber = "P";
		//				if (frmRECLStatus.InstancePtr.txtTransactionDate.Text != "")
		//				{
		//					if (Information.IsDate(frmRECLStatus.InstancePtr.txtTransactionDate.Text))
		//					{
		//						modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmRECLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//					}
		//					else
		//					{
		//						frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//						modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//					}
		//				}
		//				else
		//				{
		//					frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//					modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//				}
		//				modStatusPayments.Statics.PaymentArray[intCT].Reference = Strings.Trim(txtReference.Text + " ");
		//				modStatusPayments.Statics.PaymentArray[intCT].ReversalID = -1;
		//				// .Teller =
		//				// .TransNumber =
		//				// .PaidBy =
		//				// .CHGINTNumber =
		//				modStatusPayments.Statics.PaymentArray[intCT].Year = intYear;
		//				FillPaymentGrid(vsPayments.Rows, intCT, intYear);
		//			}
		//		}
		//		// now add the discount line
		//		for (intCT = 1; intCT <= modStatusPayments.MAX_PAYMENTS; intCT++)
		//		{
		//			// this finds a place in the array
		//			if (modStatusPayments.Statics.PaymentArray[intCT].Year == 0)
		//			{
		//				// to store the new payment record
		//				break;
		//			}
		//		}
		//		if (intCT < modStatusPayments.MAX_PAYMENTS)
		//		{
		//			// this adds it to the array of payments
		//			if (modStatusPayments.Statics.boolRE)
		//			{
		//				modStatusPayments.Statics.PaymentArray[intCT].Account = StaticSettings.TaxCollectionValues.LastAccountRE;
		//			}
		//			else
		//			{
		//				modStatusPayments.Statics.PaymentArray[intCT].Account = StaticSettings.TaxCollectionValues.LastAccountPP;
		//			}
		//			modStatusPayments.Statics.PaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//			modStatusPayments.Statics.PaymentArray[intCT].BillCode = PayCode;
		//			if (PayCode == "L")
		//			{
		//				modStatusPayments.Statics.PaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
		//			}
		//			else
		//			{
		//				modStatusPayments.Statics.PaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
		//			}
		//			modStatusPayments.Statics.PaymentArray[intCT].CashDrawer = "N";
		//			// frmRECLStatus.txtCD.Text
		//			if (modStatusPayments.Statics.PaymentArray[intCT].CashDrawer == "N")
		//			{
		//				modStatusPayments.Statics.PaymentArray[intCT].BudgetaryAccountNumber = frmRECLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0);
		//			}
		//			modStatusPayments.Statics.PaymentArray[intCT].Code = "D";
		//			modStatusPayments.Statics.PaymentArray[intCT].Comments = frmRECLStatus.InstancePtr.txtComments.Text;
		//			modStatusPayments.Statics.PaymentArray[intCT].EffectiveInterestDate = modStatusPayments.Statics.EffectiveDate;
		//			modStatusPayments.Statics.PaymentArray[intCT].GeneralLedger = "N";
		//			// frmRECLStatus.txtCash.Text
		//			modStatusPayments.Statics.PaymentArray[intCT].Period = Strings.Left(frmRECLStatus.InstancePtr.cmbPeriod.Items[frmRECLStatus.InstancePtr.cmbPeriod.SelectedIndex].ToString(), 1);
		//			modStatusPayments.Statics.PaymentArray[intCT].PreLienInterest = 0;
		//			if (blnDiscountCredit)
		//			{
		//				// MAL@20071005: credit exists
		//				// .Principal = CCur(frmRECLStatus.txtDiscPrin.Text)
		//				modStatusPayments.Statics.PaymentArray[intCT].Principal = FCConvert.ToDecimal(frmRECLStatus.InstancePtr.lblDiscTotal.Text);
		//			}
		//			else
		//			{
		//				modStatusPayments.Statics.PaymentArray[intCT].Principal = FCConvert.ToDecimal(frmRECLStatus.InstancePtr.txtDiscDisc.Text);
		//			}
		//			modStatusPayments.Statics.PaymentArray[intCT].CurrentInterest = 0;
		//			modStatusPayments.Statics.PaymentArray[intCT].LienCost = 0;
		//			modStatusPayments.Statics.PaymentArray[intCT].ReceiptNumber = "P";
		//			if (frmRECLStatus.InstancePtr.txtTransactionDate.Text != "")
		//			{
		//				if (Information.IsDate(frmRECLStatus.InstancePtr.txtTransactionDate.Text))
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmRECLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//				}
		//				else
		//				{
		//					frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//					modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//				}
		//			}
		//			else
		//			{
		//				frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//				modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//			}
		//			modStatusPayments.Statics.PaymentArray[intCT].ResCode = lngCLResCode;
		//			modStatusPayments.Statics.PaymentArray[intCT].Reference = "DISCOUNT";
		//			// Trim$(frmRECLStatus.txtReference & " ")
		//			modStatusPayments.Statics.PaymentArray[intCT].ReversalID = -1;
		//			// .Teller =
		//			// .TransNumber =
		//			// .PaidBy =
		//			// .CHGINTNumber =
		//			modStatusPayments.Statics.PaymentArray[intCT].Year = intYear;
		//			FillPaymentGrid(frmRECLStatus.InstancePtr.vsPayments.Rows, intCT, intYear);
		//		}
		//		// Add Discount Credit, If Applicable - Create the same payment records as pressing F11 to redist the credit would
		//		if (blnDiscountCredit)
		//		{
		//			this.lblDiscHiddenTotal.Text = FCConvert.ToString((FCConvert.ToDouble(frmRECLStatus.InstancePtr.txtDiscDisc.Text) - modCLCalculations.CalculateAccountTotal(lngAcct, modStatusPayments.Statics.boolRE, false, modStatusPayments.Statics.EffectiveDate)));
		//			if (CheckForLastYearOverpayment_Discount())
		//			{
		//				for (intCT = 1; intCT <= modStatusPayments.MAX_PAYMENTS; intCT++)
		//				{
		//					// this finds a place in the array
		//					if (modStatusPayments.Statics.PaymentArray[intCT].Year == 0)
		//					{
		//						// to store the new payment record
		//						break;
		//					}
		//				}
		//				for (intCT2 = intCT + 1; intCT2 <= modStatusPayments.MAX_PAYMENTS; intCT2++)
		//				{
		//					// this finds a second place in the array
		//					if (modStatusPayments.Statics.PaymentArray[intCT2].Year == 0)
		//					{
		//						// to store the new payment record
		//						break;
		//					}
		//				}
		//				if (intCT < modStatusPayments.MAX_PAYMENTS && intCT2 < modStatusPayments.MAX_PAYMENTS)
		//				{
		//					//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
		//					modStatusPayments.Statics.PaymentArray[intCT] = new modStatusPayments.Payment(0);
		//					modStatusPayments.Statics.PaymentArray[intCT2] = new modStatusPayments.Payment(0);
		//					// kk02042015 trocl-1225  Add 2 payment records - 1 to offset the credit, 1 prepay
		//					// kk02042015 trocl-1225  Add a payment record to the current bill to offset the moved credit
		//					// this adds it to the array of payments
		//					if (modStatusPayments.Statics.boolRE)
		//					{
		//						modStatusPayments.Statics.PaymentArray[intCT].Account = StaticSettings.TaxCollectionValues.LastAccountRE;
		//					}
		//					else
		//					{
		//						modStatusPayments.Statics.PaymentArray[intCT].Account = StaticSettings.TaxCollectionValues.LastAccountPP;
		//					}
		//					modStatusPayments.Statics.PaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//					modStatusPayments.Statics.PaymentArray[intCT].BillCode = PayCode;
		//					if (PayCode == "L")
		//					{
		//						modStatusPayments.Statics.PaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
		//					}
		//					else
		//					{
		//						modStatusPayments.Statics.PaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
		//					}
		//					modStatusPayments.Statics.PaymentArray[intCT].CashDrawer = txtCD.Text;
		//					modStatusPayments.Statics.PaymentArray[intCT].Code = "P";
		//					modStatusPayments.Statics.PaymentArray[intCT].Comments = frmRECLStatus.InstancePtr.txtComments.Text;
		//					modStatusPayments.Statics.PaymentArray[intCT].EffectiveInterestDate = modStatusPayments.Statics.EffectiveDate;
		//					modStatusPayments.Statics.PaymentArray[intCT].GeneralLedger = "Y";
		//					modStatusPayments.Statics.PaymentArray[intCT].Period = Strings.Left(frmRECLStatus.InstancePtr.cmbPeriod.Items[frmRECLStatus.InstancePtr.cmbPeriod.SelectedIndex].ToString(), 1);
		//					modStatusPayments.Statics.PaymentArray[intCT].PreLienInterest = 0;
		//					modStatusPayments.Statics.PaymentArray[intCT].Principal = FCConvert.ToDecimal(this.lblDiscHiddenTotal.Text) * -1;
		//					modStatusPayments.Statics.PaymentArray[intCT].CurrentInterest = 0;
		//					modStatusPayments.Statics.PaymentArray[intCT].LienCost = 0;
		//					modStatusPayments.Statics.PaymentArray[intCT].ReceiptNumber = "P";
		//					if (frmRECLStatus.InstancePtr.txtTransactionDate.Text != "")
		//					{
		//						if (Information.IsDate(frmRECLStatus.InstancePtr.txtTransactionDate.Text))
		//						{
		//							modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmRECLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//						}
		//						else
		//						{
		//							frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//							modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//						}
		//					}
		//					else
		//					{
		//						frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//						modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//					}
		//					modStatusPayments.Statics.PaymentArray[intCT].ResCode = lngCLResCode;
		//					modStatusPayments.Statics.PaymentArray[intCT].Reference = "AUTO";
		//					modStatusPayments.Statics.PaymentArray[intCT].ReversalID = -1;
		//					modStatusPayments.Statics.PaymentArray[intCT].Year = intYear;
		//					FillPaymentGrid(frmRECLStatus.InstancePtr.vsPayments.Rows, intCT, intYear);
		//					// this adds it to the array of payments
		//					if (modStatusPayments.Statics.boolRE)
		//					{
		//						modStatusPayments.Statics.PaymentArray[intCT2].Account = StaticSettings.TaxCollectionValues.LastAccountRE;
		//						modStatusPayments.Statics.PaymentArray[intCT2].BillCode = "R";
		//						// kk02042015 trocl-1225  We're creating a new bill, it shouldn't ever be a lien
		//					}
		//					else
		//					{
		//						modStatusPayments.Statics.PaymentArray[intCT2].Account = StaticSettings.TaxCollectionValues.LastAccountPP;
		//						modStatusPayments.Statics.PaymentArray[intCT2].BillCode = "P";
		//					}
		//					modStatusPayments.Statics.PaymentArray[intCT2].ActualSystemDate = DateTime.Today;
		//					// .BillCode = PayCode              'kk02042015 trocl-1225  We're creating a new bill, it shouldn't ever be a lien
		//					// If PayCode = "L" Then
		//					// .BillKey = rsLienRec.Get_Fields("LienRecordNumber")
		//					// Else
		//					modStatusPayments.Statics.PaymentArray[intCT2].BillKey = lngNewBillKey;
		//					// End If
		//					// kk02042015 trocl-1225  Make same as pressing F11 to redist credit
		//					modStatusPayments.Statics.PaymentArray[intCT2].CashDrawer = txtCD.Text;
		//					// .CashDrawer = "N" ' frmRECLStatus.txtCD.Text
		//					// If .CashDrawer = "N" Then
		//					// .BudgetaryAccountNumber = frmRECLStatus.txtAcctNumber.TextMatrix(0, 0)
		//					// End If
		//					modStatusPayments.Statics.PaymentArray[intCT2].Code = "Y";
		//					modStatusPayments.Statics.PaymentArray[intCT2].Comments = frmRECLStatus.InstancePtr.txtComments.Text;
		//					modStatusPayments.Statics.PaymentArray[intCT2].EffectiveInterestDate = modStatusPayments.Statics.EffectiveDate;
		//					modStatusPayments.Statics.PaymentArray[intCT2].GeneralLedger = "Y";
		//					// kk02042015 trocl-1225  Make same as pressing F11 to redist credit    '.GeneralLedger = "N" ' frmRECLStatus.txtCash.Text
		//					modStatusPayments.Statics.PaymentArray[intCT2].Period = Strings.Left(frmRECLStatus.InstancePtr.cmbPeriod.Items[frmRECLStatus.InstancePtr.cmbPeriod.SelectedIndex].ToString(), 1);
		//					modStatusPayments.Statics.PaymentArray[intCT2].PreLienInterest = 0;
		//					// .Principal = CCur(frmRECLStatus.txtDiscDisc.Text)
		//					modStatusPayments.Statics.PaymentArray[intCT2].Principal = FCConvert.ToDecimal(this.lblDiscHiddenTotal.Text);
		//					modStatusPayments.Statics.PaymentArray[intCT2].CurrentInterest = 0;
		//					modStatusPayments.Statics.PaymentArray[intCT2].LienCost = 0;
		//					modStatusPayments.Statics.PaymentArray[intCT2].ReceiptNumber = "P";
		//					if (frmRECLStatus.InstancePtr.txtTransactionDate.Text != "")
		//					{
		//						if (Information.IsDate(frmRECLStatus.InstancePtr.txtTransactionDate.Text))
		//						{
		//							modStatusPayments.Statics.PaymentArray[intCT2].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmRECLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//						}
		//						else
		//						{
		//							frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//							modStatusPayments.Statics.PaymentArray[intCT2].RecordedTransactionDate = DateTime.Today;
		//						}
		//					}
		//					else
		//					{
		//						frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//						modStatusPayments.Statics.PaymentArray[intCT2].RecordedTransactionDate = DateTime.Today;
		//					}
		//					modStatusPayments.Statics.PaymentArray[intCT2].ResCode = lngCLResCode;
		//					modStatusPayments.Statics.PaymentArray[intCT2].Reference = "CREDIT";
		//					// Trim$(frmRECLStatus.txtReference & " ")
		//					modStatusPayments.Statics.PaymentArray[intCT2].ReversalID = -1;
		//					// .Teller =
		//					// .TransNumber =
		//					// .PaidBy =
		//					// .CHGINTNumber =
		//					// .Year = intYear
		//					modStatusPayments.Statics.PaymentArray[intCT2].Year = intYear + 10;
		//					// FillPaymentGrid frmRECLStatus.vsPayments.rows, intCT, intYear
		//					FillPaymentGrid(frmRECLStatus.InstancePtr.vsPayments.Rows, intCT2, intYear + 10);
		//				}
		//			}
		//		}
		//	}
		//	else
		//	{
		//		// kk02042015 trocl-1225  Change for readability    'Case Else
		//		cmdDisc_Click(2);
		//		// 2 = Cancel
		//	}
		//	// kk02042015                                       'End Select
		//	modStatusPayments.YearPending(FCConvert.ToInt16(intYear), 1);
		//	rsTemp.DisposeOf();
		//	rsLienRec.DisposeOf();
		//}

		public void FillPaymentGrid(int cRow, int intCT, int intYear, int lngID = 0)
		{
			frmRECLStatus.InstancePtr.vsPayments.AddItem("", cRow);
			if (intCT < 0)
			{
				frmRECLStatus.InstancePtr.vsPayments.TextMatrix(cRow, 9, FCConvert.ToString(intCT));
				intCT *= -1;
			}
			else
			{
				frmRECLStatus.InstancePtr.vsPayments.TextMatrix(cRow, 9, FCConvert.ToString(intCT));
			}
			// this adds it to the grid
			frmRECLStatus.InstancePtr.vsPayments.TextMatrix(cRow, 0, modExtraModules.FormatYear(intYear.ToString()));
			frmRECLStatus.InstancePtr.vsPayments.TextMatrix(cRow, 1, Strings.Format(modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate, "MM/dd/yyyy"));
			frmRECLStatus.InstancePtr.vsPayments.TextMatrix(cRow, 2, modStatusPayments.Statics.PaymentArray[intCT].Reference);
			frmRECLStatus.InstancePtr.vsPayments.TextMatrix(cRow, 3, modStatusPayments.Statics.PaymentArray[intCT].Period);
			frmRECLStatus.InstancePtr.vsPayments.TextMatrix(cRow, 4, modStatusPayments.Statics.PaymentArray[intCT].Code);
			frmRECLStatus.InstancePtr.vsPayments.TextMatrix(cRow, 5, modStatusPayments.Statics.PaymentArray[intCT].CashDrawer + "   " + modStatusPayments.Statics.PaymentArray[intCT].GeneralLedger);
			frmRECLStatus.InstancePtr.vsPayments.TextMatrix(cRow, 6, Strings.Format(modStatusPayments.Statics.PaymentArray[intCT].Principal, "#,##0.00"));
			frmRECLStatus.InstancePtr.vsPayments.TextMatrix(cRow, 7, Strings.Format(modStatusPayments.Statics.PaymentArray[intCT].CurrentInterest + modStatusPayments.Statics.PaymentArray[intCT].PreLienInterest, "#,##0.00"));
			frmRECLStatus.InstancePtr.vsPayments.TextMatrix(cRow, 8, Strings.Format(modStatusPayments.Statics.PaymentArray[intCT].LienCost, "#,##0.00"));
			frmRECLStatus.InstancePtr.vsPayments.TextMatrix(cRow, 10, FCConvert.ToString(lngID));
			frmRECLStatus.InstancePtr.vsPayments.TextMatrix(cRow, 11, FCConvert.ToString(modStatusPayments.Statics.PaymentArray[intCT].CHGINTNumber));
			frmRECLStatus.InstancePtr.vsPayments.TextMatrix(cRow, 12, Strings.Format(modStatusPayments.Statics.PaymentArray[intCT].Principal + modStatusPayments.Statics.PaymentArray[intCT].PreLienInterest + modStatusPayments.Statics.PaymentArray[intCT].CurrentInterest + modStatusPayments.Statics.PaymentArray[intCT].LienCost, "#,##0.00"));
		}

		private string GetAccountNote(ref int lngAccount, ref bool boolPopUp)
		{
			string GetAccountNote = "";
			// this function will return then string of the note
			// if boolPopUp is set, then pop the message up
			clsDRWrapper rsNote = new clsDRWrapper();
			if (modStatusPayments.Statics.boolRE)
			{
				rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(lngAccount) + " AND Type = 'RE'", modExtraModules.strCLDatabase);
			}
			else
			{
				rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(lngAccount) + " AND Type = 'PP'", modExtraModules.strCLDatabase);
			}
			if (rsNote.EndOfFile() != true)
			{
				GetAccountNote = FCConvert.ToString(rsNote.Get_Fields_String("Comment"));
				if (FCConvert.ToInt32(rsNote.Get_Fields_Int32("Priority")) > 0)
				{
					boolPopUp = true;
					// this will get passed back out of this function
				}
				else
				{
					boolPopUp = false;
				}
			}
			else
			{
			}
			return GetAccountNote;
		}

		//private void vsRateInfo_MouseMove(object sender, DataGridViewCellMouseEventArgs e)
		//{
		//	// this will show the contents of the box in the tooltip in case it is too large for the box
		//	int lngMR;
		//	int lngMC;
		//	lngMR = vsRateInfo.MouseRow;
		//	lngMC = vsRateInfo.MouseCol;
		//	if (lngMR > 0 && lngMC > 0)
		//	{
		//		ToolTip1.SetToolTip(vsRateInfo, vsRateInfo.TextMatrix(lngMR, lngMC));
		//	}
		//}

        private void vsRateInfo_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            // this will show the contents of the box in the tooltip in case it is too large for the box
            int lngMR;
            int lngMC;
            lngMR = vsRateInfo.GetFlexRowIndex(e.RowIndex);
            lngMC = vsRateInfo.GetFlexColIndex(e.ColumnIndex);
            if (lngMR > 0 && lngMC > 0)
            {
                DataGridViewCell cell = vsRateInfo[e.ColumnIndex, e.RowIndex];
                cell.ToolTipText = vsRateInfo.TextMatrix(lngMR, lngMC);
            }
        }


        public bool CheckTaxClubPayments(int lngDefYear)
		{
			bool CheckTaxClubPayments = false;
			clsDRWrapper rsTC = new clsDRWrapper();
			clsDRWrapper rsBL = new clsDRWrapper();
			double dblInt/*unused?*/;
			// this will check to see if this account has tax club payments due
			// if it does, then show the tax club label so the user can see it and
			// click it to automatically load the payment
			if (modStatusPayments.Statics.boolRE)
			{
				rsTC.OpenRecordset("SELECT * FROM TaxClub WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND [Year] = " + FCConvert.ToString(lngDefYear) + " AND Type = 'RE' AND TotalPayments >= TotalPaid", modExtraModules.strCLDatabase);
			}
			else
			{
				rsTC.OpenRecordset("SELECT * FROM TaxClub WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND [Year] = " + FCConvert.ToString(lngDefYear) + " AND Type = 'PP' AND TotalPayments >= TotalPaid", modExtraModules.strCLDatabase);
			}
			if (rsTC.EndOfFile())
			{
				// no pending Tax club activity
				lblTaxClub.Visible = false;
			}
			else
			{
				CheckTaxClubPayments = true;
				// there is pending tax club activity
				if (Conversion.Val(rsTC.Get_Fields_Double("TotalPayments")) - Conversion.Val(rsTC.Get_Fields_Double("TotalPaid")) > Conversion.Val(rsTC.Get_Fields_Double("PaymentAmount")))
				{
					lblTaxClub.Text = "TC: " + Strings.Format(rsTC.Get_Fields_Double("PaymentAmount"), "#,##0.00");
				}
				else
				{
					lblTaxClub.Text = "TC: " + Strings.Format((Conversion.Val(rsTC.Get_Fields_Double("TotalPayments")) - Conversion.Val(rsTC.Get_Fields_Double("TotalPaid"))), "#,##0.00");
				}
				// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
				lblTaxClub.Tag = rsTC.Get_Fields("Year");
				// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
				ToolTip1.SetToolTip(lblTaxClub, "Tax Club : " + modExtraModules.FormatYear(FCConvert.ToString(rsTC.Get_Fields("Year"))) + "   Interest Deferred : " + Strings.Format(dblLastTCInt, "$#,##0.00"));
				lblTaxClub.Visible = true;
			}
			return CheckTaxClubPayments;
		}

		private void CreateTaxClubPayment()
		{
			// this will create a tax club payment
			int intCT;
			int intYR;
			double dblP;
			modStatusPayments.Statics.lngCHGINT = 0;
			intYR = FCConvert.ToInt32(Math.Round(Conversion.Val(lblTaxClub.Tag)));
			// this will set the year
			for (intCT = 0; intCT <= cmbYear.Items.Count - 1; intCT++)
			{
				if (modExtraModules.FormatYear(intYR.ToString()) == Strings.Trim(cmbYear.Items[intCT].ToString()) || modExtraModules.FormatYear(intYR.ToString()) + "*" == Strings.Trim(cmbYear.Items[intCT].ToString()))
				{
					cmbYear.SelectedIndex = intCT;
					break;
				}
			}
			// default values
			txtCD.Text = "Y";
			txtCash.Text = "Y";
			txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtReference.Text = "TAXCLUB";
			// this will find the payment period in the combobox that matches the one on the payment
			for (intCT = 0; intCT <= cmbPeriod.Items.Count - 1; intCT++)
			{
				if ("A" == Strings.Left(cmbPeriod.Items[intCT].ToString(), 1))
				{
					cmbPeriod.SelectedIndex = intCT;
					break;
				}
			}
			frmRECLStatus.InstancePtr.boolChangingCode = true;
			// this will find the payment code in the combobox
			for (intCT = 0; intCT <= cmbCode.Items.Count - 1; intCT++)
			{
				if ("U" == Strings.Left(cmbCode.Items[intCT].ToString(), 1))
				{
					cmbCode.SelectedIndex = intCT;
					break;
				}
			}
			frmRECLStatus.InstancePtr.boolChangingCode = false;
			// set the total variables
			dblP = FCConvert.ToDouble(Strings.Right(lblTaxClub.Text, lblTaxClub.Text.Length - 3));
			txtComments.Text = "";
			txtPrincipal.Text = Strings.Format(dblP, "#,##0.00");
			txtInterest.Text = Strings.Format(0, "#,##0.00");
			txtCosts.Text = Strings.Format(0, "#,##0.00");
		}

		private void InputBoxAdjustment_2(bool boolCollapse)
		{
			InputBoxAdjustment(ref boolCollapse);
		}

		private void InputBoxAdjustment(ref bool boolCollapse)
		{
			// VBto upgrade warning: dblTemp As double	OnWriteFCConvert.ToDouble(
			double dblTemp = 0;
			// this will adjust the amount input boxes to only show one unless it is a correction
			boolCollapsedInputBoxes = boolCollapse;
			if (Conversion.Val(txtPrincipal.Text) == 0)
				txtPrincipal.Text = "0.00";
			if (Conversion.Val(txtInterest.Text) == 0)
				txtInterest.Text = "0.00";
			if (Conversion.Val(txtCosts.Text) == 0)
				txtCosts.Text = "0.00";
			// hide/show the labels and textboxes
			txtPrincipal.Visible = !boolCollapse;
			txtCosts.Visible = !boolCollapse;
			lblPrincipal.Visible = !boolCollapse;
			lblCosts.Visible = !boolCollapse;
			if (boolCollapse)
			{
				// make sure the interest field has all of the values from the other if the user had it in there first
				dblTemp = FCConvert.ToDouble(txtPrincipal.Text) + FCConvert.ToDouble(txtCosts.Text);
				txtPrincipal.Text = "0.00";
				txtCosts.Text = "0.00";
				txtInterest.Text = Strings.Format(FCConvert.ToDouble(txtInterest.Text) + dblTemp, "#,##0.00");
				lblInterest.Text = "Amount";
			}
			else
			{
				lblInterest.Text = "Interest";
			}
		}

		private void vsRateInfo_RowColChange(object sender, EventArgs e)
		{
			// if this is the applied through date, then allow the user to edit it
			if (modStatusPayments.Statics.boolRE)
			{
				switch (vsRateInfo.Row)
				{
					case 11:
						{
							if (vsRateInfo.Col == 4)
							{
								vsRateInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else
							{
								vsRateInfo.Editable = FCGrid.EditableSettings.flexEDNone;
							}
							break;
						}
					default:
						{
							vsRateInfo.Editable = FCGrid.EditableSettings.flexEDNone;
							break;
						}
				}
				//end switch
			}
			else
			{
				switch (vsRateInfo.Row)
				{
					case 13:
						{
							if (vsRateInfo.Col == 5)
							{
								vsRateInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else
							{
								vsRateInfo.Editable = FCGrid.EditableSettings.flexEDNone;
							}
							break;
						}
					default:
						{
							vsRateInfo.Editable = FCGrid.EditableSettings.flexEDNone;
							break;
						}
				}
				//end switch
			}
		}

		private void vsRateInfo_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsRateInfo.CurrentCell.IsInEditMode)
			{
				//FC:FINAL:MSH - save and use correct indexes of the cell
				int row = vsRateInfo.GetFlexRowIndex(e.RowIndex);
				int col = vsRateInfo.GetFlexColIndex(e.ColumnIndex);
				string strTemp = "";
				if ((modStatusPayments.Statics.boolRE && col == 4 && row == 11) || (!modStatusPayments.Statics.boolRE && col == 5 && row == 13))
				{
					// this is the applied through date
					if (Information.IsDate(vsRateInfo.EditText))
					{
						if (DateAndTime.DateValue(vsRateInfo.EditText).ToOADate() != 0)
						{
							// allow and change if different
							if (modStatusPayments.Statics.boolRE)
							{
								strTemp = Strings.Trim(vsRateInfo.TextMatrix(11, 4));
							}
							else
							{
								strTemp = Strings.Trim(vsRateInfo.TextMatrix(13, 5));
							}
							if (Strings.Trim(vsRateInfo.EditText) != strTemp)
							{
								DialogResult messageBoxResult = FCMessageBox.Show("Are you sure that you would like to edit the Applied Through Date.", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Edit Date");
								if (messageBoxResult == DialogResult.Yes)
								{
									if (modStatusPayments.Statics.boolRE)
									{
										modGlobalFunctions.AddCYAEntry_728("CL", "Edit Applied Through Date", "Account - " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE), "Year - " + modExtraModules.FormatYear(Strings.Right(fraRateInfo.Text, 6)), "New - " + vsRateInfo.EditText, "Old - " + vsRateInfo.TextMatrix(11, 4));
									}
									else
									{
										modGlobalFunctions.AddCYAEntry_728("CL", "Edit Applied Through Date", "Account - " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP), "Year - " + modExtraModules.FormatYear(Strings.Right(fraRateInfo.Text, 6)), "New - " + vsRateInfo.EditText, "Old - " + vsRateInfo.TextMatrix(11, 4));
									}
									SaveNewAppliedThroughDate_8(DateAndTime.DateValue(vsRateInfo.EditText), FCConvert.ToInt32(modExtraModules.FormatYear(Strings.Right(fraRateInfo.Text, 6))));
								}
								else if (messageBoxResult == DialogResult.No)
								{
									e.Cancel = true;
									vsRateInfo.Editable = FCGrid.EditableSettings.flexEDNone;
								}
								else if (messageBoxResult == DialogResult.Cancel)
								{
									vsRateInfo.EditText = strTemp;
								}
							}
						}
						else
						{
							e.Cancel = true;
							FCMessageBox.Show("Please enter a valid date.", MsgBoxStyle.Critical, "Edit Applied Through Date");
						}
					}
					else
					{
						e.Cancel = true;
						FCMessageBox.Show("Please enter a valid date.", MsgBoxStyle.Critical, "Edit Applied Through Date");
					}
				}
			}
		}
		// VBto upgrade warning: lngEditYear As int	OnWrite(string)
		private void SaveNewAppliedThroughDate_8(DateTime dtNewDate, int lngEditYear)
		{
			SaveNewAppliedThroughDate(ref dtNewDate, ref lngEditYear);
		}

		private void SaveNewAppliedThroughDate(ref DateTime dtNewDate, ref int lngEditYear)
		{
			// this routine will set the new date on the bill/lien
			clsDRWrapper rsEdit = new clsDRWrapper();
			if (modStatusPayments.Statics.boolRE)
			{
				rsEdit.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingType = 'RE' AND BillingYear = " + FCConvert.ToString(lngEditYear), modExtraModules.strCLDatabase);
				if (!rsEdit.EndOfFile())
				{
					if (((rsEdit.Get_Fields_Int32("LienRecordNumber"))) != 0)
					{
						// lien
						rsEdit.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsEdit.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
						if (!rsEdit.EndOfFile())
						{
							rsEdit.Edit();
							rsEdit.Set_Fields("InterestAppliedThroughDate", dtNewDate);
							rsEdit.Update();
						}
						else
						{
							FCMessageBox.Show("Lien not found, date not changed.", MsgBoxStyle.Exclamation, "Error Saving Date");
						}
					}
					else
					{
						// regular
						rsEdit.Edit();
						rsEdit.Set_Fields("InterestAppliedThroughDate", dtNewDate);
						rsEdit.Update();
					}
				}
				else
				{
					FCMessageBox.Show("Bill not found, date not changed.", MsgBoxStyle.Exclamation, "Error Saving Date");
				}
			}
			else
			{
				rsEdit.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingType = 'PP' AND BillingYear = " + FCConvert.ToString(lngEditYear), modExtraModules.strCLDatabase);
				if (!rsEdit.EndOfFile())
				{
					if (((rsEdit.Get_Fields_Int32("LienRecordNumber"))) != 0)
					{
						// lien
						rsEdit.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsEdit.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
						if (!rsEdit.EndOfFile())
						{
							rsEdit.Edit();
							rsEdit.Set_Fields("InterestAppliedThroughDate", dtNewDate);
							rsEdit.Update();
						}
						else
						{
							FCMessageBox.Show("Lien not found, date not changed.", MsgBoxStyle.Exclamation, "Error Saving Date");
						}
					}
					else
					{
						// regular
						rsEdit.Edit();
						rsEdit.Set_Fields("InterestAppliedThroughDate", dtNewDate);
						rsEdit.Update();
					}
				}
				else
				{
					FCMessageBox.Show("Bill not found, date not changed.", MsgBoxStyle.Exclamation, "Error Saving Date");
				}
			}
		}

		//private void SetToNewAccount(ref int lngNewAcctNumber)
		//{
		//	// this routine will reset the form with a new account number
		//	if (modStatusPayments.Statics.boolRE)
		//	{
		//		if (lngNewAcctNumber != StaticSettings.TaxCollectionValues.LastAccountRE)
		//		{
		//			StaticSettings.TaxCollectionValues.LastAccountRE = lngNewAcctNumber;
		//			CreateStatusQueries(ref lngNewAcctNumber);
		//			modStatusPayments.Statics.EffectiveDate = DateTime.Today;
		//			// this will reset the effective date to today
		//			frmRECLStatus.InstancePtr.Text = "Real Estate Account Status as of " + Strings.Format(modStatusPayments.Statics.EffectiveDate, "MM/dd/yyyy");
		//			// show the correct correction
		//			FormReset = true;
		//			// tell the startup function to allow a reset
		//			Form_Activate();
		//			// force the activate to run
		//			// Doevents
		//			FormReset = false;
		//			// turn of the reset
		//		}
		//	}
		//	else
		//	{
		//		if (lngNewAcctNumber != StaticSettings.TaxCollectionValues.LastAccountPP)
		//		{
		//			StaticSettings.TaxCollectionValues.LastAccountPP = lngNewAcctNumber;
		//			CreateStatusQueries(ref lngNewAcctNumber);
		//			modStatusPayments.Statics.EffectiveDate = DateTime.Today;
		//			// this will reset the effective date to today
		//			frmRECLStatus.InstancePtr.Text = "Real Estate Account Status as of " + FCConvert.ToString(modStatusPayments.Statics.EffectiveDate);
		//			FormReset = true;
		//			Form_Activate();
		//			// Doevents
		//			FormReset = false;
		//		}
		//	}
		//}

		//private void CreateStatusQueries(ref int lngAcct)
		//{
		//	// this will create the two queries strings that the status screen will use
		//	string strSQL;
		//	// this will load all of the bills
		//	strSQL = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct);
		//	modStatusPayments.Statics.strCurrentAccountBills = strSQL;
		//	// this will load all of the accounts
		//	strSQL = "SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(lngAcct);
		//	modStatusPayments.Statics.strCurrentAccountPayments = strSQL;
		//}
		//// VBto upgrade warning: 'Return' As short	OnWrite(string, short)
		private short FindLatestBillYear()
		{
			short FindLatestBillYear = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsYear = new clsDRWrapper();
				// this function will return the lastest billing year
				if (modStatusPayments.Statics.boolRE)
				{
					rsYear.OpenRecordset("SELECT Top 1 BillingYear FROM BillingMaster WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingType = 'RE' ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
				}
				else
				{
					rsYear.OpenRecordset("SELECT Top 1 BillingYear FROM BillingMaster WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingType = 'PP' ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
				}
				if (!rsYear.EndOfFile())
				{
					// return the latest year
					FindLatestBillYear = FCConvert.ToInt16((FCConvert.ToString(FCUtils.iDiv(FCConvert.ToInt32(rsYear.Get_Fields_Int32("BillingYear")), 10)) + "1"));
				}
				else
				{
					FindLatestBillYear = -1;
				}
				return FindLatestBillYear;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FindLatestBillYear = -1;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Finding Latest Year");
			}
			return FindLatestBillYear;
		}

		private bool CheckForLastYearOverpayment()
		{
			bool CheckForLastYearOverpayment = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will check to see if there is going to be an overpayment on the last
				// bill that is on this account...if there is, then it will prompt the user to create a new
				// bill and apply the credit to that bill as a prepayment
				int intYear = 0;
				int intCT/*unused?*/;
				double dblPaymentTotal;
				int lngAcct = 0;
				string strType = "";
				clsDRWrapper rsCheck = new clsDRWrapper();
				if (modStatusPayments.Statics.boolRE)
				{
					lngAcct = StaticSettings.TaxCollectionValues.LastAccountRE;
					strType = "RE";
				}
				else
				{
					lngAcct = StaticSettings.TaxCollectionValues.LastAccountPP;
					strType = "PP";
				}
				dblPaymentTotal = FCConvert.ToDouble(txtPrincipal.Text) + FCConvert.ToDouble(txtInterest.Text) + FCConvert.ToDouble(txtCosts.Text);
				if (dblPaymentTotal > modCLCalculations.CalculateAccountTotal(lngAcct, modStatusPayments.Statics.boolRE, false, modStatusPayments.Statics.EffectiveDate))
				{
					// check to see if this is being applied to the last year
					// if so see if the user would want to adda billing year
					intYear = FindLatestBillYear() + 10;
					rsCheck.OpenRecordset("SELECT Top 1 BillingYear, TaxDue1, TaxDue2, TaxDue3, TaxDue4  FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " AND BillingType = '" + strType + "' ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
					if (!rsCheck.EndOfFile())
					{
						// check to see if the latest bill is a prepay year
						if (Conversion.Val(rsCheck.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsCheck.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsCheck.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsCheck.Get_Fields_Decimal("TaxDue4")) > 0)
						{
							// this is a billed year keep going
						}
						else
						{
							// this is a prepayment year, do not make multiple
							CheckForLastYearOverpayment = false;
							return CheckForLastYearOverpayment;
						}
					}
					else
					{
					}
					DialogResult messageBoxResult = FCMessageBox.Show("There is an overpayment on the last billed year.  Would you like to create a new billing year and apply the overpayment to that?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Overpayment");
					if (messageBoxResult == DialogResult.Yes)
					{
						// add a billing year
						modStatusPayments.CreateBlankCLBill_2(intYear, lngAcct, strType);
						CheckForLastYearOverpayment = true;
					}
					else if (messageBoxResult == DialogResult.Cancel)
					{
						CheckForLastYearOverpayment = false;
					}
				}
				return CheckForLastYearOverpayment;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CheckForLastYearOverpayment = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Last Year Overpayment");
			}
			return CheckForLastYearOverpayment;
		}

		private bool CheckForLastYearOverpayment_Discount()
		{
			bool CheckForLastYearOverpayment_Discount = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// MAL@20071005: Added to handle discount credits
				// this routine will check to see if there is going to be an overpayment on the last
				// bill that is on this account...if there is, then it will prompt the user to create a new
				// bill and apply the credit to that bill as a prepayment
				int intYear = 0;
				int intCT/*unused?*/;
				double dblPaymentTotal;
				int lngAcct = 0;
				string strType = "";
				clsDRWrapper rsCheck = new clsDRWrapper();
				if (modStatusPayments.Statics.boolRE)
				{
					lngAcct = StaticSettings.TaxCollectionValues.LastAccountRE;
					strType = "RE";
				}
				else
				{
					lngAcct = StaticSettings.TaxCollectionValues.LastAccountPP;
					strType = "PP";
				}
				dblPaymentTotal = FCConvert.ToDouble(lblDiscHiddenTotal.Text);
				if (dblPaymentTotal > (modCLCalculations.CalculateAccountTotal(lngAcct, modStatusPayments.Statics.boolRE, false, modStatusPayments.Statics.EffectiveDate) - FCConvert.ToDouble(this.txtDiscDisc.Text)))
				{
					// check to see if this is being applied to the last year
					// if so see if the user would want to adda billing year
					intYear = FindLatestBillYear() + 10;
					rsCheck.OpenRecordset("SELECT Top 1 BillingYear, TaxDue1, TaxDue2, TaxDue3, TaxDue4  FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " AND BillingType = '" + strType + "' ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
					if (!rsCheck.EndOfFile())
					{
						// check to see if the latest bill is a prepay year
						if (Conversion.Val(rsCheck.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsCheck.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsCheck.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsCheck.Get_Fields_Decimal("TaxDue4")) > 0)
						{
							// this is a billed year keep going
						}
						else
						{
							// this is a prepayment year, do not make multiple
							CheckForLastYearOverpayment_Discount = false;
							return CheckForLastYearOverpayment_Discount;
						}
					}
					else
					{
					}
					DialogResult messageBoxResult = FCMessageBox.Show("There is an overpayment on the last billed year.  Would you like to create a new billing year and apply the overpayment to that?", MsgBoxStyle.Question | MsgBoxStyle.YesNoCancel, "Overpayment");
					if (messageBoxResult == DialogResult.Yes)
					{
						// add a billing year
						modStatusPayments.CreateBlankCLBill_2(intYear, lngAcct, strType);
						lngNewBillKey = GetBillKey(intYear, lngAcct);
						CheckForLastYearOverpayment_Discount = true;
					}
					else if (messageBoxResult == DialogResult.Cancel)
					{
						CheckForLastYearOverpayment_Discount = false;
					}
				}
				return CheckForLastYearOverpayment_Discount;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				CheckForLastYearOverpayment_Discount = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Last Year Overpayment");
			}
			return CheckForLastYearOverpayment_Discount;
		}

		private void RemoveSavedPayments()
		{
			// this routine will remove all of the payments that are already saved in the database
			// and will be reset when the effective date is reset or the screen is reset
			int lngCT;
			int lngIndex;
			for (lngCT = vsPayments.Rows - 1; lngCT >= 1; lngCT--)
			{
				if (Conversion.Val(vsPayments.TextMatrix(lngCT, 10)) != 0)
				{
					// this means that there is a record created for this in the DB
					lngIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(lngCT, 9))));
					// get the index in the array
					modStatusPayments.Statics.PaymentArray[lngIndex].Year = 0;
					modStatusPayments.Statics.PaymentArray[lngIndex].Account = 0;
					modStatusPayments.Statics.PaymentArray[lngIndex].ID = 0;
					modStatusPayments.Statics.PaymentArray[lngIndex].LienCost = 0;
					modStatusPayments.Statics.PaymentArray[lngIndex].PreLienInterest = 0;
					modStatusPayments.Statics.PaymentArray[lngIndex].Principal = 0;
					modStatusPayments.Statics.PaymentArray[lngIndex].CurrentInterest = 0;
					vsPayments.RemoveItem(lngCT);
				}
			}
		}

		private bool PaymentForDefaultYear()
		{
			bool PaymentForDefaultYear = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngDefYear = 0;
				if (modStatusPayments.Statics.boolRE)
				{
					lngDefYear = modStatusPayments.FindDefaultYear( StaticSettings.TaxCollectionValues.LastAccountRE);
				}
				else
				{
					lngDefYear = modStatusPayments.FindDefaultYear( StaticSettings.TaxCollectionValues.LastAccountPP);
				}
				if (Conversion.Val(modExtraModules.FormatYear(cmbYear.Items[cmbYear.SelectedIndex].ToString().Replace("*", ""))) == lngDefYear)
				{
					// This is the correct year so this is fine
					PaymentForDefaultYear = true;
				}
				return PaymentForDefaultYear;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				// function is false
				PaymentForDefaultYear = false;
			}
			return PaymentForDefaultYear;
		}

		private string GetRefundedAbabtementAccount_8(bool boolLien, string strYear)
		{
			return GetRefundedAbabtementAccount(ref boolLien, ref strYear);
		}

		private string GetRefundedAbabtementAccount(ref bool boolLien, ref string strYear)
		{
			string GetRefundedAbabtementAccount = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsAcct = new clsDRWrapper();
				if (modStatusPayments.Statics.boolRE)
				{
					if (boolLien)
					{
						rsAcct.OpenRecordset("SELECT Account1, Year1 FROM Type WHERE TypeCode = 91", modExtraModules.strCRDatabase);
					}
					else
					{
						rsAcct.OpenRecordset("SELECT Account1, Year1 FROM Type WHERE TypeCode = 90", modExtraModules.strCRDatabase);
					}
				}
				else
				{
					rsAcct.OpenRecordset("SELECT Account1, Year1 FROM Type WHERE TypeCode = 92", modExtraModules.strCRDatabase);
				}
				if (rsAcct.EndOfFile())
				{
					GetRefundedAbabtementAccount = "M NO ENTRY";
				}
				else
				{
					if (FCConvert.ToBoolean(rsAcct.Get_Fields_Boolean("Year1")) && Strings.Left(FCConvert.ToString(rsAcct.Get_Fields_String("Account1")), 1) == "G")
					{
						GetRefundedAbabtementAccount = Strings.Left(FCConvert.ToString(rsAcct.Get_Fields_String("Account1")), FCConvert.ToString(rsAcct.Get_Fields_String("Account1")).Length - 2) + strYear;
					}
					else
					{
						if (Strings.Left(FCConvert.ToString(rsAcct.Get_Fields_String("Account1")), 1) == "M" && Strings.InStr(2, "XX", FCConvert.ToString(rsAcct.Get_Fields_String("Account1"))) != 0)
						{
							GetRefundedAbabtementAccount = FCConvert.ToString(rsAcct.Get_Fields_String("Account1")).Replace("XX", strYear);
						}
						else
						{
							GetRefundedAbabtementAccount = FCConvert.ToString(rsAcct.Get_Fields_String("Account1"));
						}
					}
				}
				return GetRefundedAbabtementAccount;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " _ " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Finding Refunded Abatement Account");
			}
			return GetRefundedAbabtementAccount;
		}

		private int GetBillKey(int lngYear, int lngAcct)
		{
			int GetBillKey = 0;
			// MAL@20071004
			clsDRWrapper rsBill = new clsDRWrapper();
			int lngResult = 0;
			rsBill.OpenRecordset("SELECT * FROM BillingMaster WHERE BillingYear = " + FCConvert.ToString(lngYear) + " AND Account = " + FCConvert.ToString(lngAcct), modExtraModules.strCLDatabase);
			if (rsBill.RecordCount() > 0)
			{
				lngResult = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
			}
			GetBillKey = lngResult;
			return GetBillKey;
		}

		//private void ShowWriteoffFrame(bool boolCheckInt = false)
		//{
		//	// this sub will show the Write-Off Frame with calculated figures, users can change the amounts
		//	double dblTotal = 0;
		//	// the total payment needed
		//	double dblDisc/*unused?*/;
		//	// total discount
		//	double dblDiscPayment/*unused?*/;
		//	// how much is due after discount is taken
		//	RETRYWRITEOFF:
		//	;
		//	if (Strings.Trim(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 1)) != "")
		//	{
		//		if (Conversion.Val(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 1), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 1).Length - 2)) != 0)
		//		{
		//			dblTemp[1] = FCConvert.ToDouble(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 1), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 1).Length - 2));
		//		}
		//	}
		//	if (Strings.Trim(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 2)) != "")
		//	{
		//		if (Conversion.Val(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 2), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 2).Length - 2)) != 0)
		//		{
		//			dblTemp[2] = FCConvert.ToDouble(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 2), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 2).Length - 2));
		//		}
		//	}
		//	if (Strings.Trim(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 3)) != "")
		//	{
		//		if (Conversion.Val(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 3), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 3).Length - 2)) != 0)
		//		{
		//			dblTemp[3] = FCConvert.ToDouble(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 3), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 3).Length - 2));
		//		}
		//	}
		//	if (Strings.Trim(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 4)) != "")
		//	{
		//		if (Conversion.Val(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 4), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 4).Length - 2)) != 0)
		//		{
		//			dblTemp[4] = FCConvert.ToDouble(Strings.Right(frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 4), frmRECLStatus.InstancePtr.vsPeriod.TextMatrix(0, 4).Length - 2));
		//		}
		//	}
		//	// find which year is being written off
		//	intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(Strings.Left(frmRECLStatus.InstancePtr.cmbYear.Items[frmRECLStatus.InstancePtr.cmbYear.SelectedIndex].ToString(), 6)))));
		//	// find the total due for this year
		//	if (modStatusPayments.Statics.boolRE)
		//	{
		//		rsDisc.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'RE'", modExtraModules.strCLDatabase);
		//		lngAccount = StaticSettings.TaxCollectionValues.LastAccountRE;
		//	}
		//	else
		//	{
		//		rsDisc.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'PP'", modExtraModules.strCLDatabase);
		//		lngAccount = StaticSettings.TaxCollectionValues.LastAccountPP;
		//	}
		//	if (rsDisc.RecordCount() != 0)
		//	{
		//		if (((rsDisc.Get_Fields_Int32("LienRecordNumber"))) == 0)
		//		{
		//			// regular billing
		//			// Get Current Interest
		//			dblTempTotal = modCLCalculations.CalculateAccountCL(ref rsDisc, lngAccount, modStatusPayments.Statics.EffectiveDate, ref dblCurrInt);
		//			dblPrin = (Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue1")) + Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue2")) + Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue3")) + Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue4"))) - Conversion.Val(rsDisc.Get_Fields_Decimal("PrincipalPaid"));
		//			dblInt = (Conversion.Val(rsDisc.Get_Fields_Decimal("InterestCharged")) + Conversion.Val(rsDisc.Get_Fields_Decimal("InterestPaid"))) - dblCurrInt;
		//			dblPLI = 0;
		//			dblCosts = Conversion.Val(rsDisc.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsDisc.Get_Fields_Decimal("DemandFeesPaid"));
		//		}
		//		else
		//		{
		//			// lien
		//			LRN = FCConvert.ToInt32(rsDisc.Get_Fields_Int32("LienRecordNumber"));
		//			if (rsDisc.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(LRN), modExtraModules.strCLDatabase))
		//			{
		//				if (rsDisc.RecordCount() != 0)
		//				{
		//					dblTempTotal = modCLCalculations.CalculateAccountCLLien(rsDisc, modStatusPayments.Statics.EffectiveDate, ref dblCurrInt);
		//					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//					dblPrin = Conversion.Val(rsDisc.Get_Fields("Principal")) - Conversion.Val(rsDisc.Get_Fields_Decimal("PrincipalPaid"));
		//					dblInt = FCConvert.ToDouble((rsDisc.Get_Fields_Decimal("InterestCharged") - rsDisc.Get_Fields_Decimal("InterestPaid") * -1)) - dblCurrInt;
		//					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//					// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//					dblPLI = Conversion.Val(rsDisc.Get_Fields("Interest")) - Conversion.Val(rsDisc.Get_Fields("PLIPaid"));
		//					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//					// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//					dblCosts = Conversion.Val(rsDisc.Get_Fields("Costs")) - Conversion.Val(rsDisc.Get_Fields("MaturityFee")) - Conversion.Val(rsDisc.Get_Fields_Decimal("CostsPaid"));
		//				}
		//				else
		//				{
		//					// lien record not found
		//					FCMessageBox.Show("Lien record number, " + FCConvert.ToString(LRN) + ", not found.", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "ERROR");
		//				}
		//			}
		//			else
		//			{
		//				FCMessageBox.Show("Error opening table LienRec.", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "ERROR");
		//			}
		//		}
		//		dblTotal = dblPrin - dblInt + dblPLI + dblCosts;
		//		// Show Values
		//		frmRECLStatus.InstancePtr.txtWOPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
		//		frmRECLStatus.InstancePtr.txtWOCurInterest.Text = Strings.Format(dblInt * -1, "#,##0.00");
		//		frmRECLStatus.InstancePtr.txtWOPLI.Text = Strings.Format(dblPLI, "#,##0.00");
		//		frmRECLStatus.InstancePtr.txtWOCosts.Text = Strings.Format(dblCosts, "#,##0.00");
		//		frmRECLStatus.InstancePtr.lblWOTotal.Text = Strings.Format(dblTotal, "#,##0.00");
		//		// align the frame
		//		frmRECLStatus.InstancePtr.fraWriteOff.Left = FCConvert.ToInt32((frmRECLStatus.InstancePtr.Width - frmRECLStatus.InstancePtr.fraWriteOff.Width) / 2.0);
		//		frmRECLStatus.InstancePtr.fraWriteOff.Top = FCConvert.ToInt32((frmRECLStatus.InstancePtr.ClientArea.Height - frmRECLStatus.InstancePtr.fraWriteOff.Height) / 2.0);
		//		// show the frame
		//		frmRECLStatus.InstancePtr.fraWriteOff.Visible = true;
		//		//frmRECLStatus.InstancePtr.mnuPaymentSaveExit.Enabled = false;
		//		//frmRECLStatus.InstancePtr.mnuPaymentSave.Enabled = false;
		//		if (((rsDisc.Get_Fields_Int32("LienRecordNumber"))) == 0)
		//		{
		//			frmRECLStatus.InstancePtr.txtWOPLI.Enabled = false;
		//		}
		//		else
		//		{
		//			frmRECLStatus.InstancePtr.txtWOPLI.Enabled = true;
		//		}
		//		frmRECLStatus.InstancePtr.GRID.Visible = false;
		//		frmRECLStatus.InstancePtr.fraPayment.Visible = false;
		//		frmRECLStatus.InstancePtr.mnuPayment.Visible = false;
		//		//frmRECLStatus.InstancePtr.mnuPaymentPreview.Visible = false;
		//	}
		//	else
		//	{
		//		// no current record
		//		if ((FCMessageBox.Show("There is no bill for year " + modExtraModules.FormatYear(FCConvert.ToString(intYear)) + ".", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "Writeoff") == DialogResult.No) || (FCMessageBox.Show("There is no bill for year " + modExtraModules.FormatYear(FCConvert.ToString(intYear)) + ".", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "Writeoff") == DialogResult.Cancel))
		//		{
		//			frmRECLStatus.InstancePtr.boolChangingCode = true;
		//			// MAL@2007115
		//			// frmRECLStatus.cmbCode.ListIndex = 3     'set the code box back to a payment
		//			frmRECLStatus.InstancePtr.cmbCode.SelectedIndex = modStatusPayments.GetPaymentCodeListIndex("P");
		//			frmRECLStatus.InstancePtr.boolChangingCode = false;
		//		}
		//	}
		//	rsDisc.Reset();
		//}
		// VBto upgrade warning: intYear As short	OnWriteFCConvert.ToDouble(
		public void GetAllCLAccounts(int lngType, short intYear = 0)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return the correct Account numbers to use in the Account Box
				clsDRWrapper rsBD = new clsDRWrapper();
				if (modGlobalConstants.Statics.gboolCR)
				{
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						rsBD.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngType) + " AND TownKey = " + FCConvert.ToString(lngCLResCode), modExtraModules.strCRDatabase);
					}
					else
					{
						rsBD.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngType), modExtraModules.strCRDatabase);
					}
					if (!rsBD.EndOfFile())
					{
						switch (lngType)
						{
							case 90:
							case 92:
							case 890:
							case 892:
								{
									if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account1"))))
									{
										strPrinAcct = FCConvert.ToString(rsBD.Get_Fields_String("Account1"));
										// Principal
										if (rsBD.Get_Fields_Boolean("Year1"))
										{
											// Append Year to End of Account
											strPrinAcct = Strings.Left(strPrinAcct, strPrinAcct.Length - 3);
											strPrinAcct += "-" + Strings.Right(intYear.ToString(), 2);
										}
										else
										{
											strPrinAcct = strPrinAcct;
										}
									}
									else
									{
										strPrinAcct = "";
									}
									if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account2"))))
									{
										strIntAcct = FCConvert.ToString(rsBD.Get_Fields_String("Account2"));
										// Interest
										if (rsBD.Get_Fields_Boolean("Year2"))
										{
											// Append Year to End of Account
											strIntAcct = Strings.Left(strIntAcct, strIntAcct.Length - 3);
											strIntAcct += "-" + Strings.Right(intYear.ToString(), 2);
										}
										else
										{
											strIntAcct = strIntAcct;
										}
									}
									else
									{
										strIntAcct = "";
									}
									strPLIAcct = "";
									// Pre-Lien Interest
									if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account4"))))
									{
										strCostAcct = FCConvert.ToString(rsBD.Get_Fields_String("Account4"));
										// Costs
										if (rsBD.Get_Fields_Boolean("Year4"))
										{
											// Append Year to End of Account
											strCostAcct = Strings.Left(strCostAcct, strCostAcct.Length - 3);
											strCostAcct += "-" + Strings.Right(intYear.ToString(), 2);
										}
										else
										{
											strCostAcct = strCostAcct;
										}
									}
									else
									{
										strCostAcct = "";
									}
									break;
								}
							case 91:
							case 891:
								{
									if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account1"))))
									{
										strPrinAcct = FCConvert.ToString(rsBD.Get_Fields_String("Account1"));
										// Principal
										if (rsBD.Get_Fields_Boolean("Year1"))
										{
											// Append Year to End of Account
											strPrinAcct = Strings.Left(strPrinAcct, strPrinAcct.Length - 3);
											strPrinAcct += "-" + Strings.Right(intYear.ToString(), 2);
										}
										else
										{
											strPrinAcct = strPrinAcct;
										}
									}
									else
									{
										strPrinAcct = "";
									}
									if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account2"))))
									{
										strIntAcct = FCConvert.ToString(rsBD.Get_Fields_String("Account2"));
										// Interest
										if (rsBD.Get_Fields_Boolean("Year2"))
										{
											// Append Year to End of Account
											strIntAcct = Strings.Left(strIntAcct, strIntAcct.Length - 3);
											strIntAcct += "-" + Strings.Right(intYear.ToString(), 2);
										}
										else
										{
											strIntAcct = strIntAcct;
										}
									}
									else
									{
										strIntAcct = "";
									}
									if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account3"))))
									{
										strPLIAcct = FCConvert.ToString(rsBD.Get_Fields_String("Account3"));
										// Pre-Lien Interest
										if (rsBD.Get_Fields_Boolean("Year3"))
										{
											// Append Year to End of Account
											strPLIAcct = Strings.Left(strPLIAcct, strPLIAcct.Length - 3);
											strPLIAcct += "-" + Strings.Right(intYear.ToString(), 2);
										}
										else
										{
											strPLIAcct = strPLIAcct;
										}
									}
									else
									{
										strPLIAcct = "";
									}
									if (modValidateAccount.AccountValidate(FCConvert.ToString(rsBD.Get_Fields_String("Account4"))))
									{
										strCostAcct = FCConvert.ToString(rsBD.Get_Fields_String("Account4"));
										// Costs
										if (rsBD.Get_Fields_Boolean("Year4"))
										{
											// Append Year to End of Account
											strCostAcct = Strings.Left(strCostAcct, strCostAcct.Length - 3);
											strCostAcct += "-" + Strings.Right(intYear.ToString(), 2);
										}
										else
										{
											strCostAcct = strCostAcct;
										}
									}
									else
									{
										strCostAcct = "";
									}
									break;
								}
						}
						//end switch
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Account Information Error");
			}
		}

		//private void AddWriteOfftoList()
		//{
		//	// MAL@20071010
		//	double dblPrinNeed;
		//	double dblIntNeed;
		//	double dblPLINeed;
		//	double dblCostNeed;
		//	int intYear;
		//	int intCT;
		//	int intTemp/*unused?*/;
		//	int lngAcct = 0;
		//	string PayCode = "";
		//	string strTemp = "";
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	clsDRWrapper rsLienRec = new clsDRWrapper();
		//	intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(Strings.Left(frmRECLStatus.InstancePtr.cmbYear.Items[frmRECLStatus.InstancePtr.cmbYear.SelectedIndex].ToString(), 6)))));
		//	if (modStatusPayments.Statics.boolRE)
		//	{
		//		strTemp = "SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'RE'";
		//		lngAcct = StaticSettings.TaxCollectionValues.LastAccountRE;
		//	}
		//	else
		//	{
		//		strTemp = "SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'PP'";
		//		lngAcct = StaticSettings.TaxCollectionValues.LastAccountPP;
		//	}
		//	rsTemp.OpenRecordset(strTemp, modExtraModules.strCLDatabase);
		//	if (rsTemp.RecordCount() != 0)
		//	{
		//	}
		//	else
		//	{
		//		FCMessageBox.Show("There is no bill created for " + modExtraModules.FormatYear(intYear.ToString()) + ".", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "ERROR");
		//		return;
		//	}
		//	for (intCT = 1; intCT <= modStatusPayments.MAX_PAYMENTS; intCT++)
		//	{
		//		// this finds a place in the array
		//		if (modStatusPayments.Statics.PaymentArray[intCT].Year == 0)
		//		{
		//			// to store the new payment record
		//			break;
		//		}
		//	}
		//	//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
		//	modStatusPayments.Statics.PaymentArray[intCT] = new modStatusPayments.Payment(0);
		//	// get the next year and its total due values
		//	if (((rsTemp.Get_Fields_Int32("LienRecordNumber"))) == 0)
		//	{
		//		dblPrinNeed = Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4")) - Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid"));
		//		dblIntNeed = (Conversion.Val(rsTemp.Get_Fields_Decimal("InterestCharged")) + Conversion.Val(rsTemp.Get_Fields_Decimal("InterestPaid"))) * -1;
		//		dblPLINeed = 0;
		//		dblCostNeed = Conversion.Val(rsTemp.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsTemp.Get_Fields_Decimal("DemandFeesPaid"));
		//		if (modStatusPayments.Statics.boolRE)
		//		{
		//			PayCode = "R";
		//		}
		//		else
		//		{
		//			PayCode = "P";
		//		}
		//	}
		//	else
		//	{
		//		PayCode = "L";
		//		rsLienRec.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsTemp.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
		//		if (rsLienRec.RecordCount() != 0)
		//		{
		//			// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//			dblPrinNeed = Conversion.Val(rsLienRec.Get_Fields("Principal")) - Conversion.Val(rsLienRec.Get_Fields_Decimal("PrincipalPaid"));
		//			dblIntNeed = Conversion.Val(rsLienRec.Get_Fields_Decimal("InterestCharged")) - Conversion.Val(rsLienRec.Get_Fields_Decimal("InterestPaid")) + modStatusPayments.Statics.dblCurrentInt[(intYear) - modExtraModules.DEFAULTSUBTRACTIONVALUE];
		//			// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//			// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//			dblPLINeed = Conversion.Val(rsLienRec.Get_Fields("Interest")) - Conversion.Val(rsLienRec.Get_Fields("PLIPaid"));
		//			// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//			// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//			dblCostNeed = Conversion.Val(rsLienRec.Get_Fields("Costs")) - Conversion.Val(rsLienRec.Get_Fields("MaturityFee")) - Conversion.Val(rsLienRec.Get_Fields_Decimal("CostsPaid"));
		//		}
		//	}
		//	// Principal
		//	if (intCT < modStatusPayments.MAX_PAYMENTS)
		//	{
		//		if (txtWOPrincipal.Text != "" && Conversion.Val(txtWOPrincipal.Text) != 0)
		//		{
		//			if (FCConvert.ToDouble(txtWOPrincipal.Text) != 0)
		//			{
		//				// this adds it to the array of payments
		//				modStatusPayments.Statics.PaymentArray[intCT].Account = lngAcct;
		//				modGlobalFunctions.AddCYAEntry_8("CL", "Non-Budgetary Principal. RE Account: " + FCConvert.ToString(modStatusPayments.Statics.PaymentArray[intCT].Account));
		//				modStatusPayments.Statics.PaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//				modStatusPayments.Statics.PaymentArray[intCT].BillCode = PayCode;
		//				if (PayCode == "L")
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
		//				}
		//				else
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
		//				}
		//				txtReference.Text = "NB";
		//				modStatusPayments.Statics.PaymentArray[intCT].CashDrawer = "N";
		//				// allow the user to set it to yesterday
		//				modStatusPayments.Statics.PaymentArray[intCT].Code = "N";
		//				modStatusPayments.Statics.PaymentArray[intCT].ResCode = lngCLResCode;
		//				modStatusPayments.Statics.PaymentArray[intCT].Comments = frmRECLStatus.InstancePtr.txtComments.Text;
		//				modStatusPayments.Statics.PaymentArray[intCT].EffectiveInterestDate = modStatusPayments.Statics.EffectiveDate;
		//				modStatusPayments.Statics.PaymentArray[intCT].GeneralLedger = "N";
		//				modStatusPayments.Statics.PaymentArray[intCT].Period = Strings.Left(cmbPeriod.Items[cmbPeriod.SelectedIndex].ToString(), 1);
		//				modStatusPayments.Statics.PaymentArray[intCT].PreLienInterest = 0;
		//				if (txtWOPrincipal.Text != "" && Conversion.Val(txtWOPrincipal.Text) != 0)
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].Principal = FCConvert.ToDecimal(txtWOPrincipal.Text);
		//				}
		//				else
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].Principal = 0;
		//				}
		//				modStatusPayments.Statics.PaymentArray[intCT].CurrentInterest = 0;
		//				modStatusPayments.Statics.PaymentArray[intCT].LienCost = 0;
		//				modStatusPayments.Statics.PaymentArray[intCT].ReceiptNumber = "P";
		//				if (frmRECLStatus.InstancePtr.txtTransactionDate.Text != "")
		//				{
		//					if (Information.IsDate(frmRECLStatus.InstancePtr.txtTransactionDate.Text))
		//					{
		//						modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmRECLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//					}
		//					else
		//					{
		//						frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//						modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//					}
		//				}
		//				else
		//				{
		//					frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//					modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//				}
		//				modStatusPayments.Statics.PaymentArray[intCT].Reference = Strings.Trim(txtReference.Text + " ");
		//				modStatusPayments.Statics.PaymentArray[intCT].ReversalID = 0;
		//				modStatusPayments.Statics.PaymentArray[intCT].Year = intYear;
		//				modStatusPayments.Statics.PaymentArray[intCT].BudgetaryAccountNumber = strPrinAcct;
		//				FillPaymentGrid(vsPayments.Rows, intCT, intYear);
		//			}
		//		}
		//	}
		//	for (intCT = 1; intCT <= modStatusPayments.MAX_PAYMENTS; intCT++)
		//	{
		//		// this finds a place in the array
		//		if (modStatusPayments.Statics.PaymentArray[intCT].Year == 0)
		//		{
		//			// to store the new payment record
		//			break;
		//		}
		//	}
		//	// Interest
		//	if (intCT < modStatusPayments.MAX_PAYMENTS)
		//	{
		//		if (txtWOCurInterest.Text != "" && Conversion.Val(txtWOCurInterest.Text) != 0)
		//		{
		//			if (FCConvert.ToDouble(txtWOCurInterest.Text) != 0)
		//			{
		//				// this adds it to the array of payments
		//				modStatusPayments.Statics.PaymentArray[intCT].Account = lngAcct;
		//				modGlobalFunctions.AddCYAEntry_8("CL", "Non-Budgetary Interest. RE Account: " + FCConvert.ToString(modStatusPayments.Statics.PaymentArray[intCT].Account));
		//				modStatusPayments.Statics.PaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//				modStatusPayments.Statics.PaymentArray[intCT].BillCode = PayCode;
		//				if (PayCode == "L")
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
		//				}
		//				else
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
		//				}
		//				txtReference.Text = "NB";
		//				modStatusPayments.Statics.PaymentArray[intCT].CashDrawer = "N";
		//				// allow the user to set it to yesterday
		//				modStatusPayments.Statics.PaymentArray[intCT].Code = "N";
		//				modStatusPayments.Statics.PaymentArray[intCT].ResCode = lngCLResCode;
		//				modStatusPayments.Statics.PaymentArray[intCT].Comments = frmRECLStatus.InstancePtr.txtComments.Text;
		//				modStatusPayments.Statics.PaymentArray[intCT].EffectiveInterestDate = modStatusPayments.Statics.EffectiveDate;
		//				modStatusPayments.Statics.PaymentArray[intCT].GeneralLedger = "N";
		//				// cannot write this off
		//				modStatusPayments.Statics.PaymentArray[intCT].Period = Strings.Left(cmbPeriod.Items[cmbPeriod.SelectedIndex].ToString(), 1);
		//				modStatusPayments.Statics.PaymentArray[intCT].PreLienInterest = 0;
		//				modStatusPayments.Statics.PaymentArray[intCT].Principal = 0;
		//				if (txtWOCurInterest.Text != "" && Conversion.Val(txtWOCurInterest.Text) != 0)
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(txtWOCurInterest.Text);
		//				}
		//				else
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].CurrentInterest = 0;
		//				}
		//				modStatusPayments.Statics.PaymentArray[intCT].LienCost = 0;
		//				modStatusPayments.Statics.PaymentArray[intCT].ReceiptNumber = "P";
		//				if (frmRECLStatus.InstancePtr.txtTransactionDate.Text != "")
		//				{
		//					if (Information.IsDate(frmRECLStatus.InstancePtr.txtTransactionDate.Text))
		//					{
		//						modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmRECLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//					}
		//					else
		//					{
		//						frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//						modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//					}
		//				}
		//				else
		//				{
		//					frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//					modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//				}
		//				modStatusPayments.Statics.PaymentArray[intCT].Reference = Strings.Trim(txtReference.Text + " ");
		//				modStatusPayments.Statics.PaymentArray[intCT].ReversalID = 0;
		//				modStatusPayments.Statics.PaymentArray[intCT].Year = intYear;
		//				modStatusPayments.Statics.PaymentArray[intCT].BudgetaryAccountNumber = strIntAcct;
		//				FillPaymentGrid(vsPayments.Rows, intCT, intYear);
		//			}
		//		}
		//	}
		//	if (PayCode == "L")
		//	{
		//		// Lien Record - Show PLI line
		//		for (intCT = 1; intCT <= modStatusPayments.MAX_PAYMENTS; intCT++)
		//		{
		//			// this finds a place in the array
		//			if (modStatusPayments.Statics.PaymentArray[intCT].Year == 0)
		//			{
		//				// to store the new payment record
		//				break;
		//			}
		//		}
		//		if (intCT < modStatusPayments.MAX_PAYMENTS)
		//		{
		//			if (txtWOPLI.Text != "" && Conversion.Val(txtWOPLI.Text) != 0)
		//			{
		//				if (FCConvert.ToDouble(txtWOPLI.Text) != 0)
		//				{
		//					//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
		//					modStatusPayments.Statics.PaymentArray[intCT] = new modStatusPayments.Payment(0);
		//					// this adds it to the array of payments
		//					modStatusPayments.Statics.PaymentArray[intCT].Account = lngAcct;
		//					modGlobalFunctions.AddCYAEntry_8("CL", "Non-Budgetary Pre-Lien Interest. RE Account: " + FCConvert.ToString(modStatusPayments.Statics.PaymentArray[intCT].Account));
		//					modStatusPayments.Statics.PaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//					modStatusPayments.Statics.PaymentArray[intCT].BillCode = PayCode;
		//					if (PayCode == "L")
		//					{
		//						modStatusPayments.Statics.PaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
		//					}
		//					else
		//					{
		//						modStatusPayments.Statics.PaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
		//					}
		//					txtReference.Text = "NB";
		//					modStatusPayments.Statics.PaymentArray[intCT].CashDrawer = "N";
		//					// allow the user to set it to yesterday
		//					modStatusPayments.Statics.PaymentArray[intCT].Code = "N";
		//					modStatusPayments.Statics.PaymentArray[intCT].ResCode = lngCLResCode;
		//					modStatusPayments.Statics.PaymentArray[intCT].Comments = frmRECLStatus.InstancePtr.txtComments.Text;
		//					modStatusPayments.Statics.PaymentArray[intCT].EffectiveInterestDate = modStatusPayments.Statics.EffectiveDate;
		//					modStatusPayments.Statics.PaymentArray[intCT].GeneralLedger = "N";
		//					modStatusPayments.Statics.PaymentArray[intCT].Period = Strings.Left(cmbPeriod.Items[cmbPeriod.SelectedIndex].ToString(), 1);
		//					if (txtWOPLI.Text != "" && Conversion.Val(txtWOPLI.Text) != 0)
		//					{
		//						modStatusPayments.Statics.PaymentArray[intCT].PreLienInterest = FCConvert.ToDecimal(txtWOPLI.Text);
		//					}
		//					else
		//					{
		//						modStatusPayments.Statics.PaymentArray[intCT].PreLienInterest = 0;
		//					}
		//					modStatusPayments.Statics.PaymentArray[intCT].Principal = 0;
		//					modStatusPayments.Statics.PaymentArray[intCT].CurrentInterest = 0;
		//					modStatusPayments.Statics.PaymentArray[intCT].LienCost = 0;
		//					modStatusPayments.Statics.PaymentArray[intCT].ReceiptNumber = "P";
		//					if (frmRECLStatus.InstancePtr.txtTransactionDate.Text != "")
		//					{
		//						if (Information.IsDate(frmRECLStatus.InstancePtr.txtTransactionDate.Text))
		//						{
		//							modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmRECLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//						}
		//						else
		//						{
		//							frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//							modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//						}
		//					}
		//					else
		//					{
		//						frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//						modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//					}
		//					modStatusPayments.Statics.PaymentArray[intCT].Reference = Strings.Trim(txtReference.Text + " ");
		//					modStatusPayments.Statics.PaymentArray[intCT].ReversalID = 0;
		//					modStatusPayments.Statics.PaymentArray[intCT].Year = intYear;
		//					modStatusPayments.Statics.PaymentArray[intCT].BudgetaryAccountNumber = strPLIAcct;
		//					FillPaymentGrid(vsPayments.Rows, intCT, intYear);
		//				}
		//			}
		//		}
		//	}
		//	for (intCT = 1; intCT <= modStatusPayments.MAX_PAYMENTS; intCT++)
		//	{
		//		// this finds a place in the array
		//		if (modStatusPayments.Statics.PaymentArray[intCT].Year == 0)
		//		{
		//			// to store the new payment record
		//			break;
		//		}
		//	}
		//	// Costs
		//	if (intCT < modStatusPayments.MAX_PAYMENTS)
		//	{
		//		if (txtWOCosts.Text != "" && Conversion.Val(txtWOCosts.Text) != 0)
		//		{
		//			if (FCConvert.ToDouble(txtWOCosts.Text) != 0)
		//			{
		//				//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
		//				modStatusPayments.Statics.PaymentArray[intCT] = new modStatusPayments.Payment(0);
		//				// this adds it to the array of payments
		//				modStatusPayments.Statics.PaymentArray[intCT].Account = lngAcct;
		//				modGlobalFunctions.AddCYAEntry_8("CL", "Non-Budgetary Costs. RE Account: " + FCConvert.ToString(modStatusPayments.Statics.PaymentArray[intCT].Account));
		//				modStatusPayments.Statics.PaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//				modStatusPayments.Statics.PaymentArray[intCT].BillCode = PayCode;
		//				if (PayCode == "L")
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
		//				}
		//				else
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
		//				}
		//				txtReference.Text = "NB";
		//				modStatusPayments.Statics.PaymentArray[intCT].CashDrawer = "N";
		//				// allow the user to set it to yesterday
		//				modStatusPayments.Statics.PaymentArray[intCT].Code = "N";
		//				modStatusPayments.Statics.PaymentArray[intCT].ResCode = lngCLResCode;
		//				modStatusPayments.Statics.PaymentArray[intCT].Comments = frmRECLStatus.InstancePtr.txtComments.Text;
		//				modStatusPayments.Statics.PaymentArray[intCT].EffectiveInterestDate = modStatusPayments.Statics.EffectiveDate;
		//				modStatusPayments.Statics.PaymentArray[intCT].GeneralLedger = "N";
		//				// cannot write this off
		//				modStatusPayments.Statics.PaymentArray[intCT].Period = Strings.Left(cmbPeriod.Items[cmbPeriod.SelectedIndex].ToString(), 1);
		//				modStatusPayments.Statics.PaymentArray[intCT].PreLienInterest = 0;
		//				modStatusPayments.Statics.PaymentArray[intCT].Principal = 0;
		//				modStatusPayments.Statics.PaymentArray[intCT].CurrentInterest = 0;
		//				if (txtWOCosts.Text != "" && Conversion.Val(txtWOCosts.Text) != 0)
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].LienCost = FCConvert.ToDecimal(txtWOCosts.Text);
		//				}
		//				else
		//				{
		//					modStatusPayments.Statics.PaymentArray[intCT].LienCost = 0;
		//				}
		//				modStatusPayments.Statics.PaymentArray[intCT].ReceiptNumber = "P";
		//				if (frmRECLStatus.InstancePtr.txtTransactionDate.Text != "")
		//				{
		//					if (Information.IsDate(frmRECLStatus.InstancePtr.txtTransactionDate.Text))
		//					{
		//						modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmRECLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//					}
		//					else
		//					{
		//						frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//						modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//					}
		//				}
		//				else
		//				{
		//					frmRECLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//					modStatusPayments.Statics.PaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//				}
		//				modStatusPayments.Statics.PaymentArray[intCT].Reference = Strings.Trim(txtReference.Text + " ");
		//				modStatusPayments.Statics.PaymentArray[intCT].ReversalID = 0;
		//				modStatusPayments.Statics.PaymentArray[intCT].Year = intYear;
		//				modStatusPayments.Statics.PaymentArray[intCT].BudgetaryAccountNumber = strCostAcct;
		//				FillPaymentGrid(vsPayments.Rows, intCT, intYear);
		//			}
		//		}
		//	}
		//	modStatusPayments.YearPending(intYear, 1);
		//}

		private void UpdateWOTotals()
		{
			// VBto upgrade warning: curPrincipal As double	OnWrite(double, short)
			decimal curPrincipal;
			// VBto upgrade warning: curInterest As double	OnWrite(double, short)
			decimal curInterest;
			// VBto upgrade warning: curPLI As double	OnWrite(double, short)
			decimal curPLI;
			// VBto upgrade warning: curCosts As double	OnWrite(double, short)
			decimal curCosts;
			if (txtWOPrincipal.Text != "" && Conversion.Val(txtWOPrincipal.Text) != 0)
			{
				curPrincipal = FCConvert.ToDecimal(txtWOPrincipal.Text);
			}
			else
			{
				curPrincipal = 0;
			}
			if (txtWOCurInterest.Text != "" && Conversion.Val(txtWOCurInterest.Text) != 0)
			{
				curInterest = FCConvert.ToDecimal(txtWOCurInterest.Text);
			}
			else
			{
				curInterest = 0;
			}
			if (txtWOPLI.Text != "" && Conversion.Val(txtWOPLI.Text) != 0)
			{
				curPLI = FCConvert.ToDecimal(txtWOPLI.Text);
			}
			else
			{
				curPLI = 0;
			}
			if (txtWOCosts.Text != "" && Conversion.Val(txtWOCosts.Text) != 0)
			{
				curCosts = FCConvert.ToDecimal(txtWOCosts.Text);
			}
			else
			{
				curCosts = 0;
			}
			lblWOTotal.Text = Strings.Format(curPrincipal + curInterest + curPLI + curCosts, "#,##0.00");
		}

		private void UpdateTextFormats()
		{
			txtWOPrincipal.Text = Strings.Format(txtWOPrincipal.Text, "#,##0.00");
			txtWOCurInterest.Text = Strings.Format(txtWOCurInterest.Text, "#,##0.00");
			txtWOPLI.Text = Strings.Format(txtWOPLI.Text, "#,##0.00");
			txtWOCosts.Text = Strings.Format(txtWOCosts.Text, "#,##0.00");
		}

		private bool WriteoffPaymentExists()
		{
			bool WriteoffPaymentExists = false;
			bool blnResult;
			int lngCurrent;
			blnResult = false;
			if (vsPayments.Rows > 1)
			{
				for (lngCurrent = 0; lngCurrent <= vsPayments.Rows - 1; lngCurrent++)
				{
					if (Strings.Left(vsPayments.TextMatrix(lngCurrent, lngColCode), 1) == "N")
					{
						// kgk 07-01-2011  .TextMatrix(.Row, lngColCode)  this is only checking the first row
						if (vsPayments.TextMatrix(lngCurrent, 0) == cmbYear.Items[cmbYear.SelectedIndex].ToString())
						{
							// kgk 07-01-2011  trocl-  Add check for same year
							blnResult = true;
							break;
						}
					}
				}
				// lngCurrent
			}
			WriteoffPaymentExists = blnResult;
			return WriteoffPaymentExists;
		}

		private bool ValidateRefundPayment()
		{
			bool ValidateRefundPayment = false;
			bool blnResult;
			double dblTotal;
			blnResult = true;
			// Optimistic
			// Get Values
			// find which year is being refunded
			intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(modExtraModules.FormatYear(Strings.Left(frmRECLStatus.InstancePtr.cmbYear.Items[frmRECLStatus.InstancePtr.cmbYear.SelectedIndex].ToString(), 6)))));
			// find the total due for this year
			if (modStatusPayments.Statics.boolRE)
			{
				rsDisc.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'RE'", modExtraModules.strCLDatabase);
				lngAccount = StaticSettings.TaxCollectionValues.LastAccountRE;
			}
			else
			{
				rsDisc.OpenRecordset("SELECT * FROM (" + modStatusPayments.Statics.strCurrentAccountBills + ") AS qCAB WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingYear = " + FCConvert.ToString(intYear) + " AND BillingType = 'PP'", modExtraModules.strCLDatabase);
				lngAccount = StaticSettings.TaxCollectionValues.LastAccountPP;
			}
			if (rsDisc.RecordCount() != 0)
			{
				if (((rsDisc.Get_Fields_Int32("LienRecordNumber"))) == 0)
				{
					// regular billing
					// Get Current Interest
					dblTempTotal = modCLCalculations.CalculateAccountCL(ref rsDisc, lngAccount, modStatusPayments.Statics.EffectiveDate, ref dblCurrInt);
					dblPrin = (Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue1")) + Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue2")) + Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue3")) + Conversion.Val(rsDisc.Get_Fields_Decimal("Taxdue4"))) - Conversion.Val(rsDisc.Get_Fields_Decimal("PrincipalPaid"));
					dblInt = (Conversion.Val(rsDisc.Get_Fields_Decimal("InterestCharged")) + Conversion.Val(rsDisc.Get_Fields_Decimal("InterestPaid"))) - dblCurrInt;
					dblPLI = 0;
					dblCosts = Conversion.Val(rsDisc.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsDisc.Get_Fields_Decimal("DemandFeesPaid"));
				}
				else
				{
					// lien
					LRN = FCConvert.ToInt32(rsDisc.Get_Fields_Int32("LienRecordNumber"));
					if (rsDisc.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(LRN), modExtraModules.strCLDatabase))
					{
						if (rsDisc.RecordCount() != 0)
						{
							dblTempTotal = modCLCalculations.CalculateAccountCLLien(rsDisc, modStatusPayments.Statics.EffectiveDate, ref dblCurrInt);
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							dblPrin = Conversion.Val(rsDisc.Get_Fields("Principal")) - Conversion.Val(rsDisc.Get_Fields_Decimal("PrincipalPaid"));
							dblInt = FCConvert.ToDouble(rsDisc.Get_Fields_Decimal("InterestCharged") - rsDisc.Get_Fields_Decimal("InterestPaid") * -1) - dblCurrInt;
							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
							dblPLI = Conversion.Val(rsDisc.Get_Fields("Interest")) - Conversion.Val(rsDisc.Get_Fields("PLIPaid"));
							// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
							dblCosts = Conversion.Val(rsDisc.Get_Fields("Costs")) - Conversion.Val(rsDisc.Get_Fields("MaturityFee")) - Conversion.Val(rsDisc.Get_Fields_Decimal("CostsPaid"));
						}
						else
						{
							// lien record not found
							FCMessageBox.Show("Lien record number, " + FCConvert.ToString(LRN) + ", not found.", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "ERROR");
							blnResult = false;
							goto LeaveFunction;
						}
					}
					else
					{
						FCMessageBox.Show("Error opening table LienRec.", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "ERROR");
						blnResult = false;
						goto LeaveFunction;
					}
				}
				dblTotal = dblPrin - dblInt + dblPLI + dblCosts;
				// Check for Zero Principal Balance
				if (dblPrin == 0)
				{
					FCMessageBox.Show("Principal Balance is zero. Refund can only be made on Principal.", MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, "Zero Balance");
					blnResult = false;
					goto LeaveFunction;
				}
				// Check for Balance Due
				if (dblPrin > 0)
				{
					FCMessageBox.Show("Credit Balance does not exist for this Bill.", MsgBoxStyle.OkOnly | MsgBoxStyle.Exclamation, "Balance Due");
					blnResult = false;
					goto LeaveFunction;
				}
			}
			else
			{
				blnResult = false;
				goto LeaveFunction;
			}
			LeaveFunction:
			;
			ValidateRefundPayment = blnResult;
			return ValidateRefundPayment;
		}

		private bool IsInTaxClub(int lngYear, int lngAcct)
		{
			bool IsInTaxClub = false;
			// Tracker Reference: 14033
			bool blnResult = false;
			clsDRWrapper rsTC = new clsDRWrapper();
			if (modStatusPayments.Statics.boolRE)
			{
				rsTC.OpenRecordset("SELECT * FROM TaxClub WHERE type = 'RE' and Account = " + FCConvert.ToString(lngAcct) + " AND [Year] = " + FCConvert.ToString(lngYear), modExtraModules.strCLDatabase);
			}
			else
			{
				rsTC.OpenRecordset("SELECT * FROM TaxClub WHERE type = 'PP' and Account = " + FCConvert.ToString(lngAcct) + " AND [Year] = " + FCConvert.ToString(lngYear), modExtraModules.strCLDatabase);
			}
			blnResult = rsTC.RecordCount() > 0;
			IsInTaxClub = blnResult;
			return IsInTaxClub;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuPaymentSave_Click(sender, e);
		}

		//private void cmdFileOptionsPrint_Click(object sender, EventArgs e)
		//{
		//	this.mnuFileOptionsPrint_Click(sender, e);
		//}

		//private void cmdProcessEffective_Click(object sender, EventArgs e)
		//{
		//	this.mnuProcessEffective_Click(sender, e);
		//}

		//private void cmdPaymentPreview_Click(object sender, EventArgs e)
		//{
		//	this.mnuPaymentPreview_Click(sender, e);
		//}

		private void cmdProcessChangeAccount_Click(object sender, EventArgs e)
		{
			this.mnuProcessChangeAccount_Click(sender, e);
		}

		private void btnSaveExit_Click(object sender, EventArgs e)
		{
			this.mnuPaymentSaveExit_Click(sender, e);
		}

		private void FillGroupInfo()
		{
			string strTemp;
			clsDRWrapper rsTemp = new clsDRWrapper();
			int lngAccount = 0;
			double dblTotal = 0;
			double dblTotalBalance = 0;
			int intBasisBak = 0;
			double dblOPIntRateBak = 0;
			//Backup copy of Basis and OverPay Int Rate - the same globals are used in CL and UT
			//but the values might not be the same
			intBasisBak = modGlobal.Statics.gintBasis;
			dblOPIntRateBak = modExtraModules.Statics.dblOverPayRate;
			lngGroupID = 0;
			strTemp = "";
			if (lngGroupNumber != 0)
			{
				//rsTemp.OpenRecordset "SELECT * FROM ModuleAssociation INNER JOIN GroupMaster ON ModuleAssociation.GroupNumber = GroupMaster.ID WHERE GroupMaster.GroupNumber = " & lngGroupNumber & " AND NOT (Module = 'RE' AND Account = " & CurrentAccountRE & ") ORDER BY Module", strGNDatabase
				rsTemp.OpenRecordset("SELECT GroupMaster.ID, Module, Account FROM ModuleAssociation INNER JOIN GroupMaster ON ModuleAssociation.GroupNumber = GroupMaster.ID WHERE GroupMaster.GroupNumber = " + lngGroupNumber + " ORDER BY Module", "CentralData");
				if (!rsTemp.EndOfFile())
				{
					lngGroupID = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
					while (!rsTemp.EndOfFile())
					{
						if (strTemp == "")
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							strTemp = "Group Balance (" + rsTemp.Get_Fields_String("Module") + " #" + rsTemp.Get_Fields("Account");
						}
						else
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							strTemp = strTemp + ", " + rsTemp.Get_Fields_String("Module") + " #" + rsTemp.Get_Fields("Account");
						}
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						lngAccount = FCConvert.ToInt32(rsTemp.Get_Fields("Account"));
						if (FCConvert.ToString(rsTemp.Get_Fields_String("Module")) == "RE")
						{
							modGlobal.Statics.gintBasis = intBasisBak;
							modExtraModules.Statics.dblOverPayRate = dblOPIntRateBak;
							dblTotal = modCLCalculations.CalculateAccountTotal(lngAccount, true);
						}
						else if (FCConvert.ToString(rsTemp.Get_Fields_String("Module")) == "PP")
						{
							modGlobal.Statics.gintBasis = intBasisBak;
							modExtraModules.Statics.dblOverPayRate = dblOPIntRateBak;
							dblTotal = modCLCalculations.CalculateAccountTotal(lngAccount, false);
						}
						else if (FCConvert.ToString(rsTemp.Get_Fields_String("Module")) == "UT")
						{
							modUTCalculations.UTCalcSetup();
							dblTotal = modUTCalculations.CalculateAccountUTTotal(modUTFunctions.GetAccountKeyUT(ref lngAccount), true) + modUTCalculations.CalculateAccountUTTotal(modUTFunctions.GetAccountKeyUT(ref lngAccount), false);
						}
						dblTotalBalance = dblTotalBalance + dblTotal;
						rsTemp.MoveNext();
					}
				}
			}
			if (strTemp != "")
			{
				strTemp = strTemp + ") is " + Strings.Format(dblTotalBalance, "#,##0.00");
				if (boolPayment)
				{
					lblGrpInfo_Pmt.ToolTipText = strTemp;
					lblGrpInfo_Pmt.Visible = true;
					lblGrpInfo.Visible = false;
				}
				else
				{
					lblGrpInfo.ToolTipText = strTemp;
					lblGrpInfo.Visible = true;
					lblGrpInfo_Pmt.Visible = false;
				}
			}
			else
			{
				lblGrpInfo_Pmt.Visible = false;
				lblGrpInfo.Visible = false;
			}
			//Restore the original Basis and OverPay Int Rate
			modGlobal.Statics.gintBasis = intBasisBak;
			modExtraModules.Statics.dblOverPayRate = dblOPIntRateBak;
		}

        private void imgNote_Click(object sender, EventArgs e)
        {
            imgNote.Visible = true;   
            // Reset ImageSource because click on Image was causing image to disappear.
            this.imgNote.ImageSource = "imgnote?color=#707884";
        }

        private void lblTaxClub_Click(object sender, EventArgs e)
        {

        }
    }
}
