﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cVendor
	{
		//=========================================================
		private int lngRecordID;
		private int lngVendorNumber;
		private string strCheckName = string.Empty;
		private string strCheckAddress1 = string.Empty;
		private string strCheckAddress2 = string.Empty;
		private string strCheckAddress3 = string.Empty;
		private string strCheckAddress4 = string.Empty;
		private string strEmail = string.Empty;
		private bool boolUpdated;
		private bool boolDeleted;
		private string strStatus = string.Empty;
		private bool bool1099;
		private string strCheckCity = string.Empty;
		private string strCheckZip = string.Empty;
		private string strCheckZip4 = string.Empty;
		private string strCheckState = string.Empty;

		public string CheckState
		{
			set
			{
				strCheckState = value;
				IsUpdated = true;
			}
			get
			{
				string CheckState = "";
				CheckState = strCheckState;
				return CheckState;
			}
		}

		public string CheckCity
		{
			set
			{
				strCheckCity = value;
				IsUpdated = true;
			}
			get
			{
				string CheckCity = "";
				CheckCity = strCheckCity;
				return CheckCity;
			}
		}

		public string CheckZip
		{
			set
			{
				strCheckZip = value;
				IsUpdated = true;
			}
			get
			{
				string CheckZip = "";
				CheckZip = strCheckZip;
				return CheckZip;
			}
		}

		public string CheckZip4
		{
			set
			{
				strCheckZip4 = value;
				IsUpdated = true;
			}
			get
			{
				string CheckZip4 = "";
				CheckZip4 = strCheckZip4;
				return CheckZip4;
			}
		}

		public bool Ten99
		{
			set
			{
				bool1099 = value;
				IsUpdated = true;
			}
			get
			{
				bool Ten99 = false;
				Ten99 = bool1099;
				return Ten99;
			}
		}

		public string Status
		{
			set
			{
				strStatus = value;
				IsUpdated = true;
			}
			get
			{
				string Status = "";
				Status = strStatus;
				return Status;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
				IsUpdated = true;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int VendorNumber
		{
			set
			{
				lngVendorNumber = value;
				IsUpdated = true;
			}
			get
			{
				int VendorNumber = 0;
				VendorNumber = lngVendorNumber;
				return VendorNumber;
			}
		}

		public string CheckName
		{
			set
			{
				strCheckName = value;
				IsUpdated = true;
			}
			get
			{
				string CheckName = "";
				CheckName = strCheckName;
				return CheckName;
			}
		}

		public string CheckAddress1
		{
			set
			{
				strCheckAddress1 = value;
				IsUpdated = true;
			}
			get
			{
				string CheckAddress1 = "";
				CheckAddress1 = strCheckAddress1;
				return CheckAddress1;
			}
		}

		public string CheckAddress2
		{
			set
			{
				strCheckAddress2 = value;
				IsUpdated = true;
			}
			get
			{
				string CheckAddress2 = "";
				CheckAddress2 = strCheckAddress2;
				return CheckAddress2;
			}
		}

		public string CheckAddress3
		{
			set
			{
				strCheckAddress3 = value;
				IsUpdated = true;
			}
			get
			{
				string CheckAddress3 = "";
				CheckAddress3 = strCheckAddress3;
				return CheckAddress3;
			}
		}

		public string CheckAddress4
		{
			set
			{
				strCheckAddress4 = value;
				IsUpdated = true;
			}
			get
			{
				string CheckAddress4 = "";
				CheckAddress4 = strCheckAddress4;
				return CheckAddress4;
			}
		}

		public string EMail
		{
			set
			{
				strEmail = value;
				IsUpdated = true;
			}
			get
			{
				string EMail = "";
				EMail = strEmail;
				return EMail;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}
	}
}
