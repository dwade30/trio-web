﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using Global;
using fecherFoundation;

namespace Global
{
	/// <summary>
	/// Summary description for frmSelectCustomCheckFormat.
	/// </summary>
	partial class frmSelectCustomCheckFormat : BaseForm
	{
		public fecherFoundation.FCComboBox cmbNonNegotiable;
		public fecherFoundation.FCComboBox cboCheckSelection;
		public fecherFoundation.FCLabel lblNegotiable;
		public fecherFoundation.FCLabel lblNonNegotiable;
		//private Wisej.Web.MainMenu MainMenu1;
		//public fecherFoundation.FCToolStripMenuItem mnuProcess;
		//public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		//public fecherFoundation.FCToolStripMenuItem Seperator;
		//public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSelectCustomCheckFormat));
			this.cmbNonNegotiable = new fecherFoundation.FCComboBox();
			this.cboCheckSelection = new fecherFoundation.FCComboBox();
			this.lblNegotiable = new fecherFoundation.FCLabel();
			this.lblNonNegotiable = new fecherFoundation.FCLabel();
			this.cmdProcessQuit = new Wisej.Web.Button();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessQuit);
			this.BottomPanel.Location = new System.Drawing.Point(0, 193);
			this.BottomPanel.Size = new System.Drawing.Size(527, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbNonNegotiable);
			this.ClientArea.Controls.Add(this.cboCheckSelection);
			this.ClientArea.Controls.Add(this.lblNegotiable);
			this.ClientArea.Controls.Add(this.lblNonNegotiable);
			this.ClientArea.Size = new System.Drawing.Size(527, 133);
			this.ClientArea.TabIndex = 0;
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(527, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(334, 30);
			this.HeaderText.Text = "Select Custom Check Format";
			// 
			// cmbNonNegotiable
			// 
			this.cmbNonNegotiable.AutoSize = false;
			this.cmbNonNegotiable.BackColor = System.Drawing.SystemColors.Window;
			this.cmbNonNegotiable.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbNonNegotiable.FormattingEnabled = true;
			this.cmbNonNegotiable.Location = new System.Drawing.Point(198, 90);
			this.cmbNonNegotiable.Name = "cmbNonNegotiable";
			this.cmbNonNegotiable.Size = new System.Drawing.Size(285, 40);
			this.cmbNonNegotiable.TabIndex = 3;
			this.cmbNonNegotiable.Visible = false;
			// 
			// cboCheckSelection
			// 
			this.cboCheckSelection.AutoSize = false;
			this.cboCheckSelection.BackColor = System.Drawing.SystemColors.Window;
			this.cboCheckSelection.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboCheckSelection.FormattingEnabled = true;
			this.cboCheckSelection.Location = new System.Drawing.Point(198, 30);
			this.cboCheckSelection.Name = "cboCheckSelection";
			this.cboCheckSelection.Size = new System.Drawing.Size(285, 40);
			this.cboCheckSelection.TabIndex = 1;
			this.cboCheckSelection.Text = "Combo1";
			// 
			// lblNegotiable
			// 
			this.lblNegotiable.Location = new System.Drawing.Point(20, 44);
			this.lblNegotiable.Name = "lblNegotiable";
			this.lblNegotiable.Size = new System.Drawing.Size(88, 24);
			this.lblNegotiable.TabIndex = 0;
			this.lblNegotiable.Text = "NEGOTIABLE";
			this.lblNegotiable.Visible = false;
			// 
			// lblNonNegotiable
			// 
			this.lblNonNegotiable.Location = new System.Drawing.Point(20, 104);
			this.lblNonNegotiable.Name = "lblNonNegotiable";
			this.lblNonNegotiable.Size = new System.Drawing.Size(109, 24);
			this.lblNonNegotiable.TabIndex = 2;
			this.lblNonNegotiable.Text = "NON-NEGOTIABLE";
			this.lblNonNegotiable.Visible = false;
			// 
			// cmdProcessQuit
			// 
			this.cmdProcessQuit.AppearanceKey = "acceptButton";
			this.cmdProcessQuit.Location = new System.Drawing.Point(209, 30);
			this.cmdProcessQuit.Name = "cmdProcessQuit";
			this.cmdProcessQuit.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessQuit.Size = new System.Drawing.Size(119, 48);
			this.cmdProcessQuit.TabIndex = 4;
			this.cmdProcessQuit.Text = "Process";
			this.cmdProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmSelectCustomCheckFormat
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(527, 301);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSelectCustomCheckFormat";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Select Custom Check Format";
			this.Load += new System.EventHandler(this.frmSelectCustomCheckFormat_Load);
			this.Activated += new System.EventHandler(this.frmSelectCustomCheckFormat_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSelectCustomCheckFormat_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Button cmdProcessQuit;
	}
}
