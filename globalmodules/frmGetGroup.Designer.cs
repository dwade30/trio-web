//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

#if TWPP0000
using TWPP0000;
using modGlobalVariables = TWPP0000.modSecurity;


#elif TWBL0000
using TWBL0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmGetGroup.
	/// </summary>
	partial class frmGetGroup : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSearchBy;
		public fecherFoundation.FCLabel lblSearchBy;
		public fecherFoundation.FCComboBox cmbContain;
		public fecherFoundation.FCLabel lblContain;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> optModule;
		public fecherFoundation.FCComboBox cmbGroupNumber;
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox optModule_2;
		public fecherFoundation.FCCheckBox optModule_1;
		public fecherFoundation.FCCheckBox optModule_0;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuClearSearch;
		public fecherFoundation.FCToolStripMenuItem mnuSearch;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private System.ComponentModel.IContainer components;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetGroup));
            this.cmbSearchBy = new fecherFoundation.FCComboBox();
            this.lblSearchBy = new fecherFoundation.FCLabel();
            this.cmbContain = new fecherFoundation.FCComboBox();
            this.lblContain = new fecherFoundation.FCLabel();
            this.cmbGroupNumber = new fecherFoundation.FCComboBox();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.txtSearch = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.optModule_2 = new fecherFoundation.FCCheckBox();
            this.optModule_1 = new fecherFoundation.FCCheckBox();
            this.optModule_0 = new fecherFoundation.FCCheckBox();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.cmdClear = new fecherFoundation.FCButton();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClearSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdNewGroup = new fecherFoundation.FCButton();
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optModule_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optModule_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.optModule_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 599);
            this.BottomPanel.Size = new System.Drawing.Size(731, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbGroupNumber);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(731, 539);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdNewGroup);
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Controls.Add(this.cmdSearch);
            this.TopPanel.Size = new System.Drawing.Size(731, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNewGroup, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(228, 30);
            this.HeaderText.Text = "Group Maintenance";
            // 
            // cmbSearchBy
            // 
            this.cmbSearchBy.AutoSize = false;
            this.cmbSearchBy.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbSearchBy.FormattingEnabled = true;
            this.cmbSearchBy.Items.AddRange(new object[] {
            "Name",
            "Account",
            "Location",
            "Map / Lot"});
            this.cmbSearchBy.Location = new System.Drawing.Point(442, 43);
            this.cmbSearchBy.Name = "cmbSearchBy";
            this.cmbSearchBy.Size = new System.Drawing.Size(166, 40);
            this.cmbSearchBy.TabIndex = 2;
            this.cmbSearchBy.Text = "Name";
            // 
            // lblSearchBy
            // 
            this.lblSearchBy.AutoSize = true;
            this.lblSearchBy.Location = new System.Drawing.Point(260, 57);
            this.lblSearchBy.Name = "lblSearchBy";
            this.lblSearchBy.Size = new System.Drawing.Size(85, 16);
            this.lblSearchBy.TabIndex = 1;
            this.lblSearchBy.Text = "SEARCH BY";
            // 
            // cmbContain
            // 
            this.cmbContain.AutoSize = false;
            this.cmbContain.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbContain.FormattingEnabled = true;
            this.cmbContain.Items.AddRange(new object[] {
            "Contain",
            "Start With",
            "End With"});
            this.cmbContain.Location = new System.Drawing.Point(442, 103);
            this.cmbContain.Name = "cmbContain";
            this.cmbContain.Size = new System.Drawing.Size(166, 40);
            this.cmbContain.TabIndex = 4;
            this.cmbContain.Text = "Start With";
            // 
            // lblContain
            // 
            this.lblContain.AutoSize = true;
            this.lblContain.Location = new System.Drawing.Point(260, 117);
            this.lblContain.Name = "lblContain";
            this.lblContain.Size = new System.Drawing.Size(176, 16);
            this.lblContain.TabIndex = 3;
            this.lblContain.Text = "FIND ALL RECORDS THAT";
            // 
            // cmbGroupNumber
            // 
            this.cmbGroupNumber.AutoSize = false;
            this.cmbGroupNumber.BackColor = System.Drawing.SystemColors.Window;
            this.cmbGroupNumber.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbGroupNumber.FormattingEnabled = true;
            this.cmbGroupNumber.Location = new System.Drawing.Point(136, 30);
            this.cmbGroupNumber.Name = "cmbGroupNumber";
            this.cmbGroupNumber.SelectedIndexChanged += cmbGroupNumber_SelectedIndexChanged;
            this.cmbGroupNumber.Size = new System.Drawing.Size(281, 40);
            this.cmbGroupNumber.TabIndex = 1;
            // 
            // Frame3
            // 
            this.Frame3.AppearanceKey = "groupBoxLeftBorder";
            this.Frame3.Controls.Add(this.Frame5);
            this.Frame3.Controls.Add(this.cmbSearchBy);
            this.Frame3.Controls.Add(this.lblSearchBy);
            this.Frame3.Controls.Add(this.cmbContain);
            this.Frame3.Controls.Add(this.lblContain);
            this.Frame3.Controls.Add(this.Frame1);
            this.Frame3.Location = new System.Drawing.Point(30, 90);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(673, 427);
            this.Frame3.TabIndex = 2;
            this.Frame3.Text = "Search";
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.txtSearch);
            this.Frame5.Controls.Add(this.Label2);
            this.Frame5.Location = new System.Drawing.Point(20, 232);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(275, 141);
            this.Frame5.TabIndex = 5;
            this.Frame5.Text = "Find All";
            // 
            // txtSearch
            // 
            this.txtSearch.AutoSize = false;
            this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearch.LinkItem = null;
            this.txtSearch.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtSearch.LinkTopic = null;
            this.txtSearch.Location = new System.Drawing.Point(20, 66);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(229, 40);
            this.txtSearch.TabIndex = 1;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(215, 26);
            this.Label2.TabIndex = 0;
            this.Label2.Text = "ENTER CRITERIA TO SEARCH BY";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.optModule_2);
            this.Frame1.Controls.Add(this.optModule_1);
            this.Frame1.Controls.Add(this.optModule_0);
            this.Frame1.Location = new System.Drawing.Point(20, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(198, 173);
            this.Frame1.TabIndex = 0;
            this.Frame1.Text = "Search In";
            // 
            // optModule_2
            // 
            this.optModule_2.Location = new System.Drawing.Point(20, 124);
            this.optModule_2.Name = "optModule_2";
            this.optModule_2.Size = new System.Drawing.Size(98, 23);
            this.optModule_2.TabIndex = 2;
            this.optModule_2.Text = "Utility Billing";
            this.optModule_2.CheckedChanged += new System.EventHandler(this.optModule_CheckedChanged);
            // 
            // optModule_1
            // 
            this.optModule_1.Location = new System.Drawing.Point(20, 77);
            this.optModule_1.Name = "optModule_1";
            this.optModule_1.Size = new System.Drawing.Size(133, 23);
            this.optModule_1.TabIndex = 1;
            this.optModule_1.Text = "Personal Property";
            this.optModule_1.CheckedChanged += new System.EventHandler(this.optModule_CheckedChanged);
            // 
            // optModule_0
            // 
            this.optModule_0.Checked = true;
            this.optModule_0.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.optModule_0.Location = new System.Drawing.Point(20, 30);
            this.optModule_0.Name = "optModule_0";
            this.optModule_0.Size = new System.Drawing.Size(95, 23);
            this.optModule_0.TabIndex = 0;
            this.optModule_0.Text = "Real Estate";
            this.optModule_0.CheckedChanged += new System.EventHandler(this.optModule_CheckedChanged);
            // 
            // cmdSearch
            // 
            this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSearch.AppearanceKey = "toolbarButton";
            this.cmdSearch.ImageSource = "button-search";
            this.cmdSearch.Location = new System.Drawing.Point(644, 29);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(81, 24);
            this.cmdSearch.TabIndex = 4;
            this.cmdSearch.Text = "Search";
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.AppearanceKey = "toolbarButton";
            this.cmdClear.Location = new System.Drawing.Point(544, 29);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(94, 24);
            this.cmdClear.TabIndex = 3;
            this.cmdClear.Text = "Clear Search";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(30, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(425, 19);
            this.Label3.TabIndex = 0;
            this.Label3.Text = "GET GROUP AND ASSOCIATION INFORMATION";
            this.Label3.Visible = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(114, 22);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "GROUP";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuNew,
            this.mnuSepar2,
            this.mnuClearSearch,
            this.mnuSearch,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuNew
            // 
            this.mnuNew.Index = 0;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Text = "New Group";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuClearSearch
            // 
            this.mnuClearSearch.Index = 2;
            this.mnuClearSearch.Name = "mnuClearSearch";
            this.mnuClearSearch.Text = "Clear Search";
            this.mnuClearSearch.Click += new System.EventHandler(this.mnuClearSearch_Click);
            // 
            // mnuSearch
            // 
            this.mnuSearch.Index = 3;
            this.mnuSearch.Name = "mnuSearch";
            this.mnuSearch.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSearch.Text = "Search";
            this.mnuSearch.Click += new System.EventHandler(this.mnuSearch_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 4;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 5;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdNewGroup
            // 
            this.cmdNewGroup.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNewGroup.AppearanceKey = "toolbarButton";
            this.cmdNewGroup.Location = new System.Drawing.Point(450, 29);
            this.cmdNewGroup.Name = "cmdNewGroup";
            this.cmdNewGroup.Size = new System.Drawing.Size(88, 24);
            this.cmdNewGroup.TabIndex = 5;
            this.cmdNewGroup.Text = "New Group";
            this.cmdNewGroup.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(261, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(114, 48);
            this.cmdProcess.TabIndex = 5;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.cmbGroupNumber_SelectedIndexChanged);
            // 
            // frmGetGroup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(731, 707);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmGetGroup";
            this.ShowInTaskbar = false;
            this.Text = "Group Maintenance";
            this.Load += new System.EventHandler(this.frmGetGroup_Load);
            this.Activated += new System.EventHandler(this.frmGetGroup_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetGroup_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetGroup_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.optModule_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optModule_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.optModule_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}

        private void CmbGroupNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
        #endregion

        private FCButton cmdNewGroup;
		public FCButton cmdProcess;
	}
}