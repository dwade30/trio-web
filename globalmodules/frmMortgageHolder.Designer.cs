﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmMortgageHolder.
	/// </summary>
	partial class frmMortgageHolder : BaseForm
	{
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkRcvCopies;
		public fecherFoundation.FCFrame fraMortContact;
		public fecherFoundation.FCTextBox txtContact;
		public fecherFoundation.FCTextBox txtPhone;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel lblContact;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtAddress3;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame framHoldersList;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCGrid HelpGrid;
		public fecherFoundation.FCGrid Grid;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel lblAccountType;
		public fecherFoundation.FCLabel lblMortgageHolder;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCButton cmdFileDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuChangeNumber;
		public fecherFoundation.FCToolStripMenuItem mnuMortgageHolderLabel;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteMort;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrintMortHolderInfo;
		public fecherFoundation.FCToolStripMenuItem mnuPop;
		public fecherFoundation.FCToolStripMenuItem mnuAdd;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuCancel;
		public fecherFoundation.FCToolStripMenuItem mnuHelpPop;
		public fecherFoundation.FCToolStripMenuItem mnuHelpPopEdit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar4;
		public fecherFoundation.FCToolStripMenuItem mnuHelpPopCancel;
		//private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMortgageHolder));
            this.Frame1 = new fecherFoundation.FCFrame();
            this.lblLocation = new fecherFoundation.FCLabel();
            this.lblLocationLabel = new fecherFoundation.FCLabel();
            this.chkRcvCopies = new fecherFoundation.FCCheckBox();
            this.fraMortContact = new fecherFoundation.FCFrame();
            this.txtContact = new fecherFoundation.FCTextBox();
            this.txtPhone = new fecherFoundation.FCTextBox();
            this.Label9 = new fecherFoundation.FCLabel();
            this.lblContact = new fecherFoundation.FCLabel();
            this.txtZip4 = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtAddress3 = new fecherFoundation.FCTextBox();
            this.txtAddress2 = new fecherFoundation.FCTextBox();
            this.txtAddress1 = new fecherFoundation.FCTextBox();
            this.txtName = new fecherFoundation.FCTextBox();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblAccountType = new fecherFoundation.FCLabel();
            this.framHoldersList = new fecherFoundation.FCFrame();
            this.HelpGrid = new fecherFoundation.FCGrid();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.Grid = new fecherFoundation.FCGrid();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.lblMortgageHolder = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuChangeNumber = new fecherFoundation.FCToolStripMenuItem();
            this.mnuMortgageHolderLabel = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteMort = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrintMortHolderInfo = new fecherFoundation.FCToolStripMenuItem();
            this.cmdFileDeleteRow = new fecherFoundation.FCButton();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPop = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCancel = new fecherFoundation.FCToolStripMenuItem();
            this.mnuHelpPop = new fecherFoundation.FCToolStripMenuItem();
            this.mnuHelpPopEdit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar4 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuHelpPopCancel = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdMortHolderList = new fecherFoundation.FCButton();
            this.cmdFileAdd = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRcvCopies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMortContact)).BeginInit();
            this.fraMortContact.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framHoldersList)).BeginInit();
            this.framHoldersList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HelpGrid)).BeginInit();
            this.HelpGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileDeleteRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMortHolderList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileAdd)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 692);
            this.BottomPanel.Size = new System.Drawing.Size(919, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.framHoldersList);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Grid);
            this.ClientArea.Controls.Add(this.lblAccountType);
            this.ClientArea.Controls.Add(this.lblMortgageHolder);
            this.ClientArea.Size = new System.Drawing.Size(939, 725);
            this.ClientArea.Controls.SetChildIndex(this.lblMortgageHolder, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblAccountType, 0);
            this.ClientArea.Controls.SetChildIndex(this.Grid, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.framHoldersList, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdMortHolderList);
            this.TopPanel.Controls.Add(this.cmdFileAdd);
            this.TopPanel.Controls.Add(this.cmdFileDeleteRow);
            this.TopPanel.Controls.Add(this.lblAccount);
            this.TopPanel.Size = new System.Drawing.Size(939, 60);
            this.TopPanel.Controls.SetChildIndex(this.lblAccount, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileDeleteRow, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileAdd, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdMortHolderList, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.AutoSize = false;
            this.HeaderText.Size = new System.Drawing.Size(221, 30);
            this.HeaderText.Text = "Mortgage Holder";
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.lblLocation);
            this.Frame1.Controls.Add(this.lblLocationLabel);
            this.Frame1.Controls.Add(this.chkRcvCopies);
            this.Frame1.Controls.Add(this.fraMortContact);
            this.Frame1.Controls.Add(this.txtZip4);
            this.Frame1.Controls.Add(this.txtZip);
            this.Frame1.Controls.Add(this.txtState);
            this.Frame1.Controls.Add(this.txtCity);
            this.Frame1.Controls.Add(this.txtAddress3);
            this.Frame1.Controls.Add(this.txtAddress2);
            this.Frame1.Controls.Add(this.txtAddress1);
            this.Frame1.Controls.Add(this.txtName);
            this.Frame1.Controls.Add(this.Label8);
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(939, 312);
            this.Frame1.TabIndex = 14;
            this.Frame1.UseMnemonic = false;
            // 
            // lblLocation
            // 
            this.lblLocation.Location = new System.Drawing.Point(30, 274);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(414, 40);
            this.lblLocation.TabIndex = 32;
            this.lblLocation.Text = "LOCATION";
            this.lblLocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLocationLabel
            // 
            this.lblLocationLabel.AutoSize = true;
            this.lblLocationLabel.Location = new System.Drawing.Point(120, 284);
            this.lblLocationLabel.Name = "lblLocationLabel";
            this.lblLocationLabel.Size = new System.Drawing.Size(4, 15);
            this.lblLocationLabel.TabIndex = 31;
            this.lblLocationLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkRcvCopies
            // 
            this.chkRcvCopies.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRcvCopies.Location = new System.Drawing.Point(549, 178);
            this.chkRcvCopies.Name = "chkRcvCopies";
            this.chkRcvCopies.Size = new System.Drawing.Size(126, 22);
            this.chkRcvCopies.TabIndex = 29;
            this.chkRcvCopies.Text = "Receive Copies:";
            // 
            // fraMortContact
            // 
            this.fraMortContact.Controls.Add(this.txtContact);
            this.fraMortContact.Controls.Add(this.txtPhone);
            this.fraMortContact.Controls.Add(this.Label9);
            this.fraMortContact.Controls.Add(this.lblContact);
            this.fraMortContact.Location = new System.Drawing.Point(549, 20);
            this.fraMortContact.Name = "fraMortContact";
            this.fraMortContact.Size = new System.Drawing.Size(365, 138);
            this.fraMortContact.TabIndex = 26;
            this.fraMortContact.Text = "Contact Information";
            this.fraMortContact.UseMnemonic = false;
            // 
            // txtContact
            // 
            this.txtContact.BackColor = System.Drawing.SystemColors.Window;
            this.txtContact.Location = new System.Drawing.Point(150, 30);
            this.txtContact.MaxLength = 20;
            this.txtContact.Name = "txtContact";
            this.txtContact.Size = new System.Drawing.Size(195, 40);
            this.txtContact.TabIndex = 9;
            this.txtContact.TextChanged += new System.EventHandler(this.txtContact_TextChanged);
            // 
            // txtPhone
            // 
            this.txtPhone.BackColor = System.Drawing.SystemColors.Window;
            this.txtPhone.Location = new System.Drawing.Point(150, 80);
            this.txtPhone.MaxLength = 20;
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(195, 40);
            this.txtPhone.TabIndex = 10;
            this.txtPhone.TextChanged += new System.EventHandler(this.txtPhone_TextChanged);
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(20, 94);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(116, 15);
            this.Label9.TabIndex = 28;
            this.Label9.Text = "CONTACT PHONE";
            this.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblContact
            // 
            this.lblContact.AutoSize = true;
            this.lblContact.Location = new System.Drawing.Point(20, 44);
            this.lblContact.Name = "lblContact";
            this.lblContact.Size = new System.Drawing.Size(108, 15);
            this.lblContact.TabIndex = 27;
            this.lblContact.Text = "CONTACT NAME";
            this.lblContact.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtZip4
            // 
            this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip4.Location = new System.Drawing.Point(849, 220);
            this.txtZip4.MaxLength = 4;
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Size = new System.Drawing.Size(65, 40);
            this.txtZip4.TabIndex = 8;
            this.txtZip4.TextChanged += new System.EventHandler(this.txtZip4_TextChanged);
            this.txtZip4.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtZip4_KeyPress);
            // 
            // txtZip
            // 
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.Location = new System.Drawing.Point(749, 220);
            this.txtZip.MaxLength = 5;
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(74, 40);
            this.txtZip.TabIndex = 7;
            this.txtZip.TextChanged += new System.EventHandler(this.txtZip_TextChanged);
            this.txtZip.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtZip_KeyPress);
            // 
            // txtState
            // 
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtState.Location = new System.Drawing.Point(615, 220);
            this.txtState.MaxLength = 2;
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(60, 40);
            this.txtState.TabIndex = 6;
            this.txtState.TextChanged += new System.EventHandler(this.txtState_TextChanged);
            this.txtState.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtState_KeyPress);
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.Location = new System.Drawing.Point(115, 220);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(414, 40);
            this.txtCity.TabIndex = 5;
            this.txtCity.TextChanged += new System.EventHandler(this.txtCity_TextChanged);
            this.txtCity.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCity_KeyPress);
            // 
            // txtAddress3
            // 
            this.txtAddress3.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress3.Location = new System.Drawing.Point(115, 170);
            this.txtAddress3.Name = "txtAddress3";
            this.txtAddress3.Size = new System.Drawing.Size(414, 40);
            this.txtAddress3.TabIndex = 4;
            this.txtAddress3.TextChanged += new System.EventHandler(this.txtAddress3_TextChanged);
            // 
            // txtAddress2
            // 
            this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress2.Location = new System.Drawing.Point(115, 120);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(414, 40);
            this.txtAddress2.TabIndex = 3;
            this.txtAddress2.TextChanged += new System.EventHandler(this.txtAddress2_TextChanged);
            // 
            // txtAddress1
            // 
            this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress1.Location = new System.Drawing.Point(115, 70);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(414, 40);
            this.txtAddress1.TabIndex = 2;
            this.txtAddress1.TextChanged += new System.EventHandler(this.txtAddress1_TextChanged);
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.Location = new System.Drawing.Point(115, 20);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(414, 40);
            this.txtName.TabIndex = 1;
            this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(703, 234);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(24, 15);
            this.Label8.TabIndex = 25;
            this.Label8.Text = "ZIP";
            this.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(549, 234);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(45, 15);
            this.Label7.TabIndex = 24;
            this.Label7.Text = "STATE";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(834, 234);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(8, 15);
            this.Label6.TabIndex = 23;
            this.Label6.Text = "-";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(30, 234);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(33, 15);
            this.Label5.TabIndex = 22;
            this.Label5.Text = "CITY";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(30, 184);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(66, 15);
            this.Label4.TabIndex = 21;
            this.Label4.Text = "ADDRESS";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(30, 134);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(66, 15);
            this.Label3.TabIndex = 17;
            this.Label3.Text = "ADDRESS";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(30, 84);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(66, 15);
            this.Label2.TabIndex = 16;
            this.Label2.Text = "ADDRESS";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(30, 34);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(41, 15);
            this.Label1.TabIndex = 15;
            this.Label1.Text = "NAME";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAccountType
            // 
            this.lblAccountType.AutoSize = true;
            this.lblAccountType.Location = new System.Drawing.Point(30, 30);
            this.lblAccountType.Name = "lblAccountType";
            this.lblAccountType.Size = new System.Drawing.Size(137, 15);
            this.lblAccountType.TabIndex = 12;
            this.lblAccountType.Text = "MORTGAGE HOLDER";
            this.lblAccountType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblAccountType.Visible = false;
            // 
            // framHoldersList
            // 
            this.framHoldersList.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.framHoldersList.AppearanceKey = "groupBoxNoBorders";
            this.framHoldersList.BackColor = System.Drawing.Color.FromName("@window");
            this.framHoldersList.Controls.Add(this.HelpGrid);
            this.framHoldersList.Location = new System.Drawing.Point(30, 306);
            this.framHoldersList.Name = "framHoldersList";
            this.framHoldersList.Size = new System.Drawing.Size(880, 386);
            this.framHoldersList.TabIndex = 20;
            this.framHoldersList.UseMnemonic = false;
            this.framHoldersList.Visible = false;
            // 
            // HelpGrid
            // 
            this.HelpGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.HelpGrid.Controls.Add(this.cmdCancel);
            this.HelpGrid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.HelpGrid.ExtendLastCol = true;
            this.HelpGrid.FixedCols = 0;
            this.HelpGrid.Name = "HelpGrid";
            this.HelpGrid.RowHeadersVisible = false;
            this.HelpGrid.Rows = 1;
            this.HelpGrid.ShowFocusCell = false;
            this.HelpGrid.Size = new System.Drawing.Size(1560, 509);
            this.HelpGrid.TabIndex = 13;
            this.HelpGrid.TabStop = false;
            this.HelpGrid.DoubleClick += new System.EventHandler(this.HelpGrid_DblClick);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Location = new System.Drawing.Point(420, 243);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(90, 32);
            this.cmdCancel.TabIndex = 1;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Visible = false;
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // Grid
            // 
            this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Grid.Cols = 5;
            this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnKeystrokeOrF2;
            this.Grid.ExtendLastCol = true;
            this.Grid.FixedCols = 0;
            this.Grid.Location = new System.Drawing.Point(30, 306);
            this.Grid.Name = "Grid";
            this.Grid.RowHeadersVisible = false;
            this.Grid.Rows = 50;
            this.Grid.ShowFocusCell = false;
            this.Grid.Size = new System.Drawing.Size(880, 386);
            this.Grid.TabIndex = 11;
            this.Grid.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.Grid_CellValueChanged);
            this.Grid.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.Grid_CellFormatting);
            this.Grid.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.Grid_BeforeEdit);
            this.Grid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.Grid_ValidateEdit);
            this.Grid.CurrentCellChanged += new System.EventHandler(this.Grid_RowColChange);
            this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
            this.Grid.KeyDown += new Wisej.Web.KeyEventHandler(this.grid_KeyDown);
            // 
            // lblAccount
            // 
            this.lblAccount.AppearanceKey = "Header";
            this.lblAccount.Font = new System.Drawing.Font("@header", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.lblAccount.Location = new System.Drawing.Point(272, 26);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(172, 30);
            this.lblAccount.TabIndex = 19;
            // 
            // lblMortgageHolder
            // 
            this.lblMortgageHolder.AutoSize = true;
            this.lblMortgageHolder.Location = new System.Drawing.Point(30, 334);
            this.lblMortgageHolder.Name = "lblMortgageHolder";
            this.lblMortgageHolder.Size = new System.Drawing.Size(126, 15);
            this.lblMortgageHolder.TabIndex = 18;
            this.lblMortgageHolder.Text = "MORTGAGES HELD";
            this.lblMortgageHolder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuChangeNumber,
            this.mnuMortgageHolderLabel,
            this.mnuDeleteMort,
            this.mnuFilePrintMortHolderInfo});
            this.MainMenu1.Name = null;
            // 
            // mnuChangeNumber
            // 
            this.mnuChangeNumber.Index = 0;
            this.mnuChangeNumber.Name = "mnuChangeNumber";
            this.mnuChangeNumber.Text = "Change Mortgage Holder Number";
            this.mnuChangeNumber.Click += new System.EventHandler(this.mnuChangeNumber_Click);
            // 
            // mnuMortgageHolderLabel
            // 
            this.mnuMortgageHolderLabel.Index = 1;
            this.mnuMortgageHolderLabel.Name = "mnuMortgageHolderLabel";
            this.mnuMortgageHolderLabel.Text = "Mortgage Holder Label";
            this.mnuMortgageHolderLabel.Click += new System.EventHandler(this.mnuMortgageHolderLabel_Click);
            // 
            // mnuDeleteMort
            // 
            this.mnuDeleteMort.Index = 2;
            this.mnuDeleteMort.Name = "mnuDeleteMort";
            this.mnuDeleteMort.Text = "Delete Mortgage Holder";
            this.mnuDeleteMort.Click += new System.EventHandler(this.mnuDeleteMort_Click);
            // 
            // mnuFilePrintMortHolderInfo
            // 
            this.mnuFilePrintMortHolderInfo.Index = 3;
            this.mnuFilePrintMortHolderInfo.Name = "mnuFilePrintMortHolderInfo";
            this.mnuFilePrintMortHolderInfo.Text = "Print Mortgage Holder Info";
            this.mnuFilePrintMortHolderInfo.Visible = false;
            this.mnuFilePrintMortHolderInfo.Click += new System.EventHandler(this.mnuFilePrintMortHolderInfo_Click);
            // 
            // cmdFileDeleteRow
            // 
            this.cmdFileDeleteRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileDeleteRow.Location = new System.Drawing.Point(665, 26);
            this.cmdFileDeleteRow.Name = "cmdFileDeleteRow";
            this.cmdFileDeleteRow.Size = new System.Drawing.Size(84, 24);
            this.cmdFileDeleteRow.TabIndex = 58;
            this.cmdFileDeleteRow.Text = "Delete Row";
            this.cmdFileDeleteRow.Click += new System.EventHandler(this.cmdFileDeleteRow_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = -1;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuFileSeperator2
            // 
            this.mnuFileSeperator2.Index = -1;
            this.mnuFileSeperator2.Name = "mnuFileSeperator2";
            this.mnuFileSeperator2.Text = "-";
            // 
            // mnuPop
            // 
            this.mnuPop.Index = -1;
            this.mnuPop.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAdd,
            this.mnuDelete,
            this.mnuSepar,
            this.mnuCancel});
            this.mnuPop.Name = "mnuPop";
            this.mnuPop.Text = "PopUp";
            this.mnuPop.Visible = false;
            // 
            // mnuAdd
            // 
            this.mnuAdd.Index = 0;
            this.mnuAdd.Name = "mnuAdd";
            this.mnuAdd.Text = "Add New Row";
            this.mnuAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete Row";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 2;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuCancel
            // 
            this.mnuCancel.Index = 3;
            this.mnuCancel.Name = "mnuCancel";
            this.mnuCancel.Text = "Cancel";
            // 
            // mnuHelpPop
            // 
            this.mnuHelpPop.Index = -1;
            this.mnuHelpPop.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuHelpPopEdit,
            this.mnuSepar4,
            this.mnuHelpPopCancel});
            this.mnuHelpPop.Name = "mnuHelpPop";
            this.mnuHelpPop.Text = "HelpPopUp";
            this.mnuHelpPop.Visible = false;
            // 
            // mnuHelpPopEdit
            // 
            this.mnuHelpPopEdit.Index = 0;
            this.mnuHelpPopEdit.Name = "mnuHelpPopEdit";
            this.mnuHelpPopEdit.Text = "Edit Mortgage Holder";
            this.mnuHelpPopEdit.Click += new System.EventHandler(this.mnuHelpPopEdit_Click);
            // 
            // mnuSepar4
            // 
            this.mnuSepar4.Index = 1;
            this.mnuSepar4.Name = "mnuSepar4";
            this.mnuSepar4.Text = "-";
            // 
            // mnuHelpPopCancel
            // 
            this.mnuHelpPopCancel.Index = 2;
            this.mnuHelpPopCancel.Name = "mnuHelpPopCancel";
            this.mnuHelpPopCancel.Text = "Cancel";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(466, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(97, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Save";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdMortHolderList
            // 
            this.cmdMortHolderList.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdMortHolderList.Location = new System.Drawing.Point(753, 26);
            this.cmdMortHolderList.Name = "cmdMortHolderList";
            this.cmdMortHolderList.Shortcut = Wisej.Web.Shortcut.F2;
            this.cmdMortHolderList.Size = new System.Drawing.Size(142, 24);
            this.cmdMortHolderList.TabIndex = 56;
            this.cmdMortHolderList.Text = "Mortgage Holder List";
            this.cmdMortHolderList.Visible = false;
            this.cmdMortHolderList.Click += new System.EventHandler(this.cmdMortHolderList_Click);
            // 
            // cmdFileAdd
            // 
            this.cmdFileAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileAdd.Location = new System.Drawing.Point(589, 26);
            this.cmdFileAdd.Name = "cmdFileAdd";
            this.cmdFileAdd.Shortcut = Wisej.Web.Shortcut.CtrlShiftN;
            this.cmdFileAdd.Size = new System.Drawing.Size(74, 24);
            this.cmdFileAdd.TabIndex = 57;
            this.cmdFileAdd.Text = "New Row";
            this.cmdFileAdd.Click += new System.EventHandler(this.cmdFileAdd_Click);
            // 
            // frmMortgageHolder
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(939, 785);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmMortgageHolder";
            this.ShowInTaskbar = false;
            this.Text = "Account Association Information";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmMortgageHolder_Load);
            this.Resize += new System.EventHandler(this.frmMortgageHolder_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmMortgageHolder_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMortgageHolder_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkRcvCopies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMortContact)).EndInit();
            this.fraMortContact.ResumeLayout(false);
            this.fraMortContact.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framHoldersList)).EndInit();
            this.framHoldersList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HelpGrid)).EndInit();
            this.HelpGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileDeleteRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMortHolderList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileAdd)).EndInit();
            this.ResumeLayout(false);

		}


        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
		public FCButton cmdMortHolderList;
		public FCButton cmdFileAdd;
        public FCLabel lblLocationLabel;
        public FCLabel lblLocation;
    }
}
