//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;


namespace Global
{
	/// <summary>
	/// Summary description for rptARDailyAuditMaster.
	/// </summary>
	public class rptARDailyAuditMaster : fecherFoundation.FCForm
	{

// nObj = 1
//   0	rptARDailyAuditMaster	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/10/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/10/2005              *
		// ********************************************************
		public bool boolWide; // True if this report is in Wide Format
		public int lngWidth; // This is the Print Width of the report
		bool boolDone;
		public bool boolOrderByReceipt; // order by receipt
		public bool boolCloseOutAudit; // Actually close out the payments
		public bool boolLandscape; // is the report to be printed landscape?

		private void ActiveReport_FetchData(ref bool EOF)
		{
			// this should let the detail section fire once
			if (boolDone) {
				EOF = boolDone;
			} else {
				EOF = boolDone;
				boolDone = true;
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			switch (KeyCode) {
				
				case Keys.Escape:
				{
					Close();
					break;
				}
			} //end switch
		}

		private void ActiveReport_PageStart()
		{
			FCGlobal.Screen.MousePointer = 1;
			frmWait.InstancePtr.Hide();
		}

		private void ActiveReport_ReportEnd()
		{
			FCGlobal.Screen.MousePointer = 1;
			frmWait.InstancePtr.Hide();
		}

		private void ActiveReport_ReportStart()
		{
			FCGlobal.Screen.MousePointer = 11;
			frmWait.InstancePtr.Init("Please Wait..."+"\r\n"+"Loading", true);
		}

		private void SetupFields()
		{
			// find out which way the user wants to print

			boolWide = boolLandscape;

			if (boolWide) { // wide format
				lngWidth = 13500;
				Printer.Orientation = ddOLandscape;
			} else { // narrow format
				lngWidth = 10750;
				Printer.Orientation = ddOPortrait;
			}

			this.PrintWidth = lngWidth;
			sarARDailyAudit.Width = lngWidth;
		}

		public void StartUp(ref bool boolCloseOut, ref bool boolPassLandscape)
		{
			try
			{	// On Error GoTo ERROR_HANDLER
				boolCloseOutAudit = boolCloseOut;

				// if the boolAppend is on then the first report will ALWAYS be the RE audit
				// at the end of the first report the PP audit will be printed
				boolLandscape = boolPassLandscape;

				modGlobalFunctions.SetFixedSizeReport(ref this, ref MDIParent.InstancePtr.GRID);

				SetupFields();

				frmWait.InstancePtr.IncrementProgress();

				return;
			}
			catch
			{	// ERROR_HANDLER:
				MessageBox.Show("Error #"+Convert.ToString(Information.Err().Number)+" - "+Information.Err().Description+".", "Error In Start Up", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_Terminate()
		{
			try
			{	// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsCloseOut = new clsDRWrapper();
				string strSQL = "";

				// This will move all of the reports back 1 number until 9 and then save this as the first
				if (boolCloseOutAudit) {
					modGlobalFunctions.IncrementSavedReports_2("LastARDailyAudit");
					this.Pages.Save("RPT\\LastARDailyAudit1.RDF");
				}
				return;
			}
			catch
			{	// ERROR_HANDLER:

			}
		}

		private void Detail_Format()
		{
			sarARDailyAudit.Object = new arARDailyAudit();
		}

		private void rptARDailyAuditMaster_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptARDailyAuditMaster.Caption	= "Daily Audit Report";
			//rptARDailyAuditMaster.Icon	= "rptARDailtAuditMaster.dsx":0000";
			//rptARDailyAuditMaster.Left	= 0;
			//rptARDailyAuditMaster.Top	= 0;
			//rptARDailyAuditMaster.Width	= 11880;
			//rptARDailyAuditMaster.Height	= 8595;
			//rptARDailyAuditMaster.StartUpPosition	= 3;
			//rptARDailyAuditMaster.SectionData	= "rptARDailtAuditMaster.dsx":058A;
			//End Unmaped Properties
		}
	}
}