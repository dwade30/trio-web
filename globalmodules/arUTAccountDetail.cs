﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for arUTAccountDetail.
	/// </summary>
	public partial class arUTAccountDetail : FCSectionReport
	{
		// nObj = 1
		//   0	arUTAccountDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/11/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/25/2006              *
		// ********************************************************
		int lngRow;
		int lngPDNumber;
		int lngPDTop;
		double dblPDTotal;
		bool boolPerDiem;
		string strBillText = "";
		bool boolSecondGrid;
		bool boolBothGrids;
		bool boolFirstLineBoth;

		public arUTAccountDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static arUTAccountDetail InstancePtr
		{
			get
			{
				return (arUTAccountDetail)Sys.GetInstance(typeof(arUTAccountDetail));
			}
		}

		protected arUTAccountDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (lngRow < 0)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			if (frmUTCLStatus.InstancePtr.WGRID.Visible && frmUTCLStatus.InstancePtr.WGRID.Rows > 1 && frmUTCLStatus.InstancePtr.SGRID.Visible && frmUTCLStatus.InstancePtr.SGRID.Rows > 1)
			{
				boolBothGrids = true;
				boolFirstLineBoth = true;
			}
			else
			{
				boolBothGrids = false;
			}
			SetHeaderString();
			boolPerDiem = false;
			modUTFunctions.Statics.intPerdiemCounter = 0;
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			if (boolBothGrids)
			{
				// kk12222014 trouts-121   If both grids then set up the sewer per diem table
				CreatePerDiemTable_2(false);
			}
			else
			{
				if (frmUTCLStatus.InstancePtr.boolShowingWater)
				{
					CreatePerDiemTable_2(true);
				}
				else
				{
					CreatePerDiemTable_2(false);
				}
			}
			lngRow = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (boolBothGrids)
			{
				// show both
				if (boolSecondGrid)
				{
					lngRow = FindNextVisibleRow_6(lngRow, true);
					BindFields(ref frmUTCLStatus.InstancePtr.WGRID);
				}
				else
				{
					lngRow = FindNextVisibleRow_6(lngRow, false);
					BindFields(ref frmUTCLStatus.InstancePtr.SGRID);
				}
			}
			else if (frmUTCLStatus.InstancePtr.boolShowingWater)
			{
				lngRow = FindNextVisibleRow_6(lngRow, true);
				BindFields(ref frmUTCLStatus.InstancePtr.WGRID);
			}
			else
			{
				lngRow = FindNextVisibleRow_6(lngRow, false);
				BindFields(ref frmUTCLStatus.InstancePtr.SGRID);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}
		// vbPorter upgrade warning: Grid As object	OnWrite(VSFlex7LCtl.VSFlexGrid)
		private void BindFields(ref FCGrid Grid)
		{
			// this will print the row from the grid
			lnTotals.Visible = false;
			// reset the visible property to false so that only a subtotal line will have a horizontal line above it
			lnTotals.LineWeight = 0;
			//subPerDiem = null; // kk01152015 trout-1134  Make sure the per diem stuff is hidden first
			subPerDiem.Report = null;
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			if (boolBothGrids && boolFirstLineBoth)
			{
				fldYear.Text = "Sewer";
				fldDate.Text = "";
				fldRef.Text = "";
				fldCode.Text = "";
				fldPrincipal.Text = "";
				fldTax.Text = "";
				fldInterest.Text = "";
				fldCosts.Text = "";
				fldTotal.Text = "";
				fldName.Text = "";
				lngRow = 0;
				boolFirstLineBoth = false;
			}
			else if (lngRow >= 0)
			{
				// check for the signal to exit the loop
				// figure out what type of line it is
				if (Grid.RowOutlineLevel(lngRow) == 1)
				{
					// is it a header line
					HideFields_2(false);
					// Year
					fldYear.Text = Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber);
					// Date
					fldDate.Text = Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColDate);
					// Ref
					fldRef.Text = Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColRef);
					// fldRef.Text = ""
					// Code
					fldCode.Text = "";
					// Principal
					fldPrincipal.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColPrincipal), "#,##0.00");
					// Tax
					fldTax.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColTax), "#,##0.00");
					// Interest
					fldInterest.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColInterest), "#,##0.00");
					// Costs
					fldCosts.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColCosts), "#,##0.00");
					// Total
					fldTotal.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColTotal), "#,##0.00");
				}
				else if (Strings.Left(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColRef), 10) == "Billed To:")
				{
					// is it a name line
					HideFields_2(true);
					// Name
					fldName.Text = Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColRef) + " " + Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColPaymentCode);
				}
				else
				{
					if (Strings.Left(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber), 14) == "Account Totals")
					{
						// is it the last line?
						HideFields_2(false);
						fldName.Visible = true;
						fldYear.Visible = false;
						fldDate.Visible = false;
						fldRef.Visible = false;
						fldCode.Visible = false;
						fldName.Left = 0;
						fldName.Width = fldPrincipal.Left;
						// show the line above this row and make it darker
						lnTotals.Visible = true;
						lnTotals.LineWeight = 2;
						// total line
						fldName.Text = Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber);
						// Principal
						fldPrincipal.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColPrincipal), "#,##0.00");
						// Tax
						fldTax.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColTax), "#,##0.00");
						// Interest
						fldInterest.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColInterest), "#,##0.00");
						// Costs
						fldCosts.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColCosts), "#,##0.00");
						// Total
						fldTotal.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColTotal), "#,##0.00");
						// kk12222014 trouts-121  Move this out of Report footer so it shows up for both services
						// But in testing this condition was never run
						if (boolPerDiem)
						{
							subPerDiem.Report = new srptUTPerDiemList();
						}
						else
						{
							subPerDiem.Report = null;
						}
					}
					else if (Grid.TextMatrix(lngRow - 1, frmUTCLStatus.InstancePtr.lngGRIDColLineCode) == "=")
					{
						// is this line a hidden totals line?
						HideFields_2(true);
						fldName.Text = "";
					}
					else
					{
						HideFields_2(false);
						fldYear.Text = "";
						// Date
						fldDate.Text = Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColDate);
						// Ref
						if (Strings.Left(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColRef), 4) == "Lien" && Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColRef) == Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColDate))
						{
							fldRef.Text = "";
						}
						else
						{
							fldRef.Text = Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColRef);
						}
						if (Strings.Left(fldRef.Text, 5) == "Total")
						{
							lnTotals.Visible = true;
							lnTotals.LineWeight = 1;
						}
						else
						{
							lnTotals.Visible = false;
						}
						// Code
						fldCode.Text = Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColPaymentCode);
						if (fldCode.Text.Length > 1)
						{
							fldRef.Text = "";
							fldCode.Text = "";
						}
						// kk01152015 trout-1134 Only do this on the last line
						// show the line above this row and make it darker
						// lnTotals.Visible = True
						// lnTotals.LineWeight = 2
						// Principal
						fldPrincipal.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColPrincipal), "#,##0.00");
						// Tax
						fldTax.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColTax), "#,##0.00");
						// Interest
						fldInterest.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColInterest), "#,##0.00");
						// Costs
						fldCosts.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColCosts), "#,##0.00");
						// Total
						fldTotal.Text = Strings.Format(Grid.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngGRIDColTotal), "#,##0.00");
						// kk01152015 trout-1134  This is the totals line and payment details, only show the per diem stuff after the totals line
						if (Grid.RowOutlineLevel(lngRow) == 0)
						{
							// show the line above this row and make it darker
							lnTotals.Visible = true;
							lnTotals.LineWeight = 2;
							lnTotals.X1 = 0;
							// kk12222014 trouts-121  Move this out of Report footer so it shows up for both services
							if (boolPerDiem)
							{
								subPerDiem.Report = new srptUTPerDiemList();
								subPerDiem.Visible = true;
							}
						}
					}
				}
			}
			else
			{
				fldYear.Text = "";
				fldDate.Text = "";
				fldRef.Text = "";
				fldCode.Text = "";
				fldPrincipal.Text = "";
				fldTax.Text = "";
				fldInterest.Text = "";
				fldCosts.Text = "";
				fldTotal.Text = "";
				fldName.Text = "";
				subPerDiem.Report = null;
				// kk12222014 trouts-121   Hide the per diem subreport after the sewer grid is printed
				//subPerDiem.Visible = false;
				boolPerDiem = false;
				if (boolBothGrids && boolSecondGrid == false)
				{
					// if the second grid has not been printed then reset the counter and allow it to count until reset at a negative number
					this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
					boolSecondGrid = true;
					lngRow = 0;
					fldYear.Text = "Water";
					if (boolBothGrids)
					{
						// kk12222014 trouts-121  Reset the per diem table for water
						CreatePerDiemTable_2(true);
					}
					// kk trouts-6 03012013  Change Water to Stormwater for Bangor
					if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
					{
						fldYear.Text = "Stormwater";
						fldYear.Width = 1100 / 1440F;
						// kk12222014  Change the width so the line isn't split
					}
				}
			}
		}

		private int FindNextVisibleRow_6(int lngR, bool boolWater)
		{
			return FindNextVisibleRow(ref lngR, ref boolWater);
		}

		private int FindNextVisibleRow(ref int lngR, ref bool boolWater)
		{
			int FindNextVisibleRow = 0;
			// this function will start at the row number passed in and return the next row to print
			// this function will return -1 if there are no more rows to show
			int lngNext = 0;
			int intLvl;
			bool boolFound = false;
			int lngMax = 0;
			if (boolWater)
			{
				boolFound = false;
				lngNext = lngR + 1;
				lngMax = frmUTCLStatus.InstancePtr.WGRID.Rows;
				if (lngNext < lngMax)
				{
					boolFound = false;
					while (!(boolFound || frmUTCLStatus.InstancePtr.WGRID.RowOutlineLevel(lngNext) < 2))
					{
						if (frmUTCLStatus.InstancePtr.WGRID.IsCollapsed(LastParentRow(lngNext, ref boolWater)) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							lngNext += 1;
							boolFound = false;
							if (lngNext >= lngMax)
							{
								return FindNextVisibleRow;
							}
						}
						else
						{
							boolFound = true;
						}
					}
					FindNextVisibleRow = lngNext;
				}
			}
			else
			{
				boolFound = false;
				lngNext = lngR + 1;
				lngMax = frmUTCLStatus.InstancePtr.SGRID.Rows;
				if (lngNext < lngMax)
				{
					boolFound = false;
					while (!(boolFound || frmUTCLStatus.InstancePtr.SGRID.RowOutlineLevel(lngNext) < 2))
					{
						if (frmUTCLStatus.InstancePtr.SGRID.IsCollapsed(LastParentRow(lngNext, ref boolWater)) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							lngNext += 1;
							boolFound = false;
							if (lngNext >= lngMax)
							{
								return FindNextVisibleRow;
							}
						}
						else
						{
							boolFound = true;
						}
					}
					FindNextVisibleRow = lngNext;
				}
			}
			if (FindNextVisibleRow == 0)
				FindNextVisibleRow = -1;
			return FindNextVisibleRow;
		}
		// vbPorter upgrade warning: CurRow As Variant --> As int
		public int LastParentRow(int CurRow, ref bool boolWater)
		{
			int LastParentRow = 0;
			int l;
			int curLvl;
			l = CurRow;
			if (boolWater)
			{
				curLvl = frmUTCLStatus.InstancePtr.WGRID.RowOutlineLevel(l);
				l -= 1;
				while (frmUTCLStatus.InstancePtr.WGRID.RowOutlineLevel(l) > 1)
				{
					l -= 1;
				}
			}
			else
			{
				curLvl = frmUTCLStatus.InstancePtr.SGRID.RowOutlineLevel(l);
				l -= 1;
				while (frmUTCLStatus.InstancePtr.SGRID.RowOutlineLevel(l) > 1)
				{
					l -= 1;
				}
			}
			LastParentRow = l;
			return LastParentRow;
		}

		private void HideFields_2(bool boolHide)
		{
			HideFields(ref boolHide);
		}

		private void HideFields(ref bool boolHide)
		{
			// this sub will either hide or show the fields depending on which variable is shown
			fldYear.Visible = !boolHide;
			fldDate.Visible = !boolHide;
			fldRef.Visible = !boolHide;
			fldCode.Visible = !boolHide;
			fldPrincipal.Visible = !boolHide;
			fldTax.Visible = !boolHide;
			fldInterest.Visible = !boolHide;
			fldCosts.Visible = !boolHide;
			fldTotal.Visible = !boolHide;
			fldName.Visible = boolHide;
			if (boolHide)
			{
				// makes sure that the name field is in the proper position
				fldName.Left = 1F;
				fldName.Width = this.PrintWidth - 2F;
			}
		}

		private void SetHeaderString()
		{
			// this will set the correct header for this account
			string strTemp;
			int lngAcctNum;
			clsDRWrapper rsMaster = new clsDRWrapper();
			string strAddr1 = "";
			string strAddr2 = "";
			string strAddr3 = "";
			string strAddr4 = "";
			strTemp = "UT";
			lngAcctNum = frmUTCLStatus.InstancePtr.CurrentAccount;
			rsMaster.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + FCConvert.ToString(lngAcctNum), modExtraModules.strUTDatabase);
			if (!rsMaster.EndOfFile())
			{
				strAddr1 = FCConvert.ToString(rsMaster.Get_Fields_String("OAddress1"));
				strAddr2 = FCConvert.ToString(rsMaster.Get_Fields_String("OAddress2"));
				strAddr3 = FCConvert.ToString(rsMaster.Get_Fields_String("OAddress3"));
				if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OZip4"))) != "")
				{
					strAddr4 = rsMaster.Get_Fields_String("OCity") + ", " + rsMaster.Get_Fields_String("OState") + " " + rsMaster.Get_Fields_String("OZip") + "-" + rsMaster.Get_Fields_String("OZip4");
				}
				else
				{
					strAddr4 = rsMaster.Get_Fields_String("OCity") + ", " + rsMaster.Get_Fields_String("OState") + " " + rsMaster.Get_Fields_String("OZip");
				}
				lblAcreage.Text = "RE Acct: " + rsMaster.Get_Fields_Int32("REAccount");
				if (Strings.Trim(strAddr3) == "")
				{
					strAddr3 = strAddr4;
					strAddr4 = "";
				}
				if (Strings.Trim(strAddr2) == "")
				{
					strAddr2 = strAddr3;
					strAddr3 = strAddr4;
					strAddr4 = "";
				}
				if (Strings.Trim(strAddr1) == "")
				{
					strAddr1 = strAddr2;
					strAddr2 = strAddr3;
					strAddr3 = strAddr4;
					strAddr4 = "";
				}
			}
			else
			{
				strAddr1 = "";
				strAddr2 = "";
				strAddr3 = "";
				strAddr4 = "";
				lblAcreage.Text = "";
			}
			lblAddress1.Text = strAddr1;
			lblAddress2.Text = strAddr2;
			lblAddress3.Text = strAddr3;
			lblAddress4.Text = strAddr4;
			strTemp += " Account " + FCConvert.ToString(lngAcctNum) + " Detail";
			strTemp += "\r\n" + "as of " + Strings.Format(modUTFunctions.Statics.UTEffectiveDate, "MM/dd/yyyy");
			if (boolBothGrids)
			{
				lblHeader.Text = strTemp;
			}
			else if (frmUTCLStatus.InstancePtr.boolShowingWater)
			{
				lblHeader.Text = strTemp + " - Water";
				// kk trouts-6 02282013  Change Water to Stormwater for Bangor
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					lblHeader.Text = strTemp + " - Stormwater";
				}
			}
			else
			{
				lblHeader.Text = strTemp + " - Sewer";
			}
			lblName.Text = "Name: " + frmUTCLStatus.InstancePtr.lblOwnersName.Text;
			lblLocation.Text = "Location: " + frmUTCLStatus.InstancePtr.lblLocation.Text;
			lblMapLot.Text = "Map/Lot: " + frmUTCLStatus.InstancePtr.lblMapLot.Text;
		}

		private void CreatePerDiemTable_2(bool boolWater)
		{
			CreatePerDiemTable(ref boolWater);
		}

		private void CreatePerDiemTable(ref bool boolWater)
		{
			int lngRW;
			FCUtils.EraseSafe(modUTFunctions.Statics.pdiInfo);
			// kk12222014 trouts-121   Reset pdiInfo array and count for 2nd pass
			modUTFunctions.Statics.intPerdiemCounter = 0;
			if (boolWater)
			{
				for (lngRW = 1; lngRW <= frmUTCLStatus.InstancePtr.WGRID.Rows - 1; lngRW++)
				{
					if (Strings.Trim(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(lngRW, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber)) != "")
					{
						strBillText = Strings.Trim(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(lngRW, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber));
					}
					if (Conversion.Val(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(lngRW, frmUTCLStatus.InstancePtr.lngGRIDColPerDiem)) != 0)
					{
						AddPerDiem(ref lngRW, ref boolWater);
					}
				}
			}
			else
			{
				for (lngRW = 1; lngRW <= frmUTCLStatus.InstancePtr.SGRID.Rows - 1; lngRW++)
				{
					if (Strings.Trim(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(lngRW, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber)) != "")
					{
						strBillText = Strings.Trim(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(lngRW, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber));
					}
					if (Conversion.Val(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(lngRW, frmUTCLStatus.InstancePtr.lngGRIDColPerDiem)) != 0)
					{
						AddPerDiem(ref lngRW, ref boolWater);
					}
				}
			}
		}

		private void AddPerDiem(ref int lngRNum, ref bool boolWater)
		{
			object obNew;
			string strTemp = "";
			if (boolWater)
			{
				Array.Resize(ref modUTFunctions.Statics.pdiInfo, modUTFunctions.Statics.intPerdiemCounter + 1);
				if (Strings.Trim(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(lngRNum, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber)) == "")
				{
					modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(frmUTCLStatus.InstancePtr.LastParentRow(lngRNum, ref boolWater), frmUTCLStatus.InstancePtr.lngGRIDColBillNumber));
					if (boolWater)
					{
						Array.Resize(ref modUTFunctions.Statics.pdiInfo, modUTFunctions.Statics.intPerdiemCounter + 1);
						if (Strings.Trim(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(lngRNum, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber)) == "")
						{
							modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(frmUTCLStatus.InstancePtr.LastParentRow(lngRNum, ref boolWater), frmUTCLStatus.InstancePtr.lngGRIDColBillNumber));
						}
						else
						{
							modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(lngRNum, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber));
						}
					}
					else
					{
						modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(lngRNum, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber));
						modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].dblPerDiem = Conversion.Val(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(lngRNum, frmUTCLStatus.InstancePtr.lngGRIDColPerDiem));
					}
				}
				else
				{
					Array.Resize(ref modUTFunctions.Statics.pdiInfo, modUTFunctions.Statics.intPerdiemCounter + 1);
					if (Strings.Trim(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(lngRNum, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber)) == "")
					{
						modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(frmUTCLStatus.InstancePtr.LastParentRow(lngRNum, ref boolWater), frmUTCLStatus.InstancePtr.lngGRIDColBillNumber));
					}
					else
					{
						modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(lngRNum, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber));
					}
					modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].dblPerDiem = Conversion.Val(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(lngRNum, frmUTCLStatus.InstancePtr.lngGRIDColPerDiem));
					modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].dblPerDiem = Conversion.Val(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(lngRNum, frmUTCLStatus.InstancePtr.lngGRIDColPerDiem));
				}
			}
			else
			{
				Array.Resize(ref modUTFunctions.Statics.pdiInfo, modUTFunctions.Statics.intPerdiemCounter + 1);
				if (Strings.Trim(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(lngRNum, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber)) == "")
				{
					modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(frmUTCLStatus.InstancePtr.LastParentRow(lngRNum, ref boolWater), frmUTCLStatus.InstancePtr.lngGRIDColBillNumber));
				}
				else
				{
					modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(lngRNum, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber));
				}
				modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].dblPerDiem = Conversion.Val(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(lngRNum, frmUTCLStatus.InstancePtr.lngGRIDColPerDiem));
			}
			modUTFunctions.Statics.intPerdiemCounter += 1;
			boolPerDiem = true;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// kk12222014 trouts-121  Moved per diem info out of report footer so it shows correctly for both grids'    If boolPerDiem Then
			// Set subPerDiem = New srptUTPerDiemList
			// Else
			// Set subPerDiem = Nothing
			// End If
		}

		private void arUTAccountDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arUTAccountDetail.Caption	= "Account Detail";
			//arUTAccountDetail.Icon	= "arUTAccountDetail.dsx":0000";
			//arUTAccountDetail.Left	= 0;
			//arUTAccountDetail.Top	= 0;
			//arUTAccountDetail.Width	= 15255;
			//arUTAccountDetail.Height	= 9405;
			//arUTAccountDetail.StartUpPosition	= 3;
			//arUTAccountDetail.SectionData	= "arUTAccountDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
