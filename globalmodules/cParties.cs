﻿using fecherFoundation;
using fecherFoundation.Extensions;
using System;
using TWSharedLibrary;
using Wisej.Web;
using TWSharedLibrary.Data;
namespace Global
{
    public class cPartyController
    {
        private string strLastError = string.Empty;
        private int lngLastError;
        private string strConnectionName = "";

        private void ClearErrors()
        {
            strLastError = "";
            lngLastError = 0;
        }

        public bool LoadPartyByGuid(ref cParty tParty, string strPartyGuid, bool boolPartyOnly = false)
        {
            bool LoadPartyByGuid = false;
            ClearErrors();
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                if (tParty == null)
                {
                    tParty = new cParty();
                }
                else
                {
                    tParty.Clear();
                }

                rsLoad.OpenRecordset("select id from parties where partyGUID = '" + strPartyGuid + "'", strConnectionName);

                if (!rsLoad.EndOfFile())
                {
                    LoadPartyByGuid = LoadParty(ref tParty, FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id")), boolPartyOnly);
                }

                return LoadPartyByGuid;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex)
                                          .Number;
                strLastError = ex.GetBaseException()
                                 .Message;
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return LoadPartyByGuid;
        }
        // VBto upgrade warning: tParty As cParty	OnWrite(cParty)
        public bool SaveParty(ref cParty tParty, bool boolPartyOnly = false, bool boolCreateOnly = false)
        {
            bool SaveParty = false;
            ClearErrors();
            bool boolReturn;
            boolReturn = true;
            try
            {
                // On Error GoTo ErrorHandler
                if (!(tParty == null))
                {
                    clsDRWrapper rsSave = new clsDRWrapper();
                    rsSave.OpenRecordset("select * from parties where id = " + tParty.ID, strConnectionName);
                    if (rsSave.EndOfFile())
                    {
                        rsSave.AddNew();
                        rsSave.Set_Fields("DateCreated", DateTime.Now);
                        rsSave.Set_Fields("CreatedBy", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
                    }
                    else if (boolCreateOnly)
                    {
                        SaveParty = true;
                        return SaveParty;
                    }
                    rsSave.Set_Fields("PartyGUID", tParty.PartyGUID);
                    rsSave.Set_Fields("PartyType", tParty.PartyType);
                    rsSave.Set_Fields("FirstName", tParty.FirstName);
                    rsSave.Set_Fields("MiddleName", tParty.MiddleName);
                    rsSave.Set_Fields("LastName", tParty.LastName);
                    rsSave.Set_Fields("Designation", tParty.Designation);
                    rsSave.Set_Fields("Email", tParty.Email);
                    rsSave.Set_Fields("WebAddress", tParty.WebAddress);
                    rsSave.Update();
                    if (tParty.ID < 1)
                    {
                        tParty.ID = rsSave.Get_Fields_Int32("ID");
                    }
                    if (!boolPartyOnly)
                    {
                        foreach (cPartyPhoneNumber tPhone in tParty.PhoneNumbers)
                        {
                            rsSave.OpenRecordset("select * from CentralPhoneNumbers where id = " + FCConvert.ToString(tPhone.ID), strConnectionName);
                            if (rsSave.EndOfFile())
                            {
                                rsSave.AddNew();
                            }
                            rsSave.Set_Fields("PartyID", tParty.ID);
                            rsSave.Set_Fields("PhoneNumber", tPhone.PhoneNumber);
                            rsSave.Set_Fields("Description", tPhone.Description);
                            rsSave.Set_Fields("PhoneOrder", tPhone.PhoneOrder);
                            rsSave.Set_Fields("Extension", tPhone.Extension);
                            rsSave.Update();
                            tPhone.ID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("id"));
                            tPhone.ParentID = tParty.ID;
                        }
                        // tPhone
                        foreach (cPartyComment tComment in tParty.Comments)
                        {
                            rsSave.OpenRecordset("select * from CentralComments where id = " + FCConvert.ToString(tComment.ID), strConnectionName);
                            if (rsSave.EndOfFile())
                            {
                                rsSave.AddNew();
                            }
                            rsSave.Set_Fields("Comment", tComment.Comment);
                            rsSave.Set_Fields("ProgModule", tComment.ProgModule);
                            rsSave.Set_Fields("EnteredBy", tComment.EnteredBy);
                            rsSave.Set_Fields("LastModified", tComment.LastModified);
                            rsSave.Set_Fields("PartyID", tParty.ID);
                            rsSave.Update();
                            tComment.ID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("id"));
                            tComment.PartyID = tParty.ID;
                        }
                        // tComment
                        foreach (cPartyAddress tAddress in tParty.Addresses)
                        {
                            rsSave.OpenRecordset("select * from Addresses where id = " + FCConvert.ToString(tAddress.ID), strConnectionName);
                            if (rsSave.EndOfFile())
                            {
                                rsSave.AddNew();
                            }
                            rsSave.Set_Fields("PartyID", tParty.ID);
                            rsSave.Set_Fields("Address1", tAddress.Address1);
                            rsSave.Set_Fields("Address2", tAddress.Address2);
                            rsSave.Set_Fields("Address3", tAddress.Address3);
                            rsSave.Set_Fields("city", tAddress.City);
                            rsSave.Set_Fields("state", tAddress.State);
                            rsSave.Set_Fields("zip", tAddress.Zip);
                            rsSave.Set_Fields("country", tAddress.Country);
                            rsSave.Set_Fields("OverrideName", tAddress.OverrideName);
                            rsSave.Set_Fields("AddressType", tAddress.AddressType);
                            rsSave.Set_Fields("seasonal", tAddress.Seasonal);
                            rsSave.Set_Fields("ProgModule", tAddress.ProgModule);
                            rsSave.Set_Fields("Comment", tAddress.Comment);
                            rsSave.Set_Fields("StartMonth", tAddress.StartMonth);
                            rsSave.Set_Fields("StartDay", tAddress.StartDay);
                            rsSave.Set_Fields("EndMonth", tAddress.EndMonth);
                            rsSave.Set_Fields("EndDay", tAddress.EndDay);
                            rsSave.Set_Fields("ModAccountID", tAddress.ModAccountID);
                            rsSave.Update();
                            tAddress.ID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("ID"));
                            tAddress.PartyID = tParty.ID;
                        }
                        // tAddress
                        foreach (cContact tContact in tParty.Contacts)
                        {
                            rsSave.OpenRecordset("select * from contacts where id = " + FCConvert.ToString(tContact.ID), strConnectionName);
                            if (rsSave.EndOfFile())
                            {
                                rsSave.AddNew();
                            }
                            rsSave.Set_Fields("Description", tContact.Description);
                            rsSave.Set_Fields("Name", tContact.Name);
                            rsSave.Set_Fields("Email", tContact.Email);
                            rsSave.Set_Fields("Comment", tContact.Comment);
                            rsSave.Set_Fields("PartyID", tParty.ID);
                            tContact.PartyID = tParty.ID;
                            rsSave.Update();
                            tContact.ID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("id"));
                            foreach (cPartyPhoneNumber tPhone in tContact.PhoneNumbers)
                            {
                                rsSave.OpenRecordset("select * from ContactPhoneNumbers where id = " + FCConvert.ToString(tPhone.ID), strConnectionName);
                                if (rsSave.EndOfFile())
                                {
                                    rsSave.AddNew();
                                }
                                rsSave.Set_Fields("ContactID", tContact.ID);
                                tPhone.ParentID = tContact.ID;
                                rsSave.Set_Fields("PhoneNumber", tPhone.PhoneNumber);
                                rsSave.Set_Fields("Description", tPhone.Description);
                                rsSave.Set_Fields("PhoneOrder", tPhone.PhoneOrder);
                                rsSave.Set_Fields("Extension", tPhone.Extension);
                                rsSave.Update();
                                tPhone.ID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("id"));
                            }
                            // tPhone
                        }
                        // tContact
                    }
                    SaveParty = boolReturn;
                }
                return SaveParty;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
            return SaveParty;
        }

        public bool LoadParty(ref cParty tParty, int lngPartyID, bool boolPartyOnly = false)
        {
            bool LoadParty = false;
            ClearErrors();

            clsDRWrapper rsParties = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                if (tParty == null)
                {
                    tParty = new cParty();
                }
                else
                {
                    tParty.Clear();
                }

                rsParties.OpenRecordset("select * from parties where id = " + FCConvert.ToString(lngPartyID), strConnectionName);

                if (!rsParties.EndOfFile())
                {
                    tParty.CreatedBy = rsParties.Get_Fields_String("createdby");

                    if (Information.IsDate(rsParties.Get_Fields("datecreated")))
                    {
                        tParty.DateCreated = rsParties.Get_Fields_DateTime("datecreated");
                    }

                    tParty.Designation = rsParties.Get_Fields_String("designation");
                    tParty.Email = rsParties.Get_Fields_String("email");
                    tParty.FirstName = rsParties.Get_Fields_String("firstname");
                    tParty.LastName = rsParties.Get_Fields_String("lastname");
                    tParty.MiddleName = rsParties.Get_Fields_String("middlename");
                    if (!String.IsNullOrWhiteSpace(rsParties.Get_Fields_String("partyguid")))
                    {
	                    tParty.PartyGUID = rsParties.GUIDToString(rsParties.Get_Fields("partyguid"));
                    }
                    tParty.PartyType = rsParties.Get_Fields_Int32("PartyType");
                    tParty.WebAddress = rsParties.Get_Fields_String("webaddress");
                    tParty.ID = rsParties.Get_Fields_Int32("id");

                    if (!boolPartyOnly)
                    {
                        FillParty(ref tParty);
                    }

                    if (lngLastError != 0 || strLastError != "")
                    {
                        return LoadParty;
                    }

                    LoadParty = true;
                }

                return LoadParty;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
            finally
            {
                rsParties.DisposeOf();
            }
            return LoadParty;
        }
        // VBto upgrade warning: lngPartyID As int	OnWrite(double, int)
        public cParty GetParty(int lngPartyID, bool boolPartyOnly = false)
        {
            cParty GetParty = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                cParty tParty;
                // Set tParty = New cParty
                tParty = null;
                if (!LoadParty(ref tParty, lngPartyID, boolPartyOnly))
                {
                    return null;
                }
                GetParty = tParty;
                return GetParty;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
            return GetParty;
        }

        public cParty GetPartyByGUID(string strGuid, bool boolPartyOnly = false)
        {
            cParty GetPartyByGUID = null;
            ClearErrors();
            cParty tParty;
            tParty = null;
            LoadPartyByGuid(ref tParty, strGuid, boolPartyOnly);
            GetPartyByGUID = tParty;

            return GetPartyByGUID;
        ErrorHandler:
            ;
            lngLastError = Information.Err().Number;
            strLastError = Information.Err().Description;
            return GetPartyByGUID;
        }

        public void FillParty(ref cParty tParty)
        {
            clsDRWrapper rsTemp = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                cPartyAddress tAddress;
                cPartyComment tComment;
                cPartyPhoneNumber tPhone;
                rsTemp.OpenRecordset("select * from addresses where partyid = " + tParty.ID + " order by addresstype", strConnectionName);

                while (!rsTemp.EndOfFile())
                {
                    App.DoEvents();
                    tAddress = new cPartyAddress();
                    tAddress.Address1 = rsTemp.Get_Fields_String("address1");
                    tAddress.Address2 = rsTemp.Get_Fields_String("address2");
                    tAddress.Address3 = rsTemp.Get_Fields_String("address3");
                    tAddress.AddressType = rsTemp.Get_Fields_String("addresstype");
                    tAddress.City = rsTemp.Get_Fields_String("city");
                    tAddress.Comment = rsTemp.Get_Fields_String("comment");
                    tAddress.Country = rsTemp.Get_Fields_String("country");
                    tAddress.EndDay = rsTemp.Get_Fields_Int32("endday");
                    tAddress.EndMonth = rsTemp.Get_Fields_Int32("endmonth");
                    tAddress.ID = rsTemp.Get_Fields_Int32("id");
                    tAddress.ModAccountID = rsTemp.Get_Fields_Int32("modaccountid");
                    tAddress.OverrideName = rsTemp.Get_Fields_String("overridename");
                    tAddress.PartyID = rsTemp.Get_Fields_Int32("partyid");
                    tAddress.ProgModule = rsTemp.Get_Fields_String("progmodule");
                    tAddress.Seasonal = rsTemp.Get_Fields_Boolean("seasonal");
                    tAddress.StartDay = rsTemp.Get_Fields_Int32("startday");
                    tAddress.StartMonth = rsTemp.Get_Fields_Int32("startmonth");
                    tAddress.State = rsTemp.Get_Fields_String("state");
                    tAddress.Zip = rsTemp.Get_Fields_String("zip");
                    tParty.Addresses.Add(tAddress);
                    rsTemp.MoveNext();
                }

                rsTemp.OpenRecordset("select * from centralphonenumbers where partyid = " + tParty.ID + "order by phoneorder", strConnectionName);

                while (!rsTemp.EndOfFile())
                {
                    App.DoEvents();
                    tPhone = new cPartyPhoneNumber();
                    tPhone.Description = rsTemp.Get_Fields_String("description");
                    tPhone.Extension = rsTemp.Get_Fields_String("extension");
                    tPhone.ID = rsTemp.Get_Fields_Int32("id");
                    tPhone.ParentID = rsTemp.Get_Fields_Int32("partyid");
                    tPhone.PhoneNumber = rsTemp.Get_Fields_String("PhoneNumber");
                    tPhone.PhoneOrder = rsTemp.Get_Fields_Int32("phoneorder");
                    tParty.PhoneNumbers.Add(tPhone);
                    rsTemp.MoveNext();
                }

                rsTemp.OpenRecordset("select * from centralcomments where partyid = " + tParty.ID + " order by progmodule", strConnectionName);

                while (!rsTemp.EndOfFile())
                {
                    App.DoEvents();
                    tComment = new cPartyComment();
                    tComment.Comment = rsTemp.Get_Fields_String("comment");
                    tComment.EnteredBy = rsTemp.Get_Fields_String("enteredby");
                    tComment.ID = rsTemp.Get_Fields_Int32("id");
                    tComment.LastModified = rsTemp.Get_Fields_DateTime("lastmodified");
                    tComment.PartyID = rsTemp.Get_Fields_Int32("partyid");
                    tComment.ProgModule = rsTemp.Get_Fields_String("progmodule");
                    tParty.Comments.Add(tComment);
                    rsTemp.MoveNext();
                }

                rsTemp.OpenRecordset("select * from contacts where partyid = " + tParty.ID, strConnectionName);
                cContact tContact;

                while (!rsTemp.EndOfFile())
                {
                    tContact = new cContact();
                    tContact.Comment = rsTemp.Get_Fields_String("comment");
                    tContact.Description = rsTemp.Get_Fields_String("description");
                    tContact.Email = rsTemp.Get_Fields_String("email");
                    tContact.ID = rsTemp.Get_Fields_Int32("id");
                    tContact.Name = rsTemp.Get_Fields_String("name");
                    tContact.PartyID = rsTemp.Get_Fields_Int32("partyid");
                    FillContact(ref tContact);
                    tParty.Contacts.Add(tContact);
                    rsTemp.MoveNext();
                }

                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
            finally
            {
                rsTemp.DisposeOf();
            }
        }

        private void FillContact(ref cContact tContact)
        {
            try
            {
                // On Error GoTo ErrorHandler
                if (!(tContact == null))
                {
                    using (clsDRWrapper rsTemp = new clsDRWrapper())
                    {
                        rsTemp.OpenRecordset(
                            "select * from contactphonenumbers where contactid = " + tContact.ID +
                            " order by phoneorder", strConnectionName);
                        cPartyPhoneNumber tPhone;
                        while (!rsTemp.EndOfFile())
                        {
                            tPhone = new cPartyPhoneNumber();
                            tPhone.Description = rsTemp.Get_Fields_String("description");
                            tPhone.Extension = rsTemp.Get_Fields_String("extension");
                            tPhone.ID = rsTemp.Get_Fields_Int32("id");
                            tPhone.ParentID = rsTemp.Get_Fields_Int32("contactid");
                            tPhone.PhoneNumber = rsTemp.Get_Fields_String("PhoneNumber");
                            tPhone.PhoneOrder = rsTemp.Get_Fields_Int32("phoneorder");
                            tContact.PhoneNumbers.Add(tPhone);
                            rsTemp.MoveNext();
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
        }

        public FCCollection GetParties(string strSQL, bool boolPartiesOnly = false)
        {
            FCCollection GetParties = null;
            GetParties = null;
            ClearErrors();
            clsDRWrapper rsParties = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                FCCollection tColl = new FCCollection();
                cParty tParty;
                rsParties.OpenRecordset(strSQL, strConnectionName);

                while (!rsParties.EndOfFile())
                {
                    App.DoEvents();
                    tParty = new cParty();
                    tParty.CreatedBy = rsParties.Get_Fields_String("createdby");

                    if (Information.IsDate(rsParties.Get_Fields("Datecreated")))
                    {
                        tParty.DateCreated = rsParties.Get_Fields_DateTime("datecreated");
                    }

                    tParty.Designation = rsParties.Get_Fields_String("designation");
                    tParty.Email = rsParties.Get_Fields_String("email");
                    tParty.FirstName = rsParties.Get_Fields_String("firstname");
                    tParty.LastName = rsParties.Get_Fields_String("lastname");
                    tParty.MiddleName = rsParties.Get_Fields_String("middlename");
                    tParty.PartyGUID = rsParties.GUIDToString(rsParties.Get_Fields("partyguid"));
                    tParty.PartyType = rsParties.Get_Fields_Int32("PartyType");
                    tParty.WebAddress = rsParties.Get_Fields_String("webaddress");
                    tParty.ID = rsParties.Get_Fields_Int32("id");

                    if (!boolPartiesOnly)
                    {
                        FillParty(ref tParty);
                    }

                    if (lngLastError != 0 || strLastError != "")
                    {
                        return GetParties;
                    }

                    tColl.Add(tParty);
                    rsParties.MoveNext();
                }

                GetParties = tColl;

                return GetParties;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
            finally
            {
                rsParties.DisposeOf();
            }
            return GetParties;
        }

        public cPartyController()
        {
            strConnectionName = "CentralParties";
        }

        public string ConnectionName
        {
            set
            {
                strConnectionName = value;
            }
            get
            {
                string ConnectionName = "";
                ConnectionName = strConnectionName;
                return ConnectionName;
            }
        }

        public cParty FindFirstMatchingParty(ref cParty partyCompare)
        {
            cParty FindFirstMatchingParty = null;
            FindFirstMatchingParty = null;
            if (partyCompare != null)
            {
                string strSQL = "";
                if (partyCompare.PartyType == 0)
                {
                    // individual
                    strSQL = "select * from parties where partytype = 0 and ";
                    strSQL += $" firstname = '{modGlobalFunctions.EscapeQuotes(partyCompare.FirstName)}' and middlename = '{modGlobalFunctions.EscapeQuotes(partyCompare.MiddleName)}' and lastname = '{modGlobalFunctions.EscapeQuotes(partyCompare.LastName)}'";
                    strSQL += $" and designation = '{modGlobalFunctions.EscapeQuotes(partyCompare.Designation)}'";
                }
                else
                {
                    strSQL = "select * from parties where partytype = 1 and ";
                    strSQL += $"firstname = '{modGlobalFunctions.EscapeQuotes(partyCompare.FirstName)}'";
                }
                FCCollection tColl = new FCCollection();
                tColl = GetParties(strSQL, false);
                if (tColl != null)
                {
                    if (tColl.Count > 0)
                    {
                        FindFirstMatchingParty = tColl[1];
                    }
                }
            }
            return FindFirstMatchingParty;
        }

        public bool PartiesHaveSameAddress(ref cParty party1, ref cParty party2)
        {
            bool PartiesHaveSameAddress = false;

            if (party1 != null && party2 != null && party1.Addresses.Count > 0 && party2.Addresses.Count > 0)
            {
                foreach (cPartyAddress Addr1 in party1.Addresses)
                {
                    foreach (cPartyAddress Addr2 in party2.Addresses)
                    {
                        if (Strings.LCase(Addr1.Address1) == Strings.LCase(Addr2.Address1)
                            && Strings.LCase(Addr1.Address2) == Strings.LCase(Addr2.Address2)
                            && Strings.LCase(Addr1.Address3) == Strings.LCase(Addr2.Address3)
                            && Strings.LCase(Addr1.City) == Strings.LCase(Addr2.City)
                            && Strings.LCase(Addr1.State) == Strings.LCase(Addr2.State)
                            && Strings.LCase(Addr1.Zip) == Strings.LCase(Addr2.Zip))
                        {
                            PartiesHaveSameAddress = true;
                            return PartiesHaveSameAddress;
                        }
                    }
                }
            }
            PartiesHaveSameAddress = false;
            return PartiesHaveSameAddress;
        }

        public cPartyAddress GetPrimaryAddressByPartyID(int lngPartyID)
        {
            cPartyAddress GetPrimaryAddressByPartyID = null;
            string strSQL;
            strSQL = "Select top 1 * from Addresses where PartyID = " + FCConvert.ToString(lngPartyID) + " and AddressType = 'Primary'";
            using (clsDRWrapper rsTemp = new clsDRWrapper())
            {
                cPartyAddress tAddress;
                rsTemp.OpenRecordset(strSQL, "CentralParties");
                if (!rsTemp.EndOfFile())
                {
                    tAddress = new cPartyAddress();
                    tAddress.Address1 = rsTemp.Get_Fields_String("address1");
                    tAddress.Address2 = rsTemp.Get_Fields_String("address2");
                    tAddress.Address3 = rsTemp.Get_Fields_String("address3");
                    tAddress.AddressType = rsTemp.Get_Fields_String("addresstype");
                    tAddress.City = rsTemp.Get_Fields_String("city");
                    tAddress.Comment = rsTemp.Get_Fields_String("comment");
                    tAddress.Country = rsTemp.Get_Fields_String("country");
                    tAddress.EndDay = rsTemp.Get_Fields_Int32("endday");
                    tAddress.EndMonth = rsTemp.Get_Fields_Int32("endmonth");
                    tAddress.ID = rsTemp.Get_Fields_Int32("id");
                    tAddress.ModAccountID = rsTemp.Get_Fields_Int32("modaccountid");
                    tAddress.OverrideName = rsTemp.Get_Fields_String("overridename");
                    tAddress.PartyID = rsTemp.Get_Fields_Int32("partyid");
                    tAddress.ProgModule = rsTemp.Get_Fields_String("progmodule");
                    tAddress.Seasonal = rsTemp.Get_Fields_Boolean("seasonal");
                    tAddress.StartDay = rsTemp.Get_Fields_Int32("startday");
                    tAddress.StartMonth = rsTemp.Get_Fields_Int32("startmonth");
                    tAddress.State = rsTemp.Get_Fields_String("state");
                    tAddress.Zip = rsTemp.Get_Fields_String("zip");
                    GetPrimaryAddressByPartyID = tAddress;
                }
            }

            return GetPrimaryAddressByPartyID;
        }

        public void DeleteParty(int lngPartyID)
        {
            // On Error GoTo ErrorHandler
            ClearErrors();
            clsDRWrapper rsSave = new clsDRWrapper();
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                rsLoad.OpenRecordset($"select id from contacts where partyid = {FCConvert.ToString(lngPartyID)}", "CentralParties");
                while (!rsLoad.EndOfFile())
                {
                    //Application.DoEvents();
                    rsSave.Execute($"delete from contactphoneumbers where contactid = {rsLoad.Get_Fields_Int32("id")}", "CentralParties", false);
                    rsLoad.MoveNext();
                }
                rsSave.Execute($"delete from contacts where partyid = {FCConvert.ToString(lngPartyID)}", "CentralParties", false);
                rsSave.Execute($"delete from centralphonenumbers where partyid = {FCConvert.ToString(lngPartyID)}", "CentralParties", false);
                //Application.DoEvents();
                rsSave.Execute($"delete from centralcomments where partyid = {FCConvert.ToString(lngPartyID)}", "CentralParties", false);
                rsSave.Execute($"delete from addresses where partyid = {FCConvert.ToString(lngPartyID)}", "CentralParties", false);
                //Application.DoEvents();
                rsSave.Execute($"delete from parties where id = {FCConvert.ToString(lngPartyID)}", "CentralParties", false);
            }
            finally
            {
                rsSave.DisposeOf();
                rsLoad.DisposeOf();
            }
        }

        public string LastErrorMessage
        {
            get => strLastError;
        }
        public int LastError
        {
            get => lngLastError;
        }
    }
}
