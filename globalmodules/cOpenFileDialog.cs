﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Runtime.InteropServices;
using fecherFoundation;

namespace Global
{
	public class cOpenFileDialog
	{
		//=========================================================
		[DllImport("comdlg32.dll", EntryPoint = "GetOpenFileNameA")]
		private static extern int GetOpenFileName(ref OPENFILENAME pOpenfilename);

		[DllImport("comdlg32.dll", EntryPoint = "GetSaveFileNameA")]
		private static extern int GetSaveFileName(ref OPENFILENAME pOpenfilename);

		[StructLayout(LayoutKind.Sequential)]
		private struct OPENFILENAME
		{
			public int lStructSize;
			public int hwndOwner;
			public int hInstance;
			public string lpstrFilter;
			public string lpstrCustomFilter;
			public int nMaxCustFilter;
			public int nFilterIndex;
			public string lpstrFile;
			public int nMaxFile;
			public string lpstrFileTitle;
			public int nMaxFileTitle;
			public string lpstrInitialDir;
			public string lpstrTitle;
			public int flags;
			public short nFileOffset;
			public short nFileExtension;
			public string lpstrDefExt;
			public int lCustData;
			public int lpfnHook;
			public string lpTemplateName;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public OPENFILENAME(int unusedParam)
			{
				this.lStructSize = 0;
				this.hwndOwner = 0;
				this.hInstance = 0;
				this.lpstrFilter = string.Empty;
				this.lpstrCustomFilter = string.Empty;
				this.nMaxCustFilter = 0;
				this.nFilterIndex = 0;
				this.lpstrFile = string.Empty;
				this.nMaxFile = 0;
				this.lpstrFileTitle = string.Empty;
				this.nMaxFileTitle = 0;
				this.lpstrInitialDir = string.Empty;
				this.lpstrTitle = string.Empty;
				this.flags = 0;
				this.nFileOffset = 0;
				this.nFileExtension = 0;
				this.lpstrDefExt = string.Empty;
				this.lCustData = 0;
				this.lpfnHook = 0;
				this.lpTemplateName = string.Empty;
			}
		};

		private string strTitle = string.Empty;
		private string strInitialDir = string.Empty;
		private string strFilter = string.Empty;
		private int lngFlags;
		private int intFilterIndex;
		private string strFileName = string.Empty;
		private string[] strFileNames = null;
		private bool boolMultiselect;
		private string strDefaultExtension = string.Empty;

		public string DefaultExtension
		{
			set
			{
				strDefaultExtension = value;
			}
			get
			{
				string DefaultExtension = "";
				DefaultExtension = strDefaultExtension;
				return DefaultExtension;
			}
		}

		public int Options
		{
			set
			{
				lngFlags = value;
			}
			get
			{
				int Options = 0;
				Options = lngFlags;
				return Options;
			}
		}

		public bool MultiSelect
		{
			set
			{
				boolMultiselect = value;
			}
			get
			{
				bool MultiSelect = false;
				MultiSelect = boolMultiselect;
				return MultiSelect;
			}
		}

		public string Title
		{
			set
			{
				strTitle = value;
			}
			get
			{
				string Title = "";
				Title = strTitle;
				return Title;
			}
		}

		public string InitialDirectory
		{
			set
			{
				strInitialDir = value;
			}
			get
			{
				string InitialDirectory = "";
				InitialDirectory = strInitialDir;
				return InitialDirectory;
			}
		}

		public string Filter
		{
			set
			{
				strFilter = value;
			}
			get
			{
				string Filter = "";
				Filter = strFilter;
				return Filter;
			}
		}

		public short FilterIndex
		{
			set
			{
				value = FCConvert.ToInt16(intFilterIndex);
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short FilterIndex = 0;
				FilterIndex = FCConvert.ToInt16(intFilterIndex);
				return FilterIndex;
			}
		}

		public string FileName
		{
			set
			{
				strFileName = value;
			}
			get
			{
				string FileName = "";
				FileName = strFileName;
				return FileName;
			}
		}

		public string[] FileNames
		{
			get
			{
				string[] FileNames = null;
				FileNames = strFileNames;
				return FileNames;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As int
		public int ShowDialog()
		{
			int ShowDialog = 0;
			// ByRef own As Form)
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			OPENFILENAME openfile = new OPENFILENAME(0);
			openfile.lStructSize = Marshal.SizeOf(openfile);
			// openfile.hwndOwner = own.hwnd
			//FC:TODO:AM
			//openfile.hInstance = Support.GetHInstance();
			if (strFilter != "")
			{
				// sFilter = "Batch Files (*.bat)" & Chr(0) & "*.BAT" & Chr(0)
				openfile.lpstrFilter = strFilter;
				if (intFilterIndex > 0)
				{
					openfile.nFilterIndex = intFilterIndex;
				}
			}
			if (!boolMultiselect)
			{
				openfile.flags = lngFlags;
			}
			else
			{
				//FC:TODO:PJ File-Handling (Options)
				//openfile.flags = lngFlags+vbPorterConverter.cdlOFNAllowMultiselect;
				openfile.flags = lngFlags;
			}
			openfile.lpstrFile = Strings.Space(254);
			if (strFileName != "")
			{
				openfile.lpstrFile = strFileName + Strings.Space(254 - strFileName.Length);
			}
			else
			{
				openfile.lpstrFile = Strings.Space(254);
			}
			openfile.lpstrDefExt = strDefaultExtension;
			openfile.nMaxFile = 255;
			openfile.lpstrFileTitle = Strings.Space(254);
			openfile.nMaxFileTitle = 255;
			openfile.lpstrInitialDir = strInitialDir;
			openfile.lpstrTitle = strTitle;
			int lngReturn;
			lngReturn = GetOpenFileName(ref openfile);
			if (lngReturn != 0)
			{
				string strTemp = "";
				strTemp = Strings.Trim(openfile.lpstrFile);
				if (Strings.Right(strTemp, 1) == FCConvert.ToString(Convert.ToChar(0)))
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				if (Strings.Right(strTemp, 1) == FCConvert.ToString(Convert.ToChar(0)))
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
				}
				int intSep = 0;
				intSep = Strings.InStr(1, strTemp, FCConvert.ToString(Convert.ToChar(0)), CompareConstants.vbBinaryCompare);
				if (intSep > 0)
				{
					string[] strTemps = null;
					string strPath = "";
					strTemps = Strings.Split(strTemp, FCConvert.ToString(Convert.ToChar(0)), -1, CompareConstants.vbBinaryCompare);
					strPath = strTemps[0];
					strFileNames = new string[Information.UBound(strTemps, 1) + 1];
					int x;
					for (x = 1; x <= Information.UBound(strTemps, 1); x++)
					{
						strFileNames[x - 1] = strTemps[0] + "\\" + strTemps[x];
					}
					strFileName = strFileNames[0];
				}
				else
				{
					strFileName = strTemp;
					strFileNames = Strings.Split(strTemp, FCConvert.ToString(Convert.ToChar(0)), 1, CompareConstants.vbBinaryCompare);
				}
				ShowDialog = lngReturn;
			}
			return ShowDialog;
		}
	}
}
