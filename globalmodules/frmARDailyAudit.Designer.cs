﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWAR0000;

namespace Global
{
	/// <summary>
	/// Summary description for frmARDailyAudit.
	/// </summary>
	partial class frmARDailyAudit : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAudit;
		public fecherFoundation.FCComboBox cmbAlignment;
		public fecherFoundation.FCLabel lblAlignment;
		public fecherFoundation.FCFrame fraPreAudit;
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCButton cmdPassword;
		public fecherFoundation.FCTextBox txtAuditPassword;
		public fecherFoundation.FCLabel lblAuditPassword;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.cmbAudit = new fecherFoundation.FCComboBox();
			this.cmbAlignment = new fecherFoundation.FCComboBox();
			this.lblAlignment = new fecherFoundation.FCLabel();
			this.fraPreAudit = new fecherFoundation.FCFrame();
			this.cmdDone = new fecherFoundation.FCButton();
			this.cmdPassword = new fecherFoundation.FCButton();
			this.txtAuditPassword = new fecherFoundation.FCTextBox();
			this.lblAuditPassword = new fecherFoundation.FCLabel();
			//this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPreAudit)).BeginInit();
			this.fraPreAudit.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPassword)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 359);
			this.BottomPanel.Size = new System.Drawing.Size(610, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraPreAudit);
			this.ClientArea.Controls.Add(this.cmdDone);
			this.ClientArea.Size = new System.Drawing.Size(610, 299);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(610, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(211, 30);
			this.HeaderText.Text = "Daily Audit Report";
			// 
			// cmbAudit
			// 
			this.cmbAudit.AutoSize = false;
			this.cmbAudit.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAudit.FormattingEnabled = true;
			this.cmbAudit.Items.AddRange(new object[] {
				"Audit Preview",
				"Audit Report and Close Out"
			});
			this.cmbAudit.Location = new System.Drawing.Point(20, 30);
			this.cmbAudit.Name = "cmbAudit";
			this.cmbAudit.Size = new System.Drawing.Size(389, 40);
			this.cmbAudit.TabIndex = 2;
			// 
			// cmbAlignment
			// 
			this.cmbAlignment.AutoSize = false;
			this.cmbAlignment.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAlignment.FormattingEnabled = true;
			this.cmbAlignment.Items.AddRange(new object[] {
				"Portrait",
				"Landscape"
			});
			this.cmbAlignment.Location = new System.Drawing.Point(184, 95);
			this.cmbAlignment.Name = "cmbAlignment";
			this.cmbAlignment.Size = new System.Drawing.Size(225, 40);
			this.cmbAlignment.TabIndex = 0;
			// 
			// lblAlignment
			// 
			this.lblAlignment.AutoSize = true;
			this.lblAlignment.Location = new System.Drawing.Point(20, 109);
			this.lblAlignment.Name = "lblAlignment";
			this.lblAlignment.Size = new System.Drawing.Size(92, 15);
			this.lblAlignment.TabIndex = 1;
			this.lblAlignment.Text = "ORIENTATION";
			// 
			// fraPreAudit
			// 
			this.fraPreAudit.Controls.Add(this.cmbAlignment);
			this.fraPreAudit.Controls.Add(this.lblAlignment);
			this.fraPreAudit.Controls.Add(this.cmbAudit);
			this.fraPreAudit.Location = new System.Drawing.Point(30, 30);
			this.fraPreAudit.Name = "fraPreAudit";
			this.fraPreAudit.Size = new System.Drawing.Size(428, 165);
			this.fraPreAudit.TabIndex = 9;
			this.fraPreAudit.Text = "Select Audit Process";
			this.fraPreAudit.Visible = false;
			// 
			// cmdDone
			// 
			this.cmdDone.AppearanceKey = "acceptButton";
			this.cmdDone.Location = new System.Drawing.Point(30, 252);
			this.cmdDone.Name = "cmdDone";
			this.cmdDone.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdDone.Size = new System.Drawing.Size(141, 40);
			this.cmdDone.TabIndex = 4;
			this.cmdDone.Text = "Print Preview";
			this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
			// 
			// cmdPassword
			// 
			this.cmdPassword.AppearanceKey = "toolbarButton";
			this.cmdPassword.Location = new System.Drawing.Point(51, 83);
			this.cmdPassword.Name = "cmdPassword";
			this.cmdPassword.Size = new System.Drawing.Size(78, 23);
			this.cmdPassword.TabIndex = 6;
			this.cmdPassword.Text = "Process";
			// 
			// txtAuditPassword
			// 
			this.txtAuditPassword.AutoSize = false;
			this.txtAuditPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtAuditPassword.Location = new System.Drawing.Point(32, 55);
			this.txtAuditPassword.Name = "txtAuditPassword";
			this.txtAuditPassword.Size = new System.Drawing.Size(112, 40);
			this.txtAuditPassword.TabIndex = 5;
			// 
			// lblAuditPassword
			// 
			this.lblAuditPassword.Location = new System.Drawing.Point(38, 16);
			this.lblAuditPassword.Name = "lblAuditPassword";
			this.lblAuditPassword.Size = new System.Drawing.Size(116, 31);
			this.lblAuditPassword.TabIndex = 8;
			this.lblAuditPassword.Text = "PLEASE ENTER YOUR AUDIT PASSWORD";
			// 
			// MainMenu1
			// 
			//this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
			//this.mnuFile});
			//this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePrint.Text = "Print Preview";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 2;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// frmARDailyAudit
			// 
			this.ClientSize = new System.Drawing.Size(610, 467);
			this.KeyPreview = true;
			//this.Menu = this.MainMenu1;
			this.Name = "frmARDailyAudit";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Daily Audit Report";
			this.Load += new System.EventHandler(this.frmARDailyAudit_Load);
			this.Activated += new System.EventHandler(this.frmARDailyAudit_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmARDailyAudit_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPreAudit)).EndInit();
			this.fraPreAudit.ResumeLayout(false);
			this.fraPreAudit.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPassword)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
