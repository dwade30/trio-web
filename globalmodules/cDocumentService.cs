﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using SharedApplication.CentralDocuments;

namespace Global
{
	//public class cDocumentService
	//{
	//	//=========================================================
	//	// This class is a wrapper to help transition between the old linked documents that point to a file and the new documents database that contain the actual document
	//	// This class helps mitigate the need to deal with both types at once
	//	//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
	//	//private cCentralDocumentController cenDocCont = new cCentralDocumentController();
	//	private cCentralDocumentController cenDocCont_AutoInitialized;

	//	private cCentralDocumentController cenDocCont
	//	{
	//		get
	//		{
	//			if (cenDocCont_AutoInitialized == null)
	//			{
	//				cenDocCont_AutoInitialized = new cCentralDocumentController();
	//			}
	//			return cenDocCont_AutoInitialized;
	//		}
	//		set
	//		{
	//			cenDocCont_AutoInitialized = value;
	//		}
	//	}

	//	private string strLinkedDB = string.Empty;
	//	private string strLinkReferenceType = string.Empty;
	//	private int lngReferenceID;
	//	private bool boolLinkTypeisInteger;
	//	private string strLastError = "";
	//	private int lngLastError;

	//	public string LastErrorMessage
	//	{
	//		get
	//		{
	//			string LastErrorMessage = "";
	//			LastErrorMessage = strLastError;
	//			return LastErrorMessage;
	//		}
	//	}

	//	public int LastErrorNumber
	//	{
	//		get
	//		{
	//			int LastErrorNumber = 0;
	//			LastErrorNumber = lngLastError;
	//			return LastErrorNumber;
	//		}
	//	}
	//	// vbPorter upgrade warning: 'Return' As Variant --> As bool
	//	public bool HadError
	//	{
	//		get
	//		{
	//			bool HadError = false;
	//			HadError = lngLastError != 0;
	//			return HadError;
	//		}
	//	}

	//	public void ClearErrors()
	//	{
	//		strLastError = "";
	//		lngLastError = 0;
	//	}

	//	private void SetError(int lngError, string strError)
	//	{
	//		strLastError = strError;
	//		lngLastError = lngError;
	//	}

	//	public bool LinkTypeIsInteger
	//	{
	//		set
	//		{
	//			boolLinkTypeisInteger = value;
	//		}
	//		get
	//		{
	//			bool LinkTypeIsInteger = false;
	//			LinkTypeIsInteger = boolLinkTypeisInteger;
	//			return LinkTypeIsInteger;
	//		}
	//	}

	//	public int LinkReferenceID
	//	{
	//		set
	//		{
	//			lngReferenceID = value;
	//		}
	//		get
	//		{
	//			int LinkReferenceID = 0;
	//			LinkReferenceID = lngReferenceID;
	//			return LinkReferenceID;
	//		}
	//	}

	//	public string LinkReferenceType
	//	{
	//		set
	//		{
	//			strLinkReferenceType = value;
	//		}
	//		get
	//		{
	//			string LinkReferenceType = "";
	//			LinkReferenceType = strLinkReferenceType;
	//			return LinkReferenceType;
	//		}
	//	}

	//	public string LinkedDB
	//	{
	//		set
	//		{
	//			strLinkedDB = value;
	//		}
	//		get
	//		{
	//			string LinkedDB = "";
	//			LinkedDB = strLinkedDB;
	//			return LinkedDB;
	//		}
	//	}

	//	public cCentralDocument GetCentralDocument(int lngId)
	//	{
	//		cCentralDocument GetCentralDocument = null;
	//		GetCentralDocument = cenDocCont.GetCentralDocument(lngId);
	//		return GetCentralDocument;
	//	}

	//	public cGenericCollection GetDocumentList(string DataGroup, string referenceType, int referenceID, string altReference)
	//	{
	//		cGenericCollection ret = null;
	//		ClearErrors();
	//		try
	//		{
	//			// On Error GoTo ErrorHandler
	//			string strSQL = "";
	//			clsDRWrapper rsLoad = new clsDRWrapper();
	//			cGenericCollection collDocuments = new cGenericCollection();

 //               //if (strLinkedDB != "")
	//			//{
	//			//	if (boolLinkTypeisInteger)
	//			//	{
	//			//		strSQL = "select * from LinkedDocuments where DocReferenceType = " + strLinkReferenceType + " and DocReferenceID = " + FCConvert.ToString(referenceID) + " order by filename ";
	//			//	}
	//			//	else
	//			//	{
	//			//		strSQL = "select * from LinkedDocuments where DocReferenceType = '" + strLinkReferenceType + "' and DocReferenceID = " + FCConvert.ToString(referenceID) + " order by filename ";
	//			//	}
	//			//	rsLoad.OpenRecordset(strSQL, strLinkedDB);
	//			//	while (!rsLoad.EndOfFile())
	//			//	{
	//			//		dInfo = new cDocumentInfo();
	//			//		dInfo.IsCentralDocument = false;
	//			//		dInfo.CentralDocumentInfo.ID = rsLoad.Get_Fields_Int32("ID");
	//			//		dInfo.CentralDocumentInfo.ItemDescription = rsLoad.Get_Fields_String("Description");
	//			//		dInfo.CentralDocumentInfo.ItemName = rsLoad.Get_Fields_String("FileName");
	//			//		dInfo.CentralDocumentInfo.ReferenceType = rsLoad.Get_Fields_String("DocReferenceType");
	//			//		dInfo.CentralDocumentInfo.ReferenceID = rsLoad.Get_Fields_Int32("DocReferenceID");
	//			//		dInfo.CentralDocumentInfo.MediaType = Strings.LCase(Strings.Right("   " + rsLoad.Get_Fields_String("filename"), 3)) != "pdf" ? "Image" : "PDF";
	//			//		collDocuments.AddItem(dInfo);
	//			//		rsLoad.MoveNext();
	//			//	}
	//			//}

	//			cGenericCollection collCenDocs;
	//			if (referenceID == 0 && altReference != "")
	//			{
	//				collCenDocs = cenDocCont.GetCentralDocumentInfoesByAltReference(DataGroup, referenceType, altReference);
	//				if (cenDocCont.HadError)
	//				{
	//					return ret;
	//				}
	//			}
	//			else
	//			{
	//				collCenDocs = cenDocCont.GetCentralDocumentInfoesByReference(DataGroup, ref referenceType, referenceID);
	//				if (cenDocCont.HadError)
	//				{
	//					return ret;
	//				}
	//			}
	//			if (collCenDocs != null)
	//			{
	//				collCenDocs.MoveFirst();
	//				while (collCenDocs.IsCurrent())
	//				{
	//					var dInfo = new cDocumentInfo();
	//					dInfo.IsCentralDocument = true;
	//					dInfo.CentralDocumentInfo = (cCentralDocumentInfo)collCenDocs.GetCurrentItem();
	//					collDocuments.AddItem(dInfo);
	//					collCenDocs.MoveNext();
	//				}
	//			}
	//			ret = collDocuments;
	//			return ret;
	//		}
	//		catch (Exception ex)
	//		{
	//			// ErrorHandler:
	//			SetError(Information.Err(ex).Number, Information.Err(ex).Description);
	//		}
	//		return ret;
	//	}

	//	public string SavePDFDocumentAsTempFile(ref cCentralDocument cenDoc, string strPath, string strFileName)
	//	{
	//		string SavePDFDocumentAsTempFile = "";
	//		ClearErrors();
	//		try
	//		{
	//			// On Error GoTo ErrorHandler
	//			if (!(cenDoc == null))
	//			{
	//				if (Directory.Exists(strPath))
	//				{
	//					string strTempPath = "";
	//					strTempPath = strPath;
	//					if (strTempPath != "")
	//					{
	//						if (Strings.Right(strTempPath, 1) != "\\")
	//						{
	//							strTempPath += "\\";
	//						}
	//					}
	//					string strFile = "";
	//					if (strFileName == "")
	//					{
	//						strFile = Path.GetRandomFileName();
	//						strFile = "Temp_" + strFile + ".pdf";
	//					}
	//					else
	//					{
	//						strFile = strFileName;
	//					}
	//					int fileHandleNum;
	//					fileHandleNum = FCFileSystem.FreeFile();
	//					FCFileSystem.FileOpen(fileHandleNum, strTempPath + strFile, fecherFoundation.VisualBasicLayer.OpenMode.Binary, (fecherFoundation.VisualBasicLayer.OpenAccess)(-1), (fecherFoundation.VisualBasicLayer.OpenShare)(-1), -1);
	//					FCFileSystem.FilePut(fileHandleNum, cenDoc.ItemData, 1, true, false);
	//					FCFileSystem.FileClose(fileHandleNum);
	//					SavePDFDocumentAsTempFile = strTempPath + strFile;
	//				}
	//			}
	//			return SavePDFDocumentAsTempFile;
	//		}
	//		catch (Exception ex)
	//		{
	//			// ErrorHandler:
	//			SetError(Information.Err(ex).Number, Information.Err(ex).Description);
	//		}
	//		return SavePDFDocumentAsTempFile;
	//	}

	//	public void DeleteDocument(ref cDocumentInfo dInfo)
	//	{
	//		ClearErrors();
	//		try
	//		{
	//			// On Error GoTo ErrorHandler
	//			if (!(dInfo == null))
	//			{
	//				if (dInfo.IsCentralDocument)
	//				{
	//					cenDocCont.DeleteCentralDocument(dInfo.CentralDocumentInfo.ID);
	//					if (cenDocCont.HadError)
	//					{
	//						SetError(cenDocCont.LastErrorNumber, cenDocCont.LastErrorMessage);
	//					}
	//				}
	//				//else
	//				//{
	//				//	clsDRWrapper rsSave = new clsDRWrapper();
	//				//	rsSave.Execute("delete from LinkedDocuments where id = " + dInfo.CentralDocumentInfo.ID, strLinkedDB);
	//				//}
	//			}
	//			return;
	//		}
	//		catch (Exception ex)
	//		{
	//			// ErrorHandler:
	//			SetError(Information.Err(ex).Number, Information.Err(ex).Description);
	//		}
	//	}
	//}
}
