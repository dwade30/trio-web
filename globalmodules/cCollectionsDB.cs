﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using SharedApplication.Extensions;
using SharedApplication.PersonalProperty.Models;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	public class cCollectionsDB
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;
		const string DatabaseToUpdate = "Collections";

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cVersionInfo GetVersion()
		{
			cVersionInfo GetVersion = null;
			try
			{
				// On Error GoTo ErrorHandler
				ClearErrors();
				cVersionInfo tVer = new cVersionInfo();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from dbversion", DatabaseToUpdate);
				if (!rsLoad.EndOfFile())
				{
					tVer.Build = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Build"));
					tVer.Major = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Major"));
					tVer.Minor = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Minor"));
					tVer.Revision = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Revision"));
				}
				GetVersion = tVer;
				return GetVersion;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = ex.GetBaseException().Message;
				lngLastError = Information.Err(ex).Number;
			}
			return GetVersion;
		}

		public void SetVersion(cVersionInfo nVersion)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (!(nVersion == null))
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from dbversion", DatabaseToUpdate);
					if (rsSave.EndOfFile())
					{
						rsSave.AddNew();
					}
					else
					{
						rsSave.Edit();
					}
					rsSave.Set_Fields("Major", nVersion.Major);
					rsSave.Set_Fields("Minor", nVersion.Minor);
					rsSave.Set_Fields("Revision", nVersion.Revision);
					rsSave.Set_Fields("Build", nVersion.Build);
					rsSave.Update();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = ex.GetBaseException().Message;
				lngLastError = Information.Err(ex).Number;
			}
		}

		private bool FieldExists(string strTable, string strField, string strDB)
		{
			bool FieldExists = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL;
				clsDRWrapper rsLoad = new clsDRWrapper();
				strSQL = "Select column_name from information_schema.columns where column_name = '" + strField + "' and table_name = '" + strTable + "'";
				rsLoad.OpenRecordset(strSQL, strDB);
				if (!rsLoad.EndOfFile())
				{
					FieldExists = true;
				}
				return FieldExists;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return FieldExists;
		}

		private bool TableExists(string strTable, string strDB)
		{
			bool TableExists = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL;
				clsDRWrapper rsLoad = new clsDRWrapper();
				strSQL = "select * from sys.tables where name = '" + strTable + "' and type = 'U'";
				rsLoad.OpenRecordset(strSQL, strDB);
				if (!rsLoad.EndOfFile())
				{
					TableExists = true;
				}
				return TableExists;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return TableExists;
		}

		private string GetTableCreationHeaderSQL(string strTableName)
		{
			string GetTableCreationHeaderSQL = "";
			string strSQL;
			strSQL = "if not exists (select * from information_schema.tables where table_name = N'" + strTableName + "') " + "\r\n";
			strSQL += "Create Table [dbo].[" + strTableName + "] (";
			strSQL += "[ID] [int] IDENTITY (1,1) NOT NULL,";
			GetTableCreationHeaderSQL = strSQL;
			return GetTableCreationHeaderSQL;
		}

		private string GetTablePKSql(string strTableName)
		{
			string GetTablePKSql = "";
			string strSQL;
			strSQL = " Constraint [PK_" + strTableName + "] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
			GetTablePKSql = strSQL;
			return GetTablePKSql;
		}

		private string GetDataType(string strTableName, string strFieldName, string strDB)
		{
			string GetDataType = "";
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			strSQL = "select data_type from information_schema.columns where table_name = '" + strTableName + "' and column_name = '" + strFieldName + "'";
			rsLoad.OpenRecordset(strSQL, strDB);
			if (!rsLoad.EndOfFile())
			{
				// TODO: Field [data_type] not found!! (maybe it is an alias?)
				GetDataType = FCConvert.ToString(rsLoad.Get_Fields("data_type"));
			}
			return GetDataType;
		}

		public bool CheckVersion()
		{
			bool CheckVersion = false;
			ClearErrors();

            clsDRWrapper rsTest = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                cVersionInfo nVer = new cVersionInfo();
                cVersionInfo tVer = new cVersionInfo();
                cVersionInfo cVer;
                bool boolNeedUpdate /*unused?*/;

                if (!rsTest.DBExists(DatabaseToUpdate))
                {
                    return CheckVersion;
                }

                cVer = GetVersion();

                if (cVer == null)
                {
                    cVer = new cVersionInfo();
                }

                nVer.Major = 1;

                // default to 1.0.0.0
                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (OldCheckDatabaseStructure())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 2;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddThirdAddressFieldToBills())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 3;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddTaxServiceEmail())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }


                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 0;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddAddLienCharge())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddIdentifiersToPayments())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 2;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddPreviousInterestDate())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                //tVer.Major = 6;
                //tVer.Minor = 0;
                //tVer.Revision = 3;
                //tVer.Build = 0;
                //if (tVer.IsNewer(cVer))
                //{
                //    if (AddChargedInterestIdentifier())
                //    {
                //        nVer.Copy(tVer);
                //        if (cVer.IsOlder(nVer))
                //        {
                //            SetVersion(nVer);
                //        }
                //    }
                //    else
                //    {
                //        return false;
                //    }
                //}

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 4;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (FillTransactionIdentifier())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 5;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (RemoveChargedInterestIdentifier())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 6;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddBatchIdentifier())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }


                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 7;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddBillTypeFieldToBatchRecover())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 8;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddImportedPayments())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 9;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddImportIdToBatchRecover())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 10;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddPaymentIdentifierToImportedPayments())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 11;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddExternalPaymentIdentifierToPaymentRec())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 12;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddPaymentIdentifierToBatchRecover())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 13;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddProcessedToBatchRecover())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 14;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (Add30DNOfficersPhone())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }


                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 15;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddCorrelationIdentifier())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 16;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddAddLienCharge())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 17;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (CheckDiscountPercentDataType())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 18;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AddRealEstatePersonalPropertyBillingMasterViews())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 19;
                tVer.Build = 0;

                //if (tVer.IsNewer(cVer))
                //{
                //    if (AlterRealEstateBillingMasterView())
                //    {
                //        nVer.Copy(tVer);

                //        if (cVer.IsOlder(nVer))
                //        {
                //            SetVersion(nVer);
                //        }
                //    }
                //    else
                //    {
                //        return false;
                //    }
                //}

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision += 1;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddAccountPartyBillViews())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision += 1;
                tVer.Build = 0;

                if (tVer.IsNewer(cVer))
                {
                    if (AlterRealEstateBillingMasterView())
                    {
                        nVer.Copy(tVer);

                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                strLastError = ex.GetBaseException().Message;
                lngLastError = Information.Err(ex).Number;
            }
            finally
            {
                rsTest.DisposeOf();
            }
			return CheckVersion;
		}

        private bool AlterRealEstateBillingMasterView()
        {
            clsDRWrapper rsUpdate = new clsDRWrapper();

            try
            {
                if ( rsUpdate.DBExists("RealEstate"))
                {
                    rsUpdate.OpenRecordset("select* FROM sys.views where name = 'RealEstateMasterBillingMasterView'",
                        "Collections");
                    if (!rsUpdate.EndOfFile())
                    {
                        rsUpdate.Execute("Drop View [dbo].[RealEstateMasterBillingMasterView]", "Collections");
                    }

                    var sql = "CREATE VIEW [dbo].[RealEstateMasterBillingMasterView] as "
                              + "SELECT BillingMaster.ID, BillingMaster.Account, BillingMaster.BillingType, BillingMaster.BillingYear, BillingMaster.Name1, BillingMaster.Name2, BillingMaster.Address1, BillingMaster.Address2, "
                              + "BillingMaster.Address3, BillingMaster.MapLot, BillingMaster.streetnumber, BillingMaster.Apt, BillingMaster.StreetName, BillingMaster.LandValue, BillingMaster.BuildingValue, "
                              + "BillingMaster.ExemptValue, BillingMaster.TranCode, BillingMaster.LandCode, BillingMaster.BuildingCode, BillingMaster.OtherCode1, BillingMaster.OtherCode2, BillingMaster.TaxDue1, "
                              + "BillingMaster.TaxDue2, BillingMaster.TaxDue3, BillingMaster.TaxDue4, BillingMaster.LienRecordNumber, BillingMaster.PrincipalPaid, BillingMaster.InterestPaid, BillingMaster.InterestCharged, "
                              + "BillingMaster.DemandFees, BillingMaster.DemandFeesPaid, BillingMaster.InterestAppliedThroughDate, BillingMaster.RateKey, BillingMaster.TransferFromBillingDateFirst, "
                              + "BillingMaster.TransferFromBillingDateLast, BillingMaster.WhetherBilledBefore, BillingMaster.OwnerGroup, BillingMaster.Category1, BillingMaster.Category2, BillingMaster.Category3, "
                              + "BillingMaster.Category4, BillingMaster.Category5, BillingMaster.Category6, BillingMaster.Category7, BillingMaster.Category8, BillingMaster.Category9, BillingMaster.Acres, "
                              + "BillingMaster.HomesteadExemption, BillingMaster.OtherExempt1, BillingMaster.OtherExempt2, BillingMaster.BookPage, BillingMaster.PPAssessment, BillingMaster.Exempt1, "
                              + "BillingMaster.Exempt2, BillingMaster.Exempt3, BillingMaster.LienStatusEligibility, BillingMaster.LienProcessStatus, BillingMaster.Copies, BillingMaster.CertifiedMailNumber, "
                              + "BillingMaster.AbatementPaymentMade, BillingMaster.TGMixedAcres, BillingMaster.TGSoftAcres, BillingMaster.TGHardAcres, BillingMaster.TGMixedValue, BillingMaster.TGSoftValue, "
                              + "BillingMaster.TGHardValue, BillingMaster.Ref2, BillingMaster.Ref1, BillingMaster.Zip, BillingMaster.ShowRef1, BillingMaster.LienProcessExclusion, BillingMaster.IMPBTrackingNumber, "
                              + "BillingMaster.MailingAddress3, BillingMaster.PreviousInterestAppliedDate, Master.ID AS MasterId, Master.LastBldgVal, "
                              + "Master.LastLandVal, Master.hlUpdate, Master.HIUpdCode, Master.HLCompValBldg, Master.HLCompValLand, Master.HLValBldg, "
                              + "Master.HIValBldgCode, Master.HLValLand, Master.HIValLandCode, Master.HLAlt1Bldg, Master.HIAlt1BldgCode, Master.HLAlt2Bldg, "
                              + "Master.HIAlt2BldgCode, Master.PINeighborhood, Master.PIStreetCode, Master.PIXCoord, Master.PIYCoord, Master.PIZone, Master.PISecZone, "
                              + "Master.PITopography1, Master.PITopography2, Master.PIUtilities1, Master.PIUtilities2, Master.PIStreet, Master.PIOpen1, Master.PIOpen2, "
                              + "Master.PISalePrice, Master.PISaleType, Master.PISaleFinancing, Master.PISaleVerified, Master.PISaleValidity, Master.HLDOLExemption, "
                              + "Master.PILand1Type, Master.PILand1UnitsA, Master.PILand1UnitsB, Master.PILand1Inf, Master.PILand1InfCode, Master.PILand2Type, "
                              + "Master.PILand2UnitsA, Master.PILand2UnitsB, Master.PILand2Inf, Master.PILand2InfCode, Master.PILand3Type, Master.PILand3UnitsA, "
                              + "Master.PILand3UnitsB, Master.PILand3Inf, Master.PILand3InfCode, "
                              + "Master.PILand4Type, Master.PILand4UnitsA, Master.PILand4UnitsB, "
                              + "Master.PILand4Inf, Master.PILand4InfCode, Master.PILand5Type, "
                              + "Master.PILand5UnitsA, Master.PILand5UnitsB, Master.PILand5Inf, "
                              + "Master.PILand5InfCode, Master.PILand6Type, Master.PILand6UnitsA, "
                              + "Master.PILand6UnitsB, Master.PILand6Inf, Master.PILand6InfCode, "
                              + "Master.PILand7Type, Master.PILand7UnitsA, Master.PILand7UnitsB, "
                              + "Master.PILand7Inf, Master.PILand7InfCode, Master.PIAcres, "
                              + "Master.RSPropertyCode, Master.RSDwellingCode, Master.RSOutbuildingCode, "
                              + "Master.OwnerPartyID, Master.SecOwnerPartyID, Master.RSLocNumAlph, "
                              + "Master.RSLocApt, Master.RSLocStreet, Master.RSRef1, Master.RSRef2, "
                              + "Master.RILandCode, Master.RIBldgCode, Master.RLLandVal, Master.RLBldgVal, "
                              + "Master.CorrExemption, Master.RLExemption, Master.RIExemptCD1, "
                              + "Master.RIExemptCD2, Master.RIExemptCD3, Master.RITranCode, "
                              + "Master.RSMapLot, Master.RSPreviousMaster, Master.RSAccount, "
                              + "Master.RSCard, Master.RSDeleted, Master.SaleDate, Master.TaxAcquired, "
                              + "Master.RSSoft, Master.RSHard, Master.RSMixed, Master.RSSoftValue, "
                              + "Master.RSHardValue, Master.RSMixedValue, Master.HomesteadValue, "
                              + "Master.InBankruptcy, Master.ExemptVal1, Master.ExemptVal2, "
                              + "Master.ExemptVal3, Master.EntranceCode, Master.InformationCode, "
                              + "Master.DateInspected, Master.RSOtherValue, Master.RSOther, "
                              + "Master.HasHomestead, Master.CurrLandExempt, Master.CurrBldgExempt, "
                              + "Master.LandExempt, Master.BldgExempt, Master.ExemptPct1, "
                              + "Master.ExemptPct2, Master.ExemptPct3, Master.CurrHomesteadValue, "
                              + "Master.CurrHasHomestead, Master.CurrExemptVal1, Master.CurrExemptVal2, "
                              + "Master.CurrExemptVal3, Master.RevocableTrust, Master.AccountLocked, "
                              + "Master.ReasonAccountLocked, Master.DateCreated, Master.TADate, "
                              + "Master.PropertyCode, Master.ZoneOverride, Master.CardID, Master.AccountID, "
                              + "Master.DeedName1, Master.DeedName2, BookPage.Book as LinkedBook, BookPage.Page as LinkedPage "
                              + "FROM BillingMaster INNER JOIN " + rsUpdate.Get_GetFullDBName("RealEstate") + ".dbo.Master "
                              + "ON BillingMaster.Account = Master.RSAccount AND Master.RSCard = 1 AND BillingMaster.BillingType = 'RE' "
                              + "LEFT JOIN " + rsUpdate.Get_GetFullDBName("RealEstate") + ".dbo.BookPage "
                              + "ON Master.RSAccount = BookPage.Account AND BookPage.Card = 1";


                    rsUpdate.Execute(sql, "Collections");
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddRealEstatePersonalPropertyBillingMasterViews()
        {
            clsDRWrapper rsUpdate = new clsDRWrapper();

            try
            {
                if (StaticSettings.gGlobalActiveModuleSettings.RealEstateIsActive)
                {
                    rsUpdate.OpenRecordset("select* FROM sys.views where name = 'RealEstateMasterBillingMasterView'",
                        "Collections");
                    if (!rsUpdate.EndOfFile())
                    {
                        rsUpdate.Execute("Drop View [dbo].[RealEstateMasterBillingMasterView]", "Collections");
                    }

                    var sql = "CREATE VIEW [dbo].[RealEstateMasterBillingMasterView] as "
                              + "SELECT BillingMaster.ID, BillingMaster.Account, BillingMaster.BillingType, BillingMaster.BillingYear, BillingMaster.Name1, BillingMaster.Name2, BillingMaster.Address1, BillingMaster.Address2, "
                              + "BillingMaster.Address3, BillingMaster.MapLot, BillingMaster.streetnumber, BillingMaster.Apt, BillingMaster.StreetName, BillingMaster.LandValue, BillingMaster.BuildingValue, "
                              + "BillingMaster.ExemptValue, BillingMaster.TranCode, BillingMaster.LandCode, BillingMaster.BuildingCode, BillingMaster.OtherCode1, BillingMaster.OtherCode2, BillingMaster.TaxDue1, "
                              + "BillingMaster.TaxDue2, BillingMaster.TaxDue3, BillingMaster.TaxDue4, BillingMaster.LienRecordNumber, BillingMaster.PrincipalPaid, BillingMaster.InterestPaid, BillingMaster.InterestCharged, "
                              + "BillingMaster.DemandFees, BillingMaster.DemandFeesPaid, BillingMaster.InterestAppliedThroughDate, BillingMaster.RateKey, BillingMaster.TransferFromBillingDateFirst, "
                              + "BillingMaster.TransferFromBillingDateLast, BillingMaster.WhetherBilledBefore, BillingMaster.OwnerGroup, BillingMaster.Category1, BillingMaster.Category2, BillingMaster.Category3, "
                              + "BillingMaster.Category4, BillingMaster.Category5, BillingMaster.Category6, BillingMaster.Category7, BillingMaster.Category8, BillingMaster.Category9, BillingMaster.Acres, "
                              + "BillingMaster.HomesteadExemption, BillingMaster.OtherExempt1, BillingMaster.OtherExempt2, BillingMaster.BookPage, BillingMaster.PPAssessment, BillingMaster.Exempt1, "
                              + "BillingMaster.Exempt2, BillingMaster.Exempt3, BillingMaster.LienStatusEligibility, BillingMaster.LienProcessStatus, BillingMaster.Copies, BillingMaster.CertifiedMailNumber, "
                              + "BillingMaster.AbatementPaymentMade, BillingMaster.TGMixedAcres, BillingMaster.TGSoftAcres, BillingMaster.TGHardAcres, BillingMaster.TGMixedValue, BillingMaster.TGSoftValue, "
                              + "BillingMaster.TGHardValue, BillingMaster.Ref2, BillingMaster.Ref1, BillingMaster.Zip, BillingMaster.ShowRef1, BillingMaster.LienProcessExclusion, BillingMaster.IMPBTrackingNumber, "
                              + "BillingMaster.MailingAddress3, BillingMaster.PreviousInterestAppliedDate, Master.ID AS MasterId, Master.LastBldgVal, "
                              + "Master.LastLandVal, Master.hlUpdate, Master.HIUpdCode, Master.HLCompValBldg, Master.HLCompValLand, Master.HLValBldg, "
                              + "Master.HIValBldgCode, Master.HLValLand, Master.HIValLandCode, Master.HLAlt1Bldg, Master.HIAlt1BldgCode, Master.HLAlt2Bldg, "
                              + "Master.HIAlt2BldgCode, Master.PINeighborhood, Master.PIStreetCode, Master.PIXCoord, Master.PIYCoord, Master.PIZone, Master.PISecZone, "
                              + "Master.PITopography1, Master.PITopography2, Master.PIUtilities1, Master.PIUtilities2, Master.PIStreet, Master.PIOpen1, Master.PIOpen2, "
                              + "Master.PISalePrice, Master.PISaleType, Master.PISaleFinancing, Master.PISaleVerified, Master.PISaleValidity, Master.HLDOLExemption, "
                              + "Master.PILand1Type, Master.PILand1UnitsA, Master.PILand1UnitsB, Master.PILand1Inf, Master.PILand1InfCode, Master.PILand2Type, "
                              + "Master.PILand2UnitsA, Master.PILand2UnitsB, Master.PILand2Inf, Master.PILand2InfCode, Master.PILand3Type, Master.PILand3UnitsA, "
                              + "Master.PILand3UnitsB, Master.PILand3Inf, Master.PILand3InfCode, "
                              + "Master.PILand4Type, Master.PILand4UnitsA, Master.PILand4UnitsB, "
                              + "Master.PILand4Inf, Master.PILand4InfCode, Master.PILand5Type, "
                              + "Master.PILand5UnitsA, Master.PILand5UnitsB, Master.PILand5Inf, "
                              + "Master.PILand5InfCode, Master.PILand6Type, Master.PILand6UnitsA, "
                              + "Master.PILand6UnitsB, Master.PILand6Inf, Master.PILand6InfCode, "
                              + "Master.PILand7Type, Master.PILand7UnitsA, Master.PILand7UnitsB, "
                              + "Master.PILand7Inf, Master.PILand7InfCode, Master.PIAcres, "
                              + "Master.RSPropertyCode, Master.RSDwellingCode, Master.RSOutbuildingCode, "
                              + "Master.OwnerPartyID, Master.SecOwnerPartyID, Master.RSLocNumAlph, "
                              + "Master.RSLocApt, Master.RSLocStreet, Master.RSRef1, Master.RSRef2, "
                              + "Master.RILandCode, Master.RIBldgCode, Master.RLLandVal, Master.RLBldgVal, "
                              + "Master.CorrExemption, Master.RLExemption, Master.RIExemptCD1, "
                              + "Master.RIExemptCD2, Master.RIExemptCD3, Master.RITranCode, "
                              + "Master.RSMapLot, Master.RSPreviousMaster, Master.RSAccount, "
                              + "Master.RSCard, Master.RSDeleted, Master.SaleDate, Master.TaxAcquired, "
                              + "Master.RSSoft, Master.RSHard, Master.RSMixed, Master.RSSoftValue, "
                              + "Master.RSHardValue, Master.RSMixedValue, Master.HomesteadValue, "
                              + "Master.InBankruptcy, Master.ExemptVal1, Master.ExemptVal2, "
                              + "Master.ExemptVal3, Master.EntranceCode, Master.InformationCode, "
                              + "Master.DateInspected, Master.RSOtherValue, Master.RSOther, "
                              + "Master.HasHomestead, Master.CurrLandExempt, Master.CurrBldgExempt, "
                              + "Master.LandExempt, Master.BldgExempt, Master.ExemptPct1, "
                              + "Master.ExemptPct2, Master.ExemptPct3, Master.CurrHomesteadValue, "
                              + "Master.CurrHasHomestead, Master.CurrExemptVal1, Master.CurrExemptVal2, "
                              + "Master.CurrExemptVal3, Master.RevocableTrust, Master.AccountLocked, "
                              + "Master.ReasonAccountLocked, Master.DateCreated, Master.TADate, "
                              + "Master.PropertyCode, Master.ZoneOverride, Master.CardID, Master.AccountID, "
                              + "Master.DeedName1, Master.DeedName2 "
                              + "FROM BillingMaster INNER JOIN " + rsUpdate.Get_GetFullDBName("RealEstate") +
                              ".dbo.Master "
                              + "ON BillingMaster.Account = Master.RSAccount AND Master.RSCard = 1 AND BillingMaster.BillingType = 'RE'";


                    rsUpdate.Execute(sql, "Collections");
                }

                if (StaticSettings.gGlobalActiveModuleSettings.PersonalPropertyIsActive)
                {
                    rsUpdate.OpenRecordset(
                        "select* FROM sys.views where name = 'PersonalPropertyMasterBillingMasterView'", "Collections");
                    if (!rsUpdate.EndOfFile())
                    {
                        rsUpdate.Execute("Drop View [dbo].[PersonalPropertyMasterBillingMasterView]", "Collections");
                    }

                    var sql = "CREATE VIEW [dbo].[PersonalPropertyMasterBillingMasterView] as "
                              + "SELECT BillingMaster.ID, BillingMaster.Account, BillingMaster.BillingType, BillingMaster.BillingYear, BillingMaster.Name1, BillingMaster.Name2, BillingMaster.Address1, BillingMaster.Address2, "
                              + "BillingMaster.Address3, BillingMaster.MapLot, BillingMaster.streetnumber, BillingMaster.Apt, BillingMaster.StreetName, BillingMaster.LandValue, BillingMaster.BuildingValue, "
                              + "BillingMaster.ExemptValue, BillingMaster.TranCode, BillingMaster.LandCode, BillingMaster.BuildingCode, BillingMaster.OtherCode1, BillingMaster.OtherCode2, BillingMaster.TaxDue1, "
                              + "BillingMaster.TaxDue2, BillingMaster.TaxDue3, BillingMaster.TaxDue4, BillingMaster.LienRecordNumber, BillingMaster.PrincipalPaid, BillingMaster.InterestPaid, BillingMaster.InterestCharged, "
                              + "BillingMaster.DemandFees, BillingMaster.DemandFeesPaid, BillingMaster.InterestAppliedThroughDate, BillingMaster.RateKey, BillingMaster.TransferFromBillingDateFirst, "
                              + "BillingMaster.TransferFromBillingDateLast, BillingMaster.WhetherBilledBefore, BillingMaster.OwnerGroup, BillingMaster.Category1, BillingMaster.Category2, BillingMaster.Category3, "
                              + "BillingMaster.Category4, BillingMaster.Category5, BillingMaster.Category6, BillingMaster.Category7, BillingMaster.Category8, BillingMaster.Category9, BillingMaster.Acres, "
                              + "BillingMaster.HomesteadExemption, BillingMaster.OtherExempt1, BillingMaster.OtherExempt2, BillingMaster.BookPage, BillingMaster.PPAssessment, BillingMaster.Exempt1, "
                              + "BillingMaster.Exempt2, BillingMaster.Exempt3, BillingMaster.LienStatusEligibility, BillingMaster.LienProcessStatus, BillingMaster.Copies, BillingMaster.CertifiedMailNumber, "
                              + "BillingMaster.AbatementPaymentMade, BillingMaster.TGMixedAcres, BillingMaster.TGSoftAcres, BillingMaster.TGHardAcres, BillingMaster.TGMixedValue, BillingMaster.TGSoftValue, "
                              + "BillingMaster.TGHardValue, BillingMaster.Ref2, BillingMaster.Ref1, BillingMaster.Zip, BillingMaster.ShowRef1, BillingMaster.LienProcessExclusion, BillingMaster.IMPBTrackingNumber, "
                              + "BillingMaster.MailingAddress3, BillingMaster.PreviousInterestAppliedDate, "
                              + "PPMaster.ID AS MasterId, PPMaster.RealAssoc, PPMaster.PartyID, PPMaster.StreetNumber AS MasterStreetNumber, "
                              + "PPMaster.Street, PPMaster.Value, PPMaster.Exemption, PPMaster.ExemptCode1, PPMaster.ExemptCode2, PPMaster.TranCode AS MasterTranCode, "
                              + "PPMaster.BusinessCode, PPMaster.StreetCode, PPMaster.Open1, PPMaster.Open2, PPMaster.MO, PPMaster.DA, PPMaster.YR, PPMaster.CompValue, PPMaster.ORCode, "
                              + "PPMaster.UPDCode, PPMaster.Deleted, PPMaster.RBCode, PPMaster.SquareFootage, PPMaster.StreetApt, PPMaster.Comment, PPMaster.Exemption1, PPMaster.Exemption2, PPMaster.AccountID "
                              + "FROM BillingMaster INNER JOIN " + rsUpdate.Get_GetFullDBName("PersonalProperty") +
                              ".dbo.PPMaster "
                              + "ON BillingMaster.Account = PPMaster.Account AND BillingMaster.BillingType = 'PP'";


                    rsUpdate.Execute(sql, "Collections");
                }   

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddAccountPartyBillViews()
        {
            return AddREAccountPartyBillView() && AddPPAccountPartyBillView();
        }

        private bool AddREAccountPartyBillView()
        {
            var rsSave = new clsDRWrapper();
            var viewName = "RealEstateBillAccountPartyView";
            try
            {

                    rsSave.OpenRecordset("select* FROM sys.views where name = '" + viewName + "'",
                        "Collections");
                    if (!rsSave.EndOfFile())
                    {
                        rsSave.Execute("Drop View [dbo].[" + viewName +"]", "Collections");
                    }

                    var sql = "CREATE VIEW [dbo].[" + viewName + "] as "
                              + "SELECT BillingMaster.ID, BillingMaster.Account, BillingMaster.BillingType, BillingMaster.BillingYear, BillingMaster.Name1, BillingMaster.Name2, BillingMaster.Address1, BillingMaster.Address2, "
                              + "BillingMaster.Address3, BillingMaster.MapLot, BillingMaster.streetnumber, BillingMaster.Apt, BillingMaster.StreetName, BillingMaster.LandValue, BillingMaster.BuildingValue, "
                              + "BillingMaster.ExemptValue, BillingMaster.TranCode, BillingMaster.LandCode, BillingMaster.BuildingCode, BillingMaster.OtherCode1, BillingMaster.OtherCode2, BillingMaster.TaxDue1, "
                              + "BillingMaster.TaxDue2, BillingMaster.TaxDue3, BillingMaster.TaxDue4, BillingMaster.LienRecordNumber, BillingMaster.PrincipalPaid, BillingMaster.InterestPaid, BillingMaster.InterestCharged, "
                              + "BillingMaster.DemandFees, BillingMaster.DemandFeesPaid, BillingMaster.InterestAppliedThroughDate, BillingMaster.RateKey, BillingMaster.TransferFromBillingDateFirst, "
                              + "BillingMaster.TransferFromBillingDateLast, BillingMaster.WhetherBilledBefore, BillingMaster.OwnerGroup, BillingMaster.Category1, BillingMaster.Category2, BillingMaster.Category3, "
                              + "BillingMaster.Category4, BillingMaster.Category5, BillingMaster.Category6, BillingMaster.Category7, BillingMaster.Category8, BillingMaster.Category9, BillingMaster.Acres, "
                              + "BillingMaster.HomesteadExemption, BillingMaster.OtherExempt1, BillingMaster.OtherExempt2, BillingMaster.BookPage, BillingMaster.PPAssessment, BillingMaster.Exempt1, "
                              + "BillingMaster.Exempt2, BillingMaster.Exempt3, BillingMaster.LienStatusEligibility, BillingMaster.LienProcessStatus, BillingMaster.Copies, BillingMaster.CertifiedMailNumber, "
                              + "BillingMaster.AbatementPaymentMade, BillingMaster.TGMixedAcres, BillingMaster.TGSoftAcres, BillingMaster.TGHardAcres, BillingMaster.TGMixedValue, BillingMaster.TGSoftValue, "
                              + "BillingMaster.TGHardValue, BillingMaster.Ref2, BillingMaster.Ref1, BillingMaster.Zip, BillingMaster.ShowRef1, BillingMaster.LienProcessExclusion, BillingMaster.IMPBTrackingNumber, "
                              + "BillingMaster.MailingAddress3, BillingMaster.PreviousInterestAppliedDate,  "
                              + " AccountPartyAddressView.ID AS AccountPartyAddressViewId, AccountPartyAddressView.LastBldgVal, "
                              + "AccountPartyAddressView.LastLandVal,"
                              + " AccountPartyAddressView.PINeighborhood, AccountPartyAddressView.PIStreetCode, AccountPartyAddressView.PIZone, AccountPartyAddressView.PISecZone, "
                              + "AccountPartyAddressView.PITopography1, AccountPartyAddressView.PITopography2, AccountPartyAddressView.PIUtilities1, AccountPartyAddressView.PIUtilities2, AccountPartyAddressView.PIStreet, AccountPartyAddressView.PIOpen1, AccountPartyAddressView.PIOpen2, "
                              + " AccountPartyAddressView.PIAcres, "
                              + "AccountPartyAddressView.RSPropertyCode, "
                              + "AccountPartyAddressView.OwnerPartyID, AccountPartyAddressView.SecOwnerPartyID, AccountPartyAddressView.RSLocNumAlph, "
                              + "AccountPartyAddressView.RSLocApt, AccountPartyAddressView.RSLocStreet, AccountPartyAddressView.RSRef1, AccountPartyAddressView.RSRef2, "
                              + "AccountPartyAddressView.RILandCode, AccountPartyAddressView.RIBldgCode, AccountPartyAddressView.RLLandVal, AccountPartyAddressView.RLBldgVal, "
                              + " AccountPartyAddressView.RLExemption, AccountPartyAddressView.RIExemptCD1, "
                              + "AccountPartyAddressView.RIExemptCD2, AccountPartyAddressView.RIExemptCD3, AccountPartyAddressView.RITranCode, "
                              + "AccountPartyAddressView.RSMapLot, AccountPartyAddressView.RSPreviousMaster, AccountPartyAddressView.RSAccount, "
                              + "AccountPartyAddressView.RSCard, AccountPartyAddressView.RSDeleted, AccountPartyAddressView.SaleDate, AccountPartyAddressView.TaxAcquired, "
                              + "AccountPartyAddressView.RSSoft, AccountPartyAddressView.RSHard, AccountPartyAddressView.RSMixed, AccountPartyAddressView.RSSoftValue, "
                              + "AccountPartyAddressView.RSHardValue, AccountPartyAddressView.RSMixedValue, AccountPartyAddressView.HomesteadValue, "
                              + "AccountPartyAddressView.InBankruptcy, AccountPartyAddressView.ExemptVal1, AccountPartyAddressView.ExemptVal2, "
                              + "AccountPartyAddressView.ExemptVal3, "
                              + " AccountPartyAddressView.RSOtherValue, AccountPartyAddressView.RSOther, "
                              + "AccountPartyAddressView.HasHomestead,"
                              + "AccountPartyAddressView.LandExempt, AccountPartyAddressView.BldgExempt, AccountPartyAddressView.ExemptPct1, "
                              + "AccountPartyAddressView.ExemptPct2, AccountPartyAddressView.ExemptPct3,  AccountPartyAddressView.RevocableTrust, AccountPartyAddressView.TADate, "
                              + "AccountPartyAddressView.PropertyCode, AccountPartyAddressView.ZoneOverride, AccountPartyAddressView.CardID, AccountPartyAddressView.AccountID, "
                              + "AccountPartyAddressView.DeedName1, AccountPartyAddressView.DeedName2,  "
                              + "AccountPartyAddressView.OwnerGuid, AccountPartyAddressView.OwnerPartyType, AccountPartyAddressView.OwnerFirstName, AccountPartyAddressView.OwnerMiddleName, AccountPartyAddressView.OwnerLastName, AccountPartyAddressView.OwnerDesignation, AccountPartyAddressView.OwnerEmail,"
                              + "AccountPartyAddressView.Address1 as OwnerAddress1, AccountPartyAddressView.Address2 as OwnerAddress2, AccountPartyAddressView.Address3 as OwnerAddress3, AccountPartyAddressView.City as OwnerCity ,"
                              + "AccountPartyAddressView.State as OwnerState, AccountPartyAddressView.Zip as OwnerZip,"
                              + "AccountPartyAddressView.SecOwnerGuid,AccountPartyAddressView.SecOwnerPartyType, AccountPartyAddressView.SecOwnerFirstname, AccountPartyAddressView.SecOwnerMiddleName, "
                              + "AccountPartyAddressView.SecOwnerLastName, AccountPartyAddressView.SecOwnerDesignation "
                              + ",  LienRec.Costs as LienCosts, LienRec.CostsPaid as LienCostsPaid, LienRec.Interest as LienInterest, LienRec.InterestAppliedThroughDate as LienInterestAppliedThroughDate, "
                              + " LienRec.InterestCharged as LienInterestCharged, LienRec.InterestPaid as LienInterestPaid, LienRec.MaturityFee as LienMaturityFee,LienRec.Book as LienBook, LienRec.Page as LienPage, LienRec.PLIPaid as LienPLIPaid,"
                              + " LienRec.PreviousInterestAppliedDate as LienPreviousInterestAppliedDate, LienRec.Principal as LienPrincipal, LienRec.PrincipalPaid as LienPrincipalPaid, LienRec.PrintedLDN as LienPrintedLDN,"
                              + " LienRec.RateKey as LienRateKey, LienRec.Status as LienStatus"
                              + " FROM BillingMaster INNER JOIN " + rsSave.Get_GetFullDBName("RealEstate") +
                              ".dbo.AccountPartyAddressView "
                              + "ON BillingMaster.Account = AccountPartyAddressView.RSAccount  AND BillingMaster.BillingType = 'RE' "
                              + " left join LienRec on BillingMaster.LienRecordNumber = LienRec.ID";
                              


                    rsSave.Execute(sql, "Collections");


                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                rsSave.Dispose();
            }
        }

        private bool AddPPAccountPartyBillView()
        {
            var rsSave = new clsDRWrapper();
            var viewName = "PersonalPropertyBillAccountPartyView";
            try
            {
                rsSave.OpenRecordset("select* FROM sys.views where name = '" + viewName + "'",
                    "Collections");
                if (!rsSave.EndOfFile())
                {
                    rsSave.Execute("Drop View [dbo].[" + viewName + "]", "Collections");
                }

                var sql = "CREATE VIEW [dbo].[" + viewName + "] as "
                          + "SELECT BillingMaster.ID, BillingMaster.Account, BillingMaster.BillingType, BillingMaster.BillingYear, BillingMaster.Name1, BillingMaster.Name2, BillingMaster.Address1, BillingMaster.Address2, "
                          + "BillingMaster.Address3, BillingMaster.MapLot, BillingMaster.streetnumber, BillingMaster.Apt, BillingMaster.StreetName, BillingMaster.LandValue, BillingMaster.BuildingValue, "
                          + "BillingMaster.ExemptValue, BillingMaster.TranCode, BillingMaster.LandCode, BillingMaster.BuildingCode, BillingMaster.OtherCode1, BillingMaster.OtherCode2, BillingMaster.TaxDue1, "
                          + "BillingMaster.TaxDue2, BillingMaster.TaxDue3, BillingMaster.TaxDue4, BillingMaster.LienRecordNumber, BillingMaster.PrincipalPaid, BillingMaster.InterestPaid, BillingMaster.InterestCharged, "
                          + "BillingMaster.DemandFees, BillingMaster.DemandFeesPaid, BillingMaster.InterestAppliedThroughDate, BillingMaster.RateKey, BillingMaster.TransferFromBillingDateFirst, "
                          + "BillingMaster.TransferFromBillingDateLast, BillingMaster.WhetherBilledBefore, BillingMaster.OwnerGroup, BillingMaster.Category1, BillingMaster.Category2, BillingMaster.Category3, "
                          + "BillingMaster.Category4, BillingMaster.Category5, BillingMaster.Category6, BillingMaster.Category7, BillingMaster.Category8, BillingMaster.Category9, BillingMaster.Acres, "
                          + "BillingMaster.HomesteadExemption, BillingMaster.OtherExempt1, BillingMaster.OtherExempt2, BillingMaster.BookPage, BillingMaster.PPAssessment, BillingMaster.Exempt1, "
                          + "BillingMaster.Exempt2, BillingMaster.Exempt3, BillingMaster.LienStatusEligibility, BillingMaster.LienProcessStatus, BillingMaster.Copies, BillingMaster.CertifiedMailNumber, "
                          + "BillingMaster.AbatementPaymentMade, BillingMaster.TGMixedAcres, BillingMaster.TGSoftAcres, BillingMaster.TGHardAcres, BillingMaster.TGMixedValue, BillingMaster.TGSoftValue, "
                          + "BillingMaster.TGHardValue, BillingMaster.Ref2, BillingMaster.Ref1, BillingMaster.Zip, BillingMaster.ShowRef1, BillingMaster.LienProcessExclusion, BillingMaster.IMPBTrackingNumber, "
                          + "BillingMaster.MailingAddress3, BillingMaster.PreviousInterestAppliedDate, "
                          + "AccountPartyAddressView.ID AS MasterId, AccountPartyAddressView.RealAssoc, AccountPartyAddressView.PartyID, AccountPartyAddressView.StreetNumber AS MasterStreetNumber, "
                          + "AccountPartyAddressView.Street, AccountPartyAddressView.Value, AccountPartyAddressView.Exemption, AccountPartyAddressView.ExemptCode1, AccountPartyAddressView.ExemptCode2, AccountPartyAddressView.TranCode AS MasterTranCode, "
                          + "AccountPartyAddressView.BusinessCode, AccountPartyAddressView.StreetCode, AccountPartyAddressView.Open1, AccountPartyAddressView.Open2,  AccountPartyAddressView.CompValue,  "
                          + " AccountPartyAddressView.Deleted, AccountPartyAddressView.RBCode, AccountPartyAddressView.SquareFootage, AccountPartyAddressView.StreetApt, AccountPartyAddressView.Comment, AccountPartyAddressView.Exemption1, AccountPartyAddressView.Exemption2, AccountPartyAddressView.AccountID, "
                          + " AccountPartyAddressView.PartyGuid, AccountPartyAddressView.PartyType, AccountPartyAddressView.FirstName as OwnerFirstName, AccountPartyAddressView.MiddleName as OwnerMiddleName, AccountPartyAddressView.LastName as OwnerLastName, AccountPartyAddressView.Designation as OwnerDesignation,"
                          + " AccountPartyAddressView.Email as OwnerEmail, AccountPartyAddressView.Address1 as OwnerAddress1, AccountPartyAddressView.Address2 as OwnerAddress2, AccountPartyAddressView.Address3 as OwnerAddress3, "
                          + " AccountPartyAddressView.City as OwnerCity, AccountPartyAddressView.State as OwnerState, AccountPartyAddressView.Zip as OwnerZip ,AccountPartyAddressView.Country as OwnerCountry "
                          + "FROM BillingMaster INNER JOIN " + rsSave.Get_GetFullDBName("PersonalProperty") +
                          ".dbo.AccountPartyAddressView "
                          + "ON BillingMaster.Account = AccountPartyAddressView.Account AND BillingMaster.BillingType = 'PP'";
                         

                rsSave.Execute(sql, "Collections");

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                rsSave.Dispose();
            }
        }

        private bool AddThirdAddressFieldToBills()
		{
			bool AddThirdAddressFieldToBills = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsUpdate = new clsDRWrapper();
				if (!FieldExists("BillingMaster", "MailingAddress3", DatabaseToUpdate))
				{
					rsUpdate.Execute("ALTER TABLE BillingMaster ADD MailingAddress3 nvarchar(255) NULL", DatabaseToUpdate);
					rsUpdate.Execute("update billingmaster set MailingAddress3 = ''", DatabaseToUpdate);
				}
				AddThirdAddressFieldToBills = true;
				return AddThirdAddressFieldToBills;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddThirdAddressFieldToBills;
		}

		private bool OldCheckDatabaseStructure()
		{
			bool OldCheckDatabaseStructure = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsDB = new clsDRWrapper();
				App.DoEvents();
				// Make sure Version is in the Collections table
				rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'Version' AND TABLE_NAME = 'Collections'", DatabaseToUpdate);
				if (rsDB.EndOfFile())
				{
					rsDB.Execute("ALTER TABLE.[dbo].[Collections] ADD [Version] nvarchar(50) NULL", DatabaseToUpdate);
				}
				App.DoEvents();
				// kk 09252013 trocl-30  Lien Discharge Filing Fee Increase
				rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'LienFeeIncrRunOn' AND TABLE_NAME = 'Collections'", DatabaseToUpdate);
				if (rsDB.EndOfFile())
				{
					rsDB.Execute("ALTER TABLE.[dbo].[Collections] ADD [LienFeeIncrRunOn] datetime NULL", DatabaseToUpdate);
					rsDB.Execute("UPDATE Collections SET LienFeeIncrRunOn = '" + Strings.Format(FCConvert.ToDateTime(0), "MM/dd/yyyy") + "'", DatabaseToUpdate);
				}
				App.DoEvents();
				// kk03162015 trocrs-36  Add option to disable auto-fill payment amount on collection screen
				rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'DisableAutoPmtFill' AND TABLE_NAME = 'Collections'", DatabaseToUpdate);
				if (rsDB.EndOfFile())
				{
					rsDB.Execute("ALTER TABLE.[dbo].[Collections] ADD [DisableAutoPmtFill] bit NULL", DatabaseToUpdate);
					rsDB.Execute("UPDATE Collections SET DisableAutoPmtFill = 0", DatabaseToUpdate);
				}
				App.DoEvents();
				// Make sure BillingMaster is indexed on Account
				rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_BillingMaster_Account'", DatabaseToUpdate);
				if (rsDB.EndOfFile())
				{
					rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_BillingMaster_Account] ON [dbo].[BillingMaster] " + "([Account] ASC, [BillingType] Asc) " + "INCLUDE ([LienRecordNumber]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", DatabaseToUpdate);
				}
				App.DoEvents();
				// Make sure BillingMaster is indexed on Name1
				rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_BillingMaster_Name1'", DatabaseToUpdate);
				if (rsDB.EndOfFile())
				{
					rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_BillingMaster_Name1] ON [dbo].[BillingMaster] " + "([Name1] ASC, [BillingType] Asc) " + "INCLUDE ([LienRecordNumber]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", DatabaseToUpdate);
				}
				App.DoEvents();
				// Make sure PaymentRec is indexed on Account
				rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_PaymentRec_Account'", DatabaseToUpdate);
				if (rsDB.EndOfFile())
				{
					rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_PaymentRec_Account] ON [dbo].[PaymentRec] " + "([Account] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", DatabaseToUpdate);
				}
				App.DoEvents();
				// Make sure PaymentRec is indexed on BillKey
				rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_PaymentRec_BillKey'", DatabaseToUpdate);
				if (rsDB.EndOfFile())
				{
					rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_PaymentRec_BillKey] ON [dbo].[PaymentRec] " + "([BillKey] Asc, [BillCode] ASC) " + "INCLUDE ([Code],[Principal],[PreLienInterest],[CurrentInterest],[LienCost],[ReceiptNumber],[RecordedTransactionDate]) " + "WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", DatabaseToUpdate);
				}
				App.DoEvents();
				// Make sure RE Master is indexed on RSAccount
				rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_Master_RSAccount'", "RealEstate");
				if (rsDB.EndOfFile())
				{
					rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_Master_RSAccount] ON [dbo].[Master] " + "([RSAccount] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", "RealEstate");
				}
				App.DoEvents();
				// Make sure PP PPMaster is indexed on Account
				rsDB.OpenRecordset("SELECT name FROM sys.indexes WHERE Name = 'IX_PPMaster_Account'", "PersonalProperty");
				if (rsDB.EndOfFile())
				{
					rsDB.Execute("CREATE NONCLUSTERED INDEX [IX_PPMaster_Account] ON [dbo].[PPMaster] " + "([Account] Asc)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]", "PersonalProperty");
				}
				App.DoEvents();
				// kk09302014 trocl-1197  Fix for Discount, Prepayment and Abatement Interest Rates
				if (!UpdateRateEntryAndStorage())
				{
					return OldCheckDatabaseStructure;
				}
				// kk09242015 trocls-63  Add IMPB Tracking Number to CMFNumbers and BillingMaster tables
				rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'IMPBTrackingNumber' AND TABLE_NAME = 'CMFNumbers'", DatabaseToUpdate);
				if (rsDB.EndOfFile())
				{
					rsDB.Execute("ALTER TABLE.[dbo].[CMFNumbers] ADD [IMPBTrackingNumber] nvarchar(30) NULL", DatabaseToUpdate);
				}
				rsDB.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'IMPBTrackingNumber' AND TABLE_NAME = 'BillingMaster'", DatabaseToUpdate);
				if (rsDB.EndOfFile())
				{
					rsDB.Execute("ALTER TABLE.[dbo].[BillingMaster] ADD [IMPBTrackingNumber] nvarchar(30) NULL", DatabaseToUpdate);
				}
				App.DoEvents();
				// trocls-68
				if (!AlterBookPageFields())
				{
					return OldCheckDatabaseStructure;
				}
				OldCheckDatabaseStructure = true;
				return OldCheckDatabaseStructure;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return OldCheckDatabaseStructure;
		}

		private bool UpdateRateEntryAndStorage()
		{
			bool UpdateRateEntryAndStorage = false;
			// kk09302014 trocl-1197  Change the Discount, Prepayment and Abatement Interest Rates
			// to be entered as Percentages and stored as decimal (7.00% -> 0.07)
			clsDRWrapper rsColl = new clsDRWrapper();
			double dblDiscRate = 0;
			double dblPrepayRate = 0;
			double dblAbateRate = 0;
			try
			{
				// On Error GoTo ErrorHandler
				rsColl.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'RateEntryUpdateDone' AND TABLE_NAME = 'Collections'", DatabaseToUpdate);
				if (rsColl.EndOfFile())
				{
					rsColl.Execute("ALTER TABLE.[dbo].[Collections] ADD [RateEntryUpdateDone] bit NULL", DatabaseToUpdate);
				}
				rsColl.OpenRecordset("SELECT RateEntryUpdateDone FROM Collections", DatabaseToUpdate);
				if (!FCConvert.ToBoolean(rsColl.Get_Fields_Boolean("RateEntryUpdateDone")))
				{
					rsColl.OpenRecordset("SELECT DiscountPercent, OverPayInterestRate, AbatementInterestRate FROM Collections", DatabaseToUpdate);
					if (!rsColl.EndOfFile())
					{
						// TODO: Check the table for the column [DiscountPercent] and replace with corresponding Get_Field method
						dblDiscRate = Conversion.Val(rsColl.Get_Fields("DiscountPercent"));
						dblPrepayRate = Conversion.Val(rsColl.Get_Fields_Double("OverPayInterestRate"));
						dblAbateRate = Conversion.Val(rsColl.Get_Fields_Double("AbatementInterestRate"));
						rsColl.Execute("ALTER TABLE [dbo].[Collections] DROP COLUMN [DiscountPercent]", DatabaseToUpdate);
						rsColl.Execute("ALTER TABLE dbo.Collections ADD DiscountPercent float(53) NULL", DatabaseToUpdate);
						rsColl.OpenRecordset("SELECT * FROM Collections", DatabaseToUpdate);
						rsColl.Edit();
						rsColl.Set_Fields("RateEntryUpdateDone", true);
						rsColl.Set_Fields("DiscountPercent", dblDiscRate / 10000);
						rsColl.Set_Fields("OverPayInterestRate", dblPrepayRate / 100);
						rsColl.Set_Fields("AbatementInterestRate", dblAbateRate / 10000);
						rsColl.Update();
					}
				}
				UpdateRateEntryAndStorage = true;
				return UpdateRateEntryAndStorage;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return UpdateRateEntryAndStorage;
		}

		private bool AlterBookPageFields()
		{
			bool AlterBookPageFields = false;
			bool boolBook = false;
			bool boolPage = false;
			bool boolBookDischargeNeededArchive = false;
			bool boolPageDischargeNeededArchive = false;
			bool boolBookLien = false;
			bool boolPageLien = false;
			string strSQL = "";
			clsDRWrapper rsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				if (GetDataType("DischargeNeeded", "Book", DatabaseToUpdate) == "int")
				{
					boolBook = true;
				}
				if (GetDataType("DischargeNeeded", "Page", DatabaseToUpdate) == "int")
				{
					boolPage = true;
				}
				if (GetDataType("DischargeNeededArchive", "Book", DatabaseToUpdate) == "int")
				{
					boolBookDischargeNeededArchive = true;
				}
				if (GetDataType("DischargeNeededArchive", "Page", DatabaseToUpdate) == "int")
				{
					boolPageDischargeNeededArchive = true;
				}
				if (GetDataType("LienRec", "Book", DatabaseToUpdate) == "int")
				{
					boolBookLien = true;
				}
				if (GetDataType("LienRec", "Page", DatabaseToUpdate) == "int")
				{
					boolPageLien = true;
				}
				if (boolBook)
				{
					strSQL = "Alter table DischargeNeeded alter column Book varchar(255) null";
					rsSave.Execute(strSQL, DatabaseToUpdate);
				}
				if (boolPage)
				{
					strSQL = "Alter table DischargeNeeded alter column Page varchar(255) null";
					rsSave.Execute(strSQL, DatabaseToUpdate);
				}
				if (boolBookDischargeNeededArchive)
				{
					strSQL = "Alter table DischargeNeededArchive alter column Book varchar(255) null";
					rsSave.Execute(strSQL, DatabaseToUpdate);
				}
				if (boolPageDischargeNeededArchive)
				{
					strSQL = "Alter table DischargeNeededArchive alter column Page varchar(255) null";
					rsSave.Execute(strSQL, DatabaseToUpdate);
				}
				if (boolBookLien)
				{
					strSQL = "Alter table LienRec alter column Book varchar(255) null";
					rsSave.Execute(strSQL, DatabaseToUpdate);
				}
				if (boolPageLien)
				{
					strSQL = "Alter table LienRec alter column Page varchar(255) null";
					rsSave.Execute(strSQL, DatabaseToUpdate);
				}
				AlterBookPageFields = true;
				return AlterBookPageFields;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AlterBookPageFields;
		}

        private bool CheckDiscountPercentDataType()
        {
            if (GetDataType("Collections", "DiscountPercent", DatabaseToUpdate) != "float")
            {
                var rsSave = new clsDRWrapper();
                var strSQL = "Alter table Collections alter column DiscountPercent float null";
                rsSave.Execute(strSQL, DatabaseToUpdate);
            }

            return true;
        }

        private bool AddTaxServiceEmail()
		{
			bool AddTaxServiceEmail = false;
			// kk01182017 trocl-1326  Add TaxServiceEmail field to Collections table
			clsDRWrapper rsColl = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				rsColl.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'TaxServiceEmail' AND TABLE_NAME = 'Collections'", DatabaseToUpdate);
				if (rsColl.EndOfFile())
				{
					rsColl.Execute("ALTER TABLE .[dbo].[Collections] ADD [TaxServiceEmail] varchar(255) NULL", DatabaseToUpdate);
					rsColl.OpenRecordset("SELECT * FROM Collections", DatabaseToUpdate);
					rsColl.Edit();
					rsColl.Set_Fields("TaxServiceEmail", "eastern_daq.edg@corelogic.com,mmcfadden@CoreLogic.com");
					rsColl.Update();
				}
				AddTaxServiceEmail = true;
				return AddTaxServiceEmail;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddTaxServiceEmail;
		}

        private bool AddAddLienCharge()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                rsSave.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'AddLienCharge' AND TABLE_NAME = 'Collections'", DatabaseToUpdate);
                if (rsSave.EndOfFile())
                {
                    rsSave.Execute("ALTER TABLE .[dbo].[Collections] ADD [AddLienCharge] bit NULL", DatabaseToUpdate);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool AddIdentifiersToPayments()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                rsSave.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'CorrelationIdentifier' AND TABLE_NAME = 'PaymentRec'", DatabaseToUpdate);
                if (rsSave.EndOfFile())
                {
                    rsSave.Execute("ALTER TABLE .[dbo].[PaymentRec] ADD [CorrelationIdentifier] uniqueidentifier NULL", DatabaseToUpdate);
                    rsSave.Execute("Update PaymentRec set CorrelationIdentifier = NEWID()", DatabaseToUpdate);
                }

                rsSave.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'PaymentIdentifier' AND TABLE_NAME = 'PaymentRec'", DatabaseToUpdate);
                if (rsSave.EndOfFile())
                {
                    rsSave.Execute("ALTER TABLE .[dbo].[PaymentRec] ADD [PaymentIdentifier] uniqueidentifier NULL", DatabaseToUpdate);
                    rsSave.Execute("update PaymentRec set PaymentIdentifier = NEWID()", DatabaseToUpdate);
                }
                rsSave.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'TransactionIdentifier' AND TABLE_NAME = 'PaymentRec'", DatabaseToUpdate);
                if (rsSave.EndOfFile())
                {
                    rsSave.Execute("ALTER TABLE .[dbo].[PaymentRec] ADD [TransactionIdentifier] uniqueidentifier NULL", DatabaseToUpdate);
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        private bool AddPreviousInterestDate()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                rsSave.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'PreviousInterestAppliedDate' AND TABLE_NAME = 'BillingMaster'", DatabaseToUpdate);
                if (rsSave.EndOfFile())
                {
                    rsSave.Execute("ALTER TABLE .[dbo].[BillingMaster] ADD [PreviousInterestAppliedDate] DateTime NULL", DatabaseToUpdate);                    
                }
                rsSave.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'PreviousInterestAppliedDate' AND TABLE_NAME = 'LienRec'", DatabaseToUpdate);
                if (rsSave.EndOfFile())
                {
                    rsSave.Execute("ALTER TABLE .[dbo].[LienRec] ADD [PreviousInterestAppliedDate] DateTime NULL", DatabaseToUpdate);
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        private bool AddBatchIdentifier()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                rsSave.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'BatchIdentifier' AND TABLE_NAME = 'BatchRecover'", DatabaseToUpdate);
                if (rsSave.EndOfFile())
                {
                    rsSave.Execute("ALTER TABLE .[dbo].[BatchRecover] ADD [BatchIdentifier] UniqueIdentifier NULL", DatabaseToUpdate);
                    var rsLoad = new clsDRWrapper();
                    rsLoad.OpenRecordset("select distinct TellerId, PaidBy,BatchRecoverDate  from BatchRecover",DatabaseToUpdate);
                    while (!rsLoad.EndOfFile())
                    {
                        var identifier = Guid.NewGuid();
                        rsSave.Execute(
                            "update BatchRecover set BatchIdentifier = '" + identifier.ToString() +
                            "' where TellerId = '" + rsLoad.Get_Fields_String("TellerId") + "' and PaidBy = '" +
                            rsLoad.Get_Fields_String("PaidBy") + "' and BatchRecoverDate = '" +
                            rsLoad.Get_Fields_DateTime("BatchRecoverDate").FormatAndPadShortDate() + "'",
                            DatabaseToUpdate);
                        rsLoad.MoveNext();
                    }

                }                
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        private bool AddChargedInterestIdentifier()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                rsSave.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'ChargedInterestIdentifier' AND TABLE_NAME = 'PaymentRec'", DatabaseToUpdate);
                if (rsSave.EndOfFile())
                {
                    rsSave.Execute("ALTER TABLE .[dbo].[PaymentRec] ADD [ChargedInterestIdentifier] uniqueidentifier NULL", DatabaseToUpdate);
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        private bool RemoveChargedInterestIdentifier()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                rsSave.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'ChargedInterestIdentifier' AND TABLE_NAME = 'PaymentRec'", DatabaseToUpdate);
                if (!rsSave.EndOfFile())
                {
                    rsSave.Execute("ALTER TABLE .[dbo].[PaymentRec] Drop Column [ChargedInterestIdentifier] ", DatabaseToUpdate);
                }

            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        private bool FillTransactionIdentifier()
        {
            var rsChargedInt = new clsDRWrapper();
            var rsSave = new clsDRWrapper();

            try
            {
                rsSave.Execute("IF EXISTS (select name from sys.indexes where name = 'idx_id') DROP INDEX PaymentRec.idx_id;",DatabaseToUpdate);
                rsSave.Execute("CREATE INDEX idx_id on PaymentRec (Id);",DatabaseToUpdate);
                rsSave.Execute("IF EXISTS (select name from sys.indexes where name = 'idx_chgintnumber') DROP INDEX PaymentRec.idx_chgintnumber;",DatabaseToUpdate);
                rsSave.Execute("CREATE INDEX idx_chgintnumber on PaymentRec (CHGINTNumber);",DatabaseToUpdate);
                rsSave.OpenRecordset("SELECT distinct TransactionIdentifier, CHGINTNumber FROM PaymentRec WHERE CHGINTNumber <> 0", DatabaseToUpdate);

                if (!rsSave.EndOfFile())
                {
                    do
                    {
                        var transId = rsSave.Get_Fields_Guid("TransactionIdentifier");

                        rsChargedInt.OpenRecordset(
                            "SELECT * FROM PaymentRec WHERE Id = " + rsSave.Get_Fields_Int16("CHGINTNumber"), DatabaseToUpdate);

                        if (!rsChargedInt.EndOfFile())
                        {
                           // var interestIdentifier = rsChargedInt.Get_Fields_Guid("PaymentIdentifier");
                            rsChargedInt.Edit();
                            rsChargedInt.Set_Fields("TransactionIdentifier", transId);
                            rsChargedInt.Update();

                            //rsSave.Edit();
                            //rsSave.Set_Fields("ChargedInterestIdentifier", interestIdentifier);
                            //rsSave.Update();
                        }

                        rsSave.MoveNext();
                    } while (!rsSave.EndOfFile());
                }

                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                rsSave.DisposeOf();
                rsChargedInt.DisposeOf();
            }
        }

        private bool AddBillTypeFieldToBatchRecover()
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsUpdate = new clsDRWrapper();
                if (!FieldExists("BatchRecover", "BillingType", DatabaseToUpdate))
                {
                    rsUpdate.Execute("ALTER TABLE [BatchRecover] ADD [BillingType] nvarchar(10) NULL", DatabaseToUpdate);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }


        private bool AddProcessedToBatchRecover()
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsUpdate = new clsDRWrapper();
                if (!FieldExists("BatchRecover", "Processed", DatabaseToUpdate))
                {
                    rsUpdate.Execute("ALTER TABLE .[dbo].[BatchRecover] Add [Processed] bit NULL", DatabaseToUpdate);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddPaymentIdentifierToBatchRecover()
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsUpdate = new clsDRWrapper();
                if (!FieldExists("BatchRecover", "PaymentIdentifier", DatabaseToUpdate))
                {
                    rsUpdate.Execute("ALTER TABLE .[dbo].[BatchRecover] Add [PaymentIdentifier] nvarchar (64) NULL", DatabaseToUpdate);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddExternalPaymentIdentifierToPaymentRec()
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsUpdate = new clsDRWrapper();
                if (!FieldExists("PaymentRec", "ExternalPaymentIdentifier", DatabaseToUpdate))
                {
                    rsUpdate.Execute("Alter Table .[dbo].[PaymentRec] Add [ExternalPaymentIdentifier] nvarchar(64) NULL", DatabaseToUpdate);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddPaymentIdentifierToImportedPayments()
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsUpdate = new clsDRWrapper();
                if (!FieldExists("ImportedPayments", "PaymentIdentifier", DatabaseToUpdate))
                {
                    rsUpdate.Execute("alter table .[dbo].[ImportedPayments] Add [PaymentIdentifier] nvarchar(64) NULL", DatabaseToUpdate);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddImportIdToBatchRecover()
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsUpdate = new clsDRWrapper();
                if (!FieldExists("BatchRecover", "ImportID", DatabaseToUpdate))
                {
                    rsUpdate.Execute("ALTER TABLE .[dbo].[BatchRecover] Add [ImportID] int NULL", DatabaseToUpdate);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool AddImportedPayments()
        {
            string strSql;
            clsDRWrapper rsExec = new clsDRWrapper();

            try
            {
                strSql = GetTableCreationHeaderSQL("ImportedPayments");
                strSql = strSql + "[BillId] [int] NOT NULL,";
                strSql = strSql + "[BillType] [varchar] (25) NOT NULL,";
                strSql = strSql + "[BillNumber] [varchar] (25) NOT NULL,";
                strSql = strSql + "[AccountNumber] [int] NOT NULL,";
                strSql = strSql + "[PaymentDate] [DateTime] NOT NULL,";
                strSql = strSql + "[PaymentAmount] [Decimal] (18,2) NOT NULL,";
                strSql = strSql + "[PaidBy] [varchar] (255) NOT NULL,";
                strSql = strSql + "[AppliedDateTime] [DateTime] NULL,";
                strSql = strSql + "[AppliedId] [int] NOT NULL,";
                strSql = strSql + "[PaymentIdentifier] [nvarchar] (64) NULL,";
                strSql = strSql + GetTablePKSql("ImportedPayments");

                rsExec.Execute(strSql, "Collections");

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private bool AddCorrelationIdentifier()
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsUpdate = new clsDRWrapper();
                if (!FieldExists("PaymentRec", "CorrelationIdentifier", "Collections"))
                {
                    rsUpdate.Execute("Alter Table.[dbo].[PaymentRec] Add [CorrelationIdentifier] uniqueidentifier NULL", "Collections");
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool Add30DNOfficersPhone()
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsUpdate = new clsDRWrapper();
                if (!FieldExists("Control_30DayNotice", "OfficersPhone", DatabaseToUpdate))
                {
                    rsUpdate.Execute("ALTER TABLE .[dbo].[Control_30DayNotice] Add [OfficersPhone] varchar(255) NULL", DatabaseToUpdate);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
