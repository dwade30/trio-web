﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using System.IO;
using Global;
using TWSharedLibrary;
#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWBD0000
using TWBD0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arAllLienDischargeNotice : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/07/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/03/2006              *
		// ********************************************************
		clsDRWrapper rsLien = new clsDRWrapper();
		clsDRWrapper rsRateRec = new clsDRWrapper();
		string strMainText;
		string strTreasurerName = "";
		DateTime dtPaymentDate;
		string strOwnerName = "";
		// VBto upgrade warning: dtLienDate As DateTime	OnWriteFCConvert.ToInt16(
		DateTime dtLienDate;
		string strBook = "";
		string strPage = "";
		string strCounty = "";
		string strHisHer = "";
		string strMuniName = "";
		int lngLineLen;
		string strSignerDesignation = "";
		string strSignerName = "";
		DateTime dtCommissionExpiration;
		string strTitle = "";
		int lngCurrentLien;
		DateTime dtTreasSignDate;
		DateTime dtAppearedDate;
		bool boolDefaultAppearedDate;
		string strYear = "";

		public arAllLienDischargeNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Discharge Notice";
            this.ReportEnd += ArAllLienDischargeNotice_ReportEnd;
		}

        private void ArAllLienDischargeNotice_ReportEnd(object sender, EventArgs e)
        {
			rsLien.DisposeOf();
            rsRateRec.DisposeOf();

		}

        public static arAllLienDischargeNotice InstancePtr
		{
			get
			{
				return (arAllLienDischargeNotice)Sys.GetInstance(typeof(arAllLienDischargeNotice));
			}
		}

		protected arAllLienDischargeNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            rsLien.DisposeOf();
            rsRateRec.DisposeOf();
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsLien.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// SetVariables
				if (Strings.Trim(modGlobalConstants.Statics.gstrTownSealPath) != "" && modCLCalculations.Statics.gboolCLUseTownSealLienDischarge)
				{
					// MAL@20080611: Check for file's existence
					// Tracker Reference: 14008
					if (File.Exists(modGlobalConstants.Statics.gstrTownSealPath))
					{
						imgTownSeal.Visible = true;
						imgTownSeal.Image = FCUtils.LoadPicture(modGlobalConstants.Statics.gstrTownSealPath);
					}
					else
					{
						imgTownSeal.Visible = false;
					}
				}
				else
				{
					imgTownSeal.Visible = false;
				}
				// kgk 11-4-2011 trocl=823  Print adjustments not working - added from arLienDischargeNotice
				// set the top margin          'kk02042016 trout-1197  New margins effective 10/2015 - 1.5" Top 1st page, 1.5" Bottom last page
				this.PageSettings.Margins.Top = 1.5F + FCConvert.ToSingle(modGlobal.Statics.gdblLDNAdjustmentTop);
				this.PageSettings.Margins.Bottom = 1.5F + FCConvert.ToSingle(modGlobal.Statics.gdblLDNAdjustmentBottom);
				this.PageSettings.Margins.Left = 0.75F + FCConvert.ToSingle(modGlobal.Statics.gdblLDNAdjustmentSide);
				this.PageSettings.Margins.Right = this.PageSettings.Margins.Left;
				this.PrintWidth = 8.5F - (this.PageSettings.Margins.Left + this.PageSettings.Margins.Right);
				//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Report Start");
			}
		}

		private void SetStrings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strPaymentDate = "";
				string strFormatPaymentDate = "";
				string strFormatTreasSignDate = "";
				string strAppearedDateText = "";
				lngLineLen = 35;
				// this is how many underscores are in the printed lines
				if (dtPaymentDate.ToOADate() != 0)
				{
					strPaymentDate = Strings.Format(dtPaymentDate, "MMMM dd, yyyy");
					strFormatPaymentDate = Strings.Format(dtPaymentDate, "MMMM dd, yyyy");
				}
				else
				{
					strPaymentDate = Strings.StrDup(15, "_");
					strFormatPaymentDate = Strings.StrDup(15, "_");
				}
				if (dtTreasSignDate.ToOADate() != 0)
				{
					strFormatTreasSignDate = Strings.Format(dtTreasSignDate, "MMMM dd, yyyy");
				}
				else
				{
					// if there is no treasurer signing date, then use the current date
					strFormatTreasSignDate = Strings.Format(DateTime.Today, "MMMM dd, yyyy");
				}
				if (dtAppearedDate.ToOADate() != 0)
				{
					strAppearedDateText = Strings.Format(dtAppearedDate, "MMMM dd, yyyy");
				}
				else
				{
					if (boolDefaultAppearedDate)
					{
						// default this to the payment date
						strAppearedDateText = Strings.Format(dtPaymentDate, "MMMM dd, yyyy");
					}
					else
					{
						// if the user does not want to default this to the payment date then leave a blank line for them to write it in
						strAppearedDateText = Strings.StrDup(15, "_");
					}
				}
				fldMuni.Text = lblTownHeader.Text;
				lblTitleBar.Text = "DISCHARGE OF MORTGAGE FOR TAX COLLECTOR'S LIEN CERTIFICATE";
				lblLegalDescription.Text = "Title 36, M.R.S.A. Section 943";
				strMainText = "    I, " + strTreasurerName + ", in my capacity as " + strTitle + " of the municipality of ";
				strMainText += strMuniName + ", hereby acknowledge that on " + strPaymentDate;
				strMainText += " I received full payment and satisfaction of the debt secured by the " + strYear + " tax lien mortgage against property assessed to ";
				strMainText += strOwnerName + " created by the recording of a tax lien certificate dated ";
				if (dtLienDate.ToOADate() != 0)
				{
					strMainText += FCConvert.ToString(dtLienDate) + " in Book " + strBook + ", at Page " + strPage + " of the ";
				}
				else
				{
					strMainText += Strings.StrDup(15, "_") + " in Book " + strBook + ", at Page " + strPage + " of the ";
				}
				strMainText += strCounty + " County Registry of Deeds, and in consideration thereof I hereby discharge said tax lien mortgage.";
				fldMainText.Text = strMainText;
				fldTopDate.Text = "";
				// "Dated: " & Format(dtPaymentDate, "MMMM dd, yyyy")
				// fldSigLine.Text = String(lngLineLen, "_")
				fldSigTitle.Text = "";
				// "Treasurer"
				fldSigName.Text = strTreasurerName + ", " + strTitle;
				fldSigDate.Text = "Dated: " + strFormatTreasSignDate;
				// fldACKNOWLEDGEMENT = "State of Maine"
				fldACKNOWLEDGEMENT.Text = "ACKNOWLEDGEMENT";
				strMainText = strMuniName + "\r\n";
				strMainText += "State of Maine" + "\r\n" + strCounty + " County, ss." + "\r\n" + "\r\n";
				strMainText += "Personally appeared before me, on " + strAppearedDateText;
				strMainText += ", the above-named " + strTreasurerName;
				strMainText += ", who acknowledged the foregoing to be ";
				strMainText += strHisHer + " free act and deed in " + strHisHer + " capacity as " + strTitle + ".";
				fldBottomText.Text = strMainText;
				fldNotaryLine.Text = Strings.StrDup(lngLineLen, "_");
				if (strSignerName != "" && strSignerDesignation != "")
				{
					fldNotaryTitle.Text = strSignerName + ", " + strSignerDesignation;
					// "Notary Public / Attorney at Law"
				}
				else if (strSignerName != "")
				{
					fldNotaryTitle.Text = strSignerName;
				}
				else
				{
					fldNotaryTitle.Text = strSignerDesignation;
				}
				// fldNotaryPrintLine.Text = String(lngLineLen, "_")
				// fldNotaryPrintTitle.Text = strSignerName ' "Printed Name"
				// fldNotaryCommissionLine.Text = String(lngLineLen, "_")
				if (dtCommissionExpiration.ToOADate() != 0)
				{
					fldNotaryCommissionTitle.Text = "My commission expires: " + Strings.Format(dtCommissionExpiration, "MMMM dd, yyyy");
				}
				else
				{
					fldNotaryCommissionTitle.Text = "My commission expires: ";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Variables");
			}
		}

		public void Init(string strPassTeasName, string strPassMuni, string strPassCounty, string strPassHisHer, string strPassSignerName, string strPassSignerDesignation, DateTime dtPassCommissionExpiration, string strPassTitle = "Treasurer", DateTime? dtPassTreasSignDate = null/*= DateTime.Now*/, DateTime? dtPassAppearedDate = null/*= DateTime.Now*/, bool boolPassDefaultAppearedDate = false, bool blnPrintArchive = false)
		{
			if (!dtPassTreasSignDate.HasValue)
			{
				dtPassTreasSignDate = DateTime.Now;
			}
			if (!dtPassAppearedDate.HasValue)
			{
				dtPassAppearedDate = DateTime.Now;
			}
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will set all of the variables needed
				if (!blnPrintArchive)
				{
					rsLien.OpenRecordset("SELECT * FROM LienRec INNER JOIN DischargeNeeded ON LienRec.ID = DischargeNeeded.LienKey WHERE Printed = 0", modExtraModules.strCLDatabase);
				}
				else
				{
					rsLien.OpenRecordset("SELECT * FROM LienRec INNER JOIN DischargeNeededArchive ON LienRec.ID = DischargeNeededArchive.LienKey", modExtraModules.strCLDatabase);
				}
				// Row - 1  'Treasurer Name
				strTreasurerName = strPassTeasName;
				// Row - 2  'Muni Name
				strMuniName = strPassMuni;
				// Row - 3  'County
				strCounty = strPassCounty;
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					lblTownHeader.Text = modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName;
				}
				else
				{
					lblTownHeader.Text = strMuniName;
				}
				boolDefaultAppearedDate = boolPassDefaultAppearedDate;
				// Row - 10 'His/Her
				strHisHer = Strings.LCase(strPassHisHer);
				if (Strings.Trim(strHisHer) == "")
				{
					strHisHer = "his/her";
				}
				// title
				strTitle = strPassTitle;
				// Row - 11 'Signer Name
				strSignerName = strPassSignerName;
				// Row - 12 'Signer's Designation
				strSignerDesignation = strPassSignerDesignation;
				// Row - 13 ' Commission expiration date
				dtCommissionExpiration = dtPassCommissionExpiration;
				// treasurer signing date
				dtTreasSignDate = dtPassTreasSignDate.Value;
				// appeared date
				dtAppearedDate = dtPassAppearedDate.Value;
				if (modGlobal.Statics.gboolUseSigFile)
				{
					imgSig.Visible = true;
					//imgSig.ZOrder(1);
					// fldSigLine.ZOrder 0
					imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrTreasSigPath);
					// kk02052016 trout-1197  Rework lien notice. Change the way signature line is located.
					// The adjustments are applied before the text field is expanded, so we have to limit the adjustment amount.
					// If the signature moves into the rtbText area or above it, the signature will not be pushed down when the rtbText is expanded.
					if (modGlobal.Statics.gdblLienSigAdjust < 0)
					{
						// The digital signature starts out directly below the rich text box
						imgSig.Top = fldMainText.Top + fldMainText.Height;
						// Don't allow it to go any higher
					}
					else
					{
						if (imgSig.Top + modGlobal.Statics.gdblLienSigAdjust < Line2.Y1)
						{
							// The farthest it can move is so the top is even with the signature line
							imgSig.Top += FCConvert.ToSingle(modGlobal.Statics.gdblLienSigAdjust);
						}
						else
						{
							imgSig.Top = Line2.Y1;
						}
					}
				}
				else
				{
					imgSig.Visible = false;
				}
				frmLienDischargeNotice.InstancePtr.Unload();
				// Me.Show vbModal, MDIParent
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing");
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will update all of the lien record that have just been printed
                rsLien.MoveFirst();
                while (!rsLien.EndOfFile())
                {
                    // kk 01092012 trocls-16  can't update recordset with a join
                    // rsLien.Edit
                    // rsLien.Fields("PrintedLDN") = True
                    // rsLien.Update
                    rsTemp.Execute(
                        "UPDATE LienRec SET PrintedLDN = 1 WHERE ID = " +
                        FCConvert.ToString(rsLien.Get_Fields_Int32("LienKey")), modExtraModules.strCLDatabase);
                    rsLien.MoveNext();
                }

                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Error Updating Lien Records");
            }
            finally
            {
				rsTemp.DisposeOf();
                rsLien.DisposeOf();
                rsRateRec.DisposeOf();

			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsLien.EndOfFile())
			{
				lngCurrentLien = FCConvert.ToInt32(rsLien.Get_Fields_Int32("ID"));
				FillVariables();
				SetStrings();
				rsLien.MoveNext();
			}
			else
			{
			}
		}

		private void FillVariables()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsInfo = new clsDRWrapper();
				clsDRWrapper rsREMaster = new clsDRWrapper();
				string strFields;
				strFields = "Account,Name1,Name2,BillingYear,LienRec.RateKey as LienRateKey,Book,Page,MapLot,TranCode   ";
				rsInfo.OpenRecordset("SELECT " + strFields + " FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE LienRec.ID = " + FCConvert.ToString(lngCurrentLien), modExtraModules.strCLDatabase);
				if (!rsInfo.EndOfFile())
				{
					// Row - 4  'Name1
					strOwnerName = FCConvert.ToString(rsInfo.Get_Fields_String("Name1"));
					// Row - 5  'Name2
					if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name2"))) != "")
					{
						strOwnerName += " " + FCConvert.ToString(rsInfo.Get_Fields_String("Name2"));
					}
					// Row - 6  'Payment Date
					dtPaymentDate = (DateTime)rsLien.Get_Fields_DateTime("DatePaid");
					// TODO: Field [LienRateKey] not found!! (maybe it is an alias?)
					rsRateRec.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(rsInfo.Get_Fields("LienRateKey")));
					if (!rsRateRec.EndOfFile())
					{
						// Row - 7  'Filing Date
						dtLienDate = (DateTime)rsRateRec.Get_Fields_DateTime("BillingDate");
					}
					else
					{
						dtLienDate = DateTime.FromOADate(0);
					}
					// Row - 8  'Book
					// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
					strBook = FCConvert.ToString(rsInfo.Get_Fields("Book"));
					if (Strings.Trim(strBook) == "")
					{
						// Or val(strBook) = 0 Then     'kk11112014 trout-1111  Add check for Book = 0
						strBook = Strings.StrDup(10, "_");
					}
					// Row - 9  'Page
					// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
					strPage = FCConvert.ToString(rsInfo.Get_Fields("Page"));
					if (Strings.Trim(strPage) == "")
					{
						// Or val(strPage) = 0 Then     'kk11112014 trout-1111  Add check for Page = 0
						strPage = Strings.StrDup(10, "_");
					}
					if (FCConvert.ToString(rsInfo.Get_Fields_String("MapLot")) != "")
					{
						lblMapLot.Text = "Map Lot : " + FCConvert.ToString(rsInfo.Get_Fields_String("MapLot"));
					}
					else
					{
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						rsREMaster.OpenRecordset("SELECT RSMapLot FROM Master WHERE RSCard = 1 AND RSAccount = " + FCConvert.ToString(rsInfo.Get_Fields("Account")));
						if (!rsREMaster.EndOfFile())
						{
							lblMapLot.Text = "Map Lot : " + FCConvert.ToString(rsREMaster.Get_Fields_String("RSMapLot"));
						}
						else
						{
							lblMapLot.Text = "";
						}
					}
					if (modGlobal.Statics.gboolMultipleTowns)
					{
						// TODO: Check the table for the column [TranCode] and replace with corresponding Get_Field method
						strMuniName = GetMuniName_2(FCConvert.ToInt32(rsInfo.Get_Fields("TranCode")));
						if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
						{
							lblTownHeader.Text = modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName;
						}
						else
						{
							lblTownHeader.Text = strMuniName;
						}
					}
					strYear = FCConvert.ToString(Conversion.Val(Strings.Left(FCConvert.ToString(rsInfo.Get_Fields_Int32("BillingYear")), 4)));
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					fldAccount.Text = "Acct : " + modGlobal.PadToString(rsInfo.Get_Fields("Account"), 5);
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Filling Variables");
			}
		}

		private string GetMuniName_2(int lngTownNumber)
		{
			return GetMuniName(ref lngTownNumber);
		}

		private string GetMuniName(ref int lngTownNumber)
		{
			string GetMuniName = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsMulti = new clsDRWrapper();
				if (modGlobal.Statics.gboolMultipleTowns)
				{
					rsMulti.OpenRecordset("SELECT * FROM tblRegions WHERE TownNumber = " + FCConvert.ToString(lngTownNumber), "CentralData");
					if (!rsMulti.EndOfFile())
					{
						GetMuniName = FCConvert.ToString(rsMulti.Get_Fields_String("TownName"));
					}
					else
					{
						GetMuniName = modGlobalConstants.Statics.MuniName;
					}
				}
				else
				{
					GetMuniName = modGlobalConstants.Statics.MuniName;
				}
				return GetMuniName;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				GetMuniName = modGlobalConstants.Statics.MuniName;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Getting Town Name");
			}
			return GetMuniName;
		}

		private void arAllLienDischargeNotice_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arAllLienDischargeNotice.Text	= "Lien Discharge Notice";
			//arAllLienDischargeNotice.Icon	= "arAllLienDischargeNotice.dsx":0000";
			//arAllLienDischargeNotice.Left	= 0;
			//arAllLienDischargeNotice.Top	= 0;
			//arAllLienDischargeNotice.Width	= 15915;
			//arAllLienDischargeNotice.Height	= 9945;
			//arAllLienDischargeNotice.StartUpPosition	= 3;
			//arAllLienDischargeNotice.SectionData	= "arAllLienDischargeNotice.dsx":058A;
			//End Unmaped Properties
		}
	}
}
