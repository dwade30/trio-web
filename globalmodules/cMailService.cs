﻿using Wisej.Web;
using fecherFoundation;
using System;
using TWSharedLibrary;

namespace Global
{
	public class cEMailService
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cEmailConfig emailConfig = new cEmailConfig();
		//private cEMailConfigController eConfigController = new cEMailConfigController();
		private cEmailConfig emailConfig_AutoInitialized;

		private cEmailConfig emailConfig
		{
			get
			{
				if (emailConfig_AutoInitialized == null)
				{
					emailConfig_AutoInitialized = new cEmailConfig();
				}
				return emailConfig_AutoInitialized;
			}
			set
			{
				emailConfig_AutoInitialized = value;
			}
		}

		private cEMailConfigController eConfigController_AutoInitialized;

		private cEMailConfigController eConfigController
		{
			get
			{
				if (eConfigController_AutoInitialized == null)
				{
					eConfigController_AutoInitialized = new cEMailConfigController();
				}
				return eConfigController_AutoInitialized;
			}
			set
			{
				eConfigController_AutoInitialized = value;
			}
		}
		// Private Const CNSTCHILKATEMAILLICENSE As String = "HARRISMail_vEoVPOQtXRFY"
		const string CNSTCHILKATEMAILLICENSE = "HARRISMAILQ_HEdcCkDH9V0y";
		const string CNSTDefaultMailPassword = "Tr!0FuseMail1234";

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// VBto upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public void Initialize(ref cEmailConfig eConfig)
		{
			emailConfig = eConfig;
		}

		private bool ValidConfig()
		{
			bool ValidConfig = false;
			ValidConfig = false;
			if (emailConfig.Password == "")
			{
				return ValidConfig;
			}
			if (emailConfig.SMTPHost == "")
			{
				return ValidConfig;
			}
			if (emailConfig.UserName == "")
			{
				return ValidConfig;
			}
			ValidConfig = true;
			return ValidConfig;
		}

		public object GetEmailConfigByUserID(int lngUserID)
		{
			object GetEmailConfigByUserID = null;
			ClearErrors();
			cEmailConfig eConfig;
			eConfig = eConfigController.GetConfigurationByUserID(lngUserID);
			if (eConfigController.HadError)
			{
				lngLastError = eConfigController.LastErrorNumber;
				strLastError = eConfigController.LastErrorMessage;
				return GetEmailConfigByUserID;
			}
			GetEmailConfigByUserID = eConfig;
			return GetEmailConfigByUserID;
			ErrorHandler:
			;
			lngLastError = Information.Err().Number;
			strLastError = Information.Err().Description;
			return GetEmailConfigByUserID;
		}

		public cEmailConfig GetEffectiveEMailConfigByUserID(int lngUserID)
		{
			cEmailConfig GetEffectiveEMailConfigByUserID = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cEmailConfig defConfig;
				cEmailConfig uConfig;
				defConfig = eConfigController.GetConfigurationByUserID(0);
				if (eConfigController.HadError)
				{
					lngLastError = eConfigController.LastErrorNumber;
					strLastError = eConfigController.LastErrorMessage;
					return GetEffectiveEMailConfigByUserID;
				}
				defConfig.ID = 0;
				// this will not be a legit record
				uConfig = eConfigController.GetConfigurationByUserID(lngUserID);
				if (!eConfigController.HadError)
				{
					if (!(uConfig == null))
					{
						if (uConfig.ID > 0)
						{
							if (uConfig.DefaultFromAddress != "")
							{
								defConfig.DefaultFromAddress = uConfig.DefaultFromAddress;
								defConfig.AuthMethod = uConfig.AuthMethod;
								defConfig.UseSSL = uConfig.UseSSL;
								defConfig.UseTransportLayerSecurity = uConfig.UseTransportLayerSecurity;
							}
							if (uConfig.DefaultFromName != "")
							{
								defConfig.DefaultFromName = uConfig.DefaultFromName;
								defConfig.AuthMethod = uConfig.AuthMethod;
								defConfig.UseSSL = uConfig.UseSSL;
								defConfig.UseTransportLayerSecurity = uConfig.UseTransportLayerSecurity;
							}
							if (uConfig.Password != "")
							{
								defConfig.Password = uConfig.Password;
								defConfig.AuthMethod = uConfig.AuthMethod;
								defConfig.UseSSL = uConfig.UseSSL;
								defConfig.UseTransportLayerSecurity = uConfig.UseTransportLayerSecurity;
							}
							if (uConfig.SMTPHost != "")
							{
								defConfig.SMTPHost = uConfig.SMTPHost;
								defConfig.AuthMethod = uConfig.AuthMethod;
								defConfig.UseSSL = uConfig.UseSSL;
								defConfig.UseTransportLayerSecurity = uConfig.UseTransportLayerSecurity;
							}
							if (uConfig.UserName != "")
							{
								defConfig.UserName = uConfig.UserName;
								defConfig.AuthMethod = uConfig.AuthMethod;
								defConfig.UseSSL = uConfig.UseSSL;
								defConfig.UseTransportLayerSecurity = uConfig.UseTransportLayerSecurity;
							}
							if (uConfig.SMTPPort > 0)
							{
								defConfig.SMTPPort = uConfig.SMTPPort;
							}
						}
					}
					if (Strings.Trim(defConfig.SMTPHost) == string.Empty)
					{
						string strNameToUse = "";
						if (Strings.UCase(modGlobalConstants.Statics.gstrCityTown) == "TOWN" || Strings.UCase(modGlobalConstants.Statics.gstrCityTown) == "CITY")
						{
							strNameToUse = modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName;
						}
						if (strNameToUse != "")
						{
							if (defConfig.DefaultFromAddress == "")
							{
								defConfig.DefaultFromAddress = "NoReply@" + strNameToUse.Replace(" ", "") + ".com";
							}
							if (defConfig.DefaultFromName == "")
							{
								defConfig.DefaultFromName = strNameToUse;
							}
						}
						else
						{
							if (defConfig.DefaultFromAddress == "")
							{
								defConfig.DefaultFromAddress = "NoReply@" + modGlobalConstants.Statics.MuniName + "Office.com";
							}
							if (defConfig.DefaultFromName == "")
							{
								defConfig.DefaultFromName = modGlobalConstants.Statics.MuniName;
							}
						}
						defConfig.Password = CNSTDefaultMailPassword;
						defConfig.UserName = "trio";
						defConfig.SMTPHost = "SMTP.FUSEMAIL.NET";
						defConfig.AuthMethod = "LOGIN";
						defConfig.SMTPPort = 2500;
						defConfig.UseSSL = false;
						defConfig.UseTransportLayerSecurity = true;
					}
				}
				else
				{
					lngLastError = eConfigController.LastErrorNumber;
					strLastError = eConfigController.LastErrorMessage;
					return GetEffectiveEMailConfigByUserID;
				}
				GetEffectiveEMailConfigByUserID = defConfig;
				return GetEffectiveEMailConfigByUserID;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
			}
			return GetEffectiveEMailConfigByUserID;
		}

		public void SendEmail(ref cEmail eMailObject)
		{
			ClearErrors();
            Chilkat.MailMan eMailer = new Chilkat.MailMan();
            Chilkat.Email mailToSend = new Chilkat.Email();
            try
            {
                // On Error GoTo ErrorHandler
                bool lngSuccess;

                lngSuccess = false;
                lngSuccess = eMailer.UnlockComponent("HRRSGV.CBX062020_T7hQfNLa7U5n");
                //if (!lngSuccess)
                //{
                //    lngLastError = 9999;
                //    strLastError = eMailer.LastErrorText;
                //    return;
                //}

                eMailer.SmtpHost = emailConfig.SMTPHost;
                eMailer.SmtpSsl = emailConfig.UseSSL;
                if (emailConfig.SMTPPort > 0)
                {
                    eMailer.SmtpPort = emailConfig.SMTPPort;
                }
                else
                {
                    emailConfig.SMTPPort = 25;
                }

                eMailer.SmtpUsername = emailConfig.UserName;
                eMailer.SmtpPassword = emailConfig.Password;
                eMailer.SmtpAuthMethod = emailConfig.AuthMethod;
                eMailObject.SendTos.MoveFirst();
                while (eMailObject.SendTos.IsCurrent())
                {
                    mailToSend.AddTo("", (string) eMailObject.SendTos.GetCurrentItem());
                    eMailObject.SendTos.MoveNext();
                }

                eMailObject.CCs.MoveFirst();
                while (eMailObject.CCs.IsCurrent())
                {
                    mailToSend.AddCC("", (string) eMailObject.CCs.GetCurrentItem());
                    eMailObject.CCs.MoveNext();
                }

                cEmailDataAttachment dAttach;
                // VBto upgrade warning: datAttach As Variant --> As cEmailDataAttachment
                cEmailDataAttachment datAttach;
                int attachCount = 0;
                eMailObject.Attachments.MoveFirst();
                while (eMailObject.Attachments.IsCurrent())
                {
                    dAttach = (cEmailDataAttachment) eMailObject.Attachments.GetCurrentItem();
                    attachCount = eMailObject.Attachments.GetCurrentIndex() - 1;
                    datAttach = dAttach;
                    if (dAttach.IncludesData)
                    {
                        // can't test datAttach against nothing
                        mailToSend.AddDataAttachment(dAttach.AttachmentName, dAttach.GetAttachmentData());
                    }
                    else
                    {
                        mailToSend.AddFileAttachment(dAttach.AttachmentName);
                        mailToSend.SetAttachmentFilename(attachCount, dAttach.NewName);
                    }

                    eMailObject.Attachments.MoveNext();
                }

                if (eMailObject.IsHighPriority)
                {
                    mailToSend.AddHeaderField("X-Priority", "1");
                }

                if (eMailObject.PlainTextBody != "")
                {
                    mailToSend.Body = eMailObject.PlainTextBody;
                }

                if (eMailObject.HtmlBody != "")
                {
                    mailToSend.SetHtmlBody(eMailObject.HtmlBody);
                }

                mailToSend.Subject = eMailObject.Subject;
                if (eMailObject.FromName != "")
                {
                    mailToSend.FromName = eMailObject.FromName;
                }
                else
                {
                    mailToSend.FromName = emailConfig.DefaultFromName;
                }

                if (eMailObject.FromAddress != "")
                {
                    mailToSend.FromAddress = eMailObject.FromAddress;
                }
                else
                {
                    mailToSend.FromAddress = emailConfig.DefaultFromAddress;
                }

                if (eMailObject.BounceAddress != "")
                {
                    mailToSend.BounceAddress = eMailObject.BounceAddress;
                }

                if (eMailObject.ReplyToAddress != "")
                {
                    mailToSend.ReplyTo = eMailObject.ReplyToAddress;
                }

                if (emailConfig.UseTransportLayerSecurity)
                {
                    eMailer.StartTLS = true;
                }

                lngSuccess = eMailer.SendEmail(mailToSend);
                if (!lngSuccess)
                {
                    lngLastError = 9999;
                    strLastError = eMailer.LastErrorText;
                }

                eMailer.CloseSmtpConnection();
                return;
            }
            catch (Exception ex)
            {
				StaticSettings.GlobalTelemetryService.TrackException(ex);
				lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
            finally
            {
				eMailer.Dispose();
                eMailer = null;
            }
		}

		public void SendEmailCollection(ref cGenericCollection eMailColl)
		{
			ClearErrors();
			if (!(eMailColl == null))
			{
				cEmail tMail;
				eMailColl.MoveFirst();
				while (eMailColl.IsCurrent())
				{
					App.DoEvents();
					tMail = (cEmail)eMailColl.GetCurrentItem();
					SendEmail(ref tMail);
					eMailColl.MoveNext();
				}
			}
			return;
			ErrorHandler:
			;
			lngLastError = Information.Err().Number;
			strLastError = Information.Err().Description;
		}
	}
}
