﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmEmailSignatures.
	/// </summary>
	public partial class frmEmailSignatures : BaseForm
	{
		public frmEmailSignatures()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEmailSignatures InstancePtr
		{
			get
			{
				return (frmEmailSignatures)Sys.GetInstance(typeof(frmEmailSignatures));
			}
		}

		protected frmEmailSignatures _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		bool boolDataChanged;
		int intSigType;
		// VBto upgrade warning: intSignatureType As short	OnWriteFCConvert.ToInt32(
		public void Init(short intSignatureType = 1, bool boolModal = false)
		{
			intSigType = intSignatureType;
			switch (intSigType)
			{
				case modEMail.CNSTEMAILSIGNATURETYPEPERSONAL:
					{
						//Toolbar1.Buttons["btnEditPersonal"].Value = FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
						this.HeaderText.Text = "Personal Signature";
						break;
					}
				case modEMail.CNSTEMAILSIGNATURETYPEGLOBAL:
					{
						//Toolbar1.Buttons["btnEditCompany"].Value = FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
						this.HeaderText.Text = "Company Signature";
						break;
					}
				case modEMail.CNSTEMAILSIGNATURETYPEDISCLAIMER:
					{
						//Toolbar1.Buttons["btnEditDisclaimer"].Value = FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
						this.HeaderText.Text = "Disclaimer Signature";
						break;
					}
			}
			//end switch
			LoadSignature();
			if (boolModal)
			{
				this.Show(FormShowEnum.Modal);
			}
			else
			{
				this.Show(App.MainForm);
			}
		}

		private void frmEmailSignatures_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmEmailSignatures_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEmailSignatures.Icon	= "frmEmailSignatures.frx":0000";
			//frmEmailSignatures.FillStyle	= 0;
			//frmEmailSignatures.ScaleWidth	= 5880;
			//frmEmailSignatures.ScaleHeight	= 4065;
			//frmEmailSignatures.LinkTopic	= "Form2";
			//frmEmailSignatures.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Courier New";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//rtfSignature.BorderStyle	= 0;
			//rtfSignature.ScrollBars	= 2;
			//rtfSignature.AutoVerbMenu	= true;
			//rtfSignature.TextRTF	= $"frmEmailSignatures.frx":058A;
			//Toolbar1.ButtonWidth	= 820;
			//Toolbar1.ButtonHeight	= 794;
			//Buttons.NumButtons	= 4;
			//Button2.Style	= 2;
			//Button3.Style	= 2;
			//Button4.Style	= 2;
			//Images.NumListImages	= 4;
			//vsElasticLight1.OleObjectBlob	= "frmEmailSignatures.frx":2B85";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuEditCompany_Click(object sender, System.EventArgs e)
		{
			if (intSigType != modEMail.CNSTEMAILSIGNATURETYPEGLOBAL)
			{
				if (boolDataChanged)
				{
					if (FCMessageBox.Show("Save changes to the current signature?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Save?") == DialogResult.Yes)
					{
						if (!SaveSignature())
						{
							ResetButtons();
							return;
						}
					}
				}
				intSigType = modEMail.CNSTEMAILSIGNATURETYPEGLOBAL;
				LoadSignature();
			}
		}

		public void mnuEditCompany_Click()
		{
			mnuEditCompany_Click(cmdEditCompany, new System.EventArgs());
		}

		private void mnuEditDisclaimer_Click(object sender, System.EventArgs e)
		{
			if (intSigType != modEMail.CNSTEMAILSIGNATURETYPEDISCLAIMER)
			{
				if (boolDataChanged)
				{
					if (FCMessageBox.Show("Save changes to the current signature?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Save?") == DialogResult.Yes)
					{
						if (!SaveSignature())
						{
							ResetButtons();
							return;
						}
					}
				}
				intSigType = modEMail.CNSTEMAILSIGNATURETYPEDISCLAIMER;
				LoadSignature();
			}
		}

		public void mnuEditDisclaimer_Click()
		{
			mnuEditDisclaimer_Click(cmdEditDisclaimer, new System.EventArgs());
		}

		private void mnuEditPersonal_Click(object sender, System.EventArgs e)
		{
			if (intSigType != modEMail.CNSTEMAILSIGNATURETYPEPERSONAL)
			{
				if (boolDataChanged)
				{
					if (FCMessageBox.Show("Save changes to the current signature?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Save?") == DialogResult.Yes)
					{
						if (!SaveSignature())
						{
							ResetButtons();
							return;
						}
					}
				}
				intSigType = modEMail.CNSTEMAILSIGNATURETYPEPERSONAL;
				LoadSignature();
			}
		}

		public void mnuEditPersonal_Click()
		{
			mnuEditPersonal_Click(cmdEditPersonal, new System.EventArgs());
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveSignature();
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (SaveSignature())
			{
				mnuExit_Click();
			}
		}

		private void rtfSignature_KeyDown(object sender, KeyEventArgs e)
		{
			boolDataChanged = true;
		}

		private void LoadSignature()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				//FC:FINAL:AM:#i208 - check for intSigType
				//if (Toolbar1.Buttons["btnEditPersonal"].Value == FCToolBarButton.ToolbarButtonValueConstants.tbrPressed)
				if (intSigType == modEMail.CNSTEMAILSIGNATURETYPEPERSONAL)
				{
					rsLoad.OpenRecordset("select * from emailsignatures where sigtype = " + FCConvert.ToString(modEMail.CNSTEMAILSIGNATURETYPEPERSONAL) + " and userid = " + FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UserID()), "SystemSettings");
					intSigType = modEMail.CNSTEMAILSIGNATURETYPEPERSONAL;
					this.HeaderText.Text = "Personal Signature";
				}
				//else if (Toolbar1.Buttons["btnEditCompany"].Value == FCToolBarButton.ToolbarButtonValueConstants.tbrPressed)
				else if (intSigType == modEMail.CNSTEMAILSIGNATURETYPEGLOBAL)
				{
					rsLoad.OpenRecordset("select * from emailsignatures where sigtype = " + FCConvert.ToString(modEMail.CNSTEMAILSIGNATURETYPEGLOBAL), "SystemSettings");
					intSigType = modEMail.CNSTEMAILSIGNATURETYPEGLOBAL;
					this.HeaderText.Text = "Company Signature";
				}
				else
				{
					// disclaimer
					rsLoad.OpenRecordset("select * from emailsignatures where sigtype = " + FCConvert.ToString(modEMail.CNSTEMAILSIGNATURETYPEDISCLAIMER), "SystemSettings");
					intSigType = modEMail.CNSTEMAILSIGNATURETYPEDISCLAIMER;
					this.HeaderText.Text = "Disclaimer Signature";
				}
				if (!rsLoad.EndOfFile())
				{
					rtfSignature.Text = FCConvert.ToString(rsLoad.Get_Fields_String("message"));
				}
				else
				{
					rtfSignature.Text = "";
				}
				boolDataChanged = false;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In LoadSignature", MsgBoxStyle.Critical, "Error");
			}
		}

		private bool SaveSignature()
		{
			bool SaveSignature = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				SaveSignature = false;
				switch (intSigType)
				{
					case modEMail.CNSTEMAILSIGNATURETYPEPERSONAL:
						{
							rsSave.OpenRecordset("select * from emailsignatures where sigtype = " + FCConvert.ToString(modEMail.CNSTEMAILSIGNATURETYPEPERSONAL) + " and userid = " + FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UserID()), "SystemSettings");
							if (!rsSave.EndOfFile())
							{
								rsSave.Edit();
							}
							else
							{
								rsSave.AddNew();
								rsSave.Set_Fields("sigtype", modEMail.CNSTEMAILSIGNATURETYPEPERSONAL);
								rsSave.Set_Fields("userid", modGlobalConstants.Statics.clsSecurityClass.Get_UserID());
							}
							break;
						}
					case modEMail.CNSTEMAILSIGNATURETYPEGLOBAL:
						{
							rsSave.OpenRecordset("select * from emailsignatures where sigtype = " + FCConvert.ToString(modEMail.CNSTEMAILSIGNATURETYPEGLOBAL), "SystemSettings");
							if (!rsSave.EndOfFile())
							{
								rsSave.Edit();
							}
							else
							{
								rsSave.AddNew();
								rsSave.Set_Fields("sigtype", modEMail.CNSTEMAILSIGNATURETYPEGLOBAL);
								rsSave.Set_Fields("UserID", 0);
							}
							break;
						}
					case modEMail.CNSTEMAILSIGNATURETYPEDISCLAIMER:
						{
							// disclaimer
							rsSave.OpenRecordset("select * from emailsignatures where sigtype = " + FCConvert.ToString(modEMail.CNSTEMAILSIGNATURETYPEDISCLAIMER), "SystemSettings");
							if (!rsSave.EndOfFile())
							{
								rsSave.Edit();
							}
							else
							{
								rsSave.AddNew();
								rsSave.Set_Fields("sigtype", modEMail.CNSTEMAILSIGNATURETYPEDISCLAIMER);
								rsSave.Set_Fields("userid", 0);
							}
							break;
						}
				}
				//end switch
				rsSave.Set_Fields("message", rtfSignature.Text);
				rsSave.Update();
				boolDataChanged = false;
				SaveSignature = true;
				FCMessageBox.Show("Save Successful", MsgBoxStyle.Information, "Saved");
				return SaveSignature;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In SaveSignature", MsgBoxStyle.Critical, "Error");
			}
			return SaveSignature;
		}

		private void ResetButtons()
		{
			switch (intSigType)
			{
				case modEMail.CNSTEMAILSIGNATURETYPEPERSONAL:
					{
						//Toolbar1.Buttons["btnEditPersonal"].Value = FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
						this.HeaderText.Text = "Personal Signature";
						break;
					}
				case modEMail.CNSTEMAILSIGNATURETYPEGLOBAL:
					{
						//Toolbar1.Buttons["btnEditCompany"].Value = FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
						this.HeaderText.Text = "Company Signature";
						break;
					}
				case modEMail.CNSTEMAILSIGNATURETYPEDISCLAIMER:
					{
						//Toolbar1.Buttons["btnEditDisclaimer"].Value = FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
						this.HeaderText.Text = "Disclaimer Signature";
						break;
					}
			}
			//end switch
		}

		private void Toolbar1_ButtonClick(object sender, fecherFoundation.FCToolBarButtonClickEventArgs e)
		{
			ToolBarButton Button = e.Button;
			if (Strings.UCase(Button.Name) == "BTNSAVESIGNATURE")
			{
				SaveSignature();
			}
			else if (Strings.UCase(Button.Name) == "BTNEDITPERSONAL")
			{
				mnuEditPersonal_Click();
			}
			else if (Strings.UCase(Button.Name) == "BTNEDITCOMPANY")
			{
				mnuEditCompany_Click();
			}
			else if (Strings.UCase(Button.Name) == "BTNEDITDISCLAIMER")
			{
				mnuEditDisclaimer_Click();
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuSaveContinue_Click(sender, e);
		}

		private void cmdEditPersonal_Click(object sender, EventArgs e)
		{
			this.mnuEditPersonal_Click(sender, e);
		}

		private void cmdEditCompany_Click(object sender, EventArgs e)
		{
			this.mnuEditCompany_Click(sender, e);
		}

		private void cmdEditDisclaimer_Click(object sender, EventArgs e)
		{
			this.mnuEditDisclaimer_Click(sender, e);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			this.mnuSave_Click(sender, e);
		}
	}
}
