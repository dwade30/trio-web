﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Global
{
    public class cUTSettings
    {
        public bool AllowAutoPay { get; set; } = false;
        public int UnitsInYear { get; set; } = 0;
        public int AutoPayOption { get; set; } = 0;
        public string PayFirstService { get; set; } = "";
        public string TownServiceType { get; set; } = "";

        public bool OffersWaterServices()
        {
            return TownServiceType.ToLower() != "s";
        }

        public bool OffersSewerServices()
        {
            return TownServiceType.ToLower() != "w";
        }

        public bool OffersBothServices()
        {
            return TownServiceType.ToLower() == "b";
        }
    }
}
