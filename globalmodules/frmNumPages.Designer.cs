﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;

namespace Global
{
	/// <summary>
	/// Summary description for frmNumPages.
	/// </summary>
	partial class frmNumPages : BaseForm
	{
		public fecherFoundation.FCTextBox txtEndPage;
		public fecherFoundation.FCTextBox txtStartPage;
		public fecherFoundation.FCLabel label2;
		public fecherFoundation.FCLabel LblPages;
		public fecherFoundation.FCLabel Label1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuPrintMenu;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNumPages));
			this.txtEndPage = new fecherFoundation.FCTextBox();
			this.txtStartPage = new fecherFoundation.FCTextBox();
			this.label2 = new fecherFoundation.FCLabel();
			this.LblPages = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuPrintMenu = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 196);
			this.BottomPanel.Size = new System.Drawing.Size(302, 102);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtEndPage);
			this.ClientArea.Controls.Add(this.txtStartPage);
			this.ClientArea.Controls.Add(this.label2);
			this.ClientArea.Controls.Add(this.LblPages);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(302, 136);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(302, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(166, 30);
			this.HeaderText.Text = "Pages to Print";
			// 
			// txtEndPage
			// 
			this.txtEndPage.AutoSize = false;
			this.txtEndPage.BackColor = System.Drawing.SystemColors.Window;
			this.txtEndPage.LinkItem = null;
			this.txtEndPage.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEndPage.LinkTopic = null;
			this.txtEndPage.Location = new System.Drawing.Point(175, 65);
			this.txtEndPage.Name = "txtEndPage";
			this.txtEndPage.Size = new System.Drawing.Size(75, 40);
			this.txtEndPage.TabIndex = 3;
			this.txtEndPage.Text = "1";
			this.txtEndPage.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtStartPage
			// 
			this.txtStartPage.AutoSize = false;
			this.txtStartPage.BackColor = System.Drawing.SystemColors.Window;
			this.txtStartPage.LinkItem = null;
			this.txtStartPage.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStartPage.LinkTopic = null;
			this.txtStartPage.Location = new System.Drawing.Point(40, 65);
			this.txtStartPage.Name = "txtStartPage";
			this.txtStartPage.Size = new System.Drawing.Size(75, 40);
			this.txtStartPage.TabIndex = 1;
			this.txtStartPage.Text = "1";
			this.txtStartPage.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(134, 110);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(25, 15);
			this.label2.TabIndex = 4;
			this.label2.Text = "OF ";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// LblPages
			// 
			this.LblPages.AutoSize = true;
			this.LblPages.Location = new System.Drawing.Point(109, 30);
			this.LblPages.Name = "LblPages";
			this.LblPages.Size = new System.Drawing.Size(90, 15);
			this.LblPages.TabIndex = 0;
			this.LblPages.Text = "PRINT PAGES";
			this.LblPages.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(134, 81);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(25, 15);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "TO";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mnuPrintMenu
			// 
			this.mnuPrintMenu.Index = -1;
			this.mnuPrintMenu.Name = "mnuPrintMenu";
			this.mnuPrintMenu.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(80, 28);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(120, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Print";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmNumPages
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(302, 298);
			this.FillColor = 0;
			this.Name = "frmNumPages";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Pages to Print";
			this.Load += new System.EventHandler(this.frmNumPages_Load);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcess;
	}
}
