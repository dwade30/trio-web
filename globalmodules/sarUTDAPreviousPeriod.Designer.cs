﻿namespace Global
{
	/// <summary>
	/// Summary description for sarUTDAPreviousPeriod.
	/// </summary>
	partial class sarUTDAPreviousPeriod
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarUTDAPreviousPeriod));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldPaymentPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCorrectionPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPaymentInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCorrectionInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPaymentCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCorrectionCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPaymentTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCorrectionTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentPrin = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCorrectionPrin = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCorrectionInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCorrectionCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblPaymentTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCorrectionTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblTotalCash = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalPaymentPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCorrectionPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPaymentInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCorrectionInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPaymentCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCorrectionCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalPaymentTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCorrectionTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrectionPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrectionInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrectionCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrectionTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCorrectionPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCorrectionInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCorrectionCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCorrectionTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldPaymentPrin,
				this.fldCorrectionPrin,
				this.fldPaymentInterest,
				this.fldCorrectionInt,
				this.fldPaymentCosts,
				this.fldCorrectionCosts,
				this.fldTotal,
				this.fldYear,
				this.fldPaymentTax,
				this.fldCorrectionTax
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldPaymentPrin
			// 
			this.fldPaymentPrin.Height = 0.1875F;
			this.fldPaymentPrin.Left = 0.9375F;
			this.fldPaymentPrin.Name = "fldPaymentPrin";
			this.fldPaymentPrin.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPaymentPrin.Text = "0.00";
			this.fldPaymentPrin.Top = 0F;
			this.fldPaymentPrin.Width = 0.875F;
			// 
			// fldCorrectionPrin
			// 
			this.fldCorrectionPrin.Height = 0.1875F;
			this.fldCorrectionPrin.Left = 1.8125F;
			this.fldCorrectionPrin.Name = "fldCorrectionPrin";
			this.fldCorrectionPrin.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCorrectionPrin.Text = "0.00";
			this.fldCorrectionPrin.Top = 0F;
			this.fldCorrectionPrin.Width = 0.875F;
			// 
			// fldPaymentInterest
			// 
			this.fldPaymentInterest.Height = 0.1875F;
			this.fldPaymentInterest.Left = 4.5625F;
			this.fldPaymentInterest.Name = "fldPaymentInterest";
			this.fldPaymentInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPaymentInterest.Text = "0.00";
			this.fldPaymentInterest.Top = 0F;
			this.fldPaymentInterest.Width = 0.875F;
			// 
			// fldCorrectionInt
			// 
			this.fldCorrectionInt.Height = 0.1875F;
			this.fldCorrectionInt.Left = 5.4375F;
			this.fldCorrectionInt.Name = "fldCorrectionInt";
			this.fldCorrectionInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCorrectionInt.Text = "0.00";
			this.fldCorrectionInt.Top = 0F;
			this.fldCorrectionInt.Width = 0.875F;
			// 
			// fldPaymentCosts
			// 
			this.fldPaymentCosts.Height = 0.1875F;
			this.fldPaymentCosts.Left = 6.375F;
			this.fldPaymentCosts.Name = "fldPaymentCosts";
			this.fldPaymentCosts.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPaymentCosts.Text = "0.00";
			this.fldPaymentCosts.Top = 0F;
			this.fldPaymentCosts.Width = 0.875F;
			// 
			// fldCorrectionCosts
			// 
			this.fldCorrectionCosts.Height = 0.1875F;
			this.fldCorrectionCosts.Left = 7.25F;
			this.fldCorrectionCosts.Name = "fldCorrectionCosts";
			this.fldCorrectionCosts.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCorrectionCosts.Text = "0.00";
			this.fldCorrectionCosts.Top = 0F;
			this.fldCorrectionCosts.Width = 0.875F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 8.3125F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal.Text = "0.00";
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1.0625F;
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.1875F;
			this.fldYear.Left = 0F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldYear.Text = null;
			this.fldYear.Top = 0F;
			this.fldYear.Width = 0.625F;
			// 
			// fldPaymentTax
			// 
			this.fldPaymentTax.Height = 0.1875F;
			this.fldPaymentTax.Left = 2.75F;
			this.fldPaymentTax.Name = "fldPaymentTax";
			this.fldPaymentTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPaymentTax.Text = "0.00";
			this.fldPaymentTax.Top = 0F;
			this.fldPaymentTax.Width = 0.875F;
			// 
			// fldCorrectionTax
			// 
			this.fldCorrectionTax.Height = 0.1875F;
			this.fldCorrectionTax.Left = 3.625F;
			this.fldCorrectionTax.Name = "fldCorrectionTax";
			this.fldCorrectionTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCorrectionTax.Text = "0.00";
			this.fldCorrectionTax.Top = 0F;
			this.fldCorrectionTax.Width = 0.875F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader
			});
			this.ReportHeader.Name = "ReportHeader";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "- - - - - - Previous Period - - - - - -";
			this.lblHeader.Top = 0F;
			this.lblHeader.Visible = false;
			this.lblHeader.Width = 9.375F;
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblYear,
				this.lblTotal,
				this.lblPaymentPrin,
				this.lblCorrectionPrin,
				this.lblPaymentInt,
				this.lblCorrectionInt,
				this.lblPaymentCosts,
				this.lblCorrectionCosts,
				this.lblPrincipal,
				this.lblInterest,
				this.lblCosts,
				this.lnHeader,
				this.lblPaymentTax,
				this.lblCorrectionTax,
				this.lblTax
			});
			this.GroupHeader1.Height = 0.3333333F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// lblYear
			// 
			this.lblYear.Height = 0.1875F;
			this.lblYear.HyperLink = null;
			this.lblYear.Left = 0F;
			this.lblYear.Name = "lblYear";
			this.lblYear.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblYear.Text = "Bill";
			this.lblYear.Top = 0.125F;
			this.lblYear.Width = 0.625F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 8.3125F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.125F;
			this.lblTotal.Width = 1.0625F;
			// 
			// lblPaymentPrin
			// 
			this.lblPaymentPrin.Height = 0.1875F;
			this.lblPaymentPrin.HyperLink = null;
			this.lblPaymentPrin.Left = 0.9375F;
			this.lblPaymentPrin.Name = "lblPaymentPrin";
			this.lblPaymentPrin.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPaymentPrin.Text = "Payment";
			this.lblPaymentPrin.Top = 0.125F;
			this.lblPaymentPrin.Width = 0.875F;
			// 
			// lblCorrectionPrin
			// 
			this.lblCorrectionPrin.Height = 0.1875F;
			this.lblCorrectionPrin.HyperLink = null;
			this.lblCorrectionPrin.Left = 1.8125F;
			this.lblCorrectionPrin.Name = "lblCorrectionPrin";
			this.lblCorrectionPrin.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblCorrectionPrin.Text = "Correction";
			this.lblCorrectionPrin.Top = 0.125F;
			this.lblCorrectionPrin.Width = 0.875F;
			// 
			// lblPaymentInt
			// 
			this.lblPaymentInt.Height = 0.1875F;
			this.lblPaymentInt.HyperLink = null;
			this.lblPaymentInt.Left = 4.5625F;
			this.lblPaymentInt.Name = "lblPaymentInt";
			this.lblPaymentInt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPaymentInt.Text = "Payment";
			this.lblPaymentInt.Top = 0.125F;
			this.lblPaymentInt.Width = 0.875F;
			// 
			// lblCorrectionInt
			// 
			this.lblCorrectionInt.Height = 0.1875F;
			this.lblCorrectionInt.HyperLink = null;
			this.lblCorrectionInt.Left = 5.4375F;
			this.lblCorrectionInt.Name = "lblCorrectionInt";
			this.lblCorrectionInt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblCorrectionInt.Text = "Correction";
			this.lblCorrectionInt.Top = 0.125F;
			this.lblCorrectionInt.Width = 0.875F;
			// 
			// lblPaymentCosts
			// 
			this.lblPaymentCosts.Height = 0.1875F;
			this.lblPaymentCosts.HyperLink = null;
			this.lblPaymentCosts.Left = 6.375F;
			this.lblPaymentCosts.Name = "lblPaymentCosts";
			this.lblPaymentCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPaymentCosts.Text = "Payment";
			this.lblPaymentCosts.Top = 0.125F;
			this.lblPaymentCosts.Width = 0.875F;
			// 
			// lblCorrectionCosts
			// 
			this.lblCorrectionCosts.Height = 0.1875F;
			this.lblCorrectionCosts.HyperLink = null;
			this.lblCorrectionCosts.Left = 7.25F;
			this.lblCorrectionCosts.Name = "lblCorrectionCosts";
			this.lblCorrectionCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblCorrectionCosts.Text = "Correction";
			this.lblCorrectionCosts.Top = 0.125F;
			this.lblCorrectionCosts.Width = 0.875F;
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 0.9375F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblPrincipal.Text = "Principal";
			this.lblPrincipal.Top = 0F;
			this.lblPrincipal.Width = 1.75F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 4.5625F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 0F;
			this.lblInterest.Width = 1.75F;
			// 
			// lblCosts
			// 
			this.lblCosts.Height = 0.1875F;
			this.lblCosts.HyperLink = null;
			this.lblCosts.Left = 6.375F;
			this.lblCosts.Name = "lblCosts";
			this.lblCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblCosts.Text = "Costs";
			this.lblCosts.Top = 0F;
			this.lblCosts.Width = 1.75F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 0.3125F;
			this.lnHeader.Width = 7.5F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 7.5F;
			this.lnHeader.Y1 = 0.3125F;
			this.lnHeader.Y2 = 0.3125F;
			// 
			// lblPaymentTax
			// 
			this.lblPaymentTax.Height = 0.1875F;
			this.lblPaymentTax.HyperLink = null;
			this.lblPaymentTax.Left = 2.75F;
			this.lblPaymentTax.Name = "lblPaymentTax";
			this.lblPaymentTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPaymentTax.Text = "Payment";
			this.lblPaymentTax.Top = 0.125F;
			this.lblPaymentTax.Width = 0.875F;
			// 
			// lblCorrectionTax
			// 
			this.lblCorrectionTax.Height = 0.1875F;
			this.lblCorrectionTax.HyperLink = null;
			this.lblCorrectionTax.Left = 3.625F;
			this.lblCorrectionTax.Name = "lblCorrectionTax";
			this.lblCorrectionTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblCorrectionTax.Text = "Correction";
			this.lblCorrectionTax.Top = 0.125F;
			this.lblCorrectionTax.Width = 0.875F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 2.75F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 0F;
			this.lblTax.Width = 1.75F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTotalCash,
				this.fldTotalPaymentPrin,
				this.fldTotalCorrectionPrin,
				this.fldTotalPaymentInt,
				this.fldTotalCorrectionInt,
				this.fldTotalPaymentCost,
				this.fldTotalCorrectionCosts,
				this.fldTotalTotal,
				this.lnTotals,
				this.fldTotalPaymentTax,
				this.fldTotalCorrectionTax
			});
			this.GroupFooter1.Height = 0.625F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblTotalCash
			// 
			this.lblTotalCash.Height = 0.1875F;
			this.lblTotalCash.HyperLink = null;
			this.lblTotalCash.Left = 0F;
			this.lblTotalCash.Name = "lblTotalCash";
			this.lblTotalCash.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTotalCash.Text = "Totals:";
			this.lblTotalCash.Top = 0.0625F;
			this.lblTotalCash.Width = 0.5625F;
			// 
			// fldTotalPaymentPrin
			// 
			this.fldTotalPaymentPrin.Height = 0.1875F;
			this.fldTotalPaymentPrin.Left = 0.5625F;
			this.fldTotalPaymentPrin.Name = "fldTotalPaymentPrin";
			this.fldTotalPaymentPrin.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPaymentPrin.Text = "0.00";
			this.fldTotalPaymentPrin.Top = 0.0625F;
			this.fldTotalPaymentPrin.Width = 0.75F;
			// 
			// fldTotalCorrectionPrin
			// 
			this.fldTotalCorrectionPrin.Height = 0.1875F;
			this.fldTotalCorrectionPrin.Left = 1.3125F;
			this.fldTotalCorrectionPrin.Name = "fldTotalCorrectionPrin";
			this.fldTotalCorrectionPrin.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCorrectionPrin.Text = "0.00";
			this.fldTotalCorrectionPrin.Top = 0.0625F;
			this.fldTotalCorrectionPrin.Width = 0.75F;
			// 
			// fldTotalPaymentInt
			// 
			this.fldTotalPaymentInt.Height = 0.1875F;
			this.fldTotalPaymentInt.Left = 3.5625F;
			this.fldTotalPaymentInt.Name = "fldTotalPaymentInt";
			this.fldTotalPaymentInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPaymentInt.Text = "0.00";
			this.fldTotalPaymentInt.Top = 0.0625F;
			this.fldTotalPaymentInt.Width = 0.75F;
			// 
			// fldTotalCorrectionInt
			// 
			this.fldTotalCorrectionInt.Height = 0.1875F;
			this.fldTotalCorrectionInt.Left = 4.3125F;
			this.fldTotalCorrectionInt.Name = "fldTotalCorrectionInt";
			this.fldTotalCorrectionInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCorrectionInt.Text = "0.00";
			this.fldTotalCorrectionInt.Top = 0.0625F;
			this.fldTotalCorrectionInt.Width = 0.75F;
			// 
			// fldTotalPaymentCost
			// 
			this.fldTotalPaymentCost.Height = 0.1875F;
			this.fldTotalPaymentCost.Left = 5.0625F;
			this.fldTotalPaymentCost.Name = "fldTotalPaymentCost";
			this.fldTotalPaymentCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPaymentCost.Text = "0.00";
			this.fldTotalPaymentCost.Top = 0.0625F;
			this.fldTotalPaymentCost.Width = 0.75F;
			// 
			// fldTotalCorrectionCosts
			// 
			this.fldTotalCorrectionCosts.Height = 0.1875F;
			this.fldTotalCorrectionCosts.Left = 5.8125F;
			this.fldTotalCorrectionCosts.Name = "fldTotalCorrectionCosts";
			this.fldTotalCorrectionCosts.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCorrectionCosts.Text = "0.00";
			this.fldTotalCorrectionCosts.Top = 0.0625F;
			this.fldTotalCorrectionCosts.Width = 0.75F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 6.5625F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTotal.Text = "0.00";
			this.fldTotalTotal.Top = 0.0625F;
			this.fldTotalTotal.Width = 0.9375F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 0.5625F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0F;
			this.lnTotals.Width = 6.9375F;
			this.lnTotals.X1 = 0.5625F;
			this.lnTotals.X2 = 7.5F;
			this.lnTotals.Y1 = 0F;
			this.lnTotals.Y2 = 0F;
			// 
			// fldTotalPaymentTax
			// 
			this.fldTotalPaymentTax.Height = 0.1875F;
			this.fldTotalPaymentTax.Left = 2.0625F;
			this.fldTotalPaymentTax.Name = "fldTotalPaymentTax";
			this.fldTotalPaymentTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPaymentTax.Text = "0.00";
			this.fldTotalPaymentTax.Top = 0.0625F;
			this.fldTotalPaymentTax.Width = 0.75F;
			// 
			// fldTotalCorrectionTax
			// 
			this.fldTotalCorrectionTax.Height = 0.1875F;
			this.fldTotalCorrectionTax.Left = 2.8125F;
			this.fldTotalCorrectionTax.Name = "fldTotalCorrectionTax";
			this.fldTotalCorrectionTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCorrectionTax.Text = "0.00";
			this.fldTotalCorrectionTax.Top = 0.0625F;
			this.fldTotalCorrectionTax.Width = 0.75F;
			// 
			// sarUTDAPreviousPeriod
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCorrectionTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrectionPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrectionInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrectionCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCorrectionTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCorrectionPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCorrectionInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCorrectionCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCorrectionTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCorrectionPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCorrectionInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCorrectionCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCorrectionTax;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentPrin;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCorrectionPrin;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCorrectionInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentCosts;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCorrectionCosts;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCorrectionTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalCash;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaymentPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCorrectionPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaymentInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCorrectionInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaymentCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCorrectionCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaymentTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCorrectionTax;
	}
}
