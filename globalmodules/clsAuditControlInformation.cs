﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using SharedApplication.Extensions;
using Wisej.Web;

namespace Global
{
	public class clsAuditControlInformation
	{
		//=========================================================
		public enum enuControlType
		{
			FlexGrid = 1,
			TextBox = 2,
			ComboBox = 3,
			CheckBox = 4,
			OptionButton = 5,
			FileListBox = 6,
			DriveListBox = 7,
			DirListBox = 8,
			ListBox = 9,
			DeletedControl = 10,
			Label = 11,
		}

		private string strControlName = string.Empty;
		private string strControlDescriptionName = string.Empty;
		private int intRow;
		private int intCol;
		private bool useKey = false;
		private int keyValue = 0;
		private int keyCol;
		// VBto upgrade warning: strOldValue As string	OnWrite(string, bool)
		private string strOldValue = string.Empty;
		// VBto upgrade warning: strNewValue As string	OnWrite(string, bool)
		private string strNewValue = string.Empty;
		private enuControlType ctyControlType;
		private int intArrayIndex;
		private int intListIndex;

		public bool UseKeyInsteadOfRowCol
		{
			set { useKey = value; }
			get { return useKey; }
		}

		public int KeyValue
		{
			set { keyValue = value; }
			get { return keyValue; }
		}

		public int KeyCol
		{
			set { keyCol = value; }
			get { return keyCol; }
		}

		public string ControlName
		{
			set
			{
				strControlName = value;
			}
			get
			{
				string ControlName = "";
				ControlName = strControlName;
				return ControlName;
			}
		}

		public string DataDescription
		{
			set
			{
				strControlDescriptionName = value;
			}
			get
			{
				string DataDescription = "";
				DataDescription = strControlDescriptionName;
				return DataDescription;
			}
		}

		public int GridRow
		{
			set
			{
				intRow = value;
			}
			get
			{
				int GridRow = 0;
				GridRow = intRow;
				return GridRow;
			}
		}

		public int Index
		{
			set
			{
				intArrayIndex = value;
			}
			get
			{
				int Index = 0;
				Index = intArrayIndex;
				return Index;
			}
		}

		public int ListIndex
		{
			set
			{
				intListIndex = value;
			}
			get
			{
				int ListIndex = 0;
				ListIndex = intListIndex;
				return ListIndex;
			}
		}

		public int GridCol
		{
			set
			{
				intCol = value;
			}
			get
			{
				int GridCol = 0;
				GridCol = intCol;
				return GridCol;
			}
		}

		public string OldValue
		{
			set
			{
				strOldValue = value;
			}
			get
			{
				string OldValue = "";
				OldValue = strOldValue;
				return OldValue;
			}
		}

		public string NewValue
		{
			set
			{
				strNewValue = value;
			}
			get
			{
				string NewValue = "";
				NewValue = strNewValue;
				return NewValue;
			}
		}

		public enuControlType ControlType
		{
			set
			{
				ctyControlType = value;
			}
			get
			{
				enuControlType ControlType = (enuControlType)0;
				ControlType = ctyControlType;
				return ControlType;
			}
		}
		// VBto upgrade warning: frm As Form	OnWrite(frmEditCentralParties)
		public void FillOldValue(Form frm)
		{
			if (intArrayIndex >= 0)
			{
				//FC:FINAL:DSE Control arrays are ported as different controls with name folowed with "_" index
				string sArrayIndex = "_" + intArrayIndex.ToString();
				if (ctyControlType == enuControlType.CheckBox)
				{
					if ((frm.GetAllControls(strControlName + sArrayIndex) as FCCheckBox).Value == FCCheckBox.ValueSettings.Checked)
					{
						strOldValue = FCConvert.ToString(true);
					}
					else
					{
						strOldValue = FCConvert.ToString(false);
					}
				}
				else if (ctyControlType == enuControlType.ComboBox || ctyControlType == enuControlType.TextBox)
				{
					strOldValue = frm.GetAllControls(strControlName + sArrayIndex).Text;
				}
				else if (ctyControlType == enuControlType.ListBox)
				{
					strOldValue = FCConvert.ToString((frm.GetAllControls(strControlName + sArrayIndex) as FCListBox).Selected(intListIndex));
				}
				else if (ctyControlType == enuControlType.OptionButton)
				{
					strOldValue = FCConvert.ToString((frm.GetAllControls(strControlName + sArrayIndex) as FCRadioButton).Value);
				}
				else if (ctyControlType == enuControlType.Label)
				{
					strOldValue = frm.GetAllControls(strControlName + sArrayIndex).Text;
				}
				else if (ctyControlType == enuControlType.DriveListBox)
				{
					strOldValue = (frm.GetAllControls(strControlName + sArrayIndex) as FCDriveListBox).Drive;
				}
				else if (ctyControlType == enuControlType.DirListBox)
				{
					strOldValue = (frm.GetAllControls(strControlName + sArrayIndex) as FCDirListBox).Path;
				}
				else if (ctyControlType == enuControlType.FileListBox)
				{
					strOldValue = (frm.GetAllControls(strControlName + sArrayIndex) as FCFileListBox).FileName;
				}
				else if (ctyControlType == enuControlType.FlexGrid)
				{
					int savedRow = intRow;

					FCGrid grid = (frm as Form).GetAllControls(strControlName + sArrayIndex) as FCGrid;

					if (UseKeyInsteadOfRowCol)
					{
						if (keyValue != 0)
						{
							for (int i = 0; i < grid.Rows; i++)
							{
								if (grid.TextMatrix(i, keyCol).ToIntegerValue() == keyValue)
								{
									intRow = i;
									break;
								}
							}
						}
					}

					if (grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol) != grid.TextMatrix(intRow, intCol))
					{
						strOldValue = grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol);
					}
					else
					{
						if (grid.ColDataType(intCol) == FCGrid.DataTypeSettings.flexDTBoolean)
						{
							if (grid.TextMatrix(intRow, intCol) == "true")
							{
								strOldValue = FCConvert.ToString(true);
							}
							else
							{
								strOldValue = FCConvert.ToString(false);
							}
						}
						else
						{
							strOldValue = grid.TextMatrix(intRow, intCol);
						}
					}

					intRow = savedRow;
				}
			}
			else
			{
				if (ctyControlType == enuControlType.CheckBox)
				{
					if ((frm.GetAllControls(strControlName) as FCCheckBox).Value == FCCheckBox.ValueSettings.Checked)
					{
						strOldValue = FCConvert.ToString(true);
					}
					else
					{
						strOldValue = FCConvert.ToString(false);
					}
				}
				else if (ctyControlType == enuControlType.ComboBox || ctyControlType == enuControlType.TextBox)
				{
					strOldValue = frm.GetAllControls(strControlName).Text;
				}
				else if (ctyControlType == enuControlType.ListBox)
				{
					strOldValue = FCConvert.ToString((frm.GetAllControls(strControlName) as FCListBox).Selected(intListIndex));
				}
				else if (ctyControlType == enuControlType.OptionButton)
				{
					strOldValue = FCConvert.ToString((frm.GetAllControls(strControlName) as FCRadioButton).Value);
				}
				else if (ctyControlType == enuControlType.Label)
				{
					strOldValue = frm.GetAllControls(strControlName).Text;
				}
				else if (ctyControlType == enuControlType.DriveListBox)
				{
					strOldValue = (frm.GetAllControls(strControlName) as FCDriveListBox).Drive;
				}
				else if (ctyControlType == enuControlType.DirListBox)
				{
					strOldValue = (frm.GetAllControls(strControlName) as FCDirListBox).Path;
				}
				else if (ctyControlType == enuControlType.FileListBox)
				{
					strOldValue = (frm.GetAllControls(strControlName) as FCFileListBox).FileName;
				}
				else if (ctyControlType == enuControlType.FlexGrid)
				{
					int savedRow = intRow;

					FCGrid grid = (frm as Form).GetAllControls(strControlName) as FCGrid;

					if (UseKeyInsteadOfRowCol)
					{
						if (keyValue != 0)
						{
							for (int i = 0; i < grid.Rows; i++)
							{
								if (grid.TextMatrix(i, keyCol).ToIntegerValue() == keyValue)
								{
									intRow = i;
									break;
								}
							}
						}
					}

					if (grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol) != grid.TextMatrix(intRow, intCol))
					{
						strOldValue = grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol);
					}
					else
					{
						if (grid.ColDataType(intCol) == FCGrid.DataTypeSettings.flexDTBoolean)
						{
							if (grid.TextMatrix(intRow, intCol) == "true")
							{
								strOldValue = FCConvert.ToString(true);
							}
							else
							{
								strOldValue = FCConvert.ToString(false);
							}
						}
						else
						{
							strOldValue = grid.TextMatrix(intRow, intCol);
						}
					}

					intRow = savedRow;
				}
			}
		}
		// VBto upgrade warning: frm As Form	OnWrite(frmEditCentralParties)
		public void FillNewValue(Form frm)
		{			
			if (intArrayIndex >= 0)
			{
				//FC:FINAL:MSH - i.issue #1026: Control arrays are ported as different controls with name folowed with "_" index
				string sArrayIndex = "_" + intArrayIndex.ToString();
				if (ctyControlType == enuControlType.CheckBox)
				{
					if ((frm.GetAllControls(strControlName + sArrayIndex) as FCCheckBox).Value == FCCheckBox.ValueSettings.Checked)
					{
						strNewValue = FCConvert.ToString(true);
					}
					else
					{
						strNewValue = FCConvert.ToString(false);
					}
				}
				else if (ctyControlType == enuControlType.ComboBox || ctyControlType == enuControlType.TextBox)
				{
					strNewValue = frm.GetAllControls(strControlName + sArrayIndex).Text;
				}
				else if (ctyControlType == enuControlType.ListBox)
				{
					strNewValue = FCConvert.ToString((frm.GetAllControls(strControlName + sArrayIndex) as FCListBox).Selected(intListIndex));
				}
				else if (ctyControlType == enuControlType.OptionButton)
				{
					strNewValue = FCConvert.ToString((frm.GetAllControls(strControlName + sArrayIndex) as FCRadioButton).Value);
				}
				else if (ctyControlType == enuControlType.Label)
				{
					strNewValue = frm.GetAllControls(strControlName + sArrayIndex).Text;
				}
				else if (ctyControlType == enuControlType.DriveListBox)
				{
					strNewValue = (frm.GetAllControls(strControlName + sArrayIndex) as FCDriveListBox).Drive;
				}
				else if (ctyControlType == enuControlType.DirListBox)
				{
					strNewValue = (frm.GetAllControls(strControlName + sArrayIndex) as FCDirListBox).Path;
				}
				else if (ctyControlType == enuControlType.FileListBox)
				{
					strNewValue = (frm.GetAllControls(strControlName + sArrayIndex) as FCFileListBox).FileName;
				}
				else if (ctyControlType == enuControlType.FlexGrid)
				{
					int savedRow = intRow;

					FCGrid grid = frm.GetAllControls(strControlName + sArrayIndex) as FCGrid;

					if (UseKeyInsteadOfRowCol)
					{
						if (keyValue != 0)
						{
							for (int i = 0; i < grid.Rows; i++)
							{
								if (grid.TextMatrix(i, keyCol).ToIntegerValue() == keyValue)
								{
									intRow = i;
									break;
								}
							}
						}
					}

					if (grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol) != grid.TextMatrix(intRow, intCol))
					{
						strNewValue = grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol);
					}
					else
					{
						if (grid.ColDataType(intCol) == FCGrid.DataTypeSettings.flexDTBoolean)
						{
							if (grid.TextMatrix(intRow, intCol) == "true")
							{
								strNewValue = FCConvert.ToString(true);
							}
							else
							{
								strNewValue = FCConvert.ToString(false);
							}
						}
						else
						{
							strNewValue = grid.TextMatrix(intRow, intCol);
						}
					}

					intRow = savedRow;
				}
			}
			else
			{
				if (ctyControlType == enuControlType.CheckBox)
				{
					if ((frm.GetAllControls(strControlName) as FCCheckBox).Value == FCCheckBox.ValueSettings.Checked)
					{
						strNewValue = FCConvert.ToString(true);
					}
					else
					{
						strNewValue = FCConvert.ToString(false);
					}
				}
				else if (ctyControlType == enuControlType.ComboBox || ctyControlType == enuControlType.TextBox)
				{
					strNewValue = frm.GetAllControls(strControlName).Text;
				}
				else if (ctyControlType == enuControlType.ListBox)
				{
					strNewValue = FCConvert.ToString((frm.GetAllControls(strControlName) as FCListBox).Selected(intListIndex));
				}
				else if (ctyControlType == enuControlType.OptionButton)
				{
					strNewValue = FCConvert.ToString((frm.GetAllControls(strControlName) as FCRadioButton).Value);
				}
				else if (ctyControlType == enuControlType.Label)
				{
					strNewValue = frm.GetAllControls(strControlName).Text;
				}
				else if (ctyControlType == enuControlType.DriveListBox)
				{
					strNewValue = (frm.GetAllControls(strControlName) as FCDriveListBox).Drive;
				}
				else if (ctyControlType == enuControlType.DirListBox)
				{
					strNewValue = (frm.GetAllControls(strControlName) as FCDirListBox).Path;
				}
				else if (ctyControlType == enuControlType.FileListBox)
				{
					strNewValue = (frm.GetAllControls(strControlName) as FCFileListBox).FileName;
				}
				else if (ctyControlType == enuControlType.FlexGrid)
				{

					int savedRow = intRow;

					FCGrid grid = frm.GetAllControls(strControlName) as FCGrid;

					if (UseKeyInsteadOfRowCol)
					{
						if (keyValue != 0)
						{
							for (int i = 0; i < grid.Rows; i++)
							{
								if (grid.TextMatrix(i, keyCol).ToIntegerValue() == keyValue)
								{
									intRow = i;
									break;
								}
							}
						}
					}
					
					if (grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol) != grid.TextMatrix(intRow, intCol))
					{
						strNewValue = grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, intCol);
					}
					else
					{
						if (grid.ColDataType(intCol) == FCGrid.DataTypeSettings.flexDTBoolean)
						{
							if (grid.TextMatrix(intRow, intCol) == "true")
							{
								strNewValue = FCConvert.ToString(true);
							}
							else
							{
								strNewValue = FCConvert.ToString(false);
							}
						}
						else
						{
							strNewValue = grid.TextMatrix(intRow, intCol);
						}
					}

					intRow = savedRow;
				}
			}
		}

		public void Reset()
		{
			strControlName = "";
			strControlDescriptionName = "";
			intRow = 0;
			intCol = 0;
			keyCol = 0;
			useKey = false;
			keyValue = 0;
			strOldValue = "";
			strNewValue = "";
			ctyControlType = (enuControlType)(0);
			intArrayIndex = -1;
		}

		public clsAuditControlInformation() : base()
		{
			intArrayIndex = -1;
		}
	}
}
