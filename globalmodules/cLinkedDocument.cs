﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cLinkedDocument
	{
		//=========================================================
		private int lngRecordID;
		private string strFileName = string.Empty;
		private string strDescription = string.Empty;
		private int lngDocReferenceID;
		private string strDocReferenceType = string.Empty;
		private bool boolUpdated;
		private bool boolDeleted;

		public string FileName
		{
			set
			{
				IsUpdated = true;
				strFileName = value;
			}
			get
			{
				string FileName = "";
				FileName = strFileName;
				return FileName;
			}
		}

		public string Description
		{
			set
			{
				IsUpdated = true;
				strDescription = value;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public int DocReferenceID
		{
			set
			{
				IsUpdated = true;
				lngDocReferenceID = value;
			}
			get
			{
				int DocReferenceID = 0;
				DocReferenceID = lngDocReferenceID;
				return DocReferenceID;
			}
		}

		public string DocReferenceType
		{
			set
			{
				IsUpdated = true;
				strDocReferenceType = value;
			}
			get
			{
				string DocReferenceType = "";
				DocReferenceType = strDocReferenceType;
				return DocReferenceType;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}
	}
}
