using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Microsoft.VisualBasic;
using Global;

namespace TWCL0000
{
	/// <summary>
	/// Summary description for rptCLDailyAuditMaster.
	/// </summary>
	public class rptCLDailyAuditMaster : fecherFoundation.FCForm
	{

// nObj = 1
//   0	rptCLDailyAuditMaster	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/21/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/29/2005              *
		// ********************************************************
		public bool boolWide; // True if this report is in Wide Format
		public int lngWidth; // This is the Print Width of the report
		bool boolDone;
		public string strType = "";
		public bool boolShowRE; // Show RE Records?
		public bool boolOrderByReceipt; // order by receipt
		public bool boolCloseOutAudit; // Actually close out the payments
		bool boolAppendPP; // add the PP audit on after the RE audit
		public bool boolLandscape; // is the report to be printed landscape?
		bool boolFinishedRE;
		public bool boolRecreation;
		bool boolSaveThisFile;

		private void ActiveReport_FetchData(ref bool EOF)
		{
			// this should let the detail section fire once
			if (boolDone) {
				if (boolAppendPP) {
					boolAppendPP = false;
					boolShowRE = false;
					strType = "AND BillCode = 'P' ";
					EOF = false;
				} else {
					EOF = boolDone;
				}
			} else {
				EOF = boolDone;
				boolDone = true;
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			
			if (KeyCode==vbKeyEscape)
			{
				Close();
			}
		}

		private void ActiveReport_PageStart()
		{
			VBtoConverter.ScreenCursor = 1;
			frmWait.InstancePtr.Hide();
		}

		private void ActiveReport_ReportEnd()
		{
			VBtoConverter.ScreenCursor = 1;
			frmWait.InstancePtr.Hide();
		}

		private void ActiveReport_ReportStart()
		{
			VBtoConverter.ScreenCursor = 11;
			frmWait.InstancePtr.Init("Please Wait..."+"\r\n"+"Loading", true);
		}

		private void SetupFields()
		{
			// find out which way the user wants to print

			boolWide = boolLandscape;

			if (boolWide) { // wide format
				lngWidth = 15000;
				this.Printer.Orientation = ddOLandscape;
			} else { // narrow format
				lngWidth = 11400;
				this.Printer.Orientation = ddOPortrait;
			}

			this.PrintWidth = lngWidth;
			sarCLDailyAudit.Width = lngWidth;
		}

		public void StartUp(ref bool boolIsRE, ref bool boolCloseOut, ref bool boolPassLandscape, ref bool boolBoth = false, ref bool boolPassRecreation = false)
		{
			try
			{	// On Error GoTo ERROR_HANDLER
				clsDRWrapper rs = new clsDRWrapper();

				boolCloseOutAudit = boolCloseOut;
				boolAppendPP = boolBoth;
				boolRecreation = boolPassRecreation;
				// if the boolAppend is on then the first report will ALWAYS be the RE audit
				// at the end of the first report the PP audit will be printed
				if (boolAppendPP) {
					boolShowRE = true;
				} else {
					boolShowRE = boolIsRE;
				}
				boolLandscape = boolPassLandscape;

				if (boolShowRE) {
					strType = "AND (BillCode = 'R' OR BillCode = 'L') ";
				} else {
					strType = "AND BillCode = 'P' ";
				}

				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);

				SetupFields();

				// MAL@20080311: Change to correctly select the Close Out
				// Tracker Reference: 12622
				// rs.OpenRecordset "SELECT * FROM PaymentRec WHERE DailyCloseOut = 0", strCLDatabase
				rs.OpenRecordset("SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = "+Convert.ToString(modCollections.glngCurrentCloseOut), modExtraModules.strCLDatabase);
				if (rs.EndOfFile()) {
					boolSaveThisFile = false;
				} else {
					boolSaveThisFile = true;
				}

				frmWait.InstancePtr.IncrementProgress();

				// Me.Show , MDIParent
				return;
			}
			catch
			{	// ERROR_HANDLER:
				FCMessageBox.Show("Error #"+Convert.ToString(Information.Err().Number)+" - "+Information.Err().Description+".",  MsgBoxStyle.Critical, "Error In Start Up");
			}
		}

		private void ActiveReport_Terminate()
		{
			try
			{	// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsCloseOut = new clsDRWrapper();
				string strSQL = "";

				// This will move all of the reports back 1 number until 9 and then save this as the first
				// MAL@20080826: Check for recreate
				// Tracker Reference: 14245
				if (boolCloseOutAudit && boolSaveThisFile && !modGlobalConstants.Statics.gblnRecreate) {
					modGlobalFunctions.IncrementSavedReports("LastCLDailyAudit");
					this.Pages.Save("RPT\\LastCLDailyAudit1.RDF");
				}
				return;
			}
			catch
			{	// ERROR_HANDLER:

			}
		}

		private void Detail_Format()
		{
			if (boolAppendPP) {
				boolShowRE = true;
			}

			sarCLDailyAudit.Object = new arCLDailyAudit();

			if (boolAppendPP && boolFinishedRE) {
				sarCLDailyAudit.Object.Tag = "PP";
			} else if (boolShowRE) {
				sarCLDailyAudit.Object.Tag = "RE";
			}
		}

		private void rptCLDailyAuditMaster_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCLDailyAuditMaster.Caption	= "Daily Audit Report";
			//rptCLDailyAuditMaster.Icon	= "rptCLDailtAuditMaster.dsx":0000";
			//rptCLDailyAuditMaster.Left	= 0;
			//rptCLDailyAuditMaster.Top	= 0;
			//rptCLDailyAuditMaster.Width	= 11880;
			//rptCLDailyAuditMaster.Height	= 8595;
			//rptCLDailyAuditMaster.StartUpPosition	= 3;
			//rptCLDailyAuditMaster.SectionData	= "rptCLDailtAuditMaster.dsx":058A;
			//End Unmaped Properties
		}
	}
}