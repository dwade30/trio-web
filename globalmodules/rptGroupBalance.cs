﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for rptGroupBalance.
	/// </summary>
	public partial class rptGroupBalance : BaseSectionReport
	{
		public rptGroupBalance()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Group Balance";
		}

		public static rptGroupBalance InstancePtr
		{
			get
			{
				return (rptGroupBalance)Sys.GetInstance(typeof(rptGroupBalance));
			}
		}

		protected rptGroupBalance _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
	
		int lngCurrentRow;

		public void Init()
		{
			frmReportViewer.InstancePtr.Init(this, "", 0);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = false;
			if (lngCurrentRow >= frmGroupBalance.InstancePtr.GridBalance.Rows)
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngCurrentRow = 1;
			txtMuni.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "h:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngCurrentRow < frmGroupBalance.InstancePtr.GridBalance.Rows)
			{
				txtModule.Text = frmGroupBalance.InstancePtr.GridBalance.TextMatrix(lngCurrentRow, 0);
				txtAccount.Text = frmGroupBalance.InstancePtr.GridBalance.TextMatrix(lngCurrentRow, 1);
				txtName.Text = frmGroupBalance.InstancePtr.GridBalance.TextMatrix(lngCurrentRow, 2);
				txtBalance.Text = frmGroupBalance.InstancePtr.GridBalance.TextMatrix(lngCurrentRow, 3);
				lngCurrentRow += 1;
			}
			else
			{
				txtModule.Text = "";
				txtAccount.Text = "";
				txtName.Text = "";
				txtBalance.Text = "";
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			txtGroupName.Text = frmGroupBalance.InstancePtr.lblName;
			txtAddress1.Text = frmGroupBalance.InstancePtr.lblAddress1;
			txtAddress2.Text = frmGroupBalance.InstancePtr.lblAddress2;
			txtCity.Text = frmGroupBalance.InstancePtr.lblCity;
			txtState.Text = frmGroupBalance.InstancePtr.lblState;
			txtZip.Text = frmGroupBalance.InstancePtr.lblZip.Text + " " + frmGroupBalance.InstancePtr.lblZip4.Text;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			txtGroupBalance.Text = frmGroupBalance.InstancePtr.txtTotalBalance;
		}

		
	}
}
