﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmBookPageReport.
	/// </summary>
	public partial class frmBookPageReport : BaseForm
	{
		public frmBookPageReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtAccount = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtAccount.AddControlArrayElement(txtAccount_1, 1);
			this.txtAccount.AddControlArrayElement(txtAccount_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBookPageReport InstancePtr
		{
			get
			{
				return (frmBookPageReport)Sys.GetInstance(typeof(frmBookPageReport));
			}
		}

		protected frmBookPageReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/28/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/28/2005              *
		// ********************************************************
		bool boolDirty;
		bool boolLoaded;
		int intType;

		public void Init(short intPassType = 0)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// defaults to saving the lien book and page
				intType = intPassType;
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Initializing Form");
			}
		}

		private void frmBookPageReport_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
			}
		}

		private void frmBookPageReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = 0;
			}
		}

		private void frmBookPageReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBookPageReport.Icon	= "frmBookPageReport.frx":0000";
			//frmBookPageReport.FillStyle	= 0;
			//frmBookPageReport.ScaleWidth	= 3885;
			//frmBookPageReport.ScaleHeight	= 2565;
			//frmBookPageReport.LinkTopic	= "Form2";
			//frmBookPageReport.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsElasticLight1.OleObjectBlob	= "frmBookPageReport.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			// this will set the size of the form to the smaller TRIO size (1/2 normal)
			modGlobalFunctions.SetTRIOColors(this);
			// this will set all the TRIO colors
		}

		private void frmBookPageReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			if (ShowReport())
			{
				Close();
			}
		}

		private void txtAccount_Enter(int Index, object sender, System.EventArgs e)
		{
			if (txtAccount[Index].Text != "")
			{
				txtAccount[Index].SelectionStart = 0;
				txtAccount[Index].SelectionLength = txtAccount[Index].Text.Length;
			}
		}

		private void txtAccount_Enter(object sender, System.EventArgs e)
		{
			int index = txtAccount.IndexOf((FCTextBox)sender);
			txtAccount_Enter(index, sender, e);
		}

		private bool ShowReport()
		{
			bool ShowReport = false;
			int lngEligible = 0;
			if (cmbBPType.SelectedIndex == 1)
			{
				lngEligible = 3;
			}
			else
			{
				lngEligible = 0;
			}
			if (Conversion.Val(txtAccount[0].Text) != 0)
			{
				if (Conversion.Val(txtAccount[1].Text) == 0 || Conversion.Val(txtAccount[1].Text) == Conversion.Val(txtAccount[0].Text))
				{
					arBookPageAccount.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(txtAccount[0].Text)), FCConvert.ToInt32(Conversion.Val(txtAccount[0].Text)), 0, 0, lngEligible);
				}
				else
				{
					arBookPageAccount.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(txtAccount[0].Text)), FCConvert.ToInt32(Conversion.Val(txtAccount[1].Text)), 0, 0, lngEligible);
				}
			}
			else
			{
				ShowReport = false;
				FCMessageBox.Show("Please enter a valid account number to start from.", MsgBoxStyle.Exclamation, "No Starting Account");
			}
			return ShowReport;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSaveExit_Click(sender, e);
		}
	}
}
