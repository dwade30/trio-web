﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

#if TWCL0000
using TWCL0000;


#else
using TWCR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class sarDANonCash : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/03/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/10/2003              *
		// ********************************************************
		bool boolWide;
		float lngWidth;
		string strSQL;
		clsDRWrapper rsNC = new clsDRWrapper();
		clsDRWrapper rsYears = new clsDRWrapper();
		int lngYear;
		float lngRowHeight;
		bool boolRENonCash;

		public sarDANonCash()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarDANonCash_ReportEnd;
		}

        private void SarDANonCash_ReportEnd(object sender, EventArgs e)
        {
			rsNC.DisposeOf();
            rsYears.DisposeOf();

		}

        public static sarDANonCash InstancePtr
		{
			get
			{
				return (sarDANonCash)Sys.GetInstance(typeof(sarDANonCash));
			}
		}

		protected sarDANonCash _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsYears.EndOfFile())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
			//Detail_Format();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQLAddition = "";
			lngRowHeight = 270 / 1440F;
			if (FCConvert.ToString(this.UserData) == "RE")
			{
				boolRENonCash = true;
			}
			else
			{
				boolRENonCash = false;
			}
			if (boolRENonCash)
			{
				strSQLAddition = "AND BillCode <> 'P' ";
			}
			else
			{
				strSQLAddition = "AND BillCode = 'P' ";
			}
			// If rptCLDailyAuditMaster.boolShowRE Then
			// strSQL = "SELECT Year FROM PaymentRec WHERE DailyCloseOut = False " & arCLDailyAudit.strType & "AND Code <> 'I' AND Code <> 'P' GROUP BY Year"
			// Else
			strSQL = "SELECT [Year] FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND Code <> 'P' GROUP BY [Year]";
			// End If
			rsYears.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			frmWait.InstancePtr.IncrementProgress();
			if (rsYears.EndOfFile() != true)
			{
				SetupFields();
				SetupTotals();
			}
			else
			{
				EndReport();
			}
		}

		private void SetupFields()
		{
			// set the values passed on from the other forms
			boolWide = rptCLDailyAuditMaster.InstancePtr.boolLandscape;
			lngWidth = rptCLDailyAuditMaster.InstancePtr.lngWidth;
			// set the full page objects
			this.PrintWidth = lngWidth;
			lblHeader.Width = lngWidth;
			lnHeader.X2 = lngWidth;
			if (!boolWide)
			{
				// Narrow
				lblYear.Left = 0;
				lblType.Left = 1080 / 1440F;
				lblAbatement.Left = 3420 / 1440F;
				lblDiscount.Left = 5040 / 1440F;
				lblOther.Left = 6660 / 1440F;
				lblTotal.Left = 8280 / 1440F;
			}
			else
			{
				// Wide
				lblYear.Left = 0;
				lblType.Left = 1F;
				lblAbatement.Left = 4500 / 1440F;
				lblDiscount.Left = 6930 / 1440F;
				lblOther.Left = 9360 / 1440F;
				lblTotal.Left = 11790 / 1440F;
			}
			lnTotal.X1 = lblAbatement.Left;
			lnTotal.X2 = lngWidth;
			fldYear.Left = lblYear.Left;
			fldType1.Left = lblType.Left;
			fldType2.Left = lblType.Left;
			fldType3.Left = lblType.Left;
			fldType4.Left = lblType.Left;
			lblTotals.Left = lblType.Left;
			fldAbatement1.Left = lblAbatement.Left;
			fldAbatement2.Left = lblAbatement.Left;
			fldAbatement3.Left = lblAbatement.Left;
			fldAbatement4.Left = lblAbatement.Left;
			fldTotalAbatement.Left = lblAbatement.Left;
			fldDiscount1.Left = lblDiscount.Left;
			fldDiscount2.Left = lblDiscount.Left;
			fldDiscount3.Left = lblDiscount.Left;
			fldDiscount4.Left = lblDiscount.Left;
			fldTotalDiscount.Left = lblDiscount.Left;
			fldOther1.Left = lblOther.Left;
			fldOther2.Left = lblOther.Left;
			fldOther3.Left = lblOther.Left;
			fldOther4.Left = lblOther.Left;
			fldTotalOther.Left = lblOther.Left;
			fldTotal1.Left = lblTotal.Left;
			fldTotal2.Left = lblTotal.Left;
			fldTotal3.Left = lblTotal.Left;
			fldTotal4.Left = lblTotal.Left;
			fldTotalTotal.Left = lblTotal.Left;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			int intRow;
			intRow = 0;
			// reset the totals
			fldAbatement1.Text = "0.00";
			fldAbatement2.Text = "0.00";
			fldAbatement3.Text = "0.00";
			fldAbatement4.Text = "0.00";
			fldDiscount1.Text = "0.00";
			fldDiscount2.Text = "0.00";
			fldDiscount3.Text = "0.00";
			fldDiscount4.Text = "0.00";
			fldOther1.Text = "0.00";
			fldOther2.Text = "0.00";
			fldOther3.Text = "0.00";
			fldOther4.Text = "0.00";
			fldTotal1.Text = "0.00";
			fldTotal2.Text = "0.00";
			fldTotal3.Text = "0.00";
			fldTotal4.Text = "0.00";
			frmWait.InstancePtr.IncrementProgress();
			if (rsYears.EndOfFile() != true)
			{
				// Check to see if there are any years left to check
				// Fill the Year
				// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
				rsNC.FindFirstRecord("Year", rsYears.Get_Fields("Year"));
				while (!rsNC.NoMatch)
				{
					string VBtoVar = rsNC.Get_Fields_String("Code");
					if (VBtoVar == "A")
					{
						fldAbatement1.Text = Strings.Format(rsNC.Get_Fields_Double("Prin"), "#,##0.00");
						// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
						fldAbatement2.Text = Strings.Format(rsNC.Get_Fields("Interest"), "#,##0.00");
						// TODO: Field [PLI] not found!! (maybe it is an alias?)
						fldAbatement3.Text = Strings.Format(rsNC.Get_Fields("PLI"), "#,##0.00");
						// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
						fldAbatement4.Text = Strings.Format(rsNC.Get_Fields("Costs"), "#,##0.00");
					}
					else if (VBtoVar == "S")
					{
						// fldSupplemental1.Text = Format(rsNC.Fields("Prin"), "#,##0.00")
						// fldSupplemental2.Text = Format(rsNC.Fields("Interest"), "#,##0.00")
						// fldSupplemental3.Text = Format(rsNC.Fields("PLI"), "#,##0.00")
						// fldSupplemental4.Text = Format(rsNC.Fields("Costs"), "#,##0.00")
					}
					else if (VBtoVar == "D")
					{
						fldDiscount1.Text = Strings.Format(rsNC.Get_Fields_Double("Prin"), "#,##0.00");
						// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
						fldDiscount2.Text = Strings.Format(rsNC.Get_Fields("Interest"), "#,##0.00");
						// TODO: Field [PLI] not found!! (maybe it is an alias?)
						fldDiscount3.Text = Strings.Format(rsNC.Get_Fields("PLI"), "#,##0.00");
						// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
						fldDiscount4.Text = Strings.Format(rsNC.Get_Fields("Costs"), "#,##0.00");
					}
					else
					{
						fldOther1.Text = Strings.Format(rsNC.Get_Fields_Double("Prin"), "#,##0.00");
						// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
						fldOther2.Text = Strings.Format(rsNC.Get_Fields("Interest"), "#,##0.00");
						// TODO: Field [PLI] not found!! (maybe it is an alias?)
						fldOther3.Text = Strings.Format(rsNC.Get_Fields("PLI"), "#,##0.00");
						// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
						fldOther4.Text = Strings.Format(rsNC.Get_Fields("Costs"), "#,##0.00");
					}
					// show the type and year
					// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
					fldYear.Text = modExtraModules.FormatYear(FCConvert.ToString(rsNC.Get_Fields("Year")));
					// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
					rsNC.FindNextRecord("Year", rsYears.Get_Fields("Year"));
				}
				rsYears.MoveNext();
			}
			// Set the Total Field
			fldTotal1.Text = Strings.Format(FCConvert.ToDouble(fldAbatement1.Text) + FCConvert.ToDouble(fldDiscount1.Text) + FCConvert.ToDouble(fldOther1.Text), "#,##0.00");
			fldTotal2.Text = Strings.Format(FCConvert.ToDouble(fldAbatement2.Text) + FCConvert.ToDouble(fldDiscount2.Text) + FCConvert.ToDouble(fldOther2.Text), "#,##0.00");
			fldTotal3.Text = Strings.Format(FCConvert.ToDouble(fldAbatement3.Text) + FCConvert.ToDouble(fldDiscount3.Text) + FCConvert.ToDouble(fldOther3.Text), "#,##0.00");
			fldTotal4.Text = Strings.Format(FCConvert.ToDouble(fldAbatement4.Text) + FCConvert.ToDouble(fldDiscount4.Text) + FCConvert.ToDouble(fldOther4.Text), "#,##0.00");
			if (Conversion.Val(fldTotal1.Text) == 0)
			{
				// if there are no values for this line, then hide the line
				fldAbatement1.Visible = false;
				fldAbatement1.Top = 0;
				fldDiscount1.Visible = false;
				fldDiscount1.Top = 0;
				fldOther1.Visible = false;
				fldOther1.Top = 0;
				fldTotal1.Visible = false;
				fldTotal1.Top = 0;
				fldType1.Visible = false;
				fldType1.Top = 0;
			}
			else
			{
				// else move to the next line
				fldAbatement1.Visible = true;
				fldDiscount1.Visible = true;
				fldOther1.Visible = true;
				fldTotal1.Visible = true;
				fldType1.Visible = true;
				intRow += 1;
			}
			if (Conversion.Val(fldTotal2.Text) == 0)
			{
				// if there are no values for this line, then hide the line
				fldAbatement2.Visible = false;
				fldAbatement2.Top = 0;
				fldDiscount2.Visible = false;
				fldDiscount2.Top = 0;
				fldOther2.Visible = false;
				fldOther2.Top = 0;
				fldTotal2.Visible = false;
				fldTotal2.Top = 0;
				fldType2.Visible = false;
				fldType2.Top = 0;
			}
			else
			{
				fldAbatement2.Top = intRow * lngRowHeight;
				// set the top of this row to be shown
				fldDiscount2.Top = intRow * lngRowHeight;
				fldOther2.Top = intRow * lngRowHeight;
				fldTotal2.Top = intRow * lngRowHeight;
				fldType2.Top = intRow * lngRowHeight;
				fldAbatement2.Visible = true;
				fldDiscount2.Visible = true;
				fldOther2.Visible = true;
				fldTotal2.Visible = true;
				fldType2.Visible = true;
				intRow += 1;
				// else move to the next line
			}
			if (Conversion.Val(fldTotal3.Text) == 0)
			{
				// if there are no values for this line, then hide the line
				fldAbatement3.Visible = false;
				fldAbatement3.Top = 0;
				fldDiscount3.Visible = false;
				fldDiscount3.Top = 0;
				fldOther3.Visible = false;
				fldOther3.Top = 0;
				fldTotal3.Visible = false;
				fldTotal3.Top = 0;
				fldType3.Visible = false;
				fldType3.Top = 0;
			}
			else
			{
				fldAbatement3.Top = intRow * lngRowHeight;
				fldDiscount3.Top = intRow * lngRowHeight;
				fldOther3.Top = intRow * lngRowHeight;
				fldTotal3.Top = intRow * lngRowHeight;
				fldType3.Top = intRow * lngRowHeight;
				fldAbatement3.Visible = true;
				fldDiscount3.Visible = true;
				fldOther3.Visible = true;
				fldTotal3.Visible = true;
				fldType3.Visible = true;
				intRow += 1;
			}
			if (Conversion.Val(fldTotal4.Text) == 0)
			{
				// if there are no values for this line, then hide the line
				fldAbatement4.Visible = false;
				fldAbatement4.Top = 0;
				fldDiscount4.Visible = false;
				fldDiscount4.Top = 0;
				fldOther4.Visible = false;
				fldOther4.Top = 0;
				fldTotal4.Visible = false;
				fldTotal4.Top = 0;
				fldType4.Visible = false;
				fldType4.Top = 0;
			}
			else
			{
				fldAbatement4.Top = intRow * lngRowHeight;
				fldDiscount4.Top = intRow * lngRowHeight;
				fldOther4.Top = intRow * lngRowHeight;
				fldTotal4.Top = intRow * lngRowHeight;
				fldType4.Top = intRow * lngRowHeight;
				fldAbatement4.Visible = true;
				fldDiscount4.Visible = true;
				fldOther4.Visible = true;
				fldTotal4.Visible = true;
				fldType4.Visible = true;
				intRow += 1;
			}
			Detail.Height = (intRow) * lngRowHeight;
		}

		private void SetupTotals()
		{
			string strSQLAddition = "";
			if (boolRENonCash)
			{
				strSQLAddition = "AND BillCode <> 'P' ";
			}
			else
			{
				strSQLAddition = "AND BillCode = 'P' ";
			}
			// create the totals for the report
			strSQL = "(SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND CashDrawer = 'N' AND GeneralLedger = 'N') AS qTmp";
			strSQL = "SELECT SUM(Principal + LienCost + PreLienInterest + CurrentInterest) AS Tot, Code FROM " + strSQL + " GROUP BY Code";
			rsNC.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			while (!rsNC.EndOfFile())
			{
				// fill the totals in
				string VBtoVar = rsNC.Get_Fields_String("Code");
				if (VBtoVar == "A")
				{
					// TODO: Field [Tot] not found!! (maybe it is an alias?)
					fldTotalAbatement.Text = Strings.Format(rsNC.Get_Fields("Tot"), "#,##0.00");
				}
				else if (VBtoVar == "S")
				{
					// fldTotalSupplemental.Text = Format(rsNC.Fields("Tot"), "#,##0.00")
				}
				else if (VBtoVar == "D")
				{
					// TODO: Field [Tot] not found!! (maybe it is an alias?)
					fldTotalDiscount.Text = Strings.Format(rsNC.Get_Fields("Tot"), "#,##0.00");
				}
				else
				{
					// TODO: Field [Tot] not found!! (maybe it is an alias?)
					fldTotalOther.Text = Strings.Format(FCConvert.ToDouble(fldTotalOther.Text) + Conversion.Val(rsNC.Get_Fields("Tot")), "#,##0.00");
				}
				rsNC.MoveNext();
			}
			// calculate the total line
			fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldTotalAbatement.Text) + FCConvert.ToDouble(fldTotalDiscount.Text) + FCConvert.ToDouble(fldTotalOther.Text), "#,##0.00");
			// this will set the recordset for the rest of the report
			strSQL = "(SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND CashDrawer = 'N' AND GeneralLedger = 'N') AS qTmp";
			strSQL = "SELECT SUM(Principal) AS Prin, SUM(LienCost) AS Costs, SUM(PreLienInterest) AS PLI, SUM(CurrentInterest) AS Interest, Code, [Year] FROM " + strSQL + " GROUP BY [Year],Code";
			rsNC.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			// Group all of the payments for this year
		}

		private void EndReport()
		{
			// this will turn all of the labels/fields to visible = false and set the heights to 0
			lblAbatement.Visible = false;
			lblDiscount.Visible = false;
			lblHeader.Visible = false;
			lblOther.Visible = false;
			lblTotal.Visible = false;
			lblTotals.Visible = false;
			lblType.Visible = false;
			lblYear.Visible = false;
			fldAbatement1.Visible = false;
			fldAbatement2.Visible = false;
			fldAbatement3.Visible = false;
			fldAbatement4.Visible = false;
			fldDiscount1.Visible = false;
			fldDiscount2.Visible = false;
			fldDiscount3.Visible = false;
			fldDiscount4.Visible = false;
			fldOther1.Visible = false;
			fldOther2.Visible = false;
			fldOther3.Visible = false;
			fldOther4.Visible = false;
			fldTotal1.Visible = false;
			fldTotal2.Visible = false;
			fldTotal3.Visible = false;
			fldTotal4.Visible = false;
			fldTotalAbatement.Visible = false;
			fldTotalDiscount.Visible = false;
			fldTotalOther.Visible = false;
			fldTotalTotal.Visible = false;
			fldType1.Visible = false;
			fldType2.Visible = false;
			fldType3.Visible = false;
			fldType4.Visible = false;
			fldYear.Visible = false;
			ReportFooter.Height = 0;
			ReportHeader.Height = 0;
			Detail.Height = 0;
			lnHeader.Visible = false;
			lnTotal.Visible = false;
		}

		private void sarDANonCash_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarDANonCash.Caption	= "Daily Audit NonCash";
			//sarDANonCash.Icon	= "sarDANonCash.dsx":0000";
			//sarDANonCash.Left	= 0;
			//sarDANonCash.Top	= 0;
			//sarDANonCash.Width	= 11880;
			//sarDANonCash.Height	= 8490;
			//sarDANonCash.StartUpPosition	= 3;
			//sarDANonCash.SectionData	= "sarDANonCash.dsx":058A;
			//End Unmaped Properties
		}
	}
}
