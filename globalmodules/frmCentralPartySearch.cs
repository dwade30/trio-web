﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using SharedApplication;
using SharedApplication.CentralParties;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWAR0000
using TWAR0000;


#elif TWCR0000
using TWCR0000;


#elif TWPP0000
using modGlobal = TWPP0000.modGNWork;
using TWPP0000;


#elif TWGNENTY
using TWGNENTY;
using modGlobal = TWGNENTY.modGlobalRoutines;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;


#elif TWRE0000
using modGlobal = TWRE0000.modMDIParent;


#elif TWCE0000
using modGlobal = TWCE0000.modMDIParent;


#elif TWCK0000
using modGlobal = TWCK0000.modGNBas;


#elif TWMV0000
using modGlobal = TWMV0000.modGlobalRoutines;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmCentralPartySearch.
	/// </summary>
	public partial class frmCentralPartySearch : BaseForm, IModalView<ICentralPartySearchViewModel>
	{
		public frmCentralPartySearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        public frmCentralPartySearch(ICentralPartySearchViewModel viewModel) : this()
        {
            ViewModel = viewModel;
        }
		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            txtFirstName.KeyPress += TxtFirstName_KeyPress;
            txtLastName.KeyPress += TxtLastName_KeyPress;
		}

        private void TxtLastName_KeyPress(object sender, KeyPressEventArgs e)
        {
			int KeyAscii = Strings.Asc(e.KeyChar);
            if (KeyAscii == 13)
            {
                KeyAscii = 0;
                cmdSearch_Click();
            }
            e.KeyChar = Strings.Chr(KeyAscii);
		}

        private void TxtFirstName_KeyPress(object sender, KeyPressEventArgs e)
        {
			int KeyAscii = Strings.Asc(e.KeyChar);
            if (KeyAscii == 13)
            {
                KeyAscii = 0;
                cmdSearch_Click();
            }
            e.KeyChar = Strings.Chr(KeyAscii);
		}

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmCentralPartySearch InstancePtr
		{
			get
			{
				return (frmCentralPartySearch)Sys.GetInstance(typeof(frmCentralPartySearch));
			}
		}

		protected frmCentralPartySearch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int IDCol;
		int NameCol;
		int AddressCol;
		int EmailCol;
		// VBto upgrade warning: lngReturnedID As int	OnWrite(int, string)
		int lngReturnedID;
		clsDRWrapper rsData = new clsDRWrapper();
		bool blnFromAccountScreen;
		bool boolCanEdit;
		public string strMod = "";

		private void cmdNewCustomer_Click(object sender, System.EventArgs e)
		{
			lngReturnedID = 0;
			// mnuProcessSave_Click
			CreateCustomer();
		}

		private void CreateCustomer()
		{
			if (boolCanEdit)
			{
				frmEditCentralParties.Statics.lngReturnedID = 0;
				frmEditCentralParties.InstancePtr.blnFromAccountScreen = blnFromAccountScreen;
				frmEditCentralParties.InstancePtr.strModule = strMod;
				frmEditCentralParties.InstancePtr.Show(FormShowEnum.Modal);
				int lngReturn = 0;
				lngReturn = frmEditCentralParties.Statics.lngReturnedID;
				if (lngReturn > 0)
				{
					lngReturnedID = lngReturn;
					if (blnFromAccountScreen)
					{
						Close();
					}
					else
					{
						cParty tParty;
						cPartyController tPartyCont = new cPartyController();
						string strAddress = "";
						tParty = tPartyCont.GetParty(lngReturnedID);
						if (!(tParty == null))
						{
							// .AddItem ""
							SearchGrid.Rows += 1;
							SearchGrid.TextMatrix(SearchGrid.Rows - 1, IDCol, FCConvert.ToString(tParty.ID));
							SearchGrid.TextMatrix(SearchGrid.Rows - 1, NameCol, tParty.FullName);
							strAddress = "";
							if (tParty.Addresses.Count > 0)
							{
								// VBto upgrade warning: tAddr As cPartyAddress	OnWrite(Collection)
								cPartyAddress tAddr;
								tAddr = tParty.Addresses[1];
								strAddress = tAddr.GetFormattedAddress();
							}
							SearchGrid.TextMatrix(SearchGrid.Rows - 1, AddressCol, strAddress);
							SearchGrid.TextMatrix(SearchGrid.Rows - 1, EmailCol, tParty.Email);
							SearchGrid.TopRow = SearchGrid.Rows - 1;
						}
					}
				}
			}
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			SearchBth();
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmCentralPartySearch_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		public int Init()
		{
			int Init = 0;
			lngReturnedID = 0;
			blnFromAccountScreen = true;
			this.Show(FormShowEnum.Modal);
			Init = lngReturnedID;
			return Init;
		}

		private void frmCentralPartySearch_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			IDCol = 1;
			NameCol = 2;
			AddressCol = 3;
			EmailCol = 4;
			if (Strings.LCase(modGlobalConstants.Statics.clsSecurityClass.CheckOtherPermissions(modCentralParties.CNSTCentralPartiesEdit, "CP")) == "f")
			{
				boolCanEdit = true;
			}
			else
			{
				boolCanEdit = false;
			}
			if (boolCanEdit)
			{
				cmdNewCustomer.Enabled = true;
			}
			else
			{
				cmdNewCustomer.Enabled = false;
			}
			SearchGrid.TextMatrix(0, IDCol, "ID");
			SearchGrid.TextMatrix(0, NameCol, "Name");
			SearchGrid.TextMatrix(0, AddressCol, "Address");
			SearchGrid.TextMatrix(0, EmailCol, "E-Mail");
			//FC:FINAL:CHN - issue #1235: Incorrect ordering by ID column.
			SearchGrid.ColDataType(IDCol, FCGrid.DataTypeSettings.flexDTLong);
            // Call SetGridProperties
            FillSearchMethodCombo();
            this.fraWait.CenterToParent();
		}

        private void FillSearchMethodCombo()
        {
			cmbSearchMethod.Clear();
            cmbSearchMethod.Items.Add(new GenericDescriptionPair<CentralPartySearchMethod>()
                {Description = "Contain search criteria", ID = CentralPartySearchMethod.Contains});
            cmbSearchMethod.Items.Add(new GenericDescriptionPair<CentralPartySearchMethod>()
                {Description = "Start with search criteria", ID = CentralPartySearchMethod.StartsWith});
            cmbSearchMethod.Items.Add(new GenericDescriptionPair<CentralPartySearchMethod>()
                {Description = "Equal search criteria", ID = CentralPartySearchMethod.Equals});
            cmbSearchMethod.ValueMember = "ID";
            cmbSearchMethod.DisplayMember = "Description";
            cmbSearchMethod.SelectedIndex = 0;
        }

		private void frmCentralPartySearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
				// ElseIf KeyAscii = 13 Then
				// KeyAscii = 0
				// SendKeys "{TAB}"
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void frmCentralPartySearch_Resize(object sender, System.EventArgs e)
		{
			SetGridProperties();
            this.fraWait.CenterToParent();
        }

		private void SetGridProperties()
		{
			SearchGrid.Cols = 5;
			SearchGrid.ColHidden(0, true);
			SearchGrid.ColWidth(IDCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.09));
			SearchGrid.ColWidth(NameCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.35));
			SearchGrid.ColWidth(AddressCol, FCConvert.ToInt32(SearchGrid.WidthOriginal * 0.3));
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (blnFromAccountScreen)
            {
                blnFromAccountScreen = false;
				Close();
			}
			else
			{
				if (boolCanEdit)
				{
					frmEditCentralParties.Statics.lngReturnedID = lngReturnedID;
                    frmEditCentralParties.InstancePtr.blnFromAccountScreen = blnFromAccountScreen;
					frmEditCentralParties.InstancePtr.strModule = strMod;
					#if TWGNENTY
										                    frmEditCentralParties.InstancePtr.Show(FormShowEnum.Modal);
#else
					frmEditCentralParties.InstancePtr.Show(App.MainForm);
					#endif
				}
			}
		}

		public void SearchGrid_DblClick(object sender, EventArgs e)
		{
			if (SearchGrid.MouseRow < 1)
				return;
			lngReturnedID = FCConvert.ToInt32(SearchGrid.TextMatrix(SearchGrid.MouseRow, IDCol));
			btnProcess_Click(btnProcess, EventArgs.Empty);
		}

		public void SearchBth()
		{
            if (tabControl1.SelectedIndex == 1)
            {
				SearchAdvanced();
                return;
            }
			SearchSimple();
		}

        private void SearchSimple()
        {
            int intCounter/*unused?*/;
            string strWhere;
            string strAddress = "";
            strWhere = "Where ";
            if (txtName.Text != string.Empty)
            {

                strWhere += "FullName LIKE '%" + txtName.Text.Replace("'", "''") + "%' AND ";
            }
            if (txtAddress.Text != string.Empty)
            {
                strWhere += "Address1 LIKE '%" + txtAddress.Text + "%' AND ";
            }
            if (txtEmail.Text != string.Empty)
            {
                strWhere += "Email LIKE '%" + txtEmail.Text + "%' AND ";
            }
            if (Strings.Trim(strWhere) == "Where")
            {
                strWhere = string.Empty;
            }
            else
            {
                strWhere = Strings.Left(strWhere, strWhere.Length - 4);
            }
			PerformSearch("Select * from PartyAndAddressView " + strWhere + "Order by FullName");
        }

        private void SearchAdvanced()
        {
            var selectedItem = (GenericDescriptionPair<CentralPartySearchMethod>)cmbSearchMethod.SelectedItem;
            var firstName = txtFirstName.Text.Replace("'", "''");
            var lastName = txtLastName.Text.Replace("'", "''");
            switch (selectedItem.ID)
            {
				case CentralPartySearchMethod.StartsWith:
                    SearchStartingWith(firstName, lastName);
                    break;
				case CentralPartySearchMethod.Equals:
                    SearchEquals(firstName, lastName);
                    break;
				default:
                    SearchContains(firstName, lastName);
                    break;
            }
        }

        private void SearchStartingWith(string firstName, string lastName)
        {
            var strWhere = "where";
            var whereClause = "";
            var and = "";
            if (firstName.HasText())
            {
                whereClause += strWhere + " firstname like '" + firstName + "%'";
                and = " and ";
                strWhere = "";
            }

            if (lastName.HasText())
            {
                whereClause += strWhere + and + " lastname like '" + lastName + "%'";
                and = " and ";
                strWhere = "";
            }
			PerformSearch("Select * from PartyAndAddressView " + whereClause + " order by FullName");
        }

        private void SearchEquals(string firstName, string lastName)
        {
            var strWhere = "where";
            var whereClause = "";
            var and = "";
            if (firstName.HasText())
            {
                whereClause += strWhere + " firstname = '" + firstName + "'";
                and = " and ";
                strWhere = "";
            }

            if (lastName.HasText())
            {
                whereClause += strWhere + and + " lastname = '" + lastName + "'";
                and = " and ";
                strWhere = "";
            }
            PerformSearch("Select * from PartyAndAddressView " + whereClause + " order by FullName");
		}

        private void SearchContains(string firstName, string lastName)
        {
			var strWhere = "where";
            var whereClause = "";
            var and = "";
            if (firstName.HasText())
            {
                whereClause += strWhere + " FirstName like '%" + firstName + "%'";
                and = " and ";
                strWhere = "";
            }

            if (lastName.HasText())
            {
                whereClause += strWhere + and + " lastname like '%" + lastName + "%'";
                and = " and ";
                strWhere = "";
            }
            PerformSearch("Select * from PartyAndAddressView " + whereClause + " order by FullName");
		}

        private void PerformSearch(string searchSql)
        {
            try
            {
                prgWait.Visible = false;
                fraWait.Visible = true;
				SearchGrid.Visible = false;
                App.DoEvents();
                this.Refresh();
                rsData.OpenRecordset(searchSql, "CentralParties");
                if (!rsData.EndOfFile())
                {
                    LoadGrid();
                }
                else
                {
                    SearchGrid.Rows = 1;
                    fraWait.Visible = false;
                    FCMessageBox.Show("No information was found that matched your search criteria", MsgBoxStyle.Exclamation, "No Information");
                    return;
                }
                if (SearchGrid.Rows > 1)
                {
                    SearchGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 0, SearchGrid.Rows - 1, SearchGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				fraWait.Visible = false;
                FCMessageBox.Show(ex.Message, MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Error");
            }
		}

		private void LoadGrid()
		{
			string strAddress = "";
			int intCounter;
            SearchGrid.Rows = 1;
			prgWait.Maximum = rsData.RecordCount();
			prgWait.Value = 0;
			prgWait.Visible = true;
            SearchGrid.Rows = rsData.RecordCount() + 1;
			SearchGrid.Redraw = false;
            SearchGrid.Visible = false;
			int lngOHeight;

			for (intCounter = 0; intCounter <= rsData.RecordCount() - 1; intCounter++)
			{
				// .AddItem ""
				SearchGrid.TextMatrix(intCounter + 1, IDCol, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
				SearchGrid.TextMatrix(intCounter + 1, NameCol, FCConvert.ToString(rsData.Get_Fields_String("FullName")));
				strAddress = "";
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1"))) != "")
				{
					strAddress += Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1")));
				}
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2"))) != "")
				{
					if (Strings.Trim(strAddress) != "")
					{
						strAddress += "\r\n";
					}
					strAddress += Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2")));
				}
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("City"))) != "" || Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("State"))) != "" || Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip"))) != "")
				{
					if (Strings.Trim(strAddress) != "")
					{
						strAddress += "\r\n";
					}
					strAddress += Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("City"))) + ", " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("State"))) + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip")));
				}
				SearchGrid.TextMatrix(intCounter + 1, AddressCol, strAddress);
				SearchGrid.TextMatrix(intCounter + 1, EmailCol, FCConvert.ToString(rsData.Get_Fields_String("Email")));
				rsData.MoveNext();
				prgWait.Value = prgWait.Value + 1;
				// prgWait.Refresh
				App.DoEvents();
                if (intCounter % 100 == 0)
                {
                    FCUtils.ApplicationUpdate(prgWait);
                }
            }
			SearchGrid.Redraw = true;
            SearchGrid.Visible = true;
			fraWait.Visible = false;
		}

		private void txtAddress_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				KeyAscii = 0;
				cmdSearch_Click();
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtEmail_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				KeyAscii = 0;
				cmdSearch_Click();
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtName_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				KeyAscii = 0;
				cmdSearch_Click();
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuProcessSave_Click(sender, e);
		}

        public ICentralPartySearchViewModel ViewModel { get; set; }
        public void ShowModal()
        {
            lngReturnedID = 0;
            blnFromAccountScreen = true;
            this.Show(FormShowEnum.Modal);
            if (ViewModel != null)
            {
                ViewModel.PartyId = lngReturnedID;
            }
        }

        private void tabPage1_PanelCollapsed(object sender, EventArgs e)
        {

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void ClientArea_PanelCollapsed(object sender, EventArgs e)
        {

        }
    }
}
