﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
#if TWCR0000
using TWCR0000;


#else
using TWAR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for sarARDANonCash.
	/// </summary>
	public partial class sarARDANonCash : BaseSectionReport
	{
		// nObj = 1
		//   0	sarARDANonCash	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/10/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/11/2005              *
		// ********************************************************
		bool boolWide;
		float lngWidth;
		string strSQL;
		clsDRWrapper rsNC = new clsDRWrapper();
		clsDRWrapper rsBillType = new clsDRWrapper();
		int lngBillType;
		float lngRowHeight;

		public sarARDANonCash()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarARDANonCash_ReportEnd;
		}

        private void SarARDANonCash_ReportEnd(object sender, EventArgs e)
        {
			rsNC.DisposeOf();
            rsBillType.DisposeOf();
		}

        public static sarARDANonCash InstancePtr
		{
			get
			{
				return (sarARDANonCash)Sys.GetInstance(typeof(sarARDANonCash));
			}
		}

		protected sarARDANonCash _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsBillType.EndOfFile())
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngRowHeight = 270 / 1440F;
			strSQL = "SELECT BillType FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + "AND Code <> 'I' GROUP BY BillType";
			rsBillType.OpenRecordset(strSQL, modExtraModules.strARDatabase);
			frmWait.InstancePtr.IncrementProgress();
			if (rsBillType.EndOfFile() != true)
			{
				SetupFields();
				SetupTotals();
			}
			else
			{
				EndReport();
			}
		}

		private void SetupFields()
		{
			// set the values passed on from the other forms
			boolWide = rptARDailyAuditMaster.InstancePtr.boolLandscape;
			lngWidth = rptARDailyAuditMaster.InstancePtr.lngWidth;
			// set the full page objects
			this.PrintWidth = lngWidth;
			lblHeader.Width = lngWidth;
			lnHeader.X2 = lngWidth;
			if (!boolWide)
			{
				// Narrow
				lblBillType.Left = 0;
				lblType.Left = 1080 / 1440f;
				lblAbatement.Left = 3420 / 1440f;
				lblDiscount.Left = 5040 / 1440f;
				lblOther.Left = 6660 / 1440f;
				lblTotal.Left = 8280 / 1440f;
			}
			else
			{
				// Wide
				lblBillType.Left = 0;
				lblType.Left = 1440 / 1440f;
				lblAbatement.Left = 4500 / 1440f;
				lblDiscount.Left = 6930 / 1440f;
				lblOther.Left = 9360 / 1440f;
				lblTotal.Left = 11790 / 1440f;
			}
			lnTotal.X1 = lblAbatement.Left;
			lnTotal.X2 = lngWidth;
			fldBillType.Left = lblBillType.Left;
			fldType1.Left = lblType.Left;
			fldType2.Left = lblType.Left;
			fldType3.Left = lblType.Left;
			lblTotals.Left = lblType.Left;
			fldAbatement1.Left = lblAbatement.Left;
			fldAbatement2.Left = lblAbatement.Left;
			fldAbatement3.Left = lblAbatement.Left;
			fldTotalAbatement.Left = lblAbatement.Left;
			fldDiscount1.Left = lblDiscount.Left;
			fldDiscount2.Left = lblDiscount.Left;
			fldDiscount3.Left = lblDiscount.Left;
			fldTotalDiscount.Left = lblDiscount.Left;
			fldOther1.Left = lblOther.Left;
			fldOther2.Left = lblOther.Left;
			fldOther3.Left = lblOther.Left;
			fldTotalOther.Left = lblOther.Left;
			fldTotal1.Left = lblTotal.Left;
			fldTotal2.Left = lblTotal.Left;
			fldTotal3.Left = lblTotal.Left;
			fldTotalTotal.Left = lblTotal.Left;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			int intRow;
			intRow = 0;
			// reset the totals
			fldAbatement1.Text = "0.00";
			fldAbatement2.Text = "0.00";
			fldAbatement3.Text = "0.00";
			fldDiscount1.Text = "0.00";
			fldDiscount2.Text = "0.00";
			fldDiscount3.Text = "0.00";
			fldOther1.Text = "0.00";
			fldOther2.Text = "0.00";
			fldOther3.Text = "0.00";
			fldTotal1.Text = "0.00";
			fldTotal2.Text = "0.00";
			fldTotal3.Text = "0.00";
			frmWait.InstancePtr.IncrementProgress();
			if (rsBillType.EndOfFile() != true)
			{
				// Check to see if there are any years left to check
				// Fill the BillType
				// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
				rsNC.FindFirstRecord("BillType", rsBillType.Get_Fields("BillType"));
				while (!rsNC.NoMatch)
				{
					string vbPorterVar = rsNC.Get_Fields_String("Code");
					if (vbPorterVar == "A")
					{
						fldAbatement1.Text = Strings.Format(rsNC.Get_Fields_Double("Prin"), "#,##0.00");
						// TODO: Field [I] not found!! (maybe it is an alias?)
						fldAbatement2.Text = Strings.Format(rsNC.Get_Fields("I"), "#,##0.00");
						// Format(rsNC.Fields("Int"), "#,##0.00")
						// TODO: Field [T] not found!! (maybe it is an alias?)
						fldAbatement3.Text = Strings.Format(rsNC.Get_Fields("T"), "#,##0.00");
					}
					else if (vbPorterVar == "D")
					{
						fldDiscount1.Text = Strings.Format(rsNC.Get_Fields_Double("Prin"), "#,##0.00");
						// TODO: Field [I] not found!! (maybe it is an alias?)
						fldDiscount2.Text = Strings.Format(rsNC.Get_Fields("I"), "#,##0.00");
						// Format(rsNC.Fields("Int"), "#,##0.00")
						// TODO: Field [T] not found!! (maybe it is an alias?)
						fldDiscount3.Text = Strings.Format(rsNC.Get_Fields("T"), "#,##0.00");
					}
					else
					{
						fldOther1.Text = Strings.Format(rsNC.Get_Fields_Double("Prin"), "#,##0.00");
						// TODO: Field [I] not found!! (maybe it is an alias?)
						fldOther2.Text = Strings.Format(rsNC.Get_Fields("I"), "#,##0.00");
						// Format(rsNC.Fields("Int"), "#,##0.00")
						// TODO: Field [T] not found!! (maybe it is an alias?)
						fldOther3.Text = Strings.Format(rsNC.Get_Fields("T"), "#,##0.00");
					}
					// show the type and year
					// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
					fldBillType.Text = FCConvert.ToString(rsNC.Get_Fields("BillType"));
					// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
					rsNC.FindNextRecord("BillType", rsBillType.Get_Fields("BillType"));
				}
				rsBillType.MoveNext();
			}
			// Set the Total Field
			fldTotal1.Text = Strings.Format(FCConvert.ToDouble(fldAbatement1.Text) + FCConvert.ToDouble(fldDiscount1.Text) + FCConvert.ToDouble(fldOther1.Text), "#,##0.00");
			fldTotal2.Text = Strings.Format(FCConvert.ToDouble(fldAbatement2.Text) + FCConvert.ToDouble(fldDiscount2.Text) + FCConvert.ToDouble(fldOther2.Text), "#,##0.00");
			fldTotal3.Text = Strings.Format(FCConvert.ToDouble(fldAbatement3.Text) + FCConvert.ToDouble(fldDiscount3.Text) + FCConvert.ToDouble(fldOther3.Text), "#,##0.00");
			if (Conversion.Val(fldTotal1.Text) == 0)
			{
				// if there are no values for this line, then hide the line
				fldAbatement1.Visible = false;
				fldAbatement1.Top = 0;
				fldDiscount1.Visible = false;
				fldDiscount1.Top = 0;
				fldOther1.Visible = false;
				fldOther1.Top = 0;
				fldTotal1.Visible = false;
				fldTotal1.Top = 0;
				fldType1.Visible = false;
				fldType1.Top = 0;
			}
			else
			{
				// else move to the next line
				fldAbatement1.Visible = true;
				fldDiscount1.Visible = true;
				fldOther1.Visible = true;
				fldTotal1.Visible = true;
				fldType1.Visible = true;
				intRow += 1;
			}
			if (Conversion.Val(fldTotal2.Text) == 0)
			{
				// if there are no values for this line, then hide the line
				fldAbatement2.Visible = false;
				fldAbatement2.Top = 0;
				fldDiscount2.Visible = false;
				fldDiscount2.Top = 0;
				fldOther2.Visible = false;
				fldOther2.Top = 0;
				fldTotal2.Visible = false;
				fldTotal2.Top = 0;
				fldType2.Visible = false;
				fldType2.Top = 0;
			}
			else
			{
				fldAbatement2.Top = intRow * lngRowHeight;
				// set the top of this row to be shown
				fldDiscount2.Top = intRow * lngRowHeight;
				fldOther2.Top = intRow * lngRowHeight;
				fldTotal2.Top = intRow * lngRowHeight;
				fldType2.Top = intRow * lngRowHeight;
				fldAbatement2.Visible = true;
				fldDiscount2.Visible = true;
				fldOther2.Visible = true;
				fldTotal2.Visible = true;
				fldType2.Visible = true;
				intRow += 1;
				// else move to the next line
			}
			if (Conversion.Val(fldTotal3.Text) == 0)
			{
				// if there are no values for this line, then hide the line
				fldAbatement3.Visible = false;
				fldAbatement3.Top = 0;
				fldDiscount3.Visible = false;
				fldDiscount3.Top = 0;
				fldOther3.Visible = false;
				fldOther3.Top = 0;
				fldTotal3.Visible = false;
				fldTotal3.Top = 0;
				fldType3.Visible = false;
				fldType3.Top = 0;
			}
			else
			{
				fldAbatement3.Top = intRow * lngRowHeight;
				fldDiscount3.Top = intRow * lngRowHeight;
				fldOther3.Top = intRow * lngRowHeight;
				fldTotal3.Top = intRow * lngRowHeight;
				fldType3.Top = intRow * lngRowHeight;
				fldAbatement3.Visible = true;
				fldDiscount3.Visible = true;
				fldOther3.Visible = true;
				fldTotal3.Visible = true;
				fldType3.Visible = true;
				intRow += 1;
			}
			Detail.Height = (intRow) * lngRowHeight;
		}

		private void SetupTotals()
		{
			// create the totals for the report
			strSQL = "(SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + "AND Code <> 'I' AND CashDrawer = 'N' AND GeneralLedger = 'N')";
			strSQL = "SELECT SUM(Principal + Tax + Interest) AS Tot, Code FROM (" + strSQL + ") AS Temp GROUP BY Code";
			rsNC.OpenRecordset(strSQL, modExtraModules.strARDatabase);
			while (!rsNC.EndOfFile())
			{
				// fill the totals in
				string vbPorterVar = rsNC.Get_Fields_String("Code");
				if (vbPorterVar == "A")
				{
					// TODO: Field [Tot] not found!! (maybe it is an alias?)
					fldTotalAbatement.Text = Strings.Format(rsNC.Get_Fields("Tot"), "#,##0.00");
				}
				else if (vbPorterVar == "S")
				{
					// fldTotalSupplemental.Text = Format(rsNC.Fields("Tot"), "#,##0.00")
				}
				else if (vbPorterVar == "D")
				{
					// TODO: Field [Tot] not found!! (maybe it is an alias?)
					fldTotalDiscount.Text = Strings.Format(rsNC.Get_Fields("Tot"), "#,##0.00");
				}
				else
				{
					// TODO: Field [Tot] not found!! (maybe it is an alias?)
					fldTotalOther.Text = Strings.Format(FCConvert.ToDouble(fldTotalOther.Text) + FCConvert.ToDouble(rsNC.Get_Fields("Tot")), "#,##0.00");
				}
				rsNC.MoveNext();
			}
			// calculate the total line
			fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldTotalAbatement.Text) + FCConvert.ToDouble(fldTotalDiscount.Text) + FCConvert.ToDouble(fldTotalOther.Text), "#,##0.00");
			// this will set the recordset for the rest of the report
			strSQL = "(SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + " AND Code <> 'I' AND CashDrawer = 'N' AND GeneralLedger = 'N')";
			strSQL = "SELECT SUM(Principal) AS Prin, SUM(Tax) AS T, SUM(Interest) AS I, Code, BillType FROM (" + strSQL + ") AS Temp GROUP BY BillType,Code";
			rsNC.OpenRecordset(strSQL, modExtraModules.strARDatabase);
			// Group all of the payments for this year
		}

		private void EndReport()
		{
			// this will turn all of the labels/fields to visible = false and set the heights to 0
			lblAbatement.Visible = false;
			lblDiscount.Visible = false;
			lblHeader.Visible = false;
			lblOther.Visible = false;
			lblTotal.Visible = false;
			lblTotals.Visible = false;
			lblType.Visible = false;
			lblBillType.Visible = false;
			fldAbatement1.Visible = false;
			fldAbatement2.Visible = false;
			fldAbatement3.Visible = false;
			fldDiscount1.Visible = false;
			fldDiscount2.Visible = false;
			fldDiscount3.Visible = false;
			fldOther1.Visible = false;
			fldOther2.Visible = false;
			fldOther3.Visible = false;
			fldTotal1.Visible = false;
			fldTotal2.Visible = false;
			fldTotal3.Visible = false;
			fldTotalAbatement.Visible = false;
			fldTotalDiscount.Visible = false;
			fldTotalOther.Visible = false;
			fldType1.Visible = false;
			fldType2.Visible = false;
			fldType3.Visible = false;
			fldBillType.Visible = false;
			ReportFooter.Height = 0;
			ReportHeader.Height = 0;
			Detail.Height = 0;
			lnHeader.Visible = false;
			lnTotal.Visible = false;
		}

		private void sarARDANonCash_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarARDANonCash.Caption	= "Daily Audit NonCash";
			//sarARDANonCash.Icon	= "sarARDANonCash.dsx":0000";
			//sarARDANonCash.Left	= 0;
			//sarARDANonCash.Top	= 0;
			//sarARDANonCash.Width	= 11880;
			//sarARDANonCash.Height	= 8490;
			//sarARDANonCash.StartUpPosition	= 3;
			//sarARDANonCash.SectionData	= "sarARDANonCash.dsx":058A;
			//End Unmaped Properties
		}
	}
}
