﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmInterestedParties.
	/// </summary>
	public partial class frmInterestedParties : BaseForm
	{
		public frmInterestedParties()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmInterestedParties InstancePtr
		{
			get
			{
				return (frmInterestedParties)Sys.GetInstance(typeof(frmInterestedParties));
			}
		}

		protected frmInterestedParties _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		const int CNSTGRIDCOLid = 0;
		const int CNSTGRIDCOLTYPE = 1;
		const int CNSTGRIDCOLACCOUNT = 2;
		const int CNSTGRIDCOLNAME = 3;
		const int CNSTGRIDCOLADDRESS1 = 4;
		const int CNSTGRIDCOLADDRESS2 = 5;
		const int CNSTGRIDCOLCITY = 6;
		const int CNSTGRIDCOLSTATE = 7;
		const int CNSTGRIDCOLZIP = 8;
		const int CNSTGRIDCOLZIP4 = 9;
		const int CNSTGRIDCOLCOPIES = 10;
		private string strType = "";
		private string strRecipientDatabase = "";
		private int lngAssocID;
		private int lngCurrAcct;
		private int lngAccountType;
		private bool boolChangesMade;
		private bool boolEditOkay;
		// VBto upgrade warning: lngAccount As int	OnWriteFCConvert.ToDouble(
		public void Init(int lngAccount, string strModule, int lngAssociateID = 0, bool boolAllowEdit = true, int lngIDType = 0)
		{
			boolEditOkay = boolAllowEdit;
			// Pass in the Module you are using (the database you want to use) Billing and CL determine the bill type from lngAssociateID
			if (Strings.UCase(strModule) == "PP")
			{
				strRecipientDatabase = "twpp0000.vb1";
				strType = "PP";
			}
			else if (Strings.UCase(strModule) == "RE")
			{
				strRecipientDatabase = "TWRE0000.vb1";
				strType = "RE";
				lngAssociateID = 0;
			}
			else if (Strings.UCase(strModule) == "CL")
			{
				strRecipientDatabase = "TWRE0000.vb1";
				strType = "RE";
			}
			else if (Strings.UCase(strModule) == "BL")
			{
				strRecipientDatabase = "TWCL0000.vb1";
				strType = strModule;
			}
			else if (Strings.UCase(strModule) == "UT")
			{
				strRecipientDatabase = "TWRE0000.vb1";
				strType = strModule;
			}
			lngAssocID = lngAssociateID;
			lngCurrAcct = lngAccount;
			lngAccountType = lngIDType;
			boolChangesMade = false;
			if (!boolEditOkay)
			{
				cmdAddRecipient.Enabled = false;
				cmdDelete.Enabled = false;
				btnProcess.Enabled = false;
				//mnuSaveExit.Enabled = false;
			}
			if (lngAccountType > 0)
			{
				Form_Load();
			}
			this.Show(FormShowEnum.Modal);
		}

		private void frmInterestedParties_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmInterestedParties_Load(object sender, System.EventArgs e)
		{			
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			LoadGrid();
			boolChangesMade = false;
		}

		public void Form_Load()
		{
			frmInterestedParties_Load(this, new System.EventArgs());
		}
		// VBto upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolChangesMade)
			{
				if (FCMessageBox.Show("Changes have been made do you want to save first?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Exit?") == DialogResult.Yes)
				{
					if (!SaveData())
					{
						e.Cancel = true;
					}
				}
			}
		}

		private void frmInterestedParties_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			boolChangesMade = true;
		}

		private void Grid_Click(object sender, EventArgs e)
		{
			int lngCol;
			lngCol = Grid.Col;
			if (lngCol == CNSTGRIDCOLCOPIES && boolEditOkay)
			{
				Grid.ColComboList(CNSTGRIDCOLCOPIES, "Yes|No");
				Grid.Editable = FCGrid.EditableSettings.flexEDKbd;
			}
			else
			{
				Grid.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void Grid_DblClick(object sender, EventArgs e)
		{
			int lngRow/*unused?*/;
			if (Grid.Row > 0)
			{
				EditRecipient(Grid.Row);
			}
		}
		// VBto upgrade warning: lngRow As object	OnWriteFCConvert.ToInt32(
		private void EditRecipient(object lngRow)
		{
			if (FCConvert.ToInt32(lngRow) > 0)
			{
				clsAddress adrAddress = new clsAddress();
				adrAddress.Editable = boolEditOkay;
				adrAddress.Set_Address(1, Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLADDRESS1));
				adrAddress.Set_Address(2, Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLADDRESS2));
				adrAddress.City = Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLCITY);
				adrAddress.State = Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLSTATE);
				adrAddress.Zip = Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLZIP);
				adrAddress.ZipExtension = Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLZIP4);
				adrAddress.Addressee = Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLNAME);
				if (adrAddress.GetAddress())
				{
					Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLADDRESS1, adrAddress.Get_Address(1));
					Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLADDRESS2, adrAddress.Get_Address(2));
					Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLNAME, adrAddress.Addressee);
					Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLCITY, adrAddress.City);
					Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLSTATE, adrAddress.State);
					Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLZIP, adrAddress.Zip);
					Grid.TextMatrix(FCConvert.ToInt32(lngRow), CNSTGRIDCOLZIP4, adrAddress.ZipExtension);
					Grid.RowData(FCConvert.ToInt32(lngRow), true);
					boolChangesMade = true;
				}
			}
		}

		private void grid_KeyDown(object sender, KeyEventArgs e)
		{
			if (Grid.Row < 1)
				return;
			if (e.KeyCode == Keys.Delete)
			{
				DeleteRow(Grid.Row);
			}
			else if (e.KeyCode == Keys.Insert)
			{
				InsertRow(Grid.Row);
			}
			else if (e.KeyCode == Keys.Return)
			{
				if (Grid.Row > 0)
				{
					EditRecipient(Grid.Row);
				}
			}
		}

		private void mnuAddRecipient_Click(object sender, System.EventArgs e)
		{
			InsertRow(Grid.Row);
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
				return;
			DeleteRow(Grid.Row);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGrid()
		{
			Grid.ColHidden(CNSTGRIDCOLid, true);
			Grid.ColHidden(CNSTGRIDCOLTYPE, true);
			Grid.ColHidden(CNSTGRIDCOLADDRESS1, true);
			Grid.ColHidden(CNSTGRIDCOLADDRESS2, true);
			Grid.ColHidden(CNSTGRIDCOLCITY, true);
			Grid.ColHidden(CNSTGRIDCOLSTATE, true);
			Grid.ColHidden(CNSTGRIDCOLZIP, true);
			Grid.ColHidden(CNSTGRIDCOLZIP4, true);
			Grid.ColHidden(CNSTGRIDCOLACCOUNT, true);
			// .TextMatrix(0, CNSTGRIDCOLACCOUNT) = "Account"
			Grid.TextMatrix(0, CNSTGRIDCOLNAME, "Name");
			// If strType = "CL" Or strType = "UT" Then
			if (lngAccountType > 0)
			{
				Grid.TextMatrix(0, CNSTGRIDCOLCOPIES, "Receive Copies");
				Grid.ColHidden(CNSTGRIDCOLCOPIES, false);
			}
			else
			{
				Grid.ColHidden(CNSTGRIDCOLCOPIES, true);
			}
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			// .ColWidth(CNSTGRIDCOLACCOUNT) = 0.2 * GridWidth
			// If strType = "CL" Or strType = "UT" Then
			if (lngAccountType > 0)
			{
				Grid.ColWidth(CNSTGRIDCOLNAME, FCConvert.ToInt32(0.75 * GridWidth));
				Grid.ColWidth(CNSTGRIDCOLCOPIES, FCConvert.ToInt32(0.23 * GridWidth));
			}
			//FC:FINAL:DDU:#i1249 - set grid in order to see all text name
			else
			{
				Grid.ColWidth(CNSTGRIDCOLNAME, FCConvert.ToInt32(0.98 * GridWidth));
			}
		}

		private void LoadGrid()
		{
			int lngRow;
			clsDRWrapper rsLoad = new clsDRWrapper();
			string strTemp = "";
			string strOther;

			strOther = "";
			// End If
			switch (lngAccountType)
			{
			// MAL@20071231: Tracker Reference 104274
				case 0:
					{
						lblAccount.Text = FCConvert.ToString(lngCurrAcct);
						Grid.Rows = 1;
						if (strType != "UT")
						{
							rsLoad.OpenRecordset("select * from owners where (modulecode = '" + strType + "'" + strOther + ") and account = " + FCConvert.ToString(lngCurrAcct) + " AND associd = 0", strRecipientDatabase);
						}
						else
						{
							rsLoad.OpenRecordset("select * from owners where (modulecode = '" + strType + "'" + strOther + ") and account = " + FCConvert.ToString(lngCurrAcct) + " AND associd = " + FCConvert.ToString(lngAssocID), strRecipientDatabase);
						}
						while (!rsLoad.EndOfFile())
						{
							Grid.Rows += 1;
							lngRow = Grid.Rows - 1;
							Grid.RowData(lngRow, false);
							Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(rsLoad.Get_Fields_Int32("id")));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(lngCurrAcct));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(rsLoad.Get_Fields_String("name")));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS1, FCConvert.ToString(rsLoad.Get_Fields_String("address1")));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS2, FCConvert.ToString(rsLoad.Get_Fields_String("address2")));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATE, FCConvert.ToString(rsLoad.Get_Fields_String("state")));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLCITY, FCConvert.ToString(rsLoad.Get_Fields_String("city")));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLZIP, FCConvert.ToString(rsLoad.Get_Fields_String("zip")));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLZIP4, FCConvert.ToString(rsLoad.Get_Fields_String("zip4")));
							Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields_String("modulecode"))));
							rsLoad.MoveNext();
						}
						break;
					}
				case 1:
					{
						// Interested Party ID
						Grid.Rows = 1;
						rsLoad.OpenRecordset("SELECT * FROM Owners WHERE id = " + FCConvert.ToString(lngCurrAcct), strRecipientDatabase);
						if (rsLoad.RecordCount() > 0)
						{
							rsLoad.MoveFirst();
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							lngCurrAcct = FCConvert.ToInt32(rsLoad.Get_Fields("Account"));
							lblAccount.Text = FCConvert.ToString(lngCurrAcct);
							while (!rsLoad.EndOfFile())
							{
								Grid.Rows += 1;
								lngRow = Grid.Rows - 1;
								Grid.RowData(lngRow, false);
								Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(rsLoad.Get_Fields_Int32("id")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(lngCurrAcct));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(rsLoad.Get_Fields_String("Name")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS1, FCConvert.ToString(rsLoad.Get_Fields_String("Address1")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS2, FCConvert.ToString(rsLoad.Get_Fields_String("Address2")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATE, FCConvert.ToString(rsLoad.Get_Fields_String("State")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLCITY, FCConvert.ToString(rsLoad.Get_Fields_String("City")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLZIP, FCConvert.ToString(rsLoad.Get_Fields_String("Zip")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLZIP4, FCConvert.ToString(rsLoad.Get_Fields_String("Zip4")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields_String("Modulecode"))));
								if (FCConvert.CBool(rsLoad.Get_Fields_Boolean("ReceiveCopies")))
								{
									Grid.TextMatrix(lngRow, CNSTGRIDCOLCOPIES, "Yes");
								}
								else
								{
									Grid.TextMatrix(lngRow, CNSTGRIDCOLCOPIES, "No");
								}
								rsLoad.MoveNext();
							}
							// Else
							// MsgBox "Interested Party record not found!", MsgBoxStyle.Exclamation | MsgBoxStyle.OkOnly, "Not Found"
						}
						break;
					}
				case 2:
					{
						// Real Estate Account ID
						lblAccount.Text = FCConvert.ToString(lngCurrAcct);
						Grid.Rows = 1;
						rsLoad.OpenRecordset("SELECT * FROM Owners WHERE Account = " + FCConvert.ToString(lngCurrAcct) + " and associd = " + FCConvert.ToString(lngAssocID) + " AND (ModuleCode = '" + strType + "'" + strOther + ")", strRecipientDatabase);
						if (rsLoad.RecordCount() > 0)
						{
							rsLoad.MoveFirst();
							while (!rsLoad.EndOfFile())
							{
								Grid.Rows += 1;
								lngRow = Grid.Rows - 1;
								Grid.RowData(lngRow, false);
								Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(rsLoad.Get_Fields_Int32("id")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(lngCurrAcct));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(rsLoad.Get_Fields_String("Name")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS1, FCConvert.ToString(rsLoad.Get_Fields_String("Address1")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS2, FCConvert.ToString(rsLoad.Get_Fields_String("Address2")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATE, FCConvert.ToString(rsLoad.Get_Fields_String("State")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLCITY, FCConvert.ToString(rsLoad.Get_Fields_String("City")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLZIP, FCConvert.ToString(rsLoad.Get_Fields_String("Zip")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLZIP4, FCConvert.ToString(rsLoad.Get_Fields_String("Zip4")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields_String("Modulecode"))));
								if (FCConvert.CBool(rsLoad.Get_Fields_Boolean("ReceiveCopies")))
								{
									Grid.TextMatrix(lngRow, CNSTGRIDCOLCOPIES, "Yes");
								}
								else
								{
									Grid.TextMatrix(lngRow, CNSTGRIDCOLCOPIES, "No");
								}
								rsLoad.MoveNext();
							}
							// Else
							// MsgBox "No records found!", MsgBoxStyle.Exclamation | MsgBoxStyle.OkOnly, "Not Found"
						}
						break;
					}
				case 3:
					{
						// Utility Account ID
						lblAccount.Text = FCConvert.ToString(lngCurrAcct);
						Grid.Rows = 1;
						rsLoad.OpenRecordset("SELECT * FROM Owners WHERE Account = " + FCConvert.ToString(lngCurrAcct) + " AND ModuleCode = '" + strType + "'" + strOther, strRecipientDatabase);
						if (rsLoad.RecordCount() > 0)
						{
							rsLoad.MoveFirst();
							while (!rsLoad.EndOfFile())
							{
								Grid.Rows += 1;
								lngRow = Grid.Rows - 1;
								Grid.RowData(lngRow, false);
								Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(rsLoad.Get_Fields_Int32("id")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(lngCurrAcct));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME, FCConvert.ToString(rsLoad.Get_Fields_String("Name")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS1, FCConvert.ToString(rsLoad.Get_Fields_String("Address1")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS2, FCConvert.ToString(rsLoad.Get_Fields_String("Address2")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATE, FCConvert.ToString(rsLoad.Get_Fields_String("State")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLCITY, FCConvert.ToString(rsLoad.Get_Fields_String("City")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLZIP, FCConvert.ToString(rsLoad.Get_Fields_String("Zip")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLZIP4, FCConvert.ToString(rsLoad.Get_Fields_String("Zip4")));
								Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE, Strings.UCase(FCConvert.ToString(rsLoad.Get_Fields_String("ModuleCode"))));
								if (FCConvert.CBool(rsLoad.Get_Fields_Boolean("ReceiveCopies")))
								{
									Grid.TextMatrix(lngRow, CNSTGRIDCOLCOPIES, "Yes");
								}
								else
								{
									Grid.TextMatrix(lngRow, CNSTGRIDCOLCOPIES, "No");
								}
								rsLoad.MoveNext();
							}
							// Else
							// MsgBox "No records found!", MsgBoxStyle.Exclamation | MsgBoxStyle.OkOnly, "Not Found"
						}
						break;
					}
			}
			//end switch
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveData();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				//FC:FINAL:BSE #3257 used correct method call
				mnuExit_Click(sender, e);
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				int lngRow;
				clsDRWrapper rsSave = new clsDRWrapper();
				// MAL@20080114: move focus to "set" all changes to the grid data
				// Grid.Select 0, CNSTGRIDCOLNAME
				Grid.Col = -1;
				SaveData = false;
				for (lngRow = 0; lngRow <= GridDelete.Rows - 1; lngRow++)
				{
					rsSave.Execute("delete from owners where ID = " + FCConvert.ToString(Conversion.Val(GridDelete.TextMatrix(lngRow, 0))), strRecipientDatabase);
				}
				// lngRow
				GridDelete.Rows = 0;
				for (lngRow = 1; lngRow <= Grid.Rows - 1; lngRow++)
				{
					if (FCConvert.ToBoolean(Grid.RowData(lngRow)))
					{
						rsSave.OpenRecordset("select * from owners where ID = " + FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLid))), strRecipientDatabase);
						if (!rsSave.EndOfFile())
						{
							rsSave.Edit();
						}
						else
						{
							rsSave.AddNew();
							// Grid.TextMatrix(lngRow, CNSTGRIDCOLid) = rsSave.Get_Fields("id")
						}
						rsSave.Set_Fields("Name", Grid.TextMatrix(lngRow, CNSTGRIDCOLNAME));
						rsSave.Set_Fields("Address1", Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS1));
						rsSave.Set_Fields("Address2", Grid.TextMatrix(lngRow, CNSTGRIDCOLADDRESS2));
						rsSave.Set_Fields("City", Grid.TextMatrix(lngRow, CNSTGRIDCOLCITY));
						rsSave.Set_Fields("State", Grid.TextMatrix(lngRow, CNSTGRIDCOLSTATE));
						rsSave.Set_Fields("Zip", Grid.TextMatrix(lngRow, CNSTGRIDCOLZIP));
						rsSave.Set_Fields("Zip4", Grid.TextMatrix(lngRow, CNSTGRIDCOLZIP4));
						rsSave.Set_Fields("Account", FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLACCOUNT))));
						rsSave.Set_Fields("ModuleCode", Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE));
						if (Strings.LCase(Grid.TextMatrix(lngRow, CNSTGRIDCOLTYPE)) != "re")
						{
							rsSave.Set_Fields("Associd", lngAssocID);
						}
						else
						{
							rsSave.Set_Fields("Associd", 0);
						}
						if (lngAccountType > 0)
						{
							rsSave.Set_Fields("ReceiveCopies", FCConvert.CBool(Grid.TextMatrix(lngRow, CNSTGRIDCOLCOPIES) == "Yes"));
						}
						rsSave.Update();
						Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(rsSave.Get_Fields_Int32("ID")));
						Grid.RowData(lngRow, false);
					}
				}
				// lngRow
				SaveData = true;
				boolChangesMade = false;
				FCMessageBox.Show("Save Successful", MsgBoxStyle.Information, "Saved");
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In SaveData", MsgBoxStyle.Critical, "Error");
			}
			return SaveData;
		}

		private void DeleteRow(int lngRow)
		{
			int lngCRow;
			int lngTempRow/*unused?*/;
			int lngAcct/*unused?*/;
			if (lngRow < 1 || !boolEditOkay)
			{
				return;
			}
			// just this line
			if (Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLid)) > 0)
			{
				GridDelete.Rows += 1;
				lngCRow = GridDelete.Rows - 1;
				GridDelete.TextMatrix(lngCRow, 0, FCConvert.ToString(Conversion.Val(Grid.TextMatrix(lngRow, CNSTGRIDCOLid))));
			}
			Grid.RemoveItem(lngRow);
			boolChangesMade = true;
		}

		private void InsertRow(int lngRow)
		{
			int lngAcct/*unused?*/;
			int lngCRow;
			clsAddress adrAddress = new clsAddress();
			if (!boolEditOkay)
				return;
			adrAddress.Editable = boolEditOkay;
			if (adrAddress.GetAddress())
			{
				Grid.AddItem("\t" + 0, lngRow + 1);
				lngCRow = lngRow + 1;
				Grid.TextMatrix(lngCRow, CNSTGRIDCOLACCOUNT, FCConvert.ToString(lngCurrAcct));
				Grid.TextMatrix(lngCRow, CNSTGRIDCOLTYPE, strType);
				Grid.TextMatrix(lngCRow, CNSTGRIDCOLADDRESS1, adrAddress.Get_Address(1));
				Grid.TextMatrix(lngCRow, CNSTGRIDCOLADDRESS2, adrAddress.Get_Address(2));
				Grid.TextMatrix(lngCRow, CNSTGRIDCOLCITY, adrAddress.City);
				Grid.TextMatrix(lngCRow, CNSTGRIDCOLSTATE, adrAddress.State);
				Grid.TextMatrix(lngCRow, CNSTGRIDCOLZIP, adrAddress.Zip);
				Grid.TextMatrix(lngCRow, CNSTGRIDCOLZIP4, adrAddress.ZipExtension);
				Grid.TextMatrix(lngCRow, CNSTGRIDCOLNAME, adrAddress.Addressee);
				Grid.TextMatrix(lngCRow, CNSTGRIDCOLCOPIES, "Yes");
				Grid.RowData(lngCRow, true);
				boolChangesMade = true;
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
            //FC:FINAL:BSE #3257 after save form should close
            this.mnuSaveExit_Click(sender, e);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			this.mnuSave_Click(sender, e);
		}

		private void cmdAddRecipient_Click(object sender, EventArgs e)
		{
			this.mnuAddRecipient_Click(sender, e);
		}

		private void cmdDelete_Click(object sender, EventArgs e)
		{
			this.mnuDelete_Click(sender, e);
		}
	}
}
