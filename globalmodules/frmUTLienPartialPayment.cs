﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmUTLienPartialPayment.
	/// </summary>
	public partial class frmUTLienPartialPayment : BaseForm
	{
		public frmUTLienPartialPayment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUTLienPartialPayment InstancePtr
		{
			get
			{
				return (frmUTLienPartialPayment)Sys.GetInstance(typeof(frmUTLienPartialPayment));
			}
		}

		protected frmUTLienPartialPayment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               06/11/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/21/2004              *
		// ********************************************************
		clsDRWrapper rsLien = new clsDRWrapper();
		clsDRWrapper rsSettings = new clsDRWrapper();
		bool boolLoaded;
		int lngLienRecordNumber;
		string strOwner = "";
		double dblPaymentAmount;
		string strSignerName = "";
		string strMapLot = "";
		int lngAccountNumber;
		DateTime dtPaymentDate;
		string strService;
		// grid cols
		int lngColData;
		int lngColTitle;
		int lngColHidden;
		// grid rows
		int lngRowMuni;
		int lngRowState;
		int lngRowOwner1;
		int lngRowPayDate;
		int lngRowPaymentAmount;
		int lngRowSignerName;
		int lngRowMapLot;
		int lngRowSignedDate;
		// vbPorter upgrade warning: dblPassPaymentAmount As double	OnWriteFCConvert.ToDecimal(
		public void Init(DateTime dtPassPayDate, int lngPassLRN, string strYear, int lngPassAccountNumber, double dblPassPaymentAmount, string strPassService, int lngBillNum)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsInfo = new clsDRWrapper();
				bool boolRecords;
				dtPaymentDate = dtPassPayDate;
				lngLienRecordNumber = lngPassLRN;
				strService = strPassService;
				lngColData = 2;
				lngColHidden = 1;
				lngColTitle = 0;
				lngRowMuni = 0;
				lngRowState = 1;
				lngRowOwner1 = 2;
				lngRowPayDate = 3;
				lngRowPaymentAmount = 4;
				lngRowSignerName = 5;
				lngRowMapLot = 7;
				lngRowSignedDate = 6;
				rsInfo.OpenRecordset("SELECT * FROM Bill WHERE " + strService + "LienRecordNumber = " + FCConvert.ToString(lngLienRecordNumber), modExtraModules.strUTDatabase);
				if (!rsInfo.EndOfFile())
				{
					strOwner = rsInfo.Get_Fields_String("OName") + " " + rsInfo.Get_Fields_String("OName2");
					strSignerName = "";
					// rsInfo.Fields("Name1")
					strMapLot = FCConvert.ToString(rsInfo.Get_Fields_String("MapLot"));
				}
				dblPaymentAmount = dblPassPaymentAmount;
				lngAccountNumber = lngPassAccountNumber;
				if (strYear.Length > 4)
				{
					strYear = Strings.Left(strYear, 4);
				}
				// If strYear <> "" Then
				// lblYear.Text = "Year : " & strYear
				// lblYear.Visible = True
				// Else
				// lblYear.Visible = False
				// End If
				if (lngBillNum != 0)
				{
					lblYear.Text = "Bill : " + FCConvert.ToString(lngBillNum);
					lblYear.Visible = true;
				}
				else
				{
					lblYear.Visible = false;
				}
				rsInfo.OpenRecordset("SELECT * FROM Lien INNER JOIN Bill ON Lien.ID = Bill." + strService + "LienRecordNumber WHERE Lien.ID = " + FCConvert.ToString(lngLienRecordNumber), modExtraModules.strUTDatabase);
				if (!rsInfo.EndOfFile())
				{
					// keep going
				}
				else
				{
					MessageBox.Show("The lien record number " + FCConvert.ToString(lngLienRecordNumber) + " could not be found.", "Missing Lien Record", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Close();
				}
				Format_Grid(true);
				LoadSettings();
				if (lngLienRecordNumber > 0)
				{
					this.Show(FormShowEnum.Modal, App.MainForm);
				}
				else
				{
					MessageBox.Show("Cannot find a lien record for that bill.", "Cannot Find Lien", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Close();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmUTLienPartialPayment_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
			}
		}

		private void frmUTLienPartialPayment_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUTLienPartialPayment.FillStyle	= 0;
			//frmUTLienPartialPayment.ScaleWidth	= 5880;
			//frmUTLienPartialPayment.ScaleHeight	= 4575;
			//frmUTLienPartialPayment.LinkTopic	= "Form2";
			//frmUTLienPartialPayment.LockControls	= true;
			//frmUTLienPartialPayment.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			boolLoaded = true;
			Format_Grid(true);
		}

		private void frmUTLienPartialPayment_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmUTLienPartialPayment_Resize(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				FormatGrid();
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void SaveSettings()
		{
			// this will save all of the settings for the lien discharge notice
			clsDRWrapper rsTemp = new clsDRWrapper();
			DateTime dtComExp;
			bool boolTemp;
			// vbPorter upgrade warning: obRep As object --> As arUTLienPartialPayment
			arUTLienPartialPayment obRep = null;
			if (ValidateAnswers())
			{
				obRep = new arUTLienPartialPayment();
				obRep.Init(vsData.TextMatrix(lngRowMuni, lngColData), vsData.TextMatrix(lngRowState, lngColData), FCConvert.ToDouble(vsData.TextMatrix(lngRowPaymentAmount, lngColData)), vsData.TextMatrix(lngRowOwner1, lngColData), Strings.Trim(Strings.Right(lblYear.Text, 6)), DateAndTime.DateValue(vsData.TextMatrix(lngRowPayDate, lngColData)), vsData.TextMatrix(lngRowSignerName, lngColData), strService, vsData.TextMatrix(lngRowMapLot, lngColData), lngAccountNumber);
				frmReportViewer.InstancePtr.Init(obRep, string.Empty, 1, this.Modal);
				frmUTLienPartialPayment.InstancePtr.Close();
			}
		}

		private void LoadSettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsCTRL = new clsDRWrapper();
				// Muni Name
				vsData.TextMatrix(lngRowMuni, lngColData, modGlobalConstants.Statics.MuniName);
				// State
				vsData.TextMatrix(lngRowState, lngColData, "Maine");
				// Name1
				vsData.TextMatrix(lngRowOwner1, lngColData, strOwner);
				// PayDate
				vsData.TextMatrix(lngRowPayDate, lngColData, Strings.Format(dtPaymentDate, "MM/dd/yyyy"));
				// Payment Amount
				vsData.TextMatrix(lngRowPaymentAmount, lngColData, Strings.Format(dblPaymentAmount, "#,##0.00"));
				// Signer Name
				vsData.TextMatrix(lngRowSignerName, lngColData, strSignerName);
				// Signer Name
				vsData.TextMatrix(lngRowSignedDate, lngColData, Strings.Format(DateTime.Today, "MM/dd/yyyy"));
				// Map Lot
				if (strMapLot != "")
				{
					vsData.TextMatrix(lngRowMapLot, lngColData, strMapLot);
				}
				else
				{
					// if the bill record does not have the map lot, then get it from the RE Master file
					rsCTRL.OpenRecordset("SELECT * FROM Master WHERE RSCard = 1 AND RSAccount = " + FCConvert.ToString(lngAccountNumber), modExtraModules.strREDatabase);
					if (!rsCTRL.EndOfFile())
					{
						vsData.TextMatrix(lngRowMapLot, lngColData, FCConvert.ToString(rsCTRL.Get_Fields_String("RSMapLot")));
					}
					else
					{
						vsData.TextMatrix(lngRowMapLot, lngColData, "");
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
			// this will keep the data and print the lien discharge notice
		}

		private void Format_Grid(bool boolReset)
		{
			FormatGrid(boolReset);
		}

		private void FormatGrid(bool boolReset = false)
		{
			int lngWid = 0;
			lngWid = vsData.WidthOriginal;
			if (boolReset)
			{
				vsData.Rows = 0;
				vsData.Rows = 8;
			}
			vsData.ColWidth(lngColTitle, FCConvert.ToInt32(lngWid * 0.38));
			// title
			vsData.ColWidth(lngColHidden, 0);
			// hidden row
			vsData.ColWidth(lngColData, FCConvert.ToInt32(lngWid * 0.62));
			// data
			vsData.TextMatrix(lngRowMuni, lngColTitle, "Municipality");
			vsData.TextMatrix(lngRowState, lngColTitle, "State");
			vsData.TextMatrix(lngRowOwner1, lngColTitle, "Owner's Name");
			vsData.TextMatrix(lngRowPayDate, lngColTitle, "Payment Date");
			vsData.TextMatrix(lngRowPaymentAmount, lngColTitle, "Payment Amount");
			vsData.TextMatrix(lngRowSignerName, lngColTitle, "Witness' Name");
			vsData.TextMatrix(lngRowSignedDate, lngColTitle, "Signed Date");
			vsData.TextMatrix(lngRowMapLot, lngColTitle, "Map Lot");
			// set the grid height
			if ((vsData.Rows * vsData.RowHeight(0)) + 80 > (fraVariables.Height * 0.9))
			{
				vsData.Height = FCConvert.ToInt32((fraVariables.Height * 0.9));
				vsData.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			}
			else
			{
				vsData.Height = (vsData.Rows * vsData.RowHeight(0)) + 80;
				vsData.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			}
		}

		private void vsData_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsData.EditMask = string.Empty;
			vsData.ComboList = "";
			if (vsData.Row == lngRowPayDate || vsData.Row == lngRowSignedDate)
			{
				vsData.EditMask = "0#/0#/####";
			}
			else if (vsData.Row == lngRowPaymentAmount)
			{
				vsData.EditMask = "#,##0.00";
			}
		}

		private void vsData_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			FCGrid grid = sender as FCGrid;
			int keyAscii = Strings.Asc(e.KeyChar);
			if (grid.Row == lngRowPayDate || grid.Row == lngRowSignedDate || grid.Row == lngRowPaymentAmount)
			{
				// only allow numbers
				if ((keyAscii >= FCConvert.ToInt32(Keys.D0)) && keyAscii <= FCConvert.ToInt32(Keys.D9) || (keyAscii == FCConvert.ToInt32(Keys.Back)) || (keyAscii == FCConvert.ToInt32(Keys.Decimal)) || (keyAscii == 46))
				{
					// do nothing
				}
				else
				{
					keyAscii = 0;
				}
			}
		}

		private void vsData_RowColChange(object sender, System.EventArgs e)
		{
			if (vsData.Col == lngColData)
			{
				if (vsData.Row == lngRowMuni || vsData.Row == lngRowState || vsData.Row == lngRowOwner1 || vsData.Row == lngRowPayDate || vsData.Row == lngRowPaymentAmount || vsData.Row == lngRowSignerName || vsData.Row == lngRowMapLot || vsData.Row == lngRowMuni || vsData.Row == lngRowSignedDate)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
			}
			else
			{
				vsData.Editable = FCGrid.EditableSettings.flexEDNone;
				vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private bool ValidateAnswers()
		{
			bool ValidateAnswers = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				ValidateAnswers = true;
				vsData.Select(0, 1);
				// Muni Name
				if (Strings.Trim(vsData.TextMatrix(lngRowMuni, lngColData)) == "")
				{
					ValidateAnswers = false;
					MessageBox.Show("Please enter the Municipality name.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsData.Select(lngRowMuni, lngColData);
					return ValidateAnswers;
				}
				// State
				if (Strings.Trim(vsData.TextMatrix(lngRowState, lngColData)) == "")
				{
					ValidateAnswers = false;
					MessageBox.Show("Please enter the State name.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsData.Select(lngRowState, lngColData);
					return ValidateAnswers;
				}
				// Owner
				if (Strings.Trim(vsData.TextMatrix(lngRowOwner1, lngColData)) == "")
				{
					ValidateAnswers = false;
					MessageBox.Show("Please enter the Owner's name.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsData.Select(lngRowOwner1, lngColData);
					return ValidateAnswers;
				}
				// Signer Name
				if (Strings.Trim(vsData.TextMatrix(lngRowSignerName, lngColData)) == "")
				{
					ValidateAnswers = false;
					MessageBox.Show("Please enter the Witness' name.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsData.Select(lngRowSignerName, lngColData);
					return ValidateAnswers;
				}
				// Payment Date
				if (Strings.Trim(vsData.TextMatrix(lngRowPayDate, lngColData)) == "")
				{
					ValidateAnswers = false;
					MessageBox.Show("Please enter a payment date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsData.Select(lngRowPayDate, lngColData);
					return ValidateAnswers;
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowPayDate, lngColData)))
					{
						ValidateAnswers = false;
						MessageBox.Show("Please enter a valid payment date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vsData.Select(lngRowPayDate, lngColData);
						return ValidateAnswers;
					}
				}
				// Signing Date
				if (Strings.Trim(vsData.TextMatrix(lngRowSignedDate, lngColData)) == "")
				{
					ValidateAnswers = false;
					MessageBox.Show("Please enter a signing date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsData.Select(lngRowSignedDate, lngColData);
					return ValidateAnswers;
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowSignedDate, lngColData)))
					{
						ValidateAnswers = false;
						MessageBox.Show("Please enter a valid signing date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vsData.Select(lngRowSignedDate, lngColData);
						return ValidateAnswers;
					}
				}
				// Payment Amount
				if (Strings.Trim(vsData.TextMatrix(lngRowPaymentAmount, lngColData)) == "")
				{
					ValidateAnswers = false;
					MessageBox.Show("Please enter a payment amount.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsData.Select(lngRowPaymentAmount, lngColData);
					return ValidateAnswers;
				}
				return ValidateAnswers;
			}
			catch (Exception ex)
			{
				
				ValidateAnswers = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ValidateAnswers;
		}
	}
}
