﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using TWSharedLibrary.Data;
#if TWCR0000
using TWCR0000;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;
#endif
namespace Global
{
    /// <summary>
    /// Summary description for frmGetMasterAccount.
    /// </summary>
    public partial class frmGetMasterAccount : BaseForm
    {
        public frmGetMasterAccount()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmGetMasterAccount InstancePtr
        {
            get
            {
                return (frmGetMasterAccount)Sys.GetInstance(typeof(frmGetMasterAccount));
            }
        }

        protected frmGetMasterAccount _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               03/19/2003              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               11/06/2006              *
        // ********************************************************
        bool MeterTable;
        // True if a meter field is being searched for, False if Master table field is being searched for
        string strSQL = "";
        clsDRWrapper rs = new clsDRWrapper();
        clsDRWrapper rsPrev = new clsDRWrapper();
        int intAction;
        int lngColHidden;
        int lngColAccountNumber;
        int lngColName;
        int lngColLocation;
        int lngColLocationNumber;
        int lngColTenant;
        int lngColSortName;
        int lngColSortName2;
        int lngColMapLot;
        int lngColREAcct;
        int lngColSerialNumber;
        int lngColRemoteNumber;
        int lngColRef1;
        int intType;
        // 0 - Master/Meter Screen, 1 - Collection Status Screen, 2 - Collection Payment Screen
        bool boolLoaded;

        // CODE FREEZE  TROGES-88  START
        string strUTService = "";
        // kk02232018 troges-88  Add Batch Payment for UT
        clsDRWrapper rsBatch = new clsDRWrapper();
        clsDRWrapper rsBatchBackup = new clsDRWrapper();
        clsDRWrapper rsBatchLien = new clsDRWrapper();
        clsDRWrapper rsBLN = new clsDRWrapper();
        int lngBill; // Don't think we're going to use this. Unlike RE not every account will have the same bill number
        string strPaidBy = "";
        string strRef = "";
        string strTLRID = "";
        DateTime dtPaymentDate;
        DateTime dtEffectiveDate;
        double dblIntRate;
        double dblCurPrin; // will hold the value of the prin, int and cost to add to the grid
        double dblCurTax; // and will be cleared by the AccountValidate Account
        double dblCurInt;
        double dblPLInt;
        double dblCurCost; // these will be filled as a side effect of the CalculateAccount function
        DateTime dtIntPaidDate; // this will hold the InterestStartDate
        int lngBatchCHGINTNumber; // this is how the CHGINT Number will be passed back to the PaymentRec
        double dblXtraCurInt;
        bool boolResultsScreen; // this is true when the result grid is being shown
        double dblTotalDue; // this will hold the total to warn the user if they enter too much
                            // xxxx DO WE ALLOW THIS????
                            // xxxx Public intPrePayYear            As Integer                  'this will be where the prepayyear form will put the year the user select
        bool boolSearch;
        string strSearchString = "";
        string strPassSortOrder = "";
        bool boolPassActivate;
        double dblOriginalAmount;
        bool boolBatchImport;

        public int lngColBatchAcct;
        public int lngColBatchName;
        public int lngColBatchBill;
        public int lngColBatchService;
        public int lngColBatchRef;
        public int lngColBatchPrin;
        public int lngColBatchTax;
        public int lngColBatchInt;
        public int lngColBatchCost;
        public int lngColBatchTotal;
        public int lngColBatchKey;
        public int lngColBatchPrintReceipt;
        public int lngColBatchCorrectAmount;

        // YYYYYYYYYYYYYYYYYYYYYY
        public bool boolProcessingBatch;
        // YYYYYYYYYYYYYYYYYYYYYY

        int lngColBSKey;
        int lngColBSName;
        int lngColBSMapLot;
        int lngColBSAccount;


        private struct Import
        {
            public string[] Account;
            public string[] Name;
            public string[] Year;
            public string[] Amount;
            public string[] PaymentDate;
            public string[] CRLF;
        };
        Import CRImport = new Import();
        Import[] arrCRImportError = null;
        // CODE FREEZE  TROGES-88  END

        public void Init(short intPassType, bool boolAutoResize = false, int lngAcct = 0)
        {
            string strKey = "";
            string strTemp = "";
            // frmReceiptInput.boolLoadingUTsearch = True
            intType = intPassType;
            modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "UTLastAccountNumber", ref strKey);
            //Application.DoEvents();
            this.Show(App.MainForm);
            if (modGlobalConstants.Statics.gboolCR && !modUTFunctions.Statics.gboolShowLastUTAccountInCR)
            {
                // txtGetAccountNumber.Text = Val(strKey)
            }
            else
            {
                txtGetAccountNumber.Text = FCConvert.ToString(Conversion.Val(strKey));
            }
            // Open "UTOutput.txt" For Output As #1
            // Write #1, "A - " & Format(Now, "hh:mm:ss:")
            modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, "SOFTWARE\\TrioVB\\", "UTLastAccountNumber", ref strTemp);
            // Write #1, "B - " & Format(Now, "hh:mm:ss:")
            if (Strings.Trim(strTemp) != "")
            {
                modUTFunctions.Statics.lngCurrentAccountUT = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
            }
            // Write #1, "C - " & Format(Now, "hh:mm:ss:")
            vsSearch.Visible = false;
            fraCriteria.Visible = true;
            // Write #1, "D - " & Format(Now, "hh:mm:ss:")
            // set label captions on form
            txtGetAccountNumber.Text = FCConvert.ToString(modUTFunctions.Statics.lngCurrentAccountUT);
            // puts last account accessed in the textbox
            //Application.DoEvents();
            // Write #1, "E - " & Format(Now, "hh:mm:ss:")
            if (txtGetAccountNumber.Enabled && txtGetAccountNumber.Visible)
            {
                txtGetAccountNumber.Focus();
            }
            // Write #1, "F - " & Format(Now, "hh:mm:ss:")
            txtGetAccountNumber.SelectionStart = 0;
            // Write #1, "G - " & Format(Now, "hh:mm:ss:")
            txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
            // highlights text
            if (modUTCalculations.Statics.gUTSettings.AllowAutoPay)
            {
                cmdAutoPayPost.Visible = true;
            }
            else
            {
                cmdAutoPayPost.Visible = false;
            }
            if (boolAutoResize)
            {
                SetAct(intAction);
            }
        }

        public void StartProgram(int lngAccountKey, int lngAcctNumber)
        {
            // this will load the account master screen
            if (intType != 0 || lngAcctNumber > 0)
            {
                frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Account", false, 100, false);
            }
            //FC:FINAL:DDU:#i1049 - focus on correct tab
            // MAL@20070905: Check for cancelled add to stop form from unloading
            if (!modGlobalConstants.Statics.blnAddCancel)
            {
                modGlobalConstants.Statics.blnAddCancel = false;
                Close();
            }
            switch (intType)
            {
                case 0:
                    {
                        // master/meter screen
                        // save the last UT account
                        Label1.Text = "Please enter the account number.  Hit <Enter> to view the last account accessed.  Enter '0' to add a new account.";
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "UTLastAccountNumber", modGlobal.PadToString_8(lngAcctNumber, 6));
                        modGlobal.GetMasterAccount(lngAccountKey, lngAcctNumber);
                        // frmAccountMaster.Init lngAccountKey, lngAcctNumber
                        break;
                    }
                case 1:
                case 2:
                case 3:
                    {
                        // collection status or payment screen
                        Label1.Text = "Enter the account or hit Enter to view the account shown";
                        modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "UTLastAccountNumber", modGlobal.PadToString_8(lngAcctNumber, 6));
                        frmUTCLStatus.InstancePtr.Init(FCConvert.ToInt16(intType), lngAccountKey);
                        break;
                    }
            }
            //end switch
        }

        private void cmdClear_Click(object sender, System.EventArgs e)
        {
            cmbHidden.Text = "";
            txtSearch.Text = "";
            txtSearch2.Text = "";
        }

        public void cmdClear_Click()
        {
            cmdClear_Click(cmdFileClearSearch, new System.EventArgs());
        }

        private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
        {
            int PrevAccount = 0;
            // holds the last account last accessed
            if (intAction == 0)
            {
                if (Conversion.Val(txtGetAccountNumber.Text) != 0)
                {
                    PrevAccount = modUTFunctions.Statics.lngCurrentAccountUT;
                    modUTFunctions.Statics.lngCurrentAccountUT = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
                TRYAGAIN:
                    ;
                    // SQL statement to find account number
                    strSQL = "SELECT * from Master WHERE AccountNumber = " + FCConvert.ToString(modUTFunctions.Statics.lngCurrentAccountUT);
                    //Application.DoEvents();
                    rs.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                    if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                    {
                        if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("Deleted")))
                        {
                            if (MessageBox.Show("Account has been deleted.  Would you like to undelete this account?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                modGlobalFunctions.AddCYAEntry_8("UT", "Account " + FCConvert.ToString(modUTFunctions.Statics.lngCurrentAccountUT) + " undeleted.");
                                rs.Execute("UPDATE Master SET Deleted = 0 WHERE AccountNumber = " + FCConvert.ToString(modUTFunctions.Statics.lngCurrentAccountUT), modExtraModules.strUTDatabase);
                                MessageBox.Show("Account " + FCConvert.ToString(modUTFunctions.Statics.lngCurrentAccountUT) + " has been undeleted.", "Undeleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                goto TRYAGAIN;
                            }
                            else
                            {
                                SetAct(0);
                                modUTFunctions.Statics.lngCurrentAccountUT = PrevAccount;
                                txtGetAccountNumber.Text = FCConvert.ToString(modUTFunctions.Statics.lngCurrentAccountUT);
                                // puts last account accessed in the textbox
                                txtGetAccountNumber.Focus();
                                txtGetAccountNumber.SelectionStart = 0;
                                txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
                                // highlights text
                                return;
                            }
                        }
                        else
                        {
                            StartProgram(FCConvert.ToInt32(rs.Get_Fields_Int32("ID")), modUTFunctions.Statics.lngCurrentAccountUT);
                        }
                    }
                    else
                    {
                        if (intType == 0)
                        {
                            switch (MessageBox.Show("There is no account with that number, would you like to create it?", "Account Number Not Used", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                            {
                                case DialogResult.Yes:
                                    {
                                        Make_New_Master_Account(modUTFunctions.Statics.lngCurrentAccountUT);
                                        break;
                                    }
                                case DialogResult.No:
                                case DialogResult.Cancel:
                                    {
                                        modUTFunctions.Statics.lngCurrentAccountUT = PrevAccount;
                                        txtGetAccountNumber.Text = FCConvert.ToString(modUTFunctions.Statics.lngCurrentAccountUT);
                                        // puts last account accessed in the textbox
                                        txtGetAccountNumber.Focus();
                                        txtGetAccountNumber.SelectionStart = 0;
                                        txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
                                        // highlights text
                                        break;
                                    }
                            }
                            //end switch
                        }
                        else
                        {
                            MessageBox.Show("There is no account with that number.", "Missing Account Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            frmWait.InstancePtr.Unload();
                        }
                    }
                }
                else
                {
                    if (intType == 0)
                    {
                        Make_New_Master_Account();
                        // 0 = create new master account record
                    }
                    else
                    {
                        frmWait.InstancePtr.Unload();
                    }
                }
            }
            else if (intAction == 1)
            {
                SelectAccount(vsSearch.Row);
            }
        }

        public void cmdGetAccountNumber_Click()
        {
            cmdGetAccountNumber_Click(cmdSelectAccount, new System.EventArgs());
        }

        private void Make_New_Master_Account(int lngNewAcctNumber = 0)
        {
            StartProgram(-1, lngNewAcctNumber);
        }

        private void cmdQuit_Click(object sender, System.EventArgs e)
        {
            Close();
            // returns to one form higher in the form hierarchy
        }

        private void cmdSearch_Click(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                int lngCurrentAccountKey;
                bool boolContainsSearch;
                string strStartsWithSearch = "";
                string strStartsWithMaster = "";
                string strPrevOwner = "";
                string strStartsWithSearchP = "";
                string strStartsWithMasterP = "";
                bool boolShowPrevOwner;
                clsDRWrapper rsUpdate = new clsDRWrapper();
                string strMasterFields;
                boolContainsSearch = cmbSearchPlace.SelectedIndex == 1;
                boolShowPrevOwner = FCConvert.CBool(chkShowPreviousOwnerInfo.CheckState == Wisej.Web.CheckState.Checked);
                // first check to see if they clicked an option button
                if (cmbHidden.Text == "")
                {
                    MessageBox.Show("Please choose a search option.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                // loads and shows wait message
                frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Searching");
                strMasterFields = "Master.*,Master.DeedName1 AS OwnerName,DeedName2 AS SecondOwnerName," + "pBill.FullNameLF AS Name,pBill2.FullNameLF AS Name2";
                // searching the database
                if (Strings.Trim(txtSearch.Text) != "")
                {
                    if (cmbHidden.SelectedIndex == 0)
                    {
                        // Name
                        if (boolContainsSearch)
                        {
                            strStartsWithSearch = " OR (pBill.FullNameLF LIKE ('%" + txtSearch.Text + "%')) OR (pBill2.FullNameLF LIKE ('%" + txtSearch.Text + "%')) ";
                            strStartsWithMaster = " OR (DeedName1 LIKE ('%" + txtSearch.Text + "%')) OR (DeedName2 LIKE ('%" + txtSearch.Text + "%')) ";
                            strStartsWithSearchP = " OR (BName LIKE ('%" + txtSearch.Text + "%')) OR (BName2 LIKE ('%" + txtSearch.Text + "%')) ";
                            strStartsWithMasterP = " OR (OName LIKE ('%" + txtSearch.Text + "%')) OR (OName2 LIKE ('%" + txtSearch.Text + "%')) ";
                        }
                        else
                        {
                            strStartsWithSearch = "";
                            strStartsWithMaster = "";
                            strStartsWithSearchP = "";
                            strStartsWithMasterP = "";
                        }
                        // Distinct ActualAccountNumber
                        strPrevOwner = "SELECT Distinct ActualAccountNumber, BName, OName, MapLot, Location, AccountKey FROM Bill WHERE ((BName >= '" + Strings.Trim(txtSearch.Text) + "    ' AND BName <= '" + Strings.Trim(txtSearch.Text) + "zzzz') OR (OName >= '" + Strings.Trim(txtSearch.Text) + "    ' AND OName <= '" + Strings.Trim(txtSearch.Text) + "zzzz') OR (BName2 >= '" + Strings.Trim(txtSearch.Text) + "    ' AND BName2 <= '" + Strings.Trim(txtSearch.Text) + "zzzz') OR (OName2 >= '" + Strings.Trim(txtSearch.Text) + "    ' AND OName2 <= '" + Strings.Trim(txtSearch.Text) + "zzzz') " + strStartsWithSearchP + strStartsWithMasterP + ")";
                        // AND Deleted = False"
                        strSQL = "SELECT " + strMasterFields + " FROM Master INNER JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = Master.OwnerPartyID INNER JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID " + "LEFT JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID LEFT JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pBill2 ON pBill2.ID = Master.SecondBillingPartyID " + "WHERE ((pBill.FullNameLF >= '" + Strings.Trim(txtSearch.Text) + "    ' AND pBill.FullNameLF <= '" + Strings.Trim(txtSearch.Text) + "zzzz') OR (DeedName1 >= '" + Strings.Trim(txtSearch.Text) + "    ' AND DeedName1 <= '" + Strings.Trim(txtSearch.Text) + "zzzz') OR (pBill2.FullNameLF >= '" + Strings.Trim(txtSearch.Text) + "    ' AND pBill2.FullNameLF <= '" + Strings.Trim(txtSearch.Text) + "zzzz') OR (DeedName2 >= '" + Strings.Trim(txtSearch.Text) + "    ' AND DeedName2 <= '" + Strings.Trim(txtSearch.Text) + "zzzz') " + strStartsWithSearch + strStartsWithMaster + ")";
                        // AND Deleted = False"
                        // builds the SQL statement to select all records
                        rs.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                        // selects all the records and fill the recordset
                    }
                    else if (cmbHidden.SelectedIndex == 1)
                    {
                        // Mailing Address
                        if (boolContainsSearch)
                        {
                            strStartsWithSearch = " OR (pBill.Address1 LIKE ('%" + txtSearch.Text + "%')) ";
                            strStartsWithMaster = " OR (pOwn.Address1 LIKE ('%" + txtSearch.Text + "%')) ";
                            strStartsWithSearchP = " OR (BAddress1 LIKE ('%" + txtSearch.Text + "%')) ";
                            strStartsWithMasterP = " OR (OAddress1 LIKE ('%" + txtSearch.Text + "%')) ";
                        }
                        else
                        {
                            strStartsWithSearch = "";
                            strStartsWithMaster = "";
                            strStartsWithSearchP = "";
                            strStartsWithMasterP = "";
                        }
                        strPrevOwner = "SELECT Distinct ActualAccountNumber, BName, OName, MapLot, Location, AccountKey FROM Bill WHERE (BAddress1 >= '" + Strings.Trim(txtSearch.Text) + "    ' AND BAddress1 <= '" + Strings.Trim(txtSearch.Text) + "zzzz' )" + strStartsWithSearchP + strStartsWithMasterP;
                        // AND Deleted = False"
                        strSQL = "SELECT " + strMasterFields + " FROM Master INNER JOIN " + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.ID = Master.OwnerPartyID INNER JOIN " + modGlobal.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID " + "LEFT JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID LEFT JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pBill2 ON pBill2.ID = Master.SecondBillingPartyID " + "WHERE (pBill.Address1 >= '" + Strings.Trim(txtSearch.Text) + "    ' AND pBill.Address1 <= '" + Strings.Trim(txtSearch.Text) + "zzzz') " + strStartsWithSearch + strStartsWithMaster;
                        // AND Deleted = False"
                        rs.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                    }
                    else if (cmbHidden.SelectedIndex == 3)
                    {
                        // Map Lot
                        if (boolContainsSearch)
                        {
                            strStartsWithSearch = " OR (MapLot LIKE ('%" + txtSearch.Text + "%')) ";
                            strStartsWithMaster = "";
                            strStartsWithSearchP = " OR (MapLot LIKE ('%" + txtSearch.Text + "%')) ";
                            strStartsWithMasterP = "";
                        }
                        else
                        {
                            strStartsWithSearch = "";
                            strStartsWithMaster = "";
                            strStartsWithSearchP = "";
                            strStartsWithMasterP = "";
                        }
                        strPrevOwner = "SELECT Distinct ActualAccountNumber, BName, OName, MapLot, Location, AccountKey FROM Bill WHERE (MapLot >= '" + Strings.Trim(txtSearch.Text) + "    ' AND MapLot <= '" + Strings.Trim(txtSearch.Text) + "zzzz' " + strStartsWithSearch + ")";
                        // AND Deleted = False"
                        strSQL = "SELECT " + strMasterFields + " FROM Master INNER JOIN " + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.ID = Master.OwnerPartyID INNER JOIN " + modGlobal.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID " + "LEFT JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID LEFT JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pBill2 ON pBill2.ID = Master.SecondBillingPartyID " + "WHERE (MapLot >= '" + Strings.Trim(txtSearch.Text) + "    ' AND MapLot <= '" + Strings.Trim(txtSearch.Text) + "zzzz' " + strStartsWithSearch + ")";
                        // AND Deleted = False"
                        rs.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                    }
                    else if (cmbHidden.SelectedIndex == 4)
                    {
                        // RE Account Number
                        strStartsWithSearch = "";
                        strStartsWithMaster = "";
                        strStartsWithSearchP = "";
                        strStartsWithMasterP = "";
                        strPrevOwner = "";
                        strSQL = "SELECT " + strMasterFields + " FROM Master INNER JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.ID = Master.OwnerPartyID INNER JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID " + "LEFT JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyNameView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID LEFT JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyNameView pBill2 ON pBill2.ID = Master.SecondBillingPartyID "
                                 + "WHERE REAccount = " + FCConvert.ToString(FCConvert.ToInt32(Strings.Trim(txtSearch.Text)));
                        rs.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                    }
                    else if (cmbHidden.SelectedIndex == 5)
                    {
                        // Serial Number
                        if (boolContainsSearch)
                        {
                            strStartsWithSearch = "SerialNumber LIKE '%" + txtSearch.Text + "%'";
                            strStartsWithMaster = "";
                            strStartsWithSearchP = "";
                            strStartsWithMasterP = "";
                        }
                        else
                        {
                            strStartsWithSearch = "SerialNumber LIKE '" + txtSearch.Text + "%'";
                            strStartsWithMaster = "";
                            strStartsWithSearchP = "";
                            strStartsWithMasterP = "";
                        }
                        strPrevOwner = "";
                        strSQL = "SELECT " + strMasterFields + " FROM Master INNER JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.ID = Master.OwnerPartyID INNER JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID LEFT JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID LEFT JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pBill2 ON pBill2.ID = Master.SecondBillingPartyID "
                                 + "WHERE Master.ID IN (SELECT DISTINCT AccountKey FROM MeterTable WHERE " + strStartsWithSearch + ")";
                        rs.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                    }
                    else if (cmbHidden.SelectedIndex == 6)
                    {
                        // Remote Number
                        if (boolContainsSearch)
                        {
                            strStartsWithSearch = "Remote LIKE '%" + txtSearch.Text + "%'";
                            strStartsWithMaster = "";
                            strStartsWithSearchP = "";
                            strStartsWithMasterP = "";
                        }
                        else
                        {
                            strStartsWithSearch = "Remote LIKE '" + txtSearch.Text + "%'";
                            strStartsWithMaster = "";
                            strStartsWithSearchP = "";
                            strStartsWithMasterP = "";
                        }
                        strPrevOwner = "";
                        strSQL = "SELECT " + strMasterFields + " FROM Master INNER JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.ID = Master.OwnerPartyID INNER JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID LEFT JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID LEFT JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pBill2 ON pBill2.ID = Master.SecondBillingPartyID "
                                 + "WHERE Master.ID IN (SELECT DISTINCT AccountKey FROM MeterTable WHERE " + strStartsWithSearch + ")";
                        rs.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                    }
                    else if (cmbHidden.SelectedIndex == 2)
                    {
                        // Location
                        if (boolContainsSearch)
                        {
                            strStartsWithSearch = "StreetName LIKE '%" + txtSearch.Text + "%'";
                            strStartsWithMaster = "";
                            strStartsWithSearchP = "";
                            strStartsWithMasterP = "";
                        }
                        else
                        {
                            strStartsWithSearch = "StreetName LIKE '" + txtSearch.Text + "%'";
                            strStartsWithMaster = "";
                            strStartsWithSearchP = "";
                            strStartsWithMasterP = "";
                        }
                        strPrevOwner = "";
                        strSQL = "SELECT " + strMasterFields + " FROM Master INNER JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.ID = Master.OwnerPartyID INNER JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID LEFT JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID LEFT JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pBill2 ON pBill2.ID = Master.SecondBillingPartyID "
                                 + "WHERE " + strStartsWithSearch;
                        if (Conversion.Val(txtSearch2.Text) > 0)
                        {
                            strSQL += " AND StreetNumber LIKE '" + txtSearch2.Text + "%'";
                        }
                        rs.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                    }
                    else if (cmbHidden.SelectedIndex == 7)
                    {
                        // Ref 1
                        if (boolContainsSearch)
                        {
                            strStartsWithSearch = "XRef1 LIKE '%" + txtSearch.Text + "%'";
                            strStartsWithMaster = "";
                            strStartsWithSearchP = "";
                            strStartsWithMasterP = "";
                        }
                        else
                        {
                            strStartsWithSearch = "XRef1 LIKE '" + txtSearch.Text + "%'";
                            strStartsWithMaster = "";
                            strStartsWithSearchP = "";
                            strStartsWithMasterP = "";
                        }
                        strPrevOwner = "";
                        strSQL = "SELECT " + strMasterFields + " FROM Master INNER JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.ID = Master.OwnerPartyID INNER JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID LEFT JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID LEFT JOIN "
                                 + modGlobal.Statics.strDbCP + "PartyAndAddressView pBill2 ON pBill2.ID = Master.SecondBillingPartyID "
                                 + "WHERE Master.ID IN (SELECT DISTINCT AccountKey FROM MeterTable WHERE " + strStartsWithSearch + ")";
                        rs.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                    }
                    if (boolShowPrevOwner && strPrevOwner != "")
                    {
                        rsPrev.OpenRecordset(strPrevOwner, modExtraModules.strUTDatabase);
                    }
                    else
                    {
                        rsPrev.OpenRecordset("SELECT * FROM Bill WHERE ID = -1", modExtraModules.strUTDatabase);
                    }
                }
                else
                {
                    frmWait.InstancePtr.Unload();
                    MessageBox.Show("Please enter a search string.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                // if the table opened was the meter table, then get a new recordset from the Master table with the correct account numbers
                if (MeterTable == true)
                {
                    MeterTable = false;
                    if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                    {
                        // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                        StartProgram(FCConvert.ToInt32(rs.Get_Fields_Int32("AccountKey")), FCConvert.ToInt32(rs.Get_Fields_Int32("AccountNumber")));
                        return;
                    }
                }
                // the recordset is filled with the records that meet the search criteria
                if (!rs.EndOfFile() || !rsPrev.EndOfFile())
                {
                    // if not at the beginning or the end of the recordset
                    if (rs.RecordCount() != 1)
                    {
                        // if there are records in the recordset then
                        // build a screen for the user to choose the correct account
                        FillSearchGrid();
                        SetAct(1);
                        // load the account list grid
                        frmWait.InstancePtr.Unload();
                        // unloads the clockscreen
                    }
                    else
                    {
                        // if there is only one account found, then show the master screen
                        if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("Deleted")))
                        {
                            frmWait.InstancePtr.Unload();
                            if (MessageBox.Show("Account has been deleted.  Would you like to undelete this account?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                                modGlobalFunctions.AddCYAEntry_8("UT", "Account " + rs.Get_Fields("AccountNumber") + " undeleted.");
                                // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                                rsUpdate.Execute("UPDATE Master SET Deleted = 0 WHERE AccountNumber = " + rs.Get_Fields("AccountNumber"), modExtraModules.strUTDatabase);
                                // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                                MessageBox.Show("Account " + rs.Get_Fields("AccountNumber") + " has been undeleted.", "Undeleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                goto LoadAcct;
                            }
                            else
                            {
                                return;
                            }
                        }
                    LoadAcct:
                        ;
                        // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                        StartProgram(FCConvert.ToInt32(rs.Get_Fields_Int32("ID")), FCConvert.ToInt32(rs.Get_Fields_Int32("AccountNumber")));
                    }
                }
                else
                {
                    frmWait.InstancePtr.Unload();
                    MessageBox.Show("No accounts found.", "No Results", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                return;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Search", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public void cmdSearch_Click()
        {
            cmdSearch_Click(cmdFileSearch, new System.EventArgs());
        }
        // vbPorter upgrade warning: intAct As short	OnWriteFCConvert.ToInt32(
        private void SetAct(int intAct)
        {
            //FC:FINAL:DDU:#1045 - standardize form as requested
            // this will set the form and controls
            intAction = intAct;
            if (intAction == 0)
            {
                // beginning search screen
                //fraCriteria.Top = FCConvert.ToInt32((this.Height - fraCriteria.Height) / 2.0);
                //fraCriteria.Left = FCConvert.ToInt32((this.Width - fraCriteria.Width) / 2.0);
                vsSearch.Visible = false;
            }
            else if (intAction == 1)
            {
                // shows the list of accounts to choose from
                vsSearch.Top = fraCriteria.Top + fraCriteria.Height + 20;
                // (Me.Height - vsSearch.Height) / 2
                vsSearch.Left = FCConvert.ToInt32((this.Width - vsSearch.Width) / 2.0);
                vsSearch.Visible = true;
            }
        }

        private void frmGetMasterAccount_Activated(object sender, System.EventArgs e)
        {
            string strTemp = "";
            if (modGlobal.FormExist(this))
                return;
            if (!boolLoaded)
            {
                // Write #1, "H - " & Format(Now, "hh:mm:ss:")
                boolLoaded = true;
                // Close #1
            }
        }

        private void frmGetMasterAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                //FC:FINAL:AM:#3501 - execute search when the search textbox is active
                if (this.ActiveControl == txtSearch)
                {
                    return;
                }
                //FC:FINAL:AM:#3712 - use the account number only if the batch frames are not visible
                //if (Strings.Trim(txtSearch.Text) != "" && intAction == 0)
                //{
                //	// check to see if there is any text in the search string first
                //	if (fraBatchQuestions.Visible == false && fraBatch.Visible == false)
                //	{ // kk02232018 troges-88        'CODE FREEZE  TROGES-88
                //	  // else use the account number
                //		cmdGetAccountNumber_Click();
                //	}
                //	// if so, then use the search
                //}
                //else
                //{
                //	// else use the account number
                //	cmdGetAccountNumber_Click();
                //}
                if (fraBatchQuestions.Visible == false && fraBatch.Visible == false)
                {
                    cmdGetAccountNumber_Click();
                }


            }
            else if (KeyAscii == Keys.Escape)
            {
                KeyAscii = (Keys)0;
                if (vsSearch.Visible)
                {
                    SetAct(0);
                }
                else
                {
                    Close();
                }
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void frmGetMasterAccount_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmGetMasterAccount.ScaleWidth	= 9300;
            //frmGetMasterAccount.ScaleHeight	= 8130;
            //frmGetMasterAccount.LinkTopic	= "Form1";
            //End Unmaped Properties
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            modGlobalFunctions.SetTRIOColors(this);
            this.Text = "Select Master Account";
            //FC:FINAL:BSE #3499 set default value
            cmbSearchPlace.SelectedIndex = 0;
            lngColBatchCorrectAmount = 0; // CODE FREEZE  TROGES-88
            lngColBatchPrintReceipt = 1;
            lngColBatchAcct = 2;
            lngColBatchName = 3;
            lngColBatchBill = 4;
            lngColBatchService = 5;
            lngColBatchRef = 6;
            lngColBatchPrin = 7;
            lngColBatchTax = 8;
            lngColBatchInt = 9;
            lngColBatchCost = 10;
            lngColBatchTotal = 11;
            lngColBatchKey = 12;

            lngColHidden = 0;
            lngColAccountNumber = 1;
            lngColName = 2;
            lngColLocationNumber = 4;
            lngColLocation = 5;
            lngColTenant = 3;
            lngColSortName = 6;
            lngColSortName2 = 7;
            lngColMapLot = 8;
            lngColREAcct = 9;
            lngColSerialNumber = 10;
            lngColRemoteNumber = 11;
            lngColRef1 = 12;

            // trouts-266 code freeze
            if (modExtraModules.IsThisCR())
            { // Running CR, Show the posting menu only
                cmdAutoPayPost.Visible = true;
            }
            else if (modGlobalConstants.Statics.gboolCR)
            { // CR is installed but this is UT, show prenote menu only
                cmdAutoPayPost.Visible = false;
            }
            else
            { // No CR at all, show both posting and prenote
                cmdAutoPayPost.Visible = true;
            }
        }

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            boolLoaded = false;
        }

        private void frmGetMasterAccount_Resize(object sender, System.EventArgs e)
        {
            if (boolLoaded)
            {
                SetAct(intAction);
                if (intAction == 1)
                {
                    // if the grid is visible
                    FormatGrid();
                    // this will resize the cols
                    SetGridHeight();
                    // this will resize the grid height
                }

                // kk02232018 troges-88  Add code for Batch Grid                          'CODE FREEZE  TROGES-88
                if (fraBatchQuestions.Visible)
                {
                    //FC:FINAL:BSE #3548  frame should be aligned to the left-margin
                    fraBatchQuestions.Top = FCConvert.ToInt32((this.Height - fraBatchQuestions.Height) / 17.0);
                    fraBatchQuestions.Left = FCConvert.ToInt32((this.Width - fraBatchQuestions.Width) / 17.0);
                    fraValidate.Top = fraBatchQuestions.Top + cmdBatch.Top - 100;
                    fraValidate.Left = FCConvert.ToInt32((this.Width - fraValidate.Width) / 2.0);
                }
                else if (fraRecover.Visible)
                {
                    //FC:FINAL:BSE:#4286 frame should be moved up
                    fraRecover.Left = FCConvert.ToInt32((this.Width - fraRecover.Width) / 26.0);
                    //fraRecover.Left = FCConvert.ToInt32((this.Width - fraRecover.Width) / 2.0);
                    fraRecover.Top = FCConvert.ToInt32((this.Height - fraRecover.Height) / 20.0);
                    //fraRecover.Top = FCConvert.ToInt32((this.Height - fraRecover.Height) / 3.0);
                }
                else if (vsBatch.Visible)
                {
                    FormatBatchGrid();
                    fraBatch.Left = FCConvert.ToInt32((this.Width - fraBatch.Width) / 2.0);
                    fraBatch.Top = FCConvert.ToInt32((this.Height - fraBatch.Height) / 3.0);
                    fraBatch.Visible = true;
                    // XXXXXX TODO: ADD fraBatchSearchCriteria ????
                    // If fraBatchSearchCriteria.Visible Then
                    // fraBatchSearchCriteria.Left = (Me.Width - fraBatchSearchCriteria.Width) / 2
                    // fraBatchSearchCriteria.Top = (Me.Height - fraBatchSearchCriteria.Height) / 3
                }
            }
        }

        private void mnuAutoPayPost_Click(object sender, System.EventArgs e)
        {
            // trouts-266 code freeze
            //FC:TODO:DDU!!!!!!frmAutoPay.InstancePtr.Init(true); // load the AutoPay form to post payments
            Close();
        }

        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
            boolLoaded = false;
            SetAct(0);
            // MAL@20080827: Reset the form ; Tracker Reference: 15193
            // frmReceiptInput.boolLoadingUTsearch = False
        }

        private void mnuFileClearSearch_Click(object sender, System.EventArgs e)
        {
            cmdClear_Click();
        }

        private void mnuFileSearch_Click(object sender, System.EventArgs e)
        {
            cmdSearch_Click();
        }

        private void mnuQuit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void mnuSelectAccount_Click(object sender, System.EventArgs e)
        {
            cmdGetAccountNumber_Click();
        }

        private void mnuUndeleteMaster_Click(object sender, System.EventArgs e)
        {
            int AcctNum = 0;
            string strAcctNum;
            int lngKey = 0;
            // prompts the user for the account number to be undeleted
            strAcctNum = Interaction.InputBox("Please enter the Account Number that you wish to Undelete", "Undelete", "000000");
            if (Information.IsNumeric(strAcctNum) == false || Conversion.Val(strAcctNum) == 0)
            {
                // checks input for a 0 string or a non numeric string
                MessageBox.Show("Account Numbers must be numeric and nonzero.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                AcctNum = FCConvert.ToInt32(Math.Round(Conversion.Val(strAcctNum)));
                // converts the string to a long
                clsDRWrapper rsUD = new clsDRWrapper();
                // holds the recordset to get undeleted
                strSQL = "SELECT * FROM Master WHERE AccountNumber = " + FCConvert.ToString(AcctNum) + " AND ISNULL(Deleted,0) = 1";
                rsUD.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                // searches for the account
                if (rsUD.RecordCount() == 0)
                {
                    // if it is not found tell the user and return to the get account screen
                    MessageBox.Show("No account with that number has been deleted.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    rsUD.Edit();
                    // if found then undelete the record and send the user to the master account edit screen
                    lngKey = FCConvert.ToInt32(rsUD.Get_Fields_Int32("ID"));
                    rsUD.Set_Fields("Deleted", false);
                    rsUD.Update();
                    MessageBox.Show("Account Number #" + FCConvert.ToString(AcctNum) + " has been undeleted.", "Successful Undeletion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    StartProgram(lngKey, AcctNum);
                }
            }
        }

        private void optSearchType_CheckedChanged(short Index, object sender, System.EventArgs e)
        {
            //FC:FINAL:DDU:#1045 - standardize form as requested
            // Set to Default states
            //cmbSearchPlace.Visible = true;
            //lblSearchPlace.Visible = true;
            txtSearch2.Visible = false;
            txtSearch2.Text = "";
            //lblSearchInfo.Visible = true;
            lblSearch.Visible = false;
            lblSearch2.Visible = false;
            txtSearch.Left = cmbHidden.Left + cmbHidden.Width + 30;
            //FC:FINAL:CHN - issue #1304: Missing search criteria. 
            // fraCriteria.Width = txtSearch.Left + txtSearch.Width + 20; // need to change size on redesign
            cmbSearchPlace.Visible = true;
            switch (Index)
            {
                case 4:
                    {
                        // kk05292014 trouts-78  Disable the Starts With/Contains options for By RE Account
                        //cmbSearchPlace.Visible = false;
                        //lblSearchPlace.Visible = false;
                        //FC:FINAL:CHN - issue #1304: Missing search criteria. 
                        cmbSearchPlace.Visible = false;
                        break;
                    }
                case 2:
                    {
                        // Change to Street Number and Street Name search for By Location
                        txtSearch2.Visible = true;
                        //lblSearchInfo.Visible = false;
                        lblSearch.Visible = true;
                        lblSearch2.Visible = true;
                        lblSearch2.Left = cmbHidden.Left + cmbHidden.Width + 30;
                        txtSearch2.Left = lblSearch2.Left + lblSearch2.Width + 15;
                        lblSearch.Left = txtSearch2.Left + txtSearch2.Width + 30;
                        txtSearch.Left = lblSearch.Left + lblSearch.Width + 30;
                        //FC:FINAL:CHN - issue #1304: Redesign
                        // fraCriteria.Width = txtSearch.Left + txtSearch.Width + 20;
                        break;
                    }
            }
            //FC:FINAL:CHN - issue #1599: Redesign.
            cmbSearchPlace.Left = txtSearch.Left + txtSearch.Width + 15;
            //end switch
        }

        private void optSearchType_CheckedChanged(object sender, System.EventArgs e)
        {
            short index = FCConvert.ToInt16(cmbHidden.SelectedIndex);
            optSearchType_CheckedChanged(index, sender, e);
        }

        private void txtGetAccountNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            // traps the backspace ID and all keys that are non numeric
            if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
            {
                KeyAscii = (Keys)0;
                // any ID other than backspace or number keys are not allowed
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            // keyascii 39 is the forward slash and 124 is the pipe
            // SQL cannot deal with these characters and will cause an untrappable error
            if (KeyAscii == Keys.Right || KeyAscii == Keys.F13)
                KeyAscii = (Keys)0;
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void FillSearchGrid()
        {
            try
            {
                // this will fill the grid with all of the accounts that match the search criteria
                string strAcct = "";
                string strName = "";
                string strLocation = "";
                string strLocationNumber = "";
                string strTenant = "";
                string strSortList = "";
                string strSortList2 = "";
                string strML = "";
                string strREAcct = "";
                string strSerial = "";
                string strRemote = "";
                string strXRef1 = "";
                bool boolShowPrev = false;
                // reset all of the sizes and clear the grid
                FormatGrid(true);
                while (!rs.EndOfFile())
                {
                    strSortList2 = "";
                    //Application.DoEvents();
                    // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                    strAcct = FCConvert.ToString(rs.Get_Fields("AccountNumber"));
                    // TODO: Field [OwnerName] not found!! (maybe it is an alias?)
                    strName = Strings.Trim(FCConvert.ToString(rs.Get_Fields("OwnerName")));
                    strLocationNumber = Strings.Trim(FCConvert.ToString(rs.Get_Fields_Int32("streetnumber")));
                    // If Trim(rs.Fields("StreetNumber")) <> "" Then
                    // strLocation = Trim(rs.Fields("StreetNumber") & " " & rs.Fields("StreetName"))
                    // Else
                    strLocation = Strings.Trim(" " + rs.Get_Fields_String("StreetName"));
                    // End If
                    strTenant = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Name")));
                    strML = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("MapLot")));
                    strREAcct = Strings.Trim(FCConvert.ToString(rs.Get_Fields_Int32("REAccount")));
                    if (cmbHidden.SelectedIndex == 6 || cmbHidden.SelectedIndex == 5 || cmbHidden.SelectedIndex == 7)
                    {
                        strSerial = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("SerialNumber")));
                        strRemote = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Remote")));
                        strXRef1 = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("XRef1")));
                    }
                    else
                    {
                        strSerial = "";
                        strRemote = "";
                        strXRef1 = "";
                    }
                    if (cmbHidden.SelectedIndex == 2)
                    {
                        strSortList = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("StreetName"))) + " ";
                        if (Information.IsNumeric(rs.Get_Fields_Int32("StreetNumber")))
                        {
                            // in case the value is something that will blow up somehow
                            /*? On Error Resume Next  */// strSortList = strSortList & Format(CInt(rs.Fields("StreetNumber")), "000000#")
                            strSortList2 = Strings.Format(rs.Get_Fields_Int32("StreetNumber"), "000000#");
                        }
                        else
                        {
                            // strSortList = strSortList & rs.Fields("StreetNumber")
                            strSortList2 = FCConvert.ToString(rs.Get_Fields_Int32("StreetNumber"));
                        }
                    }
                    if (cmbHidden.SelectedIndex == 0)
                    {
                        // find out which one should be sorted
                        if (cmbSearchPlace.SelectedIndex == 0)
                        {
                            // starts with
                            if (Strings.UCase(Strings.Left(strName, Strings.Trim(txtSearch.Text).Length)) == Strings.UCase(Strings.Trim(txtSearch.Text)))
                            {
                                strSortList = strName;
                            }
                            else
                            {
                                strSortList = strTenant;
                            }
                        }
                        else
                        {
                            // contains
                            if (Strings.InStr(1, Strings.UCase(strName), Strings.UCase(Strings.Trim(txtSearch.Text)), CompareConstants.vbBinaryCompare) != 0)
                            {
                                strSortList = strName;
                            }
                            else
                            {
                                strSortList = strTenant;
                            }
                        }
                    }
                    // actually add the row to the grid
                    vsSearch.AddItem("");
                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAccountNumber, strAcct);
                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, strName);
                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocationNumber, strLocationNumber);
                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, strLocation);
                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColHidden, FCConvert.ToString(rs.Get_Fields_Int32("ID")));
                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColTenant, strTenant);
                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColSortName, strSortList);
                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColSortName2, strSortList2);
                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, strML);
                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColREAcct, strREAcct);
                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColSerialNumber, strSerial);
                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColRemoteNumber, strRemote);
                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColRef1, strXRef1);
                    rs.MoveNext();
                }
                //FC:FINAL:CHN - issue #948: fix ordering by number column
                vsSearch.ColDataType(4, FCGrid.DataTypeSettings.flexDTLong);
                //FC:FINAL:CHN - issue #951: fix ordering by acct column
                vsSearch.ColDataType(1, FCGrid.DataTypeSettings.flexDTLong);
                // previous owners
                if (!rsPrev.EndOfFile() && chkShowPreviousOwnerInfo.CheckState == Wisej.Web.CheckState.Checked)
                {
                    while (!rsPrev.EndOfFile())
                    {
                        //Application.DoEvents();
                        strAcct = FCConvert.ToString(rsPrev.Get_Fields_Int32("ActualAccountNumber"));
                        strName = Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("OName")));
                        strLocation = Strings.Trim(" " + rsPrev.Get_Fields_String("Location"));
                        strLocationNumber = "";
                        strTenant = Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("BName")));
                        strML = Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("MapLot")));
                        strREAcct = "";
                        // Trim(rsPrev.Fields("REAccount"))
                        strSerial = "";
                        strRemote = "";
                        strXRef1 = "";
                        if (cmbHidden.SelectedIndex == 0)
                        {
                            // find out which one should be sorted
                            if (cmbSearchPlace.SelectedIndex == 0)
                            {
                                // starts with
                                if (Strings.UCase(Strings.Left(strName, Strings.Trim(txtSearch.Text).Length)) == Strings.UCase(Strings.Trim(txtSearch.Text)))
                                {
                                    strSortList = strName;
                                }
                                else
                                {
                                    strSortList = strTenant;
                                }
                            }
                            else
                            {
                                // contains
                                if (Strings.InStr(1, Strings.UCase(strName), Strings.UCase(Strings.Trim(txtSearch.Text)), CompareConstants.vbBinaryCompare) != 0)
                                {
                                    strSortList = strName;
                                }
                                else
                                {
                                    strSortList = strTenant;
                                }
                            }
                        }
                        if (vsSearch.FindRow(strName, -1, lngColName) < 0)
                        {
                            // only add the row if it has not already been added
                            boolShowPrev = true;
                        }
                        else
                        {
                            if (vsSearch.FindRow(strTenant, -1, lngColTenant) < 0)
                            {
                                // only add the row if it has not already been added
                                boolShowPrev = true;
                            }
                            else
                            {
                                if (vsSearch.FindRow(strAcct, -1, lngColAccountNumber) < 0)
                                {
                                    // only add the row if it has not already been added
                                    boolShowPrev = true;
                                }
                                else
                                {
                                    boolShowPrev = false;
                                }
                            }
                        }
                        if (boolShowPrev)
                        {
                            // actually add the row to the grid
                            vsSearch.AddItem("");
                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAccountNumber, strAcct);
                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, strName);
                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, strLocation);
                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocationNumber, strLocationNumber);
                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColHidden, FCConvert.ToString(rsPrev.Get_Fields_Int32("AccountKey")));
                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColTenant, strTenant);
                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColSortName, strSortList);
                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, strML);
                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColREAcct, strREAcct);
                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColSerialNumber, strSerial);
                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColRemoteNumber, strRemote);
                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColRef1, strXRef1);
                            vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
                        }
                        rsPrev.MoveNext();
                    }
                }
                SetGridHeight();
                if (cmbHidden.SelectedIndex == 0 || cmbHidden.SelectedIndex == 2)
                {
                    vsSearch.Select(1, lngColSortName, vsSearch.Rows - 1, lngColSortName2);
                    vsSearch.Sort = FCGrid.SortSettings.flexSortGenericAscending;
                    vsSearch.Refresh();
                    vsSearch.Select(0, 0);
                }
                return;
            }
            catch (Exception ex)
            {
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Search Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void FormatGrid(bool boolReset = false)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                int lngWid = 0;
                lngWid = vsSearch.WidthOriginal;
                vsSearch.Cols = 13;
                if (boolReset)
                {
                    vsSearch.Rows = 1;
                    vsSearch.TextMatrix(0, lngColAccountNumber, "Acct");
                    vsSearch.TextMatrix(0, lngColName, "Owner Name");
                    vsSearch.TextMatrix(0, lngColLocation, "Location");
                    vsSearch.TextMatrix(0, lngColLocationNumber, "#");
                    vsSearch.TextMatrix(0, lngColTenant, "Tenant Name");
                    vsSearch.TextMatrix(0, lngColSortName, "Sort Name");
                    vsSearch.TextMatrix(0, lngColMapLot, "Map Lot");
                    vsSearch.TextMatrix(0, lngColREAcct, "RE Acct");
                    vsSearch.TextMatrix(0, lngColSerialNumber, "Serial Number");
                    vsSearch.TextMatrix(0, lngColRemoteNumber, "Remote Number");
                    vsSearch.TextMatrix(0, lngColRef1, "Ref 1");
                    vsSearch.ExtendLastCol = true;
                }
                vsSearch.ColWidth(lngColHidden, 0);
                vsSearch.ColWidth(lngColAccountNumber, FCConvert.ToInt32(lngWid * 0.15));
                // .ColWidth(lngColLocation) = lngWid * 0.25
                vsSearch.ColWidth(lngColLocationNumber, FCConvert.ToInt32(lngWid * 0.06));
                vsSearch.ColWidth(lngColName, FCConvert.ToInt32(lngWid * 0.25));
                vsSearch.ColWidth(lngColTenant, FCConvert.ToInt32(lngWid * 0.25));
                // .ColWidth(lngColSortName) = lngWid * 0#
                vsSearch.ColHidden(lngColSortName, true);
                vsSearch.ColHidden(lngColSortName2, true);
                // .ColWidth(lngColMapLot) = lngWid * 0#
                vsSearch.ColHidden(lngColMapLot, true);
                // .ColWidth(lngColREAcct) = lngWid * 0#
                vsSearch.ColHidden(lngColREAcct, true);
                // .ColWidth(lngColSerialNumber) = lngWid * 0#
                vsSearch.ColHidden(lngColSerialNumber, true);
                // .ColWidth(lngColRemoteNumber) = lngWid * 0#
                vsSearch.ColHidden(lngColRemoteNumber, true);
                // .ColWidth(lngColRef1) = lngWid * 0#
                vsSearch.ColHidden(lngColRef1, true);
                return;
            }
            catch (Exception ex)
            {
                
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Formatting Search Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void SetGridHeight()
        {
            // this will check to see how many rows the grid has and adjust the height accordingly
            int lngRW = 0;
            lngRW = vsSearch.Rows;
            if ((lngRW * vsSearch.RowHeight(0)) + 70 > this.Height * 0.7)
            {
                vsSearch.Height = FCConvert.ToInt32(this.Height * 0.7);
                vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
            }
            else
            {
                vsSearch.Height = (lngRW * vsSearch.RowHeight(0)) + 70;
                vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
            }
        }

        private void SelectAccount(int lngRow)
        {
            // this routine will take the row passed and find the account number
            // then send the user to the Account Master that they have selected
            int lngAcct = 0;
            int lngKey = 0;
            clsDRWrapper rsCheck = new clsDRWrapper();
            SetAct(0);
            if (lngRow > 0)
            {
                // make sure that a valid row is being passed in
                lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(vsSearch.TextMatrix(lngRow, lngColAccountNumber))));
                // get the account number from the grid
                lngKey = FCConvert.ToInt32(Math.Round(Conversion.Val(vsSearch.TextMatrix(lngRow, lngColHidden))));
                // get the account number from the grid
                if (lngAcct > 0)
                {
                    // if it is a valid account number
                    rsCheck.OpenRecordset("SELECT * from Master WHERE AccountNumber = " + FCConvert.ToString(lngAcct), modExtraModules.strUTDatabase);
                    if (FCConvert.ToBoolean(rsCheck.Get_Fields_Boolean("Deleted")))
                    {
                        if (MessageBox.Show("Account has been deleted.  Would you like to undelete this account?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            modGlobalFunctions.AddCYAEntry_8("UT", "Account " + FCConvert.ToString(lngAcct) + " undeleted.");
                            rsCheck.Execute("UPDATE Master SET Deleted = 0 WHERE AccountNumber = " + FCConvert.ToString(lngAcct), modExtraModules.strUTDatabase);
                            MessageBox.Show("Account " + FCConvert.ToString(lngAcct) + " has been undeleted.", "Undeleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            return;
                        }
                    }
                    txtGetAccountNumber.Text = FCConvert.ToString(lngAcct);
                    modUTFunctions.Statics.lngCurrentAccountUT = lngAcct;
                    StartProgram(lngKey, lngAcct);
                    // call the master screen
                }
            }
        }

        private void vsSearch_DblClick(object sender, System.EventArgs e)
        {
            if (vsSearch.Row > 0)
            {
                SelectAccount(vsSearch.Row);
            }
        }

        private void vsSearch_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsSearch[e.ColumnIndex, e.RowIndex];

            int lngMR;
            int lngMC;
            lngMR = vsSearch.GetFlexRowIndex(e.RowIndex);
            lngMC = vsSearch.GetFlexColIndex(e.ColumnIndex);
            if (lngMR > 0 && lngMC >= 0)
            {
                if (FCConvert.ToBoolean(lngMR) & lngMC > 0)
                {
                    //ToolTip1.SetToolTip(vsSearch, vsSearch.TextMatrix(lngMR, lngMC));
                    cell.ToolTipText = vsSearch.TextMatrix(lngMR, lngMC);
                }
            }
        }

        private void vsSearch_Click(object sender, EventArgs e)
        {
            //FC:FINAL:DDU:#1045 - standardize form as requested
            if (Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAccountNumber), 6)) != 0)
            {
                txtGetAccountNumber.Text = Math.Round(Conversion.Val(vsSearch.TextMatrix(vsSearch.Row, lngColAccountNumber))).ToString();
            }
        }

        // CODE FREEZE TROGES-88   FROM HERE TO THE END
        // kk02232018 troges-88  Add Batch Payment Handling like CL batch
        private void cmbBatchList_DropDown(object sender, System.EventArgs e)
        {
            modAPIsConst.SendMessageByNum(this.cmbBatchList.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
        }

        private void cmbBatchList_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            int KeyCode = FCConvert.ToInt32(e.KeyCode);
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if ((Keys)KeyCode == Keys.Space)
            {
                if (modAPIsConst.SendMessageByNum(cmbBatchList.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
                {
                    modAPIsConst.SendMessageByNum(cmbBatchList.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
                    KeyCode = 0;
                }
            }
        }

        private void cmdBatch_Click(object sender, System.EventArgs e)
        {
            if (CheckBatchQuestions())
            {
                LockBatch_2(true);
            }
        }

        private void LockBatch_2(bool boolLock) { LockBatch(ref boolLock); }
        private void LockBatch(ref bool boolLock)
        {
            fraValidate.Visible = boolLock;
            fraValidate.Top = fraBatchQuestions.Top + cmdBatch.Top - 100;
            fraValidate.Left = FCConvert.ToInt32((frmGetMasterAccount.InstancePtr.Width - fraValidate.Width) / 2.0);
            lblValidate.Text = "IMPORTANT: Verify that the information entered is correct and press Yes.";

            cmdBatch.Enabled = !boolLock;
            txtTellerID.Enabled = !boolLock;
            txtPaymentDate2.Enabled = !boolLock;
            txtEffectiveDate.Enabled = !boolLock;
            txtPaidBy.Enabled = !boolLock;
            chkReceipt.Enabled = !boolLock;
        }

        private void cmdBatchChoice_Click(object sender, System.EventArgs e)
        {
            if (cmbBatchList.SelectedIndex != -1)
            {
                string VBtoVar = FCConvert.ToString(fraRecover.Tag);

                if (VBtoVar == "P")
                {
                    // Purge
                    PurgeRecords_2(cmbBatchList.Items[cmbBatchList.SelectedIndex].ToString());
                }
                else if (VBtoVar == "R")
                {
                    // Recover
                    RecoverRecords_2(cmbBatchList.Items[cmbBatchList.SelectedIndex].ToString());
                }
                else
                {
                }
            }
            else
            {
                MessageBox.Show("Please select a batch from the drop down list.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void cmdProcessBatch_Click(object sender, System.EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to process the batch with total of " + txtTotal.Text + "?", "Process Batch", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information) == DialogResult.Yes)
            {
                // print the batch receipts
                PrintBatchReceipts();
                // this will process the whole batch
                ProcessBatch();
            }
        }

        private void CreateBatchPaymentLine(int lngAcct, double dblPrin, double dblTax, double dblInt, double dblCost, DateTime dtDate, DateTime dtETDate, int lBill, string strSrvc)
        {
            try
            {   // On Error GoTo ERROR_HANDLER
                int lngRow;
                int lngKey = 0;

                // add it to the temporary table

                rsBatchBackup.OpenRecordset("SELECT * FROM BatchRecover WHERE ID = 0", modExtraModules.strUTDatabase);
                rsBatchBackup.AddNew();

                // set the key value from the batch recover table
                lngKey = rsBatchBackup.Get_Fields_Int32("ID");

                strRef = FCConvert.ToString(lngAcct) + "-" + FCConvert.ToString(lBill);

                // add the values to the table
                rsBatchBackup.Set_Fields("TellerID", strTLRID);
                rsBatchBackup.Set_Fields("PaidBy", strPaidBy + " ");
                rsBatchBackup.Set_Fields("Ref", strRef + " ");
                rsBatchBackup.Set_Fields("BillNumber", lBill);
                rsBatchBackup.Set_Fields("Service", strSrvc);
                rsBatchBackup.Set_Fields("AccountNumber", lngAcct);
                if (lblName.Text.Length > 6)
                {
                    rsBatchBackup.Set_Fields("Name", Strings.Right(lblName.Text, lblName.Text.Length - 6));
                }
                else
                {
                    rsBatchBackup.Set_Fields("Name", lblName.Text);
                }
                rsBatchBackup.Set_Fields("Prin", dblPrin);
                rsBatchBackup.Set_Fields("Int", dblInt);
                rsBatchBackup.Set_Fields("Cost", dblCost);
                rsBatchBackup.Set_Fields("Tax", dblTax);
                rsBatchBackup.Set_Fields("BatchRecoverDate", dtDate);
                rsBatchBackup.Set_Fields("ETDate", dtETDate);
                // xxxx .Fields("Type") = " "

                // save the record
                if (!rsBatchBackup.Update(true))
                {
                    MessageBox.Show("Error adding record batch.  Please try again.", "Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }
                vsBatch.AddItem("");
                lngRow = vsBatch.Rows - 1;
                // add it to the grid
                vsBatch.TextMatrix(lngRow, lngColBatchAcct, lngAcct);
                if (lblName.Text.Length > 6)
                {
                    vsBatch.TextMatrix(lngRow, lngColBatchName, Strings.Right(lblName.Text, lblName.Text.Length - 6));
                }
                else
                {
                    vsBatch.TextMatrix(lngRow, lngColBatchName, lblName.Text);
                }
                vsBatch.TextMatrix(lngRow, lngColBatchBill, lBill);
                vsBatch.TextMatrix(lngRow, lngColBatchService, strSrvc);
                vsBatch.TextMatrix(lngRow, lngColBatchRef, strRef);
                vsBatch.TextMatrix(lngRow, lngColBatchPrin, dblPrin);
                vsBatch.TextMatrix(lngRow, lngColBatchTax, dblTax);
                vsBatch.TextMatrix(lngRow, lngColBatchInt, dblInt);
                vsBatch.TextMatrix(lngRow, lngColBatchCost, dblCost);
                vsBatch.TextMatrix(lngRow, lngColBatchTotal, dblPrin + dblInt + dblCost + dblTax);
                vsBatch.TextMatrix(lngRow, lngColBatchKey, lngKey);
                if (chkReceipt.CheckState == CheckState.Checked)
                {
                    vsBatch.TextMatrix(lngRow, lngColBatchPrintReceipt, -1);
                }
                if (dblCurPrin + dblCurInt + dblPLInt + dblCurCost + dblTax == dblOriginalAmount)
                {
                    vsBatch.TextMatrix(lngRow, lngColBatchCorrectAmount, "Yes");
                }
                else
                {
                    vsBatch.TextMatrix(lngRow, lngColBatchCorrectAmount, "No");
                }
                return;
            }
            catch (Exception ex)
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Batch Records", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void PrintBatchReceipts()
        {
            try
            {   // On Error GoTo ERROR_HANDLER
                int lngRW;

                for (lngRW = 1; lngRW <= vsBatch.Rows - 1; lngRW++)
                {
                    if (Conversion.Val(vsBatch.TextMatrix(lngRW, lngColBatchPrintReceipt)) == -1)
                    {
#if TWCR0000
                        arBatchReceipt.InstancePtr.Init(lngRW);
                        // Doevents
                        arBatchReceipt.InstancePtr.Close();
#endif
                    }
                }
                return;
            }
            catch (Exception ex)
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Batch Receipts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void mnuBatchPrint_Click(object sender, System.EventArgs e)
        {
            // 
            // TODO: TROGES-88  CREATE THE UT BATCH LISTING XXXXXXXXXXXXXXXXXXXXXX
            MessageBox.Show("TODO: CREATE THE UT BATCH LISTING REPORT");
            // 
            // Dim dtDate              As Date
            // Dim lngYR               As Long
            // 
            // If vsBatch.rows > 1 And vsBatch.Visible Then
            // check for a year and a payment date
            // boolPassActivate = True
            // arBatchListing.Init lngYear, dtPaymentDate, dtEffectiveDate
            // Else
            // MsgBox "Save payments before trying to print a listing.", vbExclamation, "No Payments"
            // End If
        }


        private void mnuFileBatchSearch_Click(object sender, System.EventArgs e)
        {
            if (fraBatch.Visible && fraBatch.Enabled)
            {
                // this will only happen when the batch screen is showing and enabled
                fraBatch.Enabled = false;
                // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
                // fraBatchSearchCriteria.Left = (Me.Width - fraBatchSearchCriteria.Width) / 2
                // fraBatchSearchCriteria.Top = (Me.Height - fraBatchSearchCriteria.Height) / 2
                // fraBatchSearchCriteria.Visible = True
                // fraBatchSearchCriteria.ZOrder (0)
                // 
                // txtBatchSearchName.SetFocus
                // txtBatchSearchName.SelStart = 0
                // txtBatchSearchName.SelLength = Len(txtBatchSearchName.Text)
            }
        }

        // TROGES-88  NO GENERIC IMPORT ROUTINE - ONLY PAYPORT
        // Private Sub mnuFileImport_Click()
        // ImportRecords
        // End Sub

        private void mnuFileImportPayPortBatch_Click(object sender, System.EventArgs e)
        {
            ImportPayportBatch();
        }

        // 
        // TODO: NO PERIODS IN UT - NEED A SERVICE OPTION THOUGH?? XXXXXXXXXXXXXXXXXXXX
        // 
        // Private Sub optPeriod_Click(Index As Integer)
        // txtBatchAccount_Validate False
        // End Sub'

        // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        // Private Sub txtBatchSearchName_KeyDown(KeyCode As Integer, Shift As Integer)
        // Select Case KeyCode
        // Case vbKeyReturn, vbKeyDown
        // KeyCode = 0
        // If cmdBatchSearch.Visible And cmdBatchSearch.Enabled Then
        // cmdBatchSearch.SetFocus
        // End If
        // End Select
        // End Sub

        private void vsBatch_Click(object sender, EventArgs e)
        {
            int lngMC;
            int lngMR;

            lngMC = vsBatch.MouseCol;
            lngMR = vsBatch.MouseRow;
            if (lngMC == lngColBatchPrintReceipt)
            {

            }
            else if (lngMC == lngColBatchCorrectAmount)
            {
                // vsBatch.EditText = vsBatch.TextMatrix(lngMR, lngMC)
            }
            else
            {

            }
        }

        // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        // Private Sub vsBatchSearch_DblClick()
        // show the account
        // If Val(vsBatchSearch.TextMatrix(vsBatchSearch.MouseRow, lngColBSAccount)) > 0 Then
        // ShowAccount vsBatchSearch.TextMatrix(vsBatchSearch.MouseRow, lngColBSAccount)
        // End If
        // End Sub
        // 
        // Private Sub vsBatchSearch_KeyDown(KeyCode As Integer, Shift As Integer)
        // Select Case KeyCode
        // Case vbKeyReturn
        // If vsBatchSearch.Row > 0 Then
        // ShowAccount vsBatchSearch.TextMatrix(vsBatchSearch.Row, lngColBSAccount)
        // End If
        // KeyCode = 0
        // End Select
        // End Sub

        // XXXXXXXXXXXX I THINK THIS CODE IS FOR THE CL ACCOUNT SEARCH NOT BATCH SEARCH
        // Private Sub vsSearch_AfterSort(ByVal Col As Long, Order As Integer)
        // Dim strField            As String
        // Dim strOrder            As String
        // 
        // If Order = 1 Then
        // strOrder = "asc"
        // Else
        // strOrder = "desc"
        // End If
        // 
        // Select Case Col
        // Case lngColAcct
        // strField = "Account"
        // Case lngColName
        // strField = "Name"
        // Case lngColLocationNumber
        // strField = "StreetNumber"
        // Case lngColLocation
        // strField = "Street"
        // Case lngColBookPage
        // strField = "BookPage"
        // Case lngColMapLot
        // strField = "MapLot"
        // End Select
        // 
        // strPassSortOrder = "ORDER BY " & strField & " " & strOrder
        // End Sub
        // 
        // Private Sub vsSearch_AfterUserResize(ByVal Row As Long, ByVal Col As Long)
        // Dim intCT               As Integer
        // Dim lngWid              As Long
        // 
        // For intCT = 0 To vsSearch.Cols - 1
        // lngWid = lngWid + vsSearch.ColWidth(intCT)
        // Next
        // 
        // If lngWid > vsSearch.Width Then
        // vsSearch.ScrollBars = flexScrollBarBoth
        // set the height of the grid
        // If (vsSearch.rows * vsSearch.RowHeight(0)) + 70 < (Me.Height - vsSearch.Top) - 1000 Then
        // vsSearch.Height = ((vsSearch.rows + 1) * vsSearch.RowHeight(0))
        // vsSearch.ScrollBars = flexScrollBarHorizontal
        // Else
        // vsSearch.Height = (Me.Height - vsSearch.Top) - 1000
        // vsSearch.ScrollBars = flexScrollBarBoth
        // End If
        // Else
        // set the height of the grid
        // If (vsSearch.rows * vsSearch.RowHeight(0)) + 70 < (Me.Height - vsSearch.Top) - 1000 Then
        // vsSearch.Height = (vsSearch.rows * vsSearch.RowHeight(0)) + 70
        // vsSearch.ScrollBars = flexScrollBarNone
        // Else
        // vsSearch.Height = (Me.Height - vsSearch.Top) - 1000
        // vsSearch.ScrollBars = flexScrollBarVertical
        // End If
        // End If
        // End Sub
        // 
        // Private Sub vsSearch_BeforeSort(ByVal Col As Long, Order As Integer)
        // If Col = lngColLocation Or Col = lngColLocationNumber Then
        // If vsSearch.rows > 1 Then
        // vsSearch.Select 1, lngColLocationNumber, 1, lngColLocation
        // End If
        // End If
        // End Sub
        // 
        // Private Sub vsSearch_Click()
        // txtGetAccountNumber.Text = Left$(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6)
        // End Sub
        // 
        // Private Sub vsSearch_DblClick()
        // If vsSearch.Row > 0 Then
        // txtGetAccountNumber.Text = vsSearch.TextMatrix(vsSearch.Row, lngColAcct)
        // cmdGetAccountNumber_Click
        // End If
        // End Sub
        // 
        // Private Sub vsSearch_KeyDown(KeyCode As Integer, Shift As Integer)
        // captures the return key to accept the account highlighted in the listbox
        // If KeyCode = vbKeyReturn Then
        // cmdGetAccountNumber_Click
        // End If
        // End Sub

        private void mnuBatchPurge_Click(object sender, System.EventArgs e)
        {
            // this will delete a Batch from the BatchRecover table

            // set tag to (P)urge
            fraRecover.Tag = "P"; // purge
            cmdBatchChoice.Text = "Purge";
            lblRecover.Text = "Choose the Batch that you would like to purge.";

            // this will refresh the combo list
            if (FillBatchCombo())
            {
                // this will clear all the other frames out
                ShowBatchQuestions();
                fraBatchQuestions.Visible = false; // hide the questions frame

                fraRecover.Left = FCConvert.ToInt32((this.Width - fraRecover.Width) / 2.0);
                fraRecover.Top = FCConvert.ToInt32((this.Height - fraRecover.Height) / 3.0);
                fraRecover.Visible = true;
            }
        }

        private void mnuBatchRecover_Click(object sender, System.EventArgs e)
        {
            // this will recover a batch from the BatchRecover table

            // set tag to (R)ecover
            fraRecover.Tag = "R"; // recover
            cmdBatchChoice.Text = "Recover";
            lblRecover.Text = "Choose the Batch that you would like to recover.";

            // this will refresh the combo list
            FillBatchCombo();

            // this will clear all the other frames out
            ShowBatchQuestions();
            fraBatchQuestions.Visible = false; // hide the questions frame
                                               //FC:FINAL:BSE:#4286 frame should be moved up
            fraRecover.Left = FCConvert.ToInt32((this.Width - fraRecover.Width) / 26.0);
            fraRecover.Top = FCConvert.ToInt32((this.Height - fraRecover.Height) / 20.0);
            fraRecover.Visible = true;
        }

        private void mnuBatchSave_Click(object sender, System.EventArgs e)
        {
            // this will save the information that has been put into the grid, into the BatchRecover table
            vsBatch.Rows = 1;
            Close();
        }

        private void mnuBatchStart_Click(object sender, System.EventArgs e)
        {
            AttemptToStartUTBatch();
            FormatBatchSearchGrid(true);
        }

        public void AttemptToStartUTBatch()
        {
            // first check to see if a batch has already been added to the last receipt
            if (!modExtraModules.AlreadyABatch(frmReceiptInput.InstancePtr))
            {
                if (vsBatch.Rows <= 1)
                {
                    // this will pop up a form that the user can input a batch update
                    ShowBatchQuestions();
                    mnuFileSearch.Visible = false;
                }
                else
                {
                    // clear out the rows (still saved in the table) and start a new batch
                    vsBatch.Rows = 1;
                    // clear out the batch info too
                    txtPaidBy.Text = "";
                    txtTellerID.Text = "";
                    // txtPaymentDate.Text = ""
                    txtPaymentDate2.Text = ""; // set this with a default
                }
            }
            else
            {
                MessageBox.Show("You can only have one batch update on each receipt.", "Multiple Batch Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void cmdValidateCancel_Click(object sender, System.EventArgs e)
        {
            LockBatch_2(false); // this will enable the boxes again
        }

        private void cmdValidateNo_Click(object sender, System.EventArgs e)
        {
            LockBatch_2(false); // this will enable the boxes again
        }

        private void cmdValidateYes_Click(object sender, System.EventArgs e)
        {
            LockBatch_2(false); // this will enable the boxes again
            ShowBatch();
            fraBatchQuestions.Visible = false;
            // XXXXXX    mnuFileImport.Enabled = True
            mnuFileImportPayPortBatch.Enabled = true;
        }


        private void txtAmount_Enter(object sender, System.EventArgs e)
        {
            txtAmount.SelectionStart = 0;
            txtAmount.SelectionLength = txtAmount.Text.Length;
        }

        private void txtAmount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            int KeyCode = FCConvert.ToInt32(e.KeyCode);
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if ((KeyCode >= FCConvert.ToInt32(Keys.D0)) && KeyCode <= FCConvert.ToInt32(Keys.D9) || (KeyCode == FCConvert.ToInt32(Keys.Divide)) || (KeyCode == FCConvert.ToInt32(Keys.Back)) || (KeyCode == FCConvert.ToInt32(Keys.Delete)) || (KeyCode == FCConvert.ToInt32(Keys.Right)))
            {
            }
            else if (KeyCode == FCConvert.ToInt32(Keys.Left))
            {
                if (txtAmount.SelectionStart == 0)
                {
                    txtBatchAccount.Focus();
                }
            }
            else if (KeyCode == FCConvert.ToInt32(Keys.Return))
            {
                if (Conversion.Val(txtAmount.Text) != 0)
                {
                    if (Conversion.Val(txtBatchAccount.Text) != 0)
                    {
                        if (MessageBox.Show("Would you like to save this payment?", "Save Payment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            // If txtAmount.DataChanged Then
                            if (Conversion.Val(txtAmount.Text) != 0)
                            {
                                if (AdjustPaymentAmount_8(FCConvert.ToDouble(txtAmount.Text)) != 0)
                                {
                                    KeyCode = 0;
                                    return; // error has occured, exit sub and do not save payment
                                }
                            }
                            else
                            {
                                MessageBox.Show("Payment must be numeric and greater than 0.", "Invalid Payment Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                            // End If
                            AddPaymentToBatch(rsBatch, FCConvert.ToInt32(FCConvert.ToDouble(txtBatchAccount.Text)));
                        }
                        else
                        {
                            txtBatchAccount.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("The account entered cannot have a value of zero.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtBatchAccount.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("The payment entered cannot be zero.", "Invalid Payment Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtAmount.Focus();
                }
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtBatchAccount_Enter(object sender, System.EventArgs e)
        {
            txtBatchAccount.SelectionStart = 0;
            txtBatchAccount.SelectionLength = txtBatchAccount.Text.Length;
        }

        private void txtBatchAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            int KeyCode = FCConvert.ToInt32(e.KeyCode);
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            bool boolMove = false;
            if ((KeyCode >= FCConvert.ToInt32(Keys.D0)) && KeyCode <= FCConvert.ToInt32(Keys.D9) || (KeyCode == FCConvert.ToInt32(Keys.Divide)) || (KeyCode == FCConvert.ToInt32(Keys.Back)) || (KeyCode == FCConvert.ToInt32(Keys.Delete)) || (KeyCode == FCConvert.ToInt32(Keys.Left)))
            {
            }
            else if (KeyCode == FCConvert.ToInt32(Keys.Right))
            {
                if (txtBatchAccount.SelectionStart == txtBatchAccount.Text.Length || txtBatchAccount.SelectionStart == 0 && txtBatchAccount.SelectionLength == txtBatchAccount.Text.Length)
                {
                    txtBatchAccount_Validate(ref boolMove);
                    if (boolMove == false)
                    {
                        txtAmount.Focus();
                    }
                }
            }
            else if (KeyCode == FCConvert.ToInt32(Keys.Return))
            {
                txtBatchAccount_Validate(ref boolMove);
                if (boolMove == false)
                {
                    txtAmount.Focus();
                }
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtBatchAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back))
            {
            }
            else
            {
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtBatchAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // this sub will check to make sure that this is a valid account and if so, will calculate
            // the current Principal, Tax, Interest and Costs and display them as default in the Amount box
            int lngAcct;
            double dblTemp = 0;
            int lngBillKey;
            double dblCurrInt = 0;
            bool boolPrevBill = false;

            string strService = "";
            int lngBillNum;

            // during the validate, this will get account information and display it for the user
            if (cmbSewer.Text == "Sewer")
            {
                strService = "S";
            }
            else
            {
                strService = "W";
            }
            if (txtBatchAccount.Text == "")
            {
                return;
            }
            lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtBatchAccount.Text)));
            lngBillNum = FCConvert.ToInt32(Math.Round(Conversion.Val(txtBillNumber.Text)));

            // make sure that only one payment per service can be in the batch from each account
            if (vsBatch.FindRow(lngAcct, -1, 1) == -1)
            {
                // reset the totals
                dblCurPrin = 0;
                dblCurTax = 0;
                dblCurInt = 0;
                dblPLInt = 0;
                dblCurCost = 0;
                dblTotalDue = 0;
                boolPrevBill = false;
                if (lngAcct != 0)
                {
                    // find the account
                    rsBatch.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(lngAcct) + " AND (Service = '" + strService + "' OR Service = 'B') ORDER BY BillNumber DESC", modExtraModules.strUTDatabase);
                    if (!rsBatch.EndOfFile() && !rsBatch.BeginningOfFile())
                    {
                        rsBatch.FindFirstRecord("BillNumber", lngBillNum);
                        if (rsBatch.NoMatch)
                        {
                            if (!boolBatchImport)
                            {
                                MessageBox.Show("Bill for account " + FCConvert.ToString(lngAcct) + " not found.  Please mark this account and add it at the end of the batch.", "Missing Bill Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            txtBatchAccount.Text = "";
                            txtAmount.Text = "";
                            txtBillNumber.Text = "";
                            e.Cancel = true;
                        }
                        else
                        {
                            lngBillKey = rsBatch.Get_Fields("ID");
                        TRYAGAIN:;
                            // kk02262018 troges-88  Rewrite the FindPreviousRecord as a query
                            // rsBatch.FindPreviousRecord , , "BillNumber < " & lngBillNum & " AND (" & strService & "PrinOwed - " & strService & "PrinPaid) > 0"
                            // If Not rsBatch.NoMatch Then
                            rsBatch.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(lngAcct) + " AND " + "(Service = '" + strService + "' OR Service = 'B') AND (" + strService + "PrinOwed - " + strService + "PrinPaid) > 0", modExtraModules.strUTDatabase);
                            if (!rsBatch.EndOfFile())
                            {
                                // XXXXX NOT DO ANYTHING WITH LIENS IN THE BATCH PROCESS
                                // If rsBatch.Fields("LienRecordNumber") <> 0 Then
                                // rsBLN.OpenRecordset "SELECT * FROM LienRec WHERE LienRecordNumber = " & rsBatch.Fields("LienRecordNumber"), strUTDatabase
                                // If CalculateAccountCLLien(rsBLN, dtEffectiveDate, dblCurrInt) = 0 Then
                                // GoTo TRYAGAIN
                                // Else
                                // boolPrevBill = True
                                // End If
                                // Else
                                // XXXXX

                                // XXXXXXXXXXXXXXXXXXXXX TODO: CHECK THESE PARAMETERS XXXXXXXXXXXXXXXXXXXXXXXXXXX
                                // XXXXXXXXXXXXX If CalculateAccountUT(rsBatch, rsBatch.Fields("Account"), dtEffectiveDate, dblCurrInt, , , , , , , , , , , , intPer) = 0 Then
                                if (modUTCalculations.CalculateAccountUT(rsBatch, dtEffectiveDate, ref dblCurrInt, FCConvert.CBool(strService == "W")) == 0)
                                {
                                    goto TRYAGAIN;
                                }
                                else
                                {
                                    boolPrevBill = true;
                                }
                                // End If
                                if (boolPrevBill && !boolBatchImport)
                                {
                                    // there are previous bills with balances
                                    if (MessageBox.Show("There are previous bill(s) with account balances.  Would you like to continue adding payment?", "Previous Account Balance", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                                    {
                                        modGlobalFunctions.AddCYAEntry_6("CR", "Batch payment with previous balances. - Acct: " + txtBatchAccount.Text);
                                    }
                                    else
                                    {
                                        e.Cancel = false;
                                        return;
                                    }
                                }
                            }
                            // this will put the recordset back to the original bill
                            // kk02262018 troges-88  Change the FindFirstRecord to a query to reload the Bill
                            // rsBatch.FindFirstRecord , , "ID = " & lngBillKey
                            rsBatch.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(lngAcct) + " AND (Service = '" + strService + "' OR Service = 'B') ORDER BY BillNumber DESC", modExtraModules.strUTDatabase);
                            // this will move the labels to right right of the amount box
                            // lblName.Left = txtAmount.Left + txtAmount.Width + 200
                            // lblLocation.Left = lblName.Left
                            // fill the labels with the account information
                            lblName.Text = "Name: " + Strings.Trim(rsBatch.Get_Fields("BName") + " ");
                            lblLocation.Text = "Location: " + Strings.Trim(rsBatch.Get_Fields("Location") + " ");
                            // XXXX NOT DEALING WITH LIENS AT THIS TIME
                            // If rsBatch.Fields("LienRecordNumber") = 0 Then
                            // XXXXXXXX                        dblOriginalAmount = CalculateAccountCL(rsBatch, lngAcct, dtEffectiveDate, dblTemp, dblCurPrin, dblCurInt, dblCurCost, , , , , , , , , intPer)
                            var tempDate = DateTime.Now;
                            dblOriginalAmount = modUTCalculations.CalculateAccountUT(rsBatch, dtEffectiveDate, ref dblTemp, FCConvert.CBool(strService == "W"), ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, ref tempDate, ref dblCurTax);
                            dblPLInt = 0;
                            txtAmount.Text = Strings.Format(dblOriginalAmount, "#,##0.00");
                            // XXXX                    Else
                            // Set rsBatchLien = New clsDataConnection
                            // rsBatchLien.OpenRecordset "SELECT * FROM LienRec WHERE LienRecordNumber = " & rsBatch.Fields("LienRecordNumber"), strUTDatabase
                            // If rsBatchLien.EndOfFile <> True And rsBatchLien.BeginningOfFile <> True Then
                            // dblOriginalAmount = CalculateAccountCLLien(rsBatchLien, dtEffectiveDate, dblTemp, dblCurPrin, dblCurInt, dblCurCost)
                            // txtAmount.Text = Format(dblOriginalAmount, "#,##0.00")
                            // dblPLInt = Round(dblOriginalAmount - (dblCurPrin + dblCurInt + dblCurCost), 2)
                            // Else
                            // MsgBox "This account has gone to Lien and the lien record was not found.", vbCritical, "Account Validation"
                            // txtBatchAccount.Text = ""
                            // txtAmount.Text = ""
                            // End If
                            // XXXX                    End If
                            if (Conversion.Val(txtAmount.Text) == 0)
                            {
                                dblTotalDue = 0;
                            }
                            else
                            {
                                dblTotalDue = FCConvert.ToDouble(txtAmount.Text);
                            }
                        }
                    }
                    else
                    {
                        if (!boolBatchImport)
                        {
                            MessageBox.Show("Please enter a valid account number.", "Account Validation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        txtBatchAccount.Text = "";
                        txtAmount.Text = "";
                        txtBillNumber.Text = "";
                        txtBatchAccount.Focus();
                    }
                }
                else
                {
                    if (!boolBatchImport)
                    {
                        MessageBox.Show("Please enter a valid account number.", "Account Validation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    txtBatchAccount.Text = "";
                    txtAmount.Text = "";
                    txtBillNumber.Text = "";
                    txtBatchAccount.Focus();
                }
            }
            else
            {
                if (!boolBatchImport)
                {
                    MessageBox.Show("Please enter an account number that has not been entered already or delete the current payment for this account.", "Multiple Payments", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                txtBatchAccount.Text = "";
                txtAmount.Text = "";
                txtBillNumber.Text = "";
            }
        }
        public void txtBatchAccount_Validate(ref bool Cancel)
        {
            txtBatchAccount_Validating(txtBatchAccount, new System.ComponentModel.CancelEventArgs(Cancel));
        }

        private void txtGetAccountNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            int KeyCode = FCConvert.ToInt32(e.KeyCode);
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);

            if (KeyCode == FCConvert.ToInt32(Keys.Return))
            {
                KeyCode = 0;
                // cmdGetAccountNumber_Click
            }
        }

        private void txtPaidBy_Enter(object sender, EventArgs e)
        {
            txtPaidBy.SelectionStart = 0;
            txtPaidBy.SelectionLength = txtPaidBy.Text.Length;
        }

        private void txtPaidBy_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            int KeyCode = FCConvert.ToInt32(e.KeyCode);
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);


            if ((KeyCode >= FCConvert.ToInt32(Keys.D0)) && KeyCode <= FCConvert.ToInt32(Keys.D9) || (KeyCode == FCConvert.ToInt32(Keys.Divide)) || (KeyCode == FCConvert.ToInt32(Keys.Back)) || (KeyCode == FCConvert.ToInt32(Keys.Delete)) || (KeyCode == FCConvert.ToInt32(Keys.Left)) || (KeyCode == FCConvert.ToInt32(Keys.Right)))
            {
            }
            else if ((KeyCode == FCConvert.ToInt32(Keys.Return)) || (KeyCode == FCConvert.ToInt32(Keys.Down)))
            {
                KeyCode = 0;
                cmdBatch.Focus();
            }
            else if (KeyCode == FCConvert.ToInt32(Keys.Up))
            {
                KeyCode = 0;
                txtEffectiveDate.Focus();
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtPaymentDate_Enter(object sender, EventArgs e)
        {
            txtPaymentDate.SelectionStart = 0;
            txtPaymentDate.SelectionLength = txtPaymentDate.Text.Length;
        }

        private void txtPaymentDate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            int KeyCode = FCConvert.ToInt32(e.KeyCode);
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if ((KeyCode >= FCConvert.ToInt32(Keys.D0)) && KeyCode <= FCConvert.ToInt32(Keys.D9) || (KeyCode == FCConvert.ToInt32(Keys.Divide)) || (KeyCode == FCConvert.ToInt32(Keys.Back)) || (KeyCode == FCConvert.ToInt32(Keys.Delete)) || (KeyCode == FCConvert.ToInt32(Keys.Left)) || (KeyCode == FCConvert.ToInt32(Keys.Right)))
            {
            }
            else if ((KeyCode == FCConvert.ToInt32(Keys.Return)) || (KeyCode == FCConvert.ToInt32(Keys.Down)))
            {
                KeyCode = 0;
                txtEffectiveDate.Focus();
            }
            else if (KeyCode == FCConvert.ToInt32(Keys.Up))
            {
                KeyCode = 0;
                txtTellerID.Focus();
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtPaymentDate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if ((KeyAscii >= Keys.Help && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back))
            {
            }
            else
            {
                KeyAscii = (Keys)0;
            }

            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void txtPaymentDate2_GotFocus(object sender, EventArgs e)
        {
            txtPaymentDate2.SelStart = 0;
            txtPaymentDate2.SelLength = txtPaymentDate2.Text.Length;
        }

        private void txtPaymentDate2_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            int KeyCode = FCConvert.ToInt32(e.KeyCode);
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if ((KeyCode >= FCConvert.ToInt32(Keys.D0)) && KeyCode <= FCConvert.ToInt32(Keys.D9) || (KeyCode == FCConvert.ToInt32(Keys.Divide)) || (KeyCode == FCConvert.ToInt32(Keys.Back)) || (KeyCode == FCConvert.ToInt32(Keys.Delete)) || (KeyCode == FCConvert.ToInt32(Keys.Left)) || (KeyCode == FCConvert.ToInt32(Keys.Right)))
            {
            }
            else if ((KeyCode == FCConvert.ToInt32(Keys.Return)) || (KeyCode == FCConvert.ToInt32(Keys.Down)))
            {
                KeyCode = 0;
                txtEffectiveDate.Focus();
            }
            else if (KeyCode == FCConvert.ToInt32(Keys.Up))
            {
                KeyCode = 0;
                txtTellerID.Focus();
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtPaymentDate2_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            if ((KeyAscii >= 47 && KeyAscii <= 57) || (KeyAscii == 8))
            {
            }
            else
            {
                KeyAscii = 0;
            }
        }

        private void txtEffectiveDate_GotFocus(object sender, EventArgs e)
        {
            txtEffectiveDate.SelStart = 0;
            txtEffectiveDate.SelLength = txtEffectiveDate.Text.Length;
        }

        private void txtEffectiveDate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            int KeyCode = FCConvert.ToInt32(e.KeyCode);
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if ((KeyCode >= FCConvert.ToInt32(Keys.D0)) && KeyCode <= FCConvert.ToInt32(Keys.D9) || (KeyCode == FCConvert.ToInt32(Keys.Divide)) || (KeyCode == FCConvert.ToInt32(Keys.Back)) || (KeyCode == FCConvert.ToInt32(Keys.Delete)) || (KeyCode == FCConvert.ToInt32(Keys.Left)) || (KeyCode == FCConvert.ToInt32(Keys.Right)))
            {
            }
            else if ((KeyCode == FCConvert.ToInt32(Keys.Return)) || (KeyCode == FCConvert.ToInt32(Keys.Down)))
            {
                KeyCode = 0;
                txtPaidBy.Focus();
            }
            else if (KeyCode == FCConvert.ToInt32(Keys.Up))
            {
                KeyCode = 0;
                txtPaymentDate2.Focus();
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtEffectiveDate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            if ((KeyAscii >= 47 && KeyAscii <= 57) || (KeyAscii == 8))
            {
            }
            else
            {
                KeyAscii = 0;
            }
        }

        private void txtSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            int KeyCode = FCConvert.ToInt32(e.KeyCode);
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == FCConvert.ToInt32(Keys.Return))
            {
                KeyCode = 0;
                cmdSearch_Click();
            }
        }

        private void ShowSearch()
        {
            // this will align the Search Screen and Show it
            fraBatch.Visible = false;
            fraBatchQuestions.Visible = false;
            fraRecover.Visible = false;

            boolSearch = true;
        }

        public void ShowBatchQuestions()
        {
            // this will align the Batch Questions Frame and Show it
            fraBatch.Visible = false;
            // xxxxxx   fraSearch.Visible = False
            fraCriteria.Visible = false;
            fraRecover.Visible = false;
            vsSearch.Visible = false;
            // xxxxxxx  lblSearchListInstruction.Visible = False
            lblSearchInfo.Visible = false;
            mnuFileClearSearch.Visible = false;

            //FC:FINAL:BSE #3548  frame should be aligned to the left-margin
            fraBatchQuestions.Left = FCConvert.ToInt32((this.Width - fraBatchQuestions.Width) / 17.0);
            fraBatchQuestions.Top = FCConvert.ToInt32((this.Height - fraBatchQuestions.Height) / 18.0);
            fraBatchQuestions.Visible = true;
            // set the defalts for the new batch
            // txtPaymentDate2.Text = Date        'no default

        }

        private void ShowBatch()
        {
            int lngRW = 0;
            // this will align the Search Screen and Show it
            FormatBatchGrid();
            this.Text = "Batch";
            rsBatchBackup.OpenRecordset("SELECT * FROM BatchRecover WHERE TellerID = '" + strTLRID + "' AND PaidBy = '" + strPaidBy + "' AND BatchRecoverDate = '" + FCConvert.ToString(dtPaymentDate) + "' AND ETDate = '" + FCConvert.ToString(dtEffectiveDate) + "'", modExtraModules.strUTDatabase);
            if (!rsBatchBackup.EndOfFile() && !rsBatchBackup.BeginningOfFile())
            { // if there are already records out there
                if (MessageBox.Show("There are already some files saved in the recovery with this Teller ID and Paid By.  These may be pending transactions, would you like to load them?  If you do not, they will be removed.", "Recover Previous Batch?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    do
                    {
                        vsBatch.AddItem("");
                        lngRW = vsBatch.Rows - 1;
                        vsBatch.TextMatrix(lngRW, lngColBatchAcct, rsBatchBackup.Get_Fields("AccountNumber"));
                        vsBatch.TextMatrix(lngRW, lngColBatchName, rsBatchBackup.Get_Fields("Name"));
                        vsBatch.TextMatrix(lngRW, lngColBatchBill, rsBatchBackup.Get_Fields("BillNumber"));
                        vsBatch.TextMatrix(lngRW, lngColBatchService, rsBatchBackup.Get_Fields("Service"));
                        vsBatch.TextMatrix(lngRW, lngColBatchRef, rsBatchBackup.Get_Fields("Ref"));
                        vsBatch.TextMatrix(lngRW, lngColBatchPrin, Strings.Format(rsBatchBackup.Get_Fields("Prin"), "#,##0.00"));
                        vsBatch.TextMatrix(lngRW, lngColBatchTax, Strings.Format(rsBatchBackup.Get_Fields("Tax"), "#,##0.00"));
                        vsBatch.TextMatrix(lngRW, lngColBatchInt, Strings.Format(rsBatchBackup.Get_Fields("Int"), "#,##0.00"));
                        vsBatch.TextMatrix(lngRW, lngColBatchCost, Strings.Format(rsBatchBackup.Get_Fields("Cost"), "#,##0.00"));
                        vsBatch.TextMatrix(lngRW, lngColBatchTotal, FCConvert.ToDouble(vsBatch.TextMatrix(lngRW, lngColBatchPrin)) + FCConvert.ToDouble(vsBatch.TextMatrix(lngRW, lngColBatchTax)) + FCConvert.ToDouble(vsBatch.TextMatrix(lngRW, lngColBatchInt)) + FCConvert.ToDouble(vsBatch.TextMatrix(lngRW, lngColBatchCost)));
                        vsBatch.TextMatrix(lngRW, lngColBatchKey, rsBatchBackup.Get_Fields("ID"));
                        if (FCConvert.CBool(rsBatchBackup.Get_Fields("PayCorrect")))
                        {
                            vsBatch.TextMatrix(lngRW, lngColBatchCorrectAmount, "Yes");
                        }
                        else
                        {
                            vsBatch.TextMatrix(lngRW, lngColBatchCorrectAmount, "No");
                        }
                        rsBatchBackup.MoveNext();
                    } while (!rsBatchBackup.EndOfFile());
                    // total the payments
                    txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");
                    mnuBatchSave.Enabled = true;
                    mnuBatchRecover.Enabled = false;
                    // XXXX mnuFileImport.Enabled = False
                    mnuFileImportPayPortBatch.Enabled = false;
                    rsBatchBackup.MoveFirst();
                }
                else
                {
                    do
                    {
                        rsBatchBackup.Delete();
                        rsBatchBackup.MoveNext();
                    } while (!rsBatchBackup.EndOfFile());
                }
            }
            fraCriteria.Visible = false;
            fraBatchQuestions.Visible = false;
            fraRecover.Visible = false;

            fraBatch.Left = FCConvert.ToInt32((this.Width - fraBatch.Width) / 2.0);
            fraBatch.Top = FCConvert.ToInt32((this.Height - fraBatch.Height) / 3.0);
            fraBatch.Visible = true;
        }

        private bool FillBatchCombo()
        {
            bool FillBatchCombo = false;
            clsDRWrapper rsBatchList = new clsDRWrapper();
            cmbBatchList.Items.Clear();
            rsBatchList.OpenRecordset("SELECT DISTINCT TellerID, BatchRecoverDate, PaidBy FROM BatchRecover", modExtraModules.strUTDatabase);
            if (!rsBatchList.EndOfFile() && !rsBatchList.BeginningOfFile())
            {
                do
                {
                    cmbBatchList.Items.Add(modGlobalFunctions.PadStringWithSpaces(FCConvert.ToString(rsBatchList.Get_Fields("BatchRecoverDate")), 10) + " " + rsBatchList.Get_Fields("TellerID") + " " + rsBatchList.Get_Fields("PaidBy"));
                    rsBatchList.MoveNext();
                } while (!rsBatchList.EndOfFile());
                if (cmbBatchList.Items.Count > 0)
                {
                    cmbBatchList.SelectedIndex = 0;
                }
                FillBatchCombo = true;
            }
            else
            {
                MessageBox.Show("No batch found.", "Empty Batch Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
                // mnuBatchRecover.Enabled = False
                // mnuBatchPurge.Enabled = False
                FillBatchCombo = false;
            }
            return FillBatchCombo;
        }

        private void FormatBatchGrid()
        {
            int lngWid = 0;
            vsBatch.Cols = 13;
            lngWid = vsBatch.WidthOriginal;
            // width of each column
            vsBatch.ColWidth(lngColBatchAcct, FCConvert.ToInt32(lngWid * 0.05)); // Account
            vsBatch.ColWidth(lngColBatchName, FCConvert.ToInt32(lngWid * 0.14)); // Name
            vsBatch.ColWidth(lngColBatchBill, FCConvert.ToInt32(lngWid * 0.05)); // BillNumber
            vsBatch.ColWidth(lngColBatchService, FCConvert.ToInt32(lngWid * 0.05)); // Service
            vsBatch.ColWidth(lngColBatchRef, FCConvert.ToInt32(lngWid * 0.1)); // Ref
            vsBatch.ColWidth(lngColBatchPrin, FCConvert.ToInt32(lngWid * 0.1)); // Principal
            vsBatch.ColWidth(lngColBatchTax, FCConvert.ToInt32(lngWid * 0.1)); // Tax
            vsBatch.ColWidth(lngColBatchInt, FCConvert.ToInt32(lngWid * 0.1)); // Interest
            vsBatch.ColWidth(lngColBatchCost, FCConvert.ToInt32(lngWid * 0.1)); // Costs
            vsBatch.ColWidth(lngColBatchTotal, FCConvert.ToInt32(lngWid * 0.12)); // Total
            vsBatch.ColWidth(lngColBatchKey, 0);
            vsBatch.ColWidth(lngColBatchPrintReceipt, FCConvert.ToInt32(lngWid * 0.04)); // Print Receipt
            vsBatch.ColWidth(lngColBatchCorrectAmount, FCConvert.ToInt32(lngWid * 0.04)); // Correct Amount
                                                                                          // Headers
            vsBatch.TextMatrix(0, lngColBatchAcct, "Acct");
            vsBatch.TextMatrix(0, lngColBatchName, "Name");
            vsBatch.TextMatrix(0, lngColBatchBill, "Bill");
            vsBatch.TextMatrix(0, lngColBatchService, "Service");
            vsBatch.TextMatrix(0, lngColBatchRef, "Ref");
            vsBatch.TextMatrix(0, lngColBatchPrin, "Principal");
            vsBatch.TextMatrix(0, lngColBatchTax, "Tax");
            vsBatch.TextMatrix(0, lngColBatchInt, "Interest");
            vsBatch.TextMatrix(0, lngColBatchCost, "Cost");
            vsBatch.TextMatrix(0, lngColBatchTotal, "Total");
            vsBatch.TextMatrix(0, lngColBatchKey, "");
            vsBatch.TextMatrix(0, lngColBatchPrintReceipt, "Rct");
            vsBatch.TextMatrix(0, lngColBatchCorrectAmount, "Auto");
            vsBatch.ColAlignment(lngColBatchAcct, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsBatch.ColAlignment(lngColBatchBill, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            // Alignment and formating for the currency
            vsBatch.ColAlignment(lngColBatchPrin, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBatch.ColAlignment(lngColBatchTax, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBatch.ColAlignment(lngColBatchInt, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBatch.ColAlignment(lngColBatchCost, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBatch.ColAlignment(lngColBatchTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBatch.ColFormat(lngColBatchPrin, "#,##0.00");
            vsBatch.ColFormat(lngColBatchTax, "#,##0.00");
            vsBatch.ColFormat(lngColBatchInt, "#,##0.00");
            vsBatch.ColFormat(lngColBatchCost, "#,##0.00");
            vsBatch.ColFormat(lngColBatchTotal, "#,##0.00");
            vsBatch.ColDataType(lngColBatchPrintReceipt, FCGrid.DataTypeSettings.flexDTBoolean);
            //vsBatch.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsBatch.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
        }

        private void ProcessBatch()
        {
            // this will go through each payment and add the correct payments to the
            // UT database and the correct receipt records to the CR database
            // this will cycle through the grid and find the total
            int lngCT;
            int lngRowCount;
            double dblSum;
            bool blnPrint;
            UpdatePrintReceiptChoice();
            lngRowCount = vsBatch.Rows - 1;
            dblSum = 0;
            blnPrint = false;
            for (lngCT = 1; lngCT <= lngRowCount; lngCT++)
            {
                dblSum += FCConvert.ToDouble(vsBatch.TextMatrix(lngCT, 7)); // Summing the Tax (8 is the costs in CL) column?  And then doing nothing with it. Take this out XXXXXXXXXXXX  TODO
                if (Strings.Trim(vsBatch.TextMatrix(lngCT, lngColBatchPrintReceipt)) != "")
                {
                    //FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
                    //if (FCConvert.ToBoolean(vsBatch.TextMatrix(lngCT, lngColBatchPrintReceipt)) == true)
                    if (FCConvert.CBool(vsBatch.TextMatrix(lngCT, lngColBatchPrintReceipt)) == true)
                    {
                        blnPrint = true;
                    }
                }
            }
            if (lngRowCount > 0)
            {
                // Create Payment Records in UT
                if (CreateBatchUTRecords())
                { // this will create all of the payment records for the Batch Update
                    if (modExtraModules.IsThisCR())
                    { // If there is CR then send the info back there
                      //FC:TODO:DDU!!!!!!CreateUTBatchReceiptEntry(blnPrint);
                    }

                    MessageBox.Show("Batch Update Successful.", "Batch Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    // clear the grid
                    vsBatch.Rows = 1;
                    // clear the BatchRecover table
                    // PurgeRecords PadStringWithSpaces(CStr(dtPaymentDate), 10) & " " & strTLRID & " " & strPaidBy
                    Close();
                }
            }
        }

        private void txtTellerID_Enter(object sender, System.EventArgs e)
        {
            txtTellerID.SelectionStart = 0;
            txtTellerID.SelectionLength = txtTellerID.Text.Length;
        }

        private void txtTellerID_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            int KeyCode = FCConvert.ToInt32(e.KeyCode);
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if ((KeyCode >= FCConvert.ToInt32(Keys.A)) && KeyCode <= FCConvert.ToInt32(Keys.Z) || (KeyCode == FCConvert.ToInt32(Keys.Delete)) || (KeyCode == FCConvert.ToInt32(Keys.Back)) || (KeyCode == FCConvert.ToInt32(Keys.Left)) || (KeyCode == FCConvert.ToInt32(Keys.Right)) || (KeyCode >= FCConvert.ToInt32(Keys.D0)) && KeyCode <= FCConvert.ToInt32(Keys.D9))
            {
            }
            else if ((KeyCode == FCConvert.ToInt32(Keys.Return)) || (KeyCode == FCConvert.ToInt32(Keys.Down)))
            {
                KeyCode = 0;
                txtPaymentDate2.Focus();
            }
            else if (KeyCode == FCConvert.ToInt32(Keys.Up))
            {
                KeyCode = 0;
                chkReceipt.Focus();
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtTellerID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            if ((KeyAscii >= FCConvert.ToInt32(Keys.A)) && KeyAscii <= FCConvert.ToInt32(Keys.Z) || (KeyAscii == FCConvert.ToInt32(Keys.Back)) || (KeyAscii >= FCConvert.ToInt32(Keys.D0)) && KeyAscii <= FCConvert.ToInt32(Keys.D9))
            {
            }
            else if (KeyAscii >= FCConvert.ToInt32(Keys.NumPad1) && KeyAscii <= FCConvert.ToInt32(Keys.F11))
            {
                KeyAscii = KeyAscii - 32;
            }
            else
            {
                KeyAscii = 0;
            }

            e.KeyChar = Strings.Chr(KeyAscii);
        }

        // VBto upgrade warning: dblNewAmount As double	OnWrite(double, string, short)
        private short AdjustPaymentAmount_8(double dblNewAmount, bool boolDoNotQueryOverPay = false) { return AdjustPaymentAmount(ref dblNewAmount, boolDoNotQueryOverPay); }
        private short AdjustPaymentAmount(ref double dblNewAmount, bool boolDoNotQueryOverPay = false)
        {
            short AdjustPaymentAmount = 0;
            try
            {   // On Error GoTo ERROR_HANDLER
                // this sub will adjust the payment amounts if the total amount is changed
                // it will affect dblCurPrin, dblCurTax, dblCurInt, dblCurCost

                if (dblNewAmount >= dblCurInt + dblPLInt)
                {
                    dblNewAmount -= (dblCurInt + dblPLInt);

                    if (dblNewAmount >= dblCurCost)
                    {
                        dblNewAmount -= dblCurCost;

                        if (dblNewAmount >= dblCurTax)
                        {
                            dblNewAmount = FCConvert.ToDouble(Strings.Format(dblNewAmount - dblCurTax, "0.00"));

                            if (dblNewAmount >= dblCurPrin)
                            {
                                dblNewAmount = FCConvert.ToDouble(Strings.Format(dblNewAmount - dblCurPrin, "0.00"));

                            }
                            else
                            { // less than interest, costs, tax and principal
                                dblCurPrin = dblNewAmount;
                                dblNewAmount = 0;
                            }
                        }
                        else
                        { // less than interest, costs and tax
                            dblCurPrin = 0;
                            dblCurTax = dblNewAmount;
                            dblNewAmount = 0;
                        }

                    }
                    else
                    { // less than interest and costs
                        dblCurPrin = 0;
                        dblCurTax = 0;
                        dblCurCost = dblNewAmount;
                        dblNewAmount = 0;
                    }

                }
                else if (dblNewAmount >= dblCurInt)
                { // less than interest
                    dblNewAmount -= dblCurInt;
                    dblPLInt = dblNewAmount;
                    dblNewAmount = 0;
                }
                else
                {
                    dblCurInt = dblNewAmount;
                    dblPLInt = 0;
                    dblCurPrin = 0;
                    dblCurTax = 0;
                    dblCurCost = 0;
                    dblNewAmount = 0;
                }

                if (Math.Round(dblNewAmount, 2) > 0)
                { // if there is leftover, then the payment is too much
                    if (boolDoNotQueryOverPay)
                    {
                        dblCurPrin += dblNewAmount;
                    }
                    else
                    {
                        if (!boolBatchImport)
                        {
                            if (MessageBox.Show("This payment is more than owed, would you like to continue?", "Overpayment", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                dblCurPrin += dblNewAmount;
                            }
                            else
                            { // they do not want the overpayment
                                AdjustPaymentAmount = -1;
                            }
                        }
                        else
                        {
                            dblCurPrin += dblNewAmount;
                        }
                    }
                }
                return AdjustPaymentAmount;
            }
            catch
            {   
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", "Error Adjusting Payment Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return AdjustPaymentAmount;
        }

        private double CalculateGridTotal()
        {
            double CalculateGridTotal = 0;
            // this will cycle through the grid and find the total
            int lngCT;
            double dblSum;
            dblSum = 0;
            for (lngCT = 1; lngCT <= vsBatch.Rows - 1; lngCT++)
            {
                if (vsBatch.TextMatrix(lngCT, lngColBatchTotal) != "" && Conversion.Val(vsBatch.TextMatrix(lngCT, lngColBatchTotal)) != 0)
                {
                    dblSum += FCConvert.ToDouble(vsBatch.TextMatrix(lngCT, lngColBatchTotal));
                }
            }
            if (dblSum > 0)
            { // turns on the menu option when calculating the total
                mnuBatchSave.Enabled = true;
                mnuBatchRecover.Enabled = false;
            }
            CalculateGridTotal = dblSum;
            return CalculateGridTotal;
        }

        private void AddPaymentToBatch(clsDRWrapper rsTemp, int lngAcct)
        {
            int lngRow;
            int lngKey = 0;
            int lngTBillNumber = 0;

            // add it to the temporary table

            // .OpenRecordset "SELECT * FROM BatchRecover", strUTDatabase
            rsBatchBackup.AddNew();

            // set the key value from the batch recover table
            lngKey = rsBatchBackup.Get_Fields_Int32("ID");
            lngTBillNumber = rsBatch.Get_Fields_Int32("BillNumber");

            strRef = FCConvert.ToString(lngAcct) + "-" + FCConvert.ToString(lngTBillNumber);

            // add the values to the table
            rsBatchBackup.Set_Fields("TellerID", strTLRID);
            rsBatchBackup.Set_Fields("PaidBy", strPaidBy + " ");
            rsBatchBackup.Set_Fields("Ref", strRef + " ");
            rsBatchBackup.Set_Fields("AccountNumber", lngAcct);
            rsBatchBackup.Set_Fields("Name", Strings.Right(lblName.Text, lblName.Text.Length - 6));
            rsBatchBackup.Set_Fields("Prin", dblCurPrin);
            rsBatchBackup.Set_Fields("Tax", dblCurTax);
            rsBatchBackup.Set_Fields("Int", dblCurInt + dblPLInt);
            rsBatchBackup.Set_Fields("Cost", dblCurCost);
            rsBatchBackup.Set_Fields("BatchRecoverDate", dtPaymentDate);
            rsBatchBackup.Set_Fields("ETDate", dtEffectiveDate);
            // XXXXXXXX        .Fields("Service") = strSrvc
            // xxx        .Fields("Type") = " "

            // save the record
            if (!rsBatchBackup.Update(true))
            {
                MessageBox.Show("Error adding record batch.  Please try again.", "Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return;
            }
            vsBatch.AddItem("");
            lngRow = vsBatch.Rows - 1;
            // add it to the grid
            vsBatch.TextMatrix(lngRow, lngColBatchAcct, lngAcct);
            vsBatch.TextMatrix(lngRow, lngColBatchName, Strings.Right(lblName.Text, lblName.Text.Length - 6));
            vsBatch.TextMatrix(lngRow, lngColBatchBill, lngBill);
            // XXXXXXXXXXXXX        .TextMatrix(lngRow, lngColBatchService) = strSrvc
            vsBatch.TextMatrix(lngRow, lngColBatchRef, strRef);
            vsBatch.TextMatrix(lngRow, lngColBatchPrin, dblCurPrin);
            vsBatch.TextMatrix(lngRow, lngColBatchTax, dblCurTax);
            vsBatch.TextMatrix(lngRow, lngColBatchInt, dblCurInt + dblPLInt);
            vsBatch.TextMatrix(lngRow, lngColBatchCost, dblCurCost);
            vsBatch.TextMatrix(lngRow, lngColBatchTotal, dblCurPrin + dblCurInt + dblPLInt + dblCurCost);
            vsBatch.TextMatrix(lngRow, lngColBatchKey, lngKey);
            if (chkReceipt.CheckState == CheckState.Checked)
            {
                vsBatch.TextMatrix(lngRow, lngColBatchPrintReceipt, -1);
            }
            else
            {
                vsBatch.TextMatrix(lngRow, lngColBatchPrintReceipt, 0);
            }
            if (dblCurPrin + dblCurInt + dblPLInt + dblCurCost == dblOriginalAmount)
            {
                vsBatch.TextMatrix(lngRow, lngColBatchCorrectAmount, "Yes");
            }
            else
            {
                vsBatch.TextMatrix(lngRow, lngColBatchCorrectAmount, "No");
            }
            // total grid
            txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");
            // This will not allow an import after a row has already been added
            // XXXX mnuFileImport.Enabled = False
            mnuFileImportPayPortBatch.Enabled = false;
            // clear the payment boxes and setfocus to the account box
            txtAmount.Text = "";
            txtBatchAccount.Text = "";
            lblName.Text = "";
            lblLocation.Text = "";
            txtBatchAccount.Focus();
        }

        private void DeletePaymentFromBatch(int lngRow)
        {
            try
            {   // On Error GoTo ERROR_HANDLER
                int lngKey = 0;

                if (MessageBox.Show("Are you sure that you would like to delete this payment line.", "Delete Payment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    return;
                }

                // get the key number for the BatchRecover table
                if (vsBatch.TextMatrix(lngRow, lngColBatchKey) != "")
                {
                    lngKey = FCConvert.ToInt32(vsBatch.TextMatrix(lngRow, lngColBatchKey));

                    // delete payment from the database
                    rsBatchBackup.OpenRecordset("SELECT * FROM BatchRecover WHERE ID = " + FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
                    if (!rsBatchBackup.EndOfFile())
                    {
                        rsBatchBackup.Delete();
                        // If rsBatchBackup.Execute("DELETE FROM BatchRecover WHERE ID = " & lngKey, strUTDatabase) Then
                        // nothing
                    }
                    else
                    {
                        MessageBox.Show("Error Executing SQL Statement on removal of backup record.", "Deletion Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }

                    // delete this payment from the grid
                    vsBatch.RemoveItem(lngRow);

                    if (vsBatch.Rows == 1)
                    {
                        mnuBatchSave.Enabled = false;
                        mnuBatchRecover.Enabled = true;
                    }
                }
                else
                {
                    // empty row
                    // delete this line from the grid
                    vsBatch.RemoveItem(lngRow);
                }

                txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");
                return;
            }
            catch
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", "Error Deleting Row", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void vsBatch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            int KeyCode = FCConvert.ToInt32(e.KeyCode);
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == FCConvert.ToInt32(Keys.Delete))
            {
                // delete a row from the grid
                DeletePaymentFromBatch(vsBatch.Row);
                if (vsBatch.Rows == 1)
                {
                    // XXXX mnuFileImport.Enabled = True
                    mnuFileImportPayPortBatch.Enabled = true;
                }
            }
        }

        private void PurgeRecords_2(string strParam) { PurgeRecords(ref strParam); }
        private void PurgeRecords(ref string strParam)
        {
            // this will find all the records that match these parameters and will delete them from the temp table
            string strPB;
            string strTID = "";
            DateTime dtDate = DateTime.Now;

            if (strParam.Length >= 10)
            {
                dtDate = DateAndTime.DateValue(Strings.Left(strParam, 10));
                strParam = Strings.Trim(Strings.Right(strParam, strParam.Length - 10));
            }

            if (strParam.Length >= 4)
            {
                strTID = Strings.Trim(Strings.Left(strParam, 4));
                strParam = Strings.Trim(Strings.Right(strParam, strParam.Length - 4));
            }

            strPB = Strings.Trim(strParam);

            if (!rsBatchBackup.Execute("DELETE FROM BatchRecover WHERE BatchRecoverDate = '" + FCConvert.ToString(dtDate) + "' AND TellerID = '" + strTID + "' AND PaidBy = '" + strPB + "'", modExtraModules.strUTDatabase, false))
            {
                MessageBox.Show("Error #" + rsBatchBackup.ErrorNumber + " - " + rsBatchBackup.ErrorDescription + ".", "Purge Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }

            fraRecover.Visible = false;
            fraBatchQuestions.Visible = false;
            fraBatch.Visible = false;
            // XXXX    fraSearch.Visible = True
        }

        private void RecoverRecords_2(string strParam) { RecoverRecords(ref strParam); }
        private void RecoverRecords(ref string strParam)
        {
            // this will find all the records that match these parameters and will delete them from the temp table
            string strPB;
            string strTID = "";
            DateTime dtDate = DateTime.Now;

            if (strParam.Length >= 10)
            {
                dtDate = DateAndTime.DateValue(Strings.Left(strParam, 10));
                strParam = Strings.Trim(Strings.Right(strParam, strParam.Length - 10));
            }

            if (strParam.Length >= 4)
            {
                strTID = Strings.Trim(Strings.Left(strParam, 4));
                strParam = Strings.Trim(Strings.Right(strParam, strParam.Length - 4));
            }

            strPB = Strings.Trim(strParam);

            FormatBatchGrid();

            if (!rsBatchBackup.OpenRecordset("SELECT * FROM BatchRecover WHERE BatchRecoverDate = '" + FCConvert.ToString(dtDate) + "' AND TellerID = '" + strTID + "' AND PaidBy = '" + strPB + "'", modExtraModules.strUTDatabase))
            {
                MessageBox.Show("Error #" + rsBatchBackup.ErrorNumber + " - " + rsBatchBackup.ErrorDescription + ".", "Batch Recover Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            else
            {
                if (!rsBatchBackup.EndOfFile() && !rsBatchBackup.BeginningOfFile())
                {
                    vsBatch.Rows = 1;
                    rsBatchBackup.MoveFirst();
                    do
                    {

                        vsBatch.AddItem("");
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchAcct, rsBatchBackup.Get_Fields("AccountNumber")); // Account Number
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchName, rsBatchBackup.Get_Fields("Name")); // Name
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchBill, rsBatchBackup.Get_Fields("BillNumber")); // Bill Number
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchService, rsBatchBackup.Get_Fields("Service")); // Service
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchRef, rsBatchBackup.Get_Fields("Ref")); // Ref
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchPrin, rsBatchBackup.Get_Fields("Prin")); // Prin
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchTax, rsBatchBackup.Get_Fields("Tax")); // Tax
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchInt, rsBatchBackup.Get_Fields("Int")); // Int
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchCost, rsBatchBackup.Get_Fields("Cost")); // Cost
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchTotal, FCConvert.ToDouble(vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchPrin)) + FCConvert.ToDouble(vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchInt)) + FCConvert.ToDouble(vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchCost))); // Total
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchKey, rsBatchBackup.Get_Fields("ID")); // Key in Table
                        if (FCConvert.CBool(rsBatchBackup.Get_Fields("PrintReceipt")))
                        { // if Print Receipt is true
                            vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchPrintReceipt, -1); // then check the box in the grid
                        }
                        else
                        {
                            vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchPrintReceipt, 0);
                        }

                        rsBatchBackup.MoveNext();
                    } while (!rsBatchBackup.EndOfFile());

                    rsBatchBackup.MoveFirst();
                    txtTellerID.Text = FCConvert.ToString(rsBatchBackup.Get_Fields("TellerID"));
                    txtPaymentDate2.Text = FCConvert.ToString(rsBatchBackup.Get_Fields("BatchRecoverDate"));
                    txtEffectiveDate.Text = FCConvert.ToString(rsBatchBackup.Get_Fields("ETDate"));
                    txtPaidBy.Text = FCConvert.ToString(rsBatchBackup.Get_Fields("PaidBy"));
                    txtBillNumber.Text = FCConvert.ToString(rsBatchBackup.Get_Fields("BillNumber"));
                    if (FCConvert.ToString(rsBatchBackup.Get_Fields("Service")) == "S")
                    {
                        cmbSewer.Text = "Sewer";
                    }
                    else
                    {
                        cmbSewer.Text = "Water";
                    }
                }
                else
                {
                    MessageBox.Show("Error processing batch with Payment Date: " + FCConvert.ToString(dtDate) + ", Teller ID: " + strTID + " and Paid By: " + strPB + ".", "Batch Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }

            // total the payments
            txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");

            // XXXX mnuFileImport.Enabled = False
            mnuFileImportPayPortBatch.Enabled = false;
            if (CheckBatchQuestions())
            {
                fraCriteria.Visible = false;
                fraBatchQuestions.Visible = false;
                fraRecover.Visible = false;


                fraBatch.Left = FCConvert.ToInt32((this.Width - fraBatch.Width) / 2.0);
                fraBatch.Top = FCConvert.ToInt32((this.Height - fraBatch.Height) / 3.0);
                fraBatch.Visible = true;

            }
            else
            {
                fraCriteria.Visible = false;
                fraBatch.Visible = true;
                fraRecover.Visible = false;

                //FC:FINAL:BSE #3548  frame should be aligned to the left-margin
                fraBatchQuestions.Left = FCConvert.ToInt32((this.Width - fraBatchQuestions.Width) / 17.0);
                fraBatchQuestions.Top = FCConvert.ToInt32((this.Height - fraBatchQuestions.Height) / 18.0);
                fraBatchQuestions.Visible = true;

            }
        }

        private void vsBatch_RowColChange(object sender, BeforeRowColChangeEventArgs e)
        {
            if (e.NewCol == lngColBatchPrintReceipt)
            {
                vsBatch.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
            }
            else
            {
                vsBatch.Editable = FCGrid.EditableSettings.flexEDNone;
            }
        }

        private bool CreateBatchUTRecords()
        {
            bool CreateBatchUTRecords = false;
            clsDRWrapper rsBatchUT = new clsDRWrapper(); // Payment Rec
            clsDRWrapper rsBillRec = new clsDRWrapper(); // Billing Master
            clsDRWrapper rsLien = new clsDRWrapper(); // Lien
            string strSQL;
            double dblTotalDue = 0;
            int lBill;
            string strSrvc = "";

            lBill = 0;
            // strSQL = "SELECT * FROM BatchRecover WHERE TellerID = '" & strTLRID & "' AND BatchRecoverDate = '" & dtPaymentDate & "' AND ETDate = '" & dtEffectiveDate & "' AND PaidBy = '" & strPaidBy & "' AND Year   = " & lngYear
            strSQL = "SELECT * FROM BatchRecover WHERE TellerID = '" + strTLRID + "' AND BatchRecoverDate = '" + FCConvert.ToString(dtPaymentDate) + "' AND ETDate = '" + FCConvert.ToString(dtEffectiveDate) + "' AND PaidBy = '" + strPaidBy + "' ";

            rsBatchBackup.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
            if (!rsBatchBackup.EndOfFile() && !rsBatchBackup.BeginningOfFile())
            {
                do
                { // cycle through all of the batch list
                  // this function will return the total due and seperate the total into interest, cost, tax and principal due
                  // it also returns the current interest in the variable dblXtraCurInt in order to create an Interest Line
                    lBill = FCConvert.ToInt32(Math.Round(Conversion.Val(rsBatchBackup.Get_Fields("BillNumber"))));
                    strSrvc = rsBatchBackup.Get_Fields_String("Service");
                    if (lBill == 0) lBill = lngBill; // XXXXXX TODO: NO DEFAULT BILL NUMBER                                                                                               '
                    strSQL = "SELECT * FROM Bill WHERE ActualAccountNumber = " + rsBatchBackup.Get_Fields("AccountNumber") + " AND BillNumber = " + FCConvert.ToString(lBill) + " AND (Service = '" + strSrvc + "' OR Service = 'B')";
                    if (rsBillRec.OpenRecordset(strSQL, modExtraModules.strUTDatabase))
                    {
                        dblCurInt = 0;
                        dblXtraCurInt = 0; // XXXXXXXXXXXXX TODO: FIX THE PARAMETER LIST
                                           // dblTotalDue = CalculateAccountUT(rsBillRec, rsBatchBackup.Fields("AccountNumber"), rsBatchBackup.Fields("ETDate"), dblXtraCurInt, dblCurPrin, dblCurInt, dblCurCost)
                        var tempDate = DateTime.Now;
                        dblTotalDue = modUTCalculations.CalculateAccountUT(rsBillRec, rsBatchBackup.Get_Fields("ETDate"), ref dblXtraCurInt, FCConvert.CBool(strSrvc == "W"), ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, ref tempDate, ref dblCurTax);
                        dblPLInt = Math.Round(dblTotalDue - (dblCurPrin + dblCurInt + dblCurCost + dblCurTax), 2);
                        strSQL = "SELECT * FROM PaymentRec WHERE ID = 0";
                        rsBatchUT.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                        rsBatchUT.AddNew();
                        rsBatchUT.Update();
                        rsBatchBackup.Set_Fields("PaymentKey", rsBatchUT.Get_Fields("ID"));
                        rsBatchBackup.Update(true);
                        AdjustPaymentAmount_8(rsBatchBackup.Get_Fields("Prin") + rsBatchBackup.Get_Fields("Int") + rsBatchBackup.Get_Fields("Cost") + rsBatchBackup.Get_Fields("Tax"), true);

                        // Account
                        rsBatchUT.Set_Fields("AccountKey", rsBillRec.Get_Fields("AccountKey"));
                        // Bill Number
                        rsBatchUT.Set_Fields("BillNumber", rsBatchBackup.Get_Fields("BillNumber"));
                        // BillKey
                        // XXXX                If rsBillRec.Fields("LienRecordNumber") = 0 Then
                        rsBatchUT.Set_Fields("BillKey", rsBillRec.Get_Fields("ID"));
                        // XXXX                Else
                        // XXXX                    rsBatchUT.Fields("BillKey") = rsBillRec.Fields("LienRecordNumber")
                        // XXXX                End If
                        // Service
                        rsBatchUT.Set_Fields("Service", strSrvc);
                        // CHGINTNumber
                        rsBatchUT.Set_Fields("CHGINTNumber", 0);
                        // CHGINTDate
                        rsBatchUT.Set_Fields("CHGINTDate", DateAndTime.DateValue(FCConvert.ToString(0)));
                        // ActualSystemDate
                        rsBatchUT.Set_Fields("ActualSystemDate", DateTime.Today);
                        // EffectiveInterestDate
                        rsBatchUT.Set_Fields("EffectiveInterestDate", rsBatchBackup.Get_Fields("ETDate"));
                        // RecordedTransactionDate
                        rsBatchUT.Set_Fields("RecordedTransactionDate", rsBatchBackup.Get_Fields("BatchRecoverDate"));
                        // Teller
                        rsBatchUT.Set_Fields("Teller", strTLRID);
                        // Reference
                        if (Strings.Trim(rsBatchBackup.Get_Fields("Ref")).Length > 10)
                        {
                            rsBatchUT.Set_Fields("Reference", Strings.Left(Strings.Trim(rsBatchBackup.Get_Fields("Ref")), 10));
                        }
                        else
                        {
                            rsBatchUT.Set_Fields("Reference", Strings.Trim(rsBatchBackup.Get_Fields("Ref")));
                        }
                        // Period
                        rsBatchUT.Set_Fields("Period", "A");
                        // Code
                        rsBatchUT.Set_Fields("Code", "P");
                        // ReceiptNumber
                        // this has to be a check to see if it is coming from CR not just if they have it
                        if (modExtraModules.IsThisCR())
                        {
                            rsBatchUT.Set_Fields("ReceiptNumber", -1);
                        }
                        else
                        {
                            rsBatchUT.Set_Fields("ReceiptNumber", 0);
                        }
                        // Principal
                        rsBatchUT.Set_Fields("Principal", Math.Round(dblCurPrin, 2));
                        // PreLienInterest
                        rsBatchUT.Set_Fields("PreLienInterest", Math.Round(dblPLInt, 2));
                        // CurrentInterest
                        rsBatchUT.Set_Fields("CurrentInterest", Math.Round(dblCurInt, 2));
                        // LienCost
                        rsBatchUT.Set_Fields("LienCost", Math.Round(dblCurCost, 2));
                        // Tax
                        rsBatchUT.Set_Fields("Tax", Math.Round(dblCurTax, 2));
                        // TransNumber
                        rsBatchUT.Set_Fields("TransNumber", 0);
                        // PaidBy
                        rsBatchUT.Set_Fields("PaidBy", strPaidBy);
                        // Comments
                        rsBatchUT.Set_Fields("Comments", "This was a batch transaction.");
                        // CashDrawer
                        rsBatchUT.Set_Fields("CashDrawer", "Y");
                        // GeneralLedger
                        rsBatchUT.Set_Fields("GeneralLedger", "Y");
                        // BudgetaryAccountNumber
                        rsBatchUT.Set_Fields("BudgetaryAccountNumber", "");
                        // BillCode
                        // XXXX                If rsBillRec.Fields("LienRecordNumber") <> 0 Then
                        // XXXX                    rsBatchUT.Fields("BillCode") = "L"
                        // XXXX                Else
                        // XXXX                    rsBatchUT.Fields("BillCode") = "R"
                        // XXXX                End If
                        rsBatchUT.Set_Fields("Lien", false);

                        if (!rsBatchUT.Update(false))
                        { // if there is an error creating this record
                            MessageBox.Show("Error #" + rsBatchUT.ErrorNumber + " - " + rsBatch.ErrorDescription + " for account number " + rsBatchBackup.Get_Fields("AccountNumber"), "Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                            CreateBatchUTRecords = false;
                        }
                        else
                        { // if the record was created correctly
                            if (dblXtraCurInt != 0)
                            { // if there is interest that has to be charged, then this will do it
                                rsBatchUT.FindFirstRecord("AccountKey", rsBillRec.Get_Fields("AccountKey"));
                                if (CreateBatchCHGINTRecord(rsBatchUT, dblXtraCurInt))
                                {
                                    rsBatchUT.Edit();
                                    rsBatchUT.Set_Fields("CHGINTNumber", lngBatchCHGINTNumber);
                                    rsBatchUT.Update(true);
                                }
                                else
                                {
                                    MessageBox.Show("Error creating Charged Interest Record for account number " + rsBatchBackup.Get_Fields("AccountNumber") + ".", "Batch CHGINT Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                                }
                            }

                            // MAL@20080603: Change to not update CR records yet
                            // Tracker Reference: 14055
                            if (!modExtraModules.IsThisCR())
                            {

                                // XXXXXXXXXXXXX DON'T PROCESS LIEN PAYMENT - ADD TAX
                                // MAL@20071210: Update Bill Record with Amounts Listed
                                // Tracker Reference: 11594
                                if (rsBillRec.Get_Fields("LienRecordNumber") != 0)
                                {
                                    // Update Lien Record
                                    // rsLien.OpenRecordset "SELECT * FROM Lien WHERE LienRecordNumber = " & SERVICE & rsBillRec.Fields("LienRecordNumber"), strUTDatabase
                                    // If rsLien.RecordCount > 0 Then
                                    // rsLien.Edit
                                    // rsLien.Fields("PrincipalPaid") = rsLien.Fields("PrincipalPaid") + Round(dblCurPrin, 2)
                                    // rsLien.Fields("InterestPaid") = rsLien.Fields("InterestPaid") + Round(dblCurInt, 2)
                                    // rsLien.Fields("CostsPaid") = rsLien.Fields("CostsPaid") + Round(dblCurCost, 2)
                                    // rsLien.Fields("TaxPaid") = rsLien.Fields("TaxPaid") + Round(dblCurTax, 2)
                                    // rsLien.Fields("PLIPaid") = rsLien.Fields("PLIPaid") + Round(dblPLInt, 2)
                                    // rsLien.Update
                                    // End If
                                }
                                else
                                {
                                    // Update Bill Record
                                    rsBillRec.Edit();
                                    rsBillRec.Set_Fields(strSrvc + "PrinPaid", rsBillRec.Get_Fields(strSrvc + "PrinPaid") + Math.Round(dblCurPrin, 2));
                                    rsBillRec.Set_Fields(strSrvc + "IntPaid", rsBillRec.Get_Fields(strSrvc + "IntPaid") + Math.Round(dblCurInt, 2));
                                    rsBillRec.Set_Fields(strSrvc + "CostPaid", rsBillRec.Get_Fields(strSrvc + "CostPaid") + Math.Round(dblCurCost, 2));
                                    rsBillRec.Set_Fields(strSrvc + "TaxPaid", rsBillRec.Get_Fields(strSrvc + "TaxPaid") + Math.Round(dblCurTax, 2));
                                    rsBillRec.Update();
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error processing account number " + rsBatchBackup.Get_Fields("AccountNumber") + ", no bill found.", "Bill Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    rsBatchBackup.MoveNext();
                } while (!rsBatchBackup.EndOfFile());
                CreateBatchUTRecords = true;
            }
            else
            {
                MessageBox.Show("Error loading batch payments.", "Batch Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                CreateBatchUTRecords = false;
            }
            return CreateBatchUTRecords;
        }

        private bool CreateBatchCHGINTRecord(clsDRWrapper rsTemp, double dblInt)
        {
            bool CreateBatchCHGINTRecord = false;
            // this will create an CHGINT interest record with a status of Pending if there is CR
            DateTime dtStartingDate; // this will be the beginning date of interest
            DateTime dtEffectiveInterestDate;
            clsDRWrapper rsCHGINT = new clsDRWrapper();
            string strSQL;

            lngBatchCHGINTNumber = 0;

            strSQL = "SELECT * FROM PaymentRec WHERE ID = 0";
            if (rsCHGINT.OpenRecordset(strSQL, modExtraModules.strUTDatabase))
            {
                rsCHGINT.AddNew();
                rsCHGINT.Update();
                lngBatchCHGINTNumber = rsCHGINT.Get_Fields_Int32("ID");
                // Account
                rsCHGINT.Set_Fields("AccountKey", rsTemp.Get_Fields("AccountKey"));
                // Bill Number
                rsCHGINT.Set_Fields("BillNumber", rsTemp.Get_Fields("BillNumber"));
                // BillKey
                rsCHGINT.Set_Fields("BillKey", rsTemp.Get_Fields("BillKey"));
                // Service
                rsCHGINT.Set_Fields("Service", rsTemp.Get_Fields("Service"));
                // CHGINTNumber
                rsCHGINT.Set_Fields("CHGINTNumber", rsTemp.Get_Fields("ID")); // XXXX? IS THIS CORRECT?
                                                                              // CHGINTDate - this is the last interest date in order to backtrack (delete payments)
                rsCHGINT.Set_Fields("CHGINTDate", dtIntPaidDate);
                // ActualSystemDate
                rsCHGINT.Set_Fields("ActualSystemDate", DateTime.Today);
                // EffectiveInterestDate
                rsCHGINT.Set_Fields("EffectiveInterestDate", rsTemp.Get_Fields("EffectiveInterestDate"));
                // RecordedTransactionDate
                rsCHGINT.Set_Fields("RecordedTransactionDate", rsTemp.Get_Fields("RecordedTransactionDAte"));
                // Teller
                rsCHGINT.Set_Fields("Teller", strTLRID);
                // Reference
                rsCHGINT.Set_Fields("Reference", "CHGINT");
                // Period
                rsCHGINT.Set_Fields("Period", "A");
                // Code
                rsCHGINT.Set_Fields("Code", "I");
                // ReceiptNumber
                // this has to be a check to see if it is coming from CR not just if they have it
                if (modExtraModules.IsThisCR())
                {
                    rsCHGINT.Set_Fields("ReceiptNumber", -1);
                }
                else
                {
                    rsCHGINT.Set_Fields("ReceiptNumber", 0);
                }
                // Principal
                rsCHGINT.Set_Fields("Principal", 0);
                // PreLienInterest
                rsCHGINT.Set_Fields("PreLienInterest", 0);
                // CurrentInterest
                rsCHGINT.Set_Fields("CurrentInterest", Math.Round(dblXtraCurInt * -1, 2));
                // LienCost
                rsCHGINT.Set_Fields("LienCost", 0);
                // Tax
                rsCHGINT.Set_Fields("Tax", 0);
                // TransNumber
                rsCHGINT.Set_Fields("TransNumber", 0);
                // PaidBy
                rsCHGINT.Set_Fields("PaidBy", strPaidBy);
                // Comments
                rsCHGINT.Set_Fields("Comments", "This was a batch transaction.");
                // CashDrawer
                rsCHGINT.Set_Fields("CashDrawer", "Y");
                // GeneralLedger
                rsCHGINT.Set_Fields("GeneralLedger", "Y");
                // BudgetaryAccountNumber
                rsCHGINT.Set_Fields("BudgetaryAccountNumber", "");
                // BillCode
                rsCHGINT.Set_Fields("Lien", rsTemp.Get_Fields("Lien"));

                if (rsCHGINT.Update(false))
                { // make sure that it updated
                    CreateBatchCHGINTRecord = true;
                }
                else
                {
                    MessageBox.Show("Error #" + rsCHGINT.ErrorNumber + " - " + rsCHGINT.ErrorDescription + ".", "Batch CHGINT Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    CreateBatchCHGINTRecord = false;
                }
            }
            else
            {
                MessageBox.Show("Error #" + rsCHGINT.ErrorNumber + " - " + rsCHGINT.ErrorDescription + ".", "Batch CHGINT Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                CreateBatchCHGINTRecord = false;
            }
            return CreateBatchCHGINTRecord;
        }

        // VBto upgrade warning: 'Return' As DateTime	OnWriteFCConvert.ToInt16(
        private DateTime GetIntStartDate(ref int lngRateKey, ref int lngYear)
        {
            DateTime GetIntStartDate = System.DateTime.Now;
            // this will return the interest start date of the bill from the rate record in case no payments have been made
            // to set the InterestPaidThroughDate
            clsDRWrapper rsRateRec = new clsDRWrapper();

            rsRateRec.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(lngRateKey));
            if (!rsRateRec.EndOfFile() && !rsRateRec.BeginningOfFile())
            {
                if (FCConvert.CBool(rsRateRec.Get_Fields("InterestStartDate1")) != 0)
                {
                    GetIntStartDate = rsRateRec.Get_Fields_DateTime("InterestStartDate1");
                }
            }

            if (GetIntStartDate.ToOADate() == 0)
            {
                GetIntStartDate = DateTime.FromOADate(0);
            }
            return GetIntStartDate;
        }

        private bool CheckBatchQuestions()
        {
            bool CheckBatchQuestions = false;
            bool boolBatch = false;
            clsDRWrapper rsCheck = new clsDRWrapper();

            // check the Teller ID
            if (Strings.Trim(txtTellerID.Text) != "")
            {
#if TWCR0000
                if (modGlobal.ValidateTellerID(Strings.Trim(txtTellerID.Text)) <= 1)
                {
                    boolBatch = true;
                    strTLRID = Strings.Trim(txtTellerID.Text);
                }
                else
                {
                    MessageBox.Show("Please enter a valid Teller ID.", "Teller Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    boolBatch = false;
                }
#endif
            }
            else
            {
                MessageBox.Show("Please enter a valid Teller ID.", "Teller Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                boolBatch = false;
            }

            if (boolBatch)
            {
                // check the Payment date
                if (Strings.Trim(txtPaymentDate2.Text) != "")
                {
                    if (Information.IsDate(txtPaymentDate2.Text))
                    {
                        dtPaymentDate = DateAndTime.DateValue(txtPaymentDate2.Text);
                        boolBatch = true;
                    }
                    else
                    {
                        MessageBox.Show("Please enter a valid payment date.", "Invalid Payment Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        boolBatch = false;
                    }
                }
                else
                {
                    MessageBox.Show("Please enter a payment date.", "Invalid Payment Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    boolBatch = false;
                }

                // check the Effective date
                if (Strings.Trim(txtEffectiveDate.Text) != "")
                {
                    if (Information.IsDate(txtEffectiveDate.Text))
                    {
                        dtEffectiveDate = DateAndTime.DateValue(txtEffectiveDate.Text);
                        boolBatch = true;
                    }
                    else
                    {
                        MessageBox.Show("Please enter a valid effective date.", "Invalid Effective Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        boolBatch = false;
                    }
                }
                else
                {
                    MessageBox.Show("Please enter a effective date.", "Invalid Effective Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    boolBatch = false;
                }
            }

            // check the Paid by name
            if (boolBatch)
            {
                if (Strings.Trim(txtPaidBy.Text) != "")
                {
                    strPaidBy = Strings.Trim(txtPaidBy.Text);
                    boolBatch = true;
                }
                else
                {
                    MessageBox.Show("Please enter a Paid By name.", "Invalid Paid By Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    boolBatch = false;
                }
            }

            CheckBatchQuestions = boolBatch;
            return CheckBatchQuestions;
        }

        private void UpdatePrintReceiptChoice()
        {
            // this sub will update the BatchRecover table with the print receipt checkbox values
            clsDRWrapper rsCHK = new clsDRWrapper();
            string strPrint = "";
            int intCT;
            string strSQL = "";
            int lngErrNum = 0;
            string strErrDesc = "";

            try
            {
                for (intCT = 1; intCT <= vsBatch.Rows - 1; intCT++)
                {
                    if (Conversion.Val(vsBatch.TextMatrix(intCT, 10)) == -1)
                    {
                        strPrint = "'True'";
                    }
                    else
                    {
                        strPrint = "'False'";
                    }

                    strSQL = "UPDATE BatchRecover Set PrintReceipt = " + strPrint + " WHERE TellerID = '" + strTLRID + "' AND BatchRecoverDate = '" + FCConvert.ToString(dtPaymentDate) + "' AND ETDate = '" + FCConvert.ToString(dtEffectiveDate) + "' AND " + "BillNumber = " + vsBatch.TextMatrix(intCT, lngColBatchBill) + " AND Service = '" + vsBatch.TextMatrix(intCT, lngColBatchService) + "' AND AccountNumber = " + vsBatch.TextMatrix(intCT, lngColBatchAcct);
                    if (!rsCHK.Execute(strSQL, modExtraModules.strUTDatabase, false))
                    {
                        lngErrNum = rsCHK.ErrorNumber;
                        strErrDesc = rsCHK.ErrorDescription;
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                if (lngErrNum != 0)
                {
                    MessageBox.Show("Error #" + FCConvert.ToString(lngErrNum) + " - " + strErrDesc + " has occurred updating the Print Receipt Column.", "Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
        }

        private void SavePayment()
        {
            if (txtAmount.Text != "")
            {
                if (MessageBox.Show("Would you like to save this payment?", "Save Payment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (txtAmount.DataChanged)
                    {
                        if (Conversion.Val(txtAmount.Text) != 0)
                        {
                            if (FCConvert.ToDouble(txtAmount.Text) > dblTotalDue)
                            {
                                if (MessageBox.Show("Payment is more than due, would you like to continue.", "Overpayment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information) != DialogResult.Yes)
                                {
                                    return;
                                }
                                else
                                {
                                    modGlobalFunctions.AddCYAEntry_6("CR", "Added CL batch overpayment. - Acct: " + txtBatchAccount.Text);
                                }
                            }
                            if (AdjustPaymentAmount_8(FCConvert.ToDouble(txtAmount.Text)) != 0)
                            {
                                return; // error has occured, exit sub and do not save payment
                            }
                        }
                        else
                        {
                            MessageBox.Show("Payment amount must be numeric and greater than 0.", "Invalid Payment Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    AddPaymentToBatch(rsBatch, FCConvert.ToInt32(FCConvert.ToDouble(txtBatchAccount.Text)));
                }
                else
                {
                    txtBatchAccount.Focus();
                }
            }
        }

        private void FormatBatchSearchGrid(bool boolReset = false)
        {
            // TODO: IS IT WORTH INCLUDING THE BATCH SEARCH???

            // Dim lngWid              As Long
            // 
            // With vsBatchSearch
            // .Cols = 4
            // lngWid = .Width
            // 
            // If boolReset Then
            // .rows = 1
            // 
            // headers
            // .TextMatrix(0, lngColBSAccount) = "Acct"
            // .TextMatrix(0, lngColBSKey) = ""
            // .TextMatrix(0, lngColBSName) = "Name"
            // .TextMatrix(0, lngColBSMapLot) = "Map / Lot"
            // 
            // .ColAlignment(lngColBSKey) = flexAlignLeftCenter
            // .ColAlignment(lngColBSName) = flexAlignLeftCenter
            // .ColAlignment(lngColBSMapLot) = flexAlignLeftCenter
            // 
            // header alignment
            // .Cell(flexcpAlignment, 0, 1, 0, .Cols - 1) = flexAlignCenterCenter
            // End If
            // 
            // width of each column
            // .ColWidth(lngColBSAccount) = lngWid * 0.15
            // .ColWidth(lngColBSKey) = 0#
            // .ColWidth(lngColBSName) = lngWid * 0.6
            // .ColWidth(lngColBSMapLot) = lngWid * 0.2
            // 
            // this will reset the height
            // If vsBatchSearch.rows * vsBatchSearch.RowHeight(0) + 70 > fraBatchSearchResults.Height - 500 Then
            // vsBatchSearch.Height = (fraBatchSearchResults.Height - 400) * 0.89
            // vsBatchSearch.ScrollBars = flexScrollBarVertical
            // Else
            // vsBatchSearch.Height = vsBatchSearch.rows * vsBatchSearch.RowHeight(0) + 70
            // vsBatchSearch.ScrollBars = flexScrollBarNone
            // End If
            // End With
        }

        private void ShowAccount(int lngAcct)
        {
            // this will show the account selected in acct box
            txtBatchAccount.Text = FCConvert.ToString(lngAcct);

            // XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            // fraBatchSearchResults.Visible = False
            // fraBatchSearchCriteria.Visible = False
            fraBatch.Enabled = true;
        }

        private void ImportPayportBatch()
        {
            bool boolOpen = false;
            StreamReader ts = null;
            try
            {
                string strFileName = "";
                Information.Err().Clear();
                MDIParent.InstancePtr.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNNoChangeDir) + FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNFileMustExist) + FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNExplorer) + FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNLongNames);
                MDIParent.InstancePtr.CommonDialog1.CancelError = true;
                MDIParent.InstancePtr.CommonDialog1.FileName = "";
                MDIParent.InstancePtr.CommonDialog1.InitDir = Environment.CurrentDirectory;
                /*? On Error Resume Next  */
                MDIParent.InstancePtr.CommonDialog1.ShowOpen();
                if (Information.Err().Number != 0)
                {
                    Information.Err().Clear();
                    return;
                }
                strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;

                boolBatchImport = true;
                boolOpen = false;
                string strLine = "";
                double dblFileTot;
                int lngLoadedYear;
                dblFileTot = 0;
                if (FCFileSystem.FileExists(strFileName))
                {
                    ts = FCFileSystem.OpenText(strFileName);
                    boolOpen = true;
                    while (!ts.EndOfStream)
                    {
                        //Application.DoEvents();
                        strLine = ts.ReadLine();
                        if (Strings.UCase(Strings.Left(strLine, 6)) != "BILLID")
                        {
                            dblFileTot += TestPayport(strLine);
                        }
                    }
                    ts.Close();
                    boolOpen = false;
                    if (MessageBox.Show("The total file amount is " + Strings.Format(dblFileTot, "#,###,###,##0.00") + " is this correct?", "Confirm Amount", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        return;
                    }
                    ts = FCFileSystem.OpenText(strFileName);
                    boolOpen = true;
                    while (!ts.EndOfStream)
                    {
                        //Application.DoEvents();
                        strLine = ts.ReadLine();
                        if (Strings.UCase(Strings.Left(strLine, 6)) != "BILLID")
                        {
                            ParsePayport(strLine, txtPaidBy.Text);
                        }
                    }
                    ts.Close();
                    boolOpen = false;
                }
                else
                {
                    MessageBox.Show("File " + strFileName + " not found", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                boolBatchImport = false;
                txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");

                txtBatchAccount.Text = "";
                txtAmount.Text = "";
                return;
            }
            catch (Exception ex)
            {
                if (boolOpen)
                {
                    ts.Close();
                }
                MessageBox.Show("Error Number " + Information.Err(ex).Description + "\r\n" + "In Import", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private double TestPayport(string strLine)
        {
            double TestPayport = 0;
            double dblReturn;
            string[] arrDet = null;

            // Payment detail format "|" Delimited record - Bill ID|Bill Type|Bill Number|Account|Pmt Date|Pmt Amount|Name

            dblReturn = 0;
            arrDet = Strings.Split(strLine, "|", -1, CompareConstants.vbBinaryCompare);
            if (Conversion.Val(arrDet[2]) > 0 && Conversion.Val(arrDet[3]) > 0)
            {
                // account is greater than 0
                dblReturn = Conversion.Val(arrDet[5]);
            }
            TestPayport = dblReturn;
            return TestPayport;
        }

        private void ParsePayport(string strLine, string strPaidName)
        {
            string[] arrDet = null;

            int lngAccount;
            int lBill;
            Decimal crPayment;
            string strPaidBy = "";
            string strSrvc;
            bool boolCancel = false;

            // Payment detail format "|" Delimited record - Bill ID|Bill Type|Bill Number|Account|Pmt Date|Pmt Amount|Name
            arrDet = Strings.Split(strLine, "|", -1, CompareConstants.vbBinaryCompare);

            lBill = FCConvert.ToInt32(Math.Round(Conversion.Val(arrDet[2])));
            lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(arrDet[3])));
            strSrvc = Strings.Left(arrDet[1], 1); // The service is SW or WA(?)

            if (lngAccount > 0)
            {
                crPayment = FCConvert.ToDecimal(Conversion.Val(Strings.Trim(arrDet[5])));

                if (crPayment == 0) return;

                strPaidBy = Strings.Trim(arrDet[6]);
                if (strPaidBy == "" || modExtraModules.IsThisCR()) strPaidBy = strPaidName;
                txtBatchAccount.Text = FCConvert.ToString(lngAccount);
                boolCancel = false;
                txtBillNumber.Text = FCConvert.ToString(lBill);
                cmbSewer.Text = strSrvc == "S" ? "Sewer" : "Water";
                txtAmount.Text = FCConvert.ToString(crPayment);
                txtBatchAccount_Validate(ref boolCancel);

                if (boolCancel) return;

                txtAmount.Text = FCConvert.ToString(crPayment);
                if (AdjustPaymentAmount_8(FCConvert.ToDouble(crPayment)) == 0)
                {
                    CreateBatchPaymentLine(lngAccount, dblCurPrin, dblCurTax, dblCurInt + dblPLInt, dblCurCost, dtPaymentDate, dtEffectiveDate, lBill, strSrvc);
                }
            }
        }
    }
}
