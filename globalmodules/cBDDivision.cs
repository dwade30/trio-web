﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cBDDivision
	{
		//=========================================================
		private string strShortDescription = string.Empty;
		private string strLongDescription = string.Empty;
		private string strFund = string.Empty;
		private string strDepartment = string.Empty;
		private int lngRecordNumber;
		private string strDivision = string.Empty;

		public string Division
		{
			set
			{
				strDivision = value;
			}
			get
			{
				string Division = "";
				Division = strDivision;
				return Division;
			}
		}

		public int ID
		{
			set
			{
				lngRecordNumber = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordNumber;
				return ID;
			}
		}

		public string Department
		{
			set
			{
				strDepartment = value;
			}
			get
			{
				string Department = "";
				Department = strDepartment;
				return Department;
			}
		}

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}

		public string Description
		{
			set
			{
				strLongDescription = value;
			}
			get
			{
				string Description = "";
				Description = strLongDescription;
				return Description;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}
	}
}
