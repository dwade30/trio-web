﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cPPSecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cPPSecuritySetup() : base()
		{
			strThisModule = "PP";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem(" Entry Personal Property", "", 1, false, ""));
			AddItem_2(CreateItem("Account Maintenance", "", 2, true, ""));
			AddItem_2(CreateItem("Account Maintenance", "Add New Accounts", 33, false, ""));
			AddItem_2(CreateItem("Account Maintenance", "Calculate", 27, false, ""));
			AddItem_2(CreateItem("Account Maintenance", "Delete Accounts", 34, false, ""));
			AddItem_2(CreateItem("Account Maintenance", "Edit Address", 30, false, ""));
			AddItem_2(CreateItem("Account Maintenance", "Edit Itemized Screen", 28, false, ""));
			AddItem_2(CreateItem("Account Maintenance", "Edit Leased Screen", 29, false, ""));
			AddItem_2(CreateItem("Account Maintenance", "Edit Master Values (non address)", 31, false, ""));
			AddItem_2(CreateItem("Account Maintenance", "View Itemized Screen", 26, false, ""));
			AddItem_2(CreateItem("Account Maintenance", "View Leased Screen", 25, false, ""));
			AddItem_2(CreateItem("Add New Accounts", "", 33, false, ""));
			AddItem_2(CreateItem("Compute Menu", "", 6, true, ""));
			AddItem_2(CreateItem("Compute Menu", "Audit Summary", 10, false, ""));
			AddItem_2(CreateItem("Compute Menu", "Calculate Exemptions", 43, false, ""));
			AddItem_2(CreateItem("Compute Menu", "Display Current or Range (calc)", 11, false, ""));
			AddItem_2(CreateItem("Compute Menu", "Print Current or Range (calc)", 12, false, ""));
			AddItem_2(CreateItem("Compute Menu", "Print Summary", 8, false, ""));
			AddItem_2(CreateItem("Compute Menu", "Transfer Values to Billing", 9, false, ""));
			AddItem_2(CreateItem("Cost Files", "", 4, true, ""));
			AddItem_2(CreateItem("Cost Files", "Edit Business Codes", 23, false, ""));
			AddItem_2(CreateItem("Cost Files", "Edit Exemptions/Trending", 22, false, ""));
			AddItem_2(CreateItem("Cost Files", "Edit Ratio/Opens Table", 21, false, ""));
			AddItem_2(CreateItem("Cost Files", "Edit Street & Tran Codes", 24, false, ""));
			AddItem_2(CreateItem("Delete Accounts", "", 34, false, ""));
			AddItem_2(CreateItem("Edit Address (Short + Long)", "", 30, false, ""));
			AddItem_2(CreateItem("File Maintenance", "", 7, true, ""));
			AddItem_2(CreateItem("File Maintenance", "Create Archives", 41, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Create User Extract", 20, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Group Maintenance", 37, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Run Archive Data", 40, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Settings", 19, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Transfer Item/Leased Info", 35, false, ""));
			AddItem_2(CreateItem("Group Maintenance", "", 37, true, ""));
			AddItem_2(CreateItem("Group Maintenance", "Edit/Add Groups", 39, false, ""));
			AddItem_2(CreateItem("Import / Export", "", 44, true, ""));
			AddItem_2(CreateItem("Import / Export", "Create Extract", 45, false, ""));
			AddItem_2(CreateItem("Import / Export", "Create User Extract", 20, false, ""));
			AddItem_2(CreateItem("Import / Export", "Itemized From File", 47, false, ""));
			AddItem_2(CreateItem("Print Menu", "", 5, true, ""));
			AddItem_2(CreateItem("Print Menu", "Deleted Accounts", 17, false, ""));
			AddItem_2(CreateItem("Print Menu", "Highest Assessment Listing", 16, false, ""));
			AddItem_2(CreateItem("Print Menu", "Labels", 18, false, ""));
			AddItem_2(CreateItem("Print Menu", "Print Account List", 13, false, ""));
			AddItem_2(CreateItem("Print Menu", "Reimbursement Notices", 15, false, ""));
			AddItem_2(CreateItem("Print Menu", "Taxpayer Notices", 14, false, ""));
			AddItem_2(CreateItem("Short Maintenance", "", 3, true, ""));
			AddItem_2(CreateItem("Short Maintenance", "Delete Accounts", 34, false, ""));
			AddItem_2(CreateItem("Short Maintenance", "Edit Address", 30, false, ""));
			AddItem_2(CreateItem("Short Maintenance", "Edit Values", 32, false, ""));
			AddItem_2(CreateItem("Undelete Accounts", "", 36, false, ""));
		}
	}
}
