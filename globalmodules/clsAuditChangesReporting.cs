﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;

#if TWCL0000
using TWCL0000;


#elif TWFA0000
using TWFA0000;


#elif TWBD0000
using TWBD0000;


#elif TWAR0000
using TWAR0000;


#elif TWCR0000
using TWCR0000;


#elif TWPP0000
using TWPP0000;


#elif TWGNENTY
using TWGNENTY;
using modGlobal = TWGNENTY.modGNBas;


#elif TWRE0000
using modGlobal = TWRE0000.modGlobalVariables;


#elif TWUT0000
using modGlobal = TWUT0000.modMain;


#elif TWMV0000
using TWMV0000;
using modGlobal = TWMV0000.MotorVehicle;


#elif TWCE0000
using modGlobal = TWCE0000.modMain;


#elif TWMV0000
using modGlobal = TWCE0000.modGlobalVariables;


#elif TWPY0000
using modGlobal = TWPY0000.modGlobalVariables;


#elif TWCK0000
using modGlobal = TWCK0000.modGNBas;
#endif
namespace Global
{
	public class clsAuditChangesReporting
	{
		//=========================================================
		private string[] strChanges = null;
		private int intTotalChanges;
		private bool blnChangesMade;

		public void AddChange(string strDesc)
		{
			if (blnChangesMade == false)
				blnChangesMade = true;
			Array.Resize(ref strChanges, intTotalChanges + 1);
			strChanges[intTotalChanges] = strDesc;
			intTotalChanges += 1;
		}

		public clsAuditChangesReporting() : base()
		{
			intTotalChanges = 0;
			blnChangesMade = false;
		}

		public void SaveToAuditChangesTable(string strLocation, string strUserField1 = "", string strUserField2 = "", string strUserField3 = "", string strUserField4 = "", string strDatabase = modGlobal.DEFAULTDATABASE)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int counter;
			modAuditReporting.CreateAuditChangesTable();
			if (blnChangesMade)
			{
				rsInfo.OpenRecordset("SELECT * FROM AuditChanges WHERE ID = 0", strDatabase);
				for (counter = 0; counter <= intTotalChanges - 1; counter++)
				{
					rsInfo.AddNew();
					rsInfo.Set_Fields("Location", strLocation);
					rsInfo.Set_Fields("UserField1", strUserField1);
					rsInfo.Set_Fields("UserField2", strUserField2);
					rsInfo.Set_Fields("UserField3", strUserField3);
					rsInfo.Set_Fields("UserField4", strUserField4);
					rsInfo.Set_Fields("ChangeDescription", strChanges[counter]);
					rsInfo.Set_Fields("UserID", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
					rsInfo.Set_Fields("DateUpdated", DateTime.Today);
					rsInfo.Set_Fields("TimeUpdated", DateAndTime.TimeOfDay);
					rsInfo.Update();
				}
			}
		}

		public void Reset()
		{
			blnChangesMade = false;
			intTotalChanges = 0;
			FCUtils.EraseSafe(strChanges);
			strChanges = new string[intTotalChanges + 1];
			strChanges[0] = "";
		}
	}
}
