//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

#if TWPP0000
using TWPP0000;
using modGlobalVariables = TWPP0000.modSecurity;


#elif TWBL0000
using TWBL0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmGroup.
	/// </summary>
	partial class frmGroup : BaseForm
	{
		public fecherFoundation.FCRichTextBox rtbComment;
		public FCGrid AccountsGrid;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtGroupNumber;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtStreetNumber;
		public fecherFoundation.FCTextBox txtApt;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtStreetName;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label10;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuBalance;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteGroup;
		public fecherFoundation.FCToolStripMenuItem mnuSepar3;
		public fecherFoundation.FCToolStripMenuItem mnuAdd;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private System.ComponentModel.IContainer components;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGroup));
            this.rtbComment = new fecherFoundation.FCRichTextBox();
            this.AccountsGrid = new fecherFoundation.FCGrid();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtCity = new fecherFoundation.FCTextBox();
            this.txtState = new fecherFoundation.FCTextBox();
            this.txtZip = new fecherFoundation.FCTextBox();
            this.txtZip4 = new fecherFoundation.FCTextBox();
            this.txtStreetNumber = new fecherFoundation.FCTextBox();
            this.txtApt = new fecherFoundation.FCTextBox();
            this.txtName = new fecherFoundation.FCTextBox();
            this.txtAddress1 = new fecherFoundation.FCTextBox();
            this.txtAddress2 = new fecherFoundation.FCTextBox();
            this.txtStreetName = new fecherFoundation.FCTextBox();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.txtGroupNumber = new fecherFoundation.FCTextBox();
            this.Label10 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBalance = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteGroup = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdBalance = new fecherFoundation.FCButton();
            this.cmdDeleteGroup = new fecherFoundation.FCButton();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtbComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccountsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBalance)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 689);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.rtbComment);
            this.ClientArea.Controls.Add(this.AccountsGrid);
            this.ClientArea.Controls.Add(this.Label10);
            this.ClientArea.Size = new System.Drawing.Size(1014, 518);
            this.ClientArea.Controls.SetChildIndex(this.Label10, 0);
            this.ClientArea.Controls.SetChildIndex(this.AccountsGrid, 0);
            this.ClientArea.Controls.SetChildIndex(this.rtbComment, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAdd);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.cmdBalance);
            this.TopPanel.Controls.Add(this.cmdDeleteGroup);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteGroup, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdBalance, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAdd, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(215, 28);
            this.HeaderText.Text = "Group Maintenance";
            // 
            // rtbComment
            // 
            this.rtbComment.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.rtbComment.Location = new System.Drawing.Point(30, 643);
            this.rtbComment.Name = "rtbComment";
            this.rtbComment.Size = new System.Drawing.Size(936, 46);
            this.rtbComment.TabIndex = 3;
            // 
            // AccountsGrid
            // 
            this.AccountsGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.AccountsGrid.Cols = 5;
            this.AccountsGrid.ExtendLastCol = true;
            this.AccountsGrid.FixedCols = 0;
            this.AccountsGrid.Location = new System.Drawing.Point(30, 317);
            this.AccountsGrid.Name = "AccountsGrid";
            this.AccountsGrid.RowHeadersVisible = false;
            this.AccountsGrid.Rows = 50;
            this.AccountsGrid.ShowFocusCell = false;
            this.AccountsGrid.Size = new System.Drawing.Size(936, 263);
            this.AccountsGrid.TabIndex = 1;
            this.AccountsGrid.ComboDropDown += new System.EventHandler(this.AccountsGrid_ComboDropDown);
            this.AccountsGrid.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.AccountsGrid_CellChanged);
            this.AccountsGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.AccountsGrid_ValidateEdit);
            this.AccountsGrid.CurrentCellChanged += new System.EventHandler(this.AccountsGrid_RowColChange);
            this.AccountsGrid.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.AccountsGrid_MouseDownEvent);
            // 
            // Frame1
            // 
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.txtGroupNumber);
            this.Frame1.Controls.Add(this.txtCity);
            this.Frame1.Controls.Add(this.txtState);
            this.Frame1.Controls.Add(this.txtZip);
            this.Frame1.Controls.Add(this.txtZip4);
            this.Frame1.Controls.Add(this.txtStreetNumber);
            this.Frame1.Controls.Add(this.txtApt);
            this.Frame1.Controls.Add(this.txtName);
            this.Frame1.Controls.Add(this.txtAddress1);
            this.Frame1.Controls.Add(this.txtAddress2);
            this.Frame1.Controls.Add(this.txtStreetName);
            this.Frame1.Controls.Add(this.Label9);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label5);
            this.Frame1.Controls.Add(this.Label6);
            this.Frame1.Controls.Add(this.Label7);
            this.Frame1.Controls.Add(this.Label8);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(1000, 266);
            this.Frame1.TabIndex = 0;
            // 
            // txtCity
            // 
            this.txtCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtCity.Enabled = false;
            this.txtCity.Location = new System.Drawing.Point(782, 30);
            this.txtCity.Name = "txtCity";
            this.txtCity.Size = new System.Drawing.Size(218, 40);
            this.txtCity.TabIndex = 11;
            // 
            // txtState
            // 
            this.txtState.BackColor = System.Drawing.SystemColors.Window;
            this.txtState.Enabled = false;
            this.txtState.Location = new System.Drawing.Point(782, 90);
            this.txtState.Name = "txtState";
            this.txtState.Size = new System.Drawing.Size(116, 40);
            this.txtState.TabIndex = 13;
            // 
            // txtZip
            // 
            this.txtZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip.Enabled = false;
            this.txtZip.Location = new System.Drawing.Point(782, 150);
            this.txtZip.MaxLength = 5;
            this.txtZip.Name = "txtZip";
            this.txtZip.Size = new System.Drawing.Size(116, 40);
            this.txtZip.TabIndex = 15;
            // 
            // txtZip4
            // 
            this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
            this.txtZip4.Enabled = false;
            this.txtZip4.Location = new System.Drawing.Point(914, 150);
            this.txtZip4.MaxLength = 4;
            this.txtZip4.Name = "txtZip4";
            this.txtZip4.Size = new System.Drawing.Size(86, 40);
            this.txtZip4.TabIndex = 17;
            // 
            // txtStreetNumber
            // 
            this.txtStreetNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetNumber.Enabled = false;
            this.txtStreetNumber.Location = new System.Drawing.Point(158, 210);
            this.txtStreetNumber.Name = "txtStreetNumber";
            this.txtStreetNumber.Size = new System.Drawing.Size(71, 40);
            this.txtStreetNumber.TabIndex = 7;
            // 
            // txtApt
            // 
            this.txtApt.BackColor = System.Drawing.SystemColors.Window;
            this.txtApt.Enabled = false;
            this.txtApt.Location = new System.Drawing.Point(239, 210);
            this.txtApt.Name = "txtApt";
            this.txtApt.Size = new System.Drawing.Size(40, 40);
            this.txtApt.TabIndex = 8;
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.Enabled = false;
            this.txtName.Location = new System.Drawing.Point(158, 30);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(405, 40);
            this.txtName.TabIndex = 1;
            // 
            // txtAddress1
            // 
            this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress1.Enabled = false;
            this.txtAddress1.Location = new System.Drawing.Point(158, 90);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(405, 40);
            this.txtAddress1.TabIndex = 3;
            // 
            // txtAddress2
            // 
            this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress2.Enabled = false;
            this.txtAddress2.Location = new System.Drawing.Point(158, 150);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(405, 40);
            this.txtAddress2.TabIndex = 5;
            // 
            // txtStreetName
            // 
            this.txtStreetName.BackColor = System.Drawing.SystemColors.Window;
            this.txtStreetName.Enabled = false;
            this.txtStreetName.Location = new System.Drawing.Point(289, 210);
            this.txtStreetName.Name = "txtStreetName";
            this.txtStreetName.Size = new System.Drawing.Size(274, 40);
            this.txtStreetName.TabIndex = 9;
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(632, 217);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(102, 27);
            this.Label9.TabIndex = 18;
            this.Label9.Text = "GROUP NUMBER";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(71, 18);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "NAME";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(89, 16);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "ADDRESS 1";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 164);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(77, 18);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "ADDRESS 2";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(632, 44);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(83, 18);
            this.Label4.TabIndex = 10;
            this.Label4.Text = "CITY";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(632, 104);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(40, 22);
            this.Label5.TabIndex = 12;
            this.Label5.Text = "STATE";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(632, 164);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(26, 23);
            this.Label6.TabIndex = 14;
            this.Label6.Text = "ZIP";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(904, 164);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(5, 20);
            this.Label7.TabIndex = 16;
            this.Label7.Text = "-";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(20, 224);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(83, 20);
            this.Label8.TabIndex = 6;
            this.Label8.Text = "ST. ADDRESS";
            // 
            // txtGroupNumber
            // 
            this.txtGroupNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtGroupNumber.Location = new System.Drawing.Point(782, 210);
            this.txtGroupNumber.Name = "txtGroupNumber";
            this.txtGroupNumber.Size = new System.Drawing.Size(116, 40);
            this.txtGroupNumber.TabIndex = 2;
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(30, 614);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(196, 18);
            this.Label10.TabIndex = 2;
            this.Label10.Text = "COMMENT";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuBalance,
            this.mnuDeleteGroup,
            this.mnuSepar3,
            this.mnuAdd,
            this.mnuDelete,
            this.mnuSepar2,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSepar,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuBalance
            // 
            this.mnuBalance.Index = 0;
            this.mnuBalance.Name = "mnuBalance";
            this.mnuBalance.Text = "Show Balance";
            this.mnuBalance.Click += new System.EventHandler(this.mnuBalance_Click);
            // 
            // mnuDeleteGroup
            // 
            this.mnuDeleteGroup.Index = 1;
            this.mnuDeleteGroup.Name = "mnuDeleteGroup";
            this.mnuDeleteGroup.Text = "Delete Group";
            this.mnuDeleteGroup.Click += new System.EventHandler(this.mnuDeleteGroup_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = 2;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuAdd
            // 
            this.mnuAdd.Index = 3;
            this.mnuAdd.Name = "mnuAdd";
            this.mnuAdd.Text = "Add new account entry";
            this.mnuAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = 4;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Text = "Delete account entry";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = 5;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 6;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 7;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 8;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 9;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(134, 38);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(92, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // cmdBalance
            // 
            this.cmdBalance.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdBalance.Location = new System.Drawing.Point(933, 29);
            this.cmdBalance.Name = "cmdBalance";
            this.cmdBalance.Size = new System.Drawing.Size(106, 24);
            this.cmdBalance.TabIndex = 1;
            this.cmdBalance.Text = "Show Balance";
            this.cmdBalance.Click += new System.EventHandler(this.mnuBalance_Click);
            // 
            // cmdDeleteGroup
            // 
            this.cmdDeleteGroup.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteGroup.Location = new System.Drawing.Point(824, 29);
            this.cmdDeleteGroup.Name = "cmdDeleteGroup";
            this.cmdDeleteGroup.Size = new System.Drawing.Size(104, 24);
            this.cmdDeleteGroup.TabIndex = 2;
            this.cmdDeleteGroup.Text = "Delete Group";
            this.cmdDeleteGroup.Click += new System.EventHandler(this.mnuDeleteGroup_Click);
            // 
            // cmdAdd
            // 
            this.cmdAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAdd.Location = new System.Drawing.Point(656, 29);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(162, 24);
            this.cmdAdd.TabIndex = 3;
            this.cmdAdd.Text = "Add new account entry";
            this.cmdAdd.Click += new System.EventHandler(this.mnuAdd_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.Location = new System.Drawing.Point(493, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Size = new System.Drawing.Size(158, 24);
            this.cmdDelete.TabIndex = 4;
            this.cmdDelete.Text = "Delete account entry";
            this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // frmGroup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmGroup";
            this.ShowInTaskbar = false;
            this.Text = "Group Maintenance";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmGroup_Load);
            this.Resize += new System.EventHandler(this.frmGroup_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGroup_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtbComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccountsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBalance)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdDeleteGroup;
		private FCButton cmdBalance;
		private FCButton cmdDelete;
		private FCButton cmdAdd;
	}
}
