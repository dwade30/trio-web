﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Wisej.Web;

#if TWGNENTY
using TWGNENTY;
#endif
namespace Global
{
	public class modSignatureFile
	{
		public const int PAYROLLSIG = 2;
		public const int BUDGETARYSIG = 3;
		public const int TREASURERSIG = 1;
		public const int UTILITYSIG = 4;
		public const int COLLECTORSIG = 5;
		public const int CLERKSIG = 6;
		// ****************** D E P R E C A T E D **********************************************
		public static bool CheckSigFolder()
		{
			bool CheckSigFolder = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will check to make sure that the file structure
				// for signature files is intact, if not create it
				if (Directory.Exists("SigFile"))
				{
					CheckSigFolder = true;
				}
				else
				{
					Directory.CreateDirectory("SigFile");
					CheckSigFolder = true;
				}
				return CheckSigFolder;
			}
			catch (Exception ex)
			{
				
				CheckSigFolder = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Directory Creation Error");
			}
			return CheckSigFolder;
		}

		public static bool SaveSignatureFile(short intType, string strSigPath, bool boolShowSuccessMessage = true)
		{
			bool SaveSignatureFile = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will set the signature file path in the SignatureFile table in GN
				string strFieldFile = "";
				string strFileName = "";
				clsDRWrapper rsSave = new clsDRWrapper();
				SaveSignatureFile = false;
				// MsgBox "1"
				if (CheckSigFolder())
				{
					// check to make sure that the path passed in is valid
					// MsgBox "2"
					if (File.Exists(strSigPath))
					{
						switch (intType)
						{
							case TREASURERSIG:
								{
									// 1  - CL
									strFieldFile = "TreasurerSigPath";
									strFileName = "TWCLTREA.SIG";
									break;
								}
							case PAYROLLSIG:
								{
									// 2  - PY
									strFieldFile = "PayrollSigPath";
									strFileName = "TWPYSIG.SIG";
									break;
								}
							case BUDGETARYSIG:
								{
									// 3  - BD
									strFieldFile = "BudgetarySigPath";
									strFileName = "TWBD0000.SIG";
									break;
								}
							case UTILITYSIG:
								{
									// 4  - UT
									strFieldFile = "UtilitySigPath";
									strFileName = "TWUT0000.SIG";
									break;
								}
							case COLLECTORSIG:
								{
									// 5  - CL
									strFieldFile = "CollectorSigPath";
									strFileName = "TWCLCOLL.SIG";
									break;
								}
							case CLERKSIG:
								{
									// 6  - CK
									strFieldFile = "ClerkSigPath";
									strFileName = "TWCK0000.SIG";
									break;
								}
						}
						//end switch
						// check to see if the path passed in is in the correct directory already and
						// if the file passed in already has the correct naming convention
						// MsgBox Path.Combine(CurDir & "\SigFile", strFileName)
						if (Path.Combine(FCFileSystem.Statics.UserDataFolder + "\\SigFile", strFileName) != strSigPath)
						{
							// if they are different then copy the file into the new one
							// MsgBox "file to copy: " & strSigPath
							File.Copy(strSigPath, Path.Combine(FCFileSystem.Statics.UserDataFolder + "\\SigFile", strFileName), true);
							// MsgBox "6"
						}
						// MsgBox "7"
						rsSave.OpenRecordset("SELECT * FROM SignatureFile", "SystemSettings");
						if (!rsSave.EndOfFile())
						{
							rsSave.Edit();
						}
						else
						{
							rsSave.AddNew();
						}
						// MsgBox "8"
						// rsSave.Get_Fields(strFieldFile) = strSigPath
						rsSave.Set_Fields(strFieldFile, Path.Combine(FCFileSystem.Statics.UserDataFolder + "\\SigFile", strFileName));
						// MsgBox "9"
						if (rsSave.Update())
						{
							if (boolShowSuccessMessage)
							{
								FCMessageBox.Show("Save successful.", MsgBoxStyle.Information, "Save File");
							}
							SaveSignatureFile = true;
						}
						else
						{
							FCMessageBox.Show("Update Failed.  Save not successful.", MsgBoxStyle.Information, "Save File");
						}
					}
					else
					{
						FCMessageBox.Show("The path: " + strSigPath + "is not valid.  Save not successful.", MsgBoxStyle.Information, "Save File");
					}
				}
				else
				{
					FCMessageBox.Show("The signature file directory cannot be created.  File not saved.", MsgBoxStyle.Critical, "Error Saving File");
				}
				return SaveSignatureFile;
			}
			catch (Exception ex)
			{
				
				SaveSignatureFile = false;
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Directory Creation Error");
			}
			return SaveSignatureFile;
		}

		public static string GetSignatureFile(ref short intType)
		{
			string GetSignatureFile = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return the signature file path by checking the SignatureFile table in GN
				string strFieldFile = "";
				clsDRWrapper rsGet = new clsDRWrapper();
				if (CheckSigFolder())
				{
					switch (intType)
					{
						case TREASURERSIG:
							{
								// 1  - CL
								strFieldFile = "TreasurerSigPath";
								break;
							}
						case PAYROLLSIG:
							{
								// 2  - PY
								strFieldFile = "PayrollSigPath";
								break;
							}
						case BUDGETARYSIG:
							{
								// 3  - BD
								strFieldFile = "BudgetarySigPath";
								break;
							}
						case UTILITYSIG:
							{
								// 4  - UT
								strFieldFile = "UtilitySigPath";
								break;
							}
						case COLLECTORSIG:
							{
								// 5  - CL
								strFieldFile = "CollectorSigPath";
								break;
							}
						case CLERKSIG:
							{
								// 6  - CK
								strFieldFile = "ClerkSigPath";
								break;
							}
					}
					//end switch
					rsGet.OpenRecordset("SELECT * FROM SignatureFile", "SystemSettings");
					if (!rsGet.EndOfFile())
					{
						GetSignatureFile = FCConvert.ToString(rsGet.Get_Fields(strFieldFile));
					}
					else
					{
						GetSignatureFile = "";
					}
				}
				else
				{
					GetSignatureFile = "";
					FCMessageBox.Show("The signature file directory cannot be created.  File not found.", MsgBoxStyle.Critical, "Error Loading File");
				}
				return GetSignatureFile;
			}
			catch (Exception ex)
			{
				
				GetSignatureFile = "";
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Directory Creation Error");
			}
			return GetSignatureFile;
		}

		public static object SetupSigInformation()
		{
			object SetupSigInformation = null;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will check the SignatureFile table to get the information for CL
				clsDRWrapper rsSig = new clsDRWrapper();
				rsSig.OpenRecordset("SELECT * FROM SignatureFile", "SystemSettings");
				if (!rsSig.EndOfFile())
				{
					// CL
					if (Strings.Trim(FCConvert.ToString(rsSig.Get_Fields_String("TreasurerSigPassword")) + " ") != "")
					{
						Statics.gstrTreasurerSigPassword = modEncrypt.ToggleEncryptCode(FCConvert.ToString(rsSig.Get_Fields_String("TreasurerSigPassword")));
					}
					else
					{
						Statics.gstrTreasurerSigPassword = "3333";
					}
					if (Strings.Trim(FCConvert.ToString(rsSig.Get_Fields_String("TreasurerSigPath")) + " ") != "")
					{
						Statics.gstrTreasSigPath = FCConvert.ToString(rsSig.Get_Fields_String("TreasurerSigPath"));
					}
					else
					{
						Statics.gstrTreasSigPath = "";
					}
					// BD
					if (Strings.Trim(FCConvert.ToString(rsSig.Get_Fields_String("BudgetarySigPassword")) + " ") != "")
					{
						Statics.gstrBudgetarySigPassword = modEncrypt.ToggleEncryptCode(FCConvert.ToString(rsSig.Get_Fields_String("BudgetarySigPassword")));
					}
					else
					{
						Statics.gstrBudgetarySigPassword = "3333";
					}
					if (Strings.Trim(FCConvert.ToString(rsSig.Get_Fields_String("BudgetarySigPath")) + " ") != "")
					{
						Statics.gstrBudgetarySigPath = FCConvert.ToString(rsSig.Get_Fields_String("BudgetarySigPath"));
					}
					else
					{
						Statics.gstrBudgetarySigPath = "";
					}
					// PY
					if (Strings.Trim(FCConvert.ToString(rsSig.Get_Fields_String("PayrollSigPassword")) + " ") != "")
					{
						Statics.gstrPayrollSigPassword = modEncrypt.ToggleEncryptCode(FCConvert.ToString(rsSig.Get_Fields_String("PayrollSigPassword")));
					}
					else
					{
						Statics.gstrPayrollSigPassword = "3333";
					}
					if (Strings.Trim(FCConvert.ToString(rsSig.Get_Fields_String("PayrollSigPath")) + " ") != "")
					{
						Statics.gstrPayrollSigPath = FCConvert.ToString(rsSig.Get_Fields_String("PayrollSigPath"));
					}
					else
					{
						Statics.gstrPayrollSigPath = "";
					}
					// UT
					if (Strings.Trim(FCConvert.ToString(rsSig.Get_Fields_String("UtilitySigPassword")) + " ") != "")
					{
						Statics.gstrUtilitySigPassword = modEncrypt.ToggleEncryptCode(FCConvert.ToString(rsSig.Get_Fields_String("UtilitySigPassword")));
					}
					else
					{
						Statics.gstrUtilitySigPassword = "3333";
					}
					if (Strings.Trim(FCConvert.ToString(rsSig.Get_Fields_String("UtilitySigPath")) + " ") != "")
					{
						Statics.gstrUtilitySigPath = FCConvert.ToString(rsSig.Get_Fields_String("UtilitySigPath"));
					}
					else
					{
						Statics.gstrUtilitySigPath = "";
					}
					// CL Collector
					if (Strings.Trim(FCConvert.ToString(rsSig.Get_Fields_String("CollectorSigPassword")) + " ") != "")
					{
						Statics.gstrCollectorSigPassword = modEncrypt.ToggleEncryptCode(FCConvert.ToString(rsSig.Get_Fields_String("CollectorSigPassword")));
					}
					else
					{
						Statics.gstrCollectorSigPassword = "3333";
					}
					if (Strings.Trim(FCConvert.ToString(rsSig.Get_Fields_String("CollectorSigPath")) + " ") != "")
					{
						Statics.gstrCollectorSigPath = FCConvert.ToString(rsSig.Get_Fields_String("CollectorSigPath"));
					}
					else
					{
						Statics.gstrCollectorSigPath = "";
					}
					// CK
					if (Strings.Trim(FCConvert.ToString(rsSig.Get_Fields_String("ClerkSigPassword")) + " ") != "")
					{
						Statics.gstrClerkSigPassword = modEncrypt.ToggleEncryptCode(FCConvert.ToString(rsSig.Get_Fields_String("ClerkSigPassword")));
					}
					else
					{
						Statics.gstrClerkSigPassword = "3333";
					}
					if (Strings.Trim(FCConvert.ToString(rsSig.Get_Fields_String("ClerkSigPath")) + " ") != "")
					{
						Statics.gstrClerkSigPath = FCConvert.ToString(rsSig.Get_Fields_String("ClerkSigPath"));
					}
					else
					{
						Statics.gstrClerkSigPath = "";
					}
					// Not Yet
				}
				else
				{
					rsSig.AddNew();
					rsSig.Update();
				}
				return SetupSigInformation;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Signature Info");
			}
			return SetupSigInformation;
		}
		// ***********************************************************************************
		public static string GetSigFileName_26(int lngID, string strModule, int lngType)
		{
			return GetSigFileName(ref lngID, ref strModule, ref lngType);
		}

		public static string GetSigFileName(ref int lngID, ref string strModule, ref int lngType)
		{
			string GetSigFileName = "";
			// returns the encrypted filename
			// filename is the 3 digit hex of the id plus the 2 digit hex values of the 2 characters in strModule plus the type converted to an alpha character
			// if we ever have more than 26 types, this will have to change
			string strFileName;
			try
			{
				// On Error GoTo ErrorHandler
				GetSigFileName = "";
				if (strModule.Length < 2)
					return GetSigFileName;
				strFileName = Strings.Right("00" + Convert.ToString(FCConvert.ToInt32(Convert.ToByte(Strings.Left(Strings.UCase(strModule), 1)[0])), 16).ToUpper(), 2) + Strings.Right("00" + Convert.ToString(FCConvert.ToInt32(Convert.ToByte(Strings.Mid(Strings.UCase(strModule), 2, 1)[0])), 16).ToUpper(), 2);
				strFileName = Strings.Right("000" + Convert.ToString(FCConvert.ToInt32(lngID), 16).ToUpper(), 3) + strFileName;
				strFileName += FCConvert.ToString(Convert.ToChar(lngType + 65));
				strFileName += ".TSF";
				// trio signature file
				GetSigFileName = strFileName;
				return GetSigFileName;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In GetSigFileName", MsgBoxStyle.Critical, "Error");
			}
			return GetSigFileName;
		}

		public static string GetSigFileFromID(ref int lngID)
		{
			string GetSigFileFromID = "";
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				GetSigFileFromID = "";
				rsLoad.OpenRecordset("Select * from signatures where id = " + FCConvert.ToString(lngID), "SystemSettings", 0, false, 0, false, "", false);
				if (rsLoad.EndOfFile())
				{
					return GetSigFileFromID;
				}
				// TODO: Check the table for the column [type] and replace with corresponding Get_Field method
				GetSigFileFromID = GetSigFileName_26(FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id")), FCConvert.ToString(rsLoad.Get_Fields_String("Module")), FCConvert.ToInt32(rsLoad.Get_Fields("type")));
				return GetSigFileFromID;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In GetSigFileFromID", MsgBoxStyle.Critical, "Error");
			}
			return GetSigFileFromID;
		}

		public static bool SigFileExists(ref int lngID)
		{
			bool SigFileExists = false;
			SigFileExists = false;
			if (lngID > 0)
			{
				string strTemp = "";
				strTemp = GetSigFileFromID(ref lngID);
				if (strTemp != "")
				{
					//FC:FINAL:DSE:#1250 Fix file path
					//if (File.Exists(strTemp))
					if (File.Exists(Path.Combine(fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder, strTemp)))
					{
						SigFileExists = true;
					}
				}
			}
			return SigFileExists;
		}

		public static bool UpdateSigFileInfo(ref int lngID, string strModule = "", int lngType = -1, string strName = "", string strPassword = "", string strModSpecific = "", int lngModSpecificID = 0)
		{
			bool UpdateSigFileInfo = false;
			// updates the record with any data that is passed to it
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				UpdateSigFileInfo = false;
				if (lngID > 0)
				{
					rsSave.OpenRecordset("select * from signatures where id = " + FCConvert.ToString(lngID), "SystemSettings");
					if (!rsSave.EndOfFile())
					{
						rsSave.Edit();
					}
					else
					{
						return UpdateSigFileInfo;
					}
					if (strModule != string.Empty)
					{
						rsSave.Set_Fields("module", strModule);
					}
					if (lngType != -1)
					{
						rsSave.Set_Fields("type", lngType);
					}
					if (strName != string.Empty)
					{
						rsSave.Set_Fields("name", strName);
					}
					if (strPassword != string.Empty)
					{
						rsSave.Set_Fields("password", modEncrypt.ToggleEncryptCode(strPassword));
					}
					if (strModSpecific != string.Empty)
					{
						rsSave.Set_Fields("modspecific", strModSpecific);
					}
					if (lngModSpecificID != 0)
					{
						rsSave.Set_Fields("ModSpecificID", lngModSpecificID);
					}
					rsSave.Update();
				}
				else
				{
					return UpdateSigFileInfo;
				}
				UpdateSigFileInfo = true;
				return UpdateSigFileInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In UpdateSigFileInfo", MsgBoxStyle.Critical, "Error");
			}
			return UpdateSigFileInfo;
		}

		public static int SaveSigFileInfo(ref int lngID, ref string strModule, ref int lngType, ref string strName, ref string strPassword, string strModSpecific = "", int lngModSpecificID = 0)
		{
			int SaveSigFileInfo = 0;
			// returns -1 for failure or the id if successful
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngSaveID;
				SaveSigFileInfo = -1;
				if (lngID > 0)
				{
					rsSave.OpenRecordset("select * from signatures where id = " + FCConvert.ToString(lngID), "SystemSettings");
					if (!rsSave.EndOfFile())
					{
						lngSaveID = lngID;
						rsSave.Edit();
					}
					else
					{
						return SaveSigFileInfo;
					}
				}
				else
				{
					rsSave.OpenRecordset("select * from signatures where id = 0", "SystemSettings");
					rsSave.AddNew();
					// lngSaveID = rsSave.Get_Fields("id")
				}
				rsSave.Set_Fields("module", strModule);
				rsSave.Set_Fields("type", lngType);
				rsSave.Set_Fields("Name", strName);
				rsSave.Set_Fields("Password", modEncrypt.ToggleEncryptCode(strPassword));
				if (strModSpecific != string.Empty)
				{
					rsSave.Set_Fields("ModSpecific", strModSpecific);
				}
				if (lngModSpecificID != 0)
				{
					rsSave.Set_Fields("ModSpecificID", lngModSpecificID);
				}
				rsSave.Update();
				lngSaveID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("id"));
				SaveSigFileInfo = lngSaveID;
				return SaveSigFileInfo;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In SaveSigFileInfo", MsgBoxStyle.Critical, "Error");
			}
			return SaveSigFileInfo;
		}

		public static string GetSignaturePassword(ref int lngID)
		{
			string GetSignaturePassword = "";
			// returns the unencrypted password or ERROR for failure
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				GetSignaturePassword = "ERROR";
				rsLoad.OpenRecordset("Select * from signatures where id = " + FCConvert.ToString(lngID), "SystemSettings");
				if (rsLoad.EndOfFile())
				{
					return GetSignaturePassword;
				}
				GetSignaturePassword = modEncrypt.ToggleEncryptCode(FCConvert.ToString(rsLoad.Get_Fields_String("password")));
				return GetSignaturePassword;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In GetSignaturePassword", MsgBoxStyle.Critical, "Error");
			}
			return GetSignaturePassword;
		}

		public class StaticVariables
		{
			// ********************************************************
			// LAST UPDATED   :               11/09/2005              *
			// MODIFIED BY    :               Jim Bertolino           *
			// *
			// DATE           :               06/30/2003              *
			// WRITTEN BY     :               Jim Bertolino           *
			// *
			// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
			// ********************************************************
			//=========================================================
			public string gstrTreasurerSigPassword = "";
			// for RE/PP Collections
			public string gstrTreasSigPath = "";
			public string gstrCollectorSigPassword = "";
			// for RE/PP Collections
			public string gstrCollectorSigPath = "";
			public string gstrClerkSigPassword = "";
			// for CK
			public string gstrClerkSigPath = "";
			public string gstrPayrollSigPassword = "";
			// for Payroll
			public string gstrPayrollSigPath = "";
			public string gstrBudgetarySigPassword = "";
			// for Budgetary
			public string gstrBudgetarySigPath = "";
			public string gstrUtilitySigPassword = "";
			// for Utility Billing
			public string gstrUtilitySigPath = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
