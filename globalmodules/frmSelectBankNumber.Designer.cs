//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWAR0000
using TWAR0000;


#elif TWBD0000
using TWBD0000;


#elif TWCR0000
using TWCR0000;


#elif TWFA0000
using TWFA0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmSelectBankNumber.
	/// </summary>
	partial class frmSelectBankNumber : BaseForm
	{
		public FCGrid vsBankNumber;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.vsBankNumber = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBankNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 294);
			this.BottomPanel.Size = new System.Drawing.Size(530, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsBankNumber);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(530, 234);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(530, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(240, 30);
			this.HeaderText.Text = "Enter Bank Numbers";
			// 
			// vsBankNumber
			// 
			this.vsBankNumber.AllowSelection = false;
			this.vsBankNumber.AllowUserToResizeColumns = false;
			this.vsBankNumber.AllowUserToResizeRows = false;
			this.vsBankNumber.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsBankNumber.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsBankNumber.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsBankNumber.BackColorBkg = System.Drawing.Color.Empty;
			this.vsBankNumber.BackColorFixed = System.Drawing.Color.Empty;
			this.vsBankNumber.BackColorSel = System.Drawing.Color.Empty;
			this.vsBankNumber.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsBankNumber.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsBankNumber.ColumnHeadersHeight = 30;
			this.vsBankNumber.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsBankNumber.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsBankNumber.DragIcon = null;
			this.vsBankNumber.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsBankNumber.FixedCols = 2;
			this.vsBankNumber.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsBankNumber.FrozenCols = 0;
			this.vsBankNumber.GridColor = System.Drawing.Color.Empty;
			this.vsBankNumber.GridColorFixed = System.Drawing.Color.Empty;
			this.vsBankNumber.Location = new System.Drawing.Point(30, 70);
			this.vsBankNumber.Name = "vsBankNumber";
			this.vsBankNumber.ReadOnly = true;
			this.vsBankNumber.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsBankNumber.RowHeightMin = 0;
			this.vsBankNumber.Rows = 1;
			this.vsBankNumber.ScrollTipText = null;
			this.vsBankNumber.ShowColumnVisibilityMenu = false;
			this.vsBankNumber.Size = new System.Drawing.Size(470, 133);
			this.vsBankNumber.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsBankNumber.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsBankNumber.TabIndex = 1;
			this.vsBankNumber.CurrentCellChanged += new System.EventHandler(this.vsBankNumber_RowColChange);
			this.vsBankNumber.Click += new System.EventHandler(this.vsBankNumber_ClickEvent);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(480, 21);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "PLEASE SELECT THE BANK NUMBER EACH CASH ACCOUNT IS ASSOCIATED WITH";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(217, 30);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(110, 48);
			this.cmdProcess.TabIndex = 0;
			this.cmdProcess.Text = "Process";
			this.cmdProcess.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmSelectBankNumber
			// 
			this.ClientSize = new System.Drawing.Size(530, 402);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmSelectBankNumber";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Enter Bank Numbers";
			this.Load += new System.EventHandler(this.frmSelectBankNumber_Load);
			this.Activated += new System.EventHandler(this.frmSelectBankNumber_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSelectBankNumber_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBankNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdProcess;
	}
}