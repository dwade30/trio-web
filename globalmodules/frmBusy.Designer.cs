﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace Global
{
	/// <summary>
	/// Summary description for frmBusy.
	/// </summary>
	partial class frmBusy : BaseForm
	{
		public fecherFoundation.FCLabel lblPleaseWait;
		public fecherFoundation.FCLabel lblMessage;
		public fecherFoundation.FCPictureBox Image1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBusy));
			this.lblPleaseWait = new fecherFoundation.FCLabel();
			this.lblMessage = new fecherFoundation.FCLabel();
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 285);
			this.BottomPanel.Size = new System.Drawing.Size(448, 0);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.lblPleaseWait);
			this.ClientArea.Controls.Add(this.lblMessage);
			this.ClientArea.Controls.Add(this.Image1);
			this.ClientArea.Location = new System.Drawing.Point(0, 0);
			this.ClientArea.Size = new System.Drawing.Size(448, 285);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(448, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(66, 30);
			this.HeaderText.Text = "Busy";
			// 
			// lblPleaseWait
			// 
			this.lblPleaseWait.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.lblPleaseWait.Location = new System.Drawing.Point(34, 118);
			this.lblPleaseWait.Name = "lblPleaseWait";
			this.lblPleaseWait.Size = new System.Drawing.Size(373, 24);
			this.lblPleaseWait.Text = "PLEASE WAIT";
			this.lblPleaseWait.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblMessage
			// 
			this.lblMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.lblMessage.Location = new System.Drawing.Point(34, 162);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(373, 34);
			this.lblMessage.TabIndex = 1;
			this.lblMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Image1
			// 
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
			this.Image1.Location = new System.Drawing.Point(187, 30);
			this.Image1.Name = "Image1";
			this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
			this.Image1.Size = new System.Drawing.Size(68, 68);
			this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.Image1.TabIndex = 2;
			this.Image1.Visible = false;
			// 
			// frmBusy
			// 
			this.ClientSize = new System.Drawing.Size(448, 232);
			this.ControlBox = false;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.None;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.Name = "frmBusy";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "";
			this.Load += new System.EventHandler(this.frmBusy_Load);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
