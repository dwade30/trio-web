﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using Microsoft.Ajax.Utilities;

namespace Global
{
	public class cJournalInfo
	{
		//=========================================================
		private string strPostedDate = "";

		public bool IsCreditMemoCorrection { get; set; } = false;
		public bool IsCreditMemo { get; set; } = false;
		public bool IsOutOfBalance { get; set; } = false;
		public bool IsOutOfBalanceByFund { get; set; } = false;
		public bool ContainsBadAccount { get; set; } = false;
		public int Id { get; set; } = 0;
		public int JournalNumber { get; set; } = 0;
		public string Description { get; set; } = "";
		public int Period { get; set; } = 0;
		public bool IsSelected { get; set; } = false;
		public string JournalType { get; set; } = "";
		public double TotalAmount { get; set; } = 0;
		public int NumberOfEntries { get; set; } = 0;
		public string PostedDate
		{
			get { return strPostedDate; }
			set
			{
				if (Information.IsDate(value))
				{
					strPostedDate = value;
				}
				else
				{
					strPostedDate = "";
				}
			}
		} 

		public string Status { get; set; } = "";
		public string StatusDescription
		{
			get
			{
				string strReturn = "";
				if (Strings.LCase(Status) == "e")
				{
					if (Strings.LCase(JournalType) == "ap")
					{
						strReturn = "Entered";
					}
					else
					{
						strReturn = "Ready to Post";
					}
				}
				else if (Strings.LCase(Status) == "p")
				{
					strReturn = "Posted";
				}
				else if (Strings.LCase(Status) == "r")
				{
					strReturn = "Check Register";
				}
				else if (Strings.LCase(Status) == "w")
				{
					//strReturn = "Warrant";
					strReturn = "Ready to Post";
				}
				else if (Strings.LCase(Status) == "x")
				{
					//strReturn = "Warrant Recap";
					strReturn = "Ready to Post";
				}
				else if (Strings.LCase(Status) == "v")
				{
					strReturn = "Warrant Preview";
				}
				else if (Strings.LCase(Status) == "c")
				{
					strReturn = "Checks";
				}

				if (ContainsBadAccount)
				{
					strReturn += "  (BA)";
				}
				else if (IsOutOfBalance)
				{
					strReturn += "  (OB)";
				}
				else if (IsOutOfBalanceByFund)
				{
					strReturn += "  (OBF)";
				}

				return strReturn;
			}
		}

		public string JournalTypeDescription
		{
			get
			{
				string strReturn = "";
				if (Strings.LCase(JournalType) == "ap")
				{
					strReturn = "Accounts Payable";
				}
				else if (Strings.LCase(JournalType) == "py")
				{
					strReturn = "Payroll";
				}
				else if (Strings.LCase(JournalType) == "gj")
				{
					strReturn = "General Journal";
				}
				else if (Strings.LCase(JournalType) == "ac")
				{
					if (!IsCreditMemo)
					{
						strReturn = "AP Correction";
					}
					else
					{
						strReturn = "Credit Memo";
					}
				}
				else if (Strings.LCase(JournalType) == "cm")
				{
					strReturn = "Credit Memo";
				}
				else if (Strings.LCase(JournalType) == "en")
				{
					strReturn = "Encumbrance";
				}
				else if (Strings.LCase(JournalType) == "cw")
				{
					strReturn = "Cash Receipts";
				}
				else if (Strings.LCase(JournalType) == "cr")
				{
					strReturn = "Cash Receipts - Manual";
				}
				else if (Strings.LCase(JournalType) == "cd")
				{
					strReturn = "Cash Disbursement";
				}

				return strReturn;
			}
		}
	}
}
