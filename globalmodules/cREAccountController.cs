﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using fecherFoundation;
using Global;
//using Microsoft.VisualBasic;

namespace TWXF0000
{
	public class cREAccountController
	{
		//=========================================================
		private string strConnectionName = "";
		private string strLastError = string.Empty;
		private int lngLastError;

		private void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
		public object HadError
		{
			get
			{
				object HadError = null;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public bool LoadAccount(ref cREAccount tAccount, int lngAccountNumber)
		{
			bool LoadAccount = false;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				LoadAccount = false;
				if (tAccount == null)
				{
					tAccount = new cREAccount();
				}
				tAccount.Clear();
				if (lngAccountNumber > 0)
				{
					clsDRWrapper rsLoad = new clsDRWrapper();
					short x;
					rsLoad.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAccountNumber) + " and rscard = 1", modGlobalVariables.Statics.strREDatabase);
					if (!rsLoad.EndOfFile())
					{
						tAccount.Account = lngAccountNumber;
						tAccount.ID = rsLoad.Get_Fields_Int32("id");
						tAccount.AccountID = rsLoad.Get_Fields_String("AccountID");
						if (!fecherFoundation.FCUtils.IsNull(rsLoad.Get_Fields("inbankruptcy")))
						{
							tAccount.InBankruptcy = FCConvert.CBool(rsLoad.Get_Fields("InBankruptcy"));
						}
						if (!fecherFoundation.FCUtils.IsNull(rsLoad.Get_Fields("TaxAcquired")))
						{
							tAccount.TaxAcquired = FCConvert.CBool(rsLoad.Get_Fields("TaxAcquired"));
						}
						if (!fecherFoundation.FCUtils.IsNull(rsLoad.Get_Fields("revocabletrust")))
						{
							tAccount.RevocableTrust = FCConvert.CBool(rsLoad.Get_Fields("revocabletrust"));
						}
						for (x = 1; x <= 3; x++)
						{
							if (Conversion.Val(rsLoad.Get_Fields("riexemptcd" + FCConvert.ToString(x))) > 0)
							{
								tAccount.Set_ExemptCode(x, rsLoad.Get_Fields_Int16("riexemptcd" + FCConvert.ToString(x)));
								tAccount.Set_ExemptPct(x, Conversion.Val(rsLoad.Get_Fields("ExemptPct" + FCConvert.ToString(x))));
								tAccount.Set_ExemptVal(x, Conversion.Val(rsLoad.Get_Fields("ExemptVal" + FCConvert.ToString(x))));
							}
						}
						// x
						tAccount.MapLot = rsLoad.Get_Fields_String("rsmaplot");
						tAccount.OwnerID = Conversion.Val(rsLoad.Get_Fields("OwnerPartyID"));
						tAccount.SecOwnerID = Conversion.Val(rsLoad.Get_Fields("SecOwnerPartyID"));
						tAccount.TranCode = Conversion.Val(rsLoad.Get_Fields("ritrancode"));
						tAccount.PreviousOwner = rsLoad.Get_Fields_String("rspreviousmaster");
						tAccount.SaleFinancing = Conversion.Val(rsLoad.Get_Fields("pisalefinancing"));
						tAccount.SaleType = Conversion.Val(rsLoad.Get_Fields("pisaletype"));
						tAccount.SaleValidity = Conversion.Val(rsLoad.Get_Fields("pisalevalidity"));
						tAccount.SaleVerified = Conversion.Val(rsLoad.Get_Fields("pisaleverified"));
						tAccount.SalePrice = Conversion.Val(rsLoad.Get_Fields("pisaleprice"));
						if (Information.IsDate(rsLoad.Get_Fields("saledate")))
						{
							if (Convert.ToDateTime(rsLoad.Get_Fields("saledate")).ToOADate() != 0)
							{
								tAccount.SaleDate = Strings.Format(rsLoad.Get_Fields("saledate"), "MM/dd/yyyy");
							}
						}
						if (tAccount.OwnerID > 0 || tAccount.SecOwnerID > 0)
						{
							cPartyController tCont = new cPartyController();
							if (tAccount.OwnerID > 0)
							{
								cParty temp = tAccount.OwnerParty;
								tCont.LoadParty(ref temp, tAccount.OwnerID);
							}
							if (tAccount.SecOwnerID > 0)
							{
								cParty temp = tAccount.SecOwnerParty;
								tCont.LoadParty(ref temp, tAccount.SecOwnerID);
							}
						}
					}
				}
				LoadAccount = true;
				return LoadAccount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = fecherFoundation.Information.Err().Number;
				strLastError = fecherFoundation.Information.Err().Description;
			}
			return LoadAccount;
		}

		public object SaveAccount(ref cREAccount tAccount)
		{
			object SaveAccount = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from master where rsaccount = " + tAccount.Account, strConnectionName);
				while (!rsSave.EndOfFile())
				{
					rsSave.Set_Fields("OwnerPartyID", tAccount.OwnerID);
					rsSave.Set_Fields("SecOwnerPartyID", tAccount.SecOwnerID);
					rsSave.Set_Fields("RSMaplot", tAccount.MapLot);
					rsSave.Set_Fields("RevocableTrust", tAccount.RevocableTrust);
					rsSave.Set_Fields("InBankruptcy", tAccount.InBankruptcy);
					rsSave.Set_Fields("TaxAcquired", tAccount.TaxAcquired);
					rsSave.Set_Fields("AccountID", tAccount.AccountID);
					rsSave.Set_Fields("ritrancode", tAccount.TranCode);
					rsSave.Set_Fields("rspreviousmaster", tAccount.PreviousOwner);
					if (Information.IsDate(tAccount.SaleDate))
					{
						rsSave.Set_Fields("SaleDate", tAccount.SaleDate);
					}
					rsSave.Set_Fields("pisalefinancing", tAccount.SaleFinancing);
					rsSave.Set_Fields("pisaletype", tAccount.SaleType);
					rsSave.Set_Fields("pisalevalidity", tAccount.SaleValidity);
					rsSave.Set_Fields("pisaleverified", tAccount.SaleVerified);
					rsSave.Set_Fields("piSalePrice", tAccount.SalePrice);
					if (FCConvert.ToInt32(rsSave.Get_Fields("Rscard")) == 1)
					{
						short x;
						for (x = 1; x <= 3; x++)
						{
							rsSave.Set_Fields("riexemptcd" + x, tAccount.Get_ExemptCode(x));
							rsSave.Set_Fields("ExemptPct" + x, tAccount.Get_ExemptPct(x));
							rsSave.Set_Fields("ExemptVal" + x, tAccount.Get_ExemptVal(x));
						}
						// x
					}
					rsSave.Update();
					rsSave.MoveNext();
				}
				return SaveAccount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				lngLastError = fecherFoundation.Information.Err().Number;
				strLastError = fecherFoundation.Information.Err().Description;
			}
			return SaveAccount;
		}

		public cREAccountController() : base()
		{
			strConnectionName = "RealEstate";
		}

		public string ConnectionName
		{
			set
			{
				strConnectionName = value;
			}
			get
			{
				string ConnectionName = "";
				ConnectionName = strConnectionName;
				return ConnectionName;
			}
		}

		public cREFullAccount LoadREFullAccountByAccount(int lngAccount)
		{
			cREFullAccount LoadREFullAccountByAccount = null;
			ClearErrors();
			cREFullAccount returnAccount = new cREFullAccount();
			clsDRWrapper rsLoad = new clsDRWrapper();
			LoadREFullAccountByAccount = null;
			rsLoad.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(lngAccount) + " and rscard = 1", strConnectionName);
			if (!rsLoad.EndOfFile())
			{
				FillREFullAccount(ref returnAccount, ref rsLoad);
				FillCards(ref returnAccount);
				returnAccount.IsUpdated = false;
				LoadREFullAccountByAccount = returnAccount;
			}
			return LoadREFullAccountByAccount;
			ErrorHandler:
			;
			SetError(fecherFoundation.Information.Err().Number, fecherFoundation.Information.Err().Description);
			return LoadREFullAccountByAccount;
		}

		public cREFullAccount LoadREFullAccountWithPartiesByAccount(int lngAccount)
		{
			cREFullAccount LoadREFullAccountWithPartiesByAccount = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				return LoadREFullAccountWithPartiesByAccount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err().Number, fecherFoundation.Information.Err().Description);
			}
			return LoadREFullAccountWithPartiesByAccount;
		}

		private void FillREFullAccount(ref cREFullAccount rAccount, ref clsDRWrapper rs)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				if (!(rAccount == null))
				{
					if (!(rs == null))
					{
						short x;
						rAccount.AccountID = rs.Get_Fields_String("AccountID");
						rAccount.AccountNumber = rs.Get_Fields_Int32("rsaccount");
						rAccount.InBankruptcy = FCConvert.CBool(rs.Get_Fields("InBankruptcy"));
						rAccount.RevocableTrust = FCConvert.CBool(rs.Get_Fields("RevocableTrust"));
						rAccount.MapLot = rs.Get_Fields_String("rsmaplot");
						rAccount.OwnerID = rs.Get_Fields_Int32("OwnerPartyID");
						rAccount.SecondOwnerID = rs.Get_Fields_Int32("SecOwnerPartyID");
						rAccount.TaxAcquired = FCConvert.CBool(rs.Get_Fields("taxacquired"));
						rAccount.IsInactive = FCConvert.CBool(rs.Get_Fields("rsdeleted"));
						rAccount.TranCode = rs.Get_Fields_Int32("ritrancode");
						rAccount.SaleInformation.FinancingCode = Conversion.Val(rs.Get_Fields("pisalefinancing"));
						rAccount.SaleInformation.Price = Conversion.Val(rs.Get_Fields("pisaleprice"));
						rAccount.SaleInformation.SaleTypeCode = Conversion.Val(rs.Get_Fields("pisaletype"));
						rAccount.SaleInformation.ValidityCode = Conversion.Val(rs.Get_Fields("pisalevalidity"));
						rAccount.SaleInformation.VerificationCode = Conversion.Val(rs.Get_Fields("pisaleverified"));
						if (Information.IsDate(rs.Get_Fields("saledate")))
						{
							if (Convert.ToDateTime(rs.Get_Fields("Saledate")).ToOADate() != 0)
							{
								rAccount.SaleInformation.SaleDate = rs.Get_Fields_String("saledate");
							}
							else
							{
								rAccount.SaleInformation.SaleDate = "";
							}
						}
						else
						{
							rAccount.SaleInformation.SaleDate = "";
						}
						cREPropertyExemption propExempt;
						rAccount.Exemptions.ClearList();
						for (x = 1; x <= 3; x++)
						{
							if (Conversion.Val(rs.Get_Fields("riexemptcd" + FCConvert.ToString(x))) > 0)
							{
								propExempt = new cREPropertyExemption();
								propExempt = rs.Get_Fields("riexemptcd" + FCConvert.ToString(x));
								propExempt.ExemptValue = Conversion.Val(rs.Get_Fields("ExemptVal" + FCConvert.ToString(x)));
								propExempt.Percentage = Conversion.Val(rs.Get_Fields("ExemptPct" + FCConvert.ToString(x)));
								rAccount.Exemptions.AddItem(propExempt);
							}
						}
						// x
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err().Number, fecherFoundation.Information.Err().Description);
			}
		}

		private void FillCards(ref cREFullAccount rAccount)
		{
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				clsDRWrapper rs = new clsDRWrapper();
				rs.OpenRecordset("select * from master where rsaccount = " + rAccount.AccountNumber + " order by rscard");
				cRECard rCard;
				rAccount.Cards.ClearList();
				int x;
				cRELandItem lItem;
				while (!rs.EndOfFile())
				{
					rCard = new cRECard();
					rCard.BillingValues.Building = Conversion.Val(rs.Get_Fields("LastBldgVal"));
					rCard.BillingValues.Land = Conversion.Val(rs.Get_Fields("LastLandVal"));
					rCard.BillingValues.Exemption = Conversion.Val(rs.Get_Fields("rlexemption"));
					rCard.Land.ClearList();
					for (x = 1; x <= 7; x++)
					{
						lItem = new cRELandItem();
						lItem.Influence = Conversion.Val(rs.Get_Fields("piland" + FCConvert.ToString(x) + "Inf"));
						lItem.InfluenceCode = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("piland" + FCConvert.ToString(x) + "InfCode"))));
						lItem.LandCode = FCConvert.ToInt32(Math.Round(Conversion.Val(rs.Get_Fields("PIland" + FCConvert.ToString(x) + "Type"))));
						lItem.LandNumber = x;
						lItem.UnitsA = FCConvert.ToString(rs.Get_Fields("piland" + FCConvert.ToString(x) + "UnitsA"));
						lItem.UnitsB = FCConvert.ToString(rs.Get_Fields("piland" + FCConvert.ToString(x) + "UnitsB"));
						lItem.IsUpdated = false;
						rCard.Land.AddItem(lItem);
					}
					// x
					rCard.IsUpdated = false;
					rAccount.Cards.AddItem(rCard);
					rs.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				SetError(fecherFoundation.Information.Err().Number, fecherFoundation.Information.Err().Description);
			}
		}

		private void SetError(int lngError, string strError)
		{
			strLastError = strError;
			lngLastError = lngError;
		}
	}
}
