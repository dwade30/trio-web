﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arBatchListing : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/26/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               05/10/2006              *
		// *
		// This report will pull the payments that will be made   *
		// in a batch payment from the grid and print them so     *
		// a town can see what they have done on paper.           *
		// ********************************************************
		int lngRow;
		string strLastYearText = "";
		bool boolDone;
		double dblSumPrin;
		double dblSumInt;
		double dblSumCost;
		double dblSumTotal;

		public arBatchListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static arBatchListing InstancePtr
		{
			get
			{
				return (arBatchListing)Sys.GetInstance(typeof(arBatchListing));
			}
		}

		protected arBatchListing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public void Init(int lngPassYear, DateTime dtPassDateOfPayment, DateTime dtInterestDate)
		{
			fldYear.Text = lngPassYear.ToString();
			fldDate.Text = Strings.Format(dtPassDateOfPayment, "MM/dd/yyyy");
			fldIntDate.Text = Strings.Format(dtInterestDate, "MM/dd/yyyy");
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngRow = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			lngRow += 1;
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			double dblTemp = 0;
			// this will print the row from the grid
			if (lngRow >= 0 && lngRow <= frmCLGetAccount.InstancePtr.vsBatch.Rows - 1)
			{
				// check for the signal to exit the loop
				// figure out what type of line it is
				HideFields_2(false);
				// Account Number
				fldAcct.Text = frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchAcct);
				// Name
				fldName.Text = Strings.Trim(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchName));
				// Principal
				dblTemp = Conversion.Val(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchPrin));
				fldPrincipal.Text = Strings.Format(dblTemp, "#,##0.00");
				dblSumPrin += dblTemp;
				// Interest
				dblTemp = Conversion.Val(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchInt));
				fldInterest.Text = Strings.Format(dblTemp, "#,##0.00");
				dblSumInt += dblTemp;
				// Costs
				dblTemp = Conversion.Val(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchCost));
				fldCosts.Text = Strings.Format(dblTemp, "#,##0.00");
				dblSumCost += dblTemp;
				// Total
				dblTemp = Conversion.Val(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchTotal));
				fldTotal.Text = Strings.Format(dblTemp, "#,##0.00");
				dblSumTotal += dblTemp;
			}
			else
			{
				boolDone = true;
				HideFields_2(true);
				fldAcct.Text = "";
				fldName.Text = "";
				fldPrincipal.Text = "";
				fldInterest.Text = "";
				fldCosts.Text = "";
				fldTotal.Text = "";
			}
		}

		private void HideFields_2(bool boolHide)
		{
			HideFields(ref boolHide);
		}

		private void HideFields(ref bool boolHide)
		{
			// this sub will either hide or show the fields depending on which variable is shown
			fldAcct.Visible = !boolHide;
			fldName.Visible = !boolHide;
			fldPrincipal.Visible = !boolHide;
			fldInterest.Visible = !boolHide;
			fldCosts.Visible = !boolHide;
			fldTotal.Visible = !boolHide;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (lngRow == 2)
			{
				lblTotalTitle.Text = "Total for " + FCConvert.ToString(lngRow - 1) + " account:";
			}
			else
			{
				lblTotalTitle.Text = "Total for " + FCConvert.ToString(lngRow - 1) + " accounts:";
			}
			fldTotalPrin.Text = Strings.Format(dblSumPrin, "#,##0.00");
			fldTotalInt.Text = Strings.Format(dblSumInt, "#,##0.00");
			fldTotalCost.Text = Strings.Format(dblSumCost, "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblSumTotal, "#,##0.00");
		}

		private void arBatchListing_Load(object sender, System.EventArgs e)
		{
		}
	}
}
