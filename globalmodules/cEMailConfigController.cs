﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;

namespace Global
{
	public class cEMailConfigController
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}
		// VBto upgrade warning: 'Return' As Variant --> As bool
		public bool HadError
		{
			get
			{
				bool HadError = false;
				HadError = lngLastError != 0;
				return HadError;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cEmailConfig GetConfigurationByUserID(int lngID)
		{
			cEmailConfig GetConfigurationByUserID = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				cEmailConfig eConfig = new cEmailConfig();
				rs.OpenRecordset("select * from emailconfiguration where userid = " + FCConvert.ToString(lngID), "SystemSettings");
				if (!rs.EndOfFile())
				{
					eConfig.LoginDomain = "";
					// not using yet
					eConfig.Password = FCConvert.ToString(rs.Get_Fields_String("Password"));
					eConfig.SMTPHost = FCConvert.ToString(rs.Get_Fields_String("Serveraddress"));
					eConfig.SMTPPort = FCConvert.ToInt32(rs.Get_Fields_Int32("serverport"));
					eConfig.UserName = FCConvert.ToString(rs.Get_Fields_String("EmailUser"));
					eConfig.UseSSL = FCConvert.ToBoolean(rs.Get_Fields_Boolean("UseSSL"));
					eConfig.UseTransportLayerSecurity = FCConvert.ToBoolean(rs.Get_Fields_Boolean("UseTLS"));
					eConfig.DefaultFromAddress = FCConvert.ToString(rs.Get_Fields_String("UserEmail"));
					eConfig.DefaultFromName = FCConvert.ToString(rs.Get_Fields_String("FromName"));
					eConfig.AuthCode = FCConvert.ToInt16(rs.Get_Fields_Int32("AuthorizationType"));
					// TODO: Check the table for the column [UserID] and replace with corresponding Get_Field method
					eConfig.UserID = FCConvert.ToInt32(rs.Get_Fields("UserID"));
					eConfig.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					eConfig.IsUpdated = false;
				}
				else if (lngID == 0)
				{
					eConfig = new cEmailConfig();
				}
				GetConfigurationByUserID = eConfig;
				return GetConfigurationByUserID;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetConfigurationByUserID;
		}

		public cEmailConfig GetConfiguration(int lngID)
		{
			cEmailConfig GetConfiguration = null;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				cEmailConfig eConfig = new cEmailConfig();
				rs.OpenRecordset("select * from emailconfiguration where id = " + FCConvert.ToString(lngID), "SystemSettings");
				if (!rs.EndOfFile())
				{
					eConfig.LoginDomain = "";
					// not using yet
					eConfig.Password = FCConvert.ToString(rs.Get_Fields_String("Password"));
					eConfig.SMTPHost = FCConvert.ToString(rs.Get_Fields_String("Serveraddress"));
					eConfig.SMTPPort = FCConvert.ToInt32(rs.Get_Fields_Int32("serverport"));
					eConfig.UserName = FCConvert.ToString(rs.Get_Fields_String("EmailUser"));
					eConfig.UseSSL = FCConvert.ToBoolean(rs.Get_Fields_Boolean("UseSSL"));
					eConfig.UseTransportLayerSecurity = FCConvert.ToBoolean(rs.Get_Fields_Boolean("UseTLS"));
					eConfig.DefaultFromAddress = FCConvert.ToString(rs.Get_Fields_String("UserEmail"));
					eConfig.DefaultFromName = FCConvert.ToString(rs.Get_Fields_String("FromName"));
					eConfig.AuthCode = FCConvert.ToInt16(rs.Get_Fields_Int32("AuthorizationType"));
					// TODO: Check the table for the column [UserID] and replace with corresponding Get_Field method
					eConfig.UserID = FCConvert.ToInt32(rs.Get_Fields("UserID"));
					eConfig.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					eConfig.IsUpdated = false;
				}
				GetConfiguration = eConfig;
				return GetConfiguration;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return GetConfiguration;
		}

		public void DeleteEmailConfiguration(int lngID)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rs = new clsDRWrapper();
				rs.Execute("Delete from emailconfiguration where id = " + FCConvert.ToString(lngID), "systemsettings");
				return;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		public void SaveEmailConfiguration(ref cEmailConfig eConfig)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (eConfig.IsDeleted)
				{
					DeleteEmailConfiguration(eConfig.ID);
					return;
				}
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from emailconfiguration where id = " + eConfig.ID, "SystemSettings");
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					if (eConfig.ID > 0)
					{
						lngLastError = 9999;
						strLastError = "Email configuration record not found";
						return;
					}
					rsSave.AddNew();
				}
				rsSave.Set_Fields("AuthorizationType", eConfig.AuthCode);
				rsSave.Set_Fields("UserEmail", eConfig.DefaultFromAddress);
				rsSave.Set_Fields("FromName", eConfig.DefaultFromName);
				rsSave.Set_Fields("DomainName", eConfig.LoginDomain);
				rsSave.Set_Fields("Password", eConfig.Password);
				rsSave.Set_Fields("serveraddress", eConfig.SMTPHost);
				rsSave.Set_Fields("ServerPort", eConfig.SMTPPort);
				rsSave.Set_Fields("EmailUser", eConfig.UserName);
				rsSave.Set_Fields("UseSSL", eConfig.UseSSL);
				rsSave.Set_Fields("UseTLS", eConfig.UseTransportLayerSecurity);
				rsSave.Set_Fields("UserID", eConfig.UserID);
				rsSave.Update();
				eConfig.ID = rsSave.Get_Fields_Int32("ID");
				eConfig.IsUpdated = false;
				return;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}

		public void SaveEmailConfigurations(ref cGenericCollection eConfigs)
		{
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				if (!(eConfigs == null))
				{
					cEmailConfig eConfig = new cEmailConfig();
					int lngIndex/*unused?*/;
					eConfigs.MoveFirst();
					while (eConfigs.IsCurrent())
					{
						if (eConfig.IsUpdated || eConfig.IsDeleted)
						{
							SaveEmailConfiguration(ref eConfig);
						}
						if (HadError)
						{
							return;
						}
						if (eConfig.IsDeleted)
						{
							eConfigs.RemoveCurrent();
						}
						eConfigs.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				lngLastError = Information.Err(ex).Number;
				strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
		}
	}
}
