﻿using fecherFoundation;
using GrapeCity.ActiveReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HTML5Viewer
{
    public class CustomReportService : GrapeCity.ActiveReports.Web.ReportService
    {
        //this.Context.Request.Params["Wisej.SessionId"]
        //public static Dictionary<string, SectionReport> GlobalSectionReportDictionary = new Dictionary<string, SectionReport>();

        protected override object OnCreateReportHandler(string reportPath)
        {   
            if (Sys.GlobalSectionReportDictionary[reportPath] != null)
            {
                //Wisej.Web.Application.RestoreSession(Sys.CurrentContext);
                return Sys.GlobalSectionReportDictionary[reportPath];
            }
            return base.OnCreateReportHandler(reportPath);
        }
    }
}