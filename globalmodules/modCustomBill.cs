﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;
using fecherFoundation;
using fecherFoundation.DataBaseLayer.ADO;

namespace Global
{
	public class modCustomBill
	{
		//=========================================================
		public const int CNSTDYNAMICREPORTTYPECUSTOMBILL = 0;
		public const int CNSTCUSTOMBILLCUSTOMTEXT = -1;
		public const int CNSTCUSTOMBILLCUSTOMRICHEDIT = -2;
		public const int CNSTCUSTOMBILLCUSTOMIMAGE = -3;
		public const int CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT = -4;
		// text editor with variables etc.
		public const int CNSTCUSTOMBILLCUSTOMRECTANGLE = -5;
		public const int CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED = -6;
		public const int CNSTCUSTOMBILLCUSTOMHORIZONTALLINE = -7;
		public const int CNSTCUSTOMBILLCUSTOMVERTICALLINE = -8;
		public const int CNSTCUSTOMBILLCUSTOMSUBREPORT = -9;
		public const int CNSTCUSTOMBILLCUSTOMBARCODE = -10;
		public const int CNSTCUSTOMBILLCUSTOMAUTOPOP = -11;
		public const int CNSTCUSTOMBILLCUSTOMDATE = -12;
		public const int CNSTCUSTOMBILLSIGNATURE = -13;

		public struct CustomBillCodeType
		{
			public int FieldID;
			// code number
			public int Category;
			public bool SpecialCase;
			// is it a special case or not
			public string FieldName;
			// field name in the database.  If this is a special case, fieldname might be useless
			public string TableName;
			// table name. If this is a special case, tablename might be useless
			public string DBName;
			// name of database.  Unlikely a report will span databases
			public int Datatype;
			// what type of data, incase error checking needs to know for instance
			public string FormatString;
			// If not blank can tell the program how to format it without making this code a special case
			public string ExtraParameters;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public CustomBillCodeType(int unusedParam)
			{
				this.FieldID = 0;
				this.Category = 0;
				this.SpecialCase = false;
				this.FieldName = string.Empty;
				this.TableName = string.Empty;
				this.DBName = string.Empty;
				this.Datatype = 0;
				this.FormatString = string.Empty;
				this.ExtraParameters = string.Empty;
			}
		};
		// kk02022015 TROUTS-51  Add these functions back in for exporting Custom Bill to Access file
		public static bool AddCustomBillTable(ref FCDatabase dbConn)
		{
			bool AddCustomBillTable = false;
			FCTableDef tbl = null;
			try
			{
				// On Error GoTo ErrorHandler
				AddCustomBillTable = false;
				foreach (FCTableDef table in dbConn.TableDefs)
				{
					if (table.Name == "CustomBills")
					{
						return AddCustomBillTable;
					}
					tbl = null;
				}
				tbl = dbConn.CreateTableDef("CustomBills");
				// build the table
				tbl.Fields.Append(tbl.CreateField("AutoID", DataTypeEnum.dbLong));
				// Call clsSave.AddTableField("AutoID", dbLong)
				tbl.Fields["AutoID"].Attributes = FCConvert.ToInt32(FCField.FieldAttributeEnum.dbAutoIncrField);
				// Call clsSave.SetFieldAttribute("AutoID", dbAutoIncrField)
				tbl.Fields.Append(tbl.CreateField("BillFormat", DataTypeEnum.dbLong));
				// Call clsSave.AddTableField("BillFormat", dbLong)
				tbl.Fields.Append(tbl.CreateField("FormatName", DataTypeEnum.dbText));
				// Call clsSave.AddTableField("FormatName", dbText)
				tbl.Fields["FormatName"].AllowZeroLength = true;
				// Call clsSave.SetFieldAllowZeroLength("FormatName", True)
				tbl.Fields.Append(tbl.CreateField("Units", DataTypeEnum.dbInteger));
				// Call clsSave.AddTableField("Units", dbInteger)               'centimeters or inches
				tbl.Fields.Append(tbl.CreateField("PageHeight", DataTypeEnum.dbDouble));
				// Call clsSave.AddTableField("PageHeight", dbDouble)
				tbl.Fields.Append(tbl.CreateField("PageWidth", DataTypeEnum.dbDouble));
				// Call clsSave.AddTableField("PageWidth", dbDouble)
				tbl.Fields.Append(tbl.CreateField("TopMargin", DataTypeEnum.dbDouble));
				// Call clsSave.AddTableField("TopMargin", dbDouble)
				tbl.Fields.Append(tbl.CreateField("BottomMargin", DataTypeEnum.dbDouble));
				// Call clsSave.AddTableField("BottomMargin", dbDouble)
				tbl.Fields.Append(tbl.CreateField("LeftMargin", DataTypeEnum.dbDouble));
				// Call clsSave.AddTableField("LeftMargin", dbDouble)
				tbl.Fields.Append(tbl.CreateField("RightMargin", DataTypeEnum.dbDouble));
				// Call clsSave.AddTableField("RightMargin", dbDouble)
				tbl.Fields.Append(tbl.CreateField("boolWide", DataTypeEnum.dbBoolean));
				// Call clsSave.AddTableField("boolWide", dbBoolean)
				tbl.Fields.Append(tbl.CreateField("BillType", DataTypeEnum.dbInteger));
				// Call clsSave.AddTableField("BillType", dbInteger)            'one or combined
				tbl.Fields.Append(tbl.CreateField("IsLaser", DataTypeEnum.dbBoolean));
				// Call clsSave.AddTableField("IsLaser", dbBoolean)
				tbl.Fields.Append(tbl.CreateField("IsDeletable", DataTypeEnum.dbBoolean));
				// Call clsSave.AddTableField("IsDeletable", dbBoolean)
				tbl.Fields.Append(tbl.CreateField("Description", DataTypeEnum.dbText, 255));
				// Call clsSave.AddTableField("Description", dbText, 255)
				tbl.Fields["Description"].AllowZeroLength = true;
				// Call clsSave.SetFieldAllowZeroLength("Description", True)
				tbl.Fields.Append(tbl.CreateField("BackgroundImage", DataTypeEnum.dbText, 255));
				// Call clsSave.AddTableField("BackgroundImage", dbText, 255)
				tbl.Fields["BackgroundImage"].AllowZeroLength = true;
				// Call clsSave.SetFieldAllowZeroLength("BackgroundImage", True)
				tbl.Fields.Append(tbl.CreateField("DefaultFontSize", DataTypeEnum.dbDouble));
				// Call clsSave.AddTableField("DefaultFontSize", dbDouble)
				dbConn.TableDefs.Append(tbl);
				AddCustomBillTable = true;
				return AddCustomBillTable;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In AddCustomBillTable", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddCustomBillTable;
		}

		public static bool AddCustomBillFields(ref FCDatabase dbConn)
		{
			bool AddCustomBillFields = false;
			FCTableDef tbl;
			try
			{
				// On Error GoTo ErrorHandler
				AddCustomBillFields = false;
				foreach (FCTableDef table in dbConn.TableDefs)
				{
					if (table.Name == "CustomBillFields")
					{
						return AddCustomBillFields;
					}
					tbl = null;
				}
				tbl = dbConn.CreateTableDef("CustomBillFields");
				// build the table
				tbl.Fields.Append(tbl.CreateField("AutoID", DataTypeEnum.dbLong));
				// Call clsSave.AddTableField("AutoID", dbLong)
				tbl.Fields["AutoID"].Attributes = FCConvert.ToInt32(FCField.FieldAttributeEnum.dbAutoIncrField);
				// Call clsSave.SetFieldAttribute("AutoID", dbAutoIncrField)
				tbl.Fields.Append(tbl.CreateField("FieldNumber", DataTypeEnum.dbLong));
				// Call clsSave.AddTableField("FieldNumber", dbLong)
				tbl.Fields.Append(tbl.CreateField("FormatID", DataTypeEnum.dbLong));
				// Call clsSave.AddTableField("FormatID", dbLong)
				tbl.Fields.Append(tbl.CreateField("FieldID", DataTypeEnum.dbLong));
				// Call clsSave.AddTableField("FieldID", dbLong)
				tbl.Fields.Append(tbl.CreateField("Top", DataTypeEnum.dbDouble));
				// Call clsSave.AddTableField("Top", dbDouble)
				tbl.Fields.Append(tbl.CreateField("Left", DataTypeEnum.dbDouble));
				// Call clsSave.AddTableField("Left", dbDouble)
				tbl.Fields.Append(tbl.CreateField("Height", DataTypeEnum.dbDouble));
				// Call clsSave.AddTableField("Height", dbDouble)
				tbl.Fields.Append(tbl.CreateField("Width", DataTypeEnum.dbDouble));
				// Call clsSave.AddTableField("Width", dbDouble)
				tbl.Fields.Append(tbl.CreateField("BillType", DataTypeEnum.dbLong));
				// Call clsSave.AddTableField("BillType", dbLong)
				tbl.Fields.Append(tbl.CreateField("Description", DataTypeEnum.dbText));
				// Call clsSave.AddTableField("Description", dbText)
				tbl.Fields["Description"].AllowZeroLength = true;
				// Call clsSave.SetFieldAllowZeroLength("Description", True)
				tbl.Fields.Append(tbl.CreateField("Alignment", DataTypeEnum.dbInteger));
				// Call clsSave.AddTableField("Alignment", dbInteger)
				tbl.Fields.Append(tbl.CreateField("UserText", DataTypeEnum.dbMemo));
				// Call clsSave.AddTableField("UserText", dbMemo)
				tbl.Fields["UserText"].AllowZeroLength = true;
				// Call clsSave.SetFieldAllowZeroLength("UserText", True)
				tbl.Fields.Append(tbl.CreateField("Font", DataTypeEnum.dbText));
				// Call clsSave.AddTableField("Font", dbText)
				tbl.Fields["Font"].AllowZeroLength = true;
				// Call clsSave.SetFieldAllowZeroLength("Font", True)
				tbl.Fields.Append(tbl.CreateField("FontSize", DataTypeEnum.dbDouble));
				// Call clsSave.AddTableField("FontSize", dbDouble)
				tbl.Fields.Append(tbl.CreateField("FontStyle", DataTypeEnum.dbInteger));
				// Call clsSave.AddTableField("FontStyle", dbInteger)
				tbl.Fields["FontStyle"].AllowZeroLength = true;
				// Call clsSave.SetFieldAllowZeroLength("FontStyle", True)
				tbl.Fields.Append(tbl.CreateField("ExtraParameters", DataTypeEnum.dbText));
				// Call clsSave.AddTableField("ExtraParameters", dbText, 255)
				tbl.Fields["ExtraParameters"].AllowZeroLength = true;
				// Call clsSave.SetFieldAllowZeroLength("ExtraParameters", True)
				tbl.Fields.Append(tbl.CreateField("OpenID", DataTypeEnum.dbLong));
				// Call clsSave.AddTableField("OpenID", dbLong)
				dbConn.TableDefs.Append(tbl);
				AddCustomBillFields = true;
				return AddCustomBillFields;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In AddCustomBillFields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddCustomBillFields;
		}

		public static bool AddModuleTable(ref FCDatabase dbConn)
		{
			bool AddModuleTable = false;
			// adds the functionality for doing custom bills
			FCTableDef tbl;
			try
			{
				// On Error GoTo ErrorHandler
				AddModuleTable = false;
				foreach (FCTableDef table in dbConn.TableDefs)
				{
					if (table.Name == "Module")
					{
						return AddModuleTable;
					}
					tbl = null;
				}
				tbl = dbConn.CreateTableDef("Module");
				// table doesn't exist
				// x    tbl.Fields.Append tbl.CreateField("AutoID", dbLong)
				// x    tbl.Fields("AutoID").Attributes = dbAutoIncrField
				tbl.Fields.Append(tbl.CreateField("Module", DataTypeEnum.dbText));
				// Call clsSave.AddTableField("Module", dbText)
				// x    Set idx = tbl.CreateIndex("PrimaryKey")
				// x    idx.Fields.Append idx.CreateField("AutoID")
				// x    idx.Primary = True
				// x    idx.Unique = True
				// x    tbl.Indexes.Append idx
				dbConn.TableDefs.Append(tbl);
				AddModuleTable = true;
				return AddModuleTable;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In AddModuleTable", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddModuleTable;
		}
	}
}
