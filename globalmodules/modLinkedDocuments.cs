//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Drawing;
using Wisej.Web;
using System.Runtime.InteropServices;
using fecherFoundation;

namespace Global
{
    public class modLinkedDocuments
    {

        //=========================================================
        // ********************************************************
        // Property of TRIO Software Corporation

        // Written By     Dave Wade
        // Date           3/25/2008

        // This module will be used to house global methods used in the global
        // forms ViewDocument, AddDocument, and ScanDocument.  This module also
        // has the procedure used to create the base table needed by these
        // global forms to work properly
        // ********************************************************

        //DefLng(modGlobalVariables.CommercialClassCategory.a-Z);

        const int MAX_PATH = 260;

        const int SHGFI_DISPLAYNAME = 0x200;
        const int SHGFI_EXETYPE = 0x2000;
        const int SHGFI_SYSICONINDEX = 0x4000; // System icon index
        const int SHGFI_LARGEICON = 0x0; // Large icon
        const int SHGFI_SMALLICON = 0x1; // Small icon
        const short ILD_TRANSPARENT = 0x1; // Display transparent
        const int SHGFI_SHELLICONSIZE = 0x4;
        const int SHGFI_TYPENAME = 0x400;
        const int BASIC_SHGFI_FLAGS = SHGFI_TYPENAME | SHGFI_SHELLICONSIZE | SHGFI_SYSICONINDEX | SHGFI_DISPLAYNAME | SHGFI_EXETYPE;


        //public struct SHFILEINFO
        //{
        //    public int hIcon;
        //    public int iIcon;
        //    public int dwAttributes;
        //    public string szDisplayName;
        //    // vbPorter upgrade warning: szTypeName As FixedString	OnWrite(string)
        //    public string szTypeName;
        //    //FC:FINAL:RPU - Use custom constructor to initialize fields
        //    public SHFILEINFO(int unusedParam)
        //    {
        //        this.hIcon = 0;
        //        this.iIcon = 0;
        //        this.dwAttributes = 0;
        //        this.szDisplayName = new string(' ',MAX_PATH);
        //        this.szTypeName = new string(' ', 80);
        //    }
        //};


        //[DllImport("shell32.dll", EntryPoint = "SHGetFileInfoA")]
        //private static extern int SHGetFileInfo(IntPtr pszPath, int dwFileAttributes, ref SHFILEINFO psfi, int cbSizeFileInfo, int uFlags);
        //private static int SHGetFileInfoWrp(ref string pszPath, short dwFileAttributes, ref SHFILEINFO psfi, int cbSizeFileInfo, bool uFlags)
        //{
        //    int ret;
        //    IntPtr ppszPath = FCUtils.GetByteFromString(pszPath);

        //    ret = SHGetFileInfo(ppszPath, dwFileAttributes, ref psfi, cbSizeFileInfo, FCConvert.ToInt32(uFlags));

        //    FCUtils.GetStringFromByte(ref pszPath, ppszPath);
        //    return ret;
        //}



        //[DllImport("comctl32.dll")]
        //private static extern int ImageList_Draw(int himl, int i, int hDCDest, int X, int y, int FLAGS);


        // vbPorter upgrade warning: iTypeColumn As short	OnWriteFCConvert.ToInt32(
        //public static void LV_AddFileToListView_18(string sPath, string sFileName, string sKeyPrefix, FCListView list, int iTypeColumn, ImageList imgLarge, ImageList imgSmall, FCPictureBox picLarge, FCPictureBox picSmall) { LV_AddFileToListView(ref sPath, ref sFileName, sKeyPrefix, ref list, ref iTypeColumn, ref imgLarge, ref imgSmall, ref picLarge, ref picSmall); }
        //public static void LV_AddFileToListView(ref string sPath, ref string sFileName, string sKeyPrefix, ref FCListView list, ref int iTypeColumn, ref ImageList imgLarge, ref ImageList imgSmall, ref FCPictureBox picLarge, ref FCPictureBox picSmall)
        //{

        //    ListViewItem itm;
        //    string sType = ""; // file type returned by API
        //    string sKey; // 2003/11/30 Added

        //    SetSizePictureBoxes(ref picLarge, ref picSmall); // need to be a certain size

        //    sKey = sKeyPrefix + sPath + sFileName;


        //    //list.Icons = imgLarge;
        //    //list.SmallIcons = imgSmall;
        //    itm = list.Items.Add(sKey, sFileName); // add the file and use it's name as the key
        //    //itm = list.FindItemWithText(sKey); // point to the listitem
        //    //FC:FINAL:SBE - not supported
        //    //AddImage_6(itm, sPath + sFileName, ref sType, picLarge, picSmall, imgLarge, imgSmall);
        //    if (iTypeColumn > -1)
        //    { // display the file description in this column
        //        FCListView.SubItemsSetText(itm, iTypeColumn, sType);
        //    }


        //    itm = null;
        //}

       //private static bool ExtractIconIntoPictureUsingFile_18(string sFileName, FCPictureBox PIC, bool bLarge, ref SHFILEINFO tSHFILEINFO) { return ExtractIconIntoPictureUsingFile(ref sFileName, ref PIC, ref bLarge, ref tSHFILEINFO); }
        //private static bool ExtractIconIntoPictureUsingFile(ref string sFileName, ref FCPictureBox PIC, ref bool bLarge, ref SHFILEINFO tSHFILEINFO)
        //{
        //    bool ExtractIconIntoPictureUsingFile = false;
        //    int hImg = 0; // The handle to the system image list
        //    int R;
        //    string sTemp = "";
        //    int iPos = 0;

        //    try
        //    {   // On Error GoTo LV_ExtractIconIntoPictureUsingFileExit
        //        // Set the pictureboxes to receive the icons.
        //        PIC.Image = FCUtils.LoadPicture(); // clear it

        //        // Draw the associated icons into the picture boxes
        //        if (bLarge)
        //        {
        //            hImg = SHGetFileInfoWrp(ref sFileName, 0, ref tSHFILEINFO, Marshal.SizeOf(tSHFILEINFO), FCConvert.ToBoolean(BASIC_SHGFI_FLAGS | SHGFI_LARGEICON));
        //        }
        //        else
        //        {
        //            hImg = SHGetFileInfoWrp(ref sFileName, 0, ref tSHFILEINFO, Marshal.SizeOf(tSHFILEINFO), FCConvert.ToBoolean(BASIC_SHGFI_FLAGS | SHGFI_SMALLICON));
        //        }
        //        R = ImageList_Draw(hImg, tSHFILEINFO.iIcon, FCConvert.ToInt32(PIC.hDC), 0, 0, ILD_TRANSPARENT);
        //        // 1999/12/07 Now make them pictures, could be loaded into an image list. Larry Rebich
        //        PIC.Image = PIC.Image;



        //        iPos = Strings.InStr(tSHFILEINFO.szTypeName, "\0", CompareConstants.vbBinaryCompare);
        //        if (iPos > 0)
        //        {
        //            tSHFILEINFO.szTypeName = Strings.Mid(tSHFILEINFO.szTypeName, 1, iPos - 1);
        //        }

        //        ExtractIconIntoPictureUsingFile = true;
        //    }
        //    catch (Exception ex)
        //    {   // LV_ExtractIconIntoPictureUsingFileExit:
        //    }
        //    return ExtractIconIntoPictureUsingFile;
        //}

        //private static bool LV_InImageList(ref ImageList imgS, ref string sKey)
        //{
        //    bool LV_InImageList = false;
        //    foreach (Image img in imgS.Images)
        //    {

        //        if (img.Tag.ToString() == sKey)
        //        {
        //            LV_InImageList = true;
        //            return LV_InImageList;
        //        }

        //    }
        //    return LV_InImageList;
        //}

        //private static void AddImage_6(ListViewItem itm, string sFileName, ref string sType, FCPictureBox picLarge, FCPictureBox picSmall, ImageList imgLarge, ImageList imgSmall) { AddImage(ref itm, ref sFileName, ref sType, ref picLarge, ref picSmall, ref imgLarge, ref imgSmall); }
        //private static void AddImage(ref ListViewItem itm, ref string sFileName, ref string sType, ref FCPictureBox picLarge, ref FCPictureBox picSmall, ref ImageList imgLarge, ref ImageList imgSmall)
        //{
        //    //FC:FINAL:RPU - Use custom constructor to initialize fields
        //    SHFILEINFO tLarge = new SHFILEINFO(0);
        //    SHFILEINFO tSmall = new SHFILEINFO(0);

        //    if (ExtractIconIntoPictureUsingFile_18(sFileName, picLarge, true, ref tLarge))
        //    {

        //        if (tLarge.iIcon > 0)
        //        {
        //            if (NotExistInImageList(ref imgLarge, ref tLarge.iIcon))
        //            {
        //                imgLarge.Images.Add("K" + FCConvert.ToString(tLarge.iIcon), picLarge.Image);
        //            }
        //            itm.ImageIndex = FCConvert.ToInt32("K" + FCConvert.ToString(tLarge.iIcon)) - 1;
        //        }

        //    }
        //    if (ExtractIconIntoPictureUsingFile_18(sFileName, picSmall, false, ref tSmall))
        //    {

        //        if (tSmall.iIcon > 0)
        //        {
        //            if (NotExistInImageList(ref imgSmall, ref tSmall.iIcon))
        //            {
        //                imgSmall.Images.Add("K" + FCConvert.ToString(tSmall.iIcon), picSmall.Image);
        //            }
        //            itm.ImageIndex = FCConvert.ToInt32("K" + FCConvert.ToString(tSmall.iIcon)) - 1;
        //        }

        //    }
        //    sType = Strings.Trim(tLarge.szTypeName);
        //}

        // vbPorter upgrade warning: imglst As ImageList	OnWrite(MSComctlLib.ImageList)
        //private static bool NotExistInImageList(ref ImageList imglst, ref int iIcon)
        //{
        //    bool NotExistInImageList = false;
        //    foreach (Image img in imglst.Images)
        //    {

        //        if (img.Tag.ToString() == "K" + FCConvert.ToString(iIcon))
        //        {
        //            return NotExistInImageList;
        //        }

        //    }
        //    NotExistInImageList = true;
        //    return NotExistInImageList;
        //}

        //private static void SetSizePictureBoxes(ref FCPictureBox picLarge, ref FCPictureBox picSmall)
        //{
        //    // make sure the picture boxes are the 'correct' size.
        //    //vbPorterConverter.SetBounds(picLarge, 0, 0, 480, 480);
        //    picLarge.Visible = false;


        //    //vbPorterConverter.SetBounds(picSmall, 0, 0, 240, 240);
        //    picSmall.Visible = false;

        //}

        //public static void LV_ClearImageListsAndRelinkToListView(ref FCListView list, ref ImageList imgLarge,
        //    ref ImageList imgSmall)
        //{
        //    //a stub to allow compiling
        //}

    }
}