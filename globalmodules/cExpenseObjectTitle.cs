﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cExpenseObjectTitle
	{
		//=========================================================
		private int lngRecordID;
		private string strExpense = string.Empty;
		private string strObject = string.Empty;
		private string strShortDescription = string.Empty;
		private string strLongDescription = string.Empty;
		private int lngBreakDownCode;
		private string strTax = string.Empty;
		private bool boolUpdated;
		private bool boolDeleted;

		public string Expense
		{
			set
			{
				strExpense = value;
				IsUpdated = true;
			}
			get
			{
				string Expense = "";
				Expense = strExpense;
				return Expense;
			}
		}

		public string ExpenseObject
		{
			set
			{
				strObject = value;
				IsUpdated = true;
			}
			get
			{
				string ExpenseObject = "";
				ExpenseObject = strObject;
				return ExpenseObject;
			}
		}

		public string Tax
		{
			set
			{
				strTax = value;
				IsUpdated = true;
			}
			get
			{
				string Tax = "";
				Tax = strTax;
				return Tax;
			}
		}

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
				IsUpdated = true;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}

		public string LongDescription
		{
			set
			{
				strLongDescription = value;
				IsUpdated = true;
			}
			get
			{
				string LongDescription = "";
				LongDescription = strLongDescription;
				return LongDescription;
			}
		}

		public int BreakdownCode
		{
			set
			{
				lngBreakDownCode = value;
				IsUpdated = true;
			}
			get
			{
				int BreakdownCode = 0;
				BreakdownCode = lngBreakDownCode;
				return BreakdownCode;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}
	}
}
