//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;

#if TWAR0000
using TWAR0000;


#elif TWCR0000
using TWCR0000;

#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmARCLStatus.
	/// </summary>
	partial class frmARCLStatus : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblCode;
		public fecherFoundation.FCCheckBox chkShowAll;
		public fecherFoundation.FCFrame fraRateInfo;
		public FCGrid vsRateInfo;
		public fecherFoundation.FCButton cmdRIClose;
		public fecherFoundation.FCPanel fraPayment;
		public fecherFoundation.FCComboBox cmbBillNumber;
		public fecherFoundation.FCTextBox txtInterest;
		public fecherFoundation.FCTextBox txtTotalPendingDue;
		public FCGrid vsPayments;
		public fecherFoundation.FCTextBox txtTax;
		public fecherFoundation.FCTextBox txtCash;
		public fecherFoundation.FCTextBox txtCD;
		public fecherFoundation.FCTextBox txtPrincipal;
		public fecherFoundation.FCTextBox txtReference;
		public fecherFoundation.FCTextBox txtComments;
		public fecherFoundation.FCTextBox txtBLID;
		public fecherFoundation.FCComboBox cmbCode;
		public Global.T2KDateBox txtTransactionDate;
		public FCGrid vsPeriod;
		public FCGrid txtAcctNumber;
		public fecherFoundation.FCLabel lblTax;
		public fecherFoundation.FCLabel Label1_9;
		public fecherFoundation.FCLabel lblTotalPendingDue;
		public fecherFoundation.FCLabel Label1_11;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel lblCode_2;
		public fecherFoundation.FCLabel lblPrincipal;
		public fecherFoundation.FCLabel lblInterest;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCLabel lblInvoice;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label1_12;
		public fecherFoundation.FCPanel fraStatusLabels;
		public fecherFoundation.FCHeader Label1_0;
		public fecherFoundation.FCHeader lblAccount;
		public fecherFoundation.FCLabel lblOwnersName;
		public FCCommonDialog CommonDialog1;
		public FCGrid GRID;
		public FCGrid vsPreview;
		public fecherFoundation.FCPictureBox imgNote;
		public fecherFoundation.FCLabel lblPaymentInfo;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessEffective;
		public fecherFoundation.FCToolStripMenuItem mnuProcessGoTo;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuProcessChangeAccount;
		public fecherFoundation.FCToolStripMenuItem mnuFileMortgageHolderInfo;
		public fecherFoundation.FCToolStripMenuItem mnuFileAccountInfo;
		public fecherFoundation.FCToolStripMenuItem mnuFileEditNote;
		public fecherFoundation.FCToolStripMenuItem mnuFilePriority;
		public fecherFoundation.FCToolStripMenuItem mnuPayment;
		public fecherFoundation.FCToolStripMenuItem mnuPaymentClearPayment;
		public fecherFoundation.FCToolStripMenuItem mnuPaymentClearList;
		public fecherFoundation.FCToolStripMenuItem mnuPaymentPreview;
		public fecherFoundation.FCToolStripMenuItem mnuPaymentSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuPaymentSave;
		public fecherFoundation.FCToolStripMenuItem mnuPaymentSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSpacer;
		public fecherFoundation.FCToolStripMenuItem mnuProcessExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmARCLStatus));
			this.chkShowAll = new fecherFoundation.FCCheckBox();
			this.fraRateInfo = new fecherFoundation.FCFrame();
			this.cmdRIClose = new fecherFoundation.FCButton();
			this.vsRateInfo = new fecherFoundation.FCGrid();
			this.fraPayment = new fecherFoundation.FCPanel();
			this.cmbBillNumber = new fecherFoundation.FCComboBox();
			this.txtInterest = new fecherFoundation.FCTextBox();
			this.txtTotalPendingDue = new fecherFoundation.FCTextBox();
			this.vsPayments = new fecherFoundation.FCGrid();
			this.txtTax = new fecherFoundation.FCTextBox();
			this.txtCash = new fecherFoundation.FCTextBox();
			this.txtCD = new fecherFoundation.FCTextBox();
			this.txtPrincipal = new fecherFoundation.FCTextBox();
			this.txtReference = new fecherFoundation.FCTextBox();
			this.txtComments = new fecherFoundation.FCTextBox();
			this.txtBLID = new fecherFoundation.FCTextBox();
			this.cmbCode = new fecherFoundation.FCComboBox();
			this.txtTransactionDate = new Global.T2KDateBox();
			this.vsPeriod = new fecherFoundation.FCGrid();
			this.txtAcctNumber = new fecherFoundation.FCGrid();
			this.lblTax = new fecherFoundation.FCLabel();
			this.Label1_9 = new fecherFoundation.FCLabel();
			this.lblTotalPendingDue = new fecherFoundation.FCLabel();
			this.Label1_11 = new fecherFoundation.FCLabel();
			this.Label1_1 = new fecherFoundation.FCLabel();
			this.lblCode_2 = new fecherFoundation.FCLabel();
			this.lblPrincipal = new fecherFoundation.FCLabel();
			this.lblInterest = new fecherFoundation.FCLabel();
			this.Label1_10 = new fecherFoundation.FCLabel();
			this.lblInvoice = new fecherFoundation.FCLabel();
			this.Label1_8 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label1_12 = new fecherFoundation.FCLabel();
			this.fraStatusLabels = new fecherFoundation.FCPanel();
			this.Label1_0 = new fecherFoundation.FCHeader();
			this.lblAccount = new fecherFoundation.FCHeader();
			this.imgNote = new fecherFoundation.FCPictureBox();
			this.cmdPaymentSave = new fecherFoundation.FCButton();
			this.cmdChangeAccount = new fecherFoundation.FCButton();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdEffectiveDate = new fecherFoundation.FCButton();
			this.lblOwnersName = new fecherFoundation.FCLabel();
			this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
			this.GRID = new fecherFoundation.FCGrid();
			this.vsPreview = new fecherFoundation.FCGrid();
			this.lblPaymentInfo = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcessGoTo = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileMortgageHolderInfo = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileAccountInfo = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileEditNote = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePriority = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPayment = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPaymentClearPayment = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPaymentClearList = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPaymentPreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessEffective = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessChangeAccount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPaymentSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPaymentSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPaymentSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSpacer = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdProcess = new fecherFoundation.FCButton();
			this.lblName = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).BeginInit();
			this.fraRateInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdRIClose)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsRateInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPayment)).BeginInit();
			this.fraPayment.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsPayments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTransactionDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcctNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraStatusLabels)).BeginInit();
			this.fraStatusLabels.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.imgNote)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPaymentSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdChangeAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEffectiveDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GRID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsPreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Controls.Add(this.cmdPaymentSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(1180, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraPayment);
			this.ClientArea.Controls.Add(this.chkShowAll);
			this.ClientArea.Controls.Add(this.lblName);
			this.ClientArea.Controls.Add(this.lblOwnersName);
			this.ClientArea.Controls.Add(this.GRID);
			this.ClientArea.Controls.Add(this.vsPreview);
			this.ClientArea.Controls.Add(this.lblPaymentInfo);
			this.ClientArea.Controls.Add(this.fraRateInfo);
			this.ClientArea.Size = new System.Drawing.Size(1180, 498);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdEffectiveDate);
			this.TopPanel.Controls.Add(this.cmdPrint);
			this.TopPanel.Controls.Add(this.cmdChangeAccount);
            this.TopPanel.Controls.Add(this.imgNote);
            this.TopPanel.Controls.Add(this.fraStatusLabels);
			this.TopPanel.Size = new System.Drawing.Size(1180, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.fraStatusLabels, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdChangeAccount, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdEffectiveDate, 0);
			// 
			// HeaderText
			// 
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// chkShowAll
			// 
			this.chkShowAll.Location = new System.Drawing.Point(484, 30);
			this.chkShowAll.Name = "chkShowAll";
			this.chkShowAll.Size = new System.Drawing.Size(106, 23);
			this.chkShowAll.TabIndex = 39;
			this.chkShowAll.Text = "Show All Bills";
			this.ToolTip1.SetToolTip(this.chkShowAll, null);
			this.chkShowAll.CheckedChanged += new System.EventHandler(this.chkShowAll_CheckedChanged);
			// 
			// fraRateInfo
			// 
			this.fraRateInfo.BackColor = System.Drawing.Color.White;
			this.fraRateInfo.Controls.Add(this.cmdRIClose);
			this.fraRateInfo.Controls.Add(this.vsRateInfo);
			this.fraRateInfo.Location = new System.Drawing.Point(30, 30);
			this.fraRateInfo.Name = "fraRateInfo";
			this.fraRateInfo.Size = new System.Drawing.Size(937, 690);
			this.fraRateInfo.TabIndex = 27;
			this.fraRateInfo.Text = "Year Information";
			this.ToolTip1.SetToolTip(this.fraRateInfo, null);
			this.fraRateInfo.Visible = false;
			// 
			// cmdRIClose
			// 
			this.cmdRIClose.AppearanceKey = "actionButton";
			this.cmdRIClose.Location = new System.Drawing.Point(425, 641);
			this.cmdRIClose.Name = "cmdRIClose";
			this.cmdRIClose.Size = new System.Drawing.Size(87, 40);
			this.cmdRIClose.TabIndex = 28;
			this.cmdRIClose.Text = "Close";
			this.ToolTip1.SetToolTip(this.cmdRIClose, null);
			this.cmdRIClose.Click += new System.EventHandler(this.cmdRIClose_Click);
			// 
			// vsRateInfo
			// 
			this.vsRateInfo.Cols = 5;
			this.vsRateInfo.ColumnHeadersVisible = false;
			this.vsRateInfo.FixedCols = 0;
			this.vsRateInfo.FixedRows = 0;
			this.vsRateInfo.Location = new System.Drawing.Point(20, 20);
			this.vsRateInfo.Name = "vsRateInfo";
			this.vsRateInfo.RowHeadersVisible = false;
			this.vsRateInfo.Rows = 20;
			this.vsRateInfo.Size = new System.Drawing.Size(899, 609);
			this.vsRateInfo.TabIndex = 34;
			this.ToolTip1.SetToolTip(this.vsRateInfo, null);
			this.vsRateInfo.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsRateInfo_BeforeEdit);
			this.vsRateInfo.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsRateInfo_ValidateEdit);
			this.vsRateInfo.CurrentCellChanged += new System.EventHandler(this.vsRateInfo_RowColChange);
			this.vsRateInfo.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsRateInfo_MouseMoveEvent);
			// 
			// fraPayment
			// 
			this.fraPayment.AppearanceKey = "groupBoxNoBorders";
			this.fraPayment.Controls.Add(this.cmbBillNumber);
			this.fraPayment.Controls.Add(this.txtInterest);
			this.fraPayment.Controls.Add(this.txtTotalPendingDue);
			this.fraPayment.Controls.Add(this.vsPayments);
			this.fraPayment.Controls.Add(this.txtTax);
			this.fraPayment.Controls.Add(this.txtCash);
			this.fraPayment.Controls.Add(this.txtCD);
			this.fraPayment.Controls.Add(this.txtPrincipal);
			this.fraPayment.Controls.Add(this.txtReference);
			this.fraPayment.Controls.Add(this.txtComments);
			this.fraPayment.Controls.Add(this.txtBLID);
			this.fraPayment.Controls.Add(this.cmbCode);
			this.fraPayment.Controls.Add(this.txtTransactionDate);
			this.fraPayment.Controls.Add(this.vsPeriod);
			this.fraPayment.Controls.Add(this.txtAcctNumber);
			this.fraPayment.Controls.Add(this.lblTax);
			this.fraPayment.Controls.Add(this.Label1_9);
			this.fraPayment.Controls.Add(this.lblTotalPendingDue);
			this.fraPayment.Controls.Add(this.Label1_11);
			this.fraPayment.Controls.Add(this.Label1_1);
			this.fraPayment.Controls.Add(this.lblCode_2);
			this.fraPayment.Controls.Add(this.lblPrincipal);
			this.fraPayment.Controls.Add(this.lblInterest);
			this.fraPayment.Controls.Add(this.Label1_10);
			this.fraPayment.Controls.Add(this.lblInvoice);
			this.fraPayment.Controls.Add(this.Label1_8);
			this.fraPayment.Controls.Add(this.Label8);
			this.fraPayment.Controls.Add(this.Label1_12);
			this.fraPayment.Location = new System.Drawing.Point(10, 249);
			this.fraPayment.MinimumSize = new System.Drawing.Size(1140, 0);
			this.fraPayment.Name = "fraPayment";
			this.fraPayment.Size = new System.Drawing.Size(1140, 469);
			this.fraPayment.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.fraPayment, null);
			this.fraPayment.Visible = false;
			// 
			// cmbBillNumber
			// 
			this.cmbBillNumber.BackColor = System.Drawing.SystemColors.Window;
			this.cmbBillNumber.Location = new System.Drawing.Point(20, 118);
			this.cmbBillNumber.Name = "cmbBillNumber";
			this.cmbBillNumber.Size = new System.Drawing.Size(138, 40);
			this.cmbBillNumber.TabIndex = 37;
			this.ToolTip1.SetToolTip(this.cmbBillNumber, null);
			this.cmbBillNumber.SelectedIndexChanged += new System.EventHandler(this.cmbBillNumber_SelectedIndexChanged);
			this.cmbBillNumber.DropDown += new System.EventHandler(this.cmbBillNumber_DropDown);
			this.cmbBillNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbBillNumber_KeyDown);
			// 
			// txtInterest
			// 
			this.txtInterest.BackColor = System.Drawing.SystemColors.Window;
			this.txtInterest.Location = new System.Drawing.Point(1034, 118);
			this.txtInterest.MaxLength = 12;
			this.txtInterest.Name = "txtInterest";
			this.txtInterest.Size = new System.Drawing.Size(90, 40);
			this.txtInterest.TabIndex = 3;
			this.txtInterest.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtInterest, null);
			this.txtInterest.Enter += new System.EventHandler(this.txtInterest_Enter);
			this.txtInterest.Validating += new System.ComponentModel.CancelEventHandler(this.txtInterest_Validating);
			this.txtInterest.KeyDown += new Wisej.Web.KeyEventHandler(this.txtInterest_KeyDown);
			this.txtInterest.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtInterest_KeyPress);
			// 
			// txtTotalPendingDue
			// 
			this.txtTotalPendingDue.Location = new System.Drawing.Point(834, 30);
			this.txtTotalPendingDue.LockedOriginal = true;
			this.txtTotalPendingDue.Name = "txtTotalPendingDue";
			this.txtTotalPendingDue.ReadOnly = true;
			this.txtTotalPendingDue.Size = new System.Drawing.Size(205, 40);
			this.txtTotalPendingDue.TabIndex = 36;
			this.txtTotalPendingDue.TabStop = false;
			this.txtTotalPendingDue.Text = "0.00";
			this.txtTotalPendingDue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtTotalPendingDue, null);
			this.txtTotalPendingDue.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTotalPendingDue_KeyPress);
			// 
			// vsPayments
			// 
			this.vsPayments.Cols = 12;
			this.vsPayments.FixedCols = 0;
			this.vsPayments.Location = new System.Drawing.Point(20, 238);
			this.vsPayments.Name = "vsPayments";
			this.vsPayments.RowHeadersVisible = false;
			this.vsPayments.Rows = 1;
			this.vsPayments.Size = new System.Drawing.Size(1003, 211);
			this.vsPayments.TabIndex = 35;
			this.ToolTip1.SetToolTip(this.vsPayments, null);
			this.vsPayments.KeyDown += new Wisej.Web.KeyEventHandler(this.vsPayments_KeyDownEvent);
			// 
			// txtTax
			// 
			this.txtTax.Location = new System.Drawing.Point(934, 118);
			this.txtTax.MaxLength = 12;
			this.txtTax.Name = "txtTax";
			this.txtTax.Size = new System.Drawing.Size(90, 40);
			this.txtTax.TabIndex = 2;
			this.txtTax.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtTax, null);
			this.txtTax.Enter += new System.EventHandler(this.txtTax_Enter);
			this.txtTax.Validating += new System.ComponentModel.CancelEventHandler(this.txtTax_Validating);
			this.txtTax.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTax_KeyDown);
			this.txtTax.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTax_KeyPress);
			// 
			// txtCash
			// 
			this.txtCash.Enabled = false;
			this.txtCash.Location = new System.Drawing.Point(632, 118);
			this.txtCash.MaxLength = 1;
			this.txtCash.Name = "txtCash";
			this.txtCash.Size = new System.Drawing.Size(41, 40);
			this.txtCash.TabIndex = 7;
			this.txtCash.Text = "Y";
			this.txtCash.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtCash, null);
			this.txtCash.DoubleClick += new System.EventHandler(this.txtCash_DoubleClick);
			this.txtCash.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCash_KeyDown);
			this.txtCash.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCash_KeyPress);
			// 
			// txtCD
			// 
			this.txtCD.Location = new System.Drawing.Point(581, 118);
			this.txtCD.MaxLength = 1;
			this.txtCD.Name = "txtCD";
			this.txtCD.Size = new System.Drawing.Size(41, 40);
			this.txtCD.TabIndex = 6;
			this.txtCD.Text = "Y";
			this.txtCD.TextAlign = Wisej.Web.HorizontalAlignment.Center;
			this.ToolTip1.SetToolTip(this.txtCD, null);
			this.txtCD.DoubleClick += new System.EventHandler(this.txtCD_DoubleClick);
			this.txtCD.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCD_KeyDown);
			this.txtCD.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCD_KeyPress);
			// 
			// txtPrincipal
			// 
			this.txtPrincipal.Location = new System.Drawing.Point(836, 118);
			this.txtPrincipal.MaxLength = 17;
			this.txtPrincipal.Name = "txtPrincipal";
			this.txtPrincipal.Size = new System.Drawing.Size(90, 40);
			this.txtPrincipal.TabIndex = 1;
			this.txtPrincipal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtPrincipal, null);
			this.txtPrincipal.Enter += new System.EventHandler(this.txtPrincipal_Enter);
			this.txtPrincipal.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrincipal_Validating);
			this.txtPrincipal.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPrincipal_KeyDown);
			this.txtPrincipal.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPrincipal_KeyPress);
			// 
			// txtReference
			// 
			this.txtReference.Location = new System.Drawing.Point(283, 118);
			this.txtReference.MaxLength = 6;
			this.txtReference.Name = "txtReference";
			this.txtReference.Size = new System.Drawing.Size(88, 40);
			this.ToolTip1.SetToolTip(this.txtReference, null);
			this.txtReference.Enter += new System.EventHandler(this.txtReference_Enter);
			this.txtReference.Validating += new System.ComponentModel.CancelEventHandler(this.txtReference_Validating);
			this.txtReference.KeyDown += new Wisej.Web.KeyEventHandler(this.txtReference_KeyDown);
			// 
			// txtComments
			// 
			this.txtComments.Location = new System.Drawing.Point(365, 178);
			this.txtComments.MaxLength = 255;
			this.txtComments.Name = "txtComments";
			this.txtComments.Size = new System.Drawing.Size(658, 40);
			this.txtComments.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtComments, null);
			this.txtComments.KeyDown += new Wisej.Web.KeyEventHandler(this.txtComments_KeyDown);
			// 
			// txtBLID
			// 
			this.txtBLID.Location = new System.Drawing.Point(20, 261);
			this.txtBLID.Name = "txtBLID";
			this.txtBLID.Size = new System.Drawing.Size(60, 40);
			this.txtBLID.TabIndex = 15;
			this.ToolTip1.SetToolTip(this.txtBLID, null);
			this.txtBLID.Visible = false;
			// 
			// cmbCode
			// 
			this.cmbCode.Location = new System.Drawing.Point(381, 118);
			this.cmbCode.Name = "cmbCode";
			this.cmbCode.Size = new System.Drawing.Size(190, 40);
			this.cmbCode.Sorted = true;
			this.cmbCode.TabIndex = 5;
			this.ToolTip1.SetToolTip(this.cmbCode, null);
			this.cmbCode.SelectedIndexChanged += new System.EventHandler(this.cmbCode_SelectedIndexChanged);
			this.cmbCode.DropDown += new System.EventHandler(this.cmbCode_DropDown);
			this.cmbCode.Enter += new System.EventHandler(this.cmbCode_Enter);
			this.cmbCode.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbCode_KeyDown);
			// 
			// txtTransactionDate
			// 
			this.txtTransactionDate.Location = new System.Drawing.Point(165, 118);
			this.txtTransactionDate.Mask = "##/##/####";
			this.txtTransactionDate.MaxLength = 10;
			this.txtTransactionDate.Name = "txtTransactionDate";
			this.txtTransactionDate.Size = new System.Drawing.Size(110, 22);
			this.txtTransactionDate.TabIndex = 30;
			this.ToolTip1.SetToolTip(this.txtTransactionDate, null);
			this.txtTransactionDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtTransactionDate_Validate);
			this.txtTransactionDate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTransactionDate_KeyDownEvent);
			// 
			// vsPeriod
			// 
			this.vsPeriod.Cols = 5;
			this.vsPeriod.ColumnHeadersVisible = false;
			this.vsPeriod.FixedCols = 0;
			this.vsPeriod.FixedRows = 0;
			this.vsPeriod.Location = new System.Drawing.Point(20, 30);
			this.vsPeriod.Name = "vsPeriod";
			this.vsPeriod.RowHeadersVisible = false;
			this.vsPeriod.Rows = 1;
			this.vsPeriod.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
			this.vsPeriod.Size = new System.Drawing.Size(255, 40);
			this.vsPeriod.TabIndex = 38;
			this.ToolTip1.SetToolTip(this.vsPeriod, null);
			this.vsPeriod.DoubleClick += new System.EventHandler(this.vsPeriod_DblClick);
			// 
			// txtAcctNumber
			// 
			this.txtAcctNumber.Cols = 1;
			this.txtAcctNumber.ColumnHeadersVisible = false;
			this.txtAcctNumber.ExtendLastCol = true;
			this.txtAcctNumber.FixedCols = 0;
			this.txtAcctNumber.FixedRows = 0;
			this.txtAcctNumber.Location = new System.Drawing.Point(683, 118);
			this.txtAcctNumber.Name = "txtAcctNumber";
			this.txtAcctNumber.RowHeadersVisible = false;
			this.txtAcctNumber.Rows = 1;
			this.txtAcctNumber.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
			this.txtAcctNumber.Size = new System.Drawing.Size(143, 42);
			this.txtAcctNumber.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.txtAcctNumber, null);
			this.txtAcctNumber.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.txtAcctNumber_ValidateEdit);
			this.txtAcctNumber.Enter += new System.EventHandler(this.txtAcctNumber_Enter);
			this.txtAcctNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtAcctNumber_Validating);
			this.txtAcctNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAcctNumber_KeyDownEvent);
			// 
			// lblTax
			// 
			this.lblTax.AutoSize = true;
			this.lblTax.Location = new System.Drawing.Point(934, 90);
			this.lblTax.Name = "lblTax";
			this.lblTax.Size = new System.Drawing.Size(34, 16);
			this.lblTax.TabIndex = 31;
			this.lblTax.Text = "TAX";
			this.lblTax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblTax, null);
			// 
			// Label1_9
			// 
			this.Label1_9.AutoSize = true;
			this.Label1_9.Location = new System.Drawing.Point(581, 90);
			this.Label1_9.Name = "Label1_9";
			this.Label1_9.Size = new System.Drawing.Size(27, 16);
			this.Label1_9.TabIndex = 18;
			this.Label1_9.Text = "CD";
			this.Label1_9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label1_9, "Cash Drawer");
			// 
			// lblTotalPendingDue
			// 
			this.lblTotalPendingDue.Location = new System.Drawing.Point(708, 44);
			this.lblTotalPendingDue.Name = "lblTotalPendingDue";
			this.lblTotalPendingDue.Size = new System.Drawing.Size(105, 16);
			this.lblTotalPendingDue.TabIndex = 29;
			this.lblTotalPendingDue.Text = "TOTAL PENDING";
			this.ToolTip1.SetToolTip(this.lblTotalPendingDue, null);
			// 
			// Label1_11
			// 
			this.Label1_11.Location = new System.Drawing.Point(683, 90);
			this.Label1_11.Name = "Label1_11";
			this.Label1_11.Size = new System.Drawing.Size(53, 15);
			this.Label1_11.TabIndex = 26;
			this.Label1_11.Text = "ACCT #";
			this.Label1_11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label1_11, null);
			// 
			// Label1_1
			// 
			this.Label1_1.AutoSize = true;
			this.Label1_1.Location = new System.Drawing.Point(165, 90);
			this.Label1_1.Name = "Label1_1";
			this.Label1_1.Size = new System.Drawing.Size(44, 16);
			this.Label1_1.TabIndex = 25;
			this.Label1_1.Text = "DATE";
			this.Label1_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label1_1, null);
			// 
			// lblCode_2
			// 
			this.lblCode_2.AutoSize = true;
			this.lblCode_2.Location = new System.Drawing.Point(381, 90);
			this.lblCode_2.Name = "lblCode_2";
			this.lblCode_2.Size = new System.Drawing.Size(47, 16);
			this.lblCode_2.TabIndex = 24;
			this.lblCode_2.Text = "CODE";
			this.lblCode_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblCode_2, null);
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.AutoSize = true;
			this.lblPrincipal.Location = new System.Drawing.Point(836, 90);
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Size = new System.Drawing.Size(79, 16);
			this.lblPrincipal.TabIndex = 23;
			this.lblPrincipal.Text = "PRINCIPAL";
			this.lblPrincipal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblPrincipal, null);
			// 
			// lblInterest
			// 
			this.lblInterest.AutoSize = true;
			this.lblInterest.Location = new System.Drawing.Point(1034, 90);
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Size = new System.Drawing.Size(74, 16);
			this.lblInterest.TabIndex = 22;
			this.lblInterest.Text = "INTEREST";
			this.lblInterest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblInterest, null);
			// 
			// Label1_10
			// 
			this.Label1_10.AutoSize = true;
			this.Label1_10.Location = new System.Drawing.Point(283, 90);
			this.Label1_10.Name = "Label1_10";
			this.Label1_10.Size = new System.Drawing.Size(35, 16);
			this.Label1_10.TabIndex = 21;
			this.Label1_10.Text = "REF";
			this.Label1_10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label1_10, null);
			// 
			// lblInvoice
			// 
			this.lblInvoice.AutoSize = true;
			this.lblInvoice.Location = new System.Drawing.Point(20, 90);
			this.lblInvoice.Name = "lblInvoice";
			this.lblInvoice.Size = new System.Drawing.Size(63, 16);
			this.lblInvoice.TabIndex = 20;
			this.lblInvoice.Text = "INVOICE";
			this.lblInvoice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblInvoice, null);
			// 
			// Label1_8
			// 
			this.Label1_8.Location = new System.Drawing.Point(632, 90);
			this.Label1_8.Name = "Label1_8";
			this.Label1_8.Size = new System.Drawing.Size(23, 15);
			this.Label1_8.TabIndex = 19;
			this.Label1_8.Text = "AC";
			this.Label1_8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label1_8, "Affect Cash");
			// 
			// Label8
			// 
			this.Label8.AutoSize = true;
			this.Label8.Location = new System.Drawing.Point(20, 192);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(177, 16);
			this.Label8.TabIndex = 17;
			this.Label8.Text = "PENDING TRANSACTIONS";
			this.ToolTip1.SetToolTip(this.Label8, null);
			// 
			// Label1_12
			// 
			this.Label1_12.AutoSize = true;
			this.Label1_12.Location = new System.Drawing.Point(235, 192);
			this.Label1_12.Name = "Label1_12";
			this.Label1_12.Size = new System.Drawing.Size(77, 16);
			this.Label1_12.TabIndex = 16;
			this.Label1_12.Text = "COMMENT";
			this.ToolTip1.SetToolTip(this.Label1_12, null);
			// 
			// fraStatusLabels
			// 
			this.fraStatusLabels.Controls.Add(this.Label1_0);
			this.fraStatusLabels.Controls.Add(this.lblAccount);
			this.fraStatusLabels.Name = "fraStatusLabels";
            this.fraStatusLabels.Location = new Point(27, 25);
			this.fraStatusLabels.Size = new System.Drawing.Size(235, 30);
			this.fraStatusLabels.TabIndex = 10;
			this.ToolTip1.SetToolTip(this.fraStatusLabels, null);
			this.fraStatusLabels.Visible = false;
			// 
			// Label1_0
			// 
			this.Label1_0.Font = new System.Drawing.Font("@header", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.Label1_0.Location = new System.Drawing.Point(0, 0);
			this.Label1_0.Name = "Label1_0";
			this.Label1_0.Size = new System.Drawing.Size(98, 28);
			this.Label1_0.TabIndex = 13;
			this.Label1_0.Text = "Account";
			this.ToolTip1.SetToolTip(this.Label1_0, null);
			// 
			// lblAccount
			// 
			this.lblAccount.BackColor = System.Drawing.Color.Transparent;
			this.lblAccount.Font = new System.Drawing.Font("@header", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblAccount.Location = new System.Drawing.Point(128, 0);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(94, 28);
			this.lblAccount.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.lblAccount, null);
			this.lblAccount.Visible = false;
			// 
			// imgNote
			// 
			this.imgNote.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgNote.ImageSource = "imgnote?color=#707884";
            this.imgNote.Location = new System.Drawing.Point(27, 0);
			this.imgNote.Name = "imgNote";
			this.imgNote.Size = new System.Drawing.Size(26, 23);
			this.imgNote.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.ToolTip1.SetToolTip(this.imgNote, null);
			this.imgNote.Visible = false;
			this.imgNote.DoubleClick += new System.EventHandler(this.imgNote_DoubleClick);
            // 
            // cmdPaymentSave
            // 
            this.cmdPaymentSave.AppearanceKey = "acceptButton";
			this.cmdPaymentSave.Enabled = false;
			this.cmdPaymentSave.Location = new System.Drawing.Point(744, 34);
			this.cmdPaymentSave.Name = "cmdPaymentSave";
			this.cmdPaymentSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.cmdPaymentSave.Size = new System.Drawing.Size(187, 40);
            this.cmdPaymentSave.TabIndex = 44;
			this.cmdPaymentSave.Text = "Save Payments";
			this.ToolTip1.SetToolTip(this.cmdPaymentSave, null);
			this.cmdPaymentSave.Click += new System.EventHandler(this.fcButton1_Click);
			// 
			// cmdChangeAccount
			// 
			this.cmdChangeAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdChangeAccount.Location = new System.Drawing.Point(925, 29);
			this.cmdChangeAccount.Name = "cmdChangeAccount";
			this.cmdChangeAccount.Shortcut = Wisej.Web.Shortcut.F6;
			this.cmdChangeAccount.Size = new System.Drawing.Size(115, 24);
			this.cmdChangeAccount.TabIndex = 43;
			this.cmdChangeAccount.Text = "Change Account";
			this.ToolTip1.SetToolTip(this.cmdChangeAccount, null);
			this.cmdChangeAccount.Click += new System.EventHandler(this.cmdChangeAccount_Click);
			// 
			// cmdPrint
			// 
			this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrint.Location = new System.Drawing.Point(785, 29);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F4;
			this.cmdPrint.Size = new System.Drawing.Size(135, 24);
			this.cmdPrint.TabIndex = 42;
			this.cmdPrint.Text = "Print Account Detail";
			this.ToolTip1.SetToolTip(this.cmdPrint, null);
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// cmdEffectiveDate
			// 
			this.cmdEffectiveDate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdEffectiveDate.Location = new System.Drawing.Point(683, 29);
			this.cmdEffectiveDate.Name = "cmdEffectiveDate";
			this.cmdEffectiveDate.Shortcut = Wisej.Web.Shortcut.F2;
			this.cmdEffectiveDate.Size = new System.Drawing.Size(97, 24);
			this.cmdEffectiveDate.TabIndex = 41;
			this.cmdEffectiveDate.Text = "Effective Date";
			this.ToolTip1.SetToolTip(this.cmdEffectiveDate, null);
			this.cmdEffectiveDate.Click += new System.EventHandler(this.cmdEffectiveDate_Click);
			// 
			// lblOwnersName
			// 
			this.lblOwnersName.BackColor = System.Drawing.Color.Transparent;
            this.lblOwnersName.Capitalize = false;
            this.lblOwnersName.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblOwnersName.Location = new System.Drawing.Point(90, 37);
			this.lblOwnersName.Name = "lblOwnersName";
			this.lblOwnersName.Size = new System.Drawing.Size(370, 20);
			this.lblOwnersName.TabIndex = 12;
			this.ToolTip1.SetToolTip(this.lblOwnersName, null);
			this.lblOwnersName.Visible = false;
			// 
			// CommonDialog1
			// 
			this.CommonDialog1.Name = "CommonDialog1";
			this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
			this.ToolTip1.SetToolTip(this.CommonDialog1, null);
			// 
			// GRID
			// 
			this.GRID.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.GRID.Cols = 20;
			this.GRID.Location = new System.Drawing.Point(30, 74);
			this.GRID.Name = "GRID";
			this.GRID.Rows = 1;
			this.GRID.Size = new System.Drawing.Size(972, 157);
			this.GRID.TabIndex = 32;
			this.ToolTip1.SetToolTip(this.GRID, null);
			this.GRID.Visible = false;
			this.GRID.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GRID_MouseMoveEvent);
			this.GRID.CurrentCellChanged += new System.EventHandler(this.GRID_RowColChange);
			this.GRID.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.GRID_MouseDownEvent);
			this.GRID.RowExpanded += new Wisej.Web.DataGridViewRowEventHandler(this.GRID_RowExpanded);
			this.GRID.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.GRID_RowCollapsed);
			this.GRID.Enter += new System.EventHandler(this.GRID_Enter);
			this.GRID.Click += new System.EventHandler(this.GRID_ClickEvent);
			this.GRID.DoubleClick += new System.EventHandler(this.GRID_DblClick);
			this.GRID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.GRID_KeyPressEvent);
			// 
			// vsPreview
			// 
			this.vsPreview.Cols = 11;
			this.vsPreview.Location = new System.Drawing.Point(259, 125);
			this.vsPreview.Name = "vsPreview";
			this.vsPreview.Rows = 1;
			this.vsPreview.Size = new System.Drawing.Size(345, 157);
			this.vsPreview.TabIndex = 33;
			this.ToolTip1.SetToolTip(this.vsPreview, null);
			this.vsPreview.Visible = false;
			// 
			// lblPaymentInfo
			// 
			this.lblPaymentInfo.Location = new System.Drawing.Point(30, 24);
			this.lblPaymentInfo.Name = "lblPaymentInfo";
			this.lblPaymentInfo.Size = new System.Drawing.Size(566, 18);
			this.lblPaymentInfo.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.lblPaymentInfo, null);
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessGoTo,
            this.mnuFileMortgageHolderInfo,
            this.mnuFileAccountInfo,
            this.mnuFileEditNote,
            this.mnuFilePriority,
            this.mnuPayment});
			this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuProcessGoTo
			// 
			this.mnuProcessGoTo.Index = 0;
			this.mnuProcessGoTo.Name = "mnuProcessGoTo";
			this.mnuProcessGoTo.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuProcessGoTo.Text = "Go To Status View";
			this.mnuProcessGoTo.Click += new System.EventHandler(this.mnuProcessGoTo_Click);
			// 
			// mnuFileMortgageHolderInfo
			// 
			this.mnuFileMortgageHolderInfo.Index = 1;
			this.mnuFileMortgageHolderInfo.Name = "mnuFileMortgageHolderInfo";
			this.mnuFileMortgageHolderInfo.Shortcut = Wisej.Web.Shortcut.F7;
			this.mnuFileMortgageHolderInfo.Text = "Mortgage Holder Information";
			this.mnuFileMortgageHolderInfo.Visible = false;
			// 
			// mnuFileAccountInfo
			// 
			this.mnuFileAccountInfo.Index = 2;
			this.mnuFileAccountInfo.Name = "mnuFileAccountInfo";
			this.mnuFileAccountInfo.Shortcut = Wisej.Web.Shortcut.F8;
			this.mnuFileAccountInfo.Text = "Show Account Info";
			// 
			// mnuFileEditNote
			// 
			this.mnuFileEditNote.Index = 3;
			this.mnuFileEditNote.Name = "mnuFileEditNote";
			this.mnuFileEditNote.Text = "Add/Edit Note";
			this.mnuFileEditNote.Click += new System.EventHandler(this.mnuFileEditNote_Click);
			// 
			// mnuFilePriority
			// 
			this.mnuFilePriority.Index = 4;
			this.mnuFilePriority.Name = "mnuFilePriority";
			this.mnuFilePriority.Text = "Set Note Priority";
			this.mnuFilePriority.Visible = false;
			this.mnuFilePriority.Click += new System.EventHandler(this.mnuFilePriority_Click);
			// 
			// mnuPayment
			// 
			this.mnuPayment.Index = 5;
			this.mnuPayment.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuPaymentClearPayment,
            this.mnuPaymentClearList,
            this.mnuPaymentPreview});
			this.mnuPayment.Name = "mnuPayment";
			this.mnuPayment.Text = "Options";
			this.mnuPayment.Visible = false;
			// 
			// mnuPaymentClearPayment
			// 
			this.mnuPaymentClearPayment.Index = 0;
			this.mnuPaymentClearPayment.Name = "mnuPaymentClearPayment";
			this.mnuPaymentClearPayment.Text = "Clear Payment Boxes";
			this.mnuPaymentClearPayment.Click += new System.EventHandler(this.mnuPaymentClearPayment_Click);
			// 
			// mnuPaymentClearList
			// 
			this.mnuPaymentClearList.Index = 1;
			this.mnuPaymentClearList.Name = "mnuPaymentClearList";
			this.mnuPaymentClearList.Text = "Clear Pending Transactions";
			this.mnuPaymentClearList.Click += new System.EventHandler(this.mnuPaymentClearList_Click);
			// 
			// mnuPaymentPreview
			// 
			this.mnuPaymentPreview.Index = 2;
			this.mnuPaymentPreview.Name = "mnuPaymentPreview";
			this.mnuPaymentPreview.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuPaymentPreview.Text = "Preview";
			this.mnuPaymentPreview.Click += new System.EventHandler(this.mnuPaymentPreview_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Enabled = false;
			this.mnuProcess.Index = -1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessEffective
			// 
			this.mnuProcessEffective.Index = -1;
			this.mnuProcessEffective.Name = "mnuProcessEffective";
			this.mnuProcessEffective.Shortcut = Wisej.Web.Shortcut.F2;
			this.mnuProcessEffective.Text = "Effective Date";
			this.mnuProcessEffective.Click += new System.EventHandler(this.mnuProcessEffective_Click);
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = -1;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuFilePrint.Text = "Print Account Detail";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuProcessChangeAccount
			// 
			this.mnuProcessChangeAccount.Index = -1;
			this.mnuProcessChangeAccount.Name = "mnuProcessChangeAccount";
			this.mnuProcessChangeAccount.Shortcut = Wisej.Web.Shortcut.F6;
			this.mnuProcessChangeAccount.Text = "Change Account";
			this.mnuProcessChangeAccount.Click += new System.EventHandler(this.mnuProcessChangeAccount_Click);
			// 
			// mnuPaymentSeperator
			// 
			this.mnuPaymentSeperator.Index = -1;
			this.mnuPaymentSeperator.Name = "mnuPaymentSeperator";
			this.mnuPaymentSeperator.Text = "-";
			// 
			// mnuPaymentSave
			// 
			this.mnuPaymentSave.Enabled = false;
			this.mnuPaymentSave.Index = -1;
			this.mnuPaymentSave.Name = "mnuPaymentSave";
			this.mnuPaymentSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuPaymentSave.Text = "Save Payments";
			this.mnuPaymentSave.Click += new System.EventHandler(this.mnuPaymentSave_Click);
			// 
			// mnuPaymentSaveExit
			// 
			this.mnuPaymentSaveExit.Enabled = false;
			this.mnuPaymentSaveExit.Index = -1;
			this.mnuPaymentSaveExit.Name = "mnuPaymentSaveExit";
			this.mnuPaymentSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPaymentSaveExit.Text = "Save Payments & Exit";
			this.mnuPaymentSaveExit.Click += new System.EventHandler(this.mnuPaymentSaveExit_Click);
			// 
			// mnuSpacer
			// 
			this.mnuSpacer.Index = -1;
			this.mnuSpacer.Name = "mnuSpacer";
			this.mnuSpacer.Text = "-";
			// 
			// mnuProcessExit
			// 
			this.mnuProcessExit.Index = -1;
			this.mnuProcessExit.Name = "mnuProcessExit";
			this.mnuProcessExit.Text = "Exit";
			this.mnuProcessExit.Click += new System.EventHandler(this.mnuProcessExit_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Enabled = false;
			this.cmdProcess.Location = new System.Drawing.Point(446, 34);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(187, 40);
			this.cmdProcess.Text = "Save Payments & Exit";
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// lblName
			// 
			this.lblName.BackColor = System.Drawing.Color.Transparent;
			this.lblName.Font = new System.Drawing.Font("@default", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblName.Location = new System.Drawing.Point(30, 37);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(60, 20);
			this.lblName.TabIndex = 40;
			this.lblName.Text = "NAME";
			this.lblName.Visible = false;
			// 
			// frmARCLStatus
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1180, 666);
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmARCLStatus";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Status Screen";
			this.ToolTip1.SetToolTip(this, null);
			this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.Load += new System.EventHandler(this.frmARCLStatus_Load);
			this.Activated += new System.EventHandler(this.frmARCLStatus_Activated);
			this.Enter += new System.EventHandler(this.frmARCLStatus_Enter);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmARCLStatus_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).EndInit();
			this.fraRateInfo.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdRIClose)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsRateInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraPayment)).EndInit();
			this.fraPayment.ResumeLayout(false);
			this.fraPayment.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsPayments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTransactionDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAcctNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraStatusLabels)).EndInit();
			this.fraStatusLabels.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.imgNote)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPaymentSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdChangeAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEffectiveDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GRID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsPreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdEffectiveDate;
		private FCButton cmdPrint;
		private FCButton cmdChangeAccount;
		private FCButton cmdProcess;
		private FCButton cmdPaymentSave;
		public FCLabel lblName;
	}
}