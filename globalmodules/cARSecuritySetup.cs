﻿//Fecher vbPorter - Version 1.0.0.40

using System.Collections.Generic;
using fecherFoundation;

namespace Global
{
	public class cARSecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cARSecuritySetup() : base()
		{
			strThisModule = "AR";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem(" Enter Accounts Receivable", "", 1, false, ""));
			AddItem_2(CreateItem("Billing Process", "", 2, true, ""));
			AddItem_2(CreateItem("Billing Process", "Auto Post Billing Process", 24, false, ""));
			AddItem_2(CreateItem("Collections", "", 3, true, ""));
			AddItem_2(CreateItem("Collections", "Charge Interest", 14, false, ""));
			AddItem_2(CreateItem("Collections", "Daily Audit Report", 10, false, ""));
			AddItem_2(CreateItem("Collections", "Load Back", 11, false, ""));
			AddItem_2(CreateItem("Collections", "Payments & Adjustments", 9, false, ""));
			AddItem_2(CreateItem("Collections", "View Status", 8, false, ""));
			AddItem_2(CreateItem(@"Customer / Bill Update", "", 21, false, ""));
			AddItem_2(CreateItem("File Maintenance", "", 6, true, ""));
			AddItem_2(CreateItem("File Maintenance", "Customize", 15, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Edit Bill Information", 22, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Edit Custom Bill Types", 20, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Rate Record Update", 23, false, ""));
			AddItem_2(CreateItem("Printing", "", 7, false, ""));
			AddItem_2(CreateItem("Type Setup", "", 4, false, ""));
		}
	}
}
