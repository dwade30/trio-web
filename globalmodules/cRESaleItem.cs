﻿//Fecher vbPorter - Version 1.0.0.59
using Microsoft.VisualBasic;

namespace TWXF0000
{
	public class cRESaleItem
	{
		//=========================================================
		private short intSaleVerification;
		private short intSaleFinancing;
		private short intSaleType;
		private double dblSalePrice;
		private short intSaleValidity;
		private string strSaleDate = "";

		public string SaleDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strSaleDate = value;
				}
				else
				{
					strSaleDate = "";
				}
			}
			get
			{
				string SaleDate = "";
				SaleDate = strSaleDate;
				return SaleDate;
			}
		}

		public short ValidityCode
		{
			set
			{
				intSaleValidity = value;
			}
			get
			{
				short ValidityCode = 0;
				ValidityCode = intSaleValidity;
				return ValidityCode;
			}
		}

		public double Price
		{
			set
			{
				dblSalePrice = value;
			}
			get
			{
				double Price = 0;
				Price = dblSalePrice;
				return Price;
			}
		}

		public short SaleTypeCode
		{
			set
			{
				intSaleType = value;
			}
			get
			{
				short SaleTypeCode = 0;
				SaleTypeCode = intSaleType;
				return SaleTypeCode;
			}
		}

		public short FinancingCode
		{
			set
			{
				intSaleFinancing = value;
			}
			get
			{
				short FinancingCode = 0;
				FinancingCode = intSaleFinancing;
				return FinancingCode;
			}
		}

		public short VerificationCode
		{
			set
			{
				intSaleVerification = value;
			}
			get
			{
				short VerificationCode = 0;
				VerificationCode = intSaleVerification;
				return VerificationCode;
			}
		}
	}
}
