﻿namespace Global
{
	public class cPartyPhoneNumber
	{
		//=========================================================
		private int lngID;
		private int lngParentID;
		private string strPhoneNumber = "";
		private string strDescription = "";
		private int intPhoneOrder;
		private string strExtension = "";

		public int ID
		{
			get
			{
				int ID = 0;
				ID = lngID;
				return ID;
			}
			set
			{
				lngID = value;
			}
		}

		public int ParentID
		{
			get
			{
				int ParentID = 0;
				ParentID = lngParentID;
				return ParentID;
			}
			set
			{
				lngParentID = value;
			}
		}

		public string PhoneNumber
		{
			get
			{
				string PhoneNumber = "";
				PhoneNumber = strPhoneNumber;
				return PhoneNumber;
			}
			set
			{
				strPhoneNumber = value;
			}
		}

		public string Description
		{
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
			set
			{
				strDescription = value;
			}
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public int PhoneOrder
		{
			get
			{
				return intPhoneOrder;
			}
			set
			{
				intPhoneOrder = value;
			}
		}

		public string Extension
		{
			get
			{
				string Extension = "";
				Extension = strExtension;
				return Extension;
			}
			set
			{
				strExtension = value;
			}
		}
	}
}
