﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using System;
using System.Collections.Generic;
using TWSharedLibrary;

namespace Global
{
	public class cSettingsController
	{
		//=========================================================
		// Owner and owner type are equivalent to the root path of the registry
		// Owner Types are:
		// blank = global. Owner would also be left blank in this case
		// Machine
		// User
		// EnvironmentID   this is for multitowns.  Most towns will use an EnvironmentID of 0
		// SettingType is similar to the sub path of the registry.  OwnerType would be like choosing local machine or local user
		// SettingType could by PY or PY/Printers etc.  just a way of categorizing the settings
		// SettingName is like KeyName in the registry
		// vbPorter upgrade warning: strOwner As string	OnWrite(string, short)
		public string GetSettingValue(string strSettingName, string strSettingType, string strOwnerType, string strOwner, string strDB)
		{
			string GetSettingValue = "";
			cSetting setVal;
			setVal = GetSetting(strSettingName, strSettingType, strOwnerType, strOwner, strDB);
			if (!(setVal == null))
			{
				GetSettingValue = setVal.SettingValue;
			}
			else
			{
				GetSettingValue = "";
			}
			return GetSettingValue;
		}

        public string GetSettingValue(string strSettingName, string strSettingType, string strOwnerType, string defaultValue = "")
        {
            string GetSettingValue = "";
            cSetting setVal;
            setVal = GetSetting(strSettingName, strSettingType, strOwnerType, TWSharedLibrary.Variables.Statics.UserName, "");
            if (!(setVal == null))
            {
                GetSettingValue = setVal.SettingValue;
            }
            else
            {
                GetSettingValue = defaultValue;
            }
            return GetSettingValue;
        }

        public cSetting GetSetting(string strSettingName, string strSettingType, string strOwnerType, string strOwner, string strDB)
		{
			cSetting GetSetting = null;
			string strDatabaseToUse;
			strDatabaseToUse = strDB;
			if (strDB == "")
			{
				strDatabaseToUse = "CentralData";
			}
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			strSQL = "select * from settings where settingname = '" + strSettingName + "' and settingtype = '" + strSettingType + "' and Owner = '" + strOwner + "' and Ownertype = '" + strOwnerType + "'";
			rsLoad.OpenRecordset(strSQL, strDatabaseToUse);
			if (!rsLoad.EndOfFile())
			{
				cSetting retSet = new cSetting();
				retSet.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
				retSet.Owner = FCConvert.ToString(rsLoad.Get_Fields_String("owner"));
				retSet.OwnerType = FCConvert.ToString(rsLoad.Get_Fields_String("ownertype"));
				retSet.SettingName = FCConvert.ToString(rsLoad.Get_Fields_String("settingname"));
				retSet.SettingType = FCConvert.ToString(rsLoad.Get_Fields_String("settingtype"));
				retSet.SettingValue = FCConvert.ToString(rsLoad.Get_Fields_String("settingvalue"));
				GetSetting = retSet;
			}
			else
			{
				GetSetting = null;
			}
			return GetSetting;
		}
		// vbPorter upgrade warning: strOwner As string	OnWrite(string, short)
		public void SaveSetting(string strValue, string strSettingName, string strSettingType, string strOwnerType, string strOwner, string strDB)
		{
			string strDatabaseToUse;
			strDatabaseToUse = strDB;
			if (strDB == "")
			{
				strDatabaseToUse = "CentralData";
			}
			string strSQL;
			clsDRWrapper rsSave = new clsDRWrapper();
			strSQL = "select * from settings where settingname = '" + strSettingName + "' and settingtype = '" + strSettingType + "' and Owner = '" + strOwner + "' and Ownertype = '" + strOwnerType + "'";
			rsSave.OpenRecordset(strSQL, strDatabaseToUse);
			if (rsSave.EndOfFile())
			{
				rsSave.AddNew();
				rsSave.Set_Fields("SettingName", strSettingName);
				rsSave.Set_Fields("SettingType", strSettingType);
				rsSave.Set_Fields("OwnerType", strOwnerType);
				rsSave.Set_Fields("Owner", strOwner);
			}
			else
			{
				rsSave.Edit();
			}
			rsSave.Set_Fields("SettingValue", strValue);
			rsSave.Update();
		}

        public void SaveSetting(string strValue, string strSettingName, string strSettingType, string strOwnerType)
        {
            SaveSetting(strValue, strSettingName, strSettingType, strOwnerType,TWSharedLibrary.Variables.Statics.UserName, "");
        }


        public void RemoveSetting(string strSettingName, string strSettingType, string strOwnerType, string strOwner, ref string strDB)
		{
			string strDatabaseToUse;
			strDatabaseToUse = strDB;
			if (strDB == "")
			{
				strDatabaseToUse = "CentralData";
			}
			string strSQL;
			clsDRWrapper rsSave = new clsDRWrapper();
			strSQL = "delete from settings where settingname = '" + strSettingName + "' and settingtype = '" + strSettingType + "' and Owner = '" + strOwner + "' and Ownertype = '" + strOwnerType + "'";
			rsSave.Execute(strSQL, strDatabaseToUse);
		}

		public void RemoveAllSettings(string strSettingName, string strSettingType, string strOwnerType, ref string strDB)
		{
			string strDatabaseToUse;
			strDatabaseToUse = strDB;
			if (strDB == "")
			{
				strDatabaseToUse = "CentralData";
			}
			string strSQL;
			clsDRWrapper rsSave = new clsDRWrapper();
			strSQL = "delete from settings where settingname = '" + strSettingName + "' and settingtype = '" + strSettingType + "' and Ownertype = '" + strOwnerType + "'";
			rsSave.Execute(strSQL, strDatabaseToUse);
		}

		public bool AddSettingsTable(string strDB)
		{
			bool AddSettingsTable = false;
			clsDRWrapper rsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				string strSQL;
				strSQL = "Select * from sys.objects where object_id = OBJECT_ID(N'Settings') and type in (N'U')";
				rsSave.OpenRecordset(strSQL, strDB);
				if (rsSave.EndOfFile())
				{
					strSQL = "CREATE TABLE [dbo].[Settings](" + "[ID] [int] IDENTITY(1,1) NOT NULL," + "[SettingName] [nvarchar](255) NULL," + "[SettingValue] [nvarchar](255) NULL," + "[SettingType] [nvarchar](255) NULL," + "[Owner] [nvarchar](255) NULL," + "[OwnerType] [nvarchar](255) NULL," + "CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED " + "([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" + ") ON [PRIMARY]";
					rsSave.Execute(strSQL, strDB);
				}
				AddSettingsTable = true;
				return AddSettingsTable;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
			return AddSettingsTable;
		}

		public FCCollection GetSettings(string strSettingType, string strOwnerType, string strOwner, string strDB)
		{
			FCCollection coll = new FCCollection();
			string strDatabaseToUse;
			strDatabaseToUse = strDB;
			if (strDB == "")
			{
				strDatabaseToUse = "CentralData";
			}
			string strSQL;
			clsDRWrapper rsLoad = new clsDRWrapper();
			strSQL = "select * from settings where settingtype = '" + strSettingType + "' and Owner = '" + strOwner + "' and Ownertype = '" + strOwnerType + "'";
			rsLoad.OpenRecordset(strSQL, strDatabaseToUse);
			cSetting retSet;
			while (!rsLoad.EndOfFile())
			{
				retSet = new cSetting();
				retSet.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
				retSet.Owner = FCConvert.ToString(rsLoad.Get_Fields_String("owner"));
				retSet.OwnerType = FCConvert.ToString(rsLoad.Get_Fields_String("ownertype"));
				retSet.SettingName = FCConvert.ToString(rsLoad.Get_Fields_String("settingname"));
				retSet.SettingType = FCConvert.ToString(rsLoad.Get_Fields_String("settingtype"));
				retSet.SettingValue = FCConvert.ToString(rsLoad.Get_Fields_String("settingvalue"));
				coll.Add(retSet);
				rsLoad.MoveNext();
			}
			return coll;
		}
	}
}
