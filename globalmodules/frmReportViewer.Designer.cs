﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmReportViewer.
	/// </summary>
	partial class frmReportViewer : BaseForm
	{
		public ARViewer ARViewer21;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuEmail;
		public fecherFoundation.FCToolStripMenuItem mnuEMailRTF;
		public fecherFoundation.FCToolStripMenuItem mnuEmailPDF;
		public fecherFoundation.FCToolStripMenuItem mnuExport;
		public fecherFoundation.FCToolStripMenuItem mnuRTF;
		public fecherFoundation.FCToolStripMenuItem mnuExportPDF;
		public fecherFoundation.FCToolStripMenuItem mnuHTML;
		public fecherFoundation.FCToolStripMenuItem mnuExcel;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReportViewer));
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuEmail = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEMailRTF = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEmailPDF = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRTF = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExportPDF = new fecherFoundation.FCToolStripMenuItem();
            this.mnuHTML = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExcel = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.toolBar1 = new Wisej.Web.ToolBar();
            this.toolBarButtonEmailPDF = new Wisej.Web.ToolBarButton();
            this.toolBarButtonEmailRTF = new Wisej.Web.ToolBarButton();
            this.toolBarButtonExportPDF = new Wisej.Web.ToolBarButton();
            this.toolBarButtonExportRTF = new Wisej.Web.ToolBarButton();
            this.toolBarButtonExportHTML = new Wisej.Web.ToolBarButton();
            this.toolBarButtonExportExcel = new Wisej.Web.ToolBarButton();
            this.toolBarButtonPrint = new Wisej.Web.ToolBarButton();
            this.ARViewer21 = new Global.ARViewer();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 560);
            this.BottomPanel.Size = new System.Drawing.Size(949, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.ARViewer21);
            this.ClientArea.Controls.Add(this.toolBar1);
            this.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.ClientArea.Size = new System.Drawing.Size(969, 560);
            this.ClientArea.Controls.SetChildIndex(this.toolBar1, 0);
            this.ClientArea.Controls.SetChildIndex(this.ARViewer21, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(969, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEmail,
            this.mnuExport,
            this.mnuPrint});
            this.MainMenu1.Name = null;
            // 
            // mnuEmail
            // 
            this.mnuEmail.Index = 0;
            this.mnuEmail.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEMailRTF,
            this.mnuEmailPDF});
            this.mnuEmail.Name = "mnuEmail";
            this.mnuEmail.Text = "E-mail";
            // 
            // mnuEMailRTF
            // 
            this.mnuEMailRTF.Index = 0;
            this.mnuEMailRTF.Name = "mnuEMailRTF";
            this.mnuEMailRTF.Text = "E-mail as Rich Text";
            this.mnuEMailRTF.Click += new System.EventHandler(this.mnuEMailRTF_Click);
            // 
            // mnuEmailPDF
            // 
            this.mnuEmailPDF.Index = 1;
            this.mnuEmailPDF.Name = "mnuEmailPDF";
            this.mnuEmailPDF.Text = "E-mail as PDF";
            this.mnuEmailPDF.Click += new System.EventHandler(this.mnuEmailPDF_Click);
            // 
            // mnuExport
            // 
            this.mnuExport.Index = 1;
            this.mnuExport.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuRTF,
            this.mnuExportPDF,
            this.mnuHTML,
            this.mnuExcel});
            this.mnuExport.Name = "mnuExport";
            this.mnuExport.Text = "Export";
            // 
            // mnuRTF
            // 
            this.mnuRTF.Index = 0;
            this.mnuRTF.Name = "mnuRTF";
            this.mnuRTF.Text = "Export as Rich Text";
            this.mnuRTF.Click += new System.EventHandler(this.mnuRTF_Click);
            // 
            // mnuExportPDF
            // 
            this.mnuExportPDF.Index = 1;
            this.mnuExportPDF.Name = "mnuExportPDF";
            this.mnuExportPDF.Text = "Export as PDF";
            this.mnuExportPDF.Click += new System.EventHandler(this.mnuExportPDF_Click);
            // 
            // mnuHTML
            // 
            this.mnuHTML.Index = 2;
            this.mnuHTML.Name = "mnuHTML";
            this.mnuHTML.Text = "Export as HTML";
            this.mnuHTML.Click += new System.EventHandler(this.mnuHTML_Click);
            // 
            // mnuExcel
            // 
            this.mnuExcel.Index = 3;
            this.mnuExcel.Name = "mnuExcel";
            this.mnuExcel.Text = "Export as Excel";
            this.mnuExcel.Click += new System.EventHandler(this.mnuExcel_Click);
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 2;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = -1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // toolBar1
            // 
            this.toolBar1.AutoSize = false;
            this.toolBar1.BackColor = System.Drawing.Color.FromArgb(244, 247, 249);
            this.toolBar1.Buttons.AddRange(new Wisej.Web.ToolBarButton[] {
            this.toolBarButtonEmailPDF,
            this.toolBarButtonEmailRTF,
            this.toolBarButtonExportPDF,
            this.toolBarButtonExportRTF,
            this.toolBarButtonExportHTML,
            this.toolBarButtonExportExcel,
            this.toolBarButtonPrint});
            this.toolBar1.Location = new System.Drawing.Point(0, 0);
            this.toolBar1.Name = "toolBar1";
            this.toolBar1.Size = new System.Drawing.Size(969, 40);
            this.toolBar1.TabIndex = 0;
            this.toolBar1.TabStop = false;
            this.toolBar1.ButtonClick += new Wisej.Web.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
            // 
            // toolBarButtonEmailPDF
            // 
            this.toolBarButtonEmailPDF.ImageSource = "icon-report-email-pdf";
            this.toolBarButtonEmailPDF.Name = "toolBarButtonEmailPDF";
            this.toolBarButtonEmailPDF.ToolTipText = "Email as PDF";
            // 
            // toolBarButtonEmailRTF
            // 
            this.toolBarButtonEmailRTF.ImageSource = "icon-report-email-rtf";
            this.toolBarButtonEmailRTF.Name = "toolBarButtonEmailRTF";
            this.toolBarButtonEmailRTF.ToolTipText = "Email as RTF";
            // 
            // toolBarButtonExportPDF
            // 
            this.toolBarButtonExportPDF.ImageSource = "icon-report-export-pdf";
            this.toolBarButtonExportPDF.Name = "toolBarButtonExportPDF";
            this.toolBarButtonExportPDF.ToolTipText = "Export as PDF";
            // 
            // toolBarButtonExportRTF
            // 
            this.toolBarButtonExportRTF.ImageSource = "icon-report-export-rtf";
            this.toolBarButtonExportRTF.Name = "toolBarButtonExportRTF";
            this.toolBarButtonExportRTF.ToolTipText = "Export as RTF";
            // 
            // toolBarButtonExportHTML
            // 
            this.toolBarButtonExportHTML.ImageSource = "icon-report-export-html";
            this.toolBarButtonExportHTML.Name = "toolBarButtonExportHTML";
            this.toolBarButtonExportHTML.ToolTipText = "Export as HTML";
            // 
            // toolBarButtonExportExcel
            // 
            this.toolBarButtonExportExcel.ImageSource = "icon-report-export-excel";
            this.toolBarButtonExportExcel.Name = "toolBarButtonExportExcel";
            this.toolBarButtonExportExcel.ToolTipText = "Export as Excel";
            // 
            // toolBarButtonPrint
            // 
            this.toolBarButtonPrint.ImageSource = "icon-report-print";
            this.toolBarButtonPrint.Name = "toolBarButtonPrint";
            this.toolBarButtonPrint.ToolTipText = "Print";
            // 
            // ARViewer21
            // 
            this.ARViewer21.Dock = Wisej.Web.DockStyle.Fill;
            this.ARViewer21.Location = new System.Drawing.Point(0, 40);
            this.ARViewer21.Name = "ARViewer21";
            this.ARViewer21.ScrollBars = false;
            this.ARViewer21.Size = new System.Drawing.Size(969, 520);
            this.ARViewer21.TabIndex = 1;
            // 
            // frmReportViewer
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(969, 560);
            this.Menu = this.MainMenu1;
            this.Name = "frmReportViewer";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Report Viewer";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmReportViewer_Load);
            this.Activated += new System.EventHandler(this.frmReportViewer_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReportViewer_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion

		private ToolBar toolBar1;
		private ToolBarButton toolBarButtonEmailPDF;
		private ToolBarButton toolBarButtonExportPDF;
		private ToolBarButton toolBarButtonPrint;
		private ToolBarButton toolBarButtonEmailRTF;
		private ToolBarButton toolBarButtonExportRTF;
		private ToolBarButton toolBarButtonExportHTML;
		private ToolBarButton toolBarButtonExportExcel;
	}
}
