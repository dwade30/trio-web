﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

#if TWCR0000
using TWCR0000;


#elif TWUT0000
using modGlobal = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for sarUTDATypeSummary.
	/// </summary>
	public partial class sarUTDATypeSummary : FCSectionReport
	{
		// nObj = 1
		//   0	sarUTDATypeSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/10/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/15/2004              *
		// ********************************************************
		float lngWidth;
		clsDRWrapper rsData = new clsDRWrapper();
		string strSQL = "";
		bool boolEndReport;
		double dblPrinSum;
		double dblTaxSum;
		double dblIntSum;
		double dblCostSum;
		bool boolWide;
		int intType;

		public sarUTDATypeSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarUTDATypeSummary_ReportEnd;
		}

        private void SarUTDATypeSummary_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        public static sarUTDATypeSummary InstancePtr
		{
			get
			{
				return (sarUTDATypeSummary)Sys.GetInstance(typeof(sarUTDATypeSummary));
			}
		}

		protected sarUTDATypeSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true)
			{
				eArgs.EOF = false;
				BindFields();
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQLAddition = "";
			// intType = rptUTDailyAuditMaster.intType
			intType = FCConvert.ToInt16(this.UserData);
			switch (intType)
			{
				case 0:
					{
						strSQLAddition = "AND Service <> 'S' ";
						break;
					}
				case 1:
					{
						strSQLAddition = "AND Service <> 'W' ";
						break;
					}
				case 2:
					{
						strSQLAddition = "";
						break;
					}
			}
			//end switch
			// default the totals
			fldTotalPrin.Text = " 0.00";
			fldTotalTax.Text = "0.00";
			fldTotalInt.Text = " 0.00";
			fldTotalCosts.Text = " 0.00";
			fldTotalTotal.Text = " 0.00";
			// this will put the SQL string together and set the order
			if (modGlobalConstants.Statics.gboolCR)
			{
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(Tax) AS TTax, SUM(CurrentInterest + PreLienInterest) AS Interest, SUM(LienCost) AS Cost, Code FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' AND ReceiptNumber > 0 GROUP BY Code ORDER BY Code";
			}
			else
			{
				strSQL = "SELECT SUM(Principal) AS Prin, SUM(Tax) AS TTax, SUM(CurrentInterest + PreLienInterest) AS Interest, SUM(LienCost) AS Cost, Code FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND Code <> 'I' GROUP BY Code ORDER BY Code";
			}
			rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			SetupFields();
			frmWait.InstancePtr.IncrementProgress();
			if (rsData.EndOfFile())
			{
				EndRoutine();
			}
		}

		private void SetupFields()
		{
			// this will adjust the fields to thier correct
			boolWide = rptUTDailyAuditMaster.InstancePtr.boolLandscape;
			lngWidth = rptUTDailyAuditMaster.InstancePtr.lngWidth;
			// set the full page objects
			this.PrintWidth = lngWidth * 1.1F;
			// Me.Detail.Width = lngWidth
			lblHeader.Width = lngWidth - 1 / 1440F;
			lnHeader.X2 = lngWidth - 1 / 1440F;
			// header labels/lines
			if (!boolWide)
			{
				lblType.Left = 0;
				lblDesc.Left = 720 / 1440F;
				lblPrincipal.Left = 2760 / 1440F;
				lblTax.Left = 4080 / 1440F;
				lblInterest.Left = 5610 / 1440F;
				lblCosts.Left = 7140 / 1440F;
				lblTotal.Left = 8670 / 1440F;
			}
			else
			{
				lblType.Left = 0;
				lblDesc.Left = 1350 / 1440F;
				lblPrincipal.Left = 3470 / 1440F;
				lblTax.Left = 5730 / 1440F;
				lblInterest.Left = 7800 / 1440F;
				lblCosts.Left = 9580 / 1440F;
				lblTotal.Left = 11080 / 1440F;
			}
			// Lefts
			// detail section
			fldType.Left = lblType.Left;
			fldDesc.Left = lblDesc.Left;
			fldPrincipal.Left = lblPrincipal.Left;
			fldTax.Left = lblTax.Left;
			fldInterest.Left = lblInterest.Left;
			fldCosts.Left = lblCosts.Left;
			fldTotal.Left = lblTotal.Left;
			// footer labels/fields
			lblTotals.Left = lblDesc.Left;
			fldTotalPrin.Left = lblPrincipal.Left;
			fldTotalTax.Left = lblTax.Left;
			fldTotalInt.Left = lblInterest.Left;
			fldTotalCosts.Left = lblCosts.Left;
			fldTotalTotal.Left = lblTotal.Left;
			// Widths
			// detail section
			fldType.Width = lblType.Width;
			fldDesc.Width = lblDesc.Width;
			fldPrincipal.Width = lblPrincipal.Width;
			fldTax.Width = lblTax.Width;
			fldInterest.Width = lblInterest.Width;
			fldCosts.Width = lblCosts.Width;
			fldTotal.Width = lblTotal.Width;
			// footer labels/fields
			lblTotals.Width = lblDesc.Width;
			fldTotalPrin.Width = lblPrincipal.Width;
			fldTotalTax.Width = lblTax.Width;
			fldTotalInt.Width = lblInterest.Width;
			fldTotalCosts.Width = lblCosts.Width;
			fldTotalTotal.Width = lblTotal.Width;
			lnTotals.X1 = lblPrincipal.Left;
			lnTotals.X2 = fldTotalTotal.Left + fldTotalTotal.Width;
		}

		private void BindFields()
		{
			// this will fill the fields with the correct data
			frmWait.InstancePtr.IncrementProgress();
			fldType.Text = rsData.Get_Fields_String("Code");
			fldDesc.Text = GetDesc_2(rsData.Get_Fields_String("Code"));
			fldPrincipal.Text = Strings.Format(rsData.Get_Fields_Double("Prin"), "#,##0.00");
			dblPrinSum += FCConvert.ToDouble(rsData.Get_Fields_Double("Prin"));
			fldTax.Text = Strings.Format(rsData.Get_Fields("TTax"), "#,##0.00");
			dblTaxSum += FCConvert.ToDouble(rsData.Get_Fields("TTax"));
			fldInterest.Text = Strings.Format(rsData.Get_Fields("Interest"), "#,##0.00");
			dblIntSum += FCConvert.ToDouble(rsData.Get_Fields("Interest"));
			fldCosts.Text = Strings.Format(rsData.Get_Fields("Cost"), "#,##0.00");
			dblCostSum += FCConvert.ToDouble(rsData.Get_Fields("Cost"));
			fldTotal.Text = Strings.Format(rsData.Get_Fields_Decimal("Prin") + rsData.Get_Fields("Interest") + rsData.Get_Fields("TTax") + rsData.Get_Fields("Cost"), "#,##0.00");
			// move to the next record
			rsData.MoveNext();
		}

		private void EndRoutine()
		{
			// this will show a message that there are no records to process and hide the rest of the report
			lnHeader.Visible = false;
			lblHeader.Visible = false;
			lnTotals.Visible = false;
			lblPrincipal.Visible = false;
			lblTax.Visible = false;
			lblInterest.Visible = false;
			lblCosts.Visible = false;
			lblTotal.Visible = false;
			fldTotalPrin.Visible = false;
			fldTotalTax.Visible = false;
			fldTotalInt.Visible = false;
			fldTotalCosts.Visible = false;
			fldTotalTotal.Visible = false;
			lblTotals.Visible = false;
			// lblTotals.Width = lngWidth - lblTotals.Left
			// lblTotals.Text = "There are no records to process."
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalPrin.Text = Strings.Format(dblPrinSum, "#,##0.00");
			fldTotalTax.Text = Strings.Format(dblTaxSum, "#,##0.00");
			fldTotalInt.Text = Strings.Format(dblIntSum, "#,##0.00");
			fldTotalCosts.Text = Strings.Format(dblCostSum, "#,##0.00");
			// calculate the total line
			fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldTotalPrin.Text) + FCConvert.ToDouble(fldTotalTax.Text) + FCConvert.ToDouble(fldTotalInt.Text) + FCConvert.ToDouble(fldTotalCosts.Text), "#,##0.00");
		}

		private string GetDesc_2(string strCode)
		{
			return GetDesc(ref strCode);
		}

		private string GetDesc(ref string strCode)
		{
			string GetDesc = "";
			if (Strings.UCase(strCode) == "3")
			{
				GetDesc = "Demand Fees";
			}
			else if (Strings.UCase(strCode) == "A")
			{
				GetDesc = "Abatement";
			}
			else if (Strings.UCase(strCode) == "C")
			{
				GetDesc = "Correction";
			}
			else if (Strings.UCase(strCode) == "D")
			{
				GetDesc = "Discount";
			}
			else if (Strings.UCase(strCode) == "L")
			{
				GetDesc = "Lien Costs";
			}
			else if (Strings.UCase(strCode) == "P")
			{
				GetDesc = "Payment";
			}
			else if (Strings.UCase(strCode) == "R")
			{
				GetDesc = "Refunded Abatement";
			}
			else if (Strings.UCase(strCode) == "S")
			{
				GetDesc = "Supplemental";
			}
			else if (Strings.UCase(strCode) == "U")
			{
				GetDesc = "Tax Club";
			}
			else if (Strings.UCase(strCode) == "Y")
			{
				GetDesc = "Pre Payment";
			}
			else
			{
				GetDesc = "";
			}
			return GetDesc;
		}

		private void sarUTDATypeSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarUTDATypeSummary.Caption	= "Daily Audit Detail";
			//sarUTDATypeSummary.Icon	= "sarUTDATypeSummary.dsx":0000";
			//sarUTDATypeSummary.Left	= 0;
			//sarUTDATypeSummary.Top	= 0;
			//sarUTDATypeSummary.Width	= 11880;
			//sarUTDATypeSummary.Height	= 8175;
			//sarUTDATypeSummary.StartUpPosition	= 3;
			//sarUTDATypeSummary.SectionData	= "sarUTDATypeSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
