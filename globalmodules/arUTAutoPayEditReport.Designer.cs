using fecherFoundation;
using TWSharedLibrary;

namespace Global
{
    /// <summary>
    /// Summary description for arUTAutoPayEditReport.
    /// </summary>
    partial class arUTAutoPayEditReport : BaseSectionReport
    {
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader pageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Detail detail;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter pageFooter;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
            rsData.Dispose();
            base.Dispose(disposing);
        }


        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(arUTAutoPayEditReport));
            this.pageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.pageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPageHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBook = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBill = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBill = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.reportHeader1 = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.reportFooter1 = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblBankruptDisclaimer = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBatchNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblProcessDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBankruptDisclaimer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBatchNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProcessDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // pageHeader
            // 
            this.pageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblDate,
            this.lblPageHeader,
            this.lblAccount,
            this.lblName,
            this.lblBook,
            this.lblDate1,
            this.lblAmount,
            this.lblBill,
            this.lblPage,
            this.lblMuniName,
            this.lblTime,
            this.lblBatchNumber,
            this.lblProcessDate});
            this.pageHeader.Height = 1.125F;
            this.pageHeader.Name = "pageHeader";
            // 
            // detail
            // 
            this.detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAccount,
            this.fldName,
            this.fldBook,
            this.fldBill,
            this.fldDate1,
            this.fldAmount});
            this.detail.Height = 0.21875F;
            this.detail.Name = "detail";
            // 
            // pageFooter
            // 
            this.pageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblBankruptDisclaimer,
            this.lblFooter});
            this.pageFooter.Height = 0.4479167F;
            this.pageFooter.Name = "pageFooter";
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 6.39F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1F;
            // 
            // lblPageHeader
            // 
            this.lblPageHeader.Height = 0.25F;
            this.lblPageHeader.HyperLink = null;
            this.lblPageHeader.Left = 0.03000021F;
            this.lblPageHeader.Name = "lblPageHeader";
            this.lblPageHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblPageHeader.Text = "Auto-Pay Edit List";
            this.lblPageHeader.Top = 0F;
            this.lblPageHeader.Width = 7.353F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.1875F;
            this.lblAccount.HyperLink = null;
            this.lblAccount.Left = 0F;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.lblAccount.Text = "Account";
            this.lblAccount.Top = 0.883F;
            this.lblAccount.Width = 1.133F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.1875F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 1.213F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left";
            this.lblName.Text = "Name";
            this.lblName.Top = 0.883F;
            this.lblName.Width = 2.271F;
            // 
            // lblBook
            // 
            this.lblBook.Height = 0.1875F;
            this.lblBook.HyperLink = null;
            this.lblBook.Left = 3.548F;
            this.lblBook.Name = "lblBook";
            this.lblBook.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.lblBook.Text = "Book";
            this.lblBook.Top = 0.883F;
            this.lblBook.Visible = false;
            this.lblBook.Width = 0.875F;
            // 
            // lblDate1
            // 
            this.lblDate1.Height = 0.1875F;
            this.lblDate1.HyperLink = null;
            this.lblDate1.Left = 5.5F;
            this.lblDate1.Name = "lblDate1";
            this.lblDate1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.lblDate1.Text = "DW Date";
            this.lblDate1.Top = 0.883F;
            this.lblDate1.Visible = false;
            this.lblDate1.Width = 0.8400002F;
            // 
            // lblAmount
            // 
            this.lblAmount.Height = 0.1875F;
            this.lblAmount.HyperLink = null;
            this.lblAmount.Left = 6.383F;
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.lblAmount.Text = "Amount";
            this.lblAmount.Top = 0.883F;
            this.lblAmount.Visible = false;
            this.lblAmount.Width = 1F;
            // 
            // lblBill
            // 
            this.lblBill.Height = 0.1875F;
            this.lblBill.HyperLink = null;
            this.lblBill.Left = 4.515012F;
            this.lblBill.Name = "lblBill";
            this.lblBill.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.lblBill.Text = "Bill";
            this.lblBill.Top = 0.883F;
            this.lblBill.Visible = false;
            this.lblBill.Width = 0.9269886F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 6.39F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; font-size: 10pt";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.3125F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; font-size: 10pt";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1F;
            // 
            // fldAccount
            // 
            this.fldAccount.Height = 0.19F;
            this.fldAccount.Left = 0.03F;
            this.fldAccount.Name = "fldAccount";
            this.fldAccount.Style = "font-family: Tahoma; font-size: 9.75pt; text-align: right; ddo-char-set: 0";
            this.fldAccount.Top = 0F;
            this.fldAccount.Width = 1.103333F;
            // 
            // fldName
            // 
            this.fldName.Height = 0.19F;
            this.fldName.Left = 1.209F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: Tahoma; font-size: 9.75pt; text-align: left; ddo-char-set: 0";
            this.fldName.Top = 0F;
            this.fldName.Width = 2.275F;
            // 
            // fldBook
            // 
            this.fldBook.Height = 0.19F;
            this.fldBook.Left = 3.548F;
            this.fldBook.Name = "fldBook";
            this.fldBook.Style = "font-family: Tahoma; font-size: 9.75pt; text-align: right; ddo-char-set: 0";
            this.fldBook.Top = 0F;
            this.fldBook.Width = 0.8750004F;
            // 
            // fldBill
            // 
            this.fldBill.Height = 0.19F;
            this.fldBill.Left = 4.515F;
            this.fldBill.Name = "fldBill";
            this.fldBill.Style = "font-family: Tahoma; font-size: 9.75pt; text-align: right; ddo-char-set: 0";
            this.fldBill.Top = 0F;
            this.fldBill.Width = 0.927F;
            // 
            // fldDate1
            // 
            this.fldDate1.Height = 0.19F;
            this.fldDate1.Left = 5.5F;
            this.fldDate1.Name = "fldDate1";
            this.fldDate1.Style = "font-family: Tahoma; font-size: 9.75pt; text-align: right; ddo-char-set: 0";
            this.fldDate1.Top = 0F;
            this.fldDate1.Width = 0.8399997F;
            // 
            // fldAmount
            // 
            this.fldAmount.Height = 0.19F;
            this.fldAmount.Left = 6.39F;
            this.fldAmount.Name = "fldAmount";
            this.fldAmount.Style = "font-family: Tahoma; font-size: 9.75pt; text-align: right; ddo-char-set: 0";
            this.fldAmount.Top = 0F;
            this.fldAmount.Width = 0.9930001F;
            // 
            // reportHeader1
            // 
            this.reportHeader1.Height = 0F;
            this.reportHeader1.Name = "reportHeader1";
            // 
            // reportFooter1
            // 
            this.reportFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblTotals,
            this.fldTotalTotal});
            this.reportFooter1.Name = "reportFooter1";
            // 
            // lblTotals
            // 
            this.lblTotals.Height = 0.1875F;
            this.lblTotals.HyperLink = null;
            this.lblTotals.Left = 3.548F;
            this.lblTotals.Name = "lblTotals";
            this.lblTotals.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right";
            this.lblTotals.Text = "Total:";
            this.lblTotals.Top = 0.031F;
            this.lblTotals.Width = 2.792125F;
            // 
            // fldTotalTotal
            // 
            this.fldTotalTotal.Height = 0.19F;
            this.fldTotalTotal.Left = 6.397F;
            this.fldTotalTotal.Name = "fldTotalTotal";
            this.fldTotalTotal.Style = "font-family: Tahoma; font-size: 9.75pt; text-align: right; ddo-char-set: 0";
            this.fldTotalTotal.Top = 0.031F;
            this.fldTotalTotal.Width = 0.993F;
            // 
            // lblBankruptDisclaimer
            // 
            this.lblBankruptDisclaimer.Height = 0.1875F;
            this.lblBankruptDisclaimer.HyperLink = null;
            this.lblBankruptDisclaimer.Left = 0F;
            this.lblBankruptDisclaimer.Name = "lblBankruptDisclaimer";
            this.lblBankruptDisclaimer.Style = "font-family: \'Tahoma\'; font-size: 9.75pt; font-weight: normal; text-align: center" +
    "; ddo-char-set: 0";
            this.lblBankruptDisclaimer.Text = "Account numbers in parentheses are flagged to not process";
            this.lblBankruptDisclaimer.Top = 0.26F;
            this.lblBankruptDisclaimer.Width = 7.39F;
            // 
            // lblFooter
            // 
            this.lblFooter.Height = 0.1875F;
            this.lblFooter.HyperLink = null;
            this.lblFooter.Left = 0.03F;
            this.lblFooter.Name = "lblFooter";
            this.lblFooter.Style = "font-family: \'Tahoma\'; font-size: 9.75pt; font-weight: bold; text-align: center; " +
    "ddo-char-set: 0";
            this.lblFooter.Text = "";
            this.lblFooter.Top = 0F;
            this.lblFooter.Width = 7.39F;
            // 
            // lblBatchNumber
            // 
            this.lblBatchNumber.Height = 0.1875F;
            this.lblBatchNumber.HyperLink = null;
            this.lblBatchNumber.Left = 2.312F;
            this.lblBatchNumber.Name = "lblBatchNumber";
            this.lblBatchNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.lblBatchNumber.Text = null;
            this.lblBatchNumber.Top = 0.28125F;
            this.lblBatchNumber.Width = 3.188F;
            // 
            // lblProcessDate
            // 
            this.lblProcessDate.Height = 0.1875F;
            this.lblProcessDate.HyperLink = null;
            this.lblProcessDate.Left = 2.312F;
            this.lblProcessDate.Name = "lblProcessDate";
            this.lblProcessDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.lblProcessDate.Text = null;
            this.lblProcessDate.Top = 0.469F;
            this.lblProcessDate.Width = 3.188001F;
            // 
            // arUTAutoPayEditReport
            // 
            this.MasterReport = false;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.46875F;
            this.Sections.Add(this.reportHeader1);
            this.Sections.Add(this.pageHeader);
            this.Sections.Add(this.detail);
            this.Sections.Add(this.pageFooter);
            this.Sections.Add(this.reportFooter1);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Arial; font-style: normal; text-decoration: none; font-weight: norma" +
            "l; font-size: 10pt; color: Black", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 16pt; font-weight: bold", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: Times New Roman; font-size: 14pt; font-weight: bold; font-style: ita" +
            "lic", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-size: 13pt; font-weight: bold", "Heading3", "Normal"));
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBankruptDisclaimer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBatchNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblProcessDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBook;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBill;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBill;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate1;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBankruptDisclaimer;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader reportHeader1;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter reportFooter1;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblBatchNumber;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblProcessDate;
    }
}
