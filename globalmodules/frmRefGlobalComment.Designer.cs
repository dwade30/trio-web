//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWPP0000
{
    /// <summary>
    /// Summary description for frmRefGlobalComment.
    /// </summary>
    partial class frmRefGlobalComment : BaseForm
    {
        public fecherFoundation.FCRichTextBox rtbText;
        public fecherFoundation.FCLabel lblTitle;
        private MainMenu MainMenu1;
        public fecherFoundation.FCToolStripMenuItem mnuFile;
        public fecherFoundation.FCToolStripMenuItem mnuPrint;
        public fecherFoundation.FCToolStripMenuItem mnuSepar3;
        public fecherFoundation.FCToolStripMenuItem mnuClear;
        public fecherFoundation.FCToolStripMenuItem mnuSepar2;
        public fecherFoundation.FCToolStripMenuItem mnuSave;
        public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
        public fecherFoundation.FCToolStripMenuItem mnuSepar;
        public fecherFoundation.FCToolStripMenuItem mnuExit;

	 protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rtbText = new fecherFoundation.FCRichTextBox();
            this.lblTitle = new fecherFoundation.FCLabel();
            this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSaveExit = new fecherFoundation.FCButton();
            this.cmdClear = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtbText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSaveExit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 630);
            this.BottomPanel.Size = new System.Drawing.Size(674, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.rtbText);
            this.ClientArea.Controls.Add(this.lblTitle);
            this.ClientArea.Size = new System.Drawing.Size(674, 570);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPrint);
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Size = new System.Drawing.Size(674, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPrint, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(119, 30);
            this.HeaderText.Text = "Comment";
            // 
            // rtbText
            // 
            this.rtbText.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.rtbText.Location = new System.Drawing.Point(30, 60);
            this.rtbText.Name = "rtbText";
            this.rtbText.Size = new System.Drawing.Size(606, 455);
            this.rtbText.TabIndex = 1;
            //FC:FINAL:AKV:#3681 - 	Short Maintenance - Focus missing from comment textbox
            //this.rtbText.TabStop = false;
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(30, 30);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(606, 16);
            this.lblTitle.TabIndex = 2;
            // 
            // MainMenu1
            // 
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = -1;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSepar3
            // 
            this.mnuSepar3.Index = -1;
            this.mnuSepar3.Name = "mnuSepar3";
            this.mnuSepar3.Text = "-";
            // 
            // mnuClear
            // 
            this.mnuClear.Index = -1;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear";
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // mnuSepar2
            // 
            this.mnuSepar2.Index = -1;
            this.mnuSepar2.Name = "mnuSepar2";
            this.mnuSepar2.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = -1;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSaveExit
            // 
            this.cmdSaveExit.AppearanceKey = "acceptButton";
            this.cmdSaveExit.Location = new System.Drawing.Point(280, 30);
            this.cmdSaveExit.Name = "cmdSaveExit";
            this.cmdSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSaveExit.Size = new System.Drawing.Size(112, 48);
            this.cmdSaveExit.Text = "Save & Exit";
            this.cmdSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.Location = new System.Drawing.Point(588, 29);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(48, 23);
            this.cmdClear.TabIndex = 2;
            this.cmdClear.Text = "Clear";
            this.cmdClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPrint.Location = new System.Drawing.Point(534, 29);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(48, 23);
            this.cmdPrint.TabIndex = 3;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // frmRefGlobalComment
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(674, 738);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmRefGlobalComment";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Comment";
            this.Load += new System.EventHandler(this.frmRefGlobalComment_Load);
            this.Resize += new System.EventHandler(this.frmRefGlobalComment_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRefGlobalComment_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtbText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private System.ComponentModel.IContainer components;
		private FCButton cmdSaveExit;
		private FCButton cmdPrint;
		private FCButton cmdClear;
        //FC:FINAL:MSH - i.issue #1351: remove second 'Save' button
		//private FCButton cmdSave;
	}
}