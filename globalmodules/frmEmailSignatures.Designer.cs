﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmEmailSignatures.
	/// </summary>
	partial class frmEmailSignatures : BaseForm
	{
		public fecherFoundation.FCRichTextBox rtfSignature;
		private fecherFoundation.FCToolBarButton btnSaveSignature;
		private fecherFoundation.FCToolBarButton btnEditPersonal;
		private fecherFoundation.FCToolBarButton btnEditCompany;
		private fecherFoundation.FCToolBarButton btnEditDisclaimer;
		public Wisej.Web.ImageList ImageList1;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmailSignatures));
			this.rtfSignature = new fecherFoundation.FCRichTextBox();
			this.btnSaveSignature = new fecherFoundation.FCToolBarButton();
			this.btnEditPersonal = new fecherFoundation.FCToolBarButton();
			this.btnEditCompany = new fecherFoundation.FCToolBarButton();
			this.btnEditDisclaimer = new fecherFoundation.FCToolBarButton();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcess = new fecherFoundation.FCButton();
			this.cmdEditPersonal = new fecherFoundation.FCButton();
			this.cmdEditCompany = new fecherFoundation.FCButton();
			this.cmdEditDisclaimer = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.rtfSignature)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditPersonal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditCompany)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditDisclaimer)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 292);
			this.BottomPanel.Size = new System.Drawing.Size(781, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.rtfSignature);
			this.ClientArea.Size = new System.Drawing.Size(781, 232);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdEditDisclaimer);
			this.TopPanel.Controls.Add(this.cmdEditCompany);
			this.TopPanel.Controls.Add(this.cmdEditPersonal);
			this.TopPanel.Size = new System.Drawing.Size(781, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdEditPersonal, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdEditCompany, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdEditDisclaimer, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(26, 26);
			this.HeaderText.Size = new System.Drawing.Size(130, 30);
			this.HeaderText.Text = "Signatures";
			// 
			// rtfSignature
			// 
			this.rtfSignature.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.rtfSignature.AutoSize = false;
			this.rtfSignature.Location = new System.Drawing.Point(25, 30);
			this.rtfSignature.Multiline = true;
			this.rtfSignature.Name = "rtfSignature";
			this.rtfSignature.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
			this.rtfSignature.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
			this.rtfSignature.Size = new System.Drawing.Size(733, 177);
			this.rtfSignature.TabIndex = 1;
			//FC:FINAL:MSH - i.issue #1608: assign missing handler
			this.rtfSignature.KeyDown += new KeyEventHandler(this.rtfSignature_KeyDown);
			// 
			// btnSaveSignature
			// 
			this.btnSaveSignature.ImageIndex = 0;
			this.btnSaveSignature.Key = "btnSaveSignature";
			this.btnSaveSignature.Name = "btnSaveSignature";
			this.btnSaveSignature.ToolTipText = "Save";
			this.btnSaveSignature.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnEditPersonal
			// 
			this.btnEditPersonal.ImageIndex = 1;
			this.btnEditPersonal.Key = "btnEditPersonal";
			this.btnEditPersonal.Name = "btnEditPersonal";
			this.btnEditPersonal.ToolTipText = "Edit Personal Signature";
			this.btnEditPersonal.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnEditCompany
			// 
			this.btnEditCompany.ImageIndex = 2;
			this.btnEditCompany.Key = "btnEditCompany";
			this.btnEditCompany.Name = "btnEditCompany";
			this.btnEditCompany.ToolTipText = "Edit Company Signature";
			this.btnEditCompany.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnEditDisclaimer
			// 
			this.btnEditDisclaimer.ImageIndex = 3;
			this.btnEditDisclaimer.Key = "btnEditDisclaimer";
			this.btnEditDisclaimer.Name = "btnEditDisclaimer";
			this.btnEditDisclaimer.ToolTipText = "Edit Disclaimer Message";
			this.btnEditDisclaimer.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// ImageList1
			// 
			this.ImageList1.ImageSize = new System.Drawing.Size(24, 24);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(343, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(94, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Save";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// cmdEditPersonal
			// 
			this.cmdEditPersonal.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdEditPersonal.AppearanceKey = "toolbarButton";
			this.cmdEditPersonal.Location = new System.Drawing.Point(307, 27);
			this.cmdEditPersonal.Name = "cmdEditPersonal";
			this.cmdEditPersonal.Size = new System.Drawing.Size(158, 24);
			this.cmdEditPersonal.TabIndex = 52;
			this.cmdEditPersonal.Text = "Edit Personal Signature";
			this.cmdEditPersonal.Click += new System.EventHandler(this.cmdEditPersonal_Click);
			// 
			// cmdEditCompany
			// 
			this.cmdEditCompany.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdEditCompany.AppearanceKey = "toolbarButton";
			this.cmdEditCompany.Location = new System.Drawing.Point(469, 27);
			this.cmdEditCompany.Name = "cmdEditCompany";
			this.cmdEditCompany.Size = new System.Drawing.Size(161, 24);
			this.cmdEditCompany.TabIndex = 53;
			this.cmdEditCompany.Text = "Edit Company Signature";
			this.cmdEditCompany.Click += new System.EventHandler(this.cmdEditCompany_Click);
			// 
			// cmdEditDisclaimer
			// 
			this.cmdEditDisclaimer.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdEditDisclaimer.AppearanceKey = "toolbarButton";
			this.cmdEditDisclaimer.Location = new System.Drawing.Point(633, 27);
			this.cmdEditDisclaimer.Name = "cmdEditDisclaimer";
			this.cmdEditDisclaimer.Size = new System.Drawing.Size(105, 24);
			this.cmdEditDisclaimer.TabIndex = 54;
			this.cmdEditDisclaimer.Text = "Edit Disclaimer";
			this.cmdEditDisclaimer.Click += new System.EventHandler(this.cmdEditDisclaimer_Click);
			// 
			// frmEmailSignatures
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(781, 400);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmEmailSignatures";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Signatures";
			this.Load += new System.EventHandler(this.frmEmailSignatures_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEmailSignatures_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.rtfSignature)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditPersonal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditCompany)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdEditDisclaimer)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
		public FCButton cmdEditPersonal;
		public FCButton cmdEditCompany;
		public FCButton cmdEditDisclaimer;
	}
}
