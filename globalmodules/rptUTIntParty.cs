﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TWCL0000;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptUTIntParty : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Corey Grey              *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :               Melissa Lamprecht       *
		// LAST UPDATED   :               09/05/2007              *
		// ********************************************************
		// MAL@20070905: Changed code (borrowed from RE report) to conform to utility requirements
		clsDRWrapper clsre = new clsDRWrapper();
		int lngLastAccount;
		double dblTotalSum;

		public rptUTIntParty()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Interested Party List";
		}

		public static rptUTIntParty InstancePtr
		{
			get
			{
				return (rptUTIntParty)Sys.GetInstance(typeof(rptUTIntParty));
			}
		}

		protected rptUTIntParty _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsre?.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsre.EndOfFile();
			//Detail_Format();
		}

		public void Init(bool boolNameOrder)
		{
			string strSQL = "";
			string strOrder = "";
			string strCurrentModule;
			string strDbCL;
			strDbCL = clsre.Get_GetFullDBName(modExtraModules.strCLDatabase) + ".dbo.";
			// If boolNameOrder Then
			// strOrder = " ORDER BY Master.OwnerName, Master.AccountNumber "
			// Else
			// strOrder = " ORDER BY Master.AccountNumber "
			// End If
			strCurrentModule = Strings.Mid(modGlobal.DEFAULTDATABASE, 3, 2);
			lblTotalDue.Text = "Total Due as of " + Strings.Format(DateTime.Today, "MM/dd/yy");
			clsre.OpenRecordset("SELECT * FROM Master INNER JOIN " + clsre.Get_GetFullDBName(modExtraModules.strCLDatabase) + ".dbo.Owners ON Master.AccountNumber = Owners.Account WHERE Deleted = 0 AND Owners.ModuleCode = '" + strCurrentModule + "'", modExtraModules.strUTDatabase);
			if (clsre.EndOfFile())
			{
				FCMessageBox.Show("No records found.", MsgBoxStyle.Information, "No Match");
				Cancel();
				return;
			}
			clsre.InsertName("OwnerPartyID", "Own1", false, true, false, "", false, "", true, "");
			if (boolNameOrder)
			{
				clsre.ReOrder("Own1FullName, Master.AccountNumber");
			}
			else
			{
				clsre.ReOrder("Master.AccountNumber");
			}
			// Me.Show , MDIParent
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			lngLastAccount = 0;
			txtDate.Text = DateTime.Today.ToString();
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				double dblDue = 0;
				bool boolDup = false;
				if (!clsre.EndOfFile())
				{
					// TODO: Field [Key] not found!! (maybe it is an alias?)
					if (Conversion.Val(clsre.Get_Fields("Key")) != lngLastAccount)
					{
						//FC:FINAL:MSH - can't implicitly convert from int to string
						// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						txtaccount.Text = FCConvert.ToString(clsre.Get_Fields("AccountNumber"));
						// TODO: Field [OwnerName] not found!! (maybe it is an alias?)
						txtName.Text = clsre.Get_Fields("OwnerName");
						// TODO: Field [Key] not found!! (maybe it is an alias?)
						lngLastAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsre.Get_Fields("Key"))));
						boolDup = false;
					}
					else
					{
						txtaccount.Text = "";
						txtName.Text = "";
						boolDup = true;
					}
					if (FCConvert.ToBoolean(clsre.Get_Fields_Boolean("ReceiveCopies")))
					{
						txtCopies.Text = "Yes";
					}
					else
					{
						txtCopies.Text = "No";
					}
					// TODO: Field [Owners.AutoID] not found!! (maybe it is an alias?)
					txtIPNumber.Text = clsre.Get_Fields("Owners.AutoID");
					// TODO: Field [Owners.Name] not found!! (maybe it is an alias?)
					txtIPName.Text = clsre.Get_Fields("Owners.Name");
					if (!boolDup)
					{
						// Water Total
						dblDue = modUTCalculations.CalculateAccountUTTotal(lngLastAccount, true);
						// Sewer Total
						dblDue += modUTCalculations.CalculateAccountUTTotal(lngLastAccount, false);
						fldTotalDue.Text = Strings.Format(dblDue, "$#,##0.00");
					}
					else
					{
						fldTotalDue.Text = "";
					}
					dblTotalSum += dblDue;
					clsre.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Detail Format");
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotalTotalDue.Text = Strings.Format(dblTotalSum, "$#,##0.00");
		}

	}
}
