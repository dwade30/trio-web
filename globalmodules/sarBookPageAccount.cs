﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for sarBookPageAccount.
	/// </summary>
	public partial class sarBookPageAccount : FCSectionReport
	{
		// nObj = 1
		//   0	arBookPageAccount	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/27/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/07/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		public int lngYear1;
		public int lngYear2;
		public int lngAcct1;
		public int lngAcct2;

		public sarBookPageAccount()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static sarBookPageAccount InstancePtr
		{
			get
			{
				return (sarBookPageAccount)Sys.GetInstance(typeof(sarBookPageAccount));
			}
		}

		protected sarBookPageAccount _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// VBto upgrade warning: lngFirstAccount As int	OnWrite(int, double)
		// VBto upgrade warning: lngLastAccount As int	OnWrite(int, double)
		public void Init(ref int lngFirstAccount, ref int lngLastAccount, int lngFirstYear = 0, int lngLastYear = 0, int lngEligible = 0)
		{
			string strSQL;
			string strEligible = "";
			if (lngEligible != 0)
			{
				strEligible = " AND (LienProcessStatus = 3 OR LienProcessStatus = 2) AND (Taxdue1 + Taxdue2 + Taxdue3 + Taxdue4) > PrincipalPaid";
			}
			strSQL = "SELECT DISTINCT RSAccount, OwnerPartyID FROM " + rsData.CurrentPrefix + "RealEstate.dbo.Master INNER JOIN BillingMaster ON Master.RSAccount = BillingMaster.Account WHERE RSCard = 1 AND RSAccount >= " + FCConvert.ToString(lngFirstAccount) + " AND RSAccount <= " + FCConvert.ToString(lngLastAccount) + strEligible + " ORDER BY RSAccount";
			// strSQL = "SELECT RSAccount, RSName FROM Master WHERE RSCard = 1 AND RSAccount >= " & lngFirstAccount & " AND RSAccount <= " & lngLastAccount & " ORDER BY RSAccount"
			rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
			if (!rsData.EndOfFile())
			{
				rsData.InsertName("OwnerPartyID", "Own1", false, true, false, "RE", false, "", true, "");
				this.Name = "Book Page Report";
				lngYear1 = lngFirstYear;
				lngYear2 = lngLastYear;
				lngAcct1 = lngFirstAccount;
				lngAcct2 = lngLastAccount;
				SetReportHeader();
				frmReportViewer.InstancePtr.Init(this);
			}
			else
			{
				MessageBox.Show("No matching accounts.", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
				return;
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	if (KeyCode == vbKeyEscape)
		//	{
		//		Close();
		//	}
		//}
		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
			rsData.DisposeOf();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Book and Page", true, rsData.RecordCount(), true);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngAcct = 0;
				if (!rsData.EndOfFile())
				{
					frmWait.InstancePtr.IncrementProgress();
					lngAcct = FCConvert.ToInt32(rsData.Get_Fields_Int32("RSAccount"));
					lblAccount.Text = FCConvert.ToString(lngAcct);
					// TODO: Field [Own1FullName] not found!! (maybe it is an alias?)
					lblName.Text = FCConvert.ToString(rsData.Get_Fields("Own1FullName"));
					// This will set all of the sub reports
					sarBookPageREob.Report = new sarBookPageRE();
					sarBookPageREob.Report.UserData = lngAcct;
					sarBookPageMHob.Report = new sarBookPageMH();
					sarBookPageMHob.Report.UserData = lngAcct;
					if (modGlobalConstants.Statics.gboolCL || modGlobalConstants.Statics.gboolBL)
					{
						sarBookPageLNob.Report = new sarBookPageLN();
						sarBookPageLNob.Report.UserData = lngAcct;
						sarBookPageLDNob.Report = new sarBookPageLDN();
						sarBookPageLDNob.Report.UserData = lngAcct;
					}
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MsgBoxStyle.Critical, "Error In Book Page Report - Account");
			}
		}

		private void SetReportHeader()
		{
			string strYear = "";
			string strAcct = "";
			// set the report type caption
			if (lngAcct1 == lngAcct2)
			{
				strAcct = "Account : " + FCConvert.ToString(lngAcct1);
			}
			else
			{
				strAcct = "Account : " + FCConvert.ToString(lngAcct1) + " to " + FCConvert.ToString(lngAcct2);
			}
			if (lngYear1 != 0)
			{
				if (lngYear1 == lngYear2)
				{
					strYear = "Year : " + FormatBillingYear_2(lngYear1.ToString());
				}
				else
				{
					strYear = "Year : " + FormatBillingYear_2(lngYear1.ToString()) + " to " + FormatBillingYear_2(lngYear2.ToString());
				}
			}
			else
			{
				strYear = "";
			}
			lblReportType.Text = Strings.Trim(strAcct + "  " + strYear);
		}

		private string FormatBillingYear_2(string strYear)
		{
			return FormatBillingYear(ref strYear);
		}

		private string FormatBillingYear(ref string strYear)
		{
			string FormatBillingYear = "";
			if (Strings.InStr(1, strYear, "-") > 0)
			{
				if (Strings.InStr(1, strYear, "*") > 0)
				{
					FormatBillingYear = Strings.Left(strYear, 4) + FCConvert.ToString(Conversion.Val(Strings.Right(strYear, 2)));
				}
				else
				{
					FormatBillingYear = Strings.Left(strYear, 4) + Strings.Right(strYear, 1);
				}
			}
			else
			{
				if (Strings.InStr(1, strYear, "*") > 0)
				{
					FormatBillingYear = Strings.Left(strYear, 4) + "-" + FCConvert.ToString(Conversion.Val(Strings.Right(strYear, 2)));
				}
				else
				{
					FormatBillingYear = Strings.Left(strYear, 4) + "-" + Strings.Right(strYear, 1);
				}
			}
			return FormatBillingYear;
		}

		private void arBookPageAccount_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arBookPageAccount.Caption	= "Account Detail";
			//arBookPageAccount.Icon	= "sarBookPageAccount.dsx":0000";
			//arBookPageAccount.Left	= 0;
			//arBookPageAccount.Top	= 0;
			//arBookPageAccount.Width	= 11880;
			//arBookPageAccount.Height	= 8595;
			//arBookPageAccount.StartUpPosition	= 3;
			//arBookPageAccount.SectionData	= "sarBookPageAccount.dsx":058A;
			//End Unmaped Properties
		}
	}
}
