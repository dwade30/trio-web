﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.DataBaseLayer;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;

#if TWAR0000
using TWAR0000;


#elif TWBL0000
using TWBL0000;
#endif
using System.IO;

namespace Global
{
	/// <summary>
	/// Summary description for frmCustomBill.
	/// </summary>
	public partial class frmCustomBill : BaseForm
	{
		public frmCustomBill()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.CustomLabel = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.CustomLabel.AddControlArrayElement(CustomLabel_0, FCConvert.ToInt16(0));
			//FC:FINAL:BBE:#320 - set Capitalize, need to show the same case
			this.CustomLabel_0.Capitalize = false;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomBill InstancePtr
		{
			get
			{
				return (frmCustomBill)Sys.GetInstance(typeof(frmCustomBill));
			}
		}

		protected frmCustomBill _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 08/19/2004
		// ********************************************************
		// vbPorter upgrade warning: intCharWid As short --> As int	OnWrite(short, double)
		int intCharWid;
		// vbPorter upgrade warning: intLineHite As short --> As int	OnWrite(short, double)
		int intLineHite;
		int lngCustomBillID;
		// the id number of the bill.  Used to identify the unique bill
		string strThisModule = "";
		// vbPorter upgrade warning: dblSizeRatio As double	OnWriteFCConvert.ToSingle(
		double dblSizeRatio;
		// ratio of original size to what form has been resized to
		// vbPorter upgrade warning: lngPageWidth As int	OnWrite(double, int)
		int lngPageWidth;
		// The pagewidth in twips (actual, not what is seen)
		// vbPorter upgrade warning: lngPageHeight As int	OnWrite(int, double)
		int lngPageHeight;
		// The pageheight in twips (actual, not what is seen)
		double dblPageRatio;
		// The ratio of actual twips to what the shown page is sized (dblPageRatio * shownsize = actual size)
		// vbPorter upgrade warning: lngScreenPageWidth As int	OnWriteFCConvert.ToDouble(
		int lngScreenPageWidth;
		// The actual twips of the shown page (not including dblSizeRatio)
		// vbPorter upgrade warning: lngScreenPageHeight As int	OnWriteFCConvert.ToDouble(
		int lngScreenPageHeight;
		// The actual twips of the shown page (not including dblsizeratio)
		// vbPorter upgrade warning: lngOffsetY As int	OnWrite(short, double)
		int lngOffsetY;
		// The offset from the frames 0 to where the picture1.top is visibly 0
		// vbPorter upgrade warning: lngOffsetX As int	OnWrite(short, double)
		int lngOffsetX;
		// The offset from the frames 0 to where the picture1.left is visibly 0
		double dblTwipsPerUnit;
		// 1440 per inch ,567 per centimeter, 56.7 per millimeter
		// vbPorter upgrade warning: lngClickXOffset As int	OnWrite(int, float)
		int lngClickXOffset;
		// twips from left the mouse click is
		// vbPorter upgrade warning: lngClickYOffset As int	OnWrite(int, float)
		int lngClickYOffset;
		// twips from top the mouse click is
		bool boolDragging;
		bool boolInHighlightMode;
		bool boolInLassoMode;
		// vbPorter upgrade warning: lngLassoY As int	OnWriteFCConvert.ToSingle(
		int lngLassoY;
		// vbPorter upgrade warning: lngLassoX As int	OnWriteFCConvert.ToSingle(
		int lngLassoX;
		int lngGroupBoxLeft;
		int lngGroupBoxRight;
		int lngGroupBoxTop;
		int lngGroupBoxBottom;
		// constants to make changing things simple.  Don't need to change code. Can just change these constants
		const int CNSTGRIDFIELDSCOLAUTOID = 0;
		const int CNSTGRIDFIELDSCOLFIELDID = 1;
		const int CNSTGRIDFIELDSCOLFIELDNUM = 2;
		const int CNSTGRIDFIELDSCOLDESCRIPTION = 3;
		const int CNSTGRIDFIELDSCOLTOP = 4;
		const int CNSTGRIDFIELDSCOLLEFT = 5;
		const int CNSTGRIDFIELDSCOLHEIGHT = 6;
		const int CNSTGRIDFIELDSCOLWIDTH = 7;
		const int CNSTGRIDFIELDSCOLALIGNMENT = 8;
		const int CNSTGRIDFIELDSCOLCONTROL = 10;
		const int CNSTGRIDFIELDSCOLTEXT = 11;
		const int CNSTGRIDFIELDSCOLFONT = 9;
		const int CNSTGRIDFIELDSCOLFONTSIZE = 12;
		const int CNSTGRIDFIELDSCOLFONTSTYLE = 13;
		const int CNSTGRIDFIELDSCOLEXTRAPARAMETERS = 14;
		const int CNSTGRIDFIELDSCOLMISC = 15;
		const int CNSTGRIDBILLSIZECOLDATA = 1;
		const int CNSTGRIDBILLSIZECOLDESCRIPTION = 0;
		const int CNSTGRIDBILLSIZEROWNAME = 0;
		const int CNSTGRIDBILLSIZEROWUNITS = 1;
		const int CNSTGRIDBILLSIZEROWPAGEHEIGHT = 2;
		const int CNSTGRIDBILLSIZEROWPAGEWIDTH = 3;
		const int CNSTGRIDBILLSIZEROWTOPMARGIN = 4;
		const int CNSTGRIDBILLSIZEROWBOTTOMMARGIN = 5;
		const int CNSTGRIDBILLSIZEROWLEFTMARGIN = 6;
		const int CNSTGRIDBILLSIZEROWRIGHTMARGIN = 7;
		const int CNSTGRIDBILLSIZEROWSTYLE = 8;
		const int CNSTGRIDBILLSIZEROWDESCRIPTION = 9;
		const int CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE = 10;
		const int CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE = 11;
		const int CNSTCUSTOMBILLALIGNLEFT = 0;
		const int CNSTCUSTOMBILLALIGNCENTER = 1;
		const int CNSTCUSTOMBILLALIGNRIGHT = 2;
		const int CNSTCUSTOMBILLUNITSINCHES = 0;
		const int CNSTCUSTOMBILLUNITSCENTIMETERS = 1;
		const int CNSTCUSTOMBILLFONTSTYLEREGULAR = 0;
		const int CNSTCUSTOMBILLFONTSTYLEBOLD = 1;
		const int CNSTCUSTOMBILLFONTSTYLEITALIC = 2;
		const int CNSTCUSTOMBILLFONTSTYLEBOLDITALIC = 3;
		const int CNSTCUSTOMBILLMAXCONTROLS = 200;
		// not used currently.  Use if we need to not show all of the fields
		// that are created since only 255 controls can be on a form
		const int CNSTGRIDCONTROLINFOCOLDATA = 1;
		const int CNSTGRIDCONTROLINFOCOLDESCRIPTION = 0;
		const int CNSTGRIDCONTROLINFOCOLTOOLTIP = 2;
		const int CNSTGRIDCONTROLINFOROWCODE = 0;
		const int CNSTGRIDCONTROLINFOROWTYPE = 1;
		const int CNSTGRIDCONTROLINFOROWTEXT = 2;
		const int CNSTGRIDCONTROLINFOROWALIGN = 3;
		const int CNSTGRIDCONTROLINFOROWFONT = 4;
		const int CNSTGRIDCONTROLINFOROWFONTSIZE = 5;
		const int CNSTGRIDCONTROLINFOROWFONTSTYLE = 6;
		const int CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS = 7;
		// these rows must always be the last rows
		// since there are a variable number of them and we need to know if we are on those types of rows or not
		// the string to describe these is delimited by semi-colons.  Each parameters info is separated by commas
		// and if the parameter needs a dropdown, then that info is separated by |'s
		// Ex: 3 parameters Description 1,Label and Type.  Description and label can be anything but type needs
		// to be chosen from a drop down.  The paramaters string would look like this:
		// Description 1;Label;Type,Choice1|Choice2|Choice3      The first two have no other data associated since they
		// take whatever the user types in.  The Type includes a string to be made into a drop down list
		// The first item is used as the text to show to the user.  When saved, all info from the user will be delimited
		// by semi-colons.  This means that the only invalid character to be entered by the user is a semi-colon
		const int CNSTGRIDCONTROLTYPESCOLOUTLINE = 0;
		const int CNSTGRIDCONTROLTYPESCOLCODE = 1;
		const int CNSTGRIDCONTROLTYPESCOLCATEGORY = 2;
		const int CNSTGRIDCONTROLTYPESCOLNAME = 3;
		const int CNSTGRIDCONTROLTYPESCOLDESCRIPTION = 4;
		const int CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN = 5;
		const int CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH = 6;
		const int CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT = 7;
		const int CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS = 8;
		const int CNSTGRIDCONTROLTYPESCOLTOOLTIP = 9;
		const int CNSTGRIDCONTROLTYPESCOLPARAMETERSTOOLTIP = 10;
		const int CNSTGRIDLOADDELETECOLAUTOID = 0;
		const int CNSTGRIDLOADDELETECOLCHECK = 1;
		const int CNSTGRIDLOADDELETECOLNAME = 2;
		const int CNSTGRIDLOADDELETECOLDESCRIPTION = 3;
		const int CNSTGRIDLOADDELETELOAD = 0;
		const int CNSTGRIDLOADDELETEDELETE = 1;
		const int CNSTGRIDLOADDELETEEXPORT = 2;

		private struct CustomBillInfo
		{
			public int lngBillID;
			// ID
			public int lngBillFormat;
			// Type Number
			public string strFormatName;
			// Formats Name
			public int intUnits;
			// 0 is inches,1 is centimeters
			public double dblPageHeight;
			public double dblPageWidth;
			public double dblTopMargin;
			public double dblBottomMargin;
			public double dblLeftMargin;
			public double dblRightMargin;
			public bool boolWide;
			// currently not used
			public int lngBillType;
			// currently not used.  Module defined
			public bool boolIsLaser;
			public bool boolIsDeletable;
			// false if it is shipped with TRIO
			public string strDescription;
			// text describing the bill format
			public string strBackgroundImage;
			// background image to display to help line up fields
			public short intDefaultFontSize;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public CustomBillInfo(int unusedParam)
			{
				this.lngBillID = 0;
				this.lngBillFormat = 0;
				this.strFormatName = string.Empty;
				this.intUnits = 0;
				this.dblPageHeight = 0;
				this.dblPageWidth = 0;
				this.dblTopMargin = 0;
				this.dblBottomMargin = 0;
				this.dblLeftMargin = 0;
				this.dblRightMargin = 0;
				this.boolWide = false;
				this.lngBillType = 0;
				this.boolIsLaser = false;
				this.boolIsDeletable = false;
				this.strDescription = string.Empty;
				this.strBackgroundImage = string.Empty;
				this.intDefaultFontSize = 0;
			}
		};

		CustomBillInfo CurrentLoadedFormat = new CustomBillInfo(0);

		private void BillImage_DragDrop(object sender, Wisej.Web.DragEventArgs e)
		{
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp = 0;
			string strTemp = "";
			// vbPorter upgrade warning: lngRow As int	OnWrite(string, int)
			int lngRow = 0;
			double dblRatio = 0;
			int intloop;
			int intLoop2;
			var source = e.Data.GetData("fecherFoundation.FCLabel") as FCLabel;
			//FC:FINAL:SBE - #318 - changed DragAndDrop with Movable property. We don't have to calculate control position
			//Point location = this.BillImage.PointToClient(new Point(e.X, e.Y));
			//boolDragging = false;
			if (source.GetIndex() == 0)
			{
				//    // group control
				//    if (location.Y - lngClickYOffset < 0)
				//    {
				//        source.Top = 0;
				//    }
				//    else
				//    {
				//        if ((location.Y - lngClickYOffset) + source.Height > Picture1.Height)
				//        {
				//            source.Top = Picture1.Height - source.Height;
				//        }
				//        else
				//        {
				//            source.Top = FCConvert.ToInt32(location.Y - lngClickYOffset);
				//        }
				//    }
				//    if (location.X - lngClickXOffset < 0)
				//    {
				//        source.Left = 0;
				//    }
				//    else
				//    {
				//        if ((location.X - lngClickXOffset) + source.Width > Picture1.Width)
				//        {
				//            source.Left = Picture1.Width - source.Width;
				//        }
				//        else
				//        {
				//            source.Left = FCConvert.ToInt32(location.X - lngClickXOffset);
				//        }
				//    }
				// now move all controls the same amount
				int lngLeftMove = 0;
				int lngTopMove = 0;
				lngTopMove = source.Top - ctlGroupControl.Top;
				lngLeftMove = source.Left - ctlGroupControl.Left;
				for (intloop = 0; intloop <= GridHighlighted.Rows - 1; intloop++)
				{
					CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].Left = CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].Left + lngLeftMove;
					CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].Top = CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].Top + lngTopMove;
					lngRow = FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0));
					for (intLoop2 = 1; intLoop2 <= GridFields.Rows - 1; intLoop2++)
					{
						if (Conversion.Val(GridFields.TextMatrix(intLoop2, CNSTGRIDFIELDSCOLFIELDNUM)) == lngRow)
						{
							lngRow = intLoop2;
							break;
						}
					}
					// intLoop2
					dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
					dblTemp = FCConvert.ToDouble(Strings.Format(CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].Left * dblRatio, "0.0000"));
					GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
					dblTemp = FCConvert.ToDouble(Strings.Format(CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].Top * dblRatio, "0.0000"));
					GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
					GridFields.RowData(lngRow, true);
					// mark as changed
				}
				// intloop
				lngClickXOffset = 0;
				lngClickYOffset = 0;
				CustomLabel[0].Visible = false;
				MakeGroupBoxFromList();
				return;
			}
			else
			{
				// Source.Left = Source.Left - (X - lngClickXOffset)
				// Source.Top = Source.Top - (Y - lngClickYOffset)
				//FC:FINAL:SBE - #318 - changed DragAndDrop with Movable property. We don't have to calculate control position
				//if (e.Y - lngClickYOffset < 0)
				//{
				//    source.Top = 0;
				//}
				//else
				//{
				//    if ((e.Y - lngClickYOffset) + source.Height > Picture1.Height)
				//    {
				//        source.Top = Picture1.Height - source.Height;
				//    }
				//    else
				//    {
				//        source.Top = FCConvert.ToInt32(e.Y - lngClickYOffset);
				//    }
				//}
				//if (e.X - lngClickXOffset < 0)
				//{
				//    source.Left = 0;
				//}
				//else
				//{
				//    if ((e.X - lngClickXOffset) + source.Width > Picture1.Width)
				//    {
				//        source.Left = Picture1.Width - source.Width;
				//    }
				//    else
				//    {
				//        source.Left = FCConvert.ToInt32(e.X - lngClickXOffset);
				//    }
				//}
				lngClickXOffset = 0;
				lngClickYOffset = 0;
				// Source.Left = X
				// Source.Top = Y
				// strTemp = Source.Name
				// strTemp = Mid(strTemp, 12) 'chop off the CustomLabel part before the number
				// lngRow = Val(strTemp)
				lngRow = FCConvert.ToInt32(Math.Round(Conversion.Val(source.Tag)));
				for (intloop = 1; intloop <= GridFields.Rows - 1; intloop++)
				{
					if (Conversion.Val(GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM)) == lngRow)
					{
						lngRow = intloop;
						break;
					}
				}
				// intloop
				dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
				dblTemp = FCConvert.ToDouble(Strings.Format(source.LeftOriginal * dblRatio, "0.0000"));
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
				dblTemp = FCConvert.ToDouble(Strings.Format(source.TopOriginal * dblRatio, "0.0000"));
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
				GridFields.RowData(lngRow, true);
				// mark as changed
				GridFields.Row = lngRow;
			}
		}

		private void BillImage_DragOver(object sender, Wisej.Web.DragEventArgs e)
		{
			// If this covers the picture box, the picture box cant get this event
			// If Not boolDragging Then
			// boolDragging = True
			// lngClickXOffset = X
			// lngClickYOffset = Y
			// End If
		}

		private void BillImage_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			if (Button == MouseButtonConstants.LeftButton)
			{
				// if they click in empty space, unhighlight all
				UnhighlightAllControls();
				// start lasso mode
				boolInLassoMode = true;
				lngLassoX = FCConvert.ToInt32(X);
				lngLassoY = FCConvert.ToInt32(Y);
				ctlGroupControl.Left = FCConvert.ToInt32(X);
				ctlGroupControl.Top = FCConvert.ToInt32(Y);
				ctlGroupControl.Width = 0;
				ctlGroupControl.Height = 0;
				ctlGroupControl.Visible = true;
			}
			else
			{
				// only show the copy and paste stuff
				mnuAlignBottoms.Visible = false;
				mnuAlignLefts.Visible = false;
				mnuAlignRights.Visible = false;
				mnuAlignTops.Visible = false;
				mnuBringToFront.Visible = false;
				mnuSendToBack.Visible = false;
				mnuPopSepar1.Visible = false;
				mnuPopSepar2.Visible = false;
				this.PopupMenu(mnuPop);
			}
		}

		private void BillImage_MouseMove(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			int lngX = 0;
			int lngY = 0;
			if (boolInLassoMode)
			{
				lngX = lngLassoX;
				lngY = lngLassoY;
				if (lngX < X)
				{
					ctlGroupControl.Width = FCConvert.ToInt32(X - lngX);
					ctlGroupControl.Left = lngX;
				}
				else
				{
					ctlGroupControl.Width = FCConvert.ToInt32(lngX - X);
					ctlGroupControl.Left = FCConvert.ToInt32(X);
				}
				if (lngY < Y)
				{
					ctlGroupControl.Height = FCConvert.ToInt32(Y - lngY);
					ctlGroupControl.Top = lngY;
				}
				else
				{
					ctlGroupControl.Height = FCConvert.ToInt32(lngY - Y);
					ctlGroupControl.Top = FCConvert.ToInt32(Y);
				}
			}
		}

		private void BillImage_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			int intloop;
			// vbPorter upgrade warning: lngX1 As int	OnWrite(float, int)
			int lngX1 = 0;
			// vbPorter upgrade warning: lngX2 As int	OnWrite(int, float)
			int lngX2 = 0;
			// vbPorter upgrade warning: lngY1 As int	OnWrite(float, int)
			int lngY1 = 0;
			// vbPorter upgrade warning: lngY2 As int	OnWrite(int, float)
			int lngY2 = 0;
			ctlGroupControl.Visible = false;
			if (boolInLassoMode)
			{
				if (X < lngLassoX)
				{
					lngX1 = FCConvert.ToInt32(X);
					lngX2 = lngLassoX;
				}
				else
				{
					lngX1 = lngLassoX;
					lngX2 = FCConvert.ToInt32(X);
				}
				if (Y < lngLassoY)
				{
					lngY1 = FCConvert.ToInt32(Y);
					lngY2 = lngLassoY;
				}
				else
				{
					lngY1 = lngLassoY;
					lngY2 = FCConvert.ToInt32(Y);
				}
				boolInLassoMode = false;
				ctlGroupControl.Visible = false;
				// now select all that were in the lasso
				for (intloop = 1; intloop <= GridFields.Rows - 1; intloop++)
				{
					if (FieldCollisionTest(FCConvert.ToInt32(GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM)), ref lngX1, ref lngY1, ref lngX2, ref lngY2))
					{
						boolInHighlightMode = true;
						HighlightControl(FCConvert.ToInt32(GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM)));
						GridHighlighted.Rows += 1;
						GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0, GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM));
						GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 1, GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDID));
					}
				}
				// intloop
				HighlightAllInList();
			}
		}
		// vbPorter upgrade warning: intFnum As short	OnWrite(string)
		private bool FieldCollisionTest(int intFnum, ref int lngX1, ref int lngY1, ref int lngX2, ref int lngY2)
		{
			bool FieldCollisionTest = false;
			// tests for a collision between this field and the bounding box
			int lngTestX1;
			int lngTestX2;
			int lngTestY1;
			int lngTestY2;
			FieldCollisionTest = false;
			lngTestX1 = CustomLabel[intFnum].Left;
			lngTestX2 = CustomLabel[intFnum].Left + CustomLabel[intFnum].Width;
			lngTestY1 = CustomLabel[intFnum].Top;
			lngTestY2 = CustomLabel[intFnum].Top + CustomLabel[intFnum].Height;
			// easy tests.
			if (lngTestX1 > lngX2)
				return FieldCollisionTest;
			if (lngTestX2 < lngX1)
				return FieldCollisionTest;
			if (lngTestY1 > lngY2)
				return FieldCollisionTest;
			if (lngTestY2 < lngY1)
				return FieldCollisionTest;
			// something is within the range, test to see if there is true overlap though
			if ((lngTestX1 >= lngX1 && lngTestX1 <= lngX2) || (lngTestX2 >= lngX1 && lngTestX2 <= lngX2) || (lngTestX1 <= lngX1 && lngTestX2 >= lngX2))
			{
				// the widths over lap now see if they also overlap vertically
				if ((lngTestY1 >= lngY1 && lngTestY1 <= lngY2) || (lngTestY2 >= lngY1 && lngTestY2 <= lngY2) || (lngTestY1 <= lngY1 && lngTestY2 >= lngY2))
				{
					FieldCollisionTest = true;
				}
			}
			return FieldCollisionTest;
		}

		private void cmdLoadDeleteCancel_Click(object sender, System.EventArgs e)
		{
			framLoadDelete.Visible = false;
			if (Conversion.Val(GridLoadDelete.Tag) == CNSTGRIDLOADDELETEEXPORT)
			{
				ExportFormat();
			}
		}

		private void ExportFormat()
		{
			// export any of the formats checked in gridloaddelete
			string strTemp = "";
			string strDBPath = "";
			string strDBFile = "";
			string strImExDatabase;
			int X;
			bool boolChoseSome;
			try
			{
				// On Error GoTo ErrorHandler
				boolChoseSome = false;
				for (X = 1; X <= GridLoadDelete.Rows - 1; X++)
				{
					if (GridLoadDelete.TextMatrix(X, CNSTGRIDLOADDELETECOLCHECK) != string.Empty)
					{
						if (FCConvert.CBool(GridLoadDelete.TextMatrix(X, CNSTGRIDLOADDELETECOLCHECK)))
						{
							boolChoseSome = true;
						}
					}
				}
				// X
				if (!boolChoseSome)
					return;
				// first see what file they want to save this to
				//FC:FINAL:AM:#i731 - download instead the file
				// App.MainForm.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNCreatePrompt	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//App.MainForm.CommonDialog1.Text = "Export File To Create";
				//// .FileTitle = "Choose a directory and a name for the file to export"
				//App.MainForm.CommonDialog1.InitDir = Application.StartupPath;
				//App.MainForm.CommonDialog1.Filter = "Database files  (*.mdb)|*.mdb";
				//App.MainForm.CommonDialog1.DefaultExt = "mdb";
				////- App.MainForm.CommonDialog1.CancelError = true;
				//App.MainForm.CommonDialog1.ShowSave();
				//strTemp = App.MainForm.CommonDialog1.FileName;
				strTemp = Path.Combine(FCFileSystem.Statics.UserDataFolder, "export.mdb");
				strDBPath = Path.GetDirectoryName(strTemp);
				strDBFile = Path.GetFileName(strTemp);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				//FC:FINAL:AM:#3739 - don't reset the path
                //if (strDBPath == FCFileSystem.Statics.UserDataFolder)
				//	strDBPath = "";
				// no need to change paths when it is the curdir
				if (strDBPath != string.Empty)
				{
					if (Strings.Right(strDBPath, 1) != "\\")
						strDBPath += "\\";
				}
				strImExDatabase = strDBPath + strDBFile;
				if (File.Exists(strImExDatabase))
				{
					File.Delete(strImExDatabase);
				}
				if (!CreateFormatDatabase(ref strDBPath, ref strDBFile))
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("Could not create file", "File not created", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				if (!FillFormatDatabase(strDBPath, strDBFile))
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("Could not save data in file", "File not updated", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				//FC:FINAL:CHN - i.issue #1559: no export message and incorrect process.
				MessageBox.Show("Export complete", "Exported", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCUtils.Download(strTemp);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (Information.Err(ex).Number == FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlCancel))
					return;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ExportFormat", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: strDBPath As object	OnWrite(string)
		// vbPorter upgrade warning: strDBFile As object	OnWrite(string)
		private bool FillFormatDatabase(object strDBPath, string strDBFile)
		{
			bool FillFormatDatabase = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			clsDRWrapper clsLoad = new clsDRWrapper();
			int X;
			int lngFID = 0;
			int lngNewFID = 0;
			string strCols = "";
			string strVals = "";
			try
			{
				// On Error GoTo ErrorHandler
				FillFormatDatabase = false;
				clsSave.MegaGroup = "";
				// kk02022015 trouts-51
				clsSave.GroupName = "";
				// clsSave.GroupName = strDBPath
				clsSave.Execute("INSERT INTO [Module] ([Module]) Values ('" + strThisModule + "')", strDBFile);
				for (X = 1; X <= GridLoadDelete.Rows - 1; X++)
				{
					// if row is checked, then export it
					if (GridLoadDelete.TextMatrix(X, CNSTGRIDLOADDELETECOLCHECK) != string.Empty)
					{
						if (FCConvert.CBool(GridLoadDelete.TextMatrix(X, CNSTGRIDLOADDELETECOLCHECK)))
						{
							lngFID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridLoadDelete.TextMatrix(X, CNSTGRIDLOADDELETECOLAUTOID))));
							clsLoad.OpenRecordset("select * from CustomBills where ID = " + FCConvert.ToString(lngFID), "tw" + strThisModule + "0000.vb1");
							strCols = "BillFormat,FormatName,Units,PageHeight,PageWidth,TopMargin,LeftMargin,RightMargin," + "boolWide,BillType,IsLaser,IsDeletable,Description,BackgroundImage";
							// TODO: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
							strVals = clsLoad.Get_Fields("BillFormat") + ",'" + clsLoad.Get_Fields_String("FormatName") + "'," + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("Units"))) + "," + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("PageHeight"))) + "," + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("PageWidth"))) + "," + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("TopMargin"))) + "," + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("LeftMargin"))) + "," + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("RightMargin"))) + "," + clsLoad.Get_Fields_Boolean("boolWide") + "," + clsLoad.Get_Fields("BillType") + "," + clsLoad.Get_Fields_Boolean("IsLaser") + "," + clsLoad.Get_Fields_Boolean("IsDeletable") + ",'" + clsLoad.Get_Fields_String("Description") + "','" + clsLoad.Get_Fields_String("BackGroundImage") + "'";
							clsSave.Execute("INSERT INTO CustomBills (" + strCols + ") VALUES (" + strVals + ")", strDBFile);
							clsSave.OpenRecordset("SELECT MAX(AutoID) AS MaxID FROM CustomBills", strDBFile);
							// TODO: Field [MaxID] not found!! (maybe it is an alias?)
							lngNewFID = FCConvert.ToInt32(Math.Round(Conversion.Val(clsSave.Get_Fields("MaxID"))));
							// now save all the fields that go with this format
							clsLoad.OpenRecordset("select * from custombillfields where formatid = " + FCConvert.ToString(lngFID), "tw" + strThisModule + "0000.vb1");
							while (!clsLoad.EndOfFile())
							{
								strCols = "FormatID,FieldNumber,FieldID,[Top],[Left],Height,Width,BillType,Description,Alignment," + "UserText,Font,FontSize,FontStyle,ExtraParameters";
								//FC:FINAL:DSE Exception when field contains special character
								string strExtraParameter = modGlobalFunctions.EscapeQuotes(clsLoad.Get_Fields_String("extraparameters"));
								if (strExtraParameter.Contains("'"))
								{
									strExtraParameter = strExtraParameter.Replace("'", "''");
								}
								// TODO: Check the table for the column [FieldID] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [Width] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [FontSize] and replace with corresponding Get_Field method
								strVals = FCConvert.ToString(lngNewFID) + "," + clsLoad.Get_Fields_Int32("FieldNumber") + "," + clsLoad.Get_Fields("FieldID") + "," + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("Top"))) + "," + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("Left"))) + "," + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("Height"))) + "," + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("Width"))) + "," + clsLoad.Get_Fields("BillType") + ",'" + clsLoad.Get_Fields_String("Description") + "'," + clsLoad.Get_Fields_Int16("Alignment") + ",'" + clsLoad.Get_Fields_String("UserText") + "','" + clsLoad.Get_Fields_String("Font") + "'," + clsLoad.Get_Fields("FontSize") + "," + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("FontStyle"))) + ",'" + strExtraParameter + "'";
								clsSave.Execute("INSERT INTO CustomBillFields (" + strCols + ") VALUES (" + strVals + ")", strDBFile);
								clsLoad.MoveNext();
							}
						}
					}
				}
				// X
				FillFormatDatabase = true;
				return FillFormatDatabase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In FillFormatDatabase", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillFormatDatabase;
		}

		private bool CreateFormatDatabase(ref string strDBPath, ref string strDBFile)
		{
			bool CreateFormatDatabase = false;
			FCDatabase dbCreate;
			clsDRWrapper clsSave = new clsDRWrapper();
			int X;
			string strConn;
			bool boolTblExists;
			// 
			string strTableName = "";
			try
			{
				// On Error GoTo ErrorHandler
				CreateFormatDatabase = false;
				dbCreate = FCDAO.DBEngine.CreateDatabase(strDBPath + strDBFile, "dbLangGeneral");
				//, dbVersion40);
				// kk02022015 trouts-51  Need to create the tables in the new export database - put this code back in
				if (!modCustomBill.AddModuleTable(ref dbCreate))
				{
					return CreateFormatDatabase;
				}
				if (!modCustomBill.AddCustomBillTable(ref dbCreate))
				{
					return CreateFormatDatabase;
				}
				if (!modCustomBill.AddCustomBillFields(ref dbCreate))
				{
					return CreateFormatDatabase;
				}
				dbCreate.Close();
				clsSave.MegaGroup = "";
				// kk02022015 trouts-51
				clsSave.GroupName = "";
				// clsSave.GroupName = strDBPath
				//FC:FINAL:AM:#i731 use OleDB
				//strConn = clsSave.MakeODBCConnectionStringForAccess(strDBPath + strDBFile, false);
				//clsSave.AddConnection(strConn, strDBFile, FCConvert.ToInt32(clsDRWrapper.dbConnectionTypes.ODBC);
				strConn = clsSave.MakeOleDBConnectionStringForAccess(strDBPath + strDBFile, false);
				clsSave.AddConnection(strConn, strDBFile, FCConvert.ToInt32(clsDRWrapper.dbConnectionTypes.OLEDB));
				CreateFormatDatabase = true;
				return CreateFormatDatabase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CreateFormatDatabase", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateFormatDatabase;
		}

		private void cmdOKControlInfo_Click(object sender, System.EventArgs e)
		{
			// fill gridfields with this new info
			int X;
			string strTemp = "";
			mnuProcess.Enabled = true;
			framPage.Enabled = true;
			if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
			{
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(0));
			}
			else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
			{
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(0));
			}
			else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
			{
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLMISC, FCConvert.ToString(Conversion.Val(GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT))));
			}
			GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLCONTROL, GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDATA));
			GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLTEXT, GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA));
			GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLALIGNMENT, GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWALIGN, CNSTGRIDCONTROLINFOCOLDATA));
			GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONT, GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONT, CNSTGRIDCONTROLINFOCOLDATA));
			GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSIZE, GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSIZE, CNSTGRIDCONTROLINFOCOLDATA));
			if (Strings.UCase(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSTYLE, CNSTGRIDCONTROLINFOCOLDATA)) == "BOLD")
			{
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSTYLE, FCConvert.ToString(CNSTCUSTOMBILLFONTSTYLEBOLD));
			}
			else if (Strings.UCase(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSTYLE, CNSTGRIDCONTROLINFOCOLDATA)) == "BOLD ITALIC")
			{
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSTYLE, FCConvert.ToString(CNSTCUSTOMBILLFONTSTYLEBOLDITALIC));
			}
			else if (Strings.UCase(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSTYLE, CNSTGRIDCONTROLINFOCOLDATA)) == "ITALIC")
			{
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSTYLE, FCConvert.ToString(CNSTCUSTOMBILLFONTSTYLEITALIC));
			}
			else
			{
				// regular
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSTYLE, FCConvert.ToString(CNSTCUSTOMBILLFONTSTYLEREGULAR));
			}
			GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID, GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA));
			if (GridControlInfo.Rows > CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS)
			{
				// there were extra parameters
				strTemp = "";
				for (X = CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS; X <= GridControlInfo.Rows - 1; X++)
				{
					strTemp += GridControlInfo.TextMatrix(X, CNSTGRIDCONTROLINFOCOLDATA) + ";";
				}
				// X
				if (strTemp != string.Empty)
				{
					strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
					// drop the last semi-colon
				}
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLEXTRAPARAMETERS, strTemp);
			}
			GridFields.RowData(GridFields.Row, true);
			// mark as edited
			framControlInfo.Visible = false;
			ReDrawField(GridFields.Row);
			if (!boolInHighlightMode)
			{
				UnhighlightAllControls();
				GridHighlighted.Rows = 1;
				GridHighlighted.TextMatrix(0, 0, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM));
				GridHighlighted.TextMatrix(0, 1, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID));
				HighlightControl(FCConvert.ToInt32(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM)));
			}
			GridFields.Focus();
		}

		public void cmdOKControlInfo_Click()
		{
			cmdOKControlInfo_Click(cmdOKControlInfo, new System.EventArgs());
		}

		private void cmdOKControlInfoNew_Click(object sender, System.EventArgs e)
		{
			double dblRatio;
			double dblDefWidth;
			double dblDefHeight;
			// vbPorter upgrade warning: lngW As int	OnWriteFCConvert.ToDouble(
			int lngW;
			// vbPorter upgrade warning: lngH As int	OnWriteFCConvert.ToDouble(
			int lngH;
			int lngFNum;
			int lngRow;
			int lngTemp = 0;
			lngRow = GridFields.Row;
			lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDNUM))));
			dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
			if (Conversion.Val(GridControlTypes.TextMatrix(GridControlTypes.Row, CNSTGRIDCONTROLTYPESCOLCODE)) != Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)))
			{
				lngTemp = GridControlTypes.FindRow(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA), -1, CNSTGRIDCONTROLTYPESCOLCODE);
				if (lngTemp >= 0)
				{
					GridControlTypes.Row = lngTemp;
				}
			}
			dblDefWidth = Conversion.Val(GridControlTypes.TextMatrix(GridControlTypes.Row, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH));
			dblDefHeight = Conversion.Val(GridControlTypes.TextMatrix(GridControlTypes.Row, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT));
			if (Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA)) == "CENTIMETERS")
			{
				// defaults are in inches
				dblDefWidth *= 2.54;
				dblDefHeight *= 2.54;
			}
			else
			{
				// inches, do nothing
			}
			lngW = FCConvert.ToInt32(dblDefWidth * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			lngH = FCConvert.ToInt32(dblDefHeight * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
			{
				// don't put against top side or it wont be easy to see
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(1));
				// doesn't matter what units we use, just need to see this
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(dblDefWidth));
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(0));
				CustomLabel[FCConvert.ToInt16(lngFNum)].WidthOriginal = lngW;
				CustomLabel[FCConvert.ToInt16(lngFNum)].HeightOriginal = 0;
				CustomLabel[FCConvert.ToInt16(lngFNum)].TopOriginal = FCConvert.ToInt32(dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				// 1 unit
			}
			else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
			{
				// don't put against left side or it wont be easy to see
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(0));
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(0));
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(dblDefHeight));
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(1));
				// doesn't matter what units
				CustomLabel[FCConvert.ToInt16(lngFNum)].WidthOriginal = 0;
				CustomLabel[FCConvert.ToInt16(lngFNum)].HeightOriginal = lngH;
				CustomLabel[FCConvert.ToInt16(lngFNum)].TopOriginal = 0;
				CustomLabel[FCConvert.ToInt16(lngFNum)].LeftOriginal = FCConvert.ToInt32(dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			}
			else
			{
				if (dblDefWidth > 0)
				{
					GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(dblDefWidth));
					CustomLabel[FCConvert.ToInt16(lngFNum)].WidthOriginal = lngW;
				}
				if (dblDefHeight > 0)
				{
					GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(dblDefHeight));
					CustomLabel[FCConvert.ToInt16(lngFNum)].HeightOriginal = lngH;
				}
				if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
				{
					GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLMISC, FCConvert.ToString(Conversion.Val(GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT))));
				}
			}
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION, GridControlTypes.TextMatrix(GridControlTypes.Row, CNSTGRIDCONTROLTYPESCOLNAME));
			cmdOKControlInfoNew.Visible = false;
			cmdOKControlInfo.Visible = true;
			//FC:FINAL:BBE:#321 - bring the visible button to the front, otherwise the click event will not be raised.
			cmdOKControlInfo.BringToFront();
			cmdOKControlInfo_Click();
		}
		// Private Sub cmdOKFont_Click()
		// Dim lngFNum As Long
		//
		//
		// GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONT) = GridFont.TextMatrix(CNSTGRIDFONTROWDATA, CNSTGRIDFONTCOLNAME)
		// GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSIZE) = GridFont.TextMatrix(CNSTGRIDFONTROWDATA, CNSTGRIDFONTCOLSIZE)
		//
		// lngFNum = Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))
		// Me.Controls("CustomLabel" & lngFNum).Font = GridFont.TextMatrix(CNSTGRIDFONTROWDATA, CNSTGRIDFONTCOLNAME)
		// Me.Controls("CustomLabel" & lngFNum).Font.Size = Val(GridFont.TextMatrix(CNSTGRIDFONTROWDATA, CNSTGRIDFONTCOLSIZE)) * dblSizeRatio / dblPageRatio
		//
		// framFont.Visible = False
		// End Sub
		private void cmdUserDefinedTextOK_Click(object sender, System.EventArgs e)
		{
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA, rtbUserText.Text);
			framUserText.Visible = false;
		}

		private void CustomLabel_Click(short Index, object sender, System.EventArgs e)
		{
			// give this focus
			int intloop;
			int lngRow = 0;
			// lngClickXOffset = CustomLabel(Index).Left
			// lngClickYOffset = CustomLabel(Index).Top
			for (intloop = 1; intloop <= GridFields.Rows - 1; intloop++)
			{
				if (Conversion.Val(GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM)) == Index)
				{
					lngRow = intloop;
					break;
				}
			}
			// intloop
			GridFields.Row = lngRow;
			GridFields.TopRow = lngRow;
			GridFields.Focus();
		}

		private void CustomLabel_Click(object sender, System.EventArgs e)
		{
			short index = CustomLabel.GetIndex((FCLabel)sender);
			CustomLabel_Click(index, sender, e);
		}

		private void CustomLabel_DragDrop(short Index, object sender, Wisej.Web.DragEventArgs e)
		{
			double dblRatio = 0;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp = 0;
			// vbPorter upgrade warning: lngRow As int	OnWrite(string, int)
			int lngRow = 0;
			int intloop;
			int intLoop2;
			Control source = sender as Control;
			if (Strings.UCase(source.GetName()) == "CUSTOMLABEL" && source.GetIndex() == 0)
			{
				// in group mode
				if (e.Y - lngClickYOffset + CustomLabel[Index].Top < 0)
				{
					source.Top = 0;
				}
				else
				{
					if ((e.Y - lngClickYOffset + CustomLabel[Index].Top) + source.Height > Picture1.Height)
					{
						source.Top = Picture1.Height - source.Height;
					}
					else
					{
						source.Top = FCConvert.ToInt32(e.Y - lngClickYOffset + CustomLabel[Index].Top);
					}
				}
				if (e.X - lngClickXOffset + CustomLabel[Index].Left < 0)
				{
					source.Left = 0;
				}
				else
				{
					if ((e.X - lngClickXOffset + CustomLabel[Index].Left) + source.Width > Picture1.Width)
					{
						source.Left = Picture1.Width - source.Width;
					}
					else
					{
						source.Left = FCConvert.ToInt32(e.X - lngClickXOffset + CustomLabel[Index].Left);
					}
				}
				// now move all controls the same amount
				int lngLeftMove = 0;
				int lngTopMove = 0;
				lngTopMove = source.Top - ctlGroupControl.Top;
				lngLeftMove = source.Left - ctlGroupControl.Left;
				for (intloop = 0; intloop <= GridHighlighted.Rows - 1; intloop++)
				{
					CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].Left = CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].Left + lngLeftMove;
					CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].Top = CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].Top + lngTopMove;
					lngRow = FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0));
					for (intLoop2 = 1; intLoop2 <= GridFields.Rows - 1; intLoop2++)
					{
						if (Conversion.Val(GridFields.TextMatrix(intLoop2, CNSTGRIDFIELDSCOLFIELDNUM)) == lngRow)
						{
							lngRow = intLoop2;
							break;
						}
					}
					// intLoop2
					dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
					dblTemp = FCConvert.ToDouble(Strings.Format(CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].Left * dblRatio, "0.0000"));
					GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
					dblTemp = FCConvert.ToDouble(Strings.Format(CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].Top * dblRatio, "0.0000"));
					GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
					GridFields.RowData(lngRow, true);
					// mark as changed
				}
				// intloop
				lngClickXOffset = 0;
				lngClickYOffset = 0;
				CustomLabel[0].Visible = false;
				MakeGroupBoxFromList();
				return;
			}
			else
			{
				if (e.Y - lngClickYOffset + CustomLabel[Index].Top < 0)
				{
					source.Top = 0;
				}
				else
				{
					if ((e.Y - lngClickYOffset + CustomLabel[Index].Top) + source.Height > Picture1.Height)
					{
						source.Top = Picture1.Height - source.Height;
					}
					else
					{
						source.Top = FCConvert.ToInt32(e.Y - lngClickYOffset + CustomLabel[Index].Top);
					}
				}
				if (e.X - lngClickXOffset + CustomLabel[Index].Left < 0)
				{
					source.Left = 0;
				}
				else
				{
					if ((e.X - lngClickXOffset + CustomLabel[Index].Left) + source.Width > Picture1.Width)
					{
						source.Left = Picture1.Width - source.Width;
					}
					else
					{
						source.Left = FCConvert.ToInt32(e.X - lngClickXOffset + CustomLabel[Index].Left);
					}
				}
				lngRow = FCConvert.ToInt32(Math.Round(Conversion.Val(source.Tag)));
				for (intloop = 1; intloop <= GridFields.Rows - 1; intloop++)
				{
					if (Conversion.Val(GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM)) == lngRow)
					{
						lngRow = intloop;
						break;
					}
				}
				// intloop
				dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
				dblTemp = FCConvert.ToDouble(Strings.Format(source.Left * dblRatio, "0.0000"));
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
				dblTemp = FCConvert.ToDouble(Strings.Format(source.Top * dblRatio, "0.0000"));
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
				GridFields.RowData(lngRow, true);
				// mark as changed
				// Source.Left = X - lngClickXOffset + CustomLabel(Index).Left
				// Source.Top = Y - lngClickYOffset + CustomLabel(Index).Top
				lngClickXOffset = 0;
				lngClickYOffset = 0;
				// lngClickXOffset = X
				// lngClickYOffset = Y
			}
		}
		//FC:FINAL:SBE - #318 - changed DragAndDrop with Movable property. Use MouseUp to check when 'drop' is done
		private void CustomLabel_0_MouseUp(object sender, MouseEventArgs e)
		{
			BillImage_DragDrop(sender, new DragEventArgs(DragDropEffects.Move, DragDropEffects.Move, sender, e.Location));
		}

		private void CustomLabel_DragDrop(object sender, Wisej.Web.DragEventArgs e)
		{
			short index = CustomLabel.GetIndex((FCLabel)sender);
			CustomLabel_DragDrop(index, sender, e);
		}

		private void CustomLabel_MouseDown(short Index, object sender, Wisej.Web.MouseEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X));
			float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y));
			int intloop;
			int lngRow = 0;
			int lngTemp = 0;
			// lngClickXOffset = CustomLabel(Index).Left
			// lngClickYOffset = CustomLabel(Index).Top
			if (Button == MouseButtonConstants.LeftButton)
			{
				//lngClickXOffset = FCConvert.ToInt32(X);
				//lngClickYOffset = FCConvert.ToInt32(Y);
				lngClickXOffset = CustomLabel[Index].Left;
				lngClickYOffset = CustomLabel[Index].Top;
				for (intloop = 1; intloop <= GridFields.Rows - 1; intloop++)
				{
					if (Conversion.Val(GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM)) == Index)
					{
						lngRow = intloop;
						break;
					}
				}
				// intloop
				if (Shift != FCConvert.ToInt32(ShiftConstants.CtrlMask))
				{
					if (!IsInHighlightList(ref Index) || !boolInHighlightMode || (IsInHighlightList(ref Index) && GridHighlighted.Rows <= 1))
					{
						boolInHighlightMode = false;
						UnhighlightAllControls();
						GridHighlighted.Rows = 1;
						GridHighlighted.TextMatrix(0, 0, FCConvert.ToString(Index));
						GridHighlighted.TextMatrix(0, 1, FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID))));
						ShowGroupBox_2(false);
					}
					else
					{
						if (GridHighlighted.Rows > 0)
						{
							lngTemp = GridHighlighted.FindRow(Index, 0, 0);
							if (lngTemp >= 0)
							{
								GridHighlighted.RemoveItem(lngTemp);
								GridHighlighted.Rows += 1;
								GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0, FCConvert.ToString(Index));
								GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 1, FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID))));
							}
							MakeGroupBoxFromList();
						}
					}
				}
				else
				{
					boolInHighlightMode = true;
					if (GridHighlighted.Rows > 0)
					{
						// don't want duplicate entries
						lngTemp = GridHighlighted.FindRow(Index, 0, 0);
						if (lngTemp >= 0)
						{
							GridHighlighted.RemoveItem(lngTemp);
						}
					}
					GridHighlighted.Rows += 1;
					GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0, FCConvert.ToString(Index));
					GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 1, FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID))));
					ShowGroupBox();
				}
				CustomLabel[Index].BackStyle = 1;
				CustomLabel[Index].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
                HighlightAllInList();
				GridFields.Row = lngRow;
				GridFields.TopRow = lngRow;
				GridFields.Focus();
                
				if (Shift != FCConvert.ToInt32(ShiftConstants.CtrlMask))
				{
					if (!boolInHighlightMode)
					{
						CustomLabel[0].Visible = false;
						CustomLabel[0].BorderStyle = 0;
						CustomLabel[Index].Focus();
						//CustomLabel[Index].Drag();
					}
					else
					{
						CustomLabel[0].Visible = true;
						CustomLabel[0].BackStyle = 0;
						CustomLabel[0].BorderStyle = 0;
						// CustomLabel(Index).Drag
						MakeGroupBoxFromList();
						CustomLabel[0].Left = ctlGroupControl.Left;
						CustomLabel[0].Top = ctlGroupControl.Top;
						CustomLabel[0].Height = ctlGroupControl.Height;
						CustomLabel[0].Width = ctlGroupControl.Width;
						// adjust the offsets since we clicked on a field not the customlabel(0) ctl
						lngClickYOffset = CustomLabel[Index].Top - CustomLabel[0].Top + lngClickYOffset;
						lngClickXOffset = CustomLabel[Index].Left - CustomLabel[0].Left + lngClickXOffset;
						//CustomLabel[0].Drag();
					}
				}
			}
			else if (Button == MouseButtonConstants.RightButton)
			{
				// lets pop up a menu
				// show all of the options
				mnuAlignBottoms.Visible = true;
				mnuAlignLefts.Visible = true;
				mnuAlignRights.Visible = true;
				mnuAlignTops.Visible = true;
				mnuBringToFront.Visible = true;
				mnuSendToBack.Visible = true;
				mnuPopSepar1.Visible = true;
				mnuPopSepar2.Visible = true;
				this.PopupMenu(mnuPop);
			}
		}

		private void CustomLabel_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		{
			short index = CustomLabel.GetIndex((FCLabel)sender);
			CustomLabel_MouseDown(index, sender, e);
		}

		private bool IsInHighlightList(ref short intFieldNum)
		{
			bool IsInHighlightList = false;
			int X;
			IsInHighlightList = false;
			for (X = 0; X <= GridHighlighted.Rows - 1; X++)
			{
				if (FCConvert.ToDouble(GridHighlighted.TextMatrix(X, 0)) == intFieldNum)
				{
					IsInHighlightList = true;
					return IsInHighlightList;
				}
			}
			// X
			return IsInHighlightList;
		}

		private object HighlightAllInList()
		{
			object HighlightAllInList = null;
			int X;
			for (X = 0; X <= GridHighlighted.Rows - 1; X++)
			{
				CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0))].BackColor = ColorTranslator.FromOle(Information.RGB(155, 193, 223));
			}
			// X
			if (GridHighlighted.Rows > 0)
			{
				CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0))].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
			}
			return HighlightAllInList;
		}

		private void UnhighlightAllControls(bool boolLeaveLast = false)
		{
			int intloop;
			int lngRow;
			ShowGroupBox_2(false);
			for (intloop = GridHighlighted.Rows - 1; intloop >= 0; intloop--)
			{
				if (!boolLeaveLast || intloop < GridHighlighted.Rows - 1)
				{
					if (Conversion.Val(GridHighlighted.TextMatrix(intloop, 1)) == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED)
					{
						CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].BackStyle = 1;
						CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].BackColor = Color.Black;
					}
					else
					{
						CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].BackColor = Color.White;
						CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(intloop, 0))].BackStyle = 0;
					}
					// If Val(GridFields.TextMatrix(intLoop, CNSTGRIDFIELDSCOLFIELDNUM)) > 0 Then
					// If Val(GridFields.TextMatrix(intLoop, CNSTGRIDFIELDSCOLFIELDID)) = CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED Then
					// CustomLabel(Val(GridFields.TextMatrix(intLoop, CNSTGRIDFIELDSCOLFIELDNUM))).BackStyle = 1
					// CustomLabel(Val(GridFields.TextMatrix(intLoop, CNSTGRIDFIELDSCOLFIELDNUM))).BackColor = vbBlack
					// Else
					// CustomLabel(Val(GridFields.TextMatrix(intLoop, CNSTGRIDFIELDSCOLFIELDNUM))).BackStyle = vbTransparent
					// End If
					// End If
					GridHighlighted.RemoveItem(intloop);
				}
			}
			// intloop
			//Application.DoEvents();
		}

		private void MakeGroupBoxFromList()
		{
			int lngL;
			int lngR;
			int lngT;
			int lngB;
			int X;
			int lngTemp = 0;
			// make a bounding box for all selected controls
			// initialize so it wont fail
			lngB = 0;
			lngT = 200000;
			lngR = 0;
			lngL = 200000;
			if (GridHighlighted.Rows > 0)
			{
				for (X = 0; X <= GridHighlighted.Rows - 1; X++)
				{
					if (CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0))].Left < lngL)
					{
						lngL = CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0))].Left;
					}
					if (CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0))].Top < lngT)
					{
						lngT = CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0))].Top;
					}
					lngTemp = CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0))].Left + CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0))].Width;
					if (lngTemp > lngR)
					{
						lngR = lngTemp;
					}
					lngTemp = CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0))].Top + CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0))].Height;
					if (lngTemp > lngB)
					{
						lngB = lngTemp;
					}
				}
				// X
			}
			else
			{
				lngT = 0;
				lngL = 0;
			}
			ctlGroupControl.Left = FCConvert.ToInt32(FCUtils.TwipsToPixelsX(lngL));
			ctlGroupControl.Top = FCConvert.ToInt32(FCUtils.TwipsToPixelsY(lngT));
			ctlGroupControl.Height = FCConvert.ToInt32(FCUtils.TwipsToPixelsY(lngB - lngT));
			ctlGroupControl.Width = FCConvert.ToInt32(FCUtils.TwipsToPixelsX(lngR - lngL));
			lngGroupBoxBottom = lngB;
			lngGroupBoxLeft = lngL;
			lngGroupBoxRight = lngR;
			lngGroupBoxTop = lngT;
		}

		private void ShowGroupBox_2(bool boolMakeVisible)
		{
			ShowGroupBox(boolMakeVisible);
		}

		private void ShowGroupBox(bool boolMakeVisible = true)
		{
			if (boolMakeVisible)
			{
				// ctlGroupControl.Visible = True
				ctlGroupControl.Visible = false;
				MakeGroupBoxFromList();
			}
			else
			{
				ctlGroupControl.Visible = false;
				MakeGroupBoxFromList();
			}
		}
		// vbPorter upgrade warning: lngCNum As int	OnWrite(string, int)
		private void HighlightControl(int lngCNum)
		{
			CustomLabel[FCConvert.ToInt16(lngCNum)].BackStyle = 1;
			CustomLabel[FCConvert.ToInt16(lngCNum)].BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
			//Application.DoEvents();
		}

		private void frmCustomBill_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			int lngFNum;
			int lngPosition;
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						if (mnuProcess.Enabled)
						{
							if (MessageBox.Show("Are you sure you want to exit the screen?", "Exit?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								mnuExit_Click();
							}
						}
						break;
					}
			}
			//end switch
		}

		private void frmCustomBill_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomBill.FillStyle	= 0;
			//frmCustomBill.ScaleWidth	= 9225;
			//frmCustomBill.ScaleHeight	= 7965;
			//frmCustomBill.LinkTopic	= "Form2";
			//frmCustomBill.LockControls	= true;
			//frmCustomBill.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			string strTemp;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			dblSizeRatio = RatioLine.X2 / 1440;
			SetupGridControlTypes();
			SetupGridControlInfo();
			SetupGridBillSize();
			SetupGridFields();
			SetupGridLoadDelete();
			InitializeValues();
			ResizePage();
			strTemp = modRegistry.GetRegistryKey("CustomBillHorizontalIncrements");
			if (Conversion.Val(strTemp) == 0)
			{
				// default to full
				intCharWid = 144;
			}
			else
			{
				intCharWid = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			}
			strTemp = modRegistry.GetRegistryKey("CustomBillVerticalIncrements");
			if (Conversion.Val(strTemp) == 0)
			{
				// default to full
				intLineHite = 240;
			}
			else
			{
				intLineHite = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			}
			switch (intCharWid)
			{
				case 72:
					{
						mnuFullCharFullLine.Checked = false;
						mnuMoveHalfCharHalfLine.Checked = true;
						mnuMoveQuarterCharQuarterLine.Checked = false;
						break;
					}
				case 36:
					{
						mnuFullCharFullLine.Checked = false;
						mnuMoveHalfCharHalfLine.Checked = false;
						mnuMoveQuarterCharQuarterLine.Checked = true;
						break;
					}
				default:
					{
						mnuFullCharFullLine.Checked = true;
						mnuMoveQuarterCharQuarterLine.Checked = false;
						mnuMoveHalfCharHalfLine.Checked = false;
						break;
					}
			}
			//end switch
			LoadCustomBill_6(CurrentLoadedFormat.lngBillID, "twUT0000.vb1");
			//FC:FINAL:AM: attach event handler after load
			this.Resize += new System.EventHandler(this.frmCustomBill_Resize);
		}

		private void InitializeValues()
		{
			// initializes things like the pageratio etc.
			// this defaults to letter size
			lngPageWidth = FCConvert.ToInt32(8.5 * 1440);
			// assuming no margins
			lngPageHeight = 11 * 1440;
			// assuming no margins
			//FC:FINAL:BBE:#323 - make preview larger 50%
			//dblPageRatio = 1.5; // The beginning size will be half (not including dblSizeRatio when the form is resized) of the page size
			dblPageRatio = 0.8;
			dblTwipsPerUnit = 1440;
			// inches
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA, "Inches");
			lngScreenPageWidth = FCConvert.ToInt32(lngPageWidth / dblPageRatio);
			lngScreenPageHeight = FCConvert.ToInt32(lngPageHeight / dblPageRatio);
			//lngOffsetY = 240;
			//lngOffsetX = 120;
			lngOffsetY = 290;
			lngOffsetX = 435;
			Picture1.BackColor = Color.White;
		}

		private void ResizePage()
		{
			// resizes the picture box
			double dblTemp;
			int lngTemp;
			int X;
			Picture1.WidthOriginal = FCConvert.ToInt32(lngScreenPageWidth * dblSizeRatio);
			Picture1.HeightOriginal = FCConvert.ToInt32(lngScreenPageHeight * dblSizeRatio);
			for (X = 1; X <= GridFields.Rows - 1; X++)
			{
				if (Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM)) > 0)
				{
					CustomLabel[FCConvert.ToInt32(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM))].FontSize = FCConvert.ToSingle(dblSizeRatio / dblPageRatio * Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFONTSIZE)));
					CustomLabel[FCConvert.ToInt32(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM))].HeightOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLHEIGHT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
					CustomLabel[FCConvert.ToInt32(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM))].WidthOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLWIDTH)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
					CustomLabel[FCConvert.ToInt32(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM))].LeftOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLLEFT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
					CustomLabel[FCConvert.ToInt32(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM))].TopOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLTOP)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				}
			}
			// X
			BillImage.WidthOriginal = Picture1.WidthOriginal;
			BillImage.HeightOriginal = Picture1.HeightOriginal;
			// lngTemp = Picture1.HeightOriginal
			// need to make the 0 to max of the scroll bar the height of the page - the visible part of the page
			// so when you scroll all the way it doesn't scroll the page out of sight
			// lngTemp = (lngTemp - (framPage.Height)) / 20   'subtract the visible frame height (leave the bottom offset)
			// VScroll1.Max = lngTemp + lngOffsetY
			VScroll1_Change();
			HScroll1_Change();
		}

		private void frmCustomBill_Resize(object sender, System.EventArgs e)
		{
			dblSizeRatio = RatioLine.X2 / 1440;
			// line is originally 1440 wide (1 inch)
			lngOffsetY = FCConvert.ToInt32(290 * dblSizeRatio);
			lngOffsetX = FCConvert.ToInt32(435 * dblSizeRatio);
			ResizePage();
			ResizeGridFields();
			ResizeGridBillSize();
			ResizeGridControlInfo();
			ResizeGridControlTypes();
			ResizeGridLoadDelete();
			//cmdOKControlInfo.Top = (framControlInfo.Height) - cmdOKControlInfo.Height; //- 50;
			//cmdOKControlInfoNew.Top = cmdOKControlInfo.Top;
			//FC:FINAL:DDU:#i970 - fixed frames show
			//FC:FINAL:BBE:#323 - show/hide frames should be centered
			//framUserText.CenterToContainer(this.ClientArea);
			//framLoadDelete.CenterToContainer(this.ClientArea);
			//framControlInfo.CenterToContainer(this.ClientArea);
		}

		private void GridBillSize_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			switch (GridBillSize.GetFlexRowIndex(e.RowIndex))
			{
				case CNSTGRIDBILLSIZEROWSTYLE:
					{
						GridBillSize.ComboList = "Laser";
						break;
					}
				case CNSTGRIDBILLSIZEROWUNITS:
					{
						GridBillSize.ComboList = "Centimeters|Inches";
						break;
					}
				case CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE:
					{
						if (Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA)) != "NONE" && Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA)) != "CHOOSE NEW")
						{
							GridBillSize.ComboList = Strings.Trim(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA)) + "|None|Choose New";
						}
						else
						{
							GridBillSize.ComboList = "None|Choose New";
						}
						break;
					}
				case CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE:
					{
						GridBillSize.ComboList = "8|9|10|12|14|16";
						break;
					}
				default:
					{
						GridBillSize.ComboList = string.Empty;
						// don't want a combo
						break;
					}
			}
			//end switch
		}

		private void GridBillSize_ChangeEdit(object sender, Wisej.Web.DataGridViewCellEventArgs e)
		{
			string strTemp = "";
			string strOld = "";
			try
			{
				// On Error GoTo ErrorHandler
				if (GridBillSize.Row == CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE)
				{
					if (Strings.UCase(GridBillSize.EditText) == "NONE")
					{
						// BillImage.Visible = False
						BillImage.Image = null;
						GridBillSize.ComboList = "None|Choose New";
						GridBillSize.EditText = "None";
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA, "None");
						GridBillSize.Col = CNSTGRIDBILLSIZECOLDESCRIPTION;
						GridBillSize.Refresh();
						GridBillSize.Col = CNSTGRIDBILLSIZECOLDATA;
					}
					else if (Strings.UCase(GridBillSize.EditText) == "CHOOSE NEW")
					{
						strOld = GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA);
						// App.MainForm.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
						App.MainForm.CommonDialog1.Text = "Load Image";
						App.MainForm.CommonDialog1.FileName = "";
						App.MainForm.CommonDialog1.Filter = "Images (*.bmp,*.gif,*.jpg)|*.bmp;*.gif;*.jpg";
						App.MainForm.CommonDialog1.InitDir = FCFileSystem.Statics.UserDataFolder;
						App.MainForm.CommonDialog1.CancelError = false;
						App.MainForm.CommonDialog1.ShowOpen();
						strTemp = App.MainForm.CommonDialog1.FileName;
						if (strTemp != string.Empty)
						{
							BillImage.Image = FCUtils.LoadPicture(strTemp);
							BillImage.WidthOriginal = Picture1.WidthOriginal;
							BillImage.HeightOriginal = Picture1.HeightOriginal;
							BillImage.Visible = true;
							GridBillSize.ComboList = Strings.Trim(strTemp) + "|None|Choose New";
							GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA, Strings.Trim(strTemp));
							GridBillSize.EditText = strTemp;
						}
						else
						{
							GridBillSize.EditText = strOld;
							GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA, strOld);
						}
					}
					else
					{
						// don't need to do anything
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GridBillSize_ChangeEdit", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GridBillSize_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// vbPorter upgrade warning: dblTemp As double	OnWrite(double, string)
			double dblTemp = 0;
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp;
			// vbPorter upgrade warning: lngPHeight As int	OnWriteFCConvert.ToDouble(
			int lngPHeight;
			// vbPorter upgrade warning: lngLMargin As int	OnWriteFCConvert.ToDouble(
			int lngLMargin;
			// vbPorter upgrade warning: lngRMargin As int	OnWriteFCConvert.ToDouble(
			int lngRMargin;
			// vbPorter upgrade warning: lngTMargin As int	OnWriteFCConvert.ToDouble(
			int lngTMargin;
			// vbPorter upgrade warning: lngBMargin As int	OnWriteFCConvert.ToDouble(
			int lngBMargin;
			// vbPorter upgrade warning: lngPWidth As int	OnWriteFCConvert.ToDouble(
			int lngPWidth;
			string strTemp = "";
			int X;
			switch (GridBillSize.GetFlexRowIndex(e.RowIndex))
			{
				case CNSTGRIDBILLSIZEROWUNITS:
					{
						strTemp = Strings.UCase(GridBillSize.EditText);
						if (strTemp != Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA)) && Strings.Trim(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA)) != string.Empty)
						{
							// changed units so convert
							if (strTemp == "CENTIMETERS")
							{
								// from inch to cent
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA));
								dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA));
								dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA));
								dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA));
								dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp * 2.54, "0.0000"));
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA));
								dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp * 2.54, "0.0000"));
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA));
								dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								for (X = 1; X <= GridFields.Rows - 1; X++)
								{
									dblTemp = Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLTOP));
									dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
									GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
									dblTemp = Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLLEFT));
									dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
									GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
									dblTemp = Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLHEIGHT));
									dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
									GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(dblTemp));
									dblTemp = Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLWIDTH));
									dblTemp = FCConvert.ToDouble(Strings.Format(2.54 * dblTemp, "0.0000"));
									GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(dblTemp));
								}
								// X
							}
							else if (strTemp == "INCHES")
							{
								// from cent to inch
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA));
								if (dblTemp > 0)
								{
									dblTemp /= 2.54;
								}
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA));
								if (dblTemp > 0)
								{
									dblTemp /= 2.54;
								}
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA));
								if (dblTemp > 0)
								{
									dblTemp /= 2.54;
								}
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA));
								if (dblTemp > 0)
								{
									dblTemp /= 2.54;
								}
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA));
								if (dblTemp > 0)
								{
									dblTemp /= 2.54;
								}
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								dblTemp = Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA));
								if (dblTemp > 0)
								{
									dblTemp /= 2.54;
								}
								GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(dblTemp));
								for (X = 1; X <= GridFields.Rows - 1; X++)
								{
									dblTemp = Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLTOP));
									if (dblTemp > 0)
									{
										dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp / 2.54, "0.0000"));
									}
									GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
									dblTemp = Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLLEFT));
									if (dblTemp > 0)
									{
										dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp / 2.54, "0.0000"));
									}
									GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
									dblTemp = Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLHEIGHT));
									if (dblTemp > 0)
									{
										dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp / 2.54, "0.0000"));
									}
									GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(dblTemp));
									dblTemp = Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLWIDTH));
									if (dblTemp > 0)
									{
										dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp / 2.54, "0.0000"));
									}
									GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(dblTemp));
								}
								// X
							}
						}
						else
						{
							strTemp = Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA));
						}
						break;
					}
				default:
					{
						strTemp = Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA));
						break;
					}
			}
			//end switch
			if (strTemp == "CENTIMETERS")
			{
				dblTwipsPerUnit = 567;
			}
			else if (strTemp == "INCHES")
			{
				dblTwipsPerUnit = 1440;
			}
			else
			{
			}
			dblTemp = Conversion.Val(GridBillSize.EditText);
			lngTemp = FCConvert.ToInt32(dblTemp * dblTwipsPerUnit);
			lngPHeight = FCConvert.ToInt32(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA)) * dblTwipsPerUnit);
			lngPWidth = FCConvert.ToInt32(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA)) * dblTwipsPerUnit);
			lngLMargin = FCConvert.ToInt32(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA)) * dblTwipsPerUnit);
			lngRMargin = FCConvert.ToInt32(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA)) * dblTwipsPerUnit);
			lngTMargin = FCConvert.ToInt32(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA)) * dblTwipsPerUnit);
			lngBMargin = FCConvert.ToInt32(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA)) * dblTwipsPerUnit);
			switch (GridBillSize.GetFlexRowIndex(e.RowIndex))
			{
				case CNSTGRIDBILLSIZEROWBOTTOMMARGIN:
					{
						lngPageHeight = lngPHeight - lngTMargin - lngTemp;
						lngScreenPageHeight = FCConvert.ToInt32(lngPageHeight / dblPageRatio);
						break;
					}
				case CNSTGRIDBILLSIZEROWTOPMARGIN:
					{
						lngPageHeight = lngPHeight - lngTemp - lngBMargin;
						lngScreenPageHeight = FCConvert.ToInt32(lngPageHeight / dblPageRatio);
						break;
					}
				case CNSTGRIDBILLSIZEROWPAGEHEIGHT:
					{
						lngPageHeight = lngTemp - lngTMargin - lngBMargin;
						lngScreenPageHeight = FCConvert.ToInt32(lngPageHeight / dblPageRatio);
						break;
					}
				case CNSTGRIDBILLSIZEROWPAGEWIDTH:
					{
						lngPageWidth = lngTemp - lngLMargin - lngRMargin;
						lngScreenPageWidth = FCConvert.ToInt32(lngPageWidth / dblPageRatio);
						break;
					}
				case CNSTGRIDBILLSIZEROWLEFTMARGIN:
					{
						lngPageWidth = lngPWidth - lngTemp - lngRMargin;
						lngScreenPageWidth = FCConvert.ToInt32(lngPageWidth / dblPageRatio);
						break;
					}
				case CNSTGRIDBILLSIZEROWRIGHTMARGIN:
					{
						lngPageWidth = lngPWidth - lngLMargin - lngTemp;
						lngScreenPageWidth = FCConvert.ToInt32(lngPageWidth / dblPageRatio);
						break;
					}
			}
			//end switch
			ResizePage();
		}

		private void GridControlInfo_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// change dropdowns
			string strTemp = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			GridControlInfo.ComboList = "";
			switch (GridControlInfo.GetFlexRowIndex(e.RowIndex))
			{
				case CNSTGRIDCONTROLINFOROWTEXT:
					{
						if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
						{
							clsLoad.OpenRecordset("select * from custombills where ID <> " + FCConvert.ToString(lngCustomBillID) + " order by formatname", "twce0000.vb1");
							while (!clsLoad.EndOfFile())
							{
								strTemp += "#" + clsLoad.Get_Fields_Int32("ID") + ";" + clsLoad.Get_Fields_String("FormatName") + "|";
								clsLoad.MoveNext();
							}
							if (strTemp != string.Empty)
							{
								strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
							}
							GridControlInfo.ComboList = strTemp;
						}
						else
						{
							GridControlInfo.ComboList = "...";
						}
						break;
					}
				case CNSTGRIDCONTROLINFOROWTYPE:
					{
						GridControlInfo.ComboList = "...";
						break;
					}
				case CNSTGRIDCONTROLINFOROWALIGN:
					{
						GridControlInfo.ComboList = "Left|Right|Center";
						break;
					}
				case CNSTGRIDCONTROLINFOROWFONT:
					{
						GridControlInfo.ComboList = "Courier New|Tahoma";
						break;
					}
				case CNSTGRIDCONTROLINFOROWFONTSIZE:
					{
						GridControlInfo.ComboList = "8|9|10|12|14|16";
						break;
					}
				case CNSTGRIDCONTROLINFOROWFONTSTYLE:
					{
						GridControlInfo.ComboList = "Regular|Bold|Italic|Bold Italic";
						break;
					}
				default:
					{
						if (GridControlInfo.GetFlexRowIndex(e.RowIndex) >= CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS)
						{
							GridControlInfo.ComboList = FCConvert.ToString(GridControlInfo.RowData(GridControlInfo.GetFlexRowIndex(e.RowIndex)));
						}
						break;
					}
			}
			//end switch
		}

		private void ReshowimgControlImage(string strFileName)
		{
			double dblPicSizeRatio = 0;
			if (Strings.Trim(strFileName) != string.Empty)
			{
				imgControlImage.Image = FCUtils.LoadPicture(strFileName);
				imgControlImage.WidthOriginal = framControlInfo.WidthOriginal - imgControlImage.LeftOriginal - 100;
				// make 100 pixels less than border
				imgControlImage.HeightOriginal = framControlInfo.HeightOriginal - imgControlImage.TopOriginal - 45;
				// now make the image proportional, not stretched funny
				dblPicSizeRatio = imgControlImage.Image.Width / imgControlImage.Image.Height;
				if (imgControlImage.HeightOriginal * dblPicSizeRatio > imgControlImage.WidthOriginal)
				{
					// shape is the other way
					imgControlImage.HeightOriginal = FCConvert.ToInt32(imgControlImage.WidthOriginal / dblPicSizeRatio);
				}
				else
				{
					imgControlImage.WidthOriginal = FCConvert.ToInt32(imgControlImage.HeightOriginal * dblPicSizeRatio);
				}
			}
			else
			{
				imgControlImage.Image = null;
			}
		}

		private void GridControlInfo_CellButtonClick(object sender, System.EventArgs e)
		{
			string strTemp = "";
			int lngTemp = 0;
			clsDRWrapper clsLoad = new clsDRWrapper();
			//FC:FINAL:RPU:i654-Center framUserText
			framUserText.CenterToContainer(this.ClientArea);
			switch (GridControlInfo.Row)
			{
				case CNSTGRIDCONTROLINFOROWTEXT:
					{
						if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE)
						{
							// get image path and file name
							App.MainForm.CommonDialog1.Text = "Load Image";
							App.MainForm.CommonDialog1.FileName = "";
							App.MainForm.CommonDialog1.Filter = "Images (*.bmp,*.gif,*.jpg)|*.bmp;*.gif;*.jpg";
							// App.MainForm.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNFileMustExist+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
							App.MainForm.CommonDialog1.InitDir = FCFileSystem.Statics.UserDataFolder;
							App.MainForm.CommonDialog1.ShowOpen();
							strTemp = App.MainForm.CommonDialog1.FileName;
							if (strTemp != string.Empty)
							{
								GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA, strTemp);
							}
							ReshowimgControlImage(strTemp);
						}
						else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
						{
							// must select a predefined report
						}
						else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMRICHEDIT)
						{
							rtbUserText.Text = GridControlInfo.TextMatrix(GridControlInfo.Row, GridControlInfo.Col);
							// must size the control so text will wrap the same
							framUserText.Visible = true;
							framUserText.BringToFront();
							rtbUserText.Focus();
						}
						else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT)
						{
							rtbUserText.Text = GridControlInfo.TextMatrix(GridControlInfo.Row, GridControlInfo.Col);
							framUserText.Visible = true;
							framUserText.BringToFront();
							rtbUserText.Focus();
						}
						else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED)
						{
							rtbUserText.Text = GridControlInfo.TextMatrix(GridControlInfo.Row, GridControlInfo.Col);
							framUserText.Visible = true;
							framUserText.BringToFront();
							rtbUserText.Focus();
						}
						else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP)
						{
							rtbUserText.Text = GridControlInfo.TextMatrix(GridControlInfo.Row, GridControlInfo.Col);
							framUserText.Visible = true;
							framUserText.BringToFront();
							rtbUserText.Focus();
						}
						else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
						{
							lngTemp = frmChooseDynamicReport.InstancePtr.Init(strThisModule, modCustomBill.CNSTDYNAMICREPORTTYPECUSTOMBILL);
							if (lngTemp == 0)
							{
								// cancelled
								strTemp = GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA);
								if (Strings.Trim(strTemp) == string.Empty)
								{
									strTemp = "Dynamic Report";
								}
							}
							else if (lngTemp < 0)
							{
								// no reports
								MessageBox.Show("There are no dynamic documents to load");
								strTemp = "Dynamic Report";
								lngTemp = 0;
								GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA, strTemp);
								GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT, lngTemp);
							}
							else
							{
								strTemp = "Dynamic Report";
								clsLoad.OpenRecordset("select text from DYNAMICREPORTS where reporttype = " + FCConvert.ToString(modCustomBill.CNSTDYNAMICREPORTTYPECUSTOMBILL) + " and ID = " + FCConvert.ToString(lngTemp), "tw" + strThisModule + "0000.vb1");
								if (!clsLoad.EndOfFile())
								{
									// TODO: Field [text] not found!! (maybe it is an alias?)
									strTemp = FCConvert.ToString(clsLoad.Get_Fields("text"));
								}
								GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA, strTemp);
								GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT, lngTemp);
							}
							GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT, Conversion.Val(GridControlInfo.RowData(CNSTGRIDCONTROLINFOROWTEXT)));
						}
						break;
					}
				case CNSTGRIDCONTROLINFOROWTYPE:
					{
						GridControlTypes.Visible = true;
						//framControlInfo.HeightOriginal = (fcFrame1.TopOriginal + framPage.HeightOriginal) - framControlInfo.TopOriginal;
						// framControlInfo.Height = (framPage.Top) - framControlInfo.Top
						GridControlTypes.BringToFront();
						break;
					}
			}
			//end switch
		}

		private void GridControlInfo_MouseMoveEvent(object sender, MouseEventArgs e)
		{
			int lngX;
			int lngY;
			lngX = GridControlInfo.MouseCol;
			lngY = GridControlInfo.MouseRow;
			if (lngY < 0)
			{
				ToolTip1.SetToolTip(GridControlInfo, "");
				return;
			}
			ToolTip1.SetToolTip(GridControlInfo, GridControlInfo.TextMatrix(lngY, CNSTGRIDCONTROLINFOCOLTOOLTIP));
		}

		private void GridControlInfo_RowColChange(object sender, System.EventArgs e)
		{
			int lngTemp;
			GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			if (GridControlInfo.Col == CNSTGRIDCONTROLINFOCOLDATA)
			{
				switch (GridControlInfo.Row)
				{
					case CNSTGRIDCONTROLINFOROWTEXT:
						{
							lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA))));
							if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE)
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMRICHEDIT)
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT)
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED)
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP)
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else if (lngTemp == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
							{
							}
							else
							{
								GridControlInfo.Editable = FCGrid.EditableSettings.flexEDNone;
							}
							break;
						}
				}
				//end switch
			}
		}

		private void GridControlTypes_ClickEvent(object sender, System.EventArgs e)
		{
			int lngRow;
			// vbPorter upgrade warning: lngCode As int	OnWrite(string)
			int lngCode;
			//FC:FINAL:AM:#i650 - don't hide the grid if the expand/collapse button was clicked
			if (GridControlTypes.Col == 2)
			{
				return;
			}
			lngRow = GridControlTypes.Row;
			if (lngRow < 1)
				return;
			lngCode = FCConvert.ToInt32(GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLCODE));
			if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) != lngCode)
			{
				// clear the extra parameters since they belong to a different code
				GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLEXTRAPARAMETERS, "");
			}
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA, FCConvert.ToString(lngCode));
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDATA, GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLNAME));
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWALIGN, CNSTGRIDCONTROLINFOCOLDATA, GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN));
			if (lngCode == modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE)
			{
				imgControlImage.Visible = true;
				if (Strings.Trim(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA)) != string.Empty)
				{
					if (File.Exists(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA)))
					{
						imgControlImage.Image = FCUtils.LoadPicture(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA));
					}
					else
					{
						imgControlImage.Visible = false;
					}
				}
				else
				{
					imgControlImage.Image = null;
				}
			}
			else
			{
				imgControlImage.Image = null;
				imgControlImage.Visible = false;
			}
			ResizeGridControlInfo();
			//framControlInfo.HeightOriginal = fcFrame1.TopOriginal - framControlInfo.TopOriginal;
			GridControlTypes.Visible = false;
		}

		private void GridControlTypes_MouseMoveEvent(object sender, MouseEventArgs e)
		{
			int lngX;
			int lngY;
			lngX = GridControlTypes.MouseCol;
			lngY = GridControlTypes.MouseRow;
			if (lngY < 0)
				return;
			ToolTip1.SetToolTip(GridControlTypes, GridControlTypes.TextMatrix(lngY, CNSTGRIDCONTROLTYPESCOLTOOLTIP));
		}

		private void GridFields_CellButtonClick(object sender, System.EventArgs e)
		{
			string strTemp = "";
			int lngTemp = 0;
			framPage.Enabled = false;
			mnuProcess.Enabled = false;
			boolInHighlightMode = false;
			UnhighlightAllControls();
			HighlightControl(FCConvert.ToInt32(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM)));
			switch (GridFields.Col)
			{
				case CNSTGRIDFIELDSCOLCONTROL:
					{
						GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWALIGN, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLALIGNMENT));
						GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONT, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONT));
						GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSIZE, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSIZE));
						switch (FCConvert.ToInt32(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFONTSTYLE)))
						{
							case CNSTCUSTOMBILLFONTSTYLEBOLD:
								{
									strTemp = "Bold";
									break;
								}
							case CNSTCUSTOMBILLFONTSTYLEBOLDITALIC:
								{
									strTemp = "Bold Italic";
									break;
								}
							case CNSTCUSTOMBILLFONTSTYLEITALIC:
								{
									strTemp = "Italic";
									break;
								}
							default:
								{
									// regular
									strTemp = "Regular";
									break;
								}
						}
						//end switch
						GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSTYLE, CNSTGRIDCONTROLINFOCOLDATA, strTemp);
						GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLTEXT));
						GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID));
						lngTemp = GridControlTypes.FindRow(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID), -1, CNSTGRIDCONTROLTYPESCOLCODE);
						if (lngTemp >= 0)
						{
							GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDATA, GridControlTypes.TextMatrix(lngTemp, CNSTGRIDCONTROLTYPESCOLNAME));
						}
						else
						{
							// shouldn't be able to happen
							GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDATA, "");
						}
						framControlInfo.Visible = true;
						ResizeGridControlInfo();
						if (FCConvert.ToDouble(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE)
						{
							if (File.Exists(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLTEXT)))
							{
								ReshowimgControlImage(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLTEXT));
							}
							else
							{
								imgControlImage.Image = null;
							}
						}
						framControlInfo.BringToFront();
						break;
					}
			}
			//end switch
		}

		private void GridFields_ClickEvent(object sender, System.EventArgs e)
		{
			if (GridFields.Row < 1)
				return;
			boolInHighlightMode = false;
			UnhighlightAllControls();
			HighlightControl(FCConvert.ToInt32(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM)));
			GridHighlighted.Rows = 1;
			GridHighlighted.TextMatrix(0, 0, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM));
			GridHighlighted.TextMatrix(0, 1, GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID));
		}

		private void GridFields_KeyDownEvent(object sender, Wisej.Web.KeyEventArgs e)
		{
			// vbPorter upgrade warning: lngFNum As int	OnWrite(string, int)
			int lngFNum = 0;
			// vbPorter upgrade warning: lngPosition As int	OnWrite(double, int)
			int lngPosition = 0;
			double dblRatio = 0;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp = 0;
			int intloop;
			// vbPorter upgrade warning: lngWidth As int	OnWrite(double, int)
			int lngWidth = 0;
			// vbPorter upgrade warning: lngHeight As int	OnWriteFCConvert.ToDouble(
			int lngHeight = 0;
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp = 0;
			int X;
			int Y;
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Insert:
					{
						KeyCode = 0;
						if (boolInHighlightMode)
							return;
						mnuAddField_Click();
						break;
					}
				case Keys.Delete:
					{
						KeyCode = 0;
						if (!boolInHighlightMode)
						{
							mnuDeleteField_Click();
						}
						else
						{
							if (MessageBox.Show("Are you sure you want to delete all fields highlighted?", "Delete All Selected?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
							{
								for (X = GridHighlighted.Rows - 1; X >= 0; X--)
								{
									lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0));
									CustomLabel.Unload(lngFNum);
									// now get rid of the row
									for (Y = 1; Y <= GridFields.Rows - 1; Y++)
									{
										if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
										{
											if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLAUTOID)) > 0)
											{
												// if its < 0 then it was never saved, so don't have to worry about it
												GridDeleted.AddItem(FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLAUTOID))));
											}
											GridFields.RemoveItem(Y);
											break;
										}
									}
									// Y
									GridHighlighted.RemoveItem(X);
								}
								// X
								boolInHighlightMode = false;
								ShowGroupBox_2(false);
							}
						}
						break;
					}
				case Keys.Left:
					{
						// move control left one grid unit (character width?)
						if (e.Control)
						{
							KeyCode = 0;
							if (!boolInHighlightMode)
							{
								if (GridFields.Row >= 1)
								{
									lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
									if (lngFNum > 0)
									{
										lngPosition = FCConvert.ToInt32(CustomLabel[FCConvert.ToInt16(lngFNum)].Left - (intCharWid * dblSizeRatio / dblPageRatio));
										if (lngPosition < 0)
											lngPosition = 0;
										CustomLabel[FCConvert.ToInt16(lngFNum)].Left = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
										GridFields.RowData(GridFields.Row, true);
									}
								}
							}
							else
							{
								int lngLeftMove = 0;
								lngPosition = FCConvert.ToInt32(ctlGroupControl.Left - (intCharWid * dblSizeRatio / dblPageRatio));
								if (lngPosition < 0)
									lngPosition = 0;
								lngLeftMove = ctlGroupControl.Left - lngPosition;
								if (lngLeftMove > 0)
								{
									for (X = 0; X <= GridHighlighted.Rows - 1; X++)
									{
										lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0));
										lngPosition = CustomLabel[FCConvert.ToInt16(lngFNum)].Left - lngLeftMove;
										if (lngPosition <= 0)
											lngPosition = 0;
										CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0))].Left = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										for (Y = 1; Y <= GridFields.Rows - 1; Y++)
										{
											if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
											{
												GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
												GridFields.RowData(Y, true);
												break;
											}
										}
										// Y
									}
									// X
								}
								ShowGroupBox();
							}
						}
						else if (e.Shift)
						{
							// make it smaller by a character width
							KeyCode = 0;
							if (boolInHighlightMode)
							{
								// can't resize more than one at a time
								return;
							}
							if (GridFields.Row >= 1)
							{
								if (Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
								{
									// can't change the width of a horizontal line
									return;
								}
								lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
								if (lngFNum > 0)
								{
									lngWidth = FCConvert.ToInt32(CustomLabel[FCConvert.ToInt16(lngFNum)].Width - (intCharWid * dblSizeRatio / dblPageRatio));
									if (lngWidth < intCharWid)
										lngWidth = intCharWid;
									CustomLabel[FCConvert.ToInt16(lngFNum)].Width = lngWidth;
									dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
									dblTemp = FCConvert.ToDouble(Strings.Format(lngWidth * dblRatio, "0.0000"));
									GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(dblTemp));
									GridFields.RowData(GridFields.Row, true);
								}
							}
						}
						break;
					}
				case Keys.Right:
					{
						// move control right one grid unit (Character width?)
						if (e.Control)
						{
							KeyCode = 0;
							if (!boolInHighlightMode)
							{
								if (GridFields.Row >= 1)
								{
									lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
									if (lngFNum > 0)
									{
										lngPosition = FCConvert.ToInt32(CustomLabel[FCConvert.ToInt16(lngFNum)].Left + (intCharWid * dblSizeRatio / dblPageRatio));
										if (lngPosition + CustomLabel[FCConvert.ToInt16(lngFNum)].Width > Picture1.Width)
											lngPosition = Picture1.Width - CustomLabel[FCConvert.ToInt16(lngFNum)].Width;
										CustomLabel[FCConvert.ToInt16(lngFNum)].Left = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
										GridFields.RowData(GridFields.Row, true);
									}
								}
							}
							else
							{
								int lngRightMove = 0;
								lngPosition = FCConvert.ToInt32(ctlGroupControl.Left + (intCharWid * dblSizeRatio / dblPageRatio));
								if (lngPosition + ctlGroupControl.Width > Picture1.Width)
									lngPosition = Picture1.Width - ctlGroupControl.Width;
								lngRightMove = lngPosition - ctlGroupControl.Left;
								if (lngRightMove > 0)
								{
									for (X = 0; X <= GridHighlighted.Rows - 1; X++)
									{
										lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0));
										lngPosition = CustomLabel[FCConvert.ToInt16(lngFNum)].Left + lngRightMove;
										if (lngPosition + CustomLabel[FCConvert.ToInt16(lngFNum)].Width > Picture1.Width)
											lngPosition = Picture1.Width - CustomLabel[FCConvert.ToInt16(lngFNum)].Width;
										CustomLabel[FCConvert.ToInt16(lngFNum)].Left = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										for (Y = 1; Y <= GridFields.Rows - 1; Y++)
										{
											if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
											{
												GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
												GridFields.RowData(Y, true);
												break;
											}
										}
										// Y
									}
									// X
								}
								ShowGroupBox();
							}
						}
						else if (e.Shift)
						{
							// make Wider by a character
							KeyCode = 0;
							if (boolInHighlightMode)
								return;
							if (GridFields.Row >= 1)
							{
								if (Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
								{
									// can't change the width of a horizontal line
									return;
								}
								lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
								if (lngFNum > 0)
								{
									lngWidth = FCConvert.ToInt32(CustomLabel[FCConvert.ToInt16(lngFNum)].Width + (intCharWid * dblSizeRatio / dblPageRatio));
									if (CustomLabel[FCConvert.ToInt16(lngFNum)].Left + lngWidth <= Picture1.Width)
									{
										// only make wider if there is room
										CustomLabel[FCConvert.ToInt16(lngFNum)].Width = lngWidth;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngWidth * dblRatio, "0.0000"));
										GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(dblTemp));
										GridFields.RowData(GridFields.Row, true);
									}
								}
							}
						}
						break;
					}
				case Keys.Up:
					{
						// move control up one grid unit (Text Line height?)
						if (e.Control)
						{
							KeyCode = 0;
							if (!boolInHighlightMode)
							{
								if (GridFields.Row >= 1)
								{
									lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
									if (lngFNum > 0)
									{
										lngPosition = FCConvert.ToInt32(CustomLabel[FCConvert.ToInt16(lngFNum)].Top - (intLineHite * dblSizeRatio / dblPageRatio));
										if (lngPosition < 0)
											lngPosition = 0;
										CustomLabel[FCConvert.ToInt16(lngFNum)].Top = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
										GridFields.RowData(GridFields.Row, true);
									}
								}
							}
							else
							{
								int lngUpMove = 0;
								lngPosition = FCConvert.ToInt32(ctlGroupControl.Top - (intLineHite * dblSizeRatio / dblPageRatio));
								if (lngPosition < 0)
									lngPosition = 0;
								lngUpMove = ctlGroupControl.Top - lngPosition;
								if (lngUpMove > 0)
								{
									for (X = 0; X <= GridHighlighted.Rows - 1; X++)
									{
										lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0));
										lngPosition = CustomLabel[FCConvert.ToInt16(lngFNum)].Top - lngUpMove;
										if (lngPosition < 0)
											lngPosition = 0;
										CustomLabel[FCConvert.ToInt16(lngFNum)].Top = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										for (Y = 1; Y <= GridFields.Rows - 1; Y++)
										{
											if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
											{
												GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
												GridFields.RowData(Y, true);
												break;
											}
										}
										// Y
									}
									// X
								}
								ShowGroupBox();
							}
						}
						else if (e.Shift)
						{
							KeyCode = 0;
							if (boolInHighlightMode)
								return;
							if (GridFields.Row >= 1)
							{
								if (Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
								{
									// can't change the width of a horizontal line
									return;
								}
								lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
								if (lngFNum > 0)
								{
									lngHeight = FCConvert.ToInt32(CustomLabel[FCConvert.ToInt16(lngFNum)].Height - (intLineHite * dblSizeRatio / dblPageRatio));
									lngTemp = FCConvert.ToInt32(intLineHite * dblSizeRatio / dblPageRatio);
									// If lngHeight < 240 Then lngHeight = 240
									if (lngHeight >= lngTemp)
									{
										// only make a change if it is at least 240 high
										CustomLabel[FCConvert.ToInt16(lngFNum)].Height = lngHeight;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngHeight * dblRatio, "0.0000"));
										GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(dblTemp));
										GridFields.RowData(GridFields.Row, true);
									}
								}
							}
						}
						break;
					}
				case Keys.Down:
					{
						// move control down one grid unit (Text Line height?)
						if (e.Control)
						{
							KeyCode = 0;
							if (!boolInHighlightMode)
							{
								if (GridFields.Row >= 1)
								{
									lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
									if (lngFNum > 0)
									{
										lngPosition = FCConvert.ToInt32(CustomLabel[FCConvert.ToInt16(lngFNum)].Top + (intLineHite * dblSizeRatio / dblPageRatio));
										if (lngPosition + CustomLabel[FCConvert.ToInt16(lngFNum)].Height > Picture1.Height)
											lngPosition = Picture1.Height - CustomLabel[FCConvert.ToInt16(lngFNum)].Height;
										CustomLabel[FCConvert.ToInt16(lngFNum)].Top = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
										GridFields.RowData(GridFields.Row, true);
									}
								}
							}
							else
							{
								int lngDownMove = 0;
								lngPosition = FCConvert.ToInt32(ctlGroupControl.Top + (intLineHite * dblSizeRatio / dblPageRatio));
								if (lngPosition + ctlGroupControl.Height > Picture1.Height)
									lngPosition = Picture1.Height - ctlGroupControl.Height;
								lngDownMove = lngPosition - ctlGroupControl.Top;
								if (lngDownMove > 0)
								{
									for (X = 0; X <= GridHighlighted.Rows - 1; X++)
									{
										lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0));
										lngPosition = CustomLabel[FCConvert.ToInt16(lngFNum)].Top + lngDownMove;
										if (lngPosition + CustomLabel[FCConvert.ToInt16(lngFNum)].Height > Picture1.Height)
											lngPosition = Picture1.Height - CustomLabel[FCConvert.ToInt16(lngFNum)].Height;
										CustomLabel[FCConvert.ToInt16(lngFNum)].Top = lngPosition;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
										for (Y = 1; Y <= GridFields.Rows - 1; Y++)
										{
											if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
											{
												GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
												GridFields.RowData(Y, true);
												break;
											}
										}
										// Y
									}
									// X
								}
								ShowGroupBox();
							}
						}
						else if (e.Shift)
						{
							KeyCode = 0;
							if (boolInHighlightMode)
								return;
							if (GridFields.Row >= 1)
							{
								if (Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
								{
									// can't change the width of a horizontal line
									return;
								}
								lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLFIELDNUM))));
								if (lngFNum > 0)
								{
									lngHeight = FCConvert.ToInt32(CustomLabel[FCConvert.ToInt16(lngFNum)].Height + (intLineHite * dblSizeRatio / dblPageRatio));
									if (lngHeight + CustomLabel[FCConvert.ToInt16(lngFNum)].Top <= Picture1.Height)
									{
										CustomLabel[FCConvert.ToInt16(lngFNum)].Height = lngHeight;
										dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
										dblTemp = FCConvert.ToDouble(Strings.Format(lngHeight * dblRatio, "0.0000"));
										GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(dblTemp));
										GridFields.RowData(GridFields.Row, true);
									}
								}
							}
						}
						break;
					}
			}
			//end switch
		}

		private void GridFields_KeyUpEvent(object sender, Wisej.Web.KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.ControlKey:
					{
						// UnhighlightAllControls (True)
						break;
					}
			}
			//end switch
		}

		private void GridFields_RowColChange(object sender, System.EventArgs e)
		{
			// With GridFields
			// Select Case .Col
			// Case CNSTGRIDFIELDSCOLTEXT
			// 
			// If Val(.TextMatrix(.Row, CNSTGRIDFIELDSCOLFIELDID)) = CNSTCUSTOMBILLCUSTOMTEXT Or Val(.TextMatrix(.Row, CNSTGRIDFIELDSCOLFIELDID)) = CNSTCUSTOMBILLCUSTOMRICHEDIT Then
			// .Editable = flexEDKbdMouse
			// Else
			// .Editable = flexEDNone
			// End If
			// Case Else
			// .Editable = flexEDKbdMouse
			// End Select
			// End With
		}

		private void GridFields_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:AM:#i650 - check for edit mode
			if (GridFields.IsCurrentCellInEditMode)
			{
				int Row = GridFields.GetFlexRowIndex(e.RowIndex);
				int Col = GridFields.GetFlexColIndex(e.ColumnIndex);
				// mark as changed
				GridFields.RowData(Row, true);
				GridFields.TextMatrix(Row, Col, GridFields.EditText);
				switch (Col)
				{
					case CNSTGRIDFIELDSCOLWIDTH:
						{
							if (Conversion.Val(GridFields.TextMatrix(Row, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
							{
								GridFields.TextMatrix(Row, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(0));
								GridFields.EditText = FCConvert.ToString(0);
								// Cancel = True
							}
							break;
						}
					case CNSTGRIDFIELDSCOLHEIGHT:
						{
							if (Conversion.Val(GridFields.TextMatrix(Row, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
							{
								GridFields.TextMatrix(Row, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(0));
								GridFields.EditText = FCConvert.ToString(0);
								// Cancel = True
							}
							break;
						}
					default:
						{
							break;
						}
				}
				//end switch
				ReDrawField(Row);
			}
		}

		private void GridLoadDelete_DblClick(object sender, System.EventArgs e)
		{
			// loads or deletes the format in the current row
			int lngRow;
			int lngID = 0;
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				if (Conversion.Val(GridLoadDelete.Tag) == CNSTGRIDLOADDELETEEXPORT)
				{
					return;
				}
				lngRow = GridLoadDelete.MouseRow;
				if (Conversion.Val(GridLoadDelete.Tag) == CNSTGRIDLOADDELETELOAD)
				{
					// loading
					LoadCustomBill_8(FCConvert.ToInt32(Conversion.Val(GridLoadDelete.TextMatrix(lngRow, CNSTGRIDLOADDELETECOLAUTOID))), "tw" + strThisModule + "0000.vb1");
					GridHighlighted.Rows = 0;
					framLoadDelete.Visible = false;
				}
				else
				{
					// deleting.  Check to see if it is the one we are currently on
					lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridLoadDelete.TextMatrix(lngRow, CNSTGRIDLOADDELETECOLAUTOID))));
					if (lngID == CurrentLoadedFormat.lngBillID)
					{
						// will have to save as new since it will be deleted now
						// don't clear the screen in case they made a mistake, so they can save it again
						CurrentLoadedFormat.lngBillID = 0;
					}
					modGlobalFunctions.AddCYAEntry_6(strThisModule, "Deleted custom bill format", "Format " + GridLoadDelete.TextMatrix(lngRow, CNSTGRIDLOADDELETECOLAUTOID), GridLoadDelete.TextMatrix(lngRow, CNSTGRIDLOADDELETECOLNAME));
					clsSave.Execute("delete from custombillfields where formatid = " + FCConvert.ToString(lngID), "tw" + strThisModule + "0000.vb1");
					clsSave.Execute("delete from custombills where ID = " + FCConvert.ToString(lngID), "tw" + strThisModule + "0000.vb1");
					MessageBox.Show("Format deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
					framLoadDelete.Visible = false;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GridLoadDelete_DblClick", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GridLoadDelete_RowColChange(object sender, System.EventArgs e)
		{
			switch (GridLoadDelete.Col)
			{
				case CNSTGRIDLOADDELETECOLCHECK:
					{
						GridLoadDelete.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						GridLoadDelete.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void HScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp;
			double dblTemp;
			//dblTemp = HScroll1.Value / 100.0;
			//lngTemp = FCConvert.ToInt32((Picture1.WidthOriginal - framPage.WidthOriginal + (lngOffsetX * 2)) * dblTemp);
			//Picture1.LeftOriginal = -lngTemp + lngOffsetX;
		}

		public void HScroll1_Change()
		{
			//HScroll1_ValueChanged(HScroll1, new System.EventArgs());
		}

		private void HScroll1_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Up:
					{
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Down:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
		}

		private void HScroll1_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp;
			double dblTemp;
			//dblTemp = HScroll1.Value / 100.0;
			//lngTemp = FCConvert.ToInt32((Picture1.WidthOriginal - framPage.WidthOriginal + (lngOffsetX * 2)) * dblTemp);
			//Picture1.LeftOriginal = -lngTemp + (lngOffsetX);
		}

		private int AddANewfield(ref int lngRow)
		{
			int AddANewfield = 0;
			// Accepts the row in gridfields to get its info from
			// Returns the control number so we can store that in gridfields
			int X;
			int lngFNum;
			try
			{
				// On Error GoTo ErrorHandler
				AddANewfield = -1;
				// get the next control number
				lngFNum = 0;
				for (X = 1; X <= GridFields.Rows - 1; X++)
				{
					if (Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM)) > lngFNum)
					{
						lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM))));
					}
				}
				// X
				lngFNum += 1;
				CustomLabel.Load(FCConvert.ToInt16(lngFNum));
				//FC:FINAL:BBE:#320 - set Capitalize, need to show the same case
				CustomLabel[FCConvert.ToInt16(lngFNum)].Capitalize = false;
				CustomLabel[FCConvert.ToInt16(lngFNum)].Tag = lngFNum;
				CustomLabel[FCConvert.ToInt16(lngFNum)].LeftOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				CustomLabel[FCConvert.ToInt16(lngFNum)].HeightOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				CustomLabel[FCConvert.ToInt16(lngFNum)].WidthOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				CustomLabel[FCConvert.ToInt16(lngFNum)].TopOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				CustomLabel[FCConvert.ToInt16(lngFNum)].Visible = true;
				CustomLabel[lngFNum].UseMnemonic = false;
				CustomLabel[FCConvert.ToInt16(lngFNum)].BringToFront();
				CustomLabel[FCConvert.ToInt16(lngFNum)].BackColor = Color.White;
				CustomLabel[FCConvert.ToInt16(lngFNum)].ForeColor = Color.Black;
				CustomLabel[lngFNum].Appearance = 0;
				CustomLabel[FCConvert.ToInt16(lngFNum)].BorderStyle = 1;
				CustomLabel[FCConvert.ToInt16(lngFNum)].BackStyle = 0;
				CustomLabel[FCConvert.ToInt16(lngFNum)].FontName = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONT);
				CustomLabel[FCConvert.ToInt16(lngFNum)].FontSize = FCConvert.ToSingle(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSIZE)) * dblSizeRatio / dblPageRatio);
				CustomLabel[lngFNum].Alignment = CNSTCUSTOMBILLALIGNLEFT;
                CustomLabel[lngFNum].AllowDrag = true;
                CustomLabel[lngFNum].AllowDrop = true;
                CustomLabel[lngFNum].Enabled = true;
                CustomLabel[lngFNum].MouseDown += CustomLabel_MouseDown;
                CustomLabel[lngFNum].MouseUp += CustomLabel_0_MouseUp;
                CustomLabel[lngFNum].Movable = true;
                GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDNUM, FCConvert.ToString(lngFNum));
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLAUTOID, FCConvert.ToString(0));
				GridFields.RowData(lngRow, true);
				ReDrawField(lngRow);
				AddANewfield = lngFNum;
				return AddANewfield;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In AddANewField", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddANewfield;
		}

		private void mnuAddField_Click(object sender, System.EventArgs e)
		{
			// add a new row,default it to something and make them choose a type so you don't ever have
			// bad data
			int lngRow;
			int lngFNum;
			int X;
			string strTemp = "";
			int lngTemp;
			mnuProcess.Enabled = false;
			framPage.Enabled = false;
			UnhighlightAllControls();
			boolInHighlightMode = false;
			lngFNum = 0;
			for (X = 1; X <= GridFields.Rows - 1; X++)
			{
				if (Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM)) > lngFNum)
				{
					lngFNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM))));
				}
			}
			// X
			lngFNum += 1;
			// create a row
			GridFields.Rows += 1;
			lngRow = GridFields.Rows - 1;
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDNUM, FCConvert.ToString(lngFNum));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLAUTOID, FCConvert.ToString(0));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLALIGNMENT, "Left");
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLCONTROL, "Label");
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION, "Label");
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(0));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(0));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT, "Label");
			if (Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA)) == "CENTIMETERS")
			{
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(2.54));
				// 1 inch
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(0.4233));
				// 1 line high
			}
			else if (Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA)) == "INCHES")
			{
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(1));
				// 1 inch
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(0.1667));
				// 1 line high
			}
			else
			{
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(1));
				// 1 inch
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(0.1667));
				// 1 line high
			}
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONT, "Tahoma");
				// .TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSIZE) = "10"
				GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSTYLE, FCConvert.ToString(CNSTCUSTOMBILLFONTSTYLEREGULAR));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSIZE, GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE, CNSTGRIDBILLSIZECOLDATA));
			GridFields.Row = lngRow;
			GridFields.Col = CNSTGRIDFIELDSCOLDESCRIPTION;
			// create a control
			// Call frmCustomBill.Controls.Add("VB.Label", "CustomLabel" & lngFnum, Picture1)
			CustomLabel.Load(FCConvert.ToInt16(lngFNum));
			//FC:FINAL:BBE:#320 - set Capitalize, need to show the same case
			CustomLabel[FCConvert.ToInt16(lngFNum)].Capitalize = false;
			// With frmCustomBill.Controls("CustomLabel" & lngFnum)
			CustomLabel[FCConvert.ToInt16(lngFNum)].Tag = lngFNum;
			CustomLabel[FCConvert.ToInt16(lngFNum)].LeftOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			CustomLabel[FCConvert.ToInt16(lngFNum)].HeightOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			CustomLabel[FCConvert.ToInt16(lngFNum)].WidthOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			CustomLabel[FCConvert.ToInt16(lngFNum)].TopOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			CustomLabel[FCConvert.ToInt16(lngFNum)].Text = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION);
			CustomLabel[FCConvert.ToInt16(lngFNum)].Visible = true;
			CustomLabel[lngFNum].UseMnemonic = false;
			CustomLabel[FCConvert.ToInt16(lngFNum)].BringToFront();
			CustomLabel[FCConvert.ToInt16(lngFNum)].BackColor = Color.White;
			CustomLabel[FCConvert.ToInt16(lngFNum)].ForeColor = Color.Black;
			CustomLabel[lngFNum].Appearance = 0;
			CustomLabel[FCConvert.ToInt16(lngFNum)].BorderStyle = 1;
			CustomLabel[FCConvert.ToInt16(lngFNum)].BackStyle = 0;
			CustomLabel[FCConvert.ToInt16(lngFNum)].FontName = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONT);
			CustomLabel[FCConvert.ToInt16(lngFNum)].FontSize = FCConvert.ToSingle(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSIZE)) * dblSizeRatio / dblPageRatio);
			CustomLabel[lngFNum].Alignment = CNSTCUSTOMBILLALIGNLEFT;
			CustomLabel[lngFNum].AllowDrag = true;
			CustomLabel[lngFNum].AllowDrop = true;
			CustomLabel[lngFNum].Enabled = true;
			//CustomLabel[lngFNum].Click += CustomLabel_Click;
			CustomLabel[lngFNum].MouseDown += CustomLabel_MouseDown;
			//CustomLabel[lngFNum].DragDrop += CustomLabel_DragDrop;
			CustomLabel[lngFNum].MouseUp += CustomLabel_0_MouseUp;
			CustomLabel[lngFNum].Movable = true;
			GridFields.TopRow = GridFields.Rows - 1;
			GridFields.Row = GridFields.Rows - 1;
			// have them choose the type
			cmdOKControlInfo.Visible = false;
			cmdOKControlInfoNew.Visible = true;
			//FC:FINAL:BBE:#321 - bring the visible button to the front, otherwise the click event will not be raised.
			cmdOKControlInfoNew.BringToFront();
			// do this the same as if you click on gridfields to edit the information
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWALIGN, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLALIGNMENT));
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONT, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONT));
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSIZE, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSIZE));
			switch (FCConvert.ToInt32(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSTYLE)))
			{
				case CNSTCUSTOMBILLFONTSTYLEBOLD:
					{
						strTemp = "Bold";
						break;
					}
				case CNSTCUSTOMBILLFONTSTYLEBOLDITALIC:
					{
						strTemp = "Bold Italic";
						break;
					}
				case CNSTCUSTOMBILLFONTSTYLEITALIC:
					{
						strTemp = "Italic";
						break;
					}
				default:
					{
						// regular
						strTemp = "Regular";
						break;
					}
			}
			//end switch
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSTYLE, CNSTGRIDCONTROLINFOCOLDATA, strTemp);
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT));
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA, GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID));
			lngTemp = GridControlTypes.FindRow(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID), -1, CNSTGRIDCONTROLTYPESCOLCODE);
			if (lngTemp >= 0)
			{
				GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDATA, GridControlTypes.TextMatrix(lngTemp, CNSTGRIDCONTROLTYPESCOLNAME));
			}
			else
			{
				// shouldn't be able to happen
				GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDATA, "");
			}
			framControlInfo.Visible = true;
			ResizeGridControlInfo();
			// If GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID) = CNSTCUSTOMBILLCUSTOMIMAGE Then
			// If File.Exists(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT)) Then
			// ReshowimgControlImage (GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT))
			// Else
			// Set imgControlImage.Picture = Nothing
			// End If
			// End If
			GridHighlighted.Rows = 1;
			GridHighlighted.TextMatrix(0, 0, FCConvert.ToString(lngFNum));
			GridHighlighted.TextMatrix(0, 1, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT));
			framControlInfo.BringToFront();
		}

		public void mnuAddField_Click()
		{
			mnuAddField_Click(mnuAddField, new System.EventArgs());
		}

		private void mnuAlignBottoms_Click(object sender, System.EventArgs e)
		{
			int lngB;
			int X;
			int Y;
			// vbPorter upgrade warning: lngFNum As int	OnWrite(string)
			int lngFNum;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp = 0;
			double dblRatio = 0;
			int lngPosition = 0;
			lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0));
			lngB = CustomLabel[FCConvert.ToInt16(lngFNum)].Top + CustomLabel[FCConvert.ToInt16(lngFNum)].Height;
			for (X = 0; X <= GridHighlighted.Rows - 1; X++)
			{
				lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0));
				CustomLabel[FCConvert.ToInt16(lngFNum)].Top = lngB - CustomLabel[FCConvert.ToInt16(lngFNum)].Height;
				lngPosition = CustomLabel[FCConvert.ToInt16(lngFNum)].Top;
				dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
				dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
				for (Y = 1; Y <= GridFields.Rows - 1; Y++)
				{
					if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
					{
						GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
						GridFields.RowData(Y, true);
						break;
					}
				}
				// Y
			}
			// X
		}

		private void mnuAlignLefts_Click(object sender, System.EventArgs e)
		{
			// align lefts with the left of the last in the list
			int lngL;
			int X;
			int Y;
			// vbPorter upgrade warning: lngFNum As int	OnWrite(string)
			int lngFNum = 0;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp;
			double dblRatio;
			lngL = CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0))].Left;
			dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
			dblTemp = FCConvert.ToDouble(Strings.Format(lngL * dblRatio, "0.0000"));
			for (X = 0; X <= GridHighlighted.Rows - 1; X++)
			{
				lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0));
				CustomLabel[FCConvert.ToInt16(lngFNum)].Left = lngL;
				for (Y = 1; Y <= GridFields.Rows - 1; Y++)
				{
					if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
					{
						GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
						GridFields.RowData(Y, true);
						break;
					}
				}
				// Y
			}
			// X
		}

		private void mnuAlignRights_Click(object sender, System.EventArgs e)
		{
			// align rights with the right of the last in the list
			int lngR;
			int X;
			int Y;
			// vbPorter upgrade warning: lngFNum As int	OnWrite(string)
			int lngFNum;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp = 0;
			double dblRatio = 0;
			int lngPosition = 0;
			lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0));
			lngR = CustomLabel[FCConvert.ToInt16(lngFNum)].Left + CustomLabel[FCConvert.ToInt16(lngFNum)].Width;
			for (X = 0; X <= GridHighlighted.Rows - 1; X++)
			{
				lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0));
				CustomLabel[FCConvert.ToInt16(lngFNum)].Left = lngR - CustomLabel[FCConvert.ToInt16(lngFNum)].Width;
				lngPosition = CustomLabel[FCConvert.ToInt16(lngFNum)].Left;
				dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
				dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
				for (Y = 1; Y <= GridFields.Rows - 1; Y++)
				{
					if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
					{
						GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
						GridFields.RowData(Y, true);
						break;
					}
				}
				// Y
			}
			// X
		}

		private void mnuAlignTops_Click(object sender, System.EventArgs e)
		{
			int lngT;
			int X;
			int Y;
			// vbPorter upgrade warning: lngFNum As int	OnWrite(string)
			int lngFNum;
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp;
			double dblRatio;
			int lngPosition;
			lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0));
			lngT = CustomLabel[FCConvert.ToInt16(lngFNum)].Top;
			lngPosition = CustomLabel[FCConvert.ToInt16(lngFNum)].Top;
			dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
			dblTemp = FCConvert.ToDouble(Strings.Format(lngPosition * dblRatio, "0.0000"));
			for (X = 0; X <= GridHighlighted.Rows - 1; X++)
			{
				lngFNum = FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0));
				CustomLabel[FCConvert.ToInt16(lngFNum)].Top = lngT;
				for (Y = 1; Y <= GridFields.Rows - 1; Y++)
				{
					if (Conversion.Val(GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM)) == lngFNum)
					{
						GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
						GridFields.RowData(Y, true);
						break;
					}
				}
				// Y
			}
			// X
		}

		private void mnuBringToFront_Click(object sender, System.EventArgs e)
		{
			int X;
			for (X = 0; X <= GridHighlighted.Rows - 1; X++)
			{
				CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0))].BringToFront();
			}
			// X
		}

		private void mnuCopyControls_Click(object sender, System.EventArgs e)
		{
			int X;
			int Y;
			int lngRow = 0;
			if (GridHighlighted.Rows > 0)
			{
				GridCopy.Rows = 0;
				// loop through each highlighted control
				for (X = 0; X <= GridHighlighted.Rows - 1; X++)
				{
					lngRow = 0;
					for (Y = 1; Y <= GridFields.Rows - 1; Y++)
					{
						if (GridFields.TextMatrix(Y, CNSTGRIDFIELDSCOLFIELDNUM) == GridHighlighted.TextMatrix(X, 0))
						{
							// matched
							lngRow = Y;
							break;
						}
					}
					// Y
					if (lngRow > 0)
					{
						GridCopy.Rows += 1;
						for (Y = 0; Y <= GridFields.Cols - 1; Y++)
						{
							// copy each column
							GridCopy.TextMatrix(GridCopy.Rows - 1, Y, GridFields.TextMatrix(lngRow, Y));
						}
						// Y
					}
				}
				// X
				if (GridCopy.Rows > 0)
				{
					mnuPasteControls.Enabled = true;
				}
			}
		}

		private void mnuDeleteField_Click(object sender, System.EventArgs e)
		{
			int lngRow;
			int lngFldNum;
			int X;
			// delete the row and the control
			if (GridFields.Row < 1)
				return;
			lngRow = GridFields.Row;
			// get rid of the control first
			lngFldNum = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDNUM))));
			// frmCustomBill.Controls.Remove ("CustomLabel" & lngFldNum)
			CustomLabel.Unload(lngFldNum);
			// now get rid of the row
			if (Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLAUTOID)) > 0)
			{
				// if its < 0 then it was never saved, so don't have to worry about it
				GridDeleted.AddItem(FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLAUTOID))));
			}
			GridFields.RemoveItem(lngRow);
			for (X = 0; X <= GridHighlighted.Rows - 1; X++)
			{
				if (FCConvert.ToDouble(GridHighlighted.TextMatrix(X, 0)) == lngFldNum)
				{
					GridHighlighted.RemoveItem(X);
					break;
				}
			}
			// X
		}

		public void mnuDeleteField_Click()
		{
			mnuDeleteField_Click(mnuDeleteField, new System.EventArgs());
		}

		private void mnuDeleteFormat_Click(object sender, System.EventArgs e)
		{
			LoadGridLoadDelete_2(true);
			GridLoadDelete.Tag = (System.Object)(CNSTGRIDLOADDELETEDELETE);
			framLoadDelete.Visible = true;
			framLoadDelete.BringToFront();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupGridBillSize()
		{
			GridBillSize.Cols = 2;
			GridBillSize.Rows = 12;
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDESCRIPTION, "Name");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDESCRIPTION, "Page Height");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDESCRIPTION, "Page Width");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDESCRIPTION, "Bottom Margin");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDESCRIPTION, "Left Margin");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDESCRIPTION, "Right Margin");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDESCRIPTION, "Top Margin");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWSTYLE, CNSTGRIDBILLSIZECOLDESCRIPTION, "Style");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDESCRIPTION, CNSTGRIDBILLSIZECOLDESCRIPTION, "Description");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDESCRIPTION, "Units");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDESCRIPTION, "Background");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE, CNSTGRIDBILLSIZECOLDESCRIPTION, "Default Font Size");
			GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE, CNSTGRIDBILLSIZECOLDATA, "10");
			//FC:FINAL:CHN - issue #1507: Change Fixed Cols color.
			for (int i = 0; i < GridBillSize.Rows; i++)
			{
				GridBillSize.Cell(FCGrid.CellPropertySettings.flexcpBackColor, i, 0, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
			//FC:FINAL:DDU:#2863 - aligned left data column
			GridBillSize.ColAlignment(CNSTGRIDBILLSIZECOLDATA, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ResizeGridBillSize()
		{
			int GridWidth = 0;
			GridWidth = GridBillSize.WidthOriginal;
			GridBillSize.ColWidth(CNSTGRIDBILLSIZECOLDESCRIPTION, FCConvert.ToInt32(0.5 * GridWidth));
			GridBillSize.ColWidth(1, FCConvert.ToInt32(0.45 * GridWidth));
		}

		private void SetupGridLoadDelete()
		{
			GridLoadDelete.Cols = 4;
			GridLoadDelete.Rows = 1;
			GridLoadDelete.ColHidden(CNSTGRIDLOADDELETECOLAUTOID, true);
			GridLoadDelete.ColHidden(CNSTGRIDLOADDELETECOLCHECK, true);
			GridLoadDelete.ColDataType(CNSTGRIDLOADDELETECOLCHECK, FCGrid.DataTypeSettings.flexDTBoolean);
			GridLoadDelete.TextMatrix(0, CNSTGRIDLOADDELETECOLDESCRIPTION, "Description");
			GridLoadDelete.TextMatrix(0, CNSTGRIDLOADDELETECOLNAME, "Format Name");
			GridLoadDelete.TextMatrix(0, CNSTGRIDLOADDELETECOLCHECK, "Export");
			GridLoadDelete.ColAlignment(CNSTGRIDLOADDELETECOLCHECK, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void ResizeGridLoadDelete()
		{
			int GridWidth = 0;
			GridWidth = GridLoadDelete.WidthOriginal;
			if (!GridLoadDelete.ColHidden(CNSTGRIDLOADDELETECOLCHECK))
			{
				GridLoadDelete.ColWidth(CNSTGRIDLOADDELETECOLNAME, FCConvert.ToInt32(0.06 * GridWidth));
			}
			GridLoadDelete.ColWidth(CNSTGRIDLOADDELETECOLNAME, FCConvert.ToInt32(0.2 * GridWidth));
			GridLoadDelete.ExtendLastCol = true;
		}

		private void LoadGridLoadDelete_2(bool boolForDeleting, bool boolForExporting = false)
		{
			LoadGridLoadDelete(boolForDeleting, boolForExporting);
		}

		private void LoadGridLoadDelete_8(bool boolForDeleting, bool boolForExporting = false)
		{
			LoadGridLoadDelete(boolForDeleting, boolForExporting);
		}

		private void LoadGridLoadDelete(bool boolForDeleting = false, bool boolForExporting = false)
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngRow;
			bool boolShowUnDeletables = false;
			try
			{
				// On Error GoTo ErrorHandler
				// load with the current formats
				// if deleting, don't bother to show the ones that are permanent and can't be deleted
				if (boolForDeleting)
				{
					boolShowUnDeletables = false;
				}
				else
				{
					boolShowUnDeletables = true;
				}
				if (boolForExporting)
				{
					GridLoadDelete.ColHidden(CNSTGRIDLOADDELETECOLCHECK, false);
					cmdLoadDeleteCancel.Text = "OK";
				}
				else
				{
					GridLoadDelete.ColHidden(CNSTGRIDLOADDELETECOLCHECK, true);
					cmdLoadDeleteCancel.Text = "Cancel";
				}
				ResizeGridLoadDelete();
				GridLoadDelete.Rows = 1;
				if (!boolShowUnDeletables)
				{
					clsLoad.OpenRecordset("select * from custombills where isnull(isdeletable,0) = 1 order by formatname", "tw" + strThisModule + "0000.vb1");
				}
				else
				{
					clsLoad.OpenRecordset("select * from custombills order by formatname", "tw" + strThisModule + "0000.vb1");
				}
				while (!clsLoad.EndOfFile())
				{
					GridLoadDelete.Rows += 1;
					lngRow = GridLoadDelete.Rows - 1;
					GridLoadDelete.TextMatrix(lngRow, CNSTGRIDLOADDELETECOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
					GridLoadDelete.TextMatrix(lngRow, CNSTGRIDLOADDELETECOLDESCRIPTION, Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))));
					GridLoadDelete.TextMatrix(lngRow, CNSTGRIDLOADDELETECOLNAME, Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("FormatName"))));
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In LoadGridLoadDelete", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupGridControlInfo()
		{
			string strTemp = "";
			GridControlInfo.Cols = 3;
			GridControlInfo.FixedCols = 1;
			GridControlInfo.Rows = 8;
			GridControlInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTYPE, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Type");
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Text");
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWALIGN, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Alignment");
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONT, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Font Name");
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSIZE, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Font Size");
			GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWFONTSTYLE, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Font Style");
			GridControlInfo.ColHidden(CNSTGRIDCONTROLINFOCOLTOOLTIP, true);
			GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWCODE, true);
		}

		private void ResizeGridControlInfo()
		{
			int GridWidth = 0;
			string[] strParms = null;
			// holds different parameters
			string[] strAry = null;
			// holds one parameters information
			string[] strFparms = null;
			// holds the parameters saved in the fields table
			string[] strCList = null;
			// if a parameter has a combolist, this will hold the choices so we can have a default choice
			int X;
			// loop counter
			int lngRow = 0;
			string strTemp = "";
			string[] strParmsToolTips = null;
			GridControlInfo.Rows = CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS;
			// this will be all rows but the extra parameters
			GridWidth = GridControlInfo.WidthOriginal;
			GridControlInfo.ColWidth(CNSTGRIDCONTROLINFOCOLDESCRIPTION, FCConvert.ToInt32(0.6 * GridWidth));
			if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE)
			{
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWALIGN, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONT, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSIZE, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSTYLE, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWTEXT, false);
				//GridControlInfo.HeightOriginal = 2 * GridControlInfo.RowHeight(CNSTGRIDCONTROLINFOROWTYPE) + 80;
				GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "File");
				imgControlImage.Visible = true;
			}
			else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLE || Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE || Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
			{
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWALIGN, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONT, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSIZE, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSTYLE, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWTEXT, true);
				//GridControlInfo.HeightOriginal = GridControlInfo.RowHeight(CNSTGRIDCONTROLINFOROWTYPE) + 80;
				imgControlImage.Visible = false;
			}
			else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMSUBREPORT)
			{
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWALIGN, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONT, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSIZE, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSTYLE, true);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWTEXT, false);
				GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Report Type");
				//GridControlInfo.HeightOriginal = GridControlInfo.RowHeight(CNSTGRIDCONTROLINFOROWTYPE) * 2 + 80;
				imgControlImage.Visible = false;
			}
			else if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
			{
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWALIGN, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONT, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSIZE, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSTYLE, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWTEXT, false);
				//GridControlInfo.HeightOriginal = (GridControlInfo.Rows - 1) * GridControlInfo.RowHeight(CNSTGRIDCONTROLINFOROWTYPE) + 80;
				GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Document");
				imgControlImage.Visible = false;
			}
			else
			{
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWALIGN, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONT, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSIZE, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWFONTSTYLE, false);
				GridControlInfo.RowHidden(CNSTGRIDCONTROLINFOROWTEXT, false);
				// check for extra parameters
				lngRow = GridControlTypes.FindRow(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA), 1, CNSTGRIDCONTROLTYPESCOLCODE);
				if (lngRow > 0)
				{
					if (Strings.Trim(GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS)) != string.Empty)
					{
						strParms = Strings.Split(GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS), ";", -1, CompareConstants.vbTextCompare);
						strTemp = GridFields.TextMatrix(GridFields.Row, CNSTGRIDFIELDSCOLEXTRAPARAMETERS);
						if (strTemp != string.Empty)
						{
							strFparms = Strings.Split(strTemp, ";", -1, CompareConstants.vbTextCompare);
						}
						for (X = 0; X <= Information.UBound(strParms, 1); X++)
						{
							GridControlInfo.Rows += 1;
							strAry = Strings.Split(strParms[X], ",", -1, CompareConstants.vbTextCompare);
							GridControlInfo.TextMatrix(GridControlInfo.Rows - 1, CNSTGRIDCONTROLINFOCOLDESCRIPTION, strAry[0]);
							if (strTemp != string.Empty)
							{
								if (Information.UBound(strFparms, 1) >= X)
								{
									GridControlInfo.TextMatrix(GridControlInfo.Rows - 1, CNSTGRIDCONTROLINFOCOLDATA, strFparms[X]);
								}
							}
							if (Information.UBound(strAry, 1) > 0)
							{
								// extra information
								GridControlInfo.RowData(GridControlInfo.Rows - 1, strAry[1]);
								// store the dropdown list
								strCList = Strings.Split(strAry[1], "|", -1, CompareConstants.vbTextCompare);
								if (strTemp == string.Empty)
								{
									GridControlInfo.TextMatrix(GridControlInfo.Rows - 1, CNSTGRIDCONTROLINFOCOLDATA, strCList[0]);
									// default to first choice
								}
								else if (Information.UBound(strFparms, 1) < X)
								{
									GridControlInfo.TextMatrix(GridControlInfo.Rows - 1, CNSTGRIDCONTROLINFOCOLDATA, strCList[0]);
									// default to first choice
								}
								else if (strFparms[X] == "")
								{
									GridControlInfo.TextMatrix(GridControlInfo.Rows - 1, CNSTGRIDCONTROLINFOCOLDATA, strCList[0]);
									// default to first choice
								}
							}
						}
						// X
						if (Strings.Trim(GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLPARAMETERSTOOLTIP)) != string.Empty)
						{
							strParmsToolTips = Strings.Split(GridControlTypes.TextMatrix(lngRow, CNSTGRIDCONTROLTYPESCOLPARAMETERSTOOLTIP), ";", -1, CompareConstants.vbTextCompare);
							for (X = 0; X <= Information.UBound(strParmsToolTips, 1); X++)
							{
								GridControlInfo.TextMatrix(X + CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS, CNSTGRIDCONTROLINFOCOLTOOLTIP, strParmsToolTips[X]);
							}
							// X
						}
					}
				}
				//if (GridControlInfo.Rows >= CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS + 1)
				//{
				//    GridControlInfo.HeightOriginal = (CNSTGRIDCONTROLINFOROWSTARTOFEXTRAPARAMETERS - 1) * GridControlInfo.RowHeight(CNSTGRIDCONTROLINFOROWTYPE) + 80;
				//}
				//else
				//{
				//    GridControlInfo.HeightOriginal = (GridControlInfo.Rows - 1) * GridControlInfo.RowHeight(CNSTGRIDCONTROLINFOROWTYPE) + 80;
				//}
				GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Text");
				if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP)
				{
					GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWTEXT, CNSTGRIDCONTROLINFOCOLDESCRIPTION, "Description");
				}
				imgControlImage.Visible = false;
				if (Conversion.Val(GridControlInfo.TextMatrix(CNSTGRIDCONTROLINFOROWCODE, CNSTGRIDCONTROLINFOCOLDATA)) == modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP)
				{
					GridControlInfo.RowHidden(GridControlInfo.Rows - 1, true);
				}
			}
		}

		private void SetupGridFields()
		{
			GridFields.Cols = 16;
			GridFields.FixedCols = CNSTGRIDFIELDSCOLFIELDNUM + 1;
			GridFields.Rows = 3;
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLFIELDNUM, "Field");
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLDESCRIPTION, "Name");
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLHEIGHT, "Height");
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLLEFT, "Left");
			// .TextMatrix(0, CNSTGRIDFIELDSCOLTEXT) = "Text"
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLTOP, "Top");
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLWIDTH, "Width");
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLALIGNMENT, "Align");
			// .TextMatrix(0, CNSTGRIDFIELDSCOLFONT) = "Font"
			GridFields.TextMatrix(0, CNSTGRIDFIELDSCOLCONTROL, "Field Info");
			GridFields.ColAlignment(CNSTGRIDFIELDSCOLTOP, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridFields.ColAlignment(CNSTGRIDFIELDSCOLLEFT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridFields.ColAlignment(CNSTGRIDFIELDSCOLWIDTH, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GridFields.ColAlignment(CNSTGRIDFIELDSCOLHEIGHT, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridFields.ColAlignment(CNSTGRIDFIELDSCOLFIELDNUM, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GridFields.ColHidden(CNSTGRIDFIELDSCOLAUTOID, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLFIELDID, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLFONTSIZE, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLFONT, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLTEXT, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLALIGNMENT, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLFONTSTYLE, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLEXTRAPARAMETERS, true);
			GridFields.ColHidden(CNSTGRIDFIELDSCOLMISC, true);
			GridFields.ColComboList(CNSTGRIDFIELDSCOLCONTROL, "...");
			// .ColComboList(CNSTGRIDFIELDSCOLALIGNMENT) = "Left|Right|Center"
			// .ColComboList(CNSTGRIDFIELDSCOLFONT) = "Courier New|Tahoma"
			// .ColComboList(CNSTGRIDFIELDSCOLFONT) = "..."
			GridFields.CellButtonPicture = null;
			//GridFields.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, CNSTGRIDFIELDSCOLFIELDNUM, GridFields.Rows - 1, CNSTGRIDFIELDSCOLFIELDNUM, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//FC:FINAL:AM:#i654 - don't extend the last col because the width becomes too small
			//GridFields.ExtendLastCol = true;
			// now do same thing for gridcopy except for any visual or user interactive stuff
			GridCopy.Cols = 16;
			GridCopy.Rows = 0;
		}

		private void ResizeGridFields()
		{
			// resizes the columns for the grid
			int GridWidth = 0;
			GridWidth = GridFields.WidthOriginal;
			GridFields.ColWidth(CNSTGRIDFIELDSCOLFIELDNUM, FCConvert.ToInt32(0.07 * GridWidth));
			//FC:FINAL:BBE:#323 - adjust column size
			GridFields.ColWidth(CNSTGRIDFIELDSCOLDESCRIPTION, FCConvert.ToInt32(0.27 * GridWidth));
			GridFields.ColWidth(CNSTGRIDFIELDSCOLCONTROL, FCConvert.ToInt32(0.22 * GridWidth));
			GridFields.ColWidth(CNSTGRIDFIELDSCOLTOP, FCConvert.ToInt32(0.1 * GridWidth));
			GridFields.ColWidth(CNSTGRIDFIELDSCOLLEFT, FCConvert.ToInt32(0.1 * GridWidth));
			GridFields.ColWidth(CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToInt32(0.11 * GridWidth));
			// .ColWidth(CNSTGRIDFIELDSCOLALIGNMENT) = 0.09 * GridWidth
			// .ColWidth(CNSTGRIDFIELDSCOLFONT) = 0.12 * GridWidth
			GridFields.ColWidth(CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToInt32(0.11 * GridWidth));
			// .ColWidth(CNSTGRIDFIELDSCOLTEXT) = 0.25 * GridWidth
		}

		private void SetupGridControlTypes()
		{
			GridControlTypes.Rows = 1;
			GridControlTypes.Cols = 11;
			//FC:FINAL:AM: add expand button
			GridControlTypes.AddExpandButton(1);
			GridControlTypes.RowHeadersWidth = 10;
			GridControlTypes.TextMatrix(0, CNSTGRIDCONTROLTYPESCOLCATEGORY, "Category");
			GridControlTypes.TextMatrix(0, CNSTGRIDCONTROLTYPESCOLNAME, "Type");
			GridControlTypes.TextMatrix(0, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "Description");
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLCODE, true);
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, true);
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, true);
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, true);
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS, true);
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLTOOLTIP, true);
			GridControlTypes.ColHidden(CNSTGRIDCONTROLTYPESCOLPARAMETERSTOOLTIP, true);
			GridControlTypes.Editable = FCGrid.EditableSettings.flexEDNone;
			LoadGridControlTypes();
            //FC:FINAL:SBE - redesign grid level tree task
            modColorScheme.ColorGrid(GridControlTypes);
        }

		private void ResizeGridControlTypes()
		{
			int GridWidth = 0;
			GridWidth = GridControlTypes.WidthOriginal;
			GridControlTypes.ColWidth(CNSTGRIDCONTROLTYPESCOLCATEGORY, FCConvert.ToInt32(0.2 * GridWidth));
			GridControlTypes.ColWidth(CNSTGRIDCONTROLTYPESCOLNAME, FCConvert.ToInt32(0.2 * GridWidth));
		}

		private void LoadGridControlTypes()
		{
			// loads the custom bill codes and stores them in the grid
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			string strLastCategory;
			int lngCurRow;
			string strTemp = "";
			bool boolUseDynamicDocuments;
			try
			{
				// On Error GoTo ErrorHandler
				boolUseDynamicDocuments = false;
				// If clsLoad.UpdateDatabaseTable("DynamicReportCodes", "tw" & strThisModule & "0000.vb1") Then
				// Call clsLoad.OpenRecordset("select * from dynamicreportcodes where reporttype = " & CNSTDYNAMICREPORTTYPECUSTOMBILL, "tw" & strThisModule & "0000.vb1")
				// If Not clsLoad.EndOfFile Then
				// boolUseDynamicDocuments = True
				// End If
				// End If
				strSQL = "Select * from custombillcodes order by categoryno,orderno";
				clsLoad.OpenRecordset(strSQL, "tw" + strThisModule + "0000.vb1");
				strLastCategory = "";
				GridControlTypes.Rows = 1;
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "Misc");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Label");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "User defined text field");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(0.1667));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.RowOutlineLevel(lngCurRow, 1);
				GridControlTypes.IsSubtotal(lngCurRow, true);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Image");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "User defined Image");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.RowOutlineLevel(lngCurRow, 2);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLE));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Rectangle");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "An unfilled rectangle");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.RowOutlineLevel(lngCurRow, 2);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Rectangle (Filled)");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "An filled rectangle.  Text will be in white.");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.RowOutlineLevel(lngCurRow, 2);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Horizontal Line");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "A horizontal line");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(0));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.RowOutlineLevel(lngCurRow, 2);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Vertical Line");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "A vertical line");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(0));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.RowOutlineLevel(lngCurRow, 2);
				// GridControlTypes.Rows = GridControlTypes.Rows + 1
				// lngCurRow = GridControlTypes.Rows - 1
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE) = CNSTCUSTOMBILLCUSTOMSUBREPORT
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY) = ""
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME) = "Sub Report"
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION) = "A form within a form"
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT) = 0.1667
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH) = 6
				// GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN) = CNSTCUSTOMBILLALIGNLEFT
				// GridControlTypes.RowOutlineLevel(lngCurRow) = 1
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMBARCODE));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Bar Code");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "A bar code");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(0.5));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(2));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Center");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS, "Barcode Caption,Show Caption|Don't Show");
				GridControlTypes.RowOutlineLevel(lngCurRow, 2);
				GridControlTypes.Rows += 1;
				lngCurRow = GridControlTypes.Rows - 1;
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Prompt Text");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "A text field that the user is prompted to supply a value for");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(0.1667));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(1));
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
				GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS, "User Text,Don't Save|Save);Default Text");
				GridControlTypes.RowOutlineLevel(lngCurRow, 2);
				if (boolUseDynamicDocuments)
				{
					GridControlTypes.Rows += 1;
					lngCurRow = GridControlTypes.Rows - 1;
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT));
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, "");
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, "Dynamic Document");
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, "A user defined document");
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(1));
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(2));
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, "Left");
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS, "");
					GridControlTypes.RowOutlineLevel(lngCurRow, 2);
				}
				while (!clsLoad.EndOfFile())
				{
					GridControlTypes.Rows += 1;
					lngCurRow = GridControlTypes.Rows - 1;
					// TODO: Check the table for the column [category] and replace with corresponding Get_Field method
					if (FCConvert.ToString(clsLoad.Get_Fields("category")) != strLastCategory)
					{
						// TODO: Check the table for the column [category] and replace with corresponding Get_Field method
						strLastCategory = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields("category")));
						GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCATEGORY, strLastCategory);
						GridControlTypes.RowOutlineLevel(lngCurRow, 1);
						GridControlTypes.IsSubtotal(lngCurRow, true);
					}
					else
					{
						GridControlTypes.RowOutlineLevel(lngCurRow, 2);
						GridControlTypes.IsSubtotal(lngCurRow, false);
					}
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLCODE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int32("id"))));
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLNAME, Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("Description"))));
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDESCRIPTION, Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("helpdescription"))));
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTHEIGHT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("defaultheight"))));
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTWIDTH, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("defaultwidth"))));
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLEXTRAPARAMETERS, FCConvert.ToString(clsLoad.Get_Fields_String("extraparameters")));
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLTOOLTIP, FCConvert.ToString(clsLoad.Get_Fields_String("tooltiptext")));
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLPARAMETERSTOOLTIP, FCConvert.ToString(clsLoad.Get_Fields_String("ParametersToolTip")));
					switch (clsLoad.Get_Fields_Int16("defaultalignment"))
					{
						case CNSTCUSTOMBILLALIGNRIGHT:
							{
								strTemp = "Right";
								break;
							}
						case CNSTCUSTOMBILLALIGNCENTER:
							{
								strTemp = "Center";
								break;
							}
						default:
							{
								strTemp = "Left";
								break;
							}
					}
					//end switch
					GridControlTypes.TextMatrix(lngCurRow, CNSTGRIDCONTROLTYPESCOLDEFAULTALIGN, strTemp);
					clsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In LoadGridControlTypes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void MakeNewFormat()
		{
			// Clears out the current one
			// to get a new one without clearing, you would do a Save As instead
			int X;
			try
			{
				// On Error GoTo ErrorHandler
				GridHighlighted.Rows = 0;
				// clear out the highlighted fields since there aren't any
				for (X = GridFields.Rows - 1; X >= 1; X--)
				{
					// step backward since the row numbers will change when you delete rows
					if (Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM)) > 0 && Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDID)) != 0)
					{
						// Me.Controls.Remove ("CustomLabel" & Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM)))
						CustomLabel.Unload(FCConvert.ToInt32(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM)));
					}
					GridFields.RemoveItem(X);
				}
				// X
				GridDeleted.Rows = 0;
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDESCRIPTION, CNSTGRIDBILLSIZECOLDATA, "");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDATA, "New Type");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA, "11");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA, "8.5");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWSTYLE, CNSTGRIDBILLSIZECOLDATA, "Laser");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA, "0");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA, "0");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA, "0");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA, "0");
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA, "Inches");
				CurrentLoadedFormat.boolIsDeletable = true;
				CurrentLoadedFormat.boolIsLaser = true;
				CurrentLoadedFormat.dblBottomMargin = 0;
				CurrentLoadedFormat.dblLeftMargin = 0;
				CurrentLoadedFormat.dblPageHeight = 11;
				CurrentLoadedFormat.dblPageWidth = 8.5;
				CurrentLoadedFormat.dblRightMargin = 0;
				CurrentLoadedFormat.dblTopMargin = 0;
				CurrentLoadedFormat.intUnits = CNSTCUSTOMBILLUNITSINCHES;
				CurrentLoadedFormat.lngBillID = 0;
				CurrentLoadedFormat.strDescription = "";
				CurrentLoadedFormat.strFormatName = "New Type";
				dblTwipsPerUnit = 1440;
				InitializeValues();
				ResizePage();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In MakeNewFormat", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuExportFormat_Click(object sender, System.EventArgs e)
		{
			// get a list of formats to export
			LoadGridLoadDelete_8(false, true);
			GridLoadDelete.Tag = (System.Object)(CNSTGRIDLOADDELETEEXPORT);
			framLoadDelete.Visible = true;
			framLoadDelete.BringToFront();
		}

		private void mnuFillCharFullLine_Click()
		{
		}

		private void mnuFullCharFullLine_Click(object sender, System.EventArgs e)
		{
			mnuFullCharFullLine.Checked = true;
			mnuMoveHalfCharHalfLine.Checked = false;
			mnuMoveQuarterCharQuarterLine.Checked = false;
			intCharWid = 144;
			intLineHite = 240;
			modRegistry.SaveRegistryKey("CustomBillHorizontalIncrements", FCConvert.ToString(intCharWid));
			modRegistry.SaveRegistryKey("CustomBillVerticalIncrements", FCConvert.ToString(intLineHite));
		}

		private void mnuImportFormat_Click(object sender, System.EventArgs e)
		{
			ImportFormat();
		}

		private void ImportFormat()
		{
			// loads custom formats from a database
			string strTemp = "";
			string strDBPath = "";
			string strDBFile = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			// for the mdb file
			clsDRWrapper clsSave = new clsDRWrapper();
			// this modules database
			int lngSrcID = 0;
			// formats ID in the mdb file
			int lngDestID = 0;
			// formats ID in this modules database
			clsDRWrapper clsFields = new clsDRWrapper();
			// the fields table in the mdb file
			string strConn;
			try
			{
				// On Error GoTo ErrorHandler
				// get the file to load
				// App.MainForm.CommonDialog1.Flags = vbPorterConverter.cdlOFNExplorer+vbPorterConverter.cdlOFNNoChangeDir+vbPorterConverter.cdlOFNLongNames+vbPorterConverter.cdlOFNCreatePrompt	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				App.MainForm.CommonDialog1.Text = "File To Import";
				// .FileTitle = "Choose a directory and a name for the file to export"
				App.MainForm.CommonDialog1.InitDir = FCFileSystem.Statics.UserDataFolder;
				App.MainForm.CommonDialog1.Filter = "Database files  (*.mdb)|*.mdb";
				App.MainForm.CommonDialog1.DefaultExt = "mdb";
				App.MainForm.CommonDialog1.CancelError = true;
				App.MainForm.CommonDialog1.ShowOpen();
				strTemp = App.MainForm.CommonDialog1.FileName;
				strDBPath = Path.GetDirectoryName(strTemp);
				strDBFile = Path.GetFileName(strTemp);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                //FC:FINAL:AM:#3215 - no need for this code
                //if (strDBPath == FCFileSystem.Statics.UserDataFolder)
				//	strDBPath = "";
				// no need to change paths when it is the curdir
				if (strDBPath != string.Empty)
				{
					if (Strings.Right(strDBPath, 1) != "\\")
						strDBPath += "\\";
				}
				if (!File.Exists(strDBPath + strDBFile))
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("File doesn't exist", "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				// kk01302015 trouts-51  Change to use ODBC for Access
				// x    clsLoad.GroupName = strDBPath
				// x    'the file exists, so open it and see if it is compatible and from this module
				// x    If clsLoad.UpdateDatabaseTable("Module", strDBFile) Then
				// x        Call clsLoad.OpenRecordset("select * from module", strDBFile)
				clsLoad.MegaGroup = "";
				clsLoad.GroupName = "";
				//FC:FINAL:AM:#i731 - use OleDb
				//strConn = clsLoad.MakeODBCConnectionStringForAccess(strDBPath + strDBFile, false);
				//clsLoad.AddConnection(strConn, strDBFile, FCConvert.ToInt32(clsDRWrapper.dbConnectionTypes.ODBC);
				strConn = clsLoad.MakeOleDBConnectionStringForAccess(strDBPath + strDBFile, false);
				clsLoad.AddConnection(strConn, strDBFile, FCConvert.ToInt32(clsDRWrapper.dbConnectionTypes.OLEDB));
				clsLoad.OpenRecordset("SELECT [Module] FROM [Module]", strDBFile);
				if (!clsLoad.EndOfFile())
				{
					if (!(Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("Module"))) == Strings.UCase(strThisModule)))
					{
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
						MessageBox.Show("The formats in this database are not for this program", "Incorrect Module", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				else
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("The database does not appear to be in the correct format", "Invalid Database", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				// x    Else
				// x        Screen.MousePointer = vbDefault
				// x        MsgBox "The database does not appear to be in the correct format", vbExclamation, "Invalid Database"
				// x        Exit Sub
				// x    End If
				// it is for this module
				clsLoad.OpenRecordset("select * from custombills order by AutoID", strDBFile);
				if (!clsLoad.EndOfFile())
				{
					while (!clsLoad.EndOfFile())
					{
						clsSave.OpenRecordset("select * from custombills where ID = -1", "tw" + strThisModule + "0000.vb1");
						// TODO: Field [AutoID] not found!! (maybe it is an alias?)
						lngSrcID = FCConvert.ToInt32(clsLoad.Get_Fields("AutoID"));
						clsSave.AddNew();
						// TODO: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
						clsSave.Set_Fields("BillFormat", clsLoad.Get_Fields("BillFormat"));
						clsSave.Set_Fields("FormatName", clsLoad.Get_Fields_String("FormatName"));
						clsSave.Set_Fields("Units", clsLoad.Get_Fields_Int16("Units"));
						clsSave.Set_Fields("PageHeight", clsLoad.Get_Fields_Double("PageHeight"));
						clsSave.Set_Fields("PageWidth", clsLoad.Get_Fields_Double("PageWidth"));
						clsSave.Set_Fields("TopMargin", clsLoad.Get_Fields_Double("TopMargin"));
						clsSave.Set_Fields("LeftMargin", clsLoad.Get_Fields_Double("LeftMargin"));
						clsSave.Set_Fields("RightMargin", clsLoad.Get_Fields_Double("RightMargin"));
						clsSave.Set_Fields("boolWide", clsLoad.Get_Fields_Boolean("boolWide"));
						// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
						clsSave.Set_Fields("BillType", clsLoad.Get_Fields("BillType"));
						clsSave.Set_Fields("IsLaser", clsLoad.Get_Fields_Boolean("IsLaser"));
						clsSave.Set_Fields("IsDeletable", clsLoad.Get_Fields_Boolean("IsDeletable"));
						clsSave.Set_Fields("Description", clsLoad.Get_Fields_String("Description"));
						clsSave.Set_Fields("BackgroundImage", clsLoad.Get_Fields_String("BackgroundImage"));
						//FC:FINAL:CHN - i.issue #1559: no export message and incorrect process.
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDESCRIPTION, CNSTGRIDBILLSIZECOLDATA, clsLoad.Get_Fields_String("Description"));
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDATA, clsLoad.Get_Fields_String("FormatName"));
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA, clsLoad.Get_Fields_Double("PageHeight"));
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA, clsLoad.Get_Fields_Double("PageWidth"));
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWSTYLE, CNSTGRIDBILLSIZECOLDATA, "Laser");
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA, clsLoad.Get_Fields_Double("TopMargin"));
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA, clsLoad.Get_Fields_Double("BottomMargin"));
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA, clsLoad.Get_Fields_Double("LeftMargin"));
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA, clsLoad.Get_Fields_Double("RightMargin"));
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA, clsLoad.Get_Fields_Int16("Units"));
						CurrentLoadedFormat.lngBillFormat = clsLoad.Get_Fields_Int32("BillFormat");
						CurrentLoadedFormat.lngBillType = clsLoad.Get_Fields_Int32("BillType");
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA, clsLoad.Get_Fields_String("BackgroundImage"));
						if (Conversion.Val(clsLoad.Get_Fields_Double("DefaultFontSize")) > 0)
						{
							GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("DefaultFontSize"))));
						}
						else
						{
							GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE, CNSTGRIDBILLSIZECOLDATA, "10");
						}
						clsSave.Update();
						lngDestID = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
						// The format is imported, now import all of the fields
						clsFields.GroupName = "";
						// kk01302015 trouts-51  clsFields.GroupName = strDBPath
						clsFields.MegaGroup = "";
						clsFields.OpenRecordset("select * from custombillfields where formatid = " + FCConvert.ToString(lngSrcID) + " order by fieldnumber", strDBFile);
						if (!clsFields.EndOfFile())
						{
							clsSave.OpenRecordset("select * from custombillfields where ID = -1", "tw" + strThisModule + "0000.vb1");
							while (!clsFields.EndOfFile())
							{
								clsSave.AddNew();
								clsSave.Set_Fields("FormatID", lngDestID);
								clsSave.Set_Fields("FieldNumber", clsFields.Get_Fields_Int32("FieldNumber"));
								// TODO: Check the table for the column [FieldID] and replace with corresponding Get_Field method
								clsSave.Set_Fields("FieldID", clsFields.Get_Fields("FieldID"));
								clsSave.Set_Fields("Top", clsFields.Get_Fields_Double("Top"));
								clsSave.Set_Fields("Left", clsFields.Get_Fields_Double("Left"));
								clsSave.Set_Fields("Height", clsFields.Get_Fields_Double("Height"));
								// TODO: Check the table for the column [Width] and replace with corresponding Get_Field method
								clsSave.Set_Fields("Width", clsFields.Get_Fields("Width"));
								// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
								clsSave.Set_Fields("BillType", clsFields.Get_Fields("BillType"));
								clsSave.Set_Fields("Description", clsFields.Get_Fields_String("Description"));
								clsSave.Set_Fields("Alignment", clsFields.Get_Fields_Int16("Alignment"));
								clsSave.Set_Fields("UserText", clsFields.Get_Fields_String("UserText"));
								clsSave.Set_Fields("Font", clsFields.Get_Fields_String("Font"));
								// TODO: Check the table for the column [FontSize] and replace with corresponding Get_Field method
								clsSave.Set_Fields("FontSize", clsFields.Get_Fields("FontSize"));
								clsSave.Set_Fields("FontStyle", FCConvert.ToString(Conversion.Val(clsFields.Get_Fields_Int16("FontStyle"))));
								clsSave.Set_Fields("ExtraParameters", modGlobalFunctions.EscapeQuotes(clsFields.Get_Fields_String("ExtraParameters")));
								clsSave.Update();
								clsFields.MoveNext();
							}
						}
						clsLoad.MoveNext();
					}
				}
				else
				{
					// no formats in it
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					MessageBox.Show("The database has no formats in it", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Import Complete", "Imported", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (Information.Err(ex).Number == FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlCancel))
				{
					return;
				}
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In ImportFormat", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuLoadFormat_Click(object sender, System.EventArgs e)
		{
			LoadGridLoadDelete_2(false);
			GridLoadDelete.Tag = (System.Object)(CNSTGRIDLOADDELETELOAD);
			framLoadDelete.Visible = true;
			framLoadDelete.BringToFront();
		}

		private void mnuMoveHalfCharHalfLine_Click(object sender, System.EventArgs e)
		{
			mnuFullCharFullLine.Checked = false;
			mnuMoveHalfCharHalfLine.Checked = true;
			mnuMoveQuarterCharQuarterLine.Checked = false;
			intCharWid = FCConvert.ToInt32((144.0 / 2));
			intLineHite = FCConvert.ToInt32((240.0 / 2));
			modRegistry.SaveRegistryKey("CustomBillHorizontalIncrements", FCConvert.ToString(intCharWid));
			modRegistry.SaveRegistryKey("CustomBillVerticalIncrements", FCConvert.ToString(intLineHite));
		}

		private void mnuMoveQuarterCharQuarterLine_Click(object sender, System.EventArgs e)
		{
			mnuFullCharFullLine.Checked = false;
			mnuMoveHalfCharHalfLine.Checked = false;
			mnuMoveQuarterCharQuarterLine.Checked = true;
			intCharWid = FCConvert.ToInt32((144.0 / 4));
			intLineHite = FCConvert.ToInt32((240.0 / 4));
			modRegistry.SaveRegistryKey("CustomBillHorizontalIncrements", FCConvert.ToString(intCharWid));
			modRegistry.SaveRegistryKey("CustomBillVerticalIncrements", FCConvert.ToString(intLineHite));
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			if (MessageBox.Show("This will clear the current format from the screen and start with a new blank format." + "\r\n" + "Do you want to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
			{
				return;
			}
			else
			{
				MakeNewFormat();
			}
		}

		private void mnuPasteControls_Click(object sender, System.EventArgs e)
		{
			int X;
			int Y;
			int lngFRow = 0;
			int lngCtrlNum = 0;
			// unhighlight because the pasted ones will be the selected ones
			UnhighlightAllControls();
			// loop through each control
			for (X = 0; X <= GridCopy.Rows - 1; X++)
			{
				// loop through each column and copy
				GridFields.Rows += 1;
				lngFRow = GridFields.Rows - 1;
				for (Y = 0; Y <= GridCopy.Cols - 1; Y++)
				{
					GridFields.TextMatrix(lngFRow, Y, GridCopy.TextMatrix(X, Y));
				}
				// Y
				// now add the new field
				lngCtrlNum = AddANewfield(ref lngFRow);
				if (lngCtrlNum > 0)
				{
					GridHighlighted.Rows += 1;
					GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 0, FCConvert.ToString(lngCtrlNum));
					GridHighlighted.TextMatrix(GridHighlighted.Rows - 1, 1, GridFields.TextMatrix(lngFRow, CNSTGRIDFIELDSCOLFIELDID));
					HighlightControl(lngCtrlNum);
				}
			}
			// X
			// highlight and select all of the ones we just pasted
			HighlightAllInList();
			GridFields.Row = GridFields.Rows - 1;
			GridFields.TopRow = GridFields.Row;
			GridFields.Focus();
		}

		private void mnuPrintSample_Click(object sender, System.EventArgs e)
		{
			clsCustomBill testClass = new clsCustomBill();
			if (CurrentLoadedFormat.lngBillID < 1)
			{
				testClass.FormatID = frmChooseCustomBillType.InstancePtr.Init(strThisModule, "TW" + strThisModule + "0000.vb1");
			}
			else
			{
				testClass.FormatID = CurrentLoadedFormat.lngBillID;
			}
			if (testClass.FormatID > 0)
			{
				testClass.PrinterName = "";
				testClass.DBFile = "TW" + strThisModule + "0000.vb1";
				testClass.Module = strThisModule;
				testClass.SQL = "";
				testClass.UsePrinterFonts = true;
				rptCustomBillTest.InstancePtr.Init(testClass);
			}
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			// UnhighlightAllControls (True)
			// boolInHighlightMode = False
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			if (!CurrentLoadedFormat.boolIsDeletable)
			{
				SaveCustomBillAs();
				return;
			}
			if (CurrentLoadedFormat.lngBillID == 0)
			{
				SaveCustomBillAs();
				return;
			}
			// Isn't new and is editable
			SaveCustomBill();
		}

		private bool SaveCustomBillAs()
		{
			bool SaveCustomBillAs = false;
			// save, but create a new format
			object strInput;
			string strTemp;
			int X;
			try
			{
				// On Error GoTo ErrorHandler
				SaveCustomBillAs = false;
				if (CurrentLoadedFormat.lngBillID > 0)
				{
					if (MessageBox.Show("This will save the current format as a new format." + "\r\n" + "Continue?", "Save as New?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
					{
						return SaveCustomBillAs;
					}
				}
				CurrentLoadedFormat.lngBillID = 0;
				CurrentLoadedFormat.boolIsDeletable = true;
				strInput = "";
				strTemp = Strings.Trim(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDATA));
				if (frmInput.InstancePtr.Init(ref strInput, "Format", "Enter a name for the new format", 2880, false, modGlobalConstants.InputDTypes.idtString, strTemp))
				{
					if (Strings.Trim(FCConvert.ToString(strInput)) == string.Empty)
					{
						MessageBox.Show("Cannot save with a blank format name", "No Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return SaveCustomBillAs;
					}
					GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDATA, strInput);
					for (X = 1; X <= GridFields.Rows - 1; X++)
					{
						// doing a save as so even the unchanged ones need to be saved.  Mark as updated
						GridFields.RowData(X, true);
					}
					// X
					if (SaveCustomBill())
					{
						SaveCustomBillAs = true;
					}
				}
				return SaveCustomBillAs;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveCustomBillAs", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveCustomBillAs;
		}

		private void mnuSaveAs_Click(object sender, System.EventArgs e)
		{
			SaveCustomBillAs();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (!CurrentLoadedFormat.boolIsDeletable)
			{
				if (SaveCustomBillAs())
				{
					Close();
				}
				return;
			}
			if (CurrentLoadedFormat.lngBillID == 0)
			{
				if (SaveCustomBillAs())
				{
					Close();
				}
				return;
			}
			// Isn't new and is editable
			if (SaveCustomBill())
			{
				Close();
			}
		}

		private void mnuSendToBack_Click(object sender, System.EventArgs e)
		{
			int X;
			for (X = 0; X <= GridHighlighted.Rows - 1; X++)
			{
				CustomLabel[FCConvert.ToInt32(GridHighlighted.TextMatrix(X, 0))].ZOrder(ZOrderConstants.SendToBack);
			}
			// X
			// make sure the background image is actually in background
			BillImage.ZOrder(ZOrderConstants.SendToBack);
		}

		private void mnuZoomIn_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:BBE:#323 - make preview larger 50%
			//if (dblPageRatio <= 0.6) return;
			if (dblPageRatio <= 0.3)
				return;
			dblPageRatio -= 0.2;
			lngScreenPageWidth = FCConvert.ToInt32(lngPageWidth / dblPageRatio);
			lngScreenPageHeight = FCConvert.ToInt32(lngPageHeight / dblPageRatio);
			ResizePage();
		}

		private void mnuZoomOut_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:BBE:#323 - make preview larger 50%
			//if (dblPageRatio >= 3) return;
			if (dblPageRatio >= 1.5)
				return;
			dblPageRatio += 0.2;
			lngScreenPageWidth = FCConvert.ToInt32(lngPageWidth / dblPageRatio);
			lngScreenPageHeight = FCConvert.ToInt32(lngPageHeight / dblPageRatio);
			ResizePage();
		}
		//
		private void Picture1_DragDrop(object sender, Wisej.Web.DragEventArgs e)
		{
			// vbPorter upgrade warning: dblTemp As double	OnWrite(string)
			double dblTemp;
			string strTemp = "";
			int lngRow;
			double dblRatio;
			int intloop;
			Control source = e.Data.GetData("fecherFoundation.FCLabel") as Control;
			Point location = this.Picture1.PointToClient(new Point(e.X, e.Y));
			boolDragging = false;
			// Source.Left = Source.Left + (X - lngClickXOffset)
			// Source.Top = Source.Top + (Y - lngClickYOffset)
			if (location.Y - lngClickYOffset < 0)
			{
				source.Top = 0;
			}
			else
			{
				if ((location.Y - lngClickYOffset) + source.Height > Picture1.Height)
				{
					source.Top = Picture1.Height - source.Height;
				}
				else
				{
					source.Top = FCConvert.ToInt32(location.Y - lngClickYOffset);
				}
			}
			if (location.X - lngClickXOffset < 0)
			{
				source.Left = 0;
			}
			else
			{
				if ((location.X - lngClickXOffset) + source.Width > Picture1.Width)
				{
					source.Left = Picture1.Width - source.Width;
				}
				else
				{
					source.Left = FCConvert.ToInt32(location.X - lngClickXOffset);
				}
			}
			lngClickXOffset = 0;
			lngClickYOffset = 0;
			// strTemp = Source.Name
			// strTemp = Mid(strTemp, 12) 'chop off the CustomLabel part before the number
			// lngRow = Val(strTemp)
			lngRow = FCConvert.ToInt32(Math.Round(Conversion.Val(source.Tag)));
			for (intloop = 1; intloop <= GridFields.Rows - 1; intloop++)
			{
				if (Conversion.Val(GridFields.TextMatrix(intloop, CNSTGRIDFIELDSCOLFIELDNUM)) == lngRow)
				{
					lngRow = intloop;
					break;
				}
			}
			// intloop
			dblRatio = (dblPageRatio / (dblTwipsPerUnit * dblSizeRatio));
			dblTemp = FCConvert.ToDouble(Strings.Format(source.Left * dblRatio, "0.0000"));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(dblTemp));
			dblTemp = FCConvert.ToDouble(Strings.Format(source.Top * dblRatio, "0.0000"));
			GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(dblTemp));
			GridFields.RowData(lngRow, true);
			// mark as changed
			GridFields.Row = lngRow;
			// Source.Left = X
			// Source.Top = Y
		}

		private void Picture1_DragOver(object sender, Wisej.Web.DragEventArgs e)
		{
			// If Not boolDragging Then
			// boolDragging = True
			// lngClickXOffset = X
			// lngClickYOffset = Y
			// End If
		}

		private void Picture1_DragEnter(object sender, DragEventArgs e)
		{
			e.Effect = DragDropEffects.Move;
		}

		private void BillImage_DragEnter(object sender, DragEventArgs e)
		{
			e.Effect = DragDropEffects.Move;
		}

		private void VScroll1_ValueChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp;
			double dblTemp;
			//dblTemp = VScroll1.Value / 100.0;
			//lngTemp = FCConvert.ToInt32((Picture1.HeightOriginal - framPage.HeightOriginal + (lngOffsetY * 2)) * dblTemp);
			//Picture1.TopOriginal = -lngTemp + (lngOffsetY);
		}

		public void VScroll1_Change()
		{
			//VScroll1_ValueChanged(VScroll1, new System.EventArgs());
		}

		private void VScroll1_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
		}

		private void VScroll1_Scroll(object sender, Wisej.Web.ScrollEventArgs e)
		{
			// vbPorter upgrade warning: lngTemp As int	OnWriteFCConvert.ToDouble(
			int lngTemp;
			double dblTemp;
			//dblTemp = VScroll1.Value / 100.0;
			//lngTemp = FCConvert.ToInt32((Picture1.HeightOriginal - framPage.HeightOriginal + (lngOffsetY * 2)) * dblTemp);
			//Picture1.TopOriginal = -lngTemp + (lngOffsetY);
		}
		// vbPorter upgrade warning: lngID As int	OnWrite(int, double)
		private void LoadCustomBill_6(int lngID, string strModule)
		{
			LoadCustomBill(lngID, strModule);
		}

		private void LoadCustomBill_8(int lngID, string strModule)
		{
			LoadCustomBill(lngID, strModule);
		}

		private void LoadCustomBill(int lngID, string strModule)
		{
			// opens the record with lngID as the ID
			clsDRWrapper clsLoad = new clsDRWrapper();
			int lngFldCount;
			int lngFID = 0;
			int lngFormatNum;
			int lngTemp = 0;
			int X;
			string strTemp = "";
			clsDRWrapper clsTemp = new clsDRWrapper();
			if (lngID <= 0)
			{
				MakeNewFormat();
				return;
			}
			// first clear out what is already there
			for (X = GridFields.Rows - 1; X >= 1; X--)
			{
				// step backward since the row numbers will change when you delete rows
				if (Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM)) > 0 && Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDID)) != 0)
				{
					// Me.Controls.Remove ("CustomLabel" & Val(GridFields.TextMatrix(x, CNSTGRIDFIELDSCOLFIELDNUM)))
					CustomLabel.Unload(FCConvert.ToInt32(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM)));
				}
				GridFields.RemoveItem(X);
			}
			// X
			GridDeleted.Rows = 0;
			// take care of page size stuff first
			clsLoad.OpenRecordset("SELECT * from custombills where ID = " + FCConvert.ToString(lngID), strModule);
			if (!clsLoad.EndOfFile())
			{
				CurrentLoadedFormat.boolIsDeletable = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("isdeletable"));
				CurrentLoadedFormat.lngBillID = lngID;
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDATA, Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("formatname"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDESCRIPTION, CNSTGRIDBILLSIZECOLDATA, Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("BottomMargin"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("LeftMargin"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("RightMargin"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("TopMargin"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("PageHeight"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("PageWidth"))));
				GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(clsLoad.Get_Fields_String("backgroundimage")));
				switch (clsLoad.Get_Fields_Int16("Units"))
				{
					case CNSTCUSTOMBILLUNITSCENTIMETERS:
						{
							GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA, "Centimeters");
							dblTwipsPerUnit = 567;
							break;
						}
					case CNSTCUSTOMBILLUNITSINCHES:
						{
							GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA, "Inches");
							dblTwipsPerUnit = 1440;
							break;
						}
				}
				//end switch
					GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWSTYLE, CNSTGRIDBILLSIZECOLDATA, "Laser");
				if (Conversion.Val(clsLoad.Get_Fields_Double("DefaultFontSize")) > 0)
				{
					GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE, CNSTGRIDBILLSIZECOLDATA, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("DefaultFontSize"))));
				}
				else
				{
					GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE, CNSTGRIDBILLSIZECOLDATA, "10");
				}
				lngPageHeight = FCConvert.ToInt32((Conversion.Val(clsLoad.Get_Fields_Double("PageHeight")) - Conversion.Val(clsLoad.Get_Fields_Double("TopMargin")) - Conversion.Val(clsLoad.Get_Fields_Double("BottomMargin"))) * dblTwipsPerUnit);
				lngScreenPageHeight = FCConvert.ToInt32(lngPageHeight / dblPageRatio);
				lngPageWidth = FCConvert.ToInt32((Conversion.Val(clsLoad.Get_Fields_Double("PageWidth")) - Conversion.Val(clsLoad.Get_Fields_Double("LeftMargin")) - Conversion.Val(clsLoad.Get_Fields_Double("RightMargin"))) * dblTwipsPerUnit);
				lngScreenPageWidth = FCConvert.ToInt32(lngPageWidth / dblPageRatio);
				strTemp = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("backgroundimage")));
				if (strTemp != string.Empty)
				{
					if (File.Exists(strTemp))
					{
						BillImage.Image = FCUtils.LoadPicture(strTemp);
						BillImage.WidthOriginal = Picture1.WidthOriginal;
						BillImage.HeightOriginal = Picture1.HeightOriginal;
						BillImage.Visible = true;
						GridBillSize.ComboList = Strings.Trim(strTemp) + "|None|Choose New";
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA, Strings.Trim(strTemp));
					}
					else
					{
						GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA, "");
						BillImage.Image = null;
					}
				}
				else
				{
					BillImage.Image = null;
				}
			}
			else
			{
				CurrentLoadedFormat.lngBillID = 0;
				MakeNewFormat();
				return;
			}
			// now load the fields
			clsLoad.OpenRecordset("select * from CustomBillFields where formatid = " + FCConvert.ToString(lngID) + " order by fieldnumber", strModule);
			GridFields.Rows = 1;
			lngFldCount = 0;
			while (!clsLoad.EndOfFile())
			{
				GridFields.Rows += 1;
				lngFldCount += 1;
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
				switch (clsLoad.Get_Fields_Int16("Alignment"))
				{
					case CNSTCUSTOMBILLALIGNLEFT:
						{
							GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLALIGNMENT, "Left");
							break;
						}
					case CNSTCUSTOMBILLALIGNRIGHT:
						{
							GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLALIGNMENT, "Right");
							break;
						}
					case CNSTCUSTOMBILLALIGNCENTER:
						{
							GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLALIGNMENT, "Center");
							break;
						}
				}
				//end switch
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLFONTSTYLE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Int16("FontStyle"))));
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("Description")));
				// TODO: Check the table for the column [FieldID] and replace with corresponding Get_Field method
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLFIELDID, FCConvert.ToString(clsLoad.Get_Fields("FieldID")));
				// TODO: Check the table for the column [fieldid] and replace with corresponding Get_Field method
				lngFID = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("fieldid"))));
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLFIELDNUM, FCConvert.ToString(lngFldCount));
				if (lngFldCount != Conversion.Val(clsLoad.Get_Fields_Int32("fieldnumber")))
				{
					// we changed the field number so mark this as a changed row
					GridFields.RowData(GridFields.Rows - 1, true);
				}
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("Height"))));
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLLEFT, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("Left"))));
				// TODO: Check the table for the column [fieldid] and replace with corresponding Get_Field method
				if (Conversion.Val(clsLoad.Get_Fields("fieldid")) != modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
				{
					GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLTEXT, FCConvert.ToString(clsLoad.Get_Fields_String("UserText")));
				}
				else
				{
					if (Conversion.Val(clsLoad.Get_Fields_String("usertext")) > 0)
					{
						clsTemp.OpenRecordset("select * from DynamicReports where ID = " + FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("usertext"))), "tw" + strThisModule + "0000.vb1");
						if (!clsTemp.EndOfFile())
						{
							// TODO: Field [text] not found!! (maybe it is an alias?)
							GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLTEXT, FCConvert.ToString(clsTemp.Get_Fields("text")));
							GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLMISC, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_String("UserText"))));
						}
					}
					GridFields.RowData(GridFields.Rows - 1, 0);
				}
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLTOP, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields_Double("top"))));
				// TODO: Check the table for the column [width] and replace with corresponding Get_Field method
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("width"))));
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLFONT, Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("Font"))));
				// TODO: Check the table for the column [fontsize] and replace with corresponding Get_Field method
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLFONTSIZE, FCConvert.ToString(Conversion.Val(clsLoad.Get_Fields("fontsize"))));
				GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLEXTRAPARAMETERS, FCConvert.ToString(clsLoad.Get_Fields_String("extraparameters")));
				// TODO: Check the table for the column [fieldid] and replace with corresponding Get_Field method
				lngTemp = GridControlTypes.FindRow(FCConvert.ToString(clsLoad.Get_Fields("fieldid")), -1, CNSTGRIDCONTROLTYPESCOLCODE);
				if (lngTemp >= 0)
				{
					GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLCONTROL, GridControlTypes.TextMatrix(lngTemp, CNSTGRIDCONTROLTYPESCOLNAME));
				}
				// now create a field on the form to represnt this field
				// Select Case lngFID
				// Case CNSTCUSTOMBILLCUSTOMIMAGE
				// Call frmCustomBill.Controls.Add("VB.Image", "CustomLabel" & lngFldCount, Picture1)
				// With frmCustomBill.Controls("CustomLabel" & lngFldCount)
				// .Left = Val(clsLoad.Fields("Left")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio
				// .Height = Val(clsLoad.Fields("height")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio
				// .Width = Val(clsLoad.Fields("width")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio
				// .Top = Val(clsLoad.Fields("Top")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio
				// If Trim(clsLoad.Fields("description")) <> vbNullString Then
				// .Caption = Trim(clsLoad.Fields("description"))
				// Else
				// .Caption = .Name
				// End If
				// .Visible = True
				// .ZOrder
				// .BackColor = vbWhite
				// .Appearance = 0
				// .BorderStyle = 1
				// If Trim(clsLoad.Fields("Font")) <> vbNullString Then
				// .Font = Trim(clsLoad.Fields("Font"))
				// .Font.Size = Val(clsLoad.Fields("fontsize")) * dblSizeRatio / dblPageRatio
				// Else
				// If UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWSTYLE, CNSTGRIDBILLSIZECOLDATA)) = "DOT MATRIX" Then
				// .Font = "COURIER NEW"
				// .Font.Size = 12 * dblSizeRatio / dblPageRatio   '12 point is 10 CPI
				// Else
				// .Font = "Tahoma"
				// .Font.Size = 10 * dblSizeRatio / dblPageRatio
				// End If
				// 
				// End If
				// .Alignment = 2  'Center
				// 
				// End With
				// Case Else
				// Call frmCustomBill.Controls.Add("VB.Label", "CustomLabel" & lngFldCount, Picture1)
				CustomLabel.Load(FCConvert.ToInt16(lngFldCount));
				//FC:FINAL:BBE:#320 - set Capitalize, need to show the same case
				CustomLabel[FCConvert.ToInt16(lngFldCount)].Capitalize = false;
				// With frmCustomBill.Controls("CustomLabel" & lngFldCount)
				// .DragMode = 1
				CustomLabel[FCConvert.ToInt16(lngFldCount)].Tag = lngFldCount;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].LeftOriginal = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Double("Left")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				CustomLabel[FCConvert.ToInt16(lngFldCount)].TopOriginal = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Double("Top")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				CustomLabel[FCConvert.ToInt16(lngFldCount)].HeightOriginal = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Double("height")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				// TODO: Check the table for the column [width] and replace with corresponding Get_Field method
				CustomLabel[FCConvert.ToInt16(lngFldCount)].WidthOriginal = FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields("width")) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BorderStyle = 1;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].MouseDown += CustomLabel_MouseDown;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].AllowDrag = true;
				//CustomLabel[FCConvert.ToInt16(lngFldCount].DragDrop += CustomLabel_DragDrop;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].Movable = true;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].MouseUp += CustomLabel_0_MouseUp;
				if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT)
				{
					if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("usertext"))) != string.Empty)
					{
						CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = FCConvert.ToString(clsLoad.Get_Fields_String("usertext"));
					}
					else
					{
						if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))) != string.Empty)
						{
							CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
						}
						else
						{
							CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = CustomLabel[FCConvert.ToInt16(lngFldCount)].Name;
						}
					}
					CustomLabel[lngFldCount].Appearance = 0;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].BackColor = Color.White;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].ForeColor = Color.Black;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].BackStyle = 0;
				}
				else if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
				{
					if (Strings.Trim(GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLTEXT)) != string.Empty)
					{
						CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLTEXT);
					}
					else
					{
						if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))) != string.Empty)
						{
							CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
						}
						else
						{
							CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = CustomLabel[FCConvert.ToInt16(lngFldCount)].Name;
						}
					}
					CustomLabel[lngFldCount].Appearance = 0;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].BackColor = Color.White;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].ForeColor = Color.Black;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].BackStyle = 0;
				}
				else if ((lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLE) || (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED))
				{
					CustomLabel[lngFldCount].Appearance = 0;
					if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED)
					{
						if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("UserText"))) != string.Empty)
						{
							CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = FCConvert.ToString(clsLoad.Get_Fields_String("Usertext"));
						}
						else
						{
							CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = "";
						}
						CustomLabel[FCConvert.ToInt16(lngFldCount)].BackStyle = 1;
						CustomLabel[FCConvert.ToInt16(lngFldCount)].BackColor = Color.Black;
						CustomLabel[FCConvert.ToInt16(lngFldCount)].ForeColor = Color.White;
					}
					else
					{
						CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = "";
						CustomLabel[FCConvert.ToInt16(lngFldCount)].BackColor = Color.White;
						CustomLabel[FCConvert.ToInt16(lngFldCount)].ForeColor = Color.Black;
						CustomLabel[FCConvert.ToInt16(lngFldCount)].BackStyle = 0;
					}
				}
				else if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
				{
					CustomLabel[lngFldCount].Appearance = 0;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = "";
					CustomLabel[FCConvert.ToInt16(lngFldCount)].BackColor = Color.Black;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].ForeColor = Color.White;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].BackStyle = 0;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].Height = 0;
					GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLHEIGHT, FCConvert.ToString(0));
				}
				else if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
				{
					CustomLabel[lngFldCount].Appearance = 0;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = "";
					CustomLabel[FCConvert.ToInt16(lngFldCount)].BackColor = Color.Black;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].ForeColor = Color.White;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].BackStyle = 0;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].Width = 0;
					GridFields.TextMatrix(GridFields.Rows - 1, CNSTGRIDFIELDSCOLWIDTH, FCConvert.ToString(0));
				}
				else
				{
					if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description"))) != string.Empty)
					{
						CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
					}
					else
					{
						CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = CustomLabel[FCConvert.ToInt16(lngFldCount)].Name;
					}
					CustomLabel[lngFldCount].Appearance = 0;
					CustomLabel[FCConvert.ToInt16(lngFldCount)].BackStyle = 0;
				}
				CustomLabel[FCConvert.ToInt16(lngFldCount)].Visible = true;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BringToFront();
				if (lngFID != modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE && lngFID != modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
				{
					if (Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("Font"))) != string.Empty)
					{
						CustomLabel[FCConvert.ToInt16(lngFldCount)].FontName = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("Font")));
						// TODO: Check the table for the column [fontsize] and replace with corresponding Get_Field method
						CustomLabel[FCConvert.ToInt16(lngFldCount)].FontSize = FCConvert.ToSingle(Conversion.Val(clsLoad.Get_Fields("fontsize")) * dblSizeRatio / dblPageRatio);
					}
					else
					{
						// If optLaser(0).Value Then
						// .Font = "COURIER NEW"
						// Else
						// .Font = "Tahoma"
						// End If
						CustomLabel[FCConvert.ToInt16(lngFldCount)].FontSize = FCConvert.ToSingle(12 * dblSizeRatio / dblPageRatio);
					}
					switch (clsLoad.Get_Fields_Int16("FontStyle"))
					{
						case CNSTCUSTOMBILLFONTSTYLEBOLD:
							{
								CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Bold, true);
								CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Italic, false);
								break;
							}
						case CNSTCUSTOMBILLFONTSTYLEBOLDITALIC:
							{
								CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Bold, true);
								CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Italic, true);
								break;
							}
						case CNSTCUSTOMBILLFONTSTYLEITALIC:
							{
								CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Bold, false);
								CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Italic, true);
								break;
							}
						default:
							{
								// regular
								CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Bold, false);
								CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Italic, false);
								break;
							}
					}
					//end switch
					CustomLabel[lngFldCount].Alignment = (HorizontalAlignment)Conversion.Val(clsLoad.Get_Fields_Int16("alignment"));
				}
                //FC:FINAL:DSE:#1734 Update the form only at the end
				//CustomLabel[FCConvert.ToInt16(lngFldCount].Refresh();
				// End Select
				clsLoad.MoveNext();
			}
            //FC:FINAL:DSE:#1734 Update the form only at the end
            FCUtils.ApplicationUpdate(this);
			GridFields.Col = 0;
			GridFields.Row = 0;
			ResizePage();
		}

		private void ReDrawField(int lngRow)
		{
			int lngFldCount;
			int lngFID = 0;
			lngFldCount = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDNUM))));
			// With frmCustomBill.Controls("CustomLabel" & lngFldCount)
			CustomLabel[FCConvert.ToInt16(lngFldCount)].LeftOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLLEFT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			CustomLabel[FCConvert.ToInt16(lngFldCount)].HeightOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLHEIGHT)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			CustomLabel[FCConvert.ToInt16(lngFldCount)].WidthOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLWIDTH)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			CustomLabel[FCConvert.ToInt16(lngFldCount)].TopOriginal = FCConvert.ToInt32(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTOP)) * dblTwipsPerUnit * dblSizeRatio / dblPageRatio);
			lngFID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFIELDID))));
			if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT)
			{
				if (Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT)) != string.Empty)
				{
					CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT);
				}
				else
				{
					if (Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION)) != string.Empty)
					{
						CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION);
					}
					else
					{
						CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = CustomLabel[FCConvert.ToInt16(lngFldCount)].Name;
					}
				}
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BackColor = Color.White;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].ForeColor = Color.Black;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BackStyle = 0;
			}
			else if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
			{
				if (Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT)) != string.Empty)
				{
					CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT);
				}
				else
				{
					if (Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION)) != string.Empty)
					{
						CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION);
					}
					else
					{
						CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = CustomLabel[FCConvert.ToInt16(lngFldCount)].Name;
					}
				}
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BackColor = Color.White;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].ForeColor = Color.Black;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BackStyle = 0;
			}
			else if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
			{
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BackColor = Color.White;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].ForeColor = Color.Black;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = "";
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BackStyle = 0;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].Height = 0;
			}
			else if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
			{
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BackColor = Color.White;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].ForeColor = Color.Black;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = "";
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BackStyle = 0;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].Width = 0;
			}
			else if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLE)
			{
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BackColor = Color.White;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].ForeColor = Color.Black;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BackStyle = 0;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = "";
			}
			else if (lngFID == modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED)
			{
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BackStyle = 1;
				// opaque
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BackColor = Color.Black;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].ForeColor = Color.White;
				if (Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT)) != string.Empty)
				{
					CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLTEXT);
				}
				else
				{
					CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = "";
				}
			}
			else
			{
				if (Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION)) != string.Empty)
				{
					CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLDESCRIPTION);
				}
				else
				{
					CustomLabel[FCConvert.ToInt16(lngFldCount)].Text = CustomLabel[FCConvert.ToInt16(lngFldCount)].Name;
				}
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BackColor = Color.White;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].ForeColor = Color.Black;
				CustomLabel[FCConvert.ToInt16(lngFldCount)].BackStyle = 0;
			}
			if (lngFID != modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE && lngFID != modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE)
			{
				if (Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONT)) != string.Empty)
				{
					CustomLabel[FCConvert.ToInt16(lngFldCount)].FontName = Strings.Trim(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONT));
					CustomLabel[FCConvert.ToInt16(lngFldCount)].FontSize = FCConvert.ToSingle(Conversion.Val(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSIZE)) * dblSizeRatio / dblPageRatio);
				}
				else
				{
					CustomLabel[FCConvert.ToInt16(lngFldCount)].FontSize = FCConvert.ToSingle(12 * dblSizeRatio / dblPageRatio);
				}
				switch (FCConvert.ToInt32(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLFONTSTYLE)))
				{
					case CNSTCUSTOMBILLFONTSTYLEBOLD:
						{
							CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Bold, true);
							CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Italic, false);
							break;
						}
					case CNSTCUSTOMBILLFONTSTYLEBOLDITALIC:
						{
							CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Bold, true);
							CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Italic, true);
							break;
						}
					case CNSTCUSTOMBILLFONTSTYLEITALIC:
						{
							CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Bold, false);
							CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Italic, true);
							break;
						}
					default:
						{
							// regular
							CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Bold, false);
							CustomLabel[FCConvert.ToInt16(lngFldCount)].Font = FCUtils.SetFontStyle(CustomLabel[FCConvert.ToInt16(lngFldCount)].Font, FontStyle.Italic, false);
							break;
						}
				}
				//end switch
				if (Strings.UCase(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLALIGNMENT)) == "RIGHT")
				{
					//FC:FINAL:CHN - issue #1066: Not working text alignment. 
					// CustomLabel[lngFldCount].Alignment = (HorizontalAlignment)CNSTCUSTOMBILLALIGNRIGHT;
					CustomLabel[lngFldCount].TextAlign = ContentAlignment.MiddleRight;
				}
				else if (Strings.UCase(GridFields.TextMatrix(lngRow, CNSTGRIDFIELDSCOLALIGNMENT)) == "CENTER")
				{
					//FC:FINAL:CHN - issue #1066: Not working text alignment. 
					// CustomLabel[lngFldCount].Alignment = (HorizontalAlignment)CNSTCUSTOMBILLALIGNCENTER;
					CustomLabel[lngFldCount].TextAlign = ContentAlignment.MiddleCenter;
				}
				else
				{
					//FC:FINAL:CHN - issue #1066: Not working text alignment. 
					// CustomLabel[lngFldCount].Alignment = CNSTCUSTOMBILLALIGNLEFT;
					CustomLabel[lngFldCount].TextAlign = ContentAlignment.MiddleLeft;
				}
			}
		}

		public void Init(string strMod, string strTitle = "Custom Bill", int lngIDToLoad = 0)
		{
			strThisModule = Strings.UCase(strMod);
			CurrentLoadedFormat.lngBillID = lngIDToLoad;
			//FC:FINAL:CHN - issue #1238: Tab menu is missing.
			this.LoadForm();
			this.Text = strTitle;
			this.Show(App.MainForm);
		}

		private bool SaveCustomBill()
		{
			bool SaveCustomBill = false;
			// returns true if there are no errors saving
			clsDRWrapper clsSave = new clsDRWrapper();
			int X;
			string strSQL = "";
			int lngLastRowChecked = 0;
			// counter used to go through the grid
			try
			{
				// On Error GoTo ErrorHandler
				SaveCustomBill = false;
				if (Strings.Trim(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDATA)) == string.Empty)
				{
					MessageBox.Show("The format name cannot be blank", "No Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SaveCustomBill;
				}
				clsSave.OpenRecordset("Select * from custombills where ID = " + FCConvert.ToString(CurrentLoadedFormat.lngBillID), "TW" + strThisModule + "0000.vb1");
				if (CurrentLoadedFormat.lngBillID == 0 || clsSave.EndOfFile())
				{
					clsSave.AddNew();
				}
				else
				{
					clsSave.Edit();
				}
				clsSave.Set_Fields("IsDeletable", CurrentLoadedFormat.boolIsDeletable);
				clsSave.Set_Fields("PageHeight", FCConvert.ToString(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEHEIGHT, CNSTGRIDBILLSIZECOLDATA))));
				clsSave.Set_Fields("PageWidth", FCConvert.ToString(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWPAGEWIDTH, CNSTGRIDBILLSIZECOLDATA))));
				clsSave.Set_Fields("TopMargin", FCConvert.ToString(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWTOPMARGIN, CNSTGRIDBILLSIZECOLDATA))));
				clsSave.Set_Fields("BottomMargin", FCConvert.ToString(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBOTTOMMARGIN, CNSTGRIDBILLSIZECOLDATA))));
				clsSave.Set_Fields("LeftMargin", FCConvert.ToString(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWLEFTMARGIN, CNSTGRIDBILLSIZECOLDATA))));
				clsSave.Set_Fields("RightMargin", FCConvert.ToString(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWRIGHTMARGIN, CNSTGRIDBILLSIZECOLDATA))));
				if (Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA)) == "CENTIMETERS")
				{
					clsSave.Set_Fields("Units", CNSTCUSTOMBILLUNITSCENTIMETERS);
				}
				else if (Strings.UCase(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWUNITS, CNSTGRIDBILLSIZECOLDATA)) == "INCHES")
				{
					clsSave.Set_Fields("Units", CNSTCUSTOMBILLUNITSINCHES);
				}
				else
				{
					clsSave.Set_Fields("Units", CNSTCUSTOMBILLUNITSINCHES);
				}
				
				clsSave.Set_Fields("IsLaser", true);
				
                clsSave.Set_Fields("Description", GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDESCRIPTION, CNSTGRIDBILLSIZECOLDATA));
				clsSave.Set_Fields("FormatName", GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWNAME, CNSTGRIDBILLSIZECOLDATA));
				clsSave.Set_Fields("BackgroundImage", Strings.Trim(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWBACKGROUNDIMAGE, CNSTGRIDBILLSIZECOLDATA)));
				clsSave.Set_Fields("DefaultFontSize", FCConvert.ToString(Conversion.Val(GridBillSize.TextMatrix(CNSTGRIDBILLSIZEROWDEFAULTFONTSIZE, CNSTGRIDBILLSIZECOLDATA))));
				clsSave.Set_Fields("BillFormat", CurrentLoadedFormat.lngBillFormat);
				clsSave.Set_Fields("BillType", CurrentLoadedFormat.lngBillType);
				if (!clsSave.Update())
				{
					return SaveCustomBill;
				}
				CurrentLoadedFormat.lngBillID = FCConvert.ToInt32(clsSave.Get_Fields_Int32("ID"));
				// Main record is saved, now update each field
				// take care of deleted rows first
				if (GridDeleted.Rows > 0)
				{
					for (X = 0; X <= GridDeleted.Rows - 1; X++)
					{
						clsSave.Execute("delete from custombillfields where ID = " + FCConvert.ToString(Conversion.Val(GridDeleted.TextMatrix(X, 0))), "TW" + strThisModule + "0000.vb1");
					}
					// X
				}
				GridDeleted.Rows = 0;
				// now take care of new and updated ones
				clsSave.OpenRecordset("select * from custombillfields where formatid = " + FCConvert.ToString(CurrentLoadedFormat.lngBillID), "TW" + strThisModule + "0000.vb1");
				lngLastRowChecked = 1;
				X = GridFields.FindRow(true, lngLastRowChecked);
				while (X > 0)
				{
					// make sure there is data and this isn't somehow a blank grid row
					if (Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDID)) != 0)
					{
						if (Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLAUTOID)) == 0)
						{
							// ID of 0 is a new one
							clsSave.AddNew();
						}
						else
						{
							// just updating
							if (clsSave.FindFirstRecord("ID", Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLAUTOID))))
							{
								clsSave.Edit();
							}
							else
							{
								clsSave.AddNew();
							}
						}
						clsSave.Set_Fields("fieldid", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDID))));
						clsSave.Set_Fields("Top", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLTOP))));
						clsSave.Set_Fields("Left", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLLEFT))));
						clsSave.Set_Fields("Height", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLHEIGHT))));
						clsSave.Set_Fields("Width", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLWIDTH))));
						clsSave.Set_Fields("Description", Strings.Trim(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLDESCRIPTION)));
						clsSave.Set_Fields("BillType", CurrentLoadedFormat.lngBillID);
						if (Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDID)) == modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
						{
							clsSave.Set_Fields("usertext", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLMISC))));
						}
						else
						{
							clsSave.Set_Fields("usertext", GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLTEXT));
						}
						clsSave.Set_Fields("Font", GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFONT));
						clsSave.Set_Fields("Fontsize", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFONTSIZE))));
						clsSave.Set_Fields("FontStyle", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFONTSTYLE))));
						clsSave.Set_Fields("ExtraParameters", modGlobalFunctions.EscapeQuotes(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLEXTRAPARAMETERS)));
						clsSave.Set_Fields("FormatID", CurrentLoadedFormat.lngBillID);
						clsSave.Set_Fields("FieldNumber", FCConvert.ToString(Conversion.Val(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLFIELDNUM))));
						if (Strings.UCase(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLALIGNMENT)) == "RIGHT")
						{
							clsSave.Set_Fields("Alignment", CNSTCUSTOMBILLALIGNRIGHT);
						}
						else if (Strings.UCase(GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLALIGNMENT)) == "CENTER")
						{
							clsSave.Set_Fields("Alignment", CNSTCUSTOMBILLALIGNCENTER);
						}
						else
						{
							clsSave.Set_Fields("Alignment", CNSTCUSTOMBILLALIGNLEFT);
						}
						if (!clsSave.Update())
						{
							return SaveCustomBill;
						}
						GridFields.TextMatrix(X, CNSTGRIDFIELDSCOLAUTOID, FCConvert.ToString(clsSave.Get_Fields_Int32("ID")));
					}
					GridFields.RowData(X, false);
					X = GridFields.FindRow(true, lngLastRowChecked);
				}
				SaveCustomBill = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Format Saved", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveCustomBill;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveCustomBill", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveCustomBill;
		}

		private void cmdAddField_Click(object sender, EventArgs e)
		{
			mnuAddField_Click(sender, e);
		}

		private void cmdDeleteField_Click(object sender, EventArgs e)
		{
			mnuDeleteField_Click(sender, e);
		}

		private void cmdSaveAs_Click(object sender, EventArgs e)
		{
			mnuSaveAs_Click(sender, e);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSave_Click(sender, e);
		}
	}
}
