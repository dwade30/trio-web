﻿namespace Global
{
	public class modLabels
	{
		//=========================================================
		public const int CNSTLBLTYPE4013 = 0;
		public const int CNSTLBLTYPE4014 = 1;
		public const int CNSTLBLTYPE4030 = 2;
		public const int CNSTLBLTYPE5160 = 3;
		public const int CNSTLBLTYPE5161 = 4;
		public const int CNSTLBLTYPE5162 = 5;
		public const int CNSTLBLTYPE5163 = 6;
		public const int CNSTLBLTYPEASSESSMENT = 7;
		public const int CNSTLBLTYPE5026 = 8;
		public const int CNSTLBLTYPE5066 = 9;
		public const int CNSTLBLTYPECERTMAILDOTMATRIX = 10;
		public const int CNSTLBLTYPECERTMAILLASER = 11;
		public const int CNSTLBLTYPEDYMO30256 = 12;
		public const int CNSTLBLTYPEDYMO30252 = 13;
		public const int CNSTLBLTYPE4065 = 14;
		public const int CNSTLBLTYPE4031 = 15;
		public const int CNSTLBLTYPE4033 = 16;
		public const int CNSTLBLTYPEDYMO4150 = 17;
		public const int CNSTLBLTYPE8066 = 18;
		public const int CNSTLBLTYPEDYMO30320 = 19;
	}
}
