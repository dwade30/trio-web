﻿using fecherFoundation;
using fecherFoundation.Extensions;
using SharedApplication;
using System;
using System.Collections.Generic;
using System.Linq;
using TWSharedLibrary;
using Wisej.Web;

#if TWXF0000
using TWXF0000;
#endif
namespace Global
{
    public class cPartyUtil
    {
        //=========================================================
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cGenericCollection listCentralPartyApps = new cGenericCollection();
        //private cPartyController partCont = new cPartyController();
        private cGenericCollection listCentralPartyApps_AutoInitialized;

        private cGenericCollection listCentralPartyApps
        {
            get
            {
                if (listCentralPartyApps_AutoInitialized == null)
                {
                    listCentralPartyApps_AutoInitialized = new cGenericCollection();
                }
                return listCentralPartyApps_AutoInitialized;
            }
            set
            {
                listCentralPartyApps_AutoInitialized = value;
            }
        }

        private cPartyController partCont_AutoInitialized;

        private cPartyController partCont
        {
            get
            {
                if (partCont_AutoInitialized == null)
                {
                    partCont_AutoInitialized = new cPartyController();
                }
                return partCont_AutoInitialized;
            }
            set
            {
                partCont_AutoInitialized = value;
            }
        }

        private bool boolREExists;
        // Public Event UpdateMessage(ByVal dblProgress As Double, ByVal strMessage As String)
        // Public Event CondensedPartiesHintsCompleted()
        // Public Event CondensedPartiesHintsCancelled()
        public bool IsPartyReferenced(int lngID)
        {
            bool IsPartyReferenced = false;
            IsPartyReferenced = false;
            listCentralPartyApps.MoveFirst();
            ICentralPartyApp cpApp;
            while (listCentralPartyApps.IsCurrent())
            {
                //Application.DoEvents();
                cpApp = (ICentralPartyApp)listCentralPartyApps.GetCurrentItem();
                if (cpApp.ReferencesParty(lngID))
                {
                    IsPartyReferenced = true;
                    return IsPartyReferenced;
                }
                listCentralPartyApps.MoveNext();
            }
            return IsPartyReferenced;
        }

        public bool IsDesignation(string strNamePart)
        {
            bool IsDesignation = false;
            try
            {
                // On Error GoTo ErrorHandler
                bool boolReturn;
                boolReturn = false;
                if (Strings.Trim(strNamePart) != "")
                {
                    if ((Strings.UCase(strNamePart) == "SR") || (Strings.UCase(strNamePart) == "JR") || (Strings.UCase(strNamePart) == "II") || (Strings.UCase(strNamePart) == "III") || (Strings.UCase(strNamePart) == "IV") || (Strings.UCase(strNamePart) == "VI") || (Strings.UCase(strNamePart) == "VII") || (Strings.UCase(strNamePart) == "VIII") || (Strings.UCase(strNamePart) == "IX"))
                    {
                        boolReturn = true;
                    }
                    else if ((Strings.UCase(strNamePart) == "I") || (Strings.UCase(strNamePart) == "V") || (Strings.UCase(strNamePart) == "X"))
                    {
                        boolReturn = true;
                    }
                    else if ((Strings.UCase(strNamePart) == "FIRST") || (Strings.UCase(strNamePart) == "SECOND") || (Strings.UCase(strNamePart) == "THIRD") || (Strings.UCase(strNamePart) == "FOURTH") || (Strings.UCase(strNamePart) == "FIFTH") || (Strings.UCase(strNamePart) == "SIXTH") || (Strings.UCase(strNamePart) == "SEVENTH") || (Strings.UCase(strNamePart) == "EIGHTH") || (Strings.UCase(strNamePart) == "NINTH") || (Strings.UCase(strNamePart) == "TENTH"))
                    {
                        boolReturn = true;
                    }
                    else if ((Strings.UCase(strNamePart) == "1ST") || (Strings.UCase(strNamePart) == "2ND") || (Strings.UCase(strNamePart) == "3RD") || (Strings.UCase(strNamePart) == "4TH") || (Strings.UCase(strNamePart) == "5TH") || (Strings.UCase(strNamePart) == "6TH") || (Strings.UCase(strNamePart) == "7TH") || (Strings.UCase(strNamePart) == "8TH") || (Strings.UCase(strNamePart) == "9TH") || (Strings.UCase(strNamePart) == "10TH"))
                    {
                        boolReturn = true;
                    }
                    else
                    {
                        if (Information.IsNumeric(strNamePart))
                        {
                            boolReturn = true;
                        }
                    }
                }
                IsDesignation = boolReturn;
                return IsDesignation;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return IsDesignation;
        }

        public cPartyUtil() : base()
        {
            clsDRWrapper rsTest = new clsDRWrapper();
            ICentralPartyApp cpApp;

            try
            {
                if (rsTest.DBExists("RealEstate"))
                {
                    cpApp = new cREPartyApp();
                    listCentralPartyApps.AddItem(cpApp);
                }
                if (rsTest.DBExists("MotorVehicle"))
                {
                    cpApp = new cMVPartyApp();
                    listCentralPartyApps.AddItem(cpApp);
                }
                if (rsTest.DBExists("Clerk"))
                {
                    cpApp = new cCKPartyApp();
                    listCentralPartyApps.AddItem(cpApp);
                }
                if (rsTest.DBExists("AccountsReceivable"))
                {
                    cpApp = new cARPartyApp();
                    listCentralPartyApps.AddItem(cpApp);
                }
                if (rsTest.DBExists("CodeEnforcement"))
                {
                    cpApp = new cCEPartyApp();
                    listCentralPartyApps.AddItem(cpApp);
                }
                if (rsTest.DBExists("PersonalProperty"))
                {
                    cpApp = new cPPPartyApp();
                    listCentralPartyApps.AddItem(cpApp);
                }
                if (rsTest.DBExists("UtilityBilling"))
                {
                    cpApp = new cUTPartyApp();
                    listCentralPartyApps.AddItem(cpApp);
                }
                if (rsTest.DBExists("CashReceipting"))
                {
                    cpApp = new cCRPartyApp();
                    listCentralPartyApps.AddItem(cpApp);
                }
            }
            finally
            {
                rsTest.DisposeOf();
            }
        }

        public FCCollection AccountsReferencingParty(int lngPartyID, string strMod)
        {
            FCCollection AccountsReferencingParty = null;
            try
            {
                // On Error GoTo ErrorHandler
                listCentralPartyApps.MoveFirst();
                ICentralPartyApp cpApp;
                List<object> tempColl = new List<object>();
                FCCollection refColl = new FCCollection();
                cCollectionHelper collHelp = new cCollectionHelper();
                AccountsReferencingParty = refColl;
                while (listCentralPartyApps.IsCurrent())
                {
                    cpApp = (ICentralPartyApp)listCentralPartyApps.GetCurrentItem();
                    if (Strings.LCase(cpApp.ModuleCode()) == Strings.LCase(strMod))
                    {
                        tempColl = cpApp.AccountsReferencingParty(lngPartyID);
                        if (tempColl != null)
                        {
                            var fc = new FCCollection { tempColl };
                            collHelp.Concatenate(ref refColl, ref fc);
                            AccountsReferencingParty = refColl;
                        }
                        return AccountsReferencingParty;
                    }
                    listCentralPartyApps.MoveNext();
                }
                return AccountsReferencingParty;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return AccountsReferencingParty;
        }

        public FCCollection GetPartyReferences(int lngPartyID)
        {
            FCCollection GetPartyReferences = null;
            try
            {
                // On Error GoTo ErrorHandler
                FCCollection retColl = new FCCollection();
                ICentralPartyApp cpApp;
                FCCollection tempColl = new FCCollection();
                cCollectionHelper collHelp = new cCollectionHelper();
                GetPartyReferences = retColl;
                cPartyRefComparator pComparator = new cPartyRefComparator();
                listCentralPartyApps.MoveFirst();
                while (listCentralPartyApps.IsCurrent())
                {
                    //Application.DoEvents();
                    cpApp = (ICentralPartyApp)listCentralPartyApps.GetCurrentItem();
                    tempColl = new FCCollection();

                    foreach (cCentralPartyReference partyReference in cpApp.AccountsReferencingParty(lngPartyID))
                    {
                        tempColl.Add(partyReference);
                    }

                    collHelp.Concatenate(ref retColl, ref tempColl);
                    listCentralPartyApps.MoveNext();
                }
                if (retColl.Count > 1)
                {
                    retColl = collHelp.Sort(retColl, (cIComparator)pComparator);
                }
                GetPartyReferences = retColl;
                return GetPartyReferences;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetPartyReferences;
        }

        private bool CouldBeSame(cParty partyOne, cParty partyTwo)
        {
            bool CouldBeSame = false;
            // VB6 Bad Scope Dim:
            double sPerc = 0;
            double sPercThreshold = 0;
            // if >= this threshhold then probably a match
            try
            {
                // On Error GoTo ErrorHandler
                sPercThreshold = 92;
                if (partyOne != null && partyTwo != null)
                {
                    if (partyOne.PartyType != partyTwo.PartyType)
                    {
                        return CouldBeSame;
                    }
                    if (partyOne.PartyType != 1)
                    {
                        if (Strings.Trim(partyOne.Designation) != "" && Strings.Trim(partyTwo.Designation) != "")
                        {
                            if (Strings.LCase(Strings.Trim(partyOne.Designation)) != Strings.LCase(Strings.Trim(partyTwo.Designation)))
                            {
                                return CouldBeSame;
                            }
                        }
                        if (Strings.Trim(partyOne.LastName) != "" || Strings.Trim(partyTwo.LastName) != "")
                        {
                            if (Strings.Trim(partyOne.MiddleName) != "" && Strings.Trim(partyTwo.MiddleName) != "")
                            {
                                if (!(partyOne.MiddleName.Length == 1 || partyTwo.MiddleName.Length == 1))
                                {
                                    sPerc = DegreeOfSimilarity_6(Strings.LCase(partyOne.FirstName) + " " + Strings.LCase(partyOne.MiddleName) + " " + Strings.LCase(partyOne.LastName), Strings.LCase(partyTwo.FirstName) + " " + Strings.LCase(partyTwo.MiddleName) + " " + Strings.LCase(partyTwo.LastName));
                                }
                                else
                                {
                                    if (Strings.Left(Strings.LCase(partyOne.MiddleName), 1) == Strings.Left(Strings.LCase(partyTwo.MiddleName), 1))
                                    {
                                        sPerc = DegreeOfSimilarity_6(Strings.LCase(partyOne.FirstName) + " " + Strings.LCase(partyTwo.LastName), Strings.LCase(partyTwo.FirstName) + " " + Strings.LCase(partyTwo.LastName));
                                    }
                                    else
                                    {
                                        return CouldBeSame;
                                    }
                                }
                            }
                            else
                            {
                                sPerc = DegreeOfSimilarity_6(Strings.LCase(partyOne.FirstName) + " " + Strings.LCase(partyTwo.LastName), Strings.LCase(partyTwo.FirstName) + " " + Strings.LCase(partyTwo.LastName));
                            }
                            if (sPerc >= sPercThreshold)
                            {
                                CouldBeSame = true;
                                return CouldBeSame;
                            }
                        }
                        else
                        {
                            // both have no last name
                            sPerc = DegreeOfSimilarity_6(Strings.LCase(partyOne.FirstName), Strings.LCase(partyTwo.FirstName));
                            if (sPerc >= sPercThreshold)
                            {
                                CouldBeSame = true;
                                return CouldBeSame;
                            }
                        }
                    }
                    else
                    {
                        sPerc = DegreeOfSimilarity_6(Strings.LCase(partyOne.FirstName), Strings.LCase(partyTwo.FirstName));
                        if (sPerc >= sPercThreshold)
                        {
                            CouldBeSame = true;
                            return CouldBeSame;
                        }
                    }
                }
                return CouldBeSame;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                sPercThreshold = sPercThreshold;
            }
            return CouldBeSame;
        }

        public FCCollection GetPartyHintsByParty(string strFirstName, string strMiddleName, string strLastName, string strDesig, int intPartyType)
        {
            FCCollection GetPartyHintsByParty = null;
            string strSQL = "";
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                FCCollection hintColl = new FCCollection();
                FCCollection partyColl = new FCCollection();
                cParty srcParty = new cParty();
                srcParty.Designation = strDesig;
                srcParty.FirstName = strFirstName;
                srcParty.MiddleName = strMiddleName;
                srcParty.LastName = strLastName;
                srcParty.PartyType = intPartyType;
                strSQL = "select * from parties where partytype = " + FCConvert.ToString(intPartyType) + " order by firstname, middlename, lastname, designation ";
                partyColl = partCont.GetParties(strSQL, true);

                foreach (var tParty in partyColl.Cast<cParty>().Where(tParty => CouldBeSame(srcParty, tParty)))
                {
                    hintColl.Add(tParty.ID);
                }

                GetPartyHintsByParty = hintColl;

                return GetPartyHintsByParty;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strSQL = strSQL;
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetPartyHintsByParty;
        }

        private double DegreeOfSimilarity_6(string s, string t)
        {
            return DegreeOfSimilarity(s, ref t);
        }

        private double DegreeOfSimilarity(string s, ref string t)
        {
            double DegreeOfSimilarity = 0;
            double lcs;
            double ms;
            if (s == "" || t == "")
            {
                DegreeOfSimilarity = 0;
                return DegreeOfSimilarity;
            }
            if (s == t)
            {
                DegreeOfSimilarity = 100;
                return DegreeOfSimilarity;
            }
            lcs = LCSLength(s, t);
            ms = (s.Length + t.Length) / 2.0;
            DegreeOfSimilarity = (lcs * 100) / ms;
            return DegreeOfSimilarity;
        }
        // <summary>
        // gets longest common string length
        // </summary>
        // <param name="s1"></param>
        // <param name="s2"></param>
        // <returns></returns>
        // <remarks></remarks>
        // vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
        private short LCSLength(string s1, string s2)
        {
            short LCSLength = 0;
            int i;
            int j;
            try
            {
                // On Error GoTo ErrorHandler
                int m;
                m = s1.Length;
                int n;
                n = s2.Length;
                int[,] matrix = new int[100 + 1, 100 + 1];
                for (i = 0; i <= m - 1; i++)
                {
                    matrix[i, 0] = 0;
                }
                // i
                for (j = 0; j <= n - 1; j++)
                {
                    matrix[0, j] = 0;
                }
                // j
                if (Strings.Mid(s1, 1, 1) == Strings.Mid(s2, 1, 1))
                {
                    matrix[0, 0] = 1;
                }
                for (i = 1; i <= m - 1; i++)
                {
                    //Application.DoEvents();
                    for (j = 1; j <= n - 1; j++)
                    {
                        if (Strings.Mid(s1, i + 1, 1) == Strings.Mid(s2, j + 1, 1))
                        {
                            matrix[i, j] = matrix[i - 1, j - 1] + 1;
                        }
                        else if (matrix[i - 1, j] >= matrix[i, j - 1])
                        {
                            matrix[i, j] = matrix[i - 1, j];
                        }
                        else
                        {
                            matrix[i, j] = matrix[i, j - 1];
                        }
                    }
                    // j
                }
                // i
                LCSLength = FCConvert.ToInt16(matrix[m - 1, n - 1]);
                return LCSLength;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                LCSLength = 0;
            }
            return LCSLength;
        }

        public FCCollection SplitName(string strName, bool boolAllowMultiples = true)
        {
            FCCollection SplitName = null;
            FCCollection tColl = new FCCollection();
            FCCollection tempColl = new FCCollection();
            SplitName = tColl;
            if (Strings.Trim(strName) == "")
            {
                return SplitName;
            }
            string strF = "";
            string strL = "";
            string strM = "";
            string strD = "";
            string strsecL = "";
            string strOtherHalf = "";
            bool boolParse;
            int intIndex;
            int x;
            string[] strAry = null;
            int intLimit;
            string strTemp = "";
            string strFirst;
            string strMiddle;
            string strLast;
            string strDesig = "";
            string strSecFirst = "";
            string strSecMiddle = "";
            string strSecLast = "";
            string strSecDesig = "";
            int intPtype1 = 0;
            int intPtype2 = 0;
            if (Strings.Left(strName + " ", 1) == "&" && strName.Length > 1)
            {
                strName = Strings.Mid(strName, 2);
            }
            strName = Strings.Trim(strName.Replace("  ", " "));
            strName = Strings.Trim(strName.Replace("  ", " "));
            strName = Strings.Trim(strName.Replace("  ", " "));
            intIndex = Strings.InStr(1, strName, ",", CompareConstants.vbBinaryCompare);
            if (intIndex > 0)
            {
                strL = Strings.Mid(strName, 1, intIndex - 1);
                strAry = Strings.Split(strL, " ", -1, CompareConstants.vbBinaryCompare);
                if (Information.UBound(strAry, 1) > 0)
                {
                    if (IsDesignation(strAry[Information.UBound(strAry, 1)]))
                    {
                        strDesig = strAry[Information.UBound(strAry, 1)];
                        strL = "";
                        for (x = 0; x <= Information.UBound(strAry, 1) - 1; x++)
                        {
                            strL += " " + strTemp;
                        }
                        strL = Strings.Trim(strL);
                    }
                }
                if (intIndex < strName.Length)
                {
                    strOtherHalf = Strings.Mid(strName, intIndex + 1);
                    bool boolAnd = false;
                    string strAnd = "";
                    strOtherHalf = Strings.Trim(strOtherHalf.Replace("  ", " "));
                    strOtherHalf = Strings.Trim(strOtherHalf.Replace("  ", " "));
                    strOtherHalf = Strings.Trim(strOtherHalf.Replace("  ", " "));
                    intIndex = Strings.InStr(1, strOtherHalf, "&", CompareConstants.vbBinaryCompare);
                    if (intIndex < 1)
                    {
                        intIndex = Strings.InStr(1, Strings.LCase(strOtherHalf), " and ", CompareConstants.vbBinaryCompare);
                        strOtherHalf = strOtherHalf.Replace(" and ", " & ");
                        strOtherHalf = strOtherHalf.Replace(" AND ", " & ");
                        strOtherHalf = strOtherHalf.Replace(" And ", " & ");
                        strOtherHalf = strOtherHalf.Replace(" ANd ", " & ");
                        strOtherHalf = strOtherHalf.Replace(" aND ", " & ");
                        boolAnd = true;
                        strAnd = strOtherHalf;
                    }
                    if (intIndex >= 1)
                    {
                        strAry = Strings.Split(strOtherHalf, "&", -1, CompareConstants.vbBinaryCompare);
                        if (Information.UBound(strAry, 1) < 2)
                        {
                            // less than 3 items
                            strAry[1] = Strings.Trim(strAry[1]);
                            strsecL = "";
                            if (ParsePartialName_1458(strAry[1], ref strF, ref strM, ref strD, ref strsecL, ref intPtype2, true))
                            {
                                strSecFirst = strF;
                                strSecMiddle = strM;
                                if (strsecL == "")
                                {
                                    strSecLast = strL;
                                }
                                else
                                {
                                    strSecLast = strsecL;
                                }
                                strSecDesig = strD;
                                strAry[0] = Strings.Trim(strAry[0]);
                                if (ParsePartialName(strAry[0], ref strF, ref strM, ref strD, ref strsecL, ref intPtype1))
                                {
                                }
                                else
                                {
                                    strSecFirst = "";
                                    strSecMiddle = "";
                                    strSecLast = "";
                                    strSecDesig = "";
                                }
                            }
                            else
                            {
                                boolParse = false;
                                if (boolAnd)
                                    strOtherHalf = strAnd;
                                strF = strOtherHalf;
                                strM = "";
                                strD = "";
                            }
                        }
                        else
                        {
                            strL = "";
                            strDesig = "";
                            strF = strName;
                            boolParse = false;
                        }
                    }
                    else
                    {
                        if (boolAnd)
                            strOtherHalf = strAnd;
                        if (ParsePartialName(strOtherHalf, ref strF, ref strM, ref strD, ref strsecL, ref intPtype1))
                        {
                            if (boolAnd && strOtherHalf == strF)
                            {
                                strOtherHalf = strAnd;
                            }
                            boolParse = true;
                        }
                        else
                        {
                            boolParse = false;
                            strF = strName;
                            strM = "";
                            strD = "";
                            strL = "";
                        }
                    }
                }
                else
                {
                    strF = strName;
                    strL = "";
                    strM = "";
                    strD = "";
                }
            }
            else
            {
                strF = strName;
                tempColl = new FCCollection();
                strAry = Strings.Split(strName, " ", -1, CompareConstants.vbBinaryCompare);
                for (x = 0; x <= Information.UBound(strAry, 1); x++)
                {
                    tempColl.Add(strAry[x]);
                }
                if (IsLikelyNotPerson(tempColl))
                {
                    intPtype1 = 1;
                    // business
                }
            }
            strFirst = strF;
            strLast = strL;
            if (strD != "")
            {
                strDesig = strD;
            }
            strMiddle = strM;
            cParty newPart;
            cCreateGUID tGuid = new cCreateGUID();
            if (strFirst != "" || strLast != "")
            {
                newPart = new cParty();
                if (boolAllowMultiples || (strSecFirst == "" && strSecLast == ""))
                {
                    newPart.Designation = strDesig;
                    newPart.FirstName = strFirst;
                    newPart.LastName = strLast;
                    newPart.MiddleName = strMiddle;
                }
                else
                {
                    newPart.Designation = "";
                    newPart.LastName = "";
                    newPart.MiddleName = "";
                    newPart.FirstName = strName;
                }
                newPart.PartyGUID = tGuid.CreateGUID();
                newPart.PartyType = intPtype1;
                tColl.Add(newPart);
            }
            if (strSecFirst != "" || strSecLast != "")
            {
                if (boolAllowMultiples || (strFirst == "" && strLast == ""))
                {
                    newPart = new cParty();
                    newPart.PartyGUID = tGuid.CreateGUID();
                    newPart.PartyType = intPtype2;
                    newPart.Designation = strSecDesig;
                    newPart.FirstName = strSecFirst;
                    newPart.LastName = strSecLast;
                    newPart.MiddleName = strSecMiddle;
                    tColl.Add(newPart);
                }
            }
            return SplitName;
        }

        private bool IsLikelyNotPerson(FCCollection nameColl)
        {
            bool IsLikelyNotPerson = false;
            try
            {
                // On Error GoTo ErrorHandler
                string strTemp = "";
                int x;
                bool boolReturn = false;
                for (x = 1; x <= nameColl.Count; x++)
                {
                    var noPeriods = Strings.UCase(nameColl[x].Replace(".", ""));

                    switch (noPeriods)
                    {
                        case "TRUSTEE":
                        case "TRUSTEES":
                        case "INC":
                        case "CORP":
                        case "COMPANY":
                        case "AT":
                        case "LLC":
                        case "ASSN":
                        case "LIMITED":
                        case "PARTNERS":
                        case "OWNERS":
                        case "ASSOC":
                        case "PROPERTIES":
                        case "APARTMENTS":
                        case "UTILITY":
                        case "UTILITIES":
                        case "UTIL":
                        case "ASSOCIATES":
                        case "ASSOCIATION":
                        case "CONSERVANCY":
                        case "PRESERVE":
                        case "ASSOCIATE":
                        case "REVOCABLE":
                        case "CORPORATION":
                        case "HOMEOWNERS":
                        case "ESTATE":
                        case "ESTATES":
                        case "BANK":
                        case "PRIVATE":
                        case "REALTY":
                        case "CREDIT":
                        case "UNION":
                        case "MAINE":
                        case "BANGOR":
                        case "TRUST":
                        case "INVESTMENTS":
                        case "PORTLAND":
                        case "PUBLIC":
                        case "STATE":
                            boolReturn = true;

                            break;
                        default:
                        {
                            switch (noPeriods)
                            {
                                case "TOWN":
                                case "CITY":
                                case "COMPANY":
                                case "BUSINESS":
                                case "INCORPORATED":
                                case "HYDRO":
                                case "ELECTRIC":
                                case "LIBRARY":
                                case "UNITED":
                                case "US":
                                case "GOVERNMENT":
                                case "GOV":
                                case "GOVT":
                                case "FEDERAL":
                                case "MUNICIPAL":
                                case "MUNICIPALITY":
                                case "COUNTY":
                                case "AUTO":
                                case "BAKERY":
                                case "ISLAND":
                                case "NATIONAL":
                                case "ACADIA":
                                case "FUEL":
                                case "PRESERVE":
                                case "PRESERVATION":
                                case "VOLUNTEER":
                                case "COLLEGE":
                                case "UNIVERSITY":
                                case "RAILROAD":
                                case "TR":
                                case "REPAIR":
                                case "VILLAGE":
                                case "CHURCH":
                                case "CONGREGATION":
                                case "MINISTRY":
                                case "PARSONAGE":
                                case "EST":
                                case "ESQ":
                                case "IRREVOCABLE":
                                case "LIVING":
                                case "MANAGE":
                                case "SCHOOL":
                                case "PROPERTY":
                                case "TAX":
                                case "GOSPEL":
                                case "ASSEMBLY":
                                case "HEIRS":
                                case "FARM":
                                case "FARMS":
                                case "ENTERPRISE":
                                case "PO":
                                case "BOX":
                                case "SYSTEM":
                                case "ASSOC":
                                case "GROUP":
                                case "SATELLITE":
                                case "LEASING":
                                case "EQUIPMENT":
                                case "INSTITUTION":
                                case "FINANCIAL":
                                    boolReturn = true;

                                    break;
                                default:
                                {
                                    switch (noPeriods)
                                    {
                                        case "SERVICE":
                                        case "PEPSI":
                                        case "DIRECTV":
                                        case "TV":
                                        case "ELECTRIC":
                                        case "AROOSTOOK":
                                        case "NETWORK":
                                        case "MICMACS":
                                        case "PARTNERSHIP":
                                        case "PLACE":
                                        case "POTATO":
                                        case "BROTHERS":
                                        case "CABLE":
                                        case "LMT":
                                        case "ET":
                                        case "HARDWARE":
                                        case "HERMITAGE":
                                        case "MECHANICAL":
                                        case "SCIENTIFIC":
                                        case "FAMILY":
                                        case "PRACTICE":
                                        case "OF":
                                        case "SONS":
                                        case "SON":
                                        case "#":
                                        case "%":
                                        case "C/O":
                                        case "C/0":
                                        case "CO":
                                            boolReturn = true;

                                            break;
                                        case "AL":
                                        case "ALS":
                                        {
                                            if (x > 1)
                                            {
                                                if (Strings.UCase(nameColl[x - 1]) == "ET")
                                                {
                                                    boolReturn = true;
                                                }
                                            }

                                            break;
                                        }
                                    }

                                    break;
                                }
                            }

                            break;
                        }
                    }
                }
                IsLikelyNotPerson = boolReturn;
                return IsLikelyNotPerson;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return IsLikelyNotPerson;
        }
        // vbPorter upgrade warning: intPType As short	OnWriteFCConvert.ToInt32(
        private bool ParsePartialName_1458(string strOriginal, ref string strFname, ref string strMName, ref string strDName, ref string strLName, ref int intPType, bool boolLast = false)
        {
            return ParsePartialName(strOriginal, ref strFname, ref strMName, ref strDName, ref strLName, ref intPType, boolLast);
        }

        private bool ParsePartialName(string strOriginal, ref string strFname, ref string strMName, ref string strDName, ref string strLName, ref int intPType, bool boolLast = false)
        {
            bool ParsePartialName = false;
            string strString;
            string[] strAry = null;
            // vbPorter upgrade warning: boolParse As string	OnWrite(bool)
            string boolParse;
            int intLimit = 0;
            int x;
            // vbPorter upgrade warning: boolReturn As Variant --> As bool
            bool boolReturn;
            FCCollection tempColl = new FCCollection();
            intPType = 0;
            try
            {
                // On Error GoTo ErrorHandler
                strFname = "";
                strMName = "";
                strDName = "";
                strString = Strings.Trim(strOriginal);
                strString = strString.Replace(".", "");
                if (!boolLast)
                {
                    if (Strings.InStr(0, strString, ",", CompareConstants.vbBinaryCompare) >= 0)
                    {
                        strFname = strOriginal;
                        return ParsePartialName;
                    }
                }
                else
                {
                    if (Strings.InStr(0, strString, ",", CompareConstants.vbBinaryCompare) >= 0)
                    {
                        strAry = Strings.Split(strString, ",", -1, CompareConstants.vbBinaryCompare);
                        strLName = strAry[0];
                        strString = Strings.Trim(strAry[1]);
                    }
                }
                boolParse = FCConvert.ToString(true);
                strAry = Strings.Split(strString, " ", -1, CompareConstants.vbBinaryCompare);
                if (Information.UBound(strAry, 1) > 2 || Information.UBound(strAry, 1) == 0)
                {
                    strFname = strString;
                    strMName = "";
                    strDName = "";
                }
                else
                {
                    intLimit = Information.UBound(strAry, 1);
                    if (intLimit < 0)
                    {
                        return ParsePartialName;
                    }
                    if ((Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "I") || (Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "V") || (Strings.UCase(strAry[Information.UBound(strAry, 1)]) == "X"))
                    {
                        if (Information.UBound(strAry, 1) == 2 && Strings.InStr(1, strOriginal, "&", CompareConstants.vbBinaryCompare) < 0)
                        {
                            strDName = strAry[Information.UBound(strAry, 1)];
                            intLimit = Information.UBound(strAry, 1) - 1;
                        }
                    }
                    else
                    {
                        if (IsDesignation(Strings.UCase(strAry[Information.UBound(strAry, 1)])))
                        {
                            strDName = strAry[Information.UBound(strAry, 1)];
                            intLimit = Information.UBound(strAry, 1) - 1;
                        }
                    }
                    tempColl = new FCCollection();
                    for (x = 0; x <= Information.UBound(strAry, 1); x++)
                    {
                        tempColl.Add(strAry[x]);
                    }
                    if (IsLikelyNotPerson(tempColl))
                    {
                        boolParse = FCConvert.ToString(false);
                        intPType = 1;
                    }
                    if (FCConvert.CBool(boolParse))
                    {
                        if (Strings.InStr(0, strString, "&", CompareConstants.vbBinaryCompare) >= 0)
                        {
                            strFname = "";
                            for (x = 0; x <= intLimit; x++)
                            {
                                strFname += strAry[x] + " ";
                            }
                            // x
                            strFname = Strings.Trim(strFname);
                            strMName = "";
                        }
                        else if (intLimit <= 1)
                        {
                            strFname = strAry[0];
                            if (intLimit > 0)
                            {
                                strMName = strAry[1];
                            }
                        }
                        else
                        {
                            strFname = "";
                            for (x = 0; x <= intLimit; x++)
                            {
                                strFname += strAry[x] + " ";
                            }
                            // x
                            strFname = Strings.Trim(strFname);
                            strMName = "";
                        }
                    }
                }
                boolReturn = true;
                ParsePartialName = boolReturn;
                return ParsePartialName;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return ParsePartialName;
        }

        public bool ChangePartyID(int lngOriginalPartyID, int lngNewPartyID)
        {
            bool ChangePartyID = false;
            listCentralPartyApps.MoveFirst();
            ICentralPartyApp cpApp;
            ChangePartyID = true;
            while (listCentralPartyApps.IsCurrent())
            {
                cpApp = (ICentralPartyApp)listCentralPartyApps.GetCurrentItem();
                if (!cpApp.ChangePartyID(lngOriginalPartyID, lngNewPartyID))
                {
                    ChangePartyID = false;
                }
                listCentralPartyApps.MoveNext();
            }
            return ChangePartyID;
        }
    }
}
