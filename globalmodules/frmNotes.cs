﻿//Fecher vbPorter - Version 1.0.0.59
#if TWPY0000
using fecherFoundation;
using TWPY0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmNotes.
	/// </summary>
	public partial class frmNotes : BaseForm
	{
		public frmNotes()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}

		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmNotes InstancePtr
		{
			get
			{
				return (frmNotes)Sys.GetInstance(typeof(frmNotes));
			}
		}
		protected static frmNotes _InstancePtr = null;


		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		public void Init(string strNotes, string strTitle)
		{
			this.Text = strTitle;
			RichTextBox1.TextRTF = strNotes;
			this.Show(App.MainForm);
		}

		private void frmNotes_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmNotes properties;
			//frmNotes.FillStyle	= 0;
			//frmNotes.ScaleWidth	= 9300;
			//frmNotes.ScaleHeight	= 7875;
			//frmNotes.LinkTopic	= "Form2";
			//frmNotes.PaletteMode	= 1  'UseZOrder;
			//RichTextBox1 properties;
			//RichTextBox1.ScrollBars	= 3;
			//RichTextBox1.ReadOnly	= -1  'True;
			//RichTextBox1.TextRTF	= $"frmNotes.frx":058A;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}
	}
}
