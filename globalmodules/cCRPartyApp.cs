﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;

using System.Collections.Generic;
using SharedApplication;
using TWSharedLibrary;

namespace Global
{
	public class cCRPartyApp : ICentralPartyApp
	{
		//=========================================================
		const string strModuleCode = "CR";

		bool ICentralPartyApp.ReferencesParty(int lngID)
		{
			bool ICentralPartyApp_ReferencesParty = false;
			ICentralPartyApp_ReferencesParty = false;
			return ICentralPartyApp_ReferencesParty;
		}

		public List<object> AccountsReferencingParty(int lngID)
		{
			List<object> CentralPartyApp_AccountsReferencingParty = null;
			var theCollection = new List<object>();
			CentralPartyApp_AccountsReferencingParty = theCollection;
			return CentralPartyApp_AccountsReferencingParty;
		}

		string ICentralPartyApp.ModuleCode()
		{
			string ICentralPartyApp_ModuleCode = "";
			ICentralPartyApp_ModuleCode = strModuleCode;
			return ICentralPartyApp_ModuleCode;
		}

		bool ICentralPartyApp.ChangePartyID(int lngOriginalPartyID, int lngNewPartyID)
		{
			bool ICentralPartyApp_ChangePartyID = false;
			clsDRWrapper rsSave = new clsDRWrapper();
			string strSQL;

            try
            {
                // On Error GoTo ErrorHandler
                var newPartyId = FCConvert.ToString(lngNewPartyID);
                var originalPartyId = FCConvert.ToString(lngOriginalPartyID);

                strSQL = $"Update archive set Namepartyid = {newPartyId} where namepartyid = {originalPartyId}";
                rsSave.Execute(strSQL, "CashReceipting");
                strSQL = $"Update lastyeararchive set Namepartyid = {newPartyId} where namepartyid = {originalPartyId}";
                rsSave.Execute(strSQL, "CashReceipting");
                strSQL = $"Update Receipt set paidbypartyid = {newPartyId} where paidbypartyid = {originalPartyId}";
                rsSave.Execute(strSQL, "CashReceipting");
                strSQL = $"Update LastYearReceipt set paidbypartyid = {newPartyId} where paidbypartyid = {originalPartyId}";
                rsSave.Execute(strSQL, "CashReceipting");

                ICentralPartyApp_ChangePartyID = true;

                return ICentralPartyApp_ChangePartyID;
            }
            catch (Exception ex)
            {
				StaticSettings.GlobalTelemetryService.TrackException(ex);
			}
            finally
            {
                rsSave.DisposeOf();
            }
			return ICentralPartyApp_ChangePartyID;
		}
	}
}
