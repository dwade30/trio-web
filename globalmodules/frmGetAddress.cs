﻿using fecherFoundation;
using System;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmGetAddress.
	/// </summary>
	public partial class frmGetAddress : BaseForm
	{
		public frmGetAddress()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetAddress InstancePtr
		{
			get
			{
				return (frmGetAddress)Sys.GetInstance(typeof(frmGetAddress));
			}
		}

		protected frmGetAddress _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private bool boolReturn;
		private string strName = string.Empty;
		private string strAddress1 = string.Empty;
		private string strAddress2 = string.Empty;
		private string strCity = string.Empty;
		private string strZip = string.Empty;
		private string strZip4 = string.Empty;
		private string strState = string.Empty;

		public bool Init(clsAddress Aobj, bool boolEditable = true)
		{
			bool Init = false;
			Init = false;
			boolReturn = false;
			txtName.Text = Aobj.Addressee;
			txtAddress1.Text = Aobj.Get_Address(1);
			txtAddress2.Text = Aobj.Get_Address(2);
			txtCity.Text = Aobj.City;
			txtState.Text = Aobj.State;
			txtZip.Text = Aobj.Zip;
			txtZip4.Text = Aobj.ZipExtension;
			strName = "";
			strAddress1 = "";
			strAddress2 = "";
			strCity = "";
			strZip = "";
			strZip4 = "";
			strState = "";
			if (!boolEditable)
			{
				btnProcess.Visible = false;
				//Seperator.Visible = false;
			}
			this.Show(FormShowEnum.Modal);
			if (boolReturn)
			{
				Aobj.Addressee = strName;
				Aobj.Set_Address(1, strAddress1);
				Aobj.Set_Address(2, strAddress2);
				Aobj.City = strCity;
				Aobj.State = strState;
				Aobj.Zip = strZip;
				Aobj.ZipExtension = strZip4;
			}
			Init = boolReturn;
			return Init;
		}

		private void frmGetAddress_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmGetAddress_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			boolReturn = true;
			strName = Strings.Trim(txtName.Text);
			strAddress1 = Strings.Trim(txtAddress1.Text);
			strAddress2 = Strings.Trim(txtAddress2.Text);
			strCity = Strings.Trim(txtCity.Text);
			strState = Strings.Trim(txtState.Text);
			strZip = txtZip.Text;
			strZip4 = txtZip4.Text;
			Close();
		}

		private void btnProcess_Click(object sender, System.EventArgs e)
		{
			this.mnuSaveContinue_Click(sender, e);
		}
	}
}
