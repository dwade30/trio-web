﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cDescriptionItem
	{
		//=========================================================
		private string strShortDescription = string.Empty;
		private string strLongDescription = string.Empty;

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}

		public string Description
		{
			set
			{
				strLongDescription = value;
			}
			get
			{
				string Description = "";
				Description = strLongDescription;
				return Description;
			}
		}
	}
}
