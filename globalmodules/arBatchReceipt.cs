﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;
using GrapeCity.ActiveReports;
using SharedApplication.Enums;
using TWSharedLibrary;
#if TWCL0000
using TWCL0000;


#else
using TWCR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arBatchReceipt : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/01/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/10/2005              *
		// ********************************************************
		float lngTotalWidth;
		// total width of the report
		int intPaymentNumber;
		// this is the payment number in the grid
		bool boolDone;
		string strPrinterName = "";
		string strPrinterPath = "";
		string strDeviceName = "";
		int lngRow;
		bool boolFirstPass;
		bool boolTextExport;
		double dblReceiptTotal;

		public arBatchReceipt()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Receipt";
		}

		public static arBatchReceipt InstancePtr
		{
			get
			{
				return (arBatchReceipt)Sys.GetInstance(typeof(arBatchReceipt));
			}
		}

		protected arBatchReceipt _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public void Init(int lngPassRow)
		{
			lngRow = lngPassRow;
            //FC:FINAL:SBE - use Print() extension for Document
            //this.Document.Printer.Print();
            //FC:FINAL:SBE - print on receipt printer
            //this.Document.Print(false);
            this.PrintReportOnDotMatrix("RcptPrinterName");

        }

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				eArgs.EOF = boolDone;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Fetch Data");
			}
		}

		private void SetArraySizes()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will set the array elements to the left of each column and it will set
				// the total size of the report depending on the size of the receipt
				int intReturn/*unused?*/;
				if ((modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolNarrowReceipt) || (!modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolCLNarrowReceipt))
				{
					// narrow
					this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("Narrow", 312, 1000);
                    //this.Document.Printer.PaperSize = 255;
                    //this.Document.Printer.PaperHeight = 1440 * 10; // 22
                    //this.Document.Printer.PaperWidth = 4500;
                    PageSettings.PaperWidth = 4500 / 1440f;
                    PageSettings.PaperHeight = 10;
                    PageSettings.Margins.Top = 0;
					PageSettings.Margins.Bottom = 0;
					PageSettings.Margins.Left = 0;
					PageSettings.Margins.Right = 0;
					this.PrintWidth = 4102 / 1440F;
				}
				else
				{
					// wide
					// Me.Printer.PaperSize = 255
					PageSettings.Margins.Left = 360 / 1440F;
					PageSettings.Margins.Right = 360 / 1440F;
					PageSettings.Margins.Top = 360 / 1440F;
					PageSettings.Margins.Bottom = 360 / 1440F;
					// Me.Printer.PaperWidth = 10000
				}
				lngTotalWidth = this.PrintWidth / 1440F;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Array Size");
			}
		}

		private void SetupReceiptFormat()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// set the lefts and widths
				lblMuniName.Width = lngTotalWidth;
				lblHeader.Width = lngTotalWidth;
				lblComment.Width = lngTotalWidth;
				lblReprint.Width = lngTotalWidth;
				lblOtherAccounts.Width = lngTotalWidth;
				if ((modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolNarrowReceipt) || (!modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolCLNarrowReceipt))
				{
					// narrow
					// move all the header fields to the left and move then on top of one another
					lblDate.Left = 0;
					lblTime.Left = 0;
					lblAccount.Left = 0;
					lblTime.Top = lblDate.Top + lblDate.Height;
					// + 100
					lblAccount.Top = lblTime.Top + lblTime.Height;
					// + 100
					lblHeaderAmt.Left = 0;
					// (lngTotalWidth - lblHeaderAmt.Width) - 300
					lblHeaderAmt.Width = lngTotalWidth;
					lblHeaderAmt.Top = lblAccount.Top + lblAccount.Height;
					// + 100
					lblHeaderAmt.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
					lnHeader.Y1 = lblHeaderAmt.Top + lblHeaderAmt.Height;
					lnHeader.Y2 = lnHeader.Y1;
					lnHeader.Visible = false;
				}
				else
				{
					// wide
					lblDate.Left = 0;
					lblTime.Left = lblDate.Left + lblDate.Width;
					lblAccount.Left = lblTime.Left + lblTime.Width;
					lblTime.Top = lblDate.Top;
					lblAccount.Top = lblDate.Top;
					lblHeaderAmt.Top = lblAccount.Top + lblAccount.Height;
					// set the line
					lnHeader.Y1 = lblHeaderAmt.Top + lblHeaderAmt.Height;
					lnHeader.Y2 = lnHeader.Y1;
					lnHeader.X1 = lblHeaderAmt.Left + 100 / 1440F;
					lnHeader.X2 = lblHeaderAmt.Left + lblHeaderAmt.Width - 100 / 1440F;
				}
				ReportHeader.Height = lnHeader.Y1 + 100 / 1440F;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Receipt Format", "Error Setting Receipt Format");
			}
		}

		private void SetMainLabels()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will put in all of the information on the top and bottom of the receipt..ie comments, date/time, and teller
				DateTime dtReceiptDate;
				dtReceiptDate = DateTime.Today;
				// 1st line
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					lblMuniName.Text = modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName;
				}
				else
				{
					lblMuniName.Text = modGlobalConstants.Statics.MuniName;
				}
				// 2nd line
				lblHeader.Text = "-----  R e c e i p t  -----";
				// 3nd line
				lblDate.Text = Strings.Format(dtReceiptDate, "MM/dd/yyyy");
				lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
				lblAccount.Text = "Account " + frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchAcct);
				// 4rd line
				if ((modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolNarrowReceipt) || (!modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolCLNarrowReceipt))
				{
					lblHeaderAmt.Text = modGlobalFunctions.PadStringWithSpaces("Amount", 28, true);
				}
				else
				{
					lblHeaderAmt.Text = "Amount";
				}
				lblOtherAccounts.Text = GetRemainingCLBalance();
				lblOtherAccounts.Visible = true;
				lblReprint.Visible = false;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Main Labels", "Error Setting Main Labels");
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// Doevents
		}

		private void arBatchReceipt_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				strPrinterName = StaticSettings.GlobalSettingService
					                 .GetSettingValue(SettingOwnerType.Machine, "RCPTPrinterName")?.SettingValue ?? "";
				//modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "RCPTPrinterName", ref strPrinterName);
				if (strPrinterName != "")
				{
					// check to see if the printer that is setup is a file rather than a printer
					if (SetupAsPrinterFile(ref strPrinterName))
					{
						// if it is then export it to a file
						boolTextExport = true;
					}
					else
					{
						boolTextExport = false;
						this.Document.Printer.PrinterName = strPrinterName;
					}
				}
				boolFirstPass = true;
				// this is for reprints
				// use the printers collection to set the printer settings
				SetArraySizes();
				SetupReceiptFormat();
				intPaymentNumber = 1;
				SetMainLabels();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Starting Receipt");
			}
		}

		private bool SetupAsPrinterFile(ref string strName)
		{
			bool SetupAsPrinterFile = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return true if the string passed int is "PRNT.txt"
				if (Strings.UCase(strName) == Strings.UCase("PRNT.TXT"))
				{
					SetupAsPrinterFile = true;
				}
				else
				{
					SetupAsPrinterFile = false;
				}
				return SetupAsPrinterFile;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Printer File");
			}
			return SetupAsPrinterFile;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (boolFirstPass)
				{
					BindFields();
					boolFirstPass = false;
				}
				else
				{
					ClearFields();
					boolDone = true;
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Detail Format");
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// strTemp = GetInfoAboutOtherAccounts(intTag, lngNumOfRows)
				if ((modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolNarrowReceipt) || (!modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolCLNarrowReceipt))
				{
					fldTotal.Width = 0;
					fldTotal.Left = 0;
					fldTotal.Visible = false;
					fldTotal.Text = "";
					lblTotal.Top = 0;
					lblTotal.Left = 0;
					lblTotal.Width = lngTotalWidth;
					lblTotal.Text = modGlobalFunctions.PadStringWithSpaces("Total:", 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblReceiptTotal, "$#,##0.00"), 13, true);
				}
				else
				{
					fldTotal.Width = lblHeaderAmt.Width;
					fldTotal.Left = lblHeaderAmt.Left;
					lblTotal.Left = fldTotal.Left - lblTotal.Width;
					fldTotal.Text = Strings.Format(dblReceiptTotal, "$#,##0.00");
					lblTotal.Text = "Total:";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Footer Format");
			}
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intRow = 0;
				float lngStartingPoint;
				float lngRowHeight;
				string strPrinType;
				bool boolAddPayment;
				lngRowHeight = 270 / 1440F;
				lblName.Text = "";
				lblLocation.Text = "";
				lblComment.Text = "";
				if ((modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolNarrowReceipt) || (!modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolCLNarrowReceipt))
				{
					// this will set the size of the labels
					lblPrincipal.Width = lngTotalWidth;
					lblPLI.Width = lngTotalWidth;
					lblInterest.Width = lngTotalWidth;
					lblCost.Width = lngTotalWidth;
					lblPrincipalAmount.Width = 0;
					// lblHeaderAmt.Width
					lblPLIAmount.Width = 0;
					// lblHeaderAmt.Width
					lblInterestAmount.Width = 0;
					// lblHeaderAmt.Width
					lblCostAmount.Width = 0;
					// lblHeaderAmt.Width
				}
				strPrinType = "Principal";
				boolAddPayment = true;
				// this is where the payment lines will be set to
				lngStartingPoint = lblLocation.Top + lblLocation.Height;
				lblName.Text = Strings.Trim(frmCLGetAccount.InstancePtr.txtPaidBy.Text);
				if (Conversion.Val(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchPrin)) != 0)
				{
					// show it
					lblPrincipal.Visible = true;
					if ((modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolNarrowReceipt) || (!modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolCLNarrowReceipt))
					{
						// Narrow
						lblPrincipal.Top = lngStartingPoint + (lngRowHeight * intRow);
						intRow += 1;
						lblPrincipalAmount.Visible = false;
						lblPrincipalAmount.Left = 0;
						lblPrincipal.Text = modGlobalFunctions.PadStringWithSpaces(strPrinType, 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchPrin), "$#,##0.00"), 13, true);
						lblPrincipalAmount.Text = "";
					}
					else
					{
						lblPrincipal.Top = lngStartingPoint + (lngRowHeight * intRow);
						lblPrincipalAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
						lblPrincipalAmount.Visible = true;
						lblPrincipalAmount.Left = lblHeaderAmt.Left;
						lblPrincipal.Text = strPrinType;
						lblPrincipalAmount.Text = Strings.Format(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchPrin), "$#,##0.00");
						intRow += 1;
					}
				}
				else
				{
					// hide it
					lblPrincipal.Visible = false;
					lblPrincipalAmount.Visible = false;
				}
				// If .TextMatrix(lngRow, frmCLGetAccount.lngColBatch) <> 0 Then
				// show it
				// lblPLI.Visible = True
				// lblPLI.Visible = True
				// lblPLIAmount.Left = lblHeaderAmt.Left
				// 
				// If (gboolCR And Not gboolNarrowReceipt) Or (Not gboolCR And Not gboolCLNarrowReceipt) Then
				// lblPLI.Top = lngStartingPoint + (lngRowHeight * intRow)
				// intRow = intRow + 1
				// 
				// lblPLIAmount.Visible = False
				// lblPLIAmount.Left = 0
				// lblPLIAmount.Top = 0
				// 
				// lblPLI.Text = PadStringWithSpaces("Pre Lien Interest", 15, False) & PadStringWithSpaces(Format(.PreLienInterest, "$#,##0.00"), 13, True)
				// lblPLIAmount.Text = ""
				// Else
				// lblPLI.Text = "Pre Lien Interest"
				// lblPLIAmount.Text = Format(.PreLienInterest, "$#,##0.00")
				// 
				// lblPLI.Top = lngStartingPoint + (lngRowHeight * intRow)
				// lblPLIAmount.Top = lngStartingPoint + (lngRowHeight * intRow)
				// intRow = intRow + 1
				// End If
				// Else
				// hide it
				lblPLI.Visible = false;
				lblPLIAmount.Visible = false;
				// End If
				if (Conversion.Val(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchInt)) != 0)
				{
					// show it
					lblInterest.Visible = true;
					lblInterestAmount.Visible = true;
					lblInterestAmount.Left = lblHeaderAmt.Left;
					if ((modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolNarrowReceipt) || (!modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolCLNarrowReceipt))
					{
						// Narrow
						lblInterest.Top = lngStartingPoint + (lngRowHeight * intRow);
						intRow += 1;
						lblInterest.Text = modGlobalFunctions.PadStringWithSpaces("Interest", 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchInt), "$#,##0.00"), 13, true);
						lblInterestAmount.Text = "";
						lblInterestAmount.Top = 0;
						lblInterestAmount.Visible = false;
						lblInterestAmount.Left = 0;
					}
					else
					{
						lblInterestAmount.Text = Strings.Format(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchInt), "$#,##0.00");
						lblInterest.Text = "Interest";
						lblInterest.Top = lngStartingPoint + (lngRowHeight * intRow);
						lblInterestAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
						intRow += 1;
					}
				}
				else
				{
					// hide it
					lblInterest.Visible = false;
					lblInterestAmount.Visible = false;
				}
				if (Conversion.Val(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchCost)) != 0)
				{
					// show it
					lblCost.Visible = true;
					lblCostAmount.Visible = true;
					lblCostAmount.Left = lblHeaderAmt.Left;
					if ((modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolNarrowReceipt) || (!modGlobalConstants.Statics.gboolCR && modGlobal.Statics.gboolCLNarrowReceipt))
					{
						// Narrow
						lblCost.Top = lngStartingPoint + (lngRowHeight * intRow);
						intRow += 1;
						lblCost.Text = "Cost";
						lblCost.Text = modGlobalFunctions.PadStringWithSpaces("Cost", 15, false) + modGlobalFunctions.PadStringWithSpaces(Strings.Format(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchCost), "$#,##0.00"), 13, true);
						lblCostAmount.Top = 0;
						lblCostAmount.Visible = false;
						lblCostAmount.Text = "";
						lblCostAmount.Left = 0;
					}
					else
					{
						lblCost.Top = lngStartingPoint + (lngRowHeight * intRow);
						lblCostAmount.Top = lngStartingPoint + (lngRowHeight * intRow);
						lblCost.Text = "Cost";
						lblCostAmount.Text = Strings.Format(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchCost), "$#,##0.00");
						intRow += 1;
					}
				}
				else
				{
					// hide it
					lblCost.Visible = false;
					lblCostAmount.Visible = false;
				}
				if (boolAddPayment)
				{
					dblReceiptTotal += FCConvert.ToDouble(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchPrin)) + FCConvert.ToDouble(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchInt)) + FCConvert.ToDouble(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchCost));
				}
				// set the height of the detail section
				Detail.Height = lngStartingPoint + ((intRow) * lngRowHeight);
				intPaymentNumber += 1;
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Detail Format");
			}
		}

		private void ClearFields()
		{
			Detail.Height = 0;
			lblPrincipal.Text = "";
			lblPrincipalAmount.Text = "";
			lblPLI.Text = "";
			lblPLIAmount.Text = "";
			lblInterest.Text = "";
			lblInterestAmount.Text = "";
			lblCost.Text = "";
			lblCostAmount.Text = "";
		}

		private string GetRemainingCLBalance()
		{
			string GetRemainingCLBalance = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngGrpNum = 0;
				double dblTotal;
				int intNumAccts = 0;
				// VBto upgrade warning: lngAcctNumber As int	OnWrite(string)
				int lngAcctNumber = 0;
				bool boolError = false;
				// VBto upgrade warning: dblPaymentAmount As double	OnWrite(string)
				double dblPaymentAmount;
				dblPaymentAmount = FCConvert.ToDouble(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchTotal));
				if (modStatusPayments.Statics.boolRE)
				{
					lngAcctNumber = FCConvert.ToInt32(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchAcct));
					lngGrpNum = modGlobalFunctions.GetGroupNumber_6(lngAcctNumber, "RE");
				}
				else
				{
					lngAcctNumber = FCConvert.ToInt32(frmCLGetAccount.InstancePtr.vsBatch.TextMatrix(lngRow, frmCLGetAccount.InstancePtr.lngColBatchAcct));
					lngGrpNum = modGlobalFunctions.GetGroupNumber_6(lngAcctNumber, "PP");
				}
				dblTotal = 0;
				if (lngGrpNum > 0)
				{
					boolError = !GroupInformation(ref intNumAccts, ref dblTotal, ref lngGrpNum, ref dblPaymentAmount);
				}
				else
				{
					// this is when the account has not been set up in a group, but it still can have a remaining balance
					if (modStatusPayments.Statics.boolRE)
					{
						// MAL@20071009: Added the Effective Date to what is getting passed in
						// Call Reference: 114968
						// dblTotal = dblTotal + CalculateAccountTotal(lngAcctNumber, True)
						dblTotal += modCLCalculations.CalculateAccountTotal(lngAcctNumber, true, false, DateAndTime.DateValue(frmCLGetAccount.InstancePtr.txtEffectiveDate.Text));
					}
					else
					{
						// dblTotal = dblTotal + CalculateAccountTotal(lngAcctNumber, False)
						dblTotal += modCLCalculations.CalculateAccountTotal(lngAcctNumber, false, false, Convert.ToDateTime(frmCLGetAccount.InstancePtr.txtEffectiveDate.Text));
					}
					dblTotal -= dblPaymentAmount;
				}
				if (boolError)
				{
					// do not do anything
					GetRemainingCLBalance = "";
				}
				else
				{
					if (intNumAccts > 1)
					{
						GetRemainingCLBalance = "Remaining Balance : " + "(" + FCConvert.ToString(intNumAccts) + ") " + Strings.Format(dblTotal, "$#,##0.00");
					}
					else
					{
						GetRemainingCLBalance = "Remaining Balance : " + Strings.Format(dblTotal, "$#,##0.00");
					}
				}
				return GetRemainingCLBalance;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Remaining Balance Error");
				/*? Resume; */
			}
			return GetRemainingCLBalance;
		}
		// VBto upgrade warning: intTotalAccounts As short	OnWriteFCConvert.ToInt32(
		public bool GroupInformation(ref int intTotalAccounts, ref double dblTotalOutstandingBalance, ref int lngGroupNumber, ref double dblPaymentAmount)
		{
			bool GroupInformation = false;
			// this function will return true if a group number has been found and will pass the number of accounts and balance back in the parameters
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsGroup = new clsDRWrapper();
				clsDRWrapper rsGrpMst = new clsDRWrapper();
				string strSQL = "";
				int intTemp = 0;
				bool boolCL = false;
				bool boolUT = false;
				// kgk trocr-302 10-24-2011  Straighten out confusion between GroupMaster.GroupNumber and ModuleAssociation.GroupNumber
				rsGrpMst.OpenRecordset("SELECT * FROM GroupMaster WHERE GroupMaster.GroupNumber = " + FCConvert.ToString(lngGroupNumber), "CentralData");
				if (!rsGrpMst.EndOfFile())
				{
					// strSQL = "SELECT * FROM ModuleAssociation WHERE GroupNumber = " & lngGroupNumber
					strSQL = "SELECT * FROM ModuleAssociation WHERE GroupNumber = " + FCConvert.ToString(rsGrpMst.Get_Fields_Int32("ID"));
					if (rsGroup.OpenRecordset(strSQL, "CentralData"))
					{
						if (!rsGroup.EndOfFile())
						{
							GroupInformation = true;
							// this will loop through all of the transactions on the receipt and check for groups
							while (!rsGroup.EndOfFile())
							{
								// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
								dblTotalOutstandingBalance += GetRemainingBalance(FCConvert.ToInt32(rsGroup.Get_Fields("Account")), FCConvert.ToString(rsGroup.Get_Fields_String("Module")), intTemp, dblPaymentAmount);
								// this will decide if the accounts have already been counted or not
								// only want this to happen one time, then clear it out
								dblPaymentAmount = 0;
								string VBtoVar = rsGroup.Get_Fields_String("Module");
								if ((VBtoVar == "RE") || (VBtoVar == "PP"))
								{
									if (boolCL)
									{
									}
									else
									{
										boolCL = true;
										// intTotalAccounts = intTotalAccounts + intTemp
									}
								}
								else if (VBtoVar == "UT")
								{
									if (boolUT)
									{
									}
									else
									{
										boolUT = true;
										// intTotalAccounts = intTotalAccounts + intTemp
									}
								}
								rsGroup.MoveNext();
							}
							intTotalAccounts = FCConvert.ToInt16(rsGroup.RecordCount());
						}
						else
						{
							intTotalAccounts = 0;
							dblTotalOutstandingBalance = 0;
							GroupInformation = false;
						}
					}
				}
				return GroupInformation;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				intTotalAccounts = 0;
				dblTotalOutstandingBalance = 0;
				GroupInformation = false;
			}
			return GroupInformation;
		}
		// VBto upgrade warning: intNumAccts As short	OnWriteFCConvert.ToInt32(
		public double GetRemainingBalance(int lngAccountNumber, string strModule, int intNumAccts, double dblPaymentAmount)
		{
			double GetRemainingBalance = 0;
			// this function will return the remaining balance from one account
			clsDRWrapper rsMod = new clsDRWrapper();
			clsDRWrapper rsTempCL = new clsDRWrapper();
			clsDRWrapper rsTempLien = new clsDRWrapper();
			string strSQL = "";
			// set the default database and the sql string for the
			rsMod.DefaultDB = modExtraModules.strCLDatabase;
			rsTempCL.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) + " AND BillingType = '" + strModule + "' ORDER BY BillingYear", modExtraModules.strCLDatabase);
			if (rsTempCL.EndOfFile() != true && rsTempCL.BeginningOfFile() != true)
			{
				do
				{
					if (((rsTempCL.Get_Fields_Int32("LienRecordNumber"))) != 0)
					{
						rsTempLien.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsTempCL.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
						if (!rsTempLien.EndOfFile())
						{
							double temp = 0;
							GetRemainingBalance += modCLCalculations.CalculateAccountCLLien(rsTempLien, DateTime.Today, ref temp);
						}
						else
						{
							// keep going
						}
					}
					else
					{
						GetRemainingBalance += modCLCalculations.CalculateAccountCL(ref rsTempCL, lngAccountNumber, DateTime.Today, 0);
					}
					rsTempCL.MoveNext();
				}
				while (!rsTempCL.EndOfFile());
				GetRemainingBalance -= dblPaymentAmount;
			}
			else
			{
				GetRemainingBalance = 0;
			}
			return GetRemainingBalance;
			ERROR_HANDLER:
			;
			GetRemainingBalance = 0;
			return GetRemainingBalance;
		}

		private void arBatchReceipt_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arBatchReceipt.Text	= "Receipt";
			//arBatchReceipt.Icon	= "arBatchReceipt.dsx":0000";
			//arBatchReceipt.Left	= 0;
			//arBatchReceipt.Top	= 0;
			//arBatchReceipt.Width	= 9525;
			//arBatchReceipt.Height	= 8595;
			//arBatchReceipt.SectionData	= "arBatchReceipt.dsx":058A;
			//End Unmaped Properties
		}
	}
}
