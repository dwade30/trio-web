﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWRE0000
using modExtraModules = TWRE0000.modGlobalVariables;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmTaxAcquiredRemoval.
	/// </summary>
	public partial class frmTaxAcquiredRemoval : BaseForm
	{
		public frmTaxAcquiredRemoval()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTaxAcquiredRemoval InstancePtr
		{
			get
			{
				return (frmTaxAcquiredRemoval)Sys.GetInstance(typeof(frmTaxAcquiredRemoval));
			}
		}

		protected frmTaxAcquiredRemoval _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/22/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/03/2006              *
		// ********************************************************
		int intAction;
		clsDRWrapper rsData = new clsDRWrapper();
		bool boolLoaded;
		double dblDemand;
		double dblCertMailFee;
		bool boolChargeMort;
		bool boolChargeCert;
		bool boolChargeForNewOwner;
		DateTime dtMailDate;
		clsDRWrapper rsRE = new clsDRWrapper();
		int lngBillingYear;
		string strYearList = "";
		int lngValidateRowYear;
		int lngGridColTaxAcquired;
		int lngGridColAccount;
		int lngGridColName;
		int lngGridColLocation;
		int lngGridColOSTaxYear;
		int lngGridColOSTaxTotal;
		int lngGridColBillKey;

		public void Init()
		{
			//FC:FINAL:AM: moved code from load and resize
			lngGridColTaxAcquired = 0;
			lngGridColBillKey = 1;
			lngGridColAccount = 2;
			lngGridColName = 3;
			lngGridColLocation = 4;
			lngGridColOSTaxYear = 5;
			lngGridColOSTaxTotal = 6;
			vsDemand.Visible = true;
			lblInstruction.Visible = true;
			SetAct();
			FormatGrid();
		}

		private void frmTaxAcquiredRemoval_Activated(object sender, System.EventArgs e)
		{
			this.Text = "Tax Acquired Removal";
			// False
			lblInstruction.Visible = true;
			App.DoEvents();
		}

		private void frmTaxAcquiredRemoval_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTaxAcquiredRemoval.Icon	= "frmTaxAcquiredRemoval.frx":0000";
			//frmTaxAcquiredRemoval.FillStyle	= 0;
			//frmTaxAcquiredRemoval.ScaleWidth	= 9195;
			//frmTaxAcquiredRemoval.ScaleHeight	= 7455;
			//frmTaxAcquiredRemoval.LinkTopic	= "Form2";
			//frmTaxAcquiredRemoval.LockControls	= -1  'True;
			//frmTaxAcquiredRemoval.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9.75";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsDemand.BackColor	= "-2147483643";
			//			//vsDemand.ForeColor	= "-2147483640";
			//vsDemand.BorderStyle	= 1;
			//vsDemand.FillStyle	= 0;
			//vsDemand.Appearance	= 1;
			//vsDemand.GridLines	= 1;
			//vsDemand.WordWrap	= 0;
			//vsDemand.ScrollBars	= 3;
			//vsDemand.RightToLeft	= 0;
			//vsDemand._cx	= 15584;
			//vsDemand._cy	= 8744;
			//vsDemand._ConvInfo	= 1;
			//vsDemand.MousePointer	= 0;
			//vsDemand.BackColorFixed	= -2147483633;
			//			//vsDemand.ForeColorFixed	= -2147483630;
			//vsDemand.BackColorSel	= -2147483635;
			//			//vsDemand.ForeColorSel	= -2147483634;
			//vsDemand.BackColorBkg	= -2147483636;
			//vsDemand.BackColorAlternate	= -2147483643;
			//vsDemand.GridColor	= -2147483633;
			//vsDemand.GridColorFixed	= -2147483632;
			//vsDemand.TreeColor	= -2147483632;
			//vsDemand.FloodColor	= 192;
			//vsDemand.SheetBorder	= -2147483642;
			//vsDemand.FocusRect	= 1;
			//vsDemand.HighLight	= 1;
			//vsDemand.AllowSelection	= -1  'True;
			//vsDemand.AllowBigSelection	= -1  'True;
			//vsDemand.AllowUserResizing	= 1;
			//vsDemand.SelectionMode	= 0;
			//vsDemand.GridLinesFixed	= 2;
			//vsDemand.GridLineWidth	= 1;
			//vsDemand.RowHeightMin	= 0;
			//vsDemand.RowHeightMax	= 0;
			//vsDemand.ColWidthMin	= 0;
			//vsDemand.ColWidthMax	= 0;
			//vsDemand.ExtendLastCol	= -1  'True;
			//vsDemand.FormatString	= "";
			//vsDemand.ScrollTrack	= -1  'True;
			//vsDemand.ScrollTips	= 0   'False;
			//vsDemand.MergeCells	= 0;
			//vsDemand.MergeCompare	= 0;
			//vsDemand.AutoResize	= -1  'True;
			//vsDemand.AutoSizeMode	= 0;
			//vsDemand.AutoSearch	= 0;
			//vsDemand.AutoSearchDelay	= 2;
			//vsDemand.MultiTotals	= -1  'True;
			//vsDemand.SubtotalPosition	= 1;
			//vsDemand.OutlineBar	= 0;
			//vsDemand.OutlineCol	= 0;
			//vsDemand.Ellipsis	= 0;
			//vsDemand.ExplorerBar	= 1;
			//vsDemand.PicturesOver	= 0   'False;
			//vsDemand.PictureType	= 0;
			//vsDemand.TabBehavior	= 0;
			//vsDemand.OwnerDraw	= 0;
			//vsDemand.ShowComboButton	= -1  'True;
			//vsDemand.TextStyle	= 0;
			//vsDemand.TextStyleFixed	= 0;
			//vsDemand.OleDragMode	= 0;
			//vsDemand.OleDropMode	= 0;
			//vsDemand.ComboSearch	= 3;
			//vsDemand.AutoSizeMouse	= -1  'True;
			//vsDemand.AllowUserFreezing	= 0;
			//vsDemand.BackColorFrozen	= 0;
			//			//vsDemand.ForeColorFrozen	= 0;
			//vsDemand.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmTaxAcquiredRemoval.frx":058A";
			//End Unmaped Properties
			//lngGridColTaxAcquired = 0;
			//lngGridColBillKey = 1;
			//lngGridColAccount = 2;
			//lngGridColName = 3;
			//lngGridColLocation = 4;
			//lngGridColOSTaxYear = 5;
			//lngGridColOSTaxTotal = 6;
			//modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			//modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmTaxAcquiredRemoval_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				Close();
			}
			else if (KeyAscii == 13)
			{
				KeyAscii = 0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		//private void frmTaxAcquiredRemoval_Resize(object sender, System.EventArgs e)
		//{
		//	FormatGrid(true);
		//	vsDemand.Visible = true;
		//	lblInstruction.Visible = true;
		//}
		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			ClearCheckBoxes();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void FormatGrid(bool boolResize = true)
		{
			int wid = 0;
			if (!boolResize)
			{
				vsDemand.Rows = 1;
			}
			vsDemand.Cols = 7;
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(lngGridColTaxAcquired, FCConvert.ToInt32(wid * 0.05));
			// checkbox
			vsDemand.ColWidth(lngGridColBillKey, 0);
			// BillKey
			vsDemand.ColWidth(lngGridColAccount, FCConvert.ToInt32(wid * 0.1));
			// Acct
			vsDemand.ColWidth(lngGridColName, FCConvert.ToInt32(wid * 0.5));
			// Account
			vsDemand.ColWidth(lngGridColLocation, FCConvert.ToInt32(wid * 0.3));
			// Location
			vsDemand.ColWidth(lngGridColOSTaxYear, 0);
			vsDemand.ColWidth(lngGridColOSTaxTotal, 0);
			vsDemand.ColFormat(lngGridColOSTaxYear, "#,##0.00");
			vsDemand.ColFormat(lngGridColOSTaxTotal, "#,##0.00");
			vsDemand.ColDataType(lngGridColTaxAcquired, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColAlignment(lngGridColAccount, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDemand.ColAlignment(lngGridColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngGridColLocation, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngGridColOSTaxYear, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDemand.ColAlignment(lngGridColOSTaxTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDemand.TextMatrix(0, lngGridColTaxAcquired, "TA");
			vsDemand.TextMatrix(0, lngGridColAccount, "Acct");
			vsDemand.TextMatrix(0, lngGridColName, "Name");
			vsDemand.TextMatrix(0, lngGridColLocation, "Location");
			vsDemand.TextMatrix(0, lngGridColOSTaxTotal, "Total Due");
			//FC:FINAL:CHN: Set column data type to fix ordering.
			vsDemand.ColDataType(lngGridColAccount, FCGrid.DataTypeSettings.flexDTLong);
		}

		private void FillDemandGrid()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				clsDRWrapper rsRK = new clsDRWrapper();
				DateTime dtBillDate;
				string strSQL = "";
				int lngIndex;
				double dblXInt/*unused?*/;
				double dblBillAmount/*unused?*/;
				double dblTotalAmount/*unused?*/;
				clsDRWrapper rsLN = new clsDRWrapper();

				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Info");
				vsDemand.Rows = 1;
				rsRE.OpenRecordset("SELECT * FROM Master WHERE RSCard = 1 AND TaxAcquired = 1 ORDER BY RSAccount", modExtraModules.strREDatabase);
				if (rsRE.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					FCMessageBox.Show("There are no accounts set to Tax Acquired.", MsgBoxStyle.Information, "No Eligible Accounts");
					Close();
				}
				lngIndex = -1;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Accounts", true, rsRE.RecordCount(), true);
				while (!rsRE.EndOfFile())
				{
					App.DoEvents();
					// add a row/element
					vsDemand.AddItem("");

					vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColAccount, FCConvert.ToString(rsRE.Get_Fields_Int32("RSAccount")));
                    vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColName, rsRE.Get_Fields_String("DeedName1"));

					if (Conversion.Val(rsRE.Get_Fields_String("RSLOCNUMALPH")) != 0)
					{
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColLocation, Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCNUMALPH")) + " " + FCConvert.ToString(rsRE.Get_Fields_String("RSLOCSTREET"))));
						// location
					}
					else
					{
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColLocation, Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLOCSTREET"))));
						// location
					}
					// End If
					// amounts
					// .TextMatrix(.rows - 1, lngGridColOSTaxYear) = dblBillAmount
					// dblTotalAmount = CalculateAccountTotal(rsData.Get_Fields("Account"), True)
					// .TextMatrix(.rows - 1, lngGridColOSTaxTotal) = dblTotalAmount
					SKIP:
					;
					rsRE.MoveNext();
				}
				if (vsDemand.Rows <= 1)
				{
					frmWait.InstancePtr.Unload();
					Close();
				}
				else
				{
					this.Show(App.MainForm);
				}
				frmWait.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Filling Grid");
			}
		}

		private void SetAct()
		{
			// this will change all of the menu options
			FillDemandGrid();
			vsDemand.Visible = true;
			lblInstruction.Visible = true;
			cmdFileSelectAll.Visible = true;
			btnProcess.Text = "Save";
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			// apply demand fees
			if (ApplyDemandFees())
			{
				Close();
			}
		}

		private bool ApplyDemandFees()
		{
			bool ApplyDemandFees = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngCount = 0;
				clsDRWrapper rsRate = new clsDRWrapper();
				string strInits = "";
				TRYAGAIN:
				;
				if (FCMessageBox.Show("Are you sure that you would like to remove these accounts from Tax Acquired status manually?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Manual Removal From Tax Acquired Status") == DialogResult.Yes)
				{
					strInits = Interaction.InputBox("Please enter your initials here", "Enter Initials");
					if (Strings.Trim(strInits) == "" || Strings.Trim(strInits).Length < 2)
					{
						FCMessageBox.Show("Please enter valid initials.", MsgBoxStyle.Exclamation, "Invalid Initials");
						goto TRYAGAIN;
					}
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Updating Tax Acquired");
					for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
					{
						// for each account in the grid
						App.DoEvents();
						if (Conversion.Val(vsDemand.TextMatrix(lngCT, 0)) == -1)
						{
							// check to see if the check box if checked
							// rsRE.FindFirstRecord "RSAccount", vsDemand.TextMatrix(lngCT, lngGridColAccount)
							rsRE.FindFirst("rsaccount = " + vsDemand.TextMatrix(lngCT, lngGridColAccount));
							if (!rsRE.NoMatch)
							{
								rsRE.Edit();
								rsRE.Set_Fields("TaxAcquired", false);
								rsRE.Update();
								lngCount += 1;
								modGlobalFunctions.AddCYAEntry_62("CL", "Manually Removed Account From Tax Acquired Status", strInits, FCConvert.ToString(rsRE.Get_Fields_Int32("RSAccount")));
							}
						}
					}
					frmWait.InstancePtr.Unload();
					if (lngCount != 1)
					{
						FCMessageBox.Show(FCConvert.ToString(lngCount) + " accounts were affected.", MsgBoxStyle.Information, "Tax Acquired Status");
					}
					else
					{
						FCMessageBox.Show(FCConvert.ToString(lngCount) + " account was affected.", MsgBoxStyle.Information, "Tax Acquired Status");
					}
					if (modGlobalConstants.Statics.gboolBD)
					{
						FCMessageBox.Show("The Budgetary journal entries were not created for these accounts, they will have to be created manually.", MsgBoxStyle.Exclamation, "Journal Entries");
					}
					Close();
					App.MainForm.Focus();
				}
				return ApplyDemandFees;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Affecting Status");
			}
			return ApplyDemandFees;
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// this will select all account in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
			}
		}

		private void vsDemand_DblClick(object sender, EventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsDemand.MouseRow;
			lngMC = vsDemand.MouseCol;
			if (lngMR == 0 && lngMC == 0)
			{
				mnuFileSelectAll_Click(cmdFileSelectAll, EventArgs.Empty);
			}
		}

		private void vsDemand_RowColChange(object sender, EventArgs e)
		{
			if (vsDemand.Col == lngGridColTaxAcquired)
			{
				vsDemand.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void ClearCheckBoxes()
		{
			// this will set all of the textboxes to unchecked
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, lngGridColTaxAcquired, FCConvert.ToString(0));
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFilePrint_Click(sender, e);
		}

		private void cmdFileSelectAll_Click(object sender, EventArgs e)
		{
			this.mnuFileSelectAll_Click(sender, e);
		}

		private void cmdFileClear_Click(object sender, EventArgs e)
		{
			this.mnuFileClear_Click(sender, e);
		}
	}
}
