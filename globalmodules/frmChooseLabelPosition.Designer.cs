﻿//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmChooseLabelPosition.
	/// </summary>
	partial class frmChooseLabelPosition : BaseForm
	{
		public FCGrid GridLabels;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private FCButton btnSave;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChooseLabelPosition));
			this.GridLabels = new fecherFoundation.FCGrid();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.btnSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridLabels)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 509);
			this.BottomPanel.Size = new System.Drawing.Size(610, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.GridLabels);
			this.ClientArea.Size = new System.Drawing.Size(610, 449);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(610, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(293, 30);
			this.HeaderText.Text = "User Defined Fill in Fields";
			// 
			// GridLabels
			// 
			this.GridLabels.AllowBigSelection = false;
			this.GridLabels.AllowSelection = false;
			this.GridLabels.AllowUserToResizeColumns = false;
			this.GridLabels.AllowUserToResizeRows = false;
			this.GridLabels.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridLabels.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridLabels.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridLabels.BackColorBkg = System.Drawing.Color.Empty;
			this.GridLabels.BackColorFixed = System.Drawing.Color.Empty;
			this.GridLabels.BackColorSel = System.Drawing.Color.Empty;
			this.GridLabels.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridLabels.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridLabels.ColumnHeadersHeight = 30;
			this.GridLabels.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridLabels.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridLabels.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridLabels.DragIcon = null;
			this.GridLabels.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridLabels.ExtendLastCol = true;
			this.GridLabels.FixedCols = 0;
			this.GridLabels.FixedRows = 0;
			this.GridLabels.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridLabels.FrozenCols = 0;
			this.GridLabels.GridColor = System.Drawing.Color.Empty;
			this.GridLabels.GridColorFixed = System.Drawing.Color.Empty;
			this.GridLabels.Location = new System.Drawing.Point(30, 0);
			this.GridLabels.Name = "GridLabels";
			this.GridLabels.ReadOnly = true;
			this.GridLabels.RowHeadersVisible = false;
			this.GridLabels.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridLabels.RowHeightMin = 0;
			this.GridLabels.Rows = 5;
			this.GridLabels.ScrollTipText = null;
			this.GridLabels.ShowColumnVisibilityMenu = false;
			this.GridLabels.Size = new System.Drawing.Size(550, 432);
			this.GridLabels.StandardTab = true;
			this.GridLabels.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridLabels.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridLabels.TabIndex = 0;
			this.GridLabels.Click += new System.EventHandler(this.GridLabels_Click);
			this.GridLabels.DoubleClick += new System.EventHandler(this.GridLabels_DblClick);
			this.GridLabels.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveContinue,
				this.mnuSepar,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 1;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// btnSave
			// 
			this.btnSave.AppearanceKey = "acceptButton";
			this.btnSave.Location = new System.Drawing.Point(244, 30);
			this.btnSave.Name = "btnSave";
			this.btnSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnSave.Size = new System.Drawing.Size(87, 48);
			this.btnSave.TabIndex = 0;
			this.btnSave.Text = "Save";
			this.btnSave.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// frmChooseLabelPosition
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(610, 617);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmChooseLabelPosition";
			this.Text = "Choose Label";
			this.Load += new System.EventHandler(this.frmChooseLabelPosition_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmChooseLabelPosition_KeyDown);
			this.Resize += new System.EventHandler(this.frmChooseLabelPosition_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridLabels)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
