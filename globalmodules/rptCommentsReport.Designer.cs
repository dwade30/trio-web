﻿namespace Global
{
	/// <summary>
	/// Summary description for rptCommentsReport.
	/// </summary>
	partial class rptCommentsReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCommentsReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldComment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRptTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRptDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSubtitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldComment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRptTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRptDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSubtitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldName,
				this.fldLocation,
				this.fldComment
			});
			this.Detail.Height = 0.3333333F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.19F;
			this.fldAccount.Left = 0F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldAccount.Text = "fldAccount";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.75F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.19F;
			this.fldName.Left = 0.84375F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldName.Text = "fldName";
			this.fldName.Top = 0F;
			this.fldName.Width = 3.0625F;
			// 
			// fldLocation
			// 
			this.fldLocation.Height = 0.19F;
			this.fldLocation.Left = 4.125F;
			this.fldLocation.Name = "fldLocation";
			this.fldLocation.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 1";
			this.fldLocation.Text = "fldLocation";
			this.fldLocation.Top = 0F;
			this.fldLocation.Width = 3.0625F;
			// 
			// fldComment
			// 
			this.fldComment.Height = 0.19F;
			this.fldComment.Left = 0.28125F;
			this.fldComment.Name = "fldComment";
			this.fldComment.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldComment.Text = "fldComment";
			this.fldComment.Top = 0.15625F;
			this.fldComment.Width = 7.21875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.lblRptTime,
				this.lblMuniName,
				this.lblPageNo,
				this.lblRptDate,
				this.lblAccount,
				this.Line1,
				this.lblName,
				this.lblLocation,
				this.lblSubtitle
			});
			this.PageHeader.Height = 0.7291667F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Comments Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.5F;
			// 
			// lblRptTime
			// 
			this.lblRptTime.Height = 0.1875F;
			this.lblRptTime.HyperLink = null;
			this.lblRptTime.Left = 0F;
			this.lblRptTime.Name = "lblRptTime";
			this.lblRptTime.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblRptTime.Text = "Time";
			this.lblRptTime.Top = 0.1875F;
			this.lblRptTime.Width = 1.5F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.lblMuniName.Text = "MuniName";
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 1.5F;
			// 
			// lblPageNo
			// 
			this.lblPageNo.Height = 0.1875F;
			this.lblPageNo.HyperLink = null;
			this.lblPageNo.Left = 6.03125F;
			this.lblPageNo.Name = "lblPageNo";
			this.lblPageNo.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.lblPageNo.Text = "PageNo";
			this.lblPageNo.Top = 0.1875F;
			this.lblPageNo.Width = 1.5F;
			// 
			// lblRptDate
			// 
			this.lblRptDate.Height = 0.1875F;
			this.lblRptDate.HyperLink = null;
			this.lblRptDate.Left = 6.03125F;
			this.lblRptDate.Name = "lblRptDate";
			this.lblRptDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.lblRptDate.Text = "Date";
			this.lblRptDate.Top = 0F;
			this.lblRptDate.Width = 1.5F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.03125F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left; ddo-" + "char-set: 1";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 0.53125F;
			this.lblAccount.Width = 0.75F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.71875F;
			this.Line1.Width = 7.46875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.46875F;
			this.Line1.Y1 = 0.71875F;
			this.Line1.Y2 = 0.71875F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.875F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left; ddo-" + "char-set: 1";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.53125F;
			this.lblName.Width = 3.0625F;
			// 
			// lblLocation
			// 
			this.lblLocation.Height = 0.1875F;
			this.lblLocation.HyperLink = null;
			this.lblLocation.Left = 4.125F;
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left; ddo-" + "char-set: 1";
			this.lblLocation.Text = "Location";
			this.lblLocation.Top = 0.53125F;
			this.lblLocation.Width = 3.0625F;
			// 
			// lblSubtitle
			// 
			this.lblSubtitle.Height = 0.1875F;
			this.lblSubtitle.HyperLink = null;
			this.lblSubtitle.Left = 1.5F;
			this.lblSubtitle.Name = "lblSubtitle";
			this.lblSubtitle.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.lblSubtitle.Text = "Start Date";
			this.lblSubtitle.Top = 0.25F;
			this.lblSubtitle.Visible = false;
			this.lblSubtitle.Width = 4.5F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptCommentsReport
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptCommentsReport_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldComment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRptTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRptDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSubtitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldComment;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRptTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNo;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRptDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSubtitle;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
