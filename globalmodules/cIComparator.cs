﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public interface cIComparator
	{
		//=========================================================
		// -1 is less than, 0 is equal and 1 is greater than
		short Compare(ref object compareA, ref object compareB);
	}
}
