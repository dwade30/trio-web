﻿using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using fecherFoundation;
using Wisej.Web;

#if TWAR0000
using TWAR0000;


#elif TWBL0000
using TWBL0000;


#elif TWCE0000
using TWCE0000;
#endif
using GrapeCity.ActiveReports;
using System.IO;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for rptCustomBillTest.
	/// </summary>
	public partial class rptCustomBillTest : BaseSectionReport
	{
		//=========================================================
		string strModuleDB;
		// database to load custom format from
		string strThisModule;
		// Two character module name
		clsCustomBill CustomBillClass;
		// class that handles all module specific parts of the report
		int intUnitsType;
		// inches or centimeters
		bool boolChoosePrinter;
		// is the printer already chosen or do we need to prompt
		int intTwipsPerUnit;
		// 1440 if in inches, 567 if in centimeters
		// Dim aryFieldsToUpdate() As String   'An array of names of controls that aren't static
		// Dim lngFieldsIndex As Long
		// Dim aryExtraParameters() As String  'an array of parameters
		// Dim aryFieldCaptions() As String
		const int CNSTCUSTOMBILLUNITSINCHES = 0;
		const int CNSTCUSTOMBILLUNITSCENTIMETERS = 1;
		// any number > 0 is not a constant. It matches the id in the custombillcodes table
		const int CNSTCUSTOMBILLCUSTOMTEXT = -1;
		const int CNSTCUSTOMBILLCUSTOMRICHEDIT = -2;
		const int CNSTCUSTOMBILLCUSTOMIMAGE = -3;
		const int CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT = -4;
		// Jims text editor with variables etc.
		const int CNSTCUSTOMBILLCUSTOMRECTANGLE = -5;
		const int CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED = -6;
		const int CNSTCUSTOMBILLCUSTOMHORIZONTALLINE = -7;
		const int CNSTCUSTOMBILLCUSTOMVERTICALLINE = -8;
		// Private Const CNSTCUSTOMBILLCUSTOMSUBREPORT = -9
		// Private Const CNSTCUSTOMBILLCUSTOMBARCODE = -10
		// Private Const CNSTCUSTOMBILLCUSTOMAUTOPOP = -11
		const int CNSTCUSTOMBILLCUSTOMDATE = -12;
		const int CNSTCUSTOMBILLSIGNATURE = -13;
		const int CNSTCUSTOMBILLFONTSTYLEREGULAR = 0;
		const int CNSTCUSTOMBILLFONTSTYLEBOLD = 1;
		const int CNSTCUSTOMBILLFONTSTYLEITALIC = 2;
		const int CNSTCUSTOMBILLFONTSTYLEBOLDITALIC = 3;
		// define a type to make it easier to pass info to functions
		private struct CustomBillFieldType
		{
			public int ID;
			public int FieldNumber;
			public int FormatID;
			public int FieldID;
			public float lngTop;
			public float lngLeft;
			public float lngHeight;
			public float lngWidth;
			public int BillType;
			public string Description;
			public int intAlignment;
			public string UserText;
			public string strFont;
			public double dblFontSize;
			public int intFontStyle;
			public string ExtraParameters;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public CustomBillFieldType(int unusedParam)
			{
				this.ID = 0;
				this.FieldNumber = 0;
				this.FormatID = 0;
				this.FieldID = 0;
				this.lngHeight = 0;
				this.lngLeft = 0;
				this.lngTop = 0;
				this.lngWidth = 0;
				this.BillType = 0;
				this.Description = string.Empty;
				this.intAlignment = 0;
				this.UserText = string.Empty;
				this.strFont = string.Empty;
				this.dblFontSize = 0;
				this.ExtraParameters = string.Empty;
				this.intFontStyle = 0;
			}
		};

		private CustomBillFieldType CurrentFieldType = new CustomBillFieldType(0);

		private struct PrinterFontType
		{
			public string strfont10CPI;
			public string strFont12CPI;
			public string strFont17CPI;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public PrinterFontType(int unusedParam)
			{
				this.strFont12CPI = string.Empty;
				this.strfont10CPI = string.Empty;
				this.strFont17CPI = string.Empty;
			}
		};

		private PrinterFontType CurrentPrinterFont = new PrinterFontType(0);
		int woogy;

		public static rptCustomBillTest InstancePtr
		{
			get
			{
				return (rptCustomBillTest)Sys.GetInstance(typeof(rptCustomBillTest));
			}
		}

		protected rptCustomBillTest _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptCustomBillTest()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// EOF = CustomBillClass.EndOfFile
		}
		// vbPorter upgrade warning: CustomBillObject As object	OnWrite(clsCustomBill)
		public void Init(clsCustomBill CustomBillObject, bool boolPreview = true)
		{
			boolChoosePrinter = true;
			CustomBillClass = CustomBillObject;
			strThisModule = CustomBillClass.Module;
			strModuleDB = CustomBillClass.DBFile;
			if (Strings.Trim(CustomBillClass.PrinterName) != string.Empty)
			{
				this.Document.Printer.PrinterName = CustomBillClass.PrinterName;
				boolChoosePrinter = false;
			}
			// lngFieldsIndex = 0
			SetupFormat();
			SetupFields();
			if (boolPreview)
			{
				//if (boolChoosePrinter)
				//{
				frmReportViewer.InstancePtr.Init(this);
				//}
				//else
				//{
				//    frmReportViewer.InstancePtr.Init(this, this.Document.Printer.PrinterName);
				//}
			}
			else
			{
				this.Document.Print();
			}
		}

		private bool SetupFormat()
		{
			bool SetupFormat = false;
			// makes the page size and margins and might have to prompt for the printer
			clsDRWrapper clsLoad = new clsDRWrapper();
			int intReturn = 0;
            try
            {
                // On Error GoTo ErrorHandler
                SetupFormat = false;
                clsLoad.OpenRecordset("select * from custombills where ID = " + CustomBillClass.FormatID, strModuleDB);
                if (!clsLoad.EndOfFile())
                {
                    if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("islaser")))
                    {
                        CustomBillClass.DotMatrixFormat = false;
                    }
                    else
                    {
                        CustomBillClass.DotMatrixFormat = true;
                    }

                    intUnitsType = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int16("units"))));
                    if ((ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields_Double("PageWidth")),
                            FCConvert.ToInt16(intUnitsType)) != 8.5 * 1440) ||
                        (ConvertTwips_2(clsLoad.Get_Fields_Double("PageHeight"), FCConvert.ToInt16(intUnitsType)) !=
                         15840))
                    {
                        // not the standard 8.5 x 11 so change it
                        this.PageSettings.PaperWidth =
                            ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields_Double("PageWidth")),
                                FCConvert.ToInt16(intUnitsType));
                        this.PageSettings.PaperHeight = ConvertTwips_2(
                            Conversion.Val(clsLoad.Get_Fields_Double("PageHeight")), FCConvert.ToInt16(intUnitsType));
                        if (Strings.Trim(CustomBillClass.PrinterName) == string.Empty)
                        {
                            boolChoosePrinter = false;
                            this.Document.Printer.PrinterName = FCGlobal.Printer.DeviceName;
                        }

                        this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CustomFormat",
                            FCConvert.ToInt32(this.PageSettings.PaperWidth * 100),
                            FCConvert.ToInt32(this.PageSettings.PaperHeight * 100));

                    }

                    this.PageSettings.Margins.Left = ConvertTwips_2(
                        Conversion.Val(clsLoad.Get_Fields_Double("leftmargin")), FCConvert.ToInt16(intUnitsType));
                    // TODO: Field [Margins.Right] not found!! (maybe it is an alias?)
                    this.PageSettings.Margins.Right =
                        ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields("Margins.Right")),
                            FCConvert.ToInt16(intUnitsType));
                    // TODO: Field [Margins.Top] not found!! (maybe it is an alias?)
                    this.PageSettings.Margins.Top = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields("Margins.Top")),
                        FCConvert.ToInt16(intUnitsType));
                    // TODO: Field [Margins.Bottom] not found!! (maybe it is an alias?)
                    this.PageSettings.Margins.Bottom =
                        ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields("Margins.Bottom")),
                            FCConvert.ToInt16(intUnitsType));
                    this.PrintWidth = this.PageSettings.PaperWidth - this.PageSettings.Margins.Left -
                                      this.PageSettings.Margins.Right;
                    this.Detail.Height = this.PageSettings.PaperHeight - this.PageSettings.Margins.Top -
                                         this.PageSettings.Margins.Bottom;
                    if (CustomBillClass.DotMatrixFormat && CustomBillClass.UsePrinterFonts)
                    {
                        // load the printer fonts from the printer
                        clsReportPrinterFunctions prtObj = new clsReportPrinterFunctions();
                        CurrentPrinterFont.strfont10CPI = prtObj.GetFont(this.Document.Printer.PrinterName, 10, "");
                        CurrentPrinterFont.strFont12CPI = prtObj.GetFont(this.Document.Printer.PrinterName, 12, "");
                        CurrentPrinterFont.strFont17CPI = prtObj.GetFont(this.Document.Printer.PrinterName, 17, "");
                    }
                }
                else
                {
                    return SetupFormat;
                }

                SetupFormat = true;
                return SetupFormat;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In SetupFormat", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				clsLoad.DisposeOf();
            }
			return SetupFormat;
		}

		private bool SetupFields()
		{
			bool SetupFields = false;
			// dynamically creates the fields
			clsDRWrapper clsLoad = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                SetupFields = false;
                clsLoad.OpenRecordset(
                    "select * from CustomBillFields where formatid = " + CustomBillClass.FormatID +
                    " order by fieldnumber", strModuleDB);
                if (clsLoad.EndOfFile())
                    return SetupFields;
                while (!clsLoad.EndOfFile())
                {
                    // create a field
                    // fill the type to be passed to the function that actually creates the field
                    CurrentFieldType.ID = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ID"));
                    CurrentFieldType.FieldNumber = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("FieldNumber"));
                    // TODO: Check the table for the column [FormatID] and replace with corresponding Get_Field method
                    CurrentFieldType.FormatID = FCConvert.ToInt32(clsLoad.Get_Fields("FormatID"));
                    // TODO: Check the table for the column [FieldID] and replace with corresponding Get_Field method
                    CurrentFieldType.FieldID = FCConvert.ToInt32(clsLoad.Get_Fields("FieldID"));
                    CurrentFieldType.lngTop =
                        ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields_Double("Top")),
                            FCConvert.ToInt16(intUnitsType)) + CustomBillClass.VerticalAlignment;
                    CurrentFieldType.lngLeft = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields_Double("Left")),
                        FCConvert.ToInt16(intUnitsType));
                    CurrentFieldType.lngHeight = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields_Double("Height")),
                        FCConvert.ToInt16(intUnitsType));
                    // TODO: Check the table for the column [Width] and replace with corresponding Get_Field method
                    CurrentFieldType.lngWidth = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields("Width")),
                        FCConvert.ToInt16(intUnitsType));
                    // TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
                    CurrentFieldType.BillType =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("BillType"))));
                    CurrentFieldType.Description = FCConvert.ToString(clsLoad.Get_Fields_String("Description"));
                    CurrentFieldType.intAlignment = FCConvert.ToInt16(clsLoad.Get_Fields_Int16("Alignment"));
                    CurrentFieldType.UserText = FCConvert.ToString(clsLoad.Get_Fields_String("UserText"));
                    CurrentFieldType.strFont = FCConvert.ToString(clsLoad.Get_Fields_String("Font"));
                    // TODO: Check the table for the column [FontSize] and replace with corresponding Get_Field method
                    CurrentFieldType.dblFontSize = FCConvert.ToDouble(clsLoad.Get_Fields("FontSize"));
                    CurrentFieldType.intFontStyle =
                        FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int16("FontStyle"))));
                    CurrentFieldType.ExtraParameters = FCConvert.ToString(clsLoad.Get_Fields_String("ExtraParameters"));
                    CreateAField(ref CurrentFieldType.FieldNumber, ref CurrentFieldType.FieldID);
                    clsLoad.MoveNext();
                }

                SetupFields = true;
                return SetupFields;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In SetupFields", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				clsLoad.DisposeOf();
            }
			return SetupFields;
		}
		// vbPorter upgrade warning: intTypeUnits As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToDouble(
		private float ConvertTwips_2(double dblUnits, short intTypeUnits)
		{
			return ConvertTwips(ref dblUnits, ref intTypeUnits);
		}

		private float ConvertTwips(ref double dblUnits, ref short intTypeUnits)
		{
			float ConvertTwips = 0;
			// converts to twips from whatever units the format is in
			switch (intTypeUnits)
			{
				case CNSTCUSTOMBILLUNITSCENTIMETERS:
					{
						ConvertTwips = FCConvert.ToInt32(dblUnits * 567);
						break;
					}
				default:
					{
						//FC:FINAL:AM: don't convert to twips
						//ConvertTwips = FCConvert.ToInt32(dblUnits * 1440);
						ConvertTwips = FCConvert.ToSingle(dblUnits);
						break;
					}
			}
			//end switch
			return ConvertTwips;
		}

		private void CreateAField(ref int lngFldNum, ref int lngFldType)
		{
			// creates a field based on the info in lngfldtype
			dynamic ctl = null;
			dynamic ctl2 = null;
			string strTemp = "";
			int lngWidth = 0;
			int lngHeight = 0;
			double dblRatio = 0;
			bool boolStatic;
			bool boolUsesFont;
			clsDRWrapper clsTemp = new clsDRWrapper();
			boolUsesFont = false;
			boolStatic = true;
			GrapeCity.ActiveReports.SectionReportModel.Picture picture;
			GrapeCity.ActiveReports.SectionReportModel.Label label;
			GrapeCity.ActiveReports.SectionReportModel.Shape shape;
			GrapeCity.ActiveReports.SectionReportModel.Line line;
			if (lngFldType == CNSTCUSTOMBILLCUSTOMIMAGE)
			{
				picture = new GrapeCity.ActiveReports.SectionReportModel.Picture();
				picture.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
				picture.Height = CurrentFieldType.lngHeight;
				picture.Width = CurrentFieldType.lngWidth;
				picture.Top = CurrentFieldType.lngTop;
				picture.Left = CurrentFieldType.lngLeft;
				if (FCConvert.CBool(Strings.Trim(FCConvert.ToString(CurrentFieldType.UserText != string.Empty))))
				{
					if (File.Exists(CurrentFieldType.UserText))
					{
						picture.Image = FCUtils.LoadPicture(CurrentFieldType.UserText);
						lngWidth = picture.Image.Width;
						lngHeight = picture.Image.Height;
						dblRatio = FCConvert.ToDouble(lngWidth) / lngHeight;
						if (dblRatio * picture.Height > picture.Width)
						{
							// keep width, change height
							picture.Height = picture.Width / FCConvert.ToSingle(dblRatio);
						}
						else
						{
							// keep height, change width
							picture.Width = picture.Height * FCConvert.ToSingle(dblRatio);
						}
					}
				}
				ctl = picture;
				//Detail.Controls.Add(picture);
				// Case CNSTCUSTOMBILLSIGNATURE
				// Set ctl = Detail.Controls.Add("ddactivereports2.image")
				// ctl.SizeMode = ddSMZoom
				// ctl.Height = CurrentFieldType.lngHeight
				// ctl.Width = CurrentFieldType.lngWidth
				// ctl.Top = CurrentFieldType.lngTop
				// ctl.Left = CurrentFieldType.lngLeft
				// Dim tSig As New clsSignature
				// Set ctl.Picture = tSig.GetCurrentSignature
				// If Not ctl.Picture Is Nothing Then
				// lngWidth = ctl.Picture.Width
				// lngHeight = ctl.Picture.Height
				// dblRatio = lngWidth / lngHeight
				// If dblRatio * ctl.Height > ctl.Width Then
				// ctl.Height = ctl.Width / dblRatio
				// Else
				// ctl.Width = ctl.Height * dblRatio
				// End If
				// End If
			}
			else if (lngFldType == CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT)
			{
				// a static field
				label = new GrapeCity.ActiveReports.SectionReportModel.Label();
				label.Alignment = (GrapeCity.ActiveReports.Document.Section.TextAlignment)CurrentFieldType.intAlignment;
				clsTemp.OpenRecordset("select * from dynamicreports where ID = " + FCConvert.ToString(Conversion.Val(CurrentFieldType.UserText)), strModuleDB);
				if (!clsTemp.EndOfFile())
				{
					// TODO: Field [text] not found!! (maybe it is an alias?)
					label.Text = clsTemp.Get_Fields("text");
				}
				else
				{
					label.Text = "Dynamic Report";
				}
				label.Height = CurrentFieldType.lngHeight;
				label.Width = CurrentFieldType.lngWidth;
				label.Top = CurrentFieldType.lngTop;
				label.Left = CurrentFieldType.lngLeft;
				boolUsesFont = true;
				shape = new GrapeCity.ActiveReports.SectionReportModel.Shape();
				shape.Style = GrapeCity.ActiveReports.SectionReportModel.ShapeType.Rectangle;
				// rectangle
				shape.Top = label.Top;
				shape.Left = label.Left;
				shape.Width = label.Width;
				shape.Height = label.Height;
				//ctl2.BackStyle = ddBKTransparent;
				ctl = label;
				ctl2 = shape;
				//Detail.Controls.Add(label);
				//Detail.Controls.Add(shape);
			}
			else if (lngFldType == CNSTCUSTOMBILLCUSTOMRICHEDIT)
			{
				// not implemented yet.  doesn't seem to be a need for it yet
			}
			else if (lngFldType == CNSTCUSTOMBILLCUSTOMTEXT)
			{
				label = new GrapeCity.ActiveReports.SectionReportModel.Label();
				label.Alignment = (GrapeCity.ActiveReports.Document.Section.TextAlignment)CurrentFieldType.intAlignment;
				label.Text = CurrentFieldType.UserText;
				label.Height = CurrentFieldType.lngHeight;
				label.Width = CurrentFieldType.lngWidth;
				label.Top = CurrentFieldType.lngTop;
				label.Left = CurrentFieldType.lngLeft;
				boolUsesFont = true;
				shape = new GrapeCity.ActiveReports.SectionReportModel.Shape();
				shape.Style = GrapeCity.ActiveReports.SectionReportModel.ShapeType.Rectangle;
				// rectangle
				shape.Top = label.Top;
				shape.Left = label.Left;
				shape.Width = label.Width;
				shape.Height = label.Height;
				//ctl2.BackStyle = ddBKTransparent;
				ctl = label;
				ctl2 = shape;
				//Detail.Controls.Add(shape);
				//Detail.Controls.Add(label);
			}
			else if (lngFldType == CNSTCUSTOMBILLCUSTOMHORIZONTALLINE)
			{
				line = new GrapeCity.ActiveReports.SectionReportModel.Line();
				line.X1 = CurrentFieldType.lngLeft;
				line.Y1 = CurrentFieldType.lngTop;
				line.X2 = line.X1 + CurrentFieldType.lngWidth;
				line.Y2 = line.Y1;
				ctl = line;
				//Detail.Controls.Add(line);
			}
			else if (lngFldType == CNSTCUSTOMBILLCUSTOMVERTICALLINE)
			{
				line = new GrapeCity.ActiveReports.SectionReportModel.Line();
				line.X1 = CurrentFieldType.lngLeft;
				line.Y1 = CurrentFieldType.lngTop;
				line.X2 = line.X1;
				line.Y2 = line.Y1 + CurrentFieldType.lngHeight;
				ctl = line;
				//Detail.Controls.Add(line);
			}
			else if (lngFldType == CNSTCUSTOMBILLCUSTOMRECTANGLE)
			{
				shape = new GrapeCity.ActiveReports.SectionReportModel.Shape();
				shape.Style = GrapeCity.ActiveReports.SectionReportModel.ShapeType.Rectangle;
				// rectangle
				shape.Top = CurrentFieldType.lngTop;
				shape.Left = CurrentFieldType.lngLeft;
				shape.Height = CurrentFieldType.lngHeight;
				shape.Width = CurrentFieldType.lngWidth;
				//ctl.BackStyle = ddBKTransparent;
				ctl = shape;
				//Detail.Controls.Add(shape);
			}
			else if (lngFldType == CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED)
			{
				label = new GrapeCity.ActiveReports.SectionReportModel.Label();
				label.Alignment = (GrapeCity.ActiveReports.Document.Section.TextAlignment)CurrentFieldType.intAlignment;
				label.Text = CurrentFieldType.UserText;
				//label.Height = CurrentFieldType.lngHeight;
				label.Width = CurrentFieldType.lngWidth;
				label.Top = CurrentFieldType.lngTop;
				label.Left = CurrentFieldType.lngLeft;
				//ctl.BackStyle = ddBKNormal;
				label.BackColor = Color.Black;
				label.ForeColor = Color.White;
				boolUsesFont = true;
				ctl = label;
				//Detail.Controls.Add(label);
			}
			else
			{
				// default to non static label
				boolStatic = false;
				label = new GrapeCity.ActiveReports.SectionReportModel.Label();
				label.Alignment = (GrapeCity.ActiveReports.Document.Section.TextAlignment)CurrentFieldType.intAlignment;
				label.Height = CurrentFieldType.lngHeight;
				label.Width = CurrentFieldType.lngWidth;
				label.Top = CurrentFieldType.lngTop;
				label.Left = CurrentFieldType.lngLeft;
				label.Text = CurrentFieldType.Description;
				boolUsesFont = true;
				shape = new GrapeCity.ActiveReports.SectionReportModel.Shape();
				shape.Style = GrapeCity.ActiveReports.SectionReportModel.ShapeType.Rectangle;
				// rectangle
				shape.Top = label.Top;
				shape.Left = label.Left;
				shape.Width = label.Width;
				shape.Height = label.Height;
				//ctl2.BackStyle = ddBKTransparent;
				ctl = label;
				ctl2 = shape;
				//Detail.Controls.Add(label);
				//Detail.Controls.Add(shape);
			}
			if (boolUsesFont)
			{
				string fontName = CurrentFieldType.strFont;
				float fontSize;
				FontStyle fontStyle = FontStyle.Regular;
				if (CustomBillClass.DotMatrixFormat && CustomBillClass.UsePrinterFonts)
				{
					if (CurrentFieldType.dblFontSize >= 12)
					{
						// 10 cpi
						if (Strings.Trim(CurrentPrinterFont.strfont10CPI) != string.Empty)
						{
							fontName = CurrentPrinterFont.strfont10CPI;
						}
					}
					else if (CurrentFieldType.dblFontSize < 10)
					{
						// 17 cpi
						if (Strings.Trim(CurrentPrinterFont.strFont17CPI) != string.Empty)
						{
							fontName = CurrentPrinterFont.strFont17CPI;
						}
					}
					else
					{
						// 12 cpi
						if (Strings.Trim(CurrentPrinterFont.strFont12CPI) != string.Empty)
						{
							fontName = CurrentPrinterFont.strFont12CPI;
						}
					}
					fontSize = 10;
					// must always be 10 when using printer fonts
				}
				else
				{
					fontSize = FCConvert.ToSingle(CurrentFieldType.dblFontSize);
				}
				switch (CurrentFieldType.intFontStyle)
				{
					case CNSTCUSTOMBILLFONTSTYLEBOLD:
						{
							//ctl.Font.Bold = true;
							//ctl.Font.Italic = false;
							fontStyle = FontStyle.Bold;
							break;
						}
					case CNSTCUSTOMBILLFONTSTYLEBOLDITALIC:
						{
							//ctl.Font.Bold = true;
							//ctl.Font.Italic = true;
							fontStyle = FontStyle.Bold | FontStyle.Italic;
							break;
						}
					case CNSTCUSTOMBILLFONTSTYLEITALIC:
						{
							//ctl.Font.Bold = false;
							//ctl.Font.Italic = true;
							fontStyle = FontStyle.Italic;
							break;
						}
					default:
						{
							// regular
							//ctl.Font.Bold = false;
							//ctl.Font.Italic = false;
							fontStyle = FontStyle.Regular;
							break;
						}
				}
				//end switch
				ctl.Font = new Font(fontName, fontSize, fontStyle);
			}
			if (ctl != null)
			{
				Detail.Controls.Add(ctl);
			}
			if (ctl2 != null)
			{
				Detail.Controls.Add(ctl2);
			}
			clsTemp.DisposeOf();
			
		}

		private void rptCustomBillTest_Load(object sender, System.EventArgs e)
		{
			
		}
	}
}
