﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Collections.Generic;
using fecherFoundation;
using Wisej.Web;

namespace Global
{
	public class BackupAndRestoreService : IBackupAndRestoreService
	{

		int _CurrentProgress;

		void IBackupAndRestoreService.Backup(dbConnInfo tConnectionInfo, ref string strError, bool blnShowError = true, string strBackupPath = "", bool boolCopyOnly = false)
		{
			try
			{
				BackupArgs targs = new BackupArgs();

				targs.dbInfo = tConnectionInfo;
				targs.strErr = strError;
				targs.strBackupPath = strBackupPath;
				targs.BlnShowErrorMessage = blnShowError;
				targs.boolCopyOnly = boolCopyOnly;
				MakeBackup(ref targs);
			}
			catch (Exception ex)
			{
				BackupCompleteArgs eArgs = new BackupCompleteArgs();
				eArgs.ErrorMessage = ex.Message;
				eArgs.Success = false;
				if (BackupCompleted != null)
				{
					BackupCompleted(this, eArgs);
				}
			}
		}

		private class BackupArgs
		{
			public dbConnInfo dbInfo = null;
			public string strBackupPath = "";
			public string strErr = string.Empty;
			public bool boolCopyOnly = false;
			public bool BlnShowErrorMessage;
		}

		private class RestoreArgs
		{
			public DatabaseInfo dbInfo;
			public int intFileNumber = 0;
			public string strErr = string.Empty;
			public bool BlnShowErrorMessage;
		}

		void IBackupAndRestoreService.Restore(DatabaseInfo temp, ref string strError, int intFileNumber = -1, bool blnShowError = true)
		{
			RestoreArgs targs = new RestoreArgs();

			targs.dbInfo = temp;
			targs.strErr = strError;
			targs.intFileNumber = intFileNumber;
			targs.BlnShowErrorMessage = blnShowError;
			MakeRestore(targs);
		}

		string IBackupAndRestoreService.GetBackupPath(string strConnectionString)
		{
			string strPath = Application.StartupPath + "\\backups";
			
			return strPath;
		}

		string IBackupAndRestoreService.GetDataPath(string strConnectionString)
		{
			string strPath = "";
			try
			{

				SqlConnection tConn = new SqlConnection(strConnectionString);
				string strSQL = "select * from sys.database_files where type = 0";
				SqlDataAdapter tAdapter = new SqlDataAdapter(strSQL, tConn);
				if (tAdapter != null)
				{
					DataTable dt = new DataTable();
					tAdapter.Fill(dt);
					if (dt.Rows.Count > 0)
					{
						string strTemp = dt.Rows[0]["physical_name"].ToString();
						strPath = Path.GetDirectoryName(strTemp);
					}
				}
			}
			catch (Exception ex)
			{
			}
			return strPath;
		}
		List<RestoreFileDetails> IBackupAndRestoreService.GetBackupHeaders(string strConnectionString)
		{
			try
			{
				string strBackupPath;
				strBackupPath = (this as IBackupAndRestoreService).GetBackupPath(strConnectionString);
				SqlConnectionStringBuilder sbld = new SqlConnectionStringBuilder(strConnectionString);
				string strUser = sbld.UserID;
				string strPassword = sbld.Password;
				string strDataSource = sbld.DataSource;
				strBackupPath = strBackupPath + "\\" + sbld.InitialCatalog + ".bak";
				return (this as IBackupAndRestoreService).GetBackupHeadersFromFile(strConnectionString, strBackupPath);

			}
			catch (Exception ex)
			{
				return null;
			}
		}

		private string GetRestoreScript(string strDBName, int intFileNumber, string strRestorePath)
		{
			System.Text.StringBuilder strSQL = new System.Text.StringBuilder();
			strSQL.Append("RESTORE DATABASE ");
			strSQL.Append(strDBName);
			strSQL.Append(" From DISK = '" + strRestorePath + "'");
			strSQL.Append(" With FILE = " + intFileNumber + ", NORECOVERY");
			return strSQL.ToString();
		}

		private void MakeRestore(RestoreArgs restInfo)
		{
			RestoreCompleteArgs mess = new RestoreCompleteArgs();
			try
			{
				bool boolAsNew = false;
				string strRestore = "";
				SqlConnectionStringBuilder tSQL = new SqlConnectionStringBuilder();
				string strDataPath = "";
				tSQL.DataSource = restInfo.dbInfo.DataSource;
				tSQL.UserID = restInfo.dbInfo.User;
				tSQL.Password = restInfo.dbInfo.Password;
				tSQL.InitialCatalog = "Master";
				tSQL.AsynchronousProcessing = true;
				if (restInfo.dbInfo.DestinationDatabase != restInfo.dbInfo.DatabaseName & restInfo.dbInfo.DestinationDatabase != "")
				{
					boolAsNew = true;
				}
				if (restInfo.intFileNumber < 1)
				{
					//use newest
					restInfo.intFileNumber = 1;
					List<RestoreFileDetails> tColl;
					if (restInfo.dbInfo.RestorePath != "")
					{
						tColl = (this as IBackupAndRestoreService).GetBackupHeadersFromFile(tSQL.ToString(), restInfo.dbInfo.RestorePath);
					}
					else
					{
						tColl = (this as IBackupAndRestoreService).GetBackupHeaders(tSQL.ToString());
					}
					System.DateTime tDate = new DateTime(2000, 1, 1);
					if (tColl != null)
					{
						foreach (RestoreFileDetails tRFD in tColl)
						{
							if (System.DateTime.Compare(tDate, tRFD.BackupDate) < 0)
							{
								tDate = tRFD.BackupDate;
								restInfo.intFileNumber = tRFD.FileNumber;
							}
						}
					}
					else
					{
						restInfo.intFileNumber = 1;
					}
				}
				
				strDataPath = (this as IBackupAndRestoreService).GetDataPath(tSQL.ToString());
				strRestore = "RESTORE DATABASE [" + restInfo.dbInfo.DestinationDatabase + "] FROM DISK = N'" + restInfo.dbInfo.RestorePath + "' WITH FILE = " + restInfo.intFileNumber + ", MOVE N'" + restInfo.dbInfo.DatabaseName + "' TO N'" + strDataPath + "\\" + restInfo.dbInfo.DestinationDatabase + ".mdf', MOVE N'" + restInfo.dbInfo.DatabaseName + "_log' TO N'" + strDataPath + "\\" + restInfo.dbInfo.DestinationDatabase + "_log.ldf', REPLACE, STATS=10";
				using (SqlConnection tConn = new SqlConnection(tSQL.ConnectionString))
				{
					tConn.Open();
					SqlCommand tComm = new SqlCommand(strRestore, tConn);
					tComm.ExecuteNonQuery();
					tConn.Close();
				}
				mess.Success = true;
				string strSQL = "";
				using (SqlConnection tConn = new SqlConnection(tSQL.ConnectionString))
				{
					tConn.Open();
					strSQL = "alter database " + restInfo.dbInfo.DestinationDatabase + " modify file(NAME='" + restInfo.dbInfo.DatabaseName + "',NEWNAME='" + restInfo.dbInfo.DestinationDatabase + "')";
					SqlCommand tComm = new SqlCommand(strSQL, tConn);
					tComm.ExecuteNonQuery();
					strSQL = "alter database " + restInfo.dbInfo.DestinationDatabase + " modify file(NAME='" + restInfo.dbInfo.DatabaseName + "_log',NEWNAME='" + restInfo.dbInfo.DestinationDatabase + "_log')";
					tComm.CommandText = strSQL;
					tComm.ExecuteNonQuery();
					tConn.Close();
				}
				//End If
			}
			catch (Exception ex)
			{
				mess.Success = false;
				mess.ErrorMessage = ex.Message;
			}
			if (RestoreCompleted != null)
			{
				RestoreCompleted(this, mess);
			}
		}
		private string ScriptBackup(string strDBName, string strBackupPath, bool boolCopyOnly)
		{
			System.Text.StringBuilder strSQL = new System.Text.StringBuilder();
			try
			{
				strSQL.Append("Backup Database " + strDBName + " ");
				strSQL.Append(" TO DISK = '" + strBackupPath + "' ");
				strSQL.Append(" WITH NOINIT ");
				if (boolCopyOnly)
				{
					strSQL.Append(", COPY_ONLY ");
				}
				strSQL.Append(", Name = '" + strDBName + " Backup'");
				strSQL.Append(", Description = 'Full backup of " + strDBName + "'");

			}
			catch (Exception ex)
			{
			}
			return strSQL.ToString();
		}

		private string GetScriptRestoreHeader(string strBackupPath)
		{
			System.Text.StringBuilder strSQL = new System.Text.StringBuilder();
			strSQL.Append("Restore HeaderOnly From DISK = N'" + strBackupPath + "' with NOUNLOAD");
			return strSQL.ToString();
		}

		private string GetDefaultBackupPath(string strConnectionString)
		{
			try
			{
				SqlConnectionStringBuilder sbld = new SqlConnectionStringBuilder(strConnectionString);
				SqlConnection tConn = new SqlConnection(strConnectionString);
				string strSQL = "declare @regreader int, @directory nvarchar(4000);exec @regreader = master.dbo.xp_instance_regread  N'HKEY_LOCAL_MACHINE',    N'Software\\Microsoft\\MSSQLServer\\MSSQLServer',    N'BackupDirectory',   @directory output, 'no_output';select @directory AS BackupDirectory";
				SqlDataAdapter tAdapter = new SqlDataAdapter(strSQL, tConn);
				string strReturn = "";
				if (tAdapter != null)
				{
					DataTable dt = new DataTable();
					RestoreFileDetails tDet = null;
					tAdapter.Fill(dt);
					if (dt.Rows.Count > 0)
					{
						strReturn = dt.Rows[0]["BackupDirectory"].ToString();
					}
				}
				return strReturn;
			}
			catch (Exception ex)
			{
				return "";
			}
		}

		List<RestoreFileDetails> IBackupAndRestoreService.GetBackupHeadersFromFile(string strConnectionString, string strBackupPath)
		{
			List<RestoreFileDetails> tColl = new List<RestoreFileDetails>();
			try
			{
				SqlConnectionStringBuilder sbld = new SqlConnectionStringBuilder(strConnectionString);
				string strSQL = GetScriptRestoreHeader(strBackupPath);
				SqlConnection tConn = new SqlConnection(strConnectionString);
				SqlDataAdapter tAdapter = new SqlDataAdapter(strSQL, tConn);
				if (tAdapter != null)
				{
					DataTable dt = new DataTable();
					RestoreFileDetails tDet = null;
					tAdapter.Fill(dt);
					
					foreach (DataRow tRow in dt.Rows)
					{
						tDet = new RestoreFileDetails();
						tDet.FileNumber = FCConvert.ToInt32(tRow["position"]);
						tDet.BackupDate = (DateTime)tRow["BackupStartDate"];
						switch (FCConvert.ToInt32(tRow["BackupType"]))
						{
							case 1:
								tDet.BackupType = "Full";
								break;
							default:
								tDet.BackupType = "Incremental";
								break;
						}
						tDet.Description = tRow["BackupName"].ToString();
						tColl.Add(tDet);
					}
				}
				return tColl;
			}
			catch (Exception ex)
			{
				tColl.Clear();
				return (tColl);
			}
		}

		private void GetBackupsScript()
		{
			System.Text.StringBuilder strSQL = new System.Text.StringBuilder();
			strSQL.Append("SELECT A.database_name, B.physical_device_name ");
			strSQL.Append(" ,A.media_set_id,A.backup_size,");
			strSQL.Append(" A.backup_start_date, A.backup_finish_date");
			strSQL.Append(" FROM msdb.dbo.backupset A ");
			strSQL.Append(" INNER JOIN msdb.dbo.backupmediafamily B ");
			strSQL.Append(" ON A.media_set_id = B.media_set_id ");
			strSQL.Append(" ORDER BY A.backup_finish_date DESC");
		}

		private SqlConnection tConn = null;

		public event EventHandler<BackupCompleteArgs> BackupCompleted;
		public event EventHandler<BackupPercentageArgs> BackupPercentageComplete;
		public event EventHandler<RestoreCompleteArgs> RestoreCompleted;
		public event EventHandler<RestorePercentageArgs> RestorePercentageComplete;

		private void MakeBackup(ref BackupArgs backInfo)
		{
			try
			{
				SqlConnectionStringBuilder tstr = new SqlConnectionStringBuilder(backInfo.dbInfo.ConnectionString);
				string strInitCat = backInfo.dbInfo.Items["InitialCatalog"];
				tstr.AsynchronousProcessing = true;
				tConn = new SqlConnection(tstr.ConnectionString);
				string strSQL = "";
				string strpath = "";
				if (backInfo.strBackupPath != "")
				{
					strpath = backInfo.strBackupPath;
				}
				else
				{
					strpath = (this as IBackupAndRestoreService).GetBackupPath(tstr.ConnectionString);
					strpath = strpath + "\\" + strInitCat + ".BAK";
				}
				strSQL = ScriptBackup(strInitCat, strpath, backInfo.boolCopyOnly);
				SqlCommand tCommand = new SqlCommand(strSQL, tConn);
				tConn.Open();
				BackupPercentageArgs tArgs = new BackupPercentageArgs();
				tArgs.PercentComplete = 0;
				if (BackupPercentageComplete != null)
				{
					BackupPercentageComplete(this, tArgs);
				}
				AsyncCallback callback = new AsyncCallback(HandleBackupCallback);
				tCommand.EndExecuteNonQuery(tCommand.BeginExecuteNonQuery());
				BackupCompleteArgs mess = new BackupCompleteArgs();
				mess.Success = true;
				if (BackupCompleted != null)
				{
					BackupCompleted(this, mess);
				}
			}
			catch (Exception ex)
			{
				if (tConn != null)
				{
					if (tConn.State == ConnectionState.Open)
					{
						tConn.Close();
					}
					tConn.Dispose();
					tConn = null;
				}
				BackupCompleteArgs mess = new BackupCompleteArgs();
				mess.ErrorMessage = ex.Message;
				mess.Success = false;
				if (BackupCompleted != null)
				{
					BackupCompleted(this, mess);
				}
			}
		}

		private void HandleBackupCallback(IAsyncResult result)
		{
			BackupCompleteArgs mess = new BackupCompleteArgs();
			try
			{
				SqlCommand command = (SqlCommand)result.AsyncState;
				command.EndExecuteNonQuery(result);
				mess.Success = true;
			}
			catch (Exception ex)
			{
				mess.Success = false;
				mess.ErrorMessage = ex.Message;
			}
			finally
			{
				if (tConn != null)
				{
					if (tConn.State == ConnectionState.Open)
					{
						tConn.Close();
					}
					tConn.Dispose();
					tConn = null;
				}
			}
			if (BackupCompleted != null)
			{
				BackupCompleted(this, mess);
			}
		}

		private delegate void RaiseBackupCompleteDelegate(BackupCompleteArgs mess);

		private void RaiseBackupCompleteInterface(BackupCompleteArgs mess)
		{
			if (BackupCompleted != null)
			{
				BackupCompleted(this, mess);
			}
		}
	}

	public class DatabaseInfoCollection
	{
		private System.Collections.ObjectModel.Collection<DatabaseInfo> tColl = new System.Collections.ObjectModel.Collection<DatabaseInfo>();
		private int intCurrent = -1;

		public void Clear()
		{
			tColl.Clear();
		}

		public int Count
		{
			get
			{
				return tColl.Count;
			}
		}

		public void Add(ref DatabaseInfo tInfo)
		{
			if (tInfo != null)
			{
				tColl.Add(tInfo);
			}
		}

		public DatabaseInfo Current()
		{
			if (intCurrent >= 0)
			{
				if (intCurrent < tColl.Count)
				{
					return tColl[intCurrent];
				}
			}
			return null;
		}

		public bool IsCurrent()
		{
			if (intCurrent < 0)
			{
				return false;
			}
			if (tColl.Count < 1)
			{
				return false;
			}
			if (intCurrent >= tColl.Count)
			{
				return false;
			}
			return true;
		}

		public bool MoveFirst()
		{
			if (tColl.Count > 0)
			{
				intCurrent = 0;
				return true;
			}
			else
			{
				intCurrent = -1;
				return false;
			}
		}

		public bool MoveLast()
		{
			if (tColl.Count > 0)
			{
				intCurrent = tColl.Count - 1;
				return true;
			}
			else
			{
				intCurrent = -1;
				return false;
			}
		}

		public bool MoveNext()
		{
			if (tColl.Count > 0)
			{
				if (intCurrent + 1 < tColl.Count)
				{
					intCurrent = intCurrent + 1;
					return true;
				}
				else
				{
					intCurrent = -1;
					return false;
				}
			}
			else
			{
				intCurrent = -1;
				return false;
			}
		}

		public bool MovePrevious()
		{
			if (tColl.Count > 0)
			{
				if (intCurrent - 1 > 0 & intCurrent - 1 < tColl.Count)
				{
					intCurrent = intCurrent - 1;
					return true;
				}
				intCurrent = -1;
				return false;
			}
			else
			{
				intCurrent = -1;
				return false;
			}
		}
	}

}
