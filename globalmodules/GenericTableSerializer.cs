﻿using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using TWSharedLibrary;

namespace Global
{
	//[ComClass(GenericJSONTableSerializer.ClassId, GenericJSONTableSerializer.InterfaceId, GenericJSONTableSerializer.EventsId)]
	public class GenericJSONTableSerializer
	{
		#region "COM GUIDs"
		// These  GUIDs provide the COM identity for this class
		// and its COM interfaces. If you change them, existing
		// clients will no longer be able to access the class.
		public const string ClassId = "CE8655C7-2D78-3C21-BCAE-156A30B0DC19";
		public const string InterfaceId = "2AB79F20-3AEB-46A4-9CC1-61C960ABE35A";
		#endregion

		public const string EventsId = "51D1A87E-CD53-470A-81AF-9AC8E5CD9C8F";

		public TableCollection GetCollectionFromJSON(string strJSON)
		{
			try
			{
				if (strJSON != null)
				{
					System.Web.Script.Serialization.JavaScriptSerializer tSer = new System.Web.Script.Serialization.JavaScriptSerializer();
					return tSer.Deserialize<TableCollection>(strJSON);
				}
				else
				{
					return null;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return null;
			}
		}

		public TableItem GetTableItemFromJSON(string strJSON)
		{
			try
			{
				if (strJSON != null)
				{
					System.Web.Script.Serialization.JavaScriptSerializer tSer = new System.Web.Script.Serialization.JavaScriptSerializer();
					return tSer.Deserialize<TableItem>(strJSON);
				}
				else
				{
					return null;
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return null;
			}
		}

		public string GetJSONFromCollection(ref TableCollection tblColl)
		{
			try
			{
				if (tblColl != null)
				{
					System.Web.Script.Serialization.JavaScriptSerializer tSer = new System.Web.Script.Serialization.JavaScriptSerializer();
					return tSer.Serialize(tblColl);
				}
				else
				{
					return "";
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return "";
			}
		}

		public string GetJSONFromTableItem(TableItem tblItem)
		{
			try
			{
				if (tblItem != null)
				{
					System.Web.Script.Serialization.JavaScriptSerializer tSer = new System.Web.Script.Serialization.JavaScriptSerializer();
					return tSer.Serialize(tblItem);
				}
				else
				{
					return "";
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				return "";
			}
		}
		//Public Function GetJSonFromObject(ByRef tOb As Object) As String
		//    Try
		//        If Not tOb Is Nothing Then
		//            Dim tSer As New System.Web.Script.Serialization.JavaScriptSerializer
		//            Return tSer.Serialize(tOb)
		//        Else
		//            Return ""
		//        End If
		//    Catch ex As Exception
		//        Return ""
		//    End Try
		//End Function
		//Public Sub GetObjectFromJSON(ByVal strJSON As String, ByRef tOb As Object)
		//    Try
		//        If Not strJSON = "" Then
		//            Dim tSer As New System.Web.Script.Serialization.JavaScriptSerializer
		//            Dim tType As System.Type = tOb.GetType
		//            tOb = Nothing
		//            Dim test As GenericInvoker
		//            test = DynamicMethods.GenericMethodInvokerMethod(Me.GetType, "GetExportedValue", {tType})
		//            tOb = test(Me, {strJSON})
		//        Else
		//            tOb = Nothing
		//        End If
		//    Catch ex As Exception
		//        tOb = Nothing
		//    End Try
		//End Sub
		//<ComVisible(False)>
		//Public Function GetGenericObjectFromJSON(Of T)(ByVal strJSON As String) As Object
		//    Try
		//        If Not strJSON = "" Then
		//            Dim tSer As New System.Web.Script.Serialization.JavaScriptSerializer
		//            Return tSer.Deserialize(Of T)(strJSON)
		//        Else
		//            Return Nothing
		//        End If
		//    Catch ex As Exception
		//        Return Nothing
		//    End Try
		//End Function
	}
	//[ComClass(TableCollection.ClassId, TableCollection.InterfaceId, TableCollection.EventsId)]
	public class TableCollection
	{
		#region "COM GUIDs"
		// These  GUIDs provide the COM identity for this class
		// and its COM interfaces. If you change them, existing
		// clients will no longer be able to access the class.
		public const string ClassId = "81A2D5E5-6EAB-3D87-8CA7-3E509EEBE144";
		public const string InterfaceId = "0718D3FF-21CC-4702-9C5D-42CA5DB7AD75";
		#endregion

		public const string EventsId = "762AC4F9-9C42-4DD9-A989-18BCB830402A";
		private string _DBName = "";

		public string DBName
		{
			get
			{
				return _DBName;
			}
			set
			{
				_DBName = value;
			}
		}

		public System.Collections.ObjectModel.Collection<TableItem> TableItems
		{
			get
			{
				return m_TableItems;
			}
			set
			{
				m_TableItems = value;
			}
		}

		private System.Collections.ObjectModel.Collection<TableItem> m_TableItems;
		//FC:FINAL:MSH - i.issue #1140: Collection initializing
		public TableCollection()
		{
			m_TableItems = new System.Collections.ObjectModel.Collection<TableItem>();
		}

		public TableItem Item(string strTableName)
		{
			foreach (TableItem tItem in TableItems)
			{
				if (tItem.TableName.ToLower() == strTableName.ToLower())
				{
					return tItem;
				}
			}
			return null;
		}

		public void AddItem(TableItem tItem)
		{
			if (tItem != null)
			{
				TableItems.Add(tItem);
			}
		}

		public int TableCount()
		{
			if (TableItems != null)
			{
				return TableItems.Count;
			}
			else
			{
				return 0;
			}
		}

		public string TableName(int intIndex)
		{
			if (TableItems != null)
			{
				return TableItems[intIndex].TableName;
			}
			else
			{
				return "";
			}
		}
		//Private _TableItems As New Collection
		//Public Property TableItems As Collection
		//    Get
		//        Return _TableItems
		//    End Get
		//    Set(value As Collection)
		//        _TableItems = value
		//    End Set
		//End Property
	}
	//[ComClass(TableItem.ClassId, TableItem.InterfaceId, TableItem.EventsId)]
	public class TableItem
	{
		#region "COM GUIDs"
		// These  GUIDs provide the COM identity for this class
		// and its COM interfaces. If you change them, existing
		// clients will no longer be able to access the class.
		public const string ClassId = "C7FEE113-0304-3450-87F2-F83AEBB2156D";
		public const string InterfaceId = "C7971E02-2CC4-46BD-813F-4E0A65CFCE0C";
		#endregion

		public const string EventsId = "A205A035-BC68-4354-98FD-C9F4BB6DD872";
		private string _TableName = "";

		public string TableName
		{
			get
			{
				return _TableName;
			}
			set
			{
				_TableName = value;
			}
		}
		//Private _Records As New System.Collections.ObjectModel.Collection(Of RecordItem)
		public System.Collections.ObjectModel.Collection<RecordItem> Records
		{
			get
			{
				return m_Records;
			}
			set
			{
				m_Records = value;
			}
		}

		private System.Collections.ObjectModel.Collection<RecordItem> m_Records;
		//FC:FINAL:MSH - i.issue #1140: Collection initializing
		public TableItem()
		{
			m_Records = new System.Collections.ObjectModel.Collection<RecordItem>();
		}

		public void AddItem(ref RecordItem tItem)
		{
			if (tItem != null)
			{
				Records.Add(tItem);
			}
		}

		public RecordItem GetRecord(int intIndex)
		{
			if (intIndex >= 0)
			{
				if (Records.Count > intIndex)
				{
					return Records[intIndex];
				}
			}
			return null;
		}

		public int RecordCount()
		{
			return Records.Count;
		}
	}
	//[ComClass(RecordItem.ClassId, RecordItem.InterfaceId, RecordItem.EventsId)]
	public class RecordItem
	{
		#region "COM GUIDs"
		// These  GUIDs provide the COM identity for this class
		// and its COM interfaces. If you change them, existing
		// clients will no longer be able to access the class.
		public const string ClassId = "147B98C6-1B77-33F4-984C-9CBD3F5F667A";
		public const string InterfaceId = "C2487A86-6088-46B6-B7AF-A738773BCFE6";
		#endregion

		public const string EventsId = "A5C62A59-C78E-4AEA-AB1A-E863FDC000CB";
		// Private _Fields As New Dictionary(Of String, String)
		public Dictionary<string, string> Fields
		{
			get
			{
				return m_Fields;
			}
			set
			{
				m_Fields = value;
			}
		}

		private Dictionary<string, string> m_Fields;
		//FC:FINAL:MSH - i.issue #1140: Dictionary initializing
		public RecordItem()
		{
			m_Fields = new Dictionary<string, string>();
		}

		public void SetItem(string strName, string strValue)
		{
			Fields[strName] = strValue;
		}

		public string GetItem(string strName)
		{
			return Fields[strName];
		}

		public int FieldsCount()
		{
			return Fields.Count;
		}

		public string FieldName(int intIndex)
		{
			if (Fields.Count > intIndex)
			{
				//FC:COMP:JEI
				//return Fields[intIndex].Key;
				int index = 0;
				foreach (KeyValuePair<string, string> item in Fields)
				{
					if (intIndex == index)
					{
						return item.Key;
					}
					index++;
				}
				return string.Empty;
			}
			else
			{
				return "";
			}
		}
	}
}
