﻿using Wisej.Web;
using fecherFoundation;

#if TWCK0000
using TWCK0000;


#elif TWPY0000
using TWPY0000;
#endif
namespace Global
{
	public class clsMousePointer
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Matthew Larrabee        *
		// Date           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               11/17/2002              *
		// ********************************************************
		// VBto upgrade warning: strTemp As Variant --> As string()
		string[] strTemp = null;

		public clsMousePointer() : base()
		{
			modErrorHandler.Statics.gstrStack += ";;" + modErrorHandler.Statics.gstrFormName + "." + modErrorHandler.Statics.gstrCurrentRoutine;
			// Screen.MousePointer = vbHourglass
		}

		~clsMousePointer()
		{
			strTemp = Strings.Split(modErrorHandler.Statics.gstrStack, ";;", -1, CompareConstants.vbBinaryCompare);
			if (Information.UBound(strTemp, 1) >= 0)
			{
				modErrorHandler.Statics.gstrStack = modErrorHandler.Statics.gstrStack.Replace(";;" + strTemp[Information.UBound(strTemp, 1)], "");
			}
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
		}
	}
}
