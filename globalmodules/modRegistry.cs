﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Runtime.InteropServices;
using Wisej.Core;
using System.Text;

namespace Global
{
	public class modRegistry
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :                                       *
		// Date           :                                       *
		// *
		// MODIFIED BY    :                                       *
		// Last Updated   :               11/17/2002              *
		// ********************************************************
		// Reg ID Security Options...
		// Public Const ID_READ = ((STANDARD_RIGHTS_READ Or ID_QUERY_VALUE Or ID_ENUMERATE_SUB_KEYS Or ID_NOTIFY) And (Not Synchronize))
		public const uint READ_CONTROL = 0x20000;
		public const int ID_QUERY_VALUE = 0x1;
		public const int ID_SET_VALUE = 0x2;
		public const int ID_CREATE_SUB_ID = 0x4;
		public const int ID_ENUMERATE_SUB_KEYS = 0x8;
		public const int ID_NOTIFY = 0x10;
		public const int ID_CREATE_LINK = 0x20;
		public const uint ID_ALL_ACCESS = ID_QUERY_VALUE + ID_SET_VALUE + ID_CREATE_SUB_ID + ID_ENUMERATE_SUB_KEYS + ID_NOTIFY + ID_CREATE_LINK + READ_CONTROL;
		// Open/Create Optionsf
		const int REG_OPTION_NON_VOLATILE = 0;
		const int REG_OPTION_VOLATILE = 0x1;
		// Structures Needed For Registry Prototypes
		public struct SECURITY_ATTRIBUTES
		{
			public int nLength;
			public int lpSecurityDescriptor;
			public bool bInheritHandle;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public SECURITY_ATTRIBUTES(int unusedParam)
			{
				this.nLength = 0;
				this.lpSecurityDescriptor = 0;
				this.bInheritHandle = false;
			}
		};

		

		public static cSettingsController setCont
		{
			get
			{
				if (Statics.setCont_AutoInitialized == null)
				{
					Statics.setCont_AutoInitialized = new cSettingsController();
				}
				return Statics.setCont_AutoInitialized;
			}
			set
			{
				Statics.setCont_AutoInitialized = value;
			}
		}
		//VBtoInfo: 0/2
		[DllImport("advapi32.dll", EntryPoint = "GetUserNameA", SetLastError = true)]
		public static extern bool GetUserName(System.Text.StringBuilder sb, ref Int32 length);
		//VBtoInfo: 1
		public static int GetUserNameWrp(ref string lpBuffer, ref int nSize)
		{
			StringBuilder sb = new StringBuilder(nSize);
			bool Ok = GetUserName(sb, ref nSize);
			lpBuffer = sb.ToString();
			return nSize;
		}

		
		
		
		public static bool SaveKeyValue(uint KeyRoot, string KeyName, string SubKeyRef, string KeyVal)
		{
			setCont.SaveSetting(KeyVal, SubKeyRef, "", TWSharedLibrary.Variables.Statics.OwnerType, TWSharedLibrary.Variables.Statics.OwnerID, "");
			return true;
		}

		public static bool GetKeyValues2(uint KeyRoot, string KeyName, string SubKeyRef, string KeyVal)
		{
			return GetKeyValues(KeyRoot, KeyName, SubKeyRef, ref KeyVal);
		}

		public static bool GetKeyValues(uint KeyRoot, string KeyName, string SubKeyRef, ref string KeyVal)
		{
			
			KeyVal = setCont.GetSettingValue(SubKeyRef, "", TWSharedLibrary.Variables.Statics.OwnerType, TWSharedLibrary.Variables.Statics.OwnerID, "");
			return true;
		}

        // VBto upgrade warning: strDefaultValue As string	OnWrite(bool)
        // VBto upgrade warning: 'Return' As Variant --> As string
        public static string GetRegistryKey_3(string KeyName, string strDefaultValue = "")
        {
            return GetRegistryKey(KeyName, "", strDefaultValue);
        }

		public static string GetRegistryKey(string KeyName, string strSubPath = "", string strDefaultValue = "")
		{
			
			modReplaceWorkFiles.Statics.gstrReturn = setCont.GetSettingValue(KeyName, strSubPath, TWSharedLibrary.Variables.Statics.OwnerType, TWSharedLibrary.Variables.Statics.OwnerID, "");
			if (string.IsNullOrEmpty(modReplaceWorkFiles.Statics.gstrReturn))
			{
				modReplaceWorkFiles.Statics.gstrReturn = strDefaultValue;
			}
			return modReplaceWorkFiles.Statics.gstrReturn;
		}

		public static bool SaveRegistryKey(string KeyName, string KeyValue, string strSubPath = "")
		{
			
			setCont.SaveSetting(KeyValue, KeyName, strSubPath, TWSharedLibrary.Variables.Statics.OwnerType, TWSharedLibrary.Variables.Statics.OwnerID, "");
			return true;
		}

		public class StaticVariables
		{
			//public static cSettingsController setCont = new cSettingsController();
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			public cSettingsController setCont_AutoInitialized;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
