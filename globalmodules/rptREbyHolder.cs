﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TWSharedLibrary;

#if TWBL0000
using TWBL0000;
using modExtraModules = TWBL0000.modGlobalVariables;


#elif TWRE0000
using modExtraModules = TWRE0000.modGlobalVariables;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptREbyHolder : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Corey Gray              *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/15/2005              *
		// ********************************************************
		clsDRWrapper clsre = new clsDRWrapper();
		clsDRWrapper clsMortgage = new clsDRWrapper();
		//clsDRWrapper clsAssoc = new clsDRWrapper();
		bool boolTax;
		// whether we should print tax info or not
		clsDRWrapper clsTaxRE = new clsDRWrapper();
		int lngBYear;
		// billing year
		double dblTaxSum;
		double dblOSSum;
		double dblTotalDueSum;
		bool boolCL = false;

		public rptREbyHolder()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Real Estate List By Mortgage Holder";
		}

		public static rptREbyHolder InstancePtr
		{
			get
			{
				return (rptREbyHolder)Sys.GetInstance(typeof(rptREbyHolder));
			}
		}

		protected rptREbyHolder _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsMortgage.Dispose();
				clsTaxRE.Dispose();
				clsre.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_DataInitialize(object sender, System.EventArgs e)
		{
			this.Fields.Add("TheBinder");
			this.Fields["TheBinder"].Value = 0;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsre.EndOfFile();
			if (!eArgs.EOF)
			{
				// do it here or the groupheader info will be behind the times
				this.Fields["TheBinder"].Value = clsre.Get_Fields_Int32("mortgageholderid");
			}
			//Detail_Format();
		}

		public void Init(bool boolNameOrder, bool boolShowTax, int lngBillYear, string strStart = "", string strEnd = "")
		{
			string strSQL = "";
			string strOrder = "";
			string str1;
			string str2;
			string str3;
			string str4;
			string strWhere = "";
			string strDBName;
			try
			{
				//FC:FINAL:AM:#i56 - set here the variable
				boolCL = modGlobalConstants.Statics.gboolCL;
				// On Error GoTo ErrorHandler
				if (boolNameOrder)
				{
					if (strEnd == string.Empty)
					{
						if (strStart == string.Empty)
						{
							strWhere = "";
						}
						else
						{
							strWhere = " and name >= '" + strStart + "' ";
						}
					}
					else
					{
						strWhere = " and name between '" + strStart + "' and '" + strEnd + "' ";
					}
				}
				else
				{
					if (strEnd == string.Empty)
					{
						if (strStart == string.Empty)
						{
							strWhere = "";
						}
						else
						{
							strWhere = " and mortgageholderid >= " + FCConvert.ToString(Conversion.Val(strStart)) + " ";
						}
					}
					else
					{
						strWhere = " and mortgageholderid between " + FCConvert.ToString(Conversion.Val(strStart)) + " and " + FCConvert.ToString(Conversion.Val(strEnd)) + " ";
					}
				}
				clsre.OpenRecordset("select * from returnaddress", "twbl0000.vb1");
				str1 = "";
				str2 = "";
				str3 = "";
				str4 = "";
				if (!clsre.EndOfFile())
				{
					str1 = Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("address1")));
					str2 = Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("address2")));
					str3 = Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("address3")));
					str4 = Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("address4")));
					if (Strings.Trim(str3) == string.Empty)
					{
						str3 = str4;
						str4 = "";
					}
					if (Strings.Trim(str2) == string.Empty)
					{
						str2 = str3;
						str3 = str4;
						str4 = "";
					}
					if (Strings.Trim(str1) == string.Empty)
					{
						str1 = str2;
						str2 = str3;
						str3 = str4;
						str4 = "";
					}
				}
				if (str1 != string.Empty)
				{
					txtReturnAddress1.Text = str1;
					txtReturnAddress2.Text = str2;
					txtReturnAddress3.Text = str3;
					txtReturnAddress4.Text = str4;
				}
				else
				{
					lblReturnTo.Visible = false;
				}
				boolTax = boolShowTax;
				lngBYear = lngBillYear;
				if (boolShowTax && boolCL)
				{
					lnTotal.Visible = true;
					txtTotalOutstanding.Visible = true;
					txtTotalTax.Visible = true;
					lblOutstanding.Visible = true;
					txtOutstanding.Visible = true;
					lblTax.Visible = true;
					txttax.Visible = true;
					// lblOutstanding.Text = "Current Year Due as of " & Format(Date, "MM/dd/yyyy")
					// lblOutstanding.Text = "Period Due as of " & Format(Date, "MM/dd/yyyy")
					lblOutstanding.Text = FCConvert.ToString(lngBYear) + " Due as of " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
					lblTaxYear.Text = "Tax Year " + FCConvert.ToString(lngBYear);
					lblTotalDue.Text = "Total Due as of " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
					txtTotalDue.Visible = true;
					txtTotalDueTotal.Visible = true;
				}
				else
				{
					lnTotal.Visible = false;
					txtTotalOutstanding.Visible = false;
					txtTotalTax.Visible = false;
					lblOutstanding.Visible = false;
					txtOutstanding.Visible = false;
					lblTax.Visible = false;
					txttax.Visible = false;
					lblTotalDue.Visible = false;
					txtTotalDue.Visible = false;
					txtTotalDueTotal.Visible = false;
					GroupFooter1.Height = 0;
				}
				if (boolNameOrder)
				{
					strOrder = " order by name ";
					// kgk InsertName   ,master.rsname "
				}
				else
				{
					strOrder = " order by mortgageholderid ";
					// ,master.rsname "
				}
				strDBName = clsre.Get_GetFullDBName("CentralData") + ".dbo.";
				string strMasterJoin;
				// Dim strMasterJoinJoin As String
				// strREFullDBName = clsReport.GetFullDBName("RealEstate")
				strMasterJoin = GetMasterJoin();
				// Dim strMasterJoinQuery As String
				// strMasterJoinQuery = "(" & strMasterJoin & ") mj"
				// Call clsre.OpenRecordset("select * from master inner join (" & strDBName & "mortgageholderS inner join " & strDBName & "mortgageassociation ON (MORtgageassociation.mortgageholderid = mortgageholders.ID)) on (mortgageassociation.account = master.rsaccount)  where mortgageassociation.module = 'RE' and not (master.rsdeleted = 1) and master.rscard = 1 " & strWhere & strOrder, "twre0000.vb1")
				clsre.OpenRecordset(strMasterJoin + " inner join (" + strDBName + "mortgageholderS inner join " + strDBName + "mortgageassociation ON (MORtgageassociation.mortgageholderid = mortgageholders.ID)) on (mortgageassociation.account = mparty.rsaccount)  where mortgageassociation.module = 'RE' and not (mparty.rsdeleted = 1) and mparty.rscard = 1 " + strWhere + strOrder, "twre0000.vb1");
				if (clsre.EndOfFile())
				{
					FCMessageBox.Show("No records found.", MsgBoxStyle.Information, "No Match");
					Cancel();
					return;
				}
				// clsre.InsertName "OwnerPartyID", "Own1", False, True, False, "", False, "", True, ""
				// If boolNameOrder Then
				// strOrder = "name, Own1FullName"
				// Else
				// strOrder = "mortgageholderid, Own1FullName "
				// End If
				frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "MortgageHolder", false, false);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In Init", MsgBoxStyle.Critical, "Error");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			txtDate.Text = DateTime.Today.ToString();
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strAddress1 = "";
				string strAddress2 = "";
				string strAddress3 = "";
				string strAddress4 = "";
				double dblTax = 0;
				double dblOS = 0;
				double dblTotalDue = 0;
				txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
				if (!clsre.EndOfFile())
				{
					// set the group binder
					if (!boolTax)
					{
						this.Fields["TheBinder"].Value = clsre.Get_Fields_Int32("mortgageholderid");
						txtaccount.Text = FCConvert.ToString(clsre.Get_Fields_Int32("rsaccount"));
						//FC:FINAL:AM:#i56 - field not in the recordset
						txtName.Text = clsre.Get_Fields("RSName");
						txtLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("rslocnumalph")) + " " + FCConvert.ToString(clsre.Get_Fields_String("rslocapt"))) + " " + FCConvert.ToString(clsre.Get_Fields_String("rslocstreet")));
						txttax.Text = "";
						txtOutstanding.Text = "";
					}
					else
					{
						this.Fields["TheBinder"].Value = clsre.Get_Fields_Int32("mortgageholderid");
						txtaccount.Text = FCConvert.ToString(clsre.Get_Fields_Int32("rsaccount"));
						//FC:FINAL:AM:#i56 - field not in the recordset
						txtName.Text = clsre.Get_Fields("RSName");
						txtLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("rslocnumalph")) + " " + FCConvert.ToString(clsre.Get_Fields_String("rslocapt"))) + " " + FCConvert.ToString(clsre.Get_Fields_String("rslocstreet")));
						// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
						clsTaxRE.OpenRecordset("select sum(taxdue1 + taxdue2 + taxdue3 + taxdue4) as taxsum from billingmaster where account = " + FCConvert.ToString(clsre.Get_Fields("account")) + " and billingyear between " + FCConvert.ToString(lngBYear * 10) + " and " + FCConvert.ToString(lngBYear * 10 + 9) + " and billingtype = 'RE'", "twcl0000.vb1");
						if (!clsTaxRE.EndOfFile())
						{
							// TODO: Field [taxsum] not found!! (maybe it is an alias?)
							if (Conversion.Val(clsTaxRE.Get_Fields("taxsum")) > 0)
							{
								// TODO: Field [taxsum] not found!! (maybe it is an alias?)
								dblTax = FCConvert.ToDouble(Conversion.Val(clsTaxRE.Get_Fields("taxsum")));
								txttax.Text = Strings.Format(dblTax, "#,###,###,##0.00");
							}
							else
							{
								txttax.Text = "0.00";
							}
							if (boolCL && boolTax)
							{
								dblOS = CalcOutstandingForYear_2(clsre.Get_Fields_Int32("rsaccount"), lngBYear);
								txtOutstanding.Text = Strings.Format(dblOS, "0.00");
								dblTotalDue = modCLCalculations.CalculateAccountTotal1(clsre.Get_Fields_Int32("RSAccount"), true);
								txtTotalDue.Text = Strings.Format(dblTotalDue, "0.00");
							}
						}
						else
						{
							txttax.Text = "0.00";
							txtOutstanding.Text = "0.00";
							txtTotalDue.Text = "0.00";
						}
					}
					dblTaxSum += dblTax;
					dblOSSum += dblOS;
					dblTotalDueSum += dblTotalDue;
					clsre.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Detail Format");
			}
		}

		private void GroupFooter1_Format(object sender, System.EventArgs e)
		{
			if (boolCL && boolTax)
			{
				txtTotalOutstanding.Text = Strings.Format(dblOSSum, "#,##0.00");
				txtTotalTax.Text = Strings.Format(dblTaxSum, "#,##0.00");
				txtTotalDueTotal.Text = Strings.Format(dblTotalDueSum, "#,##0.00");
				dblOSSum = 0;
				dblTaxSum = 0;
				dblTotalDueSum = 0;
			}
			else
			{
				txtTotalOutstanding.Text = "";
				txtTotalTax.Text = "";
				txtTotalDueTotal.Text = "";
			}
		}

		private void GroupHeader1_Format(object sender, System.EventArgs e)
		{
			string strAddress1;
			string strAddress2;
			// VBto upgrade warning: strAddress3 As object	OnWrite(object, string)
			string strAddress3;
			// VBto upgrade warning: strAddress4 As Variant --> As string
			string strAddress4;
			clsMortgage.OpenRecordset("select * from mortgageholders where ID = " + this.Fields["TheBinder"].Value, "CentralData");
			if (clsMortgage.EndOfFile())
				return;
			txtNumber.Text = this.Fields["thebinder"].Value.ToString();
			txtMortgageName.Text = clsMortgage.Get_Fields_String("name");
			strAddress1 = clsMortgage.Get_Fields_String("address1");
			strAddress2 = clsMortgage.Get_Fields_String("address2");
			// straddress3 = Trim(Trim(clsMortgage.Fields("streetnumber") & " " & clsMortgage.Fields("apt")) & " " & clsMortgage.Fields("streetname"))
			strAddress3 = clsMortgage.Get_Fields_String("address3");
			if (FCConvert.ToString(strAddress3) == "0")
				strAddress3 = "";
			strAddress4 = Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("city")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("state")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("zip")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("zip4")));
			// now condense this info
			if (FCConvert.ToString(strAddress3) == string.Empty)
			{
				strAddress3 = strAddress4;
				strAddress4 = "";
			}
			if (FCConvert.ToString(strAddress2) == string.Empty)
			{
				strAddress2 = strAddress3;
				strAddress3 = strAddress4;
				strAddress4 = "";
			}
			if (FCConvert.ToString(strAddress1) == string.Empty)
			{
				strAddress1 = strAddress2;
				strAddress2 = strAddress3;
				strAddress3 = strAddress4;
				strAddress4 = "";
			}
			txtAddress1.Text = strAddress1;
			txtAddress2.Text = strAddress2;
			txtAddress3.Text = strAddress3;
			txtAddress4.Text = strAddress4;
		}
		// VBto upgrade warning: 'Return' As Variant --> As double	OnWrite(short, double)
		private double CalcOutstandingForYear_2(int lngAccountNumber, int lngYear)
		{
			return CalcOutstandingForYear(ref lngAccountNumber, ref lngYear);
		}

		private double CalcOutstandingForYear(ref int lngAccountNumber, ref int lngYear)
		{
			double CalcOutstandingForYear = 0;
			var rsCL = new clsDRWrapper();
			var rsLien = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will calculate the total remaining balance and
                // return it as a double for the account and type passed in

                string strSQL;
                double dblTotal = 0;
                double dblXtraInt = 0;
                CalcOutstandingForYear = 0;
                strSQL = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAccountNumber) +
                         " AND BillingType = 'RE'  and billingyear between " + FCConvert.ToString(lngYear * 10) +
                         " and " + FCConvert.ToString(lngYear * 10 + 9);
                rsLien.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
                rsCL.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
                if (!rsCL.EndOfFile())
                {
                    while (!rsCL.EndOfFile())
                    {
                        // calculate each year
                        if (((rsCL.Get_Fields_Int32("LienRecordNumber"))) == 0)
                        {
                            // non-lien
                            dblTotal += modCLCalculations.CalculateAccountCL(ref rsCL, lngAccountNumber, DateTime.Today,
                                ref dblXtraInt);
                        }
                        else
                        {
                            // lien
                            // rsLien.FindFirstRecord "ID", rsCL.Fields("LienrecordNumber")
                            rsLien.FindFirst("id = " + FCConvert.ToString(rsCL.Get_Fields_Int32("lienrecordnumber")));
                            if (rsLien.NoMatch)
                            {
                                // no match has been found
                                // do nothing
                            }
                            else
                            {
                                // calculate the lien record
                                dblTotal += modCLCalculations.CalculateAccountCLLien(rsLien, DateTime.Today,
                                    ref dblXtraInt);
                            }
                        }

                        rsCL.MoveNext();
                    }
                }
                else
                {
                    // nothing found
                    dblTotal = 0;
                }

                CalcOutstandingForYear = dblTotal;
                return CalcOutstandingForYear;
            }
            catch (Exception ex)
            {

                FCMessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message +
                    ".", MsgBoxStyle.Critical, "Calculate Account Total Error");
            }
            finally
            {
                rsCL.Dispose();
				rsLien.Dispose();
            }
			return CalcOutstandingForYear;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private string GetMasterJoin()
		{
			string GetMasterJoin = "";
			// select master.*, cpo.FullNameLastFirst as RSName,cpso.FullNameLastFirst as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4 from TRIO_Test_Live_RealEstate.dbo.master left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpo on (master.ownerpartyid = cpo.PartyID) left join TRIO_Test_Live_CentralParties.dbo.PartyAndAddressView as cpso on (master.SecOwnerPartyID = cpso.PartyID)
			string strReturn;
			strReturn = "select master.*, DeedName1 as RSName,DeedName2 as RSSecOwner,cpo.address1 as rsaddr1,cpo.address2 as rsaddr2,cpo.address3 as rsaddress3,cpo.city as rsaddr3, cpo.state as rsstate,cpo.zip as rszip, '' as rszip4, cpo.email as email from ";
			clsDRWrapper tLoad = new clsDRWrapper();
			string strTemp;
			strTemp = tLoad.Get_GetFullDBName("RealEstate");
			strReturn += strTemp + ".dbo.master left join ";
			strTemp = tLoad.Get_GetFullDBName("CentralParties");
			strTemp += ".dbo.PartyAndAddressView ";
			strReturn += strTemp + " as cpo on (master.ownerpartyid = cpo.ID) left join ";
			strReturn += strTemp + " as cpso on (master.SecOwnerPartyID = cpso.ID)";
			strReturn = " select * from (" + strReturn + ") mparty ";
			GetMasterJoin = strReturn;
			tLoad.Dispose();
			return GetMasterJoin;
		}

		private void rptREbyHolder_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptREbyHolder.Text	= "Real Estate List By Mortgage Holder";
			//rptREbyHolder.Icon	= "rptREbyHolder.dsx":0000";
			//rptREbyHolder.Left	= 0;
			//rptREbyHolder.Top	= 0;
			//rptREbyHolder.Width	= 11880;
			//rptREbyHolder.Height	= 8490;
			//rptREbyHolder.StartUpPosition	= 3;
			//rptREbyHolder.SectionData	= "rptREbyHolder.dsx":058A;
			//End Unmaped Properties
		}
	}
}
