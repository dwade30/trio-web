﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;

namespace Global
{
	/// <summary>
	/// Summary description for frmEMail.
	/// </summary>
	partial class frmEMail : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCPictureBox> ImageSend;
		public fecherFoundation.FCGrid GridNames;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkDisclaimer;
		public fecherFoundation.FCCheckBox chkCompanySignature;
		public fecherFoundation.FCCheckBox chkSignature;
		public fecherFoundation.FCTextBox txtCarbonCopy;
		private fecherFoundation.FCToolBarButton Button1;
		private fecherFoundation.FCToolBarButton btnSaveDraft;
		private fecherFoundation.FCToolBarButton btnLoadDraft;
		private fecherFoundation.FCToolBarButton btnAttachFile;
		private fecherFoundation.FCToolBarButton btnDetachFile;
		private fecherFoundation.FCToolBarButton btnHighPriority;
		private fecherFoundation.FCToolBarButton btnReceipt;
		private fecherFoundation.FCToolBarButton btnSend;
		private fecherFoundation.FCToolBarButton Button9;
		private fecherFoundation.FCToolBarButton btnEditContacts;
		private fecherFoundation.FCToolBarButton btnEditGroups;
		private fecherFoundation.FCToolBarButton btnEditSignatures;
		private fecherFoundation.FCToolBarButton btnEditDisclaimer;
		private fecherFoundation.FCToolBarButton btnEditConfiguration;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCRichTextBox rtbMessage;
		public fecherFoundation.FCTextBox txtSubject;
		public fecherFoundation.FCTextBox txtMailTo;
		public fecherFoundation.FCPictureBox ImageSend_1;
		public fecherFoundation.FCPictureBox ImageSend_0;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblAttachments;
		public fecherFoundation.FCLabel lblCarbonCopy;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblSendTo;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuSepar4;
		public fecherFoundation.FCToolStripMenuItem mnuEditContacts;
		public fecherFoundation.FCToolStripMenuItem mnuEditGroups;
		public fecherFoundation.FCToolStripMenuItem mnuEditSignatures;
		public fecherFoundation.FCToolStripMenuItem mnuEditDisclaimer;
		public fecherFoundation.FCToolStripMenuItem mnuEditConfiguration;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry("frmEMail_ImageList1_1", "frmEMail_ImageList1_1");
			Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry("frmEMail_ImageList1_2", "frmEMail_ImageList1_2");
			Wisej.Web.ImageListEntry imageListEntry3 = new Wisej.Web.ImageListEntry("frmEMail_ImageList1_3", "frmEMail_ImageList1_3");
			Wisej.Web.ImageListEntry imageListEntry4 = new Wisej.Web.ImageListEntry("frmEMail_ImageList1_4", "frmEMail_ImageList1_4");
			Wisej.Web.ImageListEntry imageListEntry5 = new Wisej.Web.ImageListEntry("frmEMail_ImageList1_5", "frmEMail_ImageList1_5");
			Wisej.Web.ImageListEntry imageListEntry6 = new Wisej.Web.ImageListEntry("frmEMail_ImageList1_6", "frmEMail_ImageList1_6");
			Wisej.Web.ImageListEntry imageListEntry7 = new Wisej.Web.ImageListEntry("frmEMail_ImageList1_7", "frmEMail_ImageList1_7");
			Wisej.Web.ImageListEntry imageListEntry8 = new Wisej.Web.ImageListEntry("frmEMail_ImageList1_8", "frmEMail_ImageList1_8");
			Wisej.Web.ImageListEntry imageListEntry9 = new Wisej.Web.ImageListEntry("frmEMail_ImageList1_9", "frmEMail_ImageList1_9");
			Wisej.Web.ImageListEntry imageListEntry10 = new Wisej.Web.ImageListEntry("frmEMail_ImageList1_10", "frmEMail_ImageList1_10");
			Wisej.Web.ImageListEntry imageListEntry11 = new Wisej.Web.ImageListEntry("frmEMail_ImageList1_11", "frmEMail_ImageList1_11");
			Wisej.Web.ImageListEntry imageListEntry12 = new Wisej.Web.ImageListEntry("frmEMail_ImageList1_12", "frmEMail_ImageList1_12");
			Wisej.Web.ImageListEntry imageListEntry13 = new Wisej.Web.ImageListEntry("frmEMail_ImageList1_13", "frmEMail_ImageList1_13");
			Wisej.Web.ImageListEntry imageListEntry14 = new Wisej.Web.ImageListEntry("frmEMail_ImageList1_14", "frmEMail_ImageList1_14");
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEMail));
			this.GridNames = new fecherFoundation.FCGrid();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkDisclaimer = new fecherFoundation.FCCheckBox();
			this.chkCompanySignature = new fecherFoundation.FCCheckBox();
			this.chkSignature = new fecherFoundation.FCCheckBox();
			this.txtCarbonCopy = new fecherFoundation.FCTextBox();
			this.Button1 = new fecherFoundation.FCToolBarButton();
			this.btnSaveDraft = new fecherFoundation.FCToolBarButton();
			this.btnLoadDraft = new fecherFoundation.FCToolBarButton();
			this.btnAttachFile = new fecherFoundation.FCToolBarButton();
			this.btnDetachFile = new fecherFoundation.FCToolBarButton();
			this.btnHighPriority = new fecherFoundation.FCToolBarButton();
			this.btnReceipt = new fecherFoundation.FCToolBarButton();
			this.btnSend = new fecherFoundation.FCToolBarButton();
			this.Button9 = new fecherFoundation.FCToolBarButton();
			this.btnEditContacts = new fecherFoundation.FCToolBarButton();
			this.btnEditGroups = new fecherFoundation.FCToolBarButton();
			this.btnEditSignatures = new fecherFoundation.FCToolBarButton();
			this.btnEditDisclaimer = new fecherFoundation.FCToolBarButton();
			this.btnEditConfiguration = new fecherFoundation.FCToolBarButton();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.rtbMessage = new fecherFoundation.FCRichTextBox();
			this.txtSubject = new fecherFoundation.FCTextBox();
			this.txtMailTo = new fecherFoundation.FCTextBox();
			this.ImageSend_1 = new fecherFoundation.FCPictureBox();
			this.ImageSend_0 = new fecherFoundation.FCPictureBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblAttachments = new fecherFoundation.FCLabel();
			this.lblCarbonCopy = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.lblSendTo = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEditContacts = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEditGroups = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEditSignatures = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEditDisclaimer = new fecherFoundation.FCToolStripMenuItem();
			this.mnuEditConfiguration = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar4 = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.btnProcess = new fecherFoundation.FCButton();
			this.cmdSaveDraft = new fecherFoundation.FCButton();
			this.cmdDeleteDraft = new fecherFoundation.FCButton();
			this.cmdLoadSent = new fecherFoundation.FCButton();
			this.cmdLoadDraft = new fecherFoundation.FCButton();
			this.cmdDetachFiles = new fecherFoundation.FCButton();
			this.cmdAttach = new fecherFoundation.FCButton();
			this.chkHighPriority = new fecherFoundation.FCCheckBox();
			this.chkRequestReceipt = new fecherFoundation.FCCheckBox();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridNames)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDisclaimer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCompanySignature)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSignature)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.rtbMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ImageSend_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ImageSend_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveDraft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteDraft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoadSent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoadDraft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDetachFiles)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAttach)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHighPriority)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRequestReceipt)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 590);
			this.BottomPanel.Size = new System.Drawing.Size(1010, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkRequestReceipt);
			this.ClientArea.Controls.Add(this.chkHighPriority);
			this.ClientArea.Controls.Add(this.GridNames);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.txtCarbonCopy);
			this.ClientArea.Controls.Add(this.rtbMessage);
			this.ClientArea.Controls.Add(this.txtSubject);
			this.ClientArea.Controls.Add(this.txtMailTo);
			this.ClientArea.Controls.Add(this.ImageSend_1);
			this.ClientArea.Controls.Add(this.ImageSend_0);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblAttachments);
			this.ClientArea.Controls.Add(this.lblCarbonCopy);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.lblSendTo);
			this.ClientArea.Location = new System.Drawing.Point(0, 61);
			this.ClientArea.Size = new System.Drawing.Size(1010, 529);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdAttach);
			this.TopPanel.Controls.Add(this.cmdDetachFiles);
			this.TopPanel.Controls.Add(this.cmdLoadDraft);
			this.TopPanel.Controls.Add(this.cmdLoadSent);
			this.TopPanel.Controls.Add(this.cmdDeleteDraft);
			this.TopPanel.Controls.Add(this.cmdSaveDraft);
			this.TopPanel.Size = new System.Drawing.Size(1010, 61);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdSaveDraft, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDeleteDraft, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdLoadSent, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdLoadDraft, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDetachFiles, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAttach, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(26, 26);
			this.HeaderText.Size = new System.Drawing.Size(82, 30);
			this.HeaderText.Text = "E-mail";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// GridNames
			// 
			this.GridNames.AllowSelection = false;
			this.GridNames.AllowUserToResizeColumns = false;
			this.GridNames.AllowUserToResizeRows = false;
			this.GridNames.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridNames.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridNames.BackColorBkg = System.Drawing.Color.Empty;
			this.GridNames.BackColorFixed = System.Drawing.Color.Empty;
			this.GridNames.BackColorSel = System.Drawing.Color.Empty;
			this.GridNames.Cols = 4;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridNames.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridNames.ColumnHeadersHeight = 30;
			this.GridNames.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridNames.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridNames.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridNames.FixedCols = 0;
			this.GridNames.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridNames.FrozenCols = 0;
			this.GridNames.GridColor = System.Drawing.Color.Empty;
			this.GridNames.Location = new System.Drawing.Point(631, 30);
			this.GridNames.Name = "GridNames";
			this.GridNames.ReadOnly = true;
			this.GridNames.RowHeadersVisible = false;
			this.GridNames.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridNames.RowHeightMin = 0;
			this.GridNames.Rows = 1;
			this.GridNames.ShowColumnVisibilityMenu = false;
			this.GridNames.Size = new System.Drawing.Size(352, 384);
			this.GridNames.StandardTab = true;
			this.GridNames.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridNames.TabIndex = 11;
			this.ToolTip1.SetToolTip(this.GridNames, null);
			this.GridNames.Click += new System.EventHandler(this.GridNames_Click);
            this.GridNames.CellFormatting += new DataGridViewCellFormattingEventHandler(GridNames_CellFormatting);
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.chkDisclaimer);
			this.Frame1.Controls.Add(this.chkCompanySignature);
			this.Frame1.Controls.Add(this.chkSignature);
			this.Frame1.Location = new System.Drawing.Point(30, 434);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(581, 78);
			this.Frame1.TabIndex = 10;
			this.Frame1.Text = "Include";
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// chkDisclaimer
			// 
			this.chkDisclaimer.Checked = true;
			this.chkDisclaimer.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkDisclaimer.Location = new System.Drawing.Point(329, 30);
			this.chkDisclaimer.Name = "chkDisclaimer";
			this.chkDisclaimer.Size = new System.Drawing.Size(179, 27);
			this.chkDisclaimer.TabIndex = 2;
			this.chkDisclaimer.Text = "Disclaimer Signature";
			this.ToolTip1.SetToolTip(this.chkDisclaimer, "Include the privacy disclaimer");
			// 
			// chkCompanySignature
			// 
			this.chkCompanySignature.Location = new System.Drawing.Point(137, 30);
			this.chkCompanySignature.Name = "chkCompanySignature";
			this.chkCompanySignature.Size = new System.Drawing.Size(172, 27);
			this.chkCompanySignature.TabIndex = 1;
			this.chkCompanySignature.Text = "Company Signature";
			this.ToolTip1.SetToolTip(this.chkCompanySignature, "Include the global email signature");
			// 
			// chkSignature
			// 
			this.chkSignature.Location = new System.Drawing.Point(20, 30);
			this.chkSignature.Name = "chkSignature";
			this.chkSignature.Size = new System.Drawing.Size(97, 27);
			this.chkSignature.TabIndex = 0;
			this.chkSignature.Text = "Signature";
			this.ToolTip1.SetToolTip(this.chkSignature, "Include the personal signature for this log in");
			// 
			// txtCarbonCopy
			// 
			this.txtCarbonCopy.AutoSize = false;
			this.txtCarbonCopy.BackColor = System.Drawing.SystemColors.Window;
			this.txtCarbonCopy.Location = new System.Drawing.Point(129, 85);
			this.txtCarbonCopy.Multiline = true;
			this.txtCarbonCopy.Name = "txtCarbonCopy";
			this.txtCarbonCopy.ScrollBars = Wisej.Web.ScrollBars.Vertical;
			this.txtCarbonCopy.Size = new System.Drawing.Size(414, 40);
			this.txtCarbonCopy.TabIndex = 4;
			this.ToolTip1.SetToolTip(this.txtCarbonCopy, "Enter an address,click CC or click on a contact or group in the list.  Separate m" + "ultiple entries by commas.");
			this.txtCarbonCopy.Enter += new System.EventHandler(this.txtCarbonCopy_Enter);
			// 
			// Button1
			// 
			this.Button1.Key = "Button1";
			this.Button1.Name = "Button1";
			this.Button1.Style = fecherFoundation.FCToolBarButton.ToolBarButtonStyleSettings.tbrSeparator;
			this.Button1.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnSaveDraft
			// 
			this.btnSaveDraft.ImageIndex = 12;
			this.btnSaveDraft.Key = "btnSaveDraft";
			this.btnSaveDraft.Name = "btnSaveDraft";
			this.btnSaveDraft.ToolTipText = "Save As Draft";
			this.btnSaveDraft.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnLoadDraft
			// 
			this.btnLoadDraft.ImageIndex = 13;
			this.btnLoadDraft.Key = "btnLoadDraft";
			this.btnLoadDraft.Name = "btnLoadDraft";
			this.btnLoadDraft.ToolTipText = "Load Saved Draft";
			this.btnLoadDraft.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnAttachFile
			// 
			this.btnAttachFile.ImageIndex = 1;
			this.btnAttachFile.Key = "btnAttachFile";
			this.btnAttachFile.Name = "btnAttachFile";
			this.btnAttachFile.ToolTipText = "Attach a File";
			this.btnAttachFile.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnDetachFile
			// 
			this.btnDetachFile.ImageIndex = 2;
			this.btnDetachFile.Key = "btnDetachFile";
			this.btnDetachFile.Name = "btnDetachFile";
			this.btnDetachFile.ToolTipText = "Detach File(s)";
			this.btnDetachFile.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnHighPriority
			// 
			this.btnHighPriority.ImageIndex = 7;
			this.btnHighPriority.Key = "btnHighPriority";
			this.btnHighPriority.Name = "btnHighPriority";
			this.btnHighPriority.Style = fecherFoundation.FCToolBarButton.ToolBarButtonStyleSettings.tbrButtonGroup;
			this.btnHighPriority.ToolTipText = "High Priority";
			this.btnHighPriority.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnReceipt
			// 
			this.btnReceipt.ImageIndex = 9;
			this.btnReceipt.Key = "btnReceipt";
			this.btnReceipt.Name = "btnReceipt";
			this.btnReceipt.Style = fecherFoundation.FCToolBarButton.ToolBarButtonStyleSettings.tbrButtonGroup;
			this.btnReceipt.ToolTipText = "Request Receipt";
			this.btnReceipt.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnSend
			// 
			this.btnSend.ImageIndex = 0;
			this.btnSend.Key = "btnSend";
			this.btnSend.Name = "btnSend";
			this.btnSend.ToolTipText = "Send";
			this.btnSend.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// Button9
			// 
			this.Button9.Key = "Button9";
			this.Button9.Name = "Button9";
			this.Button9.Style = fecherFoundation.FCToolBarButton.ToolBarButtonStyleSettings.tbrSeparator;
			this.Button9.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnEditContacts
			// 
			this.btnEditContacts.ImageIndex = 3;
			this.btnEditContacts.Key = "btnEditContacts";
			this.btnEditContacts.Name = "btnEditContacts";
			this.btnEditContacts.ToolTipText = "Contacts";
			this.btnEditContacts.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnEditGroups
			// 
			this.btnEditGroups.ImageIndex = 8;
			this.btnEditGroups.Key = "btnEditGroups";
			this.btnEditGroups.Name = "btnEditGroups";
			this.btnEditGroups.ToolTipText = "Groups";
			this.btnEditGroups.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnEditSignatures
			// 
			this.btnEditSignatures.ImageIndex = 5;
			this.btnEditSignatures.Key = "btnEditSignatures";
			this.btnEditSignatures.Name = "btnEditSignatures";
			this.btnEditSignatures.ToolTipText = "Signatures";
			this.btnEditSignatures.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// btnEditDisclaimer
			// 
			this.btnEditDisclaimer.Enabled = false;
			this.btnEditDisclaimer.ImageIndex = 4;
			this.btnEditDisclaimer.Key = "btnEditDisclaimer";
			this.btnEditDisclaimer.Name = "btnEditDisclaimer";
			this.btnEditDisclaimer.ToolTipText = "Disclaimer";
			this.btnEditDisclaimer.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			this.btnEditDisclaimer.Visible = false;
			// 
			// btnEditConfiguration
			// 
			this.btnEditConfiguration.ImageIndex = 6;
			this.btnEditConfiguration.Key = "btnEditConfiguration";
			this.btnEditConfiguration.Name = "btnEditConfiguration";
			this.btnEditConfiguration.ToolTipText = "Configuration Settings";
			this.btnEditConfiguration.Value = fecherFoundation.FCToolBarButton.ToolbarButtonValueConstants.tbrPressed;
			// 
			// ImageList1
			// 
			this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
				imageListEntry1,
				imageListEntry2,
				imageListEntry3,
				imageListEntry4,
				imageListEntry5,
				imageListEntry6,
				imageListEntry7,
				imageListEntry8,
				imageListEntry9,
				imageListEntry10,
				imageListEntry11,
				imageListEntry12,
				imageListEntry13,
				imageListEntry14
			});
			this.ImageList1.ImageSize = new System.Drawing.Size(24, 24);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// rtbMessage
			// 
			this.rtbMessage.Location = new System.Drawing.Point(30, 275);
			this.rtbMessage.Multiline = true;
			this.rtbMessage.Name = "rtbMessage";
			this.rtbMessage.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
			this.rtbMessage.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
			this.rtbMessage.Size = new System.Drawing.Size(581, 139);
			this.rtbMessage.TabIndex = 9;
			this.ToolTip1.SetToolTip(this.rtbMessage, "Enter the body of the message");
			// 
			// txtSubject
			// 
			this.txtSubject.AutoSize = false;
			this.txtSubject.BackColor = System.Drawing.SystemColors.Window;
			this.txtSubject.Location = new System.Drawing.Point(129, 176);
			this.txtSubject.Name = "txtSubject";
			this.txtSubject.Size = new System.Drawing.Size(414, 40);
			this.txtSubject.TabIndex = 6;
			this.ToolTip1.SetToolTip(this.txtSubject, "Enter text to appear as the subject of the E-mail");
			// 
			// txtMailTo
			// 
			this.txtMailTo.AutoSize = false;
			this.txtMailTo.BackColor = System.Drawing.SystemColors.Window;
			this.txtMailTo.Location = new System.Drawing.Point(129, 25);
			this.txtMailTo.Name = "txtMailTo";
			this.txtMailTo.Size = new System.Drawing.Size(414, 40);
			this.txtMailTo.TabIndex = 2;
			this.ToolTip1.SetToolTip(this.txtMailTo, "Enter an address, click Send To  or click on a contact or group in the list. Sepa" + "rate multiple entries with commas.\"");
			this.txtMailTo.Enter += new System.EventHandler(this.txtMailTo_Enter);
			// 
			// ImageSend_1
			// 
			this.ImageSend_1.AllowDrop = true;
			this.ImageSend_1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.ImageSend_1.DrawStyle = ((0));
			this.ImageSend_1.DrawWidth = ((1));
			this.ImageSend_1.FillStyle = ((1));
			this.ImageSend_1.FontTransparent = true;
			this.ImageSend_1.Image = ((System.Drawing.Image)(resources.GetObject("ImageSend_1.Image")));
			this.ImageSend_1.Location = new System.Drawing.Point(559, 95);
			this.ImageSend_1.Name = "ImageSend_1";
			this.ImageSend_1.Picture = ((System.Drawing.Image)(resources.GetObject("ImageSend_1.Picture")));
			this.ImageSend_1.Size = new System.Drawing.Size(21, 21);
			this.ImageSend_1.TabIndex = 13;
			this.ToolTip1.SetToolTip(this.ImageSend_1, null);
			// 
			// ImageSend_0
			// 
			this.ImageSend_0.AllowDrop = true;
			this.ImageSend_0.BorderStyle = Wisej.Web.BorderStyle.None;
			this.ImageSend_0.DrawStyle = ((0));
			this.ImageSend_0.DrawWidth = ((1));
			this.ImageSend_0.FillStyle = ((1));
			this.ImageSend_0.FontTransparent = true;
			this.ImageSend_0.Image = ((System.Drawing.Image)(resources.GetObject("ImageSend_0.Image")));
			this.ImageSend_0.Location = new System.Drawing.Point(559, 35);
			this.ImageSend_0.Name = "ImageSend_0";
			this.ImageSend_0.Picture = ((System.Drawing.Image)(resources.GetObject("ImageSend_0.Picture")));
			this.ImageSend_0.Size = new System.Drawing.Size(21, 21);
			this.ImageSend_0.TabIndex = 14;
			this.ToolTip1.SetToolTip(this.ImageSend_0, null);
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 238);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(100, 15);
			this.Label1.TabIndex = 7;
			this.Label1.Text = "ATTACHMENTS";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// lblAttachments
			// 
			this.lblAttachments.Location = new System.Drawing.Point(129, 240);
			this.lblAttachments.Name = "lblAttachments";
			this.lblAttachments.Size = new System.Drawing.Size(414, 32);
			this.lblAttachments.TabIndex = 8;
			this.ToolTip1.SetToolTip(this.lblAttachments, null);
			// 
			// lblCarbonCopy
			// 
			this.lblCarbonCopy.AutoSize = true;
			this.lblCarbonCopy.Location = new System.Drawing.Point(30, 99);
			this.lblCarbonCopy.Name = "lblCarbonCopy";
			this.lblCarbonCopy.Size = new System.Drawing.Size(25, 15);
			this.lblCarbonCopy.TabIndex = 3;
			this.lblCarbonCopy.Text = "CC";
			this.lblCarbonCopy.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblCarbonCopy, "Choose groups or individuals to send carbon copies to");
			this.lblCarbonCopy.Click += new System.EventHandler(this.lblCarbonCopy_Click);
			// 
			// Label3
			// 
			this.Label3.AutoSize = true;
			this.Label3.Location = new System.Drawing.Point(30, 190);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(64, 15);
			this.Label3.TabIndex = 5;
			this.Label3.Text = "SUBJECT";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.Label3, null);
			// 
			// lblSendTo
			// 
			this.lblSendTo.AutoSize = true;
			this.lblSendTo.Location = new System.Drawing.Point(30, 40);
			this.lblSendTo.Name = "lblSendTo";
			this.lblSendTo.Size = new System.Drawing.Size(62, 15);
			this.lblSendTo.TabIndex = 1;
			this.lblSendTo.Text = "SEND TO";
			this.lblSendTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.ToolTip1.SetToolTip(this.lblSendTo, "Select recipient from the list of contacts");
			this.lblSendTo.Click += new System.EventHandler(this.lblSendTo_Click);
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuClear,
				this.mnuEditContacts,
				this.mnuEditGroups,
				this.mnuEditSignatures,
				this.mnuEditDisclaimer,
				this.mnuEditConfiguration
			});
			this.MainMenu1.Name = null;
			// 
			// mnuClear
			// 
			this.mnuClear.Index = 0;
			this.mnuClear.Name = "mnuClear";
			this.mnuClear.Text = "New Message";
			this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// mnuEditContacts
			// 
			this.mnuEditContacts.Index = 1;
			this.mnuEditContacts.Name = "mnuEditContacts";
			this.mnuEditContacts.Text = "Contacts";
			this.mnuEditContacts.Click += new System.EventHandler(this.mnuEditContacts_Click);
			// 
			// mnuEditGroups
			// 
			this.mnuEditGroups.Index = 2;
			this.mnuEditGroups.Name = "mnuEditGroups";
			this.mnuEditGroups.Text = "Groups";
			this.mnuEditGroups.Click += new System.EventHandler(this.mnuEditGroups_Click);
			// 
			// mnuEditSignatures
			// 
			this.mnuEditSignatures.Index = 3;
			this.mnuEditSignatures.Name = "mnuEditSignatures";
			this.mnuEditSignatures.Text = "Signatures";
			this.mnuEditSignatures.Click += new System.EventHandler(this.mnuEditSignatures_Click);
			// 
			// mnuEditDisclaimer
			// 
			this.mnuEditDisclaimer.Enabled = false;
			this.mnuEditDisclaimer.Index = 4;
			this.mnuEditDisclaimer.Name = "mnuEditDisclaimer";
			this.mnuEditDisclaimer.Text = "Disclaimer";
			this.mnuEditDisclaimer.Visible = false;
			this.mnuEditDisclaimer.Click += new System.EventHandler(this.mnuEditDisclaimer_Click);
			// 
			// mnuEditConfiguration
			// 
			this.mnuEditConfiguration.Index = 5;
			this.mnuEditConfiguration.Name = "mnuEditConfiguration";
			this.mnuEditConfiguration.Text = "Configuration Settings";
			this.mnuEditConfiguration.Click += new System.EventHandler(this.mnuEditConfiguration_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "";
			// 
			// mnuSepar4
			// 
			this.mnuSepar4.Index = -1;
			this.mnuSepar4.Name = "mnuSepar4";
			this.mnuSepar4.Text = "";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(432, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(120, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Send";
			this.ToolTip1.SetToolTip(this.btnProcess, null);
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// cmdSaveDraft
			// 
			this.cmdSaveDraft.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSaveDraft.AppearanceKey = "toolbarButton";
			this.cmdSaveDraft.Location = new System.Drawing.Point(870, 28);
			this.cmdSaveDraft.Name = "cmdSaveDraft";
			this.cmdSaveDraft.Size = new System.Drawing.Size(96, 24);
			this.cmdSaveDraft.TabIndex = 52;
			this.cmdSaveDraft.Text = "Save as Draft";
			this.ToolTip1.SetToolTip(this.cmdSaveDraft, null);
			this.cmdSaveDraft.Click += new System.EventHandler(this.cmdSaveDraft_Click);
			// 
			// cmdDeleteDraft
			// 
			this.cmdDeleteDraft.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDeleteDraft.AppearanceKey = "toolbarButton";
			this.cmdDeleteDraft.Location = new System.Drawing.Point(735, 28);
			this.cmdDeleteDraft.Name = "cmdDeleteDraft";
			this.cmdDeleteDraft.Size = new System.Drawing.Size(131, 24);
			this.cmdDeleteDraft.TabIndex = 53;
			this.cmdDeleteDraft.Text = "Delete Saved Draft";
			this.ToolTip1.SetToolTip(this.cmdDeleteDraft, null);
			this.cmdDeleteDraft.Click += new System.EventHandler(this.cmdDeleteDraft_Click);
			// 
			// cmdLoadSent
			// 
			this.cmdLoadSent.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdLoadSent.AppearanceKey = "toolbarButton";
			this.cmdLoadSent.Location = new System.Drawing.Point(626, 28);
			this.cmdLoadSent.Name = "cmdLoadSent";
			this.cmdLoadSent.Size = new System.Drawing.Size(105, 24);
			this.cmdLoadSent.TabIndex = 54;
			this.cmdLoadSent.Text = "Load Sent Mail";
			this.ToolTip1.SetToolTip(this.cmdLoadSent, null);
			this.cmdLoadSent.Click += new System.EventHandler(this.cmdLoadSent_Click);
			// 
			// cmdLoadDraft
			// 
			this.cmdLoadDraft.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdLoadDraft.AppearanceKey = "toolbarButton";
			this.cmdLoadDraft.Location = new System.Drawing.Point(503, 28);
			this.cmdLoadDraft.Name = "cmdLoadDraft";
			this.cmdLoadDraft.Size = new System.Drawing.Size(120, 24);
			this.cmdLoadDraft.TabIndex = 55;
			this.cmdLoadDraft.Text = "Load Saved Draft";
			this.ToolTip1.SetToolTip(this.cmdLoadDraft, null);
			this.cmdLoadDraft.Click += new System.EventHandler(this.cmdLoadDraft_Click);
			// 
			// cmdDetachFiles
			// 
			this.cmdDetachFiles.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDetachFiles.AppearanceKey = "toolbarButton";
			this.cmdDetachFiles.Location = new System.Drawing.Point(403, 28);
			this.cmdDetachFiles.Name = "cmdDetachFiles";
			this.cmdDetachFiles.Size = new System.Drawing.Size(96, 24);
			this.cmdDetachFiles.TabIndex = 56;
			this.cmdDetachFiles.Text = "Detach File(s)";
			this.ToolTip1.SetToolTip(this.cmdDetachFiles, null);
			this.cmdDetachFiles.Click += new System.EventHandler(this.cmdDetachFiles_Click);
			// 
			// cmdAttach
			// 
			this.cmdAttach.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAttach.AppearanceKey = "toolbarButton";
			this.cmdAttach.Location = new System.Drawing.Point(306, 28);
			this.cmdAttach.Name = "cmdAttach";
			this.cmdAttach.Size = new System.Drawing.Size(94, 24);
			this.cmdAttach.TabIndex = 57;
			this.cmdAttach.Text = "Attach File(s)";
			this.ToolTip1.SetToolTip(this.cmdAttach, null);
			this.cmdAttach.Click += new System.EventHandler(this.cmdAttach_Click);
			// 
			// chkHighPriority
			// 
			this.chkHighPriority.Location = new System.Drawing.Point(631, 460);
			this.chkHighPriority.Name = "chkHighPriority";
			this.chkHighPriority.Size = new System.Drawing.Size(117, 27);
			this.chkHighPriority.TabIndex = 15;
			this.chkHighPriority.Text = "High Priority";
			this.ToolTip1.SetToolTip(this.chkHighPriority, null);
			// 
			// chkRequestReceipt
			// 
			this.chkRequestReceipt.Location = new System.Drawing.Point(768, 460);
			this.chkRequestReceipt.Name = "chkRequestReceipt";
			this.chkRequestReceipt.Size = new System.Drawing.Size(149, 27);
			this.chkRequestReceipt.TabIndex = 16;
			this.chkRequestReceipt.Text = "Request Receipt";
			this.ToolTip1.SetToolTip(this.chkRequestReceipt, null);
			// 
			// frmEMail
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1010, 698);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmEMail";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "E-mail";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmEMail_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEMail_KeyDown);
			this.Resize += new System.EventHandler(this.frmEMail_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridNames)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkDisclaimer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCompanySignature)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSignature)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.rtbMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ImageSend_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ImageSend_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveDraft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDeleteDraft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoadSent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdLoadDraft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDetachFiles)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAttach)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHighPriority)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkRequestReceipt)).EndInit();
			this.ResumeLayout(false);
		}

        

        private void Toolbar1_ButtonClick1(object sender, FCToolBarButtonClickEventArgs e)
		{
			throw new NotImplementedException();
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
		public FCButton cmdSaveDraft;
		public FCButton cmdDeleteDraft;
		public FCButton cmdLoadSent;
		public FCButton cmdLoadDraft;
		public FCButton cmdDetachFiles;
		public FCButton cmdAttach;
		private FCCheckBox chkHighPriority;
		private FCCheckBox chkRequestReceipt;
	}
}
