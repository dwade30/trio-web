//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using Wisej.Web;

namespace Global
{
    /// <summary>
    /// Summary description for sarBookPageUT.
    /// </summary>
    public partial class sarBookPageUT : FCSectionReport
    {

        public sarBookPageUT()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
            if (_InstancePtr == null)
                _InstancePtr = this;
            this.Name = "Account Detail";
        }

        public static sarBookPageUT InstancePtr
        {
            get
            {
                return (sarBookPageUT)Sys.GetInstance(typeof(sarBookPageUT));
            }
        }

        protected sarBookPageUT _InstancePtr = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                rsData.Dispose();
            }
            base.Dispose(disposing);
        }
        // nObj = 1
        //   0	sarBookPageUT	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               12/04/2006              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               12/04/2006              *
        // ********************************************************
        int lngAcct;
        clsDRWrapper rsData = new clsDRWrapper();
        string strWS;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = rsData.EndOfFile();
        }

        private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
        {
            switch (KeyCode)
            {

                case Keys.Escape:
                    {
                        this.Close();
                        break;
                    }
            } //end switch
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            string strSQL;
            //modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
            strWS = sarUTBookPageAccount.InstancePtr.strWS;

            this.Detail.ColumnCount = 2;
            lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
            strSQL = "SELECT * FROM Master WHERE AccountNumber = " + FCConvert.ToString(lngAcct);
            rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
            if (rsData.EndOfFile())
            {
                fldBookPage.Text = "No Matches";
                lblHeader.Visible = false;
                GroupHeader1.Height = 0;
            }
        }

		private void Detail_Format(object sender, EventArgs e)
        {
            BindFields();
        }

        private void BindFields()
        {
            try
            {   // On Error GoTo ERROR_HANDLER
                if (!rsData.EndOfFile())
                {
                    // TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
                    // TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
                    fldBookPage.Text = "B" + FCConvert.ToString(rsData.Get_Fields("Book")) + " P" + FCConvert.ToString(rsData.Get_Fields("Page"));
                    rsData.MoveNext();
                }
                return;
            }
            catch (Exception ex)
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Book Page Report - RE", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void sarBookPageUT_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //sarBookPageUT properties;
            //sarBookPageUT.Caption	= "Account Detail";
            //sarBookPageUT.Icon	= "sarBookPageUT.dsx":0000";
            //sarBookPageUT.Left	= 0;
            //sarBookPageUT.Top	= 0;
            //sarBookPageUT.Width	= 11880;
            //sarBookPageUT.Height	= 8595;
            //sarBookPageUT.StartUpPosition	= 3;
            //sarBookPageUT.SectionData	= "sarBookPageUT.dsx":058A;
            //End Unmaped Properties
        }
    }
}
