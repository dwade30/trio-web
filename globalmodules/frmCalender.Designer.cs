//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;

#if TWBD0000
using TWBD0000;


#elif TWFA0000
using TWFA0000;


#elif TWAR0000
using TWAR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmCalender.
	/// </summary>
	partial class frmCalender : BaseForm
	{
		public fecherFoundation.FCComboBox cboMonth;
		public fecherFoundation.FCComboBox cboYear;
		public FCGrid vsCalender;
		public fecherFoundation.FCLabel lblMonthYear;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCalender));
			this.cboMonth = new fecherFoundation.FCComboBox();
			this.cboYear = new fecherFoundation.FCComboBox();
			this.vsCalender = new fecherFoundation.FCGrid();
			this.lblMonthYear = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsCalender)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 471);
			this.BottomPanel.Size = new System.Drawing.Size(645, 96);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cboMonth);
			this.ClientArea.Controls.Add(this.cboYear);
			this.ClientArea.Controls.Add(this.vsCalender);
			this.ClientArea.Controls.Add(this.lblMonthYear);
			this.ClientArea.Size = new System.Drawing.Size(645, 411);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(645, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(139, 30);
			this.HeaderText.Text = "Select Date";
			// 
			// cboMonth
			// 
			this.cboMonth.AutoSize = false;
			this.cboMonth.BackColor = System.Drawing.SystemColors.Window;
			this.cboMonth.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboMonth.FormattingEnabled = true;
			this.cboMonth.Items.AddRange(new object[] {
				"January",
				"February",
				"March",
				"April",
				"May",
				"June",
				"July",
				"August",
				"September",
				"October",
				"November",
				"December"
			});
			this.cboMonth.Location = new System.Drawing.Point(229, 30);
			this.cboMonth.Name = "cboMonth";
			this.cboMonth.Size = new System.Drawing.Size(130, 40);
			this.cboMonth.TabIndex = 0;
			this.cboMonth.SelectedIndexChanged += new System.EventHandler(this.cboMonth_SelectedIndexChanged);
			// 
			// cboYear
			// 
			this.cboYear.AutoSize = false;
			this.cboYear.BackColor = System.Drawing.SystemColors.Window;
			this.cboYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboYear.FormattingEnabled = true;
			this.cboYear.Location = new System.Drawing.Point(381, 30);
			this.cboYear.Name = "cboYear";
			this.cboYear.Size = new System.Drawing.Size(91, 40);
			this.cboYear.TabIndex = 1;
			this.cboYear.SelectedIndexChanged += new System.EventHandler(this.cboYear_SelectedIndexChanged);
            // 
            // vsCalender
            // 
            this.vsCalender.AllowSelection = false;
			this.vsCalender.AllowUserToResizeColumns = false;
			this.vsCalender.AllowUserToResizeRows = false;
			this.vsCalender.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsCalender.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsCalender.BackColorBkg = System.Drawing.Color.Empty;
			this.vsCalender.BackColorFixed = System.Drawing.Color.Empty;
			this.vsCalender.BackColorSel = System.Drawing.Color.Empty;
			this.vsCalender.Cols = 7;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsCalender.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsCalender.ColumnHeadersHeight = 30;
			this.vsCalender.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsCalender.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsCalender.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsCalender.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsCalender.FixedCols = 0;
			this.vsCalender.FixedRows = 1;
			this.vsCalender.FrozenCols = 0;
			this.vsCalender.FrozenRows = 0;
			this.vsCalender.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsCalender.GridColor = System.Drawing.Color.Empty;
			this.vsCalender.Location = new System.Drawing.Point(30, 93);
			this.vsCalender.Name = "vsCalender";
			this.vsCalender.ReadOnly = true;
			this.vsCalender.RowHeadersVisible = false;
			this.vsCalender.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsCalender.RowHeightMin = 0;
			this.vsCalender.Rows = 7;
			this.vsCalender.ShowColumnVisibilityMenu = false;
			this.vsCalender.Size = new System.Drawing.Size(586, 287);
			this.vsCalender.StandardTab = true;
			this.vsCalender.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsCalender.TabIndex = 2;
			this.vsCalender.CurrentCellChanged += new System.EventHandler(this.vsCalender_RowColChange);
			this.vsCalender.KeyDown += new Wisej.Web.KeyEventHandler(this.vsCalender_KeyDownEvent);
			//this.vsCalender.Click += new System.EventHandler(this.vsCalender_ClickEvent);
			this.vsCalender.CellDoubleClick += new Wisej.Web.DataGridViewCellEventHandler(this.vsCalender_DblClick);
			// 
			// lblMonthYear
			// 
			//this.lblMonthYear.Font = new System.Drawing.Font("@window-title", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
			this.lblMonthYear.Location = new System.Drawing.Point(30, 44);
			this.lblMonthYear.Name = "lblMonthYear";
			this.lblMonthYear.Size = new System.Drawing.Size(179, 26);
			this.lblMonthYear.TabIndex = 3;
			this.lblMonthYear.Text = "LABEL1";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(271, 34);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.Size = new System.Drawing.Size(88, 40);
			this.cmdProcess.TabIndex = 0;
			this.cmdProcess.Text = "Process";
			this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
			// 
			// frmCalender
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(645, 567);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCalender";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Calendar";
			this.Activated += new System.EventHandler(this.frmCalender_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCalender_KeyPress);
			this.Resize += new System.EventHandler(this.frmCalender_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsCalender)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcess;
	}
}