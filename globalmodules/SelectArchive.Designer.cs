﻿using Wisej.Web;

namespace Global
{
    partial class SelectArchive : Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new Wisej.Web.Button();
            this.comboBox1 = new fecherFoundation.FCComboBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.AppearanceKey = "acceptButton";
            this.button1.Location = new System.Drawing.Point(159, 140);
            this.button1.Name = "button1";
            this.button1.Shortcut = Wisej.Web.Shortcut.F12;
            this.button1.Size = new System.Drawing.Size(120, 48);
            this.button1.TabIndex = 0;
            this.button1.Text = "Select";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.comboBox1.Location = new System.Drawing.Point(30, 40);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(378, 40);
            this.comboBox1.TabIndex = 1;
            // 
            // SelectArchive
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = Wisej.Web.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 218);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectArchive";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Select Archive";
            this.Load += new System.EventHandler(this.SelectArchive_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Wisej.Web.Button button1;
        private fecherFoundation.FCComboBox comboBox1;
    }
}