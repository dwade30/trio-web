//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.IO;
using TWSharedLibrary;

namespace Global
{
    /// <summary>
    /// Summary description for frmNewArchive.
    /// </summary>
    public partial class frmNewArchive : BaseForm
    {


        public frmNewArchive()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
            //FC:FINAL:AM:#3411 - allow only digits
            this.udYear.AllowOnlyNumericInput();
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmNewArchive InstancePtr
        {
            get
            {
                return (frmNewArchive)Sys.GetInstance(typeof(frmNewArchive));
            }
        }
        protected static frmNewArchive _InstancePtr = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>



        //=========================================================
        /// <summary>
        /// ********************************************************
        /// </summary>
        /// <summary>
        /// Property of TRIO Software Corporation
        /// </summary>

        /// <summary>
        /// Written By
        /// </summary>
        /// <summary>
        /// Date
        /// </summary>


        /// <summary>
        /// ********************************************************
        /// </summary>


        /// <summary>
        /// Private WithEvents tBack As BackupRestore
        /// </summary>
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private FCCollection tColl = new FCCollection();
        private FCCollection tColl_AutoInitialized;
        private FCCollection tColl
        {
            get
            {
                if (tColl_AutoInitialized == null)
                {
                    tColl_AutoInitialized = new FCCollection();
                }
                return tColl_AutoInitialized;
            }
            set
            {
                tColl_AutoInitialized = value;
            }
        }
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private FCCollection restoreColl = new FCCollection();
        private FCCollection restoreColl_AutoInitialized;
        private FCCollection restoreColl
        {
            get
            {
                if (restoreColl_AutoInitialized == null)
                {
                    restoreColl_AutoInitialized = new FCCollection();
                }
                return restoreColl_AutoInitialized;
            }
            set
            {
                restoreColl_AutoInitialized = value;
            }
        }

        public delegate void ArchiveCreatedEventHandler();
        public event ArchiveCreatedEventHandler ArchiveCreated;


        public void Init(int intMinYear, int intMaxYear, int intYear)
        {
            // txtYear.Text = intYear
            //udYear.Minimum = FCConvert.ToInt16(intMinYear);
            //udYear.Maximum = FCConvert.ToInt16(intMaxYear);
            udYear.Text = intYear.ToString();

            this.Show(); // vbModal
        }

        private void btnCreate_Click(object sender, System.EventArgs e)
        {
            CreateArchive();
        }

        private void frmNewArchive_KeyDown(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);

            switch (KeyCode)
            {

                case Keys.Escape:
                    {
                        KeyCode = (Keys)0;
                        mnuExit_Click();
                        break;
                    }
            } //end switch
        }

        private void frmNewArchive_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmNewArchive properties;
            //frmNewArchive.FillStyle	= 0;
            //frmNewArchive.ScaleWidth	= 5880;
            //frmNewArchive.ScaleHeight	= 4110;
            //frmNewArchive.LinkTopic	= "Form2";
            //frmNewArchive.PaletteMode	= 1  'UseZOrder;
            //End Unmaped Properties

            // Set tBack = New BackupRestore
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
            modGlobalFunctions.SetTRIOColors(this);
        }


        private void CreateArchive()
        {
            if (Strings.Trim(txtDescription.Text) == "")
            {
                MessageBox.Show("You must provide a description before creating an archive", "Missing Description", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            int x;
            for (x = tColl.Count; x >= 1; x--)
            {
                tColl.Remove(1);
            } // x
            for (x = restoreColl.Count; x >= 1; x--)
            {
                restoreColl.Remove(1);
            } // x
            btnCreate.Enabled = false;
            mnuSaveContinue.Enabled = false;
            udYear.Enabled = false;
            cGenericCollection backColl = new cGenericCollection();
            backColl.AddItem("RealEstate");
            backColl.AddItem("CentralData");
            backColl.AddItem("CentralParties");
            backColl.AddItem("PersonalProperty");

            tColl.Add(udYear.Text + "|RealEstate");
            tColl.Add(udYear.Text + "|CentralData");
            tColl.Add(udYear.Text + "|CentralParties");
            tColl.Add(udYear.Text + "|PersonalProperty");
            restoreColl.Add(udYear.Text + "|RealEstate");
            restoreColl.Add(udYear.Text + "|CentralData");
            restoreColl.Add(udYear.Text + "|CentralParties");
            restoreColl.Add(udYear.Text + "|PersonalProperty");
            // ProcessArchiveList
            MakeArchives(FCConvert.ToInt32(udYear.Text), backColl);
            Close();
        }

        private void MakeArchives(int intYear, cGenericCollection backColl)
        {
            frmBusy bWait = new frmBusy();
            try
            {   // On Error GoTo ErrorHandler
				FCUtils.StartTask(this, () =>
				{
					backColl.MoveFirst();
					string strDB = "";
					string strDBName = "";
					cBackupRestore backRest = new cBackupRestore();
					bWait.StartBusy();
					clsDRWrapper rsLoad = new clsDRWrapper();
					string strSavePath;
					strSavePath = StaticSettings.gGlobalSettings.BackupPath;
					//FC:FINAL:MSH - i.issue #1182: create 'backup' directory if it doesn't exist
					if (!Directory.Exists(strSavePath))
					{
						Directory.CreateDirectory(strSavePath);
					}
					if (Strings.Right(" " + strSavePath, 1) != "/" && Strings.Right(" " + strSavePath, 1) != "\\")
					{
						strSavePath += "\\";
					}
					while (backColl.IsCurrent())
					{
						strDB = FCConvert.ToString(backColl.GetCurrentItem());
						bWait.Message = "Creating " + FCConvert.ToString(intYear) + " " + strDB + " backup";
						//Application.DoEvents();
						strDBName = rsLoad.Get_GetFullDBName(strDB);
						if (File.Exists(strSavePath + strDB + "Archive" + FCConvert.ToString(intYear) + ".bak"))
						{
							File.Delete(strSavePath + strDB + "Archive" + FCConvert.ToString(intYear) + ".bak");
						}
						backRest.MakeBackup(strDBName, strSavePath + strDB + "Archive" + FCConvert.ToString(intYear) + ".bak", false);
						if (backRest.HadError)
						{
							Information.Err().Raise(backRest.LastErrorNumber, null, backRest.LastErrorMessage, null, null);
						}
						backColl.MoveNext();
					}
					backColl.MoveFirst();
					while (backColl.IsCurrent())
					{
						strDB = FCConvert.ToString(backColl.GetCurrentItem());
						bWait.Message = "Restoring " + FCConvert.ToString(intYear) + " " + strDB + " archive";
						//Application.DoEvents();
						strDBName = rsLoad.Get_GetFullDBName(strDB);
						strDBName = strDBName.Replace("Live", "CommitmentArchive_" + FCConvert.ToString(intYear));
						backRest.RestoreAs(strSavePath + strDB + "Archive" + FCConvert.ToString(intYear) + ".bak", strDBName);
						backColl.MoveNext();
					}

					modGlobalFunctions.AddCYAEntry_26("RE", "Created commitment archive", FCConvert.ToString(intYear) + "");

					//Application.DoEvents();
					clsDRWrapper rsTemp = new clsDRWrapper();
					rsTemp.OpenRecordset("select * from archives where archivetype = 'CommitmentArchive' and ArchiveID = " + FCConvert.ToString(intYear), "SystemSettings");
					if (rsTemp.EndOfFile())
					{
						rsTemp.AddNew();
					}
					else
					{
						rsTemp.Edit();
					}
					rsTemp.Set_Fields("ArchiveType", "CommitmentArchive");
					rsTemp.Set_Fields("ArchiveID", intYear);
					rsTemp.Set_Fields("Description", txtDescription.Text);
					rsTemp.Set_Fields("CreationDate", Strings.Format(DateTime.Now, "MM/dd/yyyy"));
					rsTemp.Update();

					btnCreate.Enabled = true;
					mnuSaveContinue.Enabled = true;
					udYear.Enabled = true;

					bWait.StopBusy();
					bWait.Unload();
					/*- bWait = null; */
					MessageBox.Show("Archive Created", "Archived", MessageBoxButtons.OK, MessageBoxIcon.Information);
					FCUtils.UnlockUserInterface();
				}); 
                bWait.Show(FCForm.FormShowEnum.Modal);
                if (this.ArchiveCreated != null) this.ArchiveCreated();
                return;
            }
            catch (Exception ex)
            {   // ErrorHandler:
                if (!(bWait == null))
                {
                    bWait.StopBusy();
                    bWait.Unload();
                    /*- bWait = null; */
                }
                MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        /// <summary>
        /// Private Sub ProcessArchiveList()
        /// </summary>
        /// <summary>
        /// If tColl.Count > 0 Then
        /// </summary>
        /// <summary>
        /// Dim rsLoad As New clsDRWrapper
        /// </summary>
        /// <summary>
        /// Dim strTemp As String
        /// </summary>
        /// <summary>
        /// Dim strDB As String
        /// </summary>
        /// <summary>
        /// Dim intYear As Integer
        /// </summary>
        /// <summary>
        /// Dim ar() As String
        /// </summary>
        /// <summary>
        /// ar = Split(tColl(1), "|")
        /// </summary>
        /// <summary>
        /// intYear = ar(0)
        /// </summary>
        /// <summary>
        /// strDB = ar(1)
        /// </summary>
        /// <summary>
        /// strTemp = "TRIO_" & rsLoad.MegaGroup & "_CommitmentArchive_" & intYear & "_" & strDB
        /// </summary>
        /// <summary>
        /// tColl.Remove (1)
        /// </summary>
        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        /// Set tBack = New BackupRestore
        /// </summary>
        /// <summary>
        /// lblProgress.Caption = "Creating " & intYear & " " & strDB & " backup"
        /// </summary>
        /// <summary>
        /// lblProgress.Refresh
        /// </summary>
        /// <summary>
        /// DoEvents
        /// </summary>
        /// <summary>
        /// Call tBack.Backup(rsLoad.ConnectionInformation(strDB))
        /// </summary>
        /// <summary>
        /// Else
        /// </summary>
        /// <summary>
        /// btnCreate.Enabled = True
        /// </summary>
        /// <summary>
        /// mnuSaveContinue.Enabled = True
        /// </summary>
        /// <summary>
        /// udYear.Enabled = True
        /// </summary>
        /// <summary>
        /// lblProgress.Caption = udYear.Value & " Backups Created"
        /// </summary>
        /// <summary>
        /// lblProgress.Refresh
        /// </summary>
        /// <summary>
        /// DoEvents
        /// </summary>
        /// <summary>
        /// ProcessRestoreList
        /// </summary>
        /// <summary>
        /// End If
        /// </summary>
        /// <summary>
        /// End Sub
        /// </summary>

        /// <summary>
        /// Private Sub ProcessRestoreList()
        /// </summary>
        /// <summary>
        /// If restoreColl.Count > 0 Then
        /// </summary>
        /// <summary>
        /// Dim rsLoad As New clsDRWrapper
        /// </summary>
        /// <summary>
        /// Dim strTemp As String
        /// </summary>
        /// <summary>
        /// Dim strDB As String
        /// </summary>
        /// <summary>
        /// Dim intYear As Integer
        /// </summary>
        /// <summary>
        /// Dim ar() As String
        /// </summary>
        /// <summary>
        /// Dim strBackupPath As String
        /// </summary>
        /// <summary>
        /// Set tBack = New BackupRestore
        /// </summary>
        /// <summary>
        /// ar = Split(restoreColl(1), "|")
        /// </summary>
        /// <summary>
        /// intYear = ar(0)
        /// </summary>
        /// <summary>
        /// strDB = ar(1)
        /// </summary>
        /// <summary>
        /// strTemp = "TRIO_" & rsLoad.MegaGroup & "_CommitmentArchive_" & intYear & "_" & strDB
        /// </summary>
        /// <summary>
        /// restoreColl.Remove (1)
        /// </summary>
        /// <summary>
        /// strBackupPath = tBack.GetBackupPath(rsLoad.ConnectionInformation(strDB)) & "\" & rsLoad.GetFullDBName(strDB) & ".bak"
        /// </summary>
        /// <summary>
        /// lblProgress.Caption = "Creating " & intYear & " " & strDB & " archive"
        /// </summary>
        /// <summary>
        /// Call tBack.Restore(rsLoad.ConnectionInformation(strDB), strTemp, strBackupPath, rsLoad.GetFullDBName(strDB))
        /// </summary>
        /// <summary>
        /// Else
        /// </summary>
        /// <summary>
        /// Call AddCYAEntry("RE", "Created commitment archive", udYear.Value)
        /// </summary>
        /// <summary>
        /// lblProgress.Caption = udYear.Value & " Archive Created"
        /// </summary>
        /// <summary>
        /// lblProgress.Refresh
        /// </summary>
        /// <summary>
        /// DoEvents
        /// </summary>
        /// <summary>
        /// Dim rsTemp As New clsDRWrapper
        /// </summary>
        /// <summary>
        /// Call rsTemp.OpenRecordset("select * from archives", "SystemSettings")
        /// </summary>
        /// <summary>
        /// rsTemp.AddNew
        /// </summary>
        /// <summary>
        /// rsTemp.Fields("ArchiveType") = "CommitmentArchive"
        /// </summary>
        /// <summary>
        /// rsTemp.Fields("ArchiveID") = udYear.Value
        /// </summary>
        /// <summary>
        /// rsTemp.Fields("Description") = txtDescription.Text
        /// </summary>
        /// <summary>
        /// rsTemp.Fields("CreationDate") = Format(Now, "MM/dd/yyyy")
        /// </summary>
        /// <summary>
        /// rsTemp.Update
        /// </summary>
        /// <summary>
        /// 
        /// </summary>
        /// <summary>
        /// btnCreate.Enabled = True
        /// </summary>
        /// <summary>
        /// mnuSaveContinue.Enabled = True
        /// </summary>
        /// <summary>
        /// udYear.Enabled = True
        /// </summary>
        /// <summary>
        /// MsgBox "Archive Created", , "Created"
        /// </summary>
        /// <summary>
        /// Unload Me
        /// </summary>
        /// <summary>
        /// End If
        /// </summary>
        /// <summary>
        /// End Sub
        /// </summary>


        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            Close();
        }
        public void mnuExit_Click()
        {
            mnuExit_Click(mnuExit, new System.EventArgs());
        }


        private void mnuSaveContinue_Click(object sender, System.EventArgs e)
        {
            CreateArchive();
        }

        /// <summary>
        /// Private Sub tBack_BackupCompleted(ByVal tArgs As clssqlserverdataconnection.BackupCompletedArgs)
        /// </summary>
        /// <summary>
        /// lblProgress.Caption = ""
        /// </summary>
        /// <summary>
        /// If Not tArgs.success Then
        /// </summary>
        /// <summary>
        /// MsgBox "Error creating backup" & vbNewLine & tArgs.ErrorMessage, vbCritical, "Error"
        /// </summary>
        /// <summary>
        /// btnCreate.Enabled = True
        /// </summary>
        /// <summary>
        /// mnuSaveContinue.Enabled = True
        /// </summary>
        /// <summary>
        /// udYear.Enabled = True
        /// </summary>
        /// <summary>
        /// Else
        /// </summary>
        /// <summary>
        /// ProcessArchiveList
        /// </summary>
        /// <summary>
        /// End If
        /// </summary>
        /// <summary>
        /// End Sub
        /// </summary>

        /// <summary>
        /// Private Sub tBack_BackupPercentageCompleted(ByVal tArgs As clssqlserverdataconnection.BackupPercentageCompletedArgs)
        /// </summary>
        /// <summary>
        /// lblProgress.Caption = tArgs.PercentComplete
        /// </summary>
        /// <summary>
        /// lblProgress.Refresh
        /// </summary>
        /// <summary>
        /// DoEvents
        /// </summary>
        /// <summary>
        /// End Sub
        /// </summary>

        /// <summary>
        /// Private Sub tBack_RestoreCompleted(ByVal tArgs As clssqlserverdataconnection.RestoreCompletedArgs)
        /// </summary>
        /// <summary>
        /// lblProgress.Caption = ""
        /// </summary>
        /// <summary>
        /// If Not tArgs.success Then
        /// </summary>
        /// <summary>
        /// MsgBox "Error creating archive" & vbNewLine & tArgs.ErrorMessage, vbCritical, "Error"
        /// </summary>
        /// <summary>
        /// btnCreate.Enabled = True
        /// </summary>
        /// <summary>
        /// mnuSaveContinue.Enabled = True
        /// </summary>
        /// <summary>
        /// udYear.Enabled = True
        /// </summary>
        /// <summary>
        /// Else
        /// </summary>
        /// <summary>
        /// ProcessRestoreList
        /// </summary>
        /// <summary>
        /// End If
        /// </summary>
        /// <summary>
        /// End Sub
        /// </summary>

        private void tBack_RestorePercentageCompleted(RestorePercentageCompletedArgs tArgs)
        {
            lblProgress.Text = FCConvert.ToString(tArgs.PercentComplete);
            lblProgress.Refresh();
            //Application.DoEvents();
        }

    }
}