//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Drawing;
using fecherFoundation;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.CentralDocuments;
using TWSharedLibrary;
#if TWUT0000
using TWUT0000;
#endif

namespace Global
{
    /// <summary>
    /// Summary description for frmAddDocument.
    /// </summary>
    public partial class frmAddDocument : BaseForm
    {

        public frmAddDocument()
        {
            //
            // Required for Windows Form Designer support
            //
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null )
                _InstancePtr = this;
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmAddDocument InstancePtr
        {
            get
            {
                if (_InstancePtr == null) // || _InstancePtr.IsDisposed
                    _InstancePtr = new frmAddDocument();
                return _InstancePtr;
            }
        }
        protected static frmAddDocument _InstancePtr = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>



        //=========================================================
        // ********************************************************
        // Property of TRIO Software Corporation

        // Written By     Dave Wade
        // Date           3/25/2008

        // This form will be used to link new documents to accounts.  It will be called
        // using it's init function.  The 2 arguments that need to be passed are the name of the
        // link field in the LinkedDocuments table and the value to be saved into that field
        // ********************************************************

        bool blnSaved; // boolean keepign track of if a document was added or not
        private CentralDocumentService docService = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);
        private int referenceId;

        private void cmdBrowse_Click(object sender, System.EventArgs e)
        {
            // Browse through your directories for an already existing file
            string strCurDir;

            strCurDir = FCFileSystem.Statics.UserDataFolder; // save the current directory so we can set it back
            Information.Err().Clear(); // clear any errors

            // MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
            //FC:FINAL:MSH - i.issue #1128: restore property to avoid exceptions on Closing 'Upload' form
            MDIParent.InstancePtr.CommonDialog1.CancelError = true;
            /*? On Error Resume Next  */
            MDIParent.InstancePtr.CommonDialog1.Filter = "All Image Files |*.pdf;*.psd;*.j2k;*.j2c;*.gif;*.jpg;*.pcx;*.wmf;*.wbmp;*.bmp;*.tif;*.tga;*.pgx;*.ras;*.pnm;*.png;*.ico|PDF (*.pdf)|*.pdf|PhotoShop (*.psd)|*.psd|JPEG 2000 (*.j2k)|*.j2k;*.j2c|JPEG (*.jpg)|*.jpg|PCX (*.pcx)|*.pcx|WMF (*.wmf)|*.wmf|Wireless Bitmap (*.wbmp)|*.wbmp|Bitmap (*.bmp)|*.bmp|TIF (*.tif)|*.tif|TGA (*.tga)|*.tga|Gif (*.gif)|*.gif |PGX (*.pgx)|*.pgx|RAS (*.ras)|*.ras|PNM (*.pnm)|*.pnm|PNG (*.png)|*.png|Icon (*.ico)|*.ico"; // set filter for pdf files
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          // .CommonDialog1.FileName = "*.pdf"
            try
            {
                MDIParent.InstancePtr.CommonDialog1.ShowOpen();
            }
            catch (Exception ex) {StaticSettings.GlobalTelemetryService.TrackException(ex); }
            if (Information.Err().Number == 0)
            {
                lblFile.Text = MDIParent.InstancePtr.CommonDialog1.FileName; // if a file is selected show it in the file label
                if (Strings.UCase(Strings.Right(MDIParent.InstancePtr.CommonDialog1.FileName, 3)) != "PDF")
                {
                    fcViewerPanel1.LoadFileFromApplication(MDIParent.InstancePtr.CommonDialog1.FileName);
                }
               
            }
            else
            {
                modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
                return;
            }

            modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
        }
        public void cmdBrowse_Click()
        {
            cmdBrowse_Click(cmdBrowse, new System.EventArgs());
        }

        private void cmdScan_Click(object sender, System.EventArgs e)
        {
            string strFile;

            strFile = frmScanDocument.InstancePtr.Init(); // Show the global scan screen
            if (strFile != "")
            { // if a file is returned then a docuemnt was scanned and saved
                lblFile.Text = strFile; // set the file name in the label
            }
        }

        public void cmdScan_Click()
        {
            cmdScan_Click(cmdScan, new System.EventArgs());
        }

        private void frmAddDocument_Activated(object sender, System.EventArgs e)
        {
            if (modMain.FormExist(this))
            {
                return;
            }
            this.Refresh();
        }

        private void frmAddDocument_Load(object sender, System.EventArgs e)
        {

            blnSaved = false; // Initialie saved boolean

            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
            modGlobalFunctions.SetTRIOColors(this);

            SetCustomFormColors(); // set custom colors to be used in form
        }

        private void frmAddDocument_KeyPress(object sender, KeyPressEventArgs e)
        {
            FormUtilities.KeyPressHandler(e, this);
        }

        private void mnuFileBrowse_Click(object sender, System.EventArgs e)
        {
            cmdBrowse_Click();
        }

        private void mnuFileScan_Click(object sender, System.EventArgs e)
        {
            cmdScan_Click();
        }

        private void mnuProcessQuit_Click(object sender, System.EventArgs e)
        {
            this.Unload();
        }

        public bool Init( int referenceId)
        {
            bool ret = false;

            this.referenceId = referenceId; // set Link value
            this.Show(FCForm.FormShowEnum.Modal); // show form

            ret = blnSaved; // return whether or not a document was saved

            return ret;
        }

        private void mnuProcessSave_Click(object sender, System.EventArgs e)
        {
            if (lblFile.Text == "")
            {
                MessageBox.Show("You must select a document before you may continue.", "Invalid File", MessageBoxButtons.OK, MessageBoxIcon.Information);

                return;
            }

            // a file has been selected

            var doc = docService.MakeCentralDocumentFromFile(fcViewerPanel1.FileName, txtDescription.Text.Trim(), "", referenceId, "", "UtilityBilling", Guid.NewGuid(), fcViewerPanel1.ItemData);

            var cmd = new SaveCommand {Document = doc};

            docService.SaveCentralDocument(cmd);

            blnSaved = true; // set saved variable to true so the View screen knows to refresh
            this.Unload(); // return to the view screen
        }

        private void SetCustomFormColors()
        {
            lblFile.ForeColor = Color.Blue; // set file label forecolor to blue to make it show up
        }

        private void fcButton1_Click(object sender, EventArgs e)
        {
            this.mnuProcessSave_Click(sender, e);
        }
    }
}