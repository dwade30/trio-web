﻿using fecherFoundation;
using System;

namespace Global
{
	public class modPrinterDialogBox
	{
		public static bool ShowPrinterDialog(int hwnd)
		{
			bool ShowPrinterDialog = false;
			string NewPrinterName = "";
			FCCommonDialog printDlg = new FCCommonDialog();
			int pintOriginalPaperBin;
			// On Error GoTo ErrorHandler
			iLabel1:
			//printDlg.PrinterSettings.PrinterName = FCGlobal.Printer.DeviceName;
			//iLabel2: printDlg.DriverName = FCGlobal.Printer.DriverName;
			//iLabel3: printDlg.Port = FCGlobal.Printer.Port;
			//iLabel4: printDlg.PaperBin = FCGlobal.Printer.PaperBin;
			iLabel5:
			//FCGlobal.Printer.TrackDefault = false;
			iLabel6:
			Information.Err().Clear();
			iLabel7:
			printDlg.Flags = 0;
			iLabel8:
			printDlg.CancelError = true;
			/*? On Error Resume Next  */// Word and other applications make any selections here the new
			// default so we'll have to decide if we want a selection of a new
			// new paper bin to stay as the default. Matthew 8/30/2005 call id 75144
			//iLabel9: pintOriginalPaperBin = printDlg.PaperBin;
			iLabel10:
			if (Information.Err().Number == 0)
			{
				// On Error GoTo ErrorHandler
				iLabel11:
				//FC:FINAL:SBE - #i334 - if cancel is selected, and CancelError is true, exception is thrown (on error resume next)
				bool showPrinter = false;
				try
				{
					showPrinter = printDlg.ShowPrinter();
				}
				catch
				{
				}
				if (!showPrinter)
				{
					iLabel12:
					ShowPrinterDialog = false;
					return ShowPrinterDialog;
				}
				iLabel13:
				NewPrinterName = Strings.UCase(printDlg.PrinterSettings.PrinterName);
				iLabel14:
				if (Strings.UCase(FCGlobal.Printer.DeviceName) != NewPrinterName)
				{
					foreach (FCPrinter objPrinter in FCGlobal.Printers)
					{
						if (Strings.UCase(objPrinter.DeviceName) == NewPrinterName)
						{
							FCGlobal.Printer = objPrinter;
						}
					}
				}
				//FC:TODO:AM
				//gintPaperBin = printDlg.PaperBin;
				//FCGlobal.Printer.Copies = printDlg.Copies;
				//FCGlobal.Printer.Orientation = printDlg.Orientation;
				//FCGlobal.Printer.ColorMode = printDlg.ColorMode;
				//FCGlobal.Printer.Duplex = printDlg.Duplex;
				//FCGlobal.Printer.PaperBin = printDlg.PaperBin;
				//FCGlobal.Printer.PaperSize = printDlg.PaperSize;
				//FCGlobal.Printer.PrintQuality = printDlg.PrintQuality;
				ShowPrinterDialog = true;
			}
			else
			{
				// On Error GoTo ErrorHandler
				ShowPrinterDialog = false;
				return ShowPrinterDialog;
			}
			return ShowPrinterDialog;
			// ErrorHandler:
			// MsgBox "Error Number " & Err.Number & "  " & Err.Description & vbNewLine & "In ShowPrinterDialog", MsgBoxStyle.Critical, "Error"
			return ShowPrinterDialog;
		}

		public static short ShowAdvancedPrinterDialog(int hwnd)
		{
			short ShowAdvancedPrinterDialog = 0;
			// 0 = cancel
			// 1 = printer selected
			// 2 = dll not registered
			try
			{
				// On Error GoTo ErrorHandler
				if (ShowPrinterDialog(hwnd))
				{
					ShowAdvancedPrinterDialog = 1;
				}
				else
				{
					ShowAdvancedPrinterDialog = 0;
				}
				return ShowAdvancedPrinterDialog;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				if (Information.Err(ex).Number == 429)
				{
					ShowAdvancedPrinterDialog = 2;
				}
				else
				{
					ShowAdvancedPrinterDialog = 0;
				}
			}
			return ShowAdvancedPrinterDialog;
		}

		public class StaticVariables
		{
			//=========================================================
			public int gintPaperBin;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
