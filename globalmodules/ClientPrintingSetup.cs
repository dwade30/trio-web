﻿using fecherFoundation;
using Global;
using System;
using System.IO;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
    public partial class ClientPrintingSetup : Form
    {
        protected ClientPrintingSetup _InstancePtr = null;

        public ClientPrintingSetup()
        {
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

            if (_InstancePtr == null)
                _InstancePtr = this;
            this.Load += ClientPrintingSetup_Load;
        }

        /// <summary>
		/// Default instance for Form
		/// </summary>
		public static ClientPrintingSetup InstancePtr
        {
            get
            {
                return (ClientPrintingSetup)Sys.GetInstance(typeof(ClientPrintingSetup));
            }
        }

        public bool CheckConfiguration()
        {
            if (!RecheckConfiguration(false))
            {
                var dialogResult = this.ShowDialog();
                if (dialogResult == DialogResult.Cancel)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            //download TRIO Print Assistant
            //Application.Navigate(Application.Url + "TRIOAssistant/TrioAssistant.html", "_blank");
			//Application.Navigate("https://triorgdiag240.blob.core.windows.net/trioassistant/TrioAssistant.html", "_blank");
			Application.Navigate("http://trioassistant.trio-web.com/trioassistant/TrioAssistant.html", "_blank");
			//FCUtils.Download(Path.Combine(Application.StartupPath, "TRIOPrintAssistant", "setup.exe"));
		}


        private void button2_Click(object sender, EventArgs e)
        {
            clsPrinterFunctions.SaveApplicationKey(this.textBox1.Text);
            string printingKey = textBox1.Text;
            PrintingViaWebSocketsVariables.Statics.MachineName = this.textBox1.Text;
            //StaticSettings.gGlobalSettings.MachineOwnerID = this.textBox1.Text;
            RecheckConfiguration(false);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //recheck configuration
            RecheckConfiguration();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //continue
            this.Close();
        }

        private void ClientPrintingSetup_Load(object sender, EventArgs e)
        {
            var machineName =PrintingViaWebSocketsVariables.Statics.MachineName;
            //var machineName = StaticSettings.gGlobalSettings.MachineOwnerID;
            if (machineName.HasText())
            {
                this.textBox1.Text = machineName;
                if (PrintingViaWebSockets.IsWebSocketConnectionOpen(machineName))
                {
                    var applicationKey = clsPrinterFunctions.GetApplicationKey();
                    if (applicationKey.HasText())
                    {
                        this.textBox1.Text = applicationKey;
                    }
                }
            }
        }

        private bool RecheckConfiguration(bool showMessage = true)
        {
            var machineName = textBox1.Text;
            if (machineName.IsNullOrWhiteSpace())
            {
                //machineName = StaticSettings.gGlobalSettings.MachineOwnerID;
                machineName = PrintingViaWebSocketsVariables.Statics.MachineName;
                // textBox1.Text = machineName;
            }
	        bool webSocketConnetionOpen = PrintingViaWebSockets.IsWebSocketConnectionOpen(machineName);
            if (!webSocketConnetionOpen)
            {
                button1.Enabled = false;
                if (showMessage)
                {
                    MessageBox.Show("Connection not established", "Connection not established", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                return false;
            }
            else
            {
                StaticSettings.gGlobalSettings.MachineOwnerID = machineName;
                button1.Enabled = true;
                return true;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
