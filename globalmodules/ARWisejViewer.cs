﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using Wisej.Core;
using Wisej.Web;

namespace HTML5Viewer
{
    public class ARWisejViewer : Wisej.Web.Widget
    {
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string ReportServiceUrl
        {
            get
            {
                return Application.ServerVariables["HTTP_REFERER"] + "ActiveReports.ReportService.asmx";
                //return "HTML5Viewer.CustomReportService.cs";
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public string Report { get; set; }

        /// <summary>
        /// Overridden to create our initialization script.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override string InitScript
        {
            get { return BuildInitScript(); }
            set { }
        }

        /// <summary>
        /// Overridden to return our list of script resources.
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public override List<Package> Packages
        {
            get
            {
                if (base.Packages.Count == 0)
                {
                    string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                    // initialize the loader with the required libraries.
                    base.Packages.Add(new Package()
                    {
                        Name = "jquery.js",
                        Source = "http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"
                    });
                    base.Packages.Add(new Package()
                    {
                        Name = "bootstrap.js",
                        Source = "http://netdna.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"
                    });
                    base.Packages.Add(new Package()
                    {
                        Name = "knockout.js",
                        Source = "http://cdnjs.cloudflare.com/ajax/libs/knockout/3.2.0/knockout-min.js"
                    });
                    base.Packages.Add(new Package()
                    {
                        Name = "bootstrap.css",
                        Source = "http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"
                    });
                    base.Packages.Add(new Package()
                    {
                        Name = "GrapeCity.ActiveReports.Viewer.Html.js",
                        Source = GetResourceURL(assemblyName + ".JavaScript.GrapeCity.ActiveReports.Viewer.Html.js")
                    });
                    base.Packages.Add(new Package()
                    {
                        Name = "GrapeCity.ActiveReports.Viewer.Html.css",
                        Source = GetResourceURL(assemblyName + ".JavaScript.GrapeCity.ActiveReports.Viewer.Html.css")
                    });
                }

                return base.Packages;
            }
        }

        protected override void OnWebRequest(WebRequestEventArgs e)
        {
            base.OnWebRequest(e);
        }

        protected override void OnWebEvent(WisejEventArgs e)
        {
            base.OnWebEvent(e);
        }

        //protected override void OnWebRender([Dynamic] dynamic config)
        //{
        //    base.OnWebRender(config);
        //}

        private string BuildInitScript()
        {
            string assemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;

            IWisejControl me = this;
            dynamic options = new DynamicObject();
            string script = GetResourceString(assemblyName + ".JavaScript.startup.js");

            options.ReportServiceUrl = ReportServiceUrl;
            options.Report = Report;
            script = script.Replace("$options", options.ToString());

            return script;
        }
    }
}