﻿//Fecher vbPorter - Version 1.0.0.27
using System.Collections.Generic;

namespace Global
{
	public class cBDDepartment
	{
		//=========================================================
		private string strShortDescription = string.Empty;
		private string strLongDescription = string.Empty;
		private string strFund = string.Empty;
		private Dictionary<object, object> dictDivisions = new Dictionary<object, object>();
		private int lngRecordNumber;
		private string strDepartment = string.Empty;

		public string Department
		{
			set
			{
				strDepartment = value;
			}
			get
			{
				string Department = "";
				Department = strDepartment;
				return Department;
			}
		}

		public int ID
		{
			set
			{
				lngRecordNumber = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordNumber;
				return ID;
			}
		}

		public Dictionary<object, object> Divisions
		{
			get
			{
				Dictionary<object, object> Divisions = new Dictionary<object, object>();
				Divisions = dictDivisions;
				return Divisions;
			}
		}

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}

		public string Description
		{
			set
			{
				strLongDescription = value;
			}
			get
			{
				string Description = "";
				Description = strLongDescription;
				return Description;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}
	}
}
