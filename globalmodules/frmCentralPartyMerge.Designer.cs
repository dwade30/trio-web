//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;
using fecherFoundation;

#if TWGNENTY
using TWGNENTY;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmCentralPartyMerge.
	/// </summary>
	partial class frmCentralPartyMerge : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSpecific;
		public fecherFoundation.FCLabel lblSpecific;
		public fecherFoundation.FCFrame fraMatchSearch;
		public fecherFoundation.FCButton cmdMatchSearch;
		public fecherFoundation.FCFrame fraSpecific;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCTextBox txtCustomerNumber;
		public fecherFoundation.FCLabel lblCustomerInfo;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblCustomerNumber;
		public fecherFoundation.FCPanel fraMergeWizardStep1;
		public fecherFoundation.FCButton cmdStep1ClearAll;
		public fecherFoundation.FCButton cmdStep1SelectAll;
		public fecherFoundation.FCButton cmdStep1Next;
		public fecherFoundation.FCButton cmdMergeComplete;
		public fecherFoundation.FCGrid vsParties;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCPanel Frame1;
		public fecherFoundation.FCProgressBar prgDataMerged;
		public fecherFoundation.FCLabel lblPartiesChecked;
		public fecherFoundation.FCLabel shpTotalProgress;
		public fecherFoundation.FCPanel fraMergeWizardStep2;
		public fecherFoundation.FCButton cmdStep2Next;
		public fecherFoundation.FCButton cmdStep2Back;
		public fecherFoundation.FCGrid vsSelectedParties;
		public Wisej.Web.TreeView TreeView1;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCPanel fraMergeWizardStep6;
		public fecherFoundation.FCButton cmdStep6Next;
		public fecherFoundation.FCButton cmdStep6Back;
		public fecherFoundation.FCGrid vsAddressTypes;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCPanel fraMergeWizardStep5;
		public fecherFoundation.FCButton cmdStep5Back;
		public fecherFoundation.FCButton cmdStep5Next;
		public fecherFoundation.FCButton cmdStep5SelectAll;
		public fecherFoundation.FCButton cmdStep5ClearAll;
		public fecherFoundation.FCGrid vsContacts;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCPanel fraMergeWizardStep4;
		public fecherFoundation.FCButton cmdStep4ClearAll;
		public fecherFoundation.FCButton cmdStep4SelectAll;
		public fecherFoundation.FCButton cmdStep4Next;
		public fecherFoundation.FCButton cmdStep4Back;
		public fecherFoundation.FCGrid vsPhones;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCPanel fraMergeWizardStep3;
		public fecherFoundation.FCButton cmdStep3Back;
		public fecherFoundation.FCButton cmdStep3Next;
		public fecherFoundation.FCButton cmdStep3SelectAll;
		public fecherFoundation.FCButton cmdStep3ClearAll;
		public fecherFoundation.FCGrid vsAddresses;
		public fecherFoundation.FCLabel Label4;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCentralPartyMerge));
            this.cmbSpecific = new fecherFoundation.FCComboBox();
            this.lblSpecific = new fecherFoundation.FCLabel();
            this.fraMatchSearch = new fecherFoundation.FCFrame();
            this.cmdMatchSearch = new fecherFoundation.FCButton();
            this.fraSpecific = new fecherFoundation.FCFrame();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.txtCustomerNumber = new fecherFoundation.FCTextBox();
            this.lblCustomerInfo = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblCustomerNumber = new fecherFoundation.FCLabel();
            this.fraMergeWizardStep1 = new fecherFoundation.FCPanel();
            this.cmdStep1ClearAll = new fecherFoundation.FCButton();
            this.cmdStep1SelectAll = new fecherFoundation.FCButton();
            this.cmdStep1Next = new fecherFoundation.FCButton();
            this.cmdMergeComplete = new fecherFoundation.FCButton();
            this.Label3 = new fecherFoundation.FCLabel();
            this.vsParties = new fecherFoundation.FCGrid();
            this.Frame1 = new fecherFoundation.FCPanel();
            this.prgDataMerged = new fecherFoundation.FCProgressBar();
            this.lblPartiesChecked = new fecherFoundation.FCLabel();
            this.shpTotalProgress = new fecherFoundation.FCLabel();
            this.fraMergeWizardStep2 = new fecherFoundation.FCPanel();
            this.cmdStep2Next = new fecherFoundation.FCButton();
            this.cmdStep2Back = new fecherFoundation.FCButton();
            this.vsSelectedParties = new fecherFoundation.FCGrid();
            this.TreeView1 = new Wisej.Web.TreeView();
            this.Label1 = new fecherFoundation.FCLabel();
            this.fraMergeWizardStep6 = new fecherFoundation.FCPanel();
            this.cmdStep6Next = new fecherFoundation.FCButton();
            this.cmdStep6Back = new fecherFoundation.FCButton();
            this.vsAddressTypes = new fecherFoundation.FCGrid();
            this.Label7 = new fecherFoundation.FCLabel();
            this.fraMergeWizardStep5 = new fecherFoundation.FCPanel();
            this.cmdStep5Back = new fecherFoundation.FCButton();
            this.cmdStep5Next = new fecherFoundation.FCButton();
            this.cmdStep5SelectAll = new fecherFoundation.FCButton();
            this.cmdStep5ClearAll = new fecherFoundation.FCButton();
            this.vsContacts = new fecherFoundation.FCGrid();
            this.Label6 = new fecherFoundation.FCLabel();
            this.fraMergeWizardStep4 = new fecherFoundation.FCPanel();
            this.cmdStep4ClearAll = new fecherFoundation.FCButton();
            this.cmdStep4SelectAll = new fecherFoundation.FCButton();
            this.cmdStep4Next = new fecherFoundation.FCButton();
            this.cmdStep4Back = new fecherFoundation.FCButton();
            this.vsPhones = new fecherFoundation.FCGrid();
            this.Label5 = new fecherFoundation.FCLabel();
            this.fraMergeWizardStep3 = new fecherFoundation.FCPanel();
            this.cmdStep3Back = new fecherFoundation.FCButton();
            this.cmdStep3Next = new fecherFoundation.FCButton();
            this.cmdStep3SelectAll = new fecherFoundation.FCButton();
            this.cmdStep3ClearAll = new fecherFoundation.FCButton();
            this.vsAddresses = new fecherFoundation.FCGrid();
            this.Label4 = new fecherFoundation.FCLabel();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMatchSearch)).BeginInit();
            this.fraMatchSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMatchSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSpecific)).BeginInit();
            this.fraSpecific.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMergeWizardStep1)).BeginInit();
            this.fraMergeWizardStep1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep1ClearAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep1SelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep1Next)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMergeComplete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsParties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMergeWizardStep2)).BeginInit();
            this.fraMergeWizardStep2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep2Next)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep2Back)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSelectedParties)).BeginInit();
            this.vsSelectedParties.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMergeWizardStep6)).BeginInit();
            this.fraMergeWizardStep6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep6Next)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep6Back)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsAddressTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMergeWizardStep5)).BeginInit();
            this.fraMergeWizardStep5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep5Back)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep5Next)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep5SelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep5ClearAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsContacts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMergeWizardStep4)).BeginInit();
            this.fraMergeWizardStep4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep4ClearAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep4SelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep4Next)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep4Back)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsPhones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMergeWizardStep3)).BeginInit();
            this.fraMergeWizardStep3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep3Back)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep3Next)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep3SelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep3ClearAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsAddresses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 682);
            this.BottomPanel.Size = new System.Drawing.Size(660, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.fraMatchSearch);
            this.ClientArea.Controls.Add(this.fraMergeWizardStep3);
            this.ClientArea.Controls.Add(this.fraMergeWizardStep1);
            this.ClientArea.Controls.Add(this.fraMergeWizardStep2);
            this.ClientArea.Controls.Add(this.fraMergeWizardStep4);
            this.ClientArea.Controls.Add(this.fraMergeWizardStep6);
            this.ClientArea.Controls.Add(this.fraMergeWizardStep5);
            this.ClientArea.Size = new System.Drawing.Size(660, 622);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(660, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(232, 30);
            this.HeaderText.Text = "Central Party Merge";
            // 
            // cmbSpecific
            // 
            this.cmbSpecific.Items.AddRange(new object[] {
            "All",
            "Single"});
            this.cmbSpecific.Location = new System.Drawing.Point(201, 0);
            this.cmbSpecific.Name = "cmbSpecific";
            this.cmbSpecific.Size = new System.Drawing.Size(238, 40);
            this.cmbSpecific.TabIndex = 2;
            this.cmbSpecific.Text = "All";
            this.cmbSpecific.SelectedIndexChanged += new System.EventHandler(this.cmbSpecific_SelectedIndexChanged);
            // 
            // lblSpecific
            // 
            this.lblSpecific.AutoSize = true;
            this.lblSpecific.Location = new System.Drawing.Point(0, 14);
            this.lblSpecific.Name = "lblSpecific";
            this.lblSpecific.Size = new System.Drawing.Size(142, 15);
            this.lblSpecific.TabIndex = 3;
            this.lblSpecific.Text = "CUSTOMER MATCHES";
            this.lblSpecific.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraMatchSearch
            // 
            this.fraMatchSearch.AppearanceKey = "groupBoxNoBorders";
            this.fraMatchSearch.Controls.Add(this.cmdMatchSearch);
            this.fraMatchSearch.Controls.Add(this.cmbSpecific);
            this.fraMatchSearch.Controls.Add(this.lblSpecific);
            this.fraMatchSearch.Controls.Add(this.fraSpecific);
            this.fraMatchSearch.Location = new System.Drawing.Point(30, 20);
            this.fraMatchSearch.Name = "fraMatchSearch";
            this.fraMatchSearch.Size = new System.Drawing.Size(500, 370);
            // 
            // cmdMatchSearch
            // 
            this.cmdMatchSearch.AppearanceKey = "actionButton";
            this.cmdMatchSearch.Location = new System.Drawing.Point(0, 220);
            this.cmdMatchSearch.Name = "cmdMatchSearch";
            this.cmdMatchSearch.Size = new System.Drawing.Size(94, 40);
            this.cmdMatchSearch.TabIndex = 1;
            this.cmdMatchSearch.Text = "Search";
            this.cmdMatchSearch.Click += new System.EventHandler(this.cmdMatchSearch_Click);
            // 
            // fraSpecific
            // 
            this.fraSpecific.AppearanceKey = "groupBoxNoBorders";
            this.fraSpecific.Controls.Add(this.cmdSearch);
            this.fraSpecific.Controls.Add(this.txtCustomerNumber);
            this.fraSpecific.Controls.Add(this.lblCustomerInfo);
            this.fraSpecific.Controls.Add(this.Label2);
            this.fraSpecific.Controls.Add(this.lblCustomerNumber);
            this.fraSpecific.Enabled = false;
            this.fraSpecific.Location = new System.Drawing.Point(0, 60);
            this.fraSpecific.Name = "fraSpecific";
            this.fraSpecific.Size = new System.Drawing.Size(456, 140);
            this.fraSpecific.TabIndex = 3;
            // 
            // cmdSearch
            // 
            this.cmdSearch.AppearanceKey = "actionButton";
            this.cmdSearch.ImageSource = "button-search";
            this.cmdSearch.Location = new System.Drawing.Point(399, 0);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(40, 40);
            this.cmdSearch.TabIndex = 5;
            this.cmdSearch.TextImageRelation = Wisej.Web.TextImageRelation.ImageAboveText;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // txtCustomerNumber
            // 
            this.txtCustomerNumber.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
            this.txtCustomerNumber.Location = new System.Drawing.Point(201, 0);
            this.txtCustomerNumber.Name = "txtCustomerNumber";
            this.txtCustomerNumber.Size = new System.Drawing.Size(177, 40);
            this.txtCustomerNumber.TabIndex = 4;
            this.txtCustomerNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtCustomerNumber_Validating);
            this.txtCustomerNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCustomerNumber_KeyPress);
            // 
            // lblCustomerInfo
            // 
            this.lblCustomerInfo.Location = new System.Drawing.Point(201, 54);
            this.lblCustomerInfo.Name = "lblCustomerInfo";
            this.lblCustomerInfo.Size = new System.Drawing.Size(238, 80);
            this.lblCustomerInfo.TabIndex = 8;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(6, 71);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(72, 18);
            this.Label2.TabIndex = 7;
            this.Label2.Text = "CUSTOMER";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.Label2.Visible = false;
            // 
            // lblCustomerNumber
            // 
            this.lblCustomerNumber.Location = new System.Drawing.Point(0, 14);
            this.lblCustomerNumber.Name = "lblCustomerNumber";
            this.lblCustomerNumber.Size = new System.Drawing.Size(96, 18);
            this.lblCustomerNumber.TabIndex = 6;
            this.lblCustomerNumber.Text = "CUSTOMER #";
            this.lblCustomerNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraMergeWizardStep1
            // 
            this.fraMergeWizardStep1.Controls.Add(this.cmdStep1ClearAll);
            this.fraMergeWizardStep1.Controls.Add(this.cmdStep1SelectAll);
            this.fraMergeWizardStep1.Controls.Add(this.cmdStep1Next);
            this.fraMergeWizardStep1.Controls.Add(this.cmdMergeComplete);
            this.fraMergeWizardStep1.Controls.Add(this.Label3);
            this.fraMergeWizardStep1.Controls.Add(this.vsParties);
            this.fraMergeWizardStep1.Location = new System.Drawing.Point(30, 0);
            this.fraMergeWizardStep1.Name = "fraMergeWizardStep1";
            this.fraMergeWizardStep1.Size = new System.Drawing.Size(612, 532);
            this.fraMergeWizardStep1.TabIndex = 10;
            this.fraMergeWizardStep1.Visible = false;
            // 
            // cmdStep1ClearAll
            // 
            this.cmdStep1ClearAll.AppearanceKey = "actionButton";
            this.cmdStep1ClearAll.Location = new System.Drawing.Point(512, 116);
            this.cmdStep1ClearAll.Name = "cmdStep1ClearAll";
            this.cmdStep1ClearAll.Size = new System.Drawing.Size(100, 40);
            this.cmdStep1ClearAll.TabIndex = 15;
            this.cmdStep1ClearAll.Text = "Clear All";
            this.cmdStep1ClearAll.Click += new System.EventHandler(this.cmdStep1ClearAll_Click);
            // 
            // cmdStep1SelectAll
            // 
            this.cmdStep1SelectAll.AppearanceKey = "actionButton";
            this.cmdStep1SelectAll.Location = new System.Drawing.Point(512, 56);
            this.cmdStep1SelectAll.Name = "cmdStep1SelectAll";
            this.cmdStep1SelectAll.Size = new System.Drawing.Size(100, 40);
            this.cmdStep1SelectAll.TabIndex = 14;
            this.cmdStep1SelectAll.Text = "Select All";
            this.cmdStep1SelectAll.Click += new System.EventHandler(this.cmdStep1SelectAll_Click);
            // 
            // cmdStep1Next
            // 
            this.cmdStep1Next.AppearanceKey = "actionButton";
            this.cmdStep1Next.Location = new System.Drawing.Point(180, 492);
            this.cmdStep1Next.Name = "cmdStep1Next";
            this.cmdStep1Next.Size = new System.Drawing.Size(120, 40);
            this.cmdStep1Next.TabIndex = 13;
            this.cmdStep1Next.Text = "Next >";
            this.cmdStep1Next.Click += new System.EventHandler(this.cmdStep1Next_Click);
            // 
            // cmdMergeComplete
            // 
            this.cmdMergeComplete.AppearanceKey = "actionButton";
            this.cmdMergeComplete.Location = new System.Drawing.Point(0, 492);
            this.cmdMergeComplete.Name = "cmdMergeComplete";
            this.cmdMergeComplete.Size = new System.Drawing.Size(160, 40);
            this.cmdMergeComplete.TabIndex = 12;
            this.cmdMergeComplete.Text = "Merge Complete";
            this.cmdMergeComplete.Click += new System.EventHandler(this.cmdMergeComplete_Click);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(0, 14);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(500, 35);
            this.Label3.TabIndex = 16;
            this.Label3.Text = "SELECT CUSTOMERS THAT YOU WISH TO MERGE AND CLICK NEXT OR CLICK ON MERGE COMPLETE" +
    " IF THERE ARE NO PARTIES YOU WISH TO MERGE";
            // 
            // vsParties
            // 
            this.vsParties.Cols = 4;
            this.vsParties.ColumnHeadersVisible = false;
            this.vsParties.ExtendLastCol = true;
            this.vsParties.FixedCols = 0;
            this.vsParties.FixedRows = 0;
            this.vsParties.Location = new System.Drawing.Point(0, 56);
            this.vsParties.Name = "vsParties";
            this.vsParties.RowHeadersVisible = false;
            this.vsParties.Rows = 0;
            this.vsParties.ShowFocusCell = false;
            this.vsParties.Size = new System.Drawing.Size(492, 426);
            this.vsParties.TabIndex = 11;
            this.vsParties.Click += new System.EventHandler(this.vsParties_ClickEvent);
            this.vsParties.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsParties_KeyPressEvent);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.prgDataMerged);
            this.Frame1.Controls.Add(this.lblPartiesChecked);
            this.Frame1.Controls.Add(this.shpTotalProgress);
            this.Frame1.Location = new System.Drawing.Point(30, 20);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(320, 45);
            this.Frame1.TabIndex = 49;
            this.Frame1.Visible = false;
            // 
            // prgDataMerged
            // 
            this.prgDataMerged.Location = new System.Drawing.Point(146, 14);
            this.prgDataMerged.Name = "prgDataMerged";
            this.prgDataMerged.Size = new System.Drawing.Size(141, 20);
            this.prgDataMerged.TabIndex = 50;
            // 
            // lblPartiesChecked
            // 
            this.lblPartiesChecked.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            this.lblPartiesChecked.Location = new System.Drawing.Point(24, 15);
            this.lblPartiesChecked.Name = "lblPartiesChecked";
            this.lblPartiesChecked.Size = new System.Drawing.Size(120, 18);
            this.lblPartiesChecked.TabIndex = 51;
            this.lblPartiesChecked.Text = "PARTIES CHECKED";
            // 
            // shpTotalProgress
            // 
            this.shpTotalProgress.BackColor = System.Drawing.Color.White;
            this.shpTotalProgress.Location = new System.Drawing.Point(8, 7);
            this.shpTotalProgress.Name = "shpTotalProgress";
            this.shpTotalProgress.Size = new System.Drawing.Size(298, 32);
            this.shpTotalProgress.TabIndex = 52;
            // 
            // fraMergeWizardStep2
            // 
            this.fraMergeWizardStep2.Controls.Add(this.cmdStep2Next);
            this.fraMergeWizardStep2.Controls.Add(this.cmdStep2Back);
            this.fraMergeWizardStep2.Controls.Add(this.vsSelectedParties);
            this.fraMergeWizardStep2.Controls.Add(this.Label1);
            this.fraMergeWizardStep2.Location = new System.Drawing.Point(30, 0);
            this.fraMergeWizardStep2.Name = "fraMergeWizardStep2";
            this.fraMergeWizardStep2.Size = new System.Drawing.Size(612, 532);
            this.fraMergeWizardStep2.TabIndex = 43;
            this.fraMergeWizardStep2.Visible = false;
            // 
            // cmdStep2Next
            // 
            this.cmdStep2Next.AppearanceKey = "actionButton";
            this.cmdStep2Next.Location = new System.Drawing.Point(140, 492);
            this.cmdStep2Next.Name = "cmdStep2Next";
            this.cmdStep2Next.Size = new System.Drawing.Size(120, 40);
            this.cmdStep2Next.TabIndex = 45;
            this.cmdStep2Next.Text = "Next >";
            this.cmdStep2Next.Click += new System.EventHandler(this.cmdStep2Next_Click);
            // 
            // cmdStep2Back
            // 
            this.cmdStep2Back.AppearanceKey = "actionButton";
            this.cmdStep2Back.Location = new System.Drawing.Point(0, 492);
            this.cmdStep2Back.Name = "cmdStep2Back";
            this.cmdStep2Back.Size = new System.Drawing.Size(120, 40);
            this.cmdStep2Back.TabIndex = 44;
            this.cmdStep2Back.Text = "< Back";
            this.cmdStep2Back.Click += new System.EventHandler(this.cmdStep2Back_Click);
            // 
            // vsSelectedParties
            // 
            this.vsSelectedParties.Cols = 4;
            this.vsSelectedParties.ColumnHeadersVisible = false;
            this.vsSelectedParties.Controls.Add(this.TreeView1);
            this.vsSelectedParties.ExtendLastCol = true;
            this.vsSelectedParties.FixedCols = 0;
            this.vsSelectedParties.FixedRows = 0;
            this.vsSelectedParties.Location = new System.Drawing.Point(0, 56);
            this.vsSelectedParties.Name = "vsSelectedParties";
            this.vsSelectedParties.RowHeadersVisible = false;
            this.vsSelectedParties.Rows = 0;
            this.vsSelectedParties.ShowFocusCell = false;
            this.vsSelectedParties.Size = new System.Drawing.Size(492, 426);
            this.vsSelectedParties.TabIndex = 46;
            this.vsSelectedParties.Click += new System.EventHandler(this.vsSelectedParties_ClickEvent);
            this.vsSelectedParties.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsSelectedParties_KeyPressEvent);
            // 
            // TreeView1
            // 
            this.TreeView1.BackColor = System.Drawing.SystemColors.Window;
            this.TreeView1.LabelEdit = true;
            this.TreeView1.Location = new System.Drawing.Point(245, 19);
            this.TreeView1.Name = "TreeView1";
            this.TreeView1.ShowRootLines = false;
            this.TreeView1.Size = new System.Drawing.Size(2, 2);
            this.TreeView1.TabIndex = 48;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(0, 14);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(500, 36);
            this.Label1.TabIndex = 47;
            this.Label1.Text = "SELECT THE PARTY TO MERGE TO. ADDRESSES ARE FOR REFERENCE ONLY AND WILL BE CHOSEN" +
    " IN THE NEXT STEP";
            // 
            // fraMergeWizardStep6
            // 
            this.fraMergeWizardStep6.Controls.Add(this.cmdStep6Next);
            this.fraMergeWizardStep6.Controls.Add(this.cmdStep6Back);
            this.fraMergeWizardStep6.Controls.Add(this.vsAddressTypes);
            this.fraMergeWizardStep6.Controls.Add(this.Label7);
            this.fraMergeWizardStep6.Location = new System.Drawing.Point(30, 0);
            this.fraMergeWizardStep6.Name = "fraMergeWizardStep6";
            this.fraMergeWizardStep6.Size = new System.Drawing.Size(612, 532);
            this.fraMergeWizardStep6.TabIndex = 38;
            this.fraMergeWizardStep6.Visible = false;
            // 
            // cmdStep6Next
            // 
            this.cmdStep6Next.AppearanceKey = "actionButton";
            this.cmdStep6Next.Location = new System.Drawing.Point(140, 492);
            this.cmdStep6Next.Name = "cmdStep6Next";
            this.cmdStep6Next.Size = new System.Drawing.Size(120, 40);
            this.cmdStep6Next.TabIndex = 40;
            this.cmdStep6Next.Text = "Finish";
            this.cmdStep6Next.Click += new System.EventHandler(this.cmdStep6Next_Click);
            // 
            // cmdStep6Back
            // 
            this.cmdStep6Back.AppearanceKey = "actionButton";
            this.cmdStep6Back.Location = new System.Drawing.Point(0, 492);
            this.cmdStep6Back.Name = "cmdStep6Back";
            this.cmdStep6Back.Size = new System.Drawing.Size(120, 40);
            this.cmdStep6Back.TabIndex = 39;
            this.cmdStep6Back.Text = "< Back";
            this.cmdStep6Back.Click += new System.EventHandler(this.cmdStep6Back_Click);
            // 
            // vsAddressTypes
            // 
            this.vsAddressTypes.Cols = 4;
            this.vsAddressTypes.ColumnHeadersVisible = false;
            this.vsAddressTypes.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsAddressTypes.ExtendLastCol = true;
            this.vsAddressTypes.FixedCols = 0;
            this.vsAddressTypes.FixedRows = 0;
            this.vsAddressTypes.Location = new System.Drawing.Point(0, 56);
            this.vsAddressTypes.Name = "vsAddressTypes";
            this.vsAddressTypes.ReadOnly = false;
            this.vsAddressTypes.RowHeadersVisible = false;
            this.vsAddressTypes.Rows = 0;
            this.vsAddressTypes.ShowFocusCell = false;
            this.vsAddressTypes.Size = new System.Drawing.Size(492, 426);
            this.vsAddressTypes.TabIndex = 41;
            this.vsAddressTypes.CurrentCellChanged += new System.EventHandler(this.vsAddressTypes_RowColChange);
            this.vsAddressTypes.Click += new System.EventHandler(this.vsAddressTypes_ClickEvent);
            this.vsAddressTypes.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsAddressTypes_KeyPressEvent);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(0, 14);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(500, 35);
            this.Label7.TabIndex = 42;
            this.Label7.Text = "YOU MAY ONLY HAVE 1 PRIMARY ADDRESS FOR A PARTY.  SINCE YOU SELECTED TO KEEP MULT" +
    "IPLE ADDRESSES YOU WILL NEED TO SELECT WHICH ONE IS PRIMARY. ";
            // 
            // fraMergeWizardStep5
            // 
            this.fraMergeWizardStep5.Controls.Add(this.cmdStep5Back);
            this.fraMergeWizardStep5.Controls.Add(this.cmdStep5Next);
            this.fraMergeWizardStep5.Controls.Add(this.cmdStep5SelectAll);
            this.fraMergeWizardStep5.Controls.Add(this.cmdStep5ClearAll);
            this.fraMergeWizardStep5.Controls.Add(this.vsContacts);
            this.fraMergeWizardStep5.Controls.Add(this.Label6);
            this.fraMergeWizardStep5.Location = new System.Drawing.Point(30, 0);
            this.fraMergeWizardStep5.Name = "fraMergeWizardStep5";
            this.fraMergeWizardStep5.Size = new System.Drawing.Size(612, 532);
            this.fraMergeWizardStep5.TabIndex = 31;
            this.fraMergeWizardStep5.Visible = false;
            // 
            // cmdStep5Back
            // 
            this.cmdStep5Back.AppearanceKey = "actionButton";
            this.cmdStep5Back.Location = new System.Drawing.Point(0, 492);
            this.cmdStep5Back.Name = "cmdStep5Back";
            this.cmdStep5Back.Size = new System.Drawing.Size(120, 40);
            this.cmdStep5Back.TabIndex = 35;
            this.cmdStep5Back.Text = "< Back";
            this.cmdStep5Back.Click += new System.EventHandler(this.cmdStep5Back_Click);
            // 
            // cmdStep5Next
            // 
            this.cmdStep5Next.AppearanceKey = "actionButton";
            this.cmdStep5Next.Location = new System.Drawing.Point(140, 492);
            this.cmdStep5Next.Name = "cmdStep5Next";
            this.cmdStep5Next.Size = new System.Drawing.Size(116, 40);
            this.cmdStep5Next.TabIndex = 34;
            this.cmdStep5Next.Text = "Finish";
            this.cmdStep5Next.Click += new System.EventHandler(this.cmdStep5Next_Click);
            // 
            // cmdStep5SelectAll
            // 
            this.cmdStep5SelectAll.AppearanceKey = "actionButton";
            this.cmdStep5SelectAll.Location = new System.Drawing.Point(512, 56);
            this.cmdStep5SelectAll.Name = "cmdStep5SelectAll";
            this.cmdStep5SelectAll.Size = new System.Drawing.Size(100, 40);
            this.cmdStep5SelectAll.TabIndex = 33;
            this.cmdStep5SelectAll.Text = "Select All";
            this.cmdStep5SelectAll.Click += new System.EventHandler(this.cmdStep5SelectAll_Click);
            // 
            // cmdStep5ClearAll
            // 
            this.cmdStep5ClearAll.AppearanceKey = "actionButton";
            this.cmdStep5ClearAll.Location = new System.Drawing.Point(512, 116);
            this.cmdStep5ClearAll.Name = "cmdStep5ClearAll";
            this.cmdStep5ClearAll.Size = new System.Drawing.Size(100, 40);
            this.cmdStep5ClearAll.TabIndex = 32;
            this.cmdStep5ClearAll.Text = "Clear All";
            this.cmdStep5ClearAll.Click += new System.EventHandler(this.cmdStep5ClearAll_Click);
            // 
            // vsContacts
            // 
            this.vsContacts.Cols = 4;
            this.vsContacts.ColumnHeadersVisible = false;
            this.vsContacts.ExtendLastCol = true;
            this.vsContacts.FixedCols = 0;
            this.vsContacts.FixedRows = 0;
            this.vsContacts.Location = new System.Drawing.Point(0, 56);
            this.vsContacts.Name = "vsContacts";
            this.vsContacts.RowHeadersVisible = false;
            this.vsContacts.Rows = 0;
            this.vsContacts.ShowFocusCell = false;
            this.vsContacts.Size = new System.Drawing.Size(492, 426);
            this.vsContacts.TabIndex = 36;
            this.vsContacts.Click += new System.EventHandler(this.vsContacts_ClickEvent);
            this.vsContacts.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsContacts_KeyPressEvent);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(0, 14);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(500, 35);
            this.Label6.TabIndex = 37;
            this.Label6.Text = "SELECT ONE OR MORE OF THE CONTACTS ATTACHED TO THE PARTIES YOU ARE MERGING THAT Y" +
    "OU WISH TO KEEP";
            // 
            // fraMergeWizardStep4
            // 
            this.fraMergeWizardStep4.Controls.Add(this.cmdStep4ClearAll);
            this.fraMergeWizardStep4.Controls.Add(this.cmdStep4SelectAll);
            this.fraMergeWizardStep4.Controls.Add(this.cmdStep4Next);
            this.fraMergeWizardStep4.Controls.Add(this.cmdStep4Back);
            this.fraMergeWizardStep4.Controls.Add(this.vsPhones);
            this.fraMergeWizardStep4.Controls.Add(this.Label5);
            this.fraMergeWizardStep4.Location = new System.Drawing.Point(30, 0);
            this.fraMergeWizardStep4.Name = "fraMergeWizardStep4";
            this.fraMergeWizardStep4.Size = new System.Drawing.Size(612, 532);
            this.fraMergeWizardStep4.TabIndex = 24;
            this.fraMergeWizardStep4.Visible = false;
            // 
            // cmdStep4ClearAll
            // 
            this.cmdStep4ClearAll.AppearanceKey = "actionButton";
            this.cmdStep4ClearAll.Location = new System.Drawing.Point(512, 116);
            this.cmdStep4ClearAll.Name = "cmdStep4ClearAll";
            this.cmdStep4ClearAll.Size = new System.Drawing.Size(100, 40);
            this.cmdStep4ClearAll.TabIndex = 28;
            this.cmdStep4ClearAll.Text = "Clear All";
            this.cmdStep4ClearAll.Click += new System.EventHandler(this.cmdStep4ClearAll_Click);
            // 
            // cmdStep4SelectAll
            // 
            this.cmdStep4SelectAll.AppearanceKey = "actionButton";
            this.cmdStep4SelectAll.Location = new System.Drawing.Point(512, 56);
            this.cmdStep4SelectAll.Name = "cmdStep4SelectAll";
            this.cmdStep4SelectAll.Size = new System.Drawing.Size(100, 40);
            this.cmdStep4SelectAll.TabIndex = 27;
            this.cmdStep4SelectAll.Text = "Select All";
            this.cmdStep4SelectAll.Click += new System.EventHandler(this.cmdStep4SelectAll_Click);
            // 
            // cmdStep4Next
            // 
            this.cmdStep4Next.AppearanceKey = "actionButton";
            this.cmdStep4Next.Location = new System.Drawing.Point(140, 492);
            this.cmdStep4Next.Name = "cmdStep4Next";
            this.cmdStep4Next.Size = new System.Drawing.Size(120, 40);
            this.cmdStep4Next.TabIndex = 26;
            this.cmdStep4Next.Text = "Next >";
            this.cmdStep4Next.Click += new System.EventHandler(this.cmdStep4Next_Click);
            // 
            // cmdStep4Back
            // 
            this.cmdStep4Back.AppearanceKey = "actionButton";
            this.cmdStep4Back.Location = new System.Drawing.Point(0, 492);
            this.cmdStep4Back.Name = "cmdStep4Back";
            this.cmdStep4Back.Size = new System.Drawing.Size(120, 40);
            this.cmdStep4Back.TabIndex = 25;
            this.cmdStep4Back.Text = "< Back";
            this.cmdStep4Back.Click += new System.EventHandler(this.cmdStep4Back_Click);
            // 
            // vsPhones
            // 
            this.vsPhones.Cols = 4;
            this.vsPhones.ColumnHeadersVisible = false;
            this.vsPhones.ExtendLastCol = true;
            this.vsPhones.FixedCols = 0;
            this.vsPhones.FixedRows = 0;
            this.vsPhones.Location = new System.Drawing.Point(0, 56);
            this.vsPhones.Name = "vsPhones";
            this.vsPhones.RowHeadersVisible = false;
            this.vsPhones.Rows = 0;
            this.vsPhones.ShowFocusCell = false;
            this.vsPhones.Size = new System.Drawing.Size(492, 426);
            this.vsPhones.TabIndex = 29;
            this.vsPhones.Click += new System.EventHandler(this.vsPhones_ClickEvent);
            this.vsPhones.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsPhones_KeyPressEvent);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(0, 14);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(500, 36);
            this.Label5.TabIndex = 30;
            this.Label5.Text = "SELECT ONE OR MORE OF THE PHONE NUMBERS ATTACHED TO THE PARTIES YOU ARE MERGING T" +
    "HAT YOU WISH TO KEEP";
            // 
            // fraMergeWizardStep3
            // 
            this.fraMergeWizardStep3.Controls.Add(this.cmdStep3Back);
            this.fraMergeWizardStep3.Controls.Add(this.cmdStep3Next);
            this.fraMergeWizardStep3.Controls.Add(this.cmdStep3SelectAll);
            this.fraMergeWizardStep3.Controls.Add(this.cmdStep3ClearAll);
            this.fraMergeWizardStep3.Controls.Add(this.vsAddresses);
            this.fraMergeWizardStep3.Controls.Add(this.Label4);
            this.fraMergeWizardStep3.Location = new System.Drawing.Point(30, 0);
            this.fraMergeWizardStep3.Name = "fraMergeWizardStep3";
            this.fraMergeWizardStep3.Size = new System.Drawing.Size(612, 532);
            this.fraMergeWizardStep3.TabIndex = 17;
            this.fraMergeWizardStep3.Visible = false;
            // 
            // cmdStep3Back
            // 
            this.cmdStep3Back.AppearanceKey = "actionButton";
            this.cmdStep3Back.Location = new System.Drawing.Point(0, 492);
            this.cmdStep3Back.Name = "cmdStep3Back";
            this.cmdStep3Back.Size = new System.Drawing.Size(120, 40);
            this.cmdStep3Back.TabIndex = 21;
            this.cmdStep3Back.Text = "< Back";
            this.cmdStep3Back.Click += new System.EventHandler(this.cmdStep3Back_Click);
            // 
            // cmdStep3Next
            // 
            this.cmdStep3Next.AppearanceKey = "actionButton";
            this.cmdStep3Next.Location = new System.Drawing.Point(140, 492);
            this.cmdStep3Next.Name = "cmdStep3Next";
            this.cmdStep3Next.Size = new System.Drawing.Size(120, 40);
            this.cmdStep3Next.TabIndex = 20;
            this.cmdStep3Next.Text = "Next >";
            this.cmdStep3Next.Click += new System.EventHandler(this.cmdStep3Next_Click);
            // 
            // cmdStep3SelectAll
            // 
            this.cmdStep3SelectAll.AppearanceKey = "actionButton";
            this.cmdStep3SelectAll.Location = new System.Drawing.Point(512, 56);
            this.cmdStep3SelectAll.Name = "cmdStep3SelectAll";
            this.cmdStep3SelectAll.Size = new System.Drawing.Size(100, 40);
            this.cmdStep3SelectAll.TabIndex = 19;
            this.cmdStep3SelectAll.Text = "Select All";
            this.cmdStep3SelectAll.Click += new System.EventHandler(this.cmdStep3SelectAll_Click);
            // 
            // cmdStep3ClearAll
            // 
            this.cmdStep3ClearAll.AppearanceKey = "actionButton";
            this.cmdStep3ClearAll.Location = new System.Drawing.Point(512, 116);
            this.cmdStep3ClearAll.Name = "cmdStep3ClearAll";
            this.cmdStep3ClearAll.Size = new System.Drawing.Size(100, 40);
            this.cmdStep3ClearAll.TabIndex = 18;
            this.cmdStep3ClearAll.Text = "Clear All";
            this.cmdStep3ClearAll.Click += new System.EventHandler(this.cmdStep3ClearAll_Click);
            // 
            // vsAddresses
            // 
            this.vsAddresses.Cols = 4;
            this.vsAddresses.ColumnHeadersVisible = false;
            this.vsAddresses.ExtendLastCol = true;
            this.vsAddresses.FixedCols = 0;
            this.vsAddresses.FixedRows = 0;
            this.vsAddresses.Location = new System.Drawing.Point(0, 56);
            this.vsAddresses.Name = "vsAddresses";
            this.vsAddresses.RowHeadersVisible = false;
            this.vsAddresses.Rows = 0;
            this.vsAddresses.ShowFocusCell = false;
            this.vsAddresses.Size = new System.Drawing.Size(492, 426);
            this.vsAddresses.TabIndex = 22;
            this.vsAddresses.Click += new System.EventHandler(this.vsAddresses_ClickEvent);
            this.vsAddresses.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsAddresses_KeyPressEvent);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(0, 14);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(500, 36);
            this.Label4.TabIndex = 23;
            this.Label4.Text = "SELECT ONE OR MORE OF THE ADDRESSES ATTACHED TO THE PARTIES YOU ARE MERGING THAT " +
    "YOU WISH TO KEEP";
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(373, 32);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(137, 48);
            this.cmdProcessSave.Text = "Save & Exit";
            this.cmdProcessSave.Visible = false;
            // 
            // frmCentralPartyMerge
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(660, 682);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCentralPartyMerge";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Central Party Merge";
            this.Load += new System.EventHandler(this.frmCentralPartyMerge_Load);
            this.Activated += new System.EventHandler(this.frmCentralPartyMerge_Activated);
            this.Resize += new System.EventHandler(this.frmCentralPartyMerge_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCentralPartyMerge_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMatchSearch)).EndInit();
            this.fraMatchSearch.ResumeLayout(false);
            this.fraMatchSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMatchSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSpecific)).EndInit();
            this.fraSpecific.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMergeWizardStep1)).EndInit();
            this.fraMergeWizardStep1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep1ClearAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep1SelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep1Next)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMergeComplete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsParties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraMergeWizardStep2)).EndInit();
            this.fraMergeWizardStep2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep2Next)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep2Back)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSelectedParties)).EndInit();
            this.vsSelectedParties.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraMergeWizardStep6)).EndInit();
            this.fraMergeWizardStep6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep6Next)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep6Back)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsAddressTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMergeWizardStep5)).EndInit();
            this.fraMergeWizardStep5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep5Back)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep5Next)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep5SelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep5ClearAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsContacts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMergeWizardStep4)).EndInit();
            this.fraMergeWizardStep4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep4ClearAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep4SelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep4Next)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep4Back)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsPhones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMergeWizardStep3)).EndInit();
            this.fraMergeWizardStep3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep3Back)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep3Next)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep3SelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStep3ClearAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsAddresses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdProcessSave;
		private System.ComponentModel.IContainer components;
	}
}