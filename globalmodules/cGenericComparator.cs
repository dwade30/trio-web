﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using System;

namespace Global
{
	public class cGenericComparator : cIComparator
	{
		//=========================================================
		// vbPorter upgrade warning: compareA As object	OnRead(cGenericSortableItem)
		// vbPorter upgrade warning: compareB As object	OnRead(cGenericSortableItem)
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short Compare(ref object compareA, ref object compareB)
		{
			short cIComparator_Compare = 0;
			cGenericSortableItem tempA;
			cGenericSortableItem tempB;
			int x;
			tempA = (cGenericSortableItem)compareA;
			tempB = (cGenericSortableItem)compareB;
			int intReturn;
			intReturn = 0;
			if (!(tempA == null))
			{
				if (!(tempB == null))
				{
					for (x = 1; x <= tempA.SortValues.Count; x++)
					{
						//FC:FINAL:SBE - compare strings cannot be done using ">" or "<"
						if (tempA.SortValues[x] is string && tempB.SortValues[x] is string)
						{
							intReturn = string.Compare(FCConvert.ToString(tempA.SortValues[x]), FCConvert.ToString(tempB.SortValues[x]));
							break;
						}
						else
						{
							if (tempA.SortValues[x] < tempB.SortValues[x])
							{
								intReturn = -1;
								break;
							}
							else if (tempA.SortValues[x] > tempB.SortValues[x])
							{
								intReturn = 1;
								break;
							}
						}
					}
				}
			}
			cIComparator_Compare = FCConvert.ToInt16(intReturn);
			return cIComparator_Compare;
		}
	}
}
