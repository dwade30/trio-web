﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptUTbyHolder : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Corey Gray              *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :               Melissa Lamprecht       *
		// LAST UPDATED   :               09/05/2007              *
		// ********************************************************
		clsDRWrapper clsre = new clsDRWrapper();
		clsDRWrapper clsMortgage = new clsDRWrapper();
		//clsDRWrapper clsAssoc = new clsDRWrapper();
		bool boolTax;
		// whether we should print tax info or not
		//clsDRWrapper clsTaxRE = new clsDRWrapper();
		int lngBYear;
		// billing year
		double dblTaxSum;
		double dblOSSum;
		double dblTotalDueSum;

		public rptUTbyHolder()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Utility Account List By Mortgage Holder";
		}

		public static rptUTbyHolder InstancePtr
		{
			get
			{
				return (rptUTbyHolder)Sys.GetInstance(typeof(rptUTbyHolder));
			}
		}

		protected rptUTbyHolder _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsre.Dispose();
				clsMortgage.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_DataInitialize(object sender, System.EventArgs e)
		{
			this.Fields.Add("TheBinder");
			this.Fields["TheBinder"].Value = 0;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsre.EndOfFile();
			if (!eArgs.EOF)
			{
				// do it here or the groupheader info will be behind the times
				this.Fields["TheBinder"].Value = clsre.Get_Fields_Int32("MortgageHolderID");
				// "MortAssoc.MortgageHolderID")
			}
			//Detail_Format();
		}

		public void Init(bool boolNameOrder, string strStart = "", string strEnd = "")
		{
			string strSQL = "";
			string strOrder = "";
			string str1;
			string str2;
			string str3;
			string str4;
			string strWhere = "";
			string strDBName;
			// MAL@20070905: Changed code (borrowed from RE report) to conform to UT requirements
			try
			{
				// On Error GoTo ErrorHandler
				if (boolNameOrder)
				{
					if (strEnd == string.Empty)
					{
						if (strStart == string.Empty)
						{
							strWhere = "";
						}
						else
						{
							strWhere = " and MortgageHolders.Name >= '" + strStart + "' ";
						}
					}
					else
					{
						strWhere = " and MortgageHolders.Name between '" + strStart + "' and '" + strEnd + "' ";
					}
				}
				else
				{
					if (strEnd == string.Empty)
					{
						if (strStart == string.Empty)
						{
							strWhere = "";
						}
						else
						{
							strWhere = " and MortgageAssociation.MortgageHolderID >= " + FCConvert.ToString(Conversion.Val(strStart)) + " ";
						}
					}
					else
					{
						strWhere = " and MortgageAssociation.MortgageHolderID between " + FCConvert.ToString(Conversion.Val(strStart)) + " and " + FCConvert.ToString(Conversion.Val(strEnd)) + " ";
					}
				}
				clsre.OpenRecordset("SELECT * from ReturnAddress", "twbl0000.vb1");
				str1 = "";
				str2 = "";
				str3 = "";
				str4 = "";
				if (!clsre.EndOfFile())
				{
					str1 = Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("Address1")));
					str2 = Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("Address2")));
					str3 = Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("Address3")));
					str4 = Strings.Trim(FCConvert.ToString(clsre.Get_Fields_String("Address4")));
					if (Strings.Trim(str3) == string.Empty)
					{
						str3 = str4;
						str4 = "";
					}
					if (Strings.Trim(str2) == string.Empty)
					{
						str2 = str3;
						str3 = str4;
						str4 = "";
					}
					if (Strings.Trim(str1) == string.Empty)
					{
						str1 = str2;
						str2 = str3;
						str3 = str4;
						str4 = "";
					}
				}
				if (str1 != string.Empty)
				{
					txtReturnAddress1.Text = str1;
					txtReturnAddress2.Text = str2;
					txtReturnAddress3.Text = str3;
					txtReturnAddress4.Text = str4;
				}
				else
				{
					lblReturnTo.Visible = false;
				}
				lnTotal.Visible = true;
				lblTotalDue.Text = "Total Due as of " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
				txtTotalDue.Visible = true;
				txtTotalDueTotal.Visible = true;
				if (boolNameOrder)
				{
					strOrder = " Order By MortgageHolders.Name,DeedName1";
					// Master.OwnerName "   'kgk InsertName
				}
				else
				{
					strOrder = " Order By MortgageAssociation.MortgageHolderID, DeedName1 ";
					// Master.OwnerName "
				}
				strDBName = clsre.Get_GetFullDBName("CentralData") + ".dbo.";
				clsre.OpenRecordset("SELECT * from Master INNER JOIN (" + strDBName + "MortgageHolders INNER JOIN " + strDBName + "MortgageAssociation ON (MortgageAssociation.MortgageHolderID = MortgageHolders.ID)) ON (MortgageAssociation.Account = Master.AccountNumber)  WHERE MortgageAssociation.module = 'UT' AND NOT (Master.Deleted = 1) " + strWhere + strOrder);
				// , "twre0000.vb1")
				if (clsre.EndOfFile())
				{
					FCMessageBox.Show("No records found.", MsgBoxStyle.Information, "No Match");
					Cancel();
					return;
				}
				frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "MortgageHolder", false, false);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In Init", MsgBoxStyle.Critical, "Error");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			txtDate.Text = DateTime.Today.ToString();
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strAddress1 = "";
				string strAddress2 = "";
				string strAddress3 = "";
				string strAddress4 = "";
				double dblTax/*unused?*/;
				double dblOS/*unused?*/;
				double dblTotalDue = 0;
				txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
				if (!clsre.EndOfFile())
				{
					// set the group binder
					this.Fields["TheBinder"].Value = clsre.Get_Fields_Int32("MortgageHolderID");
					// "MortAssoc.MortgageHolderID")
					//FC:FINAL:MSH - can't implicitly convert from int to string
					// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					txtaccount.Text = FCConvert.ToString(clsre.Get_Fields("AccountNumber"));
					// TODO: Field [Own1FullName] not found!! (maybe it is an alias?)
					txtName.Text = clsre.Get_Fields("DeedName1");
					// trouts-226 kjr 04.13.2017
					txtLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsre.Get_Fields_Int32("StreetNumber")) + " " + FCConvert.ToString(clsre.Get_Fields_String("Apt"))) + " " + FCConvert.ToString(clsre.Get_Fields_String("StreetName")));
					// Get Water Total
					dblTotalDue = modUTCalculations.CalculateAccountUTTotal(FCConvert.ToInt32(clsre.Get_Fields_Int32("ID")), true, true, false, true);
					// trouts-226 kjr 04.13.2017
					// Get Sewer Total
					dblTotalDue += modUTCalculations.CalculateAccountUTTotal(FCConvert.ToInt32(clsre.Get_Fields_Int32("ID")), false, true, false, true);
					// trouts-226 kjr 04.13.2017
					txtTotalDue.Text = Strings.Format(dblTotalDue, "0.00");
					dblTotalDueSum += dblTotalDue;
					clsre.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Detail Format");
			}
		}

		private void GroupFooter1_Format(object sender, System.EventArgs e)
		{
			txtTotalDueTotal.Text = Strings.Format(dblTotalDueSum, "#,##0.00");
			dblTotalDueSum = 0;
		}

		private void GroupHeader1_Format(object sender, System.EventArgs e)
		{
			string strAddress1;
			string strAddress2;
			// VBto upgrade warning: strAddress3 As object	OnWrite(object, string)
			string strAddress3;
			// VBto upgrade warning: strAddress4 As Variant --> As string
			string strAddress4;
			clsMortgage.OpenRecordset("select * from MortgageHolders where ID = " + this.Fields["TheBinder"].Value, "CentralData");
			// trouts-226 kjr 04.13.2017 "twut0000.vb1")
			if (clsMortgage.EndOfFile())
				return;
			txtNumber.Text = this.Fields["thebinder"].Value.ToString();
			txtMortgageName.Text = clsMortgage.Get_Fields_String("name");
			strAddress1 = clsMortgage.Get_Fields_String("address1");
			strAddress2 = clsMortgage.Get_Fields_String("address2");
			// straddress3 = Trim(Trim(clsMortgage.Fields("streetnumber") & " " & clsMortgage.Fields("apt")) & " " & clsMortgage.Fields("streetname"))
			strAddress3 = clsMortgage.Get_Fields_String("address3");
			if (FCConvert.ToString(strAddress3) == "0")
				strAddress3 = "";
			strAddress4 = Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("city")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("state")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("zip")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("zip4")));
			// now condense this info
			if (FCConvert.ToString(strAddress3) == string.Empty)
			{
				strAddress3 = strAddress4;
				strAddress4 = "";
			}
			if (FCConvert.ToString(strAddress2) == string.Empty)
			{
				strAddress2 = strAddress3;
				strAddress3 = strAddress4;
				strAddress4 = "";
			}
			if (FCConvert.ToString(strAddress1) == string.Empty)
			{
				strAddress1 = strAddress2;
				strAddress2 = strAddress3;
				strAddress3 = strAddress4;
				strAddress4 = "";
			}
			txtAddress1.Text = strAddress1;
			txtAddress2.Text = strAddress2;
			txtAddress3.Text = strAddress3;
			txtAddress4.Text = strAddress4;
		}
		
		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		
	}
}
