namespace Global
{
    /// <summary>
    /// Summary description for arUTInformationScreen.
    /// </summary>
    partial class arUTInformationScreen
    {
        #region ActiveReport Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arUTInformationScreen));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldOut11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldOut12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut110 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut210 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut111 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut211 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut112 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut212 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut41 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut42 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut43 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut44 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut45 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut36 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut46 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut37 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut47 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut38 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut48 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut39 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut49 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut310 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut410 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut311 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut411 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut312 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut412 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut113 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut213 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut114 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut214 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut115 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut215 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut116 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut216 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut117 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut217 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut118 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut218 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut119 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut219 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut120 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut220 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut313 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut413 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut314 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut414 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut315 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut415 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut316 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut416 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut317 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut417 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut318 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut418 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut319 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut419 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut320 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut420 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut121 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut221 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut321 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut421 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOut40 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldOut11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut110)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut210)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut111)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut211)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut112)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut212)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut41)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut42)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut43)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut44)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut45)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut36)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut46)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut37)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut47)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut38)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut48)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut39)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut49)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut310)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut410)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut311)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut411)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut312)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut412)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut113)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut213)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut114)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut214)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut115)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut215)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut116)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut216)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut117)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut217)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut118)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut218)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut119)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut219)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut120)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut220)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut313)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut413)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut314)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut414)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut315)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut415)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut316)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut416)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut317)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut417)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut318)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut418)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut319)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut419)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut320)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut420)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut121)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut221)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut321)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut421)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut40)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldOut11,
            this.fldOut21,
            this.lblName,
            this.lblLocation,
            this.fldOut12,
            this.fldOut22,
            this.fldOut13,
            this.fldOut23,
            this.fldOut14,
            this.fldOut24,
            this.fldOut15,
            this.fldOut25,
            this.fldOut16,
            this.fldOut26,
            this.fldOut17,
            this.fldOut27,
            this.fldOut18,
            this.fldOut28,
            this.fldOut19,
            this.fldOut29,
            this.fldOut110,
            this.fldOut210,
            this.fldOut111,
            this.fldOut211,
            this.fldOut112,
            this.fldOut212,
            this.fldOut31,
            this.fldOut41,
            this.fldOut32,
            this.fldOut42,
            this.fldOut33,
            this.fldOut43,
            this.fldOut34,
            this.fldOut44,
            this.fldOut35,
            this.fldOut45,
            this.fldOut36,
            this.fldOut46,
            this.fldOut37,
            this.fldOut47,
            this.fldOut38,
            this.fldOut48,
            this.fldOut39,
            this.fldOut49,
            this.fldOut310,
            this.fldOut410,
            this.fldOut311,
            this.fldOut411,
            this.fldOut312,
            this.fldOut412,
            this.fldOut113,
            this.fldOut213,
            this.fldOut114,
            this.fldOut214,
            this.fldOut115,
            this.fldOut215,
            this.fldOut116,
            this.fldOut216,
            this.fldOut117,
            this.fldOut217,
            this.fldOut118,
            this.fldOut218,
            this.fldOut119,
            this.fldOut219,
            this.fldOut120,
            this.fldOut220,
            this.fldOut313,
            this.fldOut413,
            this.fldOut314,
            this.fldOut414,
            this.fldOut315,
            this.fldOut415,
            this.fldOut316,
            this.fldOut416,
            this.fldOut317,
            this.fldOut417,
            this.fldOut318,
            this.fldOut418,
            this.fldOut319,
            this.fldOut419,
            this.fldOut320,
            this.fldOut420,
            this.fldOut121,
            this.fldOut221,
            this.fldOut321,
            this.fldOut421,
            this.fldOut10,
            this.fldOut20,
            this.fldOut30,
            this.fldOut40});
			this.Detail.Height = 4.8125F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldOut11
			// 
			this.fldOut11.Height = 0.1875F;
			this.fldOut11.Left = 0F;
			this.fldOut11.Name = "fldOut11";
			this.fldOut11.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut11.Text = null;
			this.fldOut11.Top = 0.75F;
			this.fldOut11.Width = 1.625F;
			// 
			// fldOut21
			// 
			this.fldOut21.Height = 0.1875F;
			this.fldOut21.Left = 1.625F;
			this.fldOut21.Name = "fldOut21";
			this.fldOut21.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut21.Text = null;
			this.fldOut21.Top = 0.75F;
			this.fldOut21.Width = 1.875F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblName.Text = null;
			this.lblName.Top = 0.0625F;
			this.lblName.Width = 7.25F;
			// 
			// lblLocation
			// 
			this.lblLocation.Height = 0.1875F;
			this.lblLocation.HyperLink = null;
			this.lblLocation.Left = 0F;
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblLocation.Text = null;
			this.lblLocation.Top = 0.25F;
			this.lblLocation.Width = 7.25F;
			// 
			// fldOut12
			// 
			this.fldOut12.Height = 0.1875F;
			this.fldOut12.Left = 0F;
			this.fldOut12.Name = "fldOut12";
			this.fldOut12.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut12.Text = null;
			this.fldOut12.Top = 0.9375F;
			this.fldOut12.Width = 1.625F;
			// 
			// fldOut22
			// 
			this.fldOut22.Height = 0.1875F;
			this.fldOut22.Left = 1.625F;
			this.fldOut22.Name = "fldOut22";
			this.fldOut22.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut22.Text = null;
			this.fldOut22.Top = 0.9375F;
			this.fldOut22.Width = 1.875F;
			// 
			// fldOut13
			// 
			this.fldOut13.Height = 0.1875F;
			this.fldOut13.Left = 0F;
			this.fldOut13.Name = "fldOut13";
			this.fldOut13.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut13.Text = null;
			this.fldOut13.Top = 1.125F;
			this.fldOut13.Width = 1.625F;
			// 
			// fldOut23
			// 
			this.fldOut23.Height = 0.1875F;
			this.fldOut23.Left = 1.625F;
			this.fldOut23.Name = "fldOut23";
			this.fldOut23.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut23.Text = null;
			this.fldOut23.Top = 1.125F;
			this.fldOut23.Width = 1.875F;
			// 
			// fldOut14
			// 
			this.fldOut14.Height = 0.1875F;
			this.fldOut14.Left = 0F;
			this.fldOut14.Name = "fldOut14";
			this.fldOut14.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut14.Text = null;
			this.fldOut14.Top = 1.3125F;
			this.fldOut14.Width = 1.625F;
			// 
			// fldOut24
			// 
			this.fldOut24.Height = 0.1875F;
			this.fldOut24.Left = 1.625F;
			this.fldOut24.Name = "fldOut24";
			this.fldOut24.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut24.Text = null;
			this.fldOut24.Top = 1.3125F;
			this.fldOut24.Width = 1.875F;
			// 
			// fldOut15
			// 
			this.fldOut15.Height = 0.1875F;
			this.fldOut15.Left = 0F;
			this.fldOut15.Name = "fldOut15";
			this.fldOut15.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut15.Text = null;
			this.fldOut15.Top = 1.5F;
			this.fldOut15.Width = 1.625F;
			// 
			// fldOut25
			// 
			this.fldOut25.Height = 0.1875F;
			this.fldOut25.Left = 1.625F;
			this.fldOut25.Name = "fldOut25";
			this.fldOut25.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut25.Text = null;
			this.fldOut25.Top = 1.5F;
			this.fldOut25.Width = 1.875F;
			// 
			// fldOut16
			// 
			this.fldOut16.Height = 0.1875F;
			this.fldOut16.Left = 0F;
			this.fldOut16.Name = "fldOut16";
			this.fldOut16.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut16.Text = null;
			this.fldOut16.Top = 1.6875F;
			this.fldOut16.Width = 1.625F;
			// 
			// fldOut26
			// 
			this.fldOut26.Height = 0.1875F;
			this.fldOut26.Left = 1.625F;
			this.fldOut26.Name = "fldOut26";
			this.fldOut26.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut26.Text = null;
			this.fldOut26.Top = 1.6875F;
			this.fldOut26.Width = 1.875F;
			// 
			// fldOut17
			// 
			this.fldOut17.Height = 0.1875F;
			this.fldOut17.Left = 0F;
			this.fldOut17.Name = "fldOut17";
			this.fldOut17.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut17.Text = null;
			this.fldOut17.Top = 1.875F;
			this.fldOut17.Width = 1.625F;
			// 
			// fldOut27
			// 
			this.fldOut27.Height = 0.1875F;
			this.fldOut27.Left = 1.625F;
			this.fldOut27.Name = "fldOut27";
			this.fldOut27.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut27.Text = null;
			this.fldOut27.Top = 1.875F;
			this.fldOut27.Width = 1.875F;
			// 
			// fldOut18
			// 
			this.fldOut18.Height = 0.1875F;
			this.fldOut18.Left = 0F;
			this.fldOut18.Name = "fldOut18";
			this.fldOut18.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut18.Text = null;
			this.fldOut18.Top = 2.0625F;
			this.fldOut18.Width = 1.625F;
			// 
			// fldOut28
			// 
			this.fldOut28.Height = 0.1875F;
			this.fldOut28.Left = 1.625F;
			this.fldOut28.Name = "fldOut28";
			this.fldOut28.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut28.Text = null;
			this.fldOut28.Top = 2.0625F;
			this.fldOut28.Width = 1.875F;
			// 
			// fldOut19
			// 
			this.fldOut19.Height = 0.1875F;
			this.fldOut19.Left = 0F;
			this.fldOut19.Name = "fldOut19";
			this.fldOut19.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut19.Text = null;
			this.fldOut19.Top = 2.25F;
			this.fldOut19.Width = 1.625F;
			// 
			// fldOut29
			// 
			this.fldOut29.Height = 0.1875F;
			this.fldOut29.Left = 1.625F;
			this.fldOut29.Name = "fldOut29";
			this.fldOut29.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut29.Text = null;
			this.fldOut29.Top = 2.25F;
			this.fldOut29.Width = 1.875F;
			// 
			// fldOut110
			// 
			this.fldOut110.Height = 0.1875F;
			this.fldOut110.Left = 0F;
			this.fldOut110.Name = "fldOut110";
			this.fldOut110.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut110.Text = null;
			this.fldOut110.Top = 2.4375F;
			this.fldOut110.Width = 1.625F;
			// 
			// fldOut210
			// 
			this.fldOut210.Height = 0.1875F;
			this.fldOut210.Left = 1.625F;
			this.fldOut210.Name = "fldOut210";
			this.fldOut210.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut210.Text = null;
			this.fldOut210.Top = 2.4375F;
			this.fldOut210.Width = 1.875F;
			// 
			// fldOut111
			// 
			this.fldOut111.Height = 0.1875F;
			this.fldOut111.Left = 0F;
			this.fldOut111.Name = "fldOut111";
			this.fldOut111.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut111.Text = null;
			this.fldOut111.Top = 2.625F;
			this.fldOut111.Width = 1.625F;
			// 
			// fldOut211
			// 
			this.fldOut211.Height = 0.1875F;
			this.fldOut211.Left = 1.625F;
			this.fldOut211.Name = "fldOut211";
			this.fldOut211.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut211.Text = null;
			this.fldOut211.Top = 2.625F;
			this.fldOut211.Width = 1.875F;
			// 
			// fldOut112
			// 
			this.fldOut112.Height = 0.1875F;
			this.fldOut112.Left = 0F;
			this.fldOut112.Name = "fldOut112";
			this.fldOut112.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut112.Text = null;
			this.fldOut112.Top = 2.8125F;
			this.fldOut112.Width = 1.625F;
			// 
			// fldOut212
			// 
			this.fldOut212.Height = 0.1875F;
			this.fldOut212.Left = 1.625F;
			this.fldOut212.Name = "fldOut212";
			this.fldOut212.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut212.Text = null;
			this.fldOut212.Top = 2.8125F;
			this.fldOut212.Width = 1.875F;
			// 
			// fldOut31
			// 
			this.fldOut31.Height = 0.1875F;
			this.fldOut31.Left = 3.8125F;
			this.fldOut31.Name = "fldOut31";
			this.fldOut31.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut31.Text = null;
			this.fldOut31.Top = 0.75F;
			this.fldOut31.Width = 1.625F;
			// 
			// fldOut41
			// 
			this.fldOut41.Height = 0.1875F;
			this.fldOut41.Left = 5.4375F;
			this.fldOut41.Name = "fldOut41";
			this.fldOut41.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut41.Text = null;
			this.fldOut41.Top = 0.75F;
			this.fldOut41.Width = 1.875F;
			// 
			// fldOut32
			// 
			this.fldOut32.Height = 0.1875F;
			this.fldOut32.Left = 3.8125F;
			this.fldOut32.Name = "fldOut32";
			this.fldOut32.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut32.Text = null;
			this.fldOut32.Top = 0.9375F;
			this.fldOut32.Width = 1.625F;
			// 
			// fldOut42
			// 
			this.fldOut42.Height = 0.1875F;
			this.fldOut42.Left = 5.4375F;
			this.fldOut42.Name = "fldOut42";
			this.fldOut42.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut42.Text = null;
			this.fldOut42.Top = 0.9375F;
			this.fldOut42.Width = 1.875F;
			// 
			// fldOut33
			// 
			this.fldOut33.Height = 0.1875F;
			this.fldOut33.Left = 3.8125F;
			this.fldOut33.Name = "fldOut33";
			this.fldOut33.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut33.Text = null;
			this.fldOut33.Top = 1.125F;
			this.fldOut33.Width = 1.625F;
			// 
			// fldOut43
			// 
			this.fldOut43.Height = 0.1875F;
			this.fldOut43.Left = 5.4375F;
			this.fldOut43.Name = "fldOut43";
			this.fldOut43.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut43.Text = null;
			this.fldOut43.Top = 1.125F;
			this.fldOut43.Width = 1.875F;
			// 
			// fldOut34
			// 
			this.fldOut34.Height = 0.1875F;
			this.fldOut34.Left = 3.8125F;
			this.fldOut34.Name = "fldOut34";
			this.fldOut34.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut34.Text = null;
			this.fldOut34.Top = 1.3125F;
			this.fldOut34.Width = 1.625F;
			// 
			// fldOut44
			// 
			this.fldOut44.Height = 0.1875F;
			this.fldOut44.Left = 5.4375F;
			this.fldOut44.Name = "fldOut44";
			this.fldOut44.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut44.Text = null;
			this.fldOut44.Top = 1.3125F;
			this.fldOut44.Width = 1.875F;
			// 
			// fldOut35
			// 
			this.fldOut35.Height = 0.1875F;
			this.fldOut35.Left = 3.8125F;
			this.fldOut35.Name = "fldOut35";
			this.fldOut35.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut35.Text = null;
			this.fldOut35.Top = 1.5F;
			this.fldOut35.Width = 1.625F;
			// 
			// fldOut45
			// 
			this.fldOut45.Height = 0.1875F;
			this.fldOut45.Left = 5.4375F;
			this.fldOut45.Name = "fldOut45";
			this.fldOut45.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut45.Text = null;
			this.fldOut45.Top = 1.5F;
			this.fldOut45.Width = 1.875F;
			// 
			// fldOut36
			// 
			this.fldOut36.Height = 0.1875F;
			this.fldOut36.Left = 3.8125F;
			this.fldOut36.Name = "fldOut36";
			this.fldOut36.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut36.Text = null;
			this.fldOut36.Top = 1.6875F;
			this.fldOut36.Width = 1.625F;
			// 
			// fldOut46
			// 
			this.fldOut46.Height = 0.1875F;
			this.fldOut46.Left = 5.4375F;
			this.fldOut46.Name = "fldOut46";
			this.fldOut46.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut46.Text = null;
			this.fldOut46.Top = 1.6875F;
			this.fldOut46.Width = 1.875F;
			// 
			// fldOut37
			// 
			this.fldOut37.Height = 0.1875F;
			this.fldOut37.Left = 3.8125F;
			this.fldOut37.Name = "fldOut37";
			this.fldOut37.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut37.Text = null;
			this.fldOut37.Top = 1.875F;
			this.fldOut37.Width = 1.625F;
			// 
			// fldOut47
			// 
			this.fldOut47.Height = 0.1875F;
			this.fldOut47.Left = 5.4375F;
			this.fldOut47.Name = "fldOut47";
			this.fldOut47.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut47.Text = null;
			this.fldOut47.Top = 1.875F;
			this.fldOut47.Width = 1.875F;
			// 
			// fldOut38
			// 
			this.fldOut38.Height = 0.1875F;
			this.fldOut38.Left = 3.8125F;
			this.fldOut38.Name = "fldOut38";
			this.fldOut38.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut38.Text = null;
			this.fldOut38.Top = 2.0625F;
			this.fldOut38.Width = 1.625F;
			// 
			// fldOut48
			// 
			this.fldOut48.Height = 0.1875F;
			this.fldOut48.Left = 5.4375F;
			this.fldOut48.Name = "fldOut48";
			this.fldOut48.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut48.Text = null;
			this.fldOut48.Top = 2.0625F;
			this.fldOut48.Width = 1.875F;
			// 
			// fldOut39
			// 
			this.fldOut39.Height = 0.1875F;
			this.fldOut39.Left = 3.8125F;
			this.fldOut39.Name = "fldOut39";
			this.fldOut39.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut39.Text = null;
			this.fldOut39.Top = 2.25F;
			this.fldOut39.Width = 1.625F;
			// 
			// fldOut49
			// 
			this.fldOut49.Height = 0.1875F;
			this.fldOut49.Left = 5.4375F;
			this.fldOut49.Name = "fldOut49";
			this.fldOut49.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut49.Text = null;
			this.fldOut49.Top = 2.25F;
			this.fldOut49.Width = 1.875F;
			// 
			// fldOut310
			// 
			this.fldOut310.Height = 0.1875F;
			this.fldOut310.Left = 3.8125F;
			this.fldOut310.Name = "fldOut310";
			this.fldOut310.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut310.Text = null;
			this.fldOut310.Top = 2.4375F;
			this.fldOut310.Width = 1.625F;
			// 
			// fldOut410
			// 
			this.fldOut410.Height = 0.1875F;
			this.fldOut410.Left = 5.4375F;
			this.fldOut410.Name = "fldOut410";
			this.fldOut410.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut410.Text = null;
			this.fldOut410.Top = 2.4375F;
			this.fldOut410.Width = 1.875F;
			// 
			// fldOut311
			// 
			this.fldOut311.Height = 0.1875F;
			this.fldOut311.Left = 3.8125F;
			this.fldOut311.Name = "fldOut311";
			this.fldOut311.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut311.Text = null;
			this.fldOut311.Top = 2.625F;
			this.fldOut311.Width = 1.625F;
			// 
			// fldOut411
			// 
			this.fldOut411.Height = 0.1875F;
			this.fldOut411.Left = 5.4375F;
			this.fldOut411.Name = "fldOut411";
			this.fldOut411.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut411.Text = null;
			this.fldOut411.Top = 2.625F;
			this.fldOut411.Width = 1.875F;
			// 
			// fldOut312
			// 
			this.fldOut312.Height = 0.1875F;
			this.fldOut312.Left = 3.8125F;
			this.fldOut312.Name = "fldOut312";
			this.fldOut312.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut312.Text = null;
			this.fldOut312.Top = 2.8125F;
			this.fldOut312.Width = 1.625F;
			// 
			// fldOut412
			// 
			this.fldOut412.Height = 0.1875F;
			this.fldOut412.Left = 5.4375F;
			this.fldOut412.Name = "fldOut412";
			this.fldOut412.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut412.Text = null;
			this.fldOut412.Top = 2.8125F;
			this.fldOut412.Width = 1.875F;
			// 
			// fldOut113
			// 
			this.fldOut113.Height = 0.1875F;
			this.fldOut113.Left = 0F;
			this.fldOut113.Name = "fldOut113";
			this.fldOut113.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut113.Text = null;
			this.fldOut113.Top = 3F;
			this.fldOut113.Width = 1.625F;
			// 
			// fldOut213
			// 
			this.fldOut213.Height = 0.1875F;
			this.fldOut213.Left = 1.625F;
			this.fldOut213.Name = "fldOut213";
			this.fldOut213.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut213.Text = null;
			this.fldOut213.Top = 3F;
			this.fldOut213.Width = 1.875F;
			// 
			// fldOut114
			// 
			this.fldOut114.Height = 0.1875F;
			this.fldOut114.Left = 0F;
			this.fldOut114.Name = "fldOut114";
			this.fldOut114.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut114.Text = null;
			this.fldOut114.Top = 3.1875F;
			this.fldOut114.Width = 1.625F;
			// 
			// fldOut214
			// 
			this.fldOut214.Height = 0.1875F;
			this.fldOut214.Left = 1.625F;
			this.fldOut214.Name = "fldOut214";
			this.fldOut214.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut214.Text = null;
			this.fldOut214.Top = 3.1875F;
			this.fldOut214.Width = 1.875F;
			// 
			// fldOut115
			// 
			this.fldOut115.Height = 0.1875F;
			this.fldOut115.Left = 0F;
			this.fldOut115.Name = "fldOut115";
			this.fldOut115.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut115.Text = null;
			this.fldOut115.Top = 3.375F;
			this.fldOut115.Width = 1.625F;
			// 
			// fldOut215
			// 
			this.fldOut215.Height = 0.1875F;
			this.fldOut215.Left = 1.625F;
			this.fldOut215.Name = "fldOut215";
			this.fldOut215.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut215.Text = null;
			this.fldOut215.Top = 3.375F;
			this.fldOut215.Width = 1.875F;
			// 
			// fldOut116
			// 
			this.fldOut116.Height = 0.1875F;
			this.fldOut116.Left = 0F;
			this.fldOut116.Name = "fldOut116";
			this.fldOut116.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut116.Text = null;
			this.fldOut116.Top = 3.5625F;
			this.fldOut116.Width = 1.625F;
			// 
			// fldOut216
			// 
			this.fldOut216.Height = 0.1875F;
			this.fldOut216.Left = 1.625F;
			this.fldOut216.Name = "fldOut216";
			this.fldOut216.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut216.Text = null;
			this.fldOut216.Top = 3.5625F;
			this.fldOut216.Width = 1.875F;
			// 
			// fldOut117
			// 
			this.fldOut117.Height = 0.1875F;
			this.fldOut117.Left = 0F;
			this.fldOut117.Name = "fldOut117";
			this.fldOut117.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut117.Text = null;
			this.fldOut117.Top = 3.75F;
			this.fldOut117.Width = 1.625F;
			// 
			// fldOut217
			// 
			this.fldOut217.Height = 0.1875F;
			this.fldOut217.Left = 1.625F;
			this.fldOut217.Name = "fldOut217";
			this.fldOut217.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut217.Text = null;
			this.fldOut217.Top = 3.75F;
			this.fldOut217.Width = 1.875F;
			// 
			// fldOut118
			// 
			this.fldOut118.Height = 0.1875F;
			this.fldOut118.Left = 0F;
			this.fldOut118.Name = "fldOut118";
			this.fldOut118.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut118.Text = null;
			this.fldOut118.Top = 3.9375F;
			this.fldOut118.Width = 1.625F;
			// 
			// fldOut218
			// 
			this.fldOut218.Height = 0.1875F;
			this.fldOut218.Left = 1.625F;
			this.fldOut218.Name = "fldOut218";
			this.fldOut218.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut218.Text = null;
			this.fldOut218.Top = 3.9375F;
			this.fldOut218.Width = 1.875F;
			// 
			// fldOut119
			// 
			this.fldOut119.Height = 0.1875F;
			this.fldOut119.Left = 0F;
			this.fldOut119.Name = "fldOut119";
			this.fldOut119.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut119.Text = null;
			this.fldOut119.Top = 4.125F;
			this.fldOut119.Width = 1.625F;
			// 
			// fldOut219
			// 
			this.fldOut219.Height = 0.1875F;
			this.fldOut219.Left = 1.625F;
			this.fldOut219.Name = "fldOut219";
			this.fldOut219.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut219.Text = null;
			this.fldOut219.Top = 4.125F;
			this.fldOut219.Width = 1.875F;
			// 
			// fldOut120
			// 
			this.fldOut120.Height = 0.1875F;
			this.fldOut120.Left = 0F;
			this.fldOut120.Name = "fldOut120";
			this.fldOut120.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut120.Text = null;
			this.fldOut120.Top = 4.3125F;
			this.fldOut120.Width = 1.625F;
			// 
			// fldOut220
			// 
			this.fldOut220.Height = 0.1875F;
			this.fldOut220.Left = 1.625F;
			this.fldOut220.Name = "fldOut220";
			this.fldOut220.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut220.Text = null;
			this.fldOut220.Top = 4.3125F;
			this.fldOut220.Width = 1.875F;
			// 
			// fldOut313
			// 
			this.fldOut313.Height = 0.1875F;
			this.fldOut313.Left = 3.8125F;
			this.fldOut313.Name = "fldOut313";
			this.fldOut313.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut313.Text = null;
			this.fldOut313.Top = 3F;
			this.fldOut313.Width = 1.625F;
			// 
			// fldOut413
			// 
			this.fldOut413.Height = 0.1875F;
			this.fldOut413.Left = 5.4375F;
			this.fldOut413.Name = "fldOut413";
			this.fldOut413.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut413.Text = null;
			this.fldOut413.Top = 3F;
			this.fldOut413.Width = 1.875F;
			// 
			// fldOut314
			// 
			this.fldOut314.Height = 0.1875F;
			this.fldOut314.Left = 3.8125F;
			this.fldOut314.Name = "fldOut314";
			this.fldOut314.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut314.Text = null;
			this.fldOut314.Top = 3.1875F;
			this.fldOut314.Width = 1.625F;
			// 
			// fldOut414
			// 
			this.fldOut414.Height = 0.1875F;
			this.fldOut414.Left = 5.4375F;
			this.fldOut414.Name = "fldOut414";
			this.fldOut414.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut414.Text = null;
			this.fldOut414.Top = 3.1875F;
			this.fldOut414.Width = 1.875F;
			// 
			// fldOut315
			// 
			this.fldOut315.Height = 0.1875F;
			this.fldOut315.Left = 3.8125F;
			this.fldOut315.Name = "fldOut315";
			this.fldOut315.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut315.Text = null;
			this.fldOut315.Top = 3.375F;
			this.fldOut315.Width = 1.625F;
			// 
			// fldOut415
			// 
			this.fldOut415.Height = 0.1875F;
			this.fldOut415.Left = 5.4375F;
			this.fldOut415.Name = "fldOut415";
			this.fldOut415.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut415.Text = null;
			this.fldOut415.Top = 3.375F;
			this.fldOut415.Width = 1.875F;
			// 
			// fldOut316
			// 
			this.fldOut316.Height = 0.1875F;
			this.fldOut316.Left = 3.8125F;
			this.fldOut316.Name = "fldOut316";
			this.fldOut316.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut316.Text = null;
			this.fldOut316.Top = 3.5625F;
			this.fldOut316.Width = 1.625F;
			// 
			// fldOut416
			// 
			this.fldOut416.Height = 0.1875F;
			this.fldOut416.Left = 5.4375F;
			this.fldOut416.Name = "fldOut416";
			this.fldOut416.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut416.Text = null;
			this.fldOut416.Top = 3.5625F;
			this.fldOut416.Width = 1.875F;
			// 
			// fldOut317
			// 
			this.fldOut317.Height = 0.1875F;
			this.fldOut317.Left = 3.8125F;
			this.fldOut317.Name = "fldOut317";
			this.fldOut317.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut317.Text = null;
			this.fldOut317.Top = 3.75F;
			this.fldOut317.Width = 1.625F;
			// 
			// fldOut417
			// 
			this.fldOut417.Height = 0.1875F;
			this.fldOut417.Left = 5.4375F;
			this.fldOut417.Name = "fldOut417";
			this.fldOut417.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut417.Text = null;
			this.fldOut417.Top = 3.75F;
			this.fldOut417.Width = 1.875F;
			// 
			// fldOut318
			// 
			this.fldOut318.Height = 0.1875F;
			this.fldOut318.Left = 3.8125F;
			this.fldOut318.Name = "fldOut318";
			this.fldOut318.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut318.Text = null;
			this.fldOut318.Top = 3.9375F;
			this.fldOut318.Width = 1.625F;
			// 
			// fldOut418
			// 
			this.fldOut418.Height = 0.1875F;
			this.fldOut418.Left = 5.4375F;
			this.fldOut418.Name = "fldOut418";
			this.fldOut418.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut418.Text = null;
			this.fldOut418.Top = 3.9375F;
			this.fldOut418.Width = 1.875F;
			// 
			// fldOut319
			// 
			this.fldOut319.Height = 0.1875F;
			this.fldOut319.Left = 3.8125F;
			this.fldOut319.Name = "fldOut319";
			this.fldOut319.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut319.Text = null;
			this.fldOut319.Top = 4.125F;
			this.fldOut319.Width = 1.625F;
			// 
			// fldOut419
			// 
			this.fldOut419.Height = 0.1875F;
			this.fldOut419.Left = 5.4375F;
			this.fldOut419.Name = "fldOut419";
			this.fldOut419.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut419.Text = null;
			this.fldOut419.Top = 4.125F;
			this.fldOut419.Width = 1.875F;
			// 
			// fldOut320
			// 
			this.fldOut320.Height = 0.1875F;
			this.fldOut320.Left = 3.8125F;
			this.fldOut320.Name = "fldOut320";
			this.fldOut320.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut320.Text = null;
			this.fldOut320.Top = 4.3125F;
			this.fldOut320.Width = 1.625F;
			// 
			// fldOut420
			// 
			this.fldOut420.Height = 0.1875F;
			this.fldOut420.Left = 5.4375F;
			this.fldOut420.Name = "fldOut420";
			this.fldOut420.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut420.Text = null;
			this.fldOut420.Top = 4.3125F;
			this.fldOut420.Width = 1.875F;
			// 
			// fldOut121
			// 
			this.fldOut121.Height = 0.1875F;
			this.fldOut121.Left = 0F;
			this.fldOut121.Name = "fldOut121";
			this.fldOut121.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut121.Text = null;
			this.fldOut121.Top = 4.5F;
			this.fldOut121.Width = 1.625F;
			// 
			// fldOut221
			// 
			this.fldOut221.Height = 0.1875F;
			this.fldOut221.Left = 1.625F;
			this.fldOut221.Name = "fldOut221";
			this.fldOut221.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut221.Text = null;
			this.fldOut221.Top = 4.5F;
			this.fldOut221.Width = 1.875F;
			// 
			// fldOut321
			// 
			this.fldOut321.Height = 0.1875F;
			this.fldOut321.Left = 3.8125F;
			this.fldOut321.Name = "fldOut321";
			this.fldOut321.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut321.Text = null;
			this.fldOut321.Top = 4.5F;
			this.fldOut321.Width = 1.625F;
			// 
			// fldOut421
			// 
			this.fldOut421.Height = 0.1875F;
			this.fldOut421.Left = 5.4375F;
			this.fldOut421.Name = "fldOut421";
			this.fldOut421.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut421.Text = null;
			this.fldOut421.Top = 4.5F;
			this.fldOut421.Width = 1.875F;
			// 
			// fldOut10
			// 
			this.fldOut10.Height = 0.1875F;
			this.fldOut10.Left = 0F;
			this.fldOut10.Name = "fldOut10";
			this.fldOut10.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut10.Text = null;
			this.fldOut10.Top = 0.5625F;
			this.fldOut10.Width = 1.625F;
			// 
			// fldOut20
			// 
			this.fldOut20.Height = 0.1875F;
			this.fldOut20.Left = 1.625F;
			this.fldOut20.Name = "fldOut20";
			this.fldOut20.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut20.Text = null;
			this.fldOut20.Top = 0.5625F;
			this.fldOut20.Width = 1.875F;
			// 
			// fldOut30
			// 
			this.fldOut30.Height = 0.1875F;
			this.fldOut30.Left = 3.8125F;
			this.fldOut30.Name = "fldOut30";
			this.fldOut30.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldOut30.Text = null;
			this.fldOut30.Top = 0.5625F;
			this.fldOut30.Width = 1.625F;
			// 
			// fldOut40
			// 
			this.fldOut40.Height = 0.1875F;
			this.fldOut40.Left = 5.4375F;
			this.fldOut40.Name = "fldOut40";
			this.fldOut40.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldOut40.Text = null;
			this.fldOut40.Top = 0.5625F;
			this.fldOut40.Width = 1.875F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Height = 0F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblMuniName,
            this.lblTime,
            this.lblDate,
            this.lblPage,
            this.Line1});
			this.PageHeader.Height = 0.5F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.375F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
			this.lblHeader.Text = "Account Detail";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.3125F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.8125F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.25F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.0625F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.25F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.0625F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.25F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.625F;
			this.Line1.Width = 7F;
			this.Line1.X1 = 7F;
			this.Line1.X2 = 0F;
			this.Line1.Y1 = 1.625F;
			this.Line1.Y2 = 1.625F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// arUTInformationScreen
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.3125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldOut11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut110)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut210)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut111)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut211)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut112)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut212)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut41)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut42)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut43)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut44)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut45)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut36)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut46)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut37)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut47)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut38)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut48)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut39)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut49)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut310)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut410)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut311)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut411)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut312)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut412)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut113)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut213)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut114)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut214)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut115)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut215)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut116)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut216)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut117)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut217)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut118)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut218)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut119)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut219)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut120)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut220)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut313)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut413)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut314)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut414)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut315)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut415)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut316)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut416)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut317)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut417)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut318)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut418)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut319)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut419)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut320)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut420)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut121)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut221)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut321)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut421)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOut40)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion

        private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut11;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut21;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut12;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut22;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut13;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut23;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut14;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut24;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut15;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut25;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut16;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut26;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut17;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut27;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut18;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut28;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut19;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut29;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut110;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut210;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut111;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut211;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut112;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut212;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut31;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut41;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut32;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut42;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut33;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut43;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut34;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut44;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut35;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut45;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut36;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut46;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut37;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut47;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut38;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut48;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut39;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut49;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut310;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut410;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut311;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut411;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut312;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut412;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut113;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut213;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut114;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut214;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut115;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut215;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut116;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut216;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut117;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut217;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut118;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut218;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut119;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut219;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut120;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut220;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut313;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut413;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut314;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut414;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut315;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut415;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut316;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut416;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut317;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut417;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut318;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut418;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut319;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut419;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut320;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut420;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut121;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut221;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut321;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut421;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut10;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut20;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut30;
        private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOut40;
        private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
        private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
        private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
        private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
        private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
        private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
    }
}
