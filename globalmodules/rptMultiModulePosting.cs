﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GrapeCity.ActiveReports;
using TWSharedLibrary;

#if TWBD0000
using TWBD0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptMultiModulePosting : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/05/2006              *
		// ********************************************************
		public int PageCounter;
		public clsBudgetaryPosting PostInfo;
		public int intCurrentIndex;
		bool blnFirstRecord;

		public rptMultiModulePosting()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Posted Journals";
		}

		public static rptMultiModulePosting InstancePtr
		{
			get
			{
				return (rptMultiModulePosting)Sys.GetInstance(typeof(rptMultiModulePosting));
			}
		}

		protected rptMultiModulePosting _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (blnFirstRecord)
				{
					blnFirstRecord = false;
					eArgs.EOF = false;
				}
				else
				{
					intCurrentIndex += 1;
					if (intCurrentIndex >= PostInfo.JournalCount)
					{
						eArgs.EOF = true;
					}
					else
					{
						eArgs.EOF = false;
					}
					//Detail_Format();
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Fetch Data Error");
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (PostInfo.SavePostingReport)
				{
					if (PostInfo.SavedReportFileName != "")
					{
						this.Document.Save(PostInfo.SavedReportFileName + ".RDF");
					}
				}
				rptJournalSubReport = null;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Ending Report");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
				PageCounter = 0;
				blnFirstRecord = true;
				Label2.Text = modGlobalConstants.Statics.MuniName;
				Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
				if (PostInfo.PageBreakBetweenJournalsOnReport)
				{
					this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
				}
				else
				{
					this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Starting Report");
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			//clsDRWrapper rsInfo = new clsDRWrapper();
			int counter;
			if (PostInfo.WaitForReportToEnd)
			{
				for (counter = 0; counter <= PostInfo.JournalCount - 1; counter++)
				{
					modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(PostInfo.Get_JournalsToPost(counter).JournalNumber));
				}
			}
			else
			{
				frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Recalculating Account Information", true);
				frmWait.InstancePtr.Show();
				//this.Refresh();
				//troges126
				//FC:FINAL:BBE:#i744 - Call to ModBudgetaryAccounting.CalculateAccountInfo() is missing. It is called in the original.
				modBudgetaryAccounting.CalculateAccountInfo("", true);
				//modBudgetaryAccounting.CalculateAccountInfo(true, true, true, "E", "", true);
				//frmWait.InstancePtr.prgProgress.Value = 15;
				//frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Recalculating Expense Account Detail Information";
				//////////Application.DoEvents();
				////this.Refresh();
				//modBudgetaryAccounting.CalculateAccountInfo(false, true, true, "E", "", true);
				//frmWait.InstancePtr.prgProgress.Value = 30;
				//frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Recalculating Revenue Account Summary Information";
				//////////Application.DoEvents();
				////this.Refresh();
				//modBudgetaryAccounting.CalculateAccountInfo(true, true, true, "R", "", true);
				//frmWait.InstancePtr.prgProgress.Value = 45;
				//frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Recalculating Revenue Account Detail Information";
				//////////Application.DoEvents();
				////this.Refresh();
				//modBudgetaryAccounting.CalculateAccountInfo(false, true, true, "R", "", true);
				//frmWait.InstancePtr.prgProgress.Value = 60;
				//frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Recalculating Ledger Account Summary Information";
				//////////Application.DoEvents();
				////this.Refresh();
				//modBudgetaryAccounting.CalculateAccountInfo(true, true, true, "G", "", true);
				//frmWait.InstancePtr.prgProgress.Value = 75;
				//frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Recalculating Ledger Account Detail Information";
				//////////Application.DoEvents();
				////this.Refresh();
				//modBudgetaryAccounting.CalculateAccountInfo(false, true, true, "G", "", true);
				//frmWait.InstancePtr.prgProgress.Value = 90;
				//frmWait.InstancePtr.lblMessage.Text = "Please Wait...." + "\r\n" + "Finalizing Account Information";
				//////////Application.DoEvents();
				////this.Refresh();
				//rsInfo.OpenRecordset("SELECT * FROM CalculationNeeded", "TWBD0000.vb1");
				//if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				//{
				//	rsInfo.Edit();
				//	rsInfo.Set_Fields("ExpPendingSummary", false);
				//	rsInfo.Set_Fields("ExpPostedSummary", false);
				//	rsInfo.Set_Fields("RevPendingSummary", false);
				//	rsInfo.Set_Fields("RevPostedSummary", false);
				//	rsInfo.Set_Fields("LedgerPendingSummary", false);
				//	rsInfo.Set_Fields("LedgerPostedSummary", false);
				//	rsInfo.Set_Fields("ExpPendingDetail", false);
				//	rsInfo.Set_Fields("ExpPostedDetail", false);
				//	rsInfo.Set_Fields("RevPendingDetail", false);
				//	rsInfo.Set_Fields("RevPostedDetail", false);
				//	rsInfo.Set_Fields("LedgerPendingDetail", false);
				//	rsInfo.Set_Fields("LedgerPostedDetail", false);
				//	rsInfo.Update();
				//}
				//else
				//{
				//	rsInfo.AddNew();
				//	rsInfo.Set_Fields("ExpPendingSummary", false);
				//	rsInfo.Set_Fields("ExpPostedSummary", false);
				//	rsInfo.Set_Fields("RevPendingSummary", false);
				//	rsInfo.Set_Fields("RevPostedSummary", false);
				//	rsInfo.Set_Fields("LedgerPendingSummary", false);
				//	rsInfo.Set_Fields("LedgerPostedSummary", false);
				//	rsInfo.Set_Fields("ExpPendingDetail", false);
				//	rsInfo.Set_Fields("ExpPostedDetail", false);
				//	rsInfo.Set_Fields("RevPendingDetail", false);
				//	rsInfo.Set_Fields("RevPostedDetail", false);
				//	rsInfo.Set_Fields("LedgerPendingDetail", false);
				//	rsInfo.Set_Fields("LedgerPostedDetail", false);
				//	rsInfo.Update();
				//}
				frmWait.InstancePtr.Unload();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngErrorCode/*unused?*/;
                //FC:FINAL:IPI - #1047 - Detail_Format is not raised in VB6 when eArgs.EOF = true in FetchData
                if (intCurrentIndex >= PostInfo.JournalCount)
                {
                    return;
                }
                if (PostInfo.Get_JournalsToPost(intCurrentIndex).JournalType == "AP" || PostInfo.Get_JournalsToPost(intCurrentIndex).JournalType == "AC")
				{
					rptJournalSubReport.Report = rptMultiModuleAPJournals.InstancePtr;
				}
				else if (PostInfo.Get_JournalsToPost(intCurrentIndex).JournalType == "EN")
				{
					rptJournalSubReport.Report = rptMultiModuleEncJournals.InstancePtr;
				}
				else
				{
					rptJournalSubReport.Report = rptMultiModuleJournals.InstancePtr;
				}
				rptJournalSubReport.Report.UserData = intCurrentIndex;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Detail Format Error");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		public void Init(clsBudgetaryPosting clsPostingInfo, List<string> batchReports = null)
		{
			PostInfo = clsPostingInfo;
			if (clsPostingInfo.AllowPreview)
			{
				if (PostInfo.WaitForReportToEnd)
				{
					frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "", false, false);
				}
				else
				{
					frmReportViewer.InstancePtr.Init(this);
				}
			}
			else
			{
				modDuplexPrinting.DuplexPrintReport(this, "", true, batchReports: batchReports);
                this.Unload();
			}
		}

		private void rptMultiModulePosting_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMultiModulePosting.Caption	= "Posted Journals";
			//rptMultiModulePosting.Icon	= "rptMultiModulePosting.dsx":0000";
			//rptMultiModulePosting.Left	= 0;
			//rptMultiModulePosting.Top	= 0;
			//rptMultiModulePosting.Width	= 11880;
			//rptMultiModulePosting.Height	= 8595;
			//rptMultiModulePosting.StartUpPosition	= 3;
			//rptMultiModulePosting.SectionData	= "rptMultiModulePosting.dsx":058A;
			//End Unmaped Properties
		}
	}
}
