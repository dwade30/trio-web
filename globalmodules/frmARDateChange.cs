﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWAR0000
using TWAR0000;


#elif TWCR0000
using TWCR0000;

#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmARDateChange.
	/// </summary>
	public partial class frmARDateChange : BaseForm
	{
		public frmARDateChange()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmARDateChange InstancePtr
		{
			get
			{
				return (frmARDateChange)Sys.GetInstance(typeof(frmARDateChange));
			}
		}

		protected frmARDateChange _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>

		DateTime dtNew;

		public void Init(string strText, ref DateTime dtEffectiveDate)
		{
			lblInstructions.Text = strText;
			txtDate.Text = Strings.Format(dtEffectiveDate, "MM/dd/yyyy");
			this.Show(FormShowEnum.Modal, App.MainForm);
			if (txtDate.Visible && txtDate.Enabled)
			{
				txtDate.Focus();
			}
		}

		private void cmdOk_Click(object sender, System.EventArgs e)
		{
			DialogResult intAns;
			DateTime datLowDate;
			DateTime datHighDate;

			if (Information.IsDate(txtDate.Text))
			{
				datLowDate = DateAndTime.DateAdd("yyyy", -1, DateTime.Today);
				datHighDate = DateAndTime.DateAdd("yyyy", 1, DateTime.Today);
				if (DateAndTime.DateValue(txtDate.Text).ToOADate() < datLowDate.ToOADate() || DateAndTime.DateValue(txtDate.Text).ToOADate() > datHighDate.ToOADate())
				{
					intAns = MessageBox.Show("The date you are entering is different from today's date by more than a year.  Are you sure you wish to use this date?", "Change Date?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (intAns == DialogResult.No)
					{
						goto UnloadTag;
					}
				}
				dtNew = DateAndTime.DateValue(txtDate.Text);
				this.Hide();
				frmARCLStatus.InstancePtr.GetNewEffectiveInterestDate(ref dtNew);
			}
			UnloadTag:
			;
			Close();
		}

		private void frmARDateChange_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
				return;
		}

		private void frmARDateChange_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						Support.SendKeys("{tab}", false);
						break;
					}
			}
			//end switch
		}

		private void frmARDateChange_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
				Close();
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmARDateChange_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void txtDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(Strings.Left(txtDate.Text, 2)) > 12)
			{
				e.Cancel = true;
				MessageBox.Show("Please use the date format MM/dd/yyyy.", "Invalid Date Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}
	}
}
