﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmEmailGetEmailID.
	/// </summary>
	partial class frmEmailGetEmailID : BaseForm
	{
		public fecherFoundation.FCGrid Grid;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmailGetEmailID));
			this.Grid = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 302);
			this.BottomPanel.Size = new System.Drawing.Size(670, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Grid);
			this.ClientArea.Size = new System.Drawing.Size(670, 242);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(670, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(26, 26);
			this.HeaderText.Size = new System.Drawing.Size(143, 30);
			this.HeaderText.Text = "Load E-Mail";
			// 
			// Grid
			// 
			this.Grid.AllowSelection = false;
			this.Grid.AllowUserToResizeColumns = false;
			this.Grid.AllowUserToResizeRows = false;
			this.Grid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Grid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid.BackColorSel = System.Drawing.Color.Empty;
			this.Grid.Cols = 4;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.Grid.ColumnHeadersHeight = 30;
			this.Grid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid.DefaultCellStyle = dataGridViewCellStyle2;
			this.Grid.Editable = FCGrid.EditableSettings.flexEDNone;
			this.Grid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid.FixedCols = 0;
			this.Grid.FixedRows = 1;
			this.Grid.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid.FrozenCols = 0;
			this.Grid.FrozenRows = 0;
			this.Grid.GridColor = System.Drawing.Color.Empty;
			this.Grid.Location = new System.Drawing.Point(30, 25);
			this.Grid.Name = "Grid";
			this.Grid.ReadOnly = true;
			this.Grid.RowHeadersVisible = false;
			this.Grid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid.RowHeightMin = 0;
			this.Grid.Rows = 1;
			this.Grid.ShowColumnVisibilityMenu = false;
			this.Grid.Size = new System.Drawing.Size(612, 192);
			this.Grid.StandardTab = true;
			this.Grid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid.TabIndex = 0;
			this.Grid.DoubleClick += new System.EventHandler(this.Grid_DblClick);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(249, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(156, 48);
			this.btnProcess.TabIndex = 1;
			this.btnProcess.Text = "Save & Continue";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmEmailGetEmailID
			// 
			//this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(670, 410);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmEmailGetEmailID";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Load E-Mail";
			this.Load += new System.EventHandler(this.frmEmailGetEmailID_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEmailGetEmailID_KeyDown);
			this.Resize += new System.EventHandler(this.frmEmailGetEmailID_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Grid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcess;
	}
}
