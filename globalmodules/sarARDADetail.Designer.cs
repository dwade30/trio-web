﻿namespace Global
{
	/// <summary>
	/// Summary description for sarARDADetail.
	/// </summary>
	partial class sarARDADetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarARDADetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBillType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCode = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRef = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTeller = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRNum = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblCash = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRef = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTeller = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReceipt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCash = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRef)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTeller)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRef)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTeller)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCash)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldBillType,
				this.fldPrincipal,
				this.fldInterest,
				this.fldTax,
				this.fldTotal,
				this.fldCode,
				this.fldRef,
				this.fldDate,
				this.fldTeller,
				this.fldReceipt,
				this.fldCash
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblAccount,
				this.lblBillType,
				this.lblPrincipal,
				this.lblInterest,
				this.lblTax,
				this.lblTotal,
				this.lblCode,
				this.lblRef,
				this.lblDate,
				this.lblTeller,
				this.lblRNum,
				this.lnHeader,
				this.lblCash
			});
			this.GroupHeader1.Height = 0.3125F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTotals,
				this.fldTotalPrin,
				this.fldTotalInt,
				this.fldTotalTax,
				this.fldTotalTotal,
				this.lnTotals
			});
			this.GroupFooter1.Height = 0.4270833F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 0.125F;
			this.lblAccount.Width = 0.5F;
			// 
			// lblBillType
			// 
			this.lblBillType.Height = 0.1875F;
			this.lblBillType.HyperLink = null;
			this.lblBillType.Left = 0.5F;
			this.lblBillType.Name = "lblBillType";
			this.lblBillType.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblBillType.Text = "Type";
			this.lblBillType.Top = 0.125F;
			this.lblBillType.Width = 0.5F;
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 1F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPrincipal.Text = "Principal";
			this.lblPrincipal.Top = 0.125F;
			this.lblPrincipal.Width = 0.75F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 1.8125F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 0.125F;
			this.lblInterest.Width = 0.625F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 2.4375F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 0.125F;
			this.lblTax.Width = 0.6875F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 3.125F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.125F;
			this.lblTotal.Width = 0.8125F;
			// 
			// lblCode
			// 
			this.lblCode.Height = 0.1875F;
			this.lblCode.HyperLink = null;
			this.lblCode.Left = 4.125F;
			this.lblCode.Name = "lblCode";
			this.lblCode.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblCode.Text = "C";
			this.lblCode.Top = 0.125F;
			this.lblCode.Width = 0.1875F;
			// 
			// lblRef
			// 
			this.lblRef.Height = 0.1875F;
			this.lblRef.HyperLink = null;
			this.lblRef.Left = 4.6875F;
			this.lblRef.Name = "lblRef";
			this.lblRef.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblRef.Text = "Ref";
			this.lblRef.Top = 0.125F;
			this.lblRef.Width = 0.6875F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.375F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblDate.Text = "Date";
			this.lblDate.Top = 0.125F;
			this.lblDate.Width = 0.625F;
			// 
			// lblTeller
			// 
			this.lblTeller.Height = 0.1875F;
			this.lblTeller.HyperLink = null;
			this.lblTeller.Left = 6F;
			this.lblTeller.Name = "lblTeller";
			this.lblTeller.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTeller.Text = "TLR";
			this.lblTeller.Top = 0.125F;
			this.lblTeller.Width = 0.375F;
			// 
			// lblRNum
			// 
			this.lblRNum.Height = 0.1875F;
			this.lblRNum.HyperLink = null;
			this.lblRNum.Left = 6.375F;
			this.lblRNum.Name = "lblRNum";
			this.lblRNum.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblRNum.Text = "Receipt";
			this.lblRNum.Top = 0.125F;
			this.lblRNum.Width = 0.5625F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 0.3125F;
			this.lnHeader.Width = 6.9375F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 6.9375F;
			this.lnHeader.Y1 = 0.3125F;
			this.lnHeader.Y2 = 0.3125F;
			// 
			// lblCash
			// 
			this.lblCash.Height = 0.1875F;
			this.lblCash.HyperLink = null;
			this.lblCash.Left = 4.3125F;
			this.lblCash.Name = "lblCash";
			this.lblCash.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblCash.Text = "Csh";
			this.lblCash.Top = 0.125F;
			this.lblCash.Width = 0.375F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.5F;
			// 
			// fldBillType
			// 
			this.fldBillType.Height = 0.1875F;
			this.fldBillType.Left = 0.75F;
			this.fldBillType.Name = "fldBillType";
			this.fldBillType.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldBillType.Text = null;
			this.fldBillType.Top = 0F;
			this.fldBillType.Width = 0.5F;
			// 
			// fldPrincipal
			// 
			this.fldPrincipal.Height = 0.1875F;
			this.fldPrincipal.Left = 1.625F;
			this.fldPrincipal.Name = "fldPrincipal";
			this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPrincipal.Text = null;
			this.fldPrincipal.Top = 0F;
			this.fldPrincipal.Width = 0.75F;
			// 
			// fldInterest
			// 
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 2.625F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldInterest.Text = null;
			this.fldInterest.Top = 0F;
			this.fldInterest.Width = 0.625F;
			// 
			// fldTax
			// 
			this.fldTax.Height = 0.1875F;
			this.fldTax.Left = 3.625F;
			this.fldTax.Name = "fldTax";
			this.fldTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTax.Text = null;
			this.fldTax.Top = 0F;
			this.fldTax.Width = 0.6875F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 4.5625F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal.Text = null;
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 0.8125F;
			// 
			// fldCode
			// 
			this.fldCode.Height = 0.1875F;
			this.fldCode.Left = 5.6875F;
			this.fldCode.Name = "fldCode";
			this.fldCode.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCode.Text = null;
			this.fldCode.Top = 0F;
			this.fldCode.Width = 0.1875F;
			// 
			// fldRef
			// 
			this.fldRef.Height = 0.1875F;
			this.fldRef.Left = 6.5F;
			this.fldRef.Name = "fldRef";
			this.fldRef.Style = "font-family: \'Tahoma\'; white-space: nowrap";
			this.fldRef.Text = null;
			this.fldRef.Top = 0F;
			this.fldRef.Width = 0.6875F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 7.3125F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'";
			this.fldDate.Text = null;
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.625F;
			// 
			// fldTeller
			// 
			this.fldTeller.Height = 0.1875F;
			this.fldTeller.Left = 8.1875F;
			this.fldTeller.MultiLine = false;
			this.fldTeller.Name = "fldTeller";
			this.fldTeller.Style = "font-family: \'Tahoma\'; white-space: nowrap";
			this.fldTeller.Text = null;
			this.fldTeller.Top = 0F;
			this.fldTeller.Width = 0.375F;
			// 
			// fldReceipt
			// 
			this.fldReceipt.Height = 0.1875F;
			this.fldReceipt.Left = 8.75F;
			this.fldReceipt.Name = "fldReceipt";
			this.fldReceipt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldReceipt.Text = null;
			this.fldReceipt.Top = 0F;
			this.fldReceipt.Width = 0.5625F;
			// 
			// fldCash
			// 
			this.fldCash.Height = 0.1875F;
			this.fldCash.Left = 6F;
			this.fldCash.Name = "fldCash";
			this.fldCash.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldCash.Text = null;
			this.fldCash.Top = 0F;
			this.fldCash.Width = 0.375F;
			// 
			// lblTotals
			// 
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.HyperLink = null;
			this.lblTotals.Left = 0.75F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTotals.Text = "Total:";
			this.lblTotals.Top = 0.0625F;
			this.lblTotals.Width = 0.625F;
			// 
			// fldTotalPrin
			// 
			this.fldTotalPrin.Height = 0.1875F;
			this.fldTotalPrin.Left = 1.5625F;
			this.fldTotalPrin.Name = "fldTotalPrin";
			this.fldTotalPrin.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPrin.Text = "0.00";
			this.fldTotalPrin.Top = 0.0625F;
			this.fldTotalPrin.Width = 0.8125F;
			// 
			// fldTotalInt
			// 
			this.fldTotalInt.Height = 0.1875F;
			this.fldTotalInt.Left = 2.5F;
			this.fldTotalInt.Name = "fldTotalInt";
			this.fldTotalInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalInt.Text = "0.00";
			this.fldTotalInt.Top = 0.0625F;
			this.fldTotalInt.Width = 0.8125F;
			// 
			// fldTotalTax
			// 
			this.fldTotalTax.Height = 0.1875F;
			this.fldTotalTax.Left = 3.5F;
			this.fldTotalTax.Name = "fldTotalTax";
			this.fldTotalTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTax.Text = "0.00";
			this.fldTotalTax.Top = 0.0625F;
			this.fldTotalTax.Width = 0.8125F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 4.5625F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTotal.Text = "0.00";
			this.fldTotalTotal.Top = 0.0625F;
			this.fldTotalTotal.Width = 0.8125F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 0.6875F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0.0625F;
			this.lnTotals.Width = 4.75F;
			this.lnTotals.X1 = 0.6875F;
			this.lnTotals.X2 = 5.4375F;
			this.lnTotals.Y1 = 0.0625F;
			this.lnTotals.Y2 = 0.0625F;
			// 
			// sarARDADetail
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRef)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTeller)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRef)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTeller)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCash)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTeller;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReceipt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCash;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCode;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRef;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTeller;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRNum;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCash;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
	}
}
