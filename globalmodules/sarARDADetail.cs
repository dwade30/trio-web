﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
#if TWCR0000
using TWCR0000;


#else
using TWAR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for sarARDADetail.
	/// </summary>
	public partial class sarARDADetail : BaseSectionReport
	{
		// nObj = 1
		//   0	sarARDADetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/10/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/11/2005              *
		// ********************************************************
		float lngWidth;
		clsDRWrapper rsData = new clsDRWrapper();
		string strSQL;
		clsDRWrapper rsReceiptNumber = new clsDRWrapper();
		bool boolWide;
		bool boolOrderByReceipt;

		public sarARDADetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarARDADetail_ReportEnd;
		}

        private void SarARDADetail_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
			rsReceiptNumber.DisposeOf();
        }

        public static sarARDADetail InstancePtr
		{
			get
			{
				return (sarARDADetail)Sys.GetInstance(typeof(sarARDADetail));
			}
		}

		protected sarARDADetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true)
			{
				eArgs.EOF = false;
				BindFields();
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsOrder = new clsDRWrapper();
			// If rsOrder.OpenRecordset("SELECT * FROM Collections", strarDatabase, , , , , , False) Then
			// If rsOrder.EndOfFile <> True Then
			// If rsOrder.Fields("AuditSeqReceipt") Then
			// boolOrderByReceipt = True
			// Else
			// boolOrderByReceipt = False
			// End If
			// Else
			// boolOrderByReceipt = True
			// End If
			// Else
			// MsgBox "An error has occured while determining your default Audit sort order.  This will not affect the data.  The order has been set to Receipt Number.", vbInformation, "Collections Table Error"
			// boolOrderByReceipt = True
			// End If
			// 
			// Setup Totals
			strSQL = "SELECT SUM(Principal) AS Prin, SUM(Interest) AS I, SUM(Tax) AS T FROM (SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + " AND Code <> 'I') as Temp";
			rsData.OpenRecordset(strSQL, modExtraModules.strARDatabase);
			if (rsData.EndOfFile() != true)
			{
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Report", true);
				// fill the totals in
				fldTotalPrin.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Double("Prin")), "#,##0.00");
				// TODO: Field [I] not found!! (maybe it is an alias?)
				fldTotalInt.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("I")), "#,##0.00");
				// TODO: Field [T] not found!! (maybe it is an alias?)
				fldTotalTax.Text = Strings.Format(Conversion.Val(rsData.Get_Fields("T")), "#,##0.00");
				if (modGlobalConstants.Statics.gboolCR)
				{
					rsReceiptNumber.OpenRecordset("SELECT ReceiptKey, ReceiptNumber FROM Receipt", modExtraModules.strCRDatabase);
				}
				// fldTotalTotal.Text = Format(Val(rsData.Fields("Tot")), "#,##0.00")
			}
			else
			{
				fldTotalPrin.Text = " 0.00";
				fldTotalInt.Text = " 0.00";
				fldTotalTax.Text = " 0.00";
				fldTotalTotal.Text = " 0.00";
			}
			// calculate the total line
			fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldTotalPrin.Text) + FCConvert.ToDouble(fldTotalInt.Text) + FCConvert.ToDouble(fldTotalTax.Text), "#,##0.00");
			// this will put the SQL string together and set the order
			if (modGlobalConstants.Statics.gboolCR)
			{
				if (boolOrderByReceipt)
				{
					strSQL = "SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + " AND Code <> 'I' AND isnull(ReceiptNumber, 0) > 0 ORDER BY ReceiptNumber, BillType, Account, RecordedTransactionDate";
				}
				else
				{
					strSQL = "SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + " AND Code <> 'I' AND isnull(ReceiptNumber, 0) > 0 ORDER BY AccountKey, BillType, RecordedTransactionDate";
				}
			}
			else
			{
				if (boolOrderByReceipt)
				{
					strSQL = "SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + " AND Code <> 'I' ORDER BY ReceiptNumber, BillType, Account, RecordedTransactionDate";
				}
				else
				{
					strSQL = "SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngARCurrentCloseOut) + " AND Code <> 'I' ORDER BY AccountKey, BillType, RecordedTransactionDate";
				}
			}
			rsData.OpenRecordset(strSQL, modExtraModules.strARDatabase);
			SetupFields();
			frmWait.InstancePtr.IncrementProgress();
			if (rsData.EndOfFile())
			{
				EndRoutine();
			}
		}

		private void SetupFields()
		{
			// this will adjust the fields to thier correct
			lngWidth = rptARDailyAuditMaster.InstancePtr.lngWidth;
			boolWide = rptARDailyAuditMaster.InstancePtr.boolLandscape;
			// header labels/lines
			if (!boolWide)
			{
				// portrait
				lblAccount.Left = 0;
				lblBillType.Left = 720 / 1440f;
				lblPrincipal.Left = 1440 / 1440f;
				// lblInterest.Left = 2520
				lblInterest.Left = 2610 / 1440f;
				lblTax.Left = 3510 / 1440f;
				lblTotal.Left = 4500 / 1440f;
				lblCode.Left = 5940 / 1440f;
				lblCash.Left = 6210 / 1440f;
				lblRef.Left = 6750 / 1440f;
				lblDate.Left = 7740 / 1440f;
				lblTeller.Left = 8640 / 1440f;
				lblRNum.Left = 9180 / 1440f;
			}
			else
			{
				// landscape
				lblAccount.Left = 0;
				lblBillType.Left = 1080 / 1440f;
				lblPrincipal.Left = 2340 / 1440f;
				lblInterest.Left = 3780 / 1440f;
				lblTax.Left = 5220 / 1440f;
				lblTotal.Left = 6570 / 1440f;
				lblCode.Left = 8190 / 1440f;
				lblCash.Left = 8640 / 1440f;
				lblRef.Left = 9360 / 1440f;
				lblDate.Left = 10530 / 1440f;
				lblTeller.Left = 11790 / 1440f;
				lblRNum.Left = 12600 / 1440f;
			}
			lnHeader.X2 = lngWidth;
			this.PrintWidth = lngWidth;
			// detail section
			fldAccount.Left = lblAccount.Left;
			fldBillType.Left = lblBillType.Left;
			fldPrincipal.Left = lblPrincipal.Left;
			fldInterest.Left = lblInterest.Left;
			fldTax.Left = lblTax.Left;
			fldTotal.Left = lblTotal.Left;
			fldCode.Left = lblCode.Left;
			fldCash.Left = lblCash.Left;
			fldRef.Left = lblRef.Left;
			fldDate.Left = lblDate.Left;
			fldTeller.Left = lblTeller.Left;
			fldReceipt.Left = lblRNum.Left;
			// footer labels/fields
			lblTotals.Left = lblBillType.Left;
			fldTotalPrin.Left = lblPrincipal.Left - 90 / 1440f;
			fldTotalInt.Left = lblInterest.Left - 180 / 1440f;
			fldTotalTax.Left = lblTax.Left - 180 / 1440f;
			fldTotalTotal.Left = lblTotal.Left;
			lnTotals.X1 = lblTotals.Left;
			lnTotals.X2 = fldTotalTotal.Left + fldTotalTotal.Width;
		}

		private void BindFields()
		{
			// this will fill the fields with the correct data
			frmWait.InstancePtr.IncrementProgress();
			fldAccount.Text = FCConvert.ToString(modARStatusPayments.GetAccountNumberFromAccountKey_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("AccountKey"))));
			// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
			fldBillType.Text = FCConvert.ToString(rsData.Get_Fields("BillType"));
			// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
			fldPrincipal.Text = Strings.Format(rsData.Get_Fields("Principal"), "#,##0.00");
			// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
			fldInterest.Text = Strings.Format(rsData.Get_Fields("Interest"), " #,##0.00");
			// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
			fldTax.Text = Strings.Format(rsData.Get_Fields("Tax"), "#,##0.00");
			// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
			// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
			// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
			fldTotal.Text = Strings.Format(rsData.Get_Fields("Principal") + rsData.Get_Fields("Interest") + rsData.Get_Fields("Tax"), "#,##0.00");
			fldCode.Text = rsData.Get_Fields_String("Code");
			if (FCConvert.ToString(rsData.Get_Fields_String("CashDrawer")) == "Y")
			{
				fldCash.Text = "Y";
			}
			else
			{
				if (FCConvert.ToString(rsData.Get_Fields_String("GeneralLedger")) == "Y")
				{
					fldCash.Text = "N Y";
				}
				else
				{
					fldCash.Text = "N N";
				}
			}
			fldRef.Text = rsData.Get_Fields_String("Reference");
			fldTeller.Text = Strings.Left(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Teller"))) + "   ", 3);
			fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("RecordedTransactionDate"), "MM/dd/yy");
			// fldDate.Text = Format(.Fields("EffectiveInterestDate"), "MM/dd/yy")
			if (modGlobalConstants.Statics.gboolCR)
			{
				// this will find the real receipt number
				rsReceiptNumber.FindFirstRecord("ReceiptKey", rsData.Get_Fields_Int32("ReceiptNumber"));
				if (!rsReceiptNumber.NoMatch)
				{
					fldReceipt.Text = FCConvert.ToString(rsReceiptNumber.Get_Fields_Int32("ReceiptNumber"));
				}
				else
				{
					fldReceipt.Text = FCConvert.ToString(rsData.Get_Fields_Int32("ReceiptNumber"));
				}
			}
			else
			{
				fldReceipt.Text = FCConvert.ToString(rsData.Get_Fields_Int32("ReceiptNumber"));
			}
			// move to the next record
			rsData.MoveNext();
		}

		private void EndRoutine()
		{
			// this will show a message that there are no records to process and hide the rest of the report
			lnHeader.Visible = false;
			lnTotals.Visible = false;
			lblAccount.Visible = false;
			lblBillType.Visible = false;
			lblPrincipal.Visible = false;
			lblInterest.Visible = false;
			lblTax.Visible = false;
			lblTotal.Visible = false;
			lblCode.Visible = false;
			lblRef.Visible = false;
			lblDate.Visible = false;
			lblTeller.Visible = false;
			lblRNum.Visible = false;
			fldTotalPrin.Visible = false;
			fldTotalInt.Visible = false;
			fldTotalTax.Visible = false;
			fldTotalTotal.Visible = false;
			lblTotals.Visible = false;
			// lblTotals.Width = lngWidth - lblTotals.Left
			// lblTotals.Text = "There are no records to process."
		}

		private void sarARDADetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarARDADetail.Caption	= "Daily Audit Detail";
			//sarARDADetail.Icon	= "sarARDADetail.dsx":0000";
			//sarARDADetail.Left	= 0;
			//sarARDADetail.Top	= 0;
			//sarARDADetail.Width	= 11880;
			//sarARDADetail.Height	= 8175;
			//sarARDADetail.StartUpPosition	= 3;
			//sarARDADetail.SectionData	= "sarARDADetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
