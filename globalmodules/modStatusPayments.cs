﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWBD0000
using TWBD0000;


#elif TWUT0000
using TWUT0000;


#else
using TWCR0000;
#endif
namespace Global
{
	public class modStatusPayments
	{
		// This boolean will be set to let the program knwo whether the current bill selected to make a payment on is in a tax club.  This will be set to false if Auto is selected
		public struct CHGINTStruct
		{
			public int BillKey;
			public DateTime InterestAppliedThroughDate;
			public bool Reversal;
		};
		// this will hold the billkey and the date that has to be put into the InterestAppliedThroughDate
		//public struct Payment
		//{
		//	// these are the fields that each payment needs to save
		//	public int ID;
		//	public int Account;
		//	public int Year;
		//	public int BillKey;
		//	public int CHGINTNumber;
		//	public DateTime CHGINTDate;
		//	// VBto upgrade warning: ActualSystemDate As DateTime	OnWrite(DateTime, short)
		//	public DateTime ActualSystemDate;
		//	// VBto upgrade warning: EffectiveInterestDate As DateTime	OnWrite(DateTime, short)
		//	public DateTime EffectiveInterestDate;
		//	// VBto upgrade warning: RecordedTransactionDate As DateTime	OnWrite(DateTime, short)
		//	public DateTime RecordedTransactionDate;
		//	public string Teller;
		//	public string Reference;
		//	// VBto upgrade warning: Period As string	OnWrite(string, short)
		//	public string Period;
		//	public string Code;
		//	public string ReceiptNumber;
		//	// VBto upgrade warning: Principal As double	OnWrite(double, double, short)
		//	public decimal Principal;
		//	// VBto upgrade warning: PreLienInterest As double	OnWrite(double, short, double)
		//	public decimal PreLienInterest;
		//	// VBto upgrade warning: CurrentInterest As double	OnWrite(double, short, double)
		//	public decimal CurrentInterest;
		//	// VBto upgrade warning: LienCost As double	OnWrite(double, short, double)
		//	public decimal LienCost;
		//	// VBto upgrade warning: TransNumber As string	OnWriteFCConvert.ToInt16(
		//	public string TransNumber;
		//	public string PaidBy;
		//	// XXXX
		//	public string Comments;
		//	public string CashDrawer;
		//	public string GeneralLedger;
		//	public string BudgetaryAccountNumber;
		//	public string BillCode;
		//	public int ReversalID;
		//	public bool Reversal;
		//	public DateTime SetEIDate;
		//	public int ResCode;
		//	//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
		//	public Payment(int unusedParam)
		//	{
		//		this.ID = 0;
		//		this.Account = 0;
		//		this.Year = 0;
		//		this.BillKey = 0;
		//		this.CHGINTNumber = 0;
		//		this.CHGINTDate = DateTime.FromOADate(0);
		//		this.ActualSystemDate = DateTime.FromOADate(0);
		//		this.EffectiveInterestDate = DateTime.FromOADate(0);
		//		this.RecordedTransactionDate = DateTime.FromOADate(0);
		//		this.Teller = string.Empty;
		//		this.Reference = string.Empty;
		//		this.Period = string.Empty;
		//		this.Code = string.Empty;
		//		this.ReceiptNumber = string.Empty;
		//		this.Principal = 0;
		//		this.PreLienInterest = 0;
		//		this.CurrentInterest = 0;
		//		this.LienCost = 0;
		//		this.TransNumber = string.Empty;
		//		this.PaidBy = string.Empty;
		//		this.Comments = string.Empty;
		//		this.CashDrawer = string.Empty;
		//		this.GeneralLedger = string.Empty;
		//		this.BudgetaryAccountNumber = string.Empty;
		//		this.BillCode = string.Empty;
		//		//this.ReversalID = 0;
		//		// FC:FINAL:VGE - #842 ReversalID is expected to be -1 by default
		//		this.ReversalID = -1;
		//		this.Reversal = false;
		//		this.SetEIDate = DateTime.FromOADate(0);
		//		this.ResCode = 0;
		//	}
		//};
		// DJW@11/17/2010 Changed array size from 1000 to 20000
		public const int MAX_PAYMENTS = 20000;
		// this will hold one payment in order to print a reprint of the receipt for it
		// this will be used to store abatements by period in frmRECLStatus, frmTaxService, Reminder Notices, and Bills
		public struct BillPeriods
		{
			public double Per1;
			public double Per2;
			public double Per3;
			public double Per4;
		};

	
		public static DateTime GetLastInterestDate_CL(int lngCurrentAccountUT, int lngBill, DateTime dtPassDate, int lngCurrKey)
		{
			DateTime GetLastInterestDate_CL = System.DateTime.Now;
			// MAL@20070920: New function to update to the last Interest Paid Date after reversal
			// Call Reference: 113872/10850
			bool blnSuccess/*unused?*/;
			bool blnContinue;
			DateTime dtNewDate = DateTime.FromOADate(0);
			int intPer;
			int lngRK = 0;
			int lngAcctKey/*unused?*/;
			int lngBillKey = 0;
			int lngCIKey = 0;
			clsDRWrapper rsBill = new clsDRWrapper();
			clsDRWrapper rsRK = new clsDRWrapper();
			clsDRWrapper rsUpdate = new clsDRWrapper();
			string strFieldNm = "";
			blnContinue = true;
			// Optimistic
			intPer = 1;
			if (blnContinue)
			{
				rsBill.OpenRecordset("SELECT ID FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngCurrentAccountUT) + " AND BillingYear = " + FCConvert.ToString(lngBill), modExtraModules.strCLDatabase);
				if (rsBill.RecordCount() > 0)
				{
					lngBillKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
					blnContinue = true;
				}
				else
				{
					blnContinue = false;
				}
			}
			if (blnContinue)
			{
				rsBill.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(lngCurrentAccountUT) + " AND BillKey = " + FCConvert.ToString(lngBillKey) + " AND CHGINTNumber <> 0 order by effectiveinterestdate desc,ID DESC", modExtraModules.strCLDatabase);
				if (rsBill.RecordCount() > 0)
				{
					lngCIKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("CHGINTNumber"));
					if (lngCurrKey == 0)
					{
						lngCurrKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
					}
					blnContinue = true;
				}
				else
				{
					lngCurrKey = lngCurrKey;
					// MAL@20080530: Changed from False as the CHGINT number is not always needed
					blnContinue = true;
				}
			}
			if (blnContinue)
			{
				rsBill.OpenRecordset("SELECT Max(EffectiveInterestDate) as LastDate, Account FROM PaymentRec WHERE Account = " + FCConvert.ToString(lngCurrentAccountUT) + " AND ID <> " + FCConvert.ToString(lngCIKey) + " AND ID <> " + FCConvert.ToString(lngCurrKey) + " AND BillKey = " + FCConvert.ToString(lngBillKey) + " AND RecordedTransactionDate < '" + FCConvert.ToString(dtPassDate) + "' GROUP BY Account", modExtraModules.strCLDatabase);
				if (rsBill.RecordCount() > 0)
				{
					// TODO: Field [LastDate] not found!! (maybe it is an alias?)
					dtNewDate = (DateTime)rsBill.Get_Fields("LastDate");
				}
				else
				{
					// No previous payments - use Rate ID
					rsRK.OpenRecordset("SELECT RateKey FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngCurrentAccountUT) + " AND ID = " + FCConvert.ToString(lngBillKey), modExtraModules.strCLDatabase);
					if (rsRK.RecordCount() > 0)
					{
						lngRK = FCConvert.ToInt32(rsRK.Get_Fields_Int32("RateKey"));
						// rsRK.OpenRecordset "SELECT InterestStartDate" & intPer & " FROM RateRec WHERE RateKey = " & lngRK, strCLDatabase
						rsRK.OpenRecordset("select * from raterec where ID = " + FCConvert.ToString(lngRK), modExtraModules.strCLDatabase);
						if (rsRK.RecordCount() > 0)
						{
							// dtNewDate = rsRK.Get_Fields("InterestStartDate" & intPer)
							dtNewDate = DateAndTime.DateAdd("d", -1, rsRK.Get_Fields_DateTime("InterestStartDate" + FCConvert.ToString(intPer)));
						}
					}
				}
				if (!FCUtils.IsNull(dtNewDate))
				{
					GetLastInterestDate_CL = dtNewDate;
				}
			}
			else
			{
				GetLastInterestDate_CL = DateTime.Today;
			}
			return GetLastInterestDate_CL;
		}

		public class StaticVariables
		{
			public double dblInterestRate;
            public bool boolRE;
            public double gdblFlatInterestAmount;
            public bool gboolDefaultPaymentsToAuto;
        }

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
