﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

#if TWCR0000
using TWCR0000;


#elif TWUT0000
using modGlobal = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for rptUTDailyAuditMaster.
	/// </summary>
	public partial class rptUTDailyAuditMaster : BaseSectionReport
	{
		public rptUTDailyAuditMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Daily Audit Report";
		}

		public static rptUTDailyAuditMaster InstancePtr
		{
			get
			{
				return (rptUTDailyAuditMaster)Sys.GetInstance(typeof(rptUTDailyAuditMaster));
			}
		}

		protected rptUTDailyAuditMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptUTDailyAuditMaster	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/06/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/01/2005              *
		// ********************************************************
		public bool boolWide;
		// True if this report is in Wide Format
		public float lngWidth;
		// This is the Print Width of the report
		bool boolDone;
		public string strType = "";
		public bool boolOrderByReceipt;
		// order by receipt
		public bool boolCloseOutAudit;
		// Actually close out the payments
		public bool boolLandscape;
		// is the report to be printed landscape?
		public int intType;
		// 0 - Water, 1 - Sewer, 2 - Both
		bool boolSaveReport;
		bool blnBoth;
		bool blnBothSelected;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// this should let the detail section fire once
			eArgs.EOF = boolDone;
			if (blnBothSelected)
			{
				if (blnBoth)
				{
					blnBoth = false;
				}
				else
				{
					boolDone = true;
					intType = 1;
				}
			}
			else
			{
				boolDone = true;
			}
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
			// MAL@20080826: Add check for recreate before saving
			// Tracker Reference: 14245
			if (boolCloseOutAudit && boolSaveReport && !modGlobalConstants.Statics.gblnRecreate)
			{
				// save the report only if they are actually getting closed out
				if (this.Document.Pages.Count > 0)
				{
					modGlobalFunctions.IncrementSavedReports("LastUTDailyAudit");
					this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastUTDailyAudit1.RDF"));
				}
			}
        }

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading", true);
		}

		private void SetupFields()
		{
			// find out which way the user wants to print
			boolWide = boolLandscape;
			if (boolWide)
			{
				// wide format
				lngWidth = 13500 / 1440F;
				Document.Printer.Landscape = true;
			}
			else
			{
				// narrow format
				lngWidth = 10800 / 1440F;
				Document.Printer.Landscape = false;
			}
			this.PrintWidth = lngWidth;
			sarUTDailyAudit.Width = lngWidth;
		}

		public void StartUp(bool boolCloseOut, bool boolPassLandscape, short intPassType = 2, bool boolShowPrinterDialog = true, bool boolPrint = false, string strAuditPrinter = "")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsC = new clsDRWrapper();
				boolCloseOutAudit = boolCloseOut;
				if (boolCloseOutAudit)
				{
					if (modUTFunctions.Statics.TownService == "W")
					{
						intType = 0;
					}
					else if (modUTFunctions.Statics.TownService == "S")
					{
						intType = 1;
					}
					else
					{
						intType = 2;
						// automatically both
					}
				}
				else
				{
					intType = intPassType;
				}
				if (intType == 2)
				{
					blnBoth = true;
					blnBothSelected = true;
					intType = 0;
				}
				boolLandscape = boolPassLandscape;
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				SetupFields();
				frmWait.InstancePtr.IncrementProgress();
				// If IsThisCR Then
				rsC.OpenRecordset("SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut), modExtraModules.strUTDatabase);
				// Else
				// rsC.OpenRecordset "SELECT * FROM PaymentRec WHERE DailyCloseOut = 0", strUTDatabase
				// End If
				if (rsC.EndOfFile())
				{
					boolSaveReport = false;
				}
				else
				{
					boolSaveReport = true;
				}
				rsC.DisposeOf();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Start Up", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			sarUTDailyAudit.Report = new arUTDailyAudit();
			// 0 - Water, 1 - Sewer, 2 - Both
			sarUTDailyAudit.Report.UserData = intType;
		}

		private void rptUTDailyAuditMaster_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptUTDailyAuditMaster.Caption	= "Daily Audit Report";
			//rptUTDailyAuditMaster.Icon	= "rptUTDailtAuditMaster.dsx":0000";
			//rptUTDailyAuditMaster.Left	= 0;
			//rptUTDailyAuditMaster.Top	= 0;
			//rptUTDailyAuditMaster.Width	= 11880;
			//rptUTDailyAuditMaster.Height	= 8595;
			//rptUTDailyAuditMaster.StartUpPosition	= 3;
			//rptUTDailyAuditMaster.SectionData	= "rptUTDailtAuditMaster.dsx":058A;
			//End Unmaped Properties
		}
	}
}
