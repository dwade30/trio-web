﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmShowCYA.
	/// </summary>
	partial class frmShowCYA : BaseForm
	{
		public fecherFoundation.FCComboBox cmbMod;
		public FCCommonDialog CommonDialog1;
		public FCGrid vsCYA;
		public fecherFoundation.FCLabel lblInstructions;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmShowCYA));
			this.cmbMod = new fecherFoundation.FCComboBox();
			this.CommonDialog1 = new FCCommonDialog();
			this.vsCYA = new FCGrid();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.SuspendLayout();
			//
			// cmbMod
			//
			this.cmbMod.Name = "cmbMod";
			this.cmbMod.TabIndex = 1;
			this.cmbMod.Location = new System.Drawing.Point(372, 28);
			this.cmbMod.Size = new System.Drawing.Size(94, 40);
			this.cmbMod.Text = "";
			this.cmbMod.BackColor = System.Drawing.SystemColors.Window;
			this.cmbMod.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			////this.cmbMod.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.cmbMod.SelectedIndexChanged += new System.EventHandler(this.cmbMod_SelectedIndexChanged);
			this.cmbMod.DropDown += new System.EventHandler(this.cmbMod_DropDown);
			this.cmbMod.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbMod_KeyDown);
			//
			// CommonDialog1
			//
			//
			// vsCYA
			//
			this.vsCYA.Name = "vsCYA";
			this.vsCYA.Enabled = true;
			this.vsCYA.TabIndex = 0;
			this.vsCYA.Location = new System.Drawing.Point(11, 56);
			this.vsCYA.Size = new System.Drawing.Size(602, 464);
			this.vsCYA.ExplorerBar = FCGrid.ExplorerBarSettings.flexExSortShow;
			//this.vsCYA.OcxState = ((Wisej.Web.AxHost.State)(resources.GetObject("vsCYA.OcxState")));
			this.vsCYA.ColumnWidthChanged += new DataGridViewColumnEventHandler(Grid_ColumnWidthChanged);
			this.vsCYA.RowHeightChanged += new DataGridViewRowEventHandler(Grid_RowHeightChanged);
			this.vsCYA.Cols = 10;
			this.vsCYA.Editable = FCGrid.EditableSettings.flexEDNone;
			this.vsCYA.FixedCols = 0;
			this.vsCYA.FixedRows = 1;
			this.vsCYA.FrozenCols = 0;
			this.vsCYA.FrozenRows = 0;
			this.vsCYA.Rows = 1;
			this.vsCYA.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			//
			// lblInstructions
			//
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.TabIndex = 2;
			this.lblInstructions.Location = new System.Drawing.Point(65, 28);
			this.lblInstructions.Size = new System.Drawing.Size(300, 18);
			this.lblInstructions.Text = "";
			//this.lblInstructions.BackColor = System.Drawing.SystemColors.Control;
			this.lblInstructions.TextAlign = System.Drawing.ContentAlignment.TopRight;
			////this.lblInstructions.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// mnuFile
			//
			this.mnuFile.MenuItems.AddRange(new fecherFoundation.FCToolStripMenuItem[] {
				this.mnuFileSeperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			//
			// mnuFileSeperator
			//
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			//
			// mnuFileExit
			//
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new fecherFoundation.FCToolStripMenuItem[] {
				this.mnuFile
			});
			//
			// frmShowCYA
			//
			this.ClientSize = new System.Drawing.Size(627, 529);
			this.ClientArea.Controls.Add(this.cmbMod);
			this.ClientArea.Controls.Add(this.vsCYA);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.Name = "frmShowCYA";
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Activated += new System.EventHandler(this.frmShowCYA_Activated);
			this.Load += new System.EventHandler(this.frmShowCYA_Load);
			this.Resize += new System.EventHandler(this.frmShowCYA_Resize);
			this.Text = "Show Entries";
			this.cmbMod.ResumeLayout(false);
			this.vsCYA.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsCYA)).EndInit();
			this.lblInstructions.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}
