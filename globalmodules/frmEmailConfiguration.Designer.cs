﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmEmailConfiguration.
	/// </summary>
	partial class frmEmailConfiguration : BaseForm
	{
		public fecherFoundation.FCFrame framDefault;
		public fecherFoundation.FCCheckBox chkUseTLS;
		public fecherFoundation.FCTextBox txtSMTPServer;
		public fecherFoundation.FCTextBox txtUser;
		public fecherFoundation.FCTextBox txtPassword;
		public fecherFoundation.FCTextBox txtEmail;
		public fecherFoundation.FCComboBox cmbAuthType;
		public fecherFoundation.FCTextBox txtFrom;
		public fecherFoundation.FCTextBox txtPort;
		public fecherFoundation.FCCheckBox chkUseSSL;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCFrame framUser;
		public fecherFoundation.FCCheckBox chkUserUseTLS;
		public fecherFoundation.FCCheckBox chkUserUseSSL;
		public fecherFoundation.FCTextBox txtUserPort;
		public fecherFoundation.FCTextBox txtUserFrom;
		public fecherFoundation.FCComboBox cmbUserAuthType;
		public fecherFoundation.FCTextBox txtUserEmail;
		public fecherFoundation.FCTextBox txtUserPassword;
		public fecherFoundation.FCTextBox txtUserUser;
		public fecherFoundation.FCTextBox txtUserServer;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEmailConfiguration));
            this.framDefault = new fecherFoundation.FCFrame();
            this.chkUseTLS = new fecherFoundation.FCCheckBox();
            this.txtSMTPServer = new fecherFoundation.FCTextBox();
            this.txtUser = new fecherFoundation.FCTextBox();
            this.txtPassword = new fecherFoundation.FCTextBox();
            this.txtEmail = new fecherFoundation.FCTextBox();
            this.cmbAuthType = new fecherFoundation.FCComboBox();
            this.txtFrom = new fecherFoundation.FCTextBox();
            this.txtPort = new fecherFoundation.FCTextBox();
            this.chkUseSSL = new fecherFoundation.FCCheckBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.framUser = new fecherFoundation.FCFrame();
            this.chkUserUseTLS = new fecherFoundation.FCCheckBox();
            this.chkUserUseSSL = new fecherFoundation.FCCheckBox();
            this.txtUserPort = new fecherFoundation.FCTextBox();
            this.txtUserFrom = new fecherFoundation.FCTextBox();
            this.cmbUserAuthType = new fecherFoundation.FCComboBox();
            this.txtUserEmail = new fecherFoundation.FCTextBox();
            this.txtUserPassword = new fecherFoundation.FCTextBox();
            this.txtUserUser = new fecherFoundation.FCTextBox();
            this.txtUserServer = new fecherFoundation.FCTextBox();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.btnProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framDefault)).BeginInit();
            this.framDefault.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseTLS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSSL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.framUser)).BeginInit();
            this.framUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserUseTLS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserUseSSL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 492);
            this.BottomPanel.Size = new System.Drawing.Size(694, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.framDefault);
            this.ClientArea.Controls.Add(this.framUser);
            this.ClientArea.Size = new System.Drawing.Size(694, 432);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(694, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(26, 26);
            this.HeaderText.Size = new System.Drawing.Size(236, 30);
            this.HeaderText.Text = "E-mail Configuration";
            // 
            // framDefault
            // 
            this.framDefault.Controls.Add(this.chkUseTLS);
            this.framDefault.Controls.Add(this.txtSMTPServer);
            this.framDefault.Controls.Add(this.txtUser);
            this.framDefault.Controls.Add(this.txtPassword);
            this.framDefault.Controls.Add(this.txtEmail);
            this.framDefault.Controls.Add(this.cmbAuthType);
            this.framDefault.Controls.Add(this.txtFrom);
            this.framDefault.Controls.Add(this.txtPort);
            this.framDefault.Controls.Add(this.chkUseSSL);
            this.framDefault.Controls.Add(this.Label1);
            this.framDefault.Controls.Add(this.Label2);
            this.framDefault.Controls.Add(this.Label3);
            this.framDefault.Controls.Add(this.Label4);
            this.framDefault.Controls.Add(this.Label9);
            this.framDefault.Controls.Add(this.Label11);
            this.framDefault.Location = new System.Drawing.Point(30, 25);
            this.framDefault.Name = "framDefault";
            this.framDefault.Size = new System.Drawing.Size(631, 390);
            this.framDefault.Text = "Default Settings";
            this.framDefault.UseMnemonic = false;
            // 
            // chkUseTLS
            // 
            this.chkUseTLS.Location = new System.Drawing.Point(520, 335);
            this.chkUseTLS.Name = "chkUseTLS";
            this.chkUseTLS.Size = new System.Drawing.Size(79, 24);
            this.chkUseTLS.TabIndex = 14;
            this.chkUseTLS.Text = "Use TLS";
            this.ToolTip1.SetToolTip(this.chkUseTLS, "Use Transport Layer Security");
            // 
            // txtSMTPServer
            // 
            this.txtSMTPServer.BackColor = System.Drawing.SystemColors.Window;
            this.txtSMTPServer.Location = new System.Drawing.Point(165, 30);
            this.txtSMTPServer.Name = "txtSMTPServer";
            this.txtSMTPServer.Size = new System.Drawing.Size(446, 40);
            this.txtSMTPServer.TabIndex = 1;
            // 
            // txtUser
            // 
            this.txtUser.BackColor = System.Drawing.SystemColors.Window;
            this.txtUser.Location = new System.Drawing.Point(165, 130);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(223, 40);
            this.txtUser.TabIndex = 5;
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtPassword.Location = new System.Drawing.Point(165, 180);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(224, 40);
            this.txtPassword.TabIndex = 7;
            // 
            // txtEmail
            // 
            this.txtEmail.BackColor = System.Drawing.SystemColors.Window;
            this.txtEmail.Location = new System.Drawing.Point(165, 230);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(446, 40);
            this.txtEmail.TabIndex = 9;
            // 
            // cmbAuthType
            // 
            this.cmbAuthType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAuthType.Location = new System.Drawing.Point(165, 330);
            this.cmbAuthType.Name = "cmbAuthType";
            this.cmbAuthType.Size = new System.Drawing.Size(223, 40);
            this.cmbAuthType.TabIndex = 12;
            // 
            // txtFrom
            // 
            this.txtFrom.BackColor = System.Drawing.SystemColors.Window;
            this.txtFrom.Location = new System.Drawing.Point(165, 280);
            this.txtFrom.Name = "txtFrom";
            this.txtFrom.Size = new System.Drawing.Size(223, 40);
            this.txtFrom.TabIndex = 11;
            // 
            // txtPort
            // 
            this.txtPort.BackColor = System.Drawing.SystemColors.Window;
            this.txtPort.Location = new System.Drawing.Point(165, 80);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(223, 40);
            this.txtPort.TabIndex = 3;
            this.txtPort.Validating += new System.ComponentModel.CancelEventHandler(this.txtPort_Validating);
            // 
            // chkUseSSL
            // 
            this.chkUseSSL.Location = new System.Drawing.Point(408, 335);
            this.chkUseSSL.Name = "chkUseSSL";
            this.chkUseSSL.Size = new System.Drawing.Size(80, 24);
            this.chkUseSSL.TabIndex = 13;
            this.chkUseSSL.Text = "Use SSL";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(104, 17);
            this.Label1.Text = "SMTP SERVER";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(20, 144);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(45, 17);
            this.Label2.TabIndex = 4;
            this.Label2.Text = "USER";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(20, 194);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(87, 17);
            this.Label3.TabIndex = 6;
            this.Label3.Text = "PASSWORD";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(20, 244);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(54, 17);
            this.Label4.TabIndex = 8;
            this.Label4.Text = "E-MAIL";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(20, 294);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(90, 17);
            this.Label9.TabIndex = 10;
            this.Label9.Text = "FROM NAME";
            this.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(20, 94);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(45, 17);
            this.Label11.TabIndex = 2;
            this.Label11.Text = "PORT";
            this.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // framUser
            // 
            this.framUser.Controls.Add(this.chkUserUseTLS);
            this.framUser.Controls.Add(this.chkUserUseSSL);
            this.framUser.Controls.Add(this.txtUserPort);
            this.framUser.Controls.Add(this.txtUserFrom);
            this.framUser.Controls.Add(this.cmbUserAuthType);
            this.framUser.Controls.Add(this.txtUserEmail);
            this.framUser.Controls.Add(this.txtUserPassword);
            this.framUser.Controls.Add(this.txtUserUser);
            this.framUser.Controls.Add(this.txtUserServer);
            this.framUser.Controls.Add(this.Label12);
            this.framUser.Controls.Add(this.Label10);
            this.framUser.Controls.Add(this.Label8);
            this.framUser.Controls.Add(this.Label7);
            this.framUser.Controls.Add(this.Label6);
            this.framUser.Controls.Add(this.Label5);
            this.framUser.Location = new System.Drawing.Point(30, 435);
            this.framUser.Name = "framUser";
            this.framUser.Size = new System.Drawing.Size(631, 390);
            this.framUser.TabIndex = 1;
            this.framUser.Text = "Settings For If Different Than Above";
            this.framUser.UseMnemonic = false;
            // 
            // chkUserUseTLS
            // 
            this.chkUserUseTLS.Location = new System.Drawing.Point(520, 335);
            this.chkUserUseTLS.Name = "chkUserUseTLS";
            this.chkUserUseTLS.Size = new System.Drawing.Size(79, 24);
            this.chkUserUseTLS.TabIndex = 14;
            this.chkUserUseTLS.Text = "Use TLS";
            this.ToolTip1.SetToolTip(this.chkUserUseTLS, "Use Transport Layer Security");
            // 
            // chkUserUseSSL
            // 
            this.chkUserUseSSL.Location = new System.Drawing.Point(408, 335);
            this.chkUserUseSSL.Name = "chkUserUseSSL";
            this.chkUserUseSSL.Size = new System.Drawing.Size(80, 24);
            this.chkUserUseSSL.TabIndex = 13;
            this.chkUserUseSSL.Text = "Use SSL";
            // 
            // txtUserPort
            // 
            this.txtUserPort.BackColor = System.Drawing.SystemColors.Window;
            this.txtUserPort.Location = new System.Drawing.Point(165, 80);
            this.txtUserPort.Name = "txtUserPort";
            this.txtUserPort.Size = new System.Drawing.Size(223, 40);
            this.txtUserPort.TabIndex = 3;
            this.txtUserPort.Validating += new System.ComponentModel.CancelEventHandler(this.txtUserPort_Validating);
            // 
            // txtUserFrom
            // 
            this.txtUserFrom.BackColor = System.Drawing.SystemColors.Window;
            this.txtUserFrom.Location = new System.Drawing.Point(165, 280);
            this.txtUserFrom.Name = "txtUserFrom";
            this.txtUserFrom.Size = new System.Drawing.Size(223, 40);
            this.txtUserFrom.TabIndex = 11;
            // 
            // cmbUserAuthType
            // 
            this.cmbUserAuthType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbUserAuthType.Location = new System.Drawing.Point(165, 330);
            this.cmbUserAuthType.Name = "cmbUserAuthType";
            this.cmbUserAuthType.Size = new System.Drawing.Size(223, 40);
            this.cmbUserAuthType.TabIndex = 12;
            // 
            // txtUserEmail
            // 
            this.txtUserEmail.BackColor = System.Drawing.SystemColors.Window;
            this.txtUserEmail.Location = new System.Drawing.Point(165, 230);
            this.txtUserEmail.Name = "txtUserEmail";
            this.txtUserEmail.Size = new System.Drawing.Size(446, 40);
            this.txtUserEmail.TabIndex = 9;
            // 
            // txtUserPassword
            // 
            this.txtUserPassword.BackColor = System.Drawing.SystemColors.Window;
            this.txtUserPassword.Location = new System.Drawing.Point(165, 180);
            this.txtUserPassword.Name = "txtUserPassword";
            this.txtUserPassword.Size = new System.Drawing.Size(223, 40);
            this.txtUserPassword.TabIndex = 7;
            // 
            // txtUserUser
            // 
            this.txtUserUser.BackColor = System.Drawing.SystemColors.Window;
            this.txtUserUser.Location = new System.Drawing.Point(165, 130);
            this.txtUserUser.Name = "txtUserUser";
            this.txtUserUser.Size = new System.Drawing.Size(223, 40);
            this.txtUserUser.TabIndex = 5;
            // 
            // txtUserServer
            // 
            this.txtUserServer.BackColor = System.Drawing.SystemColors.Window;
            this.txtUserServer.Location = new System.Drawing.Point(165, 30);
            this.txtUserServer.Name = "txtUserServer";
            this.txtUserServer.Size = new System.Drawing.Size(446, 40);
            this.txtUserServer.TabIndex = 1;
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Location = new System.Drawing.Point(20, 94);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(45, 17);
            this.Label12.TabIndex = 2;
            this.Label12.Text = "PORT";
            this.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(20, 294);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(90, 17);
            this.Label10.TabIndex = 10;
            this.Label10.Text = "FROM NAME";
            this.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(20, 244);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(54, 17);
            this.Label8.TabIndex = 8;
            this.Label8.Text = "E-MAIL";
            this.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(20, 194);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(87, 17);
            this.Label7.TabIndex = 6;
            this.Label7.Text = "PASSWORD";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(20, 44);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(104, 17);
            this.Label6.Text = "SMTP SERVER";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(20, 144);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(45, 17);
            this.Label5.TabIndex = 4;
            this.Label5.Text = "USER";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(283, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(98, 48);
            this.btnProcess.TabIndex = 1;
            this.btnProcess.Text = "Save";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // frmEmailConfiguration
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(694, 600);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmEmailConfiguration";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "E-mail Configuration";
            this.Load += new System.EventHandler(this.frmEmailConfiguration_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEmailConfiguration_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.framDefault)).EndInit();
            this.framDefault.ResumeLayout(false);
            this.framDefault.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseTLS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSSL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.framUser)).EndInit();
            this.framUser.ResumeLayout(false);
            this.framUser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserUseTLS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUserUseSSL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton btnProcess;
	}
}
