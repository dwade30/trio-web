﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmLienDischargeNotice.
	/// </summary>
	partial class frmLienDischargeNotice : BaseForm
	{
		public fecherFoundation.FCFrame fraVariables;
		public fecherFoundation.FCGrid vsData;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel lblYear;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLienDischargeNotice));
            this.fraVariables = new fecherFoundation.FCFrame();
            this.vsData = new fecherFoundation.FCGrid();
            this.lblName = new fecherFoundation.FCLabel();
            this.lblYear = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdFilePrintBlankForm = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraVariables)).BeginInit();
            this.fraVariables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrintBlankForm)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 517);
            this.BottomPanel.Size = new System.Drawing.Size(578, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraVariables);
            this.ClientArea.Size = new System.Drawing.Size(598, 628);
            this.ClientArea.Controls.SetChildIndex(this.fraVariables, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFilePrintBlankForm);
            this.TopPanel.Size = new System.Drawing.Size(598, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFilePrintBlankForm, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(253, 30);
            this.HeaderText.Text = "Lien Discharge Notice";
            // 
            // fraVariables
            // 
            this.fraVariables.AppearanceKey = "groupBoxNoBorders";
            this.fraVariables.Controls.Add(this.vsData);
            this.fraVariables.Controls.Add(this.lblName);
            this.fraVariables.Controls.Add(this.lblYear);
            this.fraVariables.Location = new System.Drawing.Point(30, 30);
            this.fraVariables.Name = "fraVariables";
            this.fraVariables.Size = new System.Drawing.Size(565, 487);
            this.fraVariables.Text = "Confirm Data";
            this.fraVariables.UseMnemonic = false;
            // 
            // vsData
            // 
            this.vsData.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsData.Cols = 3;
            this.vsData.ColumnHeadersVisible = false;
            this.vsData.FixedRows = 0;
            this.vsData.Location = new System.Drawing.Point(0, 56);
            this.vsData.Name = "vsData";
            this.vsData.Rows = 1;
            this.vsData.Size = new System.Drawing.Size(546, 425);
            this.vsData.TabIndex = 2;
            this.vsData.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsData_BeforeEdit);
            this.vsData.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsData_ValidateEdit);
            this.vsData.CurrentCellChanged += new System.EventHandler(this.vsData_RowColChange);
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(204, 30);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(301, 20);
            this.lblName.TabIndex = 1;
            this.lblName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblYear
            // 
            this.lblYear.Location = new System.Drawing.Point(0, 30);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(130, 20);
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(196, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(156, 48);
            this.btnProcess.TabIndex = 2;
            this.btnProcess.Text = "Save & Continue";
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdFilePrintBlankForm
            // 
            this.cmdFilePrintBlankForm.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFilePrintBlankForm.Location = new System.Drawing.Point(458, 29);
            this.cmdFilePrintBlankForm.Name = "cmdFilePrintBlankForm";
            this.cmdFilePrintBlankForm.Size = new System.Drawing.Size(115, 24);
            this.cmdFilePrintBlankForm.TabIndex = 56;
            this.cmdFilePrintBlankForm.Text = "Print Blank Form";
            this.cmdFilePrintBlankForm.Click += new System.EventHandler(this.cmdFilePrintBlankForm_Click);
            // 
            // frmLienDischargeNotice
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(598, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmLienDischargeNotice";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Lien Discharge Notice";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmLienDischargeNotice_Load);
            this.Activated += new System.EventHandler(this.frmLienDischargeNotice_Activated);
            this.Resize += new System.EventHandler(this.frmLienDischargeNotice_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLienDischargeNotice_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraVariables)).EndInit();
            this.fraVariables.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrintBlankForm)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton btnProcess;
		public FCButton cmdFilePrintBlankForm;
	}
}
