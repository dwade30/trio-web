﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using fecherFoundation;

namespace Global
{
	/// <summary>
	/// Summary description for frmChooseCustomBillType.
	/// </summary>
	public partial class frmChooseCustomBillType : BaseForm
	{
		public frmChooseCustomBillType()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChooseCustomBillType InstancePtr
		{
			get
			{
				return (frmChooseCustomBillType)Sys.GetInstance(typeof(frmChooseCustomBillType));
			}
		}

		protected frmChooseCustomBillType _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey Gray
		// Date
		// 09/29/2004
		// ********************************************************
		const int CNSTGRIDBILLCOLID = 0;
		const int CNSTGRIDBILLCOLFORMAT = 1;
		const int CNSTGRIDBILLCOLHELPDESC = 2;
		int lngBillKey;

		private void frmChooseCustomBillType_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmChooseCustomBillType_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChooseCustomBillType.FillStyle	= 0;
			//frmChooseCustomBillType.ScaleWidth	= 5880;
			//frmChooseCustomBillType.ScaleHeight	= 4590;
			//frmChooseCustomBillType.LinkTopic	= "Form2";
			//frmChooseCustomBillType.LockControls	= true;
			//frmChooseCustomBillType.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		public int Init(string strMod, string strDB, string strTitle = "Choose Format")
		{
			int Init = 0;
			// returns the id number of the bill type picked.  Sends a negative number if canceled
			lngBillKey = -1;
			Init = -1;
			this.Text = strTitle;
			LoadGridBillType(ref strMod, ref strDB);
			this.Show(FormShowEnum.Modal, App.MainForm);
			Init = lngBillKey;
			return Init;
		}

		private object LoadGridBillType(ref string strMod, ref string strDB)
		{
			object LoadGridBillType = null;
			clsDRWrapper clsLoad = new clsDRWrapper();
			GridBillType.Rows = 1;
			GridBillType.Cols = 3;
			GridBillType.TextMatrix(0, CNSTGRIDBILLCOLFORMAT, "Format");
			GridBillType.TextMatrix(0, CNSTGRIDBILLCOLHELPDESC, "Description");
			GridBillType.ColHidden(0, true);
			clsLoad.OpenRecordset("select * from custombills order by formatname", strDB);
			while (!clsLoad.EndOfFile())
			{
				GridBillType.Rows += 1;
				GridBillType.TextMatrix(GridBillType.Rows - 1, CNSTGRIDBILLCOLID, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
				GridBillType.TextMatrix(GridBillType.Rows - 1, CNSTGRIDBILLCOLFORMAT, FCConvert.ToString(clsLoad.Get_Fields_String("FormatName")));
				GridBillType.TextMatrix(GridBillType.Rows - 1, CNSTGRIDBILLCOLHELPDESC, FCConvert.ToString(clsLoad.Get_Fields_String("Description")));
				clsLoad.MoveNext();
			}
			return LoadGridBillType;
		}

		private object ResizeGridBillType()
		{
			object ResizeGridBillType = null;
			int GridWidth = 0;
			GridWidth = GridBillType.WidthOriginal;
			GridBillType.ColWidth(CNSTGRIDBILLCOLFORMAT, FCConvert.ToInt32(0.5 * GridWidth));
			return ResizeGridBillType;
		}

		private void frmChooseCustomBillType_Resize(object sender, System.EventArgs e)
		{
			ResizeGridBillType();
		}

		private void GridBillType_DblClick(object sender, System.EventArgs e)
		{
			if (GridBillType.MouseRow < 1)
				return;
			lngBillKey = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBillType.TextMatrix(GridBillType.MouseRow, CNSTGRIDBILLCOLID))));
			Close();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (GridBillType.Row < 1)
			{
				MessageBox.Show("You must pick a format first", "No Format Picked", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			lngBillKey = FCConvert.ToInt32(Math.Round(Conversion.Val(GridBillType.TextMatrix(GridBillType.Row, CNSTGRIDBILLCOLID))));
			Close();
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSaveContinue_Click(sender, e);
		}
	}
}
