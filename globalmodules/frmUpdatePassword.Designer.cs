﻿//Fecher vbPorter - Version 1.0.0.27
using Wisej.Web;
using fecherFoundation;
using Global;

namespace Global
{
	/// <summary>
	/// Summary description for frmUpdatePassword.
	/// </summary>
	partial class frmUpdatePassword : BaseForm
	{
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCTextBox txtPassword2;
		public fecherFoundation.FCTextBox txtPassword1;
		public fecherFoundation.FCTextBox txtCheck;
		public fecherFoundation.FCLabel Label2_0;
		public fecherFoundation.FCLabel Label2_1;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdOK = new fecherFoundation.FCButton();
			this.txtPassword2 = new fecherFoundation.FCTextBox();
			this.txtPassword1 = new fecherFoundation.FCTextBox();
			this.txtCheck = new fecherFoundation.FCTextBox();
			this.Label2_0 = new fecherFoundation.FCLabel();
			this.Label2_1 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 359);
			this.BottomPanel.Size = new System.Drawing.Size(446, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.cmdOK);
			this.ClientArea.Controls.Add(this.txtPassword2);
			this.ClientArea.Controls.Add(this.txtPassword1);
			this.ClientArea.Controls.Add(this.txtCheck);
			this.ClientArea.Controls.Add(this.Label2_0);
			this.ClientArea.Controls.Add(this.Label2_1);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(446, 299);
			this.ClientArea.TabIndex = 0;
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(446, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(213, 30);
			this.HeaderText.Text = "Change Password";
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(129, 210);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(88, 40);
			this.cmdCancel.TabIndex = 7;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "actionButton";
			this.cmdOK.ForeColor = System.Drawing.Color.White;
			this.cmdOK.Location = new System.Drawing.Point(30, 210);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(78, 40);
			this.cmdOK.TabIndex = 6;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// txtPassword2
			// 
			this.txtPassword2.AutoSize = false;
			this.txtPassword2.BackColor = System.Drawing.SystemColors.Window;
			this.txtPassword2.LinkItem = null;
			this.txtPassword2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPassword2.LinkTopic = null;
			this.txtPassword2.Location = new System.Drawing.Point(220, 140);
			this.txtPassword2.Name = "txtPassword2";
			this.txtPassword2.Size = new System.Drawing.Size(126, 40);
			this.txtPassword2.TabIndex = 4;
			// 
			// txtPassword1
			// 
			this.txtPassword1.AutoSize = false;
			this.txtPassword1.BackColor = System.Drawing.SystemColors.Window;
			this.txtPassword1.LinkItem = null;
			this.txtPassword1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPassword1.LinkTopic = null;
			this.txtPassword1.Location = new System.Drawing.Point(220, 70);
			this.txtPassword1.Name = "txtPassword1";
			this.txtPassword1.Size = new System.Drawing.Size(126, 40);
			this.txtPassword1.TabIndex = 2;
			// 
			// txtCheck
			// 
			this.txtCheck.AutoSize = false;
			this.txtCheck.BackColor = System.Drawing.SystemColors.Window;
			this.txtCheck.LinkItem = null;
			this.txtCheck.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCheck.LinkTopic = null;
			this.txtCheck.Location = new System.Drawing.Point(351, 141);
			this.txtCheck.Name = "txtCheck";
			this.txtCheck.Size = new System.Drawing.Size(21, 40);
			this.txtCheck.TabIndex = 5;
			this.txtCheck.Visible = false;
			// 
			// Label2_0
			// 
			this.Label2_0.Location = new System.Drawing.Point(30, 154);
			this.Label2_0.Name = "Label2_0";
			this.Label2_0.Size = new System.Drawing.Size(135, 19);
			this.Label2_0.TabIndex = 3;
			this.Label2_0.Text = "CONFIRM PASSWORD";
			// 
			// Label2_1
			// 
			this.Label2_1.Location = new System.Drawing.Point(30, 84);
			this.Label2_1.Name = "Label2_1";
			this.Label2_1.Size = new System.Drawing.Size(106, 19);
			this.Label2_1.TabIndex = 1;
			this.Label2_1.Text = "PASSWORD";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(268, 17);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "YOU MUST TYPE YOUR PASSWORD TWICE";
            // 
            // frmUpdatePassword
            // 
            this.AcceptButton = this.cmdOK;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(446, 467);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmUpdatePassword";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Change Password";
			this.Load += new System.EventHandler(this.frmUpdatePassword_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUpdatePassword_KeyDown);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
