//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmGetMasterAccount.
	/// </summary>
	partial class frmGetMasterAccount : BaseForm
	{
		public fecherFoundation.FCComboBox cmbHidden;
		public fecherFoundation.FCLabel lblHidden;
		public FCGrid vsSearch;
		public fecherFoundation.FCPanel fraCriteria;
		public fecherFoundation.FCTextBox txtSearch2;
		public fecherFoundation.FCCheckBox chkShowPreviousOwnerInfo;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCLabel lblSearch2;
		public fecherFoundation.FCLabel lblSearch;
		public fecherFoundation.FCLabel lblSearchInfo;
		public fecherFoundation.FCTextBox txtGetAccountNumber;
		public fecherFoundation.FCButton cmdSelectAccount;
		public fecherFoundation.FCLabel lblLastAccount;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSelectAccount;
		public fecherFoundation.FCToolStripMenuItem mnuFileSearch;
		public fecherFoundation.FCToolStripMenuItem mnuFileClearSearch;
		public fecherFoundation.FCToolStripMenuItem mnuUndeleteMaster;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuQuit;
		public FCToolStripMenuItem mnuBatchStart;
		public FCToolStripMenuItem mnuBatchSave;
		public FCToolStripMenuItem mnuFileBatchSearch;
		public FCToolStripMenuItem mnuBatchPrint;
		public FCToolStripMenuItem mnuBatchRecover;
		public FCToolStripMenuItem mnuBatchPurge;
		public FCToolStripMenuItem mnuFileImportPayPortBatch;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.cmbHidden = new fecherFoundation.FCComboBox();
            this.lblHidden = new fecherFoundation.FCLabel();
            this.vsSearch = new fecherFoundation.FCGrid();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuBatchStart = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBatchSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileBatchSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBatchPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBatchRecover = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBatchPurge = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileImportPayPortBatch = new fecherFoundation.FCToolStripMenuItem();
            this.fraCriteria = new fecherFoundation.FCPanel();
            this.cmbSearchPlace = new fecherFoundation.FCComboBox();
            this.lblSearchPlace = new fecherFoundation.FCLabel();
            this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
            this.lblLastAccount = new fecherFoundation.FCLabel();
            this.txtSearch2 = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.chkShowPreviousOwnerInfo = new fecherFoundation.FCCheckBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.txtSearch = new fecherFoundation.FCTextBox();
            this.lblSearchInfo = new fecherFoundation.FCLabel();
            this.lblSearch2 = new fecherFoundation.FCLabel();
            this.lblSearch = new fecherFoundation.FCLabel();
            this.cmdSelectAccount = new fecherFoundation.FCButton();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSelectAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileClearSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuUndeleteMaster = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdUndeleteMaster = new fecherFoundation.FCButton();
            this.cmdFileClearSearch = new fecherFoundation.FCButton();
            this.cmdFileSearch = new fecherFoundation.FCButton();
            this.fraValidate = new fecherFoundation.FCFrame();
            this.lblValidate = new fecherFoundation.FCLabel();
            this.cmdValidateCancel = new fecherFoundation.FCButton();
            this.cmdValidateNo = new fecherFoundation.FCButton();
            this.cmdValidateYes = new fecherFoundation.FCButton();
            this.fraRecover = new fecherFoundation.FCFrame();
            this.lblRecover = new fecherFoundation.FCLabel();
            this.cmbBatchList = new fecherFoundation.FCComboBox();
            this.cmdBatchChoice = new fecherFoundation.FCButton();
            this.fraBatchQuestions = new fecherFoundation.FCFrame();
            this.cmdBatch = new fecherFoundation.FCButton();
            this.txtPaidBy = new fecherFoundation.FCTextBox();
            this.fcLabel3 = new fecherFoundation.FCLabel();
            this.lblEffectiveDate = new fecherFoundation.FCLabel();
            this.lblPaymentDate = new fecherFoundation.FCLabel();
            this.txtEffectiveDate = new Global.T2KDateBox();
            this.txtPaymentDate2 = new Global.T2KDateBox();
            this.txtPaymentDate = new fecherFoundation.FCTextBox();
            this.txtTellerID = new fecherFoundation.FCTextBox();
            this.label8 = new fecherFoundation.FCLabel();
            this.chkReceipt = new fecherFoundation.FCCheckBox();
            this.fraBatch = new fecherFoundation.FCFrame();
            this.Label11 = new fecherFoundation.FCLabel();
            this.txtTotal = new fecherFoundation.FCTextBox();
            this.cmdProcessBatch = new fecherFoundation.FCButton();
            this.vsBatch = new fecherFoundation.FCGrid();
            this.txtBillNumber = new fecherFoundation.FCTextBox();
            this.lblBillNumber = new fecherFoundation.FCLabel();
            this.cmbSewer = new fecherFoundation.FCComboBox();
            this.txtAmount = new fecherFoundation.FCTextBox();
            this.Label10 = new fecherFoundation.FCLabel();
            this.txtBatchAccount = new fecherFoundation.FCTextBox();
            this.Label9 = new fecherFoundation.FCLabel();
            this.lblLocation = new fecherFoundation.FCLabel();
            this.lblName = new fecherFoundation.FCLabel();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.cmdAutoPayPost = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraCriteria)).BeginInit();
            this.fraCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPreviousOwnerInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUndeleteMaster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileClearSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraValidate)).BeginInit();
            this.fraValidate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdValidateCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdValidateNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdValidateYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRecover)).BeginInit();
            this.fraRecover.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBatchChoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBatchQuestions)).BeginInit();
            this.fraBatchQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReceipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBatch)).BeginInit();
            this.fraBatch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAutoPayPost)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSelectAccount);
            this.BottomPanel.Location = new System.Drawing.Point(0, 620);
            this.BottomPanel.Size = new System.Drawing.Size(1048, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraBatchQuestions);
            this.ClientArea.Controls.Add(this.fraRecover);
            this.ClientArea.Controls.Add(this.fraBatch);
            this.ClientArea.Controls.Add(this.fraValidate);
            this.ClientArea.Controls.Add(this.vsSearch);
            this.ClientArea.Controls.Add(this.fraCriteria);
            this.ClientArea.Size = new System.Drawing.Size(1068, 635);
            this.ClientArea.Controls.SetChildIndex(this.fraCriteria, 0);
            this.ClientArea.Controls.SetChildIndex(this.vsSearch, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraValidate, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraBatch, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraRecover, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraBatchQuestions, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdAutoPayPost);
            this.TopPanel.Controls.Add(this.cmdFileSearch);
            this.TopPanel.Controls.Add(this.cmdFileClearSearch);
            this.TopPanel.Controls.Add(this.cmdUndeleteMaster);
            this.TopPanel.Size = new System.Drawing.Size(1068, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdUndeleteMaster, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileClearSearch, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileSearch, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAutoPayPost, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(258, 30);
            this.HeaderText.Text = "Select Master Account";
            // 
            // cmbHidden
            // 
            this.cmbHidden.Items.AddRange(new object[] {
            "Name",
            "Mailing Address",
            "Location",
            "Map / Lot",
            "RE Acct Number",
            "Serial #",
            "Remote Serial #",
            "Ref 1"});
            this.cmbHidden.Location = new System.Drawing.Point(385, 30);
            this.cmbHidden.Name = "cmbHidden";
            this.cmbHidden.Size = new System.Drawing.Size(183, 40);
            this.cmbHidden.TabIndex = 5;
            this.cmbHidden.Text = "Name";
            this.cmbHidden.TextChanged += new System.EventHandler(this.optSearchType_CheckedChanged);
            // 
            // lblHidden
            // 
            this.lblHidden.AutoSize = true;
            this.lblHidden.Location = new System.Drawing.Point(282, 44);
            this.lblHidden.Name = "lblHidden";
            this.lblHidden.Size = new System.Drawing.Size(87, 17);
            this.lblHidden.TabIndex = 12;
            this.lblHidden.Text = "SEARCH BY";
            // 
            // vsSearch
            // 
            this.vsSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsSearch.Cols = 5;
            this.vsSearch.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShow;
            this.vsSearch.FixedCols = 0;
            this.vsSearch.Location = new System.Drawing.Point(30, 376);
            this.vsSearch.Name = "vsSearch";
            this.vsSearch.RowHeadersVisible = false;
            this.vsSearch.Rows = 1;
            this.vsSearch.ShowFocusCell = false;
            this.vsSearch.Size = new System.Drawing.Size(996, 244);
            this.vsSearch.TabIndex = 1;
            this.vsSearch.Visible = false;
            this.vsSearch.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsSearch_MouseMoveEvent);
            this.vsSearch.Click += new System.EventHandler(this.vsSearch_Click);
            this.vsSearch.DoubleClick += new System.EventHandler(this.vsSearch_DblClick);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuBatchStart,
            this.mnuBatchSave,
            this.mnuFileBatchSearch,
            this.mnuBatchPrint,
            this.mnuBatchRecover,
            this.mnuBatchPurge,
            this.mnuFileImportPayPortBatch});
            this.MainMenu1.Name = null;
            // 
            // mnuBatchStart
            // 
            this.mnuBatchStart.Index = 0;
            this.mnuBatchStart.Name = "mnuBatchStart";
            this.mnuBatchStart.Text = "Start Batch Update";
            this.mnuBatchStart.Click += new System.EventHandler(this.mnuBatchStart_Click);
            // 
            // mnuBatchSave
            // 
            this.mnuBatchSave.Index = 1;
            this.mnuBatchSave.Name = "mnuBatchSave";
            this.mnuBatchSave.Text = "Save Batch Update";
            this.mnuBatchSave.Click += new System.EventHandler(this.mnuBatchSave_Click);
            // 
            // mnuFileBatchSearch
            // 
            this.mnuFileBatchSearch.Index = 2;
            this.mnuFileBatchSearch.Name = "mnuFileBatchSearch";
            this.mnuFileBatchSearch.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuFileBatchSearch.Text = "Account Search";
            this.mnuFileBatchSearch.Click += new System.EventHandler(this.mnuFileBatchSearch_Click);
            // 
            // mnuBatchPrint
            // 
            this.mnuBatchPrint.Index = 3;
            this.mnuBatchPrint.Name = "mnuBatchPrint";
            this.mnuBatchPrint.Text = "Print Current Batch Listing";
            this.mnuBatchPrint.Click += new System.EventHandler(this.mnuBatchPrint_Click);
            // 
            // mnuBatchRecover
            // 
            this.mnuBatchRecover.Index = 4;
            this.mnuBatchRecover.Name = "mnuBatchRecover";
            this.mnuBatchRecover.Text = "Recover Batch";
            this.mnuBatchRecover.Click += new System.EventHandler(this.mnuBatchRecover_Click);
            // 
            // mnuBatchPurge
            // 
            this.mnuBatchPurge.Index = 5;
            this.mnuBatchPurge.Name = "mnuBatchPurge";
            this.mnuBatchPurge.Text = "Purge Batch";
            this.mnuBatchPurge.Click += new System.EventHandler(this.mnuBatchPurge_Click);
            // 
            // mnuFileImportPayPortBatch
            // 
            this.mnuFileImportPayPortBatch.Index = 6;
            this.mnuFileImportPayPortBatch.Name = "mnuFileImportPayPortBatch";
            this.mnuFileImportPayPortBatch.Text = "Import PayPort Batch";
            this.mnuFileImportPayPortBatch.Click += new System.EventHandler(this.mnuFileImportPayPortBatch_Click);
            // 
            // fraCriteria
            // 
            this.fraCriteria.AppearanceKey = "groupBoxNoBorders";
            this.fraCriteria.Controls.Add(this.cmbSearchPlace);
            this.fraCriteria.Controls.Add(this.lblSearchPlace);
            this.fraCriteria.Controls.Add(this.txtGetAccountNumber);
            this.fraCriteria.Controls.Add(this.cmbHidden);
            this.fraCriteria.Controls.Add(this.lblLastAccount);
            this.fraCriteria.Controls.Add(this.lblHidden);
            this.fraCriteria.Controls.Add(this.txtSearch2);
            this.fraCriteria.Controls.Add(this.Label2);
            this.fraCriteria.Controls.Add(this.chkShowPreviousOwnerInfo);
            this.fraCriteria.Controls.Add(this.Label1);
            this.fraCriteria.Controls.Add(this.txtSearch);
            this.fraCriteria.Controls.Add(this.lblSearchInfo);
            this.fraCriteria.Controls.Add(this.lblSearch2);
            this.fraCriteria.Controls.Add(this.lblSearch);
            this.fraCriteria.Name = "fraCriteria";
            this.fraCriteria.Size = new System.Drawing.Size(1010, 90);
            this.fraCriteria.TabIndex = 6;
            // 
            // cmbSearchPlace
            // 
            this.cmbSearchPlace.Items.AddRange(new object[] {
            "Starts With",
            "Contains"});
            this.cmbSearchPlace.Location = new System.Drawing.Point(810, 30);
            this.cmbSearchPlace.Name = "cmbSearchPlace";
            this.cmbSearchPlace.Size = new System.Drawing.Size(183, 40);
            this.cmbSearchPlace.TabIndex = 11;
            this.cmbSearchPlace.Text = "Contains";
            // 
            // lblSearchPlace
            // 
            this.lblSearchPlace.AutoSize = true;
            this.lblSearchPlace.Location = new System.Drawing.Point(462, 150);
            this.lblSearchPlace.Name = "lblSearchPlace";
            this.lblSearchPlace.Size = new System.Drawing.Size(113, 17);
            this.lblSearchPlace.TabIndex = 2;
            this.lblSearchPlace.Text = "SEARCH PLACE";
            this.lblSearchPlace.Visible = false;
            // 
            // txtGetAccountNumber
            // 
            this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtGetAccountNumber.Location = new System.Drawing.Point(122, 30);
            this.txtGetAccountNumber.MaxLength = 9;
            this.txtGetAccountNumber.Name = "txtGetAccountNumber";
            this.txtGetAccountNumber.Size = new System.Drawing.Size(126, 40);
            this.txtGetAccountNumber.TabIndex = 3;
            this.txtGetAccountNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.txtGetAccountNumber_KeyDown);
            this.txtGetAccountNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtGetAccountNumber_KeyPress);
            // 
            // lblLastAccount
            // 
            this.lblLastAccount.Location = new System.Drawing.Point(334, 30);
            this.lblLastAccount.Name = "lblLastAccount";
            this.lblLastAccount.Size = new System.Drawing.Size(58, 20);
            this.lblLastAccount.TabIndex = 1;
            this.lblLastAccount.Visible = false;
            // 
            // txtSearch2
            // 
            this.txtSearch2.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearch2.Location = new System.Drawing.Point(629, 29);
            this.txtSearch2.MaxLength = 9;
            this.txtSearch2.Name = "txtSearch2";
            this.txtSearch2.Size = new System.Drawing.Size(90, 40);
            this.txtSearch2.TabIndex = 7;
            this.txtSearch2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtSearch2.Visible = false;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 44);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(66, 19);
            this.Label2.TabIndex = 13;
            this.Label2.Text = "ACCOUNT";
            // 
            // chkShowPreviousOwnerInfo
            // 
            this.chkShowPreviousOwnerInfo.Checked = true;
            this.chkShowPreviousOwnerInfo.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkShowPreviousOwnerInfo.Location = new System.Drawing.Point(1042, 34);
            this.chkShowPreviousOwnerInfo.Name = "chkShowPreviousOwnerInfo";
            this.chkShowPreviousOwnerInfo.Size = new System.Drawing.Size(175, 24);
            this.chkShowPreviousOwnerInfo.TabIndex = 11;
            this.chkShowPreviousOwnerInfo.Text = "Show Previous Owners";
            this.chkShowPreviousOwnerInfo.Visible = false;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 31);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(980, 15);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "PLEASE ENTER THE ACCOUNT NUMBER.  HIT <ENTER> TO VIEW THE LAST ACCOUNT ACCESSED. " +
    " ENTER \'0\' TO ADD A NEW ACCOUNT";
            this.Label1.Visible = false;
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearch.Location = new System.Drawing.Point(598, 30);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(193, 40);
            this.txtSearch.TabIndex = 8;
            this.txtSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearch_KeyDown);
            this.txtSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSearch_KeyPress);
            // 
            // lblSearchInfo
            // 
            this.lblSearchInfo.AutoSize = true;
            this.lblSearchInfo.Location = new System.Drawing.Point(685, 34);
            this.lblSearchInfo.Name = "lblSearchInfo";
            this.lblSearchInfo.Size = new System.Drawing.Size(256, 17);
            this.lblSearchInfo.TabIndex = 4;
            this.lblSearchInfo.Text = "ENTER THE CRITERIA TO SEARCH BY";
            this.lblSearchInfo.Visible = false;
            // 
            // lblSearch2
            // 
            this.lblSearch2.AutoSize = true;
            this.lblSearch2.Location = new System.Drawing.Point(602, 44);
            this.lblSearch2.Name = "lblSearch2";
            this.lblSearch2.Size = new System.Drawing.Size(15, 17);
            this.lblSearch2.TabIndex = 5;
            this.lblSearch2.Text = "#";
            this.lblSearch2.Visible = false;
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.Location = new System.Drawing.Point(706, 42);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(104, 17);
            this.lblSearch.TabIndex = 6;
            this.lblSearch.Text = "STREET NAME";
            this.lblSearch.Visible = false;
            // 
            // cmdSelectAccount
            // 
            this.cmdSelectAccount.AppearanceKey = "acceptButton";
            this.cmdSelectAccount.ForeColor = System.Drawing.Color.White;
            this.cmdSelectAccount.Location = new System.Drawing.Point(271, 30);
            this.cmdSelectAccount.Name = "cmdSelectAccount";
            this.cmdSelectAccount.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSelectAccount.Size = new System.Drawing.Size(170, 48);
            this.cmdSelectAccount.TabIndex = 5;
            this.cmdSelectAccount.Text = "Select Account";
            this.cmdSelectAccount.Click += new System.EventHandler(this.mnuSelectAccount_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSelectAccount,
            this.mnuFileSearch,
            this.mnuFileClearSearch,
            this.mnuUndeleteMaster,
            this.Seperator,
            this.mnuQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSelectAccount
            // 
            this.mnuSelectAccount.Index = 0;
            this.mnuSelectAccount.Name = "mnuSelectAccount";
            this.mnuSelectAccount.Text = "Select Account";
            this.mnuSelectAccount.Click += new System.EventHandler(this.mnuSelectAccount_Click);
            // 
            // mnuFileSearch
            // 
            this.mnuFileSearch.Index = 1;
            this.mnuFileSearch.Name = "mnuFileSearch";
            this.mnuFileSearch.Text = "Search";
            this.mnuFileSearch.Click += new System.EventHandler(this.mnuFileSearch_Click);
            // 
            // mnuFileClearSearch
            // 
            this.mnuFileClearSearch.Index = 2;
            this.mnuFileClearSearch.Name = "mnuFileClearSearch";
            this.mnuFileClearSearch.Text = "Clear Search";
            this.mnuFileClearSearch.Click += new System.EventHandler(this.mnuFileClearSearch_Click);
            // 
            // mnuUndeleteMaster
            // 
            this.mnuUndeleteMaster.Index = 3;
            this.mnuUndeleteMaster.Name = "mnuUndeleteMaster";
            this.mnuUndeleteMaster.Text = "Undelete Master Account";
            this.mnuUndeleteMaster.Click += new System.EventHandler(this.mnuUndeleteMaster_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 4;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuQuit
            // 
            this.mnuQuit.Index = 5;
            this.mnuQuit.Name = "mnuQuit";
            this.mnuQuit.Text = "Exit";
            this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
            // 
            // cmdUndeleteMaster
            // 
            this.cmdUndeleteMaster.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdUndeleteMaster.Location = new System.Drawing.Point(769, 29);
            this.cmdUndeleteMaster.Name = "cmdUndeleteMaster";
            this.cmdUndeleteMaster.Size = new System.Drawing.Size(177, 24);
            this.cmdUndeleteMaster.TabIndex = 1;
            this.cmdUndeleteMaster.Text = "Undelete Master Account";
            this.cmdUndeleteMaster.Click += new System.EventHandler(this.mnuUndeleteMaster_Click);
            // 
            // cmdFileClearSearch
            // 
            this.cmdFileClearSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileClearSearch.Location = new System.Drawing.Point(662, 29);
            this.cmdFileClearSearch.Name = "cmdFileClearSearch";
            this.cmdFileClearSearch.Size = new System.Drawing.Size(101, 24);
            this.cmdFileClearSearch.TabIndex = 2;
            this.cmdFileClearSearch.Text = "Clear Search";
            this.cmdFileClearSearch.Click += new System.EventHandler(this.mnuFileClearSearch_Click);
            // 
            // cmdFileSearch
            // 
            this.cmdFileSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileSearch.ImageSource = "button-search";
            this.cmdFileSearch.Location = new System.Drawing.Point(592, 29);
            this.cmdFileSearch.Name = "cmdFileSearch";
            this.cmdFileSearch.Size = new System.Drawing.Size(81, 24);
            this.cmdFileSearch.TabIndex = 3;
            this.cmdFileSearch.Text = "Search";
            this.cmdFileSearch.Click += new System.EventHandler(this.mnuFileSearch_Click);
            // 
            // fraValidate
            // 
            this.fraValidate.AppearanceKey = "groupBoxNoBorders";
            this.fraValidate.BackColor = System.Drawing.Color.White;
            this.fraValidate.Controls.Add(this.lblValidate);
            this.fraValidate.Controls.Add(this.cmdValidateCancel);
            this.fraValidate.Controls.Add(this.cmdValidateNo);
            this.fraValidate.Controls.Add(this.cmdValidateYes);
            this.fraValidate.Location = new System.Drawing.Point(30, 130);
            this.fraValidate.Name = "fraValidate";
            this.fraValidate.Size = new System.Drawing.Size(280, 180);
            this.fraValidate.TabIndex = 2;
            this.fraValidate.Visible = false;
            // 
            // lblValidate
            // 
            this.lblValidate.Location = new System.Drawing.Point(20, 30);
            this.lblValidate.Name = "lblValidate";
            this.lblValidate.Size = new System.Drawing.Size(237, 72);
            this.lblValidate.TabIndex = 3;
            // 
            // cmdValidateCancel
            // 
            this.cmdValidateCancel.AppearanceKey = "actionButton";
            this.cmdValidateCancel.Location = new System.Drawing.Point(177, 121);
            this.cmdValidateCancel.Name = "cmdValidateCancel";
            this.cmdValidateCancel.Size = new System.Drawing.Size(80, 40);
            this.cmdValidateCancel.TabIndex = 2;
            this.cmdValidateCancel.Text = "Cancel";
            this.cmdValidateCancel.Click += new System.EventHandler(this.cmdValidateCancel_Click);
            // 
            // cmdValidateNo
            // 
            this.cmdValidateNo.AppearanceKey = "actionButton";
            this.cmdValidateNo.Location = new System.Drawing.Point(99, 121);
            this.cmdValidateNo.Name = "cmdValidateNo";
            this.cmdValidateNo.Size = new System.Drawing.Size(60, 40);
            this.cmdValidateNo.TabIndex = 1;
            this.cmdValidateNo.Text = "No";
            this.cmdValidateNo.Click += new System.EventHandler(this.cmdValidateNo_Click);
            // 
            // cmdValidateYes
            // 
            this.cmdValidateYes.AppearanceKey = "actionButton";
            this.cmdValidateYes.Location = new System.Drawing.Point(20, 121);
            this.cmdValidateYes.Name = "cmdValidateYes";
            this.cmdValidateYes.Size = new System.Drawing.Size(60, 40);
            this.cmdValidateYes.TabIndex = 4;
            this.cmdValidateYes.Text = "Yes";
            this.cmdValidateYes.Click += new System.EventHandler(this.cmdValidateYes_Click);
            // 
            // fraRecover
            // 
            this.fraRecover.Controls.Add(this.lblRecover);
            this.fraRecover.Controls.Add(this.cmbBatchList);
            this.fraRecover.Controls.Add(this.cmdBatchChoice);
            this.fraRecover.Name = "fraRecover";
            this.fraRecover.Size = new System.Drawing.Size(392, 176);
            this.fraRecover.TabIndex = 3;
            this.fraRecover.Text = "Batch Recover";
            this.fraRecover.Visible = false;
            // 
            // lblRecover
            // 
            this.lblRecover.Location = new System.Drawing.Point(20, 35);
            this.lblRecover.Name = "lblRecover";
            this.lblRecover.Size = new System.Drawing.Size(203, 68);
            this.lblRecover.TabIndex = 2;
            // 
            // cmbBatchList
            // 
            this.cmbBatchList.Location = new System.Drawing.Point(241, 35);
            this.cmbBatchList.Name = "cmbBatchList";
            this.cmbBatchList.TabIndex = 1;
            this.cmbBatchList.DropDown += new System.EventHandler(this.cmbBatchList_DropDown);
            this.cmbBatchList.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbBatchList_KeyDown);
            // 
            // cmdBatchChoice
            // 
            this.cmdBatchChoice.AppearanceKey = "actionButton";
            this.cmdBatchChoice.Location = new System.Drawing.Point(151, 117);
            this.cmdBatchChoice.Name = "cmdBatchChoice";
            this.cmdBatchChoice.Size = new System.Drawing.Size(90, 40);
            this.cmdBatchChoice.TabIndex = 3;
            this.cmdBatchChoice.Text = "Purge";
            this.cmdBatchChoice.Click += new System.EventHandler(this.cmdBatchChoice_Click);
            // 
            // fraBatchQuestions
            // 
            this.fraBatchQuestions.Controls.Add(this.cmdBatch);
            this.fraBatchQuestions.Controls.Add(this.txtPaidBy);
            this.fraBatchQuestions.Controls.Add(this.fcLabel3);
            this.fraBatchQuestions.Controls.Add(this.lblEffectiveDate);
            this.fraBatchQuestions.Controls.Add(this.lblPaymentDate);
            this.fraBatchQuestions.Controls.Add(this.txtEffectiveDate);
            this.fraBatchQuestions.Controls.Add(this.txtPaymentDate2);
            this.fraBatchQuestions.Controls.Add(this.txtPaymentDate);
            this.fraBatchQuestions.Controls.Add(this.txtTellerID);
            this.fraBatchQuestions.Controls.Add(this.label8);
            this.fraBatchQuestions.Controls.Add(this.chkReceipt);
            this.fraBatchQuestions.Location = new System.Drawing.Point(10, 30);
            this.fraBatchQuestions.Name = "fraBatchQuestions";
            this.fraBatchQuestions.Size = new System.Drawing.Size(567, 330);
            this.fraBatchQuestions.TabIndex = 4;
            this.fraBatchQuestions.Text = "Batch Update Information";
            this.fraBatchQuestions.Visible = false;
            // 
            // cmdBatch
            // 
            this.cmdBatch.AppearanceKey = "actionButton";
            this.cmdBatch.Location = new System.Drawing.Point(270, 267);
            this.cmdBatch.Name = "cmdBatch";
            this.cmdBatch.Size = new System.Drawing.Size(80, 40);
            this.cmdBatch.TabIndex = 10;
            this.cmdBatch.Text = "Next";
            this.cmdBatch.Click += new System.EventHandler(this.cmdBatch_Click);
            // 
            // txtPaidBy
            // 
            this.txtPaidBy.Location = new System.Drawing.Point(270, 217);
            this.txtPaidBy.MaxLength = 25;
            this.txtPaidBy.Name = "txtPaidBy";
            this.txtPaidBy.Size = new System.Drawing.Size(135, 40);
            this.txtPaidBy.TabIndex = 9;
            this.txtPaidBy.Enter += new System.EventHandler(this.txtPaidBy_Enter);
            this.txtPaidBy.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPaidBy_KeyDown);
            // 
            // fcLabel3
            // 
            this.fcLabel3.Location = new System.Drawing.Point(20, 231);
            this.fcLabel3.Name = "fcLabel3";
            this.fcLabel3.Size = new System.Drawing.Size(70, 15);
            this.fcLabel3.TabIndex = 8;
            this.fcLabel3.Text = "PAID BY";
            // 
            // lblEffectiveDate
            // 
            this.lblEffectiveDate.AutoSize = true;
            this.lblEffectiveDate.Location = new System.Drawing.Point(20, 181);
            this.lblEffectiveDate.Name = "lblEffectiveDate";
            this.lblEffectiveDate.Size = new System.Drawing.Size(219, 17);
            this.lblEffectiveDate.TabIndex = 7;
            this.lblEffectiveDate.Text = "EFFECTIVE DATE (MM/DD/YYYY)";
            // 
            // lblPaymentDate
            // 
            this.lblPaymentDate.AutoSize = true;
            this.lblPaymentDate.Location = new System.Drawing.Point(20, 131);
            this.lblPaymentDate.Name = "lblPaymentDate";
            this.lblPaymentDate.Size = new System.Drawing.Size(211, 17);
            this.lblPaymentDate.TabIndex = 6;
            this.lblPaymentDate.Text = "PAYMENT DATE (MM/DD/YYYY)";
            // 
            // txtEffectiveDate
            // 
            this.txtEffectiveDate.Location = new System.Drawing.Point(270, 167);
            this.txtEffectiveDate.Mask = "##/##/####";
            this.txtEffectiveDate.MaxLength = 10;
            this.txtEffectiveDate.Name = "txtEffectiveDate";
            this.txtEffectiveDate.Size = new System.Drawing.Size(135, 22);
            this.txtEffectiveDate.TabIndex = 5;
            this.txtEffectiveDate.GotFocus += new System.EventHandler(this.txtEffectiveDate_GotFocus);
            this.txtEffectiveDate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtEffectiveDate_KeyDown);
            this.txtEffectiveDate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtEffectiveDate_KeyPress);
            // 
            // txtPaymentDate2
            // 
            this.txtPaymentDate2.Location = new System.Drawing.Point(270, 117);
            this.txtPaymentDate2.Mask = "##/##/####";
            this.txtPaymentDate2.MaxLength = 10;
            this.txtPaymentDate2.Name = "txtPaymentDate2";
            this.txtPaymentDate2.Size = new System.Drawing.Size(135, 22);
            this.txtPaymentDate2.TabIndex = 4;
            this.txtPaymentDate2.GotFocus += new System.EventHandler(this.txtPaymentDate2_GotFocus);
            this.txtPaymentDate2.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPaymentDate2_KeyDown);
            this.txtPaymentDate2.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPaymentDate2_KeyPress);
            // 
            // txtPaymentDate
            // 
            this.txtPaymentDate.Location = new System.Drawing.Point(426, 67);
            this.txtPaymentDate.MaxLength = 10;
            this.txtPaymentDate.Name = "txtPaymentDate";
            this.txtPaymentDate.Size = new System.Drawing.Size(121, 40);
            this.txtPaymentDate.TabIndex = 3;
            this.txtPaymentDate.Visible = false;
            this.txtPaymentDate.Enter += new System.EventHandler(this.txtPaymentDate_Enter);
            this.txtPaymentDate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPaymentDate_KeyDown);
            this.txtPaymentDate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPaymentDate_KeyPress);
            // 
            // txtTellerID
            // 
            this.txtTellerID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtTellerID.Location = new System.Drawing.Point(270, 67);
            this.txtTellerID.MaxLength = 3;
            this.txtTellerID.Name = "txtTellerID";
            this.txtTellerID.Size = new System.Drawing.Size(135, 40);
            this.txtTellerID.TabIndex = 2;
            this.txtTellerID.Enter += new System.EventHandler(this.txtTellerID_Enter);
            this.txtTellerID.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTellerID_KeyDown);
            this.txtTellerID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTellerID_KeyPress);
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(20, 81);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 15);
            this.label8.TabIndex = 1;
            this.label8.Text = "TELLER ID";
            // 
            // chkReceipt
            // 
            this.chkReceipt.Location = new System.Drawing.Point(20, 30);
            this.chkReceipt.Name = "chkReceipt";
            this.chkReceipt.Size = new System.Drawing.Size(220, 24);
            this.chkReceipt.TabIndex = 11;
            this.chkReceipt.Text = "Default Receipt Option to \'Yes\'";
            // 
            // fraBatch
            // 
            this.fraBatch.AppearanceKey = "groupBoxNoBorders";
            this.fraBatch.Controls.Add(this.Label11);
            this.fraBatch.Controls.Add(this.txtTotal);
            this.fraBatch.Controls.Add(this.cmdProcessBatch);
            this.fraBatch.Controls.Add(this.vsBatch);
            this.fraBatch.Controls.Add(this.txtBillNumber);
            this.fraBatch.Controls.Add(this.lblBillNumber);
            this.fraBatch.Controls.Add(this.cmbSewer);
            this.fraBatch.Controls.Add(this.txtAmount);
            this.fraBatch.Controls.Add(this.Label10);
            this.fraBatch.Controls.Add(this.txtBatchAccount);
            this.fraBatch.Controls.Add(this.Label9);
            this.fraBatch.Controls.Add(this.lblLocation);
            this.fraBatch.Controls.Add(this.lblName);
            this.fraBatch.Controls.Add(this.lblInstructions);
            this.fraBatch.Location = new System.Drawing.Point(30, 130);
            this.fraBatch.Name = "fraBatch";
            this.fraBatch.Size = new System.Drawing.Size(1000, 380);
            this.fraBatch.TabIndex = 5;
            this.fraBatch.Visible = false;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Location = new System.Drawing.Point(613, 334);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(52, 17);
            this.Label11.TabIndex = 13;
            this.Label11.Text = "TOTAL";
            // 
            // txtTotal
            // 
            this.txtTotal.Location = new System.Drawing.Point(683, 322);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(120, 40);
            this.txtTotal.TabIndex = 12;
            // 
            // cmdProcessBatch
            // 
            this.cmdProcessBatch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdProcessBatch.AppearanceKey = "actionButton";
            this.cmdProcessBatch.Location = new System.Drawing.Point(20, 322);
            this.cmdProcessBatch.Name = "cmdProcessBatch";
            this.cmdProcessBatch.Size = new System.Drawing.Size(137, 40);
            this.cmdProcessBatch.TabIndex = 11;
            this.cmdProcessBatch.Text = "Process Batch";
            this.cmdProcessBatch.Click += new System.EventHandler(this.cmdProcessBatch_Click);
            // 
            // vsBatch
            // 
            this.vsBatch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsBatch.Cols = 10;
            this.vsBatch.FixedCols = 0;
            this.vsBatch.Location = new System.Drawing.Point(20, 174);
            this.vsBatch.Name = "vsBatch";
            this.vsBatch.RowHeadersVisible = false;
            this.vsBatch.Rows = 1;
            this.vsBatch.Size = new System.Drawing.Size(960, 125);
            this.vsBatch.TabIndex = 10;
            this.vsBatch.BeforeRowColChange += new System.EventHandler<fecherFoundation.BeforeRowColChangeEventArgs>(this.vsBatch_RowColChange);
            this.vsBatch.Click += new System.EventHandler(this.vsBatch_Click);
            this.vsBatch.KeyDown += new Wisej.Web.KeyEventHandler(this.vsBatch_KeyDown);
            // 
            // txtBillNumber
            // 
            this.txtBillNumber.Location = new System.Drawing.Point(704, 105);
            this.txtBillNumber.Name = "txtBillNumber";
            this.txtBillNumber.Size = new System.Drawing.Size(99, 40);
            this.txtBillNumber.TabIndex = 9;
            // 
            // lblBillNumber
            // 
            this.lblBillNumber.AutoSize = true;
            this.lblBillNumber.Location = new System.Drawing.Point(594, 119);
            this.lblBillNumber.Name = "lblBillNumber";
            this.lblBillNumber.Size = new System.Drawing.Size(100, 17);
            this.lblBillNumber.TabIndex = 8;
            this.lblBillNumber.Text = "BILL NUMBER";
            // 
            // cmbSewer
            // 
            this.cmbSewer.Items.AddRange(new object[] {
            "Water",
            "Sewer"});
            this.cmbSewer.Location = new System.Drawing.Point(454, 105);
            this.cmbSewer.Name = "cmbSewer";
            this.cmbSewer.TabIndex = 7;
            this.cmbSewer.Text = "Water";
            // 
            // txtAmount
            // 
            this.txtAmount.Location = new System.Drawing.Point(313, 105);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(120, 40);
            this.txtAmount.TabIndex = 6;
            this.txtAmount.Enter += new System.EventHandler(this.txtAmount_Enter);
            this.txtAmount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAmount_KeyDown);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(227, 119);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(65, 15);
            this.Label10.TabIndex = 5;
            this.Label10.Text = "AMOUNT";
            // 
            // txtBatchAccount
            // 
            this.txtBatchAccount.Location = new System.Drawing.Point(114, 105);
            this.txtBatchAccount.Name = "txtBatchAccount";
            this.txtBatchAccount.Size = new System.Drawing.Size(90, 40);
            this.txtBatchAccount.TabIndex = 4;
            this.txtBatchAccount.Enter += new System.EventHandler(this.txtBatchAccount_Enter);
            this.txtBatchAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtBatchAccount_KeyDown);
            this.txtBatchAccount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtBatchAccount_KeyPress);
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 119);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(65, 15);
            this.Label9.TabIndex = 3;
            this.Label9.Text = "ACCOUNT";
            // 
            // lblLocation
            // 
            this.lblLocation.Location = new System.Drawing.Point(340, 80);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(300, 15);
            this.lblLocation.TabIndex = 2;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(20, 80);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(300, 15);
            this.lblName.TabIndex = 1;
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(20, 30);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(570, 30);
            this.lblInstructions.TabIndex = 14;
            // 
            // cmdAutoPayPost
            // 
            this.cmdAutoPayPost.Location = new System.Drawing.Point(1219, 29);
            this.cmdAutoPayPost.Name = "cmdAutoPayPost";
            this.cmdAutoPayPost.Size = new System.Drawing.Size(105, 24);
            this.cmdAutoPayPost.TabIndex = 4;
            this.cmdAutoPayPost.Text = "Post Auto-Pay";
            this.cmdAutoPayPost.Visible = false;
            this.cmdAutoPayPost.Click += new System.EventHandler(this.mnuAutoPayPost_Click);
            // 
            // frmGetMasterAccount
            // 
            this.ClientSize = new System.Drawing.Size(1068, 695);
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmGetMasterAccount";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Select Master Account";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmGetMasterAccount_Load);
            this.Activated += new System.EventHandler(this.frmGetMasterAccount_Activated);
            this.Resize += new System.EventHandler(this.frmGetMasterAccount_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetMasterAccount_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraCriteria)).EndInit();
            this.fraCriteria.ResumeLayout(false);
            this.fraCriteria.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPreviousOwnerInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdUndeleteMaster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileClearSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraValidate)).EndInit();
            this.fraValidate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdValidateCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdValidateNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdValidateYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRecover)).EndInit();
            this.fraRecover.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdBatchChoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBatchQuestions)).EndInit();
            this.fraBatchQuestions.ResumeLayout(false);
            this.fraBatchQuestions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReceipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBatch)).EndInit();
            this.fraBatch.ResumeLayout(false);
            this.fraBatch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAutoPayPost)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdUndeleteMaster;
		private FCButton cmdFileClearSearch;
		private FCButton cmdFileSearch;
		public FCComboBox cmbSearchPlace;
		public FCLabel lblSearchPlace;
		private FCFrame fraValidate;
		private FCLabel lblValidate;
		private FCButton cmdValidateCancel;
		private FCButton cmdValidateNo;
		private FCButton cmdValidateYes;
		private FCFrame fraRecover;
		private FCButton cmdBatchChoice;
		private FCFrame fraBatchQuestions;
		private FCLabel lblRecover;
		private FCComboBox cmbBatchList;
		private FCTextBox txtPaymentDate;
		public FCTextBox txtTellerID;
		private FCLabel label8;
		private FCCheckBox chkReceipt;
		private T2KDateBox txtEffectiveDate;
		public T2KDateBox txtPaymentDate2;
		public FCTextBox txtPaidBy;
		private FCLabel fcLabel3;
		private FCLabel lblEffectiveDate;
		private FCLabel lblPaymentDate;
		private FCButton cmdBatch;
		private FCFrame fraBatch;
		private FCLabel lblInstructions;
		private FCLabel lblName;
		private FCLabel Label9;
		private FCLabel lblLocation;
		private FCComboBox cmbSewer;
		private FCTextBox txtAmount;
		private FCLabel Label10;
		private FCTextBox txtBatchAccount;
		private FCTextBox txtBillNumber;
		private FCLabel lblBillNumber;
		private FCGrid vsBatch;
		public FCTextBox txtTotal;
		private FCButton cmdProcessBatch;
		private FCLabel Label11;
		private FCButton cmdAutoPayPost;
	}
}
