﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cGESecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cGESecuritySetup() : base()
		{
			strThisModule = "GE";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem("Calendar", "", 34, true, ""));
			AddItem_2(CreateItem("Calendar", "Add / Edit Tasks All Users", 37, false, ""));
			AddItem_2(CreateItem("Calendar", "Add / Edit Tasks User Only", 35, false, ""));
			AddItem_2(CreateItem("Calendar", "View Calendar", 36, false, ""));
			AddItem_2(CreateItem("Entry into Accounts Receivable", "", 38, false, ""));
			AddItem_2(CreateItem("Entry into Billing", "", 7, false, ""));
			AddItem_2(CreateItem("Entry into Budgetary System", "", 13, false, ""));
			AddItem_2(CreateItem("Entry into Cash Receipts", "", 14, false, ""));
			AddItem_2(CreateItem("Entry into Clerk", "", 10, false, ""));
			AddItem_2(CreateItem("Entry into Code Enforcement", "", 12, false, ""));
			AddItem_2(CreateItem("Entry into E911", "", 17, false, ""));
			AddItem_2(CreateItem("Entry into Fixed Assets", "", 20, false, ""));
			AddItem_2(CreateItem("Entry into Motor Vehicle", "", 5, false, ""));
			AddItem_2(CreateItem("Entry into Payroll", "", 15, false, ""));
			AddItem_2(CreateItem("Entry into Personal Property", "", 6, false, ""));
			AddItem_2(CreateItem("Entry into PP Collections", "", 18, false, ""));
			AddItem_2(CreateItem("Entry into RE Collections", "", 9, false, ""));
			AddItem_2(CreateItem("Entry into Real Estate", "", 4, false, ""));
			AddItem_2(CreateItem("Entry into Bluebook", "", 19, false, ""));
			AddItem_2(CreateItem("Entry into Tax Rate Calculator", "", 16, false, ""));
			AddItem_2(CreateItem("Entry into Utility Billing", "", 8, false, ""));
			// Call AddItem(CreateItem("Entry into Voter Registration", "", 11, False, ""))
			AddItem_2(CreateItem("System Maintenance", "", 1, true, ""));
			AddItem_2(CreateItem("System Maintenance", "Backup & Restore", 3, false, ""));
			// Call AddItem(CreateItem("System Maintenance", "Compact & Repair",23 , False, ""))
			AddItem_2(CreateItem("System Maintenance", "Customize", 29, false, ""));
			AddItem_2(CreateItem("System Maintenance", "Edit Operators", 21, false, ""));
			AddItem_2(CreateItem("System Maintenance", "E-Mail", 28, false, ""));
			AddItem_2(CreateItem("System Maintenance", "Internal Audit", 27, false, ""));
			AddItem_2(CreateItem("System Maintenance", "Password Setup", 2, false, ""));
			AddItem_2(CreateItem("System Maintenance", "Printer Setup", 31, false, ""));
			AddItem_2(CreateItem("System Maintenance", "Program Update", 26, false, ""));
			AddItem_2(CreateItem("System Maintenance", "Signatures", 30, false, ""));
			AddItem_2(CreateItem("System Maintenance", "Upload Files", 25, false, ""));
			AddItem_2(CreateItem("System Maintenance", "Audit Changes", 40, false, ""));
			AddItem_2(CreateItem("System Maintenance", "Audit Changes Archive", 41, false, ""));
			AddItem_2(CreateItem("System Maintenance", "Purge Audit Changes", 42, false, ""));
		}
	}
}
