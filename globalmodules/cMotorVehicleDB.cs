﻿//Fecher vbPorter - Version 1.0.0.59
using fecherFoundation;
//using Microsoft.VisualBasic;
using Global;
using System;
using SharedApplication.Extensions;
using SharedApplication.MotorVehicle.Commands;
using TWSharedLibrary;

namespace TWMV0000
{
    public class cMotorVehicleDB
    {
        //=========================================================
        private string strLastError = "";
        private int lngLastError;

        public string LastErrorMessage
        {
            get
            {
                string LastErrorMessage = "";
                LastErrorMessage = strLastError;
                return LastErrorMessage;
            }
        }

        public int LastErrorNumber
        {
            get
            {
                int LastErrorNumber = 0;
                LastErrorNumber = lngLastError;
                return LastErrorNumber;
            }
        }

        public void ClearErrors()
        {
            strLastError = "";
            lngLastError = 0;
        }

        public cVersionInfo GetVersion()
        {
            cVersionInfo GetVersion = null;
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                ClearErrors();
                cVersionInfo tVer = new cVersionInfo();
                clsDRWrapper rsLoad = new clsDRWrapper();
                rsLoad.OpenRecordset("select * from dbversion", "MotorVehicle");
                if (!rsLoad.EndOfFile())
                {
                    tVer.Build = FCConvert.ToInt16(rsLoad.Get_Fields("Build"));
                    tVer.Major = FCConvert.ToInt16(rsLoad.Get_Fields("Major"));
                    tVer.Minor = FCConvert.ToInt16(rsLoad.Get_Fields("Minor"));
                    tVer.Revision = FCConvert.ToInt16(rsLoad.Get_Fields("Revision"));
                }

                GetVersion = tVer;
                return GetVersion;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
            }

            return GetVersion;
        }

        public void SetVersion(ref cVersionInfo nVersion)
        {
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                if (!(nVersion == null))
                {
                    clsDRWrapper rsSave = new clsDRWrapper();
                    rsSave.OpenRecordset("select * from dbversion", "MotorVehicle");
                    if (rsSave.EndOfFile())
                    {
                        rsSave.AddNew();
                    }
                    else
                    {
                        rsSave.Edit();
                    }

                    rsSave.Set_Fields("Major", nVersion.Major);
                    rsSave.Set_Fields("Minor", nVersion.Minor);
                    rsSave.Set_Fields("Revision", nVersion.Revision);
                    rsSave.Set_Fields("Build", nVersion.Build);
                    rsSave.Update();
                }

                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
            }
        }

        public bool CheckVersion()
        {
            bool CheckVersion = false;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                cVersionInfo nVer = new cVersionInfo();
                cVersionInfo tVer = new cVersionInfo();
                cVersionInfo cVer;
                bool boolNeedUpdate;
                cVer = GetVersion();
                if (cVer == null)
                {
                    cVer = new cVersionInfo();
                }

                nVer.Major = 1;
                // default to 1.0.0.0
                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (OldCDBS())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 1;
                if (tVer.IsNewer(cVer))
                {
                    if (FCConvert.ToBoolean(AddLongTermLaserForms()))
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 2;
                if (tVer.IsNewer(cVer))
                {
                    if (AddLeaseUseTaxFormVersion())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 3;
                if (tVer.IsNewer(cVer))
                {
                    if (AddMVR3LaserForms())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 4;
                if (tVer.IsNewer(cVer))
                {
                    if (FCConvert.ToBoolean(AddFieldsForMVR3Eaudit()))
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 5;
                if (tVer.IsNewer(cVer))
                {
                    if (UpdateReason2Priority())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 6;
                if (tVer.IsNewer(cVer))
                {
                    if (Nov2017LegUpdates())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 2;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (FixPeriodCloseoutKeyField())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 2;
                tVer.Build = 1;
                if (tVer.IsNewer(cVer))
                {
                    if (AddCompanyCode())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 2;
                tVer.Build = 2;
                if (tVer.IsNewer(cVer))
                {
                    if (AddInventoryLock())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 2;
                tVer.Build = 3;
                if (tVer.IsNewer(cVer))
                {
                    if (AddTwentyFiveYearPlate())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 2;
                tVer.Build = 4;
                if (tVer.IsNewer(cVer))
                {
                    if (FixStrippedPlate())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 3;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (UpdateUseTaxCertificate())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 3;
                tVer.Build = 1;
                if (tVer.IsNewer(cVer))
                {
                    if (Jan2018AddEMPlateClass())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }

                    }
                    else
                    {
                        return CheckVersion;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 1;
                tVer.Revision = 0;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddTwentyFiveYearPlateToHeldRegistrationMaster())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 1;
                tVer.Revision = 1;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddSpecialRegResidenceCode())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 2;
                tVer.Revision = 1;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (UpdatesForConceptualDesignSept2019())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                        else
                        {
                            return CheckVersion;
                        }
                    }
                }

                tVer.Major = 1;
                tVer.Minor = 2;
                tVer.Revision = 1;
                tVer.Build = 1;
                if (tVer.IsNewer(cVer))
                {
                    if (PreparePeriodCloseOutTable())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                        else
                        {
                            return CheckVersion;
                        }
                    }
                }

                tVer.Major = 2;
                tVer.Minor = 0;
                tVer.Revision = 3;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddInventoryCodePrefixNumberSuffixIndex())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                        else
                        {
                            return CheckVersion;
                        }
                    }
                }


                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 0;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (UpdateForAutocycle())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                        else
                        {
                            return CheckVersion;
                        }
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (UpdateTownSummaryTitles())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                        else
                        {
                            return CheckVersion;
                        }
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 2;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddPassengerTruckRentalCategory())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                        else
                        {
                            return CheckVersion;
                        }
                    }
                }

                
                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 4;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (CorrectTractorClasses())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                        else
                        {
                            return CheckVersion;
                        }
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 5;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddSeparatedRegName())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                        else
                        {
                            return CheckVersion;
                        }
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 6;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
	                if (UpdateSpecialEquipmentTownSummaryTitle())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(ref nVer);
		                }
		                else
		                {
			                return CheckVersion;
		                }
	                }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 7;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
	                if (CleanBadPartyIDs())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(ref nVer);
		                }
		                else
		                {
			                return CheckVersion;
		                }
	                }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 10;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddCTAPaidField())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                        else
                        {
                            return CheckVersion;
                        }
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 11;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (ResetStyleCodes())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                        else
                        {
                            return CheckVersion;
                        }
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 12;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
	                if (AddPriorTaxReceiptField())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(ref nVer);
		                }
		                else
		                {
			                return CheckVersion;
		                }
	                }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 13;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
	                if (ResetVeteranClassesForTROWMV305())
	                {
		                nVer.Copy(tVer);
		                if (cVer.IsOlder(nVer))
		                {
			                SetVersion(ref nVer);
		                }
		                else
		                {
			                return CheckVersion;
		                }
	                }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 14;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (RemoveL5SubClass())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                        else
                        {
                            return CheckVersion;
                        }
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 15;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (UpdateSeperatedRegistrantFieldsInActivityMaster())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                        else
                        {
                            return CheckVersion;
                        }
                    }
                }

                CheckVersion = true;
                return CheckVersion;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
            }

            return CheckVersion;
        }

        private bool UpdateSeperatedRegistrantFieldsInActivityMaster()
        {
            try
            {
                cPartyController pCont = new cPartyController();
                cParty pInfo;
                clsDRWrapper rsUpdate = new clsDRWrapper();

                rsUpdate.OpenRecordset("SELECT * FROM ActivityMaster WHERE OldMVR3 = 0", "MotorVehicle");
                if (!rsUpdate.EndOfFile() && !rsUpdate.BeginningOfFile())
                {
                    do
                    {
                        if (rsUpdate.Get_Fields_Int32("PartyID1") != 0 &&
                            ((rsUpdate.Get_Fields_String("OwnerCode1") == "I" && rsUpdate.Get_Fields_String("Reg1FirstName") == "") ||
                             (rsUpdate.Get_Fields_String("OwnerCode1") != "I" && rsUpdate.Get_Fields_String("Owner1") == "")))

                        {
                            pInfo = pCont.GetParty(rsUpdate.Get_Fields_Int32("PartyID1"));

                            rsUpdate.Set_Fields("PartyName1", pInfo.FullNameMiddleInitial.ToUpper());

                            if (rsUpdate.Get_Fields_String("OwnerCode1") == "I")
                            {
                                rsUpdate.Set_Fields("Owner1", "");
                                rsUpdate.Set_Fields("Reg1LastName", pInfo.LastName.ToUpper());
                                rsUpdate.Set_Fields("Reg1FirstName", pInfo.FirstName.ToUpper());
                                rsUpdate.Set_Fields("Reg1MI", pInfo.MiddleName.Trim().Length > 1 ? pInfo.MiddleName.Left(1).ToUpper() : pInfo.MiddleName.ToUpper());
                                rsUpdate.Set_Fields("Reg1Designation", pInfo.Designation.ToUpper());
                            }
                            else
                            {
                                rsUpdate.Set_Fields("Owner1", pInfo.FullNameMiddleInitial.ToUpper());
                                rsUpdate.Set_Fields("Reg1LastName", "");
                                rsUpdate.Set_Fields("Reg1FirstName", "");
                                rsUpdate.Set_Fields("Reg1MI", "");
                                rsUpdate.Set_Fields("Reg1Designation", "");
                            }
                        }

                        if (rsUpdate.Get_Fields_Int32("PartyID2") != 0 &&
                            ((rsUpdate.Get_Fields_String("OwnerCode2") == "I" && rsUpdate.Get_Fields_String("Reg2FirstName") == "") ||
                             (rsUpdate.Get_Fields_String("OwnerCode2") != "I" && rsUpdate.Get_Fields_String("Owner2") == "")))
                        {
                            pInfo = pCont.GetParty(rsUpdate.Get_Fields_Int32("PartyID2"));

                            rsUpdate.Set_Fields("PartyName2", pInfo.FullNameMiddleInitial.ToUpper());

                            if (rsUpdate.Get_Fields_String("OwnerCode2") == "I")
                            {
                                rsUpdate.Set_Fields("Owner2", "");
                                rsUpdate.Set_Fields("Reg2LastName", pInfo.LastName.ToUpper());
                                rsUpdate.Set_Fields("Reg2FirstName", pInfo.FirstName.ToUpper());
                                rsUpdate.Set_Fields("Reg2MI", pInfo.MiddleName.Trim().Length > 1 ? pInfo.MiddleName.Left(1).ToUpper() : pInfo.MiddleName.ToUpper());
                                rsUpdate.Set_Fields("Reg2Designation", pInfo.Designation.ToUpper());
                            }
                            else
                            {
                                rsUpdate.Set_Fields("Owner2", pInfo.FullNameMiddleInitial.ToUpper());
                                rsUpdate.Set_Fields("Reg2LastName", "");
                                rsUpdate.Set_Fields("Reg2FirstName", "");
                                rsUpdate.Set_Fields("Reg2MI", "");
                                rsUpdate.Set_Fields("Reg2Designation", "");
                            }
                        }

                        if (rsUpdate.Get_Fields_Int32("PartyID3") != 0 &&
                            ((rsUpdate.Get_Fields_String("OwnerCode3") == "I" && rsUpdate.Get_Fields_String("Reg3FirstName") == "") ||
                             (rsUpdate.Get_Fields_String("OwnerCode3") != "I" && rsUpdate.Get_Fields_String("Owner3") == "")))
                        {
                            pInfo = pCont.GetParty(rsUpdate.Get_Fields_Int32("PartyID3"));

                            rsUpdate.Set_Fields("PartyName3", pInfo.FullNameMiddleInitial.ToUpper());

                            if (rsUpdate.Get_Fields_String("OwnerCode3") == "I")
                            {
                                rsUpdate.Set_Fields("Owner3", "");
                                rsUpdate.Set_Fields("Reg3LastName", pInfo.LastName.ToUpper());
                                rsUpdate.Set_Fields("Reg3FirstName", pInfo.FirstName.ToUpper());
                                rsUpdate.Set_Fields("Reg3MI", pInfo.MiddleName.Trim().Length > 1 ? pInfo.MiddleName.Left(1).ToUpper() : pInfo.MiddleName.ToUpper());
                                rsUpdate.Set_Fields("Reg3Designation", pInfo.Designation.ToUpper());
                            }
                            else
                            {
                                rsUpdate.Set_Fields("Owner3", pInfo.FullNameMiddleInitial.ToUpper());
                                rsUpdate.Set_Fields("Reg3LastName", "");
                                rsUpdate.Set_Fields("Reg3FirstName", "");
                                rsUpdate.Set_Fields("Reg3MI", "");
                                rsUpdate.Set_Fields("Reg3Designation", "");
                            }
                        }

                        rsUpdate.Update();
                        rsUpdate.MoveNext();
                    } while (!rsUpdate.EndOfFile());
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
                return false;
            }
        }

        private bool RemoveL5SubClass()
        {
            try
            {
                clsDRWrapper rsUpdate = new clsDRWrapper();
                rsUpdate.Execute("DELETE FROM Class WHERE SystemCode = 'L5'", "MotorVehicle");
                rsUpdate.Execute("UPDATE Master Set Subclass = '!!' WHERE Subclass = 'L5'", "MotorVehicle");
                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
                return false;
            }
        }

        private bool ResetStyleCodes()
        {
            var styleCodeService = StaticSettings.GlobalCommandDispatcher.Send(new GetStyleCodeService()).Result;

            return styleCodeService.InitializeStyleCodes();
        }

        private bool AddPriorTaxReceiptField()
        {
	        try
	        {
		        clsDRWrapper rsUpdate = new clsDRWrapper();
		        rsUpdate.OpenRecordset(
			        "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Master' AND COLUMN_NAME = 'PriorTaxReceipt'",
			        "MotorVehicle");
		        if (rsUpdate.RecordCount() == 0)
		        {
			        rsUpdate.Execute("ALTER TABLE Master                 ADD PriorTaxReceipt INT NULL", "MotorVehicle");
			        rsUpdate.Execute("ALTER TABLE ActivityMaster         ADD PriorTaxReceipt INT NULL", "MotorVehicle");
			        rsUpdate.Execute("ALTER TABLE ArchiveMaster          ADD PriorTaxReceipt INT NULL", "MotorVehicle");
			        rsUpdate.Execute("ALTER TABLE HeldRegistrationMaster ADD PriorTaxReceipt INT NULL", "MotorVehicle");
			        rsUpdate.Execute("ALTER TABLE PendingActivityMaster  ADD PriorTaxReceipt INT NULL", "MotorVehicle");
			        rsUpdate.Execute("ALTER TABLE tblMasterTemp          ADD PriorTaxReceipt INT NULL", "MotorVehicle");
		        }

		        return true;
	        }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
                return false;
	        }

        }

		private bool AddCTAPaidField()
        {
            try
            {
                clsDRWrapper rsUpdate = new clsDRWrapper();
                rsUpdate.OpenRecordset(
                    "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Master' AND COLUMN_NAME = 'CTAPaid'",
                    "MotorVehicle");
                if (rsUpdate.RecordCount() == 0)
                {
                    rsUpdate.Execute("ALTER TABLE Master                 ADD CTAPaid BIT NULL", "MotorVehicle");
                    rsUpdate.Execute("ALTER TABLE ActivityMaster         ADD CTAPaid BIT NULL", "MotorVehicle");
                    rsUpdate.Execute("ALTER TABLE ArchiveMaster          ADD CTAPaid BIT NULL", "MotorVehicle");
                    rsUpdate.Execute("ALTER TABLE HeldRegistrationMaster ADD CTAPaid BIT NULL", "MotorVehicle");
                    rsUpdate.Execute("ALTER TABLE PendingActivityMaster  ADD CTAPaid BIT NULL", "MotorVehicle");
                    rsUpdate.Execute("ALTER TABLE tblMasterTemp          ADD CTAPaid BIT NULL", "MotorVehicle");
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
                return false;
            }
            
        }

        private bool UpdateSpecialEquipmentTownSummaryTitle()
        {
	        try
	        {
		        // On Error GoTo ErrorHandler
		        clsDRWrapper rsUpdate = new clsDRWrapper();

		        rsUpdate.Execute(
			        "UPDATE TownSummaryHeadings SET Heading = 'SPECIAL EQUIP' WHERE Heading = 'SPECIAL EQUP'",
			        "MotorVehicle");
		        
		        return true;
	        }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
                return false;
	        }
        }

        private bool AddSeparatedRegName()
        {
            string strSql;

            try
            {
                clsDRWrapper rsSave = new clsDRWrapper();

                // Master
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1FirstName' and table_name = N'Master') " +
                    " Alter table Master add Reg1FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1LastName' and table_name = N'Master') " +
                    " Alter table Master add Reg1LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1Designation' and table_name = N'Master') " +
                    " Alter table Master add Reg1Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1MI' and table_name = N'Master') " +
                    " Alter table Master add Reg1MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2FirstName' and table_name = N'Master') " +
                    " Alter table Master add Reg2FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2LastName' and table_name = N'Master') " +
                    " Alter table Master add Reg2LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2Designation' and table_name = N'Master') " +
                    " Alter table Master add Reg2Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2MI' and table_name = N'Master') " +
                    " Alter table Master add Reg2MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3FirstName' and table_name = N'Master') " +
                    " Alter table Master add Reg3FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3LastName' and table_name = N'Master') " +
                    " Alter table Master add Reg3LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3Designation' and table_name = N'Master') " +
                    " Alter table Master add Reg3Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3MI' and table_name = N'Master') " +
                    " Alter table Master add Reg3MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner1' and table_name = N'Master') " +
                    " Alter table Master add Owner1 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner2' and table_name = N'Master') " +
                    " Alter table Master add Owner2 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner3' and table_name = N'Master') " +
                    " Alter table Master add Owner3 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                // ActivityMaster
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1FirstName' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Reg1FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1LastName' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Reg1LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1Designation' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Reg1Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1MI' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Reg1MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2FirstName' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Reg2FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2LastName' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Reg2LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2Designation' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Reg2Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2MI' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Reg2MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3FirstName' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Reg3FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3LastName' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Reg3LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3Designation' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Reg3Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3MI' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Reg3MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner1' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Owner1 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner2' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Owner2 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner3' and table_name = N'ActivityMaster') " +
                    " Alter table ActivityMaster add Owner3 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                // PendingActivityMaster
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1FirstName' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Reg1FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1LastName' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Reg1LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1Designation' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Reg1Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1MI' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Reg1MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2FirstName' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Reg2FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2LastName' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Reg2LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2Designation' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Reg2Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2MI' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Reg2MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3FirstName' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Reg3FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3LastName' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Reg3LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3Designation' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Reg3Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3MI' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Reg3MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner1' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Owner1 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner2' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Owner2 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner3' and table_name = N'PendingActivityMaster') " +
                    " Alter table PendingActivityMaster add Owner3 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                // ArchiveMaster
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1FirstName' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Reg1FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1LastName' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Reg1LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1Designation' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Reg1Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1MI' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Reg1MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2FirstName' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Reg2FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2LastName' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Reg2LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2Designation' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Reg2Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2MI' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Reg2MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3FirstName' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Reg3FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3LastName' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Reg3LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3Designation' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Reg3Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3MI' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Reg3MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner1' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Owner1 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner2' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Owner2 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner3' and table_name = N'ArchiveMaster') " +
                    " Alter table ArchiveMaster add Owner3 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                // tblMasterTemp
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1FirstName' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Reg1FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1LastName' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Reg1LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1Designation' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Reg1Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1MI' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Reg1MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2FirstName' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Reg2FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2LastName' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Reg2LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2Designation' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Reg2Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2MI' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Reg2MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3FirstName' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Reg3FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3LastName' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Reg3LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3Designation' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Reg3Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3MI' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Reg3MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner1' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Owner1 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner2' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Owner2 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner3' and table_name = N'tblMasterTemp') " +
                    " Alter table tblMasterTemp add Owner3 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                // HeldRegistrationMaster
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1FirstName' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Reg1FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1LastName' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Reg1LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1Designation' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Reg1Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg1MI' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Reg1MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2FirstName' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Reg2FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2LastName' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Reg2LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2Designation' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Reg2Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg2MI' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Reg2MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3FirstName' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Reg3FirstName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3LastName' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Reg3LastName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3Designation' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Reg3Designation nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Reg3MI' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Reg3MI nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner1' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Owner1 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner2' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Owner2 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'Owner3' and table_name = N'HeldRegistrationMaster') " +
                    " Alter table HeldRegistrationMaster add Owner3 nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");

                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
                return false;
            }
        }

        private bool CorrectTractorClasses()
        {
            try
            {
                clsDRWrapper rsUpdate = new clsDRWrapper();
                rsUpdate.Execute("UPDATE Class SET HalfRateAllowed = 'Y' WHERE BMVCode = 'TR' AND SystemCode = 'T4'", "MotorVehicle");
                rsUpdate.Execute("UPDATE Class SET HalfRateAllowed = 'N' WHERE BMVCode = 'TR' AND SystemCode = 'T5'", "MotorVehicle");
                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
                return false;
            }
        }
        private bool AddInventoryCodePrefixNumberSuffixIndex()
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsUpdate = new clsDRWrapper();
                var sql = "if not exists (select * from sys.indexes where object_id = Object_Id('Inventory') and name = 'CodePrefixNumberSuffix')";
                sql +=
                    "CREATE NONCLUSTERED INDEX[CodePrefixNumberSuffix] ON[dbo].[Inventory] ([Code] ASC, [Prefix] ASC, [Number] ASC, [Suffix] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON[PRIMARY]";
                rsUpdate.Execute(
                    sql,
                    "MotorVehicle");
                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
                return false;
            }
        }

        private bool AddPassengerTruckRentalCategory()
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsUpdate = new clsDRWrapper();

                rsUpdate.OpenRecordset("SELECT * FROM TownSummaryHeadings where categorycode = 27 and Heading = 'PASSENGER TRUCK RENTAL'", "MotorVehicle");
                if (rsUpdate.EndOfFile())
                {
                    rsUpdate.AddNew();
                    rsUpdate.Set_Fields("Heading", "PASSENGER TRUCK RENTAL");
                    rsUpdate.Set_Fields("CategoryCode", 27);
                    rsUpdate.Update();
                }
                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
                return false;
            }
        }

        private bool UpdateTownSummaryTitles()
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsUpdate = new clsDRWrapper();

                rsUpdate.Execute(
                    "UPDATE TownSummaryHeadings SET Heading = 'SPECIAL EQUP' WHERE Heading = 'SPECIAL EQUIPMENT'",
                    "MotorVehicle");
                rsUpdate.Execute(
                    "UPDATE TownSummaryHeadings SET Heading = 'RESERVE # FEES' WHERE Heading = 'RESERVE NUMBER'",
                    "MotorVehicle");
                rsUpdate.Execute(
                    "UPDATE TownSummaryHeadings SET Heading = 'SP PER CERT' WHERE Heading = 'SPECIAL PERMIT CERT'",
                    "MotorVehicle");
                rsUpdate.Execute(
                    "UPDATE TownSummaryHeadings SET Heading = 'INCREASE RVW' WHERE Heading = 'INCREASE GVW'",
                    "MotorVehicle");
                rsUpdate.Execute(
                    "UPDATE TownSummaryHeadings SET Heading = 'TRANSIT PLATES' WHERE Heading = 'TRANSIT'",
                    "MotorVehicle");
                rsUpdate.Execute(
                    "UPDATE TownSummaryHeadings SET Heading = 'SUP YOUR TRPS-NEW' WHERE Heading = 'SUP YR TRPS-NEW'",
                    "MotorVehicle");
                rsUpdate.Execute(
                    "UPDATE TownSummaryHeadings SET Heading = 'SUP YOUR TRPS-RENEWAL' WHERE Heading = 'SUP YR TRPS-RENEWAL'",
                    "MotorVehicle");
                rsUpdate.Execute(
                    "UPDATE TownSummaryHeadings SET Heading = 'UNIV OF MAINE-NEW' WHERE Heading = 'U OF M -NEW'",
                    "MotorVehicle");
                rsUpdate.Execute(
                    "UPDATE TownSummaryHeadings SET Heading = 'UNIV OF MAINE-RENEWAL' WHERE Heading = 'U OF M -RENEWAL'",
                    "MotorVehicle");
                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
                return false;
            }
        }

        private bool AddSpecialRegResidenceCode()
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsUpdate = new clsDRWrapper();
                if (!FieldExists("SpecialRegistration", "ResidenceCode", "MotorVehicle"))
                {
                    rsUpdate.Execute("Alter table SpecialRegistration add ResidenceCode nvarchar(255) null",
                        "MotorVehicle");
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
                return false;
            }
        }

        private bool AddTwentyFiveYearPlateToHeldRegistrationMaster()
        {
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper rsUpdate = new clsDRWrapper();
                if (!FieldExists("HeldRegistrationMaster", "TwentyFiveYearPlate", "MotorVehicle"))
                {
                    rsUpdate.Execute("Alter table HeldRegistrationMaster add TwentyFiveYearPlate bit null",
                        "MotorVehicle");
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = fecherFoundation.Information.Err().Description;
                lngLastError = fecherFoundation.Information.Err().Number;
                return false;
            }
        }

        private bool FieldExists(string strTable, string strField, string strDB)
        {
            bool FieldExists = false;
            try
            {
                // On Error GoTo ErrorHandler
                string strSQL;
                clsDRWrapper rsLoad = new clsDRWrapper();
                strSQL = "Select column_name from information_schema.columns where column_name = '" + strField +
                         "' and table_name = '" + strTable + "'";
                rsLoad.OpenRecordset(strSQL, strDB);
                if (!rsLoad.EndOfFile())
                {
                    FieldExists = true;
                }

                return FieldExists;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }

            return FieldExists;
        }

        // vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
        private object AddFieldsForMVR3Eaudit()
        {
            object AddFieldsForMVR3Eaudit = null;
            // kk08092016 TROMVS-57  Adding fields to support rules changes and issues discovered during updates for Laser MVR-3E
            // and the BMV audits
            clsDRWrapper rsUpdate = new clsDRWrapper();
            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Master' AND COLUMN_NAME = 'NoFeeCTA'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE Master                 ADD NoFeeCTA BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ActivityMaster         ADD NoFeeCTA BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ArchiveMaster          ADD NoFeeCTA BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE HeldRegistrationMaster ADD NoFeeCTA BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE PendingActivityMaster  ADD NoFeeCTA BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE tblMasterTemp          ADD NoFeeCTA BIT NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Master' AND COLUMN_NAME = 'DealerSalesTax'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE Master                 ADD DealerSalesTax BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ActivityMaster         ADD DealerSalesTax BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ArchiveMaster          ADD DealerSalesTax BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE HeldRegistrationMaster ADD DealerSalesTax BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE PendingActivityMaster  ADD DealerSalesTax BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE tblMasterTemp          ADD DealerSalesTax BIT NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Master' AND COLUMN_NAME = 'BaseIsSalePrice'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE Master                 ADD BaseIsSalePrice BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ActivityMaster         ADD BaseIsSalePrice BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ArchiveMaster          ADD BaseIsSalePrice BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE HeldRegistrationMaster ADD BaseIsSalePrice BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE PendingActivityMaster  ADD BaseIsSalePrice BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE tblMasterTemp          ADD BaseIsSalePrice BIT NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Master' AND COLUMN_NAME = 'SalesTaxExempt'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE Master                 ADD SalesTaxExempt BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ActivityMaster         ADD SalesTaxExempt BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ArchiveMaster          ADD SalesTaxExempt BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE HeldRegistrationMaster ADD SalesTaxExempt BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE PendingActivityMaster  ADD SalesTaxExempt BIT NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE tblMasterTemp          ADD SalesTaxExempt BIT NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Master' AND COLUMN_NAME = 'PrintPriorities'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE Master                 ADD PrintPriorities nvarchar(255) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ActivityMaster         ADD PrintPriorities nvarchar(255) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ArchiveMaster          ADD PrintPriorities nvarchar(255) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE HeldRegistrationMaster ADD PrintPriorities nvarchar(255) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE PendingActivityMaster  ADD PrintPriorities nvarchar(255) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE tblMasterTemp          ADD PrintPriorities nvarchar(255) NULL",
                    "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Master' AND COLUMN_NAME = 'ReasonCodes'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE Master                 ADD ReasonCodes nvarchar(255) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ActivityMaster         ADD ReasonCodes nvarchar(255) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ArchiveMaster          ADD ReasonCodes nvarchar(255) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE HeldRegistrationMaster ADD ReasonCodes nvarchar(255) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE PendingActivityMaster  ADD ReasonCodes nvarchar(255) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE tblMasterTemp          ADD ReasonCodes nvarchar(255) NULL",
                    "MotorVehicle");
            }

            AddFieldsForMVR3Eaudit = true;
            return AddFieldsForMVR3Eaudit;
        }

        // vbPorter upgrade warning: 'Return' As object	OnWrite(bool)
        private object AddLongTermLaserForms()
        {
            object AddLongTermLaserForms = null;
            clsDRWrapper rsUpdate = new clsDRWrapper();
            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'WMV' AND COLUMN_NAME = 'LongTermLaserForms'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("alter table wmv add LongTermLaserForms bit NULL", "MotorVehicle");
                rsUpdate.Execute("Update wmv set LongTermLaserForms = 0", "MotorVehicle");
            }

            AddLongTermLaserForms = true;
            return AddLongTermLaserForms;
        }

        private bool AddMVR3LaserForms()
        {
            bool AddMVR3LaserForms = false;
            // kk03252016 tromvs-57  Add field to store MVR-3E Laser form option
            clsDRWrapper rsUpdate = new clsDRWrapper();
            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'WMV' AND COLUMN_NAME = 'MVR3LaserForms'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE WMV ADD MVR3LaserForms bit NULL", "MotorVehicle");
                rsUpdate.Execute("Update wmv set MVR3LaserForms = 0", "MotorVehicle");
            }

            AddMVR3LaserForms = true;
            return AddMVR3LaserForms;
        }

        private bool UpdateReason2Priority()
        {
            bool UpdateReason2Priority = false;
            // kk09302016  Force R2 to the end
            clsDRWrapper rs = new clsDRWrapper();
            rs.Execute("UPDATE ReasonCodes SET Priority = 999 WHERE Code = '2'", "TWMV0000.vb1");
            UpdateReason2Priority = true;
            return UpdateReason2Priority;
        }

        private bool OldCDBS()
        {
            bool OldCDBS = false;
            clsDRWrapper rsUpdate = new clsDRWrapper();
            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'WMV' AND COLUMN_NAME = 'TownName'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE WMV ADD TownName nvarchar(100) NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE WMV ADD TownAddress1 nvarchar(100) NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE WMV ADD TownAddress2 nvarchar(100) NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE WMV ADD TownAddress3 nvarchar(100) NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE WMV ADD TownAddress4 nvarchar(100) NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE WMV ADD ReminderType nvarchar(100) NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'WMV' AND COLUMN_NAME = 'LongTermPrintUseTaxCTA'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE WMV ADD LongTermPrintUseTaxCTA bit NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE WMV ADD LongTermFTPAddress nvarchar(100) NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE WMV ADD LongTermFTPUser nvarchar(100) NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE WMV ADD LongTermFTPPassword nvarchar(100) NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'WMV' AND COLUMN_NAME = 'LongTermAutoPopulateUnit'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE WMV ADD LongTermAutoPopulateUnit bit NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'WMV' AND COLUMN_NAME = 'UseTaxFormVersion'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE WMV ADD UseTaxFormVersion int NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'WMV' AND COLUMN_NAME = 'ReminderMessage'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE WMV ADD ReminderMessage nvarchar(MAX) NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'DefaultInfo' AND COLUMN_NAME = 'RushTitleFee'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE DefaultInfo ADD RushTitleFee decimal(18, 2) NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'DefaultInfo' AND COLUMN_NAME = 'MinimumExcise'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE DefaultInfo ADD MinimumExcise decimal(18, 2) NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Master' AND COLUMN_NAME = 'RushTitleFee'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE Master ADD RushTitleFee decimal(18, 2) NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ActivityMaster ADD RushTitleFee decimal(18, 2) NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ArchiveMaster ADD RushTitleFee decimal(18, 2) NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE HeldRegistrationMaster ADD RushTitleFee decimal(18, 2) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE PendingActivityMaster ADD RushTitleFee decimal(18, 2) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE tblMasterTemp ADD RushTitleFee decimal(18, 2) NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Title' AND COLUMN_NAME = 'RushTitle'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE Title ADD RushTitle bit NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LongTermTrailerRegistrations' AND COLUMN_NAME = 'RushTitleFee'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE LongTermTrailerRegistrations ADD RushTitleFee decimal(18, 2) NULL",
                    "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LongTermTitleApplications'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("CREATE TABLE LongTermTitleApplications(ID int NOT NULL IDENTITY (1, 1)) ON [PRIMARY]",
                    "MotorVehicle");
                rsUpdate.Execute(
                    "ALTER TABLE LongTermTitleApplications ADD CONSTRAINT PK_LongTermTitleApplications PRIMARY KEY CLUSTERED(ID)",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD CTANumber nvarchar(255) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD Fee decimal(18, 2) NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD RushFee decimal(18, 2) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD Class nvarchar(255) NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD Plate nvarchar(255) NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD RegistrationDate datetime NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD CustomerName nvarchar(255) NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD OPID nvarchar(255) NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD DoubleIfApplicable int NULL",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD PeriodCloseoutID int NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD TellerCloseoutID int NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD MSRP nvarchar(255) NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LongTermTitleApplications' AND COLUMN_NAME = 'StandAlone'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD StandAlone bit NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LongTermTitleApplications' AND COLUMN_NAME = 'Units'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD Units int NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LongTermTitleApplications' AND COLUMN_NAME = 'Duplicate'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD Duplicate bit NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LongTermTitleApplications' AND COLUMN_NAME = 'DuplicateReason'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE LongTermTitleApplications ADD DuplicateReason nvarchar(255) NULL",
                    "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'FleetMaster' AND COLUMN_NAME = 'Deleted'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE FleetMaster ADD Deleted bit NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'FleetMaster' AND COLUMN_NAME = 'EMailAddress'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE FleetMaster ADD EMailAddress nvarchar(255) NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Master' AND COLUMN_NAME = 'OverrideFleetEmail'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE Master ADD OverrideFleetEmail bit NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ActivityMaster ADD OverrideFleetEmail bit NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE ArchiveMaster ADD OverrideFleetEmail bit NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE HeldRegistrationMaster ADD OverrideFleetEmail bit NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE PendingActivityMaster ADD OverrideFleetEmail bit NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE tblMasterTemp ADD OverrideFleetEmail bit NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'FleetLock'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("CREATE TABLE FleetLock(ID int NOT NULL IDENTITY (1, 1)) ON [PRIMARY]",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE FleetLock ADD CONSTRAINT PK_FleetLock PRIMARY KEY CLUSTERED(ID)",
                    "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE FleetLock ADD MasterLock bit NULL", "MotorVehicle");
                rsUpdate.Execute("ALTER TABLE FleetLock ADD OpID nvarchar(255) NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Title' AND COLUMN_NAME = 'CTANumber'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE Title ADD CTANumber nvarchar(255) NULL", "MotorVehicle");
            }

            UpdateMinimumExcise();
            modAuditReporting.CreateAuditChangesTable();
            if (MotorVehicle.Statics.gboolAllowLongTermTrailers)
            {
                rsUpdate.OpenRecordset(
                    "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LongTermTrailerRegistrations' AND COLUMN_NAME = 'UseTaxID'",
                    "MotorVehicle");
                if (rsUpdate.RecordCount() == 0)
                {
                    rsUpdate.Execute("ALTER TABLE LongTermTrailerRegistrations ADD UseTaxID int NULL", "MotorVehicle");
                }
            }

            rsUpdate.Execute("UPDATE Class SET LocationNew = 1 WHERE BMVCode = 'WB'", "MotorVehicle");
            if (LongTermTrailersAllowed())
            {
                rsUpdate.OpenRecordset(
                    "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LongTermTrailerRegistrations' AND COLUMN_NAME = 'UseTaxID'",
                    "MotorVehicle");
                if (rsUpdate.RecordCount() == 0)
                {
                    rsUpdate.Execute("ALTER TABLE LongTermTrailerRegistrations ADD UseTaxID int NULL", "MotorVehicle");
                }
            }

            AddAGCommercialClass();
            AddCRClass();
            AddCDClass();
            UpdateExceptionItemTypes();
            DeleteNoFeeDuplicateExceptionRecords();
            // UpdateDuneBuggyStockCarRegFees
            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'SpecialRegistration' AND COLUMN_NAME = 'Duplicate'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE SpecialRegistration ADD Duplicate bit NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'SpecialRegistration' AND COLUMN_NAME = 'Transfer'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE SpecialRegistration ADD Transfer bit NULL", "MotorVehicle");
            }

            rsUpdate.OpenRecordset(
                "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'WMV' AND COLUMN_NAME = 'LaserMVRT10Adjustment'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("ALTER TABLE WMV ADD LaserMVRT10Adjustment float NULL", "MotorVehicle");
            }

            UpdateReasonCodes();
            SetPrintPriorityCodes();
            OldCDBS = true;
            return OldCDBS;
        }

        private bool AddLeaseUseTaxFormVersion()
        {
            bool AddLeaseUseTaxFormVersion = false;
            bool boolReturn;
            clsDRWrapper rsUpdate = new clsDRWrapper();
            rsUpdate.OpenRecordset(
                "Select * from information_schema.columns where table_name = 'WMV' and column_name = 'LeaseUseTaxFormVersion'",
                "MotorVehicle");
            if (rsUpdate.RecordCount() == 0)
            {
                rsUpdate.Execute("Alter Table WMV Add LeaseUseTaxFormVersion int NULL", "MotorVehicle");
                rsUpdate.Execute("Update WMV set LeaseUseTaxFormVersion = 0", "MotorVehicle");
            }

            AddLeaseUseTaxFormVersion = true;
            return AddLeaseUseTaxFormVersion;
        }

        private bool LongTermTrailersAllowed()
        {
            var boolReturn = fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE TRAILER"
                              || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "MAINE MOTOR TRANSPORT"
                              || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "ACE REGISTRATION SERVICES LLC"
                              || fecherFoundation.Strings.UCase(fecherFoundation.Strings.Trim(modGlobalConstants.Statics.MuniName)) == "COUNTRYWIDE TRAILER" 
                              || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "HASKELL REGISTRATION" 
                              || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "COUNTRYWIDE TRAILER" 
                              || fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName) == "AB LEDUE ENTERPRISES";

            return boolReturn;
        }

        private void DeleteNoFeeDuplicateExceptionRecords()
        {
            clsDRWrapper rs = new clsDRWrapper();
            rs.Execute("DELETE FROM ExceptionReport WHERE Type = '4CN' AND Reason = 'No Fee Duplicate; Rental Vehicle'",
                "TWMV0000.vb1");
            rs.DisposeOf();
        }

        private void UpdateExceptionItemTypes()
        {
            clsDRWrapper rs = new clsDRWrapper();
            rs.Execute("UPDATE ExceptionReport SET Type = '6PM' WHERE Type = '4PM'", "TWMV0000.vb1");
            rs.DisposeOf();
        }

        private void UpdateMinimumExcise()
        {
            clsDRWrapper rs = new clsDRWrapper();
            rs.OpenRecordset("SELECT * FROM DefaultInfo", "TWMV0000.vb1");
            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
            {
                if (fecherFoundation.Conversion.Val(rs.Get_Fields("MinimumExcise")) == 0)
                {
                    rs.Edit();
                    rs.Set_Fields("MinimumExcise", 5);
                    rs.Update();
                }
            }
            rs.DisposeOf();
        }

        private void AddAGCommercialClass()
        {
            clsDRWrapper tempRS = new clsDRWrapper();
            tempRS.Execute("DELETE FROM Class WHERE BMVCode = 'AG' AND SystemCode = 'AG'", "MotorVehicle");
            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'AG' AND SystemCode = 'A1'", "MotorVehicle");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                // tempRS.Close
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "A1");
                tempRS.Set_Fields("BMVCode", "AG");
                tempRS.Set_Fields("Description", "AGRICULTURE (6000 LBS GVW)");
                tempRS.Set_Fields("ExciseRequired", "Y");
                tempRS.Set_Fields("RegistrationFee", 35);
                tempRS.Set_Fields("OtherFeesNew", 20);
                tempRS.Set_Fields("Otherfeesrenew", 15);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 1);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 2);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "R");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 5);
                tempRS.Update();
                //Application.DoEvents();
                // tempRS.Close
            }

            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'AG' AND SystemCode = 'A2'", "MotorVehicle");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                // tempRS.Close
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "A2");
                tempRS.Set_Fields("BMVCode", "AG");
                tempRS.Set_Fields("Description", "AGRICULTURE (10000 LBS GVW)");
                tempRS.Set_Fields("ExciseRequired", "Y");
                tempRS.Set_Fields("RegistrationFee", 37);
                tempRS.Set_Fields("OtherFeesNew", 20);
                tempRS.Set_Fields("Otherfeesrenew", 15);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 1);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 2);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "R");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 40);
                tempRS.Update();
                //Application.DoEvents();
                // tempRS.Close
            }
            tempRS.DisposeOf();
        }

        private void AddCRClass()
        {
            clsDRWrapper tempRS = new clsDRWrapper();
            tempRS.Execute("DELETE FROM Class WHERE BMVCode = 'CR' AND SystemCode = 'CR'", "TWMV0000.vb1");
            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'CR' AND SystemCode = 'C1'", "MotorVehicle");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                // tempRS.Close
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "C1");
                tempRS.Set_Fields("BMVCode", "CR");
                tempRS.Set_Fields("Description", "CONSERVATION (6000 LBS GVW)");
                tempRS.Set_Fields("ExciseRequired", "Y");
                tempRS.Set_Fields("RegistrationFee", 35);
                tempRS.Set_Fields("OtherFeesNew", 20);
                tempRS.Set_Fields("Otherfeesrenew", 15);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 1);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 2);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "R");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 5);
                tempRS.Update();
                //Application.DoEvents();
                // tempRS.Close
            }

            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'CR' AND SystemCode = 'C2'", "MotorVehicle");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                // tempRS.Close
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "C2");
                tempRS.Set_Fields("BMVCode", "CR");
                tempRS.Set_Fields("Description", "CONSERVATION (10000 LBS GVW)");
                tempRS.Set_Fields("ExciseRequired", "Y");
                tempRS.Set_Fields("RegistrationFee", 37);
                tempRS.Set_Fields("OtherFeesNew", 20);
                tempRS.Set_Fields("Otherfeesrenew", 15);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 1);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 2);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "R");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 40);
                tempRS.Update();
                //Application.DoEvents();
                // tempRS.Close
            }
            tempRS.DisposeOf();

        }

        private void AddCDClass()
        {
            clsDRWrapper tempRS = new clsDRWrapper();
            tempRS.Execute("DELETE FROM Class WHERE BMVCode = 'CD' AND SystemCode = 'CD'", "TWMV0000.vb1");
            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'CD' AND SystemCode = 'C1'", "MotorVehicle");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                // tempRS.Close
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "C1");
                tempRS.Set_Fields("BMVCode", "CD");
                tempRS.Set_Fields("Description", "CONSERVATION DISABLED (6000 LBS GVW)");
                tempRS.Set_Fields("ExciseRequired", "Y");
                tempRS.Set_Fields("RegistrationFee", 35);
                tempRS.Set_Fields("OtherFeesNew", 20);
                tempRS.Set_Fields("Otherfeesrenew", 15);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 2);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 2);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "E");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 5);
                tempRS.Update();
                //Application.DoEvents();
                // tempRS.Close
            }

            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'CD' AND SystemCode = 'C2'", "MotorVehicle");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                // tempRS.Close
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "C2");
                tempRS.Set_Fields("BMVCode", "CD");
                tempRS.Set_Fields("Description", "CONSERVATION DISABLED (10000 LBS GVW)");
                tempRS.Set_Fields("ExciseRequired", "Y");
                tempRS.Set_Fields("RegistrationFee", 37);
                tempRS.Set_Fields("OtherFeesNew", 20);
                tempRS.Set_Fields("Otherfeesrenew", 15);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 2);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 2);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "E");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 40);
                tempRS.Update();
                //Application.DoEvents();
                // tempRS.Close
            }
            tempRS.DisposeOf();
        }

        private void AddSTClass()
        {
            clsDRWrapper tempRS = new clsDRWrapper();
            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'ST' AND SystemCode = 'ST'", "MotorVehicle");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                // tempRS.Close
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "ST");
                tempRS.Set_Fields("BMVCode", "ST");
                tempRS.Set_Fields("Description", "STATE");
                tempRS.Set_Fields("ExciseRequired", "N");
                tempRS.Set_Fields("RegistrationFee", 0);
                tempRS.Set_Fields("OtherFeesNew", 0);
                tempRS.Set_Fields("Otherfeesrenew", 0);
                tempRS.Set_Fields("HalfRateAllowed", "N");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 3);
                tempRS.Set_Fields("LocationTransfer", 3);
                tempRS.Set_Fields("LocationRenewal", 3);
                tempRS.Set_Fields("NumberOfPlateStickers", 0);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "R");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 5);
                tempRS.Update();
                //Application.DoEvents();
                // tempRS.Close
            }
            tempRS.DisposeOf();
        }

        private void AddCSClass()
        {
            clsDRWrapper tempRS = new clsDRWrapper();
            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'CS' AND SystemCode = 'CS'", "MotorVehicle");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                // tempRS.Close
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "CS");
                tempRS.Set_Fields("BMVCode", "CS");
                tempRS.Set_Fields("Description", "COUNTY SHERIFF");
                tempRS.Set_Fields("ExciseRequired", "N");
                tempRS.Set_Fields("RegistrationFee", 0);
                tempRS.Set_Fields("OtherFeesNew", 0);
                tempRS.Set_Fields("Otherfeesrenew", 0);
                tempRS.Set_Fields("HalfRateAllowed", "N");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 3);
                tempRS.Set_Fields("LocationTransfer", 3);
                tempRS.Set_Fields("LocationRenewal", 3);
                tempRS.Set_Fields("NumberOfPlateStickers", 0);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "R");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 5);
                tempRS.Update();
                //Application.DoEvents();
                // tempRS.Close
            }
            tempRS.DisposeOf();
        }

        // Private Sub UpdateDuneBuggyStockCarRegFees()
        // Dim rs As New clsDRWrapper
        //
        // rs.Execute "UPDATE Class SET RegistrationFee = 7 WHERE BMVCode = 'PC' AND (SystemCode = 'P3' OR SystemCode = 'P1')", "TWMV0000.vb1"
        // End Sub
        private void UpdateReasonCodes()
        {
            clsDRWrapper rsInfo = new clsDRWrapper();
            short counter;
            rsInfo.OpenRecordset("SELECT * FROM ReasonCodes ORDER BY Code", "MotorVehicle");
            if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
            {
                do
                {
                    rsInfo.Edit();
                    string vbPorterVar = rsInfo.Get_Fields_String("Code");
                    switch (vbPorterVar)
                    {
                        case "3":
                            rsInfo.Set_Fields("Priority", "01");

                            break;
                        case "D":
                            rsInfo.Set_Fields("Priority", "02");

                            break;
                        case "4":
                            rsInfo.Set_Fields("Priority", "03");

                            break;
                        case "P":
                            rsInfo.Set_Fields("Priority", "04");

                            break;
                        case "R":
                            rsInfo.Set_Fields("Priority", "05");

                            break;
                        case "L":
                            rsInfo.Set_Fields("Priority", "06");

                            break;
                        case "#":
                            rsInfo.Set_Fields("Priority", "07");

                            break;
                        case "Z":
                            rsInfo.Set_Fields("Priority", "08");

                            break;
                        case "S":
                            rsInfo.Set_Fields("Priority", "09");

                            break;
                        case "E":
                            rsInfo.Set_Fields("Priority", "10");

                            break;
                        case "X":
                            rsInfo.Set_Fields("Priority", "11");

                            break;
                        case "W":
                            rsInfo.Set_Fields("Priority", "12");

                            break;
                        case "G":
                            rsInfo.Set_Fields("Priority", "13");

                            break;
                        case "A":
                            rsInfo.Set_Fields("Priority", "14");

                            break;
                        case "V":
                            rsInfo.Set_Fields("Priority", "15");

                            break;
                        case "U":
                            rsInfo.Set_Fields("Priority", "16");

                            break;
                        case "2":
                            rsInfo.Set_Fields("Priority", "999");

                            break;
                        default:
                            rsInfo.Set_Fields("Priority", "99");

                            break;
                    }

                    rsInfo.Update();
                    rsInfo.MoveNext();
                } while (rsInfo.EndOfFile() != true);
            }
            rsInfo.DisposeOf();
        }

        public void SetPrintPriorityCodes()
        {
            clsDRWrapper rs = new clsDRWrapper();
            short counter;
            string strPriority = "";
            clsPrintCodes oPrtCodes = new clsPrintCodes();
            // kk06142018 tromvs-96  Changed these over to use the clsPrtCodes class
            rs.Execute(
                "DELETE FROM PrintPriority WHERE CONVERT(INT, Priority) > " +
                FCConvert.ToString(oPrtCodes.MaxPrintPriority), "TWMV0000.vb1");
            rs.OpenRecordset("SELECT * FROM PrintPriority");
            for (counter = 1; counter <= oPrtCodes.MaxPrintPriority; counter++)
            {
                strPriority = Microsoft.VisualBasic.Strings.Format(counter, "00");
                if (rs.FindFirstRecord("Priority", strPriority))
                {
                    rs.Edit();
                }
                else
                {
                    rs.AddNew();
                }

                rs.Set_Fields("Priority", Microsoft.VisualBasic.Strings.Format(counter, "00"));
                rs.Set_Fields("Code", Microsoft.VisualBasic.Strings.Format(counter, "00"));
                rs.Set_Fields("Description", oPrtCodes.ReturnPrintPriorityText(counter));
                rs.Set_Fields("Include", oPrtCodes.ReturnPrintPriorityInclude(counter));
                rs.Update();
            }
            rs.DisposeOf();
        }

        // kk01102018 tromvs-96  Legislative changes for November 2017
        public bool Nov2017LegUpdates()
        {
            bool Nov2017LegUpdates = false;
            UpdateDecalCodes();
            // Update codes to match BMV codes (use 2 digits)
            UpdateVeteranClasses();
            // Change DS,DV,VT & VX 6001-26000 to commercial
            SetPrintPriorityCodes();
            // Update the Print Priorities - Remove Dup Decal and Truck Camper
            Nov2017LegUpdates = true;
            return Nov2017LegUpdates;
        }

        // kk01042018 tromvs-96  New veteran decal added. Single character decal codes will no longer work.
        // Changing the codes to two character string containing the BMV decal number.
        private void UpdateDecalCodes()
        {
            clsPrintCodes tDecals = new clsPrintCodes();
            clsDRWrapper rsUpd = new clsDRWrapper();
            clsDRWrapper rsEx = new clsDRWrapper();
            short i;
            string strCurTable = "";
            // vbPorter upgrade warning: arrTables As object	OnRead(string)
            object arrTables;
            arrTables = new object[]
            {
                "Master",
                "ActivityMaster",
                "ArchiveMaster",
                "HeldRegistrationMaster",
                "PendingActivityMaster",
                "tblMasterTemp"
            };
            for (i = 0; i <= 5; i++)
            {
                strCurTable = FCConvert.ToString(((object[])arrTables)[i]);
                rsEx.OpenRecordset("SELECT TOP 1 Battle FROM " + strCurTable +
                                   " WHERE Battle <> 'X' AND Len(Battle) = 1");
                if (!rsEx.EndOfFile())
                {
                    rsUpd.OpenRecordset("SELECT DISTINCT Battle FROM " + strCurTable +
                                        " WHERE Battle <> 'X' AND ISNULL(Battle, '') <> '' ORDER BY Battle");
                    while (!rsUpd.EndOfFile())
                    {
                        rsEx.Execute(
                            "UPDATE " + strCurTable + " SET Battle = '" +
                            tDecals.UpdateVeteranDecalCode(rsUpd.Get_Fields("Battle")) + "' WHERE Battle = '" +
                            rsUpd.Get_Fields("Battle") + "'", "MotorVehicle");
                        rsUpd.MoveNext();
                    }
                }
            }

            // i
        }

        // kk01052018 tromvs-96  Change Veteran classes DS, DV, VT and VX with weigths > 6000 lbs to commercial
        public void UpdateVeteranClasses()
        {
            clsDRWrapper tempRS = new clsDRWrapper();
            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'DS' AND SystemCode = 'D2'");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                if (tempRS.Get_Fields("RegistrationFee") != 0.01m ||
                    FCConvert.ToInt32(tempRS.Get_Fields("ShortDescription")) != 40)
                {
                    tempRS.Edit();
                    tempRS.Set_Fields("RegistrationFee", 0.01);
                    tempRS.Set_Fields("ShortDescription", 40);
                    tempRS.Set_Fields("Description", "DISABILITY SPECIAL VETERAN COMMERCIAL");
                    tempRS.Update();
                }
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "D2");
                tempRS.Set_Fields("BMVCode", "DS");
                tempRS.Set_Fields("Description", "DISABILITY SPECIAL VETERAN COMMERCIAL");
                tempRS.Set_Fields("ExciseRequired", "Y");
                tempRS.Set_Fields("RegistrationFee", 0.01);
                tempRS.Set_Fields("OtherFeesNew", 0);
                tempRS.Set_Fields("Otherfeesrenew", 0);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 2);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 2);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "R");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 40);
                tempRS.Update();
            }

            //Application.DoEvents();
            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'DV' AND SystemCode = 'D2'");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                if (FCConvert.ToInt32(tempRS.Get_Fields("ShortDescription")) != 40)
                {
                    tempRS.Edit();
                    tempRS.Set_Fields("ShortDescription", 40);
                    tempRS.Set_Fields("Description", "DISABLED VET COMMERCIAL (LOSS OF LEGS)");
                    tempRS.Update();
                }
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "D2");
                tempRS.Set_Fields("BMVCode", "DV");
                tempRS.Set_Fields("Description", "DISABLED VET COMMERCIAL (LOSS OF LEGS)");
                tempRS.Set_Fields("ExciseRequired", "N");
                tempRS.Set_Fields("RegistrationFee", 0);
                tempRS.Set_Fields("OtherFeesNew", 0);
                tempRS.Set_Fields("Otherfeesrenew", 0);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 3);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 2);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "R");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 40);
                tempRS.Update();
            }

            //Application.DoEvents();
            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'DV' AND SystemCode = 'D4'");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                if (FCConvert.ToInt32(tempRS.Get_Fields("ShortDescription")) != 40)
                {
                    tempRS.Edit();
                    tempRS.Set_Fields("ShortDescription", 40);
                    tempRS.Set_Fields("Description", "DISABLED VET COMMERCIAL (100% DISAB)");
                    tempRS.Update();
                }
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "D4");
                tempRS.Set_Fields("BMVCode", "DV");
                tempRS.Set_Fields("Description", "DISABLED VET COMMERCIAL (100% DISAB)");
                tempRS.Set_Fields("ExciseRequired", "Y");
                tempRS.Set_Fields("RegistrationFee", 0);
                tempRS.Set_Fields("OtherFeesNew", 0);
                tempRS.Set_Fields("Otherfeesrenew", 0);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 3);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 2);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "R");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 40);
                tempRS.Update();
            }

            //Application.DoEvents();
            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'VT' AND SystemCode = 'V2'");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                if (tempRS.Get_Fields("RegistrationFee") != 0.01m ||
                    FCConvert.ToInt32(tempRS.Get_Fields("ShortDescription")) != 40)
                {
                    tempRS.Edit();
                    tempRS.Set_Fields("RegistrationFee", 0.01);
                    tempRS.Set_Fields("ShortDescription", 40);
                    tempRS.Set_Fields("Description", "VETERAN COMMERCIAL");
                    tempRS.Update();
                }
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "V2");
                tempRS.Set_Fields("BMVCode", "VT");
                tempRS.Set_Fields("Description", "VETERAN COMMERCIAL");
                tempRS.Set_Fields("ExciseRequired", "Y");
                tempRS.Set_Fields("RegistrationFee", 0.01);
                tempRS.Set_Fields("OtherFeesNew", 0);
                tempRS.Set_Fields("Otherfeesrenew", 0);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 1);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 2);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "R");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 40);
                tempRS.Update();
            }

            //Application.DoEvents();
            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'VX' AND SystemCode = 'V2'");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                if (FCConvert.ToInt32(tempRS.Get_Fields("ShortDescription")) != 40)
                {
                    tempRS.Edit();
                    tempRS.Set_Fields("ShortDescription", 40);
                    tempRS.Set_Fields("Description", "DISABLED VET PARKING COMMERCIAL (LOSS OF LEGS)");
                    tempRS.Update();
                }
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "V2");
                tempRS.Set_Fields("BMVCode", "VX");
                tempRS.Set_Fields("Description", "DISABLED VET PARKING COMMERCIAL (LOSS OF LEGS)");
                tempRS.Set_Fields("ExciseRequired", "N");
                tempRS.Set_Fields("RegistrationFee", 0);
                tempRS.Set_Fields("OtherFeesNew", 0);
                tempRS.Set_Fields("Otherfeesrenew", 0);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 3);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 2);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "R");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 40);
                tempRS.Update();
            }

            //Application.DoEvents();
            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'VX' AND SystemCode = 'V4'");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                if (FCConvert.ToInt32(tempRS.Get_Fields("ShortDescription")) != 40)
                {
                    tempRS.Edit();
                    tempRS.Set_Fields("ShortDescription", 40);
                    tempRS.Set_Fields("Description", "DISABLED VET PARKING COMMERCIAL (100% DISAB)");
                    tempRS.Update();
                }
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "V4");
                tempRS.Set_Fields("BMVCode", "VX");
                tempRS.Set_Fields("Description", "DISABLED VET PARKING COMMERCIAL (100% DISAB)");
                tempRS.Set_Fields("ExciseRequired", "Y");
                tempRS.Set_Fields("RegistrationFee", 0);
                tempRS.Set_Fields("OtherFeesNew", 0);
                tempRS.Set_Fields("Otherfeesrenew", 0);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 3);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 2);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "R");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 40);
                tempRS.Update();
            }

            //Application.DoEvents();
            // Fix the Disabled Veteran Description in InventoryMaster
            tempRS.OpenRecordset("SELECT * FROM InventoryMaster WHERE MonthYearClass = 'DV'");
            if (!tempRS.EndOfFile() && !tempRS.BeginningOfFile())
            {
                if (Strings.Right(FCConvert.ToString(tempRS.Get_Fields("Description")), 2) != "AN")
                {
                    tempRS.Edit();
                    tempRS.Set_Fields("Description", "DISABLED VETERAN");
                    tempRS.Update();
                }
            }

            //Application.DoEvents();
        }

        public bool FixPeriodCloseoutKeyField()
        {
            bool FixPeriodCloseoutKeyField = false;
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                string strSql;
                strSql =
                    "If exists(Select column_name from information_schema.columns where column_name = N'PeriodCloseOutKey' and table_name = N'LongTermTrailerRegistrations') " +
                    "\r\n";
                strSql +=
                    "EXEC sp_rename 'LongTermTrailerRegistrations.PeriodCloseoutKey', 'PeriodCloseoutID','COLUMN';" +
                    "\r\n";
                // strSQL = strSQL & "GO"
                clsDRWrapper rsSave = new clsDRWrapper();
                rsSave.Execute(strSql, "MotorVehicle");
                FixPeriodCloseoutKeyField = true;
                return FixPeriodCloseoutKeyField;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }

            return FixPeriodCloseoutKeyField;
        }

        public bool AddCompanyCode()
        {
            bool AddCompanyCode = false;
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                string strSql;
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'CompanyCode' and table_name = N'FleetMaster') " +
                    "\r\n";
                strSql += " Alter table FleetMaster add CompanyCode nvarchar(255) null";
                clsDRWrapper rsSave = new clsDRWrapper();
                rsSave.Execute(strSql, "MotorVehicle");
                AddCompanyCode = true;
                return AddCompanyCode;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }

            return AddCompanyCode;
        }

        public bool AddInventoryLock()
        {
            bool AddInventoryLock = false;
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                string strSql;
                clsDRWrapper rsSave = new clsDRWrapper();
                strSql = "If not exists(select * from information_schema.tables where table_name = N'InventoryLock') " +
                         "\r\n";
                strSql += "Create Table [dbo].[InventoryLock] (";
                strSql += "[ID] [int] IDENTITY(1,1) NOT NULL,";
                strSql += "[MasterLock] [bit] NULL,";
                strSql += "[OpID][nvarchar] (255) NULL,";
                strSql +=
                    " Constraint [PK_InventoryLock] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
                rsSave.Execute(strSql, "MotorVehicle", false);
                AddInventoryLock = true;
                return AddInventoryLock;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }

            return AddInventoryLock;
        }

        public bool AddTwentyFiveYearPlate()
        {
            bool AddTwentyFiveYearPlate = false;
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                string strSql;
                clsDRWrapper rsSave = new clsDRWrapper();
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'TwentyFiveYearPlate' and table_name = N'Master') " +
                    "\r\n";
                strSql += " Alter table Master add TwentyFiveYearPlate bit null";
                rsSave.Execute(strSql, "MotorVehicle");
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'TwentyFiveYearPlate' and table_name = N'ArchiveMaster') " +
                    "\r\n";
                strSql += " Alter table ArchiveMaster add TwentyFiveYearPlate bit null";
                rsSave.Execute(strSql, "MotorVehicle");
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'TwentyFiveYearPlate' and table_name = N'ActivityMaster') " +
                    "\r\n";
                strSql += " Alter table ActivityMaster add TwentyFiveYearPlate bit null";
                rsSave.Execute(strSql, "MotorVehicle");
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'TwentyFiveYearPlate' and table_name = N'PendingActivityMaster') " +
                    "\r\n";
                strSql += " Alter table PendingActivityMaster add TwentyFiveYearPlate bit null";
                rsSave.Execute(strSql, "MotorVehicle");
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'TwentyFiveYearPlate' and table_name = N'tblMasterTemp') " +
                    "\r\n";
                strSql += " Alter table tblMasterTemp add TwentyFiveYearPlate bit null";
                rsSave.Execute(strSql, "MotorVehicle");
                AddTwentyFiveYearPlate = true;
                return AddTwentyFiveYearPlate;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }

            return AddTwentyFiveYearPlate;
        }

        private bool FixStrippedPlate()
        {
            bool FixStrippedPlate = false;
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                string strSql;
                clsDRWrapper rsSave = new clsDRWrapper();
                strSql = "update master set platestripped = Replace(platestripped, ' ','')";
                rsSave.Execute(strSql, "MotorVehicle");
                FixStrippedPlate = true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }

            return FixStrippedPlate;
        }

        // kk05012018 tromv-1361  Implement new version (2017) of use tax certificate.
        // Change the old exempt code E (Other) to X to allow for new codes that were inserted as E and F.
        // Split the Purchaser Name into last and first names.
        public bool UpdateUseTaxCertificate()
        {
            bool UpdateUseTaxCertificate = false;
            try
            {
                // On Error GoTo ErrorHandler
                fecherFoundation.Information.Err().Clear();
                string strSql;
                clsDRWrapper rsSave = new clsDRWrapper();
                // Add PurchaserLName
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'PurchaserLName' and table_name = N'UseTax') " +
                    "\r\n";
                strSql += " Alter table UseTax add PurchaserLName nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");
                // Rename PurchaserName to PurchaserFName
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'PurchaserFName' and table_name = N'UseTax') " +
                    "\r\n";
                strSql += " Execute sp_rename N'dbo.UseTax.PurchaserName', N'PurchaserFName', 'COLUMN'";
                rsSave.Execute(strSql, "MotorVehicle");
                strSql =
                    "If not exists(Select column_name from information_schema.columns where column_name = N'ExemptSalesTaxNumber' and table_name = N'UseTax') " +
                    "\r\n";
                strSql += " Alter table UseTax add ExemptSalesTaxNumber nvarchar(255) null";
                rsSave.Execute(strSql, "MotorVehicle");
                rsSave.OpenRecordset("SELECT * FROM UseTax WHERE ExemptCode = 'E'", "twmv0000.vb1");
                while (!rsSave.EndOfFile())
                {
                    rsSave.Set_Fields("ExemptCode", "X");
                    rsSave.Update();
                    rsSave.MoveNext();
                }

                // Force to update to new (2017) Use Tax Form
                rsSave.OpenRecordset("SELECT * FROM WMV", "twmv0000.vb1");
                rsSave.Set_Fields("UseTaxFormVersion", 3);
                rsSave.Update();
                UpdateUseTaxCertificate = true;
                return UpdateUseTaxCertificate;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }

            return UpdateUseTaxCertificate;
        }

        // tromvs-97 6.25.18 Add EM
        public bool Jan2018AddEMPlateClass()
        {
            bool Jan2018AddEMPlateClass = false;
            AddEMClass();
            Jan2018AddEMPlateClass = true;
            return Jan2018AddEMPlateClass;
        }

        private void AddEMClass()
        {
            clsDRWrapper tempRS = new clsDRWrapper();
            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'EM' AND SystemCode = 'E1'", "MotorVehicle");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                // tempRS.Close
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "E1");
                tempRS.Set_Fields("BMVCode", "EM");
                tempRS.Set_Fields("Description", "EMERGENCY MEDICAL SERVICES (6000 LBS GVW)");
                tempRS.Set_Fields("ExciseRequired", "Y");
                tempRS.Set_Fields("RegistrationFee", 35);
                tempRS.Set_Fields("OtherFeesNew", 5);
                tempRS.Set_Fields("Otherfeesrenew", 35);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 2);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 2);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "R");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 5);
                tempRS.Update();
                //Application.DoEvents();
                // tempRS.Close
            }

            tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'EM' AND SystemCode = 'E2'", "MotorVehicle");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                // tempRS.Close
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("SystemCode", "E2");
                tempRS.Set_Fields("BMVCode", "EM");
                tempRS.Set_Fields("Description", "EMERGENCY MEDICAL SERVICES (10000 LBS GVW)");
                tempRS.Set_Fields("ExciseRequired", "Y");
                tempRS.Set_Fields("RegistrationFee", 37);
                tempRS.Set_Fields("OtherFeesNew", 5);
                tempRS.Set_Fields("Otherfeesrenew", 37);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 2);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 2);
                tempRS.Set_Fields("renewaldate", "S");
                tempRS.Set_Fields("TitleRequired", "R");
                tempRS.Set_Fields("Emission", "R");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 39);
                tempRS.Update();
                //Application.DoEvents();
                // tempRS.Close
            }

            tempRS.OpenRecordset("SELECT * FROM InventoryMaster WHERE MonthYearClass = 'EM'");
            if (tempRS.EndOfFile() != true && tempRS.BeginningOfFile() != true)
            {
                //Application.DoEvents();
                // tempRS.Close
            }
            else
            {
                tempRS.AddNew();
                tempRS.Set_Fields("Type", "P");
                tempRS.Set_Fields("MonthYearX", "X");
                tempRS.Set_Fields("singledouble", "D");
                tempRS.Set_Fields("MonthYearClass", "EM");
                tempRS.Set_Fields("Description", "EMERGENCY MEDICAL SERVICES");
                tempRS.Update();
            }
        }

        private bool UpdatesForConceptualDesignSept2019()
        {
            bool UpdatesForConceptualDesignSept2019 = false;
            try
            {
                clsDRWrapper tempRS = new clsDRWrapper();

                tempRS.OpenRecordset("SELECT * FROM DefaultInfo", "twmv0000.vb1");
                if (!tempRS.EndOfFile())
                {
                    tempRS.Set_Fields("AgentFeeReRegLocal", 5);
                    tempRS.Set_Fields("AgentFeeReRegOOT", 6);
                    tempRS.Set_Fields("AgentFeeNewLocal", 6);
                    tempRS.Set_Fields("AgentFeeNewOOT", 7);
                    tempRS.Set_Fields("OutOfRotation", 25);
                    tempRS.Set_Fields("AgentFeeLongTermTrailerDupReg", 5);
                    tempRS.Set_Fields("DuplicateRegistration", 5);
                    tempRS.Set_Fields("DuplicateBoosterFee", 5);
                    tempRS.Update();
                }

                tempRS.OpenRecordset("SELECT * FROM ReasonCodes WHERE Code = '2'", "twmv0000.vb1");
                if (!tempRS.EndOfFile())
                {
                    tempRS.Set_Fields("Include", 0);
                    tempRS.Update();
                }

                // No Reg Fees for Gold Star Plates
                tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'GS' AND SystemCode = 'G1'");
                if (!tempRS.EndOfFile() && !tempRS.BeginningOfFile())
                {
                    if (tempRS.Get_Fields("RegistrationFee") != 0)
                    {
                        tempRS.Set_Fields("RegistrationFee", 0);
                        tempRS.Update();
                    }
                }
                else
                {
                    tempRS.AddNew();
                    tempRS.Set_Fields("BMVCode", "GS");
                    tempRS.Set_Fields("SystemCode", "G1");
                    tempRS.Set_Fields("Description", "GOLD STAR FAMILY (6000 LBS RVW)");
                    tempRS.Set_Fields("ExciseRequired", "Y");
                    tempRS.Set_Fields("RegistrationFee", 0);
                    tempRS.Set_Fields("OtherFeesNew", 0);
                    tempRS.Set_Fields("OtherFeesRenew", 0);
                    tempRS.Set_Fields("HalfRateAllowed", "Y");
                    tempRS.Set_Fields("TwoYear", "N");
                    tempRS.Set_Fields("LocationNew", 3);
                    tempRS.Set_Fields("LocationTransfer", 1);
                    tempRS.Set_Fields("LocationRenewal", 1);
                    tempRS.Set_Fields("NumberOfPlateStickers", 2);
                    tempRS.Set_Fields("RenewalDate", "S");
                    tempRS.Set_Fields("TitleRequired", "R");
                    tempRS.Set_Fields("Emission", "R");
                    tempRS.Set_Fields("InsuranceRequired", "Y");
                    tempRS.Set_Fields("ShortDescription", 5);
                    tempRS.Update();
                }

                tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'GS' AND SystemCode = 'G2'");
                if (!tempRS.EndOfFile() && !tempRS.BeginningOfFile())
                {
                    if (tempRS.Get_Fields("RegistrationFee") != 0)
                    {
                        tempRS.Set_Fields("RegistrationFee", 0);
                        tempRS.Update();
                    }
                    else
                    {
                        tempRS.AddNew();
                        tempRS.Set_Fields("BMVCode", "GS");
                        tempRS.Set_Fields("SystemCode", "G2");
                        tempRS.Set_Fields("Description", "GOLD STAR FAMILY (10000 LBS RVW)");
                        tempRS.Set_Fields("ExciseRequired", "Y");
                        tempRS.Set_Fields("RegistrationFee", 0);
                        tempRS.Set_Fields("OtherFeesNew", 0);
                        tempRS.Set_Fields("OtherFeesRenew", 0);
                        tempRS.Set_Fields("HalfRateAllowed", "Y");
                        tempRS.Set_Fields("TwoYear", "N");
                        tempRS.Set_Fields("LocationNew", 3);
                        tempRS.Set_Fields("LocationTransfer", 1);
                        tempRS.Set_Fields("LocationRenewal", 1);
                        tempRS.Set_Fields("NumberOfPlateStickers", 2);
                        tempRS.Set_Fields("RenewalDate", "S");
                        tempRS.Set_Fields("TitleRequired", "R");
                        tempRS.Set_Fields("Emission", "R");
                        tempRS.Set_Fields("InsuranceRequired", "Y");
                        tempRS.Set_Fields("ShortDescription", 39);
                        tempRS.Update();
                    }
                }

                UpdatesForConceptualDesignSept2019 = true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                UpdatesForConceptualDesignSept2019 = false;
            }

            return UpdatesForConceptualDesignSept2019;
        }

        private bool PreparePeriodCloseOutTable()
        {
            bool preparePeriodCloseOutTable = true;
            try
            {
                clsDRWrapper tempRS = new clsDRWrapper();
                tempRS.OpenRecordset("SELECT COUNT(ID) AS RecordCount FROM PeriodCloseout", "MotorVehicle");
                if (tempRS.Get_Fields_Int16("RecordCount") == 0)
                {
                    tempRS.OpenRecordset("SELECT * FROM PeriodCloseout WHERE ID = -1", "MotorVehicle");
                    tempRS.AddNew();
                    tempRS.Set_Fields("OPID", "988");
                    tempRS.Set_Fields("IssueDate", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
                    tempRS.Update();
                }
                tempRS.DisposeOf();
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                preparePeriodCloseOutTable = false;
            }
            return preparePeriodCloseOutTable;
        }

        private bool UpdateForAutocycle()
        {
            bool updateForAutocycle = true;

            try
            {
                clsDRWrapper tempRS = new clsDRWrapper();
                tempRS.OpenRecordset("SELECT * FROM TownSummaryHeadings WHERE Heading = 'AUTOCYCLE'");
                if (tempRS.EndOfFile())
                {
                    tempRS.AddNew();
                }

                tempRS.Set_Fields("Heading", "AUTOCYCLE");
                tempRS.Set_Fields("CategoryCode", 18);
                tempRS.Update();

                tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'AU' AND SystemCode = 'AU'");
                if (tempRS.EndOfFile())
                {
                    tempRS.AddNew();
                }

                tempRS.Set_Fields("BMVCode", "AU");
                tempRS.Set_Fields("SystemCode", "AU");
                tempRS.Set_Fields("Description", "AUTOCYCLE");
                tempRS.Set_Fields("ExciseRequired", "Y");
                tempRS.Set_Fields("RegistrationFee", 21);
                tempRS.Set_Fields("OtherFeesNew", 0);
                tempRS.Set_Fields("OtherFeesRenew", 0);
                tempRS.Set_Fields("HalfRateAllowed", "Y");
                tempRS.Set_Fields("TwoYear", "N");
                tempRS.Set_Fields("LocationNew", 3);
                tempRS.Set_Fields("LocationTransfer", 1);
                tempRS.Set_Fields("LocationRenewal", 1);
                tempRS.Set_Fields("NumberOfPlateStickers", 1);
                tempRS.Set_Fields("RenewalDate", "F");
                tempRS.Set_Fields("TitleRequired", "V");
                tempRS.Set_Fields("Emission", "E");
                tempRS.Set_Fields("InsuranceRequired", "Y");
                tempRS.Set_Fields("ShortDescription", 18);
                tempRS.Set_Fields("FixedMonth", 3);
                tempRS.Update();
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                updateForAutocycle = false;
            }

            return updateForAutocycle;
        }

        private bool CleanBadPartyIDs()
        {
	        bool CleanBadPartyIDs = true;
	        try
	        {
		        clsDRWrapper tempRS = new clsDRWrapper();
		        tempRS.OpenRecordset("SELECT * FROM Master WHERE OwnerCode2 = 'N' AND PartyID2 > 0");
		        if (!tempRS.EndOfFile())
		        {
			        tempRS.Execute("UPDATE Master SET PartyID2 = 0 WHERE OwnerCode2 = 'N' AND PartyID2 > 0", "MotorVehicle");
			        tempRS.Execute("UPDATE Master SET PartyID3 = 0 WHERE OwnerCode3 = 'N' AND PartyID3 > 0", "MotorVehicle");
			        tempRS.Execute("UPDATE ActivityMaster SET PartyID2 = 0 WHERE OwnerCode2 = 'N' AND PartyID2 > 0", "MotorVehicle");
			        tempRS.Execute("UPDATE ActivityMaster SET PartyID3 = 0 WHERE OwnerCode3 = 'N' AND PartyID3 > 0", "MotorVehicle");
			        tempRS.Execute("UPDATE ArchiveMaster SET PartyID2 = 0 WHERE OwnerCode2 = 'N' AND PartyID2 > 0", "MotorVehicle");
			        tempRS.Execute("UPDATE ArchiveMaster SET PartyID3 = 0 WHERE OwnerCode3 = 'N' AND PartyID3 > 0", "MotorVehicle");
			        tempRS.Execute("UPDATE FleetMaster SET PartyID2 = 0 WHERE OwnerCode2 = 'N' AND PartyID2 > 0", "MotorVehicle");
			        tempRS.Execute("UPDATE FleetMaster SET PartyID3 = 0 WHERE OwnerCode3 = 'N' AND PartyID3 > 0", "MotorVehicle");
				}
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                CleanBadPartyIDs = false;
			}
	        return CleanBadPartyIDs;
        }

        private enum RptCat
        {
			PassengerCar = 5,
			PassengerTruck = 39,
			Commercial = 40
        }

		public bool ResetVeteranClassesForTROWMV305()
		{
			bool returnValue = true;
			clsDRWrapper tempRS = new clsDRWrapper();
	        try
			{
				tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'DS' AND SystemCode = 'D1'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Decimal("RegistrationFee") != 35m ||
			            tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.PassengerCar ||
			            !tempRS.Get_Fields_String("Description").EndsWith("(6000 LBS RVW)"))
			        {
				        tempRS.Set_Fields("RegistrationFee", 35);
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerCar);
				        tempRS.Set_Fields("Description", "DISABILITY SPECIAL VETERAN (6000 LBS RVW)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "D1");
			        tempRS.Set_Fields("BMVCode", "DS");
			        tempRS.Set_Fields("Description", "DISABILITY SPECIAL VETERAN (6000 LBS RVW)");
			        tempRS.Set_Fields("ExciseRequired", "Y");
			        tempRS.Set_Fields("RegistrationFee", 35);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 2);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerCar);
			        tempRS.Update();
		        }

				tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'DS' AND SystemCode = 'D2'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Decimal("RegistrationFee") != 37m ||
			            tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.PassengerTruck ||
			            !tempRS.Get_Fields_String("Description").EndsWith("(10000 LBS RVW)"))
			        {
				        tempRS.Set_Fields("RegistrationFee", 37);
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerTruck);
				        tempRS.Set_Fields("Description", "DISABILITY SPECIAL VETERAN (10000 LBS RVW)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "D2");
			        tempRS.Set_Fields("BMVCode", "DS");
			        tempRS.Set_Fields("Description", "DISABILITY SPECIAL VETERAN (10000 LBS RVW)");
			        tempRS.Set_Fields("ExciseRequired", "Y");
			        tempRS.Set_Fields("RegistrationFee", 37);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 2);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerTruck);
			        tempRS.Update();
		        }

		        tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'DS' AND SystemCode = 'D3'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Decimal("RegistrationFee") != 0.01m ||
			            tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.Commercial ||
			            !tempRS.Get_Fields_String("Description").EndsWith("COMMERCIAL"))
			        {
				        tempRS.Set_Fields("RegistrationFee", 0.01);
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.Commercial);
				        tempRS.Set_Fields("Description", "DISABILITY SPECIAL VETERAN COMMERCIAL");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "D3");
			        tempRS.Set_Fields("BMVCode", "DS");
			        tempRS.Set_Fields("Description", "DISABILITY SPECIAL VETERAN COMMERCIAL");
			        tempRS.Set_Fields("ExciseRequired", "Y");
			        tempRS.Set_Fields("RegistrationFee", 0.01);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 2);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.Commercial);
			        tempRS.Update();
		        }

		        tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'DV' AND SystemCode = 'D1'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.PassengerCar ||
			            !tempRS.Get_Fields_String("Description").EndsWith("(LOSS OF LEGS - 6000 LBS RVW)"))
			        {
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerCar);
				        tempRS.Set_Fields("Description", "DISABLED VET (LOSS OF LEGS - 6000 LBS RVW)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "D1");
			        tempRS.Set_Fields("BMVCode", "DV");
			        tempRS.Set_Fields("Description", "DISABLED VET (LOSS OF LEGS - 6000 LBS RVW)");
			        tempRS.Set_Fields("ExciseRequired", "N");
			        tempRS.Set_Fields("RegistrationFee", 0);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 3);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerCar);
			        tempRS.Update();
		        }

		        tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'DV' AND SystemCode = 'D2'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.PassengerTruck ||
			            !tempRS.Get_Fields_String("Description").EndsWith("(LOSS OF LEGS - 10000 LBS RVW)"))
			        {
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerTruck);
				        tempRS.Set_Fields("Description", "DISABLED VET (LOSS OF LEGS - 10000 LBS RVW)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "D2");
			        tempRS.Set_Fields("BMVCode", "DV");
			        tempRS.Set_Fields("Description", "DISABLED VET (LOSS OF LEGS - 10000 LBS RVW)");
			        tempRS.Set_Fields("ExciseRequired", "N");
			        tempRS.Set_Fields("RegistrationFee", 0);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 3);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerTruck);
			        tempRS.Update();
		        }

		        tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'DV' AND SystemCode = 'D3'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.PassengerCar ||
			            !tempRS.Get_Fields_String("Description").EndsWith("(100% DISAB - 6000 LBS RVW)"))
			        {
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerCar);
				        tempRS.Set_Fields("Description", "DISABLED VET (100% DISAB - 6000 LBS RVW)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "D3");
			        tempRS.Set_Fields("BMVCode", "DV");
			        tempRS.Set_Fields("Description", "DISABLED VET (100% DISAB - 6000 LBS RVW)");
			        tempRS.Set_Fields("ExciseRequired", "Y");
			        tempRS.Set_Fields("RegistrationFee", 0);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 3);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerCar);
			        tempRS.Update();
		        }

		        tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'DV' AND SystemCode = 'D4'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.PassengerTruck ||
			            !tempRS.Get_Fields_String("Description").EndsWith("(100% DISAB - 10000 LBS RVW)"))
			        {
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerTruck);
				        tempRS.Set_Fields("Description", "DISABLED VET (100% DISAB - 10000 LBS RVW)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "D4");
			        tempRS.Set_Fields("BMVCode", "DV");
			        tempRS.Set_Fields("Description", "DISABLED VET (100% DISAB - 10000 LBS RVW)");
			        tempRS.Set_Fields("ExciseRequired", "Y");
			        tempRS.Set_Fields("RegistrationFee", 0);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 3);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerTruck);
			        tempRS.Update();
		        }

		        tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'DV' AND SystemCode = 'D5'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.Commercial ||
			            !tempRS.Get_Fields_String("Description").EndsWith("COMMERCIAL (LOSS OF LEGS)"))
			        {
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.Commercial);
				        tempRS.Set_Fields("Description", "DISABLED VET COMMERCIAL (LOSS OF LEGS)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "D5");
			        tempRS.Set_Fields("BMVCode", "DV");
			        tempRS.Set_Fields("Description", "DISABLED VET COMMERCIAL (LOSS OF LEGS)");
			        tempRS.Set_Fields("ExciseRequired", "N");
			        tempRS.Set_Fields("RegistrationFee", 0);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 3);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.Commercial);
			        tempRS.Update();
		        }

		        tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'DV' AND SystemCode = 'D6'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.Commercial ||
			            !tempRS.Get_Fields_String("Description").EndsWith("COMMERCIAL (100% DISAB)"))
			        {
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.Commercial);
				        tempRS.Set_Fields("Description", "DISABLED VET COMMERCIAL (100% DISAB)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "D6");
			        tempRS.Set_Fields("BMVCode", "DV");
			        tempRS.Set_Fields("Description", "DISABLED VET COMMERCIAL (100% DISAB)");
			        tempRS.Set_Fields("ExciseRequired", "Y");
			        tempRS.Set_Fields("RegistrationFee", 0);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 3);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.Commercial);
			        tempRS.Update();
		        }

		        tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'VT' AND SystemCode = 'V1'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Decimal("RegistrationFee") != 35m ||
			            tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.PassengerCar ||
			            !tempRS.Get_Fields_String("Description").EndsWith("(6000 LBS RVW)"))
			        {
				        tempRS.Set_Fields("RegistrationFee", 35);
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerCar);
				        tempRS.Set_Fields("Description", "VETERAN (6000 LBS RVW)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "V1");
			        tempRS.Set_Fields("BMVCode", "VT");
			        tempRS.Set_Fields("Description", "VETERAN (6000 LBS RVW)");
			        tempRS.Set_Fields("ExciseRequired", "Y");
			        tempRS.Set_Fields("RegistrationFee", 35);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 1);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerCar);
			        tempRS.Update();
		        }

				tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'VT' AND SystemCode = 'V2'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Decimal("RegistrationFee") != 37m ||
			            tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.PassengerTruck ||
			            !tempRS.Get_Fields_String("Description").EndsWith("(10000 LBS RVW)"))
			        {
				        tempRS.Set_Fields("RegistrationFee", 37);
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerTruck);
				        tempRS.Set_Fields("Description", "VETERAN (10000 LBS RVW)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "V2");
			        tempRS.Set_Fields("BMVCode", "VT");
			        tempRS.Set_Fields("Description", "VETERAN (10000 LBS RVW)");
			        tempRS.Set_Fields("ExciseRequired", "Y");
			        tempRS.Set_Fields("RegistrationFee", 37);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 1);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerTruck);
			        tempRS.Update();
		        }

		        tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'VT' AND SystemCode = 'V3'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Decimal("RegistrationFee") != 0.01m ||
			            tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.Commercial ||
						!tempRS.Get_Fields_String("Description").EndsWith("COMMERCIAL"))
			        {
				        tempRS.Edit();
				        tempRS.Set_Fields("RegistrationFee", 0.01);
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.Commercial);
				        tempRS.Set_Fields("Description", "VETERAN COMMERCIAL");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "V3");
			        tempRS.Set_Fields("BMVCode", "VT");
			        tempRS.Set_Fields("Description", "VETERAN COMMERCIAL");
			        tempRS.Set_Fields("ExciseRequired", "Y");
			        tempRS.Set_Fields("RegistrationFee", 0.01);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 1);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.Commercial);
			        tempRS.Update();
		        }

		        tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'VX' AND SystemCode = 'V1'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.PassengerCar ||
			            !tempRS.Get_Fields_String("Description").EndsWith("(LOSS OF LEGS - 6000 LBS RVW)"))
			        {
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerCar);
				        tempRS.Set_Fields("Description", "DISABLED VET PARKING (LOSS OF LEGS - 6000 LBS RVW)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "V1");
			        tempRS.Set_Fields("BMVCode", "VX");
			        tempRS.Set_Fields("Description", "DISABLED VET PARKING (LOSS OF LEGS - 6000 LBS RVW)");
			        tempRS.Set_Fields("ExciseRequired", "N");
			        tempRS.Set_Fields("RegistrationFee", 0);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 3);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerCar);
			        tempRS.Update();
		        }

				tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'VX' AND SystemCode = 'V2'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.PassengerTruck ||
			            !tempRS.Get_Fields_String("Description").EndsWith("(LOSS OF LEGS - 10000 LBS RVW)"))
			        {
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerTruck);
				        tempRS.Set_Fields("Description", "DISABLED VET PARKING (LOSS OF LEGS - 10000 LBS RVW)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "V2");
			        tempRS.Set_Fields("BMVCode", "VX");
			        tempRS.Set_Fields("Description", "DISABLED VET PARKING (LOSS OF LEGS - 10000 LBS RVW)");
			        tempRS.Set_Fields("ExciseRequired", "N");
			        tempRS.Set_Fields("RegistrationFee", 0);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 3);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerTruck);
			        tempRS.Update();
		        }

		        tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'VX' AND SystemCode = 'V3'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.PassengerCar ||
			            !tempRS.Get_Fields_String("Description").EndsWith("(100% DISAB - 6000 LBS RVW)"))
			        {
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerCar);
				        tempRS.Set_Fields("Description", "DISABLED VET PARKING (100% DISAB - 6000 LBS RVW)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "V3");
			        tempRS.Set_Fields("BMVCode", "VX");
			        tempRS.Set_Fields("Description", "DISABLED VET PARKING (100% DISAB - 6000 LBS RVW)");
			        tempRS.Set_Fields("ExciseRequired", "Y");
			        tempRS.Set_Fields("RegistrationFee", 0);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 3);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerCar);
			        tempRS.Update();
		        }

		        tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'VX' AND SystemCode = 'V4'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.PassengerTruck||
			            !tempRS.Get_Fields_String("Description").EndsWith("(100% DISAB - 10000 LBS RVW)"))
			        {
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerTruck);
				        tempRS.Set_Fields("Description", "DISABLED VET PARKING (100% DISAB - 10000 LBS RVW)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "V4");
			        tempRS.Set_Fields("BMVCode", "VX");
			        tempRS.Set_Fields("Description", "DISABLED VET PARKING (100% DISAB - 10000 LBS RVW)");
			        tempRS.Set_Fields("ExciseRequired", "Y");
			        tempRS.Set_Fields("RegistrationFee", 0);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 3);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.PassengerTruck);
			        tempRS.Update();
		        }

		        tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'VX' AND SystemCode = 'V5'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.Commercial ||
			            !tempRS.Get_Fields_String("Description").EndsWith("COMMERCIAL (LOSS OF LEGS)"))
			        {
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.Commercial);
				        tempRS.Set_Fields("Description", "DISABLED VET PARKING COMMERCIAL (LOSS OF LEGS)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "V5");
			        tempRS.Set_Fields("BMVCode", "VX");
			        tempRS.Set_Fields("Description", "DISABLED VET PARKING COMMERCIAL (LOSS OF LEGS)");
			        tempRS.Set_Fields("ExciseRequired", "N");
			        tempRS.Set_Fields("RegistrationFee", 0);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 3);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.Commercial);
			        tempRS.Update();
		        }

		        tempRS.OpenRecordset("SELECT * FROM Class WHERE BMVCode = 'VX' AND SystemCode = 'V6'");
		        if (!tempRS.EndOfFile())
		        {
			        if (tempRS.Get_Fields_Int32("ShortDescription") != (int)RptCat.Commercial ||
			            !tempRS.Get_Fields_String("Description").EndsWith("COMMERCIAL (100% DISAB)"))
			        {
				        tempRS.Set_Fields("ShortDescription", (int)RptCat.Commercial);
				        tempRS.Set_Fields("Description", "DISABLED VET PARKING COMMERCIAL (100% DISAB)");
				        tempRS.Update();
			        }
		        }
		        else
		        {
			        tempRS.AddNew();
			        tempRS.Set_Fields("SystemCode", "V6");
			        tempRS.Set_Fields("BMVCode", "VX");
			        tempRS.Set_Fields("Description", "DISABLED VET PARKING COMMERCIAL (100% DISAB)");
			        tempRS.Set_Fields("ExciseRequired", "Y");
			        tempRS.Set_Fields("RegistrationFee", 0);
			        tempRS.Set_Fields("OtherFeesNew", 0);
			        tempRS.Set_Fields("OtherFeesRenew", 0);
			        tempRS.Set_Fields("HalfRateAllowed", "Y");
			        tempRS.Set_Fields("TwoYear", "N");
			        tempRS.Set_Fields("LocationNew", 3);
			        tempRS.Set_Fields("LocationTransfer", 1);
			        tempRS.Set_Fields("LocationRenewal", 1);
			        tempRS.Set_Fields("NumberOfPlateStickers", 2);
			        tempRS.Set_Fields("RenewalDate", "S");
			        tempRS.Set_Fields("TitleRequired", "R");
			        tempRS.Set_Fields("Emission", "R");
			        tempRS.Set_Fields("InsuranceRequired", "Y");
			        tempRS.Set_Fields("ShortDescription", (int)RptCat.Commercial);
			        tempRS.Update();
		        }
			}
	        catch (Exception ex)
	        {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                returnValue = false;
	        }

	        tempRS.DisposeOf();
	        return returnValue;
        }

    }
}
