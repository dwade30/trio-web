using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Wisej.Web;
using System.Runtime.InteropServices;

namespace Global
{
    /// <summary>
    /// Summary description for frmCLGetAccount.
    /// </summary>
    partial partial class frmCLGetAccount : BaseForm
    {
        public fecherFoundation.FCComboBox cmbSearchPlace;
        public fecherFoundation.FCComboBox cmbSearchType;
        public fecherFoundation.FCComboBox cmbRE;
        public fecherFoundation.FCComboBox cmbPeriod1;
        public fecherFoundation.FCFrame fraRecover;
        public fecherFoundation.FCButton cmdBatchChoice;
        public fecherFoundation.FCComboBox cmbBatchList;
        public fecherFoundation.FCLabel lblRecover;
        public fecherFoundation.FCFrame fraBatchSearchResults;
        public fecherFoundation.FCGrid vsBatchSearch;
        public fecherFoundation.FCFrame fraBatchSearchCriteria;
        public fecherFoundation.FCButton cmdBatchSearchCancel;
        public fecherFoundation.FCButton cmdBatchSearch;
        public fecherFoundation.FCTextBox txtBatchSearchMapLot;
        public fecherFoundation.FCTextBox txtBatchSearchName;
        public fecherFoundation.FCLabel lblBatchSearchMapLot;
        public fecherFoundation.FCLabel lblBatchSearchName;
        public fecherFoundation.FCPanel fraSearch;
        public fecherFoundation.FCCheckBox chkShowDeletedAccountInfo;
        public fecherFoundation.FCCheckBox chkShowPreviousOwnerInfo;
        public fecherFoundation.FCTextBox txtSearch;
        public fecherFoundation.FCButton cmdSearch;
        public fecherFoundation.FCTextBox txtGetAccountNumber;
        public fecherFoundation.FCButton cmdGetAccountNumber;
        public fecherFoundation.FCLabel lblInstructions1;
        public fecherFoundation.FCGrid vsSearch;
        public fecherFoundation.FCFrame fraValidate;
        public fecherFoundation.FCButton cmdValidateCancel;
        public fecherFoundation.FCButton cmdValidateNo;
        public fecherFoundation.FCButton cmdValidateYes;
        public fecherFoundation.FCLabel lblValidate;
        public fecherFoundation.FCTextBox txtHold;
        public fecherFoundation.FCPanel fraBatch;
        public fecherFoundation.FCButton cmdProcessBatch;
        public fecherFoundation.FCTextBox txtTotal;
        public fecherFoundation.FCTextBox txtAmount;
        public fecherFoundation.FCTextBox txtBatchAccount;
        public fecherFoundation.FCGrid vsBatch;
        public fecherFoundation.FCLabel lblInstructions;
        public fecherFoundation.FCLabel Label11;
        public fecherFoundation.FCLabel lblName;
        public fecherFoundation.FCLabel lblLocation;
        public fecherFoundation.FCLabel Label10;
        public fecherFoundation.FCLabel Label9;
        public fecherFoundation.FCFrame fraBatchQuestions;
        public T2KDateBox txtPaymentDate2;
        public fecherFoundation.FCCheckBox chkReceipt;
        public fecherFoundation.FCTextBox txtTellerID;
        public fecherFoundation.FCButton cmdBatch;
        public fecherFoundation.FCComboBox cmbPeriod;
        public fecherFoundation.FCTextBox txtTaxYear;
        public fecherFoundation.FCTextBox txtPaymentDate;
        public fecherFoundation.FCTextBox txtPaidBy;
        public T2KDateBox txtEffectiveDate;
        public fecherFoundation.FCLabel lblEffectiveDate;
        public fecherFoundation.FCLabel Label8;
        public fecherFoundation.FCLabel Label6;
        public fecherFoundation.FCLabel lblPaymentDate;
        public fecherFoundation.FCLabel Label4;
        public fecherFoundation.FCLabel Label3;
        public fecherFoundation.FCLabel lblSearchListInstruction;
        private fecherFoundation.FCMenuStrip MainMenu1;
        public fecherFoundation.FCToolStripMenuItem mnuProcess;
        public fecherFoundation.FCToolStripMenuItem mnuBatch;
        public fecherFoundation.FCToolStripMenuItem mnuBatchStart;
        public fecherFoundation.FCToolStripMenuItem mnuBatchSave;
        public fecherFoundation.FCToolStripMenuItem mnuFileBatchSearch;
        public fecherFoundation.FCToolStripMenuItem mnuBatchPrint;
        public fecherFoundation.FCToolStripMenuItem mnuBatchRecover;
        public fecherFoundation.FCToolStripMenuItem mnuBatchPurge;
        public fecherFoundation.FCToolStripMenuItem mnuFileImport;
        public fecherFoundation.FCToolStripMenuItem mnuImportFirstAmericanBatch;
        private Wisej.Web.ToolTip ToolTip1;

	 protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCLGetAccount));
            Wisej.Web.JavaScript.ClientEvent clientEvent2 = new Wisej.Web.JavaScript.ClientEvent();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
            Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
            this.cmbSearchPlace = new fecherFoundation.FCComboBox();
            this.cmbSearchType = new fecherFoundation.FCComboBox();
            this.cmbRE = new fecherFoundation.FCComboBox();
            this.cmbPeriod1 = new fecherFoundation.FCComboBox();
            this.fraRecover = new fecherFoundation.FCFrame();
            this.cmdBatchChoice = new fecherFoundation.FCButton();
            this.cmbBatchList = new fecherFoundation.FCComboBox();
            this.lblRecover = new fecherFoundation.FCLabel();
            this.fraBatchSearchResults = new fecherFoundation.FCFrame();
            this.vsBatchSearch = new fecherFoundation.FCGrid();
            this.fraBatchSearchCriteria = new fecherFoundation.FCFrame();
            this.cmdBatchSearchCancel = new fecherFoundation.FCButton();
            this.cmdBatchSearch = new fecherFoundation.FCButton();
            this.txtBatchSearchMapLot = new fecherFoundation.FCTextBox();
            this.txtBatchSearchName = new fecherFoundation.FCTextBox();
            this.lblBatchSearchMapLot = new fecherFoundation.FCLabel();
            this.lblBatchSearchName = new fecherFoundation.FCLabel();
            this.fraSearch = new fecherFoundation.FCPanel();
            this.txtSearch = new fecherFoundation.FCTextBox();
            this.chkShowPreviousOwnerInfo = new fecherFoundation.FCCheckBox();
            this.chkShowDeletedAccountInfo = new fecherFoundation.FCCheckBox();
            this.lblSearchType = new fecherFoundation.FCLabel();
            this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
            this.cmdGetAccountNumber = new fecherFoundation.FCButton();
            this.lblInstructions1 = new fecherFoundation.FCLabel();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.vsSearch = new fecherFoundation.FCGrid();
            this.fraBatchQuestions = new fecherFoundation.FCFrame();
            this.txtPaymentDate2 = new Global.T2KDateBox();
            this.chkReceipt = new fecherFoundation.FCCheckBox();
            this.txtTellerID = new fecherFoundation.FCTextBox();
            this.cmdBatch = new fecherFoundation.FCButton();
            this.cmbPeriod = new fecherFoundation.FCComboBox();
            this.txtTaxYear = new fecherFoundation.FCTextBox();
            this.txtPaymentDate = new fecherFoundation.FCTextBox();
            this.txtPaidBy = new fecherFoundation.FCTextBox();
            this.txtEffectiveDate = new Global.T2KDateBox();
            this.lblEffectiveDate = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.lblPaymentDate = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.fraBatch = new fecherFoundation.FCPanel();
            this.lblPeriod1 = new fecherFoundation.FCLabel();
            this.cmdProcessBatch = new fecherFoundation.FCButton();
            this.txtTotal = new fecherFoundation.FCTextBox();
            this.txtAmount = new fecherFoundation.FCTextBox();
            this.txtBatchAccount = new fecherFoundation.FCTextBox();
            this.vsBatch = new fecherFoundation.FCGrid();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.lblName = new fecherFoundation.FCLabel();
            this.lblLocation = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.fraValidate = new fecherFoundation.FCFrame();
            this.cmdValidateCancel = new fecherFoundation.FCButton();
            this.cmdValidateNo = new fecherFoundation.FCButton();
            this.cmdValidateYes = new fecherFoundation.FCButton();
            this.lblValidate = new fecherFoundation.FCLabel();
            this.txtHold = new fecherFoundation.FCTextBox();
            this.lblSearchListInstruction = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuBatchStart = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBatchSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileBatchSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBatchPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBatchRecover = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBatchPurge = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileImport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuImportFirstAmericanBatch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuBatch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.btnProcess = new fecherFoundation.FCButton();
            this.cmdProcessClearSearch = new fecherFoundation.FCButton();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessGetAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessClearSearch = new fecherFoundation.FCToolStripMenuItem();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRecover)).BeginInit();
            this.fraRecover.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBatchChoice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBatchSearchResults)).BeginInit();
            this.fraBatchSearchResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsBatchSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBatchSearchCriteria)).BeginInit();
            this.fraBatchSearchCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBatchSearchCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBatchSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSearch)).BeginInit();
            this.fraSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPreviousOwnerInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowDeletedAccountInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBatchQuestions)).BeginInit();
            this.fraBatchQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentDate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReceipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBatch)).BeginInit();
            this.fraBatch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsBatch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraValidate)).BeginInit();
            this.fraValidate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdValidateCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdValidateNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdValidateYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessClearSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.btnProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 606);
            this.BottomPanel.Size = new System.Drawing.Size(1128, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraBatchSearchCriteria);
            this.ClientArea.Controls.Add(this.fraRecover);
            this.ClientArea.Controls.Add(this.fraValidate);
            this.ClientArea.Controls.Add(this.fraSearch);
            this.ClientArea.Controls.Add(this.fraBatchSearchResults);
            this.ClientArea.Controls.Add(this.vsSearch);
            this.ClientArea.Controls.Add(this.txtHold);
            this.ClientArea.Controls.Add(this.lblSearchListInstruction);
            this.ClientArea.Controls.Add(this.fraBatch);
            this.ClientArea.Controls.Add(this.fraBatchQuestions);
            this.ClientArea.Size = new System.Drawing.Size(1128, 546);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdProcessClearSearch);
            this.TopPanel.Controls.Add(this.cmdSearch);
            this.TopPanel.Size = new System.Drawing.Size(1128, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSearch, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdProcessClearSearch, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 25);
            this.HeaderText.Size = new System.Drawing.Size(176, 30);
            this.HeaderText.Text = "Select Account";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbSearchPlace
            // 
            this.cmbSearchPlace.AutoSize = false;
            this.cmbSearchPlace.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbSearchPlace.FormattingEnabled = true;
            this.cmbSearchPlace.Items.AddRange(new object[] {
            "Starts With",
            "Contains"});
            this.cmbSearchPlace.Location = new System.Drawing.Point(1082, 47);
            this.cmbSearchPlace.Name = "cmbSearchPlace";
            this.cmbSearchPlace.Size = new System.Drawing.Size(109, 40);
            this.cmbSearchPlace.TabIndex = 76;
            this.cmbSearchPlace.Text = "Starts With";
            this.ToolTip1.SetToolTip(this.cmbSearchPlace, null);
            this.cmbSearchPlace.Visible = false;
            // 
            // cmbSearchType
            // 
            this.cmbSearchType.AutoSize = false;
            this.cmbSearchType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbSearchType.FormattingEnabled = true;
            this.cmbSearchType.Items.AddRange(new object[] {
            "Name",
            "Street Name",
            "Map / Lot"});
            this.cmbSearchType.Location = new System.Drawing.Point(388, 23);
            this.cmbSearchType.Name = "cmbSearchType";
            this.cmbSearchType.Size = new System.Drawing.Size(135, 40);
            this.cmbSearchType.TabIndex = 77;
            this.cmbSearchType.Text = "Name";
            this.ToolTip1.SetToolTip(this.cmbSearchType, null);
            // 
            // cmbRE
            // 
            this.cmbRE.AutoSize = false;
            this.cmbRE.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbRE.FormattingEnabled = true;
            this.cmbRE.Items.AddRange(new object[] {
            "Real Estate",
            "Personal Property"});
            this.cmbRE.Location = new System.Drawing.Point(1121, 16);
            this.cmbRE.Name = "cmbRE";
            this.cmbRE.Size = new System.Drawing.Size(60, 40);
            this.cmbRE.TabIndex = 49;
            this.cmbRE.Text = "Real Estate";
            this.ToolTip1.SetToolTip(this.cmbRE, null);
            this.cmbRE.Visible = false;
            this.cmbRE.SelectedIndexChanged += new System.EventHandler(this.optRE_Click);
            // 
            // cmbPeriod1
            // 
            this.cmbPeriod1.AutoSize = false;
            this.cmbPeriod1.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPeriod1.FormattingEnabled = true;
            this.cmbPeriod1.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cmbPeriod1.Location = new System.Drawing.Point(124, 188);
            this.cmbPeriod1.Name = "cmbPeriod1";
            this.cmbPeriod1.Size = new System.Drawing.Size(121, 40);
            this.cmbPeriod1.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.cmbPeriod1, null);
            this.cmbPeriod1.SelectedIndexChanged += new System.EventHandler(this.optPeriod_Click);
            // 
            // fraRecover
            // 
            this.fraRecover.Controls.Add(this.cmdBatchChoice);
            this.fraRecover.Controls.Add(this.cmbBatchList);
            this.fraRecover.Controls.Add(this.lblRecover);
            this.fraRecover.Location = new System.Drawing.Point(30, 30);
            this.fraRecover.Name = "fraRecover";
            this.fraRecover.Size = new System.Drawing.Size(340, 156);
            this.fraRecover.TabIndex = 27;
            this.fraRecover.Text = "Batch Recover";
            this.ToolTip1.SetToolTip(this.fraRecover, null);
            this.fraRecover.UseMnemonic = false;
            this.fraRecover.Visible = false;
            // 
            // cmdBatchChoice
            // 
            this.cmdBatchChoice.AppearanceKey = "toolbarButton";
            this.cmdBatchChoice.Location = new System.Drawing.Point(127, 112);
            this.cmdBatchChoice.Name = "cmdBatchChoice";
            this.cmdBatchChoice.Size = new System.Drawing.Size(86, 24);
            this.cmdBatchChoice.TabIndex = 13;
            this.cmdBatchChoice.Text = "Purge";
            this.ToolTip1.SetToolTip(this.cmdBatchChoice, null);
            this.cmdBatchChoice.Click += new System.EventHandler(this.cmdBatchChoice_Click);
            // 
            // cmbBatchList
            // 
            this.cmbBatchList.AutoSize = false;
            this.cmbBatchList.BackColor = System.Drawing.SystemColors.Window;
            this.cmbBatchList.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbBatchList.FormattingEnabled = true;
            this.cmbBatchList.Location = new System.Drawing.Point(215, 30);
            this.cmbBatchList.Name = "cmbBatchList";
            this.cmbBatchList.Size = new System.Drawing.Size(105, 40);
            this.cmbBatchList.Sorted = true;
            this.cmbBatchList.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.cmbBatchList, null);
            this.cmbBatchList.DropDown += new System.EventHandler(this.cmbBatchList_DropDown);
            this.cmbBatchList.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbBatchList_KeyDown);
            // 
            // lblRecover
            // 
            this.lblRecover.Location = new System.Drawing.Point(20, 32);
            this.lblRecover.Name = "lblRecover";
            this.lblRecover.Size = new System.Drawing.Size(145, 62);
            this.lblRecover.TabIndex = 28;
            this.ToolTip1.SetToolTip(this.lblRecover, null);
            // 
            // fraBatchSearchResults
            // 
            this.fraBatchSearchResults.AppearanceKey = "groupBoxNoBorder";
            this.fraBatchSearchResults.Controls.Add(this.vsBatchSearch);
            this.fraBatchSearchResults.Location = new System.Drawing.Point(639, 297);
            this.fraBatchSearchResults.Name = "fraBatchSearchResults";
            this.fraBatchSearchResults.Size = new System.Drawing.Size(482, 251);
            this.fraBatchSearchResults.TabIndex = 56;
            this.fraBatchSearchResults.Text = "Multiple Accounts";
            this.ToolTip1.SetToolTip(this.fraBatchSearchResults, null);
            this.fraBatchSearchResults.UseMnemonic = false;
            this.fraBatchSearchResults.Visible = false;
            // 
            // vsBatchSearch
            // 
            this.vsBatchSearch.AllowSelection = false;
            this.vsBatchSearch.AllowUserToResizeColumns = false;
            this.vsBatchSearch.AllowUserToResizeRows = false;
            this.vsBatchSearch.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsBatchSearch.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsBatchSearch.BackColorBkg = System.Drawing.Color.Empty;
            this.vsBatchSearch.BackColorFixed = System.Drawing.Color.Empty;
            this.vsBatchSearch.BackColorSel = System.Drawing.Color.Empty;
            this.vsBatchSearch.Cols = 4;
            this.vsBatchSearch.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsBatchSearch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.vsBatchSearch.ColumnHeadersHeight = 30;
            this.vsBatchSearch.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsBatchSearch.DefaultCellStyle = dataGridViewCellStyle2;
            this.vsBatchSearch.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsBatchSearch.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsBatchSearch.FixedCols = 0;
            this.vsBatchSearch.ForeColorFixed = System.Drawing.SystemColors.ControlText;
            this.vsBatchSearch.FrozenCols = 0;
            this.vsBatchSearch.FrozenRows = 0;
            this.vsBatchSearch.GridColor = System.Drawing.Color.Empty;
            this.vsBatchSearch.Location = new System.Drawing.Point(30, 30);
            this.vsBatchSearch.Name = "vsBatchSearch";
            this.vsBatchSearch.ReadOnly = true;
            this.vsBatchSearch.RowHeadersVisible = false;
            this.vsBatchSearch.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsBatchSearch.RowHeightMin = 0;
            this.vsBatchSearch.Rows = 1;
            this.vsBatchSearch.ShowColumnVisibilityMenu = false;
            this.vsBatchSearch.Size = new System.Drawing.Size(458, 212);
            this.vsBatchSearch.StandardTab = true;
            this.vsBatchSearch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsBatchSearch.TabIndex = 57;
            this.ToolTip1.SetToolTip(this.vsBatchSearch, null);
            this.vsBatchSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.vsBatchSearch_KeyDown);
            this.vsBatchSearch.DoubleClick += new System.EventHandler(this.vsBatchSearch_DblClick);
            // 
            // fraBatchSearchCriteria
            // 
            this.fraBatchSearchCriteria.Controls.Add(this.cmdBatchSearchCancel);
            this.fraBatchSearchCriteria.Controls.Add(this.cmdBatchSearch);
            this.fraBatchSearchCriteria.Controls.Add(this.txtBatchSearchMapLot);
            this.fraBatchSearchCriteria.Controls.Add(this.txtBatchSearchName);
            this.fraBatchSearchCriteria.Controls.Add(this.lblBatchSearchMapLot);
            this.fraBatchSearchCriteria.Controls.Add(this.lblBatchSearchName);
            this.fraBatchSearchCriteria.Location = new System.Drawing.Point(698, 88);
            this.fraBatchSearchCriteria.Name = "fraBatchSearchCriteria";
            this.fraBatchSearchCriteria.Size = new System.Drawing.Size(315, 195);
            this.fraBatchSearchCriteria.TabIndex = 58;
            this.fraBatchSearchCriteria.Text = "Account Search";
            this.ToolTip1.SetToolTip(this.fraBatchSearchCriteria, null);
            this.fraBatchSearchCriteria.UseMnemonic = false;
            this.fraBatchSearchCriteria.Visible = false;
            // 
            // cmdBatchSearchCancel
            // 
            this.cmdBatchSearchCancel.AppearanceKey = "toolbarButton";
            this.cmdBatchSearchCancel.Location = new System.Drawing.Point(176, 147);
            this.cmdBatchSearchCancel.Name = "cmdBatchSearchCancel";
            this.cmdBatchSearchCancel.Size = new System.Drawing.Size(90, 30);
            this.cmdBatchSearchCancel.TabIndex = 64;
            this.cmdBatchSearchCancel.Text = "Cancel";
            this.ToolTip1.SetToolTip(this.cmdBatchSearchCancel, null);
            this.cmdBatchSearchCancel.Click += new System.EventHandler(this.cmdBatchSearchCancel_Click);
            // 
            // cmdBatchSearch
            // 
            this.cmdBatchSearch.AppearanceKey = "toolbarButton";
            this.cmdBatchSearch.Location = new System.Drawing.Point(74, 147);
            this.cmdBatchSearch.Name = "cmdBatchSearch";
            this.cmdBatchSearch.Size = new System.Drawing.Size(90, 30);
            this.cmdBatchSearch.TabIndex = 63;
            this.cmdBatchSearch.Text = "Search";
            this.ToolTip1.SetToolTip(this.cmdBatchSearch, null);
            this.cmdBatchSearch.Click += new System.EventHandler(this.cmdBatchSearch_Click);
            // 
            // txtBatchSearchMapLot
            // 
            this.txtBatchSearchMapLot.AutoSize = false;
            this.txtBatchSearchMapLot.BackColor = System.Drawing.SystemColors.Window;
            this.txtBatchSearchMapLot.Location = new System.Drawing.Point(103, 85);
            this.txtBatchSearchMapLot.Name = "txtBatchSearchMapLot";
            this.txtBatchSearchMapLot.Size = new System.Drawing.Size(194, 40);
            this.txtBatchSearchMapLot.TabIndex = 62;
            this.ToolTip1.SetToolTip(this.txtBatchSearchMapLot, null);
            this.txtBatchSearchMapLot.KeyDown += new Wisej.Web.KeyEventHandler(this.txtBatchSearchMapLot_KeyDown);
            // 
            // txtBatchSearchName
            // 
            this.txtBatchSearchName.AutoSize = false;
            this.txtBatchSearchName.BackColor = System.Drawing.SystemColors.Window;
            this.txtBatchSearchName.Location = new System.Drawing.Point(103, 30);
            this.txtBatchSearchName.Name = "txtBatchSearchName";
            this.txtBatchSearchName.Size = new System.Drawing.Size(194, 40);
            this.txtBatchSearchName.TabIndex = 61;
            this.ToolTip1.SetToolTip(this.txtBatchSearchName, null);
            this.txtBatchSearchName.KeyDown += new Wisej.Web.KeyEventHandler(this.txtBatchSearchName_KeyDown);
            // 
            // lblBatchSearchMapLot
            // 
            this.lblBatchSearchMapLot.Location = new System.Drawing.Point(20, 99);
            this.lblBatchSearchMapLot.Name = "lblBatchSearchMapLot";
            this.lblBatchSearchMapLot.Size = new System.Drawing.Size(66, 18);
            this.lblBatchSearchMapLot.TabIndex = 60;
            this.lblBatchSearchMapLot.Text = "MAPLOT";
            this.ToolTip1.SetToolTip(this.lblBatchSearchMapLot, null);
            // 
            // lblBatchSearchName
            // 
            this.lblBatchSearchName.Location = new System.Drawing.Point(20, 44);
            this.lblBatchSearchName.Name = "lblBatchSearchName";
            this.lblBatchSearchName.Size = new System.Drawing.Size(66, 18);
            this.lblBatchSearchName.TabIndex = 59;
            this.lblBatchSearchName.Text = "NAME";
            this.ToolTip1.SetToolTip(this.lblBatchSearchName, null);
            // 
            // fraSearch
            // 
            this.fraSearch.Controls.Add(this.txtSearch);
            this.fraSearch.Controls.Add(this.chkShowPreviousOwnerInfo);
            this.fraSearch.Controls.Add(this.chkShowDeletedAccountInfo);
            this.fraSearch.Controls.Add(this.lblSearchType);
            this.fraSearch.Controls.Add(this.txtGetAccountNumber);
            this.fraSearch.Controls.Add(this.cmbSearchType);
            this.fraSearch.Controls.Add(this.cmdGetAccountNumber);
            this.fraSearch.Controls.Add(this.cmbSearchPlace);
            this.fraSearch.Controls.Add(this.lblInstructions1);
            this.fraSearch.Controls.Add(this.cmbRE);
            this.fraSearch.Dock = Wisej.Web.DockStyle.Top;
            this.fraSearch.Location = new System.Drawing.Point(0, 0);
            this.fraSearch.Name = "fraSearch";
            this.fraSearch.Size = new System.Drawing.Size(1111, 87);
            this.fraSearch.TabIndex = 16;
            this.ToolTip1.SetToolTip(this.fraSearch, null);
            this.fraSearch.Visible = false;
            // 
            // txtSearch
            // 
            this.txtSearch.AutoSize = false;
            this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearch.Location = new System.Drawing.Point(534, 23);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(181, 40);
            this.txtSearch.TabIndex = 55;
            this.ToolTip1.SetToolTip(this.txtSearch, null);
            this.txtSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // chkShowPreviousOwnerInfo
            // 
            this.chkShowPreviousOwnerInfo.Location = new System.Drawing.Point(945, 29);
            this.chkShowPreviousOwnerInfo.Name = "chkShowPreviousOwnerInfo";
            this.chkShowPreviousOwnerInfo.Size = new System.Drawing.Size(198, 28);
            this.chkShowPreviousOwnerInfo.TabIndex = 68;
            this.chkShowPreviousOwnerInfo.Text = "Show Previous Owners";
            this.ToolTip1.SetToolTip(this.chkShowPreviousOwnerInfo, null);
            // 
            // chkShowDeletedAccountInfo
            // 
            this.chkShowDeletedAccountInfo.Location = new System.Drawing.Point(734, 29);
            this.chkShowDeletedAccountInfo.Name = "chkShowDeletedAccountInfo";
            this.chkShowDeletedAccountInfo.Size = new System.Drawing.Size(202, 28);
            this.chkShowDeletedAccountInfo.TabIndex = 75;
            this.chkShowDeletedAccountInfo.Text = "Show Deleted Accounts";
            this.ToolTip1.SetToolTip(this.chkShowDeletedAccountInfo, null);
            // 
            // lblSearchType
            // 
            this.lblSearchType.AutoSize = true;
            this.lblSearchType.Location = new System.Drawing.Point(287, 38);
            this.lblSearchType.Name = "lblSearchType";
            this.lblSearchType.Size = new System.Drawing.Size(79, 16);
            this.lblSearchType.TabIndex = 78;
            this.lblSearchType.Text = "SEARCH BY";
            this.ToolTip1.SetToolTip(this.lblSearchType, null);
            // 
            // txtGetAccountNumber
            // 
            this.txtGetAccountNumber.AutoSize = false;
            this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtGetAccountNumber.Location = new System.Drawing.Point(121, 23);
            this.txtGetAccountNumber.Name = "txtGetAccountNumber";
            this.txtGetAccountNumber.Size = new System.Drawing.Size(136, 40);
            this.txtGetAccountNumber.TabIndex = 44;
            this.ToolTip1.SetToolTip(this.txtGetAccountNumber, null);
            this.txtGetAccountNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.txtGetAccountNumber_KeyDown);
            this.txtGetAccountNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtGetAccountNumber_KeyPress);
            // 
            // cmdGetAccountNumber
            // 
            this.cmdGetAccountNumber.AppearanceKey = "toolbarButton";
            this.cmdGetAccountNumber.Location = new System.Drawing.Point(1187, 23);
            this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
            this.cmdGetAccountNumber.Size = new System.Drawing.Size(97, 28);
            this.cmdGetAccountNumber.TabIndex = 48;
            this.cmdGetAccountNumber.Text = "Get Account";
            this.ToolTip1.SetToolTip(this.cmdGetAccountNumber, null);
            this.cmdGetAccountNumber.Visible = false;
            this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
            // 
            // lblInstructions1
            // 
            this.lblInstructions1.AutoSize = true;
            this.lblInstructions1.Location = new System.Drawing.Point(30, 38);
            this.lblInstructions1.Name = "lblInstructions1";
            this.lblInstructions1.Size = new System.Drawing.Size(69, 16);
            this.lblInstructions1.TabIndex = 47;
            this.lblInstructions1.Text = "ACCOUNT";
            this.ToolTip1.SetToolTip(this.lblInstructions1, null);
            // 
            // cmdSearch
            // 
            this.cmdSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSearch.AppearanceKey = "toolbarButton";
            this.cmdSearch.ImageSource = "button-search";
            this.cmdSearch.Location = new System.Drawing.Point(1019, 29);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(77, 24);
            this.cmdSearch.TabIndex = 51;
            this.cmdSearch.Text = "Search";
            this.cmdSearch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.cmdSearch, null);
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // vsSearch
            // 
            this.vsSearch.AllowSelection = false;
            this.vsSearch.AllowUserToResizeColumns = false;
            this.vsSearch.AllowUserToResizeRows = false;
            this.vsSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsSearch.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsSearch.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsSearch.BackColorBkg = System.Drawing.Color.Empty;
            this.vsSearch.BackColorFixed = System.Drawing.Color.Empty;
            this.vsSearch.BackColorSel = System.Drawing.Color.Empty;
            this.vsSearch.Cols = 5;
            this.vsSearch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.vsSearch.ColumnHeadersHeight = 30;
            this.vsSearch.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsSearch.DefaultCellStyle = dataGridViewCellStyle4;
            this.vsSearch.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsSearch.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsSearch.FixedCols = 0;
            this.vsSearch.ForeColorFixed = System.Drawing.SystemColors.ControlText;
            this.vsSearch.FrozenCols = 0;
            this.vsSearch.FrozenRows = 0;
            this.vsSearch.GridColor = System.Drawing.Color.Empty;
            this.vsSearch.Location = new System.Drawing.Point(25, 93);
            this.vsSearch.MinimumSize = new System.Drawing.Size(500, 200);
            this.vsSearch.Name = "vsSearch";
            this.vsSearch.ReadOnly = true;
            this.vsSearch.RowHeadersVisible = false;
            this.vsSearch.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsSearch.RowHeightMin = 0;
            this.vsSearch.Rows = 1;
            this.vsSearch.ShowColumnVisibilityMenu = false;
            this.vsSearch.Size = new System.Drawing.Size(1071, 433);
            this.vsSearch.StandardTab = true;
            this.vsSearch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsSearch.TabIndex = 41;
            this.ToolTip1.SetToolTip(this.vsSearch, null);
            this.vsSearch.Visible = false;
            this.vsSearch.CurrentCellChanged += new System.EventHandler(this.vsSearch_RowColChange);
            this.vsSearch.Sorted += new System.EventHandler(this.vsSearch_AfterSort);
            this.vsSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.vsSearch_KeyDown);
            this.vsSearch.Click += new System.EventHandler(this.vsSearch_Click);
            this.vsSearch.DoubleClick += new System.EventHandler(this.VsSearch_DoubleClick);
            // 
            // fraBatchQuestions
            // 
            this.fraBatchQuestions.Controls.Add(this.txtPaymentDate2);
            this.fraBatchQuestions.Controls.Add(this.chkReceipt);
            this.fraBatchQuestions.Controls.Add(this.txtTellerID);
            this.fraBatchQuestions.Controls.Add(this.cmdBatch);
            this.fraBatchQuestions.Controls.Add(this.cmbPeriod);
            this.fraBatchQuestions.Controls.Add(this.txtTaxYear);
            this.fraBatchQuestions.Controls.Add(this.txtPaymentDate);
            this.fraBatchQuestions.Controls.Add(this.txtPaidBy);
            this.fraBatchQuestions.Controls.Add(this.txtEffectiveDate);
            this.fraBatchQuestions.Controls.Add(this.lblEffectiveDate);
            this.fraBatchQuestions.Controls.Add(this.Label8);
            this.fraBatchQuestions.Controls.Add(this.Label6);
            this.fraBatchQuestions.Controls.Add(this.lblPaymentDate);
            this.fraBatchQuestions.Controls.Add(this.Label4);
            this.fraBatchQuestions.Controls.Add(this.Label3);
            this.fraBatchQuestions.Location = new System.Drawing.Point(30, 30);
            this.fraBatchQuestions.Name = "fraBatchQuestions";
            this.fraBatchQuestions.Size = new System.Drawing.Size(413, 426);
            this.fraBatchQuestions.TabIndex = 21;
            this.fraBatchQuestions.Text = "Batch Update Information";
            this.ToolTip1.SetToolTip(this.fraBatchQuestions, null);
            this.fraBatchQuestions.UseMnemonic = false;
            this.fraBatchQuestions.Visible = false;
            // 
            // txtPaymentDate2
            // 
            clientEvent1.Event = "keydown";
            clientEvent1.JavaScript = resources.GetString("clientEvent1.JavaScript");
            this.javaScript1.GetJavaScriptEvents(this.txtPaymentDate2).Add(clientEvent1);
            this.txtPaymentDate2.MaxLength = 10;
            this.txtPaymentDate2.Location = new System.Drawing.Point(231, 167);
            this.txtPaymentDate2.Mask = "##/##/####";
            this.txtPaymentDate2.Name = "txtPaymentDate2";
            this.txtPaymentDate2.Size = new System.Drawing.Size(159, 40);
            this.txtPaymentDate2.TabIndex = 6;
            this.txtPaymentDate2.Text = "  /  /";
            this.ToolTip1.SetToolTip(this.txtPaymentDate2, null);
            this.txtPaymentDate2.GotFocus += new System.EventHandler(this.txtPaymentDate2_GotFocus);
            // 
            // chkReceipt
            // 
            this.chkReceipt.Location = new System.Drawing.Point(20, 30);
            this.chkReceipt.Name = "chkReceipt";
            this.chkReceipt.Size = new System.Drawing.Size(250, 28);
            this.chkReceipt.TabIndex = 11;
            this.chkReceipt.Text = "Default Receipt Option to \'Yes\'";
            this.ToolTip1.SetToolTip(this.chkReceipt, null);
            // 
            // txtTellerID
            // 
            this.txtTellerID.MaxLength = 3;
            this.txtTellerID.AutoSize = false;
            this.txtTellerID.BackColor = System.Drawing.SystemColors.Window;
            this.txtTellerID.Location = new System.Drawing.Point(231, 64);
            this.txtTellerID.Name = "txtTellerID";
            this.txtTellerID.Size = new System.Drawing.Size(159, 40);
            this.txtTellerID.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtTellerID, null);
            this.txtTellerID.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTellerID_KeyDown);
            this.txtTellerID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTellerID_KeyPress);
            this.txtTellerID.Enter += new System.EventHandler(this.txtTellerID_Enter);
            // 
            // cmdBatch
            // 
            this.cmdBatch.AppearanceKey = "toolbarButton";
            this.cmdBatch.Location = new System.Drawing.Point(20, 378);
            this.cmdBatch.Name = "cmdBatch";
            this.cmdBatch.Size = new System.Drawing.Size(90, 30);
            this.cmdBatch.TabIndex = 10;
            this.cmdBatch.Text = "Next";
            this.ToolTip1.SetToolTip(this.cmdBatch, null);
            this.cmdBatch.Click += new System.EventHandler(this.cmdBatch_Click);
            // 
            // cmbPeriod
            // 
            this.cmbPeriod.AutoSize = false;
            this.cmbPeriod.BackColor = System.Drawing.SystemColors.Window;
            this.cmbPeriod.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbPeriod.FormattingEnabled = true;
            this.cmbPeriod.Location = new System.Drawing.Point(231, 324);
            this.cmbPeriod.Name = "cmbPeriod";
            this.cmbPeriod.Size = new System.Drawing.Size(159, 40);
            this.cmbPeriod.Sorted = true;
            this.cmbPeriod.TabIndex = 15;
            this.ToolTip1.SetToolTip(this.cmbPeriod, null);
            this.cmbPeriod.Visible = false;
            // 
            // txtTaxYear
            // 
            this.txtTaxYear.MaxLength = 4;
            this.txtTaxYear.AutoSize = false;
            this.txtTaxYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtTaxYear.Location = new System.Drawing.Point(231, 114);
            this.txtTaxYear.Name = "txtTaxYear";
            this.txtTaxYear.Size = new System.Drawing.Size(159, 40);
            this.txtTaxYear.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtTaxYear, "If the file doesn\'t specify a year, the default year will be used");
            this.txtTaxYear.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTaxYear_KeyDown);
            this.txtTaxYear.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTaxYear_KeyPress);
            this.txtTaxYear.Enter += new System.EventHandler(this.txtTaxYear_Enter);
            // 
            // txtPaymentDate
            // 
            this.txtPaymentDate.MaxLength = 10;
            this.txtPaymentDate.AutoSize = false;
            this.txtPaymentDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtPaymentDate.Location = new System.Drawing.Point(284, 94);
            this.txtPaymentDate.Name = "txtPaymentDate";
            this.txtPaymentDate.Size = new System.Drawing.Size(72, 40);
            this.txtPaymentDate.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtPaymentDate, null);
            this.txtPaymentDate.Visible = false;
            this.txtPaymentDate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPaymentDate_KeyDown);
            this.txtPaymentDate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPaymentDate_KeyPress);
            this.txtPaymentDate.Enter += new System.EventHandler(this.txtPaymentDate_Enter);
            // 
            // txtPaidBy
            // 
            this.txtPaidBy.MaxLength = 25;
            this.txtPaidBy.AutoSize = false;
            this.txtPaidBy.BackColor = System.Drawing.SystemColors.Window;
            this.txtPaidBy.Location = new System.Drawing.Point(231, 273);
            this.txtPaidBy.Name = "txtPaidBy";
            this.txtPaidBy.Size = new System.Drawing.Size(159, 40);
            this.txtPaidBy.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.txtPaidBy, null);
            this.txtPaidBy.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPaidBy_KeyDown);
            this.txtPaidBy.Enter += new System.EventHandler(this.txtPaidBy_Enter);
            // 
            // txtEffectiveDate
            // 
            clientEvent2.Event = "keydown";
            clientEvent2.JavaScript = resources.GetString("clientEvent2.JavaScript");
            this.txtEffectiveDate.MaxLength = 10;
            this.javaScript1.GetJavaScriptEvents(this.txtEffectiveDate).Add(clientEvent2);
            this.txtEffectiveDate.Location = new System.Drawing.Point(231, 221);
            this.txtEffectiveDate.Mask = "##/##/####";
            this.txtEffectiveDate.Name = "txtEffectiveDate";
            this.txtEffectiveDate.Size = new System.Drawing.Size(159, 40);
            this.txtEffectiveDate.TabIndex = 7;
            this.txtEffectiveDate.Text = "  /  /";
            this.ToolTip1.SetToolTip(this.txtEffectiveDate, null);
            //this.txtEffectiveDate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtEffectiveDate_KeyDown);
            //this.txtEffectiveDate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtEffectiveDate_KeyPress);
            this.txtEffectiveDate.GotFocus += new System.EventHandler(this.txtEffectiveDate_GotFocus);
            // 
            // lblEffectiveDate
            // 
            this.lblEffectiveDate.AutoSize = true;
            this.lblEffectiveDate.Location = new System.Drawing.Point(20, 235);
            this.lblEffectiveDate.Name = "lblEffectiveDate";
            this.lblEffectiveDate.Size = new System.Drawing.Size(202, 16);
            this.lblEffectiveDate.TabIndex = 74;
            this.lblEffectiveDate.Text = "EFFECTIVE DATE (MM/DD/YYYY)";
            this.ToolTip1.SetToolTip(this.lblEffectiveDate, "Effective Transaction Date");
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(20, 78);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(70, 16);
            this.Label8.TabIndex = 26;
            this.Label8.Text = "TELLER ID";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(20, 291);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(56, 16);
            this.Label6.TabIndex = 25;
            this.Label6.Text = "PAID BY";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // lblPaymentDate
            // 
            this.lblPaymentDate.AutoSize = true;
            this.lblPaymentDate.Location = new System.Drawing.Point(20, 181);
            this.lblPaymentDate.Name = "lblPaymentDate";
            this.lblPaymentDate.Size = new System.Drawing.Size(193, 16);
            this.lblPaymentDate.TabIndex = 24;
            this.lblPaymentDate.Text = "PAYMENT DATE (MM/DD/YYYY)";
            this.ToolTip1.SetToolTip(this.lblPaymentDate, "Recorded Transaction Date");
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(20, 338);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(55, 16);
            this.Label4.TabIndex = 23;
            this.Label4.Text = "PERIOD";
            this.ToolTip1.SetToolTip(this.Label4, null);
            this.Label4.Visible = false;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(20, 128);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(128, 16);
            this.Label3.TabIndex = 22;
            this.Label3.Text = "DEFAULT TAX YEAR";
            this.ToolTip1.SetToolTip(this.Label3, "If the file does not specify a year, the default year will be used");
            // 
            // fraBatch
            // 
            this.fraBatch.Controls.Add(this.lblPeriod1);
            this.fraBatch.Controls.Add(this.cmbPeriod1);
            this.fraBatch.Controls.Add(this.cmdProcessBatch);
            this.fraBatch.Controls.Add(this.txtTotal);
            this.fraBatch.Controls.Add(this.txtAmount);
            this.fraBatch.Controls.Add(this.txtBatchAccount);
            this.fraBatch.Controls.Add(this.vsBatch);
            this.fraBatch.Controls.Add(this.lblInstructions);
            this.fraBatch.Controls.Add(this.Label11);
            this.fraBatch.Controls.Add(this.lblName);
            this.fraBatch.Controls.Add(this.lblLocation);
            this.fraBatch.Controls.Add(this.Label10);
            this.fraBatch.Controls.Add(this.Label9);
            this.fraBatch.Location = new System.Drawing.Point(0, 0);
            this.fraBatch.Name = "fraBatch";
            this.fraBatch.Size = new System.Drawing.Size(815, 514);
            this.fraBatch.TabIndex = 20;
            this.ToolTip1.SetToolTip(this.fraBatch, null);
            this.fraBatch.Visible = false;
            // 
            // lblPeriod1
            // 
            this.lblPeriod1.AutoSize = true;
            this.lblPeriod1.Location = new System.Drawing.Point(30, 202);
            this.lblPeriod1.Name = "lblPeriod1";
            this.lblPeriod1.Size = new System.Drawing.Size(55, 16);
            this.lblPeriod1.TabIndex = 45;
            this.lblPeriod1.Text = "PERIOD";
            this.ToolTip1.SetToolTip(this.lblPeriod1, null);
            // 
            // cmdProcessBatch
            // 
            this.cmdProcessBatch.Anchor = Wisej.Web.AnchorStyles.Bottom;
            this.cmdProcessBatch.AppearanceKey = "toolbarButton";
            this.cmdProcessBatch.Location = new System.Drawing.Point(324, -152);
            this.cmdProcessBatch.Name = "cmdProcessBatch";
            this.cmdProcessBatch.Size = new System.Drawing.Size(117, 35);
            this.cmdProcessBatch.TabIndex = 3;
            this.cmdProcessBatch.Text = "Process Batch";
            this.ToolTip1.SetToolTip(this.cmdProcessBatch, null);
            this.cmdProcessBatch.Visible = false;
            this.cmdProcessBatch.Click += new System.EventHandler(this.cmdProcessBatch_Click);
            // 
            // txtTotal
            // 
            this.txtTotal.AutoSize = false;
            this.txtTotal.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotal.Location = new System.Drawing.Point(124, 523);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(121, 40);
            this.txtTotal.TabIndex = 33;
            this.txtTotal.TabStop = false;
            this.ToolTip1.SetToolTip(this.txtTotal, null);
            // 
            // txtAmount
            // 
            this.txtAmount.AutoSize = false;
            this.txtAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtAmount.Location = new System.Drawing.Point(348, 248);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.Size = new System.Drawing.Size(121, 40);
            this.txtAmount.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.txtAmount, null);
            this.txtAmount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAmount_KeyDown);
            this.txtAmount.Enter += new System.EventHandler(this.txtAmount_Enter);
            // 
            // txtBatchAccount
            // 
            this.txtBatchAccount.AutoSize = false;
            this.txtBatchAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtBatchAccount.Location = new System.Drawing.Point(124, 248);
            this.txtBatchAccount.Name = "txtBatchAccount";
            this.txtBatchAccount.Size = new System.Drawing.Size(121, 40);
            this.txtBatchAccount.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.txtBatchAccount, null);
            this.txtBatchAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtBatchAccount_KeyDown);
            this.txtBatchAccount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtBatchAccount_KeyPress);
            this.txtBatchAccount.Enter += new System.EventHandler(this.txtBatchAccount_Enter);
            this.txtBatchAccount.Validating += new System.ComponentModel.CancelEventHandler(this.txtBatchAccount_Validating);
            // 
            // vsBatch
            // 
            this.vsBatch.AllowSelection = false;
            this.vsBatch.AllowUserToResizeColumns = false;
            this.vsBatch.AllowUserToResizeRows = false;
            this.vsBatch.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
            this.vsBatch.BackColorAlternate = System.Drawing.Color.Empty;
            this.vsBatch.BackColorBkg = System.Drawing.Color.Empty;
            this.vsBatch.BackColorFixed = System.Drawing.Color.Empty;
            this.vsBatch.BackColorSel = System.Drawing.Color.Empty;
            this.vsBatch.Cols = 10;
            this.vsBatch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.vsBatch.ColumnHeadersHeight = 30;
            this.vsBatch.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
            this.vsBatch.DefaultCellStyle = dataGridViewCellStyle6;
            this.vsBatch.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
            this.vsBatch.FixedCols = 0;
            this.vsBatch.ForeColorFixed = System.Drawing.SystemColors.ControlText;
            this.vsBatch.FrozenCols = 0;
            this.vsBatch.FrozenRows = 0;
            this.vsBatch.GridColor = System.Drawing.Color.Empty;
            this.vsBatch.Location = new System.Drawing.Point(30, 300);
            this.vsBatch.Name = "vsBatch";
            this.vsBatch.ReadOnly = true;
            this.vsBatch.RowHeadersVisible = false;
            this.vsBatch.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.vsBatch.RowHeightMin = 0;
            this.vsBatch.Rows = 1;
            this.vsBatch.ShowColumnVisibilityMenu = false;
            this.vsBatch.Size = new System.Drawing.Size(769, 200);
            this.vsBatch.StandardTab = true;
            this.vsBatch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
            this.vsBatch.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.vsBatch, null);
            this.vsBatch.CurrentCellChanged += new System.EventHandler(this.vsBatch_RowColChange);
            this.vsBatch.KeyDown += new Wisej.Web.KeyEventHandler(this.vsBatch_KeyDown);
            this.vsBatch.Click += new System.EventHandler(this.vsBatch_Click);
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(30, 30);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(582, 58);
            this.lblInstructions.TabIndex = 35;
            this.ToolTip1.SetToolTip(this.lblInstructions, null);
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(30, 537);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(54, 16);
            this.Label11.TabIndex = 34;
            this.Label11.Text = "TOTAL";
            this.ToolTip1.SetToolTip(this.Label11, null);
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(30, 108);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(284, 18);
            this.lblName.TabIndex = 32;
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblName, null);
            // 
            // lblLocation
            // 
            this.lblLocation.Location = new System.Drawing.Point(30, 148);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(284, 18);
            this.lblLocation.TabIndex = 31;
            this.lblLocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ToolTip1.SetToolTip(this.lblLocation, null);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(263, 262);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(62, 20);
            this.Label10.TabIndex = 30;
            this.Label10.Text = "AMOUNT";
            this.ToolTip1.SetToolTip(this.Label10, null);
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(30, 262);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(69, 20);
            this.Label9.TabIndex = 29;
            this.Label9.Text = "ACCOUNT";
            this.ToolTip1.SetToolTip(this.Label9, null);
            // 
            // fraValidate
            // 
            this.fraValidate.BackColor = System.Drawing.Color.FromName("@window");
            this.fraValidate.Controls.Add(this.cmdValidateCancel);
            this.fraValidate.Controls.Add(this.cmdValidateNo);
            this.fraValidate.Controls.Add(this.cmdValidateYes);
            this.fraValidate.Controls.Add(this.lblValidate);
            this.fraValidate.Location = new System.Drawing.Point(651, 26);
            this.fraValidate.Name = "fraValidate";
            this.fraValidate.Size = new System.Drawing.Size(330, 144);
            this.fraValidate.TabIndex = 36;
            this.ToolTip1.SetToolTip(this.fraValidate, null);
            this.fraValidate.UseMnemonic = false;
            this.fraValidate.Visible = false;
            // 
            // cmdValidateCancel
            // 
            this.cmdValidateCancel.AppearanceKey = "toolbarButton";
            this.cmdValidateCancel.Location = new System.Drawing.Point(230, 110);
            this.cmdValidateCancel.Name = "cmdValidateCancel";
            this.cmdValidateCancel.Size = new System.Drawing.Size(70, 23);
            this.cmdValidateCancel.TabIndex = 40;
            this.cmdValidateCancel.Text = "Cancel";
            this.ToolTip1.SetToolTip(this.cmdValidateCancel, null);
            this.cmdValidateCancel.Click += new System.EventHandler(this.cmdValidateCancel_Click);
            // 
            // cmdValidateNo
            // 
            this.cmdValidateNo.AppearanceKey = "toolbarButton";
            this.cmdValidateNo.Location = new System.Drawing.Point(130, 110);
            this.cmdValidateNo.Name = "cmdValidateNo";
            this.cmdValidateNo.Size = new System.Drawing.Size(70, 23);
            this.cmdValidateNo.TabIndex = 39;
            this.cmdValidateNo.Text = "No";
            this.ToolTip1.SetToolTip(this.cmdValidateNo, null);
            this.cmdValidateNo.Click += new System.EventHandler(this.cmdValidateNo_Click);
            // 
            // cmdValidateYes
            // 
            this.cmdValidateYes.AppearanceKey = "toolbarButton";
            this.cmdValidateYes.Location = new System.Drawing.Point(30, 110);
            this.cmdValidateYes.Name = "cmdValidateYes";
            this.cmdValidateYes.Size = new System.Drawing.Size(70, 23);
            this.cmdValidateYes.TabIndex = 38;
            this.cmdValidateYes.Text = "Yes";
            this.ToolTip1.SetToolTip(this.cmdValidateYes, null);
            this.cmdValidateYes.Click += new System.EventHandler(this.cmdValidateYes_Click);
            // 
            // lblValidate
            // 
            this.lblValidate.Location = new System.Drawing.Point(30, 30);
            this.lblValidate.Name = "lblValidate";
            this.lblValidate.Size = new System.Drawing.Size(269, 74);
            this.lblValidate.TabIndex = 37;
            this.ToolTip1.SetToolTip(this.lblValidate, null);
            // 
            // txtHold
            // 
            this.txtHold.AutoSize = false;
            this.txtHold.BackColor = System.Drawing.SystemColors.Window;
            this.txtHold.Location = new System.Drawing.Point(9, 82);
            this.txtHold.Name = "txtHold";
            this.txtHold.Size = new System.Drawing.Size(132, 40);
            this.txtHold.TabIndex = 14;
            this.ToolTip1.SetToolTip(this.txtHold, null);
            this.txtHold.Visible = false;
            // 
            // lblSearchListInstruction
            // 
            this.lblSearchListInstruction.Location = new System.Drawing.Point(117, 0);
            this.lblSearchListInstruction.Name = "lblSearchListInstruction";
            this.lblSearchListInstruction.Size = new System.Drawing.Size(236, 22);
            this.lblSearchListInstruction.TabIndex = 42;
            this.lblSearchListInstruction.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblSearchListInstruction, null);
            this.lblSearchListInstruction.Visible = false;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuBatchStart,
            this.mnuBatchSave,
            this.mnuFileBatchSearch,
            this.mnuBatchPrint,
            this.mnuBatchRecover,
            this.mnuBatchPurge,
            this.mnuFileImport,
            this.mnuImportFirstAmericanBatch});
            // 
            // mnuBatchStart
            // 
            this.mnuBatchStart.Index = 0;
            this.mnuBatchStart.Text = "S&tart Batch Update";
            this.mnuBatchStart.Click += new System.EventHandler(this.mnuBatchStart_Click);
            // 
            // mnuBatchSave
            // 
            this.mnuBatchSave.Index = 1;
            this.mnuBatchSave.Text = "&Save Batch Update";
            this.mnuBatchSave.Click += new System.EventHandler(this.mnuBatchSave_Click);
            // 
            // mnuFileBatchSearch
            // 
            this.mnuFileBatchSearch.Index = 2;
            this.mnuFileBatchSearch.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuFileBatchSearch.Text = "&Account Search";
            this.mnuFileBatchSearch.Click += new System.EventHandler(this.mnuFileBatchSearch_Click);
            // 
            // mnuBatchPrint
            // 
            this.mnuBatchPrint.Index = 3;
            this.mnuBatchPrint.Text = "&Print Current Batch Listing";
            this.mnuBatchPrint.Click += new System.EventHandler(this.mnuBatchPrint_Click);
            // 
            // mnuBatchRecover
            // 
            this.mnuBatchRecover.Index = 4;
            this.mnuBatchRecover.Text = "&Recover Batch";
            this.mnuBatchRecover.Click += new System.EventHandler(this.mnuBatchRecover_Click);
            // 
            // mnuBatchPurge
            // 
            this.mnuBatchPurge.Index = 5;
            this.mnuBatchPurge.Text = "&Purge Batch";
            this.mnuBatchPurge.Click += new System.EventHandler(this.mnuBatchPurge_Click);
            // 
            // mnuFileImport
            // 
            this.mnuFileImport.Enabled = false;
            this.mnuFileImport.Index = 6;
            this.mnuFileImport.Text = "&Import Batch File";
            this.mnuFileImport.Click += new System.EventHandler(this.mnuFileImport_Click);
            // 
            // mnuImportFirstAmericanBatch
            // 
            this.mnuImportFirstAmericanBatch.Enabled = false;
            this.mnuImportFirstAmericanBatch.Index = 7;
            this.mnuImportFirstAmericanBatch.Text = "Import CoreLogic Batch File";
            this.mnuImportFirstAmericanBatch.Click += new System.EventHandler(this.mnuImportFirstAmericanBatch_Click);
            // 
            // mnuBatch
            // 
            this.mnuBatch.Index = -1;
            this.mnuBatch.Text = "";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.Text = "";
            // 
            // btnProcess
            // 
            this.btnProcess.AppearanceKey = "acceptButton";
            this.btnProcess.Location = new System.Drawing.Point(504, 30);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnProcess.Size = new System.Drawing.Size(120, 48);
            this.btnProcess.TabIndex = 0;
            this.btnProcess.Text = "Process";
            this.ToolTip1.SetToolTip(this.btnProcess, null);
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // cmdProcessClearSearch
            // 
            this.cmdProcessClearSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdProcessClearSearch.AppearanceKey = "toolbarButton";
            this.cmdProcessClearSearch.Location = new System.Drawing.Point(918, 29);
            this.cmdProcessClearSearch.Name = "cmdProcessClearSearch";
            this.cmdProcessClearSearch.Size = new System.Drawing.Size(95, 24);
            this.cmdProcessClearSearch.TabIndex = 52;
            this.cmdProcessClearSearch.Text = "Clear Search";
            this.ToolTip1.SetToolTip(this.cmdProcessClearSearch, null);
            this.cmdProcessClearSearch.Click += new System.EventHandler(this.cmdProcessClearSearch_Click);
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = -1;
            this.mnuProcessQuit.Text = "E&xit    (Esc)";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // mnuProcessSeperator
            // 
            this.mnuProcessSeperator.Index = -1;
            this.mnuProcessSeperator.Text = "-";
            // 
            // mnuProcessGetAccount
            // 
            this.mnuProcessGetAccount.Index = -1;
            this.mnuProcessGetAccount.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessGetAccount.Text = "P&rocess";
            // 
            // mnuProcessSearch
            // 
            this.mnuProcessSearch.Index = -1;
            this.mnuProcessSearch.Text = "&Search";
            this.mnuProcessSearch.Click += new System.EventHandler(this.mnuProcessSearch_Click);
            // 
            // mnuProcessClearSearch
            // 
            this.mnuProcessClearSearch.Index = -1;
            this.mnuProcessClearSearch.Text = "&Clear Search";
            this.mnuProcessClearSearch.Click += new System.EventHandler(this.mnuProcessClearSearch_Click);
            // 
            // frmCLGetAccount
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1128, 714);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmCLGetAccount";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Select Account";
            this.ToolTip1.SetToolTip(this, null);
            this.Load += new System.EventHandler(this.frmCLGetAccount_Load);
            this.Activated += new System.EventHandler(this.frmCLGetAccount_Activated);
            this.FormClosing += new Wisej.Web.FormClosingEventHandler(this.frmCLGetAccount_FormClosing);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCLGetAccount_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCLGetAccount_KeyPress);
            this.Resize += new System.EventHandler(this.frmCLGetAccount_Resize);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRecover)).EndInit();
            this.fraRecover.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdBatchChoice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBatchSearchResults)).EndInit();
            this.fraBatchSearchResults.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsBatchSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBatchSearchCriteria)).EndInit();
            this.fraBatchSearchCriteria.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdBatchSearchCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBatchSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSearch)).EndInit();
            this.fraSearch.ResumeLayout(false);
            this.fraSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPreviousOwnerInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowDeletedAccountInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBatchQuestions)).EndInit();
            this.fraBatchQuestions.ResumeLayout(false);
            this.fraBatchQuestions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPaymentDate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkReceipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBatch)).EndInit();
            this.fraBatch.ResumeLayout(false);
            this.fraBatch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsBatch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraValidate)).EndInit();
            this.fraValidate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdValidateCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdValidateNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdValidateYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessClearSearch)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
	}
}