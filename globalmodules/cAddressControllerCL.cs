﻿using Global;
using Microsoft.Ajax.Utilities;
using SharedApplication.Extensions;
using System;

namespace TWCL0000
{
    public class cAddressControllerCL
    {
        public clsAddress GetOwnerAndAddressAtBilling(clsDRWrapper rsBill)
        {
            var strName = "";

            if (!string.IsNullOrWhiteSpace(rsBill.Get_Fields_String("Name2").Trim()))
            {
                strName = rsBill.Get_Fields_String("Name1").Trim() + " and " + rsBill.Get_Fields_String("Name2").Trim();
            }
            else
            {
                strName = rsBill.Get_Fields_String("Name1").Trim();
            }
            var tAddress = new clsAddress()
            {
                Addressee = strName,
            };
            tAddress.Set_Address(1, rsBill.Get_Fields_String("Address1"));
            tAddress.Set_Address(2, rsBill.Get_Fields_String("Address2"));
            tAddress.Set_Address(3, rsBill.Get_Fields_String("Address3"));

            return tAddress;
        }

        public clsAddress GetOwnerAtBillingCareOfCurrentOwner(clsDRWrapper rsBill)
        {
            int lngAcct = 0;
            var strName1 = "";
            var strName2 = "";
            var strAddr1 = "";
            var strAddr2 = "";
            var strAddr3 = "";
            var strCareOf = "";
            var pCont = new cPartyController();

            DateTime dtBillDate;
            var rsMastOwner = new clsDRWrapper();

            lngAcct = rsBill.Get_Fields_Int32("Account");
            strName1 = rsBill.Get_Fields_String("Name1").Trim();
            strName2 = rsBill.Get_Fields_String("Name2").Trim();
            strAddr1 = rsBill.Get_Fields_String("Address1").Trim();
            strAddr2 = rsBill.Get_Fields_String("Address2").Trim();
            strAddr3 = rsBill.Get_Fields_String("Address3").Trim();

            if (modStatusPayments.Statics.boolRE)
            {
                if (rsBill.Get_Fields_DateTime("TransferFromBillingDateFirst") != null)
                {
                    dtBillDate = rsBill.Get_Fields_DateTime("TransferFromBillingDateFirst");
                }
                else
                {
                    dtBillDate = rsBill.Get_Fields_DateTime("TransferFromBillingDateLast");
                }

                rsMastOwner.OpenRecordset(
                    "Select OwnerPartyID as OwnerID1, SecOwnerPartyID as OwnerID2, DeedName1,DeedName2 from master where rsaccount = " +
                    lngAcct, "RealEstate");
            }
            else
            {
                rsMastOwner.OpenRecordset("Select PartyID as OwnerID from PPMaster where Account = " + lngAcct,
                    "PersonalProperty");
            }

            if (!rsMastOwner.EndOfFile())
            {
                var pInfo = pCont.GetParty(rsMastOwner.Get_Fields_Int32("OwnerID1"));
                var strTemp = pInfo.FullNameLastFirst;
                if (modStatusPayments.Statics.boolRE)
                {
                    strTemp = rsMastOwner.Get_Fields_String("DeedName1");
                }
                if (pInfo != null)
                {
                    var pAdd = pInfo.GetAddress("CL", rsMastOwner.Get_Fields_Int32("OwnerID1"));
                    if (strName1.Trim().ToLower() != strTemp.Trim().ToLower())
                    {
                        strCareOf = @"C\O " + strTemp;
                    }

                    if (pAdd != null)
                    {
                        strAddr1 = pAdd.Address1.Trim();
                        strAddr2 = pAdd.Address2.Trim();
                        strAddr3 = pAdd.City.Trim() + " " + pAdd.State.Trim() + " " + pAdd.Zip.Trim();
                    }
                }
            }

            var strName = strName1;
            if (!string.IsNullOrWhiteSpace(strName2.Trim()))
            {
                strName = strName1 + " and " + strName2;
            }

            var tAddress = new clsAddress()
            {
                Addressee = strName
            };
            tAddress.Set_Address(1, strCareOf);
            tAddress.Set_Address(2, strAddr1);
            tAddress.Set_Address(3, strAddr2);
            tAddress.Set_Address(4, strAddr3);
            rsMastOwner.Dispose();
            return FormatAddress(tAddress);
        }

        private clsAddress FormatAddress(clsAddress tAddress)
        {
            var returnAddress = tAddress;
            if (string.IsNullOrWhiteSpace(returnAddress.Get_Address(3)))
            {
                returnAddress.Set_Address(3, returnAddress.Get_Address(4));
                returnAddress.Set_Address(4, "");
            }

            if (string.IsNullOrWhiteSpace(returnAddress.Get_Address(2)))
            {
                returnAddress.Set_Address(2, returnAddress.Get_Address(3));
                returnAddress.Set_Address(3, returnAddress.Get_Address(4));
                returnAddress.Set_Address(4, "");
            }

            if (string.IsNullOrWhiteSpace(returnAddress.Get_Address(1)))
            {
                returnAddress.Set_Address(1, returnAddress.Get_Address(2));
                returnAddress.Set_Address(2, returnAddress.Get_Address(3));
                returnAddress.Set_Address(3, returnAddress.Get_Address(4));
                returnAddress.Set_Address(4, "");
            }

            return returnAddress;
        }

        public clsAddress GetBilledOwnerAtLastAddress(clsDRWrapper rsBill)
        {
            int lngAcct = 0;
            var strName1 = "";
            var strName2 = "";
            var strAddr1 = "";
            var strAddr2 = "";
            var strAddr3 = "";
            var strCareOf = "";
            var strTemp = "";
            DateTime dtBillDate;
            var boolNewOwner = false;
            var boolNewAddress = false;
            var boolREMatch = false;
            var boolSameName = false;
            var pCont = new cPartyController();
            cParty pInfo;
            var rsMastOwner = new clsDRWrapper();


            lngAcct = rsBill.Get_Fields_Int32("Account");
            strName1 = rsBill.Get_Fields_String("Name1");
            strName2 = rsBill.Get_Fields_String("Name2");
            strAddr1 = rsBill.Get_Fields_String("Address1");
            strAddr2 = rsBill.Get_Fields_String("Address2");
            strAddr3 = rsBill.Get_Fields_String("Address3");

            if (modStatusPayments.Statics.boolRE)
            {
                if (rsBill.Get_Fields_DateTime("TransferFromBillingDateFirst") != null)
                {
                    dtBillDate = rsBill.Get_Fields_DateTime("TransferFromBillingDateFirst");
                }
                else
                {
                    dtBillDate = rsBill.Get_Fields_DateTime("TransferFromBillingDateLast");
                }

                boolNewOwner = modCLCalculations.NewOwner(strName1, lngAcct, dtBillDate, ref boolREMatch, ref boolSameName);
                var prevAddress = OwnerLastRecordedAddress(strName1, lngAcct, dtBillDate);
                if (boolNewOwner && prevAddress != null)
                {
                    strAddr1 = prevAddress.Get_Address(1);
                    strAddr2 = prevAddress.Get_Address(2);
                    strAddr3 = prevAddress.Get_Address(3);
                }
            }

            if (string.IsNullOrWhiteSpace(strAddr1 + strAddr2) || (boolSameName && boolREMatch) || !boolREMatch)
            {
                if (modStatusPayments.Statics.boolRE)
                {
                    rsMastOwner.OpenRecordset(
                        "Select OwnerPartyID as OwnerID, DeedName1 from master where rsaccount = " + lngAcct,
                        "RealEstate");
                }
                else
                {
                    rsMastOwner.OpenRecordset("Select PartyID as OwnerID from PPMaster Where account = " + lngAcct,
                        "PersonalProperty");
                }

                if (!rsMastOwner.EndOfFile())
                {
                    pInfo = pCont.GetParty(rsMastOwner.Get_Fields_Int32("OwnerID"));
                    strTemp = pInfo.FullNameLastFirst;
                    if (modStatusPayments.Statics.boolRE)
                    {
                        strTemp = rsMastOwner.Get_Fields_String("DeedName1");
                    }

                    if (pInfo != null)
                    {
                        var pAdd = pInfo.GetAddress("", rsMastOwner.Get_Fields_Int32("OwnerID"));
                        strCareOf = "";

                        if (pAdd != null)
                        {
                            strAddr1 = pAdd.Address1.Trim();
                            strAddr2 = pAdd.Address2.Trim();
                            strAddr3 = pAdd.City.Trim() + " " + pAdd.State.Trim() + " " + pAdd.Zip.Trim();
                        }
                    }
                }
            }

            var tAddress = new clsAddress();
            if (!string.IsNullOrWhiteSpace(strName2))
            {
                tAddress.Addressee = strName1.Trim() + " and " + strName2.Trim();
            }
            else
            {
                tAddress.Addressee = strName1.Trim();
            }

            tAddress.Set_Address(1, strCareOf.Trim());
            tAddress.Set_Address(2, strAddr1.Trim());
            tAddress.Set_Address(3, strAddr2.Trim());
            tAddress.Set_Address(4, strAddr3.Trim());
            rsMastOwner.Dispose();
            return FormatAddress(tAddress);
        }

        private clsAddress OwnerLastRecordedAddress(string strName, int lngAccount, DateTime dtBillDate)
        {
            var rsRE = new clsDRWrapper();
            var rsSR = new clsDRWrapper();
            try
            {


                //var preAddress = new clsAddress();
                dtBillDate = new DateTime(dtBillDate.Year, 4, 1);
                rsRE.OpenRecordset(
                    "select ownerpartyid, DeedName1 from master where rscard = 1 and rsaccount = " + lngAccount,
                    "RealEstate");

                if (!rsRE.EndOfFile())
                {
                    if (rsRE.Get_Fields_String("DeedName1").Replace("  ", " ").ToLower().Trim() != strName.Replace("  ", " ").ToLower().Trim())
                    {
                        rsSR.OpenRecordset(
                            "select * from srmaster where rsaccount = " + lngAccount + " and rscard = 1 and saledate > '" + dtBillDate.FormatAndPadShortDate() + "' order by saledate desc", "RealEstate");

                        while (!rsSR.EndOfFile())
                        {
                            if (rsSR.Get_Fields_String("RSName").Trim().ToLower() == strName.Trim().ToLower())
                            {
                                if (!string.IsNullOrWhiteSpace(rsSR.Get_Fields_String("rsAddr1") + rsSR.Get_Fields_String("rsAddr2") + rsSR.Get_Fields_String("rsAddr3")))
                                {
                                    var prevAddress = new clsAddress() { Addressee = strName };
                                    prevAddress.Set_Address(1, rsSR.Get_Fields_String("rsaddr1").Trim());
                                    prevAddress.Set_Address(2, rsSR.Get_Fields_String("rsaddr2").Trim());
                                    prevAddress.Set_Address(3,
                                                            (rsSR.Get_Fields_String("rsaddr3").Trim() + ", " + rsSR.Get_Fields_String("Rsstate") + " " + rsSR.Get_Fields_String("rsZip") + " " + rsSR.Get_Fields_String("rszip4")).Trim());
                                    prevAddress.City = rsSR.Get_Fields_String("rsaddr3");
                                    prevAddress.State = rsSR.Get_Fields_String("rsstate");
                                    prevAddress.Zip = rsSR.Get_Fields_String("rszip");
                                    prevAddress.ZipExtension = rsSR.Get_Fields_String("rszip4");

                                    return FormatAddress(prevAddress);
                                }
                            }

                            rsSR.MoveNext();
                        }

                        rsSR.OpenRecordset(
                            "select * from previousOwner where account = " + lngAccount + " and saledate > '" + dtBillDate.FormatAndPadShortDate() + "' order by saledate desc", "RealEstate");

                        while (!rsSR.EndOfFile())
                        {
                            if (rsSR.Get_Fields_String("Name").Trim().ToLower() == strName.Trim().ToLower())
                            {
                                var prevAddress = new clsAddress() { Addressee = strName };
                                prevAddress.Set_Address(1, rsSR.Get_Fields_String("Address1"));
                                prevAddress.Set_Address(2, rsSR.Get_Fields_String("Address2"));
                                prevAddress.Set_Address(3,
                                                        (rsSR.Get_Fields_String("City") + ", " + rsSR.Get_Fields_String("State") + " " + rsSR.Get_Fields_String("Zip") + " " + rsSR.Get_Fields_String("Zip4")).Trim());
                                prevAddress.City = rsSR.Get_Fields_String("City");
                                prevAddress.State = rsSR.Get_Fields_String("State");
                                prevAddress.Zip = rsSR.Get_Fields_String("Zip");
                                prevAddress.ZipExtension = rsSR.Get_Fields_String("Zip4");

                                return FormatAddress(prevAddress);
                            }

                            rsSR.MoveNext();
                        }
                    }
                }

                return null;
            }
            catch
            {
                return null;
            }
            finally
            {
                rsRE.DisposeOf();
                rsSR.DisposeOf();
            }


        }
    }
}