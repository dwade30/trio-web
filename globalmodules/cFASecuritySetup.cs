﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cFASecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cFASecuritySetup() : base()
		{
			strThisModule = "FA";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem(" Enter Fixed Assets", "", 1, false, ""));
			AddItem_2(CreateItem("Add Asset", "", 10, false, ""));
			AddItem_2(CreateItem("Delete Asset", "", 12, false, ""));
			AddItem_2(CreateItem("Edit Asset", "", 11, false, ""));
			AddItem_2(CreateItem("File Maintenance", "", 2, true, ""));
			AddItem_2(CreateItem("File Maintenance", "Add/Edit Class Codes", 5, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Add/Edit Dept/Div Codes", 6, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Add/Edit Location Codes", 4, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Add/Edit Vendors", 7, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Customize", 3, false, ""));
			AddItem_2(CreateItem("Post Depreciation", "", 9, false, ""));
			AddItem_2(CreateItem("Post Depreciation", "Auto Post Depreciation Journal", 13, false, ""));
			AddItem_2(CreateItem("Printing", "", 8, false, ""));
		}
	}
}
