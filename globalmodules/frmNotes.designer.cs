﻿//Fecher vbPorter - Version 1.0.0.59

namespace Global
{
	/// <summary>
	/// Summary description for frmNotes.
	/// </summary>
	partial class frmNotes
	{
		public fecherFoundation.FCRichTextBox RichTextBox1;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
                fecherFoundation.Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmNotes));
			this.RichTextBox1 = new fecherFoundation.FCRichTextBox();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.SuspendLayout();
			//
			// RichTextBox1
			//
			this.RichTextBox1.Name = "RichTextBox1";
			this.RichTextBox1.TabIndex = 0;
			this.RichTextBox1.Location = new System.Drawing.Point(3, 26);
			this.RichTextBox1.Size = new System.Drawing.Size(622, 516);
			//
			// mnuProcess
			//
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			//
			// mnuExit
			//
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			//
			// MainMenu1
			//
			this.MainMenu1.MenuItems.AddRange(new fecherFoundation.FCToolStripMenuItem[] {
				this.mnuProcess
			});
			//
			// frmNotes
			//
			this.ClientSize = new System.Drawing.Size(627, 531);
			this.ClientArea.Controls.Add(this.RichTextBox1);
			//this.ClientArea.Controls.Add(this.MainMenu1);
			this.Name = "frmNotes";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmNotes.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Load += new System.EventHandler(this.frmNotes_Load);
			this.Text = "";
			((System.ComponentModel.ISupportInitialize)(this.RichTextBox1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}