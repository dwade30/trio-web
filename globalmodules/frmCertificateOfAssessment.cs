﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWRE0000
using TWRE0000;


#elif TWBL0000
using TWBL0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmCertificateOfAssessment.
	/// </summary>
	public partial class frmCertificateOfAssessment : BaseForm
	{
		public frmCertificateOfAssessment()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label1 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label1.AddControlArrayElement(Label1_10, 10);
			this.Label1.AddControlArrayElement(Label1_9, 9);
			this.Label1.AddControlArrayElement(Label1_8, 8);
			this.Label1.AddControlArrayElement(Label1_7, 7);
			this.Label1.AddControlArrayElement(Label1_3, 3);
			this.Label1.AddControlArrayElement(Label1_6, 6);
			this.Label1.AddControlArrayElement(Label1_5, 5);
			this.Label1.AddControlArrayElement(Label1_4, 4);
			this.Label1.AddControlArrayElement(Label1_2, 2);
			this.Label1.AddControlArrayElement(Label1_1, 1);
			this.Label1.AddControlArrayElement(Label1_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCertificateOfAssessment InstancePtr
		{
			get
			{
				return (frmCertificateOfAssessment)Sys.GetInstance(typeof(frmCertificateOfAssessment));
			}
		}

		protected frmCertificateOfAssessment _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		/// <summary>
		/// ********************************************************
		/// </summary>
		/// <summary>
		/// Property of TRIO Software Corporation
		/// </summary>
		/// <summary>
		/// Written By
		/// </summary>
		/// <summary>
		/// Date
		/// </summary>
		/// <summary>
		/// ********************************************************
		/// </summary>
		private int lngTown;
		private string strBeginFiscal = string.Empty;
		private string strEndFiscal = string.Empty;
		private double dblREValuation;
		// vbPorter upgrade warning: lngTownNumber As int	OnWrite(short, double)
		public void Init(double lngREValuation, int lngTownNumber = 0, string strFiscalStart = "", string strFiscalEnd = "")
		{
			lngTown = lngTownNumber;
			strBeginFiscal = strFiscalStart;
			strEndFiscal = strFiscalEnd;
			dblREValuation = lngREValuation;
			#if ModuleID_1
						            // re
            modGlobalVariables.Statics.CustomizedInfo.CurrentTownNumber = lngTown;

#else
			// bl
			#endif
			//FC:FINAL:DDU:#i1484 - make form modeless
			this.Show(App.MainForm);
		}

		private void frmCertificateOfAssessment_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCertificateOfAssessment_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCertificateOfAssessment properties;
			//frmCertificateOfAssessment.FillStyle	= 0;
			//frmCertificateOfAssessment.ScaleWidth	= 9300;
			//frmCertificateOfAssessment.ScaleHeight	= 7575;
			//frmCertificateOfAssessment.LinkTopic	= "Form2";
			//frmCertificateOfAssessment.LockControls	= true;
			//frmCertificateOfAssessment.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			LoadData();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				rptCertificateOfAssessment.InstancePtr.Init(ref dblREValuation, ref strBeginFiscal, ref strEndFiscal, this.Modal);
			}
		}

		private void LoadData()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from taxratecalculation where isnull(townnumber,0)  = " + FCConvert.ToString(lngTown), modGlobalVariables.strREDatabase);
				if (!rsLoad.EndOfFile())
				{
					if (Information.IsDate(rsLoad.Get_Fields("commitmentdate")))
					{
						if (rsLoad.Get_Fields_DateTime("commitmentdate").ToOADate() != 0)
						{
							T2KCommitmentDate.Text = Strings.Format(rsLoad.Get_Fields_DateTime("commitmentdate"), "MM/dd/yyyy");
						}
					}
					if (!Information.IsDate(T2KCommitmentDate.Text))
					{
						T2KCommitmentDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
					}
					// TODO: Check the table for the column [duedate] and replace with corresponding Get_Field method
					if (Information.IsDate(rsLoad.Get_Fields("duedate")))
					{
						// TODO: Check the table for the column [duedate] and replace with corresponding Get_Field method
						if (rsLoad.Get_Fields("duedate").ToOADate() != 0)
						{
							// TODO: Check the table for the column [duedate] and replace with corresponding Get_Field method
							t2kDueDate.Text = Strings.Format(rsLoad.Get_Fields("duedate"), "MM/dd/yyyy");
						}
					}
					if (Information.IsDate(rsLoad.Get_Fields("duedate2")))
					{
						if (rsLoad.Get_Fields_DateTime("duedate2").ToOADate() != 0)
						{
							T2KDueDate2.Text = Strings.Format(rsLoad.Get_Fields_DateTime("duedate2"), "MM/dd/yyyy");
						}
					}
					if (Information.IsDate(rsLoad.Get_Fields("duedate3")))
					{
						if (rsLoad.Get_Fields_DateTime("duedate3").ToOADate() != 0)
						{
							T2KDueDate3.Text = Strings.Format(rsLoad.Get_Fields_DateTime("duedate3"), "MM/dd/yyyy");
						}
					}
					if (Information.IsDate(rsLoad.Get_Fields("duedate4")))
					{
						if (rsLoad.Get_Fields_DateTime("duedate4").ToOADate() != 0)
						{
							T2KDueDate4.Text = Strings.Format(rsLoad.Get_Fields_DateTime("duedate4"), "MM/dd/yyyy");
						}
					}
					if (Information.IsDate(rsLoad.Get_Fields("interestdate2")))
					{
						if (rsLoad.Get_Fields_DateTime("interestdate2").ToOADate() != 0)
						{
							T2KInterestDate2.Text = Strings.Format(rsLoad.Get_Fields_DateTime("interestdate2"), "MM/dd/yyyy");
						}
					}
					if (Information.IsDate(rsLoad.Get_Fields("interestdate")))
					{
						if (rsLoad.Get_Fields_DateTime("interestdate").ToOADate() != 0)
						{
							t2kInterestDate.Text = Strings.Format(rsLoad.Get_Fields_DateTime("interestdate"), "MM/dd/yyyy");
						}
					}
					if (Information.IsDate(rsLoad.Get_Fields("interestdate3")))
					{
						if (rsLoad.Get_Fields_DateTime("interestdate3").ToOADate() != 0)
						{
							T2KInterestDate3.Text = Strings.Format(rsLoad.Get_Fields_DateTime("interestdate3"), "MM/dd/yyyy");
						}
					}
					if (Information.IsDate(rsLoad.Get_Fields("interestdate4")))
					{
						if (rsLoad.Get_Fields_DateTime("interestdate4").ToOADate() != 0)
						{
							T2KInterestDate4.Text = Strings.Format(rsLoad.Get_Fields_DateTime("interestdate4"), "MM/dd/yyyy");
						}
					}
					if (Information.IsDate(rsLoad.Get_Fields("collectiondate")))
					{
						if (rsLoad.Get_Fields_DateTime("collectiondate").ToOADate() != 0)
						{
							t2kCollectionDate.Text = Strings.Format(rsLoad.Get_Fields_DateTime("collectiondate"), "MM/dd/yyyy");
						}
					}
					if (Conversion.Val(rsLoad.Get_Fields_Int32("startpage")) > 0)
					{
						txtPage1.Text = FCConvert.ToString(rsLoad.Get_Fields_Int32("startpage"));
					}
					if (Conversion.Val(rsLoad.Get_Fields_Int32("endpage")) > 0)
					{
						txtPage2.Text = FCConvert.ToString(rsLoad.Get_Fields_Int32("endpage"));
					}
					// TODO: Check the table for the column [county] and replace with corresponding Get_Field method
					txtCounty.Text = FCConvert.ToString(rsLoad.Get_Fields("county"));
					txtTreasurer.Text = FCConvert.ToString(rsLoad.Get_Fields_String("treasurer"));
					txtTaxCollector.Text = FCConvert.ToString(rsLoad.Get_Fields_String("taxcollector"));
					txtPercent.Text = FCConvert.ToString(Conversion.Val(rsLoad.Get_Fields_Double("interestrate")));
					if (strBeginFiscal == "" || strEndFiscal == "")
					{
						if (Information.IsDate(rsLoad.Get_Fields("FiscalStartDate")))
						{
							if (rsLoad.Get_Fields_DateTime("fiscalstartdate").ToOADate() != 0)
							{
								strBeginFiscal = Strings.Format(rsLoad.Get_Fields_DateTime("fiscalstartdate"), "MM/dd/yyyy");
							}
						}
						if (Information.IsDate(rsLoad.Get_Fields("fiscalenddate")))
						{
							if (rsLoad.Get_Fields_DateTime("fiscalenddate").ToOADate() != 0)
							{
								strEndFiscal = Strings.Format(rsLoad.Get_Fields_DateTime("fiscalenddate"), "MM/dd/yyyy");
							}
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error number " + FCConvert.ToString(Information.Err(ex).Number));
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			try
			{
				// On Error GoTo ErrorHandler
				SaveData = false;
				clsDRWrapper rsSave = new clsDRWrapper();
				if (!Information.IsDate(T2KCommitmentDate.Text))
				{
					MessageBox.Show("Invalid Commitment Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SaveData;
				}
				if (!Information.IsDate(t2kInterestDate.Text))
				{
					MessageBox.Show("Invalid Interest Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SaveData;
				}
				if (!Information.IsDate(t2kDueDate.Text))
				{
					MessageBox.Show("Invalid Due Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SaveData;
				}
				if (!Information.IsDate(t2kCollectionDate.Text))
				{
					MessageBox.Show("Invalid collection date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return SaveData;
				}
				// If Val(txtPage1.Text) < 1 Or Val(txtPage2.Text) < 1 Or Val(txtPage2.Text) < Val(txtPage1.Text) Then
				// MsgBox "Invalid page range", vbExclamation, "Invalid Pages"
				// Exit Function
				// End If
				rsSave.OpenRecordset("select * from taxratecalculation where isnull(townnumber,0)  = " + FCConvert.ToString(lngTown), modGlobalVariables.strREDatabase);
				if (!rsSave.EndOfFile())
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
				}
				if (!Information.IsDate(T2KDueDate2.Text))
				{
					rsSave.Set_Fields("duedate2", 0);
				}
				else
				{
					rsSave.Set_Fields("duedate2", T2KDueDate2.Text);
				}
				if (!Information.IsDate(T2KDueDate3.Text))
				{
					rsSave.Set_Fields("duedate3", 0);
				}
				else
				{
					rsSave.Set_Fields("duedate3", T2KDueDate3.Text);
				}
				if (!Information.IsDate(T2KDueDate4.Text))
				{
					rsSave.Set_Fields("duedate4", 0);
				}
				else
				{
					rsSave.Set_Fields("duedate4", T2KDueDate4.Text);
				}
				if (!Information.IsDate(T2KInterestDate2.Text))
				{
					rsSave.Set_Fields("interestdate2", 0);
				}
				else
				{
					rsSave.Set_Fields("interestdate2", T2KInterestDate2.Text);
				}
				if (!Information.IsDate(T2KInterestDate3.Text))
				{
					rsSave.Set_Fields("interestdate3", 0);
				}
				else
				{
					rsSave.Set_Fields("interestdate3", T2KInterestDate3.Text);
				}
				if (!Information.IsDate(T2KInterestDate4.Text))
				{
					rsSave.Set_Fields("interestdate4", 0);
				}
				else
				{
					rsSave.Set_Fields("interestdate4", T2KInterestDate4.Text);
				}
				if (!Information.IsDate(T2KCommitmentDate.Text))
				{
					rsSave.Set_Fields("commitmentdate", 0);
				}
				else
				{
					rsSave.Set_Fields("commitmentdate", T2KCommitmentDate.Text);
				}
				rsSave.Set_Fields("townnumber", lngTown);
				rsSave.Set_Fields("county", txtCounty.Text);
				rsSave.Set_Fields("taxcollector", txtTaxCollector.Text);
				rsSave.Set_Fields("Treasurer", txtTreasurer.Text);
				rsSave.Set_Fields("DueDate", t2kDueDate.Text);
				rsSave.Set_Fields("InterestDate", t2kInterestDate.Text);
				rsSave.Set_Fields("STARTpage", FCConvert.ToString(Conversion.Val(txtPage1.Text)));
				rsSave.Set_Fields("endpage", FCConvert.ToString(Conversion.Val(txtPage2.Text)));
				rsSave.Set_Fields("collectiondate", t2kCollectionDate.Text);
				rsSave.Set_Fields("interestrate", txtPercent.Text);
				rsSave.Update();
				SaveData = true;
				MessageBox.Show("Save successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void cmdSaveAndContinue_Click(object sender, EventArgs e)
		{
			mnuSaveContinue_Click(mnuSaveContinue, EventArgs.Empty);
		}
	}
}
