﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	public class modSecurityValidation
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/26/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               01/26/2004              *
		// ********************************************************
		// VBto upgrade warning: obForm As object	OnWrite(MDIParent)
		public static bool ValidPermissions(dynamic obForm, int lngFuncID, bool boolHandlePartials = true)
		{
			bool ValidPermissions = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strPerm;
				// checks the permissions and returns false if there are no permissions
				// if there are partial permissions then it calls the function for the form
				// if false is passed in then partial permissions will not be handled by the function
				// This function is for MDI menu options.  At the time this is called the child
				// menu options wont be present, so you cant disable them yet
				strPerm = FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngFuncID));
				if (strPerm == "F")
				{
					ValidPermissions = true;
				}
				else if (strPerm == "N")
				{
					ValidPermissions = false;
				}
				else if (strPerm == "P")
				{
					ValidPermissions = true;
					if (boolHandlePartials)
					{
						FCUtils.CallByName(obForm, "HandlePartialPermission", CallType.Method, lngFuncID);
					}
				}
				// ValidPermissions = True
				return ValidPermissions;
				// 
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCMessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message);
			}
			return ValidPermissions;
		}
	}
}
