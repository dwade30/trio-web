﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using fecherFoundation;

namespace Global
{
	/// <summary>
	/// Summary description for frmChooseCustomBillType.
	/// </summary>
	partial class frmChooseCustomBillType : BaseForm
	{
		public fecherFoundation.FCGrid GridBillType;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.GridBillType = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridBillType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 469);
			this.BottomPanel.Size = new System.Drawing.Size(593, 20);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdSave);
			this.ClientArea.Controls.Add(this.GridBillType);
			this.ClientArea.Size = new System.Drawing.Size(593, 409);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(593, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(182, 30);
			this.HeaderText.Text = "Choose Format";
			// 
			// GridBillType
			// 
			this.GridBillType.AllowSelection = false;
			this.GridBillType.AllowUserToResizeColumns = false;
			this.GridBillType.AllowUserToResizeRows = false;
			this.GridBillType.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.GridBillType.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridBillType.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridBillType.BackColorBkg = System.Drawing.Color.Empty;
			this.GridBillType.BackColorFixed = System.Drawing.Color.Empty;
			this.GridBillType.BackColorSel = System.Drawing.Color.Empty;
			this.GridBillType.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridBillType.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridBillType.ColumnHeadersHeight = 30;
			this.GridBillType.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridBillType.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridBillType.DragIcon = null;
			this.GridBillType.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridBillType.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.GridBillType.ExtendLastCol = true;
			this.GridBillType.FixedCols = 0;
			this.GridBillType.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridBillType.FrozenCols = 0;
			this.GridBillType.GridColor = System.Drawing.Color.Empty;
			this.GridBillType.GridColorFixed = System.Drawing.Color.Empty;
			this.GridBillType.Location = new System.Drawing.Point(30, 30);
			this.GridBillType.Name = "GridBillType";
			this.GridBillType.ReadOnly = true;
			this.GridBillType.RowHeadersVisible = false;
			this.GridBillType.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridBillType.RowHeightMin = 0;
			this.GridBillType.Rows = 1;
			this.GridBillType.ScrollTipText = null;
			this.GridBillType.ShowColumnVisibilityMenu = false;
			this.GridBillType.Size = new System.Drawing.Size(533, 233);
			this.GridBillType.StandardTab = true;
			this.GridBillType.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridBillType.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridBillType.TabIndex = 0;
			this.GridBillType.DoubleClick += new System.EventHandler(this.GridBillType_DblClick);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveContinue,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(219, 339);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(155, 48);
			this.cmdSave.TabIndex = 1;
			this.cmdSave.Text = "Save & Continue";
			this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
			// 
			// frmChooseCustomBillType
			// 
			this.ClientSize = new System.Drawing.Size(593, 489);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmChooseCustomBillType";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Choose Format";
			this.Load += new System.EventHandler(this.frmChooseCustomBillType_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmChooseCustomBillType_KeyDown);
			this.Resize += new System.EventHandler(this.frmChooseCustomBillType_Resize);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridBillType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
