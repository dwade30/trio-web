﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

#if TWCR0000
using TWCR0000;


#elif TWUT0000
using modGlobal = TWUT0000.modMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for sarUTDABookSummary.
	/// </summary>
	public partial class sarUTDABookSummary : FCSectionReport
	{
		// nObj = 1
		//   0	sarUTDABookSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/10/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/15/2004              *
		// ********************************************************
		float lngWidth;
		clsDRWrapper rsData = new clsDRWrapper();
		string strSQL = "";
		bool boolEndReport;
		double dblPrinSum;
		double dblTaxSum;
		double dblIntSum;
		double dblCostSum;
		bool boolWide;
		int intType;

		public sarUTDABookSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += SarUTDABookSummary_ReportEnd;
		}

        private void SarUTDABookSummary_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        public static sarUTDABookSummary InstancePtr
		{
			get
			{
				return (sarUTDABookSummary)Sys.GetInstance(typeof(sarUTDABookSummary));
			}
		}

		protected sarUTDABookSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile() != true)
			{
				eArgs.EOF = false;
				BindFields();
			}
			else
			{
				eArgs.EOF = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQLAddition = "";
			// intType = rptUTDailyAuditMaster.intType
			intType = FCConvert.ToInt16(this.UserData);
			// default the totals
			fldTotalPrin.Text = " 0.00";
			fldTotalTax.Text = "0.00";
			fldTotalInt.Text = " 0.00";
			fldTotalCosts.Text = " 0.00";
			fldTotalTotal.Text = " 0.00";
			// this will put the SQL string together and set the order
			// kk05072014 trouts-89  Changed SQL code for SQL server - added alias for subquery, changed MeterTable.MeterKey to MeterTable.ID, changed Bill to Bill.ID
			if (modGlobalConstants.Statics.gboolCR)
			{
				switch (intType)
				{
					case 0:
						{
							strSQLAddition = "AND PaymentRec.Service <> 'S' ";
							strSQL = "SELECT SUM(Prin) as PrinTotal, SUM(TTax) as TTaxTotal, SUM(Interest) AS InterestTotal, SUM(Cost) as CostTotal, BookNumber FROM (SELECT SUM(PaymentRec.Principal) AS Prin, SUM(PaymentRec.Tax) AS TTax, SUM(PaymentRec.CurrentInterest + PaymentRec.PreLienInterest) AS Interest, SUM(PaymentRec.LienCost) AS Cost, MeterTable.BookNumber as BookNumber FROM ((PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.ID) INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) WHERE lien = 0 AND ISNULL(PaymentRec.DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND PaymentRec.Code <> 'I' AND PaymentRec.ReceiptNumber > 0 " + "GROUP BY MeterTable.BookNumber UNION ALL SELECT SUM(PaymentRec.Principal) AS Prin, SUM(PaymentRec.Tax) AS TTax, SUM(PaymentRec.CurrentInterest + PaymentRec.PreLienInterest) AS Interest, SUM(PaymentRec.LienCost) AS Cost, MeterTable.BookNumber FROM ((PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.WLienRecordNumber) INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) " + "WHERE WCombinationLienKey = Bill.ID AND PaymentRec.Service = 'W' AND lien = 1 AND ISNULL(PaymentRec.DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " AND PaymentRec.Code <> 'I' AND PaymentRec.ReceiptNumber >= 0 GROUP BY MeterTable.BookNumber) AS qTmp GROUP BY BookNumber ORDER BY BookNumber";
							break;
						}
					case 1:
						{
							strSQLAddition = "AND PaymentRec.Service <> 'W' ";
							strSQL = "SELECT SUM(Prin) as PrinTotal, SUM(TTax) as TTaxTotal, SUM(Interest) AS InterestTotal, SUM(Cost) as CostTotal, BookNumber FROM (SELECT SUM(PaymentRec.Principal) AS Prin, SUM(PaymentRec.Tax) AS TTax, SUM(PaymentRec.CurrentInterest + PaymentRec.PreLienInterest) AS Interest, SUM(PaymentRec.LienCost) AS Cost, MeterTable.BookNumber as BookNumber FROM ((PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.ID) INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) WHERE lien = 0 AND ISNULL(PaymentRec.DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND PaymentRec.Code <> 'I' AND PaymentRec.ReceiptNumber > 0 " + "GROUP BY MeterTable.BookNumber UNION ALL SELECT SUM(PaymentRec.Principal) AS Prin, SUM(PaymentRec.Tax) AS TTax, SUM(PaymentRec.CurrentInterest + PaymentRec.PreLienInterest) AS Interest, SUM(PaymentRec.LienCost) AS Cost, MeterTable.BookNumber FROM ((PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.SLienRecordNumber) INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) " + "WHERE SCombinationLienKey = Bill.ID AND PaymentRec.Service = 'S' AND lien = 1 AND ISNULL(PaymentRec.DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " AND PaymentRec.Code <> 'I' AND PaymentRec.ReceiptNumber >= 0 GROUP BY MeterTable.BookNumber) AS qTmp GROUP BY BookNumber ORDER BY BookNumber";
							break;
						}
					case 2:
						{
							strSQLAddition = "";
							strSQL = "SELECT SUM(Prin) as PrinTotal, SUM(TTax) as TTaxTotal, SUM(Interest) AS InterestTotal, SUM(Cost) as CostTotal, BookNumber FROM (SELECT SUM(PaymentRec.Principal) AS Prin, SUM(PaymentRec.Tax) AS TTax, SUM(PaymentRec.CurrentInterest + PaymentRec.PreLienInterest) AS Interest, SUM(PaymentRec.LienCost) AS Cost, MeterTable.BookNumber as BookNumber FROM ((PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.ID) INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) WHERE lien = 0 AND ISNULL(PaymentRec.DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND PaymentRec.Code <> 'I' AND PaymentRec.ReceiptNumber > 0 " + "GROUP BY MeterTable.BookNumber UNION ALL SELECT SUM(PaymentRec.Principal) AS Prin, SUM(PaymentRec.Tax) AS TTax, SUM(PaymentRec.CurrentInterest + PaymentRec.PreLienInterest) AS Interest, SUM(PaymentRec.LienCost) AS Cost, MeterTable.BookNumber FROM ((PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.SLienRecordNumber) INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.MeterKey) " + "WHERE SCombinationLienKey = Bill.ID AND PaymentRec.Service = 'S' AND lien = 1 AND ISNULL(PaymentRec.DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " AND PaymentRec.Code <> 'I' AND PaymentRec.ReceiptNumber >= 0 GROUP BY MeterTable.BookNumber UNION ALL SELECT SUM(PaymentRec.Principal) AS Prin, SUM(PaymentRec.Tax) AS TTax, SUM(PaymentRec.CurrentInterest + PaymentRec.PreLienInterest) AS Interest, SUM(PaymentRec.LienCost) AS Cost, MeterTable.BookNumber FROM ((PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.WLienRecordNumber) INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) WHERE WCombinationLienKey = Bill.ID AND PaymentRec.Service = 'W' AND lien = 1 AND ISNULL(PaymentRec.DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " AND PaymentRec.Code <> 'I' AND PaymentRec.ReceiptNumber >= 0 GROUP BY MeterTable.BookNumber) AS qTmp GROUP BY BookNumber ORDER BY BookNumber";
							break;
						}
				}
				//end switch
			}
			else
			{
				switch (intType)
				{
					case 0:
						{
							strSQLAddition = "AND PaymentRec.Service <> 'S' ";
							strSQL = "SELECT SUM(Prin) as PrinTotal, SUM(TTax) as TTaxTotal, SUM(Interest) AS InterestTotal, SUM(Cost) as CostTotal, BookNumber FROM (SELECT SUM(PaymentRec.Principal) AS Prin, SUM(PaymentRec.Tax) AS TTax, SUM(PaymentRec.CurrentInterest + PaymentRec.PreLienInterest) AS Interest, SUM(PaymentRec.LienCost) AS Cost, MeterTable.BookNumber as BookNumber FROM ((PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.ID) INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) WHERE lien = 0 AND ISNULL(PaymentRec.DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND PaymentRec.Code <> 'I' GROUP BY MeterTable.BookNumber UNION ALL SELECT SUM(PaymentRec.Principal) AS Prin, SUM(PaymentRec.Tax) AS TTax, SUM(PaymentRec.CurrentInterest + PaymentRec.PreLienInterest) AS Interest, SUM(PaymentRec.LienCost) AS Cost, MeterTable.BookNumber FROM ((PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.WLienRecordNumber) INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) " + "WHERE WCombinationLienKey = Bill.ID AND PaymentRec.Service = 'W' AND lien = 1 AND ISNULL(PaymentRec.DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " AND PaymentRec.Code <> 'I' GROUP BY MeterTable.BookNumber) AS qTmp GROUP BY BookNumber ORDER BY BookNumber";
							break;
						}
					case 1:
						{
							strSQLAddition = "AND PaymentRec.Service <> 'W' ";
							strSQL = "SELECT SUM(Prin) as PrinTotal, SUM(TTax) as TTaxTotal, SUM(Interest) AS InterestTotal, SUM(Cost) as CostTotal, BookNumber FROM (SELECT SUM(PaymentRec.Principal) AS Prin, SUM(PaymentRec.Tax) AS TTax, SUM(PaymentRec.CurrentInterest + PaymentRec.PreLienInterest) AS Interest, SUM(PaymentRec.LienCost) AS Cost, MeterTable.BookNumber as BookNumber FROM ((PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.ID) INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) WHERE lien = 0 AND ISNULL(PaymentRec.DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND PaymentRec.Code <> 'I' GROUP BY MeterTable.BookNumber UNION ALL SELECT SUM(PaymentRec.Principal) AS Prin, SUM(PaymentRec.Tax) AS TTax, SUM(PaymentRec.CurrentInterest + PaymentRec.PreLienInterest) AS Interest, SUM(PaymentRec.LienCost) AS Cost, MeterTable.BookNumber FROM ((PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.SLienRecordNumber) INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) " + "WHERE SCombinationLienKey = Bill.ID AND PaymentRec.Service = 'S' AND lien = 1 AND ISNULL(PaymentRec.DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " AND PaymentRec.Code <> 'I' GROUP BY MeterTable.BookNumber) AS qTmp GROUP BY BookNumber ORDER BY BookNumber";
							break;
						}
					case 2:
						{
							strSQLAddition = "";
							strSQL = "SELECT SUM(Prin) as PrinTotal, SUM(TTax) as TTaxTotal, SUM(Interest) AS InterestTotal, SUM(Cost) as CostTotal, BookNumber FROM (SELECT SUM(PaymentRec.Principal) AS Prin, SUM(PaymentRec.Tax) AS TTax, SUM(PaymentRec.CurrentInterest + PaymentRec.PreLienInterest) AS Interest, SUM(PaymentRec.LienCost) AS Cost, MeterTable.BookNumber as BookNumber FROM ((PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.ID) INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) WHERE lien = 0 AND ISNULL(PaymentRec.DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " " + strSQLAddition + "AND PaymentRec.Code <> 'I' GROUP BY MeterTable.BookNumber UNION ALL SELECT SUM(PaymentRec.Principal) AS Prin, SUM(PaymentRec.Tax) AS TTax, SUM(PaymentRec.CurrentInterest + PaymentRec.PreLienInterest) AS Interest, SUM(PaymentRec.LienCost) AS Cost, MeterTable.BookNumber FROM ((PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.SLienRecordNumber) INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) " + "WHERE SCombinationLienKey = Bill.ID AND PaymentRec.Service = 'S' AND lien = 1 AND ISNULL(PaymentRec.DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " AND PaymentRec.Code <> 'I' GROUP BY MeterTable.BookNumber UNION ALL SELECT SUM(PaymentRec.Principal) AS Prin, SUM(PaymentRec.Tax) AS TTax, SUM(PaymentRec.CurrentInterest + PaymentRec.PreLienInterest) AS Interest, SUM(PaymentRec.LienCost) AS Cost, MeterTable.BookNumber FROM ((PaymentRec INNER JOIN Bill ON PaymentRec.BillKey = Bill.WLienRecordNumber) INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) WHERE WCombinationLienKey = Bill.ID AND PaymentRec.Service = 'W' AND lien = 1 AND ISNULL(PaymentRec.DailyCloseOut,0) = " + FCConvert.ToString(modGlobal.Statics.glngCurrentCloseOut) + " AND PaymentRec.Code <> 'I' GROUP BY MeterTable.BookNumber) AS qTmp GROUP BY BookNumber ORDER BY BookNumber";
							break;
						}
				}
				//end switch
			}
			rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			SetupFields();
			frmWait.InstancePtr.IncrementProgress();
			if (rsData.EndOfFile())
			{
				EndRoutine();
			}
		}

		private void SetupFields()
		{
			// this will adjust the fields to thier correct
			boolWide = rptUTDailyAuditMaster.InstancePtr.boolLandscape;
			lngWidth = rptUTDailyAuditMaster.InstancePtr.lngWidth;
			// set the full page objects
			this.PrintWidth = lngWidth * 1.1F;
			// Me.Detail.Width = lngWidth
			lblHeader.Width = lngWidth - 1 / 1440F;
			lnHeader.X2 = lngWidth - 1 / 1440F;
			// header labels/lines
			if (!boolWide)
			{
				lblBook.Left = 720 / 1440F;
				lblPrincipal.Left = 2760 / 1440F;
				lblTax.Left = 4080 / 1440F;
				lblInterest.Left = 5610 / 1440F;
				lblCosts.Left = 7140 / 1440F;
				lblTotal.Left = 8670 / 1440F;
			}
			else
			{
				lblBook.Left = 1350 / 1440F;
				lblPrincipal.Left = 3470 / 1440F;
				lblTax.Left = 5730 / 1440F;
				lblInterest.Left = 7800 / 1440F;
				lblCosts.Left = 9580 / 1440F;
				lblTotal.Left = 11080 / 1440F;
			}
			// Lefts
			// detail section
			fldBook.Left = lblBook.Left;
			fldPrincipal.Left = lblPrincipal.Left;
			fldTax.Left = lblTax.Left;
			fldInterest.Left = lblInterest.Left;
			fldCosts.Left = lblCosts.Left;
			fldTotal.Left = lblTotal.Left;
			// footer labels/fields
			lblTotals.Left = lblBook.Left;
			fldTotalPrin.Left = lblPrincipal.Left;
			fldTotalTax.Left = lblTax.Left;
			fldTotalInt.Left = lblInterest.Left;
			fldTotalCosts.Left = lblCosts.Left;
			fldTotalTotal.Left = lblTotal.Left;
			// Widths
			// detail section
			fldBook.Width = lblBook.Width;
			fldPrincipal.Width = lblPrincipal.Width;
			fldTax.Width = lblTax.Width;
			fldInterest.Width = lblInterest.Width;
			fldCosts.Width = lblCosts.Width;
			fldTotal.Width = lblTotal.Width;
			// footer labels/fields
			lblTotals.Width = lblBook.Width;
			fldTotalPrin.Width = lblPrincipal.Width;
			fldTotalTax.Width = lblTax.Width;
			fldTotalInt.Width = lblInterest.Width;
			fldTotalCosts.Width = lblCosts.Width;
			fldTotalTotal.Width = lblTotal.Width;
			lnTotals.X1 = lblPrincipal.Left;
			lnTotals.X2 = fldTotalTotal.Left + fldTotalTotal.Width;
		}

		private void BindFields()
		{
			// this will fill the fields with the correct data
			frmWait.InstancePtr.IncrementProgress();
			fldBook.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BookNumber"));
			// TODO: Field [PrinTotal] not found!! (maybe it is an alias?)
			fldPrincipal.Text = Strings.Format(rsData.Get_Fields("PrinTotal"), "#,##0.00");
			// TODO: Field [PrinTotal] not found!! (maybe it is an alias?)
			dblPrinSum += FCConvert.ToDouble(rsData.Get_Fields("PrinTotal"));
			// TODO: Field [TTaxTotal] not found!! (maybe it is an alias?)
			fldTax.Text = Strings.Format(rsData.Get_Fields("TTaxTotal"), "#,##0.00");
			// TODO: Field [TTaxTotal] not found!! (maybe it is an alias?)
			dblTaxSum += FCConvert.ToDouble(rsData.Get_Fields("TTaxTotal"));
			// TODO: Field [InterestTotal] not found!! (maybe it is an alias?)
			fldInterest.Text = Strings.Format(rsData.Get_Fields("InterestTotal"), "#,##0.00");
			// TODO: Field [InterestTotal] not found!! (maybe it is an alias?)
			dblIntSum += FCConvert.ToDouble(rsData.Get_Fields("InterestTotal"));
			// TODO: Field [CostTotal] not found!! (maybe it is an alias?)
			fldCosts.Text = Strings.Format(rsData.Get_Fields("CostTotal"), "#,##0.00");
			// TODO: Field [CostTotal] not found!! (maybe it is an alias?)
			dblCostSum += FCConvert.ToDouble(rsData.Get_Fields("CostTotal"));
			// TODO: Field [PrinTotal] not found!! (maybe it is an alias?)
			// TODO: Field [InterestTotal] not found!! (maybe it is an alias?)
			// TODO: Field [TTaxTotal] not found!! (maybe it is an alias?)
			// TODO: Field [CostTotal] not found!! (maybe it is an alias?)
			fldTotal.Text = Strings.Format(rsData.Get_Fields("PrinTotal") + rsData.Get_Fields("InterestTotal") + rsData.Get_Fields("TTaxTotal") + rsData.Get_Fields("CostTotal"), "#,##0.00");
			// move to the next record
			rsData.MoveNext();
		}

		private void EndRoutine()
		{
			// this will show a message that there are no records to process and hide the rest of the report
			lnHeader.Visible = false;
			lblHeader.Visible = false;
			lnTotals.Visible = false;
			lblPrincipal.Visible = false;
			lblTax.Visible = false;
			lblInterest.Visible = false;
			lblCosts.Visible = false;
			lblTotal.Visible = false;
			fldTotalPrin.Visible = false;
			fldTotalTax.Visible = false;
			fldTotalInt.Visible = false;
			fldTotalCosts.Visible = false;
			fldTotalTotal.Visible = false;
			lblTotals.Visible = false;
			// lblTotals.Width = lngWidth - lblTotals.Left
			// lblTotals.Text = "There are no records to process."
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalPrin.Text = Strings.Format(dblPrinSum, "#,##0.00");
			fldTotalTax.Text = Strings.Format(dblTaxSum, "#,##0.00");
			fldTotalInt.Text = Strings.Format(dblIntSum, "#,##0.00");
			fldTotalCosts.Text = Strings.Format(dblCostSum, "#,##0.00");
			// calculate the total line
			fldTotalTotal.Text = Strings.Format(FCConvert.ToDouble(fldTotalPrin.Text) + FCConvert.ToDouble(fldTotalTax.Text) + FCConvert.ToDouble(fldTotalInt.Text) + FCConvert.ToDouble(fldTotalCosts.Text), "#,##0.00");
		}

		private void sarUTDABookSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarUTDABookSummary.Caption	= "Daily Audit Detail";
			//sarUTDABookSummary.Icon	= "sarUTDABookSummary.dsx":0000";
			//sarUTDABookSummary.Left	= 0;
			//sarUTDABookSummary.Top	= 0;
			//sarUTDABookSummary.Width	= 11880;
			//sarUTDABookSummary.Height	= 8175;
			//sarUTDABookSummary.StartUpPosition	= 3;
			//sarUTDABookSummary.SectionData	= "sarUTDABookSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
