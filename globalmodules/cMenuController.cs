﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using SharedApplication.Extensions;

namespace Global
{
	public class cMenuController
	{
		//=========================================================
		// vbPorter upgrade warning: indexCount As short	OnWriteFCConvert.ToInt32(
		public cMenuChoice CreateMenuItem(string strCommand, int lngCommandCode, string strDescription, int lngSecurityCode, string strOption, string strToolTip, ref int indexCount)
		{
			cMenuChoice CreateMenuItem = null;
			cMenuChoice mItem = new cMenuChoice();
			mItem.CommandCode = lngCommandCode;
			mItem.CommandName = strCommand;
			mItem.Description = strDescription;
			mItem.MenuCaption = strOption + ".";
			mItem.ToolTip = strToolTip;
			mItem.SecurityCode = lngSecurityCode;
			if (Strings.Trim(strOption) == "")
			{
				if (indexCount < 10)
				{
					mItem.MenuCaption = FCConvert.ToString(indexCount) + ".";
				}
				else
				{
					int intAsc = 0;
					intAsc = Convert.ToByte("A"[0]);
					intAsc = (indexCount - 10) + intAsc;
					mItem.MenuCaption = FCConvert.ToString(Convert.ToChar(intAsc)) + ".";
				}
			}
			indexCount += 1;
            var permission = modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(lngSecurityCode).ToString();
			if (permission == "N" ||  permission.IsNullOrWhiteSpace())
			{
				mItem.Enabled = false;
			}

			
			CreateMenuItem = mItem;
			return CreateMenuItem;
		}
	}
}
