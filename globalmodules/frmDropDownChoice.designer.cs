//Fecher vbPorter - Version 1.0.0.59
using System;
using Wisej.Web;
using Microsoft.VisualBasic;
using fecherFoundation;

namespace Global
{
	/// <summary>
	/// Summary description for frmDropDownChoice.
	/// </summary>
	partial class frmDropDownChoice
	{
		public fecherFoundation.FCComboBox cmbChoices;
		public fecherFoundation.FCLabel lblMessage;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbChoices = new fecherFoundation.FCComboBox();
			this.lblMessage = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 242);
			this.BottomPanel.Size = new System.Drawing.Size(430, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbChoices);
			this.ClientArea.Controls.Add(this.lblMessage);
			this.ClientArea.Size = new System.Drawing.Size(430, 182);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(430, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(218, 30);
			this.HeaderText.Text = "Drop Down Choice";
			// 
			// cmbChoices
			// 
			this.cmbChoices.AutoSize = false;
			this.cmbChoices.BackColor = System.Drawing.SystemColors.Window;
			this.cmbChoices.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbChoices.FormattingEnabled = true;
			this.cmbChoices.Location = new System.Drawing.Point(20, 105);
			this.cmbChoices.Name = "cmbChoices";
			this.cmbChoices.Size = new System.Drawing.Size(381, 40);
			this.cmbChoices.TabIndex = 0;
			// 
			// lblMessage
			// 
			this.lblMessage.Location = new System.Drawing.Point(20, 30);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(381, 55);
			this.lblMessage.TabIndex = 1;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 0;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSave,
            this.mnuSepar1,
            this.mnuExit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 0;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSave.Text = "Save & Continue";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuSepar1
			// 
			this.mnuSepar1.Index = 1;
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(80, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(180, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save & Continue";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// frmDropDownChoice
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.ClientSize = new System.Drawing.Size(430, 350);
			this.FillColor = 0;
			this.Name = "frmDropDownChoice";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "";
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}