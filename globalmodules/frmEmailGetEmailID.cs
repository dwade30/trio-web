﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmEmailGetEmailID.
	/// </summary>
	public partial class frmEmailGetEmailID : BaseForm
	{
		public frmEmailGetEmailID()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEmailGetEmailID InstancePtr
		{
			get
			{
				return (frmEmailGetEmailID)Sys.GetInstance(typeof(frmEmailGetEmailID));
			}
		}

		protected frmEmailGetEmailID _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private int lngBoxType;
		private int lngUserID;
		private int lngReturn;
		const int CNSTGRIDCOLid = 0;
		const int CNSTGRIDCOLRECIPIENT = 1;
		const int CNSTGRIDCOLSUBJECT = 2;
		const int CNSTGRIDCOLSENT = 3;

		public int Init(int lngBox, int lngUser)
		{
			int Init = 0;
			lngReturn = 0;
			lngBoxType = lngBox;
			lngUserID = lngUser;
			Init = 0;
			this.Show(FormShowEnum.Modal);
			Init = lngReturn;
			return Init;
		}

		private void SetupGrid()
		{
			Grid.Rows = 1;
			Grid.TextMatrix(0, CNSTGRIDCOLRECIPIENT, "Recipient");
			Grid.TextMatrix(0, CNSTGRIDCOLSUBJECT, "Subject");
			Grid.TextMatrix(0, CNSTGRIDCOLSENT, "Sent");
			Grid.ColHidden(CNSTGRIDCOLid, true);
		}

		private void frmEmailGetEmailID_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmEmailGetEmailID_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEmailGetEmailID.Icon	= "frmEmailGetEmailID.frx":0000";
			//frmEmailGetEmailID.FillStyle	= 0;
			//frmEmailGetEmailID.ScaleWidth	= 5880;
			//frmEmailGetEmailID.ScaleHeight	= 3810;
			//frmEmailGetEmailID.LinkTopic	= "Form2";
			//frmEmailGetEmailID.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//Grid.BackColor	= "-2147483643";
			//			//Grid.ForeColor	= "-2147483640";
			//Grid.BorderStyle	= 1;
			//Grid.FillStyle	= 0;
			//Grid.Appearance	= 1;
			//Grid.GridLines	= 1;
			//Grid.WordWrap	= 0;
			//Grid.ScrollBars	= 2;
			//Grid.RightToLeft	= 0;
			//Grid._cx	= 9737;
			//Grid._cy	= 5927;
			//Grid._ConvInfo	= 1;
			//Grid.MousePointer	= 0;
			//Grid.BackColorFixed	= -2147483633;
			//			//Grid.ForeColorFixed	= -2147483630;
			//Grid.BackColorSel	= -2147483635;
			//			//Grid.ForeColorSel	= -2147483634;
			//Grid.BackColorBkg	= -2147483636;
			//Grid.BackColorAlternate	= -2147483643;
			//Grid.GridColor	= -2147483633;
			//Grid.GridColorFixed	= -2147483632;
			//Grid.TreeColor	= -2147483632;
			//Grid.FloodColor	= 192;
			//Grid.SheetBorder	= -2147483642;
			//Grid.FocusRect	= 0;
			//Grid.HighLight	= 1;
			//Grid.AllowSelection	= false;
			//Grid.AllowBigSelection	= false;
			//Grid.AllowUserResizing	= 0;
			//Grid.SelectionMode	= 1;
			//Grid.GridLinesFixed	= 2;
			//Grid.GridLineWidth	= 1;
			//Grid.RowHeightMin	= 0;
			//Grid.RowHeightMax	= 0;
			//Grid.ColWidthMin	= 0;
			//Grid.ColWidthMax	= 0;
			//Grid.ExtendLastCol	= true;
			//Grid.FormatString	= "";
			//Grid.ScrollTrack	= true;
			//Grid.ScrollTips	= false;
			//Grid.MergeCells	= 0;
			//Grid.MergeCompare	= 0;
			//Grid.AutoResize	= true;
			//Grid.AutoSizeMode	= 0;
			//Grid.AutoSearch	= 0;
			//Grid.AutoSearchDelay	= 2;
			//Grid.MultiTotals	= true;
			//Grid.SubtotalPosition	= 1;
			//Grid.OutlineBar	= 0;
			//Grid.OutlineCol	= 0;
			//Grid.Ellipsis	= 0;
			//Grid.ExplorerBar	= 0;
			//Grid.PicturesOver	= false;
			//Grid.PictureType	= 0;
			//Grid.TabBehavior	= 0;
			//Grid.OwnerDraw	= 0;
			//Grid.ShowComboButton	= true;
			//Grid.TextStyle	= 0;
			//Grid.TextStyleFixed	= 0;
			//Grid.OleDragMode	= 0;
			//Grid.OleDropMode	= 0;
			//Grid.ComboSearch	= 3;
			//Grid.AutoSizeMouse	= true;
			//Grid.AllowUserFreezing	= 0;
			//Grid.BackColorFrozen	= 0;
			//			//Grid.ForeColorFrozen	= 0;
			//Grid.WallPaperAlignment	= 9;
			//vsElasticLight1.OleObjectBlob	= "frmEmailGetEmailID.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGrid();
			LoadGrid();
		}

		private void ResizeGrid()
		{
			int GridWidth = 0;
			GridWidth = Grid.WidthOriginal;
			Grid.ColWidth(CNSTGRIDCOLRECIPIENT, FCConvert.ToInt32(0.3 * GridWidth));
			Grid.ColWidth(CNSTGRIDCOLSUBJECT, FCConvert.ToInt32(0.4 * GridWidth));
		}

		private void frmEmailGetEmailID_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_DblClick(object sender, EventArgs e)
		{
			if (Grid.MouseRow < 1)
				return;
			lngReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.MouseRow, CNSTGRIDCOLid))));
			Close();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
			{
				FCMessageBox.Show("You must select an E-mail first", MsgBoxStyle.Exclamation, "No Selection");
				return;
			}
			lngReturn = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLid))));
			Close();
		}

		private void LoadGrid()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				rsLoad.OpenRecordset("select * from emailboxes where boxtype = " + FCConvert.ToString(lngBoxType) + " and userid = " + FCConvert.ToString(lngUserID) + " order by id ", "SystemSettings");
				while (!rsLoad.EndOfFile())
				{
					Grid.Rows += 1;
					lngRow = Grid.Rows - 1;
					Grid.TextMatrix(lngRow, CNSTGRIDCOLid, FCConvert.ToString(rsLoad.Get_Fields_Int32("id")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLRECIPIENT, FCConvert.ToString(rsLoad.Get_Fields_String("recipient")));
					Grid.TextMatrix(lngRow, CNSTGRIDCOLSUBJECT, FCConvert.ToString(rsLoad.Get_Fields_String("subject")));
					if (lngBoxType == modEMail.CNSTEMAILSENTBOX)
					{
						Grid.TextMatrix(lngRow, CNSTGRIDCOLSENT, Strings.Format(rsLoad.Get_Fields_DateTime("senddate"), "MM/dd/yyyy"));
					}
					rsLoad.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In LoadGrid", MsgBoxStyle.Critical, "Error");
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuSaveContinue_Click(sender, e);
		}
	}
}
