﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for frmEmailGroups.
	/// </summary>
	public partial class frmEmailGroups : BaseForm
	{
		public frmEmailGroups()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEmailGroups InstancePtr
		{
			get
			{
				return (frmEmailGroups)Sys.GetInstance(typeof(frmEmailGroups));
			}
		}

		protected frmEmailGroups _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private int lngGroupID;
		// VBto upgrade warning: boolDataChanged As bool	OnWrite(bool, short)
		private bool boolDataChanged;
		private bool boolUnloadMe;
		private bool boolAmModal;
		const int CNSTGRIDCOLCONTACTID = 0;
		const int CNSTGRIDCOLFIRSTNAME = 1;
		const int CNSTGRIDCOLLASTNAME = 2;
		const int CNSTGRIDCOLQUICKNAME = 3;
		const int CNSTGRIDCOLEMAIL = 4;
		// Private Const CNSTGRIDCOLINCLUDE = 5
		public void Init(int lngGroup = 0, bool boolModal = false, bool boolFromContacts = false)
		{
			boolUnloadMe = false;
			boolAmModal = boolModal;
			lngGroupID = lngGroup;
			// LoadGroup
			if (boolFromContacts)
			{
				Toolbar1.Buttons[7 - 1].Visible = false;
				cmdContacts.Visible = false;
			}
			if (boolModal)
			{
				this.Show(FormShowEnum.Modal);
			}
			else
			{
				this.Show(App.MainForm);
			}
		}

		private void frmEmailGroups_Activated(object sender, System.EventArgs e)
		{
			if (boolUnloadMe)
			{
				Close();
			}
		}

		private void frmEmailGroups_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmEmailGroups_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEmailGroups.Icon	= "frmEmailGroups.frx":0000";
			//frmEmailGroups.FillStyle	= 0;
			//frmEmailGroups.ScaleWidth	= 9300;
			//frmEmailGroups.ScaleHeight	= 7605;
			//frmEmailGroups.LinkTopic	= "Form2";
			//frmEmailGroups.LockControls	= true;
			//frmEmailGroups.PaletteMode	= 1  'UseZOrder;
			//Font.Size	= "9";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//GridDelete.BackColor	= "-2147483643";
			//			//GridDelete.ForeColor	= "-2147483640";
			//GridDelete.BorderStyle	= 1;
			//GridDelete.FillStyle	= 0;
			//GridDelete.Appearance	= 1;
			//GridDelete.GridLines	= 1;
			//GridDelete.WordWrap	= 0;
			//GridDelete.ScrollBars	= 3;
			//GridDelete.RightToLeft	= 0;
			//GridDelete._cx	= 397;
			//GridDelete._cy	= 370;
			//GridDelete._ConvInfo	= 1;
			//GridDelete.MousePointer	= 0;
			//GridDelete.BackColorFixed	= -2147483633;
			//			//GridDelete.ForeColorFixed	= -2147483630;
			//GridDelete.BackColorSel	= -2147483635;
			//			//GridDelete.ForeColorSel	= -2147483634;
			//GridDelete.BackColorBkg	= -2147483636;
			//GridDelete.BackColorAlternate	= -2147483643;
			//GridDelete.GridColor	= -2147483633;
			//GridDelete.GridColorFixed	= -2147483632;
			//GridDelete.TreeColor	= -2147483632;
			//GridDelete.FloodColor	= 192;
			//GridDelete.SheetBorder	= -2147483642;
			//GridDelete.FocusRect	= 1;
			//GridDelete.HighLight	= 1;
			//GridDelete.AllowSelection	= true;
			//GridDelete.AllowBigSelection	= true;
			//GridDelete.AllowUserResizing	= 0;
			//GridDelete.SelectionMode	= 0;
			//GridDelete.GridLinesFixed	= 2;
			//GridDelete.GridLineWidth	= 1;
			//GridDelete.RowHeightMin	= 0;
			//GridDelete.RowHeightMax	= 0;
			//GridDelete.ColWidthMin	= 0;
			//GridDelete.ColWidthMax	= 0;
			//GridDelete.ExtendLastCol	= false;
			//GridDelete.FormatString	= "";
			//GridDelete.ScrollTrack	= false;
			//GridDelete.ScrollTips	= false;
			//GridDelete.MergeCells	= 0;
			//GridDelete.MergeCompare	= 0;
			//GridDelete.AutoResize	= true;
			//GridDelete.AutoSizeMode	= 0;
			//GridDelete.AutoSearch	= 0;
			//GridDelete.AutoSearchDelay	= 2;
			//GridDelete.MultiTotals	= true;
			//GridDelete.SubtotalPosition	= 1;
			//GridDelete.OutlineBar	= 0;
			//GridDelete.OutlineCol	= 0;
			//GridDelete.Ellipsis	= 0;
			//GridDelete.ExplorerBar	= 0;
			//GridDelete.PicturesOver	= false;
			//GridDelete.PictureType	= 0;
			//GridDelete.TabBehavior	= 0;
			//GridDelete.OwnerDraw	= 0;
			//GridDelete.ShowComboButton	= true;
			//GridDelete.TextStyle	= 0;
			//GridDelete.TextStyleFixed	= 0;
			//GridDelete.OleDragMode	= 0;
			//GridDelete.OleDropMode	= 0;
			//GridDelete.ComboSearch	= 3;
			//GridDelete.AutoSizeMouse	= true;
			//GridDelete.AllowUserFreezing	= 0;
			//GridDelete.BackColorFrozen	= 0;
			//			//GridDelete.ForeColorFrozen	= 0;
			//GridDelete.WallPaperAlignment	= 9;
			//GridContacts.BackColor	= "-2147483643";
			//			//GridContacts.ForeColor	= "-2147483640";
			//GridContacts.BorderStyle	= 1;
			//GridContacts.FillStyle	= 0;
			//GridContacts.Appearance	= 1;
			//GridContacts.GridLines	= 1;
			//GridContacts.WordWrap	= 0;
			//GridContacts.ScrollBars	= 2;
			//GridContacts.RightToLeft	= 0;
			//GridContacts._cx	= 15319;
			//GridContacts._cy	= 8176;
			//GridContacts._ConvInfo	= 1;
			//GridContacts.MousePointer	= 0;
			//GridContacts.BackColorFixed	= -2147483633;
			//			//GridContacts.ForeColorFixed	= -2147483630;
			//GridContacts.BackColorSel	= -2147483635;
			//			//GridContacts.ForeColorSel	= -2147483634;
			//GridContacts.BackColorBkg	= -2147483636;
			//GridContacts.BackColorAlternate	= -2147483643;
			//GridContacts.GridColor	= -2147483633;
			//GridContacts.GridColorFixed	= -2147483632;
			//GridContacts.TreeColor	= -2147483632;
			//GridContacts.FloodColor	= 192;
			//GridContacts.SheetBorder	= -2147483642;
			//GridContacts.FocusRect	= 1;
			//GridContacts.HighLight	= 0;
			//GridContacts.AllowSelection	= true;
			//GridContacts.AllowBigSelection	= true;
			//GridContacts.AllowUserResizing	= 0;
			//GridContacts.SelectionMode	= 0;
			//GridContacts.GridLinesFixed	= 2;
			//GridContacts.GridLineWidth	= 1;
			//GridContacts.RowHeightMin	= 0;
			//GridContacts.RowHeightMax	= 0;
			//GridContacts.ColWidthMin	= 0;
			//GridContacts.ColWidthMax	= 0;
			//GridContacts.ExtendLastCol	= true;
			//GridContacts.FormatString	= "";
			//GridContacts.ScrollTrack	= true;
			//GridContacts.ScrollTips	= false;
			//GridContacts.MergeCells	= 0;
			//GridContacts.MergeCompare	= 0;
			//GridContacts.AutoResize	= true;
			//GridContacts.AutoSizeMode	= 0;
			//GridContacts.AutoSearch	= 0;
			//GridContacts.AutoSearchDelay	= 2;
			//GridContacts.MultiTotals	= true;
			//GridContacts.SubtotalPosition	= 1;
			//GridContacts.OutlineBar	= 0;
			//GridContacts.OutlineCol	= 0;
			//GridContacts.Ellipsis	= 0;
			//GridContacts.ExplorerBar	= 1;
			//GridContacts.PicturesOver	= false;
			//GridContacts.PictureType	= 0;
			//GridContacts.TabBehavior	= 1;
			//GridContacts.OwnerDraw	= 0;
			//GridContacts.ShowComboButton	= true;
			//GridContacts.TextStyle	= 0;
			//GridContacts.TextStyleFixed	= 0;
			//GridContacts.OleDragMode	= 0;
			//GridContacts.OleDropMode	= 0;
			//GridContacts.ComboSearch	= 3;
			//GridContacts.AutoSizeMouse	= true;
			//GridContacts.AllowUserFreezing	= 0;
			//GridContacts.BackColorFrozen	= 0;
			//			//GridContacts.ForeColorFrozen	= 0;
			//GridContacts.WallPaperAlignment	= 9;
			//Toolbar1.ButtonWidth	= 820;
			//Toolbar1.ButtonHeight	= 794;
			//Buttons.NumButtons	= 7;
			//Images.NumListImages	= 7;
			//vsElasticLight1.OleObjectBlob	= "frmEmailGroups.frx":3660";
			//End Unmaped Properties
			boolUnloadMe = false;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			txtQuickName.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
			txtDescription.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
			SetupGridContacts();
			if (lngGroupID == 0)
			{
				int lngID = 0;
				string strTemp = "";
				string strTemp2 = "";
				lngID = 0;
				strTemp = "";
				frmEmailChooseContactGroup.InstancePtr.Init(false, true, ref lngID, ref strTemp, false, false, ref strTemp2, true);
				lngGroupID = lngID;
				if (lngID < 0)
				{
					boolUnloadMe = true;
					return;
				}
			}
			LoadGroup();
		}

		private void LoadGroup()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				if (lngGroupID != 0)
				{
					rsLoad.OpenRecordset("Select * from emailgroups where groupid = " + FCConvert.ToString(lngGroupID), "SystemSettings");
					if (!rsLoad.EndOfFile())
					{
						txtQuickName.Text = FCConvert.ToString(rsLoad.Get_Fields_String("nickname"));
						txtDescription.Text = FCConvert.ToString(rsLoad.Get_Fields_String("description"));
						rsLoad.OpenRecordset("select * from emailassociations inner join emailcontacts on (emailcontacts.contactid = emailassociations.contactid) where groupid = " + FCConvert.ToString(lngGroupID) + " order by lastname,firstname,email", "SystemSettings");
						GridContacts.Rows = 1;
						while (!rsLoad.EndOfFile())
						{
							GridContacts.Rows += 1;
							lngRow = GridContacts.Rows - 1;
							// TODO: Field [emailcontacts.contactid] not found!! (maybe it is an alias?)
							GridContacts.TextMatrix(lngRow, CNSTGRIDCOLCONTACTID, FCConvert.ToString(rsLoad.Get_Fields("emailcontacts.contactid")));
							GridContacts.TextMatrix(lngRow, CNSTGRIDCOLEMAIL, FCConvert.ToString(rsLoad.Get_Fields_String("email")));
							GridContacts.TextMatrix(lngRow, CNSTGRIDCOLFIRSTNAME, FCConvert.ToString(rsLoad.Get_Fields_String("firstname")));
							GridContacts.TextMatrix(lngRow, CNSTGRIDCOLLASTNAME, FCConvert.ToString(rsLoad.Get_Fields_String("lastname")));
							GridContacts.TextMatrix(lngRow, CNSTGRIDCOLQUICKNAME, FCConvert.ToString(rsLoad.Get_Fields_String("nickname")));
							GridContacts.RowData(lngRow, false);
							rsLoad.MoveNext();
						}
					}
					else
					{
						txtQuickName.Text = "";
						txtDescription.Text = "";
						GridContacts.Rows = 1;
					}
				}
				else
				{
					txtQuickName.Text = "";
					txtDescription.Text = "";
					GridContacts.Rows = 1;
				}
				boolDataChanged = false;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In LoadGroup", MsgBoxStyle.Critical, "Error");
			}
		}

		private void AddGroup()
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (boolDataChanged)
				{
					if (FCMessageBox.Show("Save changed data?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Save First?") == DialogResult.Yes)
					{
						if (!SaveGroup())
						{
							return;
						}
					}
				}
				lngGroupID = 0;
				txtDescription.Text = "";
				txtQuickName.Text = "";
				GridContacts.Rows = 1;
				GridDelete.Rows = 0;
				boolDataChanged = FCConvert.ToBoolean(0);
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In AddGroup", MsgBoxStyle.Critical, "Error");
			}
		}

		private void DeleteGroup()
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (lngGroupID > 0)
				{
					if (FCMessageBox.Show("This will delete this group" + "\r\n" + "Do you wish to continue?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Delete?") == DialogResult.Yes)
					{
						GridContacts.Rows = 1;
						GridDelete.Rows = 0;
						clsDRWrapper rsSave = new clsDRWrapper();
						rsSave.Execute("delete from emailassociations where groupid = " + FCConvert.ToString(lngGroupID), "SystemSettings");
						rsSave.Execute("delete from emailgroups where groupid = " + FCConvert.ToString(lngGroupID), "SystemSettings");
						txtDescription.Text = "";
						txtQuickName.Text = "";
						lngGroupID = 0;
					}
				}
				else
				{
					txtDescription.Text = "";
					txtQuickName.Text = "";
					GridContacts.Rows = 1;
					GridDelete.Rows = 0;
				}
				boolDataChanged = false;
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In DeleteGroup", MsgBoxStyle.Critical, "Error");
			}
		}

		private void DeleteContact()
		{
			try
			{
				// On Error GoTo ErrorHandler
				int lngRow;
				boolDataChanged = true;
				if (GridContacts.Rows < 2)
				{
					FCMessageBox.Show("There are no contacts to delete", MsgBoxStyle.Exclamation, "No Contacts");
					return;
				}
				lngRow = GridContacts.Row;
				if (lngRow < 1)
				{
					FCMessageBox.Show("You must select a contact first", MsgBoxStyle.Exclamation, "No Contact Selected");
					return;
				}
				GridDelete.Rows += 1;
				GridDelete.TextMatrix(GridDelete.Rows - 1, 0, FCConvert.ToString(Conversion.Val(GridContacts.TextMatrix(lngRow, CNSTGRIDCOLCONTACTID))));
				GridContacts.RemoveItem(lngRow);
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In DeleteContact", MsgBoxStyle.Critical, "Error");
			}
		}

		private void AddContact()
		{
			try
			{
				// On Error GoTo ErrorHandler
				int lngID = 0;
				clsDRWrapper rsLoad = new clsDRWrapper();
				int lngRow;
				string[] strCAry = null;
				string[] strDetAry = null;
				string strReturn;
				int x;
				bool boolDontAdd = false;
				string strTemp = "";
				strReturn = "";
				frmEmailChooseContactGroup.InstancePtr.Init(true, false, ref lngID, ref strTemp, false, true, ref strReturn, false);
				if (lngID > 0)
				{
					boolDataChanged = true;
					strCAry = Strings.Split(strReturn, ";", -1, CompareConstants.vbTextCompare);
					for (x = 0; x <= Information.UBound(strCAry, 1); x++)
					{
						strDetAry = Strings.Split(strCAry[x], ",", -1, CompareConstants.vbTextCompare);
						boolDontAdd = false;
						lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(strDetAry[1])));
						for (lngRow = 1; lngRow <= GridContacts.Rows - 1; lngRow++)
						{
							if (FCConvert.ToDouble(GridContacts.TextMatrix(lngRow, CNSTGRIDCOLCONTACTID)) == lngID)
							{
								// MsgBox "That contact has already been added to this group", MsgBoxStyle.Information, "Already Added"
								// Exit Sub
								boolDontAdd = true;
							}
						}
						// lngRow
						if (!boolDontAdd)
						{
							rsLoad.OpenRecordset("select * from emailcontacts where contactid = " + FCConvert.ToString(lngID), "SystemSettings");
							if (!rsLoad.EndOfFile())
							{
								GridContacts.Rows += 1;
								GridContacts.TextMatrix(lngRow, CNSTGRIDCOLCONTACTID, FCConvert.ToString(lngID));
								GridContacts.TextMatrix(lngRow, CNSTGRIDCOLEMAIL, FCConvert.ToString(rsLoad.Get_Fields_String("email")));
								GridContacts.TextMatrix(lngRow, CNSTGRIDCOLFIRSTNAME, FCConvert.ToString(rsLoad.Get_Fields_String("firstname")));
								GridContacts.TextMatrix(lngRow, CNSTGRIDCOLLASTNAME, FCConvert.ToString(rsLoad.Get_Fields_String("lastname")));
								GridContacts.TextMatrix(lngRow, CNSTGRIDCOLQUICKNAME, FCConvert.ToString(rsLoad.Get_Fields_String("nickname")));
								GridContacts.RowData(lngRow, true);
							}
						}
					}
					// x
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In AddContact", MsgBoxStyle.Critical, "Error");
			}
		}

		private void FindGroup()
		{
			try
			{
				// On Error GoTo ErrorHandler
				int lngID = 0;
				if (boolDataChanged)
				{
					if (FCMessageBox.Show("Save changed data?", MsgBoxStyle.Exclamation | MsgBoxStyle.YesNo, "Save First?") == DialogResult.Yes)
					{
						if (!SaveGroup())
						{
							return;
						}
					}
				}
				string temp = "";
				frmEmailChooseContactGroup.InstancePtr.Init(false, true, ref lngID, ref temp, false);
				if (lngID > 0)
				{
					lngGroupID = lngID;
					LoadGroup();
				}
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In FindGroup", MsgBoxStyle.Critical, "Error");
			}
		}

		private bool SaveGroup()
		{
			bool SaveGroup = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				int lngRow;
				SaveGroup = false;
				if (Strings.Trim(txtDescription.Text) == string.Empty)
				{
					FCMessageBox.Show("You must provide a group description", MsgBoxStyle.Exclamation, "No Description");
					return SaveGroup;
				}
				if (Strings.Trim(txtQuickName.Text) == string.Empty)
				{
					FCMessageBox.Show("You must provide a quickname", MsgBoxStyle.Exclamation, "No Quickname");
					return SaveGroup;
				}
				rsSave.OpenRecordset("select * from emailgroups where not groupid = " + FCConvert.ToString(lngGroupID) + " and nickname = '" + Strings.Trim(txtQuickName.Text) + "'", "SystemSettings");
				if (!rsSave.EndOfFile())
				{
					FCMessageBox.Show("This quickname is already used by a group", MsgBoxStyle.Exclamation, "Invalid Quickname");
					return SaveGroup;
				}
				rsSave.OpenRecordset("select * from emailcontacts where nickname = '" + Strings.Trim(txtQuickName.Text) + "'", "SystemSettings");
				if (!rsSave.EndOfFile())
				{
					FCMessageBox.Show("This quickname is already used by a contact", MsgBoxStyle.Exclamation, "Invalid Quickname");
					return SaveGroup;
				}
				rsSave.OpenRecordset("select * from emailgroups where groupid = " + FCConvert.ToString(lngGroupID), "SystemSettings");
				if (!rsSave.EndOfFile() && lngGroupID != 0)
				{
					rsSave.Edit();
				}
				else
				{
					rsSave.AddNew();
					// lngGroupID = rsSave.Get_Fields("groupid")
				}
				rsSave.Set_Fields("nickname", Strings.Trim(txtQuickName.Text));
				rsSave.Set_Fields("description", Strings.Trim(txtDescription.Text));
				rsSave.Update();
				lngGroupID = FCConvert.ToInt32(rsSave.Get_Fields_Int32("groupid"));
				for (lngRow = 0; lngRow <= GridDelete.Rows - 1; lngRow++)
				{
					rsSave.Execute("delete from emailassociations where contactid = " + FCConvert.ToString(Conversion.Val(GridDelete.TextMatrix(lngRow, 0))) + " and groupid = " + FCConvert.ToString(lngGroupID), "SystemSettings");
				}
				// lngRow
				GridDelete.Rows = 0;
				for (lngRow = 1; lngRow <= GridContacts.Rows - 1; lngRow++)
				{
					if (FCConvert.ToBoolean(GridContacts.RowData(lngRow)))
					{
						rsSave.OpenRecordset("select * from emailassociations where contactid = " + FCConvert.ToString(Conversion.Val(GridContacts.TextMatrix(lngRow, 0))) + " and groupid = " + FCConvert.ToString(lngGroupID), "SystemSettings");
						if (rsSave.EndOfFile())
						{
							rsSave.AddNew();
							rsSave.Set_Fields("contactid", FCConvert.ToString(Conversion.Val(GridContacts.TextMatrix(lngRow, 0))));
							rsSave.Set_Fields("groupid", lngGroupID);
							rsSave.Update();
						}
						GridContacts.RowData(lngRow, false);
					}
				}
				// lngRow
				boolDataChanged = false;
				SaveGroup = true;
				FCMessageBox.Show("Group saved", MsgBoxStyle.Information, "Saved");
				return SaveGroup;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In SaveGroup", MsgBoxStyle.Critical, "Error");
			}
			return SaveGroup;
		}

		private void SetupGridContacts()
		{
			// .ColDataType(CNSTGRIDCOLINCLUDE) = flexDTBoolean
			GridContacts.Rows = 1;
			GridContacts.ColHidden(CNSTGRIDCOLCONTACTID, true);
			GridContacts.TextMatrix(0, CNSTGRIDCOLEMAIL, "E-mail");
			GridContacts.TextMatrix(0, CNSTGRIDCOLFIRSTNAME, "First");
			GridContacts.TextMatrix(0, CNSTGRIDCOLLASTNAME, "Last");
			GridContacts.TextMatrix(0, CNSTGRIDCOLQUICKNAME, "Quickname");
		}

		private void ResizeGridContacts()
		{
			int GridWidth = 0;
			GridWidth = GridContacts.WidthOriginal;
			GridContacts.ColWidth(CNSTGRIDCOLFIRSTNAME, FCConvert.ToInt32(0.2 * GridWidth));
			GridContacts.ColWidth(CNSTGRIDCOLLASTNAME, FCConvert.ToInt32(0.2 * GridWidth));
			GridContacts.ColWidth(CNSTGRIDCOLQUICKNAME, FCConvert.ToInt32(0.2 * GridWidth));
		}

		private void frmEmailGroups_Resize(object sender, System.EventArgs e)
		{
			ResizeGridContacts();
		}

		private void GridContacts_KeyDown(object sender, KeyEventArgs e)
		{
			int KeyCode = e.KeyValue;
			if (e.KeyCode == Keys.Delete)
			{
				KeyCode = 0;
				DeleteContact();
			}
			else if (e.KeyCode == Keys.Insert)
			{
				KeyCode = 0;
				AddContact();
			}
		}

		private void mnuAddContact_Click(object sender, System.EventArgs e)
		{
			AddContact();
		}

		private void mnuAddGroup_Click(object sender, System.EventArgs e)
		{
			AddGroup();
		}

		private void mnuContacts_Click(object sender, System.EventArgs e)
		{
			frmEmailContacts.InstancePtr.Init(true, true);
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			DeleteGroup();
		}

		private void mnuDeleteContact_Click(object sender, System.EventArgs e)
		{
			DeleteContact();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			//mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrintAllGroups_Click(object sender, System.EventArgs e)
		{
			rptEmailContacts.InstancePtr.Init(0, boolAmModal);
		}

		private void mnuPrintContacts_Click(object sender, System.EventArgs e)
		{
			if (lngGroupID > 0)
			{
				rptEmailContacts.InstancePtr.Init(lngGroupID, boolAmModal);
			}
			else
			{
				FCMessageBox.Show("You must save this group before you can print it", MsgBoxStyle.Exclamation, "Group Not Saved");
				return;
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveGroup();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveGroup())
			{
				mnuExit_Click();
			}
		}

		private void mnuSearch_Click(object sender, System.EventArgs e)
		{
			FindGroup();
		}

		private void Toolbar1_ButtonClick(object sender, fecherFoundation.FCToolBarButtonClickEventArgs e)
		{
			ToolBarButton Button = e.Button;
			if (Strings.UCase(Button.Name) == "BTNSAVEGROUP")
			{
				SaveGroup();
			}
			else if (Strings.UCase(Button.Name) == "BTNADDGROUP")
			{
				AddGroup();
			}
			else if (Strings.UCase(Button.Name) == "BTNDELETEGROUP")
			{
				DeleteGroup();
			}
			else if (Strings.UCase(Button.Name) == "BTNSEARCHGROUP")
			{
				FindGroup();
			}
			else if (Strings.UCase(Button.Name) == "BTNADDTOGROUP")
			{
				AddContact();
			}
			else if (Strings.UCase(Button.Name) == "BTNDELETECONTACT")
			{
				DeleteContact();
			}
			else if (Strings.UCase(Button.Name) == "BTNEDITCONTACTS")
			{
				frmEmailContacts.InstancePtr.Init(true, true);
			}
		}

		private void txtDescription_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			boolDataChanged = true;
		}

		private void txtQuickName_TextChanged(object sender, System.EventArgs e)
		{
			boolDataChanged = true;
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuSave_Click(sender, e);
		}

		private void cmdContacts_Click(object sender, EventArgs e)
		{
			this.mnuContacts_Click(sender, e);
		}

		private void cmdAddContact_Click(object sender, EventArgs e)
		{
			this.mnuAddGroup_Click(sender, e);
		}

		private void cmdDeleteContact_Click(object sender, EventArgs e)
		{
			this.mnuDelete_Click(sender, e);
		}

		private void cmdSearch_Click(object sender, EventArgs e)
		{
			this.mnuSearch_Click(sender, e);
		}
	}
}
