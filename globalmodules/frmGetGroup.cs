﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using TWSharedLibrary;
#if TWPP0000
using TWPP0000;
using modGlobalVariables = TWPP0000.modSecurity;


#elif TWBL0000
using TWBL0000;


#elif TWRE0000
using TWRE0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmGetGroup.
	/// </summary>
	public partial class frmGetGroup : BaseForm
	{
		public frmGetGroup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.optModule = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.optModule.AddControlArrayElement(optModule_2, 2);
			this.optModule.AddControlArrayElement(optModule_1, 1);
			this.optModule.AddControlArrayElement(optModule_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetGroup InstancePtr
		{
			get
			{
				return (frmGetGroup)Sys.GetInstance(typeof(frmGetGroup));
			}
		}

		protected frmGetGroup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		private string strREDB = "";
		private string strPPDB = "";
		private string strGNDB = "";
		private string strCLDB = "";
		private string strUTDB = "";
		bool boolFilling;

		private void cmbGroupNumber_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper clsTest = new clsDRWrapper();
			int lngGrpNum;
			try
			{
				// On Error GoTo ErrorHandler
				if (boolFilling)
					return;
				// check for valid data
				if (cmbGroupNumber.ItemData(cmbGroupNumber.SelectedIndex) <= 0)
				{
					// new group
					mnuNew_Click();
					return;
				}
				lngGrpNum = FCConvert.ToInt32(FCConvert.ToDouble(Strings.Mid(Strings.Trim(cmbGroupNumber.Text), 1, Strings.InStr(1, Strings.Trim(cmbGroupNumber.Text), " ", CompareConstants.vbTextCompare) - 1)));
				clsTest.OpenRecordset("select * from groupmaster where groupnumber = " + FCConvert.ToString(lngGrpNum), "CentralData");
				if (clsTest.EndOfFile())
				{
					MessageBox.Show("This group number does not exist.", "Invalid Group", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				// seems legit so show it
				//! Load frmGroup;
				frmGroup.InstancePtr.Init(clsTest.Get_Fields_Int32("id"));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Group Number Click", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			txtSearch.Text = "";
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			string strSQL = "";
			string strFrom = "";
			string strWhere = "";
			string strContain = "";
			string strWhere2 = "";
			string strRESQL = "";
			string strPPSQL = "";
			string strUTSQL = "";
			// dynamically build the sql statement based on the radio button values
			try
			{
				// On Error GoTo ErrorHandler
				if (Strings.Trim(txtSearch.Text) == string.Empty)
				{
					MessageBox.Show("You must enter something to search by.", "Enter Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				// build the select part based on the correct table
				// If optModule(0).Value Then
				// real estate
				// strFrom = "select * from  master "
				// ElseIf optModule(1).Value Then
				// personal property
				// strFrom = "Select * from ppmaster "
				// Else
				// utility billing
				// strFrom = "Select * from master "
				// End If
				// 
				// set up the where clause based on the search by frame
				// If optSearchBy(0).Value Then
				// name
				// If optModule(0).Value Then
				// real estate
				// strWhere = "where rsname "
				// Else
				// personal property or utility billing
				// strWhere = "where name "
				// End If
				// ElseIf optSearchBy(1).Value Then
				// account number
				// If optModule(0).Value Then
				// real estate
				// strWhere = "where rsaccount "
				// ElseIf optModule(1).Value Then
				// personal property
				// strWhere = "where account "
				// Else
				// utility billing
				// strWhere = "where accountnumber "
				// End If
				// ElseIf optSearchBy(2).Value Then
				// location
				// If optModule(0).Value Then
				// real estate
				// strWhere = "where rslocstreet "
				// ElseIf optModule(1).Value Then
				// personal property
				// strWhere = "where street "
				// Else
				// utility billing
				// strWhere = "where streetname "
				// End If
				// ElseIf optSearchBy(3).Value Then
				// maplot
				// If optModule(0).Value Then
				// real estate
				// strWhere = "where rsmaplot "
				// Else
				// utility billing
				// strWhere = "where maplot "
				// End If
				// End If
				// 
				// strWhere2 = ""
				// 
				// If optModule(0).Value Then
				// must do extra for real estate
				// strWhere2 = " and rscard = 1 and rsdeleted <> 1 "
				// 
				// ElseIf optModule(1).Value Then
				// personal property
				// strWhere2 = " and val(deleted & '') = 0 "
				// End If
				// 
				// finish the where clause
				// If optSearchBy(1).Value Then
				// if account then do something a little different
				// 
				// strWhere = strWhere & "= " & Val(txtSearch.Text)
				// 
				// Else
				// all others can use the like statement
				// If optContain(0).Value Then
				// contains
				// strContain = "like '*" & Trim(txtSearch.Text) & "*' "
				// ElseIf optContain(1).Value Then
				// start with
				// strContain = "like '" & Trim(txtSearch.Text) & "*' "
				// Else
				// end with
				// strContain = "like '*" & Trim(txtSearch.Text) & "'"
				// End If
				// End If
				// 
				// strSQL = strFrom & strWhere & strContain & strWhere2
				// Call FillSearchGrid(strSQL)
				MakeSQL();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Search", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmGetGroup_Activated(object sender, System.EventArgs e)
		{
			FillComboBox();
		}

		private void frmGetGroup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmGetGroup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				if (Strings.UCase(this.ActiveControl.GetName()) == "TXTSEARCH")
				{
					cmdSearch_Click();
				}
				else if (Strings.UCase(this.ActiveControl.GetName()) == "TXTACCOUNT")
				{
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmGetGroup_Load(object sender, System.EventArgs e)
		{
            modGlobalFunctions.SetFixedSize(this);
			modGlobalFunctions.SetTRIOColors(this);
			
            if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTEDITGROUPS, false))
			{
				mnuNew.Enabled = false;
			}
			if (!StaticSettings.gGlobalActiveModuleSettings.RealEstateIsActive && !StaticSettings.gGlobalActiveModuleSettings.TaxBillingIsActive)
            {
				optModule[0].Enabled = false;
				optModule[1].CheckState = Wisej.Web.CheckState.Checked;
			}
			if (!StaticSettings.gGlobalActiveModuleSettings.PersonalPropertyIsActive && !StaticSettings.gGlobalActiveModuleSettings.TaxBillingIsActive)
			{
				if (optModule[1].CheckState == Wisej.Web.CheckState.Checked)
					optModule[2].CheckState = Wisej.Web.CheckState.Checked;
				optModule[1].Enabled = false;
			}
			if (!StaticSettings.gGlobalActiveModuleSettings.UtilityBillingIsActive)
			{
				if (optModule[2].CheckState == Wisej.Web.CheckState.Checked)
					optModule[2].CheckState = Wisej.Web.CheckState.Unchecked;
				optModule[2].Enabled = false;
			}
			FillComboBox();
		}

		private void FillComboBox()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsLoadName = new clsDRWrapper();
			string strName = "";
			boolFilling = true;
			cmbGroupNumber.Clear();
			cmbGroupNumber.AddItem("       Add New Group");
			clsLoad.OpenRecordset("select * from GROUPMASTER inner join moduleassociation on (groupmaster.id = moduleassociation.groupnumber) where moduleassociation.groupprimary = 1 order by groupmaster.groupnumber", "CentralData");
			if (!clsLoad.EndOfFile())
			{
				while (!clsLoad.EndOfFile())
				{
					strName = "";
					if (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("module"))) == "RE")
					{
						if (!modGlobalConstants.Statics.gboolRE && !modGlobalConstants.Statics.gboolBL)
						{
							strName = "Group Primary account in missing RE database";
						}
						else
						{
							// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
							clsLoadName.OpenRecordset("select FullNameLF from master CROSS APPLY " + clsLoadName.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(OwnerPartyID) as p where rsaccount = " + clsLoad.Get_Fields("account") + " and rscard = 1", "RealEstate");
							if (!clsLoadName.EndOfFile())
							{
								strName = Strings.Trim(FCConvert.ToString(clsLoadName.Get_Fields_String("FullNameLF")));
							}
							else
							{
								strName = "Group Primary account missing";
							}
						}
					}
					else if (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("module"))) == "PP")
					{
						if (!modGlobalConstants.Statics.gboolPP && !modGlobalConstants.Statics.gboolBL)
						{
							strName = "Group Primary account in missing PP database";
						}
						else
						{
							// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
							clsLoadName.OpenRecordset("select fullnamelf from ppmaster CROSS APPLY " + clsLoadName.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(PartyID) as p where account = " + clsLoad.Get_Fields("account"), "PersonalProperty");
							if (!clsLoadName.EndOfFile())
							{
								strName = Strings.Trim(FCConvert.ToString(clsLoadName.Get_Fields_String("FullNameLF")));
							}
							else
							{
								strName = "Group Primary account missing";
							}
						}
					}
					else if (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("module"))) == "UT")
					{
						if (!modGlobalConstants.Statics.gboolUT)
						{
							strName = "Group Primary account in missing UT database";
						}
						else
						{
							// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
							clsLoadName.OpenRecordset("select fullnamelf from master CROSS APPLY " + clsLoadName.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(BillingPartyID) as p where accountnumber = " + clsLoad.Get_Fields("account"), "UtilityBilling");
							if (!clsLoadName.EndOfFile())
							{
								strName = Strings.Trim(FCConvert.ToString(clsLoadName.Get_Fields_String("FullNameLF")));
							}
							else
							{
								strName = "Group Primary account missing";
							}
						}
					}
					// If strName <> vbNullString Then
					cmbGroupNumber.AddItem(modGlobalFunctions.PadStringWithSpaces(clsLoad.Get_Fields_Int32("groupnumber").ToString(), 4) + "  " + strName);
					cmbGroupNumber.ItemData(cmbGroupNumber.NewIndex, FCConvert.ToInt32(Conversion.Val(clsLoad.Get_Fields_Int32("groupnumber"))));
					// End If
					clsLoad.MoveNext();
				}
			}
			cmbGroupNumber.SelectedIndex = 0;
			boolFilling = false;
		}

		private void mnuClearSearch_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTEDITGROUPS, false))
			{
				MessageBox.Show("Your permission setting for creating groups is missing or set to none.", "No Permission", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			//! Load frmGroup;
			frmGroup.InstancePtr.Init(0);
		}

		public void mnuNew_Click()
		{
			mnuNew_Click(mnuNew, new System.EventArgs());
		}

		private void mnuSearch_Click(object sender, System.EventArgs e)
		{
			cmdSearch_Click();
		}

		private void optModule_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (Index == 1)
			{
				if (optModule[Index].CheckState == Wisej.Web.CheckState.Checked)
				{
					if (cmbSearchBy.SelectedIndex == 3)
					{
						// maplot is not kosher for personal property so don't allow it
						cmbSearchBy.SelectedIndex = 0;
					}
					if (cmbSearchBy.Items.Contains("Map / Lot"))
					{
						cmbSearchBy.Items.Remove("Map / Lot");
					}
				}
				else
				{
					if (!cmbSearchBy.Items.Contains("Map / Lot"))
					{
						cmbSearchBy.Items.Insert(3, "Map / Lot");
					}
				}
			}
		}

		private void optModule_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = optModule.GetIndex((FCCheckBox)sender);
			optModule_CheckedChanged(index, sender, e);
		}
		// vbPorter upgrade warning: intWhichModule As object	OnWriteFCConvert.ToInt16(
		private bool FillSearchGrid_6(string strSQL, object intWhichModule)
		{
			return FillSearchGrid(ref strSQL, ref intWhichModule);
		}

		private bool FillSearchGrid(ref string strSQL, ref object intWhichModule)
		{
			bool FillSearchGrid = false;
			// fill the search grid in the groupsearch form based on the sql statement passed in
			clsDRWrapper clsAccountInfo = new clsDRWrapper();
			clsDRWrapper clsGroupInfo = new clsDRWrapper();
			string strDBase = "";
			// name of database to open
			string strModule = "";
			// two letter abbrev. of module
			string strAccountField = "";
			// the field name that hold the account
			bool boolMatches = false;
			// whether there are any matches or not
			string strNameField = "";
			// the field name that holds the name
			string strLocMapField = "";
			// the field name that has the maplot or location information
			string strStreetNumber = "";
			// the field name that has the street number
			string strLocMapInfo = "";
			// holds the actual maplot or location data
			try
			{
				// On Error GoTo ErrorHandler
				FillSearchGrid = false;
				if (FCConvert.ToInt32(intWhichModule) == 1)
				{
					// real estate
					if (!modGlobalConstants.Statics.gboolRE && !modGlobalConstants.Statics.gboolBL)
					{
						FillSearchGrid = false;
						MessageBox.Show("Real Estate database not found.", "No database", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return FillSearchGrid;
					}
					strDBase = strREDB;
					strModule = "RE";
					strAccountField = "rsaccount";
					strNameField = "FullNameLF";
					strStreetNumber = "rslocnumalph";
					if (cmbSearchBy.SelectedIndex == 2 || optModule[1].CheckState == Wisej.Web.CheckState.Checked)
					{
						strLocMapField = "rslocstreet";
					}
					else
					{
						strLocMapField = "rsmaplot";
					}
				}
				else if (FCConvert.ToInt32(intWhichModule) == 2)
				{
					// personal property
					if (!modGlobalConstants.Statics.gboolPP && !modGlobalConstants.Statics.gboolBL)
					{
						FillSearchGrid = false;
						MessageBox.Show("Personal Property database not found.", "No database", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return FillSearchGrid;
					}
					strDBase = strPPDB;
					strModule = "PP";
					strAccountField = "account";
					strNameField = "FullNameLF";
					strLocMapField = "street";
					strStreetNumber = "streetnumber";
				}
				else if (FCConvert.ToInt32(intWhichModule) == 3)
				{
					// utility billing
					if (!modGlobalConstants.Statics.gboolUT)
					{
						FillSearchGrid = false;
						MessageBox.Show("Utility Billing database not found.", "No database", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return FillSearchGrid;
					}
					strDBase = strUTDB;
					strModule = "UT";
					strAccountField = "accountnumber";
					strNameField = "FullNameLF";
					if (cmbSearchBy.SelectedIndex == 2 || optModule[1].CheckState == Wisej.Web.CheckState.Checked)
					{
						strLocMapField = "streetname";
					}
					else
					{
						strLocMapField = "maplot";
					}
					strStreetNumber = "streetnumber";
				}
				//! Load frmWait;
				frmWait.InstancePtr.Init("Please Wait. Searching.");
				// frmWait.lblMessage.Caption = "Please Wait. Searching."
				//Application.DoEvents();
				clsAccountInfo.OpenRecordset(strSQL, strDBase);
				if (clsAccountInfo.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					FillSearchGrid = false;
					return FillSearchGrid;
				}
				else
				{
					//! Load frmGroupSearch;
					if (cmbSearchBy.SelectedIndex == 2 || optModule[1].CheckState == Wisej.Web.CheckState.Checked)
					{
						frmGroupSearch.InstancePtr.SetupSearchGrid("Location");
					}
					else
					{
						frmGroupSearch.InstancePtr.SetupSearchGrid("Map / Lot");
					}
					boolMatches = false;
					// now check each record to see if its in a group
					while (!clsAccountInfo.EndOfFile())
					{
						//Application.DoEvents();
						// if it's in a group add it to the grid and get the group data
						clsGroupInfo.OpenRecordset("select * from groupmaster inner join moduleassociation on (groupmaster.id = moduleassociation.groupnumber) where moduleassociation.account = " + clsAccountInfo.Get_Fields(strAccountField) + " and moduleassociation.module = '" + strModule + "'", "CentralData");
						if (!clsGroupInfo.EndOfFile())
						{
							boolMatches = true;
							// not put it in the grid
							if (cmbSearchBy.SelectedIndex == 2 || optModule[1].CheckState == Wisej.Web.CheckState.Checked)
							{
								strLocMapInfo = Strings.Trim(clsAccountInfo.Get_Fields(strStreetNumber) + " " + clsAccountInfo.Get_Fields(strLocMapField));
							}
							else
							{
								strLocMapInfo = FCConvert.ToString(clsAccountInfo.Get_Fields(strLocMapField));
							}
							frmGroupSearch.InstancePtr.SearchGrid.AddItem(strModule + " " + clsAccountInfo.Get_Fields(strAccountField) + "\t" + clsAccountInfo.Get_Fields(strNameField) + "\t" + strLocMapInfo + "\t" + clsGroupInfo.Get_Fields_Int32("groupnumber") + "\t" + clsGroupInfo.Get_Fields_Int32("id"));
						}
						clsAccountInfo.MoveNext();
					}
					if (!boolMatches)
					{
						frmWait.InstancePtr.Unload();
						FillSearchGrid = false;
						return FillSearchGrid;
					}
				}
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				FillSearchGrid = true;
				// frmGroupSearch.Show , MDIParent
				// Unload Me
				return FillSearchGrid;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In fill search grid", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillSearchGrid;
		}

		private void MakeSQL()
		{
			string strSQL = "";
			string strFrom = "";
			string strWhere = "";
			string strContain = "";
			string strWhere2;
			string strRESQL = "";
			string strPPSQL = "";
			string strUTSQL = "";
			bool boolMatchFound;
			// dynamically build the sql statement based on the radio button values
			// On Error GoTo ErrorHandler
			// build the select part based on the correct table
			if (optModule[0].CheckState == Wisej.Web.CheckState.Checked)
			{
				// real estate
				if (!modGlobalConstants.Statics.gboolRE && !modGlobalConstants.Statics.gboolBL)
				{
					MessageBox.Show("Real Estate database not found.  Cannot continue search.", "Missing Database", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				strRESQL = "SELECT * FROM Master as c CROSS APPLY " + modReplaceWorkFiles.Statics.rsVariables.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.OwnerPartyID) as p ";
			}
			if (optModule[1].CheckState == Wisej.Web.CheckState.Checked)
			{
				// personal property
				if (!modGlobalConstants.Statics.gboolPP && !modGlobalConstants.Statics.gboolBL)
				{
					MessageBox.Show("Personal Property database not found.  Cannot continue search.", "Missing Database", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				strPPSQL = "SELECT * FROM PPMaster as c CROSS APPLY " + modReplaceWorkFiles.Statics.rsVariables.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.PartyID) as p ";
			}
			if (optModule[2].CheckState == Wisej.Web.CheckState.Checked)
			{
				// utility billing
				if (!modGlobalConstants.Statics.gboolUT)
				{
					MessageBox.Show("Utility Billing database not found.  Cannot continue search.", "Missing Database", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				strUTSQL = "SELECT * FROM Master as c CROSS APPLY " + modReplaceWorkFiles.Statics.rsVariables.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.OwnerPartyID) as p ";
			}
			// set up the where clause based on the search by frame
			if (cmbSearchBy.SelectedIndex == 0)
			{
				// name
				if (optModule[0].CheckState == Wisej.Web.CheckState.Checked)
				{
					// real estate
					strRESQL += "where FullNameLF ";
				}
				if (optModule[1].CheckState == Wisej.Web.CheckState.Checked)
				{
					// personal property
					strPPSQL += "where FullNameLF ";
				}
				if (optModule[2].CheckState == Wisej.Web.CheckState.Checked)
				{
					// UT
					strUTSQL += "where FullNameLF ";
				}
			}
			else if (cmbSearchBy.SelectedIndex == 1)
			{
				// account number
				if (optModule[0].CheckState == Wisej.Web.CheckState.Checked)
				{
					// real estate
					strRESQL += "where rsaccount ";
				}
				if (optModule[1].CheckState == Wisej.Web.CheckState.Checked)
				{
					// personal property
					strPPSQL += "where account ";
				}
				if (optModule[2].CheckState == Wisej.Web.CheckState.Checked)
				{
					// utility billing
					strUTSQL += "where accountnumber ";
				}
			}
			else if (cmbSearchBy.SelectedIndex == 2)
			{
				// location
				if (optModule[0].CheckState == Wisej.Web.CheckState.Checked)
				{
					// real estate
					strRESQL += "where rslocstreet ";
				}
				if (optModule[1].CheckState == Wisej.Web.CheckState.Checked)
				{
					// personal property
					strPPSQL += "where street ";
				}
				if (optModule[2].CheckState == Wisej.Web.CheckState.Checked)
				{
					// utility billing
					strUTSQL += "where streetname ";
				}
			}
			else if (cmbSearchBy.SelectedIndex == 3)
			{
				// maplot
				if (optModule[0].CheckState == Wisej.Web.CheckState.Checked)
				{
					// real estate
					strRESQL += "where rsmaplot ";
				}
				if (optModule[1].CheckState == Wisej.Web.CheckState.Checked)
				{
					// utility billing
					strUTSQL += "where maplot ";
				}
			}
			// finish the where clause
			if (cmbSearchBy.SelectedIndex == 1)
			{
				// if account then do something a little different
				if (optModule[0].CheckState == Wisej.Web.CheckState.Checked)
				{
					strRESQL += "= " + FCConvert.ToString(Conversion.Val(txtSearch.Text));
				}
				if (optModule[1].CheckState == Wisej.Web.CheckState.Checked)
				{
					strPPSQL += "= " + FCConvert.ToString(Conversion.Val(txtSearch.Text));
				}
				if (optModule[2].CheckState == Wisej.Web.CheckState.Checked)
				{
					strUTSQL += "= " + FCConvert.ToString(Conversion.Val(txtSearch.Text));
				}
				strWhere += "= " + FCConvert.ToString(Conversion.Val(txtSearch.Text));
			}
			else
			{
				// all others can use the like statement
				if (cmbContain.SelectedIndex == 0)
				{
					// contains
					strContain = "like '*" + Strings.Trim(txtSearch.Text) + "*' ";
				}
				else if (cmbContain.SelectedIndex == 1)
				{
					// start with
					strContain = "like '" + Strings.Trim(txtSearch.Text) + "*' ";
				}
				else
				{
					// end with
					strContain = "like '*" + Strings.Trim(txtSearch.Text) + "'";
				}
				strRESQL += strContain;
				strPPSQL += strContain;
				strUTSQL += strContain;
			}
			strWhere2 = "";
			if (optModule[0].CheckState == Wisej.Web.CheckState.Checked)
			{
				// must do extra for real estate
				strRESQL += " and rscard = 1 and rsdeleted <> 1 ";
			}
			else if (optModule[1].CheckState == Wisej.Web.CheckState.Checked)
			{
				// personal property
				strPPSQL += " and isnull(deleted, 0)) = 0 ";
			}
			if (!(optModule[0].CheckState == Wisej.Web.CheckState.Checked))
				strRESQL = "";
			if (!(optModule[1].CheckState == Wisej.Web.CheckState.Checked))
				strPPSQL = "";
			if (!(optModule[2].CheckState == Wisej.Web.CheckState.Checked))
				strUTSQL = "";
			boolMatchFound = false;
			if (optModule[0].CheckState == Wisej.Web.CheckState.Checked)
			{
				boolMatchFound = boolMatchFound | FillSearchGrid_6(strRESQL, 1);
			}
			if (optModule[1].CheckState == Wisej.Web.CheckState.Checked)
			{
				boolMatchFound = boolMatchFound | FillSearchGrid_6(strPPSQL, 2);
			}
			if (optModule[2].CheckState == Wisej.Web.CheckState.Checked)
			{
				boolMatchFound = boolMatchFound | FillSearchGrid_6(strUTSQL, 3);
			}
			if (boolMatchFound)
			{
				frmGroupSearch.InstancePtr.Show(App.MainForm);
			}
			else
			{
				MessageBox.Show("No matches found.", "No Matches", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void cmdNewGroup_Click(object sender, EventArgs e)
		{
		}
	}
}
