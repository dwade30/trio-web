﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cUTSecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cUTSecuritySetup() : base()
		{
			strThisModule = "UT";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem("Enter Utility Billing", "", 1, false, ""));
			AddItem_2(CreateItem("Billing Process", "", 2, false, ""));
			AddItem_2(CreateItem("Collection Process", "", 3, true, ""));
			AddItem_2(CreateItem("Collection Process", "Charge Interest", 14, false, ""));
			AddItem_2(CreateItem("Collection Process", "Daily Audit Report", 10, false, ""));
			AddItem_2(CreateItem("Collection Process", "Lien Process", 13, false, ""));
			AddItem_2(CreateItem("Collection Process", "Load of Back Information", 11, false, ""));
			AddItem_2(CreateItem("Collection Process", "Mortgage Holder", 12, false, ""));
			AddItem_2(CreateItem("Collection Process", "Payments & Adjustments", 9, false, ""));
			AddItem_2(CreateItem("Collection Process", "View Status", 8, false, ""));
			AddItem_2(CreateItem("Extract for Remote Reader", "", 5, false, ""));
			AddItem_2(CreateItem("File Maintenance", "", 6, true, ""));
			AddItem_2(CreateItem("File Maintenance", "Clear Certified Mail Table", 25, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Convert Consumption File", 21, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Copy Extract to Disk", 23, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Create Database Extract", 22, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Customize", 15, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Edit Bill Information", 30, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Edit Custom Bill Types", 20, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Load Previous Readings", 16, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Meter Change Out", 19, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Purge Paid Bills", 17, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Rate Key Update", 24, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Remove From Tax Acquired", 28, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Reprint Purge Report", 18, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Reset Bills", 26, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Reset Final Bills", 27, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Split Bills", 29, false, ""));
			AddItem_2(CreateItem("Printing", "", 7, true, ""));
			AddItem_2(CreateItem("Table File Setup", "", 4, true, ""));
		}
	}
}
