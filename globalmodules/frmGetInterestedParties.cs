﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using TWCL0000;

namespace Global
{
	/// <summary>
	/// Summary description for frmGetInterestedParties.
	/// </summary>
	public partial class frmGetInterestedParties : BaseForm
	{
		public frmGetInterestedParties()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGetInterestedParties InstancePtr
		{
			get
			{
				return (frmGetInterestedParties)Sys.GetInstance(typeof(frmGetInterestedParties));
			}
		}

		protected frmGetInterestedParties _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Melissa Lamprecht       *
		// LAST UPDATED   :               12/28/2007              *
		// ********************************************************
		public string auxCmbAccountItem1 = "Real Estate Account";
		public string auxCmbAccountItem2 = "Utility Account";
		bool boolLoaded;

		private void frmGetInterestedParties_Activated(object sender, System.EventArgs e)
		{
			string strTemp = "";
			if (!boolLoaded)
			{
				if (txtAccount.Enabled && txtAccount.Visible)
				{
					txtAccount.Focus();
				}
				boolLoaded = true;
			}
		}

		private void frmGetInterestedParties_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = 0;
			}
			else if (KeyCode == Keys.Escape)
			{
				KeyCode = 0;
				mnuExit_Click();
			}
		}

		private void frmGetInterestedParties_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			clsDRWrapper clsTemp = new clsDRWrapper();
			if (KeyAscii == 13)
			{
				// return key
				KeyAscii = 0;
				if (Strings.Trim(txtSearch.Text).Length > 0)
				{
					StartSearch(true);
				}
				else
				{
					StartSearch();
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void frmGetInterestedParties_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetInterestedParties.Icon	= "frmGetInterestedParties.frx":0000";
			//frmGetInterestedParties.ScaleWidth	= 9045;
			//frmGetInterestedParties.ScaleHeight	= 6930;
			//frmGetInterestedParties.LinkTopic	= "Form1";
			//frmGetInterestedParties.LockControls	= -1  'True;
			//Font.Size	= "8.25";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//End Unmaped Properties
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
				{
					bool itemExists = false;
					//search for item
					foreach (var item in cmbAccount.Items)
					{
						if (item.ToString() == auxCmbAccountItem1)
						{
							itemExists = true;
							if (cmbAccount.Items[1].ToString() != auxCmbAccountItem1)
							{
								for (int i = 0; i < cmbAccount.Items.Count; i++)
								{
									//if not in corect index
									if (cmbAccount.Items[i].ToString() == auxCmbAccountItem1)
									{
										//set at right index
										cmbAccount.Items.RemoveAt(i);
										itemExists = false;
										break;
									}
								}
							}
							break;
						}
					}
					if (!itemExists)
					{
						cmbAccount.Items.Insert(1, auxCmbAccountItem1);
					}
				}
				else
				{
					cmbAccount.Items.RemoveAt(1);
				}
				if (modGlobalConstants.Statics.gboolUT)
				{
					bool itemExists = false;
					//search for item
					foreach (var item in cmbAccount.Items)
					{
						if (item.ToString() == auxCmbAccountItem2)
						{
							itemExists = true;
							if (cmbAccount.Items[2].ToString() != auxCmbAccountItem2)
							{
								for (int i = 0; i < cmbAccount.Items.Count; i++)
								{
									//if not in corect index
									if (cmbAccount.Items[i].ToString() == auxCmbAccountItem2)
									{
										//set at right index
										cmbAccount.Items.RemoveAt(i);
										itemExists = false;
										break;
									}
								}
							}
							break;
						}
					}
					if (!itemExists)
					{
						cmbAccount.Items.Insert(2, auxCmbAccountItem2);
					}
				}
				else
				{
					cmbAccount.Items.RemoveAt(2);
				}
				//#if ModuleID==1 || ModuleID==3	// DeadCode
				#if ModuleID
												                mnuFileReportsMHwithUT.Visible = false;
				mnuFileReportsMHwithUTAmounts.Visible = false;
				mnuFileReportsUTwithMH.Visible = false;

#endif
				cmbAccount.SelectedIndex = 0;
				lblAccountNumber.Text = "Enter the Interested Party and press 'Enter' to continue.";
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				txtAccount.Text = "";
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Form");
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			App.MainForm.Show();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuFileLabels_Click(object sender, System.EventArgs e)
		{
			frmCustomLabels.InstancePtr.Unload();
			App.DoEvents();
			frmCustomLabels.InstancePtr.Show(App.MainForm);
			Close();
		}

		private void mnuFileIntPartyListing_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptInterestedPartyMasterList.InstancePtr, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "", false, false);
		}

		private void mnuFileReportsREwithIP_Click(object sender, System.EventArgs e)
		{
			// this will show the report that has RE Accounts with the associated IP
			rptREIntParty.InstancePtr.Init(true);
		}

		private void mnuFileReportsUTwithIP_Click(object sender, System.EventArgs e)
		{
			//#if !(ModuleID==1) && !(ModuleID==3)	// ActiveBranch
			#if !ModuleID
			rptUTIntParty.InstancePtr.Init(true);
			#endif
		}

		private void mnuGetAccount_Click(object sender, System.EventArgs e)
		{
			if (Strings.Trim(txtAccount.Text).Length > 0 && Strings.Trim(txtSearch.Text) == "")
			{
				StartSearch();
			}
			else
			{
				StartSearch(true);
			}
		}

		private void optAccount_Click(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						lblAccountNumber.Text = "Enter the Interested Party and press 'Enter' to continue.";
						// fraCriteria.Caption = "Interested Party"
						// optSearchBy(0).Enabled = True
						// optSearchBy(1).Enabled = False
						// optSearchBy(2).Enabled = False
						// txtAccount.Text = lngLastMH
						break;
					}
				case 1:
					{
						lblAccountNumber.Text = "Enter the Account Number and press 'Enter' to continue.";
						// fraCriteria.Caption = "Real Estate"
						// optSearchBy(0).Enabled = True
						// optSearchBy(1).Enabled = True
						// optSearchBy(2).Enabled = True
						// txtAccount.Text = lngLastRE
						break;
					}
				case 2:
					{
						lblAccountNumber.Text = "Enter the Account Number and press 'Enter' to continue.";
						// fraCriteria.Caption = "Utility"
						// optSearchBy(0).Enabled = True
						// optSearchBy(1).Enabled = True
						// optSearchBy(2).Enabled = True
						// txtAccount.Text = lngLastUT
						break;
					}
			}
			//end switch
			if (txtAccount.Enabled && txtAccount.Visible)
			{
				txtAccount.Focus();
			}
		}

		private void optAccount_Click(object sender, System.EventArgs e)
		{
			int index = cmbAccount.SelectedIndex;
			optAccount_Click(index, sender, e);
		}

		private void StartSearch(bool boolSearch = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper clsTemp = new clsDRWrapper();
				string strSQL = "";
				string strWhere = "";
				string strBeginLike = "";
				string strEndLike = "";
				string strOrderBy = "";
				string strInnerJoin = "";
				bool boolJoined/*unused?*/;
				bool boolMatches/*unused?*/;
				int lngTemp = 0;
				string strSearchText;
				strSearchText = modGlobalFunctions.EscapeQuotes(Strings.Trim(txtSearch.Text));
				// check to see if the account box has a number first, if it does not then
				if (Strings.Trim(txtAccount.Text) == "" && Strings.Trim(strSearchText) == "")
				{
					// nothing entered in either
					FCMessageBox.Show("Please enter a value in either the Account Field or the Search Field.", MsgBoxStyle.Information, "Data Entry");
					return;
				}
				else if (((Strings.Trim(txtAccount.Text) != "" && Strings.Trim(strSearchText) == "") || (Strings.Trim(txtAccount.Text) != "" && Strings.Trim(strSearchText) != "")) && boolSearch == false)
				{
					// just the account number or both full
					strSearchText = "";
					if (Conversion.Val(txtAccount.Text) <= 0)
					{
						FCMessageBox.Show("Invalid account number");
						return;
					}
					// check if its a valid account
					if (cmbAccount.SelectedIndex == 0)
					{
						// mortgage holder
						// MAL@2008118: Changed to look directly at the RE database
						// Tracker Reference: 16196
						clsTemp.OpenRecordset("SELECT * FROM Owners WHERE AutoID = " + FCConvert.ToString(Conversion.Val(txtAccount.Text)), modExtraModules.strREDatabase);
						if (!clsTemp.EndOfFile())
						{
							// Load frmInterestedParties
							// send ip number and set to 1 to indicate it is a interested party
							frmInterestedParties.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)), "CL", 0, true, 2);
							// frmInterestedParties.Show , MDIParent
						}
						else
						{
							FCMessageBox.Show("The Interested Party number doesn't exist.", MsgBoxStyle.Information, "No Interested Party Match");
							return;
						}
					}
					else if (cmbAccount.SelectedIndex == 1)
					{
						// real estate account number
						clsTemp.OpenRecordset("SELECT * FROM Master WHERE NOT (RSDeleted = 1) AND RSCard = 1 AND RSAccount = " + FCConvert.ToString(Conversion.Val(txtAccount.Text)), modExtraModules.strREDatabase);
						if (!clsTemp.EndOfFile())
						{
							// Load frmInterestedParties
							// Pass Real Estate Account ; Indicate 2 = RE Account
							frmInterestedParties.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)), "CL", 0, true, 2);
							// frmInterestedParties.Show , MDIParent
						}
						else
						{
							FCMessageBox.Show("The Real Estate Account either doesn't exist or is deleted.", MsgBoxStyle.Information, "No Real Estate Match");
							return;
						}
					}
					else
					{
						// Utility Account Number
						clsTemp.OpenRecordset("SELECT * FROM Master WHERE NOT (Deleted = 1) AND AccountNumber = " + FCConvert.ToString(Conversion.Val(txtAccount.Text)), modExtraModules.strUTDatabase);
						if (!clsTemp.EndOfFile())
						{
							// MAL@20071106: Correct the way that it selects mortgage holders
							// Tracker Reference: 11257
							if (FCConvert.ToInt32(clsTemp.Get_Fields_Int32("REAccount")) > 0 && modGlobalConstants.Statics.gboolRE && clsTemp.Get_Fields_Boolean("UseMortgageHolder") == true)
							{
								// If clsTemp.Get_Fields("REAccount") > 0 And gboolRE Then
								lngTemp = FCConvert.ToInt32(clsTemp.Get_Fields_Int32("REAccount"));
								clsTemp.OpenRecordset("SELECT * FROM Master WHERE NOT (RSDeleted = 1) AND RSCard = 1 AND RSAccount = " + FCConvert.ToString(lngTemp), modExtraModules.strREDatabase);
								if (!clsTemp.EndOfFile())
								{
									FCMessageBox.Show("This Utility account is linked to Real Estate account #" + FCConvert.ToString(clsTemp.Get_Fields_Int32("RSAccount")) + ".", MsgBoxStyle.Information, "Linked Account");
									// Load frmInterestedParties
									// Pass Real Estate Account ; Indicate 2 = RE Account
									frmInterestedParties.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(clsTemp.Get_Fields_Int32("RSAccount"))), "UT", 0, true, 2);
									// frmInterestedParties.Show , MDIParent
								}
								else
								{
									FCMessageBox.Show("This Utility account was linked to Real Estate account #" + FCConvert.ToString(lngTemp) + ", but the link is invalid and will be removed.", MsgBoxStyle.Information, "Linked Account");
									clsTemp.OpenRecordset("SELECT * FROM Master WHERE NOT (Deleted = 1) AND AccountNumber = " + FCConvert.ToString(Conversion.Val(txtAccount.Text)), modExtraModules.strUTDatabase);
									clsTemp.Edit();
									clsTemp.Set_Fields("REAccount", 0);
									clsTemp.Update();
									// load the UT account by itself
									// Load frmInterestedParties
									// Pass Real Estate Account ; Indicate 3 = UT Account
									frmInterestedParties.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)), "UT", 0, true, 3);
									// frmInterestedParties.Show , MDIParent
								}
							}
							else
							{
								// Load frmInterestedParties
								// Pass Real Estate Account ; Indicate 3 = UT Account
								frmInterestedParties.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)), "UT", 0, true, 3);
								// frmInterestedParties.Show , MDIParent
							}
						}
						else
						{
							FCMessageBox.Show("The Utility account either doesn't exist or is deleted.", MsgBoxStyle.Information, "No Utility Match");
							return;
						}
					}
				}
				else
				{
					// No Search to Start with This Form ; Maybe An Added Feature Later (would require table/form changes)
					// just the search string
					// dynamically build sql string
					// If Trim(strSearchText) = vbNullString Then
					// MsgBox "You must enter criteria to search for.", MsgBoxStyle.Information, "No Search Criteria"
					// Exit Sub
					// End If
					// 
					// If optAccount(0).Value Then
					// strSQL = "SELECT * FROM Owners "
					// Else
					// strSQL = "SELECT * FROM Master "
					// End If
					// 
					// If optContain(0).Value Then
					// contains
					// strBeginLike = "'*"
					// strEndLike = "*'"
					// ElseIf optContain(1).Value Then
					// starts with
					// strBeginLike = "'"
					// strEndLike = "*'"
					// Else
					// ends with
					// strBeginLike = "'*"
					// strEndLike = "'"
					// End If
					// 
					// build the where clause
					// If optSearchBy(0).Value Then
					// IP name
					// If optAccount(0).Value Then
					// strWhere = " Name LIKE " & strBeginLike & Trim(strSearchText) & strEndLike
					// End If
					// End If
					// 
					// If optSearchBy(0).Value And optAccount(0).Value Then
					// strSQL = strSQL & " WHERE " & strWhere
					// End If
					// 
					// The string is built now send it to the search form and show it
					// Load frmMortgageSearch
					// If optAccount(0).Value Then
					// frmMortgageSearch.Init strSQL, 1, boolJoined, boolMatches
					// ElseIf optAccount(1).Value Then
					// frmMortgageSearch.Init strSQL, 2, boolJoined, boolMatches
					// Else    'UT
					// frmMortgageSearch.Init strSQL, 3, boolJoined, boolMatches
					// End If
					// 
					// If boolMatches Then
					// do nothing
					// Else
					// DoEvents
					// If txtSearch.Enabled And txtSearch.Visible Then
					// txtSearch.SetFocus
					// Else
					// frmGetMortgage.Show
					// End If
					// End If
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Starting Search");
			}
		}

		private void txtAccount_Enter(object sender, System.EventArgs e)
		{
			if (txtAccount.Text.Length > 0)
			{
				txtAccount.SelectionStart = 0;
				txtAccount.SelectionLength = txtAccount.Text.Length;
			}
		}

		private void lblAccount_Click(object sender, EventArgs e)
		{
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuGetAccount_Click(sender, e);
		}

		private void cmdFileReports_Click(object sender, EventArgs e)
		{
		}
		// Private Sub txtSearch_KeyPress(KeyAscii As Integer)
		// Select Case KeyAscii
		// Case 91, 93
		// KeyAscii = 0
		// End Select
		// End Sub
	}
}
