﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cCKSecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cCKSecuritySetup() : base()
		{
			strThisModule = "CK";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem(" Enter Clerk", "", 1, false, ""));
			AddItem_2(CreateItem("Births", "", 3, false, ""));
			AddItem_2(CreateItem("Deaths", "", 4, false, ""));
			AddItem_2(CreateItem("Dogs", "", 5, true, ""));
			AddItem_2(CreateItem("Dogs", "Edit Dogs/Kennels", 12, false, ""));
			AddItem_2(CreateItem("Dogs", "Edit Owners Screen", 13, false, ""));
			AddItem_2(CreateItem("Dogs", "Edit Any Transactions", 14, false, ""));
			AddItem_2(CreateItem("Dogs", "Edit Own Transactions", 16, false, ""));
			AddItem_2(CreateItem("Dogs", "Edit Tag/Sticker Inventory", 11, false, ""));
			AddItem_2(CreateItem("Dogs", "Online Registrations", 15, false, ""));
			AddItem_2(CreateItem("Dogs", "Void Any Transactions", 9, false, ""));
			AddItem_2(CreateItem("Dogs", "Void Own Transactions", 10, false, ""));
			AddItem_2(CreateItem("File Maintenance", "", 7, false, ""));
			AddItem_2(CreateItem("Marriages", "", 2, false, ""));
			AddItem_2(CreateItem("Reports", "", 8, false, ""));
		}
	}
}
