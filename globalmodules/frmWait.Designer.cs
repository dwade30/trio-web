﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmWait.
	/// </summary>
	partial class frmWait : BaseForm
	{
		public Wisej.Web.ProgressBar prgProgress;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCLabel lblMessage;
		public fecherFoundation.FCLabel lblCounter;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmWait));
			this.prgProgress = new Wisej.Web.ProgressBar();
			this.Image1 = new fecherFoundation.FCPictureBox();
			this.lblMessage = new fecherFoundation.FCLabel();
			this.lblCounter = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 301);
			this.BottomPanel.Size = new System.Drawing.Size(465, 141);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.prgProgress);
			this.ClientArea.Controls.Add(this.Image1);
			this.ClientArea.Controls.Add(this.lblMessage);
			this.ClientArea.Controls.Add(this.lblCounter);
			this.ClientArea.Location = new System.Drawing.Point(0, 43);
			this.ClientArea.Size = new System.Drawing.Size(465, 258);
			this.ClientArea.TabIndex = 0;
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(465, 43);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// prgProgress
			// 
			this.prgProgress.Location = new System.Drawing.Point(30, 230);
			this.prgProgress.Name = "prgProgress";
			this.prgProgress.Size = new System.Drawing.Size(387, 19);
			this.prgProgress.TabIndex = 2;
			this.prgProgress.Visible = false;
			// 
			// Image1
			// 
			this.Image1.AllowDrop = true;
			this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
			this.Image1.DrawStyle = ((short)(0));
			this.Image1.DrawWidth = ((short)(1));
			this.Image1.FillColor = 16777215;
			this.Image1.FillStyle = ((short)(1));
			this.Image1.FontTransparent = true;
			this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
			this.Image1.Location = new System.Drawing.Point(201, 145);
			this.Image1.Name = "Image1";
			this.Image1.Picture = ((System.Drawing.Image)(resources.GetObject("Image1.Picture")));
			this.Image1.Size = new System.Drawing.Size(68, 68);
			this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.Image1.TabIndex = 1;
			// 
			// lblMessage
			// 
			this.lblMessage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.lblMessage.Location = new System.Drawing.Point(30, 30);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(384, 94);
			this.lblMessage.TabIndex = 0;
			// 
			// lblCounter
			// 
			this.lblCounter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.lblCounter.Location = new System.Drawing.Point(30, 154);
			this.lblCounter.Name = "lblCounter";
			this.lblCounter.Size = new System.Drawing.Size(146, 47);
			this.lblCounter.TabIndex = 1;
			this.lblCounter.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// frmWait
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(465, 442);
			this.ControlBox = false;
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmWait";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Load += new System.EventHandler(this.frmWait_Load);
			this.Activated += new System.EventHandler(this.frmWait_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
