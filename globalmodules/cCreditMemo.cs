﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace Global
{
	public class cCreditMemo
	{
		//=========================================================
		private int lngRecordID;
		private int lngJournalNumber;
		private string strCreditMemoDate = "";
		private int lngVendorNumber;
		private string strCreditMemoDescription = string.Empty;
		private string strMemoNumber = string.Empty;
		private double dblCMAmount;
		private double dblLiquidatedAmount;
		private double dblAdjustments;
		private string strPostedDate = "";
		private int intCMPeriod;
		private int lngOriginalAPRecord;
		private string strFund = string.Empty;
		private string strCMStatus = string.Empty;
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collMemoDetails = new cGenericCollection();
		private cGenericCollection collMemoDetails_AutoInitialized;

		private cGenericCollection collMemoDetails
		{
			get
			{
				if (collMemoDetails_AutoInitialized == null)
				{
					collMemoDetails_AutoInitialized = new cGenericCollection();
				}
				return collMemoDetails_AutoInitialized;
			}
			set
			{
				collMemoDetails_AutoInitialized = value;
			}
		}

		private bool boolUpdated;
		private bool boolDeleted;

		public cGenericCollection CreditMemoDetails
		{
			get
			{
				cGenericCollection CreditMemoDetails = null;
				CreditMemoDetails = collMemoDetails;
				return CreditMemoDetails;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int JournalNumber
		{
			set
			{
				lngJournalNumber = value;
				IsUpdated = true;
			}
			get
			{
				int JournalNumber = 0;
				JournalNumber = lngJournalNumber;
				return JournalNumber;
			}
		}

		public string CreditMemoDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strCreditMemoDate = value;
				}
				else
				{
					strCreditMemoDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string CreditMemoDate = "";
				CreditMemoDate = strCreditMemoDate;
				return CreditMemoDate;
			}
		}

		public int VendorNumber
		{
			set
			{
				lngVendorNumber = value;
				IsUpdated = true;
			}
			get
			{
				int VendorNumber = 0;
				VendorNumber = lngVendorNumber;
				return VendorNumber;
			}
		}

		public string Description
		{
			set
			{
				strCreditMemoDescription = value;
				IsUpdated = true;
			}
			get
			{
				string Description = "";
				Description = strCreditMemoDescription;
				return Description;
			}
		}

		public string MemoNumber
		{
			set
			{
				strMemoNumber = value;
				IsUpdated = true;
			}
			get
			{
				string MemoNumber = "";
				MemoNumber = strMemoNumber;
				return MemoNumber;
			}
		}

		public double Amount
		{
			set
			{
				dblCMAmount = value;
				IsUpdated = true;
			}
			get
			{
				double Amount = 0;
				Amount = dblCMAmount;
				return Amount;
			}
		}

		public double LiquidatedAmount
		{
			set
			{
				dblLiquidatedAmount = value;
				IsUpdated = true;
			}
			get
			{
				double LiquidatedAmount = 0;
				LiquidatedAmount = dblLiquidatedAmount;
				return LiquidatedAmount;
			}
		}

		public double Adjustments
		{
			set
			{
				dblAdjustments = value;
				IsUpdated = true;
			}
			get
			{
				double Adjustments = 0;
				Adjustments = dblAdjustments;
				return Adjustments;
			}
		}

		public string PostedDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strPostedDate = value;
				}
				else
				{
					strPostedDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string PostedDate = "";
				PostedDate = strPostedDate;
				return PostedDate;
			}
		}

		public short Period
		{
			set
			{
				intCMPeriod = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short Period = 0;
				Period = FCConvert.ToInt16(intCMPeriod);
				return Period;
			}
		}

		public int OriginalAPRecord
		{
			set
			{
				lngOriginalAPRecord = value;
				IsUpdated = true;
			}
			get
			{
				int OriginalAPRecord = 0;
				OriginalAPRecord = lngOriginalAPRecord;
				return OriginalAPRecord;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
				IsUpdated = true;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}

		public string Status
		{
			set
			{
				strCMStatus = value;
				IsUpdated = true;
			}
			get
			{
				string Status = "";
				Status = strCMStatus;
				return Status;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}
	}
}
