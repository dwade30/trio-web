//Fecher vbPorter - Version 1.0.0.32
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;



namespace Global
{
    /// <summary>
    /// Summary description for frmNewArchive.
    /// </summary>
    partial class frmNewArchive : BaseForm
    {
        public fecherFoundation.FCButton btnCreate;
        public fecherFoundation.FCTextBox txtDescription;
        public fecherFoundation.FCTextBox udYear;
        public fecherFoundation.FCLabel lblProgress;
        public fecherFoundation.FCLabel Label2;
        public fecherFoundation.FCLabel Label1;
        public fecherFoundation.FCToolStripMenuItem mnuProcess;
        public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
        public fecherFoundation.FCToolStripMenuItem Seperator;
        public fecherFoundation.FCToolStripMenuItem mnuExit;
        private System.ComponentModel.IContainer components;

        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewArchive));
            this.btnCreate = new fecherFoundation.FCButton();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.udYear = new fecherFoundation.FCTextBox();
            this.lblProgress = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreate)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 327);
            this.BottomPanel.Size = new System.Drawing.Size(542, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.btnCreate);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.udYear);
            this.ClientArea.Controls.Add(this.lblProgress);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(542, 267);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(542, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(145, 30);
            this.HeaderText.Text = "Add Archive";
            // 
            // btnCreate
            // 
            //FC:FINAL:MSH - i.issue #1332: change button style to 'AcceptButton'
            //this.btnCreate.AppearanceKey = "actionButton";
            this.btnCreate.AppearanceKey = "acceptButton";
            this.btnCreate.ForeColor = System.Drawing.Color.White;
            this.btnCreate.Location = new System.Drawing.Point(30, 150);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Shortcut = Wisej.Web.Shortcut.F12;
            this.btnCreate.Size = new System.Drawing.Size(140, 40);
            this.btnCreate.TabIndex = 5;
            this.btnCreate.Text = "Create Archive";
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // txtDescription
            // 
            this.txtDescription.AutoSize = false;
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.LinkItem = null;
            this.txtDescription.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtDescription.LinkTopic = null;
            this.txtDescription.Location = new System.Drawing.Point(181, 90);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(333, 40);
            this.txtDescription.TabIndex = 3;
            // 
            // udYear
            // 
            this.udYear.AutoSize = false;
            this.udYear.LinkItem = null;
            this.udYear.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.udYear.LinkTopic = null;
            this.udYear.Location = new System.Drawing.Point(181, 30);
            this.udYear.LockedOriginal = true;
            this.udYear.MaxLength = 4;
            this.udYear.Name = "udYear";
            this.udYear.ReadOnly = false;
            this.udYear.Size = new System.Drawing.Size(333, 40);
            this.udYear.TabIndex = 1;
            this.udYear.Text = "2010";
            // 
            // lblProgress
            // 
            this.lblProgress.Location = new System.Drawing.Point(30, 210);
            this.lblProgress.Name = "lblProgress";
            this.lblProgress.Size = new System.Drawing.Size(484, 35);
            this.lblProgress.TabIndex = 5;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 104);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(92, 22);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "DESCRIPTION";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(92, 15);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "ARCHIVE YEAR";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSaveContinue,
            this.Seperator,
            this.mnuExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuSaveContinue
            // 
            this.mnuSaveContinue.Index = 0;
            this.mnuSaveContinue.Name = "mnuSaveContinue";
            this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveContinue.Text = "Save & Continue";
            this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 2;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmNewArchive
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(542, 435);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmNewArchive";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Add Archive";
            this.Load += new System.EventHandler(this.frmNewArchive_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmNewArchive_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnCreate)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
    }
}