﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cDBBackup
	{
		//=========================================================
		private string strFileName = string.Empty;
		private string strPath = string.Empty;
		private string strDataGroup = string.Empty;
		private FCCollection collRestorePoints = new FCCollection();
		private string strDatabaseType = string.Empty;
		private string strGroupType = string.Empty;
		private string strFullDBName = string.Empty;

		public string FullDBName
		{
			set
			{
				strFullDBName = value;
			}
			get
			{
				string FullDBName = "";
				FullDBName = strFullDBName;
				return FullDBName;
			}
		}

		public string GroupType
		{
			set
			{
				strGroupType = value;
			}
			get
			{
				string GroupType = "";
				GroupType = strGroupType;
				return GroupType;
			}
		}

		public string DatabaseType
		{
			set
			{
				strDatabaseType = value;
			}
			get
			{
				string DatabaseType = "";
				DatabaseType = strDatabaseType;
				return DatabaseType;
			}
		}

		public string FileName
		{
			set
			{
				strFileName = value;
			}
			get
			{
				string FileName = "";
				FileName = strFileName;
				return FileName;
			}
		}

		public string FilePath
		{
			set
			{
				strPath = value;
			}
			get
			{
				string FilePath = "";
				FilePath = strPath;
				return FilePath;
			}
		}

		public string DataGroup
		{
			set
			{
				strDataGroup = value;
			}
			get
			{
				string DataGroup = "";
				DataGroup = strDataGroup;
				return DataGroup;
			}
		}

		public FCCollection RestorePoints
		{
			get
			{
				FCCollection RestorePoints = null;
				RestorePoints = collRestorePoints;
				return RestorePoints;
			}
		}
	}
}
