﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;

#if TWCR0000
using TWCR0000;


#elif TWBD0000
using TWBD0000;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;
using modStatusPayments = TWUT0000.modMain;
using modUseCR = TWUT0000.modUTUseCR;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmUTCLStatus.
	/// </summary>
	public partial class frmUTCLStatus : BaseForm
	{
        private bool executingvsRateInfoValidateEdit = false;

        public frmUTCLStatus()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private void InitializeComponentEx()
		{
			this.cmdDisc = new System.Collections.Generic.List<fecherFoundation.FCButton>();
			this.Label1 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.cmdDisc.AddControlArrayElement(cmdDisc_2, FCConvert.ToInt16(2));
			this.cmdDisc.AddControlArrayElement(cmdDisc_1, FCConvert.ToInt16(1));
			this.cmdDisc.AddControlArrayElement(cmdDisc_0, FCConvert.ToInt16(0));
			this.Label1.AddControlArrayElement(Label1_8, FCConvert.ToInt16(8));
			this.Label1.AddControlArrayElement(Label1_4, FCConvert.ToInt16(4));
			this.Label1.AddControlArrayElement(Label1_9, FCConvert.ToInt16(9));
			this.Label1.AddControlArrayElement(Label1_11, FCConvert.ToInt16(11));
			this.Label1.AddControlArrayElement(Label1_1, FCConvert.ToInt16(1));
			this.Label1.AddControlArrayElement(Label1_2, FCConvert.ToInt16(2));
			this.Label1.AddControlArrayElement(Label1_10, FCConvert.ToInt16(10));
			this.Label1.AddControlArrayElement(Label1_3, FCConvert.ToInt16(3));
			this.Label1.AddControlArrayElement(Label1_12, FCConvert.ToInt16(12));
			//this.Label1.AddControlArrayElement(Label1_0, FCConvert.ToInt16(0));
			//FC:FINAL:DDU:#1039 - decrease row height
			vsRateInfo.RowHeight(-1, 400);
			vsPeriod.RowHeight(-1, 550);
			vsPeriod.CellBorderStyle = DataGridViewCellBorderStyle.None;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUTCLStatus InstancePtr
		{
			get
			{
				return (frmUTCLStatus)Sys.GetInstance(typeof(frmUTCLStatus));
			}
		}

		protected frmUTCLStatus _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/19/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/06/2007              *
		// ********************************************************
		clsDRWrapper rsCL = new clsDRWrapper();
		// Collections Recordset
		clsDRWrapper rsLRW = new clsDRWrapper();
		// Water Lien Record Recordset
		clsDRWrapper rsLRS = new clsDRWrapper();
		// Sewer Lien Record Recordset
		public int CurrentAccount;
		// Holds the Account Number of the Current Account
		public int CurrentAccountKey;
		// Holds the Account ID for the current account
		string CurrentOwner = "";
		string strSQL = "";
		// Temporary Holding Variable for SQL Statements
		string DataName = "";
		int intHoldBill;
		public bool FormLoaded;
		// true if form is loaded so that it can be activated many times without resetting the grid
		public bool FormReset;
		// if the effective date gets changed, then this gets set to true to allow the form to recreate the grid
		double curCosts;
		double curCurrentInterest;
		double curPreLienInterest;
		// vbPorter upgrade warning: curPrincipalDue As double	OnWriteFCConvert.ToDecimal(
		decimal curPrincipalDue;
		// vbPorter upgrade warning: curTaxDue As double	OnWriteFCConvert.ToDecimal(
		decimal curTaxDue;
		// vbPorter upgrade warning: bcodeW As FixedString	OnWrite(string)
		string bcodeW = "";
		// new char[4]; // Billing Code
		// vbPorter upgrade warning: bcodeS As FixedString	OnWrite(string)
		string bcodeS = "";
		// new char[4]; // Billing Code
		int intNumberOfPeriods;
		// vbPorter upgrade warning: OriginalBillDate As DateTime	OnWrite(DateTime, string)
		DateTime OriginalBillDate;
		int CurRowS;
		// This holds the current line number of the flexgrid
		int CurRowW;
		// This holds the current line number of the flexgrid
		int MaxRowsS;
		// Holds the max rows in the grid
		int MaxRowsW;
		// Holds the max rows in the grid
		bool CollapseFlag;
		// Tells the collapse routine to fire or not
		int ParentLineS;
		// Holds the line number of the last header line (used in swapping and holding totals)
		int ParentLineW;
		// Holds the line number of the last header line (used in swapping and holding totals)
		// vbPorter upgrade warning: sumWPrin As double	OnWrite(Decimal, short)
		double sumWPrin;
		// these sums will keep track of the account totals for the bottom subtotal line
		// vbPorter upgrade warning: sumWTax As double	OnWrite(Decimal, short)
		double sumWTax;
		// vbPorter upgrade warning: sumWInt As double	OnWrite(Decimal, short)
		double sumWInt;
		// vbPorter upgrade warning: sumWCost As double	OnWrite(Decimal, short)
		double sumWCost;
		// vbPorter upgrade warning: sumWTotal As double	OnWrite(Decimal, short)
		double sumWTotal;
		// vbPorter upgrade warning: sumSPrin As double	OnWrite(Decimal, short)
		double sumSPrin;
		// these sums will keep track of the account totals for the bottom subtotal line
		// vbPorter upgrade warning: sumSTax As double	OnWrite(Decimal, short)
		double sumSTax;
		// vbPorter upgrade warning: sumSInt As double	OnWrite(Decimal, short)
		double sumSInt;
		// vbPorter upgrade warning: sumSCost As double	OnWrite(Decimal, short)
		double sumSCost;
		// vbPorter upgrade warning: sumSTotal As double	OnWrite(Decimal, short)
		double sumSTotal;
		bool boolPayment;
		// true if this is the payment screen false if status
		bool boolDisableInsert;
		// kk03132015 trocrs-36  Add option to disable automatically filling in pmt amount
		bool boolRefKeystroke;
		// this is true if a keystroke is used to get into cmbPeriod
		bool boolNegPayments;
		string strStatusString = "";
		// this just holds the string for the menu options
		string strPaymentString = "";
		// this just holds the string for the menu options
		int lngGroupNumber;
		int lngGroupID;
		string strGroupComment = "";
		public bool boolUnloadOK;
		// this is to check to see if the user really wants to leave the screen with pending activity
		public double dblTotalPerDiem;
		// this is the sum of each year per diem to be shown with the rate info
		bool boolPop;
		// this is true when there is a priority comment on this account
		string strNoteText = "";
		// this is where the note text will be stored
		string strMailingAddress1 = "";
		// this will store the mailing address from RE
		string strMailingAddress2 = "";
		string strSupplemntalBillString;
		// this holds the string that I use to ID the parent row from others during the info swap
		string strDemandBillString;
		// this holds the string for Demand Fees
		// vbPorter upgrade warning: intLDNAnswer As short, int --> As DialogResult
		public DialogResult intLDNAnswer;
		// this will hold the answer for the lien discharge question so that the question will only be asked once
		string strCurrentBookPage = "";
		public bool boolNoCurrentInt;
		// this will be set by PaymentRecords if there is a tax club so that current interest is no calculated
		public bool boolCollapsedInputBoxes;
		public bool boolShowingWater;
		// if this variable is true then this screen is set to show the water totals...sewer is false
		public bool boolDoNotResetRTD;
		// If this is true then it will not reset the RTD to the EID
		int lngLienNumberW;
		int lngLienNumberS;
		public bool boolOnlyPrepayYear;
		public int lngUTResCode;
		bool boolUseMHFromRE;
		// This will be true if the MH screen should use the RE information
		bool boolUseREInformation;
		// This is true if the Use RE Information flag is set
		int lngAssociatedREAccount;
		clsGridAccount clsAcct = new clsGridAccount();
		bool boolExitF12Seq;
		public bool boolCheckOnlyOldestBill;
		public bool boolDoNotContinueSave;
		public int lngPreviewStyle;
		// 0 - Water, 1 - Sewer, 2 - Both
		bool boolDoNotSetService;
		string strCurrentGridSelect = "";
		// MAL@20080312: Stores which grid is selected when split view (used for right-click processes)
		int intCharCount;
		// kk03312015 trouts-127
		int accountValue = 0;
		public bool blnWBill;
		// MAL@20080610: Determines if option to pay water bill should exist even if service is not "W" or "B" ; Tracker Reference: 14076
		public bool blnSBill;
		public int lngGRIDCOLTree;
		public int lngGRIDCOLYear;
		public int lngGRIDColBillNumber;
		public int lngGridColBillKey;
		public int lngGRIDColDate;
		public int lngGRIDColRef;
		public int lngGRIDColService;
		public int lngGRIDColPaymentCode;
		public int lngGRIDColPrincipal;
		public int lngGRIDColPTC;
		public int lngGRIDColTax;
		public int lngGRIDColInterest;
		public int lngGRIDColCosts;
		public int lngGRIDColTotal;
		public int lngGRIDColLineCode;
		public int lngGRIDColPending;
		public int lngGRIDColPaymentKey;
		public int lngGRIDColCHGINTNumber;
		public int lngGRIDColPerDiem;
		public int lngPayGridColBill;
		public int lngPayGridColDate;
		public int lngPayGridColRef;
		public int lngPayGridColService;
		public int lngPayGridColCode;
		public int lngPayGridColCDAC;
		public int lngPayGridColPrincipal;
		public int lngPayGridColTax;
		public int lngPayGridColInterest;
		public int lngPayGridColCosts;
		public int lngPayGridColArrayIndex;
		public int lngPayGridColKeyNumber;
		public int lngPayGridColCHGINTNumber;
		public int lngPayGridColTotal;
		public int OwnerPartyID;
		// Interface to CR
		// vbPorter upgrade warning: intPassType As short	OnWriteFCConvert.ToInt32(
		public void Init(short intPassType, int lngPassAcctKey, bool boolShowMissingAccount = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will start this form
				clsDRWrapper rsTemp = new clsDRWrapper();
				if (intPassType == 1)
				{
					boolPayment = false;
				}
				else
				{
					boolPayment = true;
				}
				// this is the account ID rather than the account
				rsTemp.OpenRecordset("SELECT AccountNumber, ResCode FROM Master WHERE ID = " + FCConvert.ToString(lngPassAcctKey), modExtraModules.strUTDatabase);
				if (!rsTemp.EndOfFile())
				{
					// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					CurrentAccount = rsTemp.Get_Fields_Int32("AccountNumber");
					// TODO: Check the table for the column [ResCode] and replace with corresponding Get_Field method
					lngUTResCode = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields("ResCode"))));
					CurrentAccountKey = lngPassAcctKey;
					modUTFunctions.Statics.lngCurrentAccountUT = rsTemp.Get_Fields("AccountNumber");
					// MAL@20071206
					modUTFunctions.Statics.lngCurrentAccountKeyUT = lngPassAcctKey;
					//kk01202017 trout-1225
					CreateStatusQueries(ref CurrentAccountKey);
				}
				else
				{
					if (boolShowMissingAccount)
					{
						MessageBox.Show("This account does not exist.", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					Close();
					return;
				}
				frmWait.InstancePtr.Unload();
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Status Screen", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CreateStatusQueries(ref int lngAcct)
		{
			// this will create the two queries strings that the status screen will use
			string strSQL;
			// this will load all of the bills
			// Showing No bills so for any credits added to the account '05/18/2006
			// strSQL = "SELECT * FROM BillAccountInfo WHERE AccountKey = " & lngAcct ' & " AND NOT Bill.NoBill" 'BillStatus = 'B' AND
			strSQL = "SELECT  b.ID AS Bill,b.AccountKey AS AccountKey,b.BillNumber,b.BillingYear,b.Service,b.BillStatus,b.BillDate,b.BillingRateKey,b.PrevReading,b.PrevDate,b.CurReading,b.CurDate,b.FinalBillDate,b.FinalStartDate,b.FinalEndDate,b.Final," + "b.BName,b.BName2,b.BAddress1,b.BAddress2,b.BAddress3,b.BCity,b.BState,b.BZip,b.BZip4," + "b.OName,b.OName2,b.OAddress1,b.OAddress2,b.OAddress3,b.OCity,b.OState,b.OZip,b.OZip4," + "b.WPrinOwed,b.WTaxOwed,b.WIntOwed,b.WCostOwed,b.WIntAdded,b.WCostAdded,b.WPrinPaid,b.WTaxPaid,b.WIntPaid,b.WCostPaid,b.WIntPaidDate,b.WLienRecordNumber,b.WDemandGroupID,b.WBillOwner,b.WCombinationLienKey," + "b.SPrinOwed,b.STaxOwed,b.SIntOwed,b.SCostOwed,b.SIntAdded,b.SCostAdded,b.SPrinPaid,b.STaxPaid,b.SIntPaid,b.SCostPaid,b.SIntPaidDate,b.SLienRecordNumber,b.SDemandGroupID,b.SBillOwner,b.SCombinationLienKey," + "m.* FROM Bill b INNER JOIN Master m ON b.AccountKey = m.ID WHERE ISNULL(Deleted,0) = 0 AND AccountKey = " + FCConvert.ToString(lngAcct);
			// & " AND NOT (b.NoBill = 1)"
			modUTFunctions.Statics.strUTCurrentAccountBills = strSQL;
			// this will load all of the accounts
			strSQL = "SELECT * FROM PaymentRec WHERE AccountKey = " + FCConvert.ToString(lngAcct);
			modUTFunctions.Statics.strUTCurrentAccountPayments = strSQL;
		}

		private void cmbCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strTemp;
			int lngTypeCode = 0;
			int lngBillKey = 0;
			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
			//strTemp = Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1);
			strTemp = Strings.Left(cmbCode.Text, 1);
			if (modUTFunctions.UTYearCodeValidate())
			{
				//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
				//lngTypeCode = FindPaymentCode_2(cmbService.Items[cmbService.SelectedIndex].ToString(), lngBillKey);
				lngTypeCode = FindPaymentCode_2(cmbService.Text, lngBillKey);
				if ((strTemp == "A") || (strTemp == "R"))
				{
					// Abatement
					// set the Affect Cash and the Affect Cash Drawer to False and get the Abatement Account Number to put into the Account Box
					txtCash.Text = "N";
					txtCD.Text = "N";
					txtAcctNumber.TextMatrix(0, 0, GetUTAccount_2("A", lngTypeCode));
					// set the boxes
					InputBoxAdjustment_2(false);
				}
				else if (strTemp == "S")
				{
					// Supplemental
					// set the Affect Cash and the Affect Cash Drawer to False and get the Supplemental Account Number to put into the Account Box
					// cmbService.ListIndex = 0
					txtCash.Text = "N";
					txtCD.Text = "N";
					txtAcctNumber.TextMatrix(0, 0, GetUTAccount_2("S", lngTypeCode));
					// set the boxes
					InputBoxAdjustment_2(true);
				}
				else if (strTemp == "D")
				{
					// Discount
					// If Left(cmbPeriod.Text, 1) = "A" Then
					// set the Affect Cash and the Affect Cash Drawer to False and get the Discount Account Number to put into the Account Box
					ShowDiscountFrame_2(true);
					// txtCash.Text = "N"
					// txtCD.Text = "N"
					txtAcctNumber.TextMatrix(0, 0, GetUTAccount_2("D", lngTypeCode));
					// Else
					// txtCash.Text = "N"
					// txtCD.Text = "N"
					// txtacctnumber.TextMatrix(0,0) = GetUTAccount("D", 90)
					// End If
					// set the boxes
					InputBoxAdjustment_2(true);
				}
				else if (strTemp == "P")
				{
					// Payment
					txtCash.Text = "Y";
					txtCD.Text = "Y";
					txtAcctNumber.TextMatrix(0, 0, "");
					txtAcctNumber.Enabled = false;
					// set the boxes
					InputBoxAdjustment_2(true);
				}
				else if (strTemp == "U")
				{
					// Tax Club
					// set the boxes
					InputBoxAdjustment_2(false);
				}
				else if (strTemp == "Y")
				{
					// Prepayment
					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
					//if (Strings.Right(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString(), 1) != "*" && Conversion.Val(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString()) != 0)
					if (Strings.Right(cmbBillNumber.Text, 1) != "*" && Conversion.Val(cmbBillNumber.Text) != 0)
					{
						cmbCode.SelectedIndex = 2;
					}
					else
					{
						txtCash.Text = "Y";
						txtCD.Text = "Y";
						txtAcctNumber.TextMatrix(0, 0, "");
						txtAcctNumber.Enabled = false;
					}
					// set the boxes
					InputBoxAdjustment_2(true);
				}
				else if (strTemp == "C")
				{
					// set the boxes
					InputBoxAdjustment_2(false);
				}
				else
				{
					modUTFunctions.Statics.boolUTMultiLine = false;
					txtCash.Text = "Y";
					txtCD.Text = "Y";
					txtAcctNumber.TextMatrix(0, 0, "");
					txtAcctNumber.Enabled = false;
					// set the boxes
					InputBoxAdjustment_2(true);
				}
			}
			else
			{
				cmbCode.SelectedIndex = 2;
			}
		}

		private void cmbCode_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbCode.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}

		private void cmbCode_Enter(object sender, System.EventArgs e)
		{
			boolRefKeystroke = true;
		}

		private void cmbCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// left = 37
			// right = 39
			switch (KeyCode)
			{
				case Keys.Left:
					{
						boolRefKeystroke = true;
						cmbService.Focus();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						if (txtCD.Visible && txtCD.Enabled)
						{
							txtCD.Focus();
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbCode.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbCode.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmdNoteCancel_Click(object sender, System.EventArgs e)
		{
			// Hide the Note frame
			fraEditNote.Visible = false;
			if (boolShowingWater)
			{
				WGRID.Visible = true;
				lblWaterHeading.Visible = true;
			}
			SGRID.Visible = true;
			lblSewerHeading.Visible = true;
			if (modExtraModules.IsThisCR() && modGlobalConstants.Statics.gboolCR)
			{
				fraPayment.Visible = true;
				mnuPayment.Visible = true;
				cmdPaymentPreview.Visible = true;
				cmdSavePayments.Visible = true;
				cmdPaymentSaveExit.Visible = true;
			}
			else if (boolPayment)
			{
				fraPayment.Visible = true;
				mnuPayment.Visible = true;
				cmdPaymentPreview.Visible = true;
				cmdSavePayments.Visible = true;
				cmdPaymentSaveExit.Visible = true;
			}
		}

		private void cmdNoteProcess_Click(object sender, System.EventArgs e)
		{
			EditNote();
			fraEditNote.Visible = false;
			if (boolShowingWater)
			{
				WGRID.Visible = true;
				lblWaterHeading.Visible = true;
			}
			SGRID.Visible = true;
			lblSewerHeading.Visible = true;
			if (modExtraModules.IsThisCR() && modGlobalConstants.Statics.gboolCR)
			{
				fraPayment.Visible = true;
				mnuPayment.Visible = true;
				cmdPaymentPreview.Visible = true;
				cmdSavePayments.Visible = true;
				cmdPaymentSaveExit.Visible = true;
			}
			else if (boolPayment)
			{
				fraPayment.Visible = true;
				mnuPayment.Visible = true;
				cmdPaymentPreview.Visible = true;
				cmdSavePayments.Visible = true;
				cmdPaymentSaveExit.Visible = true;
			}
		}

		private void cmbService_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int intCT;
			if (!boolDoNotSetService)
			{
				//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
				//if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) != "B")
				if (Strings.Left(cmbService.Text, 1) != "B")
				{
					// this will allow the Both option to be selected
					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
					//modUTFunctions.FillUTBillNumber_8(FCConvert.CBool(Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "W"), false);
					modUTFunctions.FillUTBillNumber_8(FCConvert.CBool(Strings.Left(cmbService.Text, 1) == "W"), false);
					if (boolOnlyPrepayYear)
					{
						cmbBillNumber.SelectedIndex = 0;
						cmbCode.SelectedIndex = 4;
					}
					else
					{
						cmbBillNumber.SelectedIndex = cmbBillNumber.Items.Count - 1;
					}
					// Doevents
					cmbBillNumber_Click();
				}
				else
				{
					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
					//modUTFunctions.FillUTBillNumber_8(FCConvert.CBool(Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) != "S"), true);
					modUTFunctions.FillUTBillNumber_8(FCConvert.CBool(Strings.Left(cmbService.Text, 1) != "S"), true);
					// set the bill number to Auto
					for (intCT = 0; intCT <= cmbBillNumber.Items.Count - 1; intCT++)
					{
						if (Strings.Left(cmbBillNumber.Items[intCT].ToString(), 1) == "A")
						{
							cmbBillNumber.SelectedIndex = intCT;
							break;
						}
					}
					txtCash.Text = "Y";
					// show the total due for both grids
					vsPeriod.TextMatrix(0, 1, Strings.Format(FCConvert.ToString(Conversion.Val(WGRID.TextMatrix(WGRID.Rows - 1, lngGRIDColTotal)) + Conversion.Val(SGRID.TextMatrix(SGRID.Rows - 1, lngGRIDColTotal))), "#,##0.00"));
				}
			}
		}

		private void cmbService_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbService.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void cmbService_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						txtReference.Focus();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						cmbCode.Focus();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbService.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbService.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbBillNumber_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// this sub will show the totals line above the payment boxes with period information
			clsDRWrapper rsYear = new clsDRWrapper();
			int TotLine = 0;
			string strSQL = "";
			int lngBill = 0;
			int LRN;
			// vbPorter upgrade warning: Tax1 As Decimal	OnWrite(Decimal, double)
			Decimal Tax1;
			Decimal Tax2;
			Decimal Tax3;
			Decimal Tax4;
			int intType;
			bool boolWater = false;
			string strWS = "";
			clsDRWrapper rsCheck = new clsDRWrapper();
			intType = FindPaymentType();
			switch (intType)
			{
				case 0:
					{
						boolWater = true;
						strWS = "W";
						break;
					}
				case 1:
					{
						boolWater = false;
						strWS = "S";
						break;
					}
			}
			//end switch
			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
			//if (cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString() == "Auto")
			if (cmbBillNumber.Text == "Auto")
			{
				if (modUTFunctions.UTYearCodeValidate())
				{
					cmbCode.SelectedIndex = 2;
					vsPeriod.TextMatrix(0, 0, "Total Due:");
					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
					//if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "B")
					if (Strings.Left(cmbService.Text, 1) == "B")
					{
						vsPeriod.TextMatrix(0, 1, Strings.Format(FCConvert.ToString(Conversion.Val(WGRID.TextMatrix(WGRID.Rows - 1, lngGRIDColTotal)) + Conversion.Val(SGRID.TextMatrix(SGRID.Rows - 1, lngGRIDColTotal))), "#,##0.00"));
					}
					else
					{
						if (boolWater)
						{
							vsPeriod.TextMatrix(0, 1, Strings.Format(WGRID.TextMatrix(WGRID.Rows - 1, lngGRIDColTotal), "#,##0.00"));
						}
						else
						{
							vsPeriod.TextMatrix(0, 1, Strings.Format(SGRID.TextMatrix(SGRID.Rows - 1, lngGRIDColTotal), "#,##0.00"));
						}
					}
					vsPeriod.TextMatrix(0, 2, "");
					vsPeriod.TextMatrix(0, 3, "");
					vsPeriod.TextMatrix(0, 4, "");
					// If TownService = "B" Then
					// boolDoNotSetService = True
					// cmbService.ListIndex = 0
					// boolDoNotSetService = False
					// End If
				}
				else
				{
					cmbBillNumber.SelectedIndex = -1;
				}
			}
			else
			{
				//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
				//lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString()));
				lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbBillNumber.Text)));
				rsCheck.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE (Service = '" + strWS + "' or Service = 'B') AND BillNumber = " + FCConvert.ToString(lngBill), modExtraModules.strUTDatabase);
				if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
				{
					// do nothing
				}
				else
				{
					rsCheck.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE (Service = '" + strWS + "' or Service = 'B') AND " + strWS + "LienRecordNumber IN (SELECT Lien.ID AS LienKey FROM Lien INNER JOIN Bill On Bill." + strWS + "LienRecordNumber = Lien.ID WHERE RateKey = " + FCConvert.ToString(lngBill) + " AND AccountKey = " + FCConvert.ToString(CurrentAccountKey) + ")", modExtraModules.strUTDatabase);
					if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
					{
						// do nnothing
					}
					else
					{
						// MAL@20081222: Changed from purposely changing the service from sewer to water on pre-payment
						// Tracker Reference: 16111
						// If boolWater = False Then
						if (boolWater)
						{
							boolWater = true;
							strWS = "W";
						}
						else
						{
							boolWater = false;
							strWS = "S";
						}
					}
				}
				// kk10072016 trocr-446  Default payment type to Y - Prepay for bill number zero
				if (lngBill == 0)
				{
					cmbCode.SelectedIndex = 4;
				}
				else if (cmbCode.SelectedIndex == 4)
				{
					cmbCode.SelectedIndex = 2;
					// If the payment type is Y make it P
				}
				if (boolWater)
				{
					SetService_2("W");
				}
				else
				{
					SetService_2("S");
				}
				if (boolWater)
				{
					TotLine = (NextSpacerLine_6(WGRID.FindRow(lngBill.ToString(), 1, lngGRIDColBillNumber), true) - 1);
				}
				else
				{
					TotLine = (NextSpacerLine_6(SGRID.FindRow(lngBill.ToString(), 1, lngGRIDColBillNumber), false) - 1);
				}
				if (TotLine > 0)
				{
					if (boolWater)
					{
						strSQL = "SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE BillNumber = " + FCConvert.ToString(lngBill) + " AND Service <> 'S'";
					}
					else
					{
						strSQL = "SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE BillNumber = " + FCConvert.ToString(lngBill) + " AND Service <> 'W'";
					}
					rsYear.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
					if (rsYear.EndOfFile() != true)
					{
						if (rsYear.Get_Fields_Int32(strWS + "LienRecordNumber") != 0)
						{
							// it is a lien
							if (boolWater)
							{
								Tax1 = FCConvert.ToDecimal(WGRID.TextMatrix(TotLine, lngGRIDColTotal));
							}
							else
							{
								Tax1 = FCConvert.ToDecimal(SGRID.TextMatrix(TotLine, lngGRIDColTotal));
							}
							vsPeriod.TextMatrix(0, 0, "Total Due:");
							vsPeriod.TextMatrix(0, 1, Strings.Format(Tax1, "#,##0.00"));
						}
						else
						{
							// it is NOT a lien
							if (boolWater)
							{
								Tax1 = FCConvert.ToDecimal(rsYear.Get_Fields_Double("WPrinOwed") + rsYear.Get_Fields_Double("WTaxOwed") + rsYear.Get_Fields_Double("WIntOwed") + rsYear.Get_Fields_Double("WCostOwed") - modUTFunctions.Statics.dblUTCurrentInt[lngBill, intType]);
								Tax1 += FCConvert.ToDecimal(-rsYear.Get_Fields_Double("WIntAdded") - rsYear.Get_Fields_Double("WCostAdded"));
								Tax1 += FCConvert.ToDecimal(-rsYear.Get_Fields_Double("WPrinPaid") - rsYear.Get_Fields_Double("WTaxPaid") - rsYear.Get_Fields_Double("WIntPaid") - rsYear.Get_Fields_Double("WCostPaid"));
							}
							else
							{
								Tax1 = FCConvert.ToDecimal(rsYear.Get_Fields_Double("SPrinOwed") + rsYear.Get_Fields_Double("STaxOwed") + rsYear.Get_Fields_Double("SIntOwed") + rsYear.Get_Fields_Double("SCostOwed") - modUTFunctions.Statics.dblUTCurrentInt[lngBill, intType]);
								Tax1 += FCConvert.ToDecimal(-rsYear.Get_Fields_Double("SIntAdded") - rsYear.Get_Fields_Double("SCostAdded"));
								Tax1 += FCConvert.ToDecimal(-rsYear.Get_Fields_Double("SPrinPaid") - rsYear.Get_Fields_Double("STaxPaid") - rsYear.Get_Fields_Double("SIntPaid") - rsYear.Get_Fields_Double("SCostPaid"));
							}
							// kk10072015 trocr-446  Take out this CL stuff, Tax1 always ends up back in Tax1!
							// If Tax1 < 0 Then                    'this needs to change, needs to set the tax values earlier
							// Tax2 = Tax2 + Tax1
							// Tax1 = 0
							// If Tax2 < 0 Then
							// Tax3 = Tax3 + Tax2
							// Tax2 = 0
							// If Tax3 < 0 Then
							// Tax4 = Tax4 + Tax3
							// Tax3 = 0
							// If Tax4 < 0 Then
							// Tax1 = Tax4
							// Tax4 = 0
							// End If
							// End If
							// End If
							// End If
							vsPeriod.TextMatrix(0, 0, "Total Due:");
							vsPeriod.TextMatrix(0, 1, Strings.Format(Tax1, "#,##0.00"));
						}
					}
					else
					{
						// This is a Lien Record
						if (boolWater)
						{
							Tax1 = FCConvert.ToDecimal(WGRID.TextMatrix(TotLine, lngGRIDColTotal));
						}
						else
						{
							Tax1 = FCConvert.ToDecimal(SGRID.TextMatrix(TotLine, lngGRIDColTotal));
						}
						vsPeriod.TextMatrix(0, 0, "Total Due:");
						vsPeriod.TextMatrix(0, 1, Strings.Format(Tax1, "#,##0.00"));
					}
				}
			}
			rsYear.Reset();
		}

		public void cmbBillNumber_Click()
		{
			cmbBillNumber_SelectedIndexChanged(cmbBillNumber, new System.EventArgs());
		}

		private void cmbBillNumber_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbBillNumber.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void cmbBillNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtTransactionDate.Visible && txtTransactionDate.Enabled)
						{
							txtTransactionDate.Focus();
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Return:
					{
						// if they hit enter, move them to the next field
						break;
					}
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbBillNumber.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbBillNumber.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		public void cmdDisc_Click_2(short Index)
		{
			cmdDisc_Click(Index);
		}

		public void cmdDisc_Click(short Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// Yes
						// continue processing
						// add this payment to the UTPaymentArray and to the Bottom List
						switch (FindPaymentType())
						{
							case 0:
								{
									AddDiscountToList_2(true);
									break;
								}
							case 1:
								{
									AddDiscountToList_2(false);
									break;
								}
							default:
								{
									return;
								}
						}
						//end switch
						// hide discount fram and show grid, mnuoptions, payment frame
						fraDiscount.Visible = false;
						if (boolShowingWater)
						{
							WGRID.Visible = true;
						}
						else
						{
							SGRID.Visible = true;
						}
						fraPayment.Visible = true;
						mnuPayment.Visible = true;
						cmdPaymentPreview.Visible = true;
						break;
					}
				case 1:
					{
						// No
						// let the user adjust the amount to be paid and recalculate
						lblDiscountInstructions.Text = "Please enter the correct amount in the space provided below and press Enter.  When it is correct press 'Yes'.";
						txtDiscPrin.ReadOnly = false;
						txtDiscDisc.ReadOnly = false;
						txtDiscPrin.SelectionStart = 0;
						txtDiscPrin.SelectionLength = txtDiscPrin.Text.Length;
						txtDiscPrin.Focus();
						break;
					}
				case 2:
					{
						// Cancel
						// exit window with no recourse setting the cmbCode to "P"
						fraDiscount.Visible = false;
						if (boolShowingWater)
						{
							WGRID.Visible = true;
						}
						else
						{
							SGRID.Visible = true;
						}
						fraPayment.Visible = true;
						mnuPayment.Visible = true;
						cmdPaymentPreview.Visible = true;
						cmbCode.SelectedIndex = FCConvert.ToInt32("3");
						break;
					}
			}
			//end switch
		}

		public void cmdDisc_Click(short Index)
		{
			cmdDisc_Click(Index, cmdDisc[Index], new System.EventArgs());
		}

		private void cmdDisc_Click(object sender, System.EventArgs e)
		{
			short index = cmdDisc.GetIndex((FCButton)sender);
			cmdDisc_Click(index, sender, e);
		}

		private void cmdRIClose_Click(object sender, System.EventArgs e)
		{
			fraRateInfo.Visible = false;
		}

		public void cmdRIClose_Click()
		{
			cmdRIClose_Click(cmdRIClose, new System.EventArgs());
		}

		private void frmUTCLStatus_Activated(object sender, System.EventArgs e)
		{
			bool boolContinue = false;
			int lngAcct = 0;
			// vbPorter upgrade warning: intAns As short, int --> As DialogResult
			DialogResult intAns;
			int lngMK = 0;
			clsDRWrapper rsDefaults = new clsDRWrapper();
			//mnuProcess.Enabled = false;
			if (!modExtraModules.IsThisCR())
			{
				if (modGlobalConstants.Statics.gboolCR)
				{
					cmdProcessGoTo.Enabled = false;
					cmdProcessGoTo.Visible = false;
				}
				if (FormReset)
				{
					this.Text = "Utility Billing Account Status as of " + Strings.Format(modUTFunctions.Statics.UTEffectiveDate, "MM/dd/yyyy");
					Reset_Sum();
				}
				else
				{
					modUTFunctions.Statics.UTEffectiveDate = FCConvert.ToDateTime(Strings.Format(modUTFunctions.Statics.UTEffectiveDate, "MM/dd/yyyy"));
					this.Text = "Utility Billing Account Status as of " + Strings.Format(modUTFunctions.Statics.UTEffectiveDate, "MM/dd/yyyy");
				}
			}
			else
			{
				cmdProcessGoTo.Enabled = false;
				cmdProcessGoTo.Visible = false;
				if (FormReset)
				{
					this.Text = "Utility Billing Account Status as of " + Strings.Format(modUTFunctions.Statics.UTEffectiveDate, "MM/dd/yyyy");
					Reset_Sum();
				}
				else
				{
					if (!FormLoaded)
					{
						// kk03262015 trocrs-21 Handle lose/get focus without resetting date
						modUTFunctions.Statics.UTEffectiveDate = FCConvert.ToDateTime(Strings.Format(DateTime.Now, "MM/dd/yyyy"));
					}
					else
					{
						modUTFunctions.Statics.UTEffectiveDate = FCConvert.ToDateTime(Strings.Format(modUTFunctions.Statics.UTEffectiveDate, "MM/dd/yyyy"));
					}
					this.Text = "Utility Billing Account Status as of " + Strings.Format(modUTFunctions.Statics.UTEffectiveDate, "MM/dd/yyyy");
				}
			}
			if (FormLoaded && !FormReset)
			{
			}
			else
			{
				TryAgain:
				;
				FormLoaded = true;
				FormReset = false;
				CollapseFlag = true;
				intLDNAnswer = 0;
				// reset the answer for the lien discharge notice
				// reset all of the arrays in case the form load did not fire
				rsCL.Reset();
				rsLRW.Reset();
				rsLRS.Reset();
				FCUtils.EraseSafe(modUTFunctions.Statics.UTPaymentArray);
				//FC:FINAL:DDU:#i1047 - create array elements by constructor
				for (int intCT = 0; intCT < modUTFunctions.Statics.UTPaymentArray.Length - 1; intCT++)
				{
					modUTFunctions.Statics.UTPaymentArray[intCT] = new modUTFunctions.UTPayment(0);
				}
				FCUtils.EraseSafe(modUTFunctions.Statics.dblUTCurrentInt);
				FCUtils.EraseSafe(modUTFunctions.Statics.dateUTOldDate);
				WGRID.Rows = 1;
				SGRID.Rows = 1;
				// Doevents
				boolContinue = StartUp();
				// this start the computation
				lngAcct = CurrentAccount;
				// check for a note
				strNoteText = Strings.Trim(GetAccountNote(ref lngAcct, ref boolPop));
				if (boolContinue)
				{
					WGRID.Outline(1);
					// collapses the grid to level zero (one)
					SGRID.Outline(1);
					CollapseFlag = false;
				}
				else
				{
					// account does not exist so go back to selection screen
					if (boolPayment)
					{
						intAns = MessageBox.Show("There are no bill records for this account.  Is this a prepayment for account " + FCConvert.ToString(modUTFunctions.GetAccountNumber(ref CurrentAccountKey)) + "?", "Prepayment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
						switch (intAns)
						{
							case DialogResult.Yes:
								{
									lngMK = frmPrePayYear.InstancePtr.Init(2, CurrentAccountKey);
									// Show vbModal, MDIParent   'this will set the intprepayyear
									if (lngMK > 0)
									{
										modUTFunctions.CreateBlankUTBill(ref CurrentAccountKey, ref lngMK);
										goto TryAgain;
										// this will try this routine again to load the account
										// that should have one blank bill now
									}
									else
									{
										MessageBox.Show("Valid meter not selected.", "Invalid Meter", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										Close();
										return;
									}
									frmGetMasterAccount.InstancePtr.Init(2, true);
									break;
								}
							case DialogResult.No:
								{
									Close();
									return;
								}
							case DialogResult.Cancel:
								{
									Close();
									return;
								}
						}
						//end switch
					}
					else
					{
						frmGetMasterAccount.InstancePtr.Init(1);
						frmWait.InstancePtr.Unload();
						MessageBox.Show("No billing records exist for this account.", "No Billing Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						Close();
						return;
					}
				}
				if (boolPayment)
				{
					modUTFunctions.FillUTPaymentComboBoxes(OverrideServiceCodeForAccount(ref CurrentAccountKey), blnSBill, blnWBill);
					boolDoNotSetService = true;
					modUTFunctions.ResetUTPaymentFrame_6(boolShowingWater, false);
					boolDoNotSetService = false;
					fraPayment.Visible = true;
					fraStatusLabels.Visible = false;
					Format_PaymentGrid();
					if (!modUTFunctions.FillPendingUTTransactions())
					{
						frmWait.InstancePtr.Unload();
						Close();
					}
					modUTFunctions.CheckAllUTPending();
					SetLienVariables();
					modUTFunctions.Format_UTvsPeriod();
					if (txtReference.Enabled && txtReference.Visible)
					{
						txtReference.Focus();
					}
					cmdSavePayments.Enabled = true;
					cmdPaymentSaveExit.Enabled = true;
					// check for tax club payments
					// if there are outstanding payments, then show the tax club label to click
					CheckTaxClubPayments();
					if (NegativeBillValues())
					{
						MessageBox.Show("You have some years with negative balances.  Press F11 to apply the appropriate entries.", "Negative Bill Balances", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					// fraPayment.Visible = False
					// 
					// fraStatusLabels.Visible = True
					// If boolShowingWater Then
					// WGRID.Visible = True
					// SGRID.Visible = False
					// If WGRID.Visible And WGRID.Enabled Then
					// WGRID.SetFocus
					// End If
					// Else
					// SGRID.Visible = True
					// WGRID.Visible = False
					// If SGRID.Visible And SGRID.Enabled Then
					// SGRID.SetFocus
					// End If
					// End If
				}
			}
			if (boolPayment)
			{
				// kk03132105 trocrs-36  Option to disable automatically filling in payment amount on Insert or Double Click
				rsDefaults.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
				if (!rsDefaults.EndOfFile())
				{
					/*? On Error Resume Next  */
					boolDisableInsert = FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("DisableAutoPmtFill"));
					/*? On Error GoTo 0 */
				}
				mnuPayment.Visible = true;
				cmdPaymentPreview.Visible = true;
				cmdProcessGoTo.Text = strStatusString;
				cmdProcessGoTo.Width = 96;
				vsPayments.Visible = true;
				lblPaymentInfo.Visible = true;
				if (FormLoaded && !FormReset)
				{
				}
				else
				{
					if (NegativeBillValues())
					{
						MessageBox.Show("You have some years with negative balances.  Press F11 to apply the appropriate entries.", "Negative Bill Balances", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
			else
			{
				lblPaymentInfo.Visible = false;
				mnuPayment.Visible = false;
				cmdPaymentPreview.Visible = false;
				cmdProcessGoTo.Text = strPaymentString;
				cmdProcessGoTo.Width = 142;
			}
			//mnuProcess.Enabled = true;          
			frmWait.InstancePtr.Unload();
			if (strNoteText != "")
			{
				// if there is a note
				//if (boolPop)
				//{
				//    SHOWAGAIN:;
				//    if (MessageBox.Show("Account " + FCConvert.ToString(lngAcct) + " note." + "\r\n" + strNoteText, "Account Note", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) != DialogResult.OK)
				//    {
				//        goto SHOWAGAIN;
				//    }
				//    boolPop = false;
				//}
				accountValue = lngAcct;
				imgNote.Visible = true;
				mnuFileEditNote.Visible = true;
				mnuFilePriority.Visible = true;
			}
			else
			{
				imgNote.Visible = false;
				mnuFilePriority.Visible = false;
			}
		}
		//FC:FINAL:MSH - Issue #936: Showing MessageBox in activation time generates problem with menu, which not displayed, so we will show message
		// after fully initializing of form
		private void FrmUTCLStatus_Appear(object sender, EventArgs e)
		{
			if (boolPop)
			{
				MessageBox.Show("Account " + FCConvert.ToString(CurrentAccount) + " note." + "\r\n" + strNoteText.Replace("\n", "\r\n"), "Account Note", MessageBoxButtons.OK, MessageBoxIcon.Information);
				accountValue = 0;
				boolPop = false;
			}
		}

		private void frmUTCLStatus_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void frmUTCLStatus_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			int lngRows = 0;
			switch (KeyCode)
			{
				case Keys.Return:
					{
						break;
					}
				case Keys.Escape:
					{
						if (vsPreview.Visible == true)
						{
							HidePreview();
						}
						else if (fraRateInfo.Visible == true)
						{
							// close the rate info frame
							KeyCode = (Keys)0;
							cmdRIClose_Click();
						}
						else
						{
							KeyCode = (Keys)0;
							mnuProcessExit_Click();
						}
						break;
					}
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Insert:
					{
						KeyCode = (Keys)0;
						if (boolDisableInsert)
						{
							// kk03132015  trocrs-36  Add option to disable automatically filling in payment amount
							return;
						}
						if (boolPayment)
						{
							// create a payment line for the year or all years if it is on auto
							string strSvc = "";
							double dbl1;
							double dbl2;
							double dbl3;
							double dbl4;
							//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
							//strSvc = Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1); // kk01192015 trout-1037  move this up here
							strSvc = Strings.Left(cmbService.Text, 1);
							// kk01192015 trout-1037  move this up here
							//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
							//if (cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString() == "Auto")
							if (cmbBillNumber.Text == "Auto")
							{
								if (strSvc != "B")
								{
									// so it's easier to check   Left(cmbService.List(cmbService.ListIndex), 1)
									if (strSvc == "W")
									{
										// and look at the service selected   'If boolShowingWater Then
										lngRows = WGRID.Rows - 1;
									}
									else
									{
										lngRows = SGRID.Rows - 1;
									}
									modUTFunctions.CreateUTOppositionLine_6(lngRows, strSvc == "W");
									// use selected service               'boolShowingWater
								}
								else
								{
									// this is when Auto and Both are showing
									// This should show the total due for both combined
									txtInterest.Text = vsPeriod.TextMatrix(0, 1);
								}
							}
							else
							{
								// strSvc = Left(cmbService.List(cmbService.ListIndex), 1)         'kk01192015 trout-1037  Move this up
								if (strSvc == "B")
								{
									if (modUTFunctions.Statics.gboolPayWaterFirst)
									{
										strSvc = "W";
									}
									else
									{
										strSvc = "S";
									}
								}
								// MAL@20080703: Removed to correctly work for Sewer bills
								// Tracker Reference: 14553
								// If Left(strSVC, 1) = "W" Then
								// DJW@20090420 changed using boolShowingWater to true or false based on strSvc = W or S
								if (strSvc == "W")
								{
									// CreateUTOppositionLine NextSpacerLine(WGRID.FindRow(cmbBillNumber.List(cmbBillNumber.ListIndex), 1, lngGRIDCOLYear), boolShowingWater) - 1, boolShowingWater, False
									modUTFunctions.CreateUTOppositionLine_26(NextSpacerLine_6(WGRID.Row, true) - 1, true, false);
								}
								else
								{
									// CreateUTOppositionLine NextSpacerLine(SGRID.FindRow(cmbBillNumber.List(cmbBillNumber.ListIndex), 1, lngGRIDCOLYear), boolShowingWater) - 1, boolShowingWater, False
									modUTFunctions.CreateUTOppositionLine_26(NextSpacerLine_6(SGRID.Row, false) - 1, false, false);
								}
								// Else
								// this does not need to be seperate anymore
								// CreatePeriodOppositionLine dbl1, 0, 0, 0, 1, cmbBillNumber.List(cmbBillNumber.ListIndex)
								// End If
							}
						}
						break;
					}
			}
			//end switch
		}

		private void frmUTCLStatus_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUTCLStatus.ScaleWidth	= 8880;
			//frmUTCLStatus.ScaleHeight	= 7965;
			//frmUTCLStatus.ClipControls	= 0   'False;
			//frmUTCLStatus.LinkTopic	= "Form1";
			//End Unmaped Properties
			// set the column variables
			lngGRIDCOLTree = 0;
			lngGRIDCOLYear = 1;
			lngGRIDColBillNumber = 2;
			lngGRIDColDate = 3;
			lngGRIDColRef = 4;
			lngGRIDColService = 5;
			lngGRIDColPaymentCode = 6;
			lngGRIDColPrincipal = 7;
			lngGridColBillKey = 8;
			lngGRIDColPTC = 9;
			lngGRIDColTax = 10;
			lngGRIDColInterest = 11;
			lngGRIDColCosts = 12;
			lngGRIDColTotal = 13;
			lngGRIDColLineCode = 14;
			lngGRIDColPaymentKey = 15;
			lngGRIDColCHGINTNumber = 16;
			lngGRIDColPerDiem = 17;
			lngGRIDColPending = 18;
			lngPayGridColBill = 0;
			lngPayGridColDate = 1;
			lngPayGridColRef = 2;
			lngPayGridColService = 3;
			lngPayGridColCode = 4;
			lngPayGridColCDAC = 5;
			lngPayGridColPrincipal = 6;
			lngPayGridColTax = 7;
			lngPayGridColInterest = 8;
			lngPayGridColCosts = 9;
			lngPayGridColArrayIndex = 10;
			lngPayGridColKeyNumber = 11;
			lngPayGridColCHGINTNumber = 12;
			lngPayGridColTotal = 13;
			// This will align the payment options
			//AlignTextBoxesAndCombos();
			// set the window size
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// set the back color of any labels or textboxes
			modGlobalFunctions.SetTRIOColors(this);
			clsAcct.GRID7Light = txtAcctNumber;
			clsAcct.DefaultAccountType = "G";
			clsAcct.ToolTipText = "Hit Shift-F2 to get a valid accounts list.";
			// change the labels headers
			lblWaterHeading.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			lblSewerHeading.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			// kk trouts-6 02282013  Change Water Bill Display to Stormwater
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				lblWaterHeading.Text = "Stormwater";
			}
			// kgk trocl-816 11/2/2011  Per Brenda change the color to Red
			lblGrpInfo.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
			modUTFunctions.Statics.UTEffectiveDate = DateTime.Now;
			imgNote.ImageSource = "imgnote?color=#707884";
			// set the default databases
			rsCL.DefaultDB = modExtraModules.strUTDatabase;
			rsLRW.DefaultDB = modExtraModules.strUTDatabase;
			rsLRS.DefaultDB = modExtraModules.strUTDatabase;
			// set the strings to show
			strStatusString = "Go To Status";
			strPaymentString = "Go To Payments/Adj";
			strSupplemntalBillString = "Lien";
			strDemandBillString = "Dmd";
			// reset all variables and arrays
			Reset_Sum();
			modUTFunctions.Statics.boolUTMultiLine = false;
			FCUtils.EraseSafe(modUTFunctions.Statics.UTPaymentArray);
			FCUtils.EraseSafe(modUTFunctions.Statics.dblUTCurrentInt);
			FCUtils.EraseSafe(modUTFunctions.Statics.dateUTOldDate);
			boolUnloadOK = false;
			//FC:FINAl:SBE - #i895 - add expand button
			SGRID.AddExpandButton(1);
            WGRID.AddExpandButton(1);
            txtAcctNumber.ExtendLastCol = true;
		}

		public void SetToolTip()
		{
			// this will set all of the strings of the tool tips except ones that are in grid cells
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			bool boolContinue = false;
			if (vsPayments.Rows > 1)
			{
				if (!boolUnloadOK)
				{
					boolContinue = true;
					// kk03032014  trocrs-26  Double check for pending payments so we don't delete saved payments
					if (modExtraModules.IsThisCR())
					{
						boolContinue = !PendingSaved();
					}
					if (boolContinue)
					{
						if (MessageBox.Show("You have pending payments, would you like to exit without saving them?", "Exit Payment Screen", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							// erase the pending transactions
							while (!(vsPayments.Rows == 1))
							{
								modGlobalFunctions.AddCYAEntry_728("UT", "Deleting pending payments.", "Bill = " + vsPayments.TextMatrix(1, 0), vsPayments.TextMatrix(1, 1), "Code = " + vsPayments.TextMatrix(1, 4), vsPayments.TextMatrix(1, 3));
								DeletePaymentRow_2(1);
							}
						}
						else
						{
							e.Cancel = true;
							return;
						}
					}
				}
			}
			FormLoaded = false;
			boolDoNotResetRTD = false;
			if (!modExtraModules.IsThisCR())
			{
				if (boolPayment)
				{
					frmGetMasterAccount.InstancePtr.Init(2, true);
				}
				else
				{
					//MDIParent.InstancePtr.Show();
				}
			}
			else
			{
				// frmReceiptInput.Show , MDIParent
			}
		}

		private bool PendingSaved()
		{
			bool PendingSaved = false;
			// this function will check the pending payments to see if there are any that have not been saved yet
			// if there are any pending payments that are not saved, then a true will be returned otherwise false will be
			// kk03032014  trocrs-26  Update this function to check the status of the payment record so we aren't deleting
			// payments that have been saved in another CR session
			int intCT;
			clsDRWrapper rsPend = new clsDRWrapper();
			PendingSaved = true;
			// For intCT = 1 To vsPayments.rows - 1
			// If Val(vsPayments.TextMatrix(intCT, lngPayGridColKeyNumber)) = 0 Then
			// PendingSaved = False
			// Exit For
			// End If
			// Next
			rsPend.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE ReceiptNumber = -1 AND Code <> 'I' ORDER BY RecordedTransactionDate desc", modExtraModules.strUTDatabase);
			PendingSaved = rsPend.EndOfFile();
			return PendingSaved;
		}

		private void frmUTCLStatus_Resize(object sender, System.EventArgs e)
		{
			// when the form gets resized, this will adjust the grid size and the rows/columns
			Format_Grid_2(mnuFileShowSplitView.Visible);
			// Or Not boolPayment
			AutoSize_GRID_8(true, mnuFileShowSplitView.Visible);
			// Or Not boolPayment
			AutoSize_GRID_8(false, mnuFileShowSplitView.Visible);
			// Or Not boolPayment
			Format_PaymentGrid();
			if (fraRateInfo.Visible == true)
			{
				this.fraRateInfo.Left = FCConvert.ToInt32((this.Width - this.fraRateInfo.Width) / 3.0);
				//this.fraRateInfo.Top = FCConvert.ToInt32((this.Height - this.fraRateInfo.Height) / 3.0);
				this.vsRateInfo.SendToBack();
				this.fraRateInfo.CenterToContainer(this.ClientArea);
			}
			// Tracker Reference: 11187
			if (fraEditNote.Visible == true)
			{
				fraEditNote.Left = FCConvert.ToInt32((this.Width - fraEditNote.Width) / 2.0);
				fraEditNote.Top = FCConvert.ToInt32((this.Height - fraEditNote.Height) / 2.0);
			}
			SetHeadingLabels();
		}

		private bool StartUp()
		{
			int intError = 0;
			bool StartUp = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int blnumber = 0;
				int rc;
				int lngWBills = 0;
				int lngSBills = 0;
				bool boolLienDischarged = false;
				clsDRWrapper rsDischarge = new clsDRWrapper();
				int intCT;
				string strWS = "";
				intError = 0;
				// reset the totals
				dblTotalPerDiem = 0;
				// format the flexgrid
				Format_Grid();
				WGRID.Redraw = false;
				// turn this on when ready
				// WGRID.Visible = False
				intError = 1;
				SGRID.Redraw = false;
				// turn this on when ready
				// SGRID.Visible = False
				CurRowS = 0;
				// start on line 1
				CurRowW = 0;
				intError = 2;
				// opens the collections database to the correct billing master record
				// rsCL.OpenRecordset "SELECT * FROM (" & strUTCurrentAccountBills & ") ORDER BY BillStatus desc, BillDate desc, BillNumber Desc, Bill desc", strUTDatabase    'WHERE AccountKey = " & CurrentAccountKey & "
				rsCL.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp ORDER BY BillStatus desc,BillDate desc,BillNumber Desc,Bill desc", modExtraModules.strUTDatabase);
				// MAL@20070917: Change Order By to Bill Date
				intError = 3;
				rc = rsCL.RecordCount();
				if (rc != 0)
				{
					// check for a comment
					lblAcctComment.ForeColor = Color.Red;
					ToolTip1.SetToolTip(lblAcctComment, "This account has a comment.");
					lblTaxAcquired.ForeColor = Color.Red;
					ToolTip1.SetToolTip(lblTaxAcquired, "This account has been tax acquired.");
					if (Strings.Trim(modUTFunctions.GetUTAccountComment_2(FCConvert.ToInt32(rsCL.Get_Fields_Int32("AccountKey")))) != "")
					{
						lblAcctComment.Visible = true;
					}
					else
					{
						lblAcctComment.Visible = false;
					}
					if (boolPayment)
					{
						// MAL@20071231: 11581
						// lblAcctComment.Left = 200
						//lblTaxAcquired.Left = 180;
						//lblTaxAcquired.Top = 0;
						//lblAcctComment.Left = lblTaxAcquired.Left + lblTaxAcquired.Width + 75;
						//lblAcctComment.Top = 0;
						// kgk trout-731 09-20-2011
						//lblGrpInfo.Top = 210;
						//lblACHInfo.Top = 210;
					}
					else
					{
						// lblAcctComment.Left = 0
						//lblTaxAcquired.Left = 180;
						//lblTaxAcquired.Top = 660;
						//lblAcctComment.Left = lblTaxAcquired.Left + lblTaxAcquired.Width + 75;
						//lblAcctComment.Top = 660;
						// kgk trout-731 09-20-2011
						//lblGrpInfo.Top = 350;
						//lblACHInfo.Top = 350;
					}
					FillInUTInfo();
					lngGroupNumber = modGlobalFunctions.GetGroupNumber_6(FCConvert.ToInt32(lblAccount.Text), "UT");
					intError = 7;
					if (modExtraModules.Statics.gstrRegularIntMethod == "F")
					{
						// if this town bills by flat
						// this will check for all bills that might need flat interest amounts applied to them
						modUTFunctions.CheckForFlatInterest_2(rsCL.Get_Fields_Int32("AccountKey"));
					}
					intError = 10;
					// checks for a lien record
					if (rsCL.Get_Fields_Int32("WLienRecordNumber") != 0)
					{
						// if this is a bill is a liened bill
						strSQL = "SELECT * FROM Lien WHERE ID = " + rsCL.Get_Fields_Int32("WLienRecordNumber");
						rsLRW.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
						bcodeW = "LIEN";
						blnumber = rsLRW.Get_Fields_Int32("ID");
					}
					else if (rsCL.Get_Fields_Int32("WDemandGroupID") > 0 && !rsCL.IsFieldNull("WDemandGroupID"))
					{
						// MAL@20071102: Tracker Reference: 11069 ; 'MAL@20080303: Changed to separate W and S fields: Tracker Reference: 12580
						rsLRW.Reset();
						bcodeW = "30DN";
						// TODO: Field [Bill] not found!! (maybe it is an alias?)
						blnumber = rsCL.Get_Fields_Int32("Bill");
					}
					else
					{
						rsLRW.Reset();
						bcodeW = "BILL";
						// TODO: Field [Bill] not found!! (maybe it is an alias?)
						blnumber = rsCL.Get_Fields_Int32("Bill");
					}
					intError = 20;
					// fill the main value labels
					FillMainLabels();
					// empty right now
					intError = 30;
					// clear the abatement array
					for (intCT = 0; intCT <= Information.UBound(modUTFunctions.Statics.UTAbatePaymentsArray, 1) - 1; intCT++)
					{
						modUTFunctions.Statics.UTAbatePaymentsArray[intCT, 0].Per1 = 0;
						modUTFunctions.Statics.UTAbatePaymentsArray[intCT, 0].Per2 = 0;
						modUTFunctions.Statics.UTAbatePaymentsArray[intCT, 0].Per3 = 0;
						modUTFunctions.Statics.UTAbatePaymentsArray[intCT, 0].Per4 = 0;
						modUTFunctions.Statics.UTAbatePaymentsArray[intCT, 1].Per1 = 0;
						modUTFunctions.Statics.UTAbatePaymentsArray[intCT, 1].Per2 = 0;
						modUTFunctions.Statics.UTAbatePaymentsArray[intCT, 1].Per3 = 0;
						modUTFunctions.Statics.UTAbatePaymentsArray[intCT, 1].Per4 = 0;
					}
					intError = 40;
					boolLienDischarged = false;
					// Call FillLabels(blnumber)       'this will fill the labels at the top of the screen with account info
					// this will only put the latest billing info in
					intError = 50;
					while (!(rsCL.EndOfFile() == true))
					{
						// check all records from the same account
						// checks for a lien record
						if (rsCL.Get_Fields_Int32("WLienRecordNumber") != 0)
						{
							strSQL = "SELECT * FROM LIEN WHERE ID = " + rsCL.Get_Fields_Int32("WLienRecordNumber");
							rsLRW.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
							intError = 55;
							if (!boolLienDischarged)
							{
								// only check it if this has not been found yet
								rsDischarge.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + rsCL.Get_Fields_Int32("WLienRecordNumber") + " OR LienKey = " + rsCL.Get_Fields_Int32("SLienRecordNumber"), modExtraModules.strUTDatabase);
								// & " AND Printed = False" 'take this out so that a town can print it over and over in needed
								if (!rsDischarge.EndOfFile())
								{
									boolLienDischarged = true;
								}
							}
							intError = 60;
							if (rsLRW.EndOfFile())
							{
								// if there is no record then only use the regular record
								intError = 65;
								rsLRW.Reset();
								bcodeW = "BILL";
								// TODO: Field [Bill] not found!! (maybe it is an alias?)
								blnumber = rsCL.Get_Fields_Int32("Bill");
							}
							else
							{
								intError = 70;
								bcodeW = "LIEN";
								lngLienNumberW = FCConvert.ToInt32(rsLRW.Get_Fields_Int32("ID"));
								// TODO: Field [Bill] not found!! (maybe it is an alias?)
								blnumber = rsCL.Get_Fields_Int32("Bill");
							}
						}
						else if (rsCL.Get_Fields_Int32("WDemandGroupID") > 0 && !rsCL.IsFieldNull("WDemandGroupID"))
						{
							// MAL@20071102: Tracker Reference: 11069
							intError = 75;
							rsLRW.Reset();
							bcodeW = "30DN";
							// TODO: Field [Bill] not found!! (maybe it is an alias?)
							blnumber = rsCL.Get_Fields_Int32("Bill");
						}
						else
						{
							rsLRW.Reset();
							bcodeW = "BILL";
							// TODO: Field [Bill] not found!! (maybe it is an alias?)
							blnumber = rsCL.Get_Fields_Int32("Bill");
						}
						intError = 80;
						if (rsCL.Get_Fields_Int32("SLienRecordNumber") != 0)
						{
							strSQL = "SELECT * FROM LIEN WHERE ID = " + rsCL.Get_Fields_Int32("SLienRecordNumber");
							rsLRS.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
							intError = 81;
							if (!boolLienDischarged)
							{
								// only check it if this has not been found yet
								rsDischarge.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + rsCL.Get_Fields_Int32("SLienRecordNumber") + " OR LienKey = " + rsCL.Get_Fields_Int32("SLienRecordNumber"), modExtraModules.strUTDatabase);
								// & " AND Printed = False" 'take this out so that a town can print it over and over in needed
								if (!rsDischarge.EndOfFile())
								{
									boolLienDischarged = true;
								}
							}
							intError = 82;
							if (rsLRS.EndOfFile())
							{
								// if there is no record then only use the regular regord
								intError = 83;
								rsLRS.Reset();
								bcodeS = "BILL";
								// TODO: Field [Bill] not found!! (maybe it is an alias?)
								blnumber = rsCL.Get_Fields_Int32("Bill");
							}
							else
							{
								intError = 84;
								bcodeS = "LIEN";
								lngLienNumberS = rsLRS.Get_Fields_Int32("ID");
								// TODO: Field [Bill] not found!! (maybe it is an alias?)
								blnumber = rsCL.Get_Fields_Int32("Bill");
							}
						}
						else if (rsCL.Get_Fields_Int32("SDemandGroupID") > 0 && !rsCL.IsFieldNull("SDemandGroupID"))
						{
							// MAL@20071102: Tracker Reference: 11069
							intError = 85;
							rsLRS.Reset();
							bcodeS = "30DN";
							// TODO: Field [Bill] not found!! (maybe it is an alias?)
							blnumber = rsCL.Get_Fields_Int32("Bill");
						}
						else
						{
							rsLRS.Reset();
							bcodeS = "BILL";
							// TODO: Field [Bill] not found!! (maybe it is an alias?)
							blnumber = rsCL.Get_Fields_Int32("Bill");
						}
						// creates the header line for this bill record
						string vbPorterVar = rsCL.Get_Fields_String("Service");
						if (vbPorterVar == "W")
						{
							intError = 87;
							// fills in the rest of the payments
							FillGrid_18(blnumber, lngWBills, 0);
						}
						else if (vbPorterVar == "S")
						{
							intError = 90;
							// fills in the rest of the payments
							FillGrid_6(blnumber, 0, lngSBills);
						}
						else if (vbPorterVar == "B")
						{
							intError = 95;
							// water & sewer
							// fills in the rest of the payments
							FillGrid(ref blnumber, lngWBills, lngSBills);
							intError = 101;
						}
						intError = 105;
						// Doevents                        'DONT TAKE THIS OUT!!!!!!!!
						rsCL.MoveNext();
					}
					intError = 110;
					mnuFileDischarge.Visible = boolLienDischarged;
					// default this to false
					mnuFileSwitch.Visible = false;
					// this will adjust the initial height
					if (WGRID.Rows > 1)
					{
						Adjust_GRID_Height_8((lngWBills + 2) * WGRID.RowHeight(0), true);
						intError = 115;
						WGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, WGRID.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						WGRID_RowColChange(WGRID, EventArgs.Empty);
						WGRID_Coloring(1);
						WGRID.Select(0, lngGRIDCOLYear, 0, lngGRIDColTotal);
						WGRID.CellBorder(Color.Black, 0, 0, 0, 1, 0, 1);
						Create_Account_Totals_2(true);
						intError = 120;
                        //FC:FINAL:AM:#3779 - set the tooltips here
                        for (int row = 0; row < WGRID.Rows; row++)
                        {
                            for (int col = 1; col < WGRID.Cols; col++)
                            {
                                WGRID_SetToolTips(row, col);
                            }
                        }
                        WGRID.Redraw = true;
						// turn this on when ready
						WGRID.Visible = true;
						// SGRID.Visible = False
						boolShowingWater = true;
						//WGRID.Refresh();
					}
					// WGRID.TextMatrix(0, 0) = WGRID.TextMatrix(0, 0)
					if (SGRID.Rows > 1)
					{
						Adjust_GRID_Height_8((lngSBills + 2) * SGRID.RowHeight(0), false);
						intError = 125;
						SGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, SGRID.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						SGRID_RowColChange(SGRID, EventArgs.Empty);
						SGRID_Coloring(1);
						SGRID.Select(0, lngGRIDCOLYear, 0, lngGRIDColTotal);
						SGRID.CellBorder(Color.Black, 0, 0, 0, 1, 0, 1);
						Create_Account_Totals_2(false);
						intError = 130;
                        //FC:FINAL:AM:#3779 - set the tooltips here
                        for (int row = 0; row < SGRID.Rows; row++)
                        {
                            for (int col = 1; col < SGRID.Cols; col++)
                            {
                                SGRID_SetToolTips(row, col);
                            }
                        }
                        SGRID.Redraw = true;
						// turn this on when ready
						if (WGRID.Rows <= 1)
						{
							SGRID.Visible = true;
							// WGRID.Visible = False
							boolShowingWater = false;
						}
						else
						{
							if (boolPayment)
							{
								// I already know that both grids have bills and then must have both for townservice
								SGRID.Visible = true;
							}
							else
							{
								mnuFileSwitch.Visible = true;
								mnuFileSwitch.Text = "Show Sewer Bills";
							}
						}
						//SGRID.Refresh();
					}
					Format_Grid_1(ref boolShowingWater);
					SetHeadingLabels();
					//this.Refresh();
					intError = 135;
					strGroupComment = modGlobalFunctions.ShowGroupMessage(ref lngGroupNumber);
					// lngGroupNumber was set before with the getgroupnumber function
					intError = 137;
					if (Strings.Trim(strGroupComment) != "")
						MessageBox.Show(strGroupComment, "Group " + FCConvert.ToString(lngGroupNumber) + " Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
					intError = 140;
					StartUp = true;
				}
				else
				{
					// if this account does not exist then go back to the get account screen
					StartUp = false;
				}
				EndTag:
				;
				return StartUp;
			}
			catch (Exception ex)
			{
				
				StartUp = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Startup - " + FCConvert.ToString(intError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return StartUp;
		}

		private void CreateMasterLine_6(int BNum, bool boolWater)
		{
			CreateMasterLine(ref BNum, ref boolWater);
		}

		private void CreateMasterLine(ref int BNum, ref bool boolWater)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this creates the first line for each year
				clsDRWrapper rsRK = new clsDRWrapper();
				int lngRK;
				lngRK = FCConvert.ToInt32(Math.Round(Conversion.Val(rsCL.Get_Fields_Int32("BillingRateKey"))));
				if (lngRK == 0)
				{
					if (boolWater)
					{
						if (rsLRW.Name() != "")
						{
							if (!rsLRW.EndOfFile())
							{
								lngRK = FCConvert.ToInt32(rsLRW.Get_Fields_Int32("RateKey"));
							}
							else
							{
								lngRK = 0;
							}
						}
					}
					else
					{
						if (rsLRS.Name() != "")
						{
							if (!rsLRS.EndOfFile())
							{
								lngRK = FCConvert.ToInt32(rsLRS.Get_Fields_Int32("RateKey"));
							}
							else
							{
								lngRK = 0;
							}
						}
					}
				}
				if (lngRK != 0)
				{
					rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRK), modExtraModules.strUTDatabase);
					if (rsRK.EndOfFile() || rsRK.BeginningOfFile())
					{
						OriginalBillDate = DateTime.Today;
						MessageBox.Show("No rate ID has been defined for this bill.", "Rate ID Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						rsRK.MoveFirst();
						if (!rsRK.IsFieldNull("BillDate"))
						{
							OriginalBillDate = FCConvert.ToDateTime(Strings.Format(rsRK.Get_Fields_DateTime("BillDate"), "MM/dd/yy"));
						}
					}
				}
				if (boolWater)
				{
					CurRowW += 1;
					Add_6(CurRowW, 0, boolWater);
				}
				else
				{
					CurRowS += 1;
					Add_6(CurRowS, 0, boolWater);
				}
				if (boolWater)
				{
					GetDataFromRateTable(ref boolWater);
					// interest
					// TODO: Field [Bill] not found!! (maybe it is an alias?)
					WGRID.TextMatrix(CurRowW, lngGRIDColPaymentKey, FCConvert.ToString(rsCL.Get_Fields("Bill")));
					// TODO: Field [Bill] not found!! (maybe it is an alias?)
					WGRID.TextMatrix(CurRowW, lngGridColBillKey, FCConvert.ToString(rsCL.Get_Fields("Bill")));
					// .TextMatrix(CurRowW, lngGRIDCOLYear) = rsCL.Fields("BillingYear")
					WGRID.TextMatrix(CurRowW, lngGRIDColBillNumber, FCConvert.ToString(rsCL.Get_Fields_Int32("BillNumber")));
					if (lngRK == 0)
					{
						WGRID.TextMatrix(CurRowW, lngGRIDColDate, "No RK");
					}
					else
					{
						WGRID.TextMatrix(CurRowW, lngGRIDColDate, Strings.Format(OriginalBillDate, "MM/dd/yy"));
					}
					WGRID.TextMatrix(CurRowW, lngGRIDColPending, " ");
					ParentLineW = CurRowW;
					// this sets the parent line so that I can easily get back to it from the child rows
				}
				else
				{
					GetDataFromRateTable(ref boolWater);
					// interest
					// TODO: Field [Bill] not found!! (maybe it is an alias?)
					SGRID.TextMatrix(CurRowS, lngGRIDColPaymentKey, FCConvert.ToString(rsCL.Get_Fields("Bill")));
					// TODO: Field [Bill] not found!! (maybe it is an alias?)
					SGRID.TextMatrix(CurRowS, lngGridColBillKey, FCConvert.ToString(rsCL.Get_Fields("Bill")));
					// .TextMatrix(CurRowS, lngGRIDCOLYear) = rsCL.Fields("BillingYear")
					SGRID.TextMatrix(CurRowS, lngGRIDColBillNumber, FCConvert.ToString(rsCL.Get_Fields_Int32("BillNumber")));
					if (lngRK == 0)
					{
						SGRID.TextMatrix(CurRowS, lngGRIDColDate, "No RK");
					}
					else
					{
						SGRID.TextMatrix(CurRowS, lngGRIDColDate, Strings.Format(OriginalBillDate, "MM/dd/yy"));
					}
					SGRID.TextMatrix(CurRowS, lngGRIDColPending, " ");
					ParentLineS = CurRowS;
					// this sets the parent line so that I can easily get back to it from the child rows
				}
				rsRK.Reset();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Master Line", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private short FillGrid_6(int BNum, int lngWaterBills = 0, int lngSewerBills = 0)
		{
			return FillGrid(ref BNum, lngWaterBills, lngSewerBills);
		}

		private short FillGrid_18(int BNum, int lngWaterBills = 0, int lngSewerBills = 0)
		{
			return FillGrid(ref BNum, lngWaterBills, lngSewerBills);
		}

		private short FillGrid(ref int BNum, int lngWaterBills = 0, int lngSewerBills = 0)
		{
			short FillGrid = 0;
			// this sub will fill the subordinate rows with the billing/payment/lien information
			DateTime dateTemp = DateTime.FromOADate(0);
			bool boolTemp = false;
			string strName = "";
			int intCT;
			double dblPerDiem = 0;
			int l = 0;
			int sumRow;
			int j;
			double sum = 0;
			bool Match;
			double dblTotalDue;
			int lngDemandID;
			// MAL@20071102
			intHoldBill = 0;
			curCurrentInterest = 0;
			FillGrid = 0;
			if (modUTFunctions.Statics.TownService != "S")
			{
				if (rsCL.Get_Fields_String("Service") != "S")
				{
					FillGrid = 1;
					// Fill Grid = 0 - Nothing, 1 - Water, 2 - Sewer, 3 - Water and Sewer
					lngWaterBills += 1;
					// Increment the number of bills
					CreateMasterLine_6(BNum, true);
					// This will create the original line for this bill
					curPrincipalDue = modUTFunctions.DetermineUTPrincipal_54(rsCL, rsLRW, bcodeW, true);
					curTaxDue = modUTFunctions.DetermineUTTax_54(rsCL, rsLRW, bcodeW, true);
					// these are the subordinate rows
					// check for a different owner and make a seperate row to show the old owner name
					if (!rsCL.Get_Fields_Boolean("WBillOwner"))
					{
						// MAL@20071022: Changed the way that the billed to name is pulled
						// Call Reference: 115027
						// If Trim(rsCL.Fields("Name2")) <> "" Then
						// strName = Trim$(rsCL.Fields("Name") & " & " & rsCL.Fields("Name2"))     'fill in the labels with the record information
						// Else
						// strName = Trim$(rsCL.Fields("Name"))
						// End If
						if (Strings.Trim(rsCL.Get_Fields_String("BName2")) != "")
						{
							strName = Strings.Trim(rsCL.Get_Fields_String("BName") + " & " + rsCL.Get_Fields_String("BName2"));
							// fill in the labels with the record information
						}
						else
						{
							strName = Strings.Trim(rsCL.Get_Fields_String("BName"));
						}
					}
					else
					{
						if (Strings.Trim(rsCL.Get_Fields_String("OName2")) != "")
						{
							strName = Strings.Trim(rsCL.Get_Fields_String("OName") + " & " + rsCL.Get_Fields_String("OName2"));
							// fill in the labels with the record information
						}
						else
						{
							strName = Strings.Trim(rsCL.Get_Fields_String("OName"));
						}
					}
					if (Strings.Trim(strName) != Strings.Trim(lblOwnersName.Text))
					{
						CurRowW += 1;
						Add_24(CurRowW, 1, true);
						WGRID.TextMatrix(CurRowW, lngGRIDColRef, "Billed To:");
                        //FC:FINAL:AM:#3781 - set the name only in one column
                        WGRID.TextMatrix(CurRowW, lngGRIDColPaymentCode, strName);
                        //WGRID.TextMatrix(CurRowW, lngGRIDColPaymentCode + 1, strName);
                        //WGRID.TextMatrix(CurRowW, lngGRIDColPaymentCode + 2, strName);
                        WGRID.TextMatrix(CurRowW, lngGRIDColPaymentCode + 3, strName);
                        //WGRID.TextMatrix(CurRowW, lngGRIDColPaymentCode + 4, strName);
                        //WGRID.TextMatrix(CurRowW, lngGRIDColPaymentCode + 5, strName);
                        //WGRID.TextMatrix(CurRowW, lngGRIDColPaymentCode + 6, strName);
                        WGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, CurRowW, lngGRIDColPaymentCode, CurRowW, lngGRIDColInterest, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                        //WGRID.MergeRow(CurRowW, true);
                        WGRID.MergeRow(CurRowW, true, lngGRIDColPaymentCode, lngGRIDColPaymentCode + 6);
                        WGRID.MergeRow(CurRowW, true, lngGRIDColPaymentCode + 3, lngGRIDColPaymentCode + 6);
                        WGRID.TextMatrix(CurRowW, lngGRIDColPending, " ");
					}
					// Original Bill Line in grid
					intHoldBill = rsCL.Get_Fields_Int32("BillNumber");
					// MAL@20071105
					if (bcodeW == "30DN")
					{
						WGRID.TextMatrix(ParentLineW, lngGRIDColRef, "Dmd #" + rsCL.Get_Fields_Int32("WDemandGroupID"));
						WGRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, ParentLineW, lngGRIDColRef, true);
					}
					else
					{
						WGRID.TextMatrix(ParentLineW, lngGRIDColRef, "Original");
					}
					WGRID.TextMatrix(ParentLineW, lngGRIDColPrincipal, FCConvert.ToString(rsCL.Get_Fields_Double("WPrinOwed")));
					WGRID.TextMatrix(ParentLineW, lngGRIDColTax, FCConvert.ToString(rsCL.Get_Fields_Double("WTaxOwed")));
					WGRID.TextMatrix(ParentLineW, lngGRIDColInterest, FCConvert.ToString(rsCL.Get_Fields_Double("WIntOwed")));
					WGRID.TextMatrix(ParentLineW, lngGRIDColCosts, FCConvert.ToString(rsCL.Get_Fields_Double("WCostOwed")));
					// MAL@20080128
					WGRID.TextMatrix(ParentLineW, lngGRIDColPTC, FCConvert.ToString(rsCL.Get_Fields_Double("WPrinOwed") + rsCL.Get_Fields_Double("WTaxOwed") + rsCL.Get_Fields_Double("WCostOwed")));
					WGRID.TextMatrix(ParentLineW, lngGRIDColTotal, FCConvert.ToString(Conversion.Val(WGRID.TextMatrix(ParentLineW, lngGRIDColPrincipal)) + Conversion.Val(WGRID.TextMatrix(ParentLineW, lngGRIDColTax)) + Conversion.Val(WGRID.TextMatrix(ParentLineW, lngGRIDColInterest)) + Conversion.Val(WGRID.TextMatrix(ParentLineW, lngGRIDColCosts))));
					WGRID.TextMatrix(ParentLineW, lngGRIDColLineCode, "+");
					if (bcodeW == "LIEN")
					{
						// if this record is a lien, then show all of the Pre Lien payments first without showing the Lien line or the Lien payments
						// TODO: Field [Bill] not found!! (maybe it is an alias?)
						if (rsCL.Get_Fields_Int32("Bill") == rsCL.Get_Fields_Int32("WCombinationLienKey"))
						{
							lngWaterBills += 1;
							// add an extra line for the lien
						}
						// TODO: Field [Bill] not found!! (maybe it is an alias?)
						if (PaymentRecords(rsCL.Get_Fields_Int32("Bill"), false, WGRID, true))
						{
						}
						else
						{
							if (boolNoCurrentInt)
							{
								// this means that the last payment was a "U" so no interest should accrue but that is why the function is coming out false
								boolTemp = true;
							}
							else
							{
								MessageBox.Show("An error has occured for Bill Number " + rsCL.Get_Fields_Int32("BillNumber") + ".", "Payment Record Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						CurRowW += 1;
						Add_24(CurRowW, 1, true);
						// total line
						WGRID.TextMatrix(CurRowW, lngGRIDColPending, " ");
						Add_26(CurRowW + 1, 1, true);
						// and spacer line to hold hidden values
						WGRID.TextMatrix(CurRowW + 1, lngGRIDColPending, " ");
						// subtotal format, bold and top border
						WGRID.TextMatrix(CurRowW, lngGRIDColLineCode, "=");
						WGRID.TextMatrix(CurRowW, lngGRIDColDate, "Lien #" + rsCL.Get_Fields_Int32("WLienRecordNumber"));
						// ReturnLienRateKey(rsCL.Fields("WLienRecordNumber"))
						WGRID.TextMatrix(CurRowW, lngGRIDColRef, WGRID.TextMatrix(CurRowW, lngGRIDColDate));
						WGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, CurRowW, lngGRIDColDate, CurRowW, lngGRIDColRef, FCGrid.AlignmentSettings.flexAlignRightCenter);
						WGRID.MergeRow(CurRowW, true, lngGRIDColRef, lngGRIDColRef);
						// put an asterisk on the main line
						// .TextMatrix(ParentLineS, lngGRIDColDate) = .TextMatrix(ParentLineS, lngGRIDColDate) & "*"
						WGRID.TextMatrix(ParentLineW, lngGRIDColRef, "Lien");
						WGRID.TextMatrix(CurRowW, lngGRIDColPrincipal, "0.00");
						WGRID.TextMatrix(CurRowW, lngGRIDColPTC, "0.00");
						WGRID.TextMatrix(CurRowW, lngGRIDColTax, " 0.00");
						WGRID.TextMatrix(CurRowW, lngGRIDColInterest, "  0.00");
						WGRID.TextMatrix(CurRowW, lngGRIDColCosts, "   0.00");
						WGRID.TextMatrix(CurRowW, lngGRIDColTotal, "    0.00");
						WGRID.TextMatrix(CurRowW, lngGRIDColLineCode, "=");
						CurRowW += 1;
						// fill the spacer line as well (no calculation needed)
						WGRID.TextMatrix(CurRowW, lngGRIDColPrincipal, "0.00");
						WGRID.TextMatrix(CurRowW, lngGRIDColPTC, "0.00");
						WGRID.TextMatrix(CurRowW, lngGRIDColTax, "0.00");
						WGRID.TextMatrix(CurRowW, lngGRIDColInterest, "0.00");
						WGRID.TextMatrix(CurRowW, lngGRIDColCosts, "0.00");
						WGRID.TextMatrix(CurRowW, lngGRIDColTotal, "0.00");
						WGRID.TextMatrix(CurRowW, lngGRIDColDate, WGRID.TextMatrix(ParentLineW, lngGRIDColDate));
						WGRID.TextMatrix(CurRowW, lngGRIDColPaymentKey, WGRID.TextMatrix(ParentLineW, lngGRIDColPaymentKey));
						// MAL@20071105
						// If .TextMatrix(ParentLineW, lngGRIDColRef) = "Original" Or Left(.TextMatrix(ParentLineW, lngGRIDColRef), 4) = Left(strSupplemntalBillString, 4) Then
						if (WGRID.TextMatrix(ParentLineW, lngGRIDColRef) == "Original" || Strings.Left(WGRID.TextMatrix(ParentLineW, lngGRIDColRef), 3) == Strings.Left(strDemandBillString, 3) || Strings.Left(WGRID.TextMatrix(ParentLineW, lngGRIDColRef), 4) == Strings.Left(strSupplemntalBillString, 4))
						{
							SwapRows_8(ParentLineW, NextSpacerLine_6(ParentLineW, true), WGRID);
						}
						ParentLineW = WGRID.Rows - 1;
						// TODO: Field [Bill] not found!! (maybe it is an alias?)
						if (rsCL.Get_Fields_Int32("Bill") == rsCL.Get_Fields_Int32("WCombinationLienKey"))
						{
							// add the lien master line and the liened payments
							if (PaymentRecords(rsCL.Get_Fields_Int32("WLienRecordNumber"), true, WGRID, true))
							{
								// this writes all of the payment records
								// current interest line (if Needed)
								if (!boolNoCurrentInt)
								{
									boolNoCurrentInt = false;
									dblTotalDue = modUTCalculations.CalculateAccountUTLien(rsLRW, modUTFunctions.Statics.UTEffectiveDate, ref curCurrentInterest, true, 0, 0, 0, false, false, dateTemp, dblPerDiem);
								}
								// rsCL is a record from the Bill table
								// rsLien is a record from the LienRec table if needed
								WGRID.TextMatrix(ParentLineW, lngGRIDColPerDiem, FCConvert.ToString(dblPerDiem));
								dblTotalPerDiem += dblPerDiem;
								dblPerDiem = 0;
								modUTFunctions.Statics.dateUTOldDate[intHoldBill, 0] = dateTemp;
								// holds that last Interest Date in the grid so that the last payment can be reversed
							}
							else
							{
								// current interest line (if Needed)
								if (!boolNoCurrentInt)
								{
									boolNoCurrentInt = false;
									dblTotalDue = modUTCalculations.CalculateAccountUTLien(rsLRW, modUTFunctions.Statics.UTEffectiveDate, ref curCurrentInterest, true, 0, 0, 0, false, false, dateTemp, dblPerDiem);
								}
								WGRID.TextMatrix(ParentLineW, lngGRIDColPerDiem, FCConvert.ToString(dblPerDiem));
								dblTotalPerDiem += dblPerDiem;
								dblPerDiem = 0;
							}
						}
					}
					else
					{
						// this will show the non liened payments
						if (PaymentRecords(BNum, boolTemp, WGRID, true))
						{
							// this writes all of the payment records
							// current interest line (if Needed)
							if (!boolNoCurrentInt)
							{
								boolNoCurrentInt = false;
								curCurrentInterest = 0;
								dblTotalDue = modUTCalculations.CalculateAccountUT(rsCL, ref modUTFunctions.Statics.UTEffectiveDate, ref curCurrentInterest, true, ref dateTemp, ref dblPerDiem);
							}
							// rsCL is a record from the Bill table
							// rsLien is a record from the LienRec table if needed
							WGRID.TextMatrix(ParentLineW, lngGRIDColPerDiem, FCConvert.ToString(dblPerDiem));
							dblTotalPerDiem += dblPerDiem;
							dblPerDiem = 0;
							modUTFunctions.Statics.dateUTOldDate[intHoldBill, 0] = dateTemp;
							// holds that last Interest Date in the grid so that the last payment can be reversed
						}
						else
						{
							// current interest line (if Needed)
							if (!boolNoCurrentInt)
							{
								boolNoCurrentInt = false;
								curCurrentInterest = 0;
								dblTotalDue = modUTCalculations.CalculateAccountUT(rsCL, ref modUTFunctions.Statics.UTEffectiveDate, ref curCurrentInterest, true, ref dateTemp, ref dblPerDiem);
							}
							WGRID.TextMatrix(ParentLineW, lngGRIDColPerDiem, FCConvert.ToString(dblPerDiem));
							dblTotalPerDiem += dblPerDiem;
							dblPerDiem = 0;
						}
						// MAL@20071102: Add asterisk to line for 30DN Group
						if (bcodeW == "30DN")
						{
							WGRID.TextMatrix(ParentLineW, lngGRIDColDate, WGRID.TextMatrix(ParentLineW, lngGRIDColDate) + "**");
						}
					}
					// kgk trout-722  Show current interest on right click for all bills that are combined on Lien
					// TODO: Field [Bill] not found!! (maybe it is an alias?)
					if (bcodeW == "LIEN" && rsCL.Get_Fields_Int32("Bill") != rsCL.Get_Fields_Int32("WCombinationLienKey"))
					{
						// current interest line (if Needed)
						if (!boolNoCurrentInt)
						{
							boolNoCurrentInt = false;
							modUTCalculations.CalculateAccountUTLien(rsLRW, modUTFunctions.Statics.UTEffectiveDate, ref curCurrentInterest, false, 0, 0, 0, false, false, dateTemp, dblPerDiem);
							modUTFunctions.Statics.dblUTCurrentInt[intHoldBill, 1] = -1 * curCurrentInterest;
							// this holds the current interest totals for later use
							curCurrentInterest = 0;
						}
					}
					else
					{
						modUTFunctions.Statics.dblUTCurrentInt[intHoldBill, 0] = -1 * curCurrentInterest;
						// this holds the current interest totals for later use
					}
					if (curCurrentInterest != 0)
					{
						CurRowW += 1;
						Add_24(CurRowW, 1, true);
						curCurrentInterest *= -1;
						WGRID.TextMatrix(CurRowW, lngGRIDCOLYear, "");
						WGRID.TextMatrix(CurRowW, lngGRIDColBillNumber, "");
						WGRID.TextMatrix(CurRowW, lngGRIDColDate, "");
						WGRID.TextMatrix(CurRowW, lngGRIDColPending, " ");
						if (curCurrentInterest < 0)
						{
							WGRID.TextMatrix(CurRowW, lngGRIDColRef, "CURINT");
						}
						else
						{
							WGRID.TextMatrix(CurRowW, lngGRIDColRef, "EARNINT");
						}
						WGRID.TextMatrix(CurRowW, lngGRIDColPrincipal, FCConvert.ToString(0));
						WGRID.TextMatrix(CurRowW, lngGRIDColPTC, FCConvert.ToString(0));
						WGRID.TextMatrix(CurRowW, lngGRIDColTax, FCConvert.ToString(0));
						WGRID.TextMatrix(CurRowW, lngGRIDColInterest, FCConvert.ToString(curCurrentInterest));
						WGRID.TextMatrix(CurRowW, lngGRIDColCosts, FCConvert.ToString(0));
						WGRID.TextMatrix(CurRowW, lngGRIDColTotal, FCConvert.ToString((FCConvert.ToDecimal(WGRID.TextMatrix(CurRowW, lngGRIDColPrincipal)) + FCConvert.ToDecimal(WGRID.TextMatrix(CurRowW, lngGRIDColTax)) + FCConvert.ToDecimal(WGRID.TextMatrix(CurRowW, lngGRIDColInterest)) + FCConvert.ToDecimal(WGRID.TextMatrix(CurRowW, lngGRIDColCosts)))));
						WGRID.TextMatrix(CurRowW, lngGRIDColLineCode, "-");
						// this is used to sum the cols - is for an increase in the bill amount...it is adding to the bill
					}
					// TODO: Field [Bill] not found!! (maybe it is an alias?)
					if (bcodeW != "LIEN" || (bcodeW == "LIEN" && rsCL.Get_Fields_Int32("Bill") == rsCL.Get_Fields_Int32("WCombinationLienKey")))
					{
						// subtotals line - this will be shown on the master line when this line is not shown
						CurRowW += 1;
						Add_24(CurRowW, 1, true);
						// total line
						WGRID.TextMatrix(CurRowW, lngGRIDColPending, " ");
						Add_26(CurRowW + 1, 1, true);
						// and spacer line to hold hidden values
						WGRID.TextMatrix(CurRowW + 1, lngGRIDColPending, " ");
						// subtotal format, bold and top border
						WGRID.TextMatrix(CurRowW, lngGRIDColLineCode, "=");
						WGRID.TextMatrix(CurRowW, lngGRIDColRef, "Total");
						l = LastParentRow_6(CurRowW, true);
						for (j = lngGRIDColPrincipal; j <= lngGRIDColTotal; j++)
						{
							sum = 0;
							for (sumRow = l; sumRow <= CurRowW - 1; sumRow++)
							{
								//Application.DoEvents();
								if (Strings.Trim(WGRID.TextMatrix(sumRow, j)) != "")
								{
									if (WGRID.TextMatrix(sumRow, lngGRIDColLineCode) == "+")
									{
										sum = modGlobal.Round(sum + modGlobal.Round(FCConvert.ToDouble(WGRID.TextMatrix(sumRow, j)), 2), 2);
									}
									else if (WGRID.TextMatrix(sumRow, lngGRIDColLineCode) == "-")
									{
										sum = modGlobal.Round(sum - modGlobal.Round(FCConvert.ToDouble(WGRID.TextMatrix(sumRow, j)), 2), 2);
									}
									else if (FCConvert.ToBoolean(WGRID.TextMatrix(sumRow, lngGRIDColLineCode)) == true)
									{
										sum = modGlobal.Round(FCConvert.ToDouble(WGRID.TextMatrix(sumRow, j)), 2);
									}
								}
							}
							WGRID.TextMatrix(CurRowW, j, FCConvert.ToString(sum));
							WGRID.TextMatrix(CurRowW + 1, j, FCConvert.ToString(sum));
						}
						// spacer line
						CurRowW += 1;
						WGRID.TextMatrix(CurRowW, lngGRIDColDate, WGRID.TextMatrix(ParentLineW, lngGRIDColDate));
						// .TextMatrix(CurRowW, lngGRIDColRef) = .TextMatrix(ParentLineW, lngGRIDColRef)
						if (bcodeW == "LIEN")
						{
							WGRID.TextMatrix(CurRowW, lngGRIDColPaymentKey, FCConvert.ToString(BNum * -1));
							WGRID.TextMatrix(CurRowW, lngGridColBillKey, FCConvert.ToString(BNum * -1));
						}
						else
						{
							WGRID.TextMatrix(CurRowW, lngGRIDColPaymentKey, FCConvert.ToString(BNum));
							WGRID.TextMatrix(CurRowW, lngGridColBillKey, FCConvert.ToString(BNum));
						}
						// puts the totals in the header line to start
						// If .TextMatrix(ParentLineW, lngGRIDColRef) = "Original" Or Left(.TextMatrix(ParentLineW, lngGRIDColRef), 4) = Left(strSupplemntalBillString, 4) Then
						if (WGRID.TextMatrix(ParentLineW, lngGRIDColRef) == "Original" || Strings.Left(WGRID.TextMatrix(ParentLineW, lngGRIDColRef), 3) == Strings.Left(strDemandBillString, 3) || Strings.Left(WGRID.TextMatrix(ParentLineW, lngGRIDColRef), 4) == Strings.Left(strSupplemntalBillString, 4))
						{
							SwapRows_8(ParentLineW, NextSpacerLine_6(ParentLineW, true), WGRID);
						}
						else if (Strings.Left(WGRID.TextMatrix(ParentLineW, lngGRIDColRef), 4) == Strings.Left(strSupplemntalBillString, 4))
						{
						}
					}
				}
				WGRID.Select(0, 0);
			}
			else
			{
				WGRID.Visible = false;
			}
			if (modUTFunctions.Statics.TownService != "W")
			{
				if (rsCL.Get_Fields_String("Service") != "W")
				{
					FillGrid += 2;
					// Fill Grid = 0 - Nothing, 1 - Water, 2 - Sewer, 3 - Water and Sewer
					lngSewerBills += 1;
					// Increment the number of bills
					CreateMasterLine_6(BNum, false);
					// This will create the original line for this bill
					curPrincipalDue = modUTFunctions.DetermineUTPrincipal_54(rsCL, rsLRS, bcodeS, false);
					curTaxDue = modUTFunctions.DetermineUTTax_54(rsCL, rsLRS, bcodeS, false);
					// these are the subordinate rows
					// check for a different owner and make a seperate row to show the old owner name
					if (!rsCL.Get_Fields_Boolean("SBillOwner"))
					{
						// MAL@20071022: Changed the way the billed to name is pulled into the grid
						// Call Reference: 115027
						// If Trim(rsCL.Fields("Name2")) <> "" Then
						// strName = Trim$(rsCL.Fields("Name") & " & " & rsCL.Fields("Name2"))     'fill in the labels with the record information
						// Else
						// strName = Trim$(rsCL.Fields("Name"))
						// End If
						if (Strings.Trim(rsCL.Get_Fields_String("BName2")) != "")
						{
							strName = Strings.Trim(rsCL.Get_Fields_String("BName") + " & " + rsCL.Get_Fields_String("BName2"));
							// fill in the labels with the record information
						}
						else
						{
							strName = Strings.Trim(rsCL.Get_Fields_String("BName"));
						}
					}
					else
					{
						if (Strings.Trim(rsCL.Get_Fields_String("OName2")) != "")
						{
							strName = Strings.Trim(rsCL.Get_Fields_String("OName") + " & " + rsCL.Get_Fields_String("OName2"));
							// fill in the labels with the record information
						}
						else
						{
							strName = Strings.Trim(rsCL.Get_Fields_String("OName"));
						}
					}
					if (Strings.Trim(strName) != Strings.Trim(lblOwnersName.Text))
					{
						CurRowS += 1;
						Add_24(CurRowS, 1, false);
						SGRID.TextMatrix(CurRowS, lngGRIDColRef, "Billed To:");
                        //FC:FINAL:AM:#3781 - set the name only in one column
                        SGRID.TextMatrix(CurRowS, lngGRIDColPaymentCode, strName);
                        //SGRID.TextMatrix(CurRowS, lngGRIDColPaymentCode + 1, strName);
                        //SGRID.TextMatrix(CurRowS, lngGRIDColPaymentCode + 2, strName);
                        SGRID.TextMatrix(CurRowS, lngGRIDColPaymentCode + 3, strName);
                        //SGRID.TextMatrix(CurRowS, lngGRIDColPaymentCode + 4, strName);
                        //SGRID.TextMatrix(CurRowS, lngGRIDColPaymentCode + 5, strName);
                        //SGRID.TextMatrix(CurRowS, lngGRIDColPaymentCode + 6, strName);
                        SGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, CurRowS, lngGRIDColPaymentCode, CurRowS, lngGRIDColInterest, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						//SGRID.MergeRow(CurRowS, true);
						//FC:FINAL:VGE - #i895 Merging 'BilledTo' cells
						SGRID.MergeRow(CurRowS, true, lngGRIDColPaymentCode, lngGRIDColPaymentCode + 6);
                        SGRID.MergeRow(CurRowS, true, lngGRIDColPaymentCode + 3, lngGRIDColPaymentCode + 6);
                        SGRID.TextMatrix(CurRowS, lngGRIDColPending, " ");
					}
					// Original Bill Line in grid
					intHoldBill = rsCL.Get_Fields_Int32("BillNumber");
					// MAL@20071105
					if (bcodeS == "30DN")
					{
						SGRID.TextMatrix(ParentLineS, lngGRIDColRef, "Dmd #" + rsCL.Get_Fields_Int32("SDemandGroupID"));
						SGRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, ParentLineS, lngGRIDColRef, true);
					}
					else
					{
						SGRID.TextMatrix(ParentLineS, lngGRIDColRef, "Original");
					}
					SGRID.TextMatrix(ParentLineS, lngGRIDColPrincipal, FCConvert.ToString(rsCL.Get_Fields_Double("SPrinOwed")));
					SGRID.TextMatrix(ParentLineS, lngGRIDColTax, FCConvert.ToString(rsCL.Get_Fields_Double("STaxOwed")));
					SGRID.TextMatrix(ParentLineS, lngGRIDColInterest, FCConvert.ToString(rsCL.Get_Fields_Double("SIntOwed")));
					SGRID.TextMatrix(ParentLineS, lngGRIDColCosts, FCConvert.ToString(rsCL.Get_Fields_Double("SCostOwed")));
					SGRID.TextMatrix(ParentLineS, lngGRIDColPTC, FCConvert.ToString(rsCL.Get_Fields_Double("SPrinOwed") + rsCL.Get_Fields_Double("STaxOwed") + rsCL.Get_Fields_Double("SCostOwed")));
					SGRID.TextMatrix(ParentLineS, lngGRIDColTotal, FCConvert.ToString(Conversion.Val(SGRID.TextMatrix(ParentLineS, lngGRIDColPrincipal)) + Conversion.Val(SGRID.TextMatrix(ParentLineS, lngGRIDColTax)) + Conversion.Val(SGRID.TextMatrix(ParentLineS, lngGRIDColInterest)) + Conversion.Val(SGRID.TextMatrix(ParentLineS, lngGRIDColCosts))));
					SGRID.TextMatrix(ParentLineS, lngGRIDColLineCode, "+");
					if (bcodeS == "LIEN")
					{
						// if this record is a lien, then show all of the Pre Lien payments first without showing the Lien line or the Lien payments
						// TODO: Field [Bill] not found!! (maybe it is an alias?)
						if (rsCL.Get_Fields_Int32("Bill") == rsCL.Get_Fields_Int32("SCombinationLienKey"))
						{
							lngSewerBills += 1;
							// add an extra line for the lien
						}
						// TODO: Field [Bill] not found!! (maybe it is an alias?)
						if (PaymentRecords(rsCL.Get_Fields_Int32("Bill"), false, SGRID, false))
						{
						}
						else
						{
							if (boolNoCurrentInt)
							{
								// this means that the last payment was a "U" so no interest should accrue but that is why the function is coming out false
								boolTemp = true;
							}
							else
							{
								MessageBox.Show("An error has occured for Bill Number " + rsCL.Get_Fields_Int32("BillNumber") + ".", "Payment Record Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
						}
						CurRowS += 1;
						Add_24(CurRowS, 1, false);
						// total line
						SGRID.TextMatrix(CurRowS, lngGRIDColPending, " ");
						Add_26(CurRowS + 1, 1, false);
						// and spacer line to hold hidden values
						SGRID.TextMatrix(CurRowS + 1, lngGRIDColPending, " ");
						// subtotal format, bold and top border
						SGRID.TextMatrix(CurRowS, lngGRIDColLineCode, "=");
						SGRID.TextMatrix(CurRowS, lngGRIDColDate, "Lien #" + rsCL.Get_Fields_Int32("SLienRecordNumber"));
						SGRID.TextMatrix(CurRowS, lngGRIDColRef, SGRID.TextMatrix(CurRowS, lngGRIDColDate));
						SGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, CurRowS, lngGRIDColDate, CurRowS, lngGRIDColRef, FCGrid.AlignmentSettings.flexAlignRightCenter);
						SGRID.MergeRow(CurRowS, true, lngGRIDColDate, lngGRIDColRef);
						// put an asterisk on the main line
						// .TextMatrix(ParentLineS, lngGRIDColDate) = .TextMatrix(ParentLineS, lngGRIDColDate) & "*"
						SGRID.TextMatrix(ParentLineS, lngGRIDColRef, "Lien");
						SGRID.TextMatrix(CurRowS, lngGRIDColPrincipal, "0.00");
						SGRID.TextMatrix(CurRowS, lngGRIDColPTC, "0.00");
						SGRID.TextMatrix(CurRowS, lngGRIDColTax, " 0.00");
						SGRID.TextMatrix(CurRowS, lngGRIDColInterest, "  0.00");
						SGRID.TextMatrix(CurRowS, lngGRIDColCosts, "   0.00");
						SGRID.TextMatrix(CurRowS, lngGRIDColTotal, "    0.00");
						SGRID.TextMatrix(CurRowS, lngGRIDColLineCode, "=");
						CurRowS += 1;
						// fill the spacer line as well (no calculation needed)
						SGRID.TextMatrix(CurRowS, lngGRIDColPrincipal, "0.00");
						SGRID.TextMatrix(CurRowS, lngGRIDColPTC, "0.00");
						SGRID.TextMatrix(CurRowS, lngGRIDColTax, "0.00");
						SGRID.TextMatrix(CurRowS, lngGRIDColInterest, "0.00");
						SGRID.TextMatrix(CurRowS, lngGRIDColCosts, "0.00");
						SGRID.TextMatrix(CurRowS, lngGRIDColTotal, "0.00");
						SGRID.TextMatrix(CurRowS, lngGRIDColDate, SGRID.TextMatrix(ParentLineS, lngGRIDColDate));
						SGRID.TextMatrix(CurRowS, lngGRIDColPaymentKey, SGRID.TextMatrix(ParentLineS, lngGRIDColPaymentKey));
						// MAL@20071105
						// If .TextMatrix(ParentLineS, lngGRIDColRef) = "Original" Or Left(.TextMatrix(ParentLineS, lngGRIDColRef), 4) = Left(strSupplemntalBillString, 4) Then
						if (SGRID.TextMatrix(ParentLineS, lngGRIDColRef) == "Original" || Strings.Left(SGRID.TextMatrix(ParentLineS, lngGRIDColRef), 3) == Strings.Left(strDemandBillString, 3) || Strings.Left(SGRID.TextMatrix(ParentLineS, lngGRIDColRef), 4) == Strings.Left(strSupplemntalBillString, 4))
						{
							SwapRows_8(ParentLineS, NextSpacerLine_6(ParentLineS, false), SGRID);
						}
						ParentLineS = SGRID.Rows - 1;
						// TODO: Field [Bill] not found!! (maybe it is an alias?)
						if (rsCL.Get_Fields_Int32("Bill") == rsCL.Get_Fields_Int32("SCombinationLienKey"))
						{
							// add the lien master line and the liened payments
							if (PaymentRecords(rsCL.Get_Fields_Int32("SLienRecordNumber"), true, SGRID, false))
							{
								// this writes all of the payment records
								// current interest line (if Needed)
								if (!boolNoCurrentInt)
								{
									boolNoCurrentInt = false;
									dblTotalDue = modUTCalculations.CalculateAccountUTLien(rsLRS, modUTFunctions.Statics.UTEffectiveDate, ref curCurrentInterest, false, 0, 0, 0, false, false, dateTemp, dblPerDiem);
								}
								// rsCL is a record from the Bill table
								// rsLien is a record from the LienRec table if needed
								SGRID.TextMatrix(ParentLineS, lngGRIDColPerDiem, FCConvert.ToString(dblPerDiem));
								dblTotalPerDiem += dblPerDiem;
								dblPerDiem = 0;
								modUTFunctions.Statics.dateUTOldDate[intHoldBill, 0] = dateTemp;
								// holds that last Interest Date in the grid so that the last payment can be reversed
							}
							else
							{
								// current interest line (if Needed)
								if (!boolNoCurrentInt)
								{
									boolNoCurrentInt = false;
									dblTotalDue = modUTCalculations.CalculateAccountUTLien(rsLRS, modUTFunctions.Statics.UTEffectiveDate, ref curCurrentInterest, false, 0, 0, 0, false, false, dateTemp, dblPerDiem);
								}
								SGRID.TextMatrix(ParentLineS, lngGRIDColPerDiem, FCConvert.ToString(dblPerDiem));
								dblTotalPerDiem += dblPerDiem;
								dblPerDiem = 0;
							}
						}
					}
					else
					{
						// this will show the non liened payments
						if (PaymentRecords(BNum, boolTemp, SGRID, false))
						{
							// this writes all of the payment records
							// current interest line (if Needed)
							if (!boolNoCurrentInt)
							{
								boolNoCurrentInt = false;
								curCurrentInterest = 0;
								dblTotalDue = modUTCalculations.CalculateAccountUT(rsCL, ref modUTFunctions.Statics.UTEffectiveDate, ref curCurrentInterest, false, ref dateTemp, ref dblPerDiem);
							}
							// rsCL is a record from the Bill table
							// rsLien is a record from the LienRec table if needed
							SGRID.TextMatrix(ParentLineS, lngGRIDColPerDiem, FCConvert.ToString(dblPerDiem));
							dblTotalPerDiem += dblPerDiem;
							dblPerDiem = 0;
							modUTFunctions.Statics.dateUTOldDate[intHoldBill, 0] = dateTemp;
							// holds that last Interest Date in the grid so that the last payment can be reversed
						}
						else
						{
							// current interest line (if Needed)
							if (!boolNoCurrentInt)
							{
								boolNoCurrentInt = false;
								curCurrentInterest = 0;
								dblTotalDue = modUTCalculations.CalculateAccountUT(rsCL, ref modUTFunctions.Statics.UTEffectiveDate, ref curCurrentInterest, false, ref dateTemp, ref dblPerDiem);
							}
							SGRID.TextMatrix(ParentLineS, lngGRIDColPerDiem, FCConvert.ToString(dblPerDiem));
							dblTotalPerDiem += dblPerDiem;
							dblPerDiem = 0;
						}
						// MAL@20071102
						if (bcodeS == "30DN")
						{
							SGRID.TextMatrix(ParentLineS, lngGRIDColDate, SGRID.TextMatrix(ParentLineS, lngGRIDColDate) + "**");
						}
					}
					// kgk trout-722  Show current interest on right click for all bills that are combined on Lien
					// TODO: Field [Bill] not found!! (maybe it is an alias?)
					if (bcodeS == "LIEN" && rsCL.Get_Fields_Int32("Bill") != rsCL.Get_Fields_Int32("SCombinationLienKey"))
					{
						// current interest line (if Needed)
						if (!boolNoCurrentInt)
						{
							boolNoCurrentInt = false;
							modUTCalculations.CalculateAccountUTLien(rsLRS, modUTFunctions.Statics.UTEffectiveDate, ref curCurrentInterest, false, 0, 0, 0, false, false, dateTemp, dblPerDiem);
							modUTFunctions.Statics.dblUTCurrentInt[intHoldBill, 1] = -1 * curCurrentInterest;
							// this holds the current interest totals for later use
							curCurrentInterest = 0;
						}
					}
					else
					{
						modUTFunctions.Statics.dblUTCurrentInt[intHoldBill, 1] = -1 * curCurrentInterest;
						// this holds the current interest totals for later use
					}
					if (curCurrentInterest != 0)
					{
						CurRowS += 1;
						Add_24(CurRowS, 1, false);
						curCurrentInterest *= -1;
						SGRID.TextMatrix(CurRowS, lngGRIDCOLYear, "");
						SGRID.TextMatrix(CurRowS, lngGRIDColBillNumber, "");
						SGRID.TextMatrix(CurRowS, lngGRIDColDate, "");
						SGRID.TextMatrix(CurRowS, lngGRIDColPending, " ");
						if (curCurrentInterest < 0)
						{
							SGRID.TextMatrix(CurRowS, lngGRIDColRef, "CURINT");
						}
						else
						{
							SGRID.TextMatrix(CurRowS, lngGRIDColRef, "EARNINT");
						}
						SGRID.TextMatrix(CurRowS, lngGRIDColPrincipal, FCConvert.ToString(0));
						SGRID.TextMatrix(CurRowS, lngGRIDColPTC, FCConvert.ToString(0));
						SGRID.TextMatrix(CurRowS, lngGRIDColTax, FCConvert.ToString(0));
						SGRID.TextMatrix(CurRowS, lngGRIDColInterest, FCConvert.ToString(curCurrentInterest));
						SGRID.TextMatrix(CurRowS, lngGRIDColCosts, FCConvert.ToString(0));
						SGRID.TextMatrix(CurRowS, lngGRIDColTotal, FCConvert.ToString((FCConvert.ToDecimal(SGRID.TextMatrix(CurRowS, lngGRIDColPrincipal)) + FCConvert.ToDecimal(SGRID.TextMatrix(CurRowS, lngGRIDColTax)) + FCConvert.ToDecimal(SGRID.TextMatrix(CurRowS, lngGRIDColInterest)) + FCConvert.ToDecimal(SGRID.TextMatrix(CurRowS, lngGRIDColCosts)))));
						SGRID.TextMatrix(CurRowS, lngGRIDColLineCode, "-");
						// this is used to sum the cols - is for an increase in the bill amount...it is adding to the bill
					}
					// TODO: Field [Bill] not found!! (maybe it is an alias?)
					if (bcodeS != "LIEN" || (bcodeS == "LIEN" && rsCL.Get_Fields_Int32("Bill") == rsCL.Get_Fields_Int32("SCombinationLienKey")))
					{
						// subtotals line - this will be shown on the master line when this line is not shown
						CurRowS += 1;
						Add_24(CurRowS, 1, false);
						// total line
						SGRID.TextMatrix(CurRowS, lngGRIDColPending, " ");
						Add_26(CurRowS + 1, 1, false);
						// and spacer line to hold hidden values
						SGRID.TextMatrix(CurRowS + 1, lngGRIDColPending, " ");
						// subtotal format, bold and top border
						SGRID.TextMatrix(CurRowS, lngGRIDColLineCode, "=");
						SGRID.TextMatrix(CurRowS, lngGRIDColRef, "Total");
						l = LastParentRow_6(CurRowS, false);
						for (j = lngGRIDColPrincipal; j <= lngGRIDColTotal; j++)
						{
							sum = 0;
							for (sumRow = l; sumRow <= CurRowS - 1; sumRow++)
							{
								//Application.DoEvents();
								if (Strings.Trim(SGRID.TextMatrix(sumRow, j)) != "")
								{
									if (SGRID.TextMatrix(sumRow, lngGRIDColLineCode) == "+")
									{
										sum = modGlobal.Round(sum + modGlobal.Round(FCConvert.ToDouble(SGRID.TextMatrix(sumRow, j)), 2), 2);
									}
									else if (SGRID.TextMatrix(sumRow, lngGRIDColLineCode) == "-")
									{
										sum = modGlobal.Round(sum - modGlobal.Round(FCConvert.ToDouble(SGRID.TextMatrix(sumRow, j)), 2), 2);
									}
									else if (FCConvert.ToBoolean(SGRID.TextMatrix(sumRow, lngGRIDColLineCode)) == true)
									{
										sum = modGlobal.Round(FCConvert.ToDouble(SGRID.TextMatrix(sumRow, j)), 2);
									}
								}
							}
							SGRID.TextMatrix(CurRowS, j, FCConvert.ToString(sum));
							SGRID.TextMatrix(CurRowS + 1, j, FCConvert.ToString(sum));
						}
						// spacer line
						CurRowS += 1;
						SGRID.TextMatrix(CurRowS, lngGRIDColDate, SGRID.TextMatrix(ParentLineS, lngGRIDColDate));
						// .TextMatrix(CurRowS, lngGRIDColRef) = .TextMatrix(ParentLineS, lngGRIDColRef)
						if (bcodeS == "LIEN")
						{
							SGRID.TextMatrix(CurRowS, lngGRIDColPaymentKey, FCConvert.ToString(BNum * -1));
							SGRID.TextMatrix(CurRowS, lngGridColBillKey, FCConvert.ToString(BNum * -1));
						}
						else
						{
							SGRID.TextMatrix(CurRowS, lngGRIDColPaymentKey, FCConvert.ToString(BNum));
							SGRID.TextMatrix(CurRowS, lngGridColBillKey, FCConvert.ToString(BNum));
						}
						// puts the totals in the header line to start
						// MAL@20071105
						// If .TextMatrix(ParentLineS, lngGRIDColRef) = "Original" Or Left(.TextMatrix(ParentLineS, lngGRIDColRef), 4) = Left(strSupplemntalBillString, 4) Then
						if (SGRID.TextMatrix(ParentLineS, lngGRIDColRef) == "Original" || Strings.Left(SGRID.TextMatrix(ParentLineS, lngGRIDColRef), 3) == Strings.Left(strDemandBillString, 3) || Strings.Left(SGRID.TextMatrix(ParentLineS, lngGRIDColRef), 4) == Strings.Left(strSupplemntalBillString, 4))
						{
							SwapRows_8(ParentLineS, NextSpacerLine_6(ParentLineS, false), SGRID);
						}
						else if (Strings.Left(SGRID.TextMatrix(ParentLineW, lngGRIDColRef), 4) == Strings.Left(strSupplemntalBillString, 4))
						{
						}
					}
				}
				SGRID.Select(0, 0);
			}
			else
			{
				SGRID.Visible = false;
			}
            modColorScheme.ColorGrid(WGRID);
            modColorScheme.ColorGrid(SGRID);
            return FillGrid;
		}

		private bool CompareCol(ref double GRID_Total, ref short GRID_Col, ref short intNum)
		{
			bool CompareCol = false;
			// this checks to see if the grid totals are the same as the recordset totals
			double dblTemp = 0;
			clsDRWrapper rsLTemp = null;
			clsDRWrapper rsTemp;
			rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(CurrentAccountKey) + " AND BillNumber = " + FCConvert.ToString(intNum), modExtraModules.strUTDatabase);
			if (rsTemp.Get_Fields_Int32("LienRecordNumber") == 0)
			{
				// these are not liened
				// XXXX THIS ISN'T CALLED ANYWHERE, THESE ARE FIELDS IN THE CL BILLINGMASTER TABLE!  XXXX
				switch (GRID_Col)
				{
					case 6:
						{
							// principal line
							dblTemp = modGlobal.Round((Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4"))) - Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid")), 2);
							break;
						}
					case 7:
						{
							// interest
							dblTemp = modGlobal.Round((-1 * Conversion.Val(rsTemp.Get_Fields_Decimal("InterestCharged"))) - Conversion.Val(rsTemp.Get_Fields_Decimal("InterestPaid")) - curCurrentInterest, 2);
							break;
						}
					case 8:
						{
							// costs
							dblTemp = modGlobal.Round(Conversion.Val(rsTemp.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsTemp.Get_Fields_Decimal("DemandFeesPaid")), 2);
							break;
						}
					case 9:
						{
							// totals
							dblTemp = modGlobal.Round(((Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsTemp.Get_Fields_Decimal("TaxDue4"))) - Conversion.Val(rsTemp.Get_Fields_Decimal("PrincipalPaid"))) + (-1 * (Conversion.Val(rsTemp.Get_Fields_Decimal("InterestCharged"))) - Conversion.Val(rsTemp.Get_Fields_Decimal("InterestPaid")) - curCurrentInterest) + (Conversion.Val(rsTemp.Get_Fields_Decimal("DemandFees")) - Conversion.Val(rsTemp.Get_Fields_Decimal("DemandFeesPaid"))), 2);
							break;
						}
				}
				//end switch
				// If yr = 1996 Then Stop
				if (Conversion.Val(GRID_Total) == Conversion.Val(dblTemp))
				{
					CompareCol = true;
				}
			}
			else
			{
				rsLTemp = new clsDRWrapper();
				rsLTemp.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + rsTemp.Get_Fields_Int32("LienRecordNumber"), modExtraModules.strUTDatabase);
				// these are liened
				switch (GRID_Col)
				{
					case 6:
						{
							// principal line
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							dblTemp = modGlobal.Round(Conversion.Val(rsLTemp.Get_Fields("Principal")) - Conversion.Val(rsLTemp.Get_Fields_Decimal("PrincipalPaid")), 2);
							break;
						}
					case 7:
						{
							// interest
							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
							dblTemp = modGlobal.Round(Conversion.Val(rsLTemp.Get_Fields("Interest")) - Conversion.Val(rsLTemp.Get_Fields_Decimal("InterestCharged")) - Conversion.Val(rsLTemp.Get_Fields_Decimal("InterestPaid")) - curCurrentInterest, 2);
							break;
						}
					case 8:
						{
							// costs
							// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
							dblTemp = modGlobal.Round(Conversion.Val(rsLTemp.Get_Fields("Costs")) - Conversion.Val(rsLTemp.Get_Fields("MaturityFee")) - Conversion.Val(rsLTemp.Get_Fields_Decimal("CostsPaid")), 2);
							break;
						}
					case 9:
						{
							// totals
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
							dblTemp = modGlobal.Round((Conversion.Val(rsLTemp.Get_Fields("Principal")) + Conversion.Val(rsLTemp.Get_Fields("Interest")) - (Conversion.Val(rsLTemp.Get_Fields_Decimal("InterestCharged")) + Conversion.Val(rsLTemp.Get_Fields("MaturityFee"))) + Conversion.Val(rsLTemp.Get_Fields("Costs"))) - (Conversion.Val(rsLTemp.Get_Fields_Decimal("PrincipalPaid")) + Conversion.Val(rsLTemp.Get_Fields_Decimal("InterestPaid")) + Conversion.Val(rsLTemp.Get_Fields_Decimal("CostsPaid"))) - curCurrentInterest, 2);
							break;
						}
				}
				//end switch
				// If yr = 1996 Then Stop
				if (Conversion.Val(GRID_Total) == Conversion.Val(dblTemp))
				{
					CompareCol = true;
				}
			}
			rsLTemp.Reset();
			rsTemp.Reset();
			return CompareCol;
		}

		private void FillLabels_2(int BK)
		{
			FillLabels(ref BK);
		}

		private void FillLabels(ref int BK)
		{
			// pass in the bill ID and the information is set
			Decimal curHold;
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp;
			// sets the current information at the top of the screen
			strTemp = "SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE Bill = " + FCConvert.ToString(BK) + " ORDER BY BillNumber Desc";
			rsTemp.OpenRecordset(strTemp, modExtraModules.strUTDatabase);
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				// lblLandValue2.Text = Format(Val(.Fields("LandValue")), "#,##0")
				// lblBuildingValue2.Text = Format(Val(.Fields("BuildingValue")), "#,##0")
				// lblExemptValue2.Text = Format(Val(.Fields("ExemptValue")), "#,##0")
				// lblTotalValue2.Text = Format(Val(.Fields("LandValue")) + Val(.Fields("BuildingValue")) - Val(.Fields("ExemptValue")), "#,##0")
				// lblYear.Text = Val(FormatYear(.Fields("BillingYear")))
				// curHold = Val(.Fields("TaxDue1")) + Val(.Fields("TaxDue2")) + Val(.Fields("TaxDue3")) + Val(.Fields("TaxDue4"))
			}
			else
			{
				// Stop
			}
			rsTemp.Reset();
		}

		private void GetDataFromRateTable(ref bool boolWater)
		{
			clsDRWrapper rsRT = new clsDRWrapper();
			if (boolWater)
			{
				if (bcodeW == "LIEN")
				{
					strSQL = "SELECT * FROM RATEKeys WHERE ID = " + rsLRW.Get_Fields_Int32("Ratekey");
				}
				else
				{
					strSQL = "SELECT * FROM RATEKeys WHERE ID = " + rsCL.Get_Fields_Int32("BillingRatekey");
				}
			}
			else
			{
				if (bcodeS == "LIEN")
				{
					strSQL = "SELECT * FROM RATEKeys WHERE ID = " + rsLRS.Get_Fields_Int32("Ratekey");
				}
				else
				{
					strSQL = "SELECT * FROM RATEKeys WHERE ID = " + rsCL.Get_Fields_Int32("BillingRatekey");
				}
			}
			// If rsCl.fields("BillingYear") = 1996 Then Stop
			rsRT.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			if (rsRT.EndOfFile() != true && rsRT.BeginningOfFile() != true)
			{
				// If IsNull(rsRT.Fields("NumberOfPeriods")) = False Then intNumberOfPeriods = rsRT.Fields("NumberOfPeriods")
				if (fecherFoundation.FCUtils.IsEmptyDateTime(rsRT.Get_Fields_DateTime("IntStart")) == false)
					modUTFunctions.Statics.strUTInterestDate1 = FCConvert.ToString(rsRT.Get_Fields_DateTime("IntStart"));
				// If IsNull(rsRT.Fields("InterestStartDate2")) = False Then strUTInterestDate2 = rsRT.Fields("InterestStartDate2")
				// If IsNull(rsRT.Fields("InterestStartDate3")) = False Then strUTInterestDate3 = rsRT.Fields("InterestStartDate3")
				// If IsNull(rsRT.Fields("InterestStartDate4")) = False Then strUTInterestDate4 = rsRT.Fields("InterestStartDate4")
				// If IsNull(rsRT.Fields("InterestRate")) = False Then
				// dblUTInterestRate = rsRT.Fields("InterestRate")
				// End If
			}
			else
			{
				modUTFunctions.Statics.strUTInterestDate1 = FCConvert.ToString(DateAndTime.DateValue(FCConvert.ToString(0)));
				modUTFunctions.Statics.strUTInterestDate2 = FCConvert.ToString(DateAndTime.DateValue(FCConvert.ToString(0)));
				modUTFunctions.Statics.strUTInterestDate3 = FCConvert.ToString(DateAndTime.DateValue(FCConvert.ToString(0)));
				modUTFunctions.Statics.strUTInterestDate4 = FCConvert.ToString(DateAndTime.DateValue(FCConvert.ToString(0)));
			}
			if (modUTFunctions.Statics.dblUTInterestRate == 0)
				modUTFunctions.Statics.dblUTInterestRate = modExtraModules.Statics.dblDefaultInterestRate;
			rsRT.Reset();
		}

		private void Format_Grid_1(ref bool boolWater)
		{
			Format_Grid(false, boolWater);
		}

		private void Format_Grid_2(bool boolForceWide, bool boolWater = true)
		{
			Format_Grid(boolForceWide, boolWater);
		}

		private void Format_Grid_8(bool boolForceWide, bool boolWater = true)
		{
			Format_Grid(boolForceWide, boolWater);
		}

		private void Format_Grid(bool boolForceWide = false, bool boolWater = true)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sets the size of the grid control and the titles line
				int wid;
				double dblPaymentHeight;
				double dblStatusHeight;
				dblPaymentHeight = 0.49;
				dblStatusHeight = 0.09;
				//this.SGRID.Columns[lngGRIDColTotal - 1].DefaultCellStyle.ForeColor = Color.FromArgb(5, 204, 71);
				this.SGRID.Columns[lngGRIDColTotal - 1].DefaultCellStyle.Font = new Font("default", 16F, FontStyle.Bold, GraphicsUnit.Pixel);
				//this.WGRID.Columns[lngGRIDColTotal - 1].DefaultCellStyle.ForeColor = Color.FromArgb(5, 204, 71);
				this.WGRID.Columns[lngGRIDColTotal - 1].DefaultCellStyle.Font = new Font("default", 16F, FontStyle.Bold, GraphicsUnit.Pixel);
				if (modUTFunctions.Statics.TownService != "B")
				{
					boolForceWide = true;
					if (modUTFunctions.Statics.TownService == "W")
					{
						boolWater = true;
					}
					else
					{
						boolWater = false;
					}
				}
				if (modUTFunctions.Statics.TownService != "S")
				{
					// water grid
					WGRID.Cols = lngGRIDColPending + 1;
					if (boolPayment)
					{
						fraStatusLabels.Visible = false;
						fraPayment.Visible = true;
						// Payment Screen
						if (boolForceWide)
						{
							if (!boolWater)
							{
								// Hide Water Grid
								WGRID.Visible = false;
							}
							else
							{
								// Show Water Grid Wide
								WGRID.Width = FCConvert.ToInt32(this.Width * 0.94);
								//WGRID.Left = FCConvert.ToInt32((this.Width - WGRID.Width) / 5.0);
								WGRID.Left = 30;
								//WGRID.Height = FCConvert.ToInt32(this.Height * dblPaymentHeight);
								WGRID.Top = lblPaymentInfo.Top + lblPaymentInfo.Height + lblSewerHeading.Height;
								SGRID.Visible = false;
								WGRID.Visible = true;
							}
						}
						else
						{
							// Show Both Grids
							//WGRID.Height = FCConvert.ToInt32(this.Height * dblPaymentHeight);
							WGRID.Top = lblPaymentInfo.Top + lblPaymentInfo.Height + lblWaterHeading.Height;
							WGRID.Width = FCConvert.ToInt32(this.Width * 0.47);
							WGRID.Left = FCConvert.ToInt32((((this.Width / 2.0) - WGRID.Width) / 2));
							WGRID.Visible = true;
							SGRID.Visible = true;
						}
					}
					else
					{
						fraStatusLabels.Visible = true;
						fraPayment.Visible = false;
						// Status Screen
						if (boolForceWide)
						{
							if (!boolWater)
							{
								// Hide Water Grid
								WGRID.Visible = false;
							}
							else
							{
								// Show Water Grid Wide
								WGRID.Width = FCConvert.ToInt32(this.Width * 0.94);
								//WGRID.Left = FCConvert.ToInt32((this.Width - WGRID.Width) / 5.0);
								WGRID.Left = 30;
								WGRID.Top = fraStatusLabels.Top + fraStatusLabels.Height + lblWaterHeading.Height;
								//WGRID.Height = FCConvert.ToInt32(Math.Abs(this.Height - WGRID.Top - (this.Height * dblStatusHeight)));
								SGRID.Visible = false;
								WGRID.Visible = true;
							}
						}
						else
						{
							// Show Both Grids
							//WGRID.Height = FCConvert.ToInt32(Math.Abs(this.Height - WGRID.Top - (this.Height * dblStatusHeight)));
							WGRID.Top = fraStatusLabels.Top + fraStatusLabels.Height + lblWaterHeading.Height;
							WGRID.Width = FCConvert.ToInt32(this.Width * 0.47);
							WGRID.Left = FCConvert.ToInt32((((this.Width / 2.0) - WGRID.Width) / 2));
							WGRID.Visible = true;
							SGRID.Visible = true;
						}
					}
					// If boolPayment And Not (boolForceWide And boolWater) Then
					// .Height = frmUTCLStatus.Height * dblPaymentHeight
					// .Top = lblPaymentInfo.Top + lblPaymentInfo.Height + lblWaterHeading.Height
					// If TownService = "B" Then
					// show both grids
					// .Width = frmUTCLStatus.Width * 0.49
					// .Left = ((frmUTCLStatus.Width / 2) - .Width) / 2
					// 
					// If Not boolForceWide Then
					// WGRID.Visible = True
					// End If
					// SGRID.Visible = True
					// Else
					// show only the water grid
					// .Width = frmUTCLStatus.Width * 0.99
					// .Left = (frmUTCLStatus.Width - .Width) / 5
					// SGRID.Visible = False
					// WGRID.Visible = True
					// End If
					// ElseIf boolWater Then
					// If Not boolForceWide Then
					// fraStatusLabels.Visible = True
					// .Top = fraStatusLabels.Top + fraStatusLabels.Height + lblWaterHeading.Height
					// .Height = Abs(frmUTCLStatus.Height - .Top - (frmUTCLStatus.Height * dblStatusHeight))
					// .Width = frmUTCLStatus.Width * 0.99
					// .Left = (frmUTCLStatus.Width - .Width) / 5
					// WGRID.Visible = True
					// SGRID.Visible = False
					// Else
					// .Width = frmUTCLStatus.Width * 0.99
					// .Left = (frmUTCLStatus.Width - .Width) / 5
					// If boolPayment Then
					// .Top = lblPaymentInfo.Top + lblPaymentInfo.Height + lblWaterHeading.Height
					// .Height = frmUTCLStatus.Height * dblPaymentHeight
					// Else
					// .Top = fraStatusLabels.Top + fraStatusLabels.Height + lblWaterHeading.Height
					// .Height = Abs(frmUTCLStatus.Height - .Top - (frmUTCLStatus.Height * dblStatusHeight))
					// End If
					// SGRID.Visible = False
					// lblSewerHeading.Visible = False
					// lblWaterHeading.Left = (Me.Width - lblWaterHeading.Width) / 2
					// WGRID.Visible = True
					// End If
					// Else
					// .Top = fraStatusLabels.Top + fraStatusLabels.Height + lblSewerHeading.Height
					// .Height = Abs(frmUTCLStatus.Height - .Top - (frmUTCLStatus.Height * dblStatusHeight))
					// End If
					WGRID.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
					// column headers
					WGRID.TextMatrix(0, lngGRIDCOLYear, "Year");
					WGRID.TextMatrix(0, lngGRIDColBillNumber, "Bill");
					WGRID.TextMatrix(0, lngGRIDColDate, "Date");
					WGRID.TextMatrix(0, lngGRIDColRef, "Ref");
					WGRID.TextMatrix(0, lngGRIDColService, "P");
					WGRID.TextMatrix(0, lngGRIDColPaymentCode, "C");
					WGRID.TextMatrix(0, lngGRIDColPrincipal, "Principal");
					WGRID.TextMatrix(0, lngGRIDColPTC, "PTC");
					WGRID.TextMatrix(0, lngGRIDColTax, "Tax");
					WGRID.TextMatrix(0, lngGRIDColInterest, "Interest");
					WGRID.TextMatrix(0, lngGRIDColCosts, "Costs");
					WGRID.TextMatrix(0, lngGRIDColTotal, "Total");
					if (!(boolForceWide && !boolWater))
					{
						AutoSize_GRID_8(true, (boolForceWide && boolWater));
					}
					WGRID.ColFormat(lngGRIDColDate, "MM/dd/yy");
					WGRID.ColFormat(lngGRIDColPrincipal, "#,##0.00");
					WGRID.ColFormat(lngGRIDColPTC, "#,##0.00");
					WGRID.ColFormat(lngGRIDColTax, "#,##0.00");
					WGRID.ColFormat(lngGRIDColInterest, "#,##0.00");
					WGRID.ColFormat(lngGRIDColCosts, "#,##0.00");
					WGRID.ColFormat(lngGRIDColTotal, "#,##0.00");
					WGRID.ColAlignment(lngGRIDCOLYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					WGRID.ColAlignment(lngGRIDColBillNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					WGRID.ColAlignment(lngGRIDColPending, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					WGRID.ColAlignment(lngGRIDColRef, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    WGRID.ColAlignment(lngGRIDColPrincipal, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    WGRID.ColAlignment(lngGRIDColPTC, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    WGRID.ColAlignment(lngGRIDColTax, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    WGRID.ColAlignment(lngGRIDColInterest, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    WGRID.ColAlignment(lngGRIDColCosts, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    WGRID.ColAlignment(lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    WGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColPrincipal, 0, lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
					WGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColPTC, 0, lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
					WGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColRef, 0, lngGRIDColPaymentCode, FCGrid.AlignmentSettings.flexAlignRightCenter);
					WGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColDate, 0, lngGRIDColDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					WGRID.BackColorBkg = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					WGRID.BackColorFixed = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
				if (modUTFunctions.Statics.TownService != "W")
				{
					SGRID.Cols = lngGRIDColPending + 1;
					if (boolPayment)
					{
						fraStatusLabels.Visible = false;
						fraPayment.Visible = true;
						// Payment Screen
						if (boolForceWide)
						{
							if (boolWater)
							{
								// Show Water Grid Wide
								SGRID.Visible = false;
							}
							else
							{
								// Show Sewer Grid Wide
								SGRID.Width = FCConvert.ToInt32(this.Width * 0.94);
								//SGRID.Left = FCConvert.ToInt32((this.Width - SGRID.Width) / 5.0);
								SGRID.Left = 30;
								//SGRID.Height = FCConvert.ToInt32(this.Height * dblPaymentHeight);
								SGRID.Top = lblPaymentInfo.Top + lblPaymentInfo.Height + lblSewerHeading.Height;
								WGRID.Visible = false;
								SGRID.Visible = true;
							}
						}
						else
						{
							// Show Both Grids
							//SGRID.Height = FCConvert.ToInt32(this.Height * dblPaymentHeight);
							SGRID.Top = lblPaymentInfo.Top + lblPaymentInfo.Height + lblSewerHeading.Height;
							SGRID.Width = FCConvert.ToInt32(this.Width * 0.47);
							SGRID.Left = FCConvert.ToInt32((((this.Width / 2.0) - SGRID.Width) / 2) + (this.Width / 2.0));
							SGRID.Visible = true;
							WGRID.Visible = true;
						}
					}
					else
					{
						fraStatusLabels.Visible = true;
						fraPayment.Visible = false;
						// Status Screen
						if (boolForceWide)
						{
							if (boolWater)
							{
								// Hide Sewer Grid
								SGRID.Visible = false;
							}
							else
							{
								// Show Sewer Grid Wide
								SGRID.Width = FCConvert.ToInt32(this.Width * 0.94);
								//SGRID.Left = FCConvert.ToInt32((this.Width - SGRID.Width) / 5.0);
								SGRID.Left = 30;
								SGRID.Top = fraStatusLabels.Top + fraStatusLabels.Height + lblSewerHeading.Height;
								//SGRID.Height = FCConvert.ToInt32(Math.Abs(this.Height - SGRID.Top - (this.Height * dblStatusHeight)));
								WGRID.Visible = false;
								SGRID.Visible = true;
							}
						}
						else
						{
							// Show Both Grids
							//SGRID.Height = FCConvert.ToInt32(Math.Abs(this.Height - SGRID.Top - (this.Height * dblStatusHeight)));
							SGRID.Top = fraStatusLabels.Top + fraStatusLabels.Height + lblSewerHeading.Height;
							SGRID.Width = FCConvert.ToInt32(this.Width * 0.48);
							SGRID.Left = FCConvert.ToInt32((((this.Width / 2.0) - SGRID.Width) / 2) + (this.Width / 2.0));
							SGRID.Visible = true;
							WGRID.Visible = true;
						}
					}
					SGRID.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
					// column headers
					SGRID.TextMatrix(0, lngGRIDCOLYear, "Year");
					SGRID.TextMatrix(0, lngGRIDColBillNumber, "Bill");
					SGRID.TextMatrix(0, lngGRIDColDate, "Date");
					SGRID.TextMatrix(0, lngGRIDColRef, "Ref");
					SGRID.TextMatrix(0, lngGRIDColService, "P");
					SGRID.TextMatrix(0, lngGRIDColPaymentCode, "C");
					SGRID.TextMatrix(0, lngGRIDColPrincipal, "Principal");
					SGRID.TextMatrix(0, lngGRIDColPTC, "PTC");
					SGRID.TextMatrix(0, lngGRIDColTax, "Tax");
					SGRID.TextMatrix(0, lngGRIDColInterest, "Interest");
					SGRID.TextMatrix(0, lngGRIDColCosts, "Costs");
					SGRID.TextMatrix(0, lngGRIDColTotal, "Total");
					// If boolPayment Then
					if (!(boolForceWide && boolWater))
					{
						AutoSize_GRID_8(false, (boolForceWide && !boolWater));
					}
					// Else
					// AutoSize_GRID boolWater, True
					// End If
					SGRID.ColFormat(lngGRIDColDate, "MM/dd/yy");
					SGRID.ColFormat(lngGRIDColPrincipal, "#,##0.00");
					SGRID.ColFormat(lngGRIDColPTC, "#,##0.00");
					SGRID.ColFormat(lngGRIDColTax, "#,##0.00");
					SGRID.ColFormat(lngGRIDColInterest, "#,##0.00");
					SGRID.ColFormat(lngGRIDColCosts, "#,##0.00");
					SGRID.ColFormat(lngGRIDColTotal, "#,##0.00");
					SGRID.ColAlignment(lngGRIDCOLYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					SGRID.ColAlignment(lngGRIDColBillNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					SGRID.ColAlignment(lngGRIDColPending, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                    SGRID.ColAlignment(lngGRIDColPrincipal, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    SGRID.ColAlignment(lngGRIDColPTC, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    SGRID.ColAlignment(lngGRIDColTax, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    SGRID.ColAlignment(lngGRIDColInterest, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    SGRID.ColAlignment(lngGRIDColCosts, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    SGRID.ColAlignment(lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    SGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColPrincipal, 0, lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
					SGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColPTC, 0, lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
					SGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColRef, 0, lngGRIDColPaymentCode, FCGrid.AlignmentSettings.flexAlignRightCenter);
					SGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColDate, 0, lngGRIDColDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					SGRID.BackColorBkg = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					SGRID.BackColorFixed = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
				// set the headers
				SetHeadingLabels();
				//FC:FINAL:RPU: #i1021 - Set the fraPayment location below grids
				if (SGRID.Visible)
				{
					fraPayment.Top = SGRID.Top + SGRID.Height + 10;
				}
				else
				{
					fraPayment.Top = WGRID.Top + WGRID.Height + 10;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Formatting Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Add_6(int Row, int lvl, bool boolWater)
		{
			Add(ref Row, ref lvl, ref boolWater);
		}

		private void Add_24(int Row, int lvl, bool boolWater)
		{
			Add(ref Row, ref lvl, ref boolWater);
		}

		private void Add_26(int Row, int lvl, bool boolWater)
		{
			Add(ref Row, ref lvl, ref boolWater);
		}

		private void Add(ref int Row, ref int lvl, ref bool boolWater)
		{
			// this sub adds a row in the grid at position = row and .rowoutlinelevel = lvl
			// set the row as a group
			if (boolWater)
			{
				if (MaxRowsW == 0)
				{
					MaxRowsW = 2;
				}
				WGRID.AddItem("", Row);
				// increments the number of rows
				MaxRowsW = WGRID.Rows - 1;
				lvl += 1;
				if (lvl == 1)
				{
                    //WGRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngGRIDCOLYear, Row, WGRID.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT); // &H80000018
                    WGRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, lngGRIDColTotal, Color.FromArgb(5, 204, 71));                   
                }
				else if (lvl == 2)
				{
					WGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, Row, lngGRIDColRef, Row, WGRID.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
					WGRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngGRIDCOLYear, Row, WGRID.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249)));
					// &H80000016
					//WGRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, lngGRIDCOLYear, Row, lngGRIDCOLYear, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX); // &H80000016
				}
				else
				{
					//WGRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngGRIDCOLYear, Row, WGRID.Cols - 1, Color.White);
				}
				// set the indentation level of the group
				WGRID.RowOutlineLevel(Row, FCConvert.ToInt16(lvl));
				if (lvl <= 1)
					WGRID.IsSubtotal(Row, true);
			}
			else
			{
				if (MaxRowsS == 0)
				{
					MaxRowsS = 2;
				}
				SGRID.AddItem("", Row);
				// increments the number of rows
				MaxRowsS = SGRID.Rows - 1;
				lvl += 1;
				if (lvl == 1)
				{
                    //SGRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngGRIDCOLYear, Row, SGRID.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT); // &H80000018
                    SGRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, lngGRIDColTotal, Color.FromArgb(5, 204, 71));
                }
				else if (lvl == 2)
				{
					SGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, Row, lngGRIDColRef, Row, SGRID.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
					SGRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngGRIDCOLYear, Row, SGRID.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249)));
					// &H80000016
					//SGRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, lngGRIDCOLYear, Row, lngGRIDColBillNumber, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX); // &H80000016
				}
				else
				{
					//SGRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngGRIDCOLYear, Row, SGRID.Cols - 1, Color.White);
				}
				// set the indentation level of the group
				SGRID.RowOutlineLevel(Row, FCConvert.ToInt16(lvl));
				if (lvl <= 1)
					SGRID.IsSubtotal(Row, true);
			}
		}

		private void AutoSize_GRID_8(bool boolWater, bool boolForcedWide = false)
		{
			AutoSize_GRID(ref boolWater, boolForcedWide);
		}

		private void AutoSize_GRID(ref bool boolWater, bool boolForcedWide = false)
		{
			// sets the width of each column in order to fit in the grid without a scrollbar
			// the total if added should be slightly less then 1
			int lngTemp;
			int i;
			int wid = 0;
			//FC:FINAL:DDU:#i1039 - make visible all columns
			for (int j = 0; j < lngGRIDColPending - 1; j++)
			{
				WGRID.Columns[j].Visible = true;
				SGRID.Columns[j].Visible = true;
			}
			if (modUTFunctions.Statics.TownService == "B")
			{
				// boolPayment And
				// this will show the grids side by side
				if (!boolForcedWide)
				{
					wid = WGRID.WidthOriginal;
					WGRID.ColWidth(lngGRIDCOLTree, FCConvert.ToInt32(wid * 0.075));
					WGRID.ColWidth(lngGRIDCOLYear, 0);
					WGRID.ColWidth(lngGridColBillKey, 0);
					WGRID.ColWidth(lngGRIDColBillNumber, FCConvert.ToInt32(wid * 0.1));
					WGRID.ColWidth(lngGRIDColDate, FCConvert.ToInt32(wid * 0.2));
					WGRID.ColWidth(lngGRIDColRef, 0);
					// wid * 0.098
					WGRID.ColWidth(lngGRIDColService, 0);
					// wid * 0.04
					WGRID.ColWidth(lngGRIDColPaymentCode, 0);
					// wid * 0.04
					WGRID.ColWidth(lngGRIDColPrincipal, 0);
					// wid * 0.22
					WGRID.ColWidth(lngGRIDColPTC, FCConvert.ToInt32(wid * 0.18));
					WGRID.ColWidth(lngGRIDColTax, 0);
					// wid * 0.12
					WGRID.ColWidth(lngGRIDColInterest, FCConvert.ToInt32(wid * 0.17));
					WGRID.ColWidth(lngGRIDColCosts, 0);
					// wid * 0.12
					WGRID.ColWidth(lngGRIDColTotal, FCConvert.ToInt32(wid * 0.19));
					WGRID.ColWidth(lngGRIDColLineCode, 0);
					// Row code
					WGRID.ColWidth(lngGRIDColPaymentKey, 0);
					// Payment ID
					WGRID.ColWidth(lngGRIDColCHGINTNumber, 0);
					// CHGINT Number
					WGRID.ColWidth(lngGRIDColPerDiem, 0);
					// Per Diem in Master Line
					WGRID.ColWidth(lngGRIDColPending, FCConvert.ToInt32(wid * 0.05));
					// * Col
					wid = SGRID.WidthOriginal;
					SGRID.ColWidth(lngGRIDCOLTree, FCConvert.ToInt32(wid * 0.075));
					SGRID.ColWidth(lngGRIDCOLYear, 0);
					SGRID.ColWidth(lngGridColBillKey, 0);
					SGRID.ColWidth(lngGRIDColBillNumber, FCConvert.ToInt32(wid * 0.1));
					SGRID.ColWidth(lngGRIDColDate, FCConvert.ToInt32(wid * 0.2));
					SGRID.ColWidth(lngGRIDColRef, 0);
					// wid * 0.098
					SGRID.ColWidth(lngGRIDColService, 0);
					// wid * 0.04
					SGRID.ColWidth(lngGRIDColPaymentCode, 0);
					// wid * 0.04
					SGRID.ColWidth(lngGRIDColPrincipal, 0);
					// wid * 0.22
					SGRID.ColWidth(lngGRIDColPTC, FCConvert.ToInt32(wid * 0.18));
					SGRID.ColWidth(lngGRIDColTax, 0);
					// wid * 0.12
					SGRID.ColWidth(lngGRIDColInterest, FCConvert.ToInt32(wid * 0.17));
					SGRID.ColWidth(lngGRIDColCosts, 0);
					// wid * 0.12
					SGRID.ColWidth(lngGRIDColTotal, FCConvert.ToInt32(wid * 0.19));
					SGRID.ColWidth(lngGRIDColLineCode, 0);
					// Row code
					SGRID.ColWidth(lngGRIDColPaymentKey, 0);
					// Payment ID
					SGRID.ColWidth(lngGRIDColCHGINTNumber, 0);
					// CHGINT Number
					SGRID.ColWidth(lngGRIDColPerDiem, 0);
					// Per Diem in Master Line
					SGRID.ColWidth(lngGRIDColPending, FCConvert.ToInt32(wid * 0.05));
					// * Col
				}
				else if (boolWater)
				{
					// make the water grid larger
					wid = WGRID.WidthOriginal;
					WGRID.ColWidth(lngGRIDCOLYear, 0);
					WGRID.ColWidth(lngGridColBillKey, 0);
					WGRID.ColWidth(lngGRIDColBillNumber, FCConvert.ToInt32(wid * 0.075));
					WGRID.ColWidth(lngGRIDColDate, FCConvert.ToInt32(wid * 0.12));
					WGRID.ColWidth(lngGRIDColRef, FCConvert.ToInt32(wid * 0.09));
					WGRID.ColWidth(lngGRIDColService, 0);
					// wid * 0.04
					WGRID.ColWidth(lngGRIDColPaymentCode, FCConvert.ToInt32(wid * 0.04));
					WGRID.ColWidth(lngGRIDColPrincipal, FCConvert.ToInt32(wid * 0.12));
					WGRID.ColWidth(lngGRIDColPTC, 0);
					WGRID.ColWidth(lngGRIDColTax, FCConvert.ToInt32(wid * 0.11));
					WGRID.ColWidth(lngGRIDColInterest, FCConvert.ToInt32(wid * 0.11));
					WGRID.ColWidth(lngGRIDColCosts, FCConvert.ToInt32(wid * 0.11));
					WGRID.ColWidth(lngGRIDColTotal, FCConvert.ToInt32(wid * 0.12));
					WGRID.ColWidth(lngGRIDColLineCode, 0);
					// Row code
					WGRID.ColWidth(lngGRIDColPaymentKey, 0);
					// Payment ID
					WGRID.ColWidth(lngGRIDColCHGINTNumber, 0);
					// CHGINT Number
					WGRID.ColWidth(lngGRIDColPerDiem, 0);
					// Per Diem in Master Line
					WGRID.ColWidth(lngGRIDColPending, FCConvert.ToInt32(wid * 0.02));
					// * Col
				}
				else
				{
					// make the sewer grid larger
					wid = SGRID.WidthOriginal;
					SGRID.ColWidth(lngGRIDCOLYear, 0);
					SGRID.ColWidth(lngGridColBillKey, 0);
					SGRID.ColWidth(lngGRIDColBillNumber, FCConvert.ToInt32(wid * 0.075));
					SGRID.ColWidth(lngGRIDColDate, FCConvert.ToInt32(wid * 0.12));
					SGRID.ColWidth(lngGRIDColRef, FCConvert.ToInt32(wid * 0.09));
					SGRID.ColWidth(lngGRIDColService, 0);
					// wid * 0.04
					SGRID.ColWidth(lngGRIDColPaymentCode, FCConvert.ToInt32(wid * 0.04));
					SGRID.ColWidth(lngGRIDColPrincipal, FCConvert.ToInt32(wid * 0.12));
					SGRID.ColWidth(lngGRIDColPTC, 0);
					SGRID.ColWidth(lngGRIDColTax, FCConvert.ToInt32(wid * 0.11));
					SGRID.ColWidth(lngGRIDColInterest, FCConvert.ToInt32(wid * 0.11));
					SGRID.ColWidth(lngGRIDColCosts, FCConvert.ToInt32(wid * 0.11));
					SGRID.ColWidth(lngGRIDColTotal, FCConvert.ToInt32(wid * 0.12));
					SGRID.ColWidth(lngGRIDColLineCode, 0);
					// Row code
					SGRID.ColWidth(lngGRIDColPaymentKey, 0);
					// Payment ID
					SGRID.ColWidth(lngGRIDColCHGINTNumber, 0);
					// CHGINT Number
					SGRID.ColWidth(lngGRIDColPerDiem, 0);
					// Per Diem in Master Line
					SGRID.ColWidth(lngGRIDColPending, FCConvert.ToInt32(wid * 0.02));
					// * Col
				}
			}
			else
			{
				if (boolWater)
				{
					wid = WGRID.WidthOriginal;
					WGRID.ColWidth(lngGRIDCOLYear, 0);
					WGRID.ColWidth(lngGridColBillKey, 0);
					WGRID.ColWidth(lngGRIDColBillNumber, FCConvert.ToInt32(wid * 0.075));
					WGRID.ColWidth(lngGRIDColDate, FCConvert.ToInt32(wid * 0.12));
					WGRID.ColWidth(lngGRIDColRef, FCConvert.ToInt32(wid * 0.09));
					WGRID.ColWidth(lngGRIDColService, 0);
					// wid * 0.04
					WGRID.ColWidth(lngGRIDColPaymentCode, FCConvert.ToInt32(wid * 0.04));
					WGRID.ColWidth(lngGRIDColPrincipal, FCConvert.ToInt32(wid * 0.12));
					WGRID.ColWidth(lngGRIDColPTC, 0);
					WGRID.ColWidth(lngGRIDColTax, FCConvert.ToInt32(wid * 0.11));
					WGRID.ColWidth(lngGRIDColInterest, FCConvert.ToInt32(wid * 0.11));
					WGRID.ColWidth(lngGRIDColCosts, FCConvert.ToInt32(wid * 0.11));
					WGRID.ColWidth(lngGRIDColTotal, FCConvert.ToInt32(wid * 0.12));
					WGRID.ColWidth(lngGRIDColLineCode, 0);
					// Row code
					WGRID.ColWidth(lngGRIDColPaymentKey, 0);
					// Payment ID
					WGRID.ColWidth(lngGRIDColCHGINTNumber, 0);
					// CHGINT Number
					WGRID.ColWidth(lngGRIDColPerDiem, 0);
					// Per Diem in Master Line
					WGRID.ColWidth(lngGRIDColPending, FCConvert.ToInt32(wid * 0.02));
					// * Col
				}
				else
				{
					wid = SGRID.WidthOriginal;
					SGRID.ColWidth(lngGRIDCOLYear, 0);
					SGRID.ColWidth(lngGridColBillKey, 0);
					SGRID.ColWidth(lngGRIDColBillNumber, FCConvert.ToInt32(wid * 0.075));
					SGRID.ColWidth(lngGRIDColDate, FCConvert.ToInt32(wid * 0.12));
					SGRID.ColWidth(lngGRIDColRef, FCConvert.ToInt32(wid * 0.09));
					SGRID.ColWidth(lngGRIDColService, 0);
					// wid * 0.04
					SGRID.ColWidth(lngGRIDColPaymentCode, FCConvert.ToInt32(wid * 0.04));
					SGRID.ColWidth(lngGRIDColPrincipal, FCConvert.ToInt32(wid * 0.12));
					SGRID.ColWidth(lngGRIDColPTC, 0);
					SGRID.ColWidth(lngGRIDColTax, FCConvert.ToInt32(wid * 0.11));
					SGRID.ColWidth(lngGRIDColInterest, FCConvert.ToInt32(wid * 0.11));
					SGRID.ColWidth(lngGRIDColCosts, FCConvert.ToInt32(wid * 0.11));
					SGRID.ColWidth(lngGRIDColTotal, FCConvert.ToInt32(wid * 0.12));
					SGRID.ColWidth(lngGRIDColLineCode, 0);
					// Row code
					SGRID.ColWidth(lngGRIDColPaymentKey, 0);
					// Payment ID
					SGRID.ColWidth(lngGRIDColCHGINTNumber, 0);
					// CHGINT Number
					SGRID.ColWidth(lngGRIDColPerDiem, 0);
					// Per Diem in Master Line
					SGRID.ColWidth(lngGRIDColPending, FCConvert.ToInt32(wid * 0.02));
					// * Col
				}
			}
		}

		private void Adjust_GRID_Height_6(int ht, bool boolWater)
		{
			Adjust_GRID_Height(ref ht, ref boolWater);
		}

		private void Adjust_GRID_Height_8(int ht, bool boolWater)
		{
			/*Adjust_GRID_Height(ref ht, ref boolWater);*/
		}

		private void Adjust_GRID_Height(ref int ht, ref bool boolWater)
		{
			//FC:FINAL:DDU:#i1035 - adjustments already resolved by design
			// this adjusts the grid height depending on the amount of rows
			//if (boolWater)
			//{
			//    if (boolPayment)
			//    {
			//        if (ht < (this.Height * 0.48) + (WGRID.RowHeight(0) * 0.15))
			//        {
			//            if (WGRID.Height != ht)
			//            {
			//                WGRID.Height = FCConvert.ToInt32(ht + (WGRID.RowHeight(0) * 0.15));
			//            }
			//        }
			//        else
			//        {
			//            if (ht > (this.Height * 0.48) + (WGRID.RowHeight(0) * 0.15))
			//            {
			//                WGRID.Height = FCConvert.ToInt32((this.Height * 0.48) + (WGRID.RowHeight(0) * 0.15));
			//            }
			//            else
			//            {
			//                WGRID.Height = ht;
			//            }
			//        }
			//    }
			//    else
			//    {
			//        if (ht < this.Height * 0.75)
			//        {
			//            if (WGRID.Height != ht)
			//            {
			//                WGRID.Height = FCConvert.ToInt32(ht + (WGRID.RowHeight(0) * 0.15));
			//            }
			//        }
			//        else
			//        {
			//            if (WGRID.Height < this.Height * 0.75)
			//            {
			//                WGRID.Height = FCConvert.ToInt32(this.Height * 0.75 + (WGRID.RowHeight(0) * 0.15));
			//            }
			//        }
			//    }
			//}
			//else
			//{
			//    if (boolPayment)
			//    {
			//        if (ht < this.Height * 0.48)
			//        {
			//            if (SGRID.Height != ht)
			//            {
			//                SGRID.Height = FCConvert.ToInt32(ht + (SGRID.RowHeight(0) * 0.15));
			//            }
			//        }
			//        else
			//        {
			//            if (ht > (this.Height * 0.48) + (SGRID.RowHeight(0) * 0.15))
			//            {
			//                SGRID.Height = FCConvert.ToInt32((this.Height * 0.48) + (SGRID.RowHeight(0) * 0.15));
			//                SGRID.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//            }
			//            else
			//            {
			//                SGRID.Height = ht;
			//                SGRID.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//            }
			//        }
			//    }
			//    else
			//    {
			//        if (ht < this.Height * 0.75)
			//        {
			//            if (SGRID.Height != ht)
			//            {
			//                SGRID.Height = FCConvert.ToInt32(ht + (SGRID.RowHeight(0) * 0.15));
			//            }
			//        }
			//        else
			//        {
			//            if (SGRID.Height < this.Height * 0.75)
			//            {
			//                SGRID.Height = FCConvert.ToInt32(this.Height * 0.75 + (SGRID.RowHeight(0) * 0.15));
			//                SGRID.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//            }
			//        }
			//    }
			//}
		}

		private void lblAcctComment_Click(object sender, System.EventArgs e)
		{
			mnuFileComment_Click();
		}

		private void lblGrpInfo_Click()
		{
			//kk03202014 trocl-816  Change the Group info display - show the form from the Globals
			frmGroupBalance.InstancePtr.Init(lngGroupID);
		}

		private void lblSewerHeading_DoubleClick(object sender, System.EventArgs e)
		{
			string strWS;
			int intCT;
			strWS = "S";
			// If boolPayment Then
			if (WGRID.Visible)
			{
				Format_Grid_8(true, false);
				boolShowingWater = false;
				mnuFileSwitch.Visible = true;
				mnuFileShowSplitView.Visible = true;
				strWS = "S";
			}
			else if (WGRID.Rows > 1)
			{
				mnuFileSwitch_Click();
				Format_Grid_8(true, true);
				mnuFileSwitch.Visible = true;
				mnuFileShowSplitView.Visible = true;
				strWS = "W";
			}
			else
			{
				SGRID.Visible = true;
				WGRID.Visible = false;
			}
			// set the service of what is showing
			for (intCT = 0; intCT <= cmbService.Items.Count - 1; intCT++)
			{
				if (Strings.Left(cmbService.Items[intCT].ToString(), 1) == strWS)
				{
					cmbService.SelectedIndex = intCT;
					break;
				}
			}
			// ElseIf WGRID.rows > 1 Then
			// mnuFileSwitch_Click
			// End If
			//FC:FINAL:DDU:#i1048 - fixed incorrect alignment
			WGRID.Refresh();
			SGRID.Refresh();
		}

		private void lblWaterHeading_DoubleClick(object sender, System.EventArgs e)
		{
			string strWS;
			int intCT;
			strWS = "W";
			// If boolPayment Then
			if (SGRID.Visible)
			{
				Format_Grid_8(true, true);
				boolShowingWater = true;
				mnuFileSwitch.Visible = true;
				mnuFileShowSplitView.Visible = true;
				strWS = "W";
			}
			else if (SGRID.Rows > 1)
			{
				mnuFileSwitch_Click();
				Format_Grid_8(true, false);
				mnuFileSwitch.Visible = true;
				mnuFileShowSplitView.Visible = true;
				strWS = "S";
			}
			// set the service of what is showing
			for (intCT = 0; intCT <= cmbService.Items.Count - 1; intCT++)
			{
				if (Strings.Left(cmbService.Items[intCT].ToString(), 1) == strWS)
				{
					cmbService.SelectedIndex = intCT;
					break;
				}
			}
			// ElseIf SGRID.rows > 1 Then
			// mnuFileSwitch_Click
			// End If
		}

		private void mnuFileComment_Click(object sender, System.EventArgs e)
		{
			// MAL@20080129: Changed to get real account number
			// Tracker Reference: 12170
			// frmUTComment.Init GetAccountKeyUT(lngCurrentAccountUT)
			frmUTComment.InstancePtr.Init(ref modUTFunctions.Statics.lngCurrentAccountKeyUT);
			// kk01232017 trout-1225    ' lngCurrentAccountUT
			// If Trim(GetUTAccountComment(GetAccountKeyUT(lngCurrentAccountUT))) <> "" Then
			if (Strings.Trim(modUTFunctions.GetUTAccountComment(ref modUTFunctions.Statics.lngCurrentAccountKeyUT)) != "")//kk01232017 trout-1225    ' lngCurrentAccountUT
			{
				lblAcctComment.Visible = true;
			}
			else
			{
				lblAcctComment.Visible = false;
			}
		}

		public void mnuFileComment_Click()
		{
			mnuFileComment_Click(mnuFileComment, new System.EventArgs());
		}

		private void mnuFileConsumptionReport_Click(object sender, System.EventArgs e)
		{
			// MAL@20071217: Changed because it was getting the ID when it was already set
			// Tracker Reference: 11679
			// rptConsumptionHistory.Init GetAccountKeyUT(lngCurrentAccountUT), GetAccountKeyUT(lngCurrentAccountUT)
			rptConsumptionHistory.InstancePtr.Init(ref modUTFunctions.Statics.lngCurrentAccountUT, ref modUTFunctions.Statics.lngCurrentAccountUT);
		}

		private void mnuFileOptionsLoadValidAccount_Click(object sender, System.EventArgs e)
		{
			if (modGlobalConstants.Statics.gboolBD)
			{
				// txtAcctNumber.Text = frmLoadValidAccounts.Init(txtAcctNumber.Text)
				txtAcctNumber.TextMatrix(0, 0, frmLoadValidAccounts.InstancePtr.Init(txtAcctNumber.Text));
				txtAcctNumber.EditText = txtAcctNumber.TextMatrix(0, 0);
			}
		}

		private void mnuFilePrintRateInfo_Click(object sender, System.EventArgs e)
		{
			//arUTDInformationScreen.InstancePtr.Init(vsRateInfo.Rows, vsRateInfo.Cols, boolShowingWater);
		}

		private void mnuFileShowMeterInfo_Click(object sender, System.EventArgs e)
		{
			// this will show the meter detail report
			//arUTMeterDetail.InstancePtr.Hide();
			frmReportViewer.InstancePtr.Unload();
			// Doevents
			arUTMeterDetail.InstancePtr.Init(modUTFunctions.GetAccountKeyUT_2(FCConvert.ToInt32(lblAccount.Text)));
		}

		private void mnuFileShowSplitView_Click(object sender, System.EventArgs e)
		{
			// If boolPayment Then
			Format_Grid_8(false, false);
			mnuFileSwitch.Visible = false;
			// End If
			mnuFileShowSplitView.Visible = false;
		}

		private void mnuFileSwitch_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (boolShowingWater)
				{
					if (SGRID.Rows > 1)
					{
						WGRID.Visible = false;
						SGRID.Visible = true;
						boolShowingWater = false;
						mnuFileSwitch.Text = "Show Water Bills";
						Format_Grid_8(true, false);
					}
					else
					{
						mnuFileSwitch.Visible = false;
					}
				}
				else
				{
					if (WGRID.Rows > 1)
					{
						SGRID.Visible = false;
						WGRID.Visible = true;
						boolShowingWater = true;
						mnuFileSwitch.Text = "Show Sewer Bills";
						Format_Grid_8(true, true);
					}
					else
					{
						// turn off this option because there is nothing in this grid
						mnuFileSwitch.Visible = false;
					}
				}
				SetHeadingLabels();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Switching Service", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuFileSwitch_Click()
		{
			mnuFileSwitch_Click(mnuFileSwitch, new System.EventArgs());
		}

		private void SGRID_RowCollapsed(object sender, EventArgs e)
		{
			SGRID_Collapsed();
		}

		private void txtAcctNumber_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			clsDRWrapper rsF = new clsDRWrapper();
			boolDoNotContinueSave = false;
			if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gboolBD)
			{
				// check the fun and make sure that it matches the current type's fund
				if ((Strings.InStr(1, txtAcctNumber.EditText, "_", CompareConstants.vbBinaryCompare) == 0 || Strings.Left(txtAcctNumber.EditText, 1) == "M") && txtAcctNumber.EditText.Length > 2)
				{
					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
					//if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "W")
					if (Strings.Left(cmbService.Text, 1) == "W")
					{
						rsF.OpenRecordset("SELECT Account1 FROM Type WHERE TypeCode = 93", modExtraModules.strCRDatabase);
					}
					else
					{
						rsF.OpenRecordset("SELECT Account1 FROM Type WHERE TypeCode = 94", modExtraModules.strCRDatabase);
					}
					if (!rsF.EndOfFile())
					{
						if (modBudgetaryAccounting.GetFundFromAccount(txtAcctNumber.EditText) != modBudgetaryAccounting.GetFundFromAccount(rsF.Get_Fields_String("Account1")))
						{
							MessageBox.Show("This account's fund does not match the fund for this receipt type.", "Invalid Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							boolDoNotContinueSave = true;
						}
					}
				}
			}
		}

		private void txtNote_TextChanged(object sender, System.EventArgs e)
		{
			// kk03312015 trouts-127
			intCharCount = txtNote.Text.Length;
			lblRemainChars.Text = FCConvert.ToString((255 - intCharCount));
			if (FCConvert.ToInt16(FCConvert.ToDouble(lblRemainChars.Text)) <= 0)
			{
				lblRemainChars.Text = FCConvert.ToString(0);
			}
		}

		private void txtNote_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// kk03312015 trouts-127
			intCharCount = txtNote.Text.Length;
			lblRemainChars.Text = FCConvert.ToString((255 - intCharCount));
			if (FCConvert.ToInt16(FCConvert.ToDouble(lblRemainChars.Text)) <= 0)
			{
				lblRemainChars.Text = FCConvert.ToString(0);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTax_Enter(object sender, System.EventArgs e)
		{
			txtTax.SelectionStart = 0;
			txtTax.SelectionLength = txtTax.Text.Length;
		}

		private void txtTax_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtTax.SelectionStart == 0)
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtTax.SelectionStart == txtTax.Text.Length)
						{
							txtInterest.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtTax_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if ((KeyAscii == Keys.Back) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else if (KeyAscii == Keys.Delete)
			{
				// decimal point
				lngDecPlace = Strings.InStr(1, txtTax.Text, ".", CompareConstants.vbBinaryCompare);
				if (lngDecPlace != 0)
				{
					if (txtTax.SelectionStart < lngDecPlace && txtTax.SelectionLength + txtTax.SelectionStart >= lngDecPlace)
					{
						// if the decimal place is being highlighted and will be written over, then allow it
					}
					else
					{
						// if there is already a decimal point then stop others
						KeyAscii = (Keys)0;
					}
				}
			}
			else if (KeyAscii == Keys.Insert)
			{
				// minus
				KeyAscii = (Keys)0;
				if (Conversion.Val(txtTax.Text) == 0)
				{
					txtTax.Text = "0.00";
				}
				txtTax.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtTax.Text)) * -1, "#,##0.00");
				txtTax.SelectionStart = 0;
				txtTax.SelectionLength = txtTax.Text.Length;
			}
			else if (KeyAscii == Keys.Execute)
			{
				// plus
				KeyAscii = (Keys)0;
				if (Conversion.Val(txtTax.Text) == 0)
				{
					txtTax.Text = "0.00";
				}
				txtTax.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtTax.Text)), "#,##0.00");
				txtTax.SelectionStart = 0;
				txtTax.SelectionLength = txtTax.Text.Length;
			}
			else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
			{
				// c, C - this will clear the box
				KeyAscii = (Keys)0;
				txtTax.Text = "0.00";
				txtTax.SelectionStart = 0;
				txtTax.SelectionLength = 4;
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTax_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this is validaiting the amount entered
				if (txtTax.Text != "")
				{
					if (FCConvert.ToDecimal(txtTax.Text) > 99999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtTax.Text = Strings.Format(99999999.99, "#,##0.00");
					}
					else if (FCConvert.ToDecimal(txtTax.Text) < -99999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $-99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtTax.Text = Strings.Format(-99999999.99, "#,##0.00");
					}
					else
					{
						txtTax.Text = Strings.Format(FCConvert.ToDecimal(txtTax.Text), "#,##0.00");
					}
				}
				else
				{
					txtTax.Text = "0.00";
				}
				return;
			}
			catch (Exception ex)
			{
				
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsRateInfo_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsRateInfo.GetFlexRowIndex(e.RowIndex) == 13)
			{
				// kk Moved InterestAppliedDate row from 12 to 13
				if (vsRateInfo.GetFlexColIndex(e.ColumnIndex) == 4)
				{
					vsRateInfo.EditMask = "##/##/####";
				}
				else
				{
					vsRateInfo.EditMask = "";
				}
			}
			else
			{
				vsRateInfo.EditMask = "";
			}
		}

		private void vsRateInfo_RowColChange(object sender, System.EventArgs e)
		{
			if (vsRateInfo.Row == 13)
			{
				// kk Moved InterestAppliedDate row from 12 to 13
				if (vsRateInfo.Col == 4)
				{
					vsRateInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				else
				{
					vsRateInfo.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			else
			{
				vsRateInfo.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsRateInfo_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            bool tempExecutingvsRateInfoValidateEdit = executingvsRateInfoValidateEdit;
            try
            {
                if (executingvsRateInfoValidateEdit)
                {
                    return;
                }
                executingvsRateInfoValidateEdit = true;
                string strTemp = "";
                if (vsRateInfo.GetFlexColIndex(e.ColumnIndex) == 4 && vsRateInfo.GetFlexRowIndex(e.RowIndex) == 13)
                {
                    // kk Moved InterestAppliedDate row from 12 to 13
                    // this is the applied through date
                    if (Information.IsDate(vsRateInfo.EditText))
                    {
                        if (DateAndTime.DateValue(vsRateInfo.EditText).ToOADate() != 0)
                        {
                            // allow and change if different
                            strTemp = Strings.Trim(vsRateInfo.TextMatrix(13, 4));
                            // kk Moved InterestAppliedDate row from 12 to 13
                            if (Strings.Trim(vsRateInfo.EditText) != strTemp)
                            {
                                switch (MessageBox.Show("Are you sure that you would like to edit the Interest Paid Date.", "Edit Date", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                                {
                                    case DialogResult.Yes:
                                    {
                                        // TODO: Field [Bill] not found!! (maybe it is an alias?)
                                        modGlobalFunctions.AddCYAEntry_728("UT", "Edit Applied Through Date", "Account - " + FCConvert.ToString(CurrentAccount), "Bill ID : " + rsCL.Get_Fields("Bill"), "New - " + vsRateInfo.EditText, "Old - " + vsRateInfo.TextMatrix(13, 4));
                                        // kk Moved InterestAppliedDate row from 12 to 13
                                        // MAL@20080312: Changed to take which grid is the active grid used to bring up the right-click
                                        // Tracker Reference: 12677
                                        // SaveNewAppliedThroughDate boolShowingWater, CDate(vsRateInfo.EditText), rsCL.Fields("Bill")
                                        // TODO: Field [Bill] not found!! (maybe it is an alias?)
                                        string editedValue = vsRateInfo.EditText;
                                        //FC:FINAL:SBE - #3795 - after MessageBox, current cell is not in EditMode anymore, and EditText returns empty string. 
                                        //In this case return the TextMatrix value, which is already updated after cell is not in edit mode
                                        if (!vsRateInfo.IsCurrentCellInEditMode)
                                        {
                                            editedValue = vsRateInfo.TextMatrix(13, 4);
                                        }
                                        //SaveNewAppliedThroughDate_26(FCConvert.CBool(strCurrentGridSelect == "W"), DateAndTime.DateValue(vsRateInfo.EditText), FCConvert.ToInt32(rsCL.Get_Fields("Bill")));
                                        SaveNewAppliedThroughDate_26(FCConvert.CBool(strCurrentGridSelect == "W"), DateAndTime.DateValue(editedValue), FCConvert.ToInt32(rsCL.Get_Fields("Bill")));
                                        break;
                                    }
                                    case DialogResult.No:
                                    {
                                        e.Cancel = true;
                                        vsRateInfo.Editable = FCGrid.EditableSettings.flexEDNone;
                                        break;
                                    }
                                    case DialogResult.Cancel:
                                    {
                                        vsRateInfo.EditText = strTemp;
                                        break;
                                    }
                                }
                                //end switch
                            }
                        }
                        else
                        {
                            e.Cancel = true;
                            MessageBox.Show("Please enter a valid date.", "Edit Applied Through Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        }
                    }
                    else
                    {
                        e.Cancel = true;
                        MessageBox.Show("Please enter a valid date.", "Edit Applied Through Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                executingvsRateInfoValidateEdit = tempExecutingvsRateInfoValidateEdit;
            }
		}

		private void SaveNewAppliedThroughDate_26(bool boolWater, DateTime dtNewDate, int lngRK)
		{
			SaveNewAppliedThroughDate(ref boolWater, ref dtNewDate, ref lngRK);
		}

		private void SaveNewAppliedThroughDate(ref bool boolWater, ref DateTime dtNewDate, ref int lngRK)
		{
			// this routine will set the new date on the bill/lien
			clsDRWrapper rsEdit = new clsDRWrapper();
			string strWS = "";
			if (boolWater)
			{
				strWS = "W";
			}
			else
			{
				strWS = "S";
			}
			rsEdit.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(CurrentAccount) + " AND ID = " + FCConvert.ToString(lngRK), modExtraModules.strUTDatabase);
			if (!rsEdit.EndOfFile())
			{
				if (FCConvert.ToInt32(rsEdit.Get_Fields(strWS + "LienRecordNumber")) != 0)
				{
					// lien
					rsEdit.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsEdit.Get_Fields(strWS + "LienRecordNumber"), modExtraModules.strUTDatabase);
					if (!rsEdit.EndOfFile())
					{
						rsEdit.Edit();
						rsEdit.Set_Fields("IntPaidDate", dtNewDate);
						rsEdit.Update();
					}
					else
					{
						MessageBox.Show("Lien not found, date not changed.", "Error Saving Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				else
				{
					// regular
					rsEdit.Edit();
					rsEdit.Set_Fields(strWS + "IntPaidDate", dtNewDate);
					rsEdit.Update();
				}
			}
			else
			{
				MessageBox.Show("Bill not found, date not changed.", "Error Saving Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void GRID_RowCollapsed(object sender, EventArgs e)
		{
			WGRID_Collapsed();
		}

		private void WGRID_ClickEvent(object sender, System.EventArgs e)
		{
			// this will set the bill information to the bill being selected
			int cRow;
			int intCT;
			SetService_2("W");
			if (WGRID.RowOutlineLevel(WGRID.Row) == 1)
			{
				// .Col = lngGRIDColBillNumber And
				// check to make sure that the service combo is correct
				for (cRow = 0; cRow <= cmbService.Items.Count - 1; cRow++)
				{
					if (Strings.Left(cmbService.Items[cRow].ToString(), 1) == "W")
					{
						cmbService.SelectedIndex = cRow;
						modUTFunctions.FillUTBillNumber_8(true, false);
						break;
					}
				}
				cmbBillNumber.SelectedIndex = cmbBillNumber.Items.Count - 1;
				for (cRow = 0; cRow <= cmbBillNumber.Items.Count - 1; cRow++)
				{
					if (Strings.Trim(cmbBillNumber.Items[cRow].ToString()) == Strings.Trim(WGRID.TextMatrix(WGRID.Row, lngGRIDColBillNumber)))
					{
						cmbBillNumber.SelectedIndex = cRow;
						SetService_2("W");
						break;
					}
				}
			}
			else if (WGRID.RowOutlineLevel(WGRID.Row) == 0)
			{
				//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
				//if ("Auto" == Strings.Trim(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString()))
				if ("Auto" == Strings.Trim(cmbBillNumber.Text))
				{
					cmbBillNumber_Click();
				}
				else
				{
					for (intCT = 0; intCT <= cmbBillNumber.Items.Count - 1; intCT++)
					{
						if ("Auto" == Strings.Trim(cmbBillNumber.Items[intCT].ToString()).Replace("*", ""))
						{
							cmbBillNumber.SelectedIndex = intCT;
							SetService_2("W");
							break;
						}
					}
				}
			}
		}

		private void WGRID_Collapsed()
		{
			// this sub occurs when a gridline is collapsed or expanded
			// it will change the data in the main rows depending apon is status
			int temp;
			int counter;
			int rows = 0;
			int height1 = 0;
			bool DeptFlag = false;
			bool DivisionFlag = false;
			int cRow;
			int lngTempRow;
			// this will set the bill information to the bill being selected
			// lngTempRow = .MouseRow
			// If lngTempRow > 0 Then
			// If .RowOutlineLevel(lngTempRow) = 1 Then
			// For cRow = 0 To cmbBillNumber.ListCount - 1   'set the combo box
			// If Trim$(cmbBillNumber.List(cRow)) = Trim$(.TextMatrix(lngTempRow, lngGRIDColBillNumber)) Then
			// cmbBillNumber.ListIndex = cRow
			// SetService "W"
			// Exit For
			// End If
			// Next
			// End If
			// End If
			if (CollapseFlag == false)
			{
				for (counter = 1; counter <= WGRID.Rows - 1; counter++)
				{
					if (WGRID.RowOutlineLevel(counter) == 0)
					{
						if (WGRID.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							DeptFlag = true;
						}
						else
						{
							rows += 1;
							DeptFlag = false;
						}
					}
					else if (WGRID.RowOutlineLevel(counter) == 1)
					{
						if (DeptFlag == true)
						{
							// do nothing
						}
						else if (WGRID.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							DivisionFlag = true;
							// this function will show the totals on the header line and
							// put the original bill info on the empty line below the total line (hidden)
							// this means that this header row is closed and needs to show the totals on its line
							// MAL@20071105
							// If .TextMatrix(counter, lngGRIDColRef) = "Original" Or Left(.TextMatrix(counter, lngGRIDColRef), 4) = Left(strSupplemntalBillString, 4) Then
							if (WGRID.TextMatrix(counter, lngGRIDColRef) == "Original" || Strings.Left(WGRID.TextMatrix(counter, lngGRIDColRef), 3) == Strings.Left(strDemandBillString, 3) || Strings.Left(WGRID.TextMatrix(counter, lngGRIDColRef), 4) == Strings.Left(strSupplemntalBillString, 4))
							{
								SwapRows_8(counter, NextSpacerLine_6(counter, true), WGRID);
							}
						}
						else
						{
							rows += 1;
							DivisionFlag = false;
							// this function will show the original bill info on the header line and
							// put the totals on the empty line below the total line (hidden)
							// this means that that total row is showing so the totals must be on the total line not the header line
							if (WGRID.TextMatrix(counter, lngGRIDColRef) != "Original" && Strings.Left(WGRID.TextMatrix(counter, lngGRIDColRef), 4) != Strings.Left(strSupplemntalBillString, 4))
							{
								SwapRows_8(counter, NextSpacerLine_6(counter, true), WGRID);
							}
							// this will swap the two rows
						}
					}
					else
					{
						if (DeptFlag == true || DivisionFlag == true)
						{
							// do nothing
						}
						else
						{
							rows += 1;
						}
					}
				}
				height1 = (rows + 1) * WGRID.RowHeight(0);
				Adjust_GRID_Height_6(height1, true);
			}
		}

		private void WGRID_DblClick(object sender, System.EventArgs e)
		{
			int cRow = 0;
			int mRow = 0;
			int mCol = 0;
			clsDRWrapper rsBill = new clsDRWrapper();
			bool blnHasBillNum = false;
			mRow = WGRID.MouseRow;
			mCol = WGRID.MouseCol;
			cRow = WGRID.Row;
			if (mRow > 0 && mCol > 0)
			{
				// make sure that the user is not clicking on the top row to expand/collapse the grid
				if (WGRID.RowOutlineLevel(cRow) == 1)
				{
					if (WGRID.Col == lngGRIDColTotal || WGRID.Col == lngGRIDColPending)
					{
						// do nothing
					}
					else if (WGRID.Col == 1)
					{
						// set the year
						for (cRow = 0; cRow <= cmbBillNumber.Items.Count - 1; cRow++)
						{
							if (Strings.Trim(cmbBillNumber.Items[cRow].ToString()) == Strings.Trim(WGRID.TextMatrix(WGRID.Row, WGRID.Col)))
							{
								cmbBillNumber.SelectedIndex = cRow;
								SetService_2("W");
								break;
							}
						}
					}
					// expand the rows
					if (WGRID.IsCollapsed(WGRID.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						WGRID.IsCollapsed(WGRID.Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
					}
					else
					{
						WGRID.IsCollapsed(WGRID.Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
					}
				}
				else if (WGRID.RowOutlineLevel(cRow) == 2 && fraPayment.Visible == true && (WGRID.TextMatrix(cRow, lngGRIDColRef) != "CHGINT" && WGRID.TextMatrix(cRow, lngGRIDColRef) != "EARNINT" && WGRID.TextMatrix(cRow, lngGRIDColRef) != "CNVRSN"))
				{
					// MAL@20080616: Add check that user is not trying to reverse a payment from a liened bill
					// Tracker Reference: 14267
					if (WGRID.TextMatrix(LastParentRow_6(cRow, true), lngGridColBillKey) != "")
					{
						if (!IsLienedBill(FCConvert.ToInt32(WGRID.TextMatrix(LastParentRow_6(cRow, true), lngGridColBillKey)), "W"))
						{
							// reverse the payment on this line
							if (WGRID.TextMatrix(cRow, lngGRIDColRef) != "Billed To:")
							{
								if (Strings.Trim(WGRID.TextMatrix(cRow, lngGRIDColRef)) != "Total" && Strings.Trim(WGRID.TextMatrix(cRow, lngGRIDColRef)) != "CURINT")
								{
									if (Strings.Trim(WGRID.TextMatrix(cRow, lngGRIDColPaymentCode)) != "D")
									{
										if (MessageBox.Show("Are you sure that you would like to reverse this payment?", "Reverse Payment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
										{
											modUTFunctions.Statics.blnCreatedReverseInterest = false;
											// MAL@20080228: Reset the value
											modUTFunctions.CreateUTOppositionLine_24(cRow, true, true);
											// MAL@20070920: Added True for Reversal
										}
									}
									else
									{
										// reversing a discount
										if (MessageBox.Show("Are you sure that you would like to reverse this discount?", "Reverse Payment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
										{
											modUTFunctions.Statics.blnCreatedReverseInterest = false;
											// MAL@20080228: Reset the value
											modUTFunctions.CreateUTOppositionLine_24(cRow, true, true);
											// MAL@20070920: Added True for Reversal
										}
									}
								}
								else
								{
									//kk12152017 trout-1308/trocr-497  This code below seems to be misplaced - to get here the user dblclicked on the Total or CURINT row
									// MAL@20071102: Add check for missing bill number (conversion records)
									// Call Reference: 113872
									//rsBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + WGRID.TextMatrix(cRow, lngGridColBillKey), modExtraModules.strUTDatabase);
									//if (rsBill.RecordCount() > 0)
									//{
									//    if (FCConvert.ToInt32(rsBill.Get_Fields("BillNumber"))) == 0 || FCConvert.ToString(rsBill.Get_Fields("BillNumber")) == "")
									//    {
									//        blnHasBillNum = false;
									//    }
									//    else
									//    {
									//        blnHasBillNum = true;
									//    }
									//}
									//else
									//{
									//    blnHasBillNum = false;
									//}
									//if (!blnHasBillNum)
									//{
									//    MessageBox.Show("This item cannot be reversed automatically." + "\r\n" + "You will need to do a manual correction.", "No Automatic Reversal", MessageBoxButtons.OK, MessageBoxIcon.Information);
									//}
									//else
									//{
									//    if (boolDisableInsert)
									//    { // kk03132015  trocrs-36  Add option to disable automatically filling in payment amount
									//        return;
									//    }
									//    modUTFunctions.CreateUTOppositionLine_24(cRow, true, true); // MAL@20070920: Added True for Reversal
									//}
									modUTFunctions.CreateUTOppositionLine_6(cRow, true);
									//kk12152017 trocr-497  The user clicked on the Total or CURINT line. Not a reversal
								}
							}
						}
					}
					else
					{
						if (boolDisableInsert)
						{
							// kk03132015  trocrs-36  Add option to disable automatically filling in payment amount
							return;
						}
						modUTFunctions.CreateUTOppositionLine_24(cRow, true, false);
					}
				}
				else if (WGRID.RowOutlineLevel(cRow) == 0)
				{
					// this is the total account line
					if (boolDisableInsert)
					{
						// kk03132015  trocrs-36  Add option to disable automatically filling in payment amount
						return;
					}
					modUTFunctions.CreateUTOppositionLine_24(cRow, true, true);
					// MAL@20070920: Added True for Reversal
				}
			}
		}

		private void WGRID_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void WGRID_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 32)
			{
				// spacebar
				keyAscii = 0;
				WGRID_DblClick(sender, e);
			}
			else if (keyAscii == 13 && WGRID.RowOutlineLevel(WGRID.Row) == 1)
			{
				keyAscii = 0;
				WGRID_ClickEvent(sender, e);
			}
		}

		private void WGRID_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// catch the right click to show the rate information about that year
			int lngMRow = 0;
			int lngBK = 0;
			lngMRow = WGRID.MouseRow;
			//kk12122017 trout-706  Always set this. We'll use it in mnuFileDischarge
			strCurrentGridSelect = "W";
			if (e.Button == MouseButtons.Right && lngMRow >= 0)
			{
				// right click
				if (WGRID.RowOutlineLevel(lngMRow) == 1)
				{
					WGRID.Select(lngMRow, 1);
					// this will force the information to be correct by selecting the correct row before looking at the labels
					if (Conversion.Val(WGRID.TextMatrix(lngMRow, lngGRIDColPaymentKey)) >= 0)
					{
						lngBK = FCConvert.ToInt32(Math.Round(Conversion.Val(WGRID.TextMatrix(lngMRow, lngGRIDColPaymentKey))));
					}
					else
					{
						if (WGRID.IsCollapsed(lngMRow) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							// kgk trout-722  Added to be consistent with Sewer
							rsCL.OpenRecordset("SELECT ID FROM Bill WHERE ID = " + FCConvert.ToString(Conversion.Val(WGRID.TextMatrix(lngMRow, lngGRIDColPaymentKey)) * -1), modExtraModules.strUTDatabase);
						}
						else
						{
							rsCL.OpenRecordset("SELECT ID FROM Bill WHERE WLienRecordNumber = " + FCConvert.ToString(Conversion.Val(WGRID.TextMatrix(lngMRow, lngGRIDColPaymentKey)) * -1), modExtraModules.strUTDatabase);
						}
						if (!rsCL.EndOfFile())
						{
							lngBK = rsCL.Get_Fields_Int32("ID");
						}
						else
						{
							lngBK = 0;
						}
					}
					rsCL.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE Bill = " + FCConvert.ToString(lngBK), modExtraModules.strUTDatabase);
					if (rsCL.EndOfFile() != true && rsCL.BeginningOfFile() != true)
					{
						ShowRateInfo_168(lngMRow, 1, rsCL, WGRID, true);
					}
				}
				else if (WGRID.RowOutlineLevel(lngMRow) == 2)
				{
					ShowPaymentInfo(lngMRow, WGRID.MouseCol, WGRID);
				}
			}
			//kk12122017 trout-706 strCurrentGridSelect = "W";
		}

       //private void WGRID_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
       private void WGRID_SetToolTips(int lngMR, int lngMC)
        {
            // as the mouse moves over the grid, the tooltip will change depending on which cell the pointer is currently in
            string strTemp = "";
            //int lngMR = 0;
            //int lngMC = 0;
            //lngMR = WGRID.GetFlexRowIndex(e.RowIndex);
            //lngMC = SGRID.GetFlexColIndex(e.ColumnIndex);
            if (lngMR == 0)
            {
                if (lngMC == lngGRIDCOLYear || lngMC == lngGRIDColBillNumber)
                {
                    // Year
                    if (lngMR == WGRID.Rows - 1)
                    {
                        strTemp = "Effective Date : " + WGRID.TextMatrix(lngMR, lngMC);
                    }
                    else
                    {
                        strTemp = "";
                    }
                    //cell.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColDate)
                {
                    // Date
                    strTemp = "Recorded Date";
                    //cell.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColRef)
                {
                    // Reference
                    strTemp = "";
                    //cell.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColService)
                {
                    // Period
                    strTemp = "";
                    //cell.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColPaymentCode)
                {
                    // Code
                    strTemp += " 3 - 30 Day Notice Fee" + ", ";
                    strTemp += " A - Abatement" + ", ";
                    strTemp += " C - Correction" + ", ";
                    strTemp += " D - Discount" + ", ";
                    strTemp += " I - Interest" + ", ";
                    strTemp += " P - Payment" + ", ";
                    strTemp += " R - Refunded Abatement" + ", ";
                    strTemp += " S - Supplemental" + ", ";
                    strTemp += " U - Tax Club" + ", ";
                    strTemp += " Y - PrePayment";
                    //cell.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColPTC)
                {
                    strTemp = "Principal + Tax + Costs";
                    //cell.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColPrincipal)
                {
                    // Prin
                    strTemp = "";
                    //cell.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColInterest || lngMC == lngGRIDColTax)
                {
                    // Int
                    strTemp = "";
                    //cell.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColCosts)
                {
                    // Costs
                    strTemp = "";
                    //cell.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColTotal)
                {
                    // Total
                    strTemp = "";
                    //cell.ToolTipText = strTemp;
                }
                else
                {
                    strTemp = "";
                    //cell.ToolTipText = strTemp;
                }
                WGRID.Columns[lngMC - 1].HeaderCell.ToolTipText = strTemp;
            }
            else //if(WGRID.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                DataGridViewCell cell = WGRID[lngMC - 1, lngMR - 1];
                //lngMR = WGRID.MouseRow;
                if (lngMR > 0)
                {
                    if (WGRID.TextMatrix(lngMR, lngGRIDColLineCode) != "" && WGRID.TextMatrix(lngMR, lngGRIDColLineCode) != "=")
                    {
                        if (WGRID.TextMatrix(lngMR, lngGRIDColRef) != "CURINT" && WGRID.TextMatrix(lngMR, lngGRIDColRef) != "EARNINT")
                        {
                            if (WGRID.RowOutlineLevel(lngMR) == 1 && lngMC == lngGRIDColBillNumber)
                            {
                                // MAL@20080602: Add check for first column and then show tooltip with rate ID description
                                // Tracker Reference: 11820
                                if (lngMC == lngGRIDColBillNumber && WGRID.TextMatrix(lngMR, lngGRIDColBillNumber) != "")
                                {
                                    strTemp = GetBillDescription(WGRID.TextMatrix(lngMR, lngGRIDColBillNumber), WGRID.TextMatrix(lngMR, lngGRIDColDate), WGRID.TextMatrix(lngMR, lngGRIDColRef), "W");
                                    cell.ToolTipText = strTemp;
                                }
                                else
                                {
                                    cell.ToolTipText = "Right-Click for Rate information and totals.";
                                }
                            }
                            else if (WGRID.RowOutlineLevel(lngMR) == 2)
                            {
                                cell.ToolTipText = "Right-Click for Payment information.";
                            }
                            else
                            {
                                // MAL@20080602: Add check for first column and then show tooltip with rate ID description
                                // Tracker Reference: 11820
                                if (lngMC == lngGRIDColBillNumber && WGRID.TextMatrix(lngMR, lngGRIDColBillNumber) != "")
                                {
                                    strTemp = GetBillDescription(WGRID.TextMatrix(lngMR, lngGRIDColBillNumber), WGRID.TextMatrix(lngMR, lngGRIDColDate), WGRID.TextMatrix(lngMR, lngGRIDColRef), "W");
                                    cell.ToolTipText = strTemp;
                                }
                                // .ToolTipText = ""
                            }
                        }
                        else
                        {
                            cell.ToolTipText = "";
                        }
                    }
                    else
                    {
                        // MAL@20080602: Add check for first column and then show tooltip with rate ID description
                        // Tracker Reference: 11820
                        if (lngMC == lngGRIDColBillNumber && WGRID.TextMatrix(lngMR, lngGRIDColBillNumber) != "")
                        {
                            strTemp = GetBillDescription(WGRID.TextMatrix(lngMR, lngGRIDColBillNumber), WGRID.TextMatrix(lngMR, lngGRIDColDate), WGRID.TextMatrix(lngMR, lngGRIDColRef), "W");
                            cell.ToolTipText = strTemp;
                        }
                        // .ToolTipText = ""
                    }
                }
                else
                {
                    cell.ToolTipText = "";
                }
            }
        }

		private void WGRID_RowColChange(object sender, System.EventArgs e)
		{
			// this will change the labels depending apon which line is click
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (WGRID.RowOutlineLevel(WGRID.Row) == 1)
			{
				rsTemp.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE BillNumber = " + WGRID.TextMatrix(WGRID.Row, lngGRIDColBillNumber) + " AND AccountKey = " + FCConvert.ToString(CurrentAccountKey), modExtraModules.strUTDatabase);
				if (rsTemp.EndOfFile() || rsTemp.BeginningOfFile())
				{
				}
				else
				{
					rsTemp.MoveFirst();
					// TODO: Field [Bill] not found!! (maybe it is an alias?)
					FillLabels_2(FCConvert.ToInt32(rsTemp.Get_Fields("Bill")));
				}
			}
			rsTemp.Reset();
		}
		// vbPorter upgrade warning: CurRow As Variant --> As int
		public int LastParentRow_6(int CurRow, bool boolWater)
		{
			return LastParentRow(CurRow, ref boolWater);
		}

		public int LastParentRow(int CurRow, ref bool boolWater)
		{
			int LastParentRow = 0;
			int l = 0;
			int curLvl = 0;
			if (CurRow > 0)
			{
				if (boolWater)
				{
					l = CurRow;
					curLvl = WGRID.RowOutlineLevel(l);
					if (curLvl > 1)
					{
						l -= 1;
						while (WGRID.RowOutlineLevel(l) > 1)
						{
							l -= 1;
						}
					}
				}
				else
				{
					l = CurRow;
					curLvl = SGRID.RowOutlineLevel(l);
					if (curLvl > 1)
					{
						l -= 1;
						while (SGRID.RowOutlineLevel(l) > 1)
						{
							l -= 1;
						}
					}
				}
				LastParentRow = l;
			}
			return LastParentRow;
		}

		private void FillMainLabels()
		{
			// from the bill record then from the RE account
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp;
			string strTemp2 = "";
			int intCT;
			// sets the current information at the top of the screen
			// strTemp = "SELECT * FROM (" & strUTCurrentAccountBills & ") WHERE Bill = " & rsCL.Fields("Bill") & " AND BillingType = 'RE' ORDER BY BillingYear Desc"
			// lblLand.Text = "Land"
			// 
			// rsTemp.OpenRecordset strTemp, strUTDatabase
			// If Not (rsTemp.EndOfFile()) And Not (rsTemp.BeginningOfFile) Then
			// With rsTemp
			// lblLandValue.Text = Format(Val(.Fields("LandValue")), "#,###")
			// lblBuildingValue.Text = Format(Val(.Fields("BuildingValue")), "#,###")
			// lblExemptValue.Text = Format(Val(.Fields("ExemptValue")), "#,###")
			// lblTotalValue.Text = Format(Val(.Fields("LandValue")) + Val(.Fields("BuildingValue")) - Val(.Fields("ExemptValue")), "#,###")
			// End With
			// End If
			// UT Account Info
			strTemp = "SELECT * FROM Master WHERE ID = " + FCConvert.ToString(CurrentAccountKey);
			if (rsTemp.OpenRecordset(strTemp, modExtraModules.strUTDatabase))
			{
				if (!rsTemp.EndOfFile())
				{
					boolUseMHFromRE = FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("UseMortgageHolder"));
					boolUseREInformation = FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("UseREAccount"));
					lngAssociatedREAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields_Int32("REAccount"))));
					// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					lblTaxAcquired.Visible = modUTFunctions.IsAccountTaxAcquired_7(FCConvert.ToInt32(rsTemp.Get_Fields("AccountNumber")));
					// lblReference1.Text = Trim$(rsTemp.Fields("RSREF1") & " ")
					// strMailingAddress1 = Trim(rsTemp.Fields("RSAddr1")) & " " & Trim(rsTemp.Fields("RSAddr2"))
					// strMailingAddress2 = Trim(rsTemp.Fields("RSAddr3")) & ", " & Trim(rsTemp.Fields("RSState")) & " " & Trim(rsTemp.Fields("RSZip"))
				}
			}
			// strTemp = "SELECT Acres FROM Master WHERE ID = " & CurrentAccountKey
			// If rsTemp.OpenRecordset(strTemp, strREDatabase) Then
			// If Not (rsTemp.EndOfFile()) And Not (rsTemp.BeginningOfFile) Then
			// With rsTemp
			// lblAcreage.Text = Trim$(rsTemp.Fields("Acres") & " ")
			// End With
			// End If
			// End If
			// strTemp = "SELECT * FROM BookPage WHERE Account = " & CurrentAccountRE & " AND Card = 1 ORDER BY Line desc"
			// If rsTemp.OpenRecordset(strTemp, strREDatabase) Then
			// If rsTemp.EndOfFile Then
			// strCurrentBookPage = "Not Found"
			// Else
			// strCurrentBookPage = "B" & rsTemp.Fields("Book") & " P" & rsTemp.Fields("Page")
			// End If
			// End If
			// kk03212014 trocl-816  Change group info display
			// kgk trocl-739 04-27-11  Add indicator for all associations in UT also
			//strTemp = "";
			//         if (lngGroupNumber != 0)
			//         {
			//             rsTemp.OpenRecordset("SELECT * FROM ModuleAssociation INNER JOIN GroupMaster ON ModuleAssociation.GroupNumber = GroupMaster.ID WHERE GroupMaster.GroupNumber = " + FCConvert.ToString(lngGroupNumber) + " AND NOT (Module = 'UT' AND Account = " + FCConvert.ToString(CurrentAccount) + ") ORDER BY Module", "CentralData");
			//             while (!rsTemp.EndOfFile())
			//             {
			//                 if (Strings.InStr(strTemp, FCConvert.ToString(rsTemp.Get_Fields("Module")), CompareConstants.vbBinaryCompare) == 0)
			//                 {
			//                     if (strTemp == "")
			//                     {
			//                         strTemp = FCConvert.ToString(rsTemp.Get_Fields("Module"));
			//                     }
			//                     else
			//                     {
			//                         strTemp += " " + rsTemp.Get_Fields("Module");
			//                     }
			//                 }
			//                 if (strTemp2 == "")
			//                 {
			//                     strTemp2 = "Associated Accounts: " + rsTemp.Get_Fields("Module") + " #" + rsTemp.Get_Fields("Account");
			//                 }
			//                 else
			//                 {
			//                     strTemp2 += ", " + rsTemp.Get_Fields("Module") + " #" + rsTemp.Get_Fields("Account");
			//                 }
			//                 rsTemp.MoveNext();
			//             }
			//         }
			//         if (strTemp != "")
			//         {
			//             lblGrpInfo.Text = strTemp;
			//             ToolTip1.SetToolTip(lblGrpInfo, strTemp2);
			//             lblGrpInfo.Visible = true;
			//         }
			//         else
			//         {
			//             lblGrpInfo.Text = "";
			//             lblGrpInfo.Visible = false;
			//         }
			// kk03212014 trocl-816  Change group info display
			FillGroupInfo();
			//trouts-266 code freeze
			FillACHInfo();
			rsTemp.Reset();
		}
		// vbPorter upgrade warning: Grid As object	OnWrite(VSFlex7LCtl.VSFlexGrid)
		private void SwapRows_8(int x, int y, FCGrid Grid)
		{
			SwapRows(ref x, ref y, ref Grid);
		}

		private void SwapRows(ref int x, ref int y, ref FCGrid Grid)
		{
			// this function will switch the two rows values in the grid
			int intCol;
			string strTemp = "";
			for (intCol = lngGRIDColRef; intCol <= Grid.Cols - 1; intCol++)
			{
				strTemp = Grid.TextMatrix(x, intCol);
				Grid.TextMatrix(x, intCol, Grid.TextMatrix(y, intCol));
				Grid.TextMatrix(y, intCol, strTemp);
			}
		}

		private int NextSpacerLine_6(int x, bool boolWater, string strCheck = "=")
		{
			return NextSpacerLine(x, ref boolWater, strCheck);
		}

		private int NextSpacerLine(int x, ref bool boolWater, string strCheck = "=")
		{
			int NextSpacerLine = 0;
			// finds the next spacer line after the row number passed in by finding the next total line
			if (boolWater)
			{
				NextSpacerLine = WGRID.FindRow(strCheck, x, lngGRIDColLineCode) + 1;
			}
			else
			{
				NextSpacerLine = SGRID.FindRow(strCheck, x, lngGRIDColLineCode) + 1;
			}
			return NextSpacerLine;
		}

		private void WGRID_Coloring(int l)
		{
			// this sub will recursively call itself in order to color all of the total lines and spacer lines
			int lngTemp;
			lngTemp = NextSpacerLine_6(l, true);
			if (lngTemp > 0)
			{
				WGRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngTemp, 0, lngTemp, WGRID.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249)));
				// &H80000016 'this will hide the text that is living inside of the spacer row
				WGRID.Select(lngTemp - 1, lngGRIDColPrincipal, lngTemp - 1, lngGRIDColTotal);
				// this will color the totals lines
				WGRID.CellBorder(Color.Blue, 0, 1, 0, 0, 0, 0);
				WGRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngTemp - 1, 1, lngTemp - 1, WGRID.Cols - 1, true);
				if (Conversion.Val(WGRID.TextMatrix(lngTemp - 1, lngGRIDColPrincipal)) != 0)
				{
					sumWPrin += FCConvert.ToDouble(WGRID.TextMatrix(lngTemp - 1, lngGRIDColPrincipal));
				}
				if (Conversion.Val(WGRID.TextMatrix(lngTemp - 1, lngGRIDColInterest)) != 0)
				{
					sumWInt += FCConvert.ToDouble(WGRID.TextMatrix(lngTemp - 1, lngGRIDColInterest));
				}
				if (Conversion.Val(WGRID.TextMatrix(lngTemp - 1, lngGRIDColTax)) != 0)
				{
					sumWTax += FCConvert.ToDouble(WGRID.TextMatrix(lngTemp - 1, lngGRIDColTax));
				}
				if (Conversion.Val(WGRID.TextMatrix(lngTemp - 1, lngGRIDColCosts)) != 0)
				{
					sumWCost += FCConvert.ToDouble(WGRID.TextMatrix(lngTemp - 1, lngGRIDColCosts));
				}
				if (Conversion.Val(WGRID.TextMatrix(lngTemp - 1, lngGRIDColTotal)) != 0)
				{
					sumWTotal += FCConvert.ToDouble(WGRID.TextMatrix(lngTemp - 1, lngGRIDColTotal));
				}
				WGRID_Coloring(lngTemp + 1);
				// try again recursively
			}
			else
			{
				WGRID.Select(0, 1);
			}
		}

		private void SGRID_Coloring(int l)
		{
			// this sub will recursively call itself in order to color all of the total lines and spacer lines
			int lngTemp;
			lngTemp = NextSpacerLine_6(l, false);
			if (lngTemp > 0)
			{
				SGRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngTemp, 0, lngTemp, SGRID.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249)));
				// &H80000016 'this will hide the text that is living inside of the spacer row
				SGRID.Select(lngTemp - 1, lngGRIDColPrincipal, lngTemp - 1, lngGRIDColTotal);
				// this will color the totals lines
				SGRID.CellBorder(Color.Blue, 0, 1, 0, 0, 0, 0);
				SGRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngTemp - 1, 1, lngTemp - 1, SGRID.Cols - 1, true);
				if (Strings.Trim(SGRID.TextMatrix(lngTemp - 1, lngGRIDColPrincipal)) != "")
				{
					sumSPrin += FCConvert.ToDouble(SGRID.TextMatrix(lngTemp - 1, lngGRIDColPrincipal));
				}
				if (Strings.Trim(SGRID.TextMatrix(lngTemp - 1, lngGRIDColInterest)) != "")
				{
					sumSInt += FCConvert.ToDouble(SGRID.TextMatrix(lngTemp - 1, lngGRIDColInterest));
				}
				if (Strings.Trim(SGRID.TextMatrix(lngTemp - 1, lngGRIDColTax)) != "")
				{
					sumSTax += FCConvert.ToDouble(SGRID.TextMatrix(lngTemp - 1, lngGRIDColTax));
				}
				if (Strings.Trim(SGRID.TextMatrix(lngTemp - 1, lngGRIDColCosts)) != "")
				{
					sumSCost += FCConvert.ToDouble(SGRID.TextMatrix(lngTemp - 1, lngGRIDColCosts));
				}
				if (Strings.Trim(SGRID.TextMatrix(lngTemp - 1, lngGRIDColTotal)) != "")
				{
					sumSTotal += FCConvert.ToDouble(SGRID.TextMatrix(lngTemp - 1, lngGRIDColTotal));
				}
				SGRID_Coloring(lngTemp + 1);
				// try again recursively
			}
			else
			{
				SGRID.Select(0, 1);
			}
		}
		// vbPorter upgrade warning: Grid As object	OnWrite(VSFlex7LCtl.VSFlexGrid)
		private bool PaymentRecords(int BNumber, bool boolUseLien, FCGrid Grid, bool boolWater)
		{
			bool PaymentRecords = false;
			// VB6 Bad Scope Dim:
			int i;
			// this function will create rows subordinate to the master line for each bill record
			// and in the grid fill them with each payment made against this account
			// this function will return True if Interest Should be calculated for it
			clsDRWrapper rsRK = new clsDRWrapper();
			clsDRWrapper rsPy = new clsDRWrapper();
			// Payroll Recordset
			int intType = 0;
			int lngCount = 0;
			int lngCurrentRow = 0;
			string strLienSQl;
			string strWS = "";
			double[] dblLienTotals = new double[3 + 1];
			strLienSQl = " AND ISNULL(Lien, 0) = 1";
			if (boolWater)
			{
				strSQL = "SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE BillKey = " + FCConvert.ToString(BNumber) + " AND Service <> 'S' AND ReceiptNumber <> -1 ORDER BY RecordedTransactionDate, ActualSystemDate, ReceiptNumber, ID ";
				intType = 0;
				strWS = "W";
			}
			else
			{
				strSQL = "SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE BillKey = " + FCConvert.ToString(BNumber) + " AND Service <> 'W' AND ReceiptNumber <> -1 ORDER BY RecordedTransactionDate, ActualSystemDate, ReceiptNumber, ID ";
				intType = 1;
				strWS = "S";
			}
			if (!boolUseLien)
			{
				// payment lines --- all that apply
				rsPy.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				lngCount = rsPy.RecordCount();
				if (lngCount != 0)
				{
					rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsCL.Get_Fields_Int32("BillingRatekey"), modExtraModules.strUTDatabase);
					for (i = 1; i <= lngCount; i++)
					{
						if (FCConvert.ToDouble(Strings.Trim(rsPy.Get_Fields_Int32("ReceiptNumber") + " ")) != -1 && rsPy.Get_Fields_Boolean("LIEN") == boolUseLien)
						{
							if (boolWater)
							{
								CurRowW += 1;
								Add_6(CurRowW, 1, boolWater);
								lngCurrentRow = CurRowW;
							}
							else
							{
								CurRowS += 1;
								Add_6(CurRowS, 1, boolWater);
								lngCurrentRow = CurRowS;
							}
							// If rsPy.Fields("EffectiveInterestDate") <> 0 Then
							// .TextMatrix(lngCurrentRow, lngGRIDColDate) = rsPy.Fields("EffectiveInterestDate")
							// Else
							Grid.TextMatrix(lngCurrentRow, lngGRIDColDate, Strings.Trim(FCConvert.ToString(rsPy.Get_Fields_DateTime("RecordedTransactionDate")) + " "));
							// End If
							Grid.TextMatrix(lngCurrentRow, lngGRIDColRef, Strings.Trim(rsPy.Get_Fields_String("Reference") + " "));
							Grid.TextMatrix(lngCurrentRow, lngGRIDColService, Strings.Trim(rsPy.Get_Fields_String("Service") + " "));
							if (FCConvert.ToString(rsPy.Get_Fields("Code")) == "A")
							{
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								modUTFunctions.Statics.UTAbatePaymentsArray[rsRK.Get_Fields_Int32("ID"), intType].Per1 += FCConvert.ToDouble(rsPy.Get_Fields("Principal"));
							}
							Grid.TextMatrix(lngCurrentRow, lngGRIDColPaymentCode, Strings.Trim(rsPy.Get_Fields("Code") + " "));
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							Grid.TextMatrix(lngCurrentRow, lngGRIDColPrincipal, Strings.Trim(rsPy.Get_Fields("Principal") + " "));
							// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
							Grid.TextMatrix(lngCurrentRow, lngGRIDColTax, Strings.Trim(rsPy.Get_Fields("Tax") + " "));
							Grid.TextMatrix(lngCurrentRow, lngGRIDColInterest, Conversion.Val(rsPy.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPy.Get_Fields_Decimal("CurrentInterest")));
							Grid.TextMatrix(lngCurrentRow, lngGRIDColCosts, Conversion.Val(rsPy.Get_Fields_Decimal("LienCost")));
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
							Grid.TextMatrix(lngCurrentRow, lngGRIDColPTC, rsPy.Get_Fields("Principal") + rsPy.Get_Fields("Tax") + rsPy.Get_Fields_Decimal("LienCost"));
							Grid.TextMatrix(lngCurrentRow, lngGRIDColTotal, (FCConvert.ToDecimal(Grid.TextMatrix(lngCurrentRow, lngGRIDColPrincipal)) + FCConvert.ToDecimal(Grid.TextMatrix(lngCurrentRow, lngGRIDColTax)) + FCConvert.ToDecimal(Grid.TextMatrix(lngCurrentRow, lngGRIDColInterest)) + FCConvert.ToDecimal(Grid.TextMatrix(lngCurrentRow, lngGRIDColCosts))));
							Grid.TextMatrix(lngCurrentRow, lngGRIDColLineCode, "-");
							Grid.TextMatrix(lngCurrentRow, lngGRIDColPaymentKey, rsPy.Get_Fields_Int32("ID"));
							Grid.TextMatrix(lngCurrentRow, lngGRIDColCHGINTNumber, rsPy.Get_Fields_Int32("CHGINTNumber"));
						}
						if (FCConvert.ToString(rsPy.Get_Fields("Code")) == "U")
						{
							// this will check to see if a tax club payment was last, if so
							PaymentRecords = false;
							// do not calculate interest on this account
							boolNoCurrentInt = true;
						}
						else
						{
							PaymentRecords = true;
							boolNoCurrentInt = false;
						}
						rsPy.MoveNext();
					}
				}
				else
				{
					// no payments...
					PaymentRecords = true;
				}
			}
			if (((bcodeW == "LIEN" && boolWater) || (bcodeS == "LIEN" && !boolWater)) && boolUseLien)
			{
				// if this is liened then show the lien line
				if (boolWater)
				{
					CurRowW += 1;
					Add_6(CurRowW, 0, boolWater);
					lngCurrentRow = CurRowW;
					ParentLineW = lngCurrentRow;
					rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsLRW.Get_Fields_Int32("Ratekey"), modExtraModules.strUTDatabase);
					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngCurrentRow, lngGRIDColPrincipal, Strings.Trim(rsLRW.Get_Fields("Principal") + " "));
					// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngCurrentRow, lngGRIDColTax, Strings.Trim(rsLRW.Get_Fields("Tax") + " "));
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngCurrentRow, lngGRIDColInterest, Strings.Trim(rsLRW.Get_Fields("Interest") + " "));
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngCurrentRow, lngGRIDColCosts, Strings.Trim(rsLRW.Get_Fields("Costs") + " "));
					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngCurrentRow, lngGRIDColPTC, rsLRW.Get_Fields("Principal") + rsLRW.Get_Fields("Tax") + rsLRW.Get_Fields("Costs"));
					Grid.TextMatrix(lngCurrentRow, lngGRIDColLineCode, "+");
					Grid.TextMatrix(lngCurrentRow, lngGRIDColPaymentKey, Conversion.Val(rsLRW.Get_Fields_Int32("ID")) * -1);
					Grid.TextMatrix(lngCurrentRow, lngGRIDColBillNumber, Conversion.Val(rsLRW.Get_Fields_Int32("Ratekey")));
				}
				else
				{
					CurRowS += 1;
					Add_6(CurRowS, 0, boolWater);
					lngCurrentRow = CurRowS;
					ParentLineS = lngCurrentRow;
					rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsLRS.Get_Fields_Int32("Ratekey"), modExtraModules.strUTDatabase);
					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngCurrentRow, lngGRIDColPrincipal, Strings.Trim(rsLRS.Get_Fields("Principal") + " "));
					// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngCurrentRow, lngGRIDColTax, Strings.Trim(rsLRS.Get_Fields("Tax") + " "));
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngCurrentRow, lngGRIDColInterest, Strings.Trim(rsLRS.Get_Fields("Interest") + " "));
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngCurrentRow, lngGRIDColCosts, Strings.Trim(rsLRS.Get_Fields("Costs") + " "));
					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					Grid.TextMatrix(lngCurrentRow, lngGRIDColPTC, rsLRS.Get_Fields("Principal") + rsLRS.Get_Fields("Tax") + rsLRS.Get_Fields("Costs"));
					Grid.TextMatrix(lngCurrentRow, lngGRIDColLineCode, "+");
					Grid.TextMatrix(lngCurrentRow, lngGRIDColPaymentKey, Conversion.Val(rsLRS.Get_Fields_Int32("ID")) * -1);
					Grid.TextMatrix(lngCurrentRow, lngGRIDColBillNumber, Conversion.Val(rsLRS.Get_Fields_Int32("RateKey")));
				}
				dblLienTotals[0] = Conversion.Val(Grid.TextMatrix(lngCurrentRow, lngGRIDColPrincipal));
				dblLienTotals[1] = Conversion.Val(Grid.TextMatrix(lngCurrentRow, lngGRIDColTax));
				dblLienTotals[2] = Conversion.Val(Grid.TextMatrix(lngCurrentRow, lngGRIDColInterest));
				dblLienTotals[3] = Conversion.Val(Grid.TextMatrix(lngCurrentRow, lngGRIDColCosts));
				if (rsRK.EndOfFile() || rsRK.BeginningOfFile())
				{
					Grid.TextMatrix(lngCurrentRow, lngGRIDColDate, "*");
				}
				else
				{
					rsRK.MoveFirst();
					Grid.TextMatrix(lngCurrentRow, lngGRIDColDate, Strings.Format(rsRK.Get_Fields_DateTime("BillDate"), "MM/dd/yy") + "*");
				}
				Grid.TextMatrix(lngCurrentRow, lngGRIDColRef, "Lien #" + FCConvert.ToString(BNumber));
				// rsLRS.Fields("LienKey")
				Grid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngCurrentRow, lngGRIDColRef, true);
				Grid.TextMatrix(lngCurrentRow, lngGRIDColTotal, (FCConvert.ToDecimal(Grid.TextMatrix(lngCurrentRow, lngGRIDColPrincipal)) + FCConvert.ToDecimal(Grid.TextMatrix(lngCurrentRow, lngGRIDColTax)) + FCConvert.ToDecimal(Grid.TextMatrix(lngCurrentRow, lngGRIDColInterest)) + FCConvert.ToDecimal(Grid.TextMatrix(lngCurrentRow, lngGRIDColCosts))));
				// and show the lien payments on subsequent lines
				strSQL = "SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(CurrentAccountKey) + " AND BillKey = " + FCConvert.ToString(BNumber) + strLienSQl + " ORDER BY  RecordedTransactionDate, ID";
				rsPy.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				lngCount = rsPy.RecordCount();
				// clear the year of Pre Lien abatement info
				modUTFunctions.Statics.UTAbatePaymentsArray[rsCL.Get_Fields_Int32("BillNumber"), intType].Per1 = 0;
				modUTFunctions.Statics.UTAbatePaymentsArray[rsCL.Get_Fields_Int32("BillNumber"), intType].Per2 = 0;
				modUTFunctions.Statics.UTAbatePaymentsArray[rsCL.Get_Fields_Int32("BillNumber"), intType].Per3 = 0;
				modUTFunctions.Statics.UTAbatePaymentsArray[rsCL.Get_Fields_Int32("BillNumber"), intType].Per4 = 0;
				if (lngCount != 0)
				{
					for (i = 1; i <= lngCount; i++)
					{
						if (FCConvert.ToDouble(Strings.Trim(rsPy.Get_Fields_Int32("ReceiptNumber") + " ")) != -1)
						{
							if (boolWater)
							{
								CurRowW += 1;
								Add_6(CurRowW, 1, boolWater);
								lngCurrentRow = CurRowW;
							}
							else
							{
								CurRowS += 1;
								Add_6(CurRowS, 1, boolWater);
								lngCurrentRow = CurRowS;
							}
							Grid.TextMatrix(lngCurrentRow, lngGRIDColDate, Strings.Trim(FCConvert.ToString(rsPy.Get_Fields_DateTime("RecordedTransactionDate")) + " "));
							Grid.TextMatrix(lngCurrentRow, lngGRIDColRef, Strings.Trim(rsPy.Get_Fields_String("Reference") + " "));
							// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
							Grid.TextMatrix(lngCurrentRow, lngGRIDColService, Strings.Trim(rsPy.Get_Fields("Period") + " "));
							Grid.TextMatrix(lngCurrentRow, lngGRIDColPaymentCode, Strings.Trim(rsPy.Get_Fields("Code") + " "));
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							Grid.TextMatrix(lngCurrentRow, lngGRIDColPrincipal, Strings.Format(Strings.Trim(rsPy.Get_Fields("Principal") + " "), "#,##0.00"));
							// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
							Grid.TextMatrix(lngCurrentRow, lngGRIDColTax, Strings.Format(Strings.Trim(rsPy.Get_Fields("Tax") + " "), "#,##0.00"));
							Grid.TextMatrix(lngCurrentRow, lngGRIDColInterest, Strings.Format(modGlobal.Round(Conversion.Val(rsPy.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPy.Get_Fields_Decimal("CurrentInterest")), 2), "#,##0.00"));
							Grid.TextMatrix(lngCurrentRow, lngGRIDColCosts, Strings.Format(Conversion.Val(rsPy.Get_Fields_Decimal("LienCost")), "#,##0.00"));
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
							Grid.TextMatrix(lngCurrentRow, lngGRIDColPTC, Strings.Format(rsPy.Get_Fields("Principal") + rsPy.Get_Fields("Tax") + rsPy.Get_Fields_Decimal("LienCost"), "#,##0.00"));
							Grid.TextMatrix(lngCurrentRow, lngGRIDColTotal, (FCConvert.ToDecimal(Grid.TextMatrix(lngCurrentRow, lngGRIDColPrincipal)) + FCConvert.ToDecimal(Grid.TextMatrix(lngCurrentRow, lngGRIDColTax)) + FCConvert.ToDecimal(Grid.TextMatrix(lngCurrentRow, lngGRIDColInterest)) + FCConvert.ToDecimal(Grid.TextMatrix(lngCurrentRow, lngGRIDColCosts))));
							Grid.TextMatrix(lngCurrentRow, lngGRIDColLineCode, "-");
							// this is used to sum the cols - is for a payment...it is subtracting from the bill
							Grid.TextMatrix(lngCurrentRow, lngGRIDColPaymentKey, rsPy.Get_Fields_Int32("ID"));
						}
						if (FCConvert.ToString(rsPy.Get_Fields("Code")) == "U")
						{
							// this will check to see if a tax club payment was last, if so
							PaymentRecords = false;
							// do not calculate interest on this account
							boolNoCurrentInt = true;
						}
						else
						{
							PaymentRecords = true;
							boolNoCurrentInt = false;
						}
						rsPy.MoveNext();
					}
				}
				// If boolWater Then
				// 
				// 
				// Else
				// CurRowS = CurRowS + 1
				// Add CurRowS, 1, boolWater
				// 
				// fill the spacer line as well (no calculation needed)
				// .TextMatrix(CurRowS, lngGRIDColPrincipal) = Format(dblLienTotals(0), "#,##0.00")
				// .TextMatrix(CurRowS, lngGRIDColTax) = Format(dblLienTotals(1), "#,##0.00")
				// .TextMatrix(CurRowS, lngGRIDColInterest) = Format(dblLienTotals(2), "#,##0.00")
				// .TextMatrix(CurRowS, lngGRIDColCosts) = Format(dblLienTotals(3), "#,##0.00")
				// .TextMatrix(CurRowS, lngGRIDColTotal) = Format(dblLienTotals(0) + dblLienTotals(1) + dblLienTotals(2) + dblLienTotals(3), "#,##0.00")
				// 
				// If .TextMatrix(ParentLineS, lngGRIDColRef) = "Original" Or .TextMatrix(ParentLineS, lngGRIDColRef) = strSupplemntalBillString Then
				// SwapRows CLng(ParentLineS), NextSpacerLine(ParentLineS, False), SGRID
				// End If
				// End If
			}
			else
			{
			}
			rsPy.Reset();
			rsRK.Reset();
			return PaymentRecords;
		}

		private void imgNote_DoubleClick(object sender, System.EventArgs e)
		{
			AddEditNote();
			//clsDRWrapper rsNote = new clsDRWrapper();
			//// MAL@20071228: Change to integrate the add/edit note and note priority steps into one
			//// Tracker Reference: 11187
			//rsNote.DefaultDB = modExtraModules.strCLDatabase;
			//rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(CurrentAccount));
			//if (!rsNote.EndOfFile())
			//{
			//	if (FCConvert.ToInt32(rsNote.Get_Fields_Int32("Priority")) > 0)
			//	{
			//		boolPop = true;
			//	}
			//	else
			//	{
			//		boolPop = false;
			//	}
			//}
			//// align the frame
			//fraEditNote.Left = FCConvert.ToInt32((this.Width - fraEditNote.Width) / 2.0);
			//fraEditNote.Top = FCConvert.ToInt32((this.Height - fraEditNote.Height) / 2.0);
			//// Show the Frame
			//fraDiscount.Visible = false;
			//if (boolShowingWater)
			//{
			//	WGRID.Visible = false;
			//	lblWaterHeading.Visible = false;
			//}
			//SGRID.Visible = false;
			//lblSewerHeading.Visible = false;
			//fraPayment.Visible = false;
			//mnuPayment.Visible = false;
			//cmdPaymentPreview.Visible = false;
			//cmdSavePayments.Visible = false;
			//cmdPaymentSaveExit.Visible = false;
			//fraEditNote.Visible = true;
			//// Set the Text
			//txtNote.Text = strNoteText;
			//if (boolPop)
			//{
			//	chkPriority.CheckState = Wisej.Web.CheckState.Checked;
			//}
			//else
			//{
			//	chkPriority.CheckState = Wisej.Web.CheckState.Unchecked;
			//}
			//// EditNote
		}

		private void EditNote()
		{
			// Tracker Reference: 11187
			string strTemp;
			clsDRWrapper rsNote;
			int lngAcctNum;
			string strType;
			lngAcctNum = CurrentAccount;
			strType = "UT";
			strTemp = txtNote.Text;
			rsNote = new clsDRWrapper();
			rsNote.DefaultDB = modExtraModules.strUTDatabase;
			if (strTemp == "" && strNoteText != "")
			{
				if (MessageBox.Show("Are you sure that you would like to delete this note?", "Delete Note", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					rsNote.Execute("DELETE FROM Comments WHERE Account = " + FCConvert.ToString(lngAcctNum) + " AND Type ='UT'", "TWUT0000.vb1");
					// kk12222014 trouts-120  ' "TWCR0000.vb1"
					strNoteText = Strings.Trim(strTemp);
					imgNote.Visible = false;
				}
			}
			else
			{
				// save the next note text
				strNoteText = Strings.Trim(strTemp);
				rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(lngAcctNum) + " AND Type ='UT'");
				if (rsNote.RecordCount() > 0)
				{
					rsNote.Edit();
				}
				else
				{
					rsNote.AddNew();
				}
				rsNote.Set_Fields("Account", lngAcctNum);
				rsNote.Set_Fields("Comment", strNoteText);
				rsNote.Set_Fields("Type", strType);
				if (FCConvert.CBool(chkPriority.CheckState == Wisej.Web.CheckState.Checked))
				{
					rsNote.Set_Fields("Priority", 1);
					boolPop = true;
				}
				else
				{
					rsNote.Set_Fields("Priority", 0);
					boolPop = false;
				}
				rsNote.Update();
				imgNote.Visible = true;
			}
			rsNote.Reset();
		}

		private void lblOriginalTax_DoubleClick(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// when the user double clicks on this, the original tax will be discounted and the rest of the tax owed will be put in the tax field
				double dblNewDisc;
				double dblNewPrin;
				dblNewDisc = CalculateDiscount_2(FCConvert.ToDouble(lblOriginalTax.Text));
				dblNewPrin = FCConvert.ToDouble(lblOriginalTax.Text) - FCConvert.ToDouble(lblOriginalTax.Tag) - dblNewDisc;
				txtDiscDisc.Text = Strings.Format(dblNewDisc, "#,##0.00");
				txtDiscPrin.Text = Strings.Format(dblNewPrin, "#,##0.00");
				if (cmdDisc[0].Visible && cmdDisc[0].Enabled)
				{
					cmdDisc[0].Focus();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Calculating Original Tax Discount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void lblOriginalTax_DblClick()
		{
			lblOriginalTax_DoubleClick(lblOriginalTax, new System.EventArgs());
		}

		private void lblOriginalTaxLabel_DoubleClick(object sender, System.EventArgs e)
		{
			lblOriginalTax_DblClick();
		}

		private void lblTaxClub_DoubleClick(object sender, System.EventArgs e)
		{
			// This will create a tax club payment for this account
			CreateTaxClubPayment();
		}

		private void mnuFileAccountInfo_Click(object sender, System.EventArgs e)
		{
			ShowAccountInfo();
		}

		private void mnuFileEditNote_Click(object sender, System.EventArgs e)
		{
			AddEditNote();
		}
		//// MAL@20071213: This has become unnecessary with the new note process
		//// Tracker Reference: 11187
		//// If Not imgNote.Visible Then
		//clsDRWrapper rsNote = new clsDRWrapper();
		//// 
		//rsNote.DefaultDB = modExtraModules.strCLDatabase;
		//if (modStatusPayments.Statics.boolRE)
		//{
		//	rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE));
		//}
		//else
		//{
		//	rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP));
		//}
		//if (!rsNote.EndOfFile())
		//{
		//	if (FCConvert.ToInt32(rsNote.Get_Fields_Int32("Priority")) > 0)
		//	{
		//		boolPop = true;
		//	}
		//	else
		//	{
		//		boolPop = false;
		//	}
		//}
		//// If rsNote.EndOfFile Then        'if there is not a note for this account, add one and then edit it
		//// rsNote.AddNew
		//// If boolRE Then
		//// rsNote.Fields("Account") = CurrentAccountRE
		//// rsNote.Fields("Type") = "RE"
		//// Else
		//// rsNote.Fields("Account") = CurrentAccountPP
		//// rsNote.Fields("Type") = "PP"
		//// End If
		//// rsNote.Update
		//// End If
		//// End If
		//// MAL@20071228: Change to integrate the add/edit note and note priority steps into one
		//// Tracker Reference: 11187
		private void AddEditNote()
		{ 
			// align the frame
			fraEditNote.Left = FCConvert.ToInt32((this.Width - fraEditNote.Width) / 2.0);
			fraEditNote.Top = FCConvert.ToInt32((this.Height - fraEditNote.Height) / 2.0);
			// Show the Frame
			fraDiscount.Visible = false;
			if (boolShowingWater)
			{
				WGRID.Visible = false;
				lblWaterHeading.Visible = false;
			}
			SGRID.Visible = false;
			lblSewerHeading.Visible = false;
			fraPayment.Visible = false;
			mnuPayment.Visible = false;
			cmdPaymentPreview.Visible = false;
			cmdSavePayments.Visible = false;
			cmdPaymentSaveExit.Visible = false;
			fraEditNote.Visible = true;
			// Set the Text
			txtNote.Text = strNoteText;
			// kk03312015 trouts-127  Add remaining char count like in CL
			intCharCount = txtNote.Text.Length;
			lblRemainChars.Text = FCConvert.ToString((255 - intCharCount));
			if (FCConvert.ToInt16(FCConvert.ToDouble(lblRemainChars.Text)) <= 0)
			{
				lblRemainChars.Text = FCConvert.ToString(0);
			}
			if (boolPop)
			{
				chkPriority.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkPriority.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void mnuFileDischarge_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDischarge = new clsDRWrapper();
				int lngLienKey = 0;
				int lngRow = 0;
				int lngBill = 0;
				bool boolUseWater;
				// kk12122017 trout-706  Handle split screen view
				if (mnuFileShowSplitView.Visible)// If we're showing only Water or Sewer then use boolShowingWater
				{
					boolUseWater = boolShowingWater;
				}
				else
				{
					boolUseWater = FCConvert.ToBoolean(strCurrentGridSelect = "W");
				}
				if (boolUseWater)// kk12122017 trout-706    If boolShowingWater Then
				{
					lngRow = WGRID.Row;
					if (lngRow > 0)
					{
						if (lngRow < WGRID.Rows)
						{
							lngRow += 1;
						}
						if (Strings.Left(WGRID.TextMatrix(LastParentRow_6(lngRow, false), lngGRIDColRef), 4) == "Lien")
						{
							// DJW@1/8/2013 TROUT-706 Changed column from billnumber to paymentkey and multiplied reuslt by -1 to get lien key to use for discharge notice
							lngBill = FCConvert.ToInt32(WGRID.TextMatrix(LastParentRow_6(lngRow, true), lngGRIDColPaymentKey)) * -1;
							if (lngBill > 0)
							{
								lngLienKey = lngBill;
							}
							else
							{
								lngLienKey = 0;
							}
						}
						else
						{
							lngLienKey = 0;
						}
					}
					else
					{
						lngLienKey = 0;
					}
					if (lngLienKey > 0)
					{
						rsDischarge.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + rsCL.Get_Fields_Int32("WLienRecordNumber"), modExtraModules.strUTDatabase);
						// & " AND Printed = False"
						if (!rsDischarge.EndOfFile())
						{
							// not sure about the date...maybe have to look the record first
							// frmUTLienDischargeNotice.Init lngLienKey, Format(rsDischarge.Fields("Datepaid"), "MM/dd/yyyy"), boolShowingWater
						}
						else
						{
						}
					}
				}
				else
				{
					lngRow = SGRID.Row;
					if (lngRow > 0)
					{
						if (lngRow < SGRID.Rows)
						{
							lngRow += 1;
						}
						if (Strings.Left(SGRID.TextMatrix(LastParentRow_6(lngRow, false), lngGRIDColRef), 4) == "Lien")
						{
							// DJW@1/8/2013 TROUT-706 Changed column from billnumber to paymentkey and multiplied reuslt by -1 to get lien key to use for discharge notice
							lngBill = FCConvert.ToInt32(SGRID.TextMatrix(LastParentRow_6(lngRow, false), lngGRIDColPaymentKey)) * -1;
							if (lngBill > 0)
							{
								lngLienKey = lngBill;
							}
							else
							{
								lngLienKey = 0;
							}
						}
						else
						{
							lngLienKey = 0;
						}
					}
					else
					{
						lngLienKey = 0;
					}
					if (lngLienKey > 0)
					{
						rsDischarge.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + FCConvert.ToString(lngLienKey), modExtraModules.strUTDatabase);
						// & " AND Printed = False"
						if (!rsDischarge.EndOfFile())
						{
							// not sure about the date...maybe have to look the record first
							frmUTLienDischargeNotice.InstancePtr.Init(rsDischarge.Get_Fields_DateTime("Datepaid"), boolUseWater, lngLienKey);
							// kk12122017 trout-706  Changed from boolShowingWater to boolUseWater
						}
						else
						{
							frmUTLienDischargeNotice.InstancePtr.Init(DateAndTime.DateValue(Strings.Format(modUTFunctions.Statics.UTEffectiveDate, "MM/dd/yyyy")), boolUseWater, lngLienKey);
							// kk12122017 trout-706  Changed from boolShowingWater to boolUseWater
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Lien Discharge Notice Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileMortgageHolderInfo_Click(object sender, System.EventArgs e)
		{
			if (boolUseREInformation && boolUseMHFromRE && lngAssociatedREAccount != 0)
			{
				frmMortgageHolder.InstancePtr.Init(lngAssociatedREAccount, 2, true);
			}
			else
			{
				frmMortgageHolder.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(lblAccount.Text)), 3, false);
			}
			frmMortgageHolder.InstancePtr.Show(App.MainForm);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			// this will show the summary grid in a report format, only showing the rows that are open
			// at the time the option is choosen
			// MAL@20080715: Change the way the report is called to allow for E-mail and PDF export
			// Tracker Reference: 14600
			frmReportViewer.InstancePtr.Init(arUTAccountDetail.InstancePtr);
			//arUTAccountDetail.Show , MDIParent
		}

		private void mnuPaymentClearList_Click(object sender, System.EventArgs e)
		{
			int lngCT;
			for (lngCT = vsPayments.Rows - 1; lngCT >= 1; lngCT--)
			{
				if (lngCT >= vsPayments.Rows)
				{
					// a secondary check, so that if a CHGINT line is removed while removing the others, then the removal will restart at the last row
					if (vsPayments.Rows > 1)
					{
						lngCT = vsPayments.Rows - 1;
					}
					else
					{
						break;
					}
				}
				if (vsPayments.TextMatrix(lngCT, lngPayGridColRef) != "CHGINT")
				{
					// do not try and remove the CHGINT lines because they will be deleted by their respective payments
					// check to see if any of the payments have already been saved in the database
					if (lngCT <= vsPayments.Rows - 1)
					{
						DeletePaymentRow(lngCT);
						// this will remove it from the CR Summary screen too
					}
				}
			}
			// this will double check for CHGINT payments that may still be in the list
			if (vsPayments.Rows > 0)
			{
				for (lngCT = vsPayments.Rows - 1; lngCT >= 0; lngCT--)
				{
					if (lngCT < vsPayments.Rows - 1)
					{
						// check to see if any of the payments have already been saved in the database
						DeletePaymentRow(lngCT);
						// this will remove it from the CR Summary screen too
					}
				}
			}
			vsPayments.Rows = 1;
			FCUtils.EraseSafe(modUTFunctions.Statics.UTPaymentArray);
			modUTFunctions.CheckAllUTPending();
		}

		private void mnuPaymentClearPayment_Click(object sender, System.EventArgs e)
		{
			modUTFunctions.ResetUTPaymentFrame_6(boolShowingWater, false);
		}

		private void mnuPaymentPreview_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngBill As int	OnWrite(string)
			int lngBill = 0;
			// payments that are saved in the payment grid
			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
			//if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "W")
			if (Strings.Left(cmbService.Text, 1) == "W")
			{
				if (WGRID.Row > 0)
				{
					if (cmdPaymentPreview.Text != "Exit Preview")
					{
						if (WGRID.RowOutlineLevel(WGRID.Row) == 1)
						{
							lngBill = FCConvert.ToInt32(WGRID.TextMatrix(WGRID.Row, lngGRIDColBillNumber));
						}
						else
						{
							lngBill = FCConvert.ToInt32(WGRID.TextMatrix(LastParentRow_6(WGRID.Row, true), lngGRIDColBillNumber));
						}
						if (lngBill != 0)
						{
							// this sub will show the user a preview of the account and year taking into account all
							if (SGRID.Visible && WGRID.Visible)
							{
								lngPreviewStyle = 2;
								// both
							}
							else
							{
								lngPreviewStyle = 0;
								// water
							}
							modUTFunctions.CreateUTPreview_6(lngBill, true);
						}
					}
					else
					{
						HidePreview();
					}
				}
			}
			else
			{
				if (SGRID.Row > 0)
				{
					if (cmdPaymentPreview.Text != "Exit Preview")
					{
						if (SGRID.RowOutlineLevel(SGRID.Row) == 1)
						{
							lngBill = FCConvert.ToInt32(SGRID.TextMatrix(SGRID.Row, lngGRIDColBillNumber));
						}
						else
						{
							lngBill = FCConvert.ToInt32(SGRID.TextMatrix(LastParentRow_6(SGRID.Row, false), lngGRIDColBillNumber));
						}
						if (lngBill != 0)
						{
							// this sub will show the user a preview of the account and year taking into account all
							if (SGRID.Visible && WGRID.Visible)
							{
								lngPreviewStyle = 2;
								// both
							}
							else
							{
								lngPreviewStyle = 1;
								// sewer
							}
							modUTFunctions.CreateUTPreview_6(lngBill, false);
						}
					}
					else
					{
						HidePreview();
					}
				}
			}
		}

		private void mnuPaymentSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int i;
				bool boolWater = false;
				int intType;
				double dblOtherGrid;
				double dblAmt;
				string strOppType = "";
				string strType = "";
				bool boolNegWater = false;
				double dblNegAmount = 0;
				boolExitF12Seq = false;
				if (txtCash.Text == "N" && txtCD.Text == "N")
				{
					txtAcctNumber_Validating(false);
				}
				// this will add the current payment that is being entered into the payment frame
				// into the listbox below the boxes and into the payment array
				if (!boolPayment || vsPreview.Visible || boolDoNotContinueSave)
				{
					return;
				}
				// this is the regular payment
				int intParrIndex;
				intParrIndex = -1;
				// this is the default, if this is changed then it is the index number of the corresponding payment
				intType = FindPaymentType();
				if (intType == 0)
				{
					boolWater = true;
					strOppType = "sewer";
					strType = "water";
				}
				else
				{
					boolWater = false;
					strOppType = "water";
					strType = "sewer";
				}
				if (txtPrincipal.Text == "")
					txtPrincipal.Text = "0.00";
				if (txtTax.Text == "")
					txtTax.Text = "0.00";
				if (txtInterest.Text == "")
					txtInterest.Text = "0.00";
				if (txtCosts.Text == "")
					txtCosts.Text = "0.00";
				if (FCConvert.ToDecimal(txtPrincipal.Text) != 0 || FCConvert.ToDecimal(txtTax.Text) != 0 || FCConvert.ToDecimal(txtInterest.Text) != 0 || FCConvert.ToDecimal(txtCosts.Text) != 0)
				{
					// Write #12, "04 - " & Now & " - Add Items to List"
					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
					//if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A")
					if (Strings.Left(cmbCode.Text, 1) == "A")
					{
						// abatement = decrease the bill amount (opposite of Supplemental)
						intParrIndex = modUTFunctions.AddUTPaymentToList(vsPayments.Rows);
						modUTFunctions.AddUTCHGINTToList(vsPayments.Rows, boolWater);
						// kk10072015  ResetUTPaymentFrame CBool(Left(cmbService.List(cmbService.ListIndex), 1) = "W")
					}
					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
					//else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R")
					else if (Strings.Left(cmbCode.Text, 1) == "R")
					{
						// refunded abatement = decrease the bill amount and give the cash to the person
						intParrIndex = modUTFunctions.AddUTPaymentToList(vsPayments.Rows);
						modUTFunctions.AddUTCHGINTToList(vsPayments.Rows, boolWater);
						AddRefundedAbatementLine(intParrIndex);
						// kk10072015 trocr-446
						// set the combo box back to payment
						// For i = 0 To cmbCode.ListCount - 1
						// If Left$(cmbCode.List(i), 1) = "P" Then
						// cmbCode.ListIndex = i
						// Exit For
						// End If
						// Next
					}
						//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
						//else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "D")
						else if (Strings.Left(cmbCode.Text, 1) == "D")
					{
						// discount...this works without hitting F11
					}
							//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
							//else if ((Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "P") || (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "C"))
							else if ((Strings.Left(cmbCode.Text, 1) == "P") || (Strings.Left(cmbCode.Text, 1) == "C"))
					{
						// If Left(cmbService.List(cmbService.ListIndex), 1) = "B" Then
						// this function figures out if the payment entered is greater than the first service's bills
						// This is the old way 09/07/2006
						// If UTPaymentGreaterThanOwed(boolWater, CBool(cmbBillNumber.List(cmbBillNumber.ListIndex) <> "Auto"), False) Then
						// is the payment more than both services
						// If UTPaymentGreaterThanOwed(boolWater, CBool(cmbBillNumber.List(cmbBillNumber.ListIndex) <> "Auto"), True) Then          'this function figures out if the payment entered is greater than the years bill
						// If MsgBox("The payment entered is greater than owed, would you like to continue?", vbYesNoCancel + vbQuestion, "Payment Amount") = vbYes Then
						// boolExitF12Seq = True    'this will stop the user when they press F12 from continuing since they are getting a prepayment
						// dblAmt = CCur(txtPrincipal.Text) + CCur(txtTax.Text) + CCur(txtInterest.Text) + CCur(txtCosts.Text)
						// dblOtherGrid = GetAmountPaidToOtherGrid(boolWater, dblAmt)
						// intParrIndex = AddUTPaymentToList(vsPayments.rows, , CBool(UCase(frmUTCLStatus.txtReference.Text) = "REVERS") Or Left$(cmbCode.List(cmbCode.ListIndex), 1) = "C", , True, dblOtherGrid)
						// txtInterest.Text = dblAmt - dblOtherGrid
						// If dblAmt - dblOtherGrid <> 0 Then
						// intParrIndex = AddUTPaymentToList(vsPayments.rows, , CBool(UCase(frmUTCLStatus.txtReference.Text) = "REVERS") Or Left$(cmbCode.List(cmbCode.ListIndex), 1) = "C", , False, , , True)
						// End If
						// Else
						// do not add anything
						// 
						// End If
						// Else
						// it is not greater than the total of both grids but greater than the one tht the payment was placed against
						// dblAmt = CCur(txtPrincipal.Text) + CCur(txtTax.Text) + CCur(txtInterest.Text) + CCur(txtCosts.Text)
						// dblOtherGrid = GetAmountPaidToOtherGrid(boolWater, dblAmt)
						// intParrIndex = AddUTPaymentToList(vsPayments.rows, , CBool(UCase(frmUTCLStatus.txtReference.Text) = "REVERS") Or Left$(cmbCode.List(cmbCode.ListIndex), 1) = "C", , True, dblOtherGrid)
						// txtInterest.Text = dblAmt - dblOtherGrid
						// If dblAmt - dblOtherGrid <> 0 Then
						// intParrIndex = AddUTPaymentToList(vsPayments.rows, , CBool(UCase(frmUTCLStatus.txtReference.Text) = "REVERS") Or Left$(cmbCode.List(cmbCode.ListIndex), 1) = "C", , False, , , True)
						// End If
						// intParrIndex = AddUTPaymentToList(vsPayments.rows, , CBool(UCase(frmUTCLStatus.txtReference.Text) = "REVERS") Or Left$(cmbCode.List(cmbCode.ListIndex), 1) = "C", , , , , True)
						// End If
						// Else
						// payment is not greater than owed for the first grid...make the payment
						// intParrIndex = AddUTPaymentToList(vsPayments.rows, , CBool(UCase(frmUTCLStatus.txtReference.Text) = "REVERS") Or Left$(cmbCode.List(cmbCode.ListIndex), 1) = "C")
						// End If
						// Else
						// Service is either Water or Sewer...Do not apply this to both services
						//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
						//if (modUTFunctions.UTPaymentGreaterThanOwed_24(boolWater, FCConvert.CBool(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString() != "Auto"), FCConvert.CBool(cmbService.Items[cmbService.SelectedIndex].ToString() == "Both")))
						if (modUTFunctions.UTPaymentGreaterThanOwed_24(boolWater, FCConvert.CBool(cmbBillNumber.Text != "Auto"), FCConvert.CBool(cmbService.Text == "Both")))
						{
							// this function figures out if the payment entered is greater than the years bill
							if (MessageBox.Show("The payment entered is greater than owed, would you like to continue?", "Payment Amount", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
								//intParrIndex = modUTFunctions.AddUTPaymentToList_5475(vsPayments.Rows, FCConvert.CBool(Strings.UCase(this.txtReference.Text) == "REVERS") || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "C", true);
								intParrIndex = modUTFunctions.AddUTPaymentToList_5475(vsPayments.Rows, FCConvert.CBool(Strings.UCase(this.txtReference.Text) == "REVERS") || Strings.Left(cmbCode.Text, 1) == "C", true);
							}
						}
						else
						{
							// payment is not greater than owed for the first grid...make the payment
							//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
							//intParrIndex = modUTFunctions.AddUTPaymentToList_21(vsPayments.Rows, FCConvert.CBool(Strings.UCase(this.txtReference.Text) == "REVERS") || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "C");
							intParrIndex = modUTFunctions.AddUTPaymentToList_21(vsPayments.Rows, FCConvert.CBool(Strings.UCase(this.txtReference.Text) == "REVERS") || Strings.Left(cmbCode.Text, 1) == "C");
						}
						// End If
					}
								//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
								//else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "S")
								else if (Strings.Left(cmbCode.Text, 1) == "S")
					{
						// supplemental bill = increase the amount of the bill (Opposite of Abatement)
						intParrIndex = modUTFunctions.AddUTPaymentToList(vsPayments.Rows);
					}
									//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
									//else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "U")
									else if (Strings.Left(cmbCode.Text, 1) == "U")
					{
						// tax club = can pay in installments like a christmas club
						// when tax club is entered, there is no interest charged and the interest paid through date it not affected
						intParrIndex = modUTFunctions.AddUTPaymentToList_24(vsPayments.Rows, false, true);
					}
										//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
										//else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "Y")
										else if (Strings.Left(cmbCode.Text, 1) == "Y")
					{
						// prepayment
						// create a new Billing Year with a zero balance if it is after this year
						// create a payment record that will credit the account in the principal column
						intParrIndex = modUTFunctions.AddUTPaymentToList(vsPayments.Rows);
						modUTFunctions.AddUTCHGINTToList(vsPayments.Rows, boolWater);
					}
					else
					{
						MessageBox.Show("No Code entered!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
				else
				{
					if (vsPayments.Rows == 1)
					{
						if (NegativeBillValues(boolNegWater))
						{
							if (MessageBox.Show("Apply the negative account values to bills with outstanding balances?", "Negative Account Balances", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								// Left(cmbService.List(cmbService.ListIndex), 1)
								if (modUTFunctions.Statics.TownService == "W")
								{
									dblNegAmount = AffectAllNegativeBillValues_2(true);
								}
								else if (modUTFunctions.Statics.TownService == "S")
								{
									dblNegAmount = AffectAllNegativeBillValues_2(false);
								}
								else if (modUTFunctions.Statics.TownService == "B")
								{
									//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
									//if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "W")
									if (Strings.Left(cmbService.Text, 1) == "W")
									{
										dblNegAmount = AffectAllNegativeBillValues_2(true);
									}
											//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
											//else if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "S")
											else if (Strings.Left(cmbService.Text, 1) == "S")
									{
										dblNegAmount = AffectAllNegativeBillValues_2(false);
									}
												//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
												//else if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "B")
												else if (Strings.Left(cmbService.Text, 1) == "B")
									{
										if (boolNegWater)
										{
											dblNegAmount = AffectAllNegativeBillValues_2(true);
											dblNegAmount += AffectAllNegativeBillValues_2(false);
										}
										else
										{
											dblNegAmount = AffectAllNegativeBillValues_2(false);
											dblNegAmount += AffectAllNegativeBillValues_2(true);
										}
									}
								}
								// set to auto
								cmbBillNumber.SelectedIndex = cmbBillNumber.Items.Count - 1;
								// add the total negative amount
								txtInterest.Text = FCConvert.ToString(dblNegAmount * -1);
								txtPrincipal.Text = "0.00";
								txtCosts.Text = "0.00";
								txtTax.Text = "0.00";
								if (dblNegAmount != 0)
								{
									// add the offsetting entries
									modUTFunctions.AddUTPaymentToList_5466(vsPayments.Rows, true);
									modUTFunctions.CheckAllUTPending();
								}
							}
						}
						else
						{
							MessageBox.Show("There are no values in the payment fields.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}
				// If intParrIndex = -1 Then     'kk07142015 trouts-140  Clear the boxes after F11 to Move credit balances
				// Exit Sub
				// End If
				// reset payment boxes
				// kk10072015 trocr-446
				// txtTransactionDate.Text = Format(UTEffectiveDate, "MM/dd/yyyy")
				// txtReference.Text = ""
				// txtPrincipal.Text = Format(0, "#,##0.00")
				// txtTax.Text = Format(0, "#,##0.00")
				// txtInterest.Text = Format(0, "#,##0.00")
				// txtCosts.Text = Format(0, "#,##0.00")
				// txtComments.Text = ""
				// kk04132016 trout-1224  Move into If stmt below so we don't clear Bill Number when reversing with interest
				// ResetUTPaymentFrame CBool(Left(cmbService.List(cmbService.ListIndex), 1) = "W"), True
				if (intParrIndex == -1)
				{
					// kk07142015 trouts-140  Clear the boxes after F11 to Move credit balances
					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
					//modUTFunctions.ResetUTPaymentFrame_8(FCConvert.CBool(Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "W"), true); // kk04132016 trout-1224
					modUTFunctions.ResetUTPaymentFrame_8(FCConvert.CBool(Strings.Left(cmbService.Text, 1) == "W"), true);
					// kk04132016 trout-1224
					return;
				}
				// this is the CHGINT Payment Record
				if (modUTFunctions.Statics.lngUTCHGINT > 0)
				{
					// if this is a payment reverse that has interest charged
					modUTFunctions.AddUTCHGINTToList(vsPayments.Rows, boolWater, 0, false, false, FCConvert.ToInt16(intParrIndex));
					// then add the reverse of the interest charged to the payment grid
				}
				else if (modUTFunctions.Statics.lngUTCHGINT < 0)
				{
					modUTFunctions.AddUTCHGINTToList(vsPayments.Rows, boolWater, 0, true, false, FCConvert.ToInt16(intParrIndex));
					// then add the reverse of the interest charged to the payment grid
				}
				// Dim boolDone As Boolean
				// Dim rsCreatePy As New clsDRWrapper
				// Dim ArrayIndex As Integer
				// rsCreatePy.DEFAULTDB = databasename
				// rsCreatePy.OpenRecordset ("SELECT * FROM PaymentRec WHERE Account = 0")
				// boolDone = True
				// create the payment records
				// rsCreatePy.BeginTrans
				// Dim lngCounter As Long
				// For lngCounter = 1 To vsPayments.rows - 1
				// ArrayIndex = Val(vsPayments.TextMatrix(lngCounter, 9))       'this takes the array index from the last part of the list text
				// If Not CreateUTPaymentRecord(ArrayIndex, rsCreatePy) Then
				// rsCreatePy.RollBack
				// Exit For
				// End If
				// Next
				// If boolDone Then
				// rsCreatePy.CommitTrans
				// Else
				// MsgBox "The trasanctions caused an error and have not been applied.", vbOKOnly, "ERROR"
				// End If
				// GRID.rows = 1   'erase the old info
				// Reset_GRID      'reset status grid, just like effective date change
				modUTFunctions.CheckAllUTPending();
				// this will reset the asteriks on the right side of the form whenever another payment is added
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Payment", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuPaymentSave_Click()
		{
			mnuPaymentSave_Click(cmdSavePayments, new System.EventArgs());
		}

		private void mnuPaymentSaveExit_Click(object sender, System.EventArgs e)
		{
			int lngErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// payment list
				bool boolDone = false;
				clsDRWrapper rsCreatePy = new clsDRWrapper();
				int ArrayIndex = 0;
				DateTime dateTemp = DateTime.FromOADate(0);
				// DJW@11/17/2010 Changed array size from 1000 to 20000
				bool[] boolIntChargedToday = new bool[20000 + 1];
				// this will not let interest get charged more than once a day
				bool boolSaveForCR;
				bool boolDoNotRecalc;
				int intType = 0;
				// 0 = water, 1 = sewer
				double dblCHGINTAmt;
				DateTime dtOldIntDate;
				string strWS = "";
				int lngTemp = 0;
				int lngID = 0;
				int lngBK = 0;
				int lngCounter;
				int lngKey = 0;
				// MAL@20071026
				clsDRWrapper rsBill = new clsDRWrapper();
				bool blnContinue = false;
				// Open "UTPaymentSaveTime.txt" For Output As #12            'MAL@20071115: Add to check for slowdown
				// Write #12, "Start - " & Now
				boolExitF12Seq = false;
				lngErr = 1;
				if (txtCash.Text == "N" && txtCD.Text == "N")
				{
					txtAcctNumber_Validating(false);
				}
				lngErr = 2;
				// this sub will create all of the payment records that are saved in the
				if (!boolPayment || vsPreview.Visible || boolDoNotContinueSave)
				{
					return;
				}
				boolSaveForCR = modExtraModules.IsThisCR();
				lngErr = 10;
				// trout-1261 06.07.17 kjr Abatements are not multi-line payments. mnuPaymentSave_Click is executed below to save.
				// If lngUTCHGINT > 0 Or boolUTMultiLine Or Left$(cmbCode.List(cmbCode.ListIndex), 1) = "R" Or Left$(cmbCode.List(cmbCode.ListIndex), 1) = "A" Then       'this is a multiline so this option is not available
				if (modUTFunctions.Statics.lngUTCHGINT > 0 || modUTFunctions.Statics.boolUTMultiLine)//this is a multiline so this option is not available
				{
					// this is a multiline so this option is not available
					MessageBox.Show("This is a multiline payment.  Save the payments first. <F11>", "Save", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					// normal payment
					boolDone = true;
					if (txtCash.Text == "N" && txtCD.Text == "N" && Strings.InStr(1, "_", txtAcctNumber.TextMatrix(0, 0), CompareConstants.vbBinaryCompare) != 0)
					{
						MessageBox.Show("Please enter an account number unless this is going to affect cash.", "Account Number Missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					// Write #12, "01 - " & Now & " - Find Payment Type"       'MAL@20071115
					intType = FindPaymentType();
					// Write #12, "02 - " & Now & " - Payment Type Found"
					if (Strings.Trim(txtPrincipal.Text) == "")
						txtPrincipal.Text = "0.00";
					if (Strings.Trim(txtTax.Text) == "")
						txtTax.Text = "0.00";
					if (Strings.Trim(txtCosts.Text) == "")
						txtCosts.Text = "0.00";
					if (Strings.Trim(txtInterest.Text) == "")
						txtInterest.Text = "0.00";
					// *********************  this will just make sure that the user wants to go ahead with this action
					if (FCConvert.ToDecimal(txtPrincipal.Text) != 0 || FCConvert.ToDecimal(txtTax.Text) != 0 || FCConvert.ToDecimal(txtInterest.Text) != 0 || FCConvert.ToDecimal(txtCosts.Text) != 0)
					{
						// Write #12, "03 - " & Now & " - Save Record"
						mnuPaymentSave_Click();
						// Write #12, "04 - " & Now & " - Saved Record"
						if (boolExitF12Seq)
						{
							boolExitF12Seq = false;
							// reset the variable
							return;
						}
					}
					// MAL@20081113: Check for double payment on same account & service from CR
					// Tracker Reference: 16111
					if (boolSaveForCR)
					{
						blnContinue = !ServiceHasPendingPayment(FCConvert.ToInt32(Conversion.Val(vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColBill))), vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColService));
					}
					else
					{
						blnContinue = true;
					}
					if (blnContinue)
					{
						lngErr = 20;
						// create the payment records
						// rsCreatePy.BeginTrans
						for (lngCounter = 1; lngCounter <= vsPayments.Rows - 1; lngCounter++)
						{
							// get the recordset ready
							rsCreatePy.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE AccountKey = 0", modExtraModules.strUTDatabase);
							lngErr = 30;
							if (Conversion.Val(vsPayments.TextMatrix(lngCounter, lngPayGridColKeyNumber)) == 0)
							{
								// if this is 0 then there is no record created for it in the database
								ArrayIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(lngCounter, lngPayGridColArrayIndex))));
								// this takes the array index from the last part of the list text
								lngErr = 40;
								if (ArrayIndex < 0)
								{
									// check to see if it has already been taken care of in CreateUTPaymentRecord
									// if is has, then get skip this part because you don't want to create it again
									if (modUTFunctions.Statics.UTPaymentArray[Math.Abs(FCConvert.ToInt16(ArrayIndex))].ReversalID != -1)
									{
										// this is a CHGINT line
										if (!modUTFunctions.CreateUTCHGINTPaymentRecord_2(FCConvert.ToInt16(ArrayIndex * -1), rsCreatePy))
										{
											boolDone = false;
											break;
										}
									}
								}
								else
								{
									// regular payment line
									// Write #12, "05 - " & Now & " - Create Payment Record"
									if (!modUTFunctions.CreateUTPaymentRecord(ArrayIndex, ref rsCreatePy, ref lngCounter))
									{
										boolDone = false;
										break;
									}
								}
								lngErr = 50;
								// Write #12, "06 - " & Now & " - Created Payment Record"
								ArrayIndex = Math.Abs(FCConvert.ToInt16(ArrayIndex));
								// if this is not a reversal of CHGINT and there is some current interest then create a CHGINT line
								if (modUTFunctions.Statics.lngUTCHGINT == 0 && modUTFunctions.Statics.UTPaymentArray[ArrayIndex].ReversalID == 0 && modUTFunctions.Statics.dblUTCurrentInt[modUTFunctions.Statics.UTPaymentArray[ArrayIndex].BillNumber, intType] != 0 && !boolIntChargedToday[modUTFunctions.Statics.UTPaymentArray[ArrayIndex].Year] && modUTFunctions.Statics.UTPaymentArray[ArrayIndex].Code != "U")
								{
									// needs to add the Old Effective Interest Date to the CHGINTDate and the billkey to the field of the last payment
									lngErr = 55;
									rsCreatePy.MoveFirst();
									lngID = FCConvert.ToInt32(rsCreatePy.Get_Fields_Int32("ID"));
									vsPayments.TextMatrix(lngCounter, lngPayGridColKeyNumber, FCConvert.ToString(lngID));
									// Write #12, "07 - " & Now & " - Creating Current Interest Record"
									lngTemp = modUTFunctions.CreateUTCHGINTLine(ref ArrayIndex, ref dateTemp);
									// Write #12, "08 - " & Now & " - Created Current Interest Record"
									lngBK = FCConvert.ToInt32(rsCreatePy.Get_Fields_Int32("BillKey"));
									rsCreatePy.Edit();
									rsCreatePy.Set_Fields("CHGINTNumber", lngTemp);
									if (modUTFunctions.Statics.UTPaymentArray[ArrayIndex].ReversalID == -1)
									{
										rsCreatePy.Set_Fields("CHGINTDate", modUTFunctions.Statics.UTEffectiveDate);
									}
									else
									{
										// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
										rsCreatePy.Set_Fields("CHGINTDate", modUTFunctions.Statics.dateUTOldDate[FCConvert.ToInt32(rsCreatePy.Get_Fields("Year")) - modExtraModules.UTDEFAULTSUBTRACTIONVALUE, intType]);
									}
									rsCreatePy.Update();
									// Write #12, "09 - " & Now & " - Updated Current Bill Record"
									lngErr = 60;
									if (!boolSaveForCR)
									{
										// MAL@20071026
										rsCreatePy.OpenRecordset("SELECT * FROM Bill WHERE  ID = " + FCConvert.ToString(lngBK), modExtraModules.strUTDatabase);
										if (!rsCreatePy.EndOfFile())
										{
											if (FCConvert.ToInt32(rsCreatePy.Get_Fields(rsCreatePy.Get_Fields_String("Service") + "LienRecordNumber")) == 0)
											{
												rsCreatePy.Edit();
												rsCreatePy.Set_Fields(rsCreatePy.Get_Fields_String("Service") + "IntPaidDate", Strings.Format(modUTFunctions.Statics.UTEffectiveDate, "MM/dd/yyyy"));
												rsCreatePy.Set_Fields(rsCreatePy.Get_Fields_String("Service") + "IntAdded", rsCreatePy.Get_Fields(rsCreatePy.Get_Fields_String("Service") + "IntAdded") + modUTFunctions.Statics.dblUTCurrentInt[modUTFunctions.Statics.UTPaymentArray[ArrayIndex].BillNumber, intType]);
												rsCreatePy.Update();
											}
											else
											{
												rsCreatePy.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsCreatePy.Get_Fields(rsCreatePy.Get_Fields_String("Service") + "LienRecordNumber"), modExtraModules.strUTDatabase);
												if (!rsCreatePy.EndOfFile())
												{
													rsCreatePy.Edit();
													rsCreatePy.Set_Fields("IntPaidDate", Strings.Format(modUTFunctions.Statics.UTEffectiveDate, "MM/dd/yyyy"));
													// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
													rsCreatePy.Set_Fields("IntAdded", rsCreatePy.Get_Fields("IntAdded") + modUTFunctions.Statics.dblUTCurrentInt[modUTFunctions.Statics.UTPaymentArray[ArrayIndex].BillNumber, intType]);
													rsCreatePy.Update();
												}
											}
										}
										lngErr = 70;
									}
									lngErr = 72;
								}
								else
								{
									if (modUTFunctions.Statics.UTPaymentArray[ArrayIndex].ReversalID == -1 && modUTFunctions.Statics.UTPaymentArray[ArrayIndex].Code != "P")
									{
										// Write #12, "06A - " & Now & " - Open Bill Recordset"
										rsBill.OpenRecordset("SELECT ID FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(modUTFunctions.Statics.lngCurrentAccountUT) + " AND BillNumber = " + vsPayments.TextMatrix(lngCounter, lngPayGridColBill), modExtraModules.strUTDatabase);
										// Write #12, "06B - " & Now & " - Opened Bill Recordset"
										if (rsBill.RecordCount() > 0)
										{
											lngKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
										}
										// Write #12, "06C - " & Now & " - Retreived Bill ID"
										if (vsPayments.TextMatrix(lngCounter, lngPayGridColService) == "W")
										{
											strWS = "W";
										}
										else
										{
											strWS = "S";
										}
										// Write #12, "06D - " & Now & " - Set Service"
										// Write #12, "07 - " & Now & " - Updating Interest Date for Reversal"
										rsCreatePy.OpenRecordset("SELECT * FROM Bill WHERE  ID = " + FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
										if (!boolSaveForCR)//kk11022016  trout-1225
										{
											rsCreatePy.OpenRecordset("SELECT * FROM Bill WHERE  ID = " + lngKey, modExtraModules.strUTDatabase);
											if (!rsCreatePy.EndOfFile())
											{
												if (FCConvert.ToInt32(rsCreatePy.Get_Fields(strWS + "LienRecordNumber")) == 0)
												{
													rsCreatePy.Edit();
													// rsCreatePy.Fields("strWS") = dtOldInterestDate
													rsCreatePy.Set_Fields(strWS + "IntPaidDate", modUTFunctions.GetLastInterestDate(modUTFunctions.Statics.lngCurrentAccountKeyUT, FCConvert.ToInt32(vsPayments.TextMatrix(lngCounter, lngPayGridColBill)), strWS, modUTFunctions.Statics.UTEffectiveDate, 0));
													rsCreatePy.Update();
												}
												else
												{
													rsCreatePy.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsCreatePy.Get_Fields(rsCreatePy.Get_Fields_String("Service") + "LienRecordNumber"), modExtraModules.strUTDatabase);
													if (!rsCreatePy.EndOfFile())
													{
														rsCreatePy.Edit();
														// rsCreatePy.Fields("IntPaidDate") = dtOldInterestDate
														rsCreatePy.Set_Fields("IntPaidDate", modUTFunctions.GetLastInterestDate(modUTFunctions.Statics.lngCurrentAccountKeyUT, FCConvert.ToInt32(vsPayments.TextMatrix(lngCounter, lngPayGridColBill)), strWS, modUTFunctions.Statics.UTEffectiveDate, 0));
														rsCreatePy.Update();
													}
												}
											}
										}
										// Write #12, "08 - " & Now & " - Updated Interest Date for Reversal"
									}
								}
							}
						}
						lngErr = 80;
						if (boolDone)
						{
							boolUnloadOK = true;
							if (boolSaveForCR)
							{
								// if this is in CR then send the information back into the Receipt Input Screen
								this.Hide();
								// Write #12, "10 - " & Now & " - Fill UT Info"
								modUseCR.FillUTInfo();
								//Application.DoEvents();
								// Write #12, "11 - " & Now & " - Filled UT Info"
								FormLoaded = false;
							}
							else
							{
								Close();
							}
							Reset_Sum();
							boolUnloadOK = false;
						}
						else
						{
							MessageBox.Show("The transactions caused an error and have not been applied.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							// rsCreatePy.RollBack
						}
					}
					else
					{
						if (boolSaveForCR)
						{
							MessageBox.Show("A pending payment for this Bill and Service exists!" + "\r\n" + "You must save the pending receipt before adding another payment.", "Pending Payment", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							DeletePaymentRow_2(vsPayments.Rows - 1);
						}
					}
				}
				rsCreatePy.Reset();
				// Write #12, "END - " & Now
				// Close #12
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving/Exit Payment - " + FCConvert.ToString(lngErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuProcessChangeAccount_Click(object sender, System.EventArgs e)
		{
			// this sub will allow the user to go back to frmCLGetAccount and enter another account number
			bool boolGoBack = false;
			if (vsPayments.Rows > 1)
			{
				if (MessageBox.Show("Are you sure that you would like to leave this screen?  Any payments that you have added this session will be lost unless saved.", "Change Account", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					boolGoBack = true;
				}
				else
				{
					boolGoBack = false;
				}
			}
			else
			{
				boolGoBack = true;
			}
			if (boolGoBack)
			{
				Close();
				// Doevents
				if (boolPayment)
				{
					frmGetMasterAccount.InstancePtr.Init(2, true);
				}
				else
				{
					frmGetMasterAccount.InstancePtr.Init(1, true);
				}
			}
		}

		private void mnuProcessEffective_Click(object sender, System.EventArgs e)
		{
			// This sub will set the Effective Interest Date and reset the grid
			string strDate = "";
			// strDate = InputBox("Please enter the new Effective Interest Date.  (MM/dd/yyyy)", "Effective Interest Date", Format(EffectiveDate, "MM/dd/yyyy"))
			ShowEffectiveInterestDateForm();
			// If IsDate(strDate) Then
			// this is a valid date
			// If UTEffectiveDate <> CDate(strDate) Then
			// UTEffectiveDate = CDate(strDate)
			// frmUTCLStatus.Caption = "Real Estate Account Status as of " & UTEffectiveDate
			// Reset_GRID
			// End If
			// Else
			// this is not a valid date
			// If strDate <> "" Then       'if strDate = "" then the cancel button was pressed
			// MsgBox "The date entered: " & strDate & " is not a valid date." & vbCrLf & "Please use the format: (MM/dd/yyyy)", vbOKOnly + vbInformation, "Invalid Date"
			// End If
			// End If
		}

		private void ShowEffectiveInterestDateForm()
		{
			// this will show the input form
			// after the user has entered the new effective interest date, then
			// the form will call the GetNewEffectiveInterestDate sub to run the new setup
			frmUTDateChange.InstancePtr.Init("Please input the new effective date and then press Process", 1, ref modUTFunctions.Statics.UTEffectiveDate);
		}

		public void GetNewEffectiveInterestDate(ref DateTime dtDate, bool boolChangeRTD = true)
		{
			int counter;
			// this sub will change teh effective interest date and reset the grid
			// boolDoNothing is in case the user cancels, it is defaulted to update the form
			// this is a valid date
			if (modUTFunctions.Statics.UTEffectiveDate.ToOADate() != dtDate.ToOADate())
			{
				// DJW@01102013 TROUT-842 Delete payments before refreshign screen so charged interest lines get deleted from the database
				for (counter = 1; counter <= vsPayments.Rows - 1; counter++)
				{
					DeletePaymentRow(counter);
				}
				boolDoNotResetRTD = !boolChangeRTD;
				modUTFunctions.Statics.UTEffectiveDate = dtDate;
				this.Text = "Utility Account Status as of " + FCConvert.ToString(modUTFunctions.Statics.UTEffectiveDate);
				// FC:FINAL:VGE - #i904 Updating date in the grid
				var lngCounter = SGRID.Rows - 1;
				SGRID.TextMatrix(lngCounter, 1, Strings.Format(modUTFunctions.Statics.UTEffectiveDate, "MM/dd/yyyy"));
				SGRID.TextMatrix(lngCounter, 2, SGRID.TextMatrix(lngCounter, 1));
				SGRID.TextMatrix(lngCounter, 3, SGRID.TextMatrix(lngCounter, 1));
				SGRID.TextMatrix(lngCounter, 4, SGRID.TextMatrix(lngCounter, 1));
				SGRID.TextMatrix(lngCounter, 5, SGRID.TextMatrix(lngCounter, 1));
				SGRID.TextMatrix(lngCounter, 6, SGRID.TextMatrix(lngCounter, 1));
				FormReset = true;
				this.Refresh();
				//Application.DoEvents();
				//FC:FINAL:DSE:#i184 In VB6 Activate event is fired
				this.frmUTCLStatus_Activated(this, EventArgs.Empty);
				FormReset = false;
				// MAL@20080402: Reset the visibility of the frame (and its controls) to force a redraw
				// Tracker Reference: 13087
				if (fraPayment.Visible == true)
				{
					fraPayment.Visible = false;
					fraPayment.Visible = true;
				}
			}
		}

		private void Reset_GRID()
		{
			FormReset = true;
			WGRID.Rows = 1;
			SGRID.Rows = 1;
			if (fraPayment.Visible == true)
			{
				FCUtils.EraseSafe(modUTFunctions.Statics.UTPaymentArray);
				vsPayments.Rows = 1;
			}
			frmUTCLStatus_Activated(this, EventArgs.Empty);
		}

		private void Create_Account_Totals_2(bool boolWater)
		{
			Create_Account_Totals(ref boolWater);
		}

		private void Create_Account_Totals(ref bool boolWater)
		{
			int lngCounter = 0;
			// add a account totals row
			if (boolWater)
			{
				lngCounter = WGRID.Rows;
				Add_6(lngCounter, -1, boolWater);
				//FC:FINAL:CHN: Set concrete Columns to correct merge.
				// WGRID.MergeRow(lngCounter, true);
				WGRID.MergeRow(lngCounter, true, 2, 3);
				// .MergeCells = flexMergeSpill
				// If boolPayment Then
				WGRID.TextMatrix(lngCounter, 1, Strings.Format(modUTFunctions.Statics.UTEffectiveDate, "MM/dd/yyyy"));
				// Else
				// .TextMatrix(lngCounter, 1) = "Totals as of " & Format(UTEffectiveDate, "MM/dd/yyyy")
				// End If
				WGRID.TextMatrix(lngCounter, 2, WGRID.TextMatrix(lngCounter, 1));
				WGRID.TextMatrix(lngCounter, 3, WGRID.TextMatrix(lngCounter, 1));
				WGRID.TextMatrix(lngCounter, 4, WGRID.TextMatrix(lngCounter, 1));
				WGRID.TextMatrix(lngCounter, 5, WGRID.TextMatrix(lngCounter, 1));
				WGRID.TextMatrix(lngCounter, 6, WGRID.TextMatrix(lngCounter, 1));
				WGRID.TextMatrix(lngCounter, lngGRIDColPrincipal, Strings.Format(sumWPrin, "0.00"));
				WGRID.TextMatrix(lngCounter, lngGRIDColTax, Strings.Format(sumWTax, "0.00"));
				WGRID.TextMatrix(lngCounter, lngGRIDColInterest, Strings.Format(sumWInt, "0.000"));
				WGRID.TextMatrix(lngCounter, lngGRIDColCosts, Strings.Format(sumWCost, "0.00"));
				WGRID.TextMatrix(lngCounter, lngGRIDColPTC, Strings.Format(sumWPrin + sumWCost + sumWTax, "0.00"));
				if (modGlobal.Round((sumWPrin + sumWInt + sumWCost + sumWTax), 2) == modGlobal.Round(sumWTotal, 2))
				{
					WGRID.TextMatrix(lngCounter, lngGRIDColTotal, Strings.Format(sumWTotal, "0.000"));
				}
				else
				{
					WGRID.TextMatrix(lngCounter, lngGRIDColTotal, "ERROR(" + FCConvert.ToString(sumWTotal) + ")");
				}
				//WGRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngCounter, 1, lngCounter, WGRID.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249))); // light blue  '&H8000& pea green        '&H407000 dark green
				WGRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngCounter, 5, lngCounter, WGRID.Cols - 1, Color.FromArgb(5, 204, 71));
				WGRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngCounter, 1, lngCounter, WGRID.Cols - 1, true);
			}
			else
			{
				lngCounter = SGRID.Rows;
				Add_6(lngCounter, -1, boolWater);
				//FC:FINAL:CHN: Set correct Columns to correct merge.
				SGRID.MergeRow(lngCounter, true, 2, 3);
				//FC:FINAL:SBE - #i904 - merge cell content (starting from visible col)
				// SGRID.MergeRow(lngCounter, true, 2, 6);

				// .MergeCells = flexMergeSpill
				// If boolPayment Then
				SGRID.TextMatrix(lngCounter, 1, Strings.Format(modUTFunctions.Statics.UTEffectiveDate, "MM/dd/yyyy"));
				// Else
				// .TextMatrix(lngCounter, 1) = "Totals as of " & Format(UTEffectiveDate, "MM/dd/yyyy")
				// End If
				SGRID.TextMatrix(lngCounter, 2, SGRID.TextMatrix(lngCounter, 1));
				SGRID.TextMatrix(lngCounter, 3, SGRID.TextMatrix(lngCounter, 1));
				SGRID.TextMatrix(lngCounter, 4, SGRID.TextMatrix(lngCounter, 1));
				SGRID.TextMatrix(lngCounter, 5, SGRID.TextMatrix(lngCounter, 1));
				SGRID.TextMatrix(lngCounter, 6, SGRID.TextMatrix(lngCounter, 1));
				SGRID.TextMatrix(lngCounter, lngGRIDColPrincipal, Strings.Format(sumSPrin, "0.00"));
				SGRID.TextMatrix(lngCounter, lngGRIDColTax, Strings.Format(sumSTax, "0.00"));
				SGRID.TextMatrix(lngCounter, lngGRIDColInterest, Strings.Format(sumSInt, "0.000"));
				SGRID.TextMatrix(lngCounter, lngGRIDColCosts, Strings.Format(sumSCost, "0.00"));
				SGRID.TextMatrix(lngCounter, lngGRIDColPTC, Strings.Format(sumSPrin + sumSCost + sumSTax, "0.00"));
				if (modGlobal.Round((sumSPrin + sumSInt + sumSCost + sumSTax), 2) == modGlobal.Round(sumSTotal, 2))
				{
					SGRID.TextMatrix(lngCounter, lngGRIDColTotal, Strings.Format(sumSTotal, "0.000"));
				}
				else
				{
					SGRID.TextMatrix(lngCounter, lngGRIDColTotal, "ERROR(" + FCConvert.ToString(sumSTotal) + ")");
				}
                FCUtils.ApplicationUpdate(SGRID);
				//SGRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngCounter, 1, lngCounter, SGRID.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249))); // light blue  '&H8000& pea green        '&H407000 dark green
				SGRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngCounter, 5, lngCounter, SGRID.Cols - 1, Color.FromArgb(5, 204, 71));
				SGRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngCounter, 1, lngCounter, SGRID.Cols - 1, true);
			}
			//FC:FINAL:DDU:#i1048 - fixed incorrect alignment
			SGRID.Refresh();
			WGRID.Refresh();
		}

		private void Reset_Sum()
		{
			// this just resets the sums in order to process another account or another effective date
			sumWPrin = 0;
			sumWInt = 0;
			sumWTax = 0;
			sumWCost = 0;
			sumWTotal = 0;
			sumSPrin = 0;
			sumSInt = 0;
			sumSTax = 0;
			sumSCost = 0;
			sumSTotal = 0;
		}

		private void mnuProcessExit_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		public void mnuProcessExit_Click()
		{
			mnuProcessExit_Click(mnuProcessExit, new System.EventArgs());
		}

		private void Format_PaymentGrid()
		{
			int wid = 0;
			vsPayments.Cols = 14;
			wid = vsPayments.WidthOriginal;
			vsPayments.ColWidth(lngPayGridColBill, FCConvert.ToInt32(wid * 0.04));
			// Year
			vsPayments.ColWidth(lngPayGridColDate, FCConvert.ToInt32(wid * 0.12));
			// Date
			vsPayments.ColWidth(lngPayGridColRef, FCConvert.ToInt32(wid * 0.09));
			// Reference
			vsPayments.ColWidth(lngPayGridColService, FCConvert.ToInt32(wid * 0.04));
			// Service
			vsPayments.ColWidth(lngPayGridColCode, FCConvert.ToInt32(wid * 0.05));
			// Code
			vsPayments.ColWidth(lngPayGridColCDAC, FCConvert.ToInt32(wid * 0.05));
			// CD / Affect Cash
			vsPayments.ColWidth(lngPayGridColPrincipal, FCConvert.ToInt32(wid * 0.13));
			// Principal
			vsPayments.ColWidth(lngPayGridColTax, FCConvert.ToInt32(wid * 0.113));
			// Tax
			vsPayments.ColWidth(lngPayGridColInterest, FCConvert.ToInt32(wid * 0.12));
			// Interest
			vsPayments.ColWidth(lngPayGridColCosts, FCConvert.ToInt32(wid * 0.113));
			// Costs
			vsPayments.ColWidth(lngPayGridColArrayIndex, 0);
			// Array Index
			vsPayments.ColWidth(lngPayGridColKeyNumber, 0);
			// ID Number from AutoNumberfield in DB
			vsPayments.ColWidth(lngPayGridColCHGINTNumber, 0);
			// CHGINT Number
			vsPayments.ColWidth(lngPayGridColTotal, FCConvert.ToInt32(wid * 0.13));
			// Total
			vsPayments.ColAlignment(lngPayGridColBill, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColRef, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColService, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColCode, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// all the headers
			//vsPayments.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsPayments.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsPayments.ExtendLastCol = true;
			vsPayments.TextMatrix(0, lngPayGridColBill, "Bill");
			vsPayments.TextMatrix(0, lngPayGridColDate, "Date");
			vsPayments.TextMatrix(0, lngPayGridColRef, "Ref");
			vsPayments.TextMatrix(0, lngPayGridColService, "Svc");
			vsPayments.TextMatrix(0, lngPayGridColCode, "Code");
			vsPayments.TextMatrix(0, lngPayGridColCDAC, "Cash");
			vsPayments.TextMatrix(0, lngPayGridColPrincipal, "Principal");
			vsPayments.TextMatrix(0, lngPayGridColTax, "Tax");
			vsPayments.TextMatrix(0, lngPayGridColInterest, "Interest");
			vsPayments.TextMatrix(0, lngPayGridColCosts, "Costs");
			vsPayments.TextMatrix(0, lngPayGridColTotal, "Total");
		}

		private void mnuProcessGoTo_Click(object sender, System.EventArgs e)
		{
			if (!modExtraModules.IsThisCR())
			{
				if (boolPayment)
				{
					GoToStatus();
				}
				else
				{
					GoToPayment();
				}
			}
			if (boolPayment)
			{
				// MAL@20071231: 11581
				// lblAcctComment.Left = 200
				lblTaxAcquired.Left = 180;
				lblTaxAcquired.Top = 0;
                //FC:FINAL:BSE:#3547 should not move labels
				//lblAcctComment.Left = 560;
				//lblAcctComment.Top = 0;
				// kgk trout-731 09-20-2011
				//lblGrpInfo.Top = 210;
			}
			else
			{
				// lblAcctComment.Left = 0
				lblTaxAcquired.Left = 180;
				lblTaxAcquired.Top = 660;
                //FC:FINAL:BSE:#3547 should not move labels 
				//lblAcctComment.Left = 560;
				//lblAcctComment.Top = 660;
				// kgk trout-731 09-20-2011
				//lblGrpInfo.Top = 350;
			}
		}

		private void txtAcctNumber_Enter(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
			//if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R" || txtAcctNumber.Enabled == false)
			if (Strings.Left(cmbCode.Text, 1) == "A" || Strings.Left(cmbCode.Text, 1) == "R" || txtAcctNumber.Enabled == false)
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
		}

		private void txtAcctNumber_KeyDownEvent(object sender, KeyEventArgs e)
		{
			// left = 37
			// right = 39
			Keys KeyCode = e.KeyCode;
			switch (FCConvert.ToInt32(KeyCode))
			{
				case 37:
					{
						if (txtAcctNumber.EditSelStart == 0)
						{
							if (txtCash.Enabled == true)
							{
								if (txtCash.Visible && txtCash.Enabled)
								{
									txtCash.Focus();
								}
							}
							else
							{
								if (txtCD.Visible && txtCD.Enabled)
								{
									txtCD.Focus();
								}
							}
							KeyCode = 0;
						}
						break;
					}
				case 39:
					{
						if (txtAcctNumber.EditSelStart == txtAcctNumber.Text.Length - 1)
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
							KeyCode = 0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtAcctNumber_Validating(bool cancel)
		{
			txtAcctNumber_Validating(txtAcctNumber, new System.ComponentModel.CancelEventArgs(cancel));
		}

		private void txtAcctNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// validation of the BD account number
			string strAcct = "";
			if (txtCash.Enabled == true)
			{
				// If txtAcctNumber.Enabled = True Then
				// strAcct = txtacctnumber.TextMatrix(0,0)
				// If AccountValidate(strAcct) Then
				// Cancel = False
				// Else
				// Cancel = True
				// MsgBox "Please enter a valid account number.", vbOKOnly + vbInformation, "Invalid Account"
				// End If
				// End If
			}
			else
			{
				txtAcctNumber.Enabled = false;
			}
			clsDRWrapper rsF = new clsDRWrapper();
			boolDoNotContinueSave = false;
			if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gboolBD && txtCash.Text == "N" && txtCD.Text == "N")
			{
				// check the fun and make sure that it matches the current type's fund
				if ((Strings.InStr(1, txtAcctNumber.Text, "_", CompareConstants.vbBinaryCompare) == 0 || Strings.Left(txtAcctNumber.Text, 1) == "M") && txtAcctNumber.Text.Length > 2)
				{
					// If Left(txtAcctNumber.Text, 1) <> "M" Then
					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
					//if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "W")
					if (Strings.Left(cmbService.Text, 1) == "W")
					{
						rsF.OpenRecordset("SELECT Account1 FROM Type WHERE TypeCode = 93", modExtraModules.strCRDatabase);
					}
					else
					{
						rsF.OpenRecordset("SELECT Account1 FROM Type WHERE TypeCode = 94", modExtraModules.strCRDatabase);
					}
					if (!rsF.EndOfFile())
					{
						if (modBudgetaryAccounting.GetFundFromAccount(txtAcctNumber.Text) != modBudgetaryAccounting.GetFundFromAccount(rsF.Get_Fields_String("Account1")))
						{
							MessageBox.Show("This account's fund does not match the fund for this receipt type.", "Invalid Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							boolDoNotContinueSave = true;
						}
					}
					// End If
				}
			}
		}

		private void txtCash_DoubleClick(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
			//if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R")
			if (Strings.Left(cmbCode.Text, 1) == "A" || Strings.Left(cmbCode.Text, 1) == "R")
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
			else
			{
				if (txtCash.Text == "N")
				{
					txtCash.Text = "Y";
				}
				else
				{
					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
					//if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) != "B")
					if (Strings.Left(cmbService.Text, 1) != "B")
					{
						txtCash.Text = "N";
					}
					else
					{
						MessageBox.Show("Please select a service before selecting Non Cash.", "Invalid Service", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				CheckCash();
			}
		}

		public void txtCash_DblClick()
		{
			txtCash_DoubleClick(txtCash, new System.EventArgs());
		}

		private void txtCash_Enter(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
			//if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R")
			if (Strings.Left(cmbCode.Text, 1) == "A" || Strings.Left(cmbCode.Text, 1) == "R")
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
		}

		private void txtCash_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// left = 37
			// right = 39
			switch (KeyCode)
			{
				case Keys.Left:
					{
						if (txtCD.Visible && txtCD.Enabled)
						{
							txtCD.Focus();
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						if (txtCash.Text == "N")
						{
							if (txtAcctNumber.Visible && txtPrincipal.Enabled)
							{
								txtAcctNumber.Focus();
							}
						}
						else
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Space:
					{
						txtCash_DblClick();
						break;
					}
				case Keys.N:
					{
						//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
						//if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) != "B")
						if (Strings.Left(cmbService.Text, 1) != "B")
						{
							txtCash.Text = "N";
						}
						else
						{
							MessageBox.Show("Please select a service before selecting Non Cash.", "Invalid Service", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						break;
					}
				case Keys.Y:
					{
						txtCash.Text = "Y";
						break;
					}
				default:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
			CheckCash();
		}

		private void txtCash_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.N:
				case Keys.Y:
				case Keys.Space:
				case Keys.Back:
					{
						// do nothing
						break;
					}
				default:
					{
						KeyAscii = (Keys)0;
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCD_DoubleClick(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
			//if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R")
			if (Strings.Left(cmbCode.Text, 1) == "A" || Strings.Left(cmbCode.Text.ToString(), 1) == "R")
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
			else
			{
				if (txtCD.Text == "N")
				{
					txtCD.Text = "Y";
				}
				else
				{
					txtCD.Text = "N";
				}
				CheckCD();
			}
		}

		public void txtCD_DblClick()
		{
			txtCD_DoubleClick(txtCD, new System.EventArgs());
		}

		private void txtCD_Enter(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
			//if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R")
			if (Strings.Left(cmbCode.Text, 1) == "A" || Strings.Left(cmbCode.Text, 1) == "R")
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
		}

		private void txtCD_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						cmbCode.Focus();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtCD.Text == "N")
						{
							if (!modGlobalConstants.Statics.gboolCR && !modGlobalConstants.Statics.gboolBD)
							{
								txtCash.Enabled = false;
							}
							else
							{
								txtCash.Enabled = true;
							}
							if (txtCash.Visible && txtCash.Enabled)
							{
								txtCash.Focus();
							}
						}
						else
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Space:
					{
						txtCD_DblClick();
						break;
					}
				case Keys.N:
					{
						txtCD.Text = "N";
						break;
					}
				case Keys.Y:
					{
						txtCD.Text = "Y";
						break;
					}
				default:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
			CheckCD();
		}

		private void txtCD_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.N:
				case Keys.Y:
				case Keys.Space:
				case Keys.Back:
					{
						// do nothing
						break;
					}
				default:
					{
						KeyAscii = (Keys)0;
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtComments_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtComments.SelectionStart == 0)
						{
							if (txtCosts.Visible && txtCosts.Enabled)
							{
								txtCosts.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtComments.SelectionStart == txtComments.Text.Length)
						{
							vsPayments.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtCosts_Enter(object sender, System.EventArgs e)
		{
			txtCosts.SelectionStart = 0;
			txtCosts.SelectionLength = txtCosts.Text.Length;
		}

		private void txtCosts_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtCosts.SelectionStart == 0)
						{
							txtInterest.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtCosts.SelectionStart == txtCosts.Text.Length)
						{
							txtComments.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtCosts_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
			//if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R")
			if (Strings.Left(cmbCode.Text, 1) == "R")
			{
				KeyAscii = (Keys)0;
			}
			else
			{
				if ((KeyAscii == Keys.Back) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
				{
					// do nothing
				}
				else if (KeyAscii == Keys.Insert)
				{
					// minus
					KeyAscii = (Keys)0;
					if (Conversion.Val(txtCosts.Text) == 0)
					{
						txtCosts.Text = "0.00";
					}
					txtCosts.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtCosts.Text)) * -1, "#,##0.00");
					txtCosts.SelectionStart = 0;
					txtCosts.SelectionLength = txtCosts.Text.Length;
				}
				else if (KeyAscii == Keys.Delete)
				{
					// decimal point
					lngDecPlace = Strings.InStr(1, txtCosts.Text, ".", CompareConstants.vbBinaryCompare);
					if (lngDecPlace != 0)
					{
						if (txtCosts.SelectionStart < lngDecPlace && txtCosts.SelectionLength + txtCosts.SelectionStart >= lngDecPlace)
						{
							// if the decimal place is being highlighted and will be written over, then allow it
						}
						else
						{
							// if there is already a decimal point then stop others
							KeyAscii = (Keys)0;
						}
					}
				}
				else if (KeyAscii == Keys.Execute)
				{
					// plus
					KeyAscii = (Keys)0;
					if (Conversion.Val(txtCosts.Text) == 0)
					{
						txtCosts.Text = "0.00";
					}
					txtCosts.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtCosts.Text)), "#,##0.00");
					txtCosts.SelectionStart = 0;
					txtCosts.SelectionLength = txtCosts.Text.Length;
				}
				else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
				{
					// c, C - this will clear the box
					KeyAscii = (Keys)0;
					txtCosts.Text = "0.00";
					txtCosts.SelectionStart = 0;
					txtCosts.SelectionLength = 4;
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCosts_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (txtCosts.Text != "")
				{
					if (FCConvert.ToDecimal(txtCosts.Text) > 9999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $9,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtCosts.Text = Strings.Format(9999.99, "#,##0.00");
					}
					else if (FCConvert.ToDecimal(txtCosts.Text) < -9999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $-9,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtCosts.Text = Strings.Format(-9999.99, "#,##0.00");
					}
					else
					{
						txtCosts.Text = Strings.Format(FCConvert.ToDecimal(txtCosts.Text), "#,##0.00");
					}
				}
				else
				{
					txtCosts.Text = "0.00";
				}
				return;
			}
			catch (Exception ex)
			{
				
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtDiscDisc_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
                //FC:FINAL:SBE - global fix - validate event should not be triggered twice (issue #4605)
                //txtDiscPrin_Validate(false);
                Support.SendKeys("{TAB}");
            }
			else if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Delete) || (KeyAscii == Keys.Back))
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDiscDisc_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will check for the amount to be more than it should be
				e.Cancel = true;
				if ((FCConvert.ToDecimal(txtDiscDisc.Text) + FCConvert.ToDecimal(txtDiscPrin.Text)) > FCConvert.ToDecimal(lblDiscHiddenTotal.Text))
				{
					switch (MessageBox.Show("The amount entered is greater than the amount of principal owed for this year.  Do you wish to continue?", "Validation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
					{
						case DialogResult.Yes:
							{
								// continue
								e.Cancel = false;
								break;
							}
						case DialogResult.No:
							{
								// Cancel = False
								break;
							}
						case DialogResult.Cancel:
							{
								ShowDiscountFrame_2(false);
								break;
							}
					}
					//end switch
				}
				else
				{
					e.Cancel = false;
				}
				return;
			}
			catch (Exception ex)
			{
				
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtDiscPrin_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
                //FC:FINAL:SBE - global fix - validate event should not be triggered twice (issue #4605)
                //txtDiscPrin_Validate(false);
                Support.SendKeys("{TAB}");
            }
            else if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Delete) || (KeyAscii == Keys.Back))
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDiscPrin_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// checks for a positive value in principal
			// vbPorter upgrade warning: dblPrin As double	OnWrite(short, Decimal)
			double dblPrin = 0;
			double dblDisc = 0;
			bool boolGo;
			boolGo = true;
			if (txtDiscPrin.ReadOnly != true)
			{
				if (txtDiscPrin.Text == "")
				{
					dblPrin = 0;
				}
				else
				{
					dblPrin = FCConvert.ToDouble(FCConvert.ToDecimal(txtDiscPrin.Text));
				}
				dblDisc = CalculateDiscount_20(0, dblPrin, true);
				if (Conversion.Val(txtDiscPrin.Text) > 0)
				{
					if ((dblDisc + dblPrin) > FCConvert.ToDouble(lblDiscHiddenTotal.Text))
					{
						// If MsgBox("The amount entered is greater than the amount of principal owed for this year.  Do you wish to continue?", vbYesNoCancel + vbQuestion, "Validation") <> 7 Then
						// boolGo = False
						// Else
						// ShowDiscountFrame
						// End If
						switch (MessageBox.Show("The amount entered is greater than the amount of principal owed for this year.  Do you wish to continue?", "Validation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
						{
							case DialogResult.Yes:
								{
									// continue
									boolGo = false;
									break;
								}
							case DialogResult.No:
								{
									// Cancel = False
									break;
								}
							case DialogResult.Cancel:
								{
									ShowDiscountFrame_2(false);
									break;
								}
						}
						//end switch
					}
					if (boolGo)
					{
						dblPrin = FCConvert.ToDouble(FCConvert.ToDecimal(txtDiscPrin.Text));
						// dblDisc = CalculateDiscount(0, dblPrin)
						txtDiscDisc.Text = Strings.Format(dblDisc, "#,##0.00");
						lblDiscTotal.Text = Strings.Format(dblDisc + dblPrin, "#,##0.00");
					}
				}
				else
				{
					e.Cancel = true;
				}
			}
		}

		public void txtDiscPrin_Validate(bool Cancel)
		{
			txtDiscPrin_Validating(txtDiscPrin, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtInterest_Enter(object sender, System.EventArgs e)
		{
			txtInterest.SelectionStart = 0;
			txtInterest.SelectionLength = txtInterest.Text.Length;
		}

		private void txtInterest_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtInterest.SelectionStart == 0)
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtInterest.SelectionStart == txtInterest.Text.Length)
						{
							if (txtCosts.Visible && txtCosts.Enabled)
							{
								txtCosts.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtInterest_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
			//if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A")
			if (Strings.Left(cmbCode.Text, 1) == "R" || Strings.Left(cmbCode.Text, 1) == "A")
			{
				switch (KeyAscii)
				{
					case Keys.NumPad3:
					case Keys.C:
					case Keys.Back:
						{
							// c, C, 0 - this will clear the box
							KeyAscii = (Keys)0;
							txtInterest.Text = "0.00";
							txtInterest.SelectionStart = 0;
							txtInterest.SelectionLength = 4;
							break;
						}
				}
				//end switch
			}
			else
			{
				if ((KeyAscii == Keys.Back) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
				{
					// do nothing
				}
				else if (KeyAscii == Keys.Delete)
				{
					// decimal point
					lngDecPlace = Strings.InStr(1, txtInterest.Text, ".", CompareConstants.vbBinaryCompare);
					if (lngDecPlace != 0)
					{
						if (txtInterest.SelectionStart < lngDecPlace && txtInterest.SelectionLength + txtInterest.SelectionStart >= lngDecPlace)
						{
							// if the decimal place is being highlighted and will be written over, then allow it
						}
						else
						{
							// if there is already a decimal point then stop others
							KeyAscii = (Keys)0;
						}
					}
				}
				else if (KeyAscii == Keys.Insert)
				{
					// minus
					KeyAscii = (Keys)0;
					if (Conversion.Val(txtInterest.Text) == 0)
					{
						txtInterest.Text = "0.00";
					}
					txtInterest.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtInterest.Text)) * -1, "#,##0.00");
					txtInterest.SelectionStart = 0;
					txtInterest.SelectionLength = txtInterest.Text.Length;
				}
				else if (KeyAscii == Keys.Execute)
				{
					// plus
					KeyAscii = (Keys)0;
					if (Conversion.Val(txtInterest.Text) == 0)
					{
						txtInterest.Text = "0.00";
					}
					txtInterest.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtInterest.Text)), "#,##0.00");
					txtInterest.SelectionStart = 0;
					txtInterest.SelectionLength = txtInterest.Text.Length;
				}
				else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
				{
					// c, C - this will clear the box
					KeyAscii = (Keys)0;
					txtInterest.Text = "0.00";
					txtInterest.SelectionStart = 0;
					txtInterest.SelectionLength = 4;
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtInterest_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (txtInterest.Text != "")
				{
					if (FCConvert.ToDecimal(txtInterest.Text) > 9999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $9,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtInterest.Text = Strings.Format(9999999.99, "#,##0.00");
					}
					else if (FCConvert.ToDecimal(txtInterest.Text) < -9999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $-9,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtInterest.Text = Strings.Format(-9999999.99, "#,##0.00");
					}
					else
					{
						txtInterest.Text = Strings.Format(FCConvert.ToDecimal(txtInterest.Text), "#,##0.00");
					}
				}
				else
				{
					txtInterest.Text = "0.00";
				}
				return;
			}
			catch (Exception ex)
			{
				
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtPrincipal_Enter(object sender, System.EventArgs e)
		{
			txtPrincipal.SelectionStart = 0;
			txtPrincipal.SelectionLength = txtPrincipal.Text.Length;
		}

		private void txtPrincipal_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtPrincipal.SelectionStart == 0)
						{
							if (txtCD.Text == "N")
							{
								if (txtCash.Text == "N")
								{
									txtAcctNumber.Focus();
								}
								else
								{
									txtAcctNumber.Enabled = false;
									if (txtCash.Visible && txtCash.Enabled)
									{
										txtCash.Focus();
									}
								}
							}
							else
							{
								if (txtCD.Visible && txtCD.Enabled)
								{
									txtCD.Focus();
								}
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtPrincipal.SelectionStart == txtPrincipal.Text.Length)
						{
							txtTax.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtPrincipal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if ((KeyAscii == Keys.Back) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else if (KeyAscii == Keys.Delete)
			{
				// decimal point
				lngDecPlace = Strings.InStr(1, txtPrincipal.Text, ".", CompareConstants.vbBinaryCompare);
				if (lngDecPlace != 0)
				{
					if (txtPrincipal.SelectionStart < lngDecPlace && txtPrincipal.SelectionLength + txtPrincipal.SelectionStart >= lngDecPlace)
					{
						// if the decimal place is being highlighted and will be written over, then allow it
					}
					else
					{
						// if there is already a decimal point then stop others
						KeyAscii = (Keys)0;
					}
				}
			}
			else if (KeyAscii == Keys.Insert)
			{
				// minus
				KeyAscii = (Keys)0;
				if (Conversion.Val(txtPrincipal.Text) == 0)
				{
					txtPrincipal.Text = "0.00";
				}
				txtPrincipal.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtPrincipal.Text)) * -1, "#,##0.00");
				txtPrincipal.SelectionStart = 0;
				txtPrincipal.SelectionLength = txtPrincipal.Text.Length;
			}
			else if (KeyAscii == Keys.Execute)
			{
				// plus
				KeyAscii = (Keys)0;
				if (Conversion.Val(txtPrincipal.Text) == 0)
				{
					txtPrincipal.Text = "0.00";
				}
				txtPrincipal.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtPrincipal.Text)), "#,##0.00");
				txtPrincipal.SelectionStart = 0;
				txtPrincipal.SelectionLength = txtPrincipal.Text.Length;
			}
			else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
			{
				// c, C - this will clear the box
				KeyAscii = (Keys)0;
				txtPrincipal.Text = "0.00";
				txtPrincipal.SelectionStart = 0;
				txtPrincipal.SelectionLength = 4;
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtPrincipal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this is validaiting the amount entered
				if (txtPrincipal.Text != "")
				{
					if (FCConvert.ToDecimal(txtPrincipal.Text) > FCConvert.ToDecimal(99999999.99))
					{
						MessageBox.Show("This field cannot contain a value exceeding $99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtPrincipal.Text = Strings.Format(99999999.99, "#,##0.00");
					}
					else if (FCConvert.ToDecimal(txtPrincipal.Text) < FCConvert.ToDecimal(-99999999.99))
					{
						MessageBox.Show("This field cannot contain a value exceeding $-99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtPrincipal.Text = Strings.Format(-99999999.99, "#,##0.00");
					}
					else
					{
						txtPrincipal.Text = Strings.Format(FCConvert.ToDecimal(txtPrincipal.Text), "#,##0.00");
					}
				}
				else
				{
					txtPrincipal.Text = "0.00";
				}
				// this will check to see if this payment is an abatement or refunded abatement, if so then it will calculate the interest
				// automatically and the user can either keep it or zero the interest out
				// If Left$(cmbCode.list(cmbCode.ListIndex), 1) = "R" Or Left$(cmbCode.list(cmbCode.ListIndex), 1) = "A" Then
				// calculate the interest
				// txtInterest.Text = Format(CalculateAbatementInterest(CDbl(txtPrincipal.Text), CBool(Left(cmbService.list(cmbService.ListIndex), 1) = "W")), "#,##0.00")
				// End If
				return;
			}
			catch (Exception ex)
			{
				
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtReference_Enter(object sender, System.EventArgs e)
		{
			boolRefKeystroke = false;
			txtReference.BackColor = Color.White;
		}

		private void txtReference_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			bool boolTest = false;
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtReference.SelectionStart == 0)
						{
							if (txtTransactionDate.Visible && txtTransactionDate.Enabled)
							{
								txtTransactionDate.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtReference.SelectionStart == txtReference.Text.Length)
						{
							boolRefKeystroke = true;
							cmbService.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Return:
					{
						boolTest = false;
						txtReference_Validate(ref boolTest);
						if (boolTest)
						{
						}
						else
						{
							txtInterest.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtReference_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// catches reserved words
			if ((Strings.UCase(Strings.Trim(txtReference.Text)) == "CHGINT") || (Strings.UCase(Strings.Trim(txtReference)) == "CNVRSN") || (Strings.UCase(Strings.Trim(txtReference.Text)) == "REVRSE"))
			{
				MessageBox.Show(Strings.UCase(Strings.Trim(txtReference.Text)) + " is a reserved reference string.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}

		public void txtReference_Validate(ref bool Cancel)
		{
			txtReference_Validating(txtReference, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtTotalPendingDue_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// this will not allow any input to this text box
			KeyAscii = (Keys)0;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTransactionDate_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyValue == 37)
			{
				// left = 37
				if (txtTransactionDate.SelStart == 0)
				{
					cmbBillNumber.Focus();
					//FC:TODO:AM
					//e.KeyCode = 0;
				}
			}
			else if (e.KeyValue == 39)
			{
				// right = 39
				if (txtTransactionDate.SelStart == txtTransactionDate.Text.Length)
				{
					txtReference.Focus();
					//e.KeyCode = 0;
				}
			}
			else if (e.KeyCode == Keys.Return)
			{
				if (txtReference.Visible && txtReference.Enabled)
				{
					txtReference.Focus();
				}
				else
				{
					Support.SendKeys("{tab}", false);
				}
			}
			else if ((e.KeyCode == Keys.Back) || (e.KeyCode == Keys.Delete) || (e.KeyValue >= 48 && e.KeyValue <= 57) || (e.KeyValue == 191) || (e.KeyValue == 111) || (e.KeyValue >= 96 && e.KeyValue <= 105))
			{
				// backspace, delete, 0-9, "/", numberpad "/", 0-9 all can pass
			}
			else
			{
				//e.KeyCode = 0;
			}
		}

		private void txtTransactionDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// If txtTransactionDate = "" Then txtTransactionDate = UTEffectiveDate
			// If Not IsDate(txtTransactionDate.Text) Then
			// MsgBox "Please enter a valid date.", vbOKOnly + vbInformation, "Incorrect Date Format"
			// txtTransactionDate.SelStart = 0
			// txtTransactionDate.SelLength = Len(txtTransactionDate.Text)
			// Cancel = True
			// Else
			// txtTransactionDate.Text = Format(txtTransactionDate.Text, "MM/dd/yyyy")
			// End If
			if (Conversion.Val(Strings.Left(txtTransactionDate.Text, 2)) > 12)
			{
				e.Cancel = true;
				MessageBox.Show("Please use the date format MM/dd/yyyy.", "Invalid Date Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void vsPayments_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int l;
			if (e.KeyCode == Keys.Delete)
			{
				// If vsPayments.TextMatrix(vsPayments.Row, 2) <> "CHGINT" And vsPayments.TextMatrix(vsPayments.Row, 2) <> "EARNINT" Then
				if (vsPayments.Rows > 1)
				{
					if (MessageBox.Show("Are you sure that you would like to delete this payment?", "Delete Payment Record", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (vsPayments.TextMatrix(vsPayments.Row, lngPayGridColRef) == "AUTO" || vsPayments.TextMatrix(vsPayments.Row, lngPayGridColRef) == "OVERPAY" || vsPayments.TextMatrix(vsPayments.Row, lngPayGridColRef) == "PREPAY-A")
						{
							// if this is an automatic payment, then delete all the automatic payments
							for (l = vsPayments.Rows - 1; l >= 1; l--)
							{
								if (vsPayments.TextMatrix(l, lngPayGridColRef) == "AUTO" || vsPayments.TextMatrix(l, lngPayGridColRef) == "OVERPAY" || vsPayments.TextMatrix(l, lngPayGridColRef) == "PREPAY-A")
								{
									DeletePaymentRow(l);
								}
							}
						}
						else if (vsPayments.Row != 0)
						{
							DeletePaymentRow(vsPayments.Row);
						}
						modUTFunctions.CheckAllUTPending();
						// this resets all of the pending markers
					}
				}
				// End If
			}
			KeyCode = 0;
		}

		private void GoToStatus()
		{
			fraPayment.Visible = false;
			mnuPayment.Visible = false;
            cmdProcessGoTo.Width = 142;
            cmdPaymentPreview.Visible = false;
			cmdProcessGoTo.Text = strPaymentString;
            if (boolShowingWater)
			{
				if (WGRID.Enabled && WGRID.Visible)
				{
					WGRID.Focus();
				}
			}
			else
			{
				if (SGRID.Enabled && SGRID.Visible)
				{
					SGRID.Focus();
				}
			}
			cmdSavePayments.Enabled = false;
			cmdPaymentSaveExit.Enabled = false;
			boolPayment = false;
			SetHeadingLabels();
		}

		private void GoToPayment()
		{
			fraStatusLabels.Visible = false;
			fraPayment.Visible = true;
			mnuPayment.Visible = true;
			cmdPaymentPreview.Visible = true;
			vsPayments.Visible = true;
			cmdProcessGoTo.Text = strStatusString;
            cmdProcessGoTo.Width = 96;
			// Doevents
			this.Refresh();
			modUTFunctions.FillUTPaymentComboBoxes(OverrideServiceCodeForAccount(ref CurrentAccountKey));
			modUTFunctions.ResetUTPaymentFrame_6(boolShowingWater, false);
			Format_PaymentGrid();
			modUTFunctions.FillPendingUTTransactions();
			modUTFunctions.Format_UTvsPeriod();
			txtReference.Focus();
			cmdSavePayments.Enabled = true;
			cmdPaymentSaveExit.Enabled = true;
			boolPayment = true;
			SetHeadingLabels();
		}

		private bool NegativeBillValues(bool boolNegWater = false)
		{
			bool NegativeBillValues = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return true if any year has negative values
				int lngRow = 0;
				if (modUTFunctions.Statics.TownService != "S")
				{
					lngRow = 0;
					// check water
					do
					{
						lngRow = WGRID.FindRow("=", lngRow, lngGRIDColLineCode) + 1;
						if (lngRow > 0)
						{
							if (Conversion.Val(WGRID.TextMatrix(lngRow - 1, lngGRIDColTotal)) != 0)
							{
								if ((Conversion.Val(WGRID.TextMatrix(lngRow - 1, lngGridColBillKey)) != 0 || Strings.Right(WGRID.TextMatrix(LastParentRow_6(lngRow, true), lngGRIDColDate), 1) == "*") && WGRID.TextMatrix(LastParentRow_6(lngRow, true), lngGRIDColBillNumber) != "0")
								{
									if (FCConvert.ToDouble(WGRID.TextMatrix(lngRow - 1, lngGRIDColTotal)) < 0 && LastParentRow_6(lngRow, true) >= 1)
										NegativeBillValues = true;
								}
							}
						}
					}
					while (!(NegativeBillValues == true || WGRID.FindRow("=", lngRow, lngGRIDColLineCode) == -1));
					if (NegativeBillValues)
					{
						boolNegWater = true;
					}
				}
				if (modUTFunctions.Statics.TownService != "W")
				{
					lngRow = 0;
					// check sewer
					do
					{
						lngRow = SGRID.FindRow("=", lngRow, lngGRIDColLineCode) + 1;
						if (lngRow > 0)
						{
							if (Conversion.Val(SGRID.TextMatrix(lngRow - 1, lngGRIDColTotal)) != 0)
							{
								if ((Conversion.Val(SGRID.TextMatrix(lngRow - 1, lngGridColBillKey)) != 0 || Strings.Right(SGRID.TextMatrix(LastParentRow_6(lngRow, false), lngGRIDColDate), 1) == "*") && SGRID.TextMatrix(LastParentRow_6(lngRow, false), lngGRIDColBillNumber) != "0")
								{
									if (FCConvert.ToDouble(SGRID.TextMatrix(lngRow - 1, lngGRIDColTotal)) < 0 && LastParentRow_6(lngRow, false) >= 1)
										NegativeBillValues = true;
								}
							}
						}
					}
					while (!(NegativeBillValues == true || SGRID.FindRow("=", lngRow, lngGRIDColLineCode) == -1));
				}
				return NegativeBillValues;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Negative Values", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return NegativeBillValues;
		}

		private double AffectAllNegativeBillValues_2(bool boolWater)
		{
			return AffectAllNegativeBillValues(ref boolWater);
		}

		private double AffectAllNegativeBillValues(ref bool boolWater)
		{
			double AffectAllNegativeBillValues = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will create a payment against the negative value to zero it out and also
				// an equal amount against any newer bills, leaving the extra in the last year
				int lngRow;
				double dblNegTotal = 0;
				int lngBill = 0;
				if (boolWater)
				{
					// collapse all of the grid
					modGlobalFunctions.CollapseRows(0, WGRID, 1);
					// start at the oldest year and check for negative balances
					for (lngRow = WGRID.Rows - 2; lngRow >= 1; lngRow--)
					{
						if (WGRID.RowOutlineLevel(lngRow) == 1)
						{
							// check for the master row
							if (Conversion.Val(WGRID.TextMatrix(lngRow, lngGRIDColTotal)) != 0)
							{
								// make sure that the row is not text
								if (FCConvert.ToDouble(WGRID.TextMatrix(lngRow, lngGRIDColTotal)) < 0 && (FCConvert.ToDouble(WGRID.TextMatrix(lngRow, lngGRIDColBillNumber)) != 0 || Strings.Right(WGRID.TextMatrix(LastParentRow_6(lngRow, true), lngGRIDColDate), 1) == "*"))
								{
									// check for a value that below zero
									// dblNegTotal = dblNegTotal + CDbl(GRID.TextMatrix(lngRow, 9))
									// payment to zero out the year
									lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(WGRID.TextMatrix(lngRow, lngGRIDColBillNumber))));
									if (lngRow > 1)
									{
										dblNegTotal += AffectBillYear_164(true, lngBill, lngRow, boolWater, FCConvert.ToInt32(FCConvert.ToDouble(WGRID.TextMatrix(LastParentRow_6(lngRow, true), lngGRIDColPaymentKey))));
									}
									else
									{
										dblNegTotal += AffectBillYear_164(true, lngBill, lngRow, boolWater, FCConvert.ToInt32(FCConvert.ToDouble(WGRID.TextMatrix(1, lngGRIDColPaymentKey))));
									}
									// Exit Function
									// If lngRow > 1 Then
									// dblNegTotal = dblNegTotal - AffectBillYear(True, lngBill, lngRow, boolWater, CLng(WGRID.TextMatrix(LastParentRow(lngRow, True), lngGRIDColPaymentKey)))
									// Else
									// this is the newest bill and needs to have an overpayment line
									// dblNegTotal = dblNegTotal - AffectBillYear(True, lngBill, lngRow, boolWater, CLng(WGRID.TextMatrix(lngRow, lngGRIDColPaymentKey)))
									// End If
								}
								else if (dblNegTotal < 0)
								{
									// If CDbl(GRID.TextMatrix(lngRow, 9)) > dblNegTotal Or lngRow = 1 Then
									// the whole negative value will be used
									// dblNegTotal = dblNegTotal + AffectBillYear(False, FormatYear(GRID.TextMatrix(lngRow, 1)), lngRow)
									// Else
									// this year will not use the whole amt
									// dblNegTotal = AffectBillYear(False, CInt(GRID.TextMatrix(lngRow, 1)), lngRow)
									// End If
								}
							}
						}
					}
				}
				else
				{
					// collapse all of the grid
					modGlobalFunctions.CollapseRows(0, SGRID, 1);
					SGRID.Refresh();
					// start at the oldest year and check for negative balances
					for (lngRow = SGRID.Rows - 2; lngRow >= 1; lngRow--)
					{
						if (SGRID.RowOutlineLevel(lngRow) == 1)
						{
							// check for the master row
							if (Conversion.Val(SGRID.TextMatrix(lngRow, lngGRIDColTotal)) != 0)
							{
								// make sure that the row is not text
								if (FCConvert.ToDouble(SGRID.TextMatrix(lngRow, lngGRIDColTotal)) < 0 && (FCConvert.ToDouble(SGRID.TextMatrix(lngRow, lngGRIDColBillNumber)) != 0 || Strings.Right(SGRID.TextMatrix(LastParentRow_6(lngRow, false), lngGRIDColDate), 1) == "*"))
								{
									// check for a value that below zero
									// dblNegTotal = dblNegTotal + CDbl(GRID.TextMatrix(lngRow, 9))
									// payment to zero out the year
									lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(SGRID.TextMatrix(lngRow, lngGRIDColBillNumber))));
									if (lngRow > 1)
									{
										dblNegTotal += AffectBillYear_164(true, lngBill, lngRow, boolWater, FCConvert.ToInt32(FCConvert.ToDouble(SGRID.TextMatrix(LastParentRow_6(lngRow, false), lngGRIDColPaymentKey))));
									}
									else
									{
										dblNegTotal += AffectBillYear_164(true, lngBill, lngRow, boolWater, FCConvert.ToInt32(FCConvert.ToDouble(SGRID.TextMatrix(1, lngGRIDColPaymentKey))));
									}
									// Exit Function
								}
								else if (dblNegTotal < 0)
								{
									// If CDbl(GRID.TextMatrix(lngRow, 9)) > dblNegTotal Or lngRow = 1 Then
									// the whole negative value will be used
									// dblNegTotal = dblNegTotal + AffectBillYear(False, FormatYear(GRID.TextMatrix(lngRow, 1)), lngRow)
									// Else
									// this year will not use the whole amt
									// dblNegTotal = AffectBillYear(False, CInt(GRID.TextMatrix(lngRow, 1)), lngRow)
									// End If
								}
							}
						}
					}
				}
				AffectAllNegativeBillValues = dblNegTotal;
				return AffectAllNegativeBillValues;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Affecting Bill Values", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AffectAllNegativeBillValues;
		}

		private double AffectBillYear_164(bool boolNegative, int lngBill, int lngRow, bool boolWater, int lngBK, bool boolForceBillNumber = false)
		{
			return AffectBillYear(ref boolNegative, ref lngBill, ref lngRow, ref boolWater, ref lngBK, boolForceBillNumber);
		}

		private double AffectBillYear(ref bool boolNegative, ref int lngBill, ref int lngRow, ref bool boolWater, ref int lngBK, bool boolForceBillNumber = false)
		{
			double AffectBillYear = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will create a bill record to zero out the payments are best as possible
				// it will use as much as needed by the year to be payed off, or as much as it has in dblNegValue
				clsDRWrapper rs = new clsDRWrapper();
				clsDRWrapper rsPay = new clsDRWrapper();
				clsDRWrapper rsLien = new clsDRWrapper();
				string strSQL = "";
				double dblPrinNeed = 0;
				double dblTaxNeed = 0;
				double dblIntNeed = 0;
				double dblCostNeed = 0;
				double dblPrin;
				double dblInt;
				double dblCost;
				double dblNegValue = 0;
				int i;
				int intType = 0;
				string strWS = "";
				if (boolWater)
				{
					AffectBillYear = FCConvert.ToDouble(WGRID.TextMatrix(lngRow, lngGRIDColTotal));
					dblNegValue = FCConvert.ToDouble(WGRID.TextMatrix(lngRow, lngGRIDColTotal));
					intType = 0;
					strWS = "W";
				}
				else
				{
					AffectBillYear = FCConvert.ToDouble(SGRID.TextMatrix(lngRow, lngGRIDColTotal));
					dblNegValue = FCConvert.ToDouble(SGRID.TextMatrix(lngRow, lngGRIDColTotal));
					intType = 1;
					strWS = "S";
				}
				if (boolNegative)
				{
					// this creates a negative bill to zero out the neg year
					// this will return true if the creation was successful
					strSQL = "SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(CurrentAccountKey) + " AND Bill = " + FCConvert.ToString(Math.Abs(lngBK));
					rs.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						txtReference.Text = "AUTO";
						if (boolForceBillNumber)
						{
							for (i = 0; i <= cmbBillNumber.Items.Count - 1; i++)
							{
								if (Conversion.Val(cmbBillNumber.Items[i].ToString()) == lngBill)
								{
									cmbBillNumber.SelectedIndex = i;
									break;
								}
							}
						}
						cmbCode.SelectedIndex = 1;
						// If TownService = "B" Then
						// If boolWater Then
						// cmbService.ListIndex = 2
						// Else
						// cmbService.ListIndex = 1
						// End If
						// Else
						// cmbService.ListIndex = 0
						// End If
						for (i = 0; i <= cmbCode.Items.Count - 1; i++)
						{
							if (Strings.Left(cmbCode.Items[i].ToString(), 1) == "P")
							{
								cmbCode.SelectedIndex = i;
								break;
							}
						}
						if (FCConvert.ToInt32(rs.Get_Fields(strWS + "LienRecordNumber")) == 0)
						{
							// regular bill record
							dblPrinNeed = modGlobal.Round(rs.Get_Fields(strWS + "PrinOwed") - rs.Get_Fields(strWS + "PrinPaid"), 2);
							dblTaxNeed = rs.Get_Fields(strWS + "TaxOwed") - rs.Get_Fields(strWS + "TaxPaid");
							dblIntNeed = rs.Get_Fields(strWS + "IntOwed") - rs.Get_Fields(strWS + "IntAdded") + modUTFunctions.Statics.dblUTCurrentInt[lngBill, intType] - rs.Get_Fields(strWS + "IntPaid");
							dblCostNeed = rs.Get_Fields(strWS + "CostOwed") - rs.Get_Fields(strWS + "CostAdded") - rs.Get_Fields(strWS + "CostPaid");
						}
						else
						{
							// lien record
							strSQL = "SELECT * FROM Lien WHERE ID = " + rs.Get_Fields(strWS + "LienRecordNumber");
							rsLien.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
							dblPrinNeed = rsLien.Get_Fields("Principal") - rsLien.Get_Fields("PrinPaid");
							// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
							dblTaxNeed = rsLien.Get_Fields("Tax") - rsLien.Get_Fields("TaxPaid");
							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
							dblIntNeed = rsLien.Get_Fields("Interest") - rsLien.Get_Fields("IntAdded") - modUTFunctions.Statics.dblUTCurrentInt[lngBill, intType] - rsLien.Get_Fields("IntPaid") - rsLien.Get_Fields("PLIPaid");
							// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
							dblCostNeed = rsLien.Get_Fields("Costs") - rsLien.Get_Fields("MaturityFee") - rsLien.Get_Fields_Double("CostPaid");
						}
						if (dblPrinNeed == 0 && dblTaxNeed == 0 && dblIntNeed == 0 && dblCostNeed == 0)
						{
							return AffectBillYear;
						}
						if (dblPrinNeed < 0)
						{
							if (dblIntNeed > dblNegValue)
							{
								txtInterest.Text = Strings.Format(dblIntNeed, "#,##0.00");
								dblNegValue -= dblIntNeed;
							}
							else
							{
								txtInterest.Text = Strings.Format(dblNegValue, "#,##0.00");
								dblNegValue = 0;
							}
							if (dblCostNeed > dblNegValue)
							{
								txtCosts.Text = Strings.Format(dblCostNeed, "#,##0.00");
								dblNegValue -= dblCostNeed;
							}
							else
							{
								txtCosts.Text = Strings.Format(dblNegValue, "#,##0.00");
								dblNegValue = 0;
							}
							if (dblTaxNeed > dblNegValue)
							{
								txtTax.Text = Strings.Format(dblTaxNeed, "#,##0.00");
								dblNegValue -= dblTaxNeed;
							}
							else
							{
								txtTax.Text = Strings.Format(dblNegValue, "#,##0.00");
								dblNegValue = 0;
							}
							if (dblPrinNeed > dblNegValue)
							{
								txtPrincipal.Text = Strings.Format(dblPrinNeed, "#,##0.00");
								dblNegValue -= dblPrinNeed;
							}
							else
							{
								txtPrincipal.Text = Strings.Format(dblNegValue, "#,##0.00");
								dblNegValue = 0;
							}
						}
						else
						{
							if (dblIntNeed > Math.Abs(dblNegValue) && dblIntNeed > 0 && dblNegValue < 0)
							{
								txtInterest.Text = Strings.Format(dblIntNeed, "#,##0.00");
								dblNegValue -= dblIntNeed;
							}
							else if (dblIntNeed < 0)
							{
								// MAL@20071017: Removed the -1 because if a credit exists then the amount should be negative
								// txtInterest.Text = Format(dblIntNeed * -1, "#,##0.00")
								txtInterest.Text = Strings.Format(dblIntNeed, "#,##0.00");
								dblNegValue += dblIntNeed;
								dblIntNeed = 0;
							}
							else if (dblNegValue > 0)
							{
								txtInterest.Text = Strings.Format(dblNegValue, "#,##0.00");
								dblNegValue = 0;
							}
							if (dblCostNeed > Math.Abs(dblNegValue) && dblCostNeed > 0 && dblNegValue < 0)
							{
								txtCosts.Text = Strings.Format(dblCostNeed, "#,##0.00");
								dblNegValue -= dblCostNeed;
							}
							else if (dblCostNeed < 0)
							{
								txtCosts.Text = Strings.Format(dblCostNeed, "#,##0.00");
								dblNegValue += dblCostNeed;
								dblCostNeed = 0;
							}
							else if (dblNegValue > 0)
							{
								txtCosts.Text = Strings.Format(dblNegValue, "#,##0.00");
								dblNegValue = 0;
							}
							if (dblTaxNeed > Math.Abs(dblNegValue) && dblTaxNeed > 0 && dblNegValue < 0)
							{
								txtTax.Text = Strings.Format(dblTaxNeed, "#,##0.00");
								dblNegValue -= dblCostNeed;
							}
							else if (dblTaxNeed < 0)
							{
								txtTax.Text = Strings.Format(dblNegValue * -1, "#,##0.00");
								dblNegValue += dblTaxNeed;
								dblTaxNeed = 0;
							}
							else if (dblNegValue > 0)
							{
								txtTax.Text = Strings.Format(dblNegValue, "#,##0.00");
								dblNegValue = 0;
							}
							if (dblPrinNeed >= Math.Abs(dblNegValue) && dblPrinNeed > 0 && dblNegValue < 0)
							{
								txtPrincipal.Text = Strings.Format(dblPrinNeed, "#,##0.00");
								dblNegValue -= dblPrinNeed;
							}
							else if (dblPrinNeed < Math.Abs(dblNegValue) && dblPrinNeed > 0 && dblNegValue < 0)
							{
								txtPrincipal.Text = Strings.Format(dblPrinNeed, "#,##0.00");
								dblNegValue += dblPrinNeed;
								dblPrinNeed = 0;
							}
							else if (dblPrinNeed < 0)
							{
								txtPrincipal.Text = Strings.Format(dblPrinNeed * -1, "#,##0.00");
								dblNegValue += dblPrinNeed;
								dblPrinNeed = 0;
							}
							else if (dblNegValue > 0)
							{
								txtPrincipal.Text = Strings.Format(dblNegValue, "#,##0.00");
								dblNegValue = 0;
							}
						}
						// This should add the correcting entry to the original bill that is negative
						if (Conversion.Val(txtPrincipal.Text) != 0 || Conversion.Val(txtTax.Text) != 0 || Conversion.Val(txtInterest.Text) != 0 || Conversion.Val(txtCosts.Text) != 0)
						{
							if (boolWater)
							{
								modUTFunctions.AddUTPaymentToList_160755(vsPayments.Rows, true, true, lngBill, true, 0);
								// this will not let the negative principal get reset to other places
							}
							else
							{
								modUTFunctions.AddUTPaymentToList_160755(vsPayments.Rows, true, true, lngBill, true, 1);
								// this will not let the negative principal get reset to other places
							}
						}
						cmbBillNumber.SelectedIndex = cmbBillNumber.Items.Count - 1;
						// this will set it to AUTO
						// this gets set to auto so that the pre written code will take over
						// what is this part for???
						if (boolWater)
						{
							// This resets the amount that will be added to the next bill
							dblNegValue = FCConvert.ToDouble(WGRID.TextMatrix(lngRow, lngGRIDColTotal));
						}
						else
						{
							dblNegValue = FCConvert.ToDouble(SGRID.TextMatrix(lngRow, lngGRIDColTotal));
						}
						// txtInterest.Text = Format(dblNegValue * -1, "#,##0.00")
						// If dblIntNeed > dblNegValue Then
						// txtInterest.Text = Format(dblIntNeed * -1, "#,##0.00")
						// dblNegValue = dblNegValue - dblIntNeed
						// Else
						// txtInterest.Text = Format(dblNegValue * -1, "#,##0.00")
						// dblNegValue = 0
						// End If
						// 
						// If dblCostNeed > dblNegValue Then
						// txtCosts.Text = Format(dblCostNeed * -1, "#,##0.00")
						// dblNegValue = dblNegValue - dblCostNeed
						// Else
						// txtCosts.Text = Format(dblNegValue * -1, "#,##0.00")
						// dblNegValue = 0
						// End If
						// 
						// If dblPrinNeed > dblNegValue Then
						// txtPrincipal.Text = Format(dblPrinNeed * -1, "#,##0.00")
						// dblNegValue = dblNegValue - dblPrinNeed
						// Else
						// txtPrincipal.Text = Format(dblNegValue * -1, "#,##0.00")
						// dblNegValue = 0
						// End If
						// AddUTPaymentToList vsPayments.rows
					}
				}
				rs.Reset();
				rsPay.Reset();
				rsLien.Reset();
				return AffectBillYear;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Affecting Bill Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AffectBillYear;
		}

		private void DeletePaymentRow_2(int Row)
		{
			DeletePaymentRow(Row);
		}

		private void DeletePaymentRow(int Row)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				bool boolTemp;
				int intArrIndex;
				int lngIDNumber;
				int l;
				string strCode;
				int i;
				lngIDNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(Row, lngPayGridColKeyNumber))));
				intArrIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(Row, lngPayGridColArrayIndex))));
				strCode = vsPayments.TextMatrix(Row, 4);
				boolTemp = EraseCHGINTPaymentLine(ref Row);
				if (intArrIndex > 0)
				{
					modUTFunctions.ResetUTPayment(ref intArrIndex, boolTemp, lngIDNumber);
					// this will delete if from the database if it is already stored and clear the payment array
				}
				if (lngIDNumber != 0)
				{
					for (l = 1; l <= vsPayments.Rows - 1; l++)
					{
						if (Conversion.Val(vsPayments.TextMatrix(l, lngPayGridColKeyNumber)) == lngIDNumber)
						{
							if (l != 0)
							{
								vsPayments.RemoveItem(l);
								// remove the line from the grid
							}
							break;
						}
					}
				}
				else
				{
					if (vsPayments.Rows > 2)
					{
						if (strCode == "R")
						{
							for (i = 0; i <= vsPayments.Rows - 1; i++)
							{
								lngIDNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(i, 10))));
								if (modExtraModules.IsThisCR() && lngIDNumber > 0)
								{
									modUseCR.DeleteUTPaymentFromShoppingList(ref lngIDNumber);
								}
							}
							// i
							vsPayments.Rows = 1;
							FCUtils.EraseSafe(modUTFunctions.Statics.UTPaymentArray);
						}
						else
						{
							// more than one payment showing
							vsPayments.RemoveItem(Row);
						}
					}
					else
					{
						vsPayments.Rows = 1;
						// clear the grid out
						FCUtils.EraseSafe(modUTFunctions.Statics.UTPaymentArray);
						// erase all the payments
					}
				}
				// check for CR
				if (modExtraModules.IsThisCR() && lngIDNumber > 0)
				{
					// this will delete the payment from the shopping list
					modUseCR.DeleteUTPaymentFromShoppingList(ref lngIDNumber);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Deleting Payment", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool EraseCHGINTPaymentLine(ref int Row)
		{
			bool EraseCHGINTPaymentLine = false;
			// this will check to see if there is a CHGINT line associated with the payment that is being
			// erased and find out if there is any other payments that it should be associated with instead of being erased
			// and will associate it, if needed
			int lngIndex;
			clsDRWrapper rsCHGINT = new clsDRWrapper();
			clsDRWrapper rsTemp = new clsDRWrapper();
			int lngID;
			int lngCHGINTNumber = 0;
			int i;
			bool boolEraseCHGINT;
			int lngDeleteRow;
			int lngNew = 0;
			int lngCHGINTRow = 0;
			lngDeleteRow = Row;
			// find out if the payment being deleted has a CHGINT or EARNINT line with it
			lngIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(lngDeleteRow, lngPayGridColArrayIndex))));
			lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(lngDeleteRow, lngPayGridColKeyNumber))));
			if (lngIndex > 0 && lngIndex < modUTFunctions.MAX_UTPAYMENTS && lngID > 0)
			{
				// If lngIndex > 0 And lngIndex < MAX_UTPAYMENTS Then
				rsCHGINT.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE ID = " + FCConvert.ToString(lngID), modExtraModules.strUTDatabase);
				if (rsCHGINT.EndOfFile() != true && rsCHGINT.BeginningOfFile() != true)
				{
					if (FCConvert.ToInt32(rsCHGINT.Get_Fields_Int32("CHGINTNumber")) != 0)
					{
						lngCHGINTNumber = FCConvert.ToInt32(rsCHGINT.Get_Fields_Int32("CHGINTNumber"));
					}
				}
				else if (modUTFunctions.Statics.UTPaymentArray[lngIndex].CHGINTNumber != 0)
				{
					lngCHGINTNumber = modUTFunctions.Statics.UTPaymentArray[lngIndex].CHGINTNumber;
				}
				lngNew = 0;
				// default to true then attempt to prove otherwise
				if (lngCHGINTNumber != 0)
				{
					// if there is a CHGINT or EARNINT line
					// find out if there is another line that would require a CHGINT/EARNINT line
					for (i = 1; i <= vsPayments.Rows - 1; i++)
					{
						if (!rsCHGINT.EndOfFile())
						{
							if (Conversion.Val(Strings.Right(vsPayments.TextMatrix(i, lngPayGridColBill), 4)) == rsCHGINT.Get_Fields_Int32("BillNumber") && Strings.Left(vsPayments.TextMatrix(i, lngPayGridColService), 1) == FCConvert.ToString(rsCHGINT.Get_Fields_String("Service")))
							{
								// kk01192015 Add check for same service 'check for the same bill
								if (vsPayments.TextMatrix(i, lngPayGridColRef) != "CHGINT" && vsPayments.TextMatrix(i, lngPayGridColRef) != "EARNINT" && Conversion.Val(vsPayments.TextMatrix(i, lngPayGridColKeyNumber)) != lngID && modUTFunctions.Statics.UTPaymentArray[FCConvert.ToInt32(vsPayments.TextMatrix(i, lngPayGridColArrayIndex))].EffectiveInterestDate.ToOADate() == rsCHGINT.Get_Fields_DateTime("EffectiveInterestDate").ToOADate())
								{
									// check to see if it is a CHGINT or EARNINT and has the same effective interest date
									lngNew = i;
									// this will give the row of the first payment during the same year
									break;
								}
							}
						}
					}
					// find the row that the CHGINT line is stored in the grid
					for (i = 1; i <= vsPayments.Rows - 1; i++)
					{
						if (Conversion.Val(vsPayments.TextMatrix(i, lngPayGridColKeyNumber)) == lngCHGINTNumber)
						{
							lngCHGINTRow = i;
							break;
						}
					}
					// delete the CHGINT or EARNINT line
					if (lngNew == 0)
					{
						// if there is no line to hook the CHGINT or EARNINT to
						// erase the line and the corresponding record
						rsCHGINT.Execute("DELETE FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngCHGINTNumber), modExtraModules.strUTDatabase);
						if (lngCHGINTRow > 0)
						{
							vsPayments.RemoveItem(lngCHGINTRow);
						}
						EraseCHGINTPaymentLine = true;
					}
					else
					{
						// change the CHGINT of the next payment made for that same account/year/?EDate
						if (Conversion.Val(vsPayments.TextMatrix(lngNew, lngPayGridColKeyNumber)) == 0)
						{
							EraseCHGINTPaymentLine = false;
							modUTFunctions.Statics.UTPaymentArray[FCConvert.ToInt32(vsPayments.TextMatrix(lngNew, lngPayGridColArrayIndex))].CHGINTNumber = lngCHGINTNumber;
							vsPayments.TextMatrix(lngNew, lngPayGridColCHGINTNumber, FCConvert.ToString(lngCHGINTNumber));
							if (FCConvert.ToDouble(vsPayments.TextMatrix(lngNew, lngPayGridColKeyNumber)) != 0)
							{
								// if the payment line has been saved in the database then
								// kk01192015 trout-1037  Get rid of the RE/PP code here
								// If boolRE Then
								rsCHGINT.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE ID = " + vsPayments.TextMatrix(lngNew, lngPayGridColKeyNumber), modExtraModules.strUTDatabase);
								// & " AND BillCode <> 'P'"
								// Else
								// rsCHGINT.OpenRecordset "SELECT * FROM (" & strUTCurrentAccountPayments & ") AS qTmp WHERE ID = " & vsPayments.TextMatrix(lngNew, lngPayGridColKeyNumber) & " AND BillCode = 'P'", strUTDatabase
								// End If
								if (rsCHGINT.EndOfFile() != true && rsCHGINT.BeginningOfFile() != true)
								{
									rsCHGINT.Edit();
									rsCHGINT.Set_Fields("CHGINTNumber", lngCHGINTNumber);
									rsCHGINT.Update(false);
								}
							}
						}
						else
						{
							// erase the payment line and the corresponding record
							// kk01192015 trout-1037  Get rid of the RE/PP code here
							// If boolRE Then
							rsCHGINT.Execute("DELETE FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngCHGINTNumber), modExtraModules.strUTDatabase);
							// & "AND BillCode <> 'P'"
							// Else
							// rsCHGINT.Execute "DELETE FROM PaymentRec WHERE ID = " & lngCHGINTNumber & "AND BillCode = 'P'", strUTDatabase
							// End If
							vsPayments.RemoveItem(lngCHGINTRow);
							EraseCHGINTPaymentLine = true;
						}
					}
				}
			}
			else
			{
				if (!modExtraModules.IsThisCR() && modUTFunctions.Statics.UTPaymentArray[lngIndex].CHGINTNumber != 0)
				{
					lngCHGINTNumber = modUTFunctions.Statics.UTPaymentArray[lngIndex].CHGINTNumber;
					rsCHGINT.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngCHGINTNumber), modExtraModules.strUTDatabase);
					// this will have to reverse the change in the lien/bill record
					if (modUTFunctions.Statics.UTPaymentArray[lngIndex].BillCode == "L")
					{
						// adjust the lien record
						rsTemp.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(modUTFunctions.Statics.UTPaymentArray[lngIndex].BillKey), modExtraModules.strUTDatabase);
						if (rsTemp.RecordCount() != 0 && rsCHGINT.RecordCount() != 0)
						{
							rsTemp.Edit();
							// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
							rsTemp.Set_Fields("IntAdded", (rsTemp.Get_Fields("IntAdded") - rsCHGINT.Get_Fields_Decimal("CurrentInterest")));
							rsTemp.Update();
						}
					}
					else
					{
						// adjust the bill record
						rsTemp.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE Bill = " + FCConvert.ToString(modUTFunctions.Statics.UTPaymentArray[lngIndex].BillKey), modExtraModules.strUTDatabase);
						if (rsTemp.RecordCount() != 0 && rsCHGINT.RecordCount() != 0)
						{
							rsTemp.Edit();
							// If UTPaymentArray(lngIndex).Service = "W" Then
							if (FCConvert.ToString(rsCHGINT.Get_Fields_String("Service")) == "W")
							{
								rsTemp.Set_Fields("WIntAdded", (rsTemp.Get_Fields_Double("WIntAdded") - FCConvert.ToDouble(rsCHGINT.Get_Fields_Decimal("CurrentInterest"))));
							}
							else
							{
								rsTemp.Set_Fields("SIntAdded", (rsTemp.Get_Fields_Double("SIntAdded") - FCConvert.ToDouble(rsCHGINT.Get_Fields_Decimal("CurrentInterest"))));
							}
							rsTemp.Update();
						}
					}
				}
				if (lngCHGINTRow > 0)
				{
					vsPayments.RemoveItem(lngCHGINTRow);
				}
				else
				{
					if (lngIndex > 0 && lngIndex < modUTFunctions.MAX_UTPAYMENTS)
					{
						if (modUTFunctions.Statics.UTPaymentArray[lngIndex].CHGINTNumber != 0)
						{
							lngID = modUTFunctions.Statics.UTPaymentArray[lngIndex].CHGINTNumber;
							rsCHGINT.Execute("DELETE FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngID), modExtraModules.strUTDatabase);
						}
					}
				}
			}
			rsCHGINT.Reset();
			return EraseCHGINTPaymentLine;
		}

		private void vsPeriod_DblClick(object sender, System.EventArgs e)
		{
			int intCT;
			double dblAmt;
			if (boolDisableInsert)
			{
				// kk03132015  trocrs-36  Add option to disable automatically filling in payment amount
				return;
			}
			dblAmt = FCConvert.ToDouble(vsPeriod.TextMatrix(0, 1));
			// force Auto into the Bill Number combo
			for (intCT = 0; intCT <= cmbBillNumber.Items.Count - 1; intCT++)
			{
				if (Strings.Left(cmbBillNumber.Items[intCT].ToString(), 1) == "A")
				{
					cmbBillNumber.SelectedIndex = intCT;
					break;
				}
			}
			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
			//if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "B")
			if (Strings.Left(cmbService.Text, 1) == "B")
			{
				txtInterest.Text = Strings.Format(FCConvert.ToDouble(vsPeriod.TextMatrix(0, 1)), "#,##0.00");
			}
			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
			//else if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "W")
			else if (Strings.Left(cmbService.Text, 1) == "W")
			{
				modUTFunctions.CreateUTOppositionLine_8(WGRID.Rows - 1, true);
				// create an opposition line for the whole account for water
			}
				//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
				//else if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "S")
				else if (Strings.Left(cmbService.Text, 1) == "S")
			{
				modUTFunctions.CreateUTOppositionLine_8(SGRID.Rows - 1, false);
				// create an opposition line for the whole account for sewer
			}
			vsPeriod.TextMatrix(0, 1, Strings.Format(dblAmt, "#,##0.00"));
			txtInterest.Text = Strings.Format(dblAmt, "#,##0.00");
		}

		private void vsPreview_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsPreview.Visible)
			{
				HidePreview();
				// mnuPaymentPreview.Caption = "Preview"
				// If boolShowingWater Then
				// WGRID.Visible = True
				// Else
				// SGRID.Visible = True
				// End If
				// vsPreview.Visible = False
				// vsPreview.rows = 1
			}
		}

		private void vsRateInfo_DblClick(object sender, System.EventArgs e)
		{
			double dbl1 = 0;
			double dbl2 = 0;
			double dbl3 = 0;
			double dbl4 = 0;
			if (boolPayment)
			{
				if (vsRateInfo.TextMatrix(vsRateInfo.Row, 1) == "")
					return;
				if (vsRateInfo.TextMatrix(15, 1) != "")
					dbl1 = FCConvert.ToDouble(vsRateInfo.TextMatrix(15, 1));
				if (vsRateInfo.TextMatrix(16, 1) != "")
					dbl2 = FCConvert.ToDouble(vsRateInfo.TextMatrix(16, 1));
				if (vsRateInfo.TextMatrix(17, 1) != "")
					dbl3 = FCConvert.ToDouble(vsRateInfo.TextMatrix(17, 1));
				if (vsRateInfo.TextMatrix(18, 1) != "")
					dbl4 = FCConvert.ToDouble(vsRateInfo.TextMatrix(18, 1));
				int vbPorterVar = vsRateInfo.Row;
				if (vbPorterVar >= 15 && vbPorterVar <= 18)
				{
					CreatePeriodOppositionLine_648(dbl1, dbl2, dbl3, dbl4, FCConvert.ToInt16(vsRateInfo.Row - 14), FCConvert.ToInt32(Conversion.Val(Strings.Right(fraRateInfo.Text, 6))));
				}
				else
				{
				}
			}
		}
		// vbPorter upgrade warning: intPer As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: lngBill As int	OnWriteFCConvert.ToDouble(
		private void CreatePeriodOppositionLine_648(double dblPer1, double dblPer2, double dblPer3, double dblPer4, short intPer, int lngBill)
		{
			CreatePeriodOppositionLine(ref dblPer1, ref dblPer2, ref dblPer3, ref dblPer4, ref intPer, ref lngBill);
		}

		private void CreatePeriodOppositionLine(ref double dblPer1, ref double dblPer2, ref double dblPer3, ref double dblPer4, ref short intPer, ref int lngBill)
		{
			// this will create an opposition line for the period clicked on
			int intCT;
			double dblP = 0;
			double dblT = 0;
			double dblI = 0;
			double dblC = 0;
			double dblTemp;
			clsDRWrapper rsTemp = new clsDRWrapper();
			bool boolCombine;
			int intType = 0;
			string strWS = "";
			boolCombine = !txtPrincipal.Visible;
			switch (FindPaymentType())
			{
				case 0:
					{
						intType = 0;
						strWS = "W";
						break;
					}
				case 1:
					{
						intType = 1;
						strWS = "S";
						break;
					}
			}
			//end switch
			// find what is needed for the bill chosen
			rsTemp.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(modUTFunctions.Statics.lngCurrentAccountKeyUT) + " AND BillNumber = " + FCConvert.ToString(lngBill), modExtraModules.strUTDatabase);
			if (rsTemp.EndOfFile() != true)
			{
				if (FCConvert.ToInt32(rsTemp.Get_Fields(strWS + "LienRecordNumber")) == 0)
				{
					dblP = rsTemp.Get_Fields(strWS + "PrinOwed") - rsTemp.Get_Fields(strWS + "PrinPaid");
					dblT = rsTemp.Get_Fields(strWS + "TaxOwed") - rsTemp.Get_Fields(strWS + "TaxPaid");
					dblI = (modUTFunctions.Statics.dblUTCurrentInt[lngBill, intType] * -1) - rsTemp.Get_Fields(strWS + "IntAdded") - rsTemp.Get_Fields(strWS + "IntPaid");
					dblC = rsTemp.Get_Fields(strWS + "CostOwed") - rsTemp.Get_Fields(strWS + "CostAdded") - rsTemp.Get_Fields(strWS + "CostPaid");
				}
				else
				{
					rsTemp.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTemp.Get_Fields(strWS + "LienRecordNumber"), modExtraModules.strUTDatabase);
					if (rsTemp.EndOfFile() != true)
					{
						// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
						dblP = rsTemp.Get_Fields("Principal") - rsTemp.Get_Fields("PrinPaid");
						// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
						dblT = rsTemp.Get_Fields("Tax") - rsTemp.Get_Fields("TaxPaid");
						// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
						dblI = (modUTFunctions.Statics.dblUTCurrentInt[lngBill, intType] * -1) + rsTemp.Get_Fields("Interest") - rsTemp.Get_Fields("IntAdded") - rsTemp.Get_Fields("PLIPaid") - rsTemp.Get_Fields("IntPaid");
						// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
						dblC = rsTemp.Get_Fields("Costs") - rsTemp.Get_Fields("MaturityFee") - rsTemp.Get_Fields_Double("CostPaid");
					}
				}
			}
			else
			{
				// this is a lien
				dblP = 0;
				dblT = 0;
				dblI = dblPer1 + dblPer2 + dblPer3 + dblPer4;
				dblC = 0;
			}
			// set the year
			for (intCT = 0; intCT <= cmbBillNumber.Items.Count - 1; intCT++)
			{
				if (Conversion.Val(modExtraModules.FormatYear(cmbBillNumber.Items[intCT].ToString())) == lngBill)
				{
					cmbBillNumber.SelectedIndex = intCT;
					break;
				}
			}
			dblTemp = dblP + dblT + dblI + dblC;
			if (boolCombine)
			{
				// put it all into one field
				txtInterest.Text = Strings.Format(dblTemp, "#,##0.00");
				txtPrincipal.Text = "0.00";
				txtCosts.Text = "0.00";
				txtTax.Text = "0.00";
			}
			else
			{
				// spread the payment out
				//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
				//if ((Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "C") || (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A"))
				if ((Strings.Left(cmbCode.Text, 1) == "C") || (Strings.Left(cmbCode.Text, 1) == "A"))
				{
					// spread it out
					txtPrincipal.Text = Strings.Format(dblP, "#,##0.00");
					txtTax.Text = Strings.Format(dblT, "#,##0.00");
					txtInterest.Text = Strings.Format(dblI, "#,##0.00");
					txtCosts.Text = Strings.Format(dblC, "#,##0.00");
				}
				//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
				//else if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "P")
				else if (Strings.Left(cmbCode.Text, 1) == "P")
				{
					// put it all into one field
					txtInterest.Text = Strings.Format(dblTemp, "#,##0.00");
					txtPrincipal.Text = "0.00";
					txtCosts.Text = "0.00";
					txtTax.Text = "0.00";
				}
				else
				{
					// do nothing
				}
			}
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
			rsTemp.Reset();
		}

		public string GetUTAccount_2(string strType, int lngType)
		{
			return GetUTAccount(strType, lngType);
		}

		public string GetUTAccount_6(string strType, int lngType)
		{
			return GetUTAccount(strType, lngType);
		}

		public string GetUTAccount(string strType, int lngType)
		{
			string GetUTAccountRet = "";
			clsDRWrapper rsBD = new clsDRWrapper();
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return the correct Account number to use in the Account Box				
				string strSvc = "";
				if (modGlobalConstants.Statics.gboolCR && modGlobalConstants.Statics.gboolBD)
				{
					rsBD.OpenRecordset("SELECT * FROM Type WHERE TypeCode = " + FCConvert.ToString(lngType), modExtraModules.strCRDatabase);
					if (!rsBD.EndOfFile())
					{
						//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
						//strSvc = Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1);
						strSvc = Strings.Left(cmbService.Text, 1);
						switch (lngType)
						{
							case 93:
								{
									// Water Payment
									if (strSvc != "S")
									{
										if (modUTFunctions.Statics.TownService != "S")
										{
											if (strType == "A")
											{
												if (modValidateAccount.AccountValidate(rsBD.Get_Fields_String("Account6")))
												{
													GetUTAccountRet = rsBD.Get_Fields_String("Account6");
												}
												else
												{
													GetUTAccountRet = "";
												}
											}
											else if (strType == "D")
											{
												if (modValidateAccount.AccountValidate(rsBD.Get_Fields_String("Account3")))
												{
													GetUTAccountRet = rsBD.Get_Fields_String("Account3");
												}
												else
												{
													GetUTAccountRet = "";
												}
											}
										}
										else
										{
											GetUTAccountRet = "M ABATEMENT";
										}
									}
									else
									{
										GetUTAccountRet = GetUTAccount_6(strType, 94);
									}
									break;
								}
							case 94:
								{
									// Sewer Payment
									if (strSvc != "W")
									{
										if (modUTFunctions.Statics.TownService != "W")
										{
											if (strType == "A")
											{
												if (modValidateAccount.AccountValidate(rsBD.Get_Fields_String("Account6")))
												{
													GetUTAccountRet = rsBD.Get_Fields_String("Account6");
												}
												else
												{
													GetUTAccountRet = "";
												}
											}
											else if (strType == "D")
											{
												if (modValidateAccount.AccountValidate(rsBD.Get_Fields_String("Account3")))
												{
													GetUTAccountRet = rsBD.Get_Fields_String("Account3");
												}
												else
												{
													GetUTAccountRet = "";
												}
											}
										}
										else
										{
											GetUTAccountRet = "M ABATEMENT";
										}
									}
									else
									{
										GetUTAccountRet = GetUTAccount_6(strType, 93);
									}
									break;
								}
							case 95:
								{
									// Sewer Lien Payment
									if (strSvc != "W")
									{
										if (modUTFunctions.Statics.TownService != "W")
										{
											if (strType == "A")
											{
												if (modValidateAccount.AccountValidate(rsBD.Get_Fields_String("Account6")))
												{
													GetUTAccountRet = rsBD.Get_Fields_String("Account6");
												}
												else
												{
													GetUTAccountRet = "";
												}
											}
											else if (strType == "D")
											{
												if (modValidateAccount.AccountValidate(rsBD.Get_Fields_String("Account3")))
												{
													GetUTAccountRet = rsBD.Get_Fields_String("Account3");
												}
												else
												{
													GetUTAccountRet = "";
												}
											}
										}
										else
										{
											GetUTAccountRet = "M ABATEMENT";
										}
									}
									else
									{
										GetUTAccountRet = GetUTAccount_6(strType, 96);
									}
									break;
								}
							case 96:
								{
									// Water Lien Payment
									if (strSvc != "S")
									{
										if (modUTFunctions.Statics.TownService != "S")
										{
											if (strType == "A")
											{
												if (modValidateAccount.AccountValidate(rsBD.Get_Fields_String("Account6")))
												{
													GetUTAccountRet = rsBD.Get_Fields_String("Account6");
												}
												else
												{
													GetUTAccountRet = "";
												}
											}
											else if (strType == "D")
											{
												if (modValidateAccount.AccountValidate(rsBD.Get_Fields_String("Account3")))
												{
													GetUTAccountRet = rsBD.Get_Fields_String("Account3");
												}
												else
												{
													GetUTAccountRet = "";
												}
											}
										}
										else
										{
											GetUTAccountRet = "M ABATEMENT";
										}
									}
									else
									{
										GetUTAccountRet = GetUTAccount_6(strType, 95);
									}
									break;
								}
						}
						//end switch
						if (GetUTAccountRet == "")
						{
							MessageBox.Show("Error in type " + FCConvert.ToString(lngType) + ".  Please make sure that the account is setup.", "Account Setup Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							if (strType == "A")
							{
								GetUTAccountRet = "M ABATEMENT";
							}
							else if (strType == "D")
							{
								if (lngType == 96 || lngType == 97)
								{
									MessageBox.Show("You cannot discount a lien.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
								else
								{
									GetUTAccountRet = "M DISCOUNT";
								}
							}
						}
					}
					else
					{
						// UT Payment - missing type code in CR
						if (strType == "A")
						{
							GetUTAccountRet = "M ABATEMENT";
						}
						else if (strType == "D")
						{
							GetUTAccountRet = "M DISCOUNT";
						}
					}
				}
				else
				{
					// UT Payment - no BD or CR
					if (strType == "A")
					{
						GetUTAccountRet = "M ABATEMENT";
					}
					else if (strType == "D")
					{
						GetUTAccountRet = "M DISCOUNT";
					}
				}
				rsBD.Reset();
				return GetUTAccountRet;
			}
			catch (Exception ex)
			{
				
				GetUTAccountRet = "";
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Account Information Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				rsBD.Reset();
			}
			return GetUTAccountRet;
		}

		private void FillInUTInfo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsMainInfo = new clsDRWrapper();
//				rsMainInfo.OpenRecordset("SELECT c.*, p.FullNameLF as OwnerFullNameLF, q.FullNameLF as SecondOwnerFullNameLF FROM Master as c CROSS APPLY " + rsMainInfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.OwnerPartyID) as p CROSS APPLY " + rsMainInfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.SecondOwnerPartyID) as q WHERE AccountNumber = " + FCConvert.ToString(CurrentAccount), modExtraModules.strUTDatabase);
				rsMainInfo.OpenRecordset(modUTFunctions.UTMasterQuery(CurrentAccountKey), modExtraModules.strUTDatabase);
				if (!rsMainInfo.EndOfFile())
				{
					// rsCL.FindFirstRecord "BillingYear", Year(Date)
					// TODO: Field [SecondOwnerName] not found!! (maybe it is an alias?)
					if (Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields("SecondOwnerName"))) != "")
					{
						// TODO: Field [OwnerFullNameLF] not found!! (maybe it is an alias?)
						// TODO: Field [SecondOwnerFullNameLF] not found!! (maybe it is an alias?)
						lblOwnersName.Text = Strings.Trim(rsMainInfo.Get_Fields("OwnerName") + ", " + rsMainInfo.Get_Fields("SecondOwnerName"));
						// fill in the labels with the record information
					}
					else
					{
						// TODO: Field [OwnerFullNameLF] not found!! (maybe it is an alias?)
						lblOwnersName.Text = Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields("OwnerName")));
					}
					OwnerPartyID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMainInfo.Get_Fields_Int32("OwnerPartyID"))));
					// Set PartyID for CR
					CurrentOwner = Strings.Trim(lblOwnersName.Text);
					lblOwnersName.Visible = true;
					lblOwnersName.BackColor = Color.White;
					lblMapLot.Text = Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields_String("MapLot")));
					lblMapLot.Visible = true;
					lblMapLot.BackColor = Color.White;
					lblLocation.Text = Strings.Trim(Strings.Trim(rsMainInfo.Get_Fields_Int32("StreetNumber") + " ") + " " + Strings.Trim(rsMainInfo.Get_Fields_String("Apt") + " ") + " " + Strings.Trim(rsMainInfo.Get_Fields_String("StreetName") + " "));
					if (Conversion.Val(lblLocation.Text) == 0 && Strings.Trim(FCConvert.ToString(rsMainInfo.Get_Fields_String("StreetName"))) == "")
						lblLocation.Text = "";
					lblLocation.Visible = true;
					lblLocation.BackColor = Color.White;
					lblAccount.BackColor = Color.White;
					//FC:FINAL:DDU:#1068 - move account to headertext as in TWCL but let lblaccount text here to don't change logic part
					//lblAccount.Visible = true;
					// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					lblAccount.Text = FCConvert.ToString(rsCL.Get_Fields("AccountNumber"));
					this.HeaderText.Text = "Account " + FCConvert.ToString(rsCL.Get_Fields("AccountNumber"));
					// this label will display the account information when the form is in Payment Mode
					//FC:FINAL:DDU:#1068 - remove labels
					//lblPaymentInfo.Text = "Account: " + lblAccount.Text + "     Name: " + lblOwnersName.Text + "       Location: " + lblLocation.Text;
					lblPaymentInfo.Text = lblOwnersName.Text + " " + lblLocation.Text;
				}
				rsMainInfo.Reset();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling UT Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: Grid As object	OnWrite(VSFlex7LCtl.VSFlexGrid)
		private void ShowRateInfo_168(int cRow, int cCol, clsDRWrapper rsCL, FCGrid Grid, bool boolWater)
		{
			ShowRateInfo(ref cRow, ref cCol, ref rsCL, ref Grid, ref boolWater);
		}

		private void ShowRateInfo(ref int cRow, ref int cCol, ref clsDRWrapper rsCL, ref FCGrid Grid, ref bool boolWater)
		{
			int lngErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will show a box with the rate information in it
				// as soon as the box loses focus, I will make it disappear
				clsDRWrapper rsRI = new clsDRWrapper();
				clsDRWrapper rsLR = new clsDRWrapper();
				int intYear;
				int i;
				int RK;
				int LRN;
				DateTime dtInterestPDThroughDate = DateTime.FromOADate(0);
				double dblTotalAbate;
				int intType = 0;
				string strWS = "";
				double dblPrin = 0;
				double dblTax = 0;
				double dblCost = 0;
				double dblPaid = 0;
				double dblInt = 0;
				double dblAbate = 0;
				clsDRWrapper rsInt = new clsDRWrapper();
				// MAL@20080506
				if (boolWater)
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				lngErrCode = 1;
				RK = rsCL.Get_Fields_Int32("BillingRateKey");
				lngErrCode = 2;
				LRN = rsCL.Get_Fields(strWS + "LienRecordNumber");
				lngErrCode = 3;
				if (boolWater)
				{
					intType = 0;
				}
				else
				{
					intType = 1;
				}
				if (LRN != 0)
				{
					rsLR.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(LRN), modExtraModules.strUTDatabase);
					lngErrCode = 4;
					RK = FCConvert.ToInt32(rsLR.Get_Fields_Int32("RateKey"));
				}
				lngErrCode = 5;
				rsRI.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(RK), modExtraModules.strUTDatabase);
				// format grid
				this.vsRateInfo.Cols = 1;
				this.vsRateInfo.Cols = 5;
				this.vsRateInfo.Rows = 20;
				this.vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				fraRateInfo.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				intYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Grid.TextMatrix(cRow, cCol))));
				this.vsRateInfo.ColWidth(0, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.25));
				this.vsRateInfo.ColWidth(1, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.19));
				this.vsRateInfo.ColWidth(2, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.04));
				this.vsRateInfo.ColWidth(3, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.23));
				this.vsRateInfo.ColWidth(4, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.29));
				this.vsRateInfo.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
				this.vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
				// .Select 18, 0, 18, 1
				// .CellBorder 0, 0, 0, 0, 0, 0, 0
				// .Select 15, 0, 15, 1
				// .CellBorder 0, 0, 2, 0, 0, 0, 1
				this.vsRateInfo.Select(0, 0);
				this.vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
				// show the first set of information (rate information and period info)--------------
				this.vsRateInfo.TextMatrix(0, 0, "BillKey");
				this.vsRateInfo.TextMatrix(1, 0, "RateKey");
				this.vsRateInfo.TextMatrix(2, 0, "Rate Type");
				if (LRN == 0)
				{
					this.vsRateInfo.TextMatrix(3, 0, "");
				}
				else
				{
					this.vsRateInfo.TextMatrix(3, 0, "Lien Record Number");
				}
				this.vsRateInfo.TextMatrix(4, 0, "Billing Date");
				this.vsRateInfo.TextMatrix(5, 0, "Creation Date");
				this.vsRateInfo.TextMatrix(6, 0, "Period Start");
				this.vsRateInfo.TextMatrix(7, 0, "Period End");
				this.vsRateInfo.TextMatrix(8, 0, "Interest Date");
				this.vsRateInfo.TextMatrix(9, 0, "Interest Rate");
				this.vsRateInfo.TextMatrix(10, 0, "");
				this.vsRateInfo.TextMatrix(11, 0, "Principal");
				this.vsRateInfo.TextMatrix(12, 0, "Tax");
				this.vsRateInfo.TextMatrix(13, 0, "Interest");
				this.vsRateInfo.TextMatrix(14, 0, "Cost");
				if (RK != 0)
				{
					if (FCConvert.ToString(rsRI.Get_Fields_String("RateType")) != "L")
					{
						this.vsRateInfo.TextMatrix(15, 0, "Abatement");
					}
					else
					{
						this.vsRateInfo.TextMatrix(15, 0, "Maturity Fee");
					}
				}
				else
				{
					this.vsRateInfo.TextMatrix(15, 0, "Abatement");
				}
				this.vsRateInfo.TextMatrix(16, 0, "Paid");
				this.vsRateInfo.TextMatrix(17, 0, "");
				this.vsRateInfo.TextMatrix(18, 0, "Total Due");
				this.vsRateInfo.TextMatrix(19, 0, "Per Diem");
				if (rsRI.BeginningOfFile() != true && rsRI.EndOfFile() != true)
				{
					lngErrCode = 6;
					// TODO: Field [Bill] not found!! (maybe it is an alias?)
					this.vsRateInfo.TextMatrix(0, 1, rsCL.Get_Fields("Bill"));
					lngErrCode = 7;
					this.vsRateInfo.TextMatrix(1, 1, FCConvert.ToString(rsRI.Get_Fields_Int32("ID")));
					lngErrCode = 8;
					if (FCConvert.ToString(rsRI.Get_Fields_String("RateType")) == "L")
					{
						this.vsRateInfo.TextMatrix(2, 1, "Lien");
					}
					else
					{
						this.vsRateInfo.TextMatrix(2, 1, "Regular");
					}
					lngErrCode = 9;
					if (LRN == 0)
					{
						this.vsRateInfo.TextMatrix(3, 1, "");
					}
					else
					{
						this.vsRateInfo.TextMatrix(3, 1, FCConvert.ToString(LRN));
					}
					lngErrCode = 10;
					this.vsRateInfo.TextMatrix(4, 1, Strings.Format(rsRI.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy"));
					lngErrCode = 11;
					this.vsRateInfo.TextMatrix(5, 1, Strings.Format(rsRI.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy"));
					lngErrCode = 12;
					this.vsRateInfo.TextMatrix(6, 1, Strings.Format(rsRI.Get_Fields_DateTime("Start"), "MM/dd/yyyy"));
					lngErrCode = 13;
					// TODO: Check the table for the column [End] and replace with corresponding Get_Field method
					this.vsRateInfo.TextMatrix(7, 1, Strings.Format(rsRI.Get_Fields("End"), "MM/dd/yyyy"));
					lngErrCode = 14;
					this.vsRateInfo.TextMatrix(8, 1, Strings.Format(rsRI.Get_Fields_DateTime("IntStart"), "MM/dd/yyyy"));
					lngErrCode = 15;
					this.vsRateInfo.TextMatrix(9, 1, Strings.Format(rsRI.Get_Fields(strWS + "IntRate") * 100, "#,##0.00") + "%");
					lngErrCode = 16;
					this.vsRateInfo.TextMatrix(10, 1, "");
					lngErrCode = 17;
					this.vsRateInfo.TextMatrix(11, 1, "");
					lngErrCode = 18;
					this.vsRateInfo.TextMatrix(12, 1, "");
					lngErrCode = 19;
					this.vsRateInfo.TextMatrix(13, 1, "");
					lngErrCode = 20;
					this.vsRateInfo.TextMatrix(14, 1, "");
					lngErrCode = 21;
					if (LRN == 0)
					{
						// this is not a lien
						lngErrCode = 22;
						dblPrin = FCConvert.ToDouble(rsCL.Get_Fields(strWS + "PrinOwed") - modUTFunctions.Statics.UTAbatePaymentsArray[rsCL.Get_Fields_Int32("BillNumber"), intType].Per1);
						this.vsRateInfo.TextMatrix(11, 1, Strings.Format(dblPrin, "#,##0.00"));
						dblTax = FCConvert.ToDouble(rsCL.Get_Fields(strWS + "TaxOwed"));
						this.vsRateInfo.TextMatrix(12, 1, Strings.Format(dblTax, "#,##0.00"));
						dblInt = FCConvert.ToDouble(rsCL.Get_Fields(strWS + "IntOwed") + (-1 * modUTFunctions.Statics.dblUTCurrentInt[rsCL.Get_Fields_Int32("BillNumber"), intType]) - rsCL.Get_Fields(strWS + "IntAdded"));
						this.vsRateInfo.TextMatrix(13, 1, Strings.Format(dblInt, "#,##0.00"));
						dblCost = FCConvert.ToDouble(rsCL.Get_Fields(strWS + "CostOwed") - rsCL.Get_Fields(strWS + "CostAdded"));
						this.vsRateInfo.TextMatrix(14, 1, Strings.Format(dblCost, "#,##0.00"));
						dblAbate = modUTFunctions.Statics.UTAbatePaymentsArray[rsCL.Get_Fields_Int32("BillNumber"), intType].Per1;
						this.vsRateInfo.TextMatrix(15, 1, Strings.Format(dblAbate, "#,##0.00"));
						if (boolWater)
						{
							if (rsCL.Get_Fields_DateTime("WIntPaidDate").ToOADate() != 0 && !rsCL.IsFieldNull("WIntPaidDate"))
							{
								dtInterestPDThroughDate = rsCL.Get_Fields_DateTime("WIntPaidDate");
							}
							else
							{
								dtInterestPDThroughDate = (DateTime)rsRI.Get_Fields_DateTime("IntStart");
							}
						}
						else
						{
							//if (rsCL.Get_Fields("SIntPaidDate") != 0 && rsCL.Get_Fields("SIntPaidDate") != "")
							// FC:FINAL:VGE - #i895 COmparing datetime to Int as AODate.
							if (rsCL.Get_Fields_DateTime("SIntPaidDate").ToOADate() != 0)
							{
								dtInterestPDThroughDate = rsCL.Get_Fields_DateTime("SIntPaidDate");
							}
							else
							{
								dtInterestPDThroughDate = (DateTime)rsRI.Get_Fields_DateTime("IntStart");
							}
						}
						// this will find the total abatement amount to take out of the payment totals
						dblTotalAbate = modUTFunctions.Statics.UTAbatePaymentsArray[rsCL.Get_Fields_Int32("BillNumber"), intType].Per1;
						// need to use the lien costs paid even though it is not a lien because of demand fees
						dblPaid = rsCL.Get_Fields(strWS + "PrinPaid") + rsCL.Get_Fields(strWS + "TaxPaid") + rsCL.Get_Fields(strWS + "IntPaid") + rsCL.Get_Fields(strWS + "CostPaid");
						// - dblTotalAbate
						this.vsRateInfo.TextMatrix(16, 1, Strings.Format(dblPaid, "#,##0.00"));
						this.vsRateInfo.TextMatrix(18, 1, Strings.Format(dblPrin + dblTax + dblCost - dblPaid + dblAbate + dblInt, "#,##0.00"));
					}
					else
					{
						// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
						dblPrin = rsLR.Get_Fields_Double("Principal");
						this.vsRateInfo.TextMatrix(11, 1, Strings.Format(dblPrin, "#,##0.00"));
						// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
						dblTax = rsLR.Get_Fields_Double("Tax");
						this.vsRateInfo.TextMatrix(12, 1, Strings.Format(dblTax, "#,##0.00"));
						// MAL@20080506: Add check for primary bill number to get correct interest amount
						// Tracker Reference: 13300
						// If rsCL.Fields("Bill") = rsCL.Fields(strWS & "CombinationLienKey") Then
						// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
						dblInt = rsLR.Get_Fields("Interest") + (-1 * modUTFunctions.Statics.dblUTCurrentInt[rsCL.Get_Fields_Int32("BillNumber"), intType]) - rsLR.Get_Fields("IntAdded");
						// Else
						// Use primary ID for interest calc
						// rsInt.OpenRecordset "SELECT * FROM Bill WHERE Bill = " & rsCL.Fields("SCombinationLienKey"), strUTDatabase
						// If rsInt.RecordCount > 0 Then
						// dblInt = rsLR.Fields("Interest") + (-1 * dblUTCurrentInt(rsInt.Fields("BillNumber"), intType)) - rsLR.Fields("IntAdded")
						// Else
						// dblInt = 0
						// End If
						// End If
						this.vsRateInfo.TextMatrix(13, 1, Strings.Format(dblInt, "#,##0.00"));
						// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
						dblCost = rsLR.Get_Fields_Double("Costs");
						this.vsRateInfo.TextMatrix(14, 1, Strings.Format(dblCost, "#,##0.00"));
						// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
						dblAbate = FCConvert.ToDouble(rsLR.Get_Fields("MaturityFee")) * -1;
						this.vsRateInfo.TextMatrix(15, 1, Strings.Format(dblAbate, "#,##0.00"));
						// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
						dblPaid = FCConvert.ToDouble(rsLR.Get_Fields("PrinPaid") + rsLR.Get_Fields("TaxPaid") + rsLR.Get_Fields("IntPaid") + rsLR.Get_Fields("PLIPaid") + rsLR.Get_Fields_Double("CostPaid"));
						this.vsRateInfo.TextMatrix(16, 1, Strings.Format(dblPaid, "#,##0.00"));
						this.vsRateInfo.TextMatrix(18, 1, Strings.Format(dblPrin + dblTax + dblCost + dblInt + dblAbate - dblPaid, "#,##0.00"));
						// (rsLR.Fields("Principal") - rsLR.Fields("PrincipalPaid")) + (rsLR.Fields("Interest") - rsLR.Fields("InterestCharged") - rsLR.Fields("InterestPaid")) + (rsLR.Fields("Costs") - rsLR.Fields("MaturityFee") - rsLR.Fields("CostsPaid")) + dblUTCurrentInt(intYear - UTDEFAULTSUBTRACTIONVALUE, intType)
						if (!rsLR.IsFieldNull("IntPaidDate"))
						{
							dtInterestPDThroughDate = (DateTime)rsLR.Get_Fields_DateTime("IntPaidDate");
						}
					}
					if (Grid.IsCollapsed(cRow) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						int l = 0;
						l = Grid.FindRow("=", cRow, lngGRIDColLineCode) + 1;
						if (l != -1)
						{
							this.vsRateInfo.TextMatrix(19, 1, Strings.Format(Grid.TextMatrix(l, lngGRIDColPerDiem), "#,##0.0000"));
						}
					}
					else
					{
						this.vsRateInfo.TextMatrix(19, 1, Strings.Format(Grid.TextMatrix(cRow, lngGRIDColPerDiem), "#,##0.0000"));
					}
					// this checks to see if the dates are 0 and replaces them with 0
					for (i = 4; i <= 10; i++)
					{
						//FC:FINAL:DSE Use correct date string for empty date
						//if (this.vsRateInfo.TextMatrix(i, lngGRIDColDate) == "12:00:00 AM") this.vsRateInfo.TextMatrix(i, lngGRIDColDate, "");
						if (this.vsRateInfo.TextMatrix(i, lngGRIDColDate) == Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
							this.vsRateInfo.TextMatrix(i, lngGRIDColDate, "");
						if (Strings.Right(this.vsRateInfo.TextMatrix(i, 1), 1) == "%")
						{
							if (Conversion.Val(Strings.Left(this.vsRateInfo.TextMatrix(i, 1), this.vsRateInfo.TextMatrix(i, 1).Length - 1)) == 0)
							{
								this.vsRateInfo.TextMatrix(i, 1, "");
							}
						}
						else if (Conversion.Val(this.vsRateInfo.TextMatrix(i, 1)) == 0)
						{
							this.vsRateInfo.TextMatrix(i, 1, "");
						}
					}
				}
				else
				{
					this.vsRateInfo.TextMatrix(0, 1, "");
					this.vsRateInfo.TextMatrix(1, 1, "");
					this.vsRateInfo.TextMatrix(2, 1, "");
					this.vsRateInfo.TextMatrix(3, 1, "");
					this.vsRateInfo.TextMatrix(4, 1, "");
					this.vsRateInfo.TextMatrix(5, 1, "");
					this.vsRateInfo.TextMatrix(6, 1, "");
					this.vsRateInfo.TextMatrix(7, 1, "");
					this.vsRateInfo.TextMatrix(8, 1, "");
					this.vsRateInfo.TextMatrix(9, 1, "");
					this.vsRateInfo.TextMatrix(10, 1, "");
					this.vsRateInfo.TextMatrix(11, 1, "");
					this.vsRateInfo.TextMatrix(12, 1, "");
					this.vsRateInfo.TextMatrix(13, 1, "");
					this.vsRateInfo.TextMatrix(14, 1, "");
					this.vsRateInfo.TextMatrix(15, 0, "");
					this.vsRateInfo.TextMatrix(16, 0, "");
					this.vsRateInfo.TextMatrix(17, 0, "");
					this.vsRateInfo.TextMatrix(18, 0, "");
					this.vsRateInfo.TextMatrix(19, 0, "");
				}
				// show the second set of information (valuations)--------------------
				this.vsRateInfo.TextMatrix(0, 3, "Tenant");
				this.vsRateInfo.TextMatrix(1, 3, "Address");
				this.vsRateInfo.TextMatrix(2, 3, "");
				this.vsRateInfo.TextMatrix(3, 3, "");
				this.vsRateInfo.TextMatrix(4, 3, "");
				this.vsRateInfo.TextMatrix(5, 3, "");
				this.vsRateInfo.TextMatrix(6, 3, "Owner");
				this.vsRateInfo.TextMatrix(7, 3, "Address");
				this.vsRateInfo.TextMatrix(8, 3, "");
				this.vsRateInfo.TextMatrix(9, 3, "");
				this.vsRateInfo.TextMatrix(10, 3, "");
				this.vsRateInfo.TextMatrix(11, 3, "Readings");
				this.vsRateInfo.TextMatrix(12, 3, "Read Dates");
				// kk 11262013 trouts-59  Shift rows 12 & 13 down one row and add read dates
				this.vsRateInfo.TextMatrix(13, 3, "Interest Paid Date");
				// 12
				this.vsRateInfo.TextMatrix(14, 3, "Map Lot");
				// 13
				// .TextMatrix(14, 3) = ""                     ' 14 was unused
				this.vsRateInfo.TextMatrix(15, 3, "Total Per Diem");
				if (rsCL.Get_Fields_Boolean("Final"))
				{
					this.vsRateInfo.TextMatrix(16, 3, "Final Bill Date");
					this.vsRateInfo.TextMatrix(17, 3, "Final Bill Start");
					this.vsRateInfo.TextMatrix(18, 3, "Final Bill End");
				}
				else
				{
					this.vsRateInfo.TextMatrix(16, 3, "");
					this.vsRateInfo.TextMatrix(17, 3, "");
					this.vsRateInfo.TextMatrix(18, 3, "");
				}
				if (LRN != 0)
				{
					this.vsRateInfo.TextMatrix(19, 3, "Book / Page");
				}
				if (Strings.Trim(rsCL.Get_Fields_String("BName2")) != "")
				{
					this.vsRateInfo.TextMatrix(0, 4, rsCL.Get_Fields_String("BName") + " and " + rsCL.Get_Fields_String("BName2"));
				}
				else
				{
					this.vsRateInfo.TextMatrix(0, 4, rsCL.Get_Fields_String("BName"));
				}
				this.vsRateInfo.TextMatrix(1, 4, rsCL.Get_Fields_String("BAddress1"));
				this.vsRateInfo.TextMatrix(2, 4, rsCL.Get_Fields_String("BAddress2"));
				this.vsRateInfo.TextMatrix(3, 4, rsCL.Get_Fields_String("BAddress3"));
				if (Strings.Trim(rsCL.Get_Fields_String("BZip4")) != "")
				{
					this.vsRateInfo.TextMatrix(4, 4, rsCL.Get_Fields_String("BCity") + ", " + rsCL.Get_Fields_String("BState") + " " + rsCL.Get_Fields_String("BZip") + "-" + rsCL.Get_Fields_String("BZip4"));
				}
				else
				{
					this.vsRateInfo.TextMatrix(4, 4, rsCL.Get_Fields_String("BCity") + ", " + rsCL.Get_Fields_String("BState") + " " + rsCL.Get_Fields_String("BZip"));
				}
				this.vsRateInfo.TextMatrix(5, 4, "");
				if (Strings.Trim(rsCL.Get_Fields_String("OName2")) != "")
				{
					this.vsRateInfo.TextMatrix(6, 4, rsCL.Get_Fields_String("OName") + " and " + rsCL.Get_Fields_String("OName2"));
				}
				else
				{
					this.vsRateInfo.TextMatrix(6, 4, rsCL.Get_Fields_String("OName"));
				}
				this.vsRateInfo.TextMatrix(7, 4, rsCL.Get_Fields_String("OAddress1"));
				this.vsRateInfo.TextMatrix(8, 4, rsCL.Get_Fields_String("OAddress2"));
				this.vsRateInfo.TextMatrix(9, 4, rsCL.Get_Fields_String("OAddress3"));
				if (Strings.Trim(rsCL.Get_Fields_String("OZip4")) != "")
				{
					this.vsRateInfo.TextMatrix(10, 4, rsCL.Get_Fields_String("OCity") + ", " + rsCL.Get_Fields_String("OState") + " " + rsCL.Get_Fields_String("OZip") + "-" + rsCL.Get_Fields_String("OZip4"));
				}
				else
				{
					this.vsRateInfo.TextMatrix(10, 4, rsCL.Get_Fields_String("OCity") + ", " + rsCL.Get_Fields_String("OState") + " " + rsCL.Get_Fields_String("OZip"));
				}
				// kk03302015 trouts-131  Don't show -1 for no read
				// .TextMatrix(11, 4) = "Cur:" & rsCL.Fields("CurReading") & " Prev:" & rsCL.Fields("PrevReading")
				if (rsCL.Get_Fields_Int32("CurReading") >= 0)
				{
					this.vsRateInfo.TextMatrix(11, 4, "Cur:" + rsCL.Get_Fields_Int32("CurReading"));
				}
				else
				{
					this.vsRateInfo.TextMatrix(11, 4, "Cur:");
				}
				if (rsCL.Get_Fields_Int32("PrevReading") >= 0)
				{
					this.vsRateInfo.TextMatrix(11, 4, this.vsRateInfo.TextMatrix(11, 4) + " Prev:" + rsCL.Get_Fields_Int32("PrevReading"));
				}
				else
				{
					this.vsRateInfo.TextMatrix(11, 4, this.vsRateInfo.TextMatrix(11, 4) + " Prev:");
				}
				// kk03302015 trouts-131  Don't show 12/31/1899 for no read
				// kk 11262013 trouts-59  Add the read dates here and shift rows 12 & 13 down to 13 & 14
				// .TextMatrix(12, 4) = "Cur:" & Format(rsCL.Fields("CurDate"), "MM/dd/yyyy") & " Prev:" & Format(rsCL.Fields("PrevDate"), "MM/dd/yyyy")     'kk 11222013 trouts-59  Add Reading Date to rate info
				//if (rsCL.Get_Fields("CurDate") != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
				// FC:FINAL:VGE - #i895 Changing to comparison between 2 AODates
				if (FCUtils.IsValidDateTime(rsCL.Get_Fields_DateTime("CurDate")))
				{
					this.vsRateInfo.TextMatrix(12, 4, "Cur:" + Strings.Format(rsCL.Get_Fields_DateTime("CurDate"), "MM/dd/yyyy"));
				}
				else
				{
					this.vsRateInfo.TextMatrix(12, 4, "Cur:");
				}
				//if (rsCL.Get_Fields("PrevDate") != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
				// FC:FINAL:VGE - #i895 Changing to comparison between 2 AODates
				if (FCUtils.IsValidDateTime(rsCL.Get_Fields_DateTime("PrevDate")))
				{
					this.vsRateInfo.TextMatrix(12, 4, this.vsRateInfo.TextMatrix(12, 4) + " Prev:" + Strings.Format(rsCL.Get_Fields_DateTime("PrevDate"), "MM/dd/yyyy"));
				}
				else
				{
					this.vsRateInfo.TextMatrix(12, 4, this.vsRateInfo.TextMatrix(12, 4) + " Prev:");
				}
				if (dtInterestPDThroughDate.ToOADate() != 0)
				{
					this.vsRateInfo.TextMatrix(13, 4, Strings.Format(dtInterestPDThroughDate, "MM/dd/yyyy"));
					// 12
				}
				else
				{
					this.vsRateInfo.TextMatrix(13, 4, "");
				}
				this.vsRateInfo.TextMatrix(14, 4, this.lblMapLot.Text);
				// 13
				// .TextMatrix(14, 4) = frmUTCLStatus.lblReference1.Caption                ' 14 was unused
				this.vsRateInfo.TextMatrix(15, 4, Strings.Format(dblTotalPerDiem, "#,##0.0000"));
				// .TextMatrix(16, 4) = frmUTCLStatus.lblAcreage.Caption
				if (rsCL.Get_Fields_Boolean("Final"))
				{
					this.vsRateInfo.TextMatrix(16, 4, rsCL.Get_Fields_DateTime("FinalBillDate"));
					this.vsRateInfo.TextMatrix(17, 4, rsCL.Get_Fields_DateTime("FinalStartDate"));
					this.vsRateInfo.TextMatrix(18, 4, rsCL.Get_Fields_DateTime("FinalEndDate"));
				}
				else
				{
					this.vsRateInfo.TextMatrix(16, 4, "");
					this.vsRateInfo.TextMatrix(17, 4, "");
					this.vsRateInfo.TextMatrix(18, 4, "");
				}
				if (LRN != 0)
				{
					// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
					this.vsRateInfo.TextMatrix(19, 4, "B" + rsLR.Get_Fields("Book") + " P" + rsLR.Get_Fields("Page"));
				}
				// show the frame-----------------------------------------------------
				this.fraRateInfo.Text = "Rate Information";
				// - " & intYear
				this.fraRateInfo.Left = FCConvert.ToInt32((this.Width - this.fraRateInfo.Width) / 3.0);
				//this.fraRateInfo.Top = FCConvert.ToInt32((this.Height - this.fraRateInfo.Height) / 3.0);
				this.fraRateInfo.CenterToContainer(this.ClientArea);
				this.fraRateInfo.Visible = true;
				this.vsRateInfo.SendToBack();
				mnuFilePrintRateInfo.Visible = true;
				this.cmdRIClose.Focus();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Rate Information Error" + " - " + FCConvert.ToString(lngErrCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: Grid As object	OnWrite(VSFlex7LCtl.VSFlexGrid)
		private void ShowPaymentInfo(int cRow, int cCol, FCGrid Grid)
		{
			int lngErrCode = 0;
			clsDRWrapper rsRI = new clsDRWrapper();
			clsDRWrapper rsLR = new clsDRWrapper();
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will show a box with the rate information in it
				// as soon as the box loses focus, I will make it disappear
				int intYear;
				int i;
				int RK;
				int LRN;
				lngErrCode = 1;
				if (Strings.Trim(Grid.TextMatrix(cRow, lngGRIDColLineCode)) != "")
				{
					if (Conversion.Val(Grid.TextMatrix(cRow, lngGRIDColPaymentKey)) > 0 && Conversion.Val(Grid.TextMatrix(cRow, lngGRIDColCHGINTNumber)) >= 0)
					{
						rsRI.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE ID = " + Grid.TextMatrix(cRow, lngGRIDColPaymentKey), modExtraModules.strUTDatabase);
						// format grid
						this.vsRateInfo.Cols = 1;
						this.vsRateInfo.Cols = 5;
						this.vsRateInfo.Rows = 20;
						this.vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
						this.vsRateInfo.ColWidth(0, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.3));
						this.vsRateInfo.ColWidth(1, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.19));
						this.vsRateInfo.ColWidth(2, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.01));
						this.vsRateInfo.ColWidth(3, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.3));
						this.vsRateInfo.ColWidth(4, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.19));
						this.vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
						// this will hide the border of the other information on the shared grid
						this.vsRateInfo.Select(19, 0, 19, 1);
						this.vsRateInfo.CellBorder(Color.Black, 0, 0, 0, 0, 0, 0);
						this.vsRateInfo.Select(15, 0, 15, 1);
						this.vsRateInfo.CellBorder(Color.Black, 0, 0, 0, 0, 0, 0);
						this.vsRateInfo.Select(0, 0);
						this.vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 19, 1, 19, 4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						this.vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
						// show the first set of information (rate information and period info)--------------
						this.vsRateInfo.TextMatrix(0, 0, "Account");
						this.vsRateInfo.TextMatrix(1, 0, "");
						this.vsRateInfo.TextMatrix(2, 0, "");
						this.vsRateInfo.TextMatrix(3, 0, "Bill ID");
						this.vsRateInfo.TextMatrix(4, 0, "Receipt Number");
						this.vsRateInfo.TextMatrix(5, 0, "");
						this.vsRateInfo.TextMatrix(6, 0, "Recorded Transaction");
						this.vsRateInfo.TextMatrix(7, 0, "Effective Interest Date");
						this.vsRateInfo.TextMatrix(8, 0, "Actual Transaction Date");
						this.vsRateInfo.TextMatrix(9, 0, "");
						this.vsRateInfo.TextMatrix(10, 0, "Teller ID");
						this.vsRateInfo.TextMatrix(11, 0, "");
						this.vsRateInfo.TextMatrix(12, 0, "Cash Drawer");
						this.vsRateInfo.TextMatrix(13, 0, "Affect Cash");
						this.vsRateInfo.TextMatrix(14, 0, "BD Account");
						this.vsRateInfo.TextMatrix(0, 3, "ID");
						this.vsRateInfo.TextMatrix(1, 3, "Reference");
						this.vsRateInfo.TextMatrix(2, 3, "Paid By");
						this.vsRateInfo.TextMatrix(3, 3, "Daily Close Out");
						this.vsRateInfo.TextMatrix(4, 3, "");
						this.vsRateInfo.TextMatrix(5, 3, "Principal");
						this.vsRateInfo.TextMatrix(6, 3, "Tax");
						this.vsRateInfo.TextMatrix(7, 3, "Interest");
						this.vsRateInfo.TextMatrix(8, 3, "Pre Lien Interest");
						this.vsRateInfo.TextMatrix(9, 3, "Lien Costs");
						this.vsRateInfo.TextMatrix(10, 3, "");
						this.vsRateInfo.TextMatrix(11, 3, "Total");
						this.vsRateInfo.TextMatrix(19, 0, "Comments");
						lngErrCode = 1;
						if (rsRI.BeginningOfFile() != true && rsRI.EndOfFile() != true)
						{
							lngErrCode = 2;
							this.vsRateInfo.TextMatrix(0, 1, FCConvert.ToString(modUTFunctions.GetUTAccountNumber_2(FCConvert.ToInt32(rsRI.Get_Fields_Int32("AccountKey")))));
							lngErrCode = 3;
							this.vsRateInfo.TextMatrix(2, 1, "");
							// rsRI.Fields("Year")
							lngErrCode = 4;
							this.vsRateInfo.TextMatrix(3, 1, FCConvert.ToString(rsRI.Get_Fields_Int32("BillKey")));
							lngErrCode = 5;
							// check to see if this is a payment from the DOS system
							lngErrCode = 6;
							if (FCConvert.ToBoolean(rsRI.Get_Fields_Boolean("DOSReceiptNumber")))
							{
								this.vsRateInfo.TextMatrix(4, 1, FCConvert.ToString(rsRI.Get_Fields_Int32("ReceiptNumber")));
							}
							else
							{
								this.vsRateInfo.TextMatrix(4, 1, GetActualReceiptNumber_8(FCConvert.ToInt32(rsRI.Get_Fields_Int32("ReceiptNumber")), rsRI.Get_Fields_DateTime("ActualSystemDate")));
							}
							lngErrCode = 7;
							this.vsRateInfo.TextMatrix(5, 1, "");
							lngErrCode = 8;
							this.vsRateInfo.TextMatrix(6, 1, Strings.Format(rsRI.Get_Fields_DateTime("RecordedTransactionDate"), "MM/dd/yyyy"));
							lngErrCode = 9;
							this.vsRateInfo.TextMatrix(7, 1, Strings.Format(rsRI.Get_Fields_DateTime("EffectiveInterestDate"), "MM/dd/yyyy"));
							lngErrCode = 10;
							this.vsRateInfo.TextMatrix(8, 1, Strings.Format(rsRI.Get_Fields_DateTime("ActualSystemDate"), "MM/dd/yyyy"));
							lngErrCode = 11;
							this.vsRateInfo.TextMatrix(9, 1, "");
							this.vsRateInfo.TextMatrix(10, 1, FCConvert.ToString(rsRI.Get_Fields_String("Teller")));
							lngErrCode = 12;
							if (FCConvert.ToString(rsRI.Get_Fields_String("CashDrawer")) == "Y")
							{
								this.vsRateInfo.TextMatrix(12, 1, "YES");
								this.vsRateInfo.TextMatrix(13, 1, "YES");
							}
							else
							{
								this.vsRateInfo.TextMatrix(12, 1, "NO");
								if (FCConvert.ToString(rsRI.Get_Fields_String("GeneralLedger")) == "Y")
								{
									this.vsRateInfo.TextMatrix(13, 1, "YES");
								}
								else
								{
									this.vsRateInfo.TextMatrix(13, 1, "NO");
									this.vsRateInfo.TextMatrix(14, 1, rsRI.Get_Fields_String("BudgetaryAccountNumber"));
								}
							}
							this.vsRateInfo.TextMatrix(0, 4, FCConvert.ToString(rsRI.Get_Fields_Int32("ID")));
							this.vsRateInfo.TextMatrix(1, 4, rsRI.Get_Fields_String("Reference"));
							lngErrCode = 13;
							this.vsRateInfo.TextMatrix(2, 4, rsRI.Get_Fields_String("PaidBy"));
							lngErrCode = 14;
							if (FCConvert.ToBoolean(rsRI.Get_Fields_Int32("DailyCloseOut")))
							{
								this.vsRateInfo.TextMatrix(3, 4, "True");
							}
							else
							{
								this.vsRateInfo.TextMatrix(3, 4, "False");
							}
							this.vsRateInfo.TextMatrix(4, 4, "");
							lngErrCode = 15;
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							this.vsRateInfo.TextMatrix(5, 4, Strings.Format(rsRI.Get_Fields("Principal"), "#,##0.00"));
							lngErrCode = 16;
							// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
							this.vsRateInfo.TextMatrix(6, 4, Strings.Format(rsRI.Get_Fields("Tax"), "#,##0.00"));
							lngErrCode = 161;
							this.vsRateInfo.TextMatrix(7, 4, Strings.Format(rsRI.Get_Fields_Decimal("CurrentInterest"), "#,##0.00"));
							lngErrCode = 17;
							this.vsRateInfo.TextMatrix(8, 4, Strings.Format(rsRI.Get_Fields_Decimal("PreLienInterest"), "#,##0.00"));
							lngErrCode = 18;
							this.vsRateInfo.TextMatrix(9, 4, Strings.Format(rsRI.Get_Fields_Decimal("LienCost"), "#,##0.00"));
							this.vsRateInfo.TextMatrix(10, 4, "");
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							this.vsRateInfo.TextMatrix(11, 4, Strings.Format(rsRI.Get_Fields("Principal") + rsRI.Get_Fields_Decimal("CurrentInterest") + rsRI.Get_Fields_Decimal("PreLienInterest") + rsRI.Get_Fields_Decimal("LienCost"), "#,##0.00"));
							lngErrCode = 19;
							this.vsRateInfo.TextMatrix(19, 1, FCConvert.ToString(rsRI.Get_Fields_String("Comments")));
                            //FC:FINAL:AM:#3781 - set the comment only in one column
                            //this.vsRateInfo.TextMatrix(19, 2, this.vsRateInfo.TextMatrix(19, 1));
							//this.vsRateInfo.TextMatrix(19, 3, this.vsRateInfo.TextMatrix(19, 1));
							//this.vsRateInfo.TextMatrix(19, 4, this.vsRateInfo.TextMatrix(19, 1));
							//this.vsRateInfo.MergeRow(19, true);
                            this.vsRateInfo.MergeRow(19, true, 1, 4);
                        }
						else
						{
							this.vsRateInfo.TextMatrix(0, 1, "");
							this.vsRateInfo.TextMatrix(1, 1, "");
							this.vsRateInfo.TextMatrix(2, 1, "");
							this.vsRateInfo.TextMatrix(3, 1, "");
							this.vsRateInfo.TextMatrix(4, 1, "");
							this.vsRateInfo.TextMatrix(5, 1, "");
							this.vsRateInfo.TextMatrix(6, 1, "");
							this.vsRateInfo.TextMatrix(7, 1, "");
							this.vsRateInfo.TextMatrix(8, 1, "");
							this.vsRateInfo.TextMatrix(9, 1, "");
							this.vsRateInfo.TextMatrix(0, 4, "");
							this.vsRateInfo.TextMatrix(1, 4, "");
							this.vsRateInfo.TextMatrix(2, 4, "");
							this.vsRateInfo.TextMatrix(3, 4, "");
							this.vsRateInfo.TextMatrix(4, 4, "");
							this.vsRateInfo.TextMatrix(5, 0, "");
							this.vsRateInfo.TextMatrix(6, 0, "");
							this.vsRateInfo.TextMatrix(7, 0, "");
							this.vsRateInfo.TextMatrix(8, 0, "");
							this.vsRateInfo.TextMatrix(19, 0, "");
						}
						// show the second set of information (valuations)--------------------
						if (modStatusPayments.Statics.boolRE)
						{
							// RE
							this.vsRateInfo.TextMatrix(15, 0, "");
							this.vsRateInfo.TextMatrix(16, 0, "");
							this.vsRateInfo.TextMatrix(17, 0, "");
							this.vsRateInfo.TextMatrix(18, 0, "");
							this.vsRateInfo.TextMatrix(15, 1, "");
							this.vsRateInfo.TextMatrix(16, 1, "");
							this.vsRateInfo.TextMatrix(17, 1, "");
							this.vsRateInfo.TextMatrix(18, 1, "");
							this.vsRateInfo.TextMatrix(12, 3, "");
							this.vsRateInfo.TextMatrix(13, 3, "");
							this.vsRateInfo.TextMatrix(14, 3, "");
							this.vsRateInfo.TextMatrix(15, 3, "");
							this.vsRateInfo.TextMatrix(16, 3, "");
							this.vsRateInfo.TextMatrix(17, 3, "");
							this.vsRateInfo.TextMatrix(18, 3, "");
							this.vsRateInfo.TextMatrix(12, 4, "");
							this.vsRateInfo.TextMatrix(13, 4, "");
							this.vsRateInfo.TextMatrix(14, 4, "");
							this.vsRateInfo.TextMatrix(15, 4, "");
							this.vsRateInfo.TextMatrix(16, 4, "");
							this.vsRateInfo.TextMatrix(17, 4, "");
							this.vsRateInfo.TextMatrix(18, 4, "");
						}
						else
						{
							// PP
							this.vsRateInfo.TextMatrix(15, 0, "");
							this.vsRateInfo.TextMatrix(16, 0, "");
							this.vsRateInfo.TextMatrix(17, 0, "");
							this.vsRateInfo.TextMatrix(18, 0, "");
							this.vsRateInfo.TextMatrix(15, 1, "");
							this.vsRateInfo.TextMatrix(16, 1, "");
							this.vsRateInfo.TextMatrix(17, 1, "");
							this.vsRateInfo.TextMatrix(18, 1, "");
							this.vsRateInfo.TextMatrix(12, 3, "");
							this.vsRateInfo.TextMatrix(13, 3, "");
							this.vsRateInfo.TextMatrix(14, 3, "");
							this.vsRateInfo.TextMatrix(15, 3, "");
							this.vsRateInfo.TextMatrix(16, 3, "");
							this.vsRateInfo.TextMatrix(17, 3, "");
							this.vsRateInfo.TextMatrix(18, 3, "");
							this.vsRateInfo.TextMatrix(12, 4, "");
							this.vsRateInfo.TextMatrix(13, 4, "");
							this.vsRateInfo.TextMatrix(14, 4, "");
							this.vsRateInfo.TextMatrix(15, 4, "");
							this.vsRateInfo.TextMatrix(16, 4, "");
							this.vsRateInfo.TextMatrix(17, 4, "");
							this.vsRateInfo.TextMatrix(18, 4, "");
						}
						// show the frame-----------------------------------------------------
						this.fraRateInfo.Text = "Payment Information";
						this.fraRateInfo.Left = FCConvert.ToInt32((this.Width - this.fraRateInfo.Width) / 3.0);
						//this.fraRateInfo.Top = FCConvert.ToInt32((this.Height - this.fraRateInfo.Height) / 3.0);
						this.fraRateInfo.CenterToContainer(this.ClientArea);
						this.fraRateInfo.Visible = true;
						this.vsRateInfo.SendToBack();
						this.cmdRIClose.Focus();
					}
					else
					{
						// check to see if this is a lien record
						if (FCConvert.ToBoolean(Grid.TextMatrix(cRow, lngGRIDColPaymentKey)) == true && Conversion.Val(Grid.TextMatrix(cRow, lngGRIDColCHGINTNumber)) > 0)
						{
							// show the liened record info
							rsLR.OpenRecordset("SELECT * FROM Lien WHERE ID = " + Grid.TextMatrix(cRow, lngGRIDColPaymentKey), modExtraModules.strUTDatabase);
							if (rsLR.EndOfFile() != true && rsLR.BeginningOfFile() != true)
							{
								// format grid
								this.vsRateInfo.Cols = 1;
								this.vsRateInfo.Cols = 5;
								this.vsRateInfo.Rows = 1;
								this.vsRateInfo.Rows = 21;
								this.vsRateInfo.ColWidth(0, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.4));
								this.vsRateInfo.ColWidth(1, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.585));
								this.vsRateInfo.ColWidth(2, this.vsRateInfo.WidthOriginal * 0);
								this.vsRateInfo.ColWidth(3, this.vsRateInfo.WidthOriginal * 0);
								this.vsRateInfo.ColWidth(4, this.vsRateInfo.WidthOriginal * 0);
								this.vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
								// .Select 10, 0, 10, 1
								// .CellBorder 0, 0, 0, 0, 0, 0, 0
								// .Select 15, 0, 15, 1
								// .CellBorder 0, 0, 0, 0, 0, 0, 0
								// .Select 19, 0, 19, 1
								this.vsRateInfo.CellBorder(Color.Black, 0, 1, 0, 0, 0, 1);
								this.vsRateInfo.Select(15, 0, 15, 1);
								this.vsRateInfo.CellBorder(Color.Black, 0, 0, 0, 0, 0, 0);
								this.vsRateInfo.Select(0, 0);
								this.vsRateInfo.ExtendLastCol = true;
								this.vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, this.vsRateInfo.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
								this.vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
								// show the first set of information (rate information and period info)--------------
								this.vsRateInfo.TextMatrix(0, 0, "Lien Record Number");
								this.vsRateInfo.TextMatrix(1, 0, "Rate ID");
								this.vsRateInfo.TextMatrix(2, 0, "Status");
								this.vsRateInfo.TextMatrix(3, 0, "Date Created");
								this.vsRateInfo.TextMatrix(4, 0, "Interest Applied Through");
								this.vsRateInfo.TextMatrix(5, 0, "Book / Page");
								this.vsRateInfo.TextMatrix(6, 0, "Principal");
								this.vsRateInfo.TextMatrix(7, 0, "Tax");
								this.vsRateInfo.TextMatrix(8, 0, "Interest");
								this.vsRateInfo.TextMatrix(9, 0, "Costs");
								this.vsRateInfo.TextMatrix(10, 0, "Interest Charged");
								this.vsRateInfo.TextMatrix(11, 0, "Maturity Fee");
								this.vsRateInfo.TextMatrix(12, 0, "Lien Total");
								this.vsRateInfo.TextMatrix(13, 0, "");
								this.vsRateInfo.TextMatrix(14, 0, "Principal Paid");
								this.vsRateInfo.TextMatrix(15, 0, "Tax Paid");
								this.vsRateInfo.TextMatrix(16, 0, "Interest Paid");
								this.vsRateInfo.TextMatrix(17, 0, "Costs Paid");
								this.vsRateInfo.TextMatrix(18, 0, "Total Paid");
								this.vsRateInfo.TextMatrix(19, 0, "");
								this.vsRateInfo.TextMatrix(20, 0, "Total Due");
								// fill in the actual data
								this.vsRateInfo.TextMatrix(0, 1, FCConvert.ToString(rsLR.Get_Fields_Int32("ID")));
								this.vsRateInfo.TextMatrix(1, 1, FCConvert.ToString(rsLR.Get_Fields_Int32("RateKey")));
								this.vsRateInfo.TextMatrix(2, 1, rsLR.Get_Fields_String("Status"));
								this.vsRateInfo.TextMatrix(3, 1, Strings.Format(rsLR.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy"));
								this.vsRateInfo.TextMatrix(4, 1, Strings.Format(rsLR.Get_Fields_DateTime("InterestAppliedThroughDate"), "MM/dd/yyyy"));
								// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
								if (FCConvert.ToString(rsLR.Get_Fields("Book")) != "" || FCConvert.ToString(rsLR.Get_Fields("Page")) != "")
								{
									// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
									// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
									this.vsRateInfo.TextMatrix(5, 1, "B" + rsLR.Get_Fields("Book") + "P" + rsLR.Get_Fields("Page"));
								}
								else
								{
									this.vsRateInfo.TextMatrix(5, 1, "");
								}
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								this.vsRateInfo.TextMatrix(6, 1, Strings.Format(rsLR.Get_Fields("Principal"), "#,##0.00"));
								// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
								this.vsRateInfo.TextMatrix(7, 1, Strings.Format(rsLR.Get_Fields("Tax"), "#,##0.00"));
								// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
								this.vsRateInfo.TextMatrix(8, 1, Strings.Format(rsLR.Get_Fields("Interest"), "#,##0.00"));
								// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
								this.vsRateInfo.TextMatrix(9, 1, Strings.Format(rsLR.Get_Fields("Costs"), "#,##0.00"));
								this.vsRateInfo.TextMatrix(10, 1, Strings.Format(rsLR.Get_Fields_Decimal("InterestCharged") * -1, "#,##0.00"));
								// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
								this.vsRateInfo.TextMatrix(11, 1, Strings.Format(rsLR.Get_Fields("MaturityFee") * -1, "#,##0.00"));
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
								this.vsRateInfo.TextMatrix(12, 1, Strings.Format(rsLR.Get_Fields("Principal") + rsLR.Get_Fields("Interest") + rsLR.Get_Fields("Costs") - (rsLR.Get_Fields_Decimal("InterestCharged") + rsLR.Get_Fields("MaturityFee")), "#,##0.00"));
								this.vsRateInfo.TextMatrix(13, 1, "");
								this.vsRateInfo.TextMatrix(14, 1, Strings.Format(rsLR.Get_Fields_Decimal("PrincipalPaid"), "#,##0.00"));
								// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
								this.vsRateInfo.TextMatrix(15, 1, Strings.Format(rsLR.Get_Fields("TaxPaid"), "#,##0.00"));
								// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
								this.vsRateInfo.TextMatrix(16, 1, Strings.Format(rsLR.Get_Fields_Decimal("InterestPaid") + rsLR.Get_Fields("PLIPaid"), "#,##0.00"));
								this.vsRateInfo.TextMatrix(17, 1, Strings.Format(rsLR.Get_Fields_Decimal("CostsPaid"), "#,##0.00"));
								// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
								this.vsRateInfo.TextMatrix(18, 1, Strings.Format(rsLR.Get_Fields_Decimal("PrincipalPaid") + rsLR.Get_Fields("TaxPaid") + rsLR.Get_Fields_Decimal("InterestPaid") + rsLR.Get_Fields("PLIPaid") + rsLR.Get_Fields_Decimal("CostsPaid"), "#,##0.00"));
								this.vsRateInfo.TextMatrix(19, 1, "");
								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
								// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
								this.vsRateInfo.TextMatrix(20, 1, Strings.Format((rsLR.Get_Fields("Principal") + rsLR.Get_Fields("Interest") + rsLR.Get_Fields("Costs") - (rsLR.Get_Fields_Decimal("InterestCharged") + rsLR.Get_Fields("MaturityFee"))) - (rsLR.Get_Fields_Decimal("PrincipalPaid") + rsLR.Get_Fields_Decimal("InterestPaid") + rsLR.Get_Fields("PLIPaid") + rsLR.Get_Fields_Decimal("CostsPaid")), "#,##0.00"));
								// show the frame-----------------------------------------------------
								this.fraRateInfo.Text = "Lien Information";
								this.fraRateInfo.Left = FCConvert.ToInt32((this.Width - this.fraRateInfo.Width) / 3.0);
								//this.fraRateInfo.Top = FCConvert.ToInt32((this.Height - this.fraRateInfo.Height) / 3.0);
								this.fraRateInfo.CenterToContainer(this.ClientArea);
								this.fraRateInfo.Visible = true;
								this.vsRateInfo.SendToBack();
								this.cmdRIClose.Focus();
							}
						}
					}
				}
				mnuFilePrintRateInfo.Visible = true;
				rsRI.Reset();
				rsLR.Reset();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Rate Information Error" + " - " + FCConvert.ToString(lngErrCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				rsRI.Reset();
				rsLR.Reset();
			}
		}

		private void ShowAccountInfo()
		{
			int lngErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will show a frame with a lot of the account information that you would find in RE
				// as soon as the box loses focus, I will make it disappear
				clsDRWrapper rsAccountInfo = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				clsDRWrapper rsComment = new clsDRWrapper();
				string strBook = "";
				lngErrCode = 1;
				// show the account information
				rsAccountInfo.OpenRecordset("SELECT c.*, DeedName1 as OwnerFullNameLF, p.Address1 as OwnerAddress1, p.Address2 as OwnerAddress2, p.Address3 as OwnerAddress3, p.City as OwnerCity, p.PartyState as OwnerState, p.Zip as OwnerZip, DeedName2 as SecondOwnerFullNameLF, r.*, s.FullNameLF as SecondBillingFullNameLF FROM Master as c CROSS APPLY " + rsAccountInfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.OwnerPartyID, NULL, 'UT', c.AccountNumber) as p CROSS APPLY " + rsAccountInfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.SecondOwnerPartyID) as q CROSS APPLY " + rsAccountInfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.BillingPartyID, NULL, 'UT', c.AccountNumber) as r CROSS APPLY " + rsAccountInfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(c.SecondBillingPartyID) as s WHERE AccountNumber = " + FCConvert.ToString(CurrentAccount), modExtraModules.strUTDatabase);
				// format grid
				this.vsRateInfo.Cols = 1;
				this.vsRateInfo.Cols = 2;
				this.vsRateInfo.Rows = 20;
				this.vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				this.vsRateInfo.ExtendLastCol = true;
				this.vsRateInfo.ColWidth(0, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.32));
				this.vsRateInfo.ColWidth(1, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.65));
				this.vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
				// this will hide the border of the other information on the shared grid
				this.vsRateInfo.Select(19, 0, 19, 1);
				this.vsRateInfo.CellBorder(Color.Black, 0, 0, 0, 0, 0, 0);
				this.vsRateInfo.Select(15, 0, 15, 1);
				this.vsRateInfo.CellBorder(Color.Black, 0, 0, 0, 0, 0, 0);
				this.vsRateInfo.Select(0, 0);
				this.vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, this.vsRateInfo.Rows - 1, 0, this.vsRateInfo.Rows - 1, this.vsRateInfo.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				this.vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
				// show the first set of information (Account Information)--------------
				this.vsRateInfo.TextMatrix(0, 0, "Current Owner:");
				this.vsRateInfo.TextMatrix(1, 0, "Second Owner:");
				this.vsRateInfo.TextMatrix(2, 0, "Location:");
				this.vsRateInfo.TextMatrix(3, 0, "Billing Address:");
				this.vsRateInfo.TextMatrix(4, 0, "");
				this.vsRateInfo.TextMatrix(5, 0, "");
				this.vsRateInfo.TextMatrix(6, 0, "");
				// MAL@20080610: Shifted everything down to make room for Address 3 for both Billing and Owner addresses
				// Tracker Reference: 14154
				this.vsRateInfo.TextMatrix(7, 0, "");
				this.vsRateInfo.TextMatrix(8, 0, "Owner Address:");
				this.vsRateInfo.TextMatrix(9, 0, "");
				this.vsRateInfo.TextMatrix(10, 0, "");
				this.vsRateInfo.TextMatrix(11, 0, "");
				this.vsRateInfo.TextMatrix(12, 0, "");
				this.vsRateInfo.TextMatrix(13, 0, "Map Lot:");
				this.vsRateInfo.TextMatrix(14, 0, "Book Page(s):");
				this.vsRateInfo.TextMatrix(15, 0, "");
				this.vsRateInfo.TextMatrix(16, 0, "Tax Acquired:");
				this.vsRateInfo.TextMatrix(17, 0, "Associated RE Account:");
				this.vsRateInfo.TextMatrix(18, 0, "Account Group:");
				this.vsRateInfo.TextMatrix(19, 0, "Comment:");
				this.vsRateInfo.TextMatrix(0, 1, Strings.Trim(rsAccountInfo.Get_Fields_String("OwnerFullNameLF")));
				// TODO: Field [SecondBillingFullNameLF] not found!! (maybe it is an alias?)
				this.vsRateInfo.TextMatrix(1, 1, rsAccountInfo.Get_Fields_String("SecondOwnerFullNameLF"));
				this.vsRateInfo.TextMatrix(2, 1, Strings.Trim(rsAccountInfo.Get_Fields_Int32("StreetNumber") + " " + rsAccountInfo.Get_Fields_String("Apt") + " " + rsAccountInfo.Get_Fields_String("StreetName")));
				// location
				this.vsRateInfo.TextMatrix(3, 1, Strings.Trim(rsAccountInfo.Get_Fields_String("Address1")));
				this.vsRateInfo.TextMatrix(4, 1, Strings.Trim(rsAccountInfo.Get_Fields_String("Address2")));
				// MAL@20080610: Corrected to show the Address 3 and City correctly
				// Tracker Reference: 14154
				this.vsRateInfo.TextMatrix(5, 1, Strings.Trim(rsAccountInfo.Get_Fields_String("Address3")));
				// TODO: Field [PartyState] not found!! (maybe it is an alias?)
				this.vsRateInfo.TextMatrix(6, 1, Strings.Trim(Strings.Trim(rsAccountInfo.Get_Fields_String("City")) + ", " + rsAccountInfo.Get_Fields("PartyState") + " " + rsAccountInfo.Get_Fields_String("Zip")));
				// .TextMatrix(5, 1) = Trim(Trim(rsAccountInfo.Fields("BAddress3")) & " " & rsAccountInfo.Fields("BState") & ", " & rsAccountInfo.Fields("BZip"))
				// If Trim(rsAccountInfo.Fields("BZip4")) <> "" Then
				// .TextMatrix(6, 1) = .TextMatrix(6, 1) & "-" & rsAccountInfo.Fields("BZip4")
				// End If
				this.vsRateInfo.TextMatrix(7, 1, "");
				// blank line
				// TODO: Field [OwnerAddress1] not found!! (maybe it is an alias?)
				this.vsRateInfo.TextMatrix(8, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields("OwnerAddress1"))));
				// TODO: Field [OwnerAddress2] not found!! (maybe it is an alias?)
				this.vsRateInfo.TextMatrix(9, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields("OwnerAddress2"))));
				// TODO: Field [OwnerAddress3] not found!! (maybe it is an alias?)
				this.vsRateInfo.TextMatrix(10, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields("OwnerAddress3"))));
				// MAL@20080610: Changed to correctly display the Address 3 and City
				// Tracker Reference: 14154
				// TODO: Field [OwnerCity] not found!! (maybe it is an alias?)
				// TODO: Field [OwnerState] not found!! (maybe it is an alias?)
				// TODO: Field [OwnerZip] not found!! (maybe it is an alias?)
				this.vsRateInfo.TextMatrix(11, 1, Strings.Trim(Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields("OwnerCity"))) + ", " + rsAccountInfo.Get_Fields("OwnerState") + " " + rsAccountInfo.Get_Fields("OwnerZip")));
				// .TextMatrix(9, 1) = Trim(Trim(rsAccountInfo.Fields("OAddress3")) & " " & rsAccountInfo.Fields("OState") & ", " & rsAccountInfo.Fields("OZip"))
				// If Trim(rsAccountInfo.Fields("OZip4")) <> "" Then
				// .TextMatrix(11, 1) = .TextMatrix(11, 1) & "-" & rsAccountInfo.Fields("OZip4")
				// End If
				this.vsRateInfo.TextMatrix(12, 1, "");
				// blank line
				this.vsRateInfo.TextMatrix(13, 1, Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("MapLot"))));
				// rsTemp.OpenRecordset "SELECT * FROM BookPage WHERE Account = " & CurrentAccountRE & " AND Current = TRUE ORDER BY Line desc", strREDatabase
				strBook = Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("BookPage")));
				this.vsRateInfo.TextMatrix(14, 1, strBook);
				this.vsRateInfo.TextMatrix(15, 1, "");
				// blank line
				// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				if (modUTFunctions.IsAccountTaxAcquired_7(FCConvert.ToInt32(rsAccountInfo.Get_Fields("AccountNumber"))))
				{
					this.vsRateInfo.TextMatrix(16, 1, "TRUE");
				}
				else
				{
					this.vsRateInfo.TextMatrix(16, 1, "FALSE");
				}
				rsTemp.OpenRecordset("SELECT * FROM ModuleAssociation WHERE Module = 'UT' AND PrimaryAssociate = 1 AND REMasterAcct = " + FCConvert.ToString(CurrentAccount), "CentralData");
				if (!rsTemp.EndOfFile())
				{
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					this.vsRateInfo.TextMatrix(17, 1, FCConvert.ToString(rsTemp.Get_Fields("Account")));
				}
				else
				{
					this.vsRateInfo.TextMatrix(17, 1, "NONE");
				}
				this.vsRateInfo.TextMatrix(18, 1, FCConvert.ToString(lngGroupNumber));
				// kgk 11-28-2011 trocr-310  Added the database string to fetch comment from correct table
				// MAL@20070904: Changed to pull comment from UT Comment table
				// rsComment.OpenRecordset "SELECT Comment FROM CommentRec WHERE AccountKey = " & CurrentAccountKey
				rsComment.OpenRecordset("SELECT Comment FROM CommentRec WHERE AccountKey = " + FCConvert.ToString(CurrentAccountKey), modExtraModules.strUTDatabase);
				if (rsComment.RecordCount() > 0)
				{
					this.vsRateInfo.TextMatrix(19, 1, FCConvert.ToString(rsComment.Get_Fields_String("Comment")));
					// .TextMatrix(17, 1) = rsAccountInfo.Fields("Comment")
				}
				else
				{
					this.vsRateInfo.TextMatrix(19, 1, "");
				}
				// .TextMatrix(18, 0) = ""
				// .TextMatrix(19, 0) = ""
				lngErrCode = 1;
				// show the frame-----------------------------------------------------
				this.fraRateInfo.Text = "Utility Account Information";
				//this.fraRateInfo.Left = FCConvert.ToInt32((this.Width - this.fraRateInfo.Width) / 3.0);
				//this.fraRateInfo.Top = FCConvert.ToInt32((this.Height - this.fraRateInfo.Height) / 3.0);
				//FC:FINAL:MSH - Issue #935: set focus to form for correct work of BringToFront method
				this.Focus();
				this.fraRateInfo.Visible = true;
				this.fraRateInfo.CenterToContainer(this.ClientArea);
				this.fraRateInfo.BringToFront();
				mnuFilePrintRateInfo.Visible = true;
				cmdRIClose.Focus();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Rate Information Error" + " - " + FCConvert.ToString(lngErrCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: 'Return' As string	OnWrite(string, int)
		private string GetActualReceiptNumber_8(int lngRN, DateTime dtDate)
		{
			return GetActualReceiptNumber(ref lngRN, ref dtDate);
		}

		private string GetActualReceiptNumber(ref int lngRN, ref DateTime dtDate)
		{
			string GetActualReceiptNumber = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will accept a long of a receipt number ID in the CR table
				// if the receipt does not exist or any other error occurs, the function will
				// return 0, otherwise it returns the receipt number shown on the receipt
				clsDRWrapper rsRN = new clsDRWrapper();
				if (lngRN > 0)
				{
					if (modGlobalConstants.Statics.gboolCR)
					{
						// kgk 09-18-2012 trocr-357   rsRN.OpenRecordset "SELECT * FROM Receipt WHERE ReceiptKey = " & lngRN, strCRDatabase
						rsRN.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND ReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00:00 AM' AND ReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59:59 PM'", modExtraModules.strCRDatabase);
						if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
						{
							GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
						}
						else
						{
							// kgk 09-19-2011 trocr-357   rsRN.OpenRecordset "SELECT * FROM LastYearReceipt WHERE ReceiptKey = " & lngRN, strCRDatabase
							rsRN.OpenRecordset("SELECT * FROM LastYearReceipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND LastYearReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00:00 AM' AND LastYearReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59:59 PM'", modExtraModules.strCRDatabase);
							if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
							{
								GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
							}
							else
							{
								GetActualReceiptNumber = FCConvert.ToString(lngRN) + "*";
							}
						}
					}
					else
					{
						GetActualReceiptNumber = FCConvert.ToString(lngRN);
					}
				}
				else
				{
					GetActualReceiptNumber = FCConvert.ToString(0);
				}
				rsRN.Reset();
				return GetActualReceiptNumber;
			}
			catch (Exception ex)
			{
				
				GetActualReceiptNumber = FCConvert.ToString(lngRN) + "*";
			}
			return GetActualReceiptNumber;
		}

		private void CheckCD()
		{
			if (txtCD.Text == "N")
			{
				// MAL@20080207: Tracker Reference: 11087
				if (!modGlobalConstants.Statics.gboolCR && !modGlobalConstants.Statics.gboolBD)
				{
					txtCash.Enabled = false;
				}
				else
				{
					txtCash.Enabled = true;
					txtCash.TabStop = true;
				}
			}
			else
			{
				// MAL@20080207: Tracker Reference: 11087
				if (!modGlobalConstants.Statics.gboolCR && !modGlobalConstants.Statics.gboolBD)
				{
					txtCash.Enabled = false;
				}
				else
				{
					if (txtCash.Text == "N")
						txtCash.Text = "Y";
					txtCash.TabStop = false;
					txtCash.Enabled = false;
				}
				txtAcctNumber.Enabled = false;
				txtAcctNumber.TabStop = false;
			}
		}

		private void CheckCash()
		{
			if (txtCD.Text == "N")
			{
				if (txtCash.Text == "N")
				{
					txtAcctNumber.Enabled = true;
					txtAcctNumber.TabStop = true;
				}
				else
				{
					txtAcctNumber.Enabled = false;
					txtAcctNumber.TabStop = false;
				}
			}
			else
			{
				if (txtCash.Text == "N")
					txtCash.Text = "Y";
			}
		}

		private double CalculateAbatementInterest(ref double dblPrin, ref bool boolWater)
		{
			double CalculateAbatementInterest = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will find and return the current accounts abatement interest for that year
				int lngBill = 0;
				// vbPorter upgrade warning: dblInt As double	OnWrite(Decimal, double, short)
				double dblInt = 0;
				double dblPayTotal = 0;
				double dblTotalAbate;
				double dblRate = 0;
				string strTemp;
				string strType = "";
				clsDRWrapper rsBill = new clsDRWrapper();
				clsDRWrapper rsPay = new clsDRWrapper();
				clsDRWrapper rsRate = new clsDRWrapper();
				int lngAcct = 0;
				DateTime dtIntStartDate;
				dblTotalAbate = dblPrin;
				//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
				//strTemp = cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString();
				strTemp = cmbBillNumber.Text;
				if (Conversion.Val(strTemp) != 0)
				{
					lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
					// make sure that the year is valid
					// need to calculate the amount of interest for just the abatement amount
					// start at the last payment and work your way back until the bill
					lngAcct = CurrentAccount;
					if (boolWater)
					{
						// get the correct account
						strType = "W";
					}
					else
					{
						strType = "S";
					}
					strTemp = "SELECT * FROM Bill WHERE AccountKey = " + FCConvert.ToString(modUTFunctions.GetAccountKeyUT(ref lngAcct)) + " AND BillNumber = " + FCConvert.ToString(lngBill) + " AND (Service = '" + strType + "' OR Service = 'B')";
					rsBill.OpenRecordset(strTemp, modExtraModules.strUTDatabase);
					if (rsBill.EndOfFile() != true && rsBill.BeginningOfFile() != true)
					{
						// get the interest rate for this bill
						if ((FCConvert.ToInt32(rsBill.Get_Fields_Int32("WLienRecordNumber")) != 0 && boolWater) || (FCConvert.ToInt32(rsBill.Get_Fields_Int32("SLienRecordNumber")) != 0 && !boolWater))
						{
							// this is a lien and should not calc interest
							CalculateAbatementInterest = 0;
							return CalculateAbatementInterest;
						}
						rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsBill.Get_Fields_Int32("BillingRateKey"), modExtraModules.strUTDatabase);
						if (FCConvert.ToInt32(rsRate.Get_Fields(strType + "IntRate")) >= 0)
						{
							dblRate = FCConvert.ToDouble(rsRate.Get_Fields(strType + "IntRate"));
						}
						else
						{
							dblRate = modUTFunctions.Statics.dblUTInterestRate;
						}
						// check all of the payments
						if (FCConvert.ToInt32(rsBill.Get_Fields(strType + "LienRecordNumber")) == 0)
						{
							strTemp = "SELECT * FROM PaymentRec WHERE BillKey = " + rsBill.Get_Fields_Int32("ID") + " AND Lien = 0";
						}
						else
						{
							strTemp = "SELECT * FROM PaymentRec WHERE BillKey = " + rsBill.Get_Fields_Int32("ID") + " AND Lien = 1";
						}
						rsPay.OpenRecordset(strTemp, modExtraModules.strUTDatabase);
						if (rsPay.EndOfFile() != true && rsPay.BeginningOfFile() != true)
						{
							do
							{
								if (rsPay.Get_Fields_String("Code") == "P")
								{
									dtIntStartDate = (DateTime)rsPay.Get_Fields_DateTime("EffectiveInterestDate");
									if (dtIntStartDate.ToOADate() > 0)
									{
										// if the date came back correct
										// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblPayTotal = rsPay.Get_Fields("Principal") + rsPay.Get_Fields_Decimal("PreLienInterest") + rsPay.Get_Fields_Decimal("CurrentInterest") + rsPay.Get_Fields_Decimal("LienCost");
										if (dblTotalAbate < dblPayTotal)
										{
											dblPayTotal = dblTotalAbate;
										}
										if (modExtraModules.Statics.gstrRegularIntMethod == "P")
										{
											// calculate interest
											dblInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPayTotal), dtIntStartDate, modUTFunctions.Statics.UTEffectiveDate, dblRate, modGlobal.Statics.gintBasis));
										}
										else if (modExtraModules.Statics.gstrRegularIntMethod == "F")
										{
											if (FCConvert.ToDouble(rsBill.Get_Fields(strType + "IntAdded")) == 0)
											{
												#if TWUT0000
												dblInt = modUTUseCR.Statics.gdblFlatInterestAmount;
#else
												dblInt = modStatusPayments.Statics.gdblFlatInterestAmount;
												#endif
											}
											break;
										}
										else
										{
											dblInt = 0;
										}
										// return the value
										CalculateAbatementInterest += dblInt;
										dblTotalAbate -= dblPayTotal;
									}
									else
									{
										CalculateAbatementInterest = 0;
									}
								}
								rsPay.MoveNext();
							}
							while (!(dblTotalAbate <= 0 || rsPay.EndOfFile()));
						}
						else
						{
							CalculateAbatementInterest = 0;
						}
						if (modExtraModules.Statics.gstrRegularIntMethod == "P")
						{
							// this will calculate all of the interest that needs to be abated from the beginning
							dblInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(dblPrin), rsRate.Get_Fields_DateTime("IntStart"), modUTFunctions.Statics.UTEffectiveDate, dblRate, modGlobal.Statics.gintBasis));
							CalculateAbatementInterest += dblInt;
						}
						else if (modExtraModules.Statics.gstrRegularIntMethod == "F")
						{
							if (FCConvert.ToDouble(rsBill.Get_Fields(strType + "IntAdded")) == 0)
							{
								#if TWUT0000
								CalculateAbatementInterest = modUTUseCR.Statics.gdblFlatInterestAmount;
#else
								CalculateAbatementInterest = modStatusPayments.Statics.gdblFlatInterestAmount;
								#endif
							}
						}
					}
					else
					{
						CalculateAbatementInterest = 0;
					}
				}
				else
				{
					// invalid year
					CalculateAbatementInterest = 0;
				}
				rsBill.Reset();
				rsPay.Reset();
				rsRate.Reset();
				return CalculateAbatementInterest;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Calculating Abatement Interest", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CalculateAbatementInterest;
		}
		// vbPorter upgrade warning: 'Return' As DateTime	OnWrite(DateTime, short)
		private DateTime FindInterestStartDate(ref int lngAC, ref short intYR, string strType = "RE", short intPer = 1)
		{
			DateTime FindInterestStartDate = System.DateTime.Now;
			// this function will find and return the latest interest date for this account and year
			clsDRWrapper rsDate = new clsDRWrapper();
			string strSQL;
			DateTime dtIntStartDate;
			DateTime dtPerDate;
			strSQL = "WHERE Account = " + FCConvert.ToString(lngAC) + " AND BillingType = '" + strType + "' AND BillingYear = " + FCConvert.ToString(intYR);
			// get all of the payments
			rsDate.OpenRecordset("SELECT * FROM Bill " + strSQL, modExtraModules.strUTDatabase);
			if (rsDate.EndOfFile() != true && rsDate.BeginningOfFile() != true)
			{
				// find the interest paid through date
				if (rsDate.Get_Fields_DateTime("InterestAppliedThroughDate").ToOADate() != 0)
				{
					dtIntStartDate = (DateTime)rsDate.Get_Fields_DateTime("InterestAppliedThroughDate");
				}
				else
				{
					dtIntStartDate = modUTFunctions.Statics.UTEffectiveDate;
				}
				// find the interest start date for the period that is passed in
				rsDate.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + rsDate.Get_Fields_Int32("RateKey"), modExtraModules.strUTDatabase);
				if (rsDate.EndOfFile() != true && rsDate.BeginningOfFile() != true)
				{
					dtPerDate = rsDate.Get_Fields_DateTime("InterestStartDate" + FCConvert.ToString(intPer));
				}
				else
				{
					dtPerDate = modUTFunctions.Statics.UTEffectiveDate;
				}
				if (DateAndTime.DateDiff("D", dtIntStartDate, dtPerDate) < 0)
				{
					// find out which is the latest
					FindInterestStartDate = dtIntStartDate;
				}
				else
				{
					FindInterestStartDate = dtPerDate;
				}
			}
			else
			{
				FindInterestStartDate = DateTime.FromOADate(0);
			}
			rsDate.Reset();
			return FindInterestStartDate;
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		private void AddRefundedAbatementLine(int intIndex)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will add a payment to the payment grid that will not be added to and cash or go to budgetary
				// this is the refund line and will have a code of 'R' and have the exact values of the abatement
				int intRefundIndex;
				int lngRow;
				// find the next available index for the payment array
				for (intRefundIndex = 1; intRefundIndex <= modUTFunctions.MAX_UTPAYMENTS; intRefundIndex++)
				{
					if (modUTFunctions.Statics.UTPaymentArray[intRefundIndex].BillKey == 0)
						break;
				}
				if (intRefundIndex > modUTFunctions.MAX_UTPAYMENTS)
				{
					MessageBox.Show("Exeeded the maximum amount of payments. (" + FCConvert.ToString(modUTFunctions.MAX_UTPAYMENTS) + ")", "Refunded Abatement Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
					// exit w/o adding refund
				}
				// start by setting all of the defaults
				modUTFunctions.Statics.UTPaymentArray[intRefundIndex] = modUTFunctions.Statics.UTPaymentArray[intIndex];
				// set the Budgetary account number to an account that everyone will know that does not get passed through the accounting mod
				if (modExtraModules.IsThisCR())
				{
					modUTFunctions.Statics.UTPaymentArray[intRefundIndex].BudgetaryAccountNumber = GetRefundedAbabtementAccount_26(FCConvert.CBool(modUTFunctions.Statics.UTPaymentArray[intRefundIndex].BillCode == "L"), FCConvert.CBool(modUTFunctions.Statics.UTPaymentArray[intRefundIndex].Service == "W"), Conversion.Str(modUTFunctions.Statics.UTPaymentArray[intRefundIndex].Year));
				}
				else
				{
					modUTFunctions.Statics.UTPaymentArray[intRefundIndex].BudgetaryAccountNumber = "M NO ENTRY";
				}
				// force codes
				modUTFunctions.Statics.UTPaymentArray[intRefundIndex].Code = "R";
				modUTFunctions.Statics.UTPaymentArray[intRefundIndex].Period = "1";
				modUTFunctions.Statics.UTPaymentArray[intRefundIndex].CashDrawer = "N";
				modUTFunctions.Statics.UTPaymentArray[intRefundIndex].GeneralLedger = "N";
				// reverse all of the values
				modUTFunctions.Statics.UTPaymentArray[intRefundIndex].LienCost = modUTFunctions.Statics.UTPaymentArray[intIndex].LienCost * -1;
				modUTFunctions.Statics.UTPaymentArray[intRefundIndex].CurrentInterest = modUTFunctions.Statics.UTPaymentArray[intIndex].CurrentInterest * -1;
				modUTFunctions.Statics.UTPaymentArray[intRefundIndex].PreLienInterest = modUTFunctions.Statics.UTPaymentArray[intIndex].PreLienInterest * -1;
				modUTFunctions.Statics.UTPaymentArray[intRefundIndex].Principal = modUTFunctions.Statics.UTPaymentArray[intIndex].Principal * -1;
				// MAL@20081126: Add Tax Amount to Array
				// Tracker Reference: 16307
				modUTFunctions.Statics.UTPaymentArray[intRefundIndex].Tax = modUTFunctions.Statics.UTPaymentArray[intIndex].Tax * -1;
				// add the payment to the payment list
				lngRow = vsPayments.Rows;
				vsPayments.AddItem("");
				// Year
				vsPayments.TextMatrix(lngRow, lngPayGridColBill, FCConvert.ToString(modUTFunctions.Statics.UTPaymentArray[intRefundIndex].BillNumber));
				// Date
				vsPayments.TextMatrix(lngRow, lngPayGridColDate, Strings.Format(modUTFunctions.Statics.UTPaymentArray[intRefundIndex].RecordedTransactionDate, "MM/dd/yyyy"));
				// Ref
				vsPayments.TextMatrix(lngRow, lngPayGridColRef, modUTFunctions.Statics.UTPaymentArray[intRefundIndex].Reference);
				// Per
				vsPayments.TextMatrix(lngRow, lngPayGridColService, modUTFunctions.Statics.UTPaymentArray[intRefundIndex].Service);
				// Code
				vsPayments.TextMatrix(lngRow, lngPayGridColCode, modUTFunctions.Statics.UTPaymentArray[intRefundIndex].Code);
				// Cash
				vsPayments.TextMatrix(lngRow, lngPayGridColCDAC, modUTFunctions.Statics.UTPaymentArray[intRefundIndex].CashDrawer + "   " + modUTFunctions.Statics.UTPaymentArray[intRefundIndex].GeneralLedger);
				// Prin
				vsPayments.TextMatrix(lngRow, lngPayGridColPrincipal, Strings.Format(modUTFunctions.Statics.UTPaymentArray[intRefundIndex].Principal, "#,##0.00"));
				// Tax
				vsPayments.TextMatrix(lngRow, lngPayGridColTax, Strings.Format(modUTFunctions.Statics.UTPaymentArray[intRefundIndex].Tax, "#,##0.00"));
				// Int
				vsPayments.TextMatrix(lngRow, lngPayGridColInterest, Strings.Format(modUTFunctions.Statics.UTPaymentArray[intRefundIndex].CurrentInterest + modUTFunctions.Statics.UTPaymentArray[intRefundIndex].PreLienInterest, "#,##0.00"));
				// Costs
				vsPayments.TextMatrix(lngRow, lngPayGridColCosts, Strings.Format(modUTFunctions.Statics.UTPaymentArray[intRefundIndex].LienCost, "#,##0.00"));
				// Array Index
				vsPayments.TextMatrix(lngRow, lngPayGridColArrayIndex, FCConvert.ToString(intRefundIndex));
				// ID Number from autonumberfield in DB
				vsPayments.TextMatrix(lngRow, lngPayGridColKeyNumber, FCConvert.ToString(0));
				// CHGINT Number
				vsPayments.TextMatrix(lngRow, lngPayGridColCHGINTNumber, FCConvert.ToString(0));
				// Total
				vsPayments.TextMatrix(lngRow, lngPayGridColTotal, Strings.Format(modUTFunctions.Statics.UTPaymentArray[intRefundIndex].CurrentInterest + modUTFunctions.Statics.UTPaymentArray[intRefundIndex].PreLienInterest + modUTFunctions.Statics.UTPaymentArray[intRefundIndex].Principal + modUTFunctions.Statics.UTPaymentArray[intRefundIndex].LienCost + modUTFunctions.Statics.UTPaymentArray[intRefundIndex].Tax, "#,##0.00"));
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Refunded Abatement Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowDiscountFrame_2(bool boolCheckInt)
		{
			ShowDiscountFrame(boolCheckInt);
		}

		private void ShowDiscountFrame(bool boolCheckInt = false)
		{
			// this sub will show the Discount Frame with calculated figures, users can change the principal payment
			// vbPorter upgrade warning: dblTotal As double	OnWrite(double, bool)
			double dblTotal = 0;
			// the total payment needed
			double dblDisc = 0;
			// total discount
			double dblDiscPayment;
			// how much is due after discount is taken
			clsDRWrapper rsDisc = new clsDRWrapper();
			int lngBill = 0;
			int LRN = 0;
			double dblOrigPrin = 0;
			double dblPrinPaid = 0;
			bool boolWater = false;
			switch (FindPaymentType())
			{
				case 0:
					{
						boolWater = true;
						break;
					}
				case 1:
					{
						boolWater = false;
						break;
					}
				default:
					{
						return;
					}
			}
			//end switch
			// do not allow a discount if there is a negative value owed for that year and not auto in year
			if (FCConvert.ToDecimal(this.vsPeriod.TextMatrix(0, 1)) < 0)
			{
				// , Len(.vsPeriod.TextMatrix(0, 1)) - 2)) + CCur(Right$(.vsPeriod.TextMatrix(0, 2), Len(.vsPeriod.TextMatrix(0, 2)) - 2)) + CCur(Right$(.vsPeriod.TextMatrix(0, 3), Len(.vsPeriod.TextMatrix(0, 3)) - 2)) + CCur(Right$(.vsPeriod.TextMatrix(0, 4), Len(.vsPeriod.TextMatrix(0, 4)) - 2)) < 0 Then
				this.cmbCode.SelectedIndex = 0;
				return;
			}
			// find which year is being discounted
			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
			//lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(this.cmbBillNumber.Items[this.cmbBillNumber.SelectedIndex].ToString()));
			lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(this.cmbBillNumber.Text)));
			// find the total due for this year
			rsDisc.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") qTmp WHERE AccountKey = " + FCConvert.ToString(this.CurrentAccountKey) + " AND BillNumber = " + FCConvert.ToString(lngBill), modExtraModules.strUTDatabase);
			if (rsDisc.RecordCount() != 0)
			{
				if (FCConvert.ToInt32(rsDisc.Get_Fields_Int32("LienRecordNumber")) == 0)
				{
					// regular billing
					if (boolWater)
					{
						if (rsDisc.Get_Fields_Double("WINTAdded") < 0 && boolCheckInt)
						{
							if (MessageBox.Show("There is interest on this account.  Are you sure that you would like to continue?", "Account Accruing Interest", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
							{
								return;
							}
						}
						dblOrigPrin = rsDisc.Get_Fields_Double("WPrinOwed");
						dblPrinPaid = rsDisc.Get_Fields_Double("WPrinOwed");
						dblTotal = dblOrigPrin - dblPrinPaid;
					}
					else
					{
						if (rsDisc.Get_Fields_Double("SINTAdded") < 0 && boolCheckInt)
						{
							if (MessageBox.Show("There is interest on this account.  Are you sure that you would like to continue?", "Account Accruing Interest", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
							{
								return;
							}
						}
						dblOrigPrin = rsDisc.Get_Fields_Double("SPrinOwed");
						dblPrinPaid = rsDisc.Get_Fields_Double("SPrinOwed");
						dblTotal = dblOrigPrin - dblPrinPaid;
					}
				}
				else
				{
					// lien
					LRN = FCConvert.ToInt32(rsDisc.Get_Fields_Int32("LienRecordNumber"));
					if (rsDisc.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(LRN), modExtraModules.strUTDatabase))
					{
						if (rsDisc.RecordCount() != 0)
						{
							if (rsDisc.Get_Fields_Decimal("InterestCharged") < 0 && boolCheckInt)
							{
								if (MessageBox.Show("There is interest on this account.  Are you sure that you would like to continue?", "Account Accruing Interest", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
								{
									return;
								}
							}
							dblOrigPrin = FCConvert.ToDouble(rsDisc.Get_Fields_Decimal("PrinOwed"));
							// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
							dblPrinPaid = FCConvert.ToDouble(rsDisc.Get_Fields("PrinPaid"));
							dblTotal = FCConvert.ToDouble(dblOrigPrin == dblPrinPaid);
						}
						else
						{
							// lien record not found
							MessageBox.Show("Lien record number, " + FCConvert.ToString(LRN) + ", not found.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						}
					}
					else
					{
						MessageBox.Show("Error opening table LienRec.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
				// calculate discount
				dblDisc = CalculateDiscount_2(dblTotal);
				// show discount and user contribution
				this.txtDiscDisc.Text = Strings.Format(dblDisc, "#,##0.00");
				this.txtDiscPrin.Text = Strings.Format(dblTotal - dblDisc, "#,##0.00");
				this.lblDiscTotal.Text = Strings.Format(dblTotal, "#,##0.00");
				this.lblDiscHiddenTotal.Text = FCConvert.ToString(dblTotal);
				this.lblOriginalTax.Text = Strings.Format(dblOrigPrin, "#,##0.00");
				this.lblOriginalTax.Tag = Strings.Format(dblPrinPaid, "#,##0.00");
				// set the instructions
				this.lblDiscountInstructions.Text = "Is this principal payment and discount correct?";
				// lock textboxes
				this.txtDiscPrin.ReadOnly = true;
				this.txtDiscDisc.ReadOnly = true;
				// align the frame
				this.fraDiscount.Left = FCConvert.ToInt32((this.Width - this.fraDiscount.Width) / 2.0);
				this.fraDiscount.Top = FCConvert.ToInt32((this.Height - this.fraDiscount.Height) / 2.0);
				// show the frame
				this.fraDiscount.Visible = true;
				this.WGRID.Visible = false;
				this.SGRID.Visible = false;
				this.fraPayment.Visible = false;
				this.mnuPayment.Visible = false;
			}
			else
			{
				// no current record
				MessageBox.Show("No bill #" + (FCConvert.ToString(lngBill)) + ".", "Discount Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.cmbCode.SelectedIndex = 2;
				// set the code box back to a payment
			}
			rsDisc.Reset();
		}

		private double CalculateDiscount_2(double dblPrin, double dblDiscPay = 0, bool boolPassPrincipalPaid = false)
		{
			return CalculateDiscount(dblPrin, ref dblDiscPay, boolPassPrincipalPaid);
		}

		private double CalculateDiscount_20(double dblPrin, double dblDiscPay = 0, bool boolPassPrincipalPaid = false)
		{
			return CalculateDiscount(dblPrin, ref dblDiscPay, boolPassPrincipalPaid);
		}

		private double CalculateDiscount(double dblPrin, ref double dblDiscPay/* = 0 */, bool boolPassPrincipalPaid = false)
		{
			double CalculateDiscount = 0;
			// this function returns the amount of discount for the principal passed in dblPrin
			// if dblPrin is passed a value of 0 and dblDiscPay has a value passed in, this is treated as
			// how much the user wants to pay in cash and the program will calculate the discount for just
			// the amount that is to be paid
			clsDRWrapper rsDisc = new clsDRWrapper();
			double dblDiscRate;
			double dblDisc = 0;
			// find the rate
			dblDiscRate = 0;
			// kk06042015 trout-1166  Customize saves this in UtilityBilling, not CollectionControl
			// There is no process that updates CollectionControl. Was this table linked to CL at one point?
			// rsDisc.OpenRecordset "SELECT * FROM CollectionControl", strUTDatabase
			rsDisc.OpenRecordset("SELECT DiscountPercent FROM UtilityBilling", modExtraModules.strUTDatabase);
			if (rsDisc.RecordCount() > 0)
			{
				// TODO: Check the table for the column [DiscountPercent] and replace with corresponding Get_Field method
				dblDiscRate = FCConvert.ToDouble(rsDisc.Get_Fields("DiscountPercent"));
				// 10000     'kk060421015 Change to store rate as demial value to be consistent - 7% => 0.07
			}
			if (boolPassPrincipalPaid)
			{
				if (dblPrin > 0)
				{
					// if the principal is passed in
					dblDisc = ((dblPrin / (1 - dblDiscRate)) - dblPrin);
				}
				else if (dblDiscPay != 0)
				{
					// if the principal that is to be paid is passed in
					dblDisc = ((dblDiscPay / (1 - dblDiscRate)) - dblDiscPay);
					// dblDisc = dblDiscPay * dblDiscRate
				}
				else
				{
					// nothing is passed in
					CalculateDiscount = 0;
				}
			}
			else
			{
				if (dblPrin > 0)
				{
					// if the principal is passed in
					dblDisc = (dblPrin * dblDiscRate);
				}
				else if (dblDiscPay != 0)
				{
					// if the principal that is to be paid is passed in
					dblDisc = dblDiscPay * dblDiscRate;
				}
				else
				{
					// nothing is passed in
					CalculateDiscount = 0;
				}
			}
			CalculateDiscount = modGlobal.Round(dblDisc, 2);
			rsDisc.Reset();
			return CalculateDiscount;
		}

		private void AddDiscountToList_2(bool boolWater)
		{
			AddDiscountToList(ref boolWater);
		}

		private void AddDiscountToList(ref bool boolWater)
		{
			double dblPrinNeed;
			double dblIntNeed = 0;
			double dblCostNeed = 0;
			int lngBill;
			int intCT;
			// vbPorter upgrade warning: intTemp As short, int --> As DialogResult
			DialogResult intTemp;
			string PayCode = "";
			// (W)ater or (S)ewer
			string BillCode = "";
			// (R)egular or (L)ien
			string strTemp;
			clsDRWrapper rsTemp = new clsDRWrapper();
			clsDRWrapper rsLienRec = new clsDRWrapper();
			int intType;
			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
			//lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(this.cmbBillNumber.Items[this.cmbBillNumber.SelectedIndex].ToString()));
			lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(this.cmbBillNumber.Text)));
			intType = FindPaymentType();
			strTemp = "SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(CurrentAccountKey) + " AND BillingYear = " + FCConvert.ToString(lngBill);
			rsTemp.OpenRecordset(strTemp, modExtraModules.strUTDatabase);
			if (rsTemp.RecordCount() != 0)
			{
			}
			else
			{
				MessageBox.Show("Error opening database.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			for (intCT = 1; intCT <= modUTFunctions.MAX_UTPAYMENTS; intCT++)
			{
				// this finds a place in the array
				if (modUTFunctions.Statics.UTPaymentArray[intCT].Year == 0)
				{
					// to store the new payment record
					break;
				}
			}
			if (boolWater)
			{
				PayCode = "W";
			}
			else
			{
				PayCode = "S";
			}
			// XXXXXX THIS IS COLLECTIONS CODE! XXXXXXX
			// get the next year and its total due values
			if (FCConvert.ToInt32(rsTemp.Get_Fields_Int32("LienRecordNumber")) == 0)
			{
				dblPrinNeed = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("TaxDue1") + rsTemp.Get_Fields_Decimal("TaxDue2") + rsTemp.Get_Fields_Decimal("TaxDue3") + rsTemp.Get_Fields_Decimal("TaxDue4") - rsTemp.Get_Fields_Decimal("PrincipalPaid"));
				dblIntNeed = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("InterestCharged") + rsTemp.Get_Fields_Decimal("InterestPaid")) * -1;
				dblCostNeed = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("DemandFees") - rsTemp.Get_Fields_Decimal("DemandFeesPaid"));
				BillCode = "R";
			}
			else
			{
				BillCode = "L";
				rsLienRec.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTemp.Get_Fields_Int32("LienRecordNumber"), modExtraModules.strUTDatabase);
				if (rsLienRec.RecordCount() != 0)
				{
					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
					dblPrinNeed = FCConvert.ToDouble(rsLienRec.Get_Fields("Principal") - rsLienRec.Get_Fields_Decimal("PrincipalPaid"));
					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
					dblIntNeed = FCConvert.ToDouble(rsLienRec.Get_Fields("Interest") - rsLienRec.Get_Fields_Decimal("InterestCharged") - rsLienRec.Get_Fields_Decimal("InterestPaid") + modUTFunctions.Statics.dblUTCurrentInt[lngBill, intType]);
					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
					dblCostNeed = FCConvert.ToDouble(rsLienRec.Get_Fields("Costs") - rsLienRec.Get_Fields("MaturityFee") - rsLienRec.Get_Fields_Decimal("CostsPaid"));
				}
			}
			intTemp = DialogResult.Yes;
			if (dblIntNeed > 0 || dblCostNeed > 0)
			{
				intTemp = MessageBox.Show("There is outstanding Interest and/or Costs, are you sure that this is a valid discount?", "Interest and Cost Warning", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			}
			switch (intTemp)
			{
				case DialogResult.Yes:
					{
						// Yes
						if (intCT < modUTFunctions.MAX_UTPAYMENTS)
						{
							//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
							modUTFunctions.Statics.UTPaymentArray[intCT] = new modUTFunctions.UTPayment(0);
							// this adds it to the array of payments
							if (modStatusPayments.Statics.boolRE)
							{
								modUTFunctions.Statics.UTPaymentArray[intCT].Account = StaticSettings.TaxCollectionValues.LastAccountRE;
								modGlobalFunctions.AddCYAEntry_8("CL", "Override of discount amount. RE Account: " + FCConvert.ToString(modUTFunctions.Statics.UTPaymentArray[intCT].Account));
							}
							else
							{
								modUTFunctions.Statics.UTPaymentArray[intCT].Account = StaticSettings.TaxCollectionValues.LastAccountPP;
								modGlobalFunctions.AddCYAEntry_8("CL", "Override of discount amount. PP Account: " + FCConvert.ToString(modUTFunctions.Statics.UTPaymentArray[intCT].Account));
							}
							modUTFunctions.Statics.UTPaymentArray[intCT].ActualSystemDate = DateTime.Today;
							modUTFunctions.Statics.UTPaymentArray[intCT].BillCode = BillCode;
							if (BillCode == "L")
							{
								modUTFunctions.Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
							}
							else
							{
								modUTFunctions.Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillKey"));
							}
							modUTFunctions.Statics.UTPaymentArray[intCT].CashDrawer = txtCD.Text;
							// allow the user to set it to yesterday
							modUTFunctions.Statics.UTPaymentArray[intCT].Code = "P";
							modUTFunctions.Statics.UTPaymentArray[intCT].Comments = this.txtComments.Text;
							modUTFunctions.Statics.UTPaymentArray[intCT].EffectiveInterestDate = modUTFunctions.Statics.UTEffectiveDate;
							modUTFunctions.Statics.UTPaymentArray[intCT].GeneralLedger = "Y";
							// cannot write this off
							modUTFunctions.Statics.UTPaymentArray[intCT].Service = PayCode;
							modUTFunctions.Statics.UTPaymentArray[intCT].PreLienInterest = 0;
							modUTFunctions.Statics.UTPaymentArray[intCT].Principal = FCConvert.ToDecimal(txtDiscPrin.Text);
							modUTFunctions.Statics.UTPaymentArray[intCT].CurrentInterest = 0;
							modUTFunctions.Statics.UTPaymentArray[intCT].LienCost = 0;
							modUTFunctions.Statics.UTPaymentArray[intCT].ReceiptNumber = "P";
							if (this.txtTransactionDate.Text == "")
							{
								modUTFunctions.Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
							}
							else
							{
								modUTFunctions.Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(txtTransactionDate.Text, "MM/dd/yyyy"));
							}
							modUTFunctions.Statics.UTPaymentArray[intCT].Reference = Strings.Trim(txtReference.Text + " ");
							modUTFunctions.Statics.UTPaymentArray[intCT].ReversalID = -1;
							// .Teller =
							// .TransNumber =
							// .PaidBy =
							// .CHGINTNumber =
							modUTFunctions.Statics.UTPaymentArray[intCT].Year = lngBill;
							FillPaymentGrid(vsPayments.Rows, intCT, lngBill);
						}
						// now add the discount line
						for (intCT = 1; intCT <= modUTFunctions.MAX_UTPAYMENTS; intCT++)
						{
							// this finds a place in the array
							if (modUTFunctions.Statics.UTPaymentArray[intCT].Year == 0)
							{
								// to store the new payment record
								break;
							}
						}
						if (intCT < modUTFunctions.MAX_UTPAYMENTS)
						{
							// this adds it to the array of payments
							if (modStatusPayments.Statics.boolRE)
							{
								modUTFunctions.Statics.UTPaymentArray[intCT].Account = StaticSettings.TaxCollectionValues.LastAccountRE;
							}
							else
							{
								modUTFunctions.Statics.UTPaymentArray[intCT].Account = StaticSettings.TaxCollectionValues.LastAccountPP;
							}
							modUTFunctions.Statics.UTPaymentArray[intCT].ActualSystemDate = DateTime.Today;
							modUTFunctions.Statics.UTPaymentArray[intCT].BillCode = BillCode;
							if (BillCode == "L")
							{
								modUTFunctions.Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
							}
							else
							{
								modUTFunctions.Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillKey"));
							}
							modUTFunctions.Statics.UTPaymentArray[intCT].CashDrawer = "N";
							// frmUTCLStatus.txtCD.Text
							if (modUTFunctions.Statics.UTPaymentArray[intCT].CashDrawer == "N")
							{
								modUTFunctions.Statics.UTPaymentArray[intCT].BudgetaryAccountNumber = this.txtAcctNumber.TextMatrix(0, 0);
							}
							modUTFunctions.Statics.UTPaymentArray[intCT].Code = "D";
							modUTFunctions.Statics.UTPaymentArray[intCT].Comments = this.txtComments.Text;
							modUTFunctions.Statics.UTPaymentArray[intCT].EffectiveInterestDate = modUTFunctions.Statics.UTEffectiveDate;
							modUTFunctions.Statics.UTPaymentArray[intCT].GeneralLedger = "N";
							// frmUTCLStatus.txtCash.Text
							modUTFunctions.Statics.UTPaymentArray[intCT].Service = PayCode;
							modUTFunctions.Statics.UTPaymentArray[intCT].PreLienInterest = 0;
							modUTFunctions.Statics.UTPaymentArray[intCT].Principal = FCConvert.ToDecimal(this.txtDiscDisc.Text);
							modUTFunctions.Statics.UTPaymentArray[intCT].CurrentInterest = 0;
							modUTFunctions.Statics.UTPaymentArray[intCT].LienCost = 0;
							modUTFunctions.Statics.UTPaymentArray[intCT].ReceiptNumber = "P";
							if (this.txtTransactionDate.Text == "")
							{
								modUTFunctions.Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
							}
							else
							{
								modUTFunctions.Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(this.txtTransactionDate.Text, "MM/dd/yyyy"));
							}
							modUTFunctions.Statics.UTPaymentArray[intCT].Reference = Strings.Trim(this.txtReference.Text + " ");
							modUTFunctions.Statics.UTPaymentArray[intCT].ReversalID = -1;
							// .Teller =
							// .TransNumber =
							// .PaidBy =
							// .CHGINTNumber =
							modUTFunctions.Statics.UTPaymentArray[intCT].Year = lngBill;
							FillPaymentGrid(this.vsPayments.Rows, intCT, lngBill);
						}
						break;
					}
				default:
					{
						cmdDisc_Click_2(2);
						break;
					}
			}
			//end switch
			if (intType == 0)
			{
				modUTFunctions.UTBillPending_72(WGRID, lngBill, true, 1);
			}
			else
			{
				modUTFunctions.UTBillPending_72(SGRID, lngBill, false, 1);
			}
			rsTemp.Reset();
			rsLienRec.Reset();
		}

		public void FillPaymentGrid(int cRow, int intCT, int lngBill, int lngID = 0)
		{
			if (cRow == 0)
				cRow = 1;
			this.vsPayments.AddItem("", cRow);
			if (intCT < 0)
			{
				this.vsPayments.TextMatrix(cRow, lngPayGridColArrayIndex, FCConvert.ToString(intCT));
				intCT *= -1;
			}
			else
			{
				this.vsPayments.TextMatrix(cRow, lngPayGridColArrayIndex, FCConvert.ToString(intCT));
			}
			// this adds it to the grid
			this.vsPayments.TextMatrix(cRow, lngPayGridColBill, FCConvert.ToString(lngBill));
			this.vsPayments.TextMatrix(cRow, lngPayGridColDate, Strings.Format(modUTFunctions.Statics.UTPaymentArray[intCT].RecordedTransactionDate, "MM/dd/yyyy"));
			this.vsPayments.TextMatrix(cRow, lngPayGridColRef, modUTFunctions.Statics.UTPaymentArray[intCT].Reference);
			this.vsPayments.TextMatrix(cRow, lngPayGridColService, modUTFunctions.Statics.UTPaymentArray[intCT].Service);
			this.vsPayments.TextMatrix(cRow, lngPayGridColCode, modUTFunctions.Statics.UTPaymentArray[intCT].Code);
			this.vsPayments.TextMatrix(cRow, lngPayGridColCDAC, modUTFunctions.Statics.UTPaymentArray[intCT].CashDrawer + "   " + modUTFunctions.Statics.UTPaymentArray[intCT].GeneralLedger);
			this.vsPayments.TextMatrix(cRow, lngPayGridColPrincipal, Strings.Format(modUTFunctions.Statics.UTPaymentArray[intCT].Principal, "#,##0.00"));
			this.vsPayments.TextMatrix(cRow, lngPayGridColTax, Strings.Format(modUTFunctions.Statics.UTPaymentArray[intCT].Tax, "#,##0.00"));
			this.vsPayments.TextMatrix(cRow, lngPayGridColInterest, Strings.Format(modUTFunctions.Statics.UTPaymentArray[intCT].CurrentInterest + modUTFunctions.Statics.UTPaymentArray[intCT].PreLienInterest, "#,##0.00"));
			this.vsPayments.TextMatrix(cRow, lngPayGridColCosts, Strings.Format(modUTFunctions.Statics.UTPaymentArray[intCT].LienCost, "#,##0.00"));
			this.vsPayments.TextMatrix(cRow, lngPayGridColKeyNumber, FCConvert.ToString(lngID));
			this.vsPayments.TextMatrix(cRow, lngPayGridColCHGINTNumber, FCConvert.ToString(modUTFunctions.Statics.UTPaymentArray[intCT].CHGINTNumber));
			this.vsPayments.TextMatrix(cRow, lngPayGridColTotal, Strings.Format(modUTFunctions.Statics.UTPaymentArray[intCT].Principal + modUTFunctions.Statics.UTPaymentArray[intCT].Tax + modUTFunctions.Statics.UTPaymentArray[intCT].PreLienInterest + modUTFunctions.Statics.UTPaymentArray[intCT].CurrentInterest + modUTFunctions.Statics.UTPaymentArray[intCT].LienCost, "#,##0.00"));
		}

		private string GetAccountNote(ref int lngAccount, ref bool boolPopUp)
		{
			string GetAccountNote = "";
			// this function will return then string of the note
			// if boolPopUp is set, then pop the message up
			clsDRWrapper rsNote = new clsDRWrapper();
			rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(lngAccount), modExtraModules.strUTDatabase);
			if (rsNote.EndOfFile() != true)
			{
				GetAccountNote = FCConvert.ToString(rsNote.Get_Fields_String("Comment"));
				if (FCConvert.ToInt32(rsNote.Get_Fields_Int32("Priority")) > 0)
				{
					boolPopUp = true;
					// this will get passed back out of this function
				}
				else
				{
					boolPopUp = false;
				}
			}
			rsNote.Reset();
			return GetAccountNote;
		}

		private void vsRateInfo_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsRateInfo[e.ColumnIndex, e.RowIndex];
            // this will show the contents of the box in the tooltip in case it is too large for the box
            int lngMR;
			int lngMC;
			lngMR = vsRateInfo.GetFlexRowIndex(e.RowIndex);
			lngMC = vsRateInfo.GetFlexColIndex(e.ColumnIndex);
			if (lngMR >= 0 && lngMC >= 0)
			{
				//ToolTip1.SetToolTip(vsRateInfo, vsRateInfo.TextMatrix(vsRateInfo.MouseRow, vsRateInfo.MouseCol));
				cell.ToolTipText =  vsRateInfo.TextMatrix(vsRateInfo.GetFlexRowIndex(e.RowIndex), vsRateInfo.GetFlexColIndex(e.ColumnIndex));
			}
		}

		private void CheckTaxClubPayments()
		{
			// Dim rsTC                    As New clsDRWrapper
			// this will check to see if this account has tax club payments due
			// if it does, then show the tax club label so the user can see it and
			// click it to automatically load the payment
			// 
			// If boolRE Then
			// rsTC.OpenRecordset "SELECT * FROM TaxClub WHERE Account = " & CurrentAccountRE & " AND Type = 'RE' AND TotalPayments >= TotalPaid", strUTDatabase
			// Else
			// rsTC.OpenRecordset "SELECT * FROM TaxClub WHERE Account = " & CurrentAccountPP & " AND Type = 'PP' AND TotalPayments >= TotalPaid", strUTDatabase
			// End If
			// 
			// If rsTC.EndOfFile Then
			// no pending Tax club activity
			// lblTaxClub.Visible = False
			// Else
			// there is pending tax club activity
			// If rsTC.Fields("TotalPayments") - rsTC.Fields("TotalPaid") > rsTC.Fields("PaymentAmount") Then
			// lblTaxClub.Text = "TC: " & Format(rsTC.Fields("PaymentAmount"), "#,##0.00")
			// Else
			// lblTaxClub.Text = "TC: " & Format((rsTC.Fields("TotalPayments") - rsTC.Fields("TotalPaid")), "#,##0.00")
			// End If
			// lblTaxClub.Tag = rsTC.Fields("Year")
			// lblTaxClub.Visible = True
			// End If
			// 
			// rsTC.Reset
		}

		private void CreateTaxClubPayment()
		{
			// this will create a tax club payment
			// Dim intCT                       As Integer
			// Dim intYR                       As Integer
			// Dim dblP                        As Double
			// 
			// lngUTCHGINT = 0
			// intYR = Val(lblTaxClub.Tag & "1")
			// this will set the year
			// For intCT = 0 To cmbBillNumber.ListCount - 1
			// If FormatYear(CStr(intYR)) = Trim$(cmbBillNumber.List(intCT)) Then
			// cmbBillNumber.ListIndex = intCT
			// Exit For
			// End If
			// Next
			// 
			// default values
			// txtCD.Text = "Y"
			// txtCash.Text = "Y"
			// txtTransactionDate.Text = Format(Date, "MM/dd/yyyy")
			// txtReference.Text = "TAXCLUB"
			// 
			// this will find the payment period in the combobox that matches the one on the payment
			// For intCT = 0 To cmbPeriod.ListCount - 1
			// If "A" = Left$(cmbPeriod.List(intCT), 1) Then
			// cmbPeriod.ListIndex = intCT
			// Exit For
			// End If
			// Next
			// 
			// this will find the payment code in the combobox
			// For intCT = 0 To cmbCode.ListCount - 1
			// If "U" = Left$(cmbCode.List(intCT), 1) Then
			// cmbCode.ListIndex = intCT
			// Exit For
			// End If
			// Next
			// 
			// set the total variables
			// dblP = CDbl(Right(lblTaxClub.Caption, Len(lblTaxClub.Caption) - 3))
			// txtComments.Text = ""
			// 
			// txtPrincipal.Text = Format(dblP, "#,##0.00")
			// txtInterest.Text = Format(0, "#,##0.00")
			// txtCosts.Text = Format(0, "#,##0.00")
		}

		private void InputBoxAdjustment_2(bool boolCollapse)
		{
			InputBoxAdjustment(ref boolCollapse);
		}

		private void InputBoxAdjustment(ref bool boolCollapse)
		{
			// vbPorter upgrade warning: dblTemp As double	OnWriteFCConvert.ToDecimal(
			double dblTemp = 0;
			// this will adjust the amount input boxes to only show one unless it is a correction
			boolCollapsedInputBoxes = boolCollapse;
			if (Conversion.Val(txtPrincipal.Text) == 0)
				txtPrincipal.Text = "0.00";
			if (Conversion.Val(txtInterest.Text) == 0)
				txtInterest.Text = "0.00";
			if (Conversion.Val(txtCosts.Text) == 0)
				txtCosts.Text = "0.00";
			if (Conversion.Val(txtTax.Text) == 0)
				txtTax.Text = "0.00";
			// hide/show the labels and textboxes
			txtPrincipal.Visible = !boolCollapse;
			txtCosts.Visible = !boolCollapse;
			txtTax.Visible = !boolCollapse;
			lblPrincipal.Visible = !boolCollapse;
			lblCosts.Visible = !boolCollapse;
			lblTax.Visible = !boolCollapse;
			if (boolCollapse)
			{
				// make sure the interest field has all of the values from the other if the user had it in there first
				dblTemp = FCConvert.ToDouble(FCConvert.ToDecimal(txtPrincipal.Text) + FCConvert.ToDecimal(txtCosts.Text) + FCConvert.ToDecimal(txtTax.Text));
				txtPrincipal.Text = "0.00";
				txtCosts.Text = "0.00";
				txtTax.Text = "0.00";
				txtInterest.Text = Strings.Format(FCConvert.ToDouble(txtInterest.Text) + dblTemp, "#,##0.00");
				lblInterest.Text = "Amount";
			}
			else
			{
				lblInterest.Text = "Interest";
			}
		}

		private void SGRID_ClickEvent(object sender, System.EventArgs e)
		{
			// this will set the year information to the year being selected
			int cRow;
			int intCT;
			SetService_2("S");
			if (SGRID.RowOutlineLevel(SGRID.Row) == 1)
			{
				// .Col = lngGRIDColBillNumber And
				// check to make sure that the service combo is correct
				for (cRow = 0; cRow <= cmbService.Items.Count - 1; cRow++)
				{
					if (Strings.Left(cmbService.Items[cRow].ToString(), 1) == "S")
					{
						cmbService.SelectedIndex = cRow;
						modUTFunctions.FillUTBillNumber_8(false, false);
						break;
					}
				}
				cmbBillNumber.SelectedIndex = cmbBillNumber.Items.Count - 1;
				for (cRow = 0; cRow <= cmbBillNumber.Items.Count - 1; cRow++)
				{
					if (Strings.Trim(cmbBillNumber.Items[cRow].ToString()) == Strings.Trim(SGRID.TextMatrix(SGRID.Row, lngGRIDColBillNumber)))
					{
						cmbBillNumber.SelectedIndex = cRow;
						SetService_2("S");
						break;
					}
				}
			}
			else if (SGRID.RowOutlineLevel(SGRID.Row) == 0)
			{
				//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
				//if ("Auto" == Strings.Trim(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString()))
				if ("Auto" == Strings.Trim(cmbBillNumber.Text))
				{
					cmbBillNumber_Click();
				}
				else
				{
					for (intCT = 0; intCT <= cmbBillNumber.Items.Count - 1; intCT++)
					{
						if ("Auto" == Strings.Trim(cmbBillNumber.Items[intCT].ToString()).Replace("*", ""))
						{
							cmbBillNumber.SelectedIndex = intCT;
							SetService_2("S");
							break;
						}
					}
				}
			}
			// If SGRID.RowOutlineLevel(SGRID.Row) = 1 Then ' SGRID.Col = lngGRIDColBillNumber And
			// cmbBillNumber.ListIndex = cmbBillNumber.ListCount - 1
			// For cRow = 0 To cmbBillNumber.ListCount - 1
			// If Trim$(cmbBillNumber.List(cRow)) = Trim$(SGRID.TextMatrix(SGRID.Row, lngGRIDColBillNumber)) Then
			// cmbBillNumber.ListIndex = cRow
			// Exit For
			// End If
			// Next
			// 
			// check to make sure that the service combo is correct
			// For cRow = 0 To cmbService.ListCount - 1
			// If Left(cmbService.List(cRow), 1) = "S" Then
			// cmbService.ListIndex = cRow
			// Exit For
			// End If
			// Next
			// ElseIf SGRID.RowOutlineLevel(SGRID.Row) = 0 Then
			// If "Auto" = Trim$(cmbBillNumber.List(cmbBillNumber.ListIndex)) Then
			// cmbBillNumber_Click
			// Else
			// For intCT = 0 To cmbBillNumber.ListCount - 1
			// If "Auto" = Replace(Trim$(cmbBillNumber.List(intCT)), "*", "") Then
			// cmbBillNumber.ListIndex = intCT
			// Exit For
			// End If
			// Next
			// End If
			// End If
		}

		private void SGRID_Collapsed()
		{
			// this sub occurs when a gridline is collapsed or expanded
			// it will change the data in the main rows depending apon is status
			int temp;
			int counter;
			int rows = 0;
			int height1 = 0;
			bool DeptFlag = false;
			bool DivisionFlag = false;
			int cRow;
			int lngTempRow;
			// this will set the year information to the year being selected
			// lngTempRow = .MouseRow
			// If lngTempRow > 0 Then
			// If .RowOutlineLevel(lngTempRow) = 1 Then
			// For cRow = 0 To cmbBillNumber.ListCount - 1   'set the combo box
			// If Trim$(cmbBillNumber.List(cRow)) = Trim$(.TextMatrix(lngTempRow, lngGRIDColBillNumber)) Then
			// cmbBillNumber.ListIndex = cRow
			// SetService "S"
			// Exit For
			// End If
			// Next
			// End If
			// End If
			if (CollapseFlag == false)
			{
				for (counter = 1; counter <= SGRID.Rows - 1; counter++)
				{
					if (SGRID.RowOutlineLevel(counter) == 0)
					{
						if (SGRID.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							DeptFlag = true;
						}
						else
						{
							rows += 1;
							DeptFlag = false;
						}
					}
					else if (SGRID.RowOutlineLevel(counter) == 1)
					{
						if (DeptFlag == true)
						{
							// do nothing
						}
						else if (SGRID.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							DivisionFlag = true;
							// this function will show the totals on the header line and
							// put the original bill info on the empty line below the total line (hidden)
							// this means that this header row is closed and needs to show the totals on its line
							// MAL@20071105
							// If .TextMatrix(counter, lngGRIDColRef) = "Original" Or Left(.TextMatrix(counter, lngGRIDColRef), 4) = Left(strSupplemntalBillString, 4) Then
							if (SGRID.TextMatrix(counter, lngGRIDColRef) == "Original" || Strings.Left(SGRID.TextMatrix(counter, lngGRIDColRef), 3) == Strings.Left(strDemandBillString, 3) || Strings.Left(SGRID.TextMatrix(counter, lngGRIDColRef), 4) == Strings.Left(strSupplemntalBillString, 4))
							{
								SwapRows_8(counter, NextSpacerLine_6(counter, false), SGRID);
							}
						}
						else
						{
							rows += 1;
							DivisionFlag = false;
							// this function will show the original bill info on the header line and
							// put the totals on the empty line below the total line (hidden)
							// this means that that total row is showing so the totals must be on the total line not the header line
							// MAL@20071106
							// If .TextMatrix(counter, lngGRIDColRef) <> "Original" And Left(.TextMatrix(counter, lngGRIDColRef), 4) <> Left(strSupplemntalBillString, 4) Then
							if (SGRID.TextMatrix(counter, lngGRIDColRef) != "Original" && Strings.Left(SGRID.TextMatrix(counter, lngGRIDColRef), 4) != Strings.Left(strSupplemntalBillString, 4) && Strings.Left(SGRID.TextMatrix(counter, lngGRIDColRef), 3) != Strings.Left(strDemandBillString, 3))
							{
								SwapRows_8(counter, NextSpacerLine_6(counter, false), SGRID);
							}
							// this will swap the two rows
						}
					}
					else
					{
						if (DeptFlag == true || DivisionFlag == true)
						{
							// do nothing
						}
						else
						{
							rows += 1;
						}
					}
				}
				height1 = (rows + 1) * SGRID.RowHeight(0);
				Adjust_GRID_Height_6(height1, false);
			}
		}

		private void SGRID_DblClick(object sender, System.EventArgs e)
		{
			int cRow = 0;
			int mRow = 0;
			int mCol = 0;
			clsDRWrapper rsBill = new clsDRWrapper();
			bool blnHasBillNum;
			mRow = SGRID.MouseRow;
			mCol = SGRID.MouseCol;
			cRow = SGRID.Row;
			if (mRow > 0 && mCol > 0)
			{
				if (SGRID.RowOutlineLevel(cRow) == 1)
				{
					if (SGRID.Col == lngGRIDColTotal || SGRID.Col == lngGRIDColPending)
					{
						// do nothing
					}
					else if (SGRID.Col == lngGRIDColBillNumber)
					{
						// set the year
						for (cRow = 0; cRow <= cmbBillNumber.Items.Count - 1; cRow++)
						{
							if (Strings.Trim(cmbBillNumber.Items[cRow].ToString()) == Strings.Trim(SGRID.TextMatrix(SGRID.Row, SGRID.Col)))
							{
								cmbBillNumber.SelectedIndex = cRow;
								SetService_2("S");
								break;
							}
						}
					}
					// expand the rows
					if (SGRID.IsCollapsed(SGRID.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						SGRID.IsCollapsed(SGRID.Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
					}
					else
					{
						SGRID.IsCollapsed(SGRID.Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
					}
				}
				else if (SGRID.RowOutlineLevel(cRow) == 2 && fraPayment.Visible == true && (SGRID.TextMatrix(cRow, lngGRIDColRef) != "CHGINT" && SGRID.TextMatrix(cRow, lngGRIDColRef) != "EARNINT" && SGRID.TextMatrix(cRow, lngGRIDColRef) != "CNVRSN"))
				{
					// reverse the payment on this line
					// MAL@20080616: Add check that user is not trying to reverse a payment from a liened bill
					// Tracker Reference: 14267
					if (SGRID.TextMatrix(LastParentRow_6(cRow, false), lngGridColBillKey) != "")
					{
						if (!IsLienedBill(FCConvert.ToInt32(SGRID.TextMatrix(LastParentRow_6(cRow, false), lngGridColBillKey)), "S"))
						{
							if (SGRID.TextMatrix(cRow, lngGRIDColRef) != "Billed To:")
							{
								if (Strings.Trim(SGRID.TextMatrix(cRow, lngGRIDColRef)) != "Total" && Strings.Trim(SGRID.TextMatrix(cRow, lngGRIDColRef)) != "CURINT")
								{
									if (Strings.Trim(SGRID.TextMatrix(cRow, lngGRIDColPaymentCode)) != "D")
									{
										if (MessageBox.Show("Are you sure that you would like to reverse this payment?", "Reverse Payment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
										{
											modUTFunctions.Statics.blnCreatedReverseInterest = false;
											// MAL@20080228: Reset the value
											modUTFunctions.CreateUTOppositionLine_24(cRow, false, true);
											// MAL@20070920: Added True for Reversal
										}
									}
									else
									{
										// reversing a discount
										if (MessageBox.Show("Are you sure that you would like to reverse this discount?", "Reverse Payment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
										{
											modUTFunctions.Statics.blnCreatedReverseInterest = false;
											// MAL@20080228: Reset the value
											modUTFunctions.CreateUTOppositionLine_24(cRow, false, true);
											// MAL@20070920: Added True for Reversal
										}
									}
								}
								else
								{
									if (boolDisableInsert)
									{
										// kk03132015  trocrs-36  Add option to disable automatically filling in payment amount
										return;
									}
									//CreateUTOppositionLine cRow, False, True             'MAL@20070920: Added True for Reversal
									modUTFunctions.CreateUTOppositionLine_6(cRow, false);
									// kk12152017 trocr-497  User clicked on the Total or CURINT line. NOT a reversal
								}
							}
						}
					}
					else
					{
						if (boolDisableInsert)
						{
							// kk03132015  trocrs-36  Add option to disable automatically filling in payment amount
							return;
						}
						if (FCConvert.ToInt32(SGRID.TextMatrix(LastParentRow_6(cRow, false), lngGRIDColPaymentKey)) < 0)
						{
							if (SGRID.TextMatrix(cRow, lngGRIDColRef) != "Billed To:")
							{
								if (Strings.Trim(SGRID.TextMatrix(cRow, lngGRIDColRef)) != "Total" && Strings.Trim(SGRID.TextMatrix(cRow, lngGRIDColRef)) != "CURINT")
								{
									if (MessageBox.Show("Are you sure that you would like to reverse this payment?", "Reverse Payment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
									{
										modUTFunctions.Statics.blnCreatedReverseInterest = false;
										//MAL@20080228: Reset the value
										modUTFunctions.CreateUTOppositionLine_8(cRow, false, true);
										//MAL@20070920: Added True for Reversal
									}
								}
							}
						}
						else
						{
							modUTFunctions.CreateUTOppositionLine_8(cRow, false, false);
						}
					}
				}
				else if (SGRID.RowOutlineLevel(cRow) == 0)
				{
					// this is the total account line
					if (boolDisableInsert)
					{
						// kk03132015  trocrs-36  Add option to disable automatically filling in payment amount
						return;
					}
					//kk10272016 trout-1225  Add code for reversing a payment on a lien
					modUTFunctions.CreateUTOppositionLine_24(cRow, false, false);
				}
			}
		}

		private void SGRID_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void SGRID_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 32)
			{
				// spacebar
				keyAscii = 0;
				SGRID_DblClick(sender, EventArgs.Empty);
			}
			else if (keyAscii == 13 && SGRID.RowOutlineLevel(SGRID.Row) == 1)
			{
				keyAscii = 0;
				SGRID_ClickEvent(sender, EventArgs.Empty);
			}
		}

		private void SGRID_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// catch the right click to show the rate information about that year
			int lngMRow = 0;
			int lngBK = 0;
			lngMRow = SGRID.MouseRow;
			//kk12122017 trout - 706  Always set this. We'll use it in mnuFileDischarge
			strCurrentGridSelect = "S";
			if (e.Button == MouseButtons.Right && lngMRow >= 0)
			{
				// right click
				if (SGRID.RowOutlineLevel(lngMRow) == 1)
				{
					SGRID.Select(lngMRow, 1);
					// this will force the information to be correct by selecting the correct row before looking at the labels
					if (Conversion.Val(SGRID.TextMatrix(lngMRow, lngGRIDColPaymentKey)) >= 0)
					{
						lngBK = FCConvert.ToInt32(Math.Round(Conversion.Val(SGRID.TextMatrix(lngMRow, lngGRIDColPaymentKey))));
					}
					else
					{
						if (SGRID.IsCollapsed(lngMRow) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rsCL.OpenRecordset("SELECT ID FROM Bill WHERE ID = " + FCConvert.ToString(Conversion.Val(SGRID.TextMatrix(lngMRow, lngGRIDColPaymentKey)) * -1), modExtraModules.strUTDatabase);
						}
						else
						{
							rsCL.OpenRecordset("SELECT ID FROM Bill WHERE SLienRecordNumber = " + FCConvert.ToString(Conversion.Val(SGRID.TextMatrix(lngMRow, lngGRIDColPaymentKey)) * -1), modExtraModules.strUTDatabase);
						}
						if (!rsCL.EndOfFile())
						{
							lngBK = rsCL.Get_Fields_Int32("ID");
						}
						else
						{
							lngBK = 0;
						}
					}
					rsCL.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE Bill = " + FCConvert.ToString(lngBK), modExtraModules.strUTDatabase);
					if (rsCL.EndOfFile() != true && rsCL.BeginningOfFile() != true)
					{
						ShowRateInfo_168(lngMRow, 1, rsCL, SGRID, false);
					}
				}
				else if (SGRID.RowOutlineLevel(lngMRow) == 2)
				{
					ShowPaymentInfo(lngMRow, SGRID.MouseCol, SGRID);
				}
				//kk12122017 trout-706 strCurrentGridSelect = "S";
			}
		}

        //private void SGRID_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
        private void SGRID_SetToolTips(int lngMR, int lngMC)
        {
            // as the mouse moves over the grid, the tooltip will change depending on which cell the pointer is currently in
            string strTemp = "";
            //int lngMR = SGRID.GetFlexRowIndex(e.RowIndex);
            //int lngMC = SGRID.GetFlexColIndex(e.ColumnIndex);
            if (lngMR == 0)
            {
                if (lngMC == lngGRIDCOLYear || lngMC == lngGRIDColBillNumber)
                {
                    // Year
                    strTemp = "";
                    //SGRID.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColDate)
                {
                    // Date
                    strTemp = "Recorded Date";
                    //SGRID.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColRef)
                {
                    // Reference
                    strTemp = "";
                    //SGRID.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColService)
                {
                    // Period
                    strTemp = "";
                    //SGRID.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColPaymentCode)
                {
                    // Code
                    strTemp += " 3 - 30 Day Notice Fee" + ", ";
                    strTemp += " A - Abatement" + ", ";
                    strTemp += " C - Correction" + ", ";
                    strTemp += " D - Discount" + ", ";
                    strTemp += " I - Interest" + ", ";
                    strTemp += " P - Payment" + ", ";
                    strTemp += " R - Refunded Abatement" + ", ";
                    strTemp += " S - Supplemental" + ", ";
                    strTemp += " U - Tax Club" + ", ";
                    strTemp += " Y - PrePayment";
                    //SGRID.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColPTC)
                {
                    // Principal, Tax and Costs
                    strTemp = "Principal + Tax + Costs";
                    //SGRID.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColPrincipal)
                {
                    // Prin
                    strTemp = "";
                    //SGRID.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColInterest || lngMC == lngGRIDColTax)
                {
                    // Int
                    strTemp = "";
                    //SGRID.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColCosts)
                {
                    // Costs
                    strTemp = "";
                    //SGRID.ToolTipText = strTemp;
                }
                else if (lngMC == lngGRIDColTotal)
                {
                    // Total
                    strTemp = "";
                    //SGRID.ToolTipText = strTemp;
                }
                else
                {
                    strTemp = "";
                    //SGRID.ToolTipText = strTemp;
                }
                SGRID.Columns[lngMC - 1].HeaderCell.ToolTipText = strTemp;
            }
            else //if(SGRID.IsValidCell(e.ColumnIndex, e.RowIndex))
            {
                //lngMR = SGRID.MouseRow;
                DataGridViewCell cell = SGRID[lngMC - 1, lngMR - 1];
                if (lngMR > 0)
                {
                    if (SGRID.TextMatrix(lngMR, lngGRIDColLineCode) != "" && SGRID.TextMatrix(lngMR, lngGRIDColLineCode) != "=")
                    {
                        if (SGRID.TextMatrix(lngMR, lngGRIDColRef) != "CURINT" && SGRID.TextMatrix(lngMR, lngGRIDColRef) != "EARNINT")
                        {
                            if (SGRID.RowOutlineLevel(lngMR) == 1 && lngMR == 1)
                            {
                                // MAL@20080602: Add check for first column and then show tooltip with rate ID description
                                // Tracker Reference: 11820
                                if (lngMC == lngGRIDColBillNumber && SGRID.TextMatrix(lngMR, lngGRIDColBillNumber) != "")
                                {
                                    strTemp = GetBillDescription(SGRID.TextMatrix(lngMR, lngGRIDColBillNumber), SGRID.TextMatrix(lngMR, lngGRIDColDate), SGRID.TextMatrix(lngMR, lngGRIDColRef), "S");
                                    cell.ToolTipText = strTemp;
                                }
                                else
                                {
                                    cell.ToolTipText = "Right-Click for Rate information and totals.";
                                }
                            }
                            else if (SGRID.RowOutlineLevel(lngMR) == 2)
                            {
                                cell.ToolTipText = "Right-Click for Payment information.";
                            }
                            else
                            {
                                // MAL@20080602: Add check for first column and then show tooltip with rate ID description
                                // Tracker Reference: 11820
                                if (lngMC == lngGRIDColBillNumber && SGRID.TextMatrix(lngMR, lngGRIDColBillNumber) != "")
                                {
                                    strTemp = GetBillDescription(SGRID.TextMatrix(lngMR, lngGRIDColBillNumber), SGRID.TextMatrix(lngMR, lngGRIDColDate), SGRID.TextMatrix(lngMR, lngGRIDColRef), "S");
                                    cell.ToolTipText = strTemp;
                                }
                                // .ToolTipText = ""
                            }
                        }
                        else
                        {
                            cell.ToolTipText = "";
                        }
                    }
                    else
                    {
                        // MAL@20080602: Add check for first column and then show tooltip with rate ID description
                        // Tracker Reference: 11820
                        if (lngMC == lngGRIDColBillNumber && SGRID.TextMatrix(lngMR, lngGRIDColBillNumber) != "")
                        {
                            strTemp = GetBillDescription(SGRID.TextMatrix(lngMR, lngGRIDColBillNumber), SGRID.TextMatrix(lngMR, lngGRIDColDate), SGRID.TextMatrix(lngMR, lngGRIDColRef), "S");
                            cell.ToolTipText = strTemp;
                        }
                        // .ToolTipText = ""
                    }
                }
                else
                {
                    cell.ToolTipText = "";
                }
            }
        }

		private void SGRID_RowColChange(object sender, System.EventArgs e)
		{
			// this will change the labels depending apon which line is click
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (SGRID.RowOutlineLevel(SGRID.Row) == 1)
			{
				rsTemp.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE BillNumber = " + SGRID.TextMatrix(SGRID.Row, lngGRIDColBillNumber) + " AND AccountKey = " + FCConvert.ToString(CurrentAccountKey), modExtraModules.strUTDatabase);
				if (rsTemp.EndOfFile() || rsTemp.BeginningOfFile())
				{
				}
				else
				{
					rsTemp.MoveFirst();
					// TODO: Field [Bill] not found!! (maybe it is an alias?)
					FillLabels_2(FCConvert.ToInt32(rsTemp.Get_Fields("Bill")));
				}
			}
			rsTemp.Reset();
		}

		public short FindPaymentType(string strService = "")
		{
			short FindPaymentType = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will check out all the options to figure out what type of payment should be made
				// Returns 0 for water and 1 for sewer
				clsDRWrapper rsTest = new clsDRWrapper();
				if (Strings.Trim(strService) != "")
				{
					// use the passed in value
				}
				else
				{
					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
					//strService = Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1);
					strService = Strings.Left(cmbService.Text, 1);
				}
				if (strService == "W")
				{
					FindPaymentType = 0;
				}
				else if (strService == "S")
				{
					FindPaymentType = 1;
				}
				else if (strService == "B")
				{
					if (modUTFunctions.Statics.gboolPayWaterFirst)
					{
						// the user wants to pay the water first
						FindPaymentType = 0;
						// default to water
						rsTest.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE WPrinOwed > 0 AND Service <> 'S' ORDER BY BillNumber", modExtraModules.strUTDatabase);
						if (!rsTest.EndOfFile())
						{
							// there are bills, now check to see if any are outstanding
							rsTest.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE WPrinOwed - WPrinPaid > 0 AND Service <> 'S' ORDER BY BillNumber", modExtraModules.strUTDatabase);
							if (!rsTest.EndOfFile())
							{
								// this will find any outstanding bills
							}
							else
							{
								// check for water bills
								rsTest.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE WPrinOwed > 0 AND Service = 'W' ORDER BY BillNumber", modExtraModules.strUTDatabase);
								if (!rsTest.EndOfFile())
								{
									// there is a bill in the water list so return the water code
									FindPaymentType = 1;
								}
							}
						}
						else
						{
							// no water bills
							// check for sewer bills
							rsTest.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE SPrinOwed > 0 AND Service = 'S' ORDER BY BillNumber", modExtraModules.strUTDatabase);
							if (!rsTest.EndOfFile())
							{
								// there is a bill in the sewer list so return sewer
								FindPaymentType = 1;
							}
						}
					}
					else
					{
						FindPaymentType = 1;
						// default to sewer
						rsTest.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE SPrinOwed > 0 AND Service <> 'W' ORDER BY BillNumber", modExtraModules.strUTDatabase);
						if (!rsTest.EndOfFile())
						{
							// there are bills, now check to see if any are outstanding
							rsTest.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE SPrinOwed  - SPrinPaid > 0 AND Service <> 'W' ORDER BY BillNumber", modExtraModules.strUTDatabase);
							if (!rsTest.EndOfFile())
							{
								// this will find any outstanding bills
							}
							else
							{
								// check for water bills
								rsTest.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE WPrinOwed > 0 AND Service = 'W' ORDER BY BillNumber", modExtraModules.strUTDatabase);
								if (!rsTest.EndOfFile())
								{
									// there is a bill in the water list so return the water code
									FindPaymentType = 0;
								}
							}
						}
						else
						{
							// no water bills
							// check for water bills
							rsTest.OpenRecordset("SELECT * FROM (" + modUTFunctions.Statics.strUTCurrentAccountBills + ") AS qTmp WHERE WPrinOwed > 0 AND Service = 'W' ORDER BY BillNumber", modExtraModules.strUTDatabase);
							if (!rsTest.EndOfFile())
							{
								// there is a bill in the water list so return the water code
								FindPaymentType = 0;
							}
						}
					}
				}
				return FindPaymentType;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Payment Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FindPaymentType;
		}

		public int FindPaymentCode_2(string strService, int lngBillKey)
		{
			return FindPaymentCode(ref strService, ref lngBillKey);
		}

		public int FindPaymentCode(ref string strService, ref int lngBillKey)
		{
			int FindPaymentCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the correct type for the payment currently being made
				// 93 - Water Payment
				// 94 - Sewer Payment
				// 95 - Water Lien Payment
				// 96 - Sewer Lien Payment
				bool boolWater;
				bool boolLien;
				clsDRWrapper rsBill = new clsDRWrapper();
				// find out if this is a water or sewer payment
				switch (FindPaymentType(strService))
				{
					case 0:
						{
							boolWater = true;
							FindPaymentCode = 93;
							strService = "W";
							break;
						}
					case 1:
						{
							boolWater = false;
							FindPaymentCode = 94;
							strService = "S";
							break;
						}
					default:
						{
							return FindPaymentCode;
						}
				}
				//end switch
				// find out if the bill is a liened bill
				rsBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBillKey), modExtraModules.strUTDatabase);
				if (!rsBill.EndOfFile())
				{
					if (FCConvert.ToInt32(rsBill.Get_Fields(strService + "LienRecordNumber")) != 0)
					{
						boolLien = true;
						FindPaymentCode += 2;
					}
					else
					{
						boolLien = false;
					}
				}
				else
				{
					return FindPaymentCode;
				}
				return FindPaymentCode;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Payment Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FindPaymentCode;
		}

		private void SetService_2(string strService)
		{
			SetService(ref strService);
		}

		private void SetService(ref string strService)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				for (intCT = 0; intCT <= cmbService.Items.Count - 1; intCT++)
				{
					if (Strings.UCase(strService) == Strings.UCase(Strings.Left(cmbService.Items[intCT].ToString(), 1)))
					{
						boolDoNotSetService = true;
						cmbService.SelectedIndex = intCT;
						boolDoNotSetService = false;
						break;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Setting Service", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private double GetAmountPaidToOtherGrid(ref bool boolWater, ref double dblTotAmount)
		{
			double GetAmountPaidToOtherGrid = 0;
			// this function will return the amount of money to be paid to the other grid after paying off the total from the current grid
			// the boolWater variable is the grid that is being paid off first, and the dblTotAmount is the amount of the Total payment
			if (boolWater)
			{
				GetAmountPaidToOtherGrid = dblTotAmount - FCConvert.ToDouble(WGRID.TextMatrix(WGRID.Rows - 1, lngGRIDColTotal));
			}
			else
			{
				GetAmountPaidToOtherGrid = dblTotAmount - FCConvert.ToDouble(SGRID.TextMatrix(SGRID.Rows - 1, lngGRIDColTotal));
			}
			return GetAmountPaidToOtherGrid;
		}

		private void SetHeadingLabels()
		{
			// this routine will move and resize the heading labels above each of the
			lblWaterHeading.Visible = WGRID.Visible;
			lblWaterHeading.Left = WGRID.Left;
			lblWaterHeading.Width = WGRID.Width;
			lblWaterHeading.Top = WGRID.Top - lblWaterHeading.Height;
			lblSewerHeading.Visible = SGRID.Visible;
			lblSewerHeading.Left = SGRID.Left;
			lblSewerHeading.Width = SGRID.Width;
			lblSewerHeading.Top = SGRID.Top - lblSewerHeading.Height;
		}

		private void AlignTextBoxesAndCombos()
		{
			int lngTop;
			int lngHt;
			lngTop = cmbBillNumber.Top + 12;
			lngHt = cmbBillNumber.Height - 50;
			txtTransactionDate.Top = lngTop;
			txtReference.Top = lngTop;
			txtCash.Top = lngTop;
			txtCD.Top = lngTop;
			txtAcctNumber.Top = lngTop;
			txtPrincipal.Top = lngTop;
			txtTax.Top = lngTop;
			txtInterest.Top = lngTop;
			txtCosts.Top = lngTop;
			txtTransactionDate.Height = lngHt;
			txtReference.Height = lngHt;
			txtCash.Height = lngHt;
			txtCD.Height = lngHt;
			txtAcctNumber.Height = lngHt;
			txtPrincipal.Height = lngHt;
			txtTax.Height = lngHt;
			txtInterest.Height = lngHt;
			txtCosts.Height = lngHt;
			txtComments.Height = lngHt;
		}

		private string GetRefundedAbabtementAccount_26(bool boolLien, bool boolWater, string strYear)
		{
			return GetRefundedAbabtementAccount(ref boolLien, ref boolWater, ref strYear);
		}

		private string GetRefundedAbabtementAccount(ref bool boolLien, ref bool boolWater, ref string strYear)
		{
			string GetRefundedAbabtementAccount = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsAcct = new clsDRWrapper();
				if (boolLien)
				{
					if (boolWater)
					{
						rsAcct.OpenRecordset("SELECT Account1, Year1 FROM Type WHERE TypeCode = 96", modExtraModules.strCRDatabase);
					}
					else
					{
						rsAcct.OpenRecordset("SELECT Account1, Year1 FROM Type WHERE TypeCode = 95", modExtraModules.strCRDatabase);
					}
				}
				else
				{
					if (boolWater)
					{
						rsAcct.OpenRecordset("SELECT Account1, Year1 FROM Type WHERE TypeCode = 93", modExtraModules.strCRDatabase);
					}
					else
					{
						rsAcct.OpenRecordset("SELECT Account1, Year1 FROM Type WHERE TypeCode = 94", modExtraModules.strCRDatabase);
					}
				}
				if (rsAcct.EndOfFile())
				{
					GetRefundedAbabtementAccount = "M NO ENTRY";
				}
				else
				{
					if (FCConvert.ToBoolean(rsAcct.Get_Fields_Boolean("Year1")) && Strings.Left(FCConvert.ToString(rsAcct.Get_Fields_String("Account1")), 1) == "G")
					{
						GetRefundedAbabtementAccount = Strings.Left(FCConvert.ToString(rsAcct.Get_Fields_String("Account1")), FCConvert.ToString(rsAcct.Get_Fields_String("Account1")).Length - 2) + strYear;
					}
					else
					{
						if (Strings.Left(FCConvert.ToString(rsAcct.Get_Fields_String("Account1")), 1) == "M" && Strings.InStr(2, "XX", FCConvert.ToString(rsAcct.Get_Fields_String("Account1")), CompareConstants.vbBinaryCompare) != 0)
						{
							GetRefundedAbabtementAccount = FCConvert.ToString(rsAcct.Get_Fields_String("Account1")).Replace("XX", strYear);
						}
						else
						{
							GetRefundedAbabtementAccount = FCConvert.ToString(rsAcct.Get_Fields_String("Account1"));
						}
					}
				}
				return GetRefundedAbabtementAccount;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " _ " + Information.Err(ex).Description + ".", "Error Finding Refunded Abatement Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetRefundedAbabtementAccount;
		}

		private void SetLienVariables()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDefaults = new clsDRWrapper();
				rsDefaults.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
				if (!rsDefaults.EndOfFile())
				{
					boolCheckOnlyOldestBill = FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("OnlyCheckOldestBillForLien"));
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " _ " + Information.Err(ex).Description + ".", "Error Setting Lien Variable", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void HidePreview()
		{
			// close the preview frame
			vsPreview.Visible = false;
			vsPreview.Rows = 1;
			switch (lngPreviewStyle)
			{
				case 0:
					{
						// water
						WGRID.Visible = true;
						SGRID.Visible = false;
						lblWaterHeading.Visible = true;
						break;
					}
				case 2:
					{
						// both
						WGRID.Visible = true;
						lblWaterHeading.Visible = true;
						SGRID.Visible = true;
						lblSewerHeading.Visible = true;
						break;
					}
				case 1:
					{
						// sewer
						WGRID.Visible = false;
						SGRID.Visible = true;
						lblSewerHeading.Visible = true;
						break;
					}
			}
			//end switch
			cmdPaymentPreview.Text = "Preview";
		}

		private string OverrideServiceCodeForAccount(ref int lngAccountKey)
		{
			string OverrideServiceCodeForAccount = "";
			clsDRWrapper rsInfo = new clsDRWrapper();
			bool blnSewer = false;
			bool blnWater = false;
			if (modUTFunctions.Statics.TownService == "W" || modUTFunctions.Statics.TownService == "S")
			{
				OverrideServiceCodeForAccount = "";
			}
			else
			{
				rsInfo.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), "TWUT0000.vb1");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					blnSewer = false;
					blnWater = false;
					do
					{
						if (FCConvert.ToString(rsInfo.Get_Fields_String("Service")) == "S")
						{
							blnSewer = true;
						}
						else if (rsInfo.Get_Fields_String("Service") == "W")
						{
							blnWater = true;
						}
						else if (rsInfo.Get_Fields_String("Service") == "B")
						{
							blnWater = true;
							blnSewer = true;
						}
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
					// MAL@20080610: Add check for outstanding bills before determining final available services
					// Tracker Reference: 14076
					if (!blnWater)
					{
						blnWBill = HasOutstandingBills(lngAccountKey, "W");
					}
					if (!blnWater)
					{
						blnSBill = HasOutstandingBills(lngAccountKey, "S");
					}
					if (blnWater && blnSewer)
					{
						OverrideServiceCodeForAccount = "B";
					}
					else if (blnWater)
					{
						OverrideServiceCodeForAccount = "W";
					}
					else if (blnSewer)
					{
						OverrideServiceCodeForAccount = "S";
					}
				}
			}
			return OverrideServiceCodeForAccount;
		}
		// vbPorter upgrade warning: strBillNumber As string	OnReadFCConvert.ToInt32(
		private string GetBillDescription(string strBillNumber, string strBillDate, string strRef, string strService)
		{
			string GetBillDescription = "";
			// Tracker Reference: 11820
			clsDRWrapper rsBill = new clsDRWrapper();
			clsDRWrapper rsLien = new clsDRWrapper();
			clsDRWrapper rsRate = new clsDRWrapper();
			int lngLien;
			// vbPorter upgrade warning: lngRate As int	OnWrite(string)
			int lngRate = 0;
			string strDesc = "";
			rsBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + FCConvert.ToString(modUTFunctions.Statics.lngCurrentAccountKeyUT), modExtraModules.strUTDatabase);
			if (rsBill.RecordCount() > 0)
			{
				rsBill.MoveFirst();
				// rsBill.FindFirstRecord "BillNumber", strBillNumber
				if (Information.IsNumeric(strBillNumber))
				{
					rsBill.FindFirstRecord("BillNumber", strBillNumber);
				}
				if (rsBill.NoMatch)
				{
					// Probably a Lien Bill Line - Get that information
					if (Strings.Right(Strings.Trim(strBillDate), 1) == "*")
					{
						lngRate = FCConvert.ToInt32(strBillNumber);
						rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRate), modExtraModules.strUTDatabase);
						if (rsRate.RecordCount() > 0)
						{
							strDesc = FCConvert.ToString(rsRate.Get_Fields_String("Description"));
						}
						else
						{
							strDesc = "";
						}
					}
					else
					{
						strDesc = "";
					}
				}
				else
				{
					lngRate = FCConvert.ToInt32(rsBill.Get_Fields_Int32("BillingRateKey"));
					rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRate), modExtraModules.strUTDatabase);
					if (rsRate.RecordCount() > 0)
					{
						strDesc = FCConvert.ToString(rsRate.Get_Fields_String("Description"));
					}
					else
					{
						strDesc = "";
					}
				}
			}
			else
			{
				strDesc = "";
			}
			GetBillDescription = strDesc;
			return GetBillDescription;
		}

		private bool HasOutstandingBills(int lngAccountKey, string strService)
		{
			bool HasOutstandingBills = false;
			// Tracker Reference: 14076
			bool blnResult = false;
			clsDRWrapper rsBill = new clsDRWrapper();
			clsDRWrapper rsTemp = new clsDRWrapper();
			double dblAmt = 0;
			double dblXInt = 0;
			rsBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
			if (rsBill.RecordCount() > 0)
			{
				rsBill.MoveFirst();
				while (!rsBill.EndOfFile())
				{
					rsTemp.OpenRecordset("SELECT * FROM Bill WHERE ID = " + rsBill.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
					dblAmt = modUTCalculations.CalculateAccountUT(rsTemp, DateTime.Today, ref dblXInt, FCConvert.CBool(strService == "W"));
					blnResult = (dblAmt != 0);
					// kk04232014 trout-1081  Make service available if bill < 0 also
					if (blnResult)
					{
						break;
					}
					rsBill.MoveNext();
				}
			}
			HasOutstandingBills = blnResult;
			return HasOutstandingBills;
		}
		// vbPorter upgrade warning: lngBillKey As int	OnWrite(string)
		private bool IsLienedBill(int lngBillKey, string strService)
		{
			bool IsLienedBill = false;
			// Tracker Reference: 14267
			bool blnResult = false;
			clsDRWrapper rsBill = new clsDRWrapper();
			rsBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBillKey), modExtraModules.strUTDatabase);
			if (rsBill.RecordCount() > 0)
			{
				blnResult = (FCConvert.ToInt32(rsBill.Get_Fields(strService + "LienRecordNumber")) > 0);
			}
			else
			{
				blnResult = false;
			}
			IsLienedBill = blnResult;
			return IsLienedBill;
		}
		// vbPorter upgrade warning: lngBillNumber As int	OnWriteFCConvert.ToDouble(
		private bool ServiceHasPendingPayment(int lngBillNumber, string strService)
		{
			bool ServiceHasPendingPayment = false;
			// Tracker Reference: 16111
			bool blnResult = false;
			clsDRWrapper rsPymt = new clsDRWrapper();
			rsPymt.OpenRecordset("SELECT * FROM PaymentRec WHERE AccountKey = " + FCConvert.ToString(modUTFunctions.Statics.lngCurrentAccountKeyUT) + " AND BillNumber = " + FCConvert.ToString(lngBillNumber) + " AND Service = '" + strService + "' AND ReceiptNumber = -1 AND Code <> 'I'", modExtraModules.strUTDatabase);
			blnResult = rsPymt.RecordCount() > 0;
			ServiceHasPendingPayment = blnResult;
			return ServiceHasPendingPayment;
		}

		private void FillGroupInfo()
		{
			string strTemp;
			clsDRWrapper rsTemp = new clsDRWrapper();
			int lngAccount = 0;
			double dblTotal = 0;
			double dblTotalBalance = 0;
			int intBasisBak = 0;
			double dblOPIntRateBak = 0;
			//Backup copy of Basis and OverPay Int Rate - the same globals are used in CL and UT
			//but the values might not be the same
			intBasisBak = modGlobal.Statics.gintBasis;
			dblOPIntRateBak = modExtraModules.Statics.dblOverPayRate;
			lngGroupID = 0;
			strTemp = "";
			if (lngGroupNumber != 0)
			{
				//rsTemp.OpenRecordset "SELECT * FROM ModuleAssociation INNER JOIN GroupMaster ON ModuleAssociation.GroupNumber = GroupMaster.ID WHERE GroupMaster.GroupNumber = " & lngGroupNumber & " AND NOT (Module = 'RE' AND Account = " & CurrentAccountRE & ") ORDER BY Module", strGNDatabase
				rsTemp.OpenRecordset("SELECT GroupMaster.ID, Module, Account FROM ModuleAssociation INNER JOIN GroupMaster ON ModuleAssociation.GroupNumber = GroupMaster.ID WHERE GroupMaster.GroupNumber = " + lngGroupNumber + " ORDER BY Module", "CentralData");
				if (!rsTemp.EndOfFile())
				{
					lngGroupID = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID"));
					while (!rsTemp.EndOfFile())
					{
						if (strTemp == "")
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							strTemp = "Group Balance (" + rsTemp.Get_Fields_String("Module") + " #" + rsTemp.Get_Fields("Account");
						}
						else
						{
							// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
							strTemp = strTemp + ", " + rsTemp.Get_Fields_String("Module") + " #" + rsTemp.Get_Fields("Account");
						}
						// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
						lngAccount = FCConvert.ToInt32(rsTemp.Get_Fields("Account"));
						if (FCConvert.ToString(rsTemp.Get_Fields_String("Module")) == "RE")
						{
							modCLCalculations.CLCalcSetup();
							dblTotal = modCLCalculations.CalculateAccountTotal(lngAccount, true);
						}
						else if (FCConvert.ToString(rsTemp.Get_Fields_String("Module")) == "PP")
						{
							modCLCalculations.CLCalcSetup();
							dblTotal = modCLCalculations.CalculateAccountTotal(lngAccount, false);
						}
						else if (FCConvert.ToString(rsTemp.Get_Fields_String("Module")) == "UT")
						{
							modGlobal.Statics.gintBasis = intBasisBak;
							modExtraModules.Statics.dblOverPayRate = dblOPIntRateBak;
							dblTotal = modUTCalculations.CalculateAccountUTTotal(modUTFunctions.GetAccountKeyUT_2(lngAccount), true) + modUTCalculations.CalculateAccountUTTotal(modUTFunctions.GetAccountKeyUT_2(lngAccount), false);
						}
						dblTotalBalance = dblTotalBalance + dblTotal;
						rsTemp.MoveNext();
					}
				}
			}
			if (strTemp != "")
			{
				strTemp = strTemp + ") is " + Strings.Format(dblTotalBalance, "#,##0.00");
				lblGrpInfo.ToolTipText = strTemp;
				lblGrpInfo.Visible = true;
			}
			else
			{
				lblGrpInfo.Text = "";
				lblGrpInfo.Visible = false;
			}
			//Restore the original Basis and OverPay Int Rate
			modGlobal.Statics.gintBasis = intBasisBak;
			modExtraModules.Statics.dblOverPayRate = dblOPIntRateBak;
		}

		private void FillACHInfo()
		{
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp;

			lblACHInfo.ToolTipText = "";
			lblACHInfo.Visible = false;
			rsTemp.OpenRecordset("SELECT * FROM tblAcctACH WHERE AccountKey = " + modUTFunctions.Statics.lngCurrentAccountKeyUT + " AND ACHActive = 1", modExtraModules.strUTDatabase);
			while (!rsTemp.EndOfFile())
			{
				if (rsTemp.Get_Fields_Boolean("ACHActive"))
				{
					if (rsTemp.Get_Fields_Boolean("ACHPreNote"))
					{
						strTemp = "AutoPay is in PreNote status";
					}
					else
					{
						strTemp = "AutoPay is Active";
					}
					lblACHInfo.ToolTipText = strTemp;
					lblACHInfo.Visible = true;

					rsTemp.MoveNext();
				}
			}
		}
	}
}
