﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptMultiModuleJournalTotals : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/05/2006              *
		// ********************************************************
		clsDRWrapper rsFundInfo = new clsDRWrapper();
		clsDRWrapper rsExpInfo = new clsDRWrapper();
		clsDRWrapper rsRevInfo = new clsDRWrapper();
		clsDRWrapper rsGLInfo = new clsDRWrapper();
		clsDRWrapper rsCashInfo = new clsDRWrapper();
		clsDRWrapper rsEncInfo = new clsDRWrapper();
		clsDRWrapper rsDueFromInfo = new clsDRWrapper();
		clsDRWrapper rsDueToInfo = new clsDRWrapper();
		clsDRWrapper rsGeneralInfo = new clsDRWrapper();
		string strType = "";
		// VBto upgrade warning: curTotalExp As double	OnWrite(double, short)
		decimal curTotalExp;
		// VBto upgrade warning: curTotalRev As double	OnWrite(double, short)
		decimal curTotalRev;
		// VBto upgrade warning: curTotalGL As double	OnWrite(double, short)
		decimal curTotalGL;
		// VBto upgrade warning: curTotalCash As double	OnWrite(double, short)
		decimal curTotalCash;
		// VBto upgrade warning: curTotalDueTo As double	OnWrite(double, short)
		decimal curTotalDueTo;
		// VBto upgrade warning: curTotalDueFrom As double	OnWrite(double, short)
		decimal curTotalDueFrom;
		// VBto upgrade warning: curTotalEnc As double	OnWrite(double, short)
		decimal curTotalEnc;
		string MiscAccount = "";
		string PayableAccount = "";
		string PayrollAccount = "";
		string DueToAccount = "";
		string DueFromAccount = "";
		string EncAccount = "";
		string ExpAccount = "";
		string RevAccount = "";
		string strGLSQL = "";
		string strCashSQL = "";
		string strEncSQL = "";
		string strDueToSQL = "";
		string strDueFromSQL = "";

		public rptMultiModuleJournalTotals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static rptMultiModuleJournalTotals InstancePtr
		{
			get
			{
				return (rptMultiModuleJournalTotals)Sys.GetInstance(typeof(rptMultiModuleJournalTotals));
			}
		}

		protected rptMultiModuleJournalTotals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsGeneralInfo.Dispose();
				rsCashInfo.Dispose();
				rsDueFromInfo.Dispose();
				rsDueToInfo.Dispose();
				rsEncInfo.Dispose();
				rsExpInfo.Dispose();
				rsFundInfo.Dispose();
				rsGLInfo.Dispose();
				rsRevInfo.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			string strSQL;
			string strTest = "";
			string strJournalSQL = "";
			CheckNext:
			;
			eArgs.EOF = rsFundInfo.EndOfFile();
			if (eArgs.EOF)
			{
				return;
			}
			// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
			strSQL = "SELECT Department FROM DeptDivTitles WHERE Division = '" + modBudgetaryAccounting.Statics.strZeroDiv + "' AND Fund = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'";
			if (strType == "EN")
			{
				strJournalSQL = "SELECT ID FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period;
				rsExpInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'E' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSQL + ")", "TWBD0000.vb1");
				rsRevInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'R' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSQL + ")", "TWBD0000.vb1");
				// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
				rsGLInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'G' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strGLSQL, "TWBD0000.vb1");
				if (strCashSQL != "")
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsCashInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'G' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strCashSQL, "TWBD0000.vb1");
				}
				if (strEncSQL != "")
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsEncInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'G' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strEncSQL, "TWBD0000.vb1");
				}
				if (strDueToSQL != "")
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsDueToInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'G' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strDueToSQL, "TWBD0000.vb1");
				}
				if (strDueFromSQL != "")
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsDueFromInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM EncumbranceDetail WHERE left(Account, 1) = 'G' AND EncumbranceID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strDueFromSQL, "TWBD0000.vb1");
				}
			}
			else if (strType == "AP" || strType == "AC")
			{
				if (strType == "AP")
				{
					strJournalSQL = "SELECT ID FROM APJournal WHERE Status = 'P' AND CheckDate = '" + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).CheckDate + "' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period;
				}
				else
				{
					strJournalSQL = "SELECT ID FROM APJournal WHERE Status = 'P' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period;
				}
				rsExpInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal, SUM(Encumbrance) as EncumbranceTotal FROM APJournalDetail WHERE Encumbrance <> (Amount - Discount) AND left(Account, 1) = 'E' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSQL + ")", "TWBD0000.vb1");
				rsRevInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'R' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSQL + ")", "TWBD0000.vb1");
				// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
				rsGLInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'G' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strGLSQL, "TWBD0000.vb1");
				if (strCashSQL != "")
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsCashInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'G' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strCashSQL, "TWBD0000.vb1");
				}
				if (strEncSQL != "")
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsEncInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'G' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strEncSQL, "TWBD0000.vb1");
				}
				if (strDueToSQL != "")
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsDueToInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'G' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strDueToSQL, "TWBD0000.vb1");
				}
				if (strDueFromSQL != "")
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsDueFromInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal, SUM(Discount) as DiscountTotal FROM APJournalDetail WHERE left(Account, 1) = 'G' AND APJournalID IN (" + strJournalSQL + ") AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strDueFromSQL, "TWBD0000.vb1");
				}
			}
			else
			{
				rsExpInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE (Type <> 'P' OR RCB <> 'E') AND left(Account, 1) = 'E' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSQL + ")", "TWBD0000.vb1");
				rsRevInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'R' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Exp + "', 2))) IN (" + strSQL + ")", "TWBD0000.vb1");
				// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
				rsGLInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'G' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strGLSQL, "TWBD0000.vb1");
				if (strCashSQL != "")
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsCashInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'G' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strCashSQL, "TWBD0000.vb1");
				}
				if (strEncSQL != "")
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsEncInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'G' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strEncSQL, "TWBD0000.vb1");
				}
				if (strDueToSQL != "")
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsDueToInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'G' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strDueToSQL, "TWBD0000.vb1");
				}
				if (strDueFromSQL != "")
				{
					// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
					rsDueFromInfo.OpenRecordset("SELECT COUNT(Amount) as EntryCount, SUM(Amount) as JournalTotal FROM JournalEntries WHERE left(Account, 1) = 'G' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period + " AND substring(Account, 3, convert(int, left('" + modAccountTitle.Statics.Ledger + "', 2))) = '" + FCConvert.ToString(rsFundInfo.Get_Fields("Fund")) + "'" + strDueFromSQL, "TWBD0000.vb1");
				}
			}
			// TODO: Field [EntryCount] not found!! (maybe it is an alias?)
			// TODO: Field [EntryCount] not found!! (maybe it is an alias?)
			// TODO: Field [EntryCount] not found!! (maybe it is an alias?)
			// TODO: Field [EntryCount] not found!! (maybe it is an alias?)
			if (((rsExpInfo.Get_Fields("EntryCount"))) == 0 && ((rsRevInfo.Get_Fields("EntryCount"))) == 0 && ((rsGLInfo.Get_Fields("EntryCount"))) == 0 && ((rsCashInfo.Get_Fields("EntryCount"))) == 0)
			{
				if (strEncSQL != "")
				{
					// TODO: Field [EntryCount] not found!! (maybe it is an alias?)
					if (((rsEncInfo.Get_Fields("EntryCount"))) == 0)
					{
						goto CheckDueToFrom;
					}
					else
					{
						goto InfoFound;
					}
				}
				CheckDueToFrom:
				;
				if (strDueToSQL != "")
				{
					// if this is not empty then the records sets are filled and need to be tested
					// TODO: Field [EntryCount] not found!! (maybe it is an alias?)
					// TODO: Field [EntryCount] not found!! (maybe it is an alias?)
					if (rsDueToInfo.Get_Fields("EntryCount") == 0 && rsDueFromInfo.Get_Fields("EntryCount") == 0)
					{
						rsFundInfo.MoveNext();
						goto CheckNext;
					}
					else
					{
						// do nothing
					}
				}
				else
				{
					rsFundInfo.MoveNext();
					goto CheckNext;
				}
			}
			InfoFound:
			;
			// TODO: Check the table for the column [Fund] and replace with corresponding Get_Field method
			Field1.Text = FCConvert.ToString(rsFundInfo.Get_Fields("Fund"));
			if (strType == "AP" || strType == "AC")
			{
				// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
				// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
				// TODO: Field [EncumbranceTotal] not found!! (maybe it is an alias?)
				curTotalExp += FCConvert.ToDecimal(Conversion.Val(rsExpInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsExpInfo.Get_Fields("DiscountTotal")) - Conversion.Val(rsExpInfo.Get_Fields("EncumbranceTotal")));
				// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
				// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
				curTotalRev += FCConvert.ToDecimal(Conversion.Val(rsRevInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsRevInfo.Get_Fields("DiscountTotal")));
				// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
				// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
				curTotalGL += FCConvert.ToDecimal(Conversion.Val(rsGLInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsGLInfo.Get_Fields("DiscountTotal")));
				if (strCashSQL != "")
				{
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
					curTotalCash += FCConvert.ToDecimal(Conversion.Val(rsCashInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsCashInfo.Get_Fields("DiscountTotal")));
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
					Field8.Text = Strings.Format(Conversion.Val(rsCashInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsCashInfo.Get_Fields("DiscountTotal")), "#,##0.00");
				}
				else
				{
					Field8.Text = Strings.Format(0, "#,##0.00");
				}
				if (strDueToSQL != "")
				{
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
					curTotalDueTo += FCConvert.ToDecimal(Conversion.Val(rsDueToInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsDueToInfo.Get_Fields("DiscountTotal")));
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
					Field9.Text = Strings.Format(Conversion.Val(rsDueToInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsDueToInfo.Get_Fields("DiscountTotal")), "#,##0.00");
				}
				else
				{
					Field9.Text = Strings.Format(0, "#,##0.00");
				}
				if (strDueFromSQL != "")
				{
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
					curTotalDueFrom += FCConvert.ToDecimal(Conversion.Val(rsDueFromInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsDueFromInfo.Get_Fields("DiscountTotal")));
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
					Field10.Text = Strings.Format(Conversion.Val(rsDueFromInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsDueFromInfo.Get_Fields("DiscountTotal")), "#,##0.00");
				}
				else
				{
					Field10.Text = Strings.Format(0, "#,##0.00");
				}
				if (strEncSQL != "")
				{
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
					curTotalEnc += FCConvert.ToDecimal(Conversion.Val(rsEncInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsEncInfo.Get_Fields("DiscountTotal")));
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
					Field11.Text = Strings.Format(Conversion.Val(rsEncInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsEncInfo.Get_Fields("DiscountTotal")), "#,##0.00");
				}
				else
				{
					Field11.Text = Strings.Format(0, "#,##0.00");
				}
				// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
				// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
				// TODO: Field [EncumbranceTotal] not found!! (maybe it is an alias?)
				Field2.Text = Strings.Format(Conversion.Val(rsExpInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsExpInfo.Get_Fields("DiscountTotal")) - Conversion.Val(rsExpInfo.Get_Fields("EncumbranceTotal")), "#,##0.00");
				// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
				// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
				Field3.Text = Strings.Format(Conversion.Val(rsRevInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsRevInfo.Get_Fields("DiscountTotal")), "#,##0.00");
				// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
				// TODO: Field [DiscountTotal] not found!! (maybe it is an alias?)
				Field4.Text = Strings.Format(Conversion.Val(rsGLInfo.Get_Fields("JournalTotal")) - Conversion.Val(rsGLInfo.Get_Fields("DiscountTotal")), "#,##0.00");
			}
			else
			{
				// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
				curTotalExp += FCConvert.ToDecimal(Conversion.Val(rsExpInfo.Get_Fields("JournalTotal")));
				// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
				curTotalRev += FCConvert.ToDecimal(Conversion.Val(rsRevInfo.Get_Fields("JournalTotal")));
				// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
				curTotalGL += FCConvert.ToDecimal(Conversion.Val(rsGLInfo.Get_Fields("JournalTotal")));
				if (strCashSQL != "")
				{
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					curTotalCash += FCConvert.ToDecimal(Conversion.Val(rsCashInfo.Get_Fields("JournalTotal")));
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					Field8.Text = Strings.Format(Conversion.Val(rsCashInfo.Get_Fields("JournalTotal")), "#,##0.00");
				}
				else
				{
					Field8.Text = Strings.Format(0, "#,##0.00");
				}
				if (strEncSQL != "")
				{
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					curTotalEnc += FCConvert.ToDecimal(Conversion.Val(rsEncInfo.Get_Fields("JournalTotal")));
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					Field11.Text = Strings.Format(Conversion.Val(rsEncInfo.Get_Fields("JournalTotal")), "#,##0.00");
				}
				else
				{
					Field11.Text = Strings.Format(0, "#,##0.00");
				}
				if (strDueToSQL != "")
				{
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					curTotalDueTo += FCConvert.ToDecimal(Conversion.Val(rsDueToInfo.Get_Fields("JournalTotal")));
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					Field9.Text = Strings.Format(Conversion.Val(rsDueToInfo.Get_Fields("JournalTotal")), "#,##0.00");
				}
				else
				{
					Field9.Text = Strings.Format(0, "#,##0.00");
				}
				if (strDueFromSQL != "")
				{
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					curTotalDueFrom += FCConvert.ToDecimal(Conversion.Val(rsDueFromInfo.Get_Fields("JournalTotal")));
					// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
					Field10.Text = Strings.Format(Conversion.Val(rsDueFromInfo.Get_Fields("JournalTotal")), "#,##0.00");
				}
				else
				{
					Field10.Text = Strings.Format(0, "#,##0.00");
				}
				// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
				Field2.Text = Strings.Format(Conversion.Val(rsExpInfo.Get_Fields("JournalTotal")), "#,##0.00");
				// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
				Field3.Text = Strings.Format(Conversion.Val(rsRevInfo.Get_Fields("JournalTotal")), "#,##0.00");
				// TODO: Field [JournalTotal] not found!! (maybe it is an alias?)
				Field4.Text = Strings.Format(Conversion.Val(rsGLInfo.Get_Fields("JournalTotal")), "#,##0.00");
			}
			rsFundInfo.MoveNext();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			rptMultiModulePosting.InstancePtr.PageCounter = 0;
            rsFundInfo.DisposeOf();
            rsExpInfo.DisposeOf();
            rsRevInfo.DisposeOf();
            rsGLInfo.DisposeOf();
            rsCashInfo.DisposeOf();
            rsEncInfo.DisposeOf();
            rsDueFromInfo.DisposeOf();
            rsDueToInfo.DisposeOf();
            rsGeneralInfo.DisposeOf();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			rsFundInfo.OpenRecordset("SELECT DISTINCT Fund FROM LedgerTitles", "TWBD0000.vb1");
			rsGeneralInfo.OpenRecordset("SELECT * FROM StandardAccounts", "TWBD0000.vb1");
			rsGeneralInfo.FindFirstRecord("Code", "CM");
			if (rsGeneralInfo.NoMatch)
			{
				MiscAccount = "";
			}
			else
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				MiscAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			rsGeneralInfo.FindFirstRecord("Code", "CA");
			if (rsGeneralInfo.NoMatch)
			{
				PayableAccount = "";
			}
			else
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				PayableAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			rsGeneralInfo.FindFirstRecord("Code", "CP");
			if (rsGeneralInfo.NoMatch)
			{
				PayrollAccount = "";
			}
			else
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				PayrollAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			rsGeneralInfo.FindFirstRecord("Code", "EC");
			if (rsGeneralInfo.NoMatch)
			{
				ExpAccount = "";
			}
			else
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				ExpAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			rsGeneralInfo.FindFirstRecord("Code", "RC");
			if (rsGeneralInfo.NoMatch)
			{
				RevAccount = "";
			}
			else
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				RevAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			rsGeneralInfo.FindFirstRecord("Code", "EO");
			if (rsGeneralInfo.NoMatch)
			{
				EncAccount = "";
			}
			else
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				EncAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			rsGeneralInfo.FindFirstRecord("Code", "DT");
			if (rsGeneralInfo.NoMatch)
			{
				DueToAccount = "";
			}
			else
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				DueToAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			rsGeneralInfo.FindFirstRecord("Code", "DF");
			if (rsGeneralInfo.NoMatch)
			{
				DueFromAccount = "";
			}
			else
			{
				// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
				DueFromAccount = FCConvert.ToString(rsGeneralInfo.Get_Fields("Account"));
			}
			strGLSQL = "";
			strCashSQL = "";
			strEncSQL = "";
			strDueToSQL = "";
			strDueFromSQL = "";
			if (Conversion.Val(MiscAccount) != 0)
			{
				if (modAccountTitle.Statics.YearFlag)
				{
					strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + MiscAccount + "'";
					strCashSQL += " AND (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + MiscAccount + "'";
				}
				else
				{
					strGLSQL += " AND (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + MiscAccount + "' OR substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) <> '00')";
					strCashSQL += " AND ((substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + MiscAccount + "' AND substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) = '00')";
				}
			}
			if (Conversion.Val(PayableAccount) != 0)
			{
				if (modAccountTitle.Statics.YearFlag)
				{
					strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + PayableAccount + "'";
					if (strCashSQL == "")
					{
						strCashSQL += " AND (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayableAccount + "'";
					}
					else
					{
						strCashSQL += " OR substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayableAccount + "'";
					}
				}
				else
				{
					strGLSQL += " AND (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + PayableAccount + "' OR substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) <> '00')";
					if (strCashSQL == "")
					{
						strCashSQL += " AND ((substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayableAccount + "' AND substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) = '00')";
					}
					else
					{
						strCashSQL += " OR (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayableAccount + "' AND substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) = '00')";
					}
				}
			}
			if (Conversion.Val(PayrollAccount) != 0)
			{
				if (modAccountTitle.Statics.YearFlag)
				{
					strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + PayrollAccount + "'";
					if (strCashSQL == "")
					{
						strCashSQL += " AND (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayrollAccount + "'";
					}
					else
					{
						strCashSQL += " OR substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayrollAccount + "'";
					}
				}
				else
				{
					strGLSQL += " AND (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + PayrollAccount + "' OR substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) <> '00')";
					if (strCashSQL == "")
					{
						strCashSQL += " AND ((substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayrollAccount + "' AND substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) = '00')";
					}
					else
					{
						strCashSQL += " OR (substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + PayrollAccount + "' AND substring(Account, 5 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + " + " + FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))) + ", convert(int, right('" + modAccountTitle.Statics.Ledger + "', 2))) = '00')";
					}
				}
			}
			if (Conversion.Val(EncAccount) != 0)
			{
				strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + EncAccount + "'";
				strEncSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + EncAccount + "'";
			}
			if (Conversion.Val(RevAccount) != 0)
			{
				strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + RevAccount + "'";
			}
			if (Conversion.Val(ExpAccount) != 0)
			{
				strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + ExpAccount + "'";
			}
			if (Conversion.Val(DueToAccount) != 0)
			{
				strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + DueToAccount + "'";
				strDueToSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + DueToAccount + "'";
			}
			if (Conversion.Val(DueFromAccount) != 0)
			{
				strGLSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) <> '" + DueFromAccount + "'";
				strDueFromSQL += " AND substring(Account, 4 + " + FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + ", convert(int, substring('" + modAccountTitle.Statics.Ledger + "', 3, 2))) = '" + DueFromAccount + "'";
			}
			if (strCashSQL != "")
			{
				strCashSQL += ")";
			}
			rsGeneralInfo.OpenRecordset("SELECT * FROM JournalMaster WHERE JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period, "TWBD0000.vb1");
			// TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
			strType = FCConvert.ToString(rsGeneralInfo.Get_Fields("Type"));
			if (strType == "EN")
			{
				rsGeneralInfo.OpenRecordset("SELECT * FROM Encumbrances WHERE JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period, "TWBD0000.vb1");
			}
			else if (strType == "AP" || strType == "AC")
			{
				if (strType == "AP")
				{
					rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'P' AND CheckDate = '" + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).CheckDate + "' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period, "TWBD0000.vb1");
				}
				else
				{
					rsGeneralInfo.OpenRecordset("SELECT * FROM APJournal WHERE Status = 'P' AND JournalNumber = " + FCConvert.ToString(rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).JournalNumber) + " AND Period = " + rptMultiModulePosting.InstancePtr.PostInfo.Get_JournalsToPost(FCConvert.ToInt32(this.UserData)).Period, "TWBD0000.vb1");
				}
			}
			curTotalExp = 0;
			curTotalRev = 0;
			curTotalGL = 0;
			curTotalCash = 0;
			curTotalDueTo = 0;
			curTotalDueFrom = 0;
			curTotalEnc = 0;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (!(rptMultiModulePosting.InstancePtr.PostInfo == null))
			{
				if (rptMultiModulePosting.InstancePtr.PostInfo.PageBreakBetweenJournalsOnReport == false)
				{
					rptMultiModuleJournalTotals.InstancePtr.GroupFooter1.Height = 1300 / 1440F;
				}
				else
				{
					rptMultiModuleJournalTotals.InstancePtr.GroupFooter1.Height = 390 / 1440F;
				}
				Field5.Text = Strings.Format(curTotalExp, "#,##0.00");
				Field6.Text = Strings.Format(curTotalRev, "#,##0.00");
				Field7.Text = Strings.Format(curTotalGL, "#,##0.00");
				Field12.Text = Strings.Format(curTotalCash, "#,##0.00");
				Field13.Text = Strings.Format(curTotalDueTo, "#,##0.00");
				Field14.Text = Strings.Format(curTotalDueFrom, "#,##0.00");
				Field15.Text = Strings.Format(curTotalEnc, "#,##0.00");
			}
		}

		private void rptMultiModuleJournalTotals_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMultiModuleJournalTotals.Icon	= "rptMultiModuleJournalTotals.dsx":0000";
			//rptMultiModuleJournalTotals.Left	= 0;
			//rptMultiModuleJournalTotals.Top	= 0;
			//rptMultiModuleJournalTotals.Width	= 11880;
			//rptMultiModuleJournalTotals.Height	= 8595;
			//rptMultiModuleJournalTotals.StartUpPosition	= 3;
			//rptMultiModuleJournalTotals.SectionData	= "rptMultiModuleJournalTotals.dsx":058A;
			//End Unmaped Properties
		}
	}
}
