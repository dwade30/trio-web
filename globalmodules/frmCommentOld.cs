﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using TWSharedLibrary;

#if TWCL0000
using TWCL0000;


#elif TWFA0000
using TWFA0000;


#elif TWBD0000
using TWBD0000;


#elif TWAR0000
using TWAR0000;


#elif TWCR0000
using TWCR0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmCommentOld.
	/// </summary>
	public partial class frmCommentOld : BaseForm
	{
		public frmCommentOld()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCommentOld InstancePtr
		{
			get
			{
				return (frmCommentOld)Sys.GetInstance(typeof(frmCommentOld));
			}
		}

		protected frmCommentOld _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int intFrmSize;
		string strMod;
		string strTbl;
		string strFld;
		string strIDFld;
		int lngIDNumber;
		bool boolBlankComment;

		private void frmCommentOld_KeyDown(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCommentOld_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCommentOld.ScaleWidth	= 9045;
			//frmCommentOld.ScaleHeight	= 6930;
			//frmCommentOld.LinkTopic	= "Form1";
			//frmCommentOld.LockControls	= -1  'True;
			//rtbText.ScrollBars	= 2;
			//rtbText.TextRTF	= $"frmCommentOld.frx":058A;
			//End Unmaped Properties
			rtbText.Focus();
			modGlobalFunctions.SetFixedSize(this, intFrmSize);
			modGlobalFunctions.SetTRIOColors(this);
		}

		public bool Init(string strModule, string strTable, string strField, string strKeyField, int lngID, string strFormCaption = "Comment", string strControlCaption = "", short intFormSize = modGlobalConstants.TRIOWINDOWSIZEBIGGIE, bool boolNonEditable = false, bool boolModal = false)
		{
			bool init = false;
			// parameters:
			// strmodule   the two letter abbrev. for your module
			// strtable    the name of the table the comment is in
			// strfield    the name of the field the comment is in
			// strkeyfield the name of the ID field or account field etc. used to identify correct record
			// lngid       the ID,account etc. number
			// strformcaption   the caption of the form.  it is "Comment" by default
			// strcontrolcaption an optional explanation of the data in the control
			// intFormSize the size of the window (triowindowsizebiggie etc.)
			// Returns:
			// whether the comment is blank or not
			clsDRWrapper clsSave = new clsDRWrapper();
			intFrmSize = intFormSize;
			if (boolNonEditable)
			{
				rtbText.Locked = true;
				//mnuSave.Visible = false;
				//mnuSaveExit.Visible = false;
				//mnuSepar2.Visible = false;
				cmdSave.Visible = false;
			}
			this.Text = strFormCaption;
			if (!String.IsNullOrEmpty(strControlCaption))
			{
				this.HeaderText.Text = strControlCaption;
			}
			strMod = strModule;
			strTbl = strTable;
			strFld = strField;
			strIDFld = strKeyField;
			lngIDNumber = lngID;
			intFrmSize = intFormSize;
			// If intFrmSize = TRIOWINDOWSIZEBIGGIE Then
			// vsElasticLight1.Enabled = True
			// End If
			// size the control accordingly
			// Select Case intFrmSize
			// Case TRIOWINDOWMINI
			// rtbText.Width = 2775
			// rtbText.Height = 645
			// lblTitle.Width = 2775
			// Case TRIOWINDOWSIZEBIGGIE
			// don't need to do anything
			// Case TRIOWINDOWSIZEMEDIUM
			// rtbText.Width = 5745
			// rtbText.Height = 6450
			// lblTitle.Width = 5745
			// Case TRIOWINDOWSIZESMALL
			// rtbText.Width = 3825
			// rtbText.Height = 1605
			// lblTitle.Width = 3825
			// End Select
			clsSave.OpenRecordset("select " + strField + " from " + strTable + " where " + strKeyField + " = " + FCConvert.ToString(lngID), "tw" + strModule + "0000.vb1");
			if (!clsSave.EndOfFile())
			{
				rtbText.Text = FCConvert.ToString(clsSave.Get_Fields(strField));
				if (Strings.Trim(FCConvert.ToString(clsSave.Get_Fields(strField))) != string.Empty)
				{
					boolBlankComment = false;
				}
				else
				{
					boolBlankComment = true;
				}
			}
			else
			{
				rtbText.Text = "";
				boolBlankComment = true;
			}
			if (boolModal)
			{
				this.Show(FormShowEnum.Modal, App.MainForm);
			}
			else
			{
				this.Show(App.MainForm);
			}
			init = boolBlankComment;
			return init;
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			SaveComment();
		}

		//private void frmCommentOld_Resize(object sender, System.EventArgs e)
		//{
		//	if (intFrmSize == modGlobalConstants.TRIOWINDOWSIZEBIGGIE) return;
		//	rtbText.Width = frmCommentOld.InstancePtr.Width - (rtbText.Left * 2) - 100;
		//	//lblTitle.Width = rtbText.Width;
		//	rtbText.Height = this.Height - rtbText.Top - 650;
		//}
		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			rtbText.Text = "";
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				//FC:FINAL:DDU:#2252 - download to file instead to printer
				// On Error GoTo ErrorHandler
				// App.MainForm.CommonDialog1.Flags = 0	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				// App.MainForm.CommonDialog1.Flags = (vbPorterConverter.cdlPDReturnDC+vbPorterConverter.cdlPDNoPageNums)	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//App.MainForm.CommonDialog1.CancelError = true;
				// If rtbText.SelLength = 0 Then
				// App.MainForm.CommonDialog1.Flags = 0 /*? App.MainForm.CommonDialog1.Flags */	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"+vbPorterConverter.cdlPDAllPages	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				// App.MainForm.CommonDialog1.Flags = 0 /*? App.MainForm.CommonDialog1.Flags */	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"+vbPorterConverter.cdlPDSelection	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//Information.Err().Clear();
				//App.MainForm.CommonDialog1.ShowPrinter();
				//if (Information.Err().Number == 0)
				//{
				rtbText.SelPrint(0);
				//	FCGlobal.Printer.EndDoc();
				//}
				// End If
				return;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuPrint_click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			SaveComment();
		}

		private bool SaveComment()
		{
			bool SaveComment = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				SaveComment = false;
				clsSave.OpenRecordset("select * from " + strTbl + " where " + strIDFld + " = " + FCConvert.ToString(lngIDNumber), "tw" + strMod + "0000.vb1");
				if (!clsSave.EndOfFile())
				{
					clsSave.Edit();
				}
				else
				{
					clsSave.AddNew();
					clsSave.Set_Fields(strIDFld, lngIDNumber);
				}
				clsSave.Set_Fields(strFld, rtbText.Text);
				clsSave.Update();
				if (Strings.Trim(rtbText.Text) == string.Empty)
				{
					boolBlankComment = true;
				}
				else
				{
					boolBlankComment = false;
				}
				SaveComment = true;
				return SaveComment;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveComment", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveComment;
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveComment())
			{
				mnuExit_Click();
			}
		}

		private void cmdPrint_Click(object sender, EventArgs e)
		{
			mnuPrint_Click(sender, e);
		}

		private void cmdClear_Click(object sender, EventArgs e)
		{
			mnuClear_Click(sender, e);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSaveExit_Click(sender, e);
		}
	}
}
