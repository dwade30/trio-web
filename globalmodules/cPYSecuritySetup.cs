﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cPYSecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cPYSecuritySetup() : base()
		{
			strThisModule = "PY";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem("Enter Payroll", "", 1, false, ""));
			AddItem_2(CreateItem("Calendar End of Year", "", 4, true, ""));
			AddItem_2(CreateItem("Calendar End of Year", "Clear EIC Codes", 26, false, ""));
			AddItem_2(CreateItem("Employee File Update", "", 2, true, ""));
			AddItem_2(CreateItem("Employee File Update", "Check Return", 18, false, ""));
			AddItem_2(CreateItem("Employee File Update", "Contract Wages", 60, false, ""));
			AddItem_2(CreateItem("Employee File Update", "Deductions", 10, false, ""));
			AddItem_2(CreateItem("Employee File Update", "Direct Deposit", 15, false, ""));
			AddItem_2(CreateItem("Employee File Update", "Edit / Save Information", 59, false, ""));
			AddItem_2(CreateItem("Employee File Update", "Edit Pay Records", 62, false, ""));
			AddItem_2(CreateItem("Employee File Update", "Employee Health Care Setup", 71, false, ""));
			AddItem_2(CreateItem("Employee File Update", "Employee Master", 8, false, ""));
			AddItem_2(CreateItem("Employee File Update", "Employers Match", 11, false, ""));
			AddItem_2(CreateItem("Employee File Update", "MSRS Unemployment Info", 13, false, ""));
			AddItem_2(CreateItem("Employee File Update", "Non Paid MSRS Info", 17, false, ""));
			AddItem_2(CreateItem("Employee File Update", "Pay Totals", 14, false, ""));
			AddItem_2(CreateItem("Employee File Update", "Payroll Distribution", 9, false, ""));
			AddItem_2(CreateItem("Employee File Update", "Vacation Sick", 12, false, ""));
			AddItem_2(CreateItem("File Maintenance", "", 7, true, ""));
			AddItem_2(CreateItem("File Maintenance", "Adjust T & A Records", 48, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Customize", 43, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Direct Deposit Banks", 46, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Edit State Towns Counties", 44, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Employee Lengths etc", 42, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Frequency Codes", 40, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Load Back", 63, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Purge Audit", 65, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Purge Terminated Employees", 64, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Tax Status Codes", 41, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Valid Accounts", 45, false, ""));
			AddItem_2(CreateItem("Payroll Processing", "", 3, true, ""));
			AddItem_2(CreateItem("Payroll Processing", "Checks", 23, false, ""));
			AddItem_2(CreateItem("Payroll Processing", "Data Edit and Calculation", 21, false, ""));
			AddItem_2(CreateItem("Payroll Processing", "Data Entry", 20, false, ""));
			AddItem_2(CreateItem("Payroll Processing", "File Setup", 19, false, ""));
			AddItem_2(CreateItem("Payroll Processing", "Payroll Process Reports", 24, false, ""));
			AddItem_2(CreateItem("Payroll Processing", "Paryoll Process Status", 25, false, ""));
			AddItem_2(CreateItem("Payroll Processing", "Verify and Accept", 22, false, ""));
			AddItem_2(CreateItem("Printing", "", 5, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "", 6, true, ""));
			AddItem_2(CreateItem("Table and File Setups", "Change Unemployment S-Y", 68, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Check Recipient Categories", 33, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Check Recipients", 32, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Clear LTD Deductions", 67, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Create Tax Table Database", 37, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Data Entry Selection", 36, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Deduction Setup", 27, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Load New Tax Tables", 38, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Pay Category Setup", 29, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Purge Database Data", 69, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Report Selections", 35, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Setup Payroll Accounts", 34, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Standard Limits Rates", 30, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Tax Table Maintenance", 28, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Update Pay Codes", 39, false, ""));
			AddItem_2(CreateItem("Table and File Setups", "Vacation/Sick/Other Codes", 31, false, ""));
            AddItem_2(CreateItem("View Social Security Numbers","",72,true,""));
            AddItem_2(CreateItem("View Bank Accounts", "", 73, true, ""));
        }
	}
}
