﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Text;

#if TWCR0000
using TWCR0000;

#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmTrioSplash.
	/// </summary>
	partial class frmTrioSplash : BaseForm
	{
		public fecherFoundation.FCFrame fraLoading;
		public fecherFoundation.FCLabel lblLoading;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCLabel lblVersion;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblCompanyProduct;
		public fecherFoundation.FCLabel lblLicenseTo;
		public fecherFoundation.FCLabel lblProductName;
		public fecherFoundation.FCLabel lblPlatform;
		public fecherFoundation.FCLabel lblWarning;
		public fecherFoundation.FCLabel lblCompany;
		public fecherFoundation.FCLabel lblCopyright;
		public fecherFoundation.FCComboBox cboUserID;
		public fecherFoundation.FCTextBox txtPassword;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmTrioSplash));
			this.fraLoading = new fecherFoundation.FCFrame();
			this.lblLoading = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.lblVersion = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.lblCompanyProduct = new fecherFoundation.FCLabel();
			this.lblLicenseTo = new fecherFoundation.FCLabel();
			this.lblProductName = new fecherFoundation.FCLabel();
			this.lblPlatform = new fecherFoundation.FCLabel();
			this.lblWarning = new fecherFoundation.FCLabel();
			this.lblCompany = new fecherFoundation.FCLabel();
			this.lblCopyright = new fecherFoundation.FCLabel();
			this.cboUserID = new fecherFoundation.FCComboBox();
			this.txtPassword = new fecherFoundation.FCTextBox();
			this.cmdOK = new fecherFoundation.FCButton();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.SuspendLayout();
			//
			// fraLoading
			//
			this.fraLoading.Controls.Add(this.lblLoading);
			this.fraLoading.Name = "fraLoading";
			this.fraLoading.Visible = false;
			this.fraLoading.TabIndex = 16;
			this.fraLoading.Location = new System.Drawing.Point(6, 364);
			this.fraLoading.Size = new System.Drawing.Size(543, 94);
			this.fraLoading.Text = "";
			//this.fraLoading.BackColor = System.Drawing.SystemColors.Control;
			//
			// lblLoading
			//
			this.lblLoading.Name = "lblLoading";
			this.lblLoading.TabIndex = 17;
			this.lblLoading.Location = new System.Drawing.Point(103, 28);
			this.lblLoading.Size = new System.Drawing.Size(361, 38);
			this.lblLoading.Text = "Loading Software.........";
			//this.lblLoading.BackColor = System.Drawing.SystemColors.Control;
			this.lblLoading.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(0)), ((System.Byte)(0)), ((System.Byte)(192)));
			////this.lblLoading.Font = new System.Drawing.Font("MS Sans Serif", 24.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Frame1
			//
			this.Frame1.Controls.Add(this.lblVersion);
			this.Frame1.Controls.Add(this.Label2);
			this.Frame1.Controls.Add(this.lblCompanyProduct);
			this.Frame1.Controls.Add(this.lblLicenseTo);
			this.Frame1.Controls.Add(this.lblProductName);
			this.Frame1.Controls.Add(this.lblPlatform);
			this.Frame1.Controls.Add(this.lblWarning);
			this.Frame1.Controls.Add(this.lblCompany);
			this.Frame1.Controls.Add(this.lblCopyright);
			this.Frame1.Name = "Frame1";
			this.Frame1.TabIndex = 4;
			this.Frame1.Location = new System.Drawing.Point(2, 4);
			this.Frame1.Size = new System.Drawing.Size(545, 352);
			this.Frame1.Text = "";
			//this.Frame1.BackColor = System.Drawing.SystemColors.Control;
			//
			// lblVersion
			//
			this.lblVersion.Name = "lblVersion";
			this.lblVersion.Visible = false;
			this.lblVersion.TabIndex = 8;
			this.lblVersion.AutoSize = true;
			this.lblVersion.Location = new System.Drawing.Point(457, 213);
			this.lblVersion.Size = new System.Drawing.Size(69, 22);
			this.lblVersion.Text = "Version";
			//this.lblVersion.BackColor = System.Drawing.SystemColors.Control;
			this.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight;
			////this.lblVersion.Font = new System.Drawing.Font("Arial", 12.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Label2
			//
			this.Label2.Name = "Label2";
			this.Label2.TabIndex = 15;
			this.Label2.Location = new System.Drawing.Point(4, 186);
			this.Label2.Size = new System.Drawing.Size(539, 32);
			this.Label2.Text = "Software Corporation";
			//this.Label2.BackColor = System.Drawing.SystemColors.Control;
			this.Label2.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(128)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			////this.Label2.Font = new System.Drawing.Font("Times New Roman", 14.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// lblCompanyProduct
			//
			this.lblCompanyProduct.Name = "lblCompanyProduct";
			this.lblCompanyProduct.TabIndex = 12;
			this.lblCompanyProduct.AutoSize = true;
			this.lblCompanyProduct.Location = new System.Drawing.Point(5, 58);
			this.lblCompanyProduct.Size = new System.Drawing.Size(534, 33);
			this.lblCompanyProduct.Text = "Town of Trioville";
			//this.lblCompanyProduct.BackColor = System.Drawing.SystemColors.Control;
			this.lblCompanyProduct.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			////this.lblCompanyProduct.Font = new System.Drawing.Font("Arial", 18.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// lblLicenseTo
			//
			this.lblLicenseTo.Name = "lblLicenseTo";
			this.lblLicenseTo.TabIndex = 11;
			this.lblLicenseTo.Location = new System.Drawing.Point(7, 30);
			this.lblLicenseTo.Size = new System.Drawing.Size(536, 20);
			this.lblLicenseTo.Text = "Licensed To";
			//this.lblLicenseTo.BackColor = System.Drawing.SystemColors.Control;
			this.lblLicenseTo.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			////this.lblLicenseTo.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// lblProductName
			//
			this.lblProductName.Name = "lblProductName";
			this.lblProductName.TabIndex = 10;
			this.lblProductName.AutoSize = true;
			this.lblProductName.Location = new System.Drawing.Point(4, 91);
			this.lblProductName.Size = new System.Drawing.Size(538, 111);
			this.lblProductName.Text = "TRIO";
			//this.lblProductName.BackColor = System.Drawing.SystemColors.Control;
			this.lblProductName.ForeColor = System.Drawing.Color.FromArgb(((System.Byte)(128)), ((System.Byte)(0)), ((System.Byte)(0)));
			this.lblProductName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			////this.lblProductName.Font = new System.Drawing.Font("Times New Roman", 72.00F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// lblPlatform
			//
			this.lblPlatform.Name = "lblPlatform";
			this.lblPlatform.TabIndex = 9;
			this.lblPlatform.AutoSize = true;
			this.lblPlatform.Location = new System.Drawing.Point(519, 184);
			this.lblPlatform.Size = new System.Drawing.Size(6, 29);
			this.lblPlatform.Text = "";
			//this.lblPlatform.BackColor = System.Drawing.SystemColors.Control;
			this.lblPlatform.TextAlign = System.Drawing.ContentAlignment.TopRight;
			////this.lblPlatform.Font = new System.Drawing.Font("Arial", 15.75F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// lblWarning
			//
			this.lblWarning.Name = "lblWarning";
			this.lblWarning.TabIndex = 7;
			this.lblWarning.AutoSize = true;
			this.lblWarning.Location = new System.Drawing.Point(10, 290);
			this.lblWarning.Size = new System.Drawing.Size(320, 51);
			this.lblWarning.Text = "Warning:  This Program is protected by US and International Copyright Laws as discribed in Help About.  Copyright @1999 TRIO Software Corporation";
			//this.lblWarning.BackColor = System.Drawing.SystemColors.Control;
			////this.lblWarning.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// lblCompany
			//
			this.lblCompany.Name = "lblCompany";
			this.lblCompany.TabIndex = 6;
			this.lblCompany.Location = new System.Drawing.Point(380, 322);
			this.lblCompany.Size = new System.Drawing.Size(151, 18);
			this.lblCompany.Text = "TRIO Software Corporation";
			//this.lblCompany.BackColor = System.Drawing.SystemColors.Control;
			////this.lblCompany.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// lblCopyright
			//
			this.lblCopyright.Name = "lblCopyright";
			this.lblCopyright.TabIndex = 5;
			this.lblCopyright.Location = new System.Drawing.Point(380, 307);
			this.lblCopyright.Size = new System.Drawing.Size(52, 18);
			this.lblCopyright.Text = "Copyright";
			//this.lblCopyright.BackColor = System.Drawing.SystemColors.Control;
			////this.lblCopyright.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Regular), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// cboUserID
			//
			this.cboUserID.Name = "cboUserID";
			this.cboUserID.TabIndex = 3;
			this.cboUserID.Location = new System.Drawing.Point(186, 376);
			this.cboUserID.Size = new System.Drawing.Size(151, 40);
			this.cboUserID.Text = "";
			this.cboUserID.BackColor = System.Drawing.SystemColors.Window;
			this.cboUserID.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboUserID.Sorted = true;
			//
			// txtPassword
			//
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.TabIndex = 0;
			this.txtPassword.Location = new System.Drawing.Point(186, 415);
			this.txtPassword.Size = new System.Drawing.Size(151, 40);
			this.txtPassword.Text = "";
			this.txtPassword.BackColor = System.Drawing.SystemColors.Window;
			//
			// cmdOK
			//
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.TabIndex = 1;
			this.cmdOK.Location = new System.Drawing.Point(370, 374);
			this.cmdOK.Size = new System.Drawing.Size(114, 28);
			this.cmdOK.Text = "OK";
			//this.cmdOK.BackColor = System.Drawing.SystemColors.Control;
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			//
			// cmdCancel
			//
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.TabIndex = 2;
			this.cmdCancel.Location = new System.Drawing.Point(368, 411);
			this.cmdCancel.Size = new System.Drawing.Size(114, 28);
			this.cmdCancel.Text = "Cancel";
			//this.cmdCancel.BackColor = System.Drawing.SystemColors.Control;
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			//
			// Label3
			//
			this.Label3.Name = "Label3";
			this.Label3.TabIndex = 14;
			this.Label3.Location = new System.Drawing.Point(70, 413);
			this.Label3.Size = new System.Drawing.Size(120, 22);
			this.Label3.Text = "Password";
			//this.Label3.BackColor = System.Drawing.SystemColors.Control;
			////this.Label3.Font = new System.Drawing.Font("Tahoma", 11.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			//
			// Label4
			//
			this.Label4.Name = "Label4";
			this.Label4.TabIndex = 13;
			this.Label4.Location = new System.Drawing.Point(70, 378);
			this.Label4.Size = new System.Drawing.Size(120, 22);
			this.Label4.Text = "User ID";
            //this.Label4.BackColor = System.Drawing.SystemColors.Control;
            ////this.Label4.Font = new System.Drawing.Font("Tahoma", 11.25F, ((System.Drawing.FontStyle)System.Drawing.FontStyle.Bold), System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
            //
            // frmTrioSplash
            //
            this.AcceptButton = this.cmdOK;
			this.ClientSize = new System.Drawing.Size(552, 462);
			this.ClientArea.Controls.Add(this.fraLoading);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.cboUserID);
			this.ClientArea.Controls.Add(this.txtPassword);
			this.ClientArea.Controls.Add(this.cmdOK);
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label4);
			this.Name = "frmTrioSplash";
			//this.BackColor = System.Drawing.SystemColors.Control;
			this.KeyPreview = true;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.ControlBox = false;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTrioSplash_KeyPress);
			this.Load += new System.EventHandler(this.frmTrioSplash_Load);
			this.Text = "";
			this.lblLoading.ResumeLayout(false);
			this.fraLoading.ResumeLayout(false);
			this.lblVersion.ResumeLayout(false);
			this.Label2.ResumeLayout(false);
			this.lblCompanyProduct.ResumeLayout(false);
			this.lblLicenseTo.ResumeLayout(false);
			this.lblProductName.ResumeLayout(false);
			this.lblPlatform.ResumeLayout(false);
			this.lblWarning.ResumeLayout(false);
			this.lblCompany.ResumeLayout(false);
			this.lblCopyright.ResumeLayout(false);
			this.Frame1.ResumeLayout(false);
			this.Label3.ResumeLayout(false);
			this.Label4.ResumeLayout(false);
			this.ResumeLayout(false);
		}
		#endregion
	}
}
