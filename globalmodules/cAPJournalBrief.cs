﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace Global
{
    class cAPJournalBrief
    {
        //=========================================================
        private int lngRecordID;
        private int lngJournalNumber;
        private string strDescription = "";
        private int intPeriod;
        private int lngBankID;
        private string strPayableDate = "";

        public int BankID
        {
            set
            {
                lngBankID = value;
            }
            get
            {
                int BankID = 0;
                ID = lngBankID;
                return BankID;
            }
        }

        public string Description
        {
            set
            {
                strDescription = value;
            }
            get
            {
                string Description = "";
                Description = strDescription;
                return Description;
            }
        }

        public int ID
        {
            set
            {
                lngRecordID = value;
            }
            get
            {
                int ID = 0;
                ID = lngRecordID;
                return ID;
            }
        }

        public int JournalNumber
        {
            set
            {
                lngJournalNumber = value;
            }
            get
            {
                int JournalNumber = 0;
                JournalNumber = lngJournalNumber;
                return JournalNumber;
            }
        }

        public string PayableDate
        {
            set
            {
                if (Information.IsDate(value))
                {
                    strPayableDate = value;
                }
                else
                {
                    strPayableDate = "";
                }
            }
            get
            {
                string PayableDate = "";
                PayableDate = strPayableDate;
                return PayableDate;
            }
        }

        public short Period
        {
            set
            {
                intPeriod = value;
            }
            // vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
            get
            {
                short Period = 0;
                Period = FCConvert.ToInt16(intPeriod);
                return Period;
            }
        }


    }
}
