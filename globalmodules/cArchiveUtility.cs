﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace Global
{
	public class cArchiveUtility
	{
		//=========================================================
		public string DeriveGroupName(string strArchiveType, int intArchiveID)
		{
			string DeriveGroupName = "";
			if (intArchiveID > 0)
			{
				DeriveGroupName = strArchiveType + "_" + FCConvert.ToString(intArchiveID);
			}
			else
			{
				DeriveGroupName = strArchiveType;
			}
			return DeriveGroupName;
		}

		public bool ArchiveExists(string strArchiveType, int intArchiveID)
		{
			bool ArchiveExists = false;
			bool boolReturn;
			boolReturn = false;
			if (strArchiveType != "")
			{
				FCCollection tGroups = new FCCollection();
				ConnectionGroup tGroup = new ConnectionGroup();
				tGroups = GetArchiveConnectionGroups(strArchiveType, intArchiveID);
				if (!(tGroups == null))
				{
					if (tGroups.Count > 0)
					{
						boolReturn = true;
					}
				}
			}
			ArchiveExists = boolReturn;
			return ArchiveExists;
		}

		public FCCollection GetArchiveConnectionGroups(string strArchiveType, int intArchiveID, bool boolDesc = true)
		{
			FCCollection GetArchiveConnectionGroups = null;
			// ArchiveID is probably the year
			// ArchiveType is Archive for budgetary archives
			// Budgetary 2010 archive would be ArchiveID = 2010 and ArchiveType = "Archive"
			// The resulting DB name would be TRIO_TownName_Archive_2010_Budgetary
			// vbPorter upgrade warning: tReturn As Collection	OnRead(ConnectionGroup)
			FCCollection tReturn = new FCCollection();
			GetArchiveConnectionGroups = tReturn;
			bool boolMatch = false;
			ConnectionMegaGroup tMega = new ConnectionMegaGroup();
			ConnectionGroup tGroup = new ConnectionGroup();
			// vbPorter upgrade warning: checkGroup As ConnectionGroup	OnWrite(Collection)
			ConnectionGroup checkGroup = new ConnectionGroup();
			ConnectionPool tPool = new ConnectionPool();
			bool boolInserted = false;
			tMega = tPool.GetMegaGroup(tPool.DefaultMegaGroup);
			int x;
			int y;
			if (!(tMega == null))
			{
				for (x = 0; x <= tMega.GroupCount() - 1; x++)
				{
					boolMatch = true;
					tGroup = tMega.AvailableGroups(x);
					if (!(tGroup == null))
					{
						if (strArchiveType != "")
						{
							if (Strings.LCase(strArchiveType) != Strings.LCase(tGroup.GroupType))
							{
								boolMatch = false;
							}
						}
						else if (Strings.LCase(tGroup.GroupType) == "live")
						{
							boolMatch = false;
						}
						if (boolMatch && intArchiveID > 0)
						{
							if (tGroup.ID != intArchiveID)
							{
								boolMatch = false;
							}
						}
						if (boolMatch)
						{
							boolInserted = false;
							for (y = 1; y <= tReturn.Count; y++)
							{
								checkGroup = tReturn[y];
								if (fecherFoundation.Strings.CompareString(Strings.LCase(tGroup.GroupType), Strings.LCase(checkGroup.GroupType), true) < 0)
								{
									tReturn.Add(tGroup, null, y);
									boolInserted = true;
									break;
								}
								if (Strings.LCase(tGroup.GroupType) == Strings.LCase(checkGroup.GroupType))
								{
									if (boolDesc)
									{
										if (tGroup.ID > checkGroup.ID)
										{
											tReturn.Add(tGroup, null, y);
											boolInserted = true;
											break;
										}
									}
									else
									{
										if (tGroup.ID < checkGroup.ID)
										{
											tReturn.Add(tGroup, null, y);
											boolInserted = true;
											break;
										}
									}
								}
							}
							// y
							if (!boolInserted)
							{
								tReturn.Add(tGroup);
							}
						}
					}
				}
				// x
			}
			return GetArchiveConnectionGroups;
		}

		public FCCollection GetArchiveGroupNames(string strArchiveType, int intArchiveID)
		{
			FCCollection GetArchiveGroupNames = null;
			FCCollection tReturn = new FCCollection();
			FCCollection tGroups = new FCCollection();
			tGroups = GetArchiveConnectionGroups(strArchiveType, intArchiveID);
			if (!(tGroups == null))
			{
				foreach (ConnectionGroup tGroup in tGroups)
				{
					tReturn.Add(tGroup.GroupName);
				}
				// tGroup
			}
			GetArchiveGroupNames = tReturn;
			return GetArchiveGroupNames;
		}
	}
}
