﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using TWSharedLibrary;
#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWBD0000
using TWBD0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class arLienDischargeNotice : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/07/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/25/2006              *
		// ********************************************************
		string strMainText;
		string strTreasurerName = "";
		DateTime dtPaymentDate;
		string strOwnerName = "";
		DateTime dtLienDate;
		string strBook = "";
		string strPage = "";
		string strCounty = "";
		string strHisHer = "";
		string strMuniName = "";
		int lngLineLen;
		string strSignerDesignation = "";
		string strSignerName = "";
		DateTime dtCommissionExpiration;
		string strTitle = "";
		DateTime dtTreasSignDate;
		bool boolUseAbatementLine;
		// VBto upgrade warning: dtAppearedDate As DateTime	OnWrite(short, DateTime)
		DateTime dtAppearedDate;
		string strYear = "";
		bool boolBlankForm;

		public arLienDischargeNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Discharge Notice";
		}

		public static arLienDischargeNotice InstancePtr
		{
			get
			{
				return (arLienDischargeNotice)Sys.GetInstance(typeof(arLienDischargeNotice));
			}
		}

		protected arLienDischargeNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void SetStrings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsAbate = new clsDRWrapper();
				string strAppearedDateText = "";
				lngLineLen = 35;
				// this is how many underscores are in the printed lines
				if (dtAppearedDate.ToOADate() != 0)
				{
					strAppearedDateText = Strings.Format(dtAppearedDate, "MMMM dd, yyyy");
				}
				else
				{
					// if there is no treasurer signing date, then use the current date
					strAppearedDateText = Strings.StrDup(15, "_");
				}
				if (boolBlankForm)
				{
					lblTownHeader.Text = Strings.StrDup(10, "_") + " of " + Strings.StrDup(20, "_");
				}
				else
				{
					if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
					{
						lblTownHeader.Text = modGlobalConstants.Statics.gstrCityTown + " of " + strMuniName;
					}
					else
					{
						lblTownHeader.Text = strMuniName;
					}
				}
				fldMuni.Text = lblTownHeader.Text;
				// kgk 11-28-2011 trocl-842  Change title discharge notice
				// lblTitleBar.Text = "DISCHARGE OF MORTGAGE FOR TAX COLLECTOR'S LIEN CERTIFICATE"
				lblTitleBar.Text = "DISCHARGE OF TAX LIEN MORTGAGE CERTIFICATE";
				lblLegalDescription.Text = "Title 36, M.R.S.A. Section 943";
				strMainText = "    I, " + strTreasurerName + ", in my capacity as " + strTitle + " of the municipality of ";
				strMainText += strMuniName + ", hereby acknowledge that on ";
				if (boolBlankForm)
				{
					strMainText += Strings.StrDup(10, "_");
				}
				else
				{
					strMainText += Strings.Format(dtPaymentDate, "MMMM dd, yyyy");
				}
				if (boolUseAbatementLine)
				{
					rsAbate.OpenRecordset("SELECT * FROM Collections", modExtraModules.strCLDatabase);
					if (!rsAbate.EndOfFile())
					{
						strMainText += " " + FCConvert.ToString(rsAbate.Get_Fields_String("LienAbateText")) + " granted an abatement of the tax obligation secured by the " + strYear + " tax lien mortgage against property assessed to ";
					}
					else
					{
						strMainText += " ________________________ granted an abatement of the tax obligation secured by the " + strYear + " tax lien mortgage against property assessed to ";
					}
				}
				else
				{
					strMainText += " I received full payment and satisfaction of the debt secured by the " + strYear + " tax lien mortgage against property assessed to ";
				}
				strMainText += strOwnerName + " created by the recording of a tax lien certificate dated ";
				if (boolBlankForm)
				{
					strMainText += Strings.StrDup(10, "_");
				}
				else
				{
					strMainText += FCConvert.ToString(dtLienDate);
				}
				strMainText += " in Book " + strBook + ", at Page " + strPage + " of the ";
				strMainText += strCounty + " County Registry of Deeds, and in consideration thereof I hereby discharge said tax lien mortgage.";
				fldMainText.Text = strMainText;
				fldTopDate.Text = "";
				// "Dated: " & Format(dtPaymentDate, "MMMM dd, yyyy")
				// kk02082016  fldSigLine.Text = String(lngLineLen, "_")
				fldSigTitle.Text = "";
				// "Treasurer"
				fldSigName.Text = strTreasurerName + ", " + strTitle;
				if (boolBlankForm)
				{
					fldSigDate.Text = "Dated: " + Strings.StrDup(10, "_");
				}
				else
				{
					fldSigDate.Text = "Dated: " + Strings.Format(dtTreasSignDate, "MMMM dd, yyyy");
				}
				// fldACKNOWLEDGEMENT = "State of Maine"
				fldACKNOWLEDGEMENT.Text = "ACKNOWLEDGEMENT";
				strMainText = strMuniName + "\r\n";
				strMainText += "State of Maine" + "\r\n" + strCounty + " County, ss." + "\r\n" + "\r\n";
				strMainText += "Personally appeared before me, on " + strAppearedDateText + ", the above-named " + strTreasurerName;
				strMainText += ", who acknowledged the foregoing to be ";
				strMainText += strHisHer + " free act and deed in " + strHisHer + " capacity as " + strTitle + ".";
				fldBottomText.Text = strMainText;
				fldNotaryLine.Text = Strings.StrDup(lngLineLen, "_");
				if (strSignerName != "" && strSignerDesignation != "")
				{
					fldNotaryTitle.Text = strSignerName + ", " + strSignerDesignation;
					// "Notary Public / Attorney at Law"
				}
				else if (strSignerName != "")
				{
					fldNotaryTitle.Text = strSignerName;
				}
				else
				{
					fldNotaryTitle.Text = strSignerDesignation;
				}
				// fldNotaryPrintLine.Text = String(lngLineLen, "_")
				// fldNotaryPrintTitle.Text = strSignerName ' "Printed Name"
				// fldNotaryCommissionLine.Text = String(lngLineLen, "_")
				if (dtCommissionExpiration.ToOADate() != 0)
				{
					fldNotaryCommissionTitle.Text = "My commission expires: " + Strings.Format(dtCommissionExpiration, "MMMM dd, yyyy");
				}
				else
				{
					fldNotaryCommissionTitle.Text = "My commission expires: ";
				}
				// fldNotaryTitle.Text = strSignerName & ", " & strSignerDesignation  '"Notary Public / Attorney at Law"
				// fldNotaryPrintLine.Text = String(lngLineLen, "_")
				// fldNotaryPrintTitle.Text = strSignerName ' "Printed Name"
				// fldNotaryCommissionLine.Text = String(lngLineLen, "_")
				// fldNotaryCommissionTitle.Text = "My commission expires: " & Format(dtCommissionExpiration, "MMMM dd, yyyy")
				rsAbate.DisposeOf();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Setting Strings");
			}
		}
		// VBto upgrade warning: dtPassPayDate As DateTime	OnWrite(DateTime, short)
		// VBto upgrade warning: dtPassLienDate As DateTime	OnWrite(DateTime, short)
		// VBto upgrade warning: dtPassCommissionExpiration As DateTime	OnWrite(DateTime, short)
		public void Init(string strPassTeasName, string strPassMuni, string strPassCounty, string strPassName1, string strPassName2, DateTime dtPassPayDate, DateTime dtPassLienDate, string strPassBook, string strPassPage, string strPassHisHer, string strPassSignerName, string strPassSignerDesignation, DateTime dtPassCommissionExpiration, string strPassMapLot = "", string strPassTitle = "Treasurer", int lngPassAccount = 0, DateTime? dtPassTreasSignDate = null/*DateTime.Now*/, bool boolAbatementPayment = false, DateTime? dtPassAppearedDate = null/*DateTime.Now*/, string strPassYear = "", bool boolShowBlankForm = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will set all of the variables needed
				boolBlankForm = boolShowBlankForm;
				if (!dtPassTreasSignDate.HasValue)
				{
					dtPassTreasSignDate = DateTime.Now;
				}
				if (boolBlankForm)
				{
					// Row - 1  'Treasurer Name
					strTreasurerName = Strings.StrDup(10, "_");
					// Row - 2  'Muni Name
					strMuniName = Strings.StrDup(10, "_");
					// Row - 3  'County
					strCounty = Strings.StrDup(10, "_");
					// Row - 4  'Name1
					strOwnerName = Strings.StrDup(20, "_");
					// Row - 5  'Name2
					// Row - 6  'Payment Date
					dtPaymentDate = dtPassPayDate;
					// Row - 7  'Filing Date
					dtLienDate = dtPassLienDate;
					// treasurer signing date
					dtTreasSignDate = dtPassTreasSignDate.Value;
					// Row - 8  'Book
					strBook = "";
					if (Strings.Trim(strBook) == "" || Conversion.Val(strBook) == 0)
					{
						strBook = Strings.StrDup(10, "_");
					}
					// Row - 9  'Page
					strPage = "";
					if (Strings.Trim(strPage) == "" || Conversion.Val(strPage) == 0)
					{
						strPage = Strings.StrDup(10, "_");
					}
					// abatement used to discharge lien
					boolUseAbatementLine = boolAbatementPayment;
					// Row - 10 'His/Her
					strHisHer = Strings.LCase(strPassHisHer);
					if (Strings.Trim(strHisHer) == "")
					{
						strHisHer = "his/her";
					}
					// title
					strTitle = Strings.StrDup(10, "_");
					// Row - 11 'Signer Name
					strSignerName = "__________________";
					// Row - 12 'Signer's Designation
					strSignerDesignation = "__________________";
					// Row - 13 ' Commission expiration date
					dtCommissionExpiration = dtPassCommissionExpiration;
					lblMapLot.Text = "Map Lot : __________________";
					// appeared date
					dtAppearedDate = DateTime.FromOADate(0);
					// account number
					fldAccount.Text = "Acct : ________";
					strYear = Strings.StrDup(10, "_");
				}
				else
				{
					// Row - 1  'Treasurer Name
					strTreasurerName = strPassTeasName;
					// Row - 2  'Muni Name
					strMuniName = strPassMuni;
					// Row - 3  'County
					strCounty = strPassCounty;
					// Row - 4  'Name1
					strOwnerName = strPassName1;
					// Row - 5  'Name2
					if (Strings.Trim(strPassName2) != "")
					{
						strOwnerName += " " + strPassName2;
					}
					// Row - 6  'Payment Date
					dtPaymentDate = dtPassPayDate;
					// Row - 7  'Filing Date
					dtLienDate = dtPassLienDate;
					// treasurer signing date
					dtTreasSignDate = dtPassTreasSignDate.Value;
					// Row - 8  'Book
					strBook = strPassBook;
					if (Strings.Trim(strBook) == "" || Conversion.Val(strBook) == 0)
					{
						strBook = Strings.StrDup(10, "_");
					}
					// Row - 9  'Page
					strPage = strPassPage;
					if (Strings.Trim(strPage) == "" || Conversion.Val(strPage) == 0)
					{
						strPage = Strings.StrDup(10, "_");
					}
					// abatement used to discharge lien
					boolUseAbatementLine = boolAbatementPayment;
					// Row - 10 'His/Her
					strHisHer = Strings.LCase(strPassHisHer);
					if (Strings.Trim(strHisHer) == "")
					{
						strHisHer = "his/her";
					}
					// title
					strTitle = strPassTitle;
					// Row - 11 'Signer Name
					strSignerName = strPassSignerName;
					// Row - 12 'Signer's Designation
					strSignerDesignation = strPassSignerDesignation;
					// Row - 13 ' Commission expiration date
					dtCommissionExpiration = dtPassCommissionExpiration;
					lblMapLot.Text = "Map Lot : " + strPassMapLot;
					if (!dtPassAppearedDate.HasValue)
					{
						dtPassAppearedDate = DateTime.Now;
					}
					// appeared date
					dtAppearedDate = dtPassAppearedDate.Value;
					// account number
					if (lngPassAccount > 0)
					{
						// MAL@20080729: Changed to not convert to integer to avoid 'Overflow' error on long account numbers
						// Tracker Reference: 14859
						// fldAccount.Text = "Acct : " & PadToString(CInt(lngPassAccount), 5)
						fldAccount.Text = "Acct : " + modGlobal.PadToString(lngPassAccount, 5);
					}
					else
					{
						fldAccount.Text = "";
					}
					strYear = strPassYear;
				}
				if (modGlobal.Statics.gboolUseSigFile)
				{
					imgSig.Visible = true;
					//imgSig.ZOrder(1);
					// kk02082016  fldSigLine.ZOrder 0
					imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrTreasSigPath);
					// kk02052016 trout-1197  Rework lien notice. Change the way signature line is located.
					// The adjustments are applied before the text field is expanded, so we have to limit the adjustment amount.
					// If the signature moves into the rtbText area or above it, the signature will not be pushed down when the rtbText is expanded.
					if (modGlobal.Statics.gdblLienSigAdjust < 0)
					{
						// The digital signature starts out directly below the rich text box
						imgSig.Top = fldMainText.Top + fldMainText.Height;
						// Don't allow it to go any higher
					}
					else
					{
						if (imgSig.Top + modGlobal.Statics.gdblLienSigAdjust < Line2.Y1)
						{
							// The farthest it can move is so the top is even with the signature line
							imgSig.Top += FCConvert.ToSingle(modGlobal.Statics.gdblLienSigAdjust);
						}
						else
						{
							imgSig.Top = Line2.Y1;
						}
					}
				}
				else
				{
					imgSig.Visible = false;
				}
				frmReportViewer.InstancePtr.Init(this, "", 1, false, false, "Pages", false, "", "TRIO Software", false, true, "", false, false,true);
				frmLienDischargeNotice.InstancePtr.Unload();
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Init");
				/*? Resume; */
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// SetVariables
			if (Strings.Trim(modGlobalConstants.Statics.gstrTownSealPath) != "" && modCLCalculations.Statics.gboolCLUseTownSealLienDischarge)
			{
				// MAL@20080611: Add check for file's existence
				// Tracker Reference: 14008
				if (File.Exists(modGlobalConstants.Statics.gstrTownSealPath))
				{
					imgTownSeal.Visible = true;
					imgTownSeal.Image = FCUtils.LoadPicture(modGlobalConstants.Statics.gstrTownSealPath);
				}
				else
				{
					imgTownSeal.Visible = false;
				}
			}
			else
			{
				imgTownSeal.Visible = false;
			}
			// set the top margin          'kk02042016 trout-1197  New margins effective 10/2015 - 1.5" Top 1st page, 1.5" Bottom last page
			this.PageSettings.Margins.Top = 1.5F + FCConvert.ToSingle(modGlobal.Statics.gdblLDNAdjustmentTop);
			this.PageSettings.Margins.Bottom = 1.5F + FCConvert.ToSingle(modGlobal.Statics.gdblLDNAdjustmentBottom);
			this.PageSettings.Margins.Left = 0.75F + FCConvert.ToSingle(modGlobal.Statics.gdblLDNAdjustmentSide);
			this.PageSettings.Margins.Right = this.PageSettings.Margins.Left;
			this.PrintWidth = 8.5F - (this.PageSettings.Margins.Left + this.PageSettings.Margins.Right);
			SetStrings();
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
		}

		private void arLienDischargeNotice_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arLienDischargeNotice.Text	= "Lien Discharge Notice";
			//arLienDischargeNotice.Icon	= "arLienDischargeNotice.dsx":0000";
			//arLienDischargeNotice.Left	= 0;
			//arLienDischargeNotice.Top	= 0;
			//arLienDischargeNotice.Width	= 16110;
			//arLienDischargeNotice.Height	= 10005;
			//arLienDischargeNotice.StartUpPosition	= 3;
			//arLienDischargeNotice.SectionData	= "arLienDischargeNotice.dsx":058A;
			//End Unmaped Properties
		}
	}
}
