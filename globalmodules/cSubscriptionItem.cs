﻿//Fecher vbPorter - Version 1.0.0.40
namespace Global
{
	public class cSubscriptionItem
	{
		//=========================================================
		private string strName = "";
		private string strValue = string.Empty;
		private string strParentGroup = "";
		private string strGroup = "";

		public string Name
		{
			get
			{
				string Name = "";
				Name = strName;
				return Name;
			}
			set
			{
				strName = value;
			}
		}

		public string ItemValue
		{
			set
			{
				strValue = value;
			}
			get
			{
				string ItemValue = "";
				ItemValue = strValue;
				return ItemValue;
			}
		}

		public string Group
		{
			get
			{
				string Group = "";
				Group = strGroup;
				return Group;
			}
			set
			{
				strGroup = value;
			}
		}

		public string ParentGroup
		{
			get
			{
				string ParentGroup = "";
				ParentGroup = strParentGroup;
				return ParentGroup;
			}
			set
			{
				strParentGroup = value;
			}
		}
	}
}
