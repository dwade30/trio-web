﻿using fecherFoundation;
using SharedApplication.Messaging;
using SharedApplication.Telemetry;
using System;
using System.Drawing;
using System.Linq;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;

namespace Global
{
    public partial class BaseForm : fecherFoundation.FCForm
    {
        public ITelemetryService Telemetry { get; private set; }
        private bool disableFormClosingDuringWait = false;
        private bool enableCancel = false;
        private bool showProgressBar = false;
        private int height = 0;
        public bool StickPanelToBottom { get; set; } = false;
        public BaseForm()
        {
            InitializeComponent();
            TopPanel.ControlAdded += TopPanel_ControlAdded;
            if (Telemetry == null) Telemetry = StaticSettings.GlobalTelemetryService;
            Telemetry?.TrackHit(ToString().Substring(0, ToString().IndexOf(",")));
#if DEBUG
            var ctl = new Label { Text = this.ToString().Substring(0,this.ToString().IndexOf(',')) };
            ctl.Width = 400;
            TopPanel.Controls.Add(ctl);
#endif
        }

        private void ClientArea_Layout(object sender, LayoutEventArgs e)
        {
            LayoutBottomcontrol();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            LayoutBottomcontrol();
        }

        private void LayoutBottomcontrol()
        {
            if (BottomPanel != null && ClientArea != null)
            {
                BottomPanel.Width = ClientArea.Width - 20;
                if (!StickPanelToBottom)
                {
                    var bottomControl = ClientArea.Controls.Where(c => c != BottomPanel && c.Visible)
                        .OrderByDescending(c => (c.Top + c.Height)).FirstOrDefault();
                    if (bottomControl != null)
                    {
                        BottomPanel.Top = bottomControl.Top + bottomControl.Height;
                    }
                }
                else
                {
                    BottomPanel.Top = ClientArea.Height - BottomPanel.Height;
                }
            }
        }

        public void RePositionPanels()
        {
            LayoutBottomcontrol();
            
        }

        public BaseForm(ITelemetryService telemetryService)
        {
            Telemetry = telemetryService;
            InitializeComponent();
            TopPanel.ControlAdded += TopPanel_ControlAdded;
            Telemetry?.TrackHit(ToString().Substring(0, ToString().IndexOf(",")));
        }
        public void LockCloseDuringLongProcess()
        {
            disableFormClosingDuringWait = true;
        }

        public void UnlockCloseDuringLongProcess()
        {
            disableFormClosingDuringWait = false;
        }

        public void ShowWait(bool disableformClosing = true, bool showprogressBar = false, string message = "Waiting ...", bool enableCancel = false)
        {
            waitPanel.Visible = true;
            waitLabel.Text = message;
            this.enableCancel = enableCancel;
            btnCancel.Visible = this.enableCancel;
            if (waitLabel.Text != "")
            {
                FCUtils.ApplicationUpdate(waitLabel);
            }
            ClientArea.Visible = false;
            //FC:FINAL:AM:#3169 - can't disable the TopPanel because the hidden buttons on it gets visible after enabling it back
            //this.TopPanel.Enabled = false;
            height = TopPanel.Height;
            TopPanel.Height = 0;
            BottomPanel.Enabled = false;
            disableFormClosingDuringWait = disableformClosing;
            panel1.Size = !showprogressBar ? new System.Drawing.Size(312, 77) : new System.Drawing.Size(362, 121);
            showProgressBar = showprogressBar;
            progressBar1.Visible = showprogressBar;
            
        }

        public void ShowWait(string message)
        {
            ShowWait(true, false, message);
        }

        public void ShowWait(string message, bool enableCancel)
        {
            ShowWait(true,false,message, enableCancel);
        }

        public void UpdateWait(string text = "", int progressBarValue = -1, int progressBarMaximum = -1)
        {
            //update progress label text
            if (text != "")
            {
                waitLabel.Text = text;
                FCUtils.ApplicationUpdate(waitLabel);
            }

            //update panel width based on label/progressbar width
            int labelWidth = waitLabel.Left + waitLabel.Width + 10;
            int width = labelWidth;
            if (showProgressBar)
            {
                int progressBarWidth = progressBar1.Left + progressBar1.Width + 10;
                width = labelWidth > progressBarWidth ? labelWidth : progressBarWidth;
            }
            if (width > panel1.Width)
            {
                panel1.Width = width;
            }

            //update progress bar value
            if (showProgressBar)
            {
                if (progressBarMaximum != -1)
                {
                    progressBar1.Maximum = progressBarMaximum;
                    FCUtils.ApplicationUpdate(progressBar1);
                }
                if (progressBarValue != -1)
                {
                    progressBar1.Value = progressBarValue;
                    FCUtils.ApplicationUpdate(progressBar1);
                }
            }
        }

        public void UpdateCancel(bool allow)
        {
            btnCancel.Visible = allow;
            FCUtils.ApplicationUpdate(waitLabel);
        }

        public void EndWait()
        {
            waitPanel.Visible = false;
            ClientArea.Visible = true;
            //this.TopPanel.Enabled = true;
            TopPanel.Height = height;
            BottomPanel.Enabled = true;
            FCUtils.ApplicationUpdate(this);
            disableFormClosingDuringWait = false;
            fecherFoundation.FCUtils.UnlockUserInterface();
        }

        private void TopPanel_ControlAdded(object sender, ControlEventArgs e)
        {
            if (e.Control is Button)
            {
                //FC:FINAL:AM: hide buttons when they are disabled
                e.Control.VisibleChanged += Control_VisibleChanged;
                e.Control.EnabledChanged += Control_EnabledChanged;
                e.Control.SizeChanged += Control_SizeChanged;
                GenerateLayout();
            }
        }

        private void Control_SizeChanged(object sender, EventArgs e)
        {
            GenerateLayout();
        }

        private void Control_EnabledChanged(object sender, EventArgs e)
        {
            if (sender is Button button)
            {
                if (!button.IsDisposed)
                {
                    button.UserData.FromEnabledChanged = true;
                    button.Visible = button.Enabled && (button.UserData.Visible == null || button.UserData.Visible);
                    button.UserData.FromEnabledChanged = false;
                }
            }

        }

        private void Control_VisibleChanged(object sender, EventArgs e)
        {
            if (sender is Button button)
            {
                if (button.UserData.FromEnabledChanged == null || !button.UserData.FromEnabledChanged)
                {
                    button.UserData.Visible = button.Visible;
                }
                GenerateLayout();
            }
        }

        private void GenerateLayout()
        {
            if (DesignMode)
            {
                return;
            }
            int rightPosition = TopPanel.Width - 30;
            for (int i = TopPanel.Controls.Count - 1; i >= 0; i--)
            {
                Control control = TopPanel.Controls[i];
                if (control is Button && control.Visible && control.Enabled)
                {
                    control.Location = new Point(rightPosition - control.Width, 29);
                    rightPosition -= control.Width + 5;
                }
            }
        }

        private void BaseForm_Load(object sender, System.EventArgs e)
        {
            if (!DesignMode)
            {
                HeaderText.Font = new System.Drawing.Font("@header", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            }

            CenterBottomControls();
            waitPanel.Size = ClientArea.Size;
            panel1.Location = new Point(waitPanel.Width / 2, waitPanel.Height / 2) - new Size(panel1.Width / 2, panel1.Height / 2);
            waitPanel.Dock = DockStyle.Fill;
        }

        private void BottomPanel_SizeChanged(object sender, EventArgs e)
        {
            CenterBottomControls();
        }

        private void BottomPanel_ControlAdded(object sender, ControlEventArgs e)
        {
            CenterBottomControls();
        }

        private void BottomPanel_ControlRemoved(object sender, ControlEventArgs e)
        {
            CenterBottomControls();
        }

        public void CenterBottomControls()
        {
            if (DesignMode || BottomPanel.Controls.Count == 0)
            {
                return;
            }
            int width = 0;
            foreach (Control control in BottomPanel.Controls)
            {
                width += control.Width;
            }
            int boundsWidth = width + (BottomPanel.Controls.Count - 1) * 30;
            int leftDistance = (BottomPanel.Width - boundsWidth) / 2;
            //center controls
            int leftAdd = 0;
            foreach (Control control in BottomPanel.Controls)
            {
                control.Left = leftDistance + leftAdd;
                leftAdd += control.Width + 30;
            }
        }

        private void BaseForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!fecherFoundation.App.MainForm.ExitingApplication)
            {
                e.Cancel = disableFormClosingDuringWait;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            EndWait();

            if (Cancelled != null)
            {
                Cancelled(this, new EventArgs());
            }
        }

        public event EventHandler Cancelled;
    }
}
