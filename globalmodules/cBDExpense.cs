﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using System.Collections.Generic;

namespace Global
{
	public class cBDExpense
	{
		//=========================================================
		private int lngRecordNumber;
		private string strExpense = string.Empty;
		private string strShortDescription = string.Empty;
		private string strLongDescription = string.Empty;
		private int intBreakdownCode;
		private Dictionary<object, object> collObs = new Dictionary<object, object>();

		public Dictionary<object, object> ExpenseObjects
		{
			get
			{
				Dictionary<object, object> ExpenseObjects = new Dictionary<object, object>();
				ExpenseObjects = collObs;
				return ExpenseObjects;
			}
		}

		public int ID
		{
			set
			{
				lngRecordNumber = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordNumber;
				return ID;
			}
		}

		public string Expense
		{
			set
			{
				strExpense = value;
			}
			get
			{
				string Expense = "";
				Expense = strExpense;
				return Expense;
			}
		}

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}

		public string Description
		{
			set
			{
				strLongDescription = value;
			}
			get
			{
				string Description = "";
				Description = strLongDescription;
				return Description;
			}
		}

		public short BreakdownCode
		{
			set
			{
				intBreakdownCode = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				short BreakdownCode = 0;
				BreakdownCode = FCConvert.ToInt16(intBreakdownCode);
				return BreakdownCode;
			}
		}
	}
}
