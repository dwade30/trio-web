﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmUTComment.
	/// </summary>
	partial class frmUTComment : BaseForm
	{
		public fecherFoundation.FCRichTextBox txtComment;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuSep;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveandExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.txtComment = new fecherFoundation.FCRichTextBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSep = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSaveandExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdFilePrint = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 308);
            this.BottomPanel.Size = new System.Drawing.Size(624, 88);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtComment);
            this.ClientArea.Size = new System.Drawing.Size(644, 406);
            this.ClientArea.Controls.SetChildIndex(this.txtComment, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFilePrint);
            this.TopPanel.Size = new System.Drawing.Size(644, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(108, 28);
            this.HeaderText.Text = "Comment";
            // 
            // txtComment
            // 
            this.txtComment.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtComment.Location = new System.Drawing.Point(30, 30);
            this.txtComment.Name = "txtComment";
            this.txtComment.Size = new System.Drawing.Size(566, 278);
            this.txtComment.TabIndex = 1001;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrint,
            this.mnuSep,
            this.mnuFileSave,
            this.mnuFileSaveandExit,
            this.Seperator,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 0;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Text = "Print";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuSep
            // 
            this.mnuSep.Index = 1;
            this.mnuSep.Name = "mnuSep";
            this.mnuSep.Text = "-";
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 2;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuFileSaveandExit
            // 
            this.mnuFileSaveandExit.Index = 3;
            this.mnuFileSaveandExit.Name = "mnuFileSaveandExit";
            this.mnuFileSaveandExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileSaveandExit.Text = "Save and Exit";
            this.mnuFileSaveandExit.Click += new System.EventHandler(this.mnuFileSaveandExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 4;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 5;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(270, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // cmdFilePrint
            // 
            this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFilePrint.Location = new System.Drawing.Point(574, 29);
            this.cmdFilePrint.Name = "cmdFilePrint";
            this.cmdFilePrint.Size = new System.Drawing.Size(42, 24);
            this.cmdFilePrint.TabIndex = 1;
            this.cmdFilePrint.Text = "Print";
            this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // frmUTComment
            // 
            this.ClientSize = new System.Drawing.Size(644, 466);
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmUTComment";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Comment";
            this.Load += new System.EventHandler(this.frmUTComment_Load);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUTComment_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtComment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
		private FCButton cmdFilePrint;
	}
}
