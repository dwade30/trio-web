﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;

#if TWRE0000
using modBudgetaryMaster = TWRE0000.modREMain;


#elif TWBD0000
using TWBD0000;


#elif TWPY0000
using modBudgetaryMaster = TWPY0000.modSubMain;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmLogin.
	/// </summary>
	public partial class frmLogin : BaseForm
	{
		public frmLogin()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLogin InstancePtr
		{
			get
			{
				return (frmLogin)Sys.GetInstance(typeof(frmLogin));
			}
		}

		protected frmLogin _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private cSecurityUser tUser;
		SettingsInfo s = new SettingsInfo();
		WorkstationGroup[] wsgInfo;
		WorkstationGroup x = new WorkstationGroup();
		string strDataPath;
		private string strLastGroup = string.Empty;
		private int lngLastID;
		const int SWP_NOMOVE = 2;
		const int SWP_NOSIZE = 1;
		const short FLAGS = SWP_NOMOVE | SWP_NOSIZE;
		const short HWND_TOPMOST = -1;
		const int HWND_NOTOPMOST = -2;

		public cSecurityUser Login(string strCaption)
		{
			cSecurityUser Login = null;
			tUser = null;
			Login = null;
			lngLastID = FCConvert.ToInt32(Math.Round(Conversion.Val(modRegistry.GetRegistryKey("SecurityID", "", FCConvert.ToString(0)))));
			strLastGroup = modRegistry.GetRegistryKey("LastLoginGroup", "", "");
			lblGroup.Text = strCaption;
			this.Show(FormShowEnum.Modal, App.MainForm);
			Login = tUser;
			return Login;
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			tUser = null;
			Close();
		}

		private void btnLogin_Click(object sender, System.EventArgs e)
		{
			if (Strings.Trim(cmbUsers.Text) == "")
			{
				MessageBox.Show("You didn't enter a user name", "No User", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			if (Strings.Trim(txtPassword.Text) == "")
			{
				MessageBox.Show("You didn't enter a password", "No Password", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			cLogin tLogin = new cLogin();
			tUser = tLogin.GetUser(Strings.Trim(cmbUsers.Text), Strings.Trim(txtPassword.Text));
			if (!(tUser == null))
			{
				modRegistry.SaveRegistryKey("LastLoginGroup", cboDataGroup.Text, "");
				Close();
			}
			else
			{
				MessageBox.Show("Invalid user / password combination", "Invalid Credentials", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void cboDataGroup_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsLogin = new clsDRWrapper();
			strDataPath = wsgInfo[cboDataGroup.SelectedIndex].GlobalDataDirectory;
			if (Strings.Right(strDataPath, 1) != "\\")
			{
				strDataPath += "\\";
			}
			// kk04082015 troges-39  Single Config file
			// If Not s.LoadSettings(strDataPath & "trioworkstationsetup.xml") Then
			// Call MsgBox("Unable to load settings file", vbCritical, "Cannot Continue")
			// Exit Sub
			// End If
			s.GlobalDataDirectory = strDataPath;
			// If Not s.LoadSettingsAndArchives(CurDir & "\" & s.GetDefaultSettingsFileName) Then
			// MsgBox "Error loading connection information", vbCritical, "Error"
			// Exit Sub
			// End If
			rsLogin.DefaultGroup = "Live";
			if (rsLogin.MegaGroupsCount() > 0)
			{
				rsLogin.DefaultMegaGroup = wsgInfo[cboDataGroup.SelectedIndex].GroupName;
				// kk04082015 troges-39    rsLogin.AvailableMegaGroups(0).GroupName
			}
			// ChDir strDataPath
			modAPIsConst.SetCurrentDirectoryWrp(ref strDataPath);
			cLogin tLogin = new cLogin();
			string strList;
			strList = tLogin.GetUserList();
			FillUserCombo(strList);
			if (strLastGroup == cboDataGroup.Text)
			{
				int x;
				for (x = 0; x <= cmbUsers.Items.Count - 1; x++)
				{
					if (cmbUsers.ItemData(x) == lngLastID)
					{
						cmbUsers.SelectedIndex = x;
						break;
					}
				}
				// x
			}
		}

		private void FillUserCombo(string strList)
		{
			cmbUsers.Clear();
			if (strList != "")
			{
				string[] strAry = null;
				string[] strUAry = null;
				strAry = Strings.Split(strList, ",", -1, CompareConstants.vbBinaryCompare);
				int x;
				for (x = 0; x <= Information.UBound(strAry, 1); x++)
				{
					strUAry = Strings.Split(strAry[x], ";", -1, CompareConstants.vbBinaryCompare);
					cmbUsers.AddItem(strUAry[0]);
					if (Information.UBound(strUAry, 1) > 0)
					{
						cmbUsers.ItemData(cmbUsers.NewIndex, FCConvert.ToInt32(Conversion.Val(strUAry[1])));
					}
				}
				// x
			}
		}

		private void frmLogin_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F9:
					{
						if (Shift == 3)
						{
							if (Strings.UCase(txtPassword.Text) == Strings.UCase(modGlobalConstants.DATABASEPASSWORD))
							{
								// login as super user
								tUser = new cSecurityUser();
								tUser.ID = -1;
								tUser.User = "SuperUser";
								tUser.UserName = "SuperUser";
								tUser.UseSecurity = false;
								Close();
							}
						}
						break;
					}
                case Keys.F8:
                {
                    if (Shift == 3)
                    {
                      
                    }

                    break;
                }
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						Close();
						break;
					}
			}
			//end switch
		}

		private void frmLogin_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLogin.ScaleWidth	= 5355;
			//frmLogin.ScaleHeight	= 4395;
			//frmLogin.LinkTopic	= "Form2";
			//frmLogin.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			string strDataPath;
			int counter;
			strDataPath = FCFileSystem.Statics.UserDataFolder;
			if (Strings.Right(strDataPath, 1) != "\\")
			{
				strDataPath += "\\";
			}
			if (!s.LoadSettings(strDataPath + s.GetDefaultSettingsFileName()))
			{
				// kk04152015 "trioworkstationsetup.xml"
				MessageBox.Show("Unable to load settings file", "Cannot Continue", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				//Application.Exit();
				return;
			}
			cboDataGroup.Clear();
			wsgInfo = s.GetAvailableSetups();
			for (counter = 0; counter <= Information.UBound(wsgInfo, 1); counter++)
			{
				cboDataGroup.AddItem(wsgInfo[counter].GroupName);
			}
			if (cboDataGroup.Items.Count > 0)
				cboDataGroup.SelectedIndex = 0;
			modBudgetaryMaster.SetWindowPos(FCConvert.ToInt16(this.Handle.ToInt32()), HWND_TOPMOST, 0, 0, 0, 0, FLAGS);
			this.Refresh();
		}

		private bool PasswordTimesUp()
		{
			bool PasswordTimesUp = false;
			// Purpose of this procedure is to check to see if the
			// user needs to change their password due to the frequency
			// and the date the password was last changed.
			// 
			if (!(tUser == null))
			{
				// vbPorter upgrade warning: lngDays As int --> As long
				long lngDays;
				int lngFrequency = 0;
				// vbPorter upgrade warning: varResp As Variant --> As DialogResult
				DialogResult varResp;
				lngDays = 0;
				lngFrequency = 0;
				if (Information.IsDate(tUser.DateChanged))
				{
					if (tUser.DateChanged.ToOADate() != 0)
					{
						if (tUser.Frequency > 0)
						{
							lngDays = DateAndTime.DateDiff("D", tUser.DateChanged, DateTime.Today);
							if (tUser.Frequency - lngDays <= 0)
							{
								MessageBox.Show("Your password has expired. You must change it.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
								if (!modBudgetaryMaster.ChangePasswordNow(ref tUser))
								{
									MessageBox.Show("You must create a new password before you can continue", "Password Change Required", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									Application.Exit();
									return PasswordTimesUp;
								}
							}
							else if ((tUser.Frequency - lngDays) >= 1 && (tUser.Frequency - lngDays) <= 3)
							{
								varResp = MessageBox.Show("You have " + FCConvert.ToString(lngFrequency - lngDays) + " day(s) left before you MUST change your password.  Would you like to change your password now?", "Change Password", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
								if (varResp == DialogResult.Yes)
								{
									modBudgetaryMaster.ChangePasswordNow(ref tUser);
								}
							}
						}
					}
				}
			}
			return PasswordTimesUp;
		}
	}
}
