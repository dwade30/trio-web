﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using Wisej.Web;
using Microsoft.VisualBasic;
using fecherFoundation;

namespace Global
{
	/// <summary>
	/// Summary description for frmChooseSubReport.
	/// </summary>
	public partial class frmChooseSubReport : BaseForm
	{
		public frmChooseSubReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChooseSubReport InstancePtr
		{
			get
			{
				return (frmChooseSubReport)Sys.GetInstance(typeof(frmChooseSubReport));
			}
		}

		protected frmChooseSubReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		//=========================================================
		const short CNSTGRIDCOLID = 0;
		const short CNSTGRIDCOLTITLE = 1;
		const short CNSTGRIDCOLDESCRIPTION = 2;
		// vbPorter upgrade warning: lngIDChosen As int	OnWrite(short, string)
		int lngIDChosen;
		string strModDB;
		// vbPorter upgrade warning: 'Return' As int	OnWrite(bool, int)
		public int Init(ref string strMod)
		{
			int Init = 0;
			// returns ID if something chosen, otherwise it returns 0.  It will return -1 if the recordset was empty
			lngIDChosen = 0;
			strModDB = "tw" + strMod + "0000.vb1";
			Init = (false ? -1 : 0);
			if (!LoadGrid())
			{
				Init = lngIDChosen;
				Close();
				return Init;
			}
			this.Show(FCForm.FormShowEnum.Modal);
			Init = lngIDChosen;
			return Init;
		}

		private void frmChooseSubReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmChooseSubReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChooseSubReport properties;
			//frmChooseSubReport.FillStyle	= 0;
			//frmChooseSubReport.ScaleWidth	= 5880;
			//frmChooseSubReport.ScaleHeight	= 3810;
			//frmChooseSubReport.LinkTopic	= "Form2";
			//frmChooseSubReport.LockControls	= -1  'True;
			//frmChooseSubReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private bool LoadGrid()
		{
			bool LoadGrid = false;
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				fecherFoundation.Information.Err().Clear();
				LoadGrid = false;
				Grid.Rows = 1;
				Grid.TextMatrix(0, CNSTGRIDCOLTITLE, "Title");
				Grid.TextMatrix(0, CNSTGRIDCOLDESCRIPTION, "Description");
				Grid.ColHidden(CNSTGRIDCOLID, true);
				clsLoad.OpenRecordset("select ID,FormatName,description from custombills order by FormatName", strModDB);
				if (!clsLoad.EndOfFile())
				{
					while (!clsLoad.EndOfFile())
					{
						Grid.Rows += 1;
						Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLID, FCConvert.ToString(clsLoad.Get_Fields("ID")));
						Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLTITLE, FCConvert.ToString(clsLoad.Get_Fields("title")));
						Grid.TextMatrix(Grid.Rows - 1, CNSTGRIDCOLDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields("description")));
						clsLoad.MoveNext();
					}
				}
				else
				{
					lngIDChosen = -1;
					// signal nothing loaded and that it was empty
					return LoadGrid;
				}
				LoadGrid = true;
				return LoadGrid;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err().Number) + "  " + fecherFoundation.Information.Err().Description + "\r\n" + "In LoadGrid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadGrid;
		}

		private void ResizeGrid()
		{
			Grid.AutoSize(CNSTGRIDCOLTITLE);
		}

		private void frmChooseSubReport_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void Grid_DblClick(object sender, System.EventArgs e)
		{
			if (Grid.MouseRow < 1)
				return;
			mnuSaveContinue_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			if (Grid.Row < 1)
				return;
			lngIDChosen = FCConvert.ToInt32(Grid.TextMatrix(Grid.Row, CNSTGRIDCOLID));
			Close();
		}

		public void mnuSaveContinue_Click()
		{
			mnuSaveContinue_Click(mnuSaveContinue, new System.EventArgs());
		}
	}
}
