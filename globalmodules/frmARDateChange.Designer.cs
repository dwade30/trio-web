//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWAR0000
using TWAR0000;


#elif TWCR0000
using TWCR0000;

#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmARDateChange.
	/// </summary>
	partial class frmARDateChange : BaseForm
	{
		public Global.T2KDateBox txtDate;
		public fecherFoundation.FCButton cmdOk;
		public fecherFoundation.FCLabel lblInstructions;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.txtDate = new Global.T2KDateBox();
			this.cmdOk = new fecherFoundation.FCButton();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdOk);
			this.BottomPanel.Location = new System.Drawing.Point(0, 226);
			this.BottomPanel.Size = new System.Drawing.Size(464, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtDate);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Size = new System.Drawing.Size(464, 166);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(464, 60);
			this.TopPanel.Text = "Date Change";
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(157, 30);
			this.HeaderText.Text = "Date Change";
			// 
			// txtDate
			// 
			this.txtDate.Location = new System.Drawing.Point(30, 93);
			this.txtDate.Mask = "##/##/####";
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(122, 40);
			this.txtDate.TabIndex = 0;
			this.txtDate.Text = "  /  /";
			//this.txtDate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtDate_KeyDownEvent);
			this.txtDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtDate_Validate);
			// 
			// cmdOk
			// 
			this.cmdOk.AppearanceKey = "acceptButton";
			this.cmdOk.Location = new System.Drawing.Point(189, 36);
			this.cmdOk.Name = "cmdOk";
			this.cmdOk.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdOk.Size = new System.Drawing.Size(86, 40);
			this.cmdOk.TabIndex = 1;
			this.cmdOk.Text = "Process";
			this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(409, 43);
			this.lblInstructions.TabIndex = 3;
			this.lblInstructions.Text = "PLEASE ENTER DATE TO USE FOR PROCESSING THIS TRANSACTION";
			// 
			// frmARDateChange
			// 
			this.ClientSize = new System.Drawing.Size(464, 334);
			this.FormBorderStyle = Wisej.Web.FormBorderStyle.Fixed;
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmARDateChange";
			this.ShowInTaskbar = false;
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Date Change";
			this.Load += new System.EventHandler(this.frmARDateChange_Load);
			this.Activated += new System.EventHandler(this.frmARDateChange_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmARDateChange_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmARDateChange_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}