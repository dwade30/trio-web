//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Drawing;
using Wisej.Web;

namespace Global
{
    /// <summary>
    /// Summary description for frmAddDocument.
    /// </summary>
    partial class frmAddDocument : BaseForm
    {
        //public AxSCRIBBLELib.AxImageViewer imgPreview;
        //public dynamic imgPreview;
        public fecherFoundation.FCTextBox txtDescription;
        public fecherFoundation.FCButton cmdBrowse;
        public fecherFoundation.FCButton cmdScan;
        public fecherFoundation.FCLabel lblFile;
        public fecherFoundation.FCLabel Label1;
        public fecherFoundation.FCToolStripMenuItem mnuProcess;
        public fecherFoundation.FCToolStripMenuItem mnuFileBrowse;
        public fecherFoundation.FCToolStripMenuItem mnuFileScan;
        public fecherFoundation.FCToolStripMenuItem mnuSeperator2;
        public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
        public fecherFoundation.FCToolStripMenuItem Seperator;
        public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
                _InstancePtr = null;
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.cmdBrowse = new fecherFoundation.FCButton();
            this.cmdScan = new fecherFoundation.FCButton();
            this.lblFile = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileBrowse = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileScan = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSeperator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.fcViewerPanel1 = new fecherFoundation.FCViewerPanel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdScan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 503);
            this.BottomPanel.Size = new System.Drawing.Size(450, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fcViewerPanel1);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.cmdBrowse);
            this.ClientArea.Controls.Add(this.lblFile);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Location = new System.Drawing.Point(0, 0);
            this.ClientArea.Size = new System.Drawing.Size(450, 503);
            // 
            // TopPanel
            // 
            this.TopPanel.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.TopPanel.Dock = Wisej.Web.DockStyle.None;
            this.TopPanel.Size = new System.Drawing.Size(412, 60);
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.Location = new System.Drawing.Point(30, 66);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(400, 40);
            // 
            // cmdBrowse
            // 
            this.cmdBrowse.AppearanceKey = "actionButton";
            this.cmdBrowse.Location = new System.Drawing.Point(30, 443);
            this.cmdBrowse.Name = "cmdBrowse";
            this.cmdBrowse.Size = new System.Drawing.Size(100, 40);
            this.cmdBrowse.TabIndex = 1;
            this.cmdBrowse.Text = "Browse";
            this.cmdBrowse.Click += new System.EventHandler(this.cmdBrowse_Click);
            // 
            // cmdScan
            // 
            this.cmdScan.AppearanceKey = "actionButton";
            this.cmdScan.Location = new System.Drawing.Point(281, 443);
            this.cmdScan.Name = "cmdScan";
            this.cmdScan.Size = new System.Drawing.Size(149, 40);
            this.cmdScan.TabIndex = 2;
            this.cmdScan.Text = "Scan Document";
            this.cmdScan.Click += new System.EventHandler(this.cmdScan_Click);
            // 
            // lblFile
            // 
            this.lblFile.Location = new System.Drawing.Point(30, 126);
            this.lblFile.Name = "lblFile";
            this.lblFile.Size = new System.Drawing.Size(400, 27);
            this.lblFile.TabIndex = 4;
            this.lblFile.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(400, 16);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "DOCUMENT DESCRIPTION";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileBrowse,
            this.mnuFileScan,
            this.mnuSeperator2,
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFileBrowse
            // 
            this.mnuFileBrowse.Index = 0;
            this.mnuFileBrowse.Name = "mnuFileBrowse";
            this.mnuFileBrowse.Text = "Browse for Document";
            this.mnuFileBrowse.Click += new System.EventHandler(this.mnuFileBrowse_Click);
            // 
            // mnuFileScan
            // 
            this.mnuFileScan.Index = 1;
            this.mnuFileScan.Name = "mnuFileScan";
            this.mnuFileScan.Text = "Scan Document";
            this.mnuFileScan.Click += new System.EventHandler(this.mnuFileScan_Click);
            // 
            // mnuSeperator2
            // 
            this.mnuSeperator2.Index = 2;
            this.mnuSeperator2.Name = "mnuSeperator2";
            this.mnuSeperator2.Text = "-";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 3;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 4;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 5;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(167, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(122, 48);
            this.cmdSave.TabIndex = 8;
            this.cmdSave.Text = "Save & Exit";
            this.cmdSave.Click += new System.EventHandler(this.fcButton1_Click);
            // 
            // fcViewerPanel1
            // 
            this.fcViewerPanel1.BorderStyle = Wisej.Web.BorderStyle.Solid;
            this.fcViewerPanel1.Location = new System.Drawing.Point(30, 173);
            this.fcViewerPanel1.Name = "fcViewerPanel1";
            this.fcViewerPanel1.Size = new System.Drawing.Size(400, 250);
            this.fcViewerPanel1.TabIndex = 10;
            // 
            // frmAddDocument
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.ClientSize = new System.Drawing.Size(450, 611);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmAddDocument";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Add Document";
            this.Load += new System.EventHandler(this.frmAddDocument_Load);
            this.Activated += new System.EventHandler(this.frmAddDocument_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAddDocument_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBrowse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdScan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
        private fecherFoundation.FCButton cmdSave;
        private fecherFoundation.FCViewerPanel fcViewerPanel1;
    }
}