﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using GrapeCity.ActiveReports;
using TWSharedLibrary;

#if TWBD0000
using TWBD0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptEmailContacts : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/09/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/05/2006              *
		// ********************************************************
		clsDRWrapper rsLoad = new clsDRWrapper();
		clsDRWrapper rsGroups = new clsDRWrapper();

		public rptEmailContacts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Contacts";
            this.ReportEnd += RptEmailContacts_ReportEnd;
		}

        private void RptEmailContacts_ReportEnd(object sender, EventArgs e)
        {
            rsLoad.DisposeOf();
			rsGroups.DisposeOf();
        }

        public static rptEmailContacts InstancePtr
		{
			get
			{
				return (rptEmailContacts)Sys.GetInstance(typeof(rptEmailContacts));
			}
		}

		protected rptEmailContacts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
                rsLoad.Dispose();
                rsGroups.Dispose();
            }
            base.Dispose(disposing);
		}

		public void Init(int lngGroup, bool boolModal)
		{
			string strSQL;
			strSQL = "Select * from emailcontacts inner join emailassociations on (emailcontacts.contactid = emailassociations.contactid) ";
			if (lngGroup != 0)
			{
				strSQL += " where groupid = " + FCConvert.ToString(lngGroup);
			}
			else
			{
			}
			strSQL += " order by groupid, email";
			rsLoad.OpenRecordset(strSQL, "SystemSettings");
			if (rsLoad.EndOfFile())
			{
				MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else
			{
				//FC:FINAL:SBE - implemented in DataInitialize
				//GroupHeader1.GroupValue = rsLoad.Get_Fields("GroupID");
			}
			txtMuniName.Text = modGlobalConstants.Statics.MuniName;
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateTime.Now, "hh:mm tt");
			rsGroups.OpenRecordset("Select * from emailgroups order by groupid", "SystemSettings");
			if (boolModal)
			{
				frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "", false, false);
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsLoad.EndOfFile();
			if (!rsLoad.EndOfFile())
			{
				//FC:FINAL:SBE
				//GroupHeader1.GroupValue = rsLoad.Get_Fields("GroupID");
				this.Fields["GroupID"].Value = rsLoad.Get_Fields_Int32("GroupID");
			}
			//Detail_Format();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsLoad.EndOfFile())
			{
				txtEmail.Text = rsLoad.Get_Fields_String("Email");
				string strTemp = "";
				strTemp = FCConvert.ToString(rsLoad.Get_Fields_String("FirstName")) + " " + FCConvert.ToString(rsLoad.Get_Fields_String("LastName"));
				txtName.Text = strTemp;
				txtDescription.Text = rsLoad.Get_Fields_String("Description");
				rsLoad.MoveNext();
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			//FC:TODO:AM
			//if (rsGroups.FindFirstRecord("GroupID", GroupHeader1.GroupValue))
			{
				txtGroupName.Text = rsGroups.Get_Fields_String("Description");
				txtNickname.Text = rsGroups.Get_Fields_String("NickName");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			txtPage.Text = "Page " + this.PageNumber;
		}

		private void rptEmailContacts_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptEmailContacts.Caption	= "Contacts";
			//rptEmailContacts.Icon	= "rptEmailContacts.dsx":0000";
			//rptEmailContacts.Left	= 0;
			//rptEmailContacts.Top	= 0;
			//rptEmailContacts.Width	= 11880;
			//rptEmailContacts.Height	= 8490;
			//rptEmailContacts.StartUpPosition	= 3;
			//rptEmailContacts.SectionData	= "rptEmailContacts.dsx":058A;
			//End Unmaped Properties
		}

		private void RptEmailContacts_DataInitialize(object sender, System.EventArgs e)
		{
			this.Fields.Add("GroupID");
		}
	}
}
