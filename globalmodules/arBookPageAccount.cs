﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TWSharedLibrary;
#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#else
using TWBD0000;
#endif
namespace Global
{
    /// <summary>
    /// Summary description for SectionReport1.
    /// </summary>
    public partial class arBookPageAccount : BaseSectionReport
    {
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               07/27/2005              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               06/07/2006              *
        // ********************************************************
        clsDRWrapper rsData = new clsDRWrapper();
        public int lngYear1;
        public int lngYear2;
        public int lngAcct1;
        public int lngAcct2;

        public arBookPageAccount()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            if (_InstancePtr == null)
                _InstancePtr = this;
            this.Name = "Account Detail";
            this.ReportEnd += ArBookPageAccount_ReportEnd;
            this.Disposed += ArBookPageAccount_Disposed;
        }

        private void ArBookPageAccount_Disposed(object sender, EventArgs e)
        {
            rsData.Dispose();
        }

        private void ArBookPageAccount_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        public static arBookPageAccount InstancePtr
        {
            get
            {
                return (arBookPageAccount)Sys.GetInstance(typeof(arBookPageAccount));
            }
        }

        protected arBookPageAccount _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            rsData.DisposeOf();
            base.Dispose(disposing);
        }
        // VBto upgrade warning: lngFirstAccount As int	OnWrite(int, double)
        // VBto upgrade warning: lngLastAccount As int	OnWrite(int, double)
        public void Init(int lngFirstAccount, int lngLastAccount, int lngFirstYear = 0, int lngLastYear = 0, int lngEligible = 0)
        {
            string strSQL;
            string strEligible = "";
            if (lngEligible != 0)
            {
                strEligible = " AND (LienProcessStatus = 3 OR LienProcessStatus = 2) AND (Taxdue1 + Taxdue2 + Taxdue3 + Taxdue4) > PrincipalPaid";
            }
            strSQL = "SELECT DISTINCT RSAccount, DeedName1 as Own1FullName, OwnerPartyID FROM " + rsData.CurrentPrefix + "RealEstate.dbo.Master INNER JOIN BillingMaster ON Master.RSAccount = BillingMaster.Account WHERE RSCard = 1 AND RSAccount >= " + FCConvert.ToString(lngFirstAccount) + " AND RSAccount <= " + FCConvert.ToString(lngLastAccount) + strEligible + " ORDER BY RSAccount";
            // strSQL = "SELECT RSAccount, RSName FROM Master WHERE RSCard = 1 AND RSAccount >= " & lngFirstAccount & " AND RSAccount <= " & lngLastAccount & " ORDER BY RSAccount"
            try
            {
                if (rsData == null) rsData = new clsDRWrapper();
                rsData.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
                if (!rsData.EndOfFile())
                {
                    this.Name = "Book Page Report";
                    lngYear1 = lngFirstYear;
                    lngYear2 = lngLastYear;
                    lngAcct1 = lngFirstAccount;
                    lngAcct2 = lngLastAccount;
                    SetReportHeader();
                    frmReportViewer.InstancePtr.Init(this);
                }
                else
                {
                    FCMessageBox.Show("No matching accounts.", MsgBoxStyle.Exclamation, "No Accounts");
                    rsData.DisposeOf();
                    Cancel();
                    return;
                }
            }
            finally
            {
               // rsData.DisposeOf();
            }

        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = rsData.EndOfFile();
            //Detail_Format();
        }

        private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
        {
            //if (KeyCode == vbKeyEscape)
            //{
            //	Close();
            //}
        }

        private void ActiveReport_ReportEnd(object sender, EventArgs e)
        {
            frmWait.InstancePtr.Unload();
            rsData.DisposeOf();
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            //modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
            lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
            lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
            lblMuniName.Text = modGlobalConstants.Statics.MuniName;
            frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Book and Page", true, rsData.RecordCount(), true);
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            BindFields();
        }

        private void PageHeader_Format(object sender, EventArgs e)
        {
            lblPage.Text = "Page " + this.PageNumber;
        }

        private void BindFields()
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                int lngAcct = 0;
                if (!rsData.EndOfFile())
                {
                    frmWait.InstancePtr.IncrementProgress();
                    lngAcct = FCConvert.ToInt32(rsData.Get_Fields_Int32("RSAccount"));
                    lblAccount.Text = lngAcct.ToString();
                    // TODO: Field [Own1FullName] not found!! (maybe it is an alias?)
                    lblName.Text = FCConvert.ToString(rsData.Get_Fields("Own1FullName"));
                    // This will set all of the sub reports
                    sarBookPageREob.Report = new sarBookPageRE();
                    sarBookPageREob.Report.UserData = lngAcct;
                    sarBookPageMHob.Report = new sarBookPageMH();
                    sarBookPageMHob.Report.UserData = lngAcct;
                    if (modGlobalConstants.Statics.gboolCL || modGlobalConstants.Statics.gboolBL)
                    {
                        sarBookPageLNob.Report = new sarBookPageLN();
                        sarBookPageLNob.Report.UserData = lngAcct;
                        sarBookPageLDNob.Report = new sarBookPageLDN();
                        sarBookPageLDNob.Report.UserData = lngAcct;
                    }
                    rsData.MoveNext();
                }
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Book Page Report - Account");
            }
        }

        private void SetReportHeader()
        {
            string strYear = "";
            string strAcct = "";
            // set the report type caption
            if (lngAcct1 == lngAcct2)
            {
                strAcct = "Account : " + FCConvert.ToString(lngAcct1);
            }
            else
            {
                strAcct = "Account : " + FCConvert.ToString(lngAcct1) + " to " + FCConvert.ToString(lngAcct2);
            }
            if (lngYear1 != 0)
            {
                if (lngYear1 == lngYear2)
                {
                    strYear = "Year : " + FormatBillingYear_2(lngYear1.ToString());
                }
                else
                {
                    strYear = "Year : " + FormatBillingYear_2(lngYear1.ToString()) + " to " + FormatBillingYear_2(lngYear2.ToString());
                }
            }
            else
            {
                strYear = "";
            }
            lblReportType.Text = Strings.Trim(strAcct + "  " + strYear);
        }

        private string FormatBillingYear_2(string strYear)
        {
            return FormatBillingYear(ref strYear);
        }

        private string FormatBillingYear(ref string strYear)
        {
            string FormatBillingYear = "";
            if (Strings.InStr(1, strYear, "-") > 0)
            {
                if (Strings.InStr(1, strYear, "*") > 0)
                {
                    FormatBillingYear = Strings.Left(strYear, 4) + FCConvert.ToString(Conversion.Val(Strings.Right(strYear, 2)));
                }
                else
                {
                    FormatBillingYear = Strings.Left(strYear, 4) + Strings.Right(strYear, 1);
                }
            }
            else
            {
                if (Strings.InStr(1, strYear, "*") > 0)
                {
                    FormatBillingYear = Strings.Left(strYear, 4) + "-" + FCConvert.ToString(Conversion.Val(Strings.Right(strYear, 2)));
                }
                else
                {
                    FormatBillingYear = Strings.Left(strYear, 4) + "-" + Strings.Right(strYear, 1);
                }
            }
            return FormatBillingYear;
        }

        private void arBookPageAccount_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //arBookPageAccount.Text	= "Account Detail";
            //arBookPageAccount.Icon	= "sarBookPageAccount.dsx":0000";
            //arBookPageAccount.Left	= 0;
            //arBookPageAccount.Top	= 0;
            //arBookPageAccount.Width	= 11880;
            //arBookPageAccount.Height	= 8595;
            //arBookPageAccount.StartUpPosition	= 3;
            //arBookPageAccount.SectionData	= "sarBookPageAccount.dsx":058A;
            //End Unmaped Properties
        }
    }
}
