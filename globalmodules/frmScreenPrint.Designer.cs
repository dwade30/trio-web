﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmScreenPrint.
	/// </summary>
	partial class frmScreenPrint : BaseForm
	{
		public FCCommonDialog CommonDialog1_Save;
		public FCCommonDialog CommonDialog1;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdCancel;
		public FCGrid Grid1;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			this.CommonDialog1_Save = new fecherFoundation.FCCommonDialog();
			this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.Grid1 = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid1)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 357);
			this.BottomPanel.Size = new System.Drawing.Size(430, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.Grid1);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(430, 297);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(430, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(139, 30);
			this.HeaderText.Text = "Print Forms";
			// 
			// CommonDialog1_Save
			// 
			this.CommonDialog1_Save.Color = System.Drawing.Color.Black;
			this.CommonDialog1_Save.DefaultExt = null;
			this.CommonDialog1_Save.FilterIndex = ((short)(0));
			this.CommonDialog1_Save.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.CommonDialog1_Save.FontName = "Microsoft Sans Serif";
			this.CommonDialog1_Save.FontSize = 8.25F;
			this.CommonDialog1_Save.ForeColor = System.Drawing.Color.Black;
			this.CommonDialog1_Save.FromPage = 0;
			this.CommonDialog1_Save.Location = new System.Drawing.Point(0, 0);
			this.CommonDialog1_Save.Name = "CommonDialog1_Save";
			this.CommonDialog1_Save.PrinterSettings = null;
			this.CommonDialog1_Save.Size = new System.Drawing.Size(0, 0);
			this.CommonDialog1_Save.TabIndex = 0;
			this.CommonDialog1_Save.ToPage = 0;
			// 
			// CommonDialog1
			// 
			this.CommonDialog1.Color = System.Drawing.Color.Black;
			this.CommonDialog1.DefaultExt = null;
			this.CommonDialog1.FilterIndex = ((short)(0));
			this.CommonDialog1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			this.CommonDialog1.FontName = "Microsoft Sans Serif";
			this.CommonDialog1.FontSize = 8.25F;
			this.CommonDialog1.ForeColor = System.Drawing.Color.Black;
			this.CommonDialog1.FromPage = 0;
			this.CommonDialog1.Location = new System.Drawing.Point(0, 0);
			this.CommonDialog1.Name = "CommonDialog1";
			this.CommonDialog1.PrinterSettings = null;
			this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
			this.CommonDialog1.TabIndex = 0;
			this.CommonDialog1.ToPage = 0;
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.ForeColor = System.Drawing.Color.White;
			this.cmdPrint.Location = new System.Drawing.Point(171, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Size = new System.Drawing.Size(76, 48);
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.TabIndex = 2;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "toolbarButton";
			this.cmdCancel.Location = new System.Drawing.Point(287, 261);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(88, 25);
			this.cmdCancel.TabIndex = 3;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// Grid1
			// 
			this.Grid1.AllowSelection = false;
			this.Grid1.AllowUserToResizeColumns = false;
			this.Grid1.AllowUserToResizeRows = false;
			this.Grid1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.Grid1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.Grid1.BackColorAlternate = System.Drawing.Color.Empty;
			this.Grid1.BackColorBkg = System.Drawing.Color.Empty;
			this.Grid1.BackColorFixed = System.Drawing.Color.Empty;
			this.Grid1.BackColorSel = System.Drawing.Color.Empty;
			this.Grid1.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.Grid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.Grid1.ColumnHeadersHeight = 30;
			this.Grid1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.Grid1.DefaultCellStyle = dataGridViewCellStyle2;
			this.Grid1.DragIcon = null;
			this.Grid1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.Grid1.FixedCols = 0;
			this.Grid1.ForeColorFixed = System.Drawing.Color.Empty;
			this.Grid1.FrozenCols = 0;
			this.Grid1.GridColor = System.Drawing.Color.Empty;
			this.Grid1.GridColorFixed = System.Drawing.Color.Empty;
			this.Grid1.Location = new System.Drawing.Point(30, 70);
			this.Grid1.Name = "Grid1";
			this.Grid1.ReadOnly = true;
			this.Grid1.RowHeadersVisible = false;
			this.Grid1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.Grid1.RowHeightMin = 0;
			this.Grid1.Rows = 50;
			this.Grid1.ScrollTipText = null;
			this.Grid1.ShowColumnVisibilityMenu = false;
			this.Grid1.Size = new System.Drawing.Size(371, 186);
			this.Grid1.StandardTab = true;
			this.Grid1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.Grid1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.Grid1.TabIndex = 1;
			this.Grid1.Click += new System.EventHandler(this.Grid1_ClickEvent);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(352, 21);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "HIGHLIGHT THE SCREENS YOU WISH TO PRINT";
			// 
			// frmScreenPrint
			// 
			this.ClientSize = new System.Drawing.Size(430, 465);
			this.Name = "frmScreenPrint";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Print Forms";
			this.Load += new System.EventHandler(this.frmScreenPrint_Load);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Grid1)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
