﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptMHWithUTAccounts : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/21/2004              *
		// *
		// MODIFIED BY    :               Melissa Lamprecht       *
		// LAST UPDATED   :               09/05/2007              *
		// ********************************************************
		clsDRWrapper clsUT = new clsDRWrapper();
		clsDRWrapper clsMortgage = new clsDRWrapper();
		//clsDRWrapper clsAssoc = new clsDRWrapper();
		bool boolTax;
		// whether we should print tax info or not
		//clsDRWrapper clsTaxRE = new clsDRWrapper();
		int lngBYear;
		// billing year
		public rptMHWithUTAccounts()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Utility Account List by Mortgage Holder";
		}

		public static rptMHWithUTAccounts InstancePtr
		{
			get
			{
				return (rptMHWithUTAccounts)Sys.GetInstance(typeof(rptMHWithUTAccounts));
			}
		}

		protected rptMHWithUTAccounts _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsMortgage.Dispose();
				clsUT.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("TheBinder");
			this.Fields["TheBinder"].Value = 0;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsUT.EndOfFile();
			if (!eArgs.EOF)
			{
				// do it here or the groupheader info will be behind the times
				this.Fields["TheBinder"].Value = clsUT.Get_Fields_Int32("MortgageHolderID");
				// "MortAssoc.MortgageHolderID")
			}
			//Detail_Format();
		}

		public void Init(bool boolNameOrder, string strStart = "", string strEnd = "", bool boolSingleAccount = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSQL = "";
				string strOrder = "";
				string str1 = "";
				string str2 = "";
				string str3 = "";
				string str4 = "";
				string strWhere = "";
				string strDBName;
				// MAL@20070905: Changed Existing Code (borrowed from Real Estate report) to conform with Utility Billing
				// MortAssoc = MortgageAssociation Table
				if (boolSingleAccount)
				{
					txtTitle.Text = "Mortgage Holder " + strStart + " Information";
					fldReportName.Text = "with Utility Accounts";
				}
				else
				{
					txtTitle.Text = "Mortgage Holder List";
					fldReportName.Text = "with Utility Accounts";
				}
				if (boolNameOrder)
				{
					if (strEnd == string.Empty)
					{
						if (strStart == string.Empty)
						{
							strWhere = "";
						}
						else
						{
							strWhere = " and mortgageholders.name >= '" + strStart + "' ";
						}
					}
					else
					{
						strWhere = " AND mortgageholders.name >= '" + strStart + "    ' AND mortgageholders.name <= '" + strEnd + "zzzzz' ";
					}
				}
				else
				{
					if (strEnd == string.Empty)
					{
						if (strStart == string.Empty)
						{
							strWhere = "";
						}
						else
						{
							strWhere = " AND MortgageAssociation.mortgageholderid >= " + FCConvert.ToString(Conversion.Val(strStart)) + " ";
						}
					}
					else
					{
						strWhere = " AND MortgageAssociation.mortgageholderid >= " + FCConvert.ToString(Conversion.Val(strStart)) + " AND MortgageAssociation.mortgageholderid <= " + FCConvert.ToString(Conversion.Val(strEnd)) + " ";
					}
				}
				if (boolNameOrder)
				{
					// kgk CP InsertName
					strOrder = " ORDER BY Mortgageholders.Name, MortgageAssociation.Mortgageholderid ";
					// , Master.OwnerName "
				}
				else
				{
					strOrder = " ORDER BY MortgageAssociation.Mortgageholderid, ";
					// Master.OwnerName "
				}
				strDBName = clsUT.Get_GetFullDBName("CentralData") + ".dbo.";
				clsUT.OpenRecordset("SELECT * FROM Master INNER JOIN (" + strDBName + "MortgageHolders INNER JOIN " + strDBName + "MortgageAssociation " + "ON (MortgageAssociation.MortgageHolderID = MortgageHolders.ID)) ON (MortgageAssociation.Account = Master.AccountNumber) " + "where MortgageAssociation.Module = 'UT' AND Not (Master.Deleted = 1) " + strWhere + strOrder, "twut0000.vb1");
				// kgk trout-840 Add UT db  ', "twre0000.vb1")  trouts-226 kjr 04.12.17 chg MortAssoc to MortgageAssociation
				if (clsUT.EndOfFile())
				{
					FCMessageBox.Show("No records found.", MsgBoxStyle.Information, "No Match");
					Cancel();
					return;
				}
				clsUT.InsertName("OwnerPartyID", "Own1", false, true, false, "", false, "", true);
				if (boolNameOrder)
				{
					clsUT.ReOrder("Mortgageholders.Name, MortgageAssociation.Mortgageholderid, Own1FullName");
				}
				else
				{
					clsUT.ReOrder("MortgageAssociation.Mortgageholderid, Own1FullName");
				}
				// Me.Show , MDIParent
				frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", false, "", "TRIO Software", false, true, "", false, false);
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + "\r\n" + "In Init", MsgBoxStyle.Critical, "Error");
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strAddress1 = "";
			string strAddress2 = "";
			string strAddress3 = "";
			string strAddress4 = "";
			if (!clsUT.EndOfFile())
			{
				// set the group binder
				this.Fields["TheBinder"].Value = clsUT.Get_Fields_Int32("MortgageHolderID");
				//FC:FINAL:MSH - can't implicitly convert from int to string
				// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				txtaccount.Text = FCConvert.ToString(clsUT.Get_Fields("AccountNumber"));
				// TODO: Field [Own1FullName] not found!! (maybe it is an alias?)
				txtName.Text = clsUT.Get_Fields("Own1FullName");
				// clsUT.Fields("OwnerName")
				txtLocation.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsUT.Get_Fields_Int32("StreetNumber")) + " " + FCConvert.ToString(clsUT.Get_Fields_String("Apt"))) + " " + FCConvert.ToString(clsUT.Get_Fields_String("StreetName")));
				txtMapLot.Text = Strings.Trim(FCConvert.ToString(clsUT.Get_Fields_String("MapLot")));
				// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(clsUT.Get_Fields("Book"))) == "" || Strings.Trim(FCConvert.ToString(clsUT.Get_Fields("Page"))) == "")
				{
					fldBookPage.Text = "";
				}
				else
				{
					// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
					fldBookPage.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(clsUT.Get_Fields("Book"))) + "\\" + Strings.Trim(FCConvert.ToString(clsUT.Get_Fields("Page"))));
				}
				clsUT.MoveNext();
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			string strAddress1;
			string strAddress2;
			string strAddress3;
			string strAddress4;
			int intRowCt = 0;
			clsMortgage.OpenRecordset("Select * from MortgageHolders where ID = " + this.Fields["TheBinder"].Value, "CentralData");
			// trouts-226 kjr 04.13.2017 "twut0000.vb1")  'kgk trout-840 Add UT db  ', "twre0000.vb1")
			if (clsMortgage.EndOfFile())
				return;
			txtNumber.Text = this.Fields["TheBinder"].Value.ToString();
			txtMortgageName.Text = clsMortgage.Get_Fields_String("Name");
			strAddress1 = FCConvert.ToString(clsMortgage.Get_Fields_String("Address1"));
			strAddress2 = FCConvert.ToString(clsMortgage.Get_Fields_String("Address2"));
			strAddress3 = FCConvert.ToString(clsMortgage.Get_Fields_String("Address3"));
			if (strAddress3 == "0")
				strAddress3 = "";
			strAddress4 = Strings.Trim(FCConvert.ToString(clsMortgage.Get_Fields_String("City")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("State")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("Zip")) + " " + FCConvert.ToString(clsMortgage.Get_Fields_String("Zip4")));
			// now condense this info
			if (strAddress1 == string.Empty)
			{
				strAddress1 = strAddress2;
				strAddress2 = strAddress3;
				strAddress3 = strAddress4;
				strAddress4 = "";
				intRowCt += 1;
				if (strAddress2 == string.Empty)
				{
					strAddress2 = strAddress3;
					strAddress3 = strAddress4;
					strAddress4 = "";
					// intRowCt = intRowCt + 1
					if (strAddress2 == string.Empty)
					{
						strAddress2 = strAddress3;
						strAddress3 = strAddress4;
						strAddress4 = "";
						// intRowCt = intRowCt + 1
					}
				}
			}
			if (strAddress2 == string.Empty)
			{
				strAddress2 = strAddress3;
				strAddress3 = strAddress4;
				strAddress4 = "";
				intRowCt += 1;
				if (strAddress2 == string.Empty)
				{
					strAddress2 = strAddress3;
					strAddress3 = strAddress4;
					strAddress4 = "";
					// intRowCt = intRowCt + 1
				}
			}
			if (strAddress3 == string.Empty)
			{
				strAddress3 = strAddress4;
				strAddress4 = "";
				intRowCt += 1;
			}
			txtAddress1.Text = strAddress1;
			txtAddress2.Text = strAddress2;
			txtAddress3.Text = strAddress3;
			txtAddress4.Text = strAddress4;
			lblAccount.Top = txtAddress1.Top + ((4 - intRowCt) * txtAddress1.Height);
			lblName.Top = lblAccount.Top;
			lblMapLot.Top = lblAccount.Top;
			lblLocation.Top = lblAccount.Top;
			lblBookPage.Top = lblAccount.Top;
			lnAccountHeader.Y1 = lblAccount.Top + lblAccount.Height;
			lnAccountHeader.Y2 = lnAccountHeader.Y1;
			GroupHeader1.Height = lnAccountHeader.Y1 + 20 / 1440F;
			// Detail.Height = lnAccountHeader.Y1 + 3
		}

		
	}
}
