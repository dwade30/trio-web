﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;

#if TWAR0000
using TWAR0000;


#elif TWCR0000
using TWCR0000;

#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmARCLStatus.
	/// </summary>
	public partial class frmARCLStatus : BaseForm
	{
		public frmARCLStatus()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label1 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblCode = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label1.AddControlArrayElement(Label1_9, FCConvert.ToInt16(9));
			this.Label1.AddControlArrayElement(Label1_11, FCConvert.ToInt16(11));
			this.Label1.AddControlArrayElement(Label1_1, FCConvert.ToInt16(1));
			this.Label1.AddControlArrayElement(Label1_10, FCConvert.ToInt16(10));
			this.Label1.AddControlArrayElement(Label1_8, FCConvert.ToInt16(8));
			this.Label1.AddControlArrayElement(Label1_12, FCConvert.ToInt16(12));
			//this.Label1.AddControlArrayElement(Label1_0, FCConvert.ToInt16(0));
			this.lblCode.AddControlArrayElement(lblCode_2, FCConvert.ToInt16(2));
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmARCLStatus InstancePtr
		{
			get
			{
				return (frmARCLStatus)Sys.GetInstance(typeof(frmARCLStatus));
			}
		}

		protected frmARCLStatus _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/22/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/03/2006              *
		// ********************************************************
		clsDRWrapper rsCL = new clsDRWrapper();
		// Collections Recordset
		string CurrentOwner = "";
		string strSQL;
		// Temporary Holding Variable for SQL Statements
		string DataName = "";
		int intHoldBill;
		public bool FormLoaded;
		// true if form is loaded so that it can be activated many times without resetting the grid
		public bool FormReset;
		// if the effective date gets changed, then this gets set to true to allow the form to recreate the grid
		double curCurrentInterest;
		// vbPorter upgrade warning: curPrincipalDue As double	OnWriteFCConvert.ToDecimal(
		double curPrincipalDue;
		// vbPorter upgrade warning: curTaxDue As double	OnWriteFCConvert.ToDecimal(
		double curTaxDue;
		// vbPorter upgrade warning: bcode As FixedString	OnWrite(string)
		char[] bcode = new char[4];
		// Billing Code
		// vbPorter upgrade warning: OriginalBillDate As DateTime	OnWrite(DateTime, string)
		DateTime OriginalBillDate;
		int CurRow;
		// This holds the current line number of the flexgrid
		int MaxRows;
		// Holds the max rows in the grid
		bool CollapseFlag;
		// Tells the collapse routine to fire or not
		int ParentLine;
		// Holds the line number of the last header line (used in swapping and holding totals)
		// vbPorter upgrade warning: sumPrin As double	OnWrite(Decimal, short)
		double sumPrin;
		// these sums will keep track of the account totals for the bottom subtotal line
		// vbPorter upgrade warning: sumTax As double	OnWrite(Decimal, short)
		double sumTax;
		// vbPorter upgrade warning: sumInt As double	OnWrite(Decimal, short)
		double sumInt;
		// vbPorter upgrade warning: sumTotal As double	OnWrite(Decimal, short)
		double sumTotal;
		bool boolPayment;
		// true if this is the payment screen false if status
		bool boolRefKeystroke;
		// this is true if a keystroke is used to get into cmbPeriod
		bool boolNegPayments;
		string strStatusString = "";
		// this just holds the string for the menu options
		string strPaymentString = "";
		// this just holds the string for the menu options
		int lngGroupNumber;
		string strGroupComment = "";
		public bool boolUnloadOK;
		// this is to check to see if the user really wants to leave the screen with pending activity
		public double dblTotalPerDiem;
		// this is the sum of each year per diem to be shown with the rate info
		bool boolPop;
		// this is true when there is a priority comment on this account
		string strNoteText = "";
		// this is where the note text will be stored
		string strMailingAddress1 = "";
		// this will store the mailing address from RE
		string strMailingAddress2 = "";
		string strCurrentBookPage = "";
		public bool boolNoCurrentInt;
		// this will be set by PaymentRecords if there is a tax club so that current interest is no calculated
		public bool boolCollapsedInputBoxes;
		public bool boolDoNotContinueSave;
		public int lngARResCode;
		clsGridAccount clsAcct = new clsGridAccount();
		public int lngGRIDCOLTree;
		public int lngGRIDColInvoiceNumber;
		public int lngGridColBillKey;
		public int lngGRIDColRateKey;
		public int lngGRIDColDate;
		public int lngGRIDColRef;
		public int lngGRIDColPaymentCode;
		public int lngGRIDColPrincipal;
		public int lngGRIDColPTC;
		public int lngGRIDColTax;
		public int lngGRIDColInterest;
		public int lngGRIDColTotal;
		public int lngGRIDColLineCode;
		public int lngGRIDColPending;
		public int lngGRIDColPaymentKey;
		public int lngGRIDColCHGINTNumber;
		public int lngGRIDColPerDiem;
		public int lngPayGridColInvoice;
		public int lngPayGridColBillID;
		public int lngPayGridColDate;
		public int lngPayGridColRef;
		public int lngPayGridColCode;
		public int lngPayGridColCDAC;
		public int lngPayGridColPrincipal;
		public int lngPayGridColTax;
		public int lngPayGridColInterest;
		public int lngPayGridColArrayIndex;
		public int lngPayGridColKeyNumber;
		public int lngPayGridColCHGINTNumber;
		public int lngPayGridColTotal;
		public int OwnerPartyID;
		// CR Central Party Interface
		cPartyController pCont = new cPartyController();
		// vbPorter upgrade warning: pInfo As CParty	OnWrite(cParty)
		cParty pInfo = new cParty();
		// vbPorter upgrade warning: intPassType As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: lngPassAcctKey As int	OnWrite(double, int)
		public void Init(short intPassType, int lngPassAcctKey, bool boolShowMissingAccount = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will start this form
				clsDRWrapper rsTemp = new clsDRWrapper();
				if (intPassType == 1)
				{
					boolPayment = false;
				}
				else
				{
					boolPayment = true;
				}
				// Check to see if this account has been created
				rsTemp.OpenRecordset("SELECT CustomerID FROM CustomerMaster WHERE CustomerID = " + FCConvert.ToString(lngPassAcctKey), modExtraModules.strARDatabase);
				if (!rsTemp.EndOfFile())
				{
					modARStatusPayments.Statics.lngCurrentCustomerIDAR = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("CustomerID"));
					CreateStatusQueries(ref modARStatusPayments.Statics.lngCurrentCustomerIDAR);
				}
				else
				{
					if (boolShowMissingAccount)
					{
						MessageBox.Show("This account does not exist.", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					Close();
					return;
				}
				frmWait.InstancePtr.Unload();
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Status Screen", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CreateStatusQueries(ref int lngAcct)
		{
			// this will create the two queries strings that the status screen will use
			string strSQL;
			int lngTempAcct;
			// lngTempAcct = GetAccountKeyFromAccountNumber(lngAcct)
			// this will load all of the bills
			strSQL = "SELECT * FROM Bill WHERE BillStatus <> 'V' AND ActualAccountNumber = " + FCConvert.ToString(lngAcct);
			// BillStatus = 'B' AND
			modARStatusPayments.Statics.strARCurrentAccountBills = strSQL;
			// this will load all of the account payments
			strSQL = "SELECT * FROM PaymentRec WHERE ActualAccountNumber = " + FCConvert.ToString(lngAcct);
			modARStatusPayments.Statics.strARCurrentAccountPayments = strSQL;
		}

		private void chkShowAll_CheckedChanged(object sender, System.EventArgs e)
		{
			FormReset = true;
			Form_Activate();
			////Application.DoEvents();
			FormReset = false;
			if (fraPayment.Visible == true)
			{
				fraPayment.Visible = false;
				fraPayment.Visible = true;
			}
		}

		private void cmbCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strTemp;
			int lngBillKey;
			strTemp = Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1);
			if (modARStatusPayments.ARYearCodeValidate())
			{
				if (strTemp == "P")
				{
					// Payment
					txtCash.Text = "Y";
					txtCD.Text = "Y";
					txtAcctNumber.TextMatrix(0, 0, "");
					txtAcctNumber.Enabled = false;
					// set the boxes
					InputBoxAdjustment_2(true);
				}
				else if (strTemp == "C")
				{
					// set the boxes
					InputBoxAdjustment_2(false);
				}
				else if (strTemp == "Y")
				{
					// Prepayment
					if (Strings.Right(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString(), 1) != "*" && Conversion.Val(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString()) != 0)
					{
						cmbCode.SelectedIndex = 1;
					}
					else
					{
						txtCash.Text = "Y";
						txtCD.Text = "Y";
						txtAcctNumber.TextMatrix(0, 0, "");
						txtAcctNumber.Enabled = false;
					}
					// set the boxes
					InputBoxAdjustment_2(true);
				}
				else
				{
					txtCash.Text = "Y";
					txtCD.Text = "Y";
					txtAcctNumber.TextMatrix(0, 0, "");
					txtAcctNumber.Enabled = false;
					// set the boxes
					InputBoxAdjustment_2(true);
				}
			}
			else
			{
				cmbCode.SelectedIndex = 1;
			}
			CheckCash();
		}

		private void cmbCode_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbCode.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}

		private void cmbCode_Enter(object sender, System.EventArgs e)
		{
			boolRefKeystroke = true;
		}

		private void cmbCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// left = 37
			// right = 39
			switch (KeyCode)
			{
				case Keys.Left:
					{
						boolRefKeystroke = true;
						txtReference.Focus();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						if (txtCD.Visible && txtCD.Enabled)
						{
							txtCD.Focus();
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbCode.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbCode.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbBillNumber_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// this sub will show the totals line above the payment boxes with period information
			clsDRWrapper rsYear = new clsDRWrapper();
			int TotLine = 0;
			string strSQL = "";
			string strInvoice = "";
			int lngBill = 0;
			// vbPorter upgrade warning: Tax1 As Decimal	OnWrite(double, Decimal)
			Decimal Tax1;
			if (cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString() == "Auto")
			{
				if (!modARStatusPayments.ARYearCodeValidate())
				{
					cmbCode.SelectedIndex = 1;
				}
				vsPeriod.TextMatrix(0, 0, "Total Due:");
				if (GRID.TextMatrix(GRID.Rows - 1, lngGRIDColTotal) == "Total")
				{
					vsPeriod.TextMatrix(0, 1, Strings.Format(0, "#,##0.00"));
				}
				else
				{
					vsPeriod.TextMatrix(0, 1, Strings.Format(GRID.TextMatrix(GRID.Rows - 1, lngGRIDColTotal), "#,##0.00"));
				}
				vsPeriod.TextMatrix(0, 2, "");
				vsPeriod.TextMatrix(0, 3, "");
				vsPeriod.TextMatrix(0, 4, "");
			}
			else
			{
				strInvoice = cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString();
				TotLine = (NextSpacerLine(GRID.FindRow(strInvoice, 1, lngGRIDColInvoiceNumber)) - 1);
				if (TotLine > 0)
				{
					strSQL = "SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountBills + ") as temp WHERE InvoiceNumber = '" + strInvoice + "'";
					rsYear.OpenRecordset(strSQL, modExtraModules.strARDatabase);
					if (rsYear.EndOfFile() != true)
					{
						lngBill = FCConvert.ToInt32(rsYear.Get_Fields_Int32("ID"));
						Tax1 = rsYear.Get_Fields_Decimal("PrinOwed") + rsYear.Get_Fields_Decimal("TaxOwed") - FCConvert.ToDecimal(modARStatusPayments.Statics.dblARCurrentInt[lngBill]);
						// DJW changed + to - when adding in interest since it is a negative number to start
						// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
						Tax1 -= rsYear.Get_Fields("IntAdded");
						// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
						Tax1 += -rsYear.Get_Fields("PrinPaid") - rsYear.Get_Fields("TaxPaid") - rsYear.Get_Fields("IntPaid");
						vsPeriod.TextMatrix(0, 0, "Total Due:");
						vsPeriod.TextMatrix(0, 1, Strings.Format(Tax1, "#,##0.00"));
					}
				}
			}
			rsYear.Reset();
			CheckCash();
		}

		public void cmbBillNumber_Click()
		{
			cmbBillNumber_SelectedIndexChanged(cmbBillNumber, new System.EventArgs());
		}

		private void cmbBillNumber_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbBillNumber.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 150, 0);
		}

		private void cmbBillNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtTransactionDate.Visible && txtTransactionDate.Enabled)
						{
							txtTransactionDate.Focus();
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Return:
					{
						// if they hit enter, move them to the next field
						break;
					}
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbBillNumber.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbBillNumber.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmdRIClose_Click(object sender, System.EventArgs e)
		{
			fraRateInfo.Visible = false;
		}

		public void cmdRIClose_Click()
		{
			cmdRIClose_Click(cmdRIClose, new System.EventArgs());
		}

		private void frmARCLStatus_Activated(object sender, System.EventArgs e)
		{
			bool boolContinue = false;
			mnuProcess.Enabled = false;
			if (!modExtraModules.IsThisCR())
			{
				if (modGlobalConstants.Statics.gboolCR)
				{
					mnuProcessGoTo.Enabled = false;
					mnuProcessGoTo.Visible = false;
				}
				if (FormReset)
				{
					ClearARPaymentGrid();
					frmARCLStatus.InstancePtr.Text = "Account Receivable Account Status as of " + Strings.Format(modARStatusPayments.Statics.AREffectiveDate, "MM/dd/yyyy");
					Reset_Sum();
				}
				else
				{
					modARStatusPayments.Statics.AREffectiveDate = FCConvert.ToDateTime(Strings.Format(modARStatusPayments.Statics.AREffectiveDate, "MM/dd/yyyy"));
					frmARCLStatus.InstancePtr.Text = "Account Receivable Account Status as of " + Strings.Format(modARStatusPayments.Statics.AREffectiveDate, "MM/dd/yyyy");
				}
			}
			else
			{
				mnuProcessGoTo.Enabled = false;
				mnuProcessGoTo.Visible = false;
				if (FormReset)
				{
					frmARCLStatus.InstancePtr.Text = "Account Receivable Account Status as of " + Strings.Format(modARStatusPayments.Statics.AREffectiveDate, "MM/dd/yyyy");
					Reset_Sum();
				}
				else
				{
					// kk03262015 trocrs-21  Form isn't unloaded for CR, need to handle form losing/getting focus and starting new
					if (!FormLoaded)
					{
						modARStatusPayments.Statics.AREffectiveDate = FCConvert.ToDateTime(Strings.Format(DateTime.Now, "MM/dd/yyyy"));
					}
					else
					{
						modARStatusPayments.Statics.AREffectiveDate = FCConvert.ToDateTime(Strings.Format(modARStatusPayments.Statics.AREffectiveDate, "MM/dd/yyyy"));
					}
					frmARCLStatus.InstancePtr.Text = "Account Receivable Account Status as of " + Strings.Format(modARStatusPayments.Statics.AREffectiveDate, "MM/dd/yyyy");
				}
			}
			if (FormLoaded && !FormReset)
			{
			}
			else
			{
				FormLoaded = true;
				FormReset = false;
				CollapseFlag = true;
				// reset all of the arrays in case the form load did not fire
				rsCL.Reset();
				FCUtils.EraseSafe(modARStatusPayments.Statics.ARPaymentArray);
				FCUtils.EraseSafe(modARStatusPayments.Statics.dblARCurrentInt);
				FCUtils.EraseSafe(modARStatusPayments.Statics.dateAROldDate);
				GRID.Rows = 1;
				// Doevents
				boolContinue = StartUp();
				// this start the computation
				// check for a note
				strNoteText = Strings.Trim(GetAccountNote(ref modARStatusPayments.Statics.lngCurrentCustomerIDAR, ref boolPop));
				if (boolContinue)
				{
					GRID.Outline(1);
					// collapses the grid to level zero (one)
					CollapseFlag = false;
				}
				else
				{
					// account does not exist so go back to selection screen
					if (boolPayment)
					{
						frmARGetMasterAccount.InstancePtr.Init(2);
					}
					else
					{
						frmARGetMasterAccount.InstancePtr.Init(1);
					}
					frmWait.InstancePtr.Unload();
					MessageBox.Show("No billing records exist for this account.", "No Billing Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					Close();
					return;
				}
				if (boolPayment)
				{
					modARStatusPayments.FillARPaymentComboBoxes();
					modARStatusPayments.ResetARPaymentFrame();
					fraPayment.Visible = true;
					//FC:FINAL:RPU:#i582: Set this in designer
					//fraPayment.Left = FCConvert.ToInt32((this.Width - fraPayment.Width) / 3.0);
					fraStatusLabels.Visible = false;
					Format_PaymentGrid();
					modARStatusPayments.FillPendingARTransactions();
					modARStatusPayments.CheckAllARPending();
					if (txtReference.Enabled && txtReference.Visible)
					{
						txtReference.Focus();
					}
					mnuPaymentSave.Enabled = true;
					mnuPaymentSaveExit.Enabled = true;
					cmdPaymentSave.Enabled = true;
					cmdProcess.Enabled = true;
					modARStatusPayments.Format_ARvsPeriod();
				}
				else
				{
					fraPayment.Visible = false;
					fraStatusLabels.Visible = true;
					GRID.Visible = true;
					if (GRID.Visible && GRID.Enabled)
					{
						GRID.Focus();
					}
				}
			}
			if (boolPayment)
			{
				mnuPayment.Visible = true;
				mnuProcessGoTo.Text = strStatusString;
				vsPayments.Visible = true;
				lblPaymentInfo.Visible = true;
				if (NegativeBillValues())
				{
					MessageBox.Show("You have some years with negative balances.  Press F11 to apply the appropriate entries.", "Negative Bill Balances", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				lblPaymentInfo.Visible = false;
				mnuPayment.Visible = false;
				mnuProcessGoTo.Text = strPaymentString;
			}
			mnuProcess.Enabled = true;
			frmWait.InstancePtr.Unload();
			if (strNoteText != "")
			{
				// if there is a comment
				if (boolPop)
				{
					SHOWAGAIN:
					;
					//FC:FINAL:AM: show the message in a timer
					Timer timer = new Timer();
					timer.Tick += (s, a) =>
					{
						timer.Stop();
						if (FCMessageBox.Show("Account " + FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR) + " comment." + "\r\n" + strNoteText, MsgBoxStyle.OkCancel | MsgBoxStyle.Information | MsgBoxStyle.DefaultButton2, "Account Note") != DialogResult.OK)
						{
							// GoTo SHOWAGAIN
						}
					};
					timer.Start();
					boolPop = false;
				}
				imgNote.Visible = true;
				mnuFileEditNote.Visible = true;
				mnuFilePriority.Visible = true;
			}
			else
			{
				imgNote.Visible = false;
				mnuFilePriority.Visible = false;
			}
		}

		public void Form_Activate()
		{
			frmARCLStatus_Activated(this, new System.EventArgs());
		}

		private void frmARCLStatus_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void frmARCLStatus_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			int lngRows = 0;
			switch (KeyCode)
			{
				case Keys.Return:
					{
						// SendKeys "{tab}"
						break;
					}
				case Keys.Escape:
					{
						if (vsPreview.Visible == true)
						{
							// close the preview frame
							vsPreview.Visible = false;
							GRID.Visible = true;
							mnuPaymentPreview.Text = "Preview";
						}
						else if (fraRateInfo.Visible == true)
						{
							// close the rate info frame
							KeyCode = (Keys)0;
							cmdRIClose_Click();
						}
						else
						{
							KeyCode = (Keys)0;
							mnuProcessExit_Click();
						}
						break;
					}
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Insert:
					{
						if (boolPayment)
						{
							// create a payment line for the year or all years if it is on auto
							if (cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString() == "Auto")
							{
								lngRows = GRID.Rows - 1;
								modARStatusPayments.CreateAROppositionLine(lngRows);
							}
							else
							{
								modARStatusPayments.CreateAROppositionLine(NextSpacerLine(GRID.FindRow(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString(), 1, lngGRIDColInvoiceNumber)) - 1);
							}
						}
						break;
					}
			}
			//end switch
		}

		private void frmARCLStatus_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmARCLStatus.ScaleWidth	= 9300;
			//frmARCLStatus.ScaleHeight	= 8010;
			//frmARCLStatus.LinkTopic	= "Form1";
			//End Unmaped Properties
			// set the column variables
			lngGRIDCOLTree = 0;
			lngGRIDColInvoiceNumber = 1;
			lngGridColBillKey = 2;
			lngGRIDColRateKey = 3;
			lngGRIDColDate = 4;
			lngGRIDColRef = 5;
			lngGRIDColPaymentCode = 6;
			lngGRIDColPrincipal = 7;
			lngGRIDColPTC = 8;
			lngGRIDColTax = 9;
			lngGRIDColInterest = 10;
			lngGRIDColTotal = 11;
			lngGRIDColLineCode = 12;
			lngGRIDColPaymentKey = 13;
			lngGRIDColCHGINTNumber = 14;
			lngGRIDColPerDiem = 15;
			lngGRIDColPending = 16;
			lngPayGridColInvoice = 0;
			lngPayGridColBillID = 1;
			lngPayGridColDate = 2;
			lngPayGridColRef = 3;
			lngPayGridColCode = 4;
			lngPayGridColCDAC = 5;
			lngPayGridColPrincipal = 6;
			lngPayGridColTax = 7;
			lngPayGridColInterest = 8;
			lngPayGridColArrayIndex = 9;
			lngPayGridColKeyNumber = 10;
			lngPayGridColCHGINTNumber = 11;
			lngPayGridColTotal = 12;
			// set the window size
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// set the back color of any labels or textboxes
			modGlobalFunctions.SetTRIOColors(this);
			modARStatusPayments.Statics.AREffectiveDate = DateTime.Now;
			//imgNote.ImageSource = "imgNote";
			clsAcct.GRID7Light = txtAcctNumber;
			clsAcct.DefaultAccountType = "G";
			clsAcct.AccountCol = -1;
			// set the default databases
			rsCL.DefaultDB = modExtraModules.strARDatabase;
			// set the strings to show
			strStatusString = "&Go To Status";
			strPaymentString = "&Go To Payments/Adj";
			// reset all variables and arrays
			Reset_Sum();
			FCUtils.EraseSafe(modARStatusPayments.Statics.ARPaymentArray);
			FCUtils.EraseSafe(modARStatusPayments.Statics.dblARCurrentInt);
			FCUtils.EraseSafe(modARStatusPayments.Statics.dateAROldDate);
			boolUnloadOK = false;
			//FC:FINAL:AM: add expand button
			GRID.AddExpandButton();
			GRID.RowHeadersWidth = 10;
			//FC:FINAL:AM: attach event handler after load
			this.Resize += new System.EventHandler(this.frmARCLStatus_Resize);
		}

		public void SetToolTip()
		{
			// this will set all of the strings of the tool tips except ones that are in grid cells
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			bool boolContinue = false;
			if (vsPayments.Rows > 1)
			{
				if (!boolUnloadOK)
				{
					boolContinue = true;
					// kk03032014  trocrs-26  Double check for pending payments so we don't delete saved payments
					if (modExtraModules.IsThisCR())
					{
						boolContinue = !PendingSaved();
					}
					if (boolContinue)
					{
						if (MessageBox.Show("You have pending payments, would you like to exit without saving them?", "Exit Payment Screen", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							// erase the pending transactions
							while (!(vsPayments.Rows == 1))
							{
								DeletePaymentRow_2(1);
							}
						}
						else
						{
							e.Cancel = true;
							return;
						}
					}
				}
			}
			FormLoaded = false;
			if (!modExtraModules.IsThisCR())
			{
				//MDIParent.InstancePtr.Show();
			}
			else
			{
			}
		}

		private bool PendingSaved()
		{
			bool PendingSaved = false;
			// this function will check the pending payments to see if there are any that have not been saved yet
			// if there are any pending payments that are not saved, then a true will be returned otherwise false will be
			int intCT;
			clsDRWrapper rsPend = new clsDRWrapper();
			PendingSaved = true;
			// For intCT = 1 To vsPayments.rows - 1
			// If Val(vsPayments.TextMatrix(intCT, lngPayGridColKeyNumber)) = 0 Then
			// PendingSaved = False
			// Exit For
			// End If
			// Next
			rsPend.OpenRecordset("SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountPayments + ") AS qTmp WHERE ReceiptNumber = -1 AND Code <> 'I' ORDER BY RecordedTransactionDate desc", modExtraModules.strARDatabase);
			PendingSaved = rsPend.EndOfFile();
			return PendingSaved;
		}

		private void frmARCLStatus_Resize(object sender, System.EventArgs e)
		{
			// when the form gets resized, this will adjust the grid size and the rows/columns
			Format_Grid();
			AutoSize_GRID();
			Format_PaymentGrid();
			//if (fraRateInfo.Visible == true)
			//{
			//    frmARCLStatus.InstancePtr.fraRateInfo.Left = FCConvert.ToInt32((frmARCLStatus.InstancePtr.Width - frmARCLStatus.InstancePtr.fraRateInfo.Width) / 2.0);
			//    frmARCLStatus.InstancePtr.fraRateInfo.Top = FCConvert.ToInt32((frmARCLStatus.InstancePtr.Height - frmARCLStatus.InstancePtr.fraRateInfo.Height) / 2.0);
			//}
		}

		private bool StartUp()
		{
			bool StartUp = false;
			int intError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int blnumber = 0;
				int rc;
				int lnGBills = 0;
				int intCT;
				intError = 0;
				// reset the totals
				dblTotalPerDiem = 0;
				Reset_Sum();
				// kk03262015 trocrs-21  Form is not unloaded when called from CR, need to reset totals.
				// format the flexgrid
				Format_Grid();
				GRID.Redraw = false;
				// turn this on when ready
				GRID.Visible = false;
				intError = 1;
				CurRow = 0;
				intError = 2;
				// opens the collections database to the correct billing master record
				if (chkShowAll.CheckState == Wisej.Web.CheckState.Checked)
				{
					rsCL.OpenRecordset("SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountBills + ") AS Table1 ORDER BY CASE WHEN BillStatus = 'D' THEN 0 ELSE 1 END, BillDate DESC, InvoiceNumber Desc, BillNumber desc", modExtraModules.strARDatabase);
				}
				else
				{
					rsCL.OpenRecordset("SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountBills + ") AS Table1 WHERE (IsNull(PrinOwed, 0) - IsNull(IntAdded, 0) + IsNull(TaxOwed, 0) - IsNull(PrinPaid, 0) - IsNull(TaxPaid, 0) - IsNull(IntPaid, 0)) <> 0 ORDER BY CASE WHEN BillStatus = 'D' THEN 0 ELSE 1 END, BillDate DESC, InvoiceNumber Desc, BillNumber desc", modExtraModules.strARDatabase);
				}
				intError = 3;
				FillInARInfo();
				lngGroupNumber = modGlobalFunctions.GetGroupNumber_6(FCConvert.ToInt32(Math.Round(Conversion.Val(lblAccount.Text))), "AR");
				intError = 10;
				intError = 20;
				// fill the main value labels
				FillMainLabels();
				// empty right now
				rc = rsCL.RecordCount();
				if (rc != 0)
				{
					intError = 30;
					// Call FillLabels(blnumber)       'this will fill the labels at the top of the screen with account info
					// this will only put the latest billing info in
					intError = 50;
					while (!(rsCL.EndOfFile() == true))
					{
						// check all records from the same account
						bcode = "BILL".ToCharArray();
						blnumber = FCConvert.ToInt32(rsCL.Get_Fields_Int32("ID"));
						intError = 80;
						FillGrid(ref blnumber, lnGBills);
						intError = 105;
						// Doevents                        'DONT TAKE THIS OUT!!!!!!!!
						rsCL.MoveNext();
					}
					intError = 110;
					// this will adjust the initial height
					if (GRID.Rows > 1)
					{
						Adjust_GRID_Height_2((lnGBills + 2) * (GRID.RowHeight(1) + 100));
						intError = 115;
						GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, GRID.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						GRID_RowColChange();
						GRID_Coloring(1);
						GRID.Select(0, lngGRIDColInvoiceNumber, 0, lngGRIDColTotal);
						GRID.CellBorder(Color.Black, 0, 0, 0, 1, 0, 1);
						Create_Account_Totals();
						intError = 120;
						GRID.Redraw = true;
						// turn this on when ready
						GRID.Visible = true;
						GRID.Refresh();
					}
					if (boolPayment)
					{
						if (GRID.Rows > 1)
						{
							//FC:FINAL:RPU:#386 Set the grid width according to ClientArea width
							//GRID.Width = FCConvert.ToInt32(frmARCLStatus.InstancePtr.Width * 0.96);
							GRID.Width = this.ClientArea.Width - this.GRID.Left - 30;
							//GRID.Left = FCConvert.ToInt32((frmARCLStatus.InstancePtr.Width - GRID.Width) / 5.0);
							GRID.Visible = true;
							AutoSize_GRID();
						}
						else
						{
							// no bills
						}
					}
					else
					{
						//FC:FINAL:RPU:#386 Set the grid width according to ClientArea width
						//GRID.Width = FCConvert.ToInt32(frmARCLStatus.InstancePtr.Width * 0.96);
						GRID.Width = this.ClientArea.Width - this.GRID.Left - 30;
						//GRID.Left = FCConvert.ToInt32((frmARCLStatus.InstancePtr.Width - GRID.Width) / 5.0);
						AutoSize_GRID();
					}
					frmARCLStatus.InstancePtr.Refresh();
					intError = 135;
					strGroupComment = modGlobalFunctions.ShowGroupMessage(ref lngGroupNumber);
					// lngGroupNumber was set before with the getgroupnumber function
					intError = 137;
					if (Strings.Trim(strGroupComment) != "")
						MessageBox.Show(strGroupComment, "Group " + FCConvert.ToString(lngGroupNumber) + " Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
					intError = 140;
					StartUp = true;
				}
				else
				{
					// if this account does not exist then go back to the get account screen
					Adjust_GRID_Height_2((lnGBills + 2) * GRID.RowHeight(0));
					intError = 115;
					GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, GRID.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					GRID_RowColChange();
					GRID_Coloring(1);
					GRID.Select(0, lngGRIDColInvoiceNumber, 0, lngGRIDColTotal);
					GRID.CellBorder(Color.Black, 0, 0, 0, 1, 0, 1);
					Create_Account_Totals();
                    modColorScheme.ColorGrid(GRID);
                    GRID.Redraw = true;
					// turn this on when ready
					GRID.Visible = true;
					GRID.Refresh();
					StartUp = true;
				}
				EndTag:
				;
				return StartUp;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				StartUp = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Startup - " + FCConvert.ToString(intError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return StartUp;
		}

		private void CreateMasterLine(ref int BNum)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this creates the first line for each year
				clsDRWrapper rsRK = new clsDRWrapper();
				int lngRK;
				lngRK = FCConvert.ToInt32(Math.Round(Conversion.Val(rsCL.Get_Fields_Int32("BillNumber"))));
				if (lngRK != 0)
				{
					rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRK), modExtraModules.strARDatabase);
					if (rsRK.EndOfFile() || rsRK.BeginningOfFile())
					{
						OriginalBillDate = DateTime.Today;
						MessageBox.Show("No rate record has been defined for this bill.", "Rate Record Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						rsRK.MoveFirst();
						OriginalBillDate = FCConvert.ToDateTime(Strings.Format(rsRK.Get_Fields_DateTime("BillDate"), "MM/dd/yy"));
					}
				}
				CurRow += 1;
				Add_6(CurRow, 0);
				GetDataFromRateTable();
				GRID.TextMatrix(CurRow, lngGRIDColPaymentKey, FCConvert.ToString(rsCL.Get_Fields_Int32("ID")));
				GRID.TextMatrix(CurRow, lngGRIDColInvoiceNumber, FCConvert.ToString(rsCL.Get_Fields_Int32("BillNumber")));
				if (lngRK == 0)
				{
					GRID.TextMatrix(CurRow, lngGRIDColDate, "No RK");
				}
				else
				{
					GRID.TextMatrix(CurRow, lngGRIDColDate, Strings.Format(OriginalBillDate, "MM/dd/yy"));
				}
				GRID.TextMatrix(CurRow, lngGRIDColPending, " ");
				ParentLine = CurRow;
				// this sets the parent line so that I can easily get back to it from the child rows
				rsRK.Reset();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Master Line", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private short FillGrid(ref int BNum, int lnGBills = 0)
		{
			short FillGrid = 0;
			// this sub will fill the subordinate rows with the billing/payment
			DateTime dateTemp = DateTime.FromOADate(0);
			bool boolTemp;
			string strName = "";
			int intCT;
			double dblPerDiem = 0;
			int l = 0;
			int sumRow;
			int j;
			double sum = 0;
			bool Match;
			double dblTotalDue;
			intHoldBill = 0;
			curCurrentInterest = 0;
			FillGrid = 0;
			lnGBills += 1;
			// Increment the number of bills
			CreateMasterLine(ref BNum);
			// This will create the original line for this bill
			curPrincipalDue = FCConvert.ToDouble(modARStatusPayments.DetermineARPrincipal(ref rsCL));
			curTaxDue = FCConvert.ToDouble(modARStatusPayments.DetermineARTax(ref rsCL));
			// these are the subordinate rows
			// check for a different owner and make a seperate row to show the old owner name
			if (Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("BName2"))) != "")
			{
				strName = Strings.Trim(rsCL.Get_Fields_String("BName") + " & " + rsCL.Get_Fields_String("BName2"));
				// fill in the labels with the record information
			}
			else
			{
				strName = Strings.Trim(FCConvert.ToString(rsCL.Get_Fields_String("BName")));
			}
			if (Strings.Trim(strName) != Strings.Trim(lblOwnersName.Text))
			{
				CurRow += 1;
				Add_6(CurRow, 1);
				//FC:FINAL:AM: merge the cells
				GRID.MergeRow(CurRow, true, lngGRIDColPaymentCode, lngGRIDColInterest);
				GRID.TextMatrix(CurRow, lngGRIDColRef, "Billed To:");
				GRID.TextMatrix(CurRow, lngGRIDColPaymentCode, strName);
				//GRID.TextMatrix(CurRow, lngGRIDColPrincipal, strName);
				//GRID.TextMatrix(CurRow, lngGRIDColPTC, strName);
				//GRID.TextMatrix(CurRow, lngGRIDColTax, strName);
				//GRID.TextMatrix(CurRow, lngGRIDColInterest, strName);
				GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, CurRow, lngGRIDColPaymentCode, CurRow, lngGRIDColInterest, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//GRID.MergeRow(CurRow, true);
				GRID.TextMatrix(CurRow, lngGRIDColPending, " ");
			}
			// Original Bill Line in grid
			// DJW 5/27/08 Changed BillNumber to Bill
			intHoldBill = FCConvert.ToInt32(rsCL.Get_Fields_Int32("ID"));
			GRID.TextMatrix(ParentLine, lngGridColBillKey, FCConvert.ToString(rsCL.Get_Fields_Int32("ID")));
			GRID.TextMatrix(ParentLine, lngGRIDColRateKey, FCConvert.ToString(rsCL.Get_Fields_Int32("BillNumber")));
			GRID.TextMatrix(ParentLine, lngGRIDColInvoiceNumber, FCConvert.ToString(rsCL.Get_Fields_String("InvoiceNumber")));
			GRID.TextMatrix(ParentLine, lngGRIDColRef, "Original");
			GRID.TextMatrix(ParentLine, lngGRIDColPrincipal, FCConvert.ToString(rsCL.Get_Fields_Decimal("PrinOwed")));
			GRID.TextMatrix(ParentLine, lngGRIDColTax, FCConvert.ToString(rsCL.Get_Fields_Decimal("TaxOwed")));
			// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
			GRID.TextMatrix(ParentLine, lngGRIDColInterest, "0.00");
			// rsCL.Get_Fields("IntAdded")
			GRID.TextMatrix(ParentLine, lngGRIDColPTC, FCConvert.ToString(rsCL.Get_Fields_Decimal("PrinOwed") + rsCL.Get_Fields_Decimal("TaxOwed")));
			GRID.TextMatrix(ParentLine, lngGRIDColTotal, FCConvert.ToString(Conversion.Val(GRID.TextMatrix(ParentLine, lngGRIDColPrincipal)) + Conversion.Val(GRID.TextMatrix(ParentLine, lngGRIDColTax)) + Conversion.Val(GRID.TextMatrix(ParentLine, lngGRIDColInterest))));
			GRID.TextMatrix(ParentLine, lngGRIDColLineCode, "+");
			// this will show the payments
			if (PaymentRecords(ref BNum))
			{
				// this writes all of the payment records
				// current interest line (if Needed)
				if (!boolNoCurrentInt)
				{
					boolNoCurrentInt = false;
					dblTotalDue = modARCalculations.CalculateAccountAR(rsCL, modARStatusPayments.Statics.AREffectiveDate, ref curCurrentInterest, dateTemp, ref dblPerDiem);
				}
				// rsCL is a record from the Bill table
				GRID.TextMatrix(ParentLine, lngGRIDColPerDiem, FCConvert.ToString(dblPerDiem));
				dblTotalPerDiem += dblPerDiem;
				dblPerDiem = 0;
				modARStatusPayments.Statics.dateAROldDate[intHoldBill] = dateTemp;
				// holds that last Interest Date in the grid so that the last payment can be reversed
			}
			else
			{
				// current interest line (if Needed)
				if (!boolNoCurrentInt)
				{
					boolNoCurrentInt = false;
					dblTotalDue = modARCalculations.CalculateAccountAR(rsCL, dateTemp, ref curCurrentInterest, ref dblPerDiem);
				}
				GRID.TextMatrix(ParentLine, lngGRIDColPerDiem, FCConvert.ToString(dblPerDiem));
				dblTotalPerDiem += dblPerDiem;
				dblPerDiem = 0;
			}
			modARStatusPayments.Statics.dblARCurrentInt[intHoldBill] = -1 * curCurrentInterest;
			// this holds the current interest totals for later use
			if (curCurrentInterest != 0)
			{
				CurRow += 1;
				Add_6(CurRow, 1);
				curCurrentInterest *= -1;
				GRID.TextMatrix(CurRow, lngGRIDColInvoiceNumber, "");
				GRID.TextMatrix(CurRow, lngGRIDColDate, "");
				GRID.TextMatrix(CurRow, lngGRIDColPending, " ");
				if (curCurrentInterest < 0)
				{
					GRID.TextMatrix(CurRow, lngGRIDColRef, "CURINT");
				}
				else
				{
					GRID.TextMatrix(CurRow, lngGRIDColRef, "EARNINT");
				}
				GRID.TextMatrix(CurRow, lngGRIDColPrincipal, FCConvert.ToString(0));
				GRID.TextMatrix(CurRow, lngGRIDColPTC, FCConvert.ToString(0));
				GRID.TextMatrix(CurRow, lngGRIDColTax, FCConvert.ToString(0));
				GRID.TextMatrix(CurRow, lngGRIDColInterest, FCConvert.ToString(curCurrentInterest));
				GRID.TextMatrix(CurRow, lngGRIDColTotal, FCConvert.ToString((FCConvert.ToDecimal(GRID.TextMatrix(CurRow, lngGRIDColPrincipal)) + FCConvert.ToDecimal(GRID.TextMatrix(CurRow, lngGRIDColTax)) + FCConvert.ToDecimal(GRID.TextMatrix(CurRow, lngGRIDColInterest)))));
				GRID.TextMatrix(CurRow, lngGRIDColLineCode, "-");
				// this is used to sum the cols - is for an increase in the bill amount...it is adding to the bill
			}
			// subtotals line - this will be shown on the master line when this line is not shown
			CurRow += 1;
			Add_6(CurRow, 1);
			// total line
			GRID.TextMatrix(CurRow, lngGRIDColPending, " ");
			Add_8(CurRow + 1, 1);
			// and spacer line to hold hidden values
			GRID.TextMatrix(CurRow + 1, lngGRIDColPending, " ");
			// subtotal format, bold and top border
			GRID.TextMatrix(CurRow, lngGRIDColLineCode, "=");
			GRID.TextMatrix(CurRow, lngGRIDColRef, "Total");
			l = LastParentRow(CurRow);
			for (j = lngGRIDColPrincipal; j <= lngGRIDColTotal; j++)
			{
				sum = 0;
				for (sumRow = l; sumRow <= CurRow - 1; sumRow++)
				{
					////Application.DoEvents();
					if (GRID.TextMatrix(sumRow, lngGRIDColLineCode) == "+")
					{
						sum = modGlobal.Round(sum + modGlobal.Round(Conversion.Val(GRID.TextMatrix(sumRow, j)), 2), 2);
					}
					else if (GRID.TextMatrix(sumRow, lngGRIDColLineCode) == "-")
					{
						sum = modGlobal.Round(sum - modGlobal.Round(Conversion.Val(GRID.TextMatrix(sumRow, j)), 2), 2);
					}
					else if (FCConvert.ToBoolean(GRID.TextMatrix(sumRow, lngGRIDColLineCode)) == true)
					{
						sum = modGlobal.Round(Conversion.Val(GRID.TextMatrix(sumRow, j)), 2);
					}
				}
				GRID.TextMatrix(CurRow, j, FCConvert.ToString(sum));
				GRID.TextMatrix(CurRow + 1, j, FCConvert.ToString(sum));
			}
			// spacer line
			CurRow += 1;
			GRID.TextMatrix(CurRow, lngGRIDColDate, GRID.TextMatrix(ParentLine, lngGRIDColDate));
			// .TextMatrix(CurRow, lngGRIDColRef) = .TextMatrix(ParentLine, lngGRIDColRef)
			GRID.TextMatrix(CurRow, lngGRIDColPaymentKey, FCConvert.ToString(BNum));
			// puts the totals in the header line to start
			if (GRID.TextMatrix(ParentLine, lngGRIDColRef) == "Original")
			{
				SwapRows_8(ParentLine, NextSpacerLine(ParentLine));
			}
			GRID.Select(0, 0);
			return FillGrid;
		}

		private bool CompareCol(ref double GRID_Total, ref short GRID_Col, ref short intNum)
		{
			bool CompareCol = false;
			// this checks to see if the grid totals are the same as the recordset totals
			double dblTemp = 0;
			clsDRWrapper rsTemp;
			rsTemp = new clsDRWrapper();
			rsTemp.OpenRecordset("SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountBills + ") as temp WHERE ActualAccountNumber = " + FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR) + " AND BillNumber = " + FCConvert.ToString(intNum), modExtraModules.strARDatabase);
			if (GRID_Col == lngGRIDColPrincipal)
			{
				// principal line
				// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
				dblTemp = modGlobal.Round(Conversion.Val(rsTemp.Get_Fields_Decimal("PrinOwed")) - Conversion.Val(rsTemp.Get_Fields("PrinPaid")), 2);
			}
			else if (GRID_Col == lngGRIDColInterest)
			{
				// interest
				// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
				dblTemp = modGlobal.Round((-1 * Conversion.Val(rsTemp.Get_Fields("IntAdded"))) - Conversion.Val(rsTemp.Get_Fields("IntPaid")) - curCurrentInterest, 2);
			}
			else if (GRID_Col == lngGRIDColTotal)
			{
				// totals
				// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
				dblTemp = modGlobal.Round((Conversion.Val(rsTemp.Get_Fields_Decimal("Prinowed")) - Conversion.Val(rsTemp.Get_Fields("PrinPaid"))) + (-1 * (Conversion.Val(rsTemp.Get_Fields("IntAdded"))) - Conversion.Val(rsTemp.Get_Fields("IntPaid")) - curCurrentInterest), 2);
			}
			if (Conversion.Val(GRID_Total) == Conversion.Val(dblTemp))
			{
				CompareCol = true;
			}
			rsTemp.Reset();
			return CompareCol;
		}

		private void FillLabels_2(int BK)
		{
			FillLabels(ref BK);
		}

		private void FillLabels(ref int BK)
		{
			// pass in the bill ID and the information is set
			Decimal curHold;
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp;
			// sets the current information at the top of the screen
			strTemp = "SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountBills + ") as temp WHERE ID = " + FCConvert.ToString(BK) + " ORDER BY BillNumber Desc";
			rsTemp.OpenRecordset(strTemp, modExtraModules.strARDatabase);
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				// lblLandValue2.Text = Format(Val(.Get_Fields("LandValue")), "#,##0")
				// lblBuildingValue2.Text = Format(Val(.Get_Fields("BuildingValue")), "#,##0")
				// lblExemptValue2.Text = Format(Val(.Get_Fields("ExemptValue")), "#,##0")
				// lblTotalValue2.Text = Format(Val(.Get_Fields("LandValue")) + Val(.Get_Fields("BuildingValue")) - Val(.Get_Fields("ExemptValue")), "#,##0")
				// lblYear.Text = Val(FormatYear(.Get_Fields("BillingYear")))
				// curHold = Val(.Get_Fields("TaxDue1")) + Val(.Get_Fields("TaxDue2")) + Val(.Get_Fields("TaxDue3")) + Val(.Get_Fields("TaxDue4"))
			}
			else
			{
			}
			rsTemp.Reset();
		}

		private void GetDataFromRateTable()
		{
			clsDRWrapper rsRT = new clsDRWrapper();
			strSQL = "SELECT * FROM RATEKeys WHERE ID = " + rsCL.Get_Fields_Int32("BillNumber");
			rsRT.OpenRecordset(strSQL, modExtraModules.strARDatabase);
			if (rsRT.EndOfFile() != true && rsRT.BeginningOfFile() != true)
			{
				if (fecherFoundation.FCUtils.IsEmptyDateTime(rsRT.Get_Fields_DateTime("IntStart")) == false)
				{
					modARStatusPayments.Statics.strARInterestDate1 = FCConvert.ToString(rsRT.Get_Fields_DateTime("IntStart"));
				}
			}
			else
			{
				modARStatusPayments.Statics.strARInterestDate1 = FCConvert.ToString(DateAndTime.DateValue(FCConvert.ToString(0)));
			}
			if (modARStatusPayments.Statics.dblARInterestRate == 0)
			{
				modARStatusPayments.Statics.dblARInterestRate = 0;
			}
			rsRT.Reset();
		}

		private void Format_Grid(bool boolForceWide = false)
		{
			// this sets the size of the grid control and the titles line
			int wid;
			GRID.Cols = lngGRIDColPending + 1;
			if (boolPayment)
			{
				//FC:FINAL:RPU: #305- Set the height to be lower
				GRID.Height = FCConvert.ToInt32(frmARCLStatus.InstancePtr.Height * 0.40);
				//GRID.Top = lblPaymentInfo.Top + lblPaymentInfo.Height;
				//FC:FINAL:RPU:#386 Set the grid width according to ClientArea width
				//GRID.Width = FCConvert.ToInt32(frmARCLStatus.InstancePtr.Width * 0.96);
				GRID.Width = this.ClientArea.Width - this.GRID.Left - 30;
				//GRID.Left = FCConvert.ToInt32((frmARCLStatus.InstancePtr.Width - GRID.Width) / 5.0);
			}
			else
			{
				fraStatusLabels.Visible = true;
				//GRID.Top = fraStatusLabels.Top + fraStatusLabels.Height;
				//FC:FINAL:BBE:#298 - adjust the grid Height correctly
				//GRID.Height = FCConvert.ToInt32(Math.Abs(frmARCLStatus.InstancePtr.Height - GRID.Top - (frmARCLStatus.InstancePtr.Height * 0.09)));
				GRID.Height = FCConvert.ToInt32((frmARCLStatus.InstancePtr.ClientArea.Height - GRID.Top) * 0.96);
				//FC:FINAL:RPU: Enlarge the grid
				//FC:FINAL:RPU:#386 Set the grid width according to ClientArea width
				//GRID.Width = FCConvert.ToInt32(frmARCLStatus.InstancePtr.Width * 0.96);
				GRID.Width = this.ClientArea.Width - this.GRID.Left - 30;
				//GRID.Left = FCConvert.ToInt32((frmARCLStatus.InstancePtr.Width - GRID.Width) / 5.0);
			}
			GRID.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
			// column headers
			GRID.TextMatrix(0, lngGRIDColInvoiceNumber, "Invoice");
			GRID.TextMatrix(0, lngGRIDColDate, "Date");
			GRID.TextMatrix(0, lngGRIDColRef, "Ref");
			GRID.TextMatrix(0, lngGRIDColPaymentCode, "C");
			GRID.TextMatrix(0, lngGRIDColPrincipal, "Principal");
			GRID.TextMatrix(0, lngGRIDColPTC, "PTC");
			GRID.TextMatrix(0, lngGRIDColTax, "Tax");
			GRID.TextMatrix(0, lngGRIDColInterest, "Interest");
			GRID.TextMatrix(0, lngGRIDColTotal, "Total");
			AutoSize_GRID();
			GRID.ColFormat(lngGRIDColDate, "MM/dd/yy");
			GRID.ColFormat(lngGRIDColPrincipal, "#,##0.00");
			GRID.ColFormat(lngGRIDColPTC, "#,##0.00");
			GRID.ColFormat(lngGRIDColTax, "#,##0.00");
			GRID.ColFormat(lngGRIDColInterest, "#,##0.00");
			GRID.ColFormat(lngGRIDColTotal, "#,##0.00");
			GRID.ColAlignment(lngGRIDColInvoiceNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GRID.ColAlignment(lngGRIDColPending, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			GRID.ColAlignment(lngGRIDColRef, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            GRID.ColAlignment(lngGRIDColPrincipal, FCGrid.AlignmentSettings.flexAlignRightCenter);
            GRID.ColAlignment(lngGRIDColPTC, FCGrid.AlignmentSettings.flexAlignRightCenter);
            GRID.ColAlignment(lngGRIDColTax, FCGrid.AlignmentSettings.flexAlignRightCenter);
            GRID.ColAlignment(lngGRIDColInterest, FCGrid.AlignmentSettings.flexAlignRightCenter);
            GRID.ColAlignment(lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColPrincipal, 0, lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColPTC, 0, lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColRef, 0, lngGRIDColPaymentCode, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColDate, 0, lngGRIDColDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            //FC:FINAL:RPU:#305- align payments grid with main grid and adjust fraPayment location
            vsPayments.Width = GRID.Width;
			fraPayment.Width = vsPayments.Width + 20;
			fraPayment.Top = GRID.Bottom;
			//FC:FINAL:RPU:#386- enlarge comments field and align txtTotalPendingDue
			this.txtComments.Width = (this.vsPayments.Left + this.vsPayments.Width) - this.txtComments.Left;
			this.txtTotalPendingDue.Left = (this.vsPayments.Left + this.vsPayments.Width) - this.txtTotalPendingDue.Width;
			this.lblTotalPendingDue.Left = this.txtTotalPendingDue.Left - 126;
			this.vsPeriod.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
		}

		private void Add_6(int Row, int lvl)
		{
			Add(ref Row, ref lvl);
		}

		private void Add_8(int Row, int lvl)
		{
			Add(ref Row, ref lvl);
		}

		private void Add(ref int Row, ref int lvl)
		{
			// this sub adds a row in the grid at position = row and .rowoutlinelevel = lvl
			// set the row as a group
			if (MaxRows == 0)
			{
				MaxRows = 2;
			}
			GRID.AddItem("", Row);
			// increments the number of rows
			MaxRows = GRID.Rows - 1;
			lvl += 1;
			if (lvl == 1)
			{
				//GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngGRIDColInvoiceNumber, Row, GRID.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT); // &H80000018
				GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngGRIDColInvoiceNumber, Row, GRID.Cols - 1, Color.White);
                GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, lngGRIDColTotal, Row, lngGRIDColTotal, Color.FromArgb(5, 204, 71));
                // &H80000018
            }
			else if (lvl == 2)
			{
				GRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, Row, lngGRIDColRef, Row, GRID.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
				//GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngGRIDColInvoiceNumber, Row, GRID.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX); // &H80000016
				//GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, lngGRIDColInvoiceNumber, Row, lngGRIDColInvoiceNumber, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX); // &H80000016
				GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngGRIDColInvoiceNumber, Row, GRID.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249)));
				// &H80000016
				GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, lngGRIDColInvoiceNumber, Row, lngGRIDColInvoiceNumber, Color.Black);
				// &H80000016
			}
			else
			{
				GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, lngGRIDColInvoiceNumber, Row, GRID.Cols - 1, Color.White);
			}
			//GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, lngGRIDColTotal, Row, lngGRIDColTotal, Color.FromArgb(5, 204, 71));
			GRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, Row, lngGRIDColTotal, Row, lngGRIDColTotal, true);
			GRID.RowOutlineLevel(Row, lvl);
			if (lvl <= 1)
				GRID.IsSubtotal(Row, true);
		}

		private void AutoSize_GRID()
		{
			// sets the width of each column in order to fit in the grid without a scrollbar
			// the total if added should be slightly less then 1
			int lngTemp;
			int i;
			int wid = 0;
			// make the grid larger
			wid = GRID.WidthOriginal;
			GRID.ColWidth(lngGRIDColInvoiceNumber, FCConvert.ToInt32(wid * 0.15));
			GRID.ColWidth(lngGridColBillKey, 0);
			GRID.ColWidth(lngGRIDColRateKey, 0);
			GRID.ColWidth(lngGRIDColDate, FCConvert.ToInt32(wid * 0.11));
			GRID.ColWidth(lngGRIDColRef, FCConvert.ToInt32(wid * 0.1));
			GRID.ColWidth(lngGRIDColPaymentCode, FCConvert.ToInt32(wid * 0.04));
			GRID.ColWidth(lngGRIDColPrincipal, FCConvert.ToInt32(wid * 0.13));
			GRID.ColWidth(lngGRIDColPTC, 0);
			GRID.ColWidth(lngGRIDColTax, FCConvert.ToInt32(wid * 0.13));
			GRID.ColWidth(lngGRIDColInterest, FCConvert.ToInt32(wid * 0.13));
			GRID.ColWidth(lngGRIDColTotal, FCConvert.ToInt32(wid * 0.13));
			GRID.ColWidth(lngGRIDColLineCode, 0);
			// Row code
			GRID.ColWidth(lngGRIDColPaymentKey, 0);
			// Payment ID
			GRID.ColWidth(lngGRIDColCHGINTNumber, 0);
			// CHGINT Number
			GRID.ColWidth(lngGRIDColPerDiem, 0);
			// Per Diem in Master Line
			GRID.ColWidth(lngGRIDColPending, FCConvert.ToInt32(wid * 0.04));
			// * Col
		}

		private void Adjust_GRID_Height_2(int ht)
		{
			Adjust_GRID_Height(ref ht);
		}

		private void Adjust_GRID_Height(ref int ht)
		{
			//FC:FINAL:RPU:#305 don't set the height
			// this adjusts the grid height depending on the amount of rows
			//if (boolPayment)
			//{
			//    if (ht < frmARCLStatus.InstancePtr.HeightOriginal * 0.54)
			//    {
			//        if (GRID.HeightOriginal != ht)
			//        {
			//            GRID.HeightOriginal = FCConvert.ToInt32(ht + (GRID.RowHeight(0)));
			//        }
			//    }
			//    else
			//    {
			//        if (GRID.HeightOriginal < frmARCLStatus.InstancePtr.HeightOriginal * 0.54)
			//        {
			//            GRID.HeightOriginal = FCConvert.ToInt32(frmARCLStatus.InstancePtr.HeightOriginal * 0.54 + (GRID.RowHeight(0)));
			//        }
			//    }
			//}
			//else
			//{
			//    if (ht < frmARCLStatus.InstancePtr.HeightOriginal * 0.75)
			//    {
			//        if (GRID.HeightOriginal != ht)
			//        {
			//            GRID.HeightOriginal = FCConvert.ToInt32(ht + (GRID.RowHeight(0)));
			//        }
			//    }
			//    else
			//    {
			//        if (GRID.HeightOriginal < frmARCLStatus.InstancePtr.HeightOriginal * 0.75)
			//        {
			//            GRID.HeightOriginal = FCConvert.ToInt32(frmARCLStatus.InstancePtr.HeightOriginal * 0.75 + (GRID.RowHeight(0)));
			//        }
			//    }
			//}
		}

		private void txtAcctNumber_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			clsDRWrapper rsF = new clsDRWrapper();
			clsDRWrapper rsCL = new clsDRWrapper();
			boolDoNotContinueSave = false;
			if (modGlobalConstants.Statics.gboolBD)
			{
				// check the fun and make sure that it matches the current type's fund
				if ((Strings.InStr(1, txtAcctNumber.EditText, "_", CompareConstants.vbBinaryCompare) == 0 || Strings.Left(txtAcctNumber.EditText, 1) == "M") && txtAcctNumber.EditText.Length > 2)
				{
					rsCL.OpenRecordset("SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountBills + ") AS Temp WHERE InvoiceNumber = '" + cmbBillNumber.Text + "'", modExtraModules.strARDatabase);
					if (rsCL.EndOfFile() != true && rsCL.BeginningOfFile() != true)
					{
						// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
						rsF.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + rsCL.Get_Fields("BillType"), "TWAR0000.vb1");
						if (!rsF.EndOfFile())
						{
							if (modBudgetaryAccounting.GetFundFromAccount(txtAcctNumber.EditText) != modBudgetaryAccounting.GetFundFromAccount(rsF.Get_Fields_String("Account1")))
							{
								MessageBox.Show("This account's fund does not match the fund for this bill type.", "Invalid Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								boolDoNotContinueSave = true;
							}
						}
					}
				}
			}
		}

		private void txtTax_Enter(object sender, System.EventArgs e)
		{
			txtTax.SelectionStart = 0;
			txtTax.SelectionLength = txtTax.Text.Length;
		}

		private void txtTax_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtTax.SelectionStart == 0)
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtTax.SelectionStart == txtTax.Text.Length)
						{
							txtInterest.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtTax_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else if (KeyAscii == Keys.Insert)
			{
				// minus
				KeyAscii = (Keys)0;
				if (Conversion.Val(txtTax.Text) == 0)
				{
					txtTax.Text = "0.00";
				}
				txtTax.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtTax.Text)) * -1, "#,##0.00");
				txtTax.SelectionStart = 0;
				txtTax.SelectionLength = txtTax.Text.Length;
			}
			else if (KeyAscii == Keys.Execute)
			{
				// plus
				KeyAscii = (Keys)0;
				if (Conversion.Val(txtTax.Text) == 0)
				{
					txtTax.Text = "0.00";
				}
				txtTax.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtTax.Text)), "#,##0.00");
				txtTax.SelectionStart = 0;
				txtTax.SelectionLength = txtTax.Text.Length;
			}
			else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
			{
				// c, C - this will clear the box
				KeyAscii = (Keys)0;
				txtTax.Text = "0.00";
				txtTax.SelectionStart = 0;
				txtTax.SelectionLength = 4;
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTax_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this is validaiting the amount entered
				if (txtTax.Text != "")
				{
					if (FCConvert.ToDecimal(txtTax.Text) > 99999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtTax.Text = Strings.Format(99999999.99, "#,##0.00");
					}
					else if (FCConvert.ToDecimal(txtTax.Text) < -99999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $-99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtTax.Text = Strings.Format(-99999999.99, "#,##0.00");
					}
					else
					{
						txtTax.Text = Strings.Format(FCConvert.ToDecimal(txtTax.Text), "#,##0.00");
					}
				}
				else
				{
					txtTax.Text = "0.00";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GRID_ClickEvent(object sender, System.EventArgs e)
		{
			GRID_Click();
		}

		private void GRID_Click()
		{
			// this will set the bill information to the bill being selected
			int cRow;
			int intCT;
			if (GRID.RowOutlineLevel(GRID.Row) == 1)
			{
				// .Col = lngGRIDColInvoiceNumber And
				cmbBillNumber.SelectedIndex = cmbBillNumber.Items.Count - 1;
				for (cRow = 0; cRow <= cmbBillNumber.Items.Count - 1; cRow++)
				{
					if (Strings.Trim(cmbBillNumber.Items[cRow].ToString()) == Strings.Trim(GRID.TextMatrix(GRID.Row, lngGRIDColInvoiceNumber)))
					{
						cmbBillNumber.SelectedIndex = cRow;
						break;
					}
				}
			}
			else if (GRID.RowOutlineLevel(GRID.Row) == 0)
			{
				if (cmbBillNumber.SelectedIndex != -1 && "Auto" == Strings.Trim(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString()))
				{
					cmbBillNumber_Click();
				}
				else
				{
					for (intCT = 0; intCT <= cmbBillNumber.Items.Count - 1; intCT++)
					{
						if ("Auto" == Strings.Trim(cmbBillNumber.Items[intCT].ToString()).Replace("*", ""))
						{
							cmbBillNumber.SelectedIndex = intCT;
							break;
						}
					}
				}
			}
		}

		private void GRID_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			GRID_Collapsed(this.GRID.GetFlexRowIndex(e.RowIndex), false);
		}

		private void GRID_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			GRID_Collapsed(this.GRID.GetFlexRowIndex(e.RowIndex), true);
		}

		private void GRID_Collapsed(int row, bool isCollapsed)
		{
			// this sub occurs when a gridline is collapsed or expanded
			// it will change the data in the main rows depending apon is status
			int temp;
			int counter;
			int rows = 0;
			int height1 = 0;
			bool DeptFlag = false;
			bool DivisionFlag = false;
			int cRow;
			int lngTempRow = 0;
			// this will set the bill information to the bill being selected
			lngTempRow = row;
			if (lngTempRow > 0)
			{
				if (GRID.RowOutlineLevel(lngTempRow) == 1)
				{
					for (cRow = 0; cRow <= cmbBillNumber.Items.Count - 1; cRow++)
					{
						// set the combo box
						if (Strings.Trim(cmbBillNumber.Items[cRow].ToString()) == Strings.Trim(GRID.TextMatrix(lngTempRow, lngGRIDColInvoiceNumber)))
						{
							cmbBillNumber.SelectedIndex = cRow;
							break;
						}
					}
				}
			}
			if (CollapseFlag == false)
			{
				for (counter = 1; counter <= GRID.Rows - 1; counter++)
				{
					if (GRID.RowOutlineLevel(counter) == 0)
					{
						if (GRID.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							DeptFlag = true;
						}
						else
						{
							rows += 1;
							DeptFlag = false;
						}
					}
					else if (GRID.RowOutlineLevel(counter) == 1)
					{
						if (DeptFlag == true)
						{
							// do nothing
						}
						else if (GRID.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							DivisionFlag = true;
							// this function will show the totals on the header line and
							// put the original bill info on the empty line below the total line (hidden)
							// this means that this header row is closed and needs to show the totals on its line
							if (GRID.TextMatrix(counter, lngGRIDColRef) == "Original")
							{
								SwapRows_8(counter, NextSpacerLine(counter));
							}
						}
						else
						{
							rows += 1;
							DivisionFlag = false;
							// this function will show the original bill info on the header line and
							// put the totals on the empty line below the total line (hidden)
							// this means that that total row is showing so the totals must be on the total line not the header line
							if (GRID.TextMatrix(counter, lngGRIDColRef) != "Original")
							{
								SwapRows_8(counter, NextSpacerLine(counter));
							}
							// this will swap the two rows
						}
					}
					else
					{
						if (DeptFlag == true || DivisionFlag == true)
						{
							// do nothing
						}
						else
						{
							rows += 1;
						}
					}
				}
				height1 = (rows + 1) * GRID.RowHeight(1);
				Adjust_GRID_Height(ref height1);
			}
		}

		private void GRID_DblClick(object sender, System.EventArgs e)
		{
			GRID_DblClick();
		}

		private void GRID_DblClick()
		{
			int cRow = 0;
			int mRow = 0;
			int mCol = 0;
			mRow = GRID.MouseRow;
			mCol = GRID.MouseCol;
			cRow = GRID.Row;
			if (mRow > 0 && mCol > 0)
			{
				// make sure that the user is not clicking on the top row to expand/collapse the grid
				if (GRID.RowOutlineLevel(cRow) == 1)
				{
					if (GRID.Col == lngGRIDColTotal || GRID.Col == lngGRIDColPending)
					{
						// do nothing
					}
					else if (GRID.Col == 1)
					{
						// set the year
						for (cRow = 0; cRow <= cmbBillNumber.Items.Count - 1; cRow++)
						{
							if (Strings.Trim(cmbBillNumber.Items[cRow].ToString()) == Strings.Trim(GRID.TextMatrix(GRID.Row, GRID.Col)))
							{
								cmbBillNumber.SelectedIndex = cRow;
								break;
							}
						}
					}
					// expand the rows
					if (GRID.IsCollapsed(GRID.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						GRID.IsCollapsed(GRID.Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
					}
					else
					{
						GRID.IsCollapsed(GRID.Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
					}
				}
				else if (GRID.RowOutlineLevel(cRow) == 2 && fraPayment.Visible == true && (GRID.TextMatrix(cRow, lngGRIDColRef) != "CHGINT" && GRID.TextMatrix(cRow, lngGRIDColRef) != "EARNINT" && GRID.TextMatrix(cRow, lngGRIDColRef) != "CNVRSN" && GRID.TextMatrix(cRow, lngGRIDColRef) != "Interest"))
				{
					// reverse the payment on this line
					if (GRID.TextMatrix(cRow, lngGRIDColRef) != "Billed To:")
					{
						if (Strings.Trim(GRID.TextMatrix(cRow, lngGRIDColRef)) != "Total" && Strings.Trim(GRID.TextMatrix(cRow, lngGRIDColRef)) != "CURINT")
						{
							if (Strings.Trim(GRID.TextMatrix(cRow, lngGRIDColPaymentCode)) != "D")
							{
								if (MessageBox.Show("Are you sure that you would like to reverse this payment?", "Reverse Payment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									modARStatusPayments.CreateAROppositionLine(cRow, true);
								}
							}
							else
							{
								// reversing a discount
								if (MessageBox.Show("Are you sure that you would like to reverse this discount?", "Reverse Payment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
								{
									modARStatusPayments.CreateAROppositionLine(cRow, true);
								}
							}
						}
						else
						{
							modARStatusPayments.CreateAROppositionLine(cRow);
						}
					}
				}
				else if (GRID.RowOutlineLevel(cRow) == 0)
				{
					// this is the total account line
					modARStatusPayments.CreateAROppositionLine(cRow);
				}
			}
		}

		private void GRID_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void GRID_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 32)
			{
				// spacebar
				keyAscii = 0;
				GRID_DblClick();
			}
			else if (keyAscii == 13 && GRID.RowOutlineLevel(GRID.Row) == 1)
			{
				keyAscii = 0;
				GRID_Click();
			}
		}

		private void GRID_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// catch the right click to show the rate information about that year
			int lngMRow = 0;
			int lngBK = 0;
			lngMRow = GRID.MouseRow;
			if (e.Button == MouseButtons.Right)
			{
				// right click
				if (GRID.RowOutlineLevel(lngMRow) == 1)
				{
					GRID.Select(lngMRow, 1);
					// this will force the information to be correct by selecting the correct row before looking at the labels
					lngBK = FCConvert.ToInt32(Math.Round(Conversion.Val(GRID.TextMatrix(lngMRow, lngGRIDColPaymentKey))));
					rsCL.OpenRecordset("SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountBills + ") as temp WHERE ID = " + FCConvert.ToString(lngBK), modExtraModules.strARDatabase);
					if (rsCL.EndOfFile() != true && rsCL.BeginningOfFile() != true)
					{
						ShowRateInfo_6(lngMRow, 1, rsCL);
					}
				}
				else if (GRID.RowOutlineLevel(lngMRow) == 2)
				{
					ShowPaymentInfo(lngMRow, GRID.MouseCol);
				}
			}
		}

		private void GRID_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			// as the mouse moves over the grid, the tooltip will change depending on which cell the pointer is currently in
			string strTemp = "";
			int lngMR = 0;
			//FC:FINAL:MSH - added an extra comparing to avoid exceptions if index less than 0
			if (e.ColumnIndex > -1 && e.RowIndex > -1)
			{
				DataGridViewCell cell = GRID[e.ColumnIndex, e.RowIndex];
				if (e.ColumnIndex == 0)
				{
					if (e.ColumnIndex == lngGRIDColInvoiceNumber)
					{
						// Year
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else if (e.ColumnIndex == lngGRIDColDate)
					{
						// Date
						strTemp = "Recorded Date";
						cell.ToolTipText = strTemp;
					}
					else if (e.ColumnIndex == lngGRIDColRef)
					{
						// Reference
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else if (e.ColumnIndex == lngGRIDColPaymentCode)
					{
						// Code
						// strtemp = strtemp & " 3 - 30 Day Notice Fee" & ", "
						// strtemp = strtemp & " A - Abatement" & ", "
						strTemp += " C - Correction" + ", ";
						// strtemp = strtemp & " D - Discount" & ", "
						strTemp += " I - Interest" + ", ";
						strTemp += " P - Payment" + ", ";
						// strtemp = strtemp & " R - Refunded Abatement" & ", "
						// strtemp = strtemp & " S - Supplemental" & ", "
						// strtemp = strtemp & " U - Tax Club" & ", "
						strTemp += " Y - PrePayment";
						cell.ToolTipText = strTemp;
					}
					else if (e.ColumnIndex == lngGRIDColPTC)
					{
						strTemp = "Principal + Tax";
						cell.ToolTipText = strTemp;
					}
					else if (e.ColumnIndex == lngGRIDColPrincipal)
					{
						// Prin
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else if (e.ColumnIndex == lngGRIDColInterest || e.ColumnIndex == lngGRIDColTax)
					{
						// Int
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else if (e.ColumnIndex == lngGRIDColTotal)
					{
						// Total
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else
					{
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
				}
				else
				{
					lngMR = GRID.MouseRow;
					if (lngMR > 0)
					{
						if (GRID.TextMatrix(lngMR, lngGRIDColLineCode) != "" && GRID.TextMatrix(lngMR, lngGRIDColLineCode) != "=")
						{
							if (GRID.TextMatrix(lngMR, lngGRIDColRef) != "CURINT" && GRID.TextMatrix(lngMR, lngGRIDColRef) != "EARNINT")
							{
								if (GRID.RowOutlineLevel(lngMR) == 1 && e.ColumnIndex == lngGRIDColInvoiceNumber)
								{
									cell.ToolTipText = "Right-Click for Rate information and totals.";
								}
								else if (GRID.RowOutlineLevel(lngMR) == 2)
								{
									cell.ToolTipText = "Right-Click for Payment information.";
								}
								else
								{
									cell.ToolTipText = "";
								}
							}
							else
							{
								cell.ToolTipText = "";
							}
						}
						else
						{
							cell.ToolTipText = "";
						}
					}
					else
					{
						cell.ToolTipText = "";
					}
				}
			}
		}

		private void GRID_RowColChange(object sender, System.EventArgs e)
		{
			GRID_RowColChange();
		}

		private void GRID_RowColChange()
		{
			// this will change the labels depending apon which line is click
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (GRID.RowOutlineLevel(GRID.Row) == 1)
			{
				rsTemp.OpenRecordset("SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountBills + ") as temp WHERE InvoiceNumber = '" + GRID.TextMatrix(GRID.Row, lngGRIDColInvoiceNumber) + "' AND ActualAccountNumber = " + FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR), modExtraModules.strARDatabase);
				if (rsTemp.EndOfFile() || rsTemp.BeginningOfFile())
				{
				}
				else
				{
					rsTemp.MoveFirst();
					FillLabels_2(FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID")));
				}
			}
			rsTemp.Reset();
		}
		// vbPorter upgrade warning: CurRow As Variant --> As int
		public int LastParentRow(int CurRow)
		{
			int LastParentRow = 0;
			int l;
			int curLvl;
			l = CurRow;
			if (l > 0)
			{
				curLvl = GRID.RowOutlineLevel(l);
				l -= 1;
				while (GRID.RowOutlineLevel(l) > 1)
				{
					l -= 1;
				}
				LastParentRow = l;
			}
			else
			{
				LastParentRow = 0;
			}
			return LastParentRow;
		}

		private void FillMainLabels()
		{
			// from the bill record then from the RE account
			clsDRWrapper rsTemp = new clsDRWrapper();
			string strTemp;
			int intCT;
			// sets the current information at the top of the screen
			// UT Account Info
			strTemp = "SELECT * FROM CustomerMaster WHERE CustomerID = " + FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR);
			if (rsTemp.OpenRecordset(strTemp, modExtraModules.strARDatabase))
			{
				if (!rsTemp.EndOfFile())
				{
					// lblReference1.Text = Trim$(rsTemp.Get_Fields("RSREF1") & " ")
					// strMailingAddress1 = Trim(rsTemp.Get_Fields("RSAddr1")) & " " & Trim(rsTemp.Get_Fields("RSAddr2"))
					// strMailingAddress2 = Trim(rsTemp.Get_Fields("RSAddr3")) & ", " & Trim(rsTemp.Get_Fields("RSState")) & " " & Trim(rsTemp.Get_Fields("RSZip"))
				}
			}
			rsTemp.Reset();
		}

		private void SwapRows_8(int x, int y)
		{
			SwapRows(ref x, ref y);
		}

		private void SwapRows(ref int x, ref int y)
		{
			// this function will switch the two rows values in the grid
			int intCol;
			string strTemp = "";
			for (intCol = lngGRIDColDate; intCol <= GRID.Cols - 1; intCol++)
			{
				strTemp = GRID.TextMatrix(x, intCol);
				GRID.TextMatrix(x, intCol, GRID.TextMatrix(y, intCol));
				GRID.TextMatrix(y, intCol, strTemp);
			}
		}

		private int NextSpacerLine(int x, string strCheck = "=")
		{
			int NextSpacerLine = 0;
			// finds the next spacer line after the row number passed in by finding the next total line
			NextSpacerLine = GRID.FindRow(strCheck, x, lngGRIDColLineCode) + 1;
			return NextSpacerLine;
		}

		private void GRID_Coloring(int l)
		{
			// this sub will recursively call itself in order to color all of the total lines and spacer lines
			int lngTemp;
			lngTemp = NextSpacerLine(l);
			if (lngTemp > 0)
			{
				//GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngTemp, 0, lngTemp, GRID.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX); // &H80000016 'this will hide the text that is living inside of the spacer row
				GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngTemp, 0, lngTemp, GRID.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249)));
				// &H80000016 'this will hide the text that is living inside of the spacer row
				GRID.Select(lngTemp - 1, lngGRIDColPrincipal, lngTemp - 1, lngGRIDColTotal);
				// this will color the totals lines
				GRID.CellBorder(Color.Blue, 0, 1, 0, 0, 0, 0);
				GRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngTemp - 1, 1, lngTemp - 1, GRID.Cols - 1, true);
				GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngTemp - 1, 1, lngTemp - 1, GRID.Cols - 1, Color.FromArgb(5, 204, 71));
				if (Conversion.Val(GRID.TextMatrix(lngTemp - 1, lngGRIDColPrincipal)) != 0)
				{
					sumPrin += FCConvert.ToDouble(GRID.TextMatrix(lngTemp - 1, lngGRIDColPrincipal));
				}
				if (Conversion.Val(GRID.TextMatrix(lngTemp - 1, lngGRIDColInterest)) != 0)
				{
					sumInt += FCConvert.ToDouble(GRID.TextMatrix(lngTemp - 1, lngGRIDColInterest));
				}
				if (Conversion.Val(GRID.TextMatrix(lngTemp - 1, lngGRIDColTax)) != 0)
				{
					sumTax += FCConvert.ToDouble(GRID.TextMatrix(lngTemp - 1, lngGRIDColTax));
				}
				if (Conversion.Val(GRID.TextMatrix(lngTemp - 1, lngGRIDColTotal)) != 0)
				{
					sumTotal += FCConvert.ToDouble(GRID.TextMatrix(lngTemp - 1, lngGRIDColTotal));
				}
				GRID_Coloring(lngTemp + 1);
				// try again recursively
			}
			else
			{
				GRID.Select(0, 1);
			}
		}

		private bool PaymentRecords(ref int BNumber)
		{
			bool PaymentRecords = false;
			// this function will create rows subordinate to the master line for each bill record
			// and in the grid fill them with each payment made against this account
			// this function will return True if Interest Should be calculated for it
			clsDRWrapper rsRK = new clsDRWrapper();
			clsDRWrapper rsPy = new clsDRWrapper();
			// Payroll Recordset
			int lngCount = 0;
			int lngCurrentRow = 0;
			strSQL = "SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountPayments + ") AS Table1 WHERE BillKey = " + FCConvert.ToString(BNumber) + " AND isnull(ReceiptNumber, 0) <> -1 ORDER BY RecordedTransactionDate, ActualSystemDate, ReceiptNumber, ID ";
			// payment lines --- all that apply
			rsPy.OpenRecordset(strSQL, modExtraModules.strARDatabase);
			lngCount = rsPy.RecordCount();
			if (lngCount != 0)
			{
				rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsCL.Get_Fields_Int32("BillNumber"), modExtraModules.strARDatabase);
				int i;
				for (i = 1; i <= lngCount; i++)
				{
					////Application.DoEvents();
					if (FCConvert.ToDouble(Strings.Trim(rsPy.Get_Fields_Int32("ReceiptNumber") + " ")) != -1)
					{
						CurRow += 1;
						Add_6(CurRow, 1);
						lngCurrentRow = CurRow;
						// If rsPy.Get_Fields("EffectiveInterestDate") <> 0 Then
						// .TextMatrix(lngCurrentRow, lngGRIDColDate) = rsPy.Get_Fields("EffectiveInterestDate")
						// Else
						GRID.TextMatrix(lngCurrentRow, lngGRIDColDate, Strings.Trim(FCConvert.ToString(rsPy.Get_Fields_DateTime("RecordedTransactionDate")) + " "));
						// End If
						GRID.TextMatrix(lngCurrentRow, lngGRIDColRef, Strings.Trim(rsPy.Get_Fields_String("Reference") + " "));
						GRID.TextMatrix(lngCurrentRow, lngGRIDColPaymentCode, Strings.Trim(rsPy.Get_Fields_String("Code") + " "));
						// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
						GRID.TextMatrix(lngCurrentRow, lngGRIDColPrincipal, Strings.Trim(rsPy.Get_Fields("Principal") + " "));
						// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
						GRID.TextMatrix(lngCurrentRow, lngGRIDColTax, Strings.Trim(rsPy.Get_Fields("Tax") + " "));
						// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
						GRID.TextMatrix(lngCurrentRow, lngGRIDColInterest, FCConvert.ToString(Conversion.Val(rsPy.Get_Fields("Interest"))));
						// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
						// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
						GRID.TextMatrix(lngCurrentRow, lngGRIDColPTC, FCConvert.ToString(rsPy.Get_Fields("Principal") + rsPy.Get_Fields("Tax")));
						GRID.TextMatrix(lngCurrentRow, lngGRIDColTotal, FCConvert.ToString((FCConvert.ToDecimal(GRID.TextMatrix(lngCurrentRow, lngGRIDColPrincipal)) + FCConvert.ToDecimal(GRID.TextMatrix(lngCurrentRow, lngGRIDColTax)) + FCConvert.ToDecimal(GRID.TextMatrix(lngCurrentRow, lngGRIDColInterest)))));
						GRID.TextMatrix(lngCurrentRow, lngGRIDColLineCode, "-");
						GRID.TextMatrix(lngCurrentRow, lngGRIDColPaymentKey, FCConvert.ToString(rsPy.Get_Fields_Int32("ID")));
						GRID.TextMatrix(lngCurrentRow, lngGRIDColCHGINTNumber, FCConvert.ToString(rsPy.Get_Fields_Int32("CHGINTNumber")));
					}
					PaymentRecords = true;
					boolNoCurrentInt = false;
					rsPy.MoveNext();
				}
			}
			else
			{
				// no payments...
				PaymentRecords = true;
			}
			rsPy.Reset();
			rsRK.Reset();
			return PaymentRecords;
		}

		private void imgNote_DoubleClick(object sender, System.EventArgs e)
		{
			EditNote();
		}

		private void EditNote()
		{
			string strTemp;
			clsDRWrapper rsNote;
			int lngAcctNum;
			lngAcctNum = modARStatusPayments.Statics.lngCurrentCustomerIDAR;
			// this will show the note
			strTemp = Interaction.InputBox("The current account comment is:" + "\r\n" + "\r\n" + strNoteText + "\r\n" + "\r\n" + "Edit the text below to change it or clear the text to remove the note from the account", "Account " + FCConvert.ToString(lngAcctNum) + " Comment", strNoteText);
			strTemp = modGlobalFunctions.RemoveApostrophe(strTemp);
			if (Strings.Trim(strTemp) == Strings.Trim(strNoteText))
			{
				// do nothing
			}
			else
			{
				rsNote = new clsDRWrapper();
				rsNote.DefaultDB = modExtraModules.strARDatabase;
				if (strTemp == "")
				{
					if (MessageBox.Show("Are you sure that you would like to delete this comment?", "Delete Comment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						rsNote.Execute("DELETE FROM Comments WHERE Account = " + FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR) + " AND Type = 'AR'", "TWAR0000.vb1");
						strNoteText = Strings.Trim(strTemp);
						imgNote.Visible = false;
						mnuFilePriority.Visible = false;
					}
				}
				else
				{
					// save the next note text
					strNoteText = Strings.Trim(strTemp);
					rsNote.Execute("UPDATE Comments SET Comment = '" + strTemp + "' WHERE Account = " + FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR) + " AND Type = 'AR'", "TWAR0000.vb1");
					imgNote.Visible = true;
					mnuFilePriority.Visible = true;
				}
				rsNote.Reset();
			}
		}

		private void mnuFileEditNote_Click(object sender, System.EventArgs e)
		{
			// VB6 Bad Scope Dim:
			clsDRWrapper rsNote = new clsDRWrapper();
			if (!imgNote.Visible)
			{
				rsNote.DefaultDB = modExtraModules.strARDatabase;
				rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR));
				if (rsNote.EndOfFile())
				{
					// if there is not a note for this account, add one and then edit it
					rsNote.AddNew();
					rsNote.Set_Fields("Account", modARStatusPayments.Statics.lngCurrentCustomerIDAR);
					rsNote.Set_Fields("Type", "AR");
					rsNote.Update();
				}
			}
			EditNote();
			rsNote.Reset();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			// this will show the summary grid in a report format, only showing the rows that are open
			// at the time the option is choosen
			frmReportViewer.InstancePtr.Init(arARAccountDetail.InstancePtr);
		}

		private void mnuFilePriority_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngAnswer As int --> As DialogResult
			DialogResult lngAnswer;
			clsDRWrapper rsNote = new clsDRWrapper();
			// this will allow the user to set the priority of the note to pop up when the account is entered
			rsNote.DefaultDB = modExtraModules.strARDatabase;
			lngAnswer = MessageBox.Show("Would you like this note to pop up when then account is accessed.", "Note Priority", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			switch (lngAnswer)
			{
				case DialogResult.Yes:
					{
						rsNote.Execute("UPDATE Comments SET Priority = 1 WHERE Account = " + FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR) + "  AND Type = 'AR'", "TWAR0000.vb1");
						break;
					}
				case DialogResult.No:
					{
						rsNote.Execute("UPDATE Comments SET Priority = 0 WHERE Account = " + FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR) + "  AND Type = 'AR'", "TWAR0000.vb1");
						break;
					}
				default:
					{
						// do nothing
						break;
					}
			}
			//end switch
			rsNote.Reset();
		}

		private void mnuPaymentClearList_Click(object sender, System.EventArgs e)
		{
			int lngCT;
			for (lngCT = vsPayments.Rows - 1; lngCT >= 1; lngCT--)
			{
				////Application.DoEvents();
				if (lngCT >= vsPayments.Rows)
				{
					// a secondary check, so that if a CHGINT line is removed while removing the others, then the removal will restart at the last row
					if (vsPayments.Rows > 1)
					{
						lngCT = vsPayments.Rows - 1;
					}
					else
					{
						break;
					}
				}
				if (vsPayments.TextMatrix(lngCT, lngPayGridColRef) != "CHGINT")
				{
					// do not try and remove the CHGINT lines because they will be deleted by their respective payments
					// check to see if any of the payments have already been saved in the database
					if (lngCT <= vsPayments.Rows - 1)
					{
						DeletePaymentRow(lngCT);
						// this will remove it from the CR Summary screen too
					}
				}
			}
			// this will double check for CHGINT payments that may still be in the list
			if (vsPayments.Rows > 0)
			{
				for (lngCT = vsPayments.Rows - 1; lngCT >= 0; lngCT--)
				{
					////Application.DoEvents();
					if (lngCT < vsPayments.Rows - 1)
					{
						// check to see if any of the payments have already been saved in the database
						DeletePaymentRow(lngCT);
						// this will remove it from the CR Summary screen too
					}
				}
			}
			vsPayments.Rows = 1;
			FCUtils.EraseSafe(modARStatusPayments.Statics.ARPaymentArray);
			modARStatusPayments.CheckAllARPending();
		}

		private void mnuPaymentClearPayment_Click(object sender, System.EventArgs e)
		{
			modARStatusPayments.ResetARPaymentFrame();
		}

		private void mnuPaymentPreview_Click(object sender, System.EventArgs e)
		{
			string strInvoiceNumber = "";
			// this sub will show the user a preview of the account and year taking into account all
			// payments that are saved in the payment grid
			if (mnuPaymentPreview.Text != "Exit Preview" && GRID.Row >= 0)
			{
				if (GRID.RowOutlineLevel(GRID.Row) == 1)
				{
					strInvoiceNumber = GRID.TextMatrix(GRID.Row, lngGRIDColInvoiceNumber);
				}
				else
				{
					strInvoiceNumber = GRID.TextMatrix(LastParentRow(GRID.Row), lngGRIDColInvoiceNumber);
				}
				if (Conversion.Val(strInvoiceNumber) != 0)
				{
					modARStatusPayments.CreateARPreview(ref strInvoiceNumber);
				}
			}
			else
			{
				mnuPaymentPreview.Text = "Preview";
				GRID.Visible = true;
				vsPreview.Visible = false;
				vsPreview.Rows = 1;
			}
		}

		private void mnuPaymentSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int i;
				double dblNegAmount = 0;
				// this will add the current payment that is being entered into the payment frame
				// into the listbox below the boxes and into the payment array
				if (!boolPayment || vsPreview.Visible || boolDoNotContinueSave)
				{
					return;
				}
				// this is the regular payment
				int intParrIndex;
				intParrIndex = -1;
				// this is the default, if this is changed then it is the index number of the corresponding payment
				if (txtPrincipal.Text == "")
					txtPrincipal.Text = "0.00";
				if (txtTax.Text == "")
					txtTax.Text = "0.00";
				if (txtInterest.Text == "")
					txtInterest.Text = "0.00";
				if (FCConvert.ToDecimal(txtPrincipal.Text) != 0 || FCConvert.ToDecimal(txtTax.Text) != 0 || FCConvert.ToDecimal(txtInterest.Text) != 0)
				{
					if ((Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "P") || (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "C") || (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "Y"))
					{
						if (modARStatusPayments.ARPaymentGreaterThanOwed_2(FCConvert.CBool(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString() != "Auto")))
						{
							// this function figures out if the payment entered is greater than the years bill
							if (MessageBox.Show("The payment entered is greater than owed, would you like to continue?", "Payment Amount", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								// it is not greater than the total of both grids but greater than the one tht the payment was placed against
								intParrIndex = modARStatusPayments.AddARPaymentToList_615(vsPayments.Rows, FCConvert.CBool(Strings.UCase(frmARCLStatus.InstancePtr.txtReference.Text) == "REVERS") || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "C", true);
							}
							else
							{
								// do not add anything
							}
						}
						else
						{
							// payment is not greater than owed
							intParrIndex = modARStatusPayments.AddARPaymentToList_21(vsPayments.Rows, FCConvert.CBool(Strings.UCase(frmARCLStatus.InstancePtr.txtReference.Text) == "REVERS") || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "C");
						}
					}
					else
					{
						MessageBox.Show("No Code entered!", "Missing Code ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
				else
				{
					if (vsPayments.Rows == 1)
					{
						if (NegativeBillValues())
						{
							if (MessageBox.Show("Apply the negative account values to bills with outstanding balances?", "Negative Account Balances", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								// AffectAllNegativeBillValues
								dblNegAmount = AffectAllNegativeBillValues();
								// set to auto
								cmbBillNumber.SelectedIndex = cmbBillNumber.Items.Count - 1;
								// add the total negative amount
								txtInterest.Text = FCConvert.ToString(dblNegAmount * -1);
								txtPrincipal.Text = "0.00";
								txtTax.Text = "0.00";
								if (dblNegAmount != 0)
								{
									// add the offsetting entries
									modARStatusPayments.AddARPaymentToList_606(vsPayments.Rows, true);
									modARStatusPayments.CheckAllARPending();
								}
							}
						}
						else
						{
							MessageBox.Show("There are no values in the payment fields.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}
				if (intParrIndex == -1)
				{
					return;
				}
				// reset payment boxes
				txtTransactionDate.Text = Strings.Format(modARStatusPayments.Statics.AREffectiveDate, "MM/dd/yyyy");
				txtReference.Text = "";
				txtPrincipal.Text = Strings.Format(0, "#,##0.00");
				txtTax.Text = Strings.Format(0, "#,##0.00");
				txtInterest.Text = Strings.Format(0, "#,##0.00");
				txtComments.Text = "";
				// this is the CHGINT Payment Record
				if (modARStatusPayments.Statics.lngARCHGINT > 0)
				{
					// if this is a payment reverse that has interest charged
					modARStatusPayments.AddARCHGINTToList_15(vsPayments.Rows, false, FCConvert.ToInt16(intParrIndex));
					// then add the reverse of the interest charged to the payment grid
				}
				else if (modARStatusPayments.Statics.lngARCHGINT < 0)
				{
					modARStatusPayments.AddARCHGINTToList_15(vsPayments.Rows, true, FCConvert.ToInt16(intParrIndex));
					// then add the reverse of the interest charged to the payment grid
				}
				modARStatusPayments.CheckAllARPending();
				// this will reset the asteriks on the right side of the form whenever another payment is added
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Payment", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuPaymentSave_Click()
		{
			mnuPaymentSave_Click(mnuPaymentSave, new System.EventArgs());
		}

		private void mnuPaymentSaveExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will create all of the payment records that are saved in the
				if (!boolPayment || vsPreview.Visible || boolDoNotContinueSave)
				{
					return;
				}
				// payment list
				bool boolDone = false;
				clsDRWrapper rsCreatePy = new clsDRWrapper();
				int ArrayIndex = 0;
				DateTime dateTemp = DateTime.FromOADate(0);
				// DJW@11/17/2010 Changed array size from 1000 to 20000
				bool[] boolIntChargedToday = new bool[20000 + 1];
				// this will not let interest get charged more than once a day
				bool boolSaveForCR;
				bool boolDoNotRecalc;
				boolSaveForCR = modExtraModules.IsThisCR();
				
				// normal payment
				boolDone = true;
				if (txtCash.Text == "N" && txtCD.Text == "N" && Strings.InStr(1, "_", txtAcctNumber.TextMatrix(0, 0), CompareConstants.vbBinaryCompare) != 0)
				{
					MessageBox.Show("Please enter an account number unless this is going to affect cash.", "Account Number Missing", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				if (Strings.Trim(txtPrincipal.Text) == "")
					txtPrincipal.Text = "0.00";
				if (Strings.Trim(txtTax.Text) == "")
					txtTax.Text = "0.00";
				if (Strings.Trim(txtInterest.Text) == "")
					txtInterest.Text = "0.00";
				// *********************  this will just make sure that the user wants to go ahead with this action
				if (FCConvert.ToDecimal(txtPrincipal.Text) != 0 || FCConvert.ToDecimal(txtTax.Text) != 0 || FCConvert.ToDecimal(txtInterest.Text) != 0)
				{
					mnuPaymentSave_Click();
				}
				// create the payment records
				// rsCreatePy.BeginTrans
				int lngCounter;
				for (lngCounter = 1; lngCounter <= vsPayments.Rows - 1; lngCounter++)
				{
					// get the recordset ready
					rsCreatePy.OpenRecordset("SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountPayments + ") AS Table1 WHERE AccountKey = 0", modExtraModules.strARDatabase);
					if (Conversion.Val(vsPayments.TextMatrix(lngCounter, lngPayGridColKeyNumber)) == 0)
					{
						// if this is 0 then there is no record created for it in the database
						ArrayIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(lngCounter, lngPayGridColArrayIndex))));
						// this takes the array index from the last part of the list text
						if (ArrayIndex < 0)
						{
							// check to see if it has already been taken care of in CreateARPaymentRecord
							// if is has, then get skip this part because you don't want to create it again
							if (modARStatusPayments.Statics.ARPaymentArray[Math.Abs(FCConvert.ToInt16(ArrayIndex))].ReversalID != -1)
							{
								// this is a CHGINT line
								if (!modARStatusPayments.CreateARCHGINTPaymentRecord_2(FCConvert.ToInt16(ArrayIndex * -1), rsCreatePy))
								{
									boolDone = false;
									break;
								}
							}
						}
						else
						{
							// regular payment line
							if (!modARStatusPayments.CreateARPaymentRecord(ArrayIndex, ref rsCreatePy, ref lngCounter))
							{
								boolDone = false;
								break;
							}
						}
						ArrayIndex = Math.Abs(FCConvert.ToInt16(ArrayIndex));
						// if this is not a reversal of CHGINT and there is some current interest then create a CHGINT line
						if (modARStatusPayments.Statics.lngARCHGINT == 0 && modARStatusPayments.Statics.ARPaymentArray[ArrayIndex].ReversalID == 0 && modARStatusPayments.Statics.dblARCurrentInt[modARStatusPayments.Statics.ARPaymentArray[ArrayIndex].BillNumber] != 0 && !boolIntChargedToday[modARStatusPayments.Statics.ARPaymentArray[ArrayIndex].BillNumber])
						{
							// needs to add the Old Effective Interest Date to the CHGINTDate and the billkey to the field of the last payment
							int lngTemp = 0;
							int lngID = 0;
							int lngBK = 0;
							rsCreatePy.MoveFirst();
							lngID = FCConvert.ToInt32(rsCreatePy.Get_Fields_Int32("ID"));
							vsPayments.TextMatrix(lngCounter, lngPayGridColKeyNumber, FCConvert.ToString(lngID));
							lngTemp = modARStatusPayments.CreateARCHGINTLine(FCConvert.ToInt16(ArrayIndex), ref dateTemp);
							lngBK = FCConvert.ToInt32(rsCreatePy.Get_Fields_Int32("BillKey"));
							rsCreatePy.Edit();
							rsCreatePy.Set_Fields("CHGINTNumber", lngTemp);
							if (modARStatusPayments.Statics.ARPaymentArray[ArrayIndex].ReversalID == -1)
							{
								rsCreatePy.Set_Fields("CHGINTDate", modARStatusPayments.Statics.AREffectiveDate);
							}
							else
							{
								rsCreatePy.Set_Fields("CHGINTDate", modARStatusPayments.Statics.dateAROldDate[modARStatusPayments.Statics.ARPaymentArray[ArrayIndex].BillNumber]);
							}
							rsCreatePy.Update();
							if (!boolSaveForCR)
							{
								rsCreatePy.Execute("UPDATE Bill SET IntPaidDate = '" + Strings.Format(modARStatusPayments.Statics.AREffectiveDate, "MM/dd/yyyy") + "' WHERE ID = " + FCConvert.ToString(lngBK), modExtraModules.strARDatabase);
							}
							boolIntChargedToday[modARStatusPayments.Statics.ARPaymentArray[ArrayIndex].BillNumber] = true;
						}
					}
				}
				if (boolDone)
				{
					boolUnloadOK = true;
					if (boolSaveForCR)
					{
						// if this is in CR then send the information back into the Receipt Input Screen
						this.Hide();
						modUseCR.FillARInfo();
						FormLoaded = false;
					}
					else
					{
						Close();
					}
					boolUnloadOK = false;
				}
				else
				{
					MessageBox.Show("The trasanctions caused an error and have not been applied.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}

				rsCreatePy.Reset();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving/Exit Payment", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuProcessChangeAccount_Click(object sender, System.EventArgs e)
		{
			// this sub will allow the user to go back to frmCLGetAccount and enter another account number
			bool boolGoBack = false;
			if (vsPayments.Rows > 1)
			{
				if (MessageBox.Show("Are you sure that you would like to leave this screen?  Any payments that you have added this session will be lost unless saved.", "Change Account", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					boolGoBack = true;
				}
				else
				{
					boolGoBack = false;
				}
			}
			else
			{
				boolGoBack = true;
			}
			if (boolGoBack)
			{
				Close();
				// Doevents
				if (boolPayment)
				{
					frmARGetMasterAccount.InstancePtr.Init(2);
				}
				else
				{
					frmARGetMasterAccount.InstancePtr.Init(1);
				}
			}
		}

		private void mnuProcessEffective_Click(object sender, System.EventArgs e)
		{
			// This sub will set the Effective Interest Date and reset the grid
			string strDate = "";
			ShowEffectiveInterestDateForm();
		}

		private void ShowEffectiveInterestDateForm()
		{
			// this will show the input form
			// after the user has entered the new effective interest date, then
			// the form will call the GetNewEffectiveInterestDate sub to run the new setup
			frmARDateChange.InstancePtr.Init("Please input the new effective date and then press Process.", ref modARStatusPayments.Statics.AREffectiveDate);
		}

		public void GetNewEffectiveInterestDate(ref DateTime dtDate)
		{
			// this sub will change teh effective interest date and reset the grid
			// boolDoNothing is in case the user cancels, it is defaulted to update the form
			// this is a valid date
			if (modARStatusPayments.Statics.AREffectiveDate.ToOADate() != dtDate.ToOADate())
			{
				modARStatusPayments.Statics.AREffectiveDate = dtDate;
				frmARCLStatus.InstancePtr.Text = "Account Receivable Status as of " + FCConvert.ToString(modARStatusPayments.Statics.AREffectiveDate);
				FormReset = true;
				this.Refresh();
                //Application.DoEvents();
                //this.Activate();
                frmARCLStatus_Activated(this, EventArgs.Empty);
				FormReset = false;
				if (fraPayment.Visible == true)
				{
					fraPayment.Visible = false;
					fraPayment.Visible = true;
				}
			}
		}

		private void Reset_GRID()
		{
			FormReset = true;
			GRID.Rows = 1;
			if (fraPayment.Visible == true)
			{
				FCUtils.EraseSafe(modARStatusPayments.Statics.ARPaymentArray);
				vsPayments.Rows = 1;
			}
			Form_Activate();
		}

		private void Create_Account_Totals()
		{
			int lngCounter = 0;
			// add a account totals row
			lngCounter = GRID.Rows;
			Add_6(lngCounter, -1);
			//FC:FINAL:AM: merge the cells
			//GRID.MergeRow(lngCounter, true);
			GRID.MergeRow(lngCounter, true, 1, 4);
			// .MergeCells = flexMergeSpill
			GRID.TextMatrix(lngCounter, 1, "Account Totals as of " + Strings.Format(modARStatusPayments.Statics.AREffectiveDate, "MM/dd/yyyy"));
			//GRID.TextMatrix(lngCounter, 2, GRID.TextMatrix(lngCounter, 1));
			//GRID.TextMatrix(lngCounter, 3, GRID.TextMatrix(lngCounter, 1));
			//GRID.TextMatrix(lngCounter, 4, GRID.TextMatrix(lngCounter, 1));
			//GRID.TextMatrix(lngCounter, 5, GRID.TextMatrix(lngCounter, 1));
			//GRID.TextMatrix(lngCounter, 6, GRID.TextMatrix(lngCounter, 1));
			GRID.TextMatrix(lngCounter, lngGRIDColPrincipal, Strings.Format(sumPrin, "0.000"));
			GRID.TextMatrix(lngCounter, lngGRIDColTax, Strings.Format(sumTax, "0.00"));
			GRID.TextMatrix(lngCounter, lngGRIDColInterest, Strings.Format(sumInt, "0.0000"));
			GRID.TextMatrix(lngCounter, lngGRIDColPTC, Strings.Format(sumPrin + sumTax, "0.00"));
			if (modGlobal.Round((sumPrin + sumInt + sumTax), 2) == modGlobal.Round(sumTotal, 2))
			{
				GRID.TextMatrix(lngCounter, lngGRIDColTotal, Strings.Format(sumTotal, "0.000"));
			}
			else
			{
				GRID.TextMatrix(lngCounter, lngGRIDColTotal, "ERROR(" + FCConvert.ToString(sumTotal) + ")");
			}
			//GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngCounter, 1, lngCounter, GRID.Cols - 1, 0xFF8080); // light blue  '&H8000& pea green        '&H407000 dark green
			//GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngCounter, 1, lngCounter, GRID.Cols - 1, Color.White);
			//GRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngCounter, 1, lngCounter, GRID.Cols - 1, true);
			GRID.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngCounter, 1, lngCounter, GRID.Cols - 1, Color.White);
			// light blue  '&H8000& pea green        '&H407000 dark green
			GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngCounter, 1, lngCounter, 6, Color.Black);
			GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngCounter, 7, lngCounter, GRID.Cols - 1, Color.FromArgb(5, 204, 71));
			GRID.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngCounter, 1, lngCounter, GRID.Cols - 1, true);
		}

		private void Reset_Sum()
		{
			// this just resets the sums in order to process another account or another effective date
			sumPrin = 0;
			sumInt = 0;
			sumTax = 0;
			sumTotal = 0;
		}

		private void mnuProcessExit_Click(object sender, System.EventArgs e)
		{
			frmARCLStatus.InstancePtr.Close();
		}

		public void mnuProcessExit_Click()
		{
			mnuProcessExit_Click(mnuProcessExit, new System.EventArgs());
		}

		private void Format_PaymentGrid()
		{
			int wid = 0;
			vsPayments.Cols = 14;
			wid = vsPayments.WidthOriginal;
			vsPayments.ColWidth(lngPayGridColInvoice, FCConvert.ToInt32(wid * 0.11));
			// Invoice Number
			vsPayments.ColWidth(lngPayGridColBillID, wid * 0);
			// Bill ID
			vsPayments.ColWidth(lngPayGridColDate, FCConvert.ToInt32(wid * 0.1));
			// Date
			vsPayments.ColWidth(lngPayGridColRef, FCConvert.ToInt32(wid * 0.1));
			// Reference
			vsPayments.ColWidth(lngPayGridColCode, FCConvert.ToInt32(wid * 0.05));
			// Code
			vsPayments.ColWidth(lngPayGridColCDAC, FCConvert.ToInt32(wid * 0.06));
			// CD / Affect Cash
			vsPayments.ColWidth(lngPayGridColPrincipal, FCConvert.ToInt32(wid * 0.14));
			// Principal
			vsPayments.ColWidth(lngPayGridColTax, FCConvert.ToInt32(wid * 0.13));
			// Tax
			vsPayments.ColWidth(lngPayGridColInterest, FCConvert.ToInt32(wid * 0.13));
			// Interest
			vsPayments.ColWidth(lngPayGridColArrayIndex, 0);
			// Array Index
			vsPayments.ColWidth(lngPayGridColKeyNumber, 0);
			// ID Number from autonumberfield in DB
			vsPayments.ColWidth(lngPayGridColCHGINTNumber, 0);
			// CHGINT Number
			vsPayments.ColWidth(lngPayGridColTotal, FCConvert.ToInt32(wid * 0.15));
			// Total
			vsPayments.ColAlignment(lngPayGridColInvoice, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColRef, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColCode, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// all the headers
			//vsPayments.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsPayments.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsPayments.ExtendLastCol = true;
			vsPayments.TextMatrix(0, lngPayGridColInvoice, "Bill");
			vsPayments.TextMatrix(0, lngPayGridColDate, "Date");
			vsPayments.TextMatrix(0, lngPayGridColRef, "Reference");
			vsPayments.TextMatrix(0, lngPayGridColCode, "Code");
			vsPayments.TextMatrix(0, lngPayGridColCDAC, "Cash");
			vsPayments.TextMatrix(0, lngPayGridColPrincipal, "Principal");
			vsPayments.TextMatrix(0, lngPayGridColTax, "Tax");
			vsPayments.TextMatrix(0, lngPayGridColInterest, "Interest");
			vsPayments.TextMatrix(0, lngPayGridColTotal, "Total");
		}

		private void mnuProcessGoTo_Click(object sender, System.EventArgs e)
		{
			if (!modExtraModules.IsThisCR())
			{
				if (boolPayment)
				{
					GoToStatus();
				}
				else
				{
					GoToPayment();
				}
			}
		}

		private void txtAcctNumber_Enter(object sender, System.EventArgs e)
		{
			if (txtAcctNumber.Enabled == false)
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
		}

		private void txtAcctNumber_KeyDownEvent(object sender, KeyEventArgs e)
		{
			// left = 37
			// right = 39
			Keys KeyCode = e.KeyCode;
			switch (FCConvert.ToInt32(KeyCode))
			{
				case 37:
					{
						if (txtAcctNumber.EditSelStart == 0)
						{
							if (txtCash.Enabled == true)
							{
								if (txtCash.Visible && txtCash.Enabled)
								{
									txtCash.Focus();
								}
							}
							else
							{
								if (txtCD.Visible && txtCD.Enabled)
								{
									txtCD.Focus();
								}
							}
							KeyCode = 0;
						}
						break;
					}
				case 39:
					{
						if (txtAcctNumber.EditSelStart == txtAcctNumber.Text.Length - 1)
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
							KeyCode = 0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtAcctNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// validation of the BD account number
			string strAcct = "";
			if (txtCash.Enabled == true)
			{
				// If txtAcctNumber.Enabled = True Then
				// strAcct = txtacctnumber.TextMatrix(0,0)
				// If AccountValidate(strAcct) Then
				// Cancel = False
				// Else
				// Cancel = True
				// MsgBox "Please enter a valid account number.", vbOKOnly + vbInformation, "Invalid Account"
				// End If
				// End If
			}
			else
			{
				txtAcctNumber.Enabled = false;
			}
			clsDRWrapper rsF = new clsDRWrapper();
			boolDoNotContinueSave = false;
			// If gboolCR And gboolBD And txtCash.Text = "N" And txtCD.Text = "N" Then
			// check the fun and make sure that it matches the current type's fund
			// If (InStr(1, txtAcctNumber.Text, "_") = 0 Or Left(txtAcctNumber.Text, 1) = "M") And Len(txtAcctNumber.Text) > 2 Then
			// If Left(txtAcctNumber.Text, 1) <> "M" Then
			// rsF.OpenRecordset "SELECT Account1 FROM Type WHERE TypeCode = 97", strCRDatabase
			// If Not rsF.EndOfFile Then
			// If GetFundFromAccount(txtAcctNumber.Text) <> GetFundFromAccount(rsF.Get_Fields("Account1")) Then
			// MsgBox "This account's fund does not match the fund for this receipt type.", vbExclamation, "Invalid Fund"
			// boolDoNotContinueSave = True
			// End If
			// End If
			// End If
			// End If
			// End If
		}

		private void txtCash_DoubleClick(object sender, System.EventArgs e)
		{
			if (txtCash.Text == "N")
			{
				txtCash.Text = "Y";
			}
			else
			{
				txtCash.Text = "N";
			}
			CheckCash();
		}

		public void txtCash_DblClick()
		{
			txtCash_DoubleClick(txtCash, new System.EventArgs());
		}

		private void txtCash_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// left = 37
			// right = 39
			switch (KeyCode)
			{
				case Keys.Left:
					{
						if (txtCD.Visible && txtCD.Enabled)
						{
							txtCD.Focus();
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						if (txtCash.Text == "N")
						{
							if (txtAcctNumber.Visible && txtPrincipal.Enabled)
							{
								txtAcctNumber.Focus();
							}
						}
						else
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Space:
					{
						txtCash_DblClick();
						break;
					}
				case Keys.N:
					{
						txtCash.Text = "N";
						break;
					}
				case Keys.Y:
					{
						txtCash.Text = "Y";
						break;
					}
				default:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
			CheckCash();
		}

		private void txtCash_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.N:
				case Keys.Y:
				case Keys.Space:
				case Keys.Back:
					{
						// do nothing
						break;
					}
				default:
					{
						KeyAscii = (Keys)0;
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCD_DoubleClick(object sender, System.EventArgs e)
		{
			if (txtCD.Text == "N")
			{
				txtCD.Text = "Y";
			}
			else
			{
				txtCD.Text = "N";
			}
			CheckCD();
		}

		public void txtCD_DblClick()
		{
			txtCD_DoubleClick(txtCD, new System.EventArgs());
		}

		private void txtCD_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						cmbCode.Focus();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtCD.Text == "N")
						{
							if (txtCash.Visible && txtCash.Enabled)
							{
								txtCash.Focus();
							}
						}
						else
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Space:
					{
						txtCD_DblClick();
						break;
					}
				case Keys.N:
					{
						txtCD.Text = "N";
						break;
					}
				case Keys.Y:
					{
						txtCD.Text = "Y";
						break;
					}
				default:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
			CheckCD();
		}

		private void txtCD_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.N:
				case Keys.Y:
				case Keys.Space:
				case Keys.Back:
					{
						// do nothing
						break;
					}
				default:
					{
						KeyAscii = (Keys)0;
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtComments_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtComments.SelectionStart == 0)
						{
							if (txtInterest.Visible && txtInterest.Enabled)
							{
								txtInterest.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtComments.SelectionStart == txtComments.Text.Length)
						{
							vsPayments.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtInterest_Enter(object sender, System.EventArgs e)
		{
			txtInterest.SelectionStart = 0;
			txtInterest.SelectionLength = txtInterest.Text.Length;
		}

		private void txtInterest_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtInterest.SelectionStart == 0)
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtInterest.SelectionStart == txtInterest.Text.Length)
						{
							if (txtComments.Visible && txtComments.Enabled)
							{
								txtComments.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtInterest_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "R" || Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) == "A")
			{
				switch (KeyAscii)
				{
					case Keys.NumPad3:
					case Keys.C:
					case Keys.Back:
						{
							// c, C, 0 - this will clear the box
							KeyAscii = (Keys)0;
							txtInterest.Text = "0.00";
							txtInterest.SelectionStart = 0;
							txtInterest.SelectionLength = 4;
							break;
						}
				}
				//end switch
			}
			else
			{
				if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
				{
					// do nothing
				}
				else if (KeyAscii == Keys.Insert)
				{
					// minus
					KeyAscii = (Keys)0;
					if (Conversion.Val(txtInterest.Text) == 0)
					{
						txtInterest.Text = "0.00";
					}
					txtInterest.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtInterest.Text)) * -1, "#,##0.00");
					txtInterest.SelectionStart = 0;
					txtInterest.SelectionLength = txtInterest.Text.Length;
				}
				else if (KeyAscii == Keys.Execute)
				{
					// plus
					KeyAscii = (Keys)0;
					if (Conversion.Val(txtInterest.Text) == 0)
					{
						txtInterest.Text = "0.00";
					}
					txtInterest.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtInterest.Text)), "#,##0.00");
					txtInterest.SelectionStart = 0;
					txtInterest.SelectionLength = txtInterest.Text.Length;
				}
				else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
				{
					// c, C - this will clear the box
					KeyAscii = (Keys)0;
					txtInterest.Text = "0.00";
					txtInterest.SelectionStart = 0;
					txtInterest.SelectionLength = 4;
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtInterest_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (txtInterest.Text != "")
				{
					if (FCConvert.ToDecimal(txtInterest.Text) > 9999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $9,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtInterest.Text = Strings.Format(9999999.99, "#,##0.00");
					}
					else if (FCConvert.ToDecimal(txtInterest.Text) < -9999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $-9,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtInterest.Text = Strings.Format(-9999999.99, "#,##0.00");
					}
					else
					{
						txtInterest.Text = Strings.Format(FCConvert.ToDecimal(txtInterest.Text), "#,##0.00");
					}
				}
				else
				{
					txtInterest.Text = "0.00";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtPrincipal_Enter(object sender, System.EventArgs e)
		{
			txtPrincipal.SelectionStart = 0;
			txtPrincipal.SelectionLength = txtPrincipal.Text.Length;
		}

		private void txtPrincipal_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtPrincipal.SelectionStart == 0)
						{
							if (txtCD.Text == "N")
							{
								if (txtCash.Text == "N")
								{
									txtAcctNumber.Focus();
								}
								else
								{
									txtAcctNumber.Enabled = false;
									if (txtCash.Visible && txtCash.Enabled)
									{
										txtCash.Focus();
									}
								}
							}
							else
							{
								if (txtCD.Visible && txtCD.Enabled)
								{
									txtCD.Focus();
								}
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtPrincipal.SelectionStart == txtPrincipal.Text.Length)
						{
							txtInterest.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtPrincipal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else if (KeyAscii == Keys.Insert)
			{
				// minus
				KeyAscii = (Keys)0;
				if (Conversion.Val(txtPrincipal.Text) == 0)
				{
					txtPrincipal.Text = "0.00";
				}
				txtPrincipal.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtPrincipal.Text)) * -1, "#,##0.00");
				txtPrincipal.SelectionStart = 0;
				txtPrincipal.SelectionLength = txtPrincipal.Text.Length;
			}
			else if (KeyAscii == Keys.Execute)
			{
				// plus
				KeyAscii = (Keys)0;
				if (Conversion.Val(txtPrincipal.Text) == 0)
				{
					txtPrincipal.Text = "0.00";
				}
				txtPrincipal.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtPrincipal.Text)), "#,##0.00");
				txtPrincipal.SelectionStart = 0;
				txtPrincipal.SelectionLength = txtPrincipal.Text.Length;
			}
			else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
			{
				// c, C - this will clear the box
				KeyAscii = (Keys)0;
				txtPrincipal.Text = "0.00";
				txtPrincipal.SelectionStart = 0;
				txtPrincipal.SelectionLength = 4;
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtPrincipal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this is validaiting the amount entered
				if (txtPrincipal.Text != "")
				{
					if (FCConvert.ToDecimal(txtPrincipal.Text) > 99999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtPrincipal.Text = Strings.Format(99999999.99, "#,##0.00");
					}
					else if (FCConvert.ToDecimal(txtPrincipal.Text) < -99999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $-99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtPrincipal.Text = Strings.Format(-99999999.99, "#,##0.00");
					}
					else
					{
						txtPrincipal.Text = Strings.Format(FCConvert.ToDecimal(txtPrincipal.Text), "#,##0.00");
					}
				}
				else
				{
					txtPrincipal.Text = "0.00";
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtReference_Enter(object sender, System.EventArgs e)
		{
			boolRefKeystroke = false;
		}

		private void txtReference_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			bool boolTest = false;
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtReference.SelectionStart == 0)
						{
							if (txtTransactionDate.Visible && txtTransactionDate.Enabled)
							{
								txtTransactionDate.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtReference.SelectionStart == txtReference.Text.Length)
						{
							boolRefKeystroke = true;
							cmbCode.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Return:
					{
						boolTest = false;
						txtReference_Validate(ref boolTest);
						if (boolTest)
						{
						}
						else
						{
							txtInterest.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtReference_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// catches reserved words
			if ((Strings.UCase(Strings.Trim(txtReference.Text)) == "CHGINT") || (Strings.UCase(Strings.Trim(txtReference)) == "CNVRSN") || (Strings.UCase(Strings.Trim(txtReference.Text)) == "REVRSE"))
			{
				MessageBox.Show(Strings.UCase(Strings.Trim(txtReference.Text)) + " is a reserved reference string.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}

		public void txtReference_Validate(ref bool Cancel)
		{
			txtReference_Validating(txtReference, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtTotalPendingDue_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// this will not allow any input to this text box
			KeyAscii = (Keys)0;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTransactionDate_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (FCConvert.ToInt32(KeyCode) == 37)
			{
				// left = 37
				if (txtTransactionDate.SelStart == 0)
				{
					cmbBillNumber.Focus();
					KeyCode = 0;
				}
			}
			else if (FCConvert.ToInt32(KeyCode) == 39)
			{
				// right = 39
				if (txtTransactionDate.SelStart == txtTransactionDate.Text.Length)
				{
					txtReference.Focus();
					KeyCode = 0;
				}
			}
			else if (KeyCode == Keys.Return)
			{
				if (txtReference.Visible && txtReference.Enabled)
				{
					txtReference.Focus();
				}
				else
				{
					Support.SendKeys("{tab}", false);
				}
			}
			else if ((KeyCode == Keys.Back) || (KeyCode == Keys.Delete) || (FCConvert.ToInt32(KeyCode) >= 48 && FCConvert.ToInt32(KeyCode) <= 57) || (FCConvert.ToInt32(KeyCode) == 191) || (FCConvert.ToInt32(KeyCode) == 111) || (FCConvert.ToInt32(KeyCode) >= 96 && FCConvert.ToInt32(KeyCode) <= 105))
			{
				// backspace, delete, 0-9, "/", numberpad "/", 0-9 all can pass
			}
			else
			{
				KeyCode = 0;
			}
		}

		private void txtTransactionDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(Strings.Left(txtTransactionDate.Text, 2)) > 12)
			{
				e.Cancel = true;
				MessageBox.Show("Please use the date format MM/dd/yyyy.", "Invalid Date Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void vsPayments_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int l;
			if (KeyCode == Keys.Delete)
			{
				if (MessageBox.Show("Are you sure that you would like to delete this payment?", "Delete Payment Record", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (vsPayments.TextMatrix(vsPayments.Row, lngPayGridColRef) == "AUTO" || vsPayments.TextMatrix(vsPayments.Row, lngPayGridColRef) == "OVERPAY" || vsPayments.TextMatrix(vsPayments.Row, lngPayGridColRef) == "PREPAY-A")
					{
						// if this is an automatic payment, then delete all the automatic payments
						for (l = vsPayments.Rows - 1; l >= 1; l--)
						{
							if (vsPayments.TextMatrix(l, lngPayGridColRef) == "AUTO" || vsPayments.TextMatrix(l, lngPayGridColRef) == "OVERPAY" || vsPayments.TextMatrix(l, lngPayGridColRef) == "PREPAY-A")
							{
								DeletePaymentRow(l);
							}
						}
					}
					else if (vsPayments.Row != 0)
					{
						DeletePaymentRow(vsPayments.Row);
					}
					modARStatusPayments.CheckAllARPending();
					// this resets all of the pending markers
				}
			}
			KeyCode = 0;
		}

		private void GoToStatus()
		{
			fraPayment.Visible = false;
			//FC:FINAL:JEI:IT377 fixed layout
			//GRID.Top = fraStatusLabels.Top + fraStatusLabels.Height + 200;
			GRID.Top = lblOwnersName.Top + lblOwnersName.Height + 10;
			//FC:FINAL:BBE:#298 - adjust the grid Height correctly
			//GRID.Height = FCConvert.ToInt32(frmARCLStatus.InstancePtr.Height - GRID.Top - (frmARCLStatus.InstancePtr.Height * 0.09));
			GRID.Height = FCConvert.ToInt32((frmARCLStatus.InstancePtr.ClientArea.Height - GRID.Top) * 0.96);
			mnuPayment.Visible = false;
			mnuProcessGoTo.Text = strPaymentString;
			if (GRID.Enabled && GRID.Visible)
			{
				GRID.Focus();
			}
			mnuPaymentSave.Enabled = false;
			mnuPaymentSaveExit.Enabled = false;
			cmdPaymentSave.Enabled = false;
			cmdProcess.Enabled = false;
			boolPayment = false;
			// SetHeadingLabels
		}

		private void GoToPayment()
		{
			fraStatusLabels.Visible = false;
			fraPayment.Visible = true;
			mnuPayment.Visible = true;
			vsPayments.Visible = true;
			//FC:FINAL:BBE:#298 - used anchoring instead
			//fraPayment.Left = FCConvert.ToInt32((this.Width - fraPayment.Width) / 3.0);
			mnuProcessGoTo.Text = strStatusString;
			// Doevents
			//FC:FINAL:JEI:IT377 fixed layout
			//GRID.Top = lblPaymentInfo.Top + lblPaymentInfo.Height;
			GRID.Top = lblOwnersName.Top + lblOwnersName.Height + 10;
			//FC:FINAL:RPU: #305- Set the height to be lower
			GRID.Height = FCConvert.ToInt32(frmARCLStatus.InstancePtr.ClientArea.Height * 0.40);
			//FC:FINAL:JEI:IT377 fixed layout
			fraPayment.Top = GRID.Bottom;
			frmARCLStatus.InstancePtr.Refresh();
			modARStatusPayments.FillARPaymentComboBoxes();
			modARStatusPayments.ResetARPaymentFrame();
			Format_PaymentGrid();
			modARStatusPayments.FillPendingARTransactions();
			modARStatusPayments.Format_ARvsPeriod();
			txtReference.Focus();
			mnuPaymentSave.Enabled = true;
			mnuPaymentSaveExit.Enabled = true;
			cmdPaymentSave.Enabled = true;
			cmdProcess.Enabled = true;
			boolPayment = true;
			// SetHeadingLabels
		}

		private bool NegativeBillValues()
		{
			bool NegativeBillValues = false;
			// this will return true if any year has negative values
			int lngRow;
			lngRow = 0;
			do
			{
				lngRow = GRID.FindRow("=", lngRow, lngGRIDColLineCode) + 1;
				if (lngRow > 0)
				{
					if (Conversion.Val(GRID.TextMatrix(lngRow - 1, lngGRIDColTotal)) != 0)
					{
						if (FCConvert.ToDouble(GRID.TextMatrix(lngRow - 1, lngGRIDColTotal)) < 0 && LastParentRow(lngRow) > 1)
							NegativeBillValues = true;
					}
				}
			}
			while (!(NegativeBillValues == true || GRID.FindRow("=", lngRow, lngGRIDColLineCode) == -1));
			return NegativeBillValues;
		}

		private double AffectAllNegativeBillValues()
		{
			double AffectAllNegativeBillValues = 0;
			// this will create a payment against the negative value to zero it out and also
			// an equal amount against any newer bills, leaving the extra in the last year
			int lngRow;
			double dblNegTotal = 0;
			string strInvoice = "";
			// collapse all of the grid
			modGlobalFunctions.CollapseRows(0, GRID, 1);
			// start at the oldest year and check for negative balances
			for (lngRow = GRID.Rows - 2; lngRow >= 1; lngRow--)
			{
				if (GRID.RowOutlineLevel(lngRow) == 1)
				{
					// check for the master row
					if (Conversion.Val(GRID.TextMatrix(lngRow, lngGRIDColTotal)) != 0)
					{
						// make sure that the row is not text
						if (FCConvert.ToDouble(GRID.TextMatrix(lngRow, lngGRIDColTotal)) < 0 && FCConvert.ToDouble(GRID.TextMatrix(lngRow, lngGRIDColInvoiceNumber)) != 0)
						{
							// check for a value that below zero
							// payment to zero out the year
							strInvoice = GRID.TextMatrix(lngRow, lngGRIDColInvoiceNumber);
							dblNegTotal += AffectBillYear_2(true, strInvoice, lngRow);
						}
						else if (dblNegTotal < 0)
						{
						}
					}
				}
			}
			AffectAllNegativeBillValues = dblNegTotal;
			return AffectAllNegativeBillValues;
		}

		private double AffectBillYear_2(bool boolNegative, string strInvoice, int lngRow)
		{
			return AffectBillYear(ref boolNegative, ref strInvoice, ref lngRow);
		}

		private double AffectBillYear(ref bool boolNegative, ref string strInvoice, ref int lngRow)
		{
			double AffectBillYear = 0;
			// this function will create a bill record to zero out the payments are best as possible
			// it will use as much as needed by the year to be payed off, or as much as it has in dblNegValue
			clsDRWrapper rs = new clsDRWrapper();
			clsDRWrapper rsPay = new clsDRWrapper();
			string strSQL = "";
			double dblPrinNeed = 0;
			double dblTaxNeed;
			double dblIntNeed = 0;
			double dblPrin;
			double dblInt;
			double dblNegValue;
			int i;
			int lngBill = 0;
			AffectBillYear = FCConvert.ToDouble(GRID.TextMatrix(lngRow, lngGRIDColTotal));
			dblNegValue = FCConvert.ToDouble(GRID.TextMatrix(lngRow, lngGRIDColTotal));
			if (boolNegative)
			{
				// this creates a negative bill to zero out the neg year
				// this will return true if the creation was successful
				strSQL = "SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountBills + ") as temp WHERE ActualAccountNumber = " + FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR) + " AND InvoiceNumber = '" + strInvoice + "'";
				rs.OpenRecordset(strSQL, modExtraModules.strARDatabase);
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					lngBill = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
					txtReference.Text = "AUTO";
					for (i = 0; i <= cmbBillNumber.Items.Count - 1; i++)
					{
						////Application.DoEvents();
						if (Conversion.Val(cmbBillNumber.Items[i].ToString()) == Conversion.Val(strInvoice))
						{
							cmbBillNumber.SelectedIndex = i;
							break;
						}
					}
					cmbCode.SelectedIndex = 1;
					for (i = 0; i <= cmbCode.Items.Count - 1; i++)
					{
						if (Strings.Left(cmbCode.Items[i].ToString(), 1) == "P")
						{
							cmbCode.SelectedIndex = i;
							break;
						}
					}
					// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
					dblPrinNeed = modGlobal.Round(rs.Get_Fields_Decimal("PrinOwed") - rs.Get_Fields("PrinPaid"), 2);
					// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
					dblIntNeed = modGlobal.Round(rs.Get_Fields("IntAdded") + modARStatusPayments.Statics.dblARCurrentInt[lngBill] - rs.Get_Fields("IntPaid"), 2);
					if (dblIntNeed == 0 && dblPrinNeed == 0)
					{
						return AffectBillYear;
					}
					if (dblIntNeed > dblNegValue)
					{
						txtInterest.Text = Strings.Format(dblIntNeed, "#,##0.00");
						dblNegValue -= dblIntNeed;
					}
					else
					{
						txtInterest.Text = Strings.Format(dblNegValue, "#,##0.00");
						dblNegValue = 0;
					}
					if (dblPrinNeed > dblNegValue)
					{
						txtPrincipal.Text = Strings.Format(dblPrinNeed, "#,##0.00");
						dblNegValue -= dblPrinNeed;
					}
					else
					{
						txtPrincipal.Text = Strings.Format(dblNegValue, "#,##0.00");
						dblNegValue = 0;
					}
					modARStatusPayments.AddARPaymentToList_24(vsPayments.Rows, true, true);
					// this will not let the negative principal get reset to other places
					cmbBillNumber.SelectedIndex = cmbBillNumber.Items.Count - 1;
					// this will set it to AUTO
					// this gets set to auto so that the pre written code will take over
					// what is this part for???
					dblNegValue = FCConvert.ToDouble(GRID.TextMatrix(lngRow, lngGRIDColTotal));
					// DJW 6/3/2008 Commented out most of this since the auto payment will figur eout where to put the money
					// If dblIntNeed > dblNegValue Then
					// txtInterest.Text = Format(dblIntNeed * -1, "#,##0.00")
					// dblNegValue = dblNegValue - dblIntNeed
					// Else
					txtInterest.Text = Strings.Format(dblNegValue * -1, "#,##0.00");
					dblNegValue = 0;
					// End If
					// 
					// If dblPrinNeed > dblNegValue Then
					// txtPrincipal.Text = Format(dblPrinNeed * -1, "#,##0.00")
					// dblNegValue = dblNegValue - dblPrinNeed
					// Else
					// txtPrincipal.Text = Format(dblNegValue * -1, "#,##0.00")
					// dblNegValue = 0
					// End If
					// AddARPaymentToList vsPayments.rows
				}
			}
			rs.Reset();
			rsPay.Reset();
			return AffectBillYear;
		}

		private void DeletePaymentRow_2(int Row)
		{
			DeletePaymentRow(Row);
		}

		private void DeletePaymentRow(int Row)
		{
			bool boolTemp;
			int intArrIndex;
			int lngIDNumber;
			int l;
			lngIDNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(Row, lngPayGridColKeyNumber))));
			intArrIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(Row, lngPayGridColArrayIndex))));
			boolTemp = EraseCHGINTPaymentLine(ref Row);
			if (intArrIndex > 0)
			{
				modARStatusPayments.ResetARPayment(ref intArrIndex, boolTemp);
				// this will delete if from the database if it is already stored and clear the payment array
			}
			if (lngIDNumber != 0)
			{
				for (l = 1; l <= vsPayments.Rows - 1; l++)
				{
					if (Conversion.Val(vsPayments.TextMatrix(l, lngPayGridColKeyNumber)) == lngIDNumber)
					{
						if (l != 0)
						{
							vsPayments.RemoveItem(l);
							// remove the line from the grid
						}
						break;
					}
				}
			}
			else
			{
				if (vsPayments.Rows > 2)
				{
					// more than one payment showing
					vsPayments.RemoveItem(Row);
				}
				else
				{
					vsPayments.Rows = 1;
					// clear the grid out
					FCUtils.EraseSafe(modARStatusPayments.Statics.ARPaymentArray);
					// erase all the payments
				}
			}
			// check for CR
			if (modExtraModules.IsThisCR() && lngIDNumber > 0)
			{
				// this will delete the payment from the shopping list
				modUseCR.DeleteARPaymentFromShoppingList(ref lngIDNumber);
			}
		}

		private bool EraseCHGINTPaymentLine(ref int Row)
		{
			bool EraseCHGINTPaymentLine = false;
			// this will check to see if there is a CHGINT line associated with the payment that is being
			// erased and find out if there is any other payments that it should be associated with instead of being erased
			// and will associate it, if needed
			int lngIndex;
			clsDRWrapper rsCHGINT = new clsDRWrapper();
			int lngID;
			int lngCHGINTNumber = 0;
			int i;
			bool boolEraseCHGINT;
			int lngDeleteRow;
			int lngNew = 0;
			int lngCHGINTRow = 0;
			clsDRWrapper rsTemp = new clsDRWrapper();
			lngDeleteRow = Row;
			// find out if the payment being deleted has a CHGINT or EARNINT line with it
			lngIndex = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(lngDeleteRow, lngPayGridColArrayIndex))));
			lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(vsPayments.TextMatrix(lngDeleteRow, lngPayGridColKeyNumber))));
			if (lngIndex > 0 && lngIndex < modARStatusPayments.MAX_ARPAYMENTS)
			{
				rsCHGINT.OpenRecordset("SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountPayments + ") AS Table1 WHERE ID = " + FCConvert.ToString(lngID), modExtraModules.strARDatabase);
				if (rsCHGINT.EndOfFile() != true && rsCHGINT.BeginningOfFile() != true)
				{
					if (FCConvert.ToInt32(rsCHGINT.Get_Fields_Int32("CHGINTNumber")) != 0)
					{
						lngCHGINTNumber = FCConvert.ToInt32(rsCHGINT.Get_Fields_Int32("CHGINTNumber"));
					}
				}
				else if (modARStatusPayments.Statics.ARPaymentArray[lngIndex].CHGINTNumber != 0)
				{
					lngCHGINTNumber = modARStatusPayments.Statics.ARPaymentArray[lngIndex].CHGINTNumber;
				}
				lngNew = 0;
				// default to true then attempt to prove otherwise
				if (lngCHGINTNumber != 0)
				{
					// if there is a CHGINT or EARNINT line
					// find out if there is another line that would require a CHGINT/EARNINT line
					for (i = 1; i <= vsPayments.Rows - 1; i++)
					{
						if (!rsCHGINT.EndOfFile())
						{
							if (Strings.Right(vsPayments.TextMatrix(i, lngPayGridColInvoice), 4) == rsCHGINT.Get_Fields_String("InvoiceNumber"))
							{
								// check for the same bill
								if (vsPayments.TextMatrix(i, lngPayGridColRef) != "CHGINT" && vsPayments.TextMatrix(i, lngPayGridColRef) != "EARNINT" && Conversion.Val(vsPayments.TextMatrix(i, lngPayGridColKeyNumber)) != lngID && modARStatusPayments.Statics.ARPaymentArray[FCConvert.ToInt32(vsPayments.TextMatrix(i, lngPayGridColArrayIndex))].EffectiveInterestDate.ToOADate() == rsCHGINT.Get_Fields_DateTime("EffectiveInterestDate").ToOADate())
								{
									// check to see if it is a CHGINT or EARNINT and has the same effective interest date
									lngNew = i;
									// this will give the row of the first payment during the same year
									break;
								}
							}
						}
					}
					// find the row that the CHGINT line is stored in the grid
					for (i = 1; i <= vsPayments.Rows - 1; i++)
					{
						if (Conversion.Val(vsPayments.TextMatrix(i, lngPayGridColKeyNumber)) == lngCHGINTNumber)
						{
							lngCHGINTRow = i;
							break;
						}
					}
					// delete the CHGINT or EARNINT line
					if (lngNew == 0)
					{
						// if there is no line to hook the CHGINT or EARNINT to
						// erase the line and the corresponding record
						// DJW 5/27/2008 Changed following code form an execute deleting the CHGINT line to code which dits the bill records and removes the previously added interest
						// --------------------------------------------------------------------------------
						rsCHGINT.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngCHGINTNumber), modExtraModules.strARDatabase);
						if (rsCHGINT.EndOfFile() != true && rsCHGINT.BeginningOfFile() != true)
						{
							// kk 12182013 troar-61  DON'T UPDATE THE BILL RECORD FROM CR!
							if (!modExtraModules.IsThisCR())
							{
								rsTemp.OpenRecordset("SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountBills + ") as temp WHERE ID = " + rsCHGINT.Get_Fields_Int32("BillKey"), modExtraModules.strARDatabase);
								if (rsTemp.RecordCount() != 0)
								{
									rsTemp.Edit();
									// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
									// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
									rsTemp.Set_Fields("IntAdded", (rsTemp.Get_Fields("IntAdded") - rsCHGINT.Get_Fields("Interest")));
									// this might be where I should adjust the effective int date
									rsTemp.Update();
								}
							}
							rsCHGINT.Delete();
							rsCHGINT.MoveNext();
						}
						// ---------------------------------------------------------------------------------
						if (lngCHGINTRow > 0)
						{
							vsPayments.RemoveItem(lngCHGINTRow);
						}
						EraseCHGINTPaymentLine = true;
					}
					else
					{
						// change the CHGINT of the next payment made for that same account/year/?EDate
						if (Conversion.Val(vsPayments.TextMatrix(lngNew, lngPayGridColKeyNumber)) == 0)
						{
							EraseCHGINTPaymentLine = false;
							modARStatusPayments.Statics.ARPaymentArray[FCConvert.ToInt32(vsPayments.TextMatrix(lngNew, lngPayGridColArrayIndex))].CHGINTNumber = lngCHGINTNumber;
							vsPayments.TextMatrix(lngNew, lngPayGridColCHGINTNumber, FCConvert.ToString(lngCHGINTNumber));
							if (FCConvert.ToDouble(vsPayments.TextMatrix(lngNew, lngPayGridColKeyNumber)) != 0)
							{
								// if the payment line has been saved in the database then
								rsCHGINT.OpenRecordset("SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountPayments + ") AS Table1 WHERE ID = " + vsPayments.TextMatrix(lngNew, lngPayGridColKeyNumber), modExtraModules.strARDatabase);
								if (rsCHGINT.EndOfFile() != true && rsCHGINT.BeginningOfFile() != true)
								{
									rsCHGINT.Edit();
									rsCHGINT.Set_Fields("CHGINTNumber", lngCHGINTNumber);
									rsCHGINT.Update(false);
								}
							}
						}
						else
						{
							// erase the payment line and the corresponding record
							rsCHGINT.Execute("DELETE FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngCHGINTNumber), modExtraModules.strARDatabase);
							if (lngCHGINTRow > 0)
							{
								vsPayments.RemoveItem(lngCHGINTRow);
							}
							EraseCHGINTPaymentLine = true;
						}
					}
				}
			}
			else
			{
				if (lngCHGINTRow > 0)
				{
					vsPayments.RemoveItem(lngCHGINTRow);
				}
			}
			rsCHGINT.Reset();
			return EraseCHGINTPaymentLine;
		}

		private void FillInARInfo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsMainInfo = new clsDRWrapper();
				rsMainInfo.OpenRecordset("SELECT * FROM CustomerMaster WHERE CustomerID = " + FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR), modExtraModules.strARDatabase);
				if (!rsMainInfo.EndOfFile())
				{
					pInfo = pCont.GetParty(FCConvert.ToInt32(rsMainInfo.Get_Fields_Int32("PartyID")));
					lblOwnersName.Text = pInfo.FullName;
					CurrentOwner = Strings.Trim(lblOwnersName.Text);
					lblOwnersName.Visible = true;
					lblName.Visible = true;
					lblOwnersName.BackColor = Color.White;
					OwnerPartyID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMainInfo.Get_Fields_Int32("PartyID"))));
					// Set party id for CR
					lblAccount.Text = FCConvert.ToString(rsMainInfo.Get_Fields_Int32("CustomerID"));
					lblAccount.Visible = true;
					// this label will display the account information when the form is in Payment Mode
					//FC:FINAL:BBE:#332 - name label should be beside name below. Remove extra name.
					//lblPaymentInfo.Text = "Customer #: " + lblAccount.Text + "     Name: " + lblOwnersName.Text;
					lblPaymentInfo.Text = "Customer #: " + lblAccount.Text;
				}
				else
				{
					OwnerPartyID = 0;
				}
				rsMainInfo.Reset();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling UT Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowRateInfo_6(int cRow, int cCol, clsDRWrapper rsCL)
		{
			ShowRateInfo(ref cRow, ref cCol, ref rsCL);
		}

		private void ShowRateInfo(ref int cRow, ref int cCol, ref clsDRWrapper rsCL)
		{
			int lngErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will show a box with the rate information in it
				// as soon as the box loses focus, I will make it disappear
				clsDRWrapper rsRI = new clsDRWrapper();
				clsDRWrapper rsBT = new clsDRWrapper();
				int i;
				int RK;
				DateTime dtInterestPDThroughDate = DateTime.FromOADate(0);
				double dblTotalAbate;
				double dblPrin = 0;
				double dblTax = 0;
				double dblPaid = 0;
				double dblInt = 0;
				double dblAbate;
				string strInvoice = "";
				string strAddr1 = "";
				string strAddr2 = "";
				string strAddr3 = "";
				string strAddr4 = "";
				lngErrCode = 1;
				RK = rsCL.Get_Fields_Int32("BillNumber");
				lngErrCode = 2;
				lngErrCode = 5;
				rsRI.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(RK), modExtraModules.strARDatabase);
				// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
				rsBT.OpenRecordset("SELECT * FROM DefaultBillTypes WHERE TypeCode = " + rsCL.Get_Fields("BillType"), modExtraModules.strARDatabase);
				// format grid
				frmARCLStatus.InstancePtr.vsRateInfo.Cols = 1;
				frmARCLStatus.InstancePtr.vsRateInfo.Cols = 5;
				frmARCLStatus.InstancePtr.vsRateInfo.Rows = 26;
				//FC:FINAL:AM:#4 - decrease the height of rows
				vsRateInfo.RowHeight(-1, 380);
				frmARCLStatus.InstancePtr.vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				fraRateInfo.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				strInvoice = FCConvert.ToString(Conversion.Val(GRID.TextMatrix(cRow, cCol)));
				frmARCLStatus.InstancePtr.vsRateInfo.ColWidth(0, FCConvert.ToInt32(frmARCLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.25));
				frmARCLStatus.InstancePtr.vsRateInfo.ColWidth(1, FCConvert.ToInt32(frmARCLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.19));
				frmARCLStatus.InstancePtr.vsRateInfo.ColWidth(2, FCConvert.ToInt32(frmARCLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.04));
				frmARCLStatus.InstancePtr.vsRateInfo.ColWidth(3, FCConvert.ToInt32(frmARCLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.23));
				frmARCLStatus.InstancePtr.vsRateInfo.ColWidth(4, FCConvert.ToInt32(frmARCLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.29));
				frmARCLStatus.InstancePtr.vsRateInfo.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
				frmARCLStatus.InstancePtr.vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//frmARCLStatus.InstancePtr.vsRateInfo.HeightOriginal = frmARCLStatus.InstancePtr.vsRateInfo.RowHeight(0) * frmARCLStatus.InstancePtr.vsRateInfo.Rows + 50;
				frmARCLStatus.InstancePtr.vsRateInfo.Select(0, 0);
				frmARCLStatus.InstancePtr.vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
				// show the first set of information (rate information and period info)--------------
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 0, "Bill ID");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 0, "Rate Record");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 0, "Bill Description");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 0, "Created By");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 0, "");
				if (!rsBT.EndOfFile())
				{
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 0, FCConvert.ToString(rsBT.Get_Fields_String("Reference")));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 0, FCConvert.ToString(rsBT.Get_Fields_String("Control1")));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 0, FCConvert.ToString(rsBT.Get_Fields_String("Control2")));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 0, FCConvert.ToString(rsBT.Get_Fields_String("Control3")));
				}
				else
				{
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 0, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 0, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 0, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 0, "");
				}
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 0, "Billing Date");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 0, "Creation Date");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 0, "Period Start");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 0, "Period End");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 0, "Interest Date");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 0, "");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 0, "Principal");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 0, "Tax");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 0, "Interest");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(22, 0, "Paid");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(23, 0, "");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(24, 0, "Total Due");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(25, 0, "Per Diem");
				if (rsRI.BeginningOfFile() != true && rsRI.EndOfFile() != true)
				{
					string vbPorterVar = rsRI.Get_Fields_String("InterestMethod");
					if (vbPorterVar == "AP")
					{
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 0, "Interest Rate");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1, Strings.Format(rsRI.Get_Fields_Double("IntRate"), "#0.00%"));
					}
					else if (vbPorterVar == "AF")
					{
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 0, "Late Fee");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1, Strings.Format(rsRI.Get_Fields_Decimal("FlatRate"), "#,##0.00"));
					}
					else
					{
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 0, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 1, "");
					}
					lngErrCode = 6;
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 1, rsCL.Get_Fields_Int32("ID"));
					lngErrCode = 7;
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 1, FCConvert.ToString(rsRI.Get_Fields_Int32("ID")));
					lngErrCode = 8;
					// TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 1, modARStatusPayments.GetBillDescription(rsCL.Get_Fields("BillType")));
					lngErrCode = 9;
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 1, rsCL.Get_Fields_String("CreatedBy"));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1, "");
					lngErrCode = 10;
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 1, rsCL.Get_Fields_String("Reference"));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 1, rsCL.Get_Fields_String("Control1"));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 1, rsCL.Get_Fields_String("Control2"));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 1, rsCL.Get_Fields_String("Control3"));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 1, Strings.Format(rsRI.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy"));
					lngErrCode = 11;
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 1, Strings.Format(rsRI.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy"));
					lngErrCode = 12;
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 1, Strings.Format(rsRI.Get_Fields_DateTime("Start"), "MM/dd/yyyy"));
					lngErrCode = 13;
					// TODO: Check the table for the column [End] and replace with corresponding Get_Field method
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1, Strings.Format(rsRI.Get_Fields("End"), "MM/dd/yyyy"));
					lngErrCode = 14;
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 1, Strings.Format(rsRI.Get_Fields_DateTime("IntStart"), "MM/dd/yyyy"));
					lngErrCode = 15;
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 1, "");
					lngErrCode = 17;
					dblPrin = FCConvert.ToDouble(rsCL.Get_Fields_Decimal("PrinOwed"));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 1, Strings.Format(dblPrin, "#,##0.00"));
					dblTax = FCConvert.ToDouble(rsCL.Get_Fields_Decimal("TaxOwed"));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 1, Strings.Format(dblTax, "#,##0.00"));
					// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
					dblInt = (-1 * (modARStatusPayments.Statics.dblARCurrentInt[rsCL.Get_Fields_Int32("ID")] + FCConvert.ToDouble(rsCL.Get_Fields("IntAdded"))));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1, Strings.Format(dblInt, "#,##0.00"));
					lngErrCode = 18;
					//FC:FINAL:SBE - #229 - check if value is Date
					if (Information.IsDate(rsCL.Get_Fields("IntPaidDate")) && rsCL.Get_Fields_DateTime("IntPaidDate").ToOADate() != 0)
					{
						dtInterestPDThroughDate = rsCL.Get_Fields_DateTime("IntPaidDate");
					}
					else
					{
						dtInterestPDThroughDate = (DateTime)rsRI.Get_Fields_DateTime("IntStart");
					}
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(20, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(21, 1, "");
					lngErrCode = 20;
					// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
					// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
					dblPaid = FCConvert.ToDouble(rsCL.Get_Fields("PrinPaid") + rsCL.Get_Fields("TaxPaid") + rsCL.Get_Fields("IntPaid"));
					lngErrCode = 21;
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(22, 1, Strings.Format(dblPaid, "#,##0.00"));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(23, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(24, 1, Strings.Format(dblPrin + dblTax - dblPaid + dblInt, "#,##0.00"));
					lngErrCode = 22;
					if (GRID.IsCollapsed(cRow) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						int l = 0;
						l = GRID.FindRow("=", cRow, lngGRIDColLineCode) + 1;
						if (l != -1)
						{
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(25, 1, Strings.Format(GRID.TextMatrix(l, lngGRIDColPerDiem), "#,##0.0000"));
						}
					}
					else
					{
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(25, 1, Strings.Format(GRID.TextMatrix(cRow, lngGRIDColPerDiem), "#,##0.0000"));
					}
					// this checks to see if the dates are 0 and replaces them with 0
					for (i = 10; i <= 14; i++)
					{
						//FC:FINAL:DSE Use correct date string for empty date
						//if (frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(i, lngGRIDColDate) == "12:00:00 AM") frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(i, lngGRIDColDate, "");
						if (frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(i, lngGRIDColDate) == Strings.Format(DateTime.FromOADate(0), "MM/dd/yyyy"))
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(i, lngGRIDColDate, "");
						if (Strings.Right(frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(i, 1), 1) == "%")
						{
							if (Conversion.Val(Strings.Left(frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(i, 1), frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(i, 1).Length - 1)) == 0)
							{
								frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(i, 1, "");
							}
						}
						else if (Conversion.Val(frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(i, 1)) == 0)
						{
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(i, 1, "");
						}
					}
				}
				else
				{
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 1, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 0, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 0, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 0, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 0, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 0, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(20, 0, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(21, 0, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(22, 0, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(23, 0, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(24, 0, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(25, 0, "");
				}
				// show the second set of information (valuations)--------------------
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 3, "Billed");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 3, "Address");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 3, "");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 3, "");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 3, "");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 3, "Customer");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 3, "Address");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 3, "");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 3, "");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 3, "");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 3, "Interest Paid Date");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 3, "Total Per Diem");
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 3, "");
				if (!rsBT.EndOfFile())
				{
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 3, FCConvert.ToString(rsBT.Get_Fields_String("Title1")));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 3, FCConvert.ToString(rsBT.Get_Fields_String("Title2")));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 3, FCConvert.ToString(rsBT.Get_Fields_String("Title3")));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 3, FCConvert.ToString(rsBT.Get_Fields_String("Title4")));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 3, FCConvert.ToString(rsBT.Get_Fields_String("Title5")));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 3, FCConvert.ToString(rsBT.Get_Fields_String("Title6")));
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 3, "Total");
				}
				else
				{
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 3, "Fee 1");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 3, "Fee 2");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 3, "Fee 3");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 3, "Fee 4");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 3, "Fee 5");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 3, "Fee 6");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 3, "Total");
				}
				if (Strings.Trim(rsCL.Get_Fields_String("BName2")) != "")
				{
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 4, Strings.Trim(rsCL.Get_Fields_String("BName")) + " and " + Strings.Trim(rsCL.Get_Fields_String("BName2")));
				}
				else
				{
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 4, Strings.Trim(rsCL.Get_Fields_String("BName")));
				}
				strAddr1 = rsCL.Get_Fields_String("BAddress1");
				strAddr2 = rsCL.Get_Fields_String("BAddress2");
				strAddr3 = rsCL.Get_Fields_String("BAddress3");
				if (Strings.Trim(rsCL.Get_Fields_String("BZip4")) != "" && Strings.Trim(rsCL.Get_Fields_String("BZip4")) != "")
				{
					strAddr4 = rsCL.Get_Fields_String("BCity") + ", " + rsCL.Get_Fields_String("BState") + " " + rsCL.Get_Fields_String("BZip") + "-" + rsCL.Get_Fields_String("BZip4");
				}
				else
				{
					strAddr4 = rsCL.Get_Fields_String("BCity") + ", " + rsCL.Get_Fields_String("BState") + " " + rsCL.Get_Fields_String("BZip");
				}
				if (strAddr3 == "")
				{
					strAddr3 = strAddr4;
					strAddr4 = "";
				}
				if (strAddr2 == "")
				{
					strAddr2 = strAddr3;
					strAddr3 = strAddr4;
					strAddr4 = "";
				}
				if (strAddr1 == "")
				{
					strAddr1 = strAddr2;
					strAddr2 = strAddr3;
					strAddr3 = strAddr4;
					strAddr4 = "";
				}
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 4, strAddr1);
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 4, strAddr2);
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 4, strAddr3);
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 4, strAddr4);
				// rsBT.OpenRecordset "SELECT * FROM CustomerMaster WHERE ID = " & rsCL.Get_Fields("AccountKey"), strARDatabase
				rsBT.OpenRecordset("SELECT c.*, p.* FROM CustomerMaster as c CROSS APPLY " + rsBT.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(c.PartyID, NULL, 'AR', c.CustomerID) as p WHERE ID = " + rsCL.Get_Fields_Int32("AccountKey"), modExtraModules.strARDatabase);
				if (!rsBT.EndOfFile())
				{
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 4, FCConvert.ToString(rsBT.Get_Fields_String("FullName")));
					strAddr1 = FCConvert.ToString(rsBT.Get_Fields_String("Address1"));
					strAddr2 = FCConvert.ToString(rsBT.Get_Fields_String("Address2"));
					strAddr3 = FCConvert.ToString(rsBT.Get_Fields_String("Address3"));
					// TODO: Field [PartyState] not found!! (maybe it is an alias?)
					strAddr4 = rsBT.Get_Fields_String("City") + ", " + rsBT.Get_Fields("PartyState") + " " + rsBT.Get_Fields_String("Zip");
					if (strAddr3 == "")
					{
						strAddr3 = strAddr4;
						strAddr4 = "";
					}
					if (strAddr2 == "")
					{
						strAddr2 = strAddr3;
						strAddr3 = strAddr4;
						strAddr4 = "";
					}
					if (strAddr1 == "")
					{
						strAddr1 = strAddr2;
						strAddr2 = strAddr3;
						strAddr3 = strAddr4;
						strAddr4 = "";
					}
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 4, strAddr1);
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 4, strAddr2);
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 4, strAddr3);
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 4, strAddr4);
				}
				else
				{
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 4, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 4, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 4, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 4, "");
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 4, "");
				}
				if (dtInterestPDThroughDate.ToOADate() != 0)
				{
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 4, Strings.Format(dtInterestPDThroughDate, "MM/dd/yyyy"));
				}
				else
				{
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 4, "");
				}
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 4, Strings.Format(dblTotalPerDiem, "#,##0.0000"));
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 4, "");
				// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
				if (rsCL.Get_Fields("Amount1") != 0)
				{
					// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 4, Strings.Format(rsCL.Get_Fields("Amount1"), "#,##0.00"));
				}
				// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
				if (rsCL.Get_Fields("Amount2") != 0)
				{
					// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 4, Strings.Format(rsCL.Get_Fields("Amount2"), "#,##0.00"));
				}
				// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
				if (rsCL.Get_Fields("Amount3") != 0)
				{
					// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 4, Strings.Format(rsCL.Get_Fields("Amount3"), "#,##0.00"));
				}
				// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
				if (rsCL.Get_Fields("Amount4") != 0)
				{
					// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 4, Strings.Format(rsCL.Get_Fields("Amount4"), "#,##0.00"));
				}
				// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
				if (rsCL.Get_Fields("Amount5") != 0)
				{
					// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 4, Strings.Format(rsCL.Get_Fields("Amount5"), "#,##0.00"));
				}
				// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
				if (rsCL.Get_Fields("Amount6") != 0)
				{
					// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
					frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 4, Strings.Format(rsCL.Get_Fields("Amount6"), "#,##0.00"));
				}
				// TODO: Check the table for the column [Amount1] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Amount2] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Amount3] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Amount4] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Amount5] and replace with corresponding Get_Field method
				// TODO: Check the table for the column [Amount6] and replace with corresponding Get_Field method
				frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 4, Strings.Format(rsCL.Get_Fields("Amount1") + rsCL.Get_Fields("Amount2") + rsCL.Get_Fields("Amount3") + rsCL.Get_Fields("Amount4") + rsCL.Get_Fields("Amount5") + rsCL.Get_Fields("Amount6"), "#,##0.00"));
				// show the frame-----------------------------------------------------
				frmARCLStatus.InstancePtr.fraRateInfo.Text = "Rate Information";
				// - " & intYear
				//frmARCLStatus.InstancePtr.fraRateInfo.Left = FCConvert.ToInt32((frmARCLStatus.InstancePtr.Width - frmARCLStatus.InstancePtr.fraRateInfo.Width) / 2.0);
				//frmARCLStatus.InstancePtr.fraRateInfo.Top = FCConvert.ToInt32((frmARCLStatus.InstancePtr.Height - frmARCLStatus.InstancePtr.fraRateInfo.Height) / 2.0);
				//FC:FINAL:RPU:#304- Center the frame
				frmARCLStatus.InstancePtr.fraRateInfo.CenterToContainer(this.ClientArea);
				frmARCLStatus.InstancePtr.fraRateInfo.Visible = true;
				frmARCLStatus.InstancePtr.cmdRIClose.Focus();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Rate Information Error" + " - " + FCConvert.ToString(lngErrCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowPaymentInfo(int cRow, int cCol)
		{
			int lngErrCode = 0;
			clsDRWrapper rsRI = new clsDRWrapper();
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will show a box with the rate information in it
				// as soon as the box loses focus, I will make it disappear
				int intYear;
				int i;
				int RK;
				lngErrCode = 1;
				if (Strings.Trim(GRID.TextMatrix(cRow, lngGRIDColLineCode)) != "")
				{
					if (Conversion.Val(GRID.TextMatrix(cRow, lngGRIDColPaymentKey)) > 0 && Conversion.Val(GRID.TextMatrix(cRow, lngGRIDColCHGINTNumber)) >= 0)
					{
						rsRI.OpenRecordset("SELECT * FROM (" + modARStatusPayments.Statics.strARCurrentAccountPayments + ") AS Table1 WHERE ID = " + GRID.TextMatrix(cRow, lngGRIDColPaymentKey), modExtraModules.strARDatabase);
						// format grid
						frmARCLStatus.InstancePtr.vsRateInfo.Cols = 1;
						frmARCLStatus.InstancePtr.vsRateInfo.Cols = 5;
						frmARCLStatus.InstancePtr.vsRateInfo.Rows = 20;
						//FC:FINAL:AM:#4 - decrease the height of rows
						vsRateInfo.RowHeight(-1, 400);
						frmARCLStatus.InstancePtr.vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
						frmARCLStatus.InstancePtr.vsRateInfo.ColWidth(0, FCConvert.ToInt32(frmARCLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.3));
						frmARCLStatus.InstancePtr.vsRateInfo.ColWidth(1, FCConvert.ToInt32(frmARCLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.19));
						frmARCLStatus.InstancePtr.vsRateInfo.ColWidth(2, FCConvert.ToInt32(frmARCLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.01));
						frmARCLStatus.InstancePtr.vsRateInfo.ColWidth(3, FCConvert.ToInt32(frmARCLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.3));
						frmARCLStatus.InstancePtr.vsRateInfo.ColWidth(4, FCConvert.ToInt32(frmARCLStatus.InstancePtr.vsRateInfo.WidthOriginal * 0.19));
						frmARCLStatus.InstancePtr.vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
						//frmARCLStatus.InstancePtr.vsRateInfo.Height = frmARCLStatus.InstancePtr.vsRateInfo.RowHeight(0) * frmARCLStatus.InstancePtr.vsRateInfo.Rows + 50;
						// this will hide the border of the other information on the shared grid
						frmARCLStatus.InstancePtr.vsRateInfo.Select(19, 0, 19, 1);
						frmARCLStatus.InstancePtr.vsRateInfo.CellBorder(Color.Black, 0, 0, 0, 0, 0, 0);
						frmARCLStatus.InstancePtr.vsRateInfo.Select(15, 0, 15, 1);
						frmARCLStatus.InstancePtr.vsRateInfo.CellBorder(Color.Black, 0, 0, 0, 0, 0, 0);
						frmARCLStatus.InstancePtr.vsRateInfo.Select(0, 0);
						frmARCLStatus.InstancePtr.vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 19, 1, 19, 4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						frmARCLStatus.InstancePtr.vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
						// show the first set of information (rate information and period info)--------------
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 0, "Account");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 0, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 0, "Bill ID");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 0, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 0, "Receipt Number");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 0, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 0, "Recorded Transaction");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 0, "Effective Interest Date");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 0, "Actual Transaction Date");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 0, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 0, "Teller ID");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 0, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 0, "Cash Drawer");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 0, "Affect Cash");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 0, "BD Account");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 3, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 3, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 3, "Reference");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 3, "Paid By");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 3, "Daily Close Out");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 3, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 3, "Principal");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 3, "Interest");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 3, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 3, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 3, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 3, "Total");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 3, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 3, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 3, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(15, 3, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(16, 3, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(17, 3, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(18, 3, "");
						frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 0, "Comments");
						lngErrCode = 1;
						if (rsRI.BeginningOfFile() != true && rsRI.EndOfFile() != true)
						{
							lngErrCode = 2;
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 1, FCConvert.ToString(modARStatusPayments.GetAccountNumberFromAccountKey_2(FCConvert.ToInt32(rsRI.Get_Fields_Int32("AccountKey")))));
							lngErrCode = 3;
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 1, FCConvert.ToString(rsRI.Get_Fields_Int32("BillKey")));
							lngErrCode = 6;
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1, GetActualReceiptNumber_8(FCConvert.ToInt32(rsRI.Get_Fields_Int32("ReceiptNumber")), rsRI.Get_Fields_DateTime("ActualSystemDate")));
							lngErrCode = 7;
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 1, "");
							lngErrCode = 8;
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 1, Strings.Format(rsRI.Get_Fields_DateTime("RecordedTransactionDate"), "MM/dd/yyyy"));
							lngErrCode = 9;
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 1, Strings.Format(rsRI.Get_Fields_DateTime("EffectiveInterestDate"), "MM/dd/yyyy"));
							lngErrCode = 10;
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 1, Strings.Format(rsRI.Get_Fields_DateTime("ActualSystemDate"), "MM/dd/yyyy"));
							lngErrCode = 11;
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 1, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 1, FCConvert.ToString(rsRI.Get_Fields_String("Teller")));
							lngErrCode = 12;
							if (FCConvert.ToString(rsRI.Get_Fields_String("CashDrawer")) == "Y")
							{
								frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 1, "YES");
								frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1, "YES");
							}
							else
							{
								frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(12, 1, "NO");
								if (FCConvert.ToString(rsRI.Get_Fields_String("GeneralLedger")) == "Y")
								{
									frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1, "YES");
								}
								else
								{
									frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(13, 1, "NO");
									frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(14, 1, FCConvert.ToString(rsRI.Get_Fields_String("BudgetaryAccountNumber")));
								}
							}
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 4, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 4, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 4, FCConvert.ToString(rsRI.Get_Fields_String("Reference")));
							lngErrCode = 13;
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 4, FCConvert.ToString(rsRI.Get_Fields_String("PaidBy")));
							lngErrCode = 14;
							if (FCConvert.ToBoolean(rsRI.Get_Fields_Int32("DailyCloseOut")))
							{
								frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 4, "True");
							}
							else
							{
								frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 4, "False");
							}
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 4, "");
							lngErrCode = 15;
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 4, Strings.Format(rsRI.Get_Fields("Principal"), "#,##0.00"));
							lngErrCode = 16;
							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 4, Strings.Format(rsRI.Get_Fields("Interest"), "#,##0.00"));
							lngErrCode = 17;
							// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 4, Strings.Format(rsRI.Get_Fields("Tax"), "#,##0.00"));
							lngErrCode = 18;
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(10, 4, "");
							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
							// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(11, 4, Strings.Format(rsRI.Get_Fields("Principal") + rsRI.Get_Fields("Interest") + rsRI.Get_Fields("Tax"), "#,##0.00"));
							lngErrCode = 19;
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1, FCConvert.ToString(rsRI.Get_Fields_String("Comments")));
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 2, frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1));
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 3, frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1));
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 4, frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 1));
							frmARCLStatus.InstancePtr.vsRateInfo.MergeRow(19, true, 1, 4);
						}
						else
						{
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 1, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 1, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 1, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 1, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 1, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 1, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 1, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 1, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 1, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(9, 1, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(0, 4, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(1, 4, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(2, 4, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(3, 4, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(4, 4, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(5, 0, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(6, 0, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(7, 0, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(8, 0, "");
							frmARCLStatus.InstancePtr.vsRateInfo.TextMatrix(19, 0, "");
						}
						// show the frame-----------------------------------------------------
						frmARCLStatus.InstancePtr.fraRateInfo.Text = "Payment Information";
						//frmARCLStatus.InstancePtr.fraRateInfo.Left = FCConvert.ToInt32((frmARCLStatus.InstancePtr.Width - frmARCLStatus.InstancePtr.fraRateInfo.Width) / 2.0);
						//frmARCLStatus.InstancePtr.fraRateInfo.Top = FCConvert.ToInt32((frmARCLStatus.InstancePtr.Height - frmARCLStatus.InstancePtr.fraRateInfo.Height) / 2.0);
						//FC:FINAL:RPU:#304- Center the frame
						frmARCLStatus.InstancePtr.fraRateInfo.CenterToContainer(this.ClientArea);
						frmARCLStatus.InstancePtr.fraRateInfo.Visible = true;
						frmARCLStatus.InstancePtr.cmdRIClose.Focus();
					}
				}
				rsRI.Reset();
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Rate Information Error" + " - " + FCConvert.ToString(lngErrCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				rsRI.Reset();
			}
		}
		// vbPorter upgrade warning: 'Return' As string	OnWrite(string, int)
		private string GetActualReceiptNumber_8(int lngRN, DateTime dtDate)
		{
			return GetActualReceiptNumber(ref lngRN, ref dtDate);
		}

		private string GetActualReceiptNumber(ref int lngRN, ref DateTime dtDate)
		{
			string GetActualReceiptNumber = "";
			// this function will accept a long of a receipt number ID in the CR table
			// if the receipt does not exist or any other error occurs, the function will
			// return 0, otherwise it returns the receipt number shown on the receipt
			clsDRWrapper rsRN = new clsDRWrapper();
			if (lngRN > 0)
			{
				if (modGlobalConstants.Statics.gboolCR)
				{
					// kgk 09-18-2012 trocr-357  rsRN.OpenRecordset "SELECT * FROM Receipt WHERE ID = " & lngRN, strCRDatabase
					rsRN.OpenRecordset("SELECT * FROM Receipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND ReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00:00 AM' AND ReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59:59 PM'", modExtraModules.strCRDatabase);
					if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
					{
						GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
					}
					else
					{
						// kgk 09-19-2011 trocr-357
						rsRN.OpenRecordset("SELECT * FROM LastYearReceipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND LastYearReceiptDate > '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 12:00:00 AM' AND LastYearReceiptDate < '" + Strings.Format(dtDate, "MM/dd/yyyy") + " 11:59:59 PM'", modExtraModules.strCRDatabase);
						if (rsRN.EndOfFile() != true && rsRN.BeginningOfFile() != true)
						{
							GetActualReceiptNumber = FCConvert.ToString(rsRN.Get_Fields_Int32("ReceiptNumber"));
						}
						else
						{
							GetActualReceiptNumber = FCConvert.ToString(lngRN) + "*";
						}
					}
				}
				else
				{
					GetActualReceiptNumber = FCConvert.ToString(lngRN);
				}
			}
			else
			{
				GetActualReceiptNumber = FCConvert.ToString(0);
			}
			rsRN.Reset();
			return GetActualReceiptNumber;
		}

		private void CheckCD()
		{
			if (txtCD.Text == "N")
			{
				txtCash.Enabled = true;
				txtCash.TabStop = true;
			}
			else
			{
				if (txtCash.Text == "N")
					txtCash.Text = "Y";
				txtCash.TabStop = false;
				txtCash.Enabled = false;
				txtAcctNumber.Enabled = false;
				txtAcctNumber.TabStop = false;
			}
		}

		private void CheckCash()
		{
			if (txtCD.Text == "N" && Strings.Right(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString(), 1) != "*" && Conversion.Val(cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString()) != 0 && Strings.Left(cmbCode.Items[cmbCode.SelectedIndex].ToString(), 1) != "Y")
			{
				if (txtCash.Text == "N")
				{
					txtAcctNumber.Enabled = true;
					txtAcctNumber.TabStop = true;
				}
				else
				{
					txtAcctNumber.Enabled = false;
					txtAcctNumber.TabStop = false;
				}
			}
			else
			{
				if (txtCash.Text == "N")
					txtCash.Text = "Y";
				txtAcctNumber.Text = "";
			}
		}
		// vbPorter upgrade warning: 'Return' As DateTime	OnWrite(DateTime, short)
		private DateTime FindInterestStartDate(ref int lngAC, ref int lngBillNumber, string strType = "AR")
		{
			DateTime FindInterestStartDate = System.DateTime.Now;
			// this function will find and return the latest interest date for this account and year
			clsDRWrapper rsDate = new clsDRWrapper();
			string strSQL;
			DateTime dtIntStartDate;
			DateTime dtPerDate;
			strSQL = "WHERE Account = " + FCConvert.ToString(lngAC) + " AND BillingNumber = " + FCConvert.ToString(lngBillNumber);
			// get all of the payments
			rsDate.OpenRecordset("SELECT * FROM Bill " + strSQL, modExtraModules.strARDatabase);
			if (rsDate.EndOfFile() != true && rsDate.BeginningOfFile() != true)
			{
				// find the interest paid through date
				if (FCConvert.ToDateTime(rsDate.Get_Fields_DateTime("IntPaidDate") as object).ToOADate() != 0)
				{
					dtIntStartDate = (DateTime)rsDate.Get_Fields_DateTime("IntPaidDate");
				}
				else
				{
					dtIntStartDate = modARStatusPayments.Statics.AREffectiveDate;
				}
				// find the interest start date for the period that is passed in
				rsDate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsDate.Get_Fields_Int32("ID"), modExtraModules.strARDatabase);
				if (rsDate.EndOfFile() != true && rsDate.BeginningOfFile() != true)
				{
					dtPerDate = (DateTime)rsDate.Get_Fields_DateTime("IntStart");
				}
				else
				{
					dtPerDate = modARStatusPayments.Statics.AREffectiveDate;
				}
				if (DateAndTime.DateDiff("D", dtIntStartDate, dtPerDate) < 0)
				{
					// find out which is the latest
					FindInterestStartDate = dtIntStartDate;
				}
				else
				{
					FindInterestStartDate = dtPerDate;
				}
			}
			else
			{
				FindInterestStartDate = DateTime.FromOADate(0);
			}
			rsDate.Reset();
			return FindInterestStartDate;
		}

		public void FillPaymentGrid(int cRow, int intCT, string strInvoice, int lngID = 0)
		{
			if (cRow == 0)
				cRow = 1;
			frmARCLStatus.InstancePtr.vsPayments.AddItem("", cRow);
			if (intCT < 0)
			{
				frmARCLStatus.InstancePtr.vsPayments.TextMatrix(cRow, lngPayGridColArrayIndex, FCConvert.ToString(intCT));
				intCT *= -1;
			}
			else
			{
				frmARCLStatus.InstancePtr.vsPayments.TextMatrix(cRow, lngPayGridColArrayIndex, FCConvert.ToString(intCT));
			}
			// this adds it to the grid
			frmARCLStatus.InstancePtr.vsPayments.TextMatrix(cRow, lngPayGridColInvoice, strInvoice);
			frmARCLStatus.InstancePtr.vsPayments.TextMatrix(cRow, lngPayGridColBillID, FCConvert.ToString(modARStatusPayments.Statics.ARPaymentArray[intCT].BillKey));
			frmARCLStatus.InstancePtr.vsPayments.TextMatrix(cRow, lngPayGridColDate, Strings.Format(modARStatusPayments.Statics.ARPaymentArray[intCT].RecordedTransactionDate, "MM/dd/yyyy"));
			frmARCLStatus.InstancePtr.vsPayments.TextMatrix(cRow, lngPayGridColRef, modARStatusPayments.Statics.ARPaymentArray[intCT].Reference);
			frmARCLStatus.InstancePtr.vsPayments.TextMatrix(cRow, lngPayGridColCode, modARStatusPayments.Statics.ARPaymentArray[intCT].Code);
			frmARCLStatus.InstancePtr.vsPayments.TextMatrix(cRow, lngPayGridColCDAC, modARStatusPayments.Statics.ARPaymentArray[intCT].CashDrawer + "   " + modARStatusPayments.Statics.ARPaymentArray[intCT].GeneralLedger);
			frmARCLStatus.InstancePtr.vsPayments.TextMatrix(cRow, lngPayGridColPrincipal, Strings.Format(modARStatusPayments.Statics.ARPaymentArray[intCT].Principal, "#,##0.00"));
			frmARCLStatus.InstancePtr.vsPayments.TextMatrix(cRow, lngPayGridColTax, Strings.Format(modARStatusPayments.Statics.ARPaymentArray[intCT].Tax, "#,##0.00"));
			frmARCLStatus.InstancePtr.vsPayments.TextMatrix(cRow, lngPayGridColInterest, Strings.Format(modARStatusPayments.Statics.ARPaymentArray[intCT].CurrentInterest, "#,##0.00"));
			frmARCLStatus.InstancePtr.vsPayments.TextMatrix(cRow, lngPayGridColKeyNumber, FCConvert.ToString(lngID));
			frmARCLStatus.InstancePtr.vsPayments.TextMatrix(cRow, lngPayGridColCHGINTNumber, FCConvert.ToString(modARStatusPayments.Statics.ARPaymentArray[intCT].CHGINTNumber));
			frmARCLStatus.InstancePtr.vsPayments.TextMatrix(cRow, lngPayGridColTotal, Strings.Format(modARStatusPayments.Statics.ARPaymentArray[intCT].Principal + modARStatusPayments.Statics.ARPaymentArray[intCT].Tax + modARStatusPayments.Statics.ARPaymentArray[intCT].CurrentInterest, "#,##0.00"));
		}

		private string GetAccountNote(ref int lngAccount, ref bool boolPopUp)
		{
			string GetAccountNote = "";
			// this function will return then string of the note
			// if boolPopUp is set, then pop the message up
			clsDRWrapper rsNote = new clsDRWrapper();
			rsNote.OpenRecordset("SELECT * FROM Comments WHERE Account = " + FCConvert.ToString(lngAccount), modExtraModules.strARDatabase);
			if (rsNote.EndOfFile() != true)
			{
				GetAccountNote = FCConvert.ToString(rsNote.Get_Fields_String("Comment"));
				if (FCConvert.ToInt32(rsNote.Get_Fields_Int32("Priority")) > 0)
				{
					boolPopUp = true;
					// this will get passed back out of this function
				}
				else
				{
					boolPopUp = false;
				}
			}
			rsNote.Reset();
			return GetAccountNote;
		}

		private void vsRateInfo_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsRateInfo[e.ColumnIndex, e.RowIndex];
            // this will show the contents of the box in the tooltip in case it is too large for the box
            if (vsRateInfo.GetFlexRowIndex(e.RowIndex) >= 0 && vsRateInfo.GetFlexColIndex(e.ColumnIndex) >= 0)
			{
				//ToolTip1.SetToolTip(vsRateInfo, vsRateInfo.TextMatrix(vsRateInfo.MouseRow, vsRateInfo.MouseCol));
				cell.ToolTipText = vsRateInfo.TextMatrix(vsRateInfo.GetFlexRowIndex(e.RowIndex), vsRateInfo.GetFlexColIndex(e.ColumnIndex));
			}
		}

		private void vsRateInfo_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsRateInfo.Row == 10)
			{
				if (vsRateInfo.Col == 4)
				{
					vsRateInfo.EditMask = "##/##/####";
				}
				else
				{
					vsRateInfo.EditMask = "";
				}
			}
			else
			{
				vsRateInfo.EditMask = "";
			}
		}

		private void vsRateInfo_RowColChange(object sender, System.EventArgs e)
		{
			if (vsRateInfo.Row == 10)
			{
				if (vsRateInfo.Col == 4)
				{
					vsRateInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				else
				{
					vsRateInfo.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			else
			{
				vsRateInfo.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsRateInfo_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			string strTemp = "";
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsRateInfo.GetFlexRowIndex(e.RowIndex);
			int col = vsRateInfo.GetFlexColIndex(e.ColumnIndex);
			if (col == 4 && row == 10)
			{
				// this is the applied through date
				if (Information.IsDate(vsRateInfo.EditText))
				{
					if (DateAndTime.DateValue(vsRateInfo.EditText).ToOADate() != 0)
					{
						// allow and change if different
						strTemp = Strings.Trim(vsRateInfo.TextMatrix(10, 4));
						if (Strings.Trim(vsRateInfo.EditText) != strTemp)
						{
							switch (MessageBox.Show("Are you sure that you would like to edit the Interest Paid Date.", "Edit Date", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
							{
								case DialogResult.Yes:
									{
										modGlobalFunctions.AddCYAEntry_728("AR", "Edit Applied Through Date", "Account - " + FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR), "Bill ID : " + rsCL.Get_Fields_Int32("ID"), "New - " + vsRateInfo.EditText, "Old - " + vsRateInfo.TextMatrix(10, 4));
										// MAL@20080312: Changed to take which grid is the active grid used to bring up the right-click
										// Tracker Reference: 12677
										// SaveNewAppliedThroughDate boolShowingWater, CDate(vsRateInfo.EditText), rsCL.Get_Fields("Bill")
										SaveNewAppliedThroughDate_8(DateAndTime.DateValue(vsRateInfo.EditText), FCConvert.ToInt32(rsCL.Get_Fields_Int32("ID")));
										break;
									}
								case DialogResult.No:
									{
										e.Cancel = true;
										vsRateInfo.Editable = FCGrid.EditableSettings.flexEDNone;
										break;
									}
								case DialogResult.Cancel:
									{
										vsRateInfo.EditText = strTemp;
										break;
									}
							}
							//end switch
						}
					}
					else
					{
						e.Cancel = true;
						MessageBox.Show("Please enter a valid date.", "Edit Applied Through Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
				else
				{
					e.Cancel = true;
					MessageBox.Show("Please enter a valid date.", "Edit Applied Through Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void SaveNewAppliedThroughDate_8(DateTime dtNewDate, int lngRK)
		{
			SaveNewAppliedThroughDate(ref dtNewDate, ref lngRK);
		}

		private void SaveNewAppliedThroughDate(ref DateTime dtNewDate, ref int lngRK)
		{
			// this routine will set the new date on the bill/lien
			clsDRWrapper rsEdit = new clsDRWrapper();
			rsEdit.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(modARStatusPayments.Statics.lngCurrentCustomerIDAR) + " AND ID = " + FCConvert.ToString(lngRK), modExtraModules.strARDatabase);
			if (!rsEdit.EndOfFile())
			{
				// regular
				rsEdit.Edit();
				rsEdit.Set_Fields("IntPaidDate", dtNewDate);
				rsEdit.Update();
			}
			else
			{
				MessageBox.Show("Bill not found, date not changed.", "Error Saving Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void InputBoxAdjustment_2(bool boolCollapse)
		{
			InputBoxAdjustment(ref boolCollapse);
		}

		private void InputBoxAdjustment(ref bool boolCollapse)
		{
			// vbPorter upgrade warning: dblTemp As double	OnWriteFCConvert.ToDecimal(
			double dblTemp = 0;
			// this will adjust the amount input boxes to only show one unless it is a correction
			boolCollapsedInputBoxes = boolCollapse;
			if (Conversion.Val(txtPrincipal.Text) == 0)
				txtPrincipal.Text = "0.00";
			if (Conversion.Val(txtInterest.Text) == 0)
				txtInterest.Text = "0.00";
			if (Conversion.Val(txtTax.Text) == 0)
				txtTax.Text = "0.00";
			// hide/show the labels and textboxes
			txtPrincipal.Visible = !boolCollapse;
			txtTax.Visible = !boolCollapse;
			lblPrincipal.Visible = !boolCollapse;
			lblTax.Visible = !boolCollapse;
			if (boolCollapse)
			{
				// make sure the interest field has all of the values from the other if the user had it in there first
				dblTemp = FCConvert.ToDouble(FCConvert.ToDecimal(txtPrincipal.Text) + FCConvert.ToDecimal(txtTax.Text));
				txtPrincipal.Text = "0.00";
				txtTax.Text = "0.00";
				txtInterest.Text = Strings.Format(FCConvert.ToDouble(txtInterest.Text) + dblTemp, "#,##0.00");
				lblInterest.Text = "Amount";
			}
			else
			{
				lblInterest.Text = "Interest";
			}
		}

		private void ClearARPaymentGrid()
		{
			int counter;
			for (counter = 1; counter <= modARStatusPayments.MAX_ARPAYMENTS; counter++)
			{
				// this finds a place in the array
				if (modARStatusPayments.Statics.ARPaymentArray[counter].BillKey == 0)
				{
					// to store the new payment record
					return;
				}
				else
				{
					modARStatusPayments.ResetARPayment(ref counter);
				}
			}
		}

		private void vsPeriod_DblClick(object sender, System.EventArgs e)
		{
			int intCT;
			double dblAmt;
			dblAmt = FCConvert.ToDouble(vsPeriod.TextMatrix(0, 1));
			// force Auto into the Bill Number combo
			for (intCT = 0; intCT <= cmbBillNumber.Items.Count - 1; intCT++)
			{
				if (Strings.Left(cmbBillNumber.Items[intCT].ToString(), 1) == "A")
				{
					cmbBillNumber.SelectedIndex = intCT;
					break;
				}
			}
			modARStatusPayments.CreateAROppositionLine(GRID.Rows - 1, false);
			// create an opposition line for the whole account for sewer
			vsPeriod.TextMatrix(0, 1, Strings.Format(dblAmt, "#,##0.00"));
			txtInterest.Text = Strings.Format(dblAmt, "#,##0.00");
		}

		private void cmdEffectiveDate_Click(object sender, EventArgs e)
		{
			mnuProcessEffective_Click(sender, e);
		}

		private void cmdPrint_Click(object sender, EventArgs e)
		{
			mnuFilePrint_Click(sender, e);
		}

		private void cmdChangeAccount_Click(object sender, EventArgs e)
		{
			mnuProcessChangeAccount_Click(sender, e);
		}

		private void cmdProcess_Click(object sender, EventArgs e)
		{
			mnuPaymentSaveExit_Click(sender, e);
		}

		private void fcButton1_Click(object sender, EventArgs e)
		{
			mnuPaymentSave_Click();
		}
	}
}
