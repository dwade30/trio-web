﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmPartyList.
	/// </summary>
	partial class frmPartyList : BaseForm
	{
		public fecherFoundation.FCButton cmdNo;
		public fecherFoundation.FCButton cmdYes;
		public fecherFoundation.FCGrid SearchGrid;
		public Wisej.Web.ProgressBar prgWait;
		public fecherFoundation.FCLabel lblWait;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPartyList));
			this.cmdNo = new fecherFoundation.FCButton();
			this.cmdYes = new fecherFoundation.FCButton();
			this.SearchGrid = new fecherFoundation.FCGrid();
			this.prgWait = new Wisej.Web.ProgressBar();
			this.lblWait = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdYes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).BeginInit();
			this.SearchGrid.SuspendLayout();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 588);
			this.BottomPanel.Size = new System.Drawing.Size(660, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdNo);
			this.ClientArea.Controls.Add(this.cmdYes);
			this.ClientArea.Controls.Add(this.SearchGrid);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(660, 528);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(660, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(256, 30);
			this.HeaderText.Text = "Duplicate Party Check";
			// 
			// cmdNo
			// 
			this.cmdNo.AppearanceKey = "actionButton";
			this.cmdNo.Location = new System.Drawing.Point(338, 485);
			this.cmdNo.Name = "cmdNo";
			this.cmdNo.Size = new System.Drawing.Size(64, 40);
			this.cmdNo.TabIndex = 4;
			this.cmdNo.Text = "No";
			this.cmdNo.Click += new System.EventHandler(this.cmdNo_Click);
			// 
			// cmdYes
			// 
			this.cmdYes.AppearanceKey = "actionButton";
			this.cmdYes.Location = new System.Drawing.Point(252, 485);
			this.cmdYes.Name = "cmdYes";
			this.cmdYes.Size = new System.Drawing.Size(70, 40);
			this.cmdYes.TabIndex = 3;
			this.cmdYes.Text = "Yes";
			this.cmdYes.Click += new System.EventHandler(this.cmdYes_Click);
			// 
			// SearchGrid
			// 
			this.SearchGrid.AllowSelection = false;
			this.SearchGrid.AllowUserToResizeColumns = false;
			this.SearchGrid.AllowUserToResizeRows = false;
			this.SearchGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.SearchGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.SearchGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.SearchGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.SearchGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.SearchGrid.BackColorSel = System.Drawing.Color.Empty;
			this.SearchGrid.Cols = 4;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.SearchGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.SearchGrid.ColumnHeadersHeight = 30;
			this.SearchGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.SearchGrid.Controls.Add(this.prgWait);
			this.SearchGrid.Controls.Add(this.lblWait);
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.SearchGrid.DefaultCellStyle = dataGridViewCellStyle4;
			this.SearchGrid.DragIcon = null;
			this.SearchGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.SearchGrid.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.SearchGrid.FixedCols = 0;
			this.SearchGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.SearchGrid.FrozenCols = 0;
			this.SearchGrid.GridColor = System.Drawing.Color.Empty;
			this.SearchGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.SearchGrid.Location = new System.Drawing.Point(30, 66);
			this.SearchGrid.Name = "SearchGrid";
			this.SearchGrid.ReadOnly = true;
			this.SearchGrid.RowHeadersVisible = false;
			this.SearchGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.SearchGrid.RowHeightMin = 0;
			this.SearchGrid.Rows = 1;
			this.SearchGrid.ScrollTipText = null;
			this.SearchGrid.ShowColumnVisibilityMenu = false;
			this.SearchGrid.Size = new System.Drawing.Size(600, 363);
			this.SearchGrid.StandardTab = true;
			this.SearchGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.SearchGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.SearchGrid.TabIndex = 1;
			// 
			// prgWait
			// 
			this.prgWait.Location = new System.Drawing.Point(208, 173);
			this.prgWait.Name = "prgWait";
			this.prgWait.Size = new System.Drawing.Size(203, 17);
			this.prgWait.TabIndex = 1;
			// 
			// lblWait
			// 
			this.lblWait.AutoSize = true;
			this.lblWait.Location = new System.Drawing.Point(249, 135);
			this.lblWait.Name = "lblWait";
			this.lblWait.Size = new System.Drawing.Size(130, 15);
			this.lblWait.TabIndex = 0;
			this.lblWait.Text = "LOADING PARTIES...";
			this.lblWait.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(206, 449);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(248, 15);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "DO YOU WANT TO CREATE IT ANYWAY?";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(568, 15);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "THE SYSTEM HAS DETERMINED THAT THE PARTY MAY BE A DUPLICATE OF THE FOLLOWING";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// frmPartyList
			// 
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(660, 696);
			this.KeyPreview = true;
			this.Name = "frmPartyList";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Duplicate Party Check";
			this.Load += new System.EventHandler(this.frmPartyList_Load);
			this.Activated += new System.EventHandler(this.frmPartyList_Activated);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPartyList_KeyDown);
			this.Resize += new System.EventHandler(this.frmPartyList_Resize);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdYes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.SearchGrid)).EndInit();
			this.SearchGrid.ResumeLayout(false);
			this.SearchGrid.PerformLayout();
			this.ResumeLayout(false);
		}
		#endregion
	}
}
