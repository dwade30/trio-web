﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace Global
{
	/// <summary>
	/// Summary description for frmLienDischargeNoticeBP.
	/// </summary>
	partial class frmLienDischargeNoticeBP : BaseForm
	{
		public fecherFoundation.FCRichTextBox txtTop;
		public fecherFoundation.FCTextBox txtName4;
		public fecherFoundation.FCTextBox txtName3;
		public fecherFoundation.FCTextBox txtName2;
		public fecherFoundation.FCTextBox txtName1;
		public T2KDateBox txtDate;
        //public fecherFoundation.FCDateTimePicker txtDate;
        //public Wisej.Web.DateTimePicker txtDate;
        public fecherFoundation.FCRichTextBox txtBottom;
		public fecherFoundation.FCLabel lblFooter;
		public fecherFoundation.FCLabel lblBKList;
		public fecherFoundation.FCLabel lblStart;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLienDischargeNoticeBP));
			this.txtTop = new fecherFoundation.FCRichTextBox();
			this.txtName4 = new fecherFoundation.FCTextBox();
			this.txtName3 = new fecherFoundation.FCTextBox();
			this.txtName2 = new fecherFoundation.FCTextBox();
			this.txtName1 = new fecherFoundation.FCTextBox();
			this.txtDate = new Global.T2KDateBox();
            //this.txtDate = new Wisej.Web.DateTimePicker();
            //this.txtDate = new fecherFoundation.FCDateTimePicker();
			this.txtBottom = new fecherFoundation.FCRichTextBox();
			this.lblFooter = new fecherFoundation.FCLabel();
			this.lblBKList = new fecherFoundation.FCLabel();
			this.lblStart = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtTop)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBottom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 684);
			this.BottomPanel.Size = new System.Drawing.Size(627, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtTop);
			this.ClientArea.Controls.Add(this.txtName4);
			this.ClientArea.Controls.Add(this.txtName3);
			this.ClientArea.Controls.Add(this.txtName2);
			this.ClientArea.Controls.Add(this.txtName1);
			this.ClientArea.Controls.Add(this.txtDate);
			this.ClientArea.Controls.Add(this.txtBottom);
			this.ClientArea.Controls.Add(this.lblFooter);
			this.ClientArea.Controls.Add(this.lblBKList);
			this.ClientArea.Controls.Add(this.lblStart);
			this.ClientArea.Size = new System.Drawing.Size(627, 624);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(627, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(373, 35);
			this.HeaderText.Text = "Alternate Lien Discharge Notice";
			// 
			// txtTop
			// 
			this.txtTop.Location = new System.Drawing.Point(30, 145);
			this.txtTop.Multiline = true;
			this.txtTop.Name = "txtTop";
			this.txtTop.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
			this.txtTop.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
			this.txtTop.SelLength = 0;
			this.txtTop.SelStart = 0;
			//this.txtTop.SelText = "";
			this.txtTop.Size = new System.Drawing.Size(543, 40);
			this.txtTop.TabIndex = 2;
			// 
			// txtName4
			// 
			this.txtName4.AutoSize = false;
			this.txtName4.BackColor = System.Drawing.SystemColors.Window;
			this.txtName4.Location = new System.Drawing.Point(30, 575);
			this.txtName4.Name = "txtName4";
			this.txtName4.Size = new System.Drawing.Size(288, 40);
			this.txtName4.TabIndex = 9;
			// 
			// txtName3
			// 
			this.txtName3.AutoSize = false;
			this.txtName3.BackColor = System.Drawing.SystemColors.Window;
			this.txtName3.Location = new System.Drawing.Point(30, 515);
			this.txtName3.Name = "txtName3";
			this.txtName3.Size = new System.Drawing.Size(288, 40);
			this.txtName3.TabIndex = 8;
			// 
			// txtName2
			// 
			this.txtName2.AutoSize = false;
			this.txtName2.BackColor = System.Drawing.SystemColors.Window;
			this.txtName2.Location = new System.Drawing.Point(30, 455);
			this.txtName2.Name = "txtName2";
			this.txtName2.Size = new System.Drawing.Size(288, 40);
			this.txtName2.TabIndex = 7;
			// 
			// txtName1
			// 
			this.txtName1.AutoSize = false;
			this.txtName1.BackColor = System.Drawing.SystemColors.Window;
			this.txtName1.Location = new System.Drawing.Point(30, 395);
			this.txtName1.Name = "txtName1";
			this.txtName1.Size = new System.Drawing.Size(288, 40);
			this.txtName1.TabIndex = 6;
			// 
			// txtDate
			// 
			this.txtDate.Location = new System.Drawing.Point(30, 30);
			this.txtDate.Mask = "##/##/####";
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(120, 40);
			this.txtDate.TabIndex = 0;
			this.txtDate.Text = "  /  /";
			// 
			// txtBottom
			// 
			this.txtBottom.Location = new System.Drawing.Point(30, 293);
			this.txtBottom.Multiline = true;
			this.txtBottom.Name = "txtBottom";
			this.txtBottom.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
			this.txtBottom.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
			this.txtBottom.SelLength = 0;
			this.txtBottom.SelStart = 0;
			//this.txtBottom.SelText = "";
			this.txtBottom.Size = new System.Drawing.Size(543, 40);
			this.txtBottom.TabIndex = 4;
			// 
			// lblFooter
			// 
			this.lblFooter.Location = new System.Drawing.Point(30, 353);
			this.lblFooter.Name = "lblFooter";
			this.lblFooter.Size = new System.Drawing.Size(118, 22);
			this.lblFooter.TabIndex = 5;
			this.lblFooter.Text = "SINCERELY,";
			// 
			// lblBKList
			// 
			this.lblBKList.Location = new System.Drawing.Point(30, 205);
			this.lblBKList.Name = "lblBKList";
			this.lblBKList.Size = new System.Drawing.Size(543, 68);
			this.lblBKList.TabIndex = 3;
			// 
			// lblStart
			// 
			this.lblStart.Location = new System.Drawing.Point(30, 90);
			this.lblStart.Name = "lblStart";
			this.lblStart.Size = new System.Drawing.Size(223, 35);
			this.lblStart.TabIndex = 1;
			this.lblStart.Text = " ";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.Text = "File";
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(240, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(157, 48);
			this.btnProcess.TabIndex = 2;
			this.btnProcess.Text = "Save & Continue";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmLienDischargeNoticeBP
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(627, 792);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmLienDischargeNoticeBP";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Alternate Lien Discharge Notice";
			this.Load += new System.EventHandler(this.frmLienDischargeNoticeBP_Load);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLienDischargeNoticeBP_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtTop)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBottom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnProcess;
	}
}
