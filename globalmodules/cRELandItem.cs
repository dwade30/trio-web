﻿//Fecher vbPorter - Version 1.0.0.59
namespace TWXF0000
{
	public class cRELandItem
	{
		//=========================================================
		private int intLandNumber;
		private int intLandCode;
		private string strUnitsA = string.Empty;
		private string strUnitsB = string.Empty;
		private double dblInfluence;
		private int intInfluenceCode;
		private bool boolUpdated;
		private bool boolDeleted;

		public int LandNumber
		{
			set
			{
				intLandNumber = value;
				IsUpdated = true;
			}
			get
			{
				int LandNumber = 0;
				LandNumber = intLandNumber;
				return LandNumber;
			}
		}

		public int LandCode
		{
			set
			{
				intLandCode = value;
				IsUpdated = true;
			}
			get
			{
				int LandCode = 0;
				LandCode = intLandCode;
				return LandCode;
			}
		}

		public string UnitsA
		{
			set
			{
				strUnitsA = value;
				IsUpdated = true;
			}
			get
			{
				string UnitsA = "";
				UnitsA = strUnitsA;
				return UnitsA;
			}
		}

		public string UnitsB
		{
			set
			{
				strUnitsB = value;
				IsUpdated = true;
			}
			get
			{
				string UnitsB = "";
				UnitsB = strUnitsB;
				return UnitsB;
			}
		}

		public double Influence
		{
			set
			{
				dblInfluence = value;
				IsUpdated = true;
			}
			get
			{
				double Influence = 0;
				Influence = dblInfluence;
				return Influence;
			}
		}

		public int InfluenceCode
		{
			set
			{
				intInfluenceCode = value;
				IsUpdated = true;
			}
			get
			{
				int InfluenceCode = 0;
				InfluenceCode = intInfluenceCode;
				return InfluenceCode;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
		}
	}
}
