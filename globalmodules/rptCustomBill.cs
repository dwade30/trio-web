﻿using System;
using System.Drawing;
using System.Collections.Generic;
using fecherFoundation;
using Wisej.Web;
using System.IO;

#if TWAR0000
using TWAR0000;


#elif TWBL0000
using TWBL0000;
#endif
using GrapeCity.ActiveReports;
using TWSharedLibrary;

namespace Global
{
    /// <summary>
    /// Summary description for rptCustomBill.
    /// </summary>
    public partial class rptCustomBill : BaseSectionReport
    {
        // ****************** THIS IS GLOBAL. DO NOT MESS WITH IT *******************
        string strModuleDB;
        // database to load custom format from
        string strThisModule;
        // Two character module name
        dynamic CustomBillClass;
        // class that handles all module specific parts of the report
        int intUnitsType;
        // inches or centimeters
        bool boolChoosePrinter;
        // is the printer already chosen or do we need to prompt
        int intTwipsPerUnit;
        // 1440 if in inches, 567 if in centimeters
        string[] aryFieldsToUpdate = null;
        // An array of names of controls that aren't static
        int lngFieldsIndex;
        string[] aryExtraParameters = null;
        // an array of parameters
        // vbPorter upgrade warning: aryAutoPopIDs As string()	OnWrite(string(), short())
        string[] aryAutoPopIDs = null;
        string[] aryAutoPopText = null;
        const int CNSTCUSTOMBILLUNITSINCHES = 0;
        const int CNSTCUSTOMBILLUNITSCENTIMETERS = 1;
        const int CNSTCUSTOMBILLFONTSTYLEREGULAR = 0;
        const int CNSTCUSTOMBILLFONTSTYLEBOLD = 1;
        const int CNSTCUSTOMBILLFONTSTYLEITALIC = 2;
        const int CNSTCUSTOMBILLFONTSTYLEBOLDITALIC = 3;
        // define a type to make it easier to pass info to functions
        private struct CustomBillFieldType
        {
            public int ID;
            public int FieldNumber;
            public int FormatID;
            public int FieldID;
            public float lngTop;
            public float lngLeft;
            public float lngHeight;
            public float lngWidth;
            public int BillType;
            public string Description;
            public int intAlignment;
            public string UserText;
            public string strFont;
            public double dblFontSize;
            public int intFontStyle;
            public string ExtraParameters;
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public CustomBillFieldType(int unusedParam)
            {
                ID = 0;
                FieldNumber = 0;
                FormatID = 0;
                FieldID = 0;
                lngTop = 0;
                lngLeft = 0;
                lngHeight = 0;
                lngWidth = 0;
                BillType = 0;
                Description = string.Empty;
                intAlignment = 0;
                UserText = string.Empty;
                strFont = string.Empty;
                dblFontSize = 0;
                intFontStyle = 0;
                ExtraParameters = string.Empty;
            }
        };

        private CustomBillFieldType CurrentFieldType = new CustomBillFieldType(0);
        private GrapeCity.ActiveReports.Document.Section.TextAlignment Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;

        private struct PrinterFontType
        {
            public string strfont10CPI;
            public string strFont12CPI;
            public string strFont17CPI;
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public PrinterFontType(int unusedParam)
            {
                strfont10CPI = string.Empty;
                strFont12CPI = string.Empty;
                strFont17CPI = string.Empty;
            }
        };

        private PrinterFontType CurrentPrinterFont = new PrinterFontType(0);
        bool blnPrintCancel;
        // MAL@20080110: Store if user pressed Cancel at the Print dialog
        string strTag = "";
        //FC:FINAL:SBE - #3947 - store pages tag in a separate list
        List<string> pagesTag = new List<string>();

        public static rptCustomBill InstancePtr
        {
            get
            {
                return (rptCustomBill)Sys.GetInstance(typeof(rptCustomBill));
            }
        }

        protected rptCustomBill _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            base.Dispose(disposing);
        }

        public rptCustomBill()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            if (_InstancePtr == null)
                _InstancePtr = this;
            Name = "Bills";
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = CustomBillClass.EndOfFile;
            if (eArgs.EOF == false)
            {
                strTag = CustomBillClass.EmailTag();
            }
        }
        // vbPorter upgrade warning: CustomBillObject As object	OnWrite(clsCustomBillAR)
        public void Init(dynamic CustomBillObject, bool boolPreview = true, bool boolDontUseReportViewer = false, bool boolShowXsForAlignment = false, bool boolExportOnly = false, string strExportFile = "")
        {
            //FC:FINAL:SBE - #3947 - store pages tag in a separate list
            if (pagesTag == null)
            {
                pagesTag = new List<string>();
            }
            pagesTag.Clear();
            UserData = pagesTag;
            string[] aryTemp = null;
            string strTemp;
            int x;
            int intTemp = 0;
            boolChoosePrinter = true;
            CustomBillClass = CustomBillObject;
            strThisModule = CustomBillClass.Module;
            strModuleDB = CustomBillClass.DBFile;
            strTemp = frmCustomBillAutoPop.InstancePtr.Init(CustomBillClass.FormatID, strModuleDB);
            if (strTemp != string.Empty)
            {
                if (strTemp == "QUIT")
                {
                    Cancel();
                    this.Close();
                    return;
                }
                aryTemp = Strings.Split(strTemp, "|", -1, CompareConstants.vbTextCompare);
                intTemp = 0;
                for (x = 0; x <= Information.UBound(aryTemp, 1); x += 2)
                {
                    intTemp += 1;
                    Array.Resize(ref aryAutoPopIDs, intTemp + 1);
                    Array.Resize(ref aryAutoPopText, intTemp + 1);
                    aryAutoPopIDs[intTemp] = aryTemp[x];
                    aryAutoPopText[intTemp] = aryTemp[x + 1];
                }
                // x
            }
            else
            {
                // just so ubound won't give errors
                aryAutoPopIDs = new string[1 + 1];
                aryAutoPopText = new string[1 + 1];
                aryAutoPopIDs[1] = FCConvert.ToString(0);
            }
            if (Strings.Trim(CustomBillClass.PrinterName) != string.Empty)
            {
                Document.Printer.PrinterName = CustomBillClass.PrinterName;
                boolChoosePrinter = false;
            }
            if (CustomBillClass.UsePrinterFonts)
            {
            }
            lngFieldsIndex = 0;
            SetupFormat();
            // MAL@20080110: Add check for user pressing Cancel at choose printer dialog
            // Tracker Reference: 11882
            if (!blnPrintCancel)
            {
                SetupFields();
                CustomBillClass.LoadData();
                if (CustomBillClass.EndOfFile)
                {
                    MessageBox.Show("No records found", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Cancel();
                    this.Close();
                    return;
                }
                strTag = CustomBillClass.EmailTag();
                if (boolPreview)
                {
                    if (boolDontUseReportViewer)
                    {
                        // presumably the other viewer or method is handling everything
                    }
                    else
                    {
                        if (boolChoosePrinter)
                        {
                            frmReportViewer.InstancePtr.Init(this);
                        }
                        else
                        {
                            frmReportViewer.InstancePtr.Init(this, Document.Printer.PrinterName);
                        }
                    }
                }
                else
                {
                    Document.Print();
                }
            }
        }

        private bool SetupFormat()
        {
            bool SetupFormat = false;
            // makes the page size and margins and might have to prompt for the printer
            clsDRWrapper clsLoad = new clsDRWrapper();
            int intReturn = 0;
            try
            {
                // On Error GoTo ErrorHandler
                SetupFormat = false;
                clsLoad.OpenRecordset("select * from custombills where ID = " + CustomBillClass.FormatID, strModuleDB);
                if (!clsLoad.EndOfFile())
                {

                    CustomBillClass.DotMatrixFormat = false;

                    intUnitsType = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int16("units"))));
                    //FC:FINAL:MSH - issue #1775: wrong values(values already in inches)
                    //if ((ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields_Double("PageWidth"))), FCConvert.ToInt16(intUnitsType) != 8.5 * 1440) || (ConvertTwips_2(clsLoad.Get_Fields_Double("PageHeight"), FCConvert.ToInt16(intUnitsType) != 15840))
                    if ((ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields_Double("PageWidth")),
                            FCConvert.ToInt16(intUnitsType)) != 8.5) ||
                        (ConvertTwips_2(clsLoad.Get_Fields_Double("PageHeight"), FCConvert.ToInt16(intUnitsType)) !=
                         11))
                    {
                        // not the standard 8.5 x 11 so change it
                        PageSettings.PaperWidth = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields_Double("PageWidth")),
                            FCConvert.ToInt16(intUnitsType));
                        PageSettings.PaperHeight =
                            ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields_Double("PageHeight")),
                                FCConvert.ToInt16(intUnitsType));
                        if (Strings.Trim(CustomBillClass.PrinterName) == string.Empty)
                        {
                            // pick the printer
                            boolChoosePrinter = false;
                        }


                        Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CustomFormat",
                            FCConvert.ToInt32(PageSettings.PaperWidth * 100),
                            FCConvert.ToInt32(PageSettings.PaperHeight * 100));
                    }

                    PageSettings.Margins.Left = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields_Double("leftmargin")),
                        FCConvert.ToInt16(intUnitsType));
                    // TODO: Field [Margins.Right] not found!! (maybe it is an alias?)
                    //FC:FINAL:CHN - issue #1179: Different margins at VB and .net versions.
                    //this.PageSettings.Margins.Right = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields("Margins.Right"))), FCConvert.ToInt16(intUnitsType);
                    PageSettings.Margins.Right = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields("RightMargin")),
                        FCConvert.ToInt16(intUnitsType));
                    // TODO: Field [Margins.Top] not found!! (maybe it is an alias?)
                    //FC:FINAL:CHN - issue #1179: Different margins at VB and .net versions.
                    //this.PageSettings.Margins.Top = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields("Margins.Top"))), FCConvert.ToInt16(intUnitsType);
                    PageSettings.Margins.Top = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields("TopMargin")),
                        FCConvert.ToInt16(intUnitsType));
                    // TODO: Field [Margins.Bottom] not found!! (maybe it is an alias?)
                    //FC:FINAL:CHN - issue #1179: Different margins at VB and .net versions.
                    //this.PageSettings.Margins.Bottom = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields("Margins.Bottom"))), FCConvert.ToInt16(intUnitsType);
                    PageSettings.Margins.Bottom = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields("BottomMargin")),
                        FCConvert.ToInt16(intUnitsType));
                    PrintWidth = PageSettings.PaperWidth - PageSettings.Margins.Left - PageSettings.Margins.Right;
                    Detail.Height = PageSettings.PaperHeight - PageSettings.Margins.Top - PageSettings.Margins.Bottom;

                }
                else
                {
                    return SetupFormat;
                }

                SetupFormat = true;
                return SetupFormat;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show(
                    "Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " +
                    Information.Err(ex).Description + "\r\n" + "In SetupFormat", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
                clsLoad.Dispose();
            }
            return SetupFormat;
        }

        private bool SetupFields()
        {
            bool SetupFields = false;
            // dynamically creates the fields
            clsDRWrapper clsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                SetupFields = false;
                clsLoad.OpenRecordset("select * from CustomBillFields where formatid = " + CustomBillClass.FormatID + " order by fieldnumber", strModuleDB);

                if (clsLoad.EndOfFile()) return SetupFields;

                while (!clsLoad.EndOfFile())
                {
                    // create a field
                    // fill the type to be passed to the function that actually creates the field
                    CurrentFieldType.ID = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ID"));
                    CurrentFieldType.FieldNumber = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("FieldNumber"));

                    // TODO: Check the table for the column [FormatID] and replace with corresponding Get_Field method
                    CurrentFieldType.FormatID = FCConvert.ToInt32(clsLoad.Get_Fields("FormatID"));

                    // TODO: Check the table for the column [FieldID] and replace with corresponding Get_Field method
                    CurrentFieldType.FieldID = FCConvert.ToInt32(clsLoad.Get_Fields("FieldID"));
                    CurrentFieldType.lngTop = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields_Double("Top")), FCConvert.ToInt16(intUnitsType)) + CustomBillClass.VerticalAlignment;
                    CurrentFieldType.lngLeft = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields_Double("Left")), FCConvert.ToInt16(intUnitsType));
                    CurrentFieldType.lngHeight = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields_Double("Height")), FCConvert.ToInt16(intUnitsType));

                    // TODO: Check the table for the column [Width] and replace with corresponding Get_Field method
                    CurrentFieldType.lngWidth = ConvertTwips_2(Conversion.Val(clsLoad.Get_Fields("Width")), FCConvert.ToInt16(intUnitsType));

                    // TODO: Check the table for the column [BillType] and replace with corresponding Get_Field method
                    CurrentFieldType.BillType = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("BillType"))));
                    CurrentFieldType.Description = FCConvert.ToString(clsLoad.Get_Fields_String("Description"));
                    CurrentFieldType.intAlignment = FCConvert.ToInt16(clsLoad.Get_Fields_Int16("Alignment"));

                    if (CurrentFieldType.intAlignment == 0)
                    {
                        Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
                    }
                    else
                        if (CurrentFieldType.intAlignment == 1)
                    {
                        Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                    }
                    else
                            if (CurrentFieldType.intAlignment == 2)
                    {
                        Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
                    }

                    CurrentFieldType.UserText = FCConvert.ToString(clsLoad.Get_Fields_String("UserText"));
                    CurrentFieldType.strFont = FCConvert.ToString(clsLoad.Get_Fields_String("Font"));

                    // TODO: Check the table for the column [FontSize] and replace with corresponding Get_Field method
                    CurrentFieldType.dblFontSize = FCConvert.ToDouble(clsLoad.Get_Fields("FontSize"));
                    CurrentFieldType.intFontStyle = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int16("FontStyle"))));
                    CurrentFieldType.ExtraParameters = FCConvert.ToString(clsLoad.Get_Fields_String("ExtraParameters"));
                    CreateAField(ref CurrentFieldType.FieldNumber, ref CurrentFieldType.FieldID);
                    clsLoad.MoveNext();
                }

                SetupFields = true;

                return SetupFields;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SetupFields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                clsLoad.DisposeOf();
            }
            return SetupFields;
        }
        private float ConvertTwips_2(double dblUnits, short intTypeUnits)
        {
            return ConvertTwips(ref dblUnits, ref intTypeUnits);
        }

        private float ConvertTwips(ref double dblUnits, ref short intTypeUnits)
        {
            float ConvertTwips = 0;
            // converts to twips from whatever units the format is in
            switch (intTypeUnits)
            {
                case CNSTCUSTOMBILLUNITSCENTIMETERS:
                    {
                        ConvertTwips = FCConvert.ToSingle(dblUnits * 567);
                        break;
                    }
                default:
                    {
                        ConvertTwips = FCConvert.ToSingle(dblUnits);
                        //* 1440;
                        break;
                    }
            }
            //end switch
            return ConvertTwips;
        }

        private void CreateAField(ref int lngFldNum, ref int lngFldType)
        {
            // creates a field based on the info in lngfldtype
            dynamic ctl = null;
            var strTemp = "";
            var lngWidth = 0;
            var lngHeight = 0;
            double dblRatio = 0;
            bool boolStatic;
            bool boolUsesFont;
            int x;
            var clsTemp = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                boolUsesFont = false;
                boolStatic = true;

                switch (lngFldType)
                {
                    case modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE:
                        {
                            var picture = new GrapeCity.ActiveReports.SectionReportModel.Picture();
                            Detail.Controls.Add(picture);
                            picture.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
                            picture.Height = CurrentFieldType.lngHeight;
                            picture.Width = CurrentFieldType.lngWidth;
                            picture.Top = CurrentFieldType.lngTop;
                            picture.Left = CurrentFieldType.lngLeft;

                            if (FCConvert.CBool(Strings.Trim(FCConvert.ToString(CurrentFieldType.UserText != string.Empty))))
                            {
                                if (File.Exists(CurrentFieldType.UserText))
                                {
                                    picture.Image = FCUtils.LoadPicture(CurrentFieldType.UserText);
                                    lngWidth = picture.Image.Width;
                                    lngHeight = picture.Image.Height;
                                    dblRatio = FCConvert.ToDouble(lngWidth) / lngHeight;

                                    if (dblRatio * picture.Height > picture.Width)
                                    {
                                        // keep width, change height
                                        picture.Height = picture.Width / FCConvert.ToSingle(dblRatio);
                                    }
                                    else
                                    {
                                        // keep height, change width
                                        picture.Width = picture.Height * FCConvert.ToSingle(dblRatio);
                                    }
                                }
                            }

                            ctl = picture;

                            break;
                        }
                    case modCustomBill.CNSTCUSTOMBILLCUSTOMTEXT:
                        {
                            // a static field
                            var label = new GrapeCity.ActiveReports.SectionReportModel.Label();
                            Detail.Controls.Add(label);
                            label.Alignment = Alignment;

                            //(GrapeCity.ActiveReports.Document.Section.TextAlignment)CurrentFieldType.intAlignment;
                            label.Text = CurrentFieldType.UserText;
                            label.Height = CurrentFieldType.lngHeight;
                            label.Width = CurrentFieldType.lngWidth;
                            label.Top = CurrentFieldType.lngTop;
                            label.Left = CurrentFieldType.lngLeft;
                            label.MultiLine = true;
                            label.WordWrap = true;
                            boolUsesFont = true;
                            ctl = label;

                            break;
                        }
                    case modCustomBill.CNSTCUSTOMBILLCUSTOMRICHEDIT:
                        // not implemented yet.  doesn't seem to be a need for it yet
                        break;
                    case modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT:
                        {
                            // not implemented yet.
                            boolStatic = false;
                            var richedit = new GrapeCity.ActiveReports.SectionReportModel.RichTextBox();
                            Detail.Controls.Add(richedit);

                            // ctl.Alignment = CurrentFieldType.intAlignment
                            richedit.Text = "";
                            richedit.Height = CurrentFieldType.lngHeight;
                            richedit.Width = CurrentFieldType.lngWidth;
                            richedit.Top = CurrentFieldType.lngTop;
                            richedit.Left = CurrentFieldType.lngLeft;

                            //richedit.MultiLine = true;
                            richedit.CanGrow = false;
                            boolUsesFont = true;
                            Fields.Add("DynamicReport" + FCConvert.ToString(lngFldNum));
                            strTemp = "";
                            clsTemp.OpenRecordset("select * from dynamicreports where ID = " + FCConvert.ToString(Conversion.Val(CurrentFieldType.UserText)), strModuleDB);

                            if (!clsTemp.EndOfFile())
                            {
                                // TODO: Field [text] not found!! (maybe it is an alias?)
                                strTemp = FCConvert.ToString(clsTemp.Get_Fields("text"));
                            }

                            Fields["DynamicReport" + lngFldNum].Value = strTemp;
                            ctl = richedit;

                            break;
                        }
                    case modCustomBill.CNSTCUSTOMBILLCUSTOMHORIZONTALLINE:
                        {
                            var line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
                            Detail.Controls.Add(line1);
                            line1.X1 = CurrentFieldType.lngLeft;
                            line1.Y1 = CurrentFieldType.lngTop;
                            line1.X2 = line1.X1 + CurrentFieldType.lngWidth;
                            line1.Y2 = line1.Y1;
                            ctl = line1;

                            break;
                        }
                    case modCustomBill.CNSTCUSTOMBILLCUSTOMVERTICALLINE:
                        {
                            var line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
                            Detail.Controls.Add(line2);
                            line2.X1 = CurrentFieldType.lngLeft;
                            line2.Y1 = CurrentFieldType.lngTop;
                            line2.X2 = line2.X1;
                            line2.Y2 = line2.Y1 + CurrentFieldType.lngHeight;
                            ctl = line2;

                            break;
                        }
                    case modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLE:
                        {
                            var shape = new GrapeCity.ActiveReports.SectionReportModel.Shape();
                            Detail.Controls.Add(shape);
                            shape.Style = GrapeCity.ActiveReports.SectionReportModel.ShapeType.Rectangle;

                            // rectangle
                            shape.Top = CurrentFieldType.lngTop;
                            shape.Left = CurrentFieldType.lngLeft;
                            shape.Height = CurrentFieldType.lngHeight;
                            shape.Width = CurrentFieldType.lngWidth;

                            //shape.BackStyle = ddBKTransparent;
                            ctl = shape;

                            break;
                        }
                    case modCustomBill.CNSTCUSTOMBILLCUSTOMRECTANGLEFILLED:
                        {
                            var label = new GrapeCity.ActiveReports.SectionReportModel.Label();
                            Detail.Controls.Add(label);
                            label.Alignment = Alignment;

                            //(GrapeCity.ActiveReports.Document.Section.TextAlignment)CurrentFieldType.intAlignment;
                            label.Text = CurrentFieldType.UserText;

                            //label.Height = CurrentFieldType.lngHeight;
                            label.Width = CurrentFieldType.lngWidth;
                            label.Top = CurrentFieldType.lngTop;
                            label.Left = CurrentFieldType.lngLeft;

                            //label.BackStyle = ddBKNormal;
                            label.BackColor = Color.Black;
                            label.ForeColor = Color.White;
                            boolUsesFont = true;
                            ctl = label;

                            break;
                        }
                    case modCustomBill.CNSTCUSTOMBILLCUSTOMBARCODE:
                        {
                            boolStatic = false;
                            var barcode = new GrapeCity.ActiveReports.SectionReportModel.Barcode();
                            Detail.Controls.Add(barcode);
                            barcode.Alignment = (StringAlignment)Alignment;

                            //CurrentFieldType.intAlignment;
                            barcode.Height = CurrentFieldType.lngHeight;
                            barcode.Width = CurrentFieldType.lngWidth;
                            barcode.Top = CurrentFieldType.lngTop;
                            barcode.Left = CurrentFieldType.lngLeft;

                            //barcode.EnableCheckSum = false;
                            barcode.Style = GrapeCity.ActiveReports.SectionReportModel.BarCodeStyle.Code39x;
                            barcode.Font = new Font("Tahoma", 8);

                            //barcode.EnableCheckSum = false;
                            barcode.CaptionPosition = Strings.UCase(CurrentFieldType.ExtraParameters) == "SHOW CAPTION" 
                                ? GrapeCity.ActiveReports.SectionReportModel.BarCodeCaptionPosition.Below 
                                : GrapeCity.ActiveReports.SectionReportModel.BarCodeCaptionPosition.None;

                            ctl = barcode;

                            break;
                        }
                    case modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP:
                        {
                            boolStatic = true;

                            // will fill this in a different way
                            var label = new GrapeCity.ActiveReports.SectionReportModel.Label();
                            Detail.Controls.Add(label);
                            label.Alignment = Alignment;

                            //(GrapeCity.ActiveReports.Document.Section.TextAlignment)CurrentFieldType.intAlignment;
                            label.Height = CurrentFieldType.lngHeight;
                            label.Width = CurrentFieldType.lngWidth;
                            label.Top = CurrentFieldType.lngTop;
                            label.Left = CurrentFieldType.lngLeft;
                            boolUsesFont = true;
                            ctl = label;

                            break;
                        }
                    default:
                        {
                            // default to non static label
                            boolStatic = false;
                            var label = new GrapeCity.ActiveReports.SectionReportModel.Label();
                            Detail.Controls.Add(label);
                            label.Alignment = Alignment;

                            //(GrapeCity.ActiveReports.Document.Section.TextAlignment)CurrentFieldType.intAlignment;
                            //FC:FINAL:AM:#4396 - increase the field height for the PDF export
                            //label.Height = CurrentFieldType.lngHeight;
                            label.Height = Math.Max(CurrentFieldType.lngHeight, 0.185F);
                            label.Width = CurrentFieldType.lngWidth;
                            label.Top = CurrentFieldType.lngTop;
                            label.Left = CurrentFieldType.lngLeft;
                            boolUsesFont = true;
                            ctl = label;

                            break;
                        }
                }

                if (boolUsesFont)
                {
                    var fontName = CurrentFieldType.strFont;
                    float fontSize;
                    var fontStyle = FontStyle.Regular;

                    if (CustomBillClass.DotMatrixFormat && CustomBillClass.UsePrinterFonts)
                    {
                        if (CurrentFieldType.dblFontSize >= 12)
                        {
                            // 10 cpi
                            if (Strings.Trim(CurrentPrinterFont.strfont10CPI) != string.Empty)
                            {
                                fontName = CurrentPrinterFont.strfont10CPI;
                            }
                        }
                        else
                        {
                            if (CurrentFieldType.dblFontSize < 10)
                            {
                                // 17 cpi
                                if (Strings.Trim(CurrentPrinterFont.strFont17CPI) != string.Empty)
                                {
                                    fontName = CurrentPrinterFont.strFont17CPI;
                                }
                            }
                            else
                            {
                                // 12 cpi
                                if (Strings.Trim(CurrentPrinterFont.strFont12CPI) != string.Empty)
                                {
                                    fontName = CurrentPrinterFont.strFont12CPI;
                                }
                            }
                        }

                        fontSize = 10;

                        // must always be 10 when using printer fonts
                    }
                    else
                    {
                        fontSize = FCConvert.ToSingle(CurrentFieldType.dblFontSize);
                    }

                    switch (CurrentFieldType.intFontStyle)
                    {
                        case CNSTCUSTOMBILLFONTSTYLEBOLD:
                            {
                                //ctl.Font.Bold = true;
                                //ctl.Font.Italic = false;
                                fontStyle = FontStyle.Bold;

                                break;
                            }
                        case CNSTCUSTOMBILLFONTSTYLEBOLDITALIC:
                            {
                                //ctl.Font.Bold = true;
                                //ctl.Font.Italic = true;
                                fontStyle = FontStyle.Bold | FontStyle.Italic;

                                break;
                            }
                        case CNSTCUSTOMBILLFONTSTYLEITALIC:
                            {
                                //ctl.Font.Bold = false;
                                //ctl.Font.Italic = true;
                                fontStyle = FontStyle.Italic;

                                break;
                            }
                        default:
                            {
                                // regular
                                //ctl.Font.Bold = false;
                                //ctl.Font.Italic = false;
                                fontStyle = FontStyle.Regular;

                                break;
                            }
                    }

                    //end switch
                    ctl.Font = new Font(fontName, fontSize, fontStyle);
                }

                strTemp = "CustomLabel" + FCConvert.ToString(lngFldNum);
                ctl.Name = strTemp;
                ctl.Tag = lngFldType;

                // so we can tell from the control name what code to use from the custombillcode table
                if (lngFldType == modCustomBill.CNSTCUSTOMBILLCUSTOMAUTOPOP)
                {
                    ctl.Text = "";

                    for (x = 1; x <= Information.UBound(aryAutoPopIDs, 1); x++)
                    {
                        if (Conversion.Val(aryAutoPopIDs[x]) == CurrentFieldType.ID)
                        {
                            // its a match
                            aryAutoPopIDs[x] = strTemp;

                            // the array will now have the control names not ids
                        }
                    }

                    // x
                }

                if (boolStatic) return;

                // add to the list so we know to update it in the detail section
                lngFieldsIndex += 1;
                Array.Resize(ref aryFieldsToUpdate, lngFieldsIndex + 1);
                aryFieldsToUpdate[lngFieldsIndex] = ctl.Name;
                Array.Resize(ref aryExtraParameters, lngFieldsIndex + 1);
                aryExtraParameters[lngFieldsIndex] = CurrentFieldType.ExtraParameters;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show($"Error Number {FCConvert.ToString(Information.Err(ex).Number)}  {Information.Err(ex).Description}\r\nIn CreateAField", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                clsTemp.DisposeOf();
            }
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {

        }

        private void ActiveReport_Terminate(object sender, EventArgs e)
        {

        }

        private void Detail_Format(object sender, EventArgs e)
        {
            int x;
            int lngCode = 0;
            if (!CustomBillClass.EndOfFile)
            {
                if (lngFieldsIndex > 0)
                {
                    // if there are any to update.  Otherwise the array will be empty and will cause an error
                    // for each control that needs to be updated
                    for (x = 1; x <= lngFieldsIndex; x++)
                    {
                        lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Detail.Controls[aryFieldsToUpdate[x]].Tag)));

                        switch (lngCode)
                        {
                            case modCustomBill.CNSTCUSTOMBILLCUSTOMRICHEDIT:
                                // not actually used at this time
                                (Detail.Controls[aryFieldsToUpdate[x]] as dynamic).Text = CustomBillClass.GetDataByCode(lngCode, aryExtraParameters[x]);

                                break;
                            case modCustomBill.CNSTCUSTOMBILLCUSTOMIMAGE:
                                // dynamic images are not supported at this time
                                // should never get here
                                break;
                            case modCustomBill.CNSTCUSTOMBILLCUSTOMTEXTDYNAMICDOCUMENT:
                                (Detail.Controls[aryFieldsToUpdate[x]] as dynamic).Text = modDynamicReports.ParseDynamicDocumentString_18(strModuleDB, Fields["DynamicReport" + Strings.Mid(aryFieldsToUpdate[x], 12)].Value.ToString(), modCustomBill.CNSTDYNAMICREPORTTYPECUSTOMBILL, CustomBillClass);

                                break;
                            default:
                                (Detail.Controls[aryFieldsToUpdate[x]] as dynamic).Text = CustomBillClass.GetDataByCode(lngCode, aryExtraParameters[x]);

                                break;
                        }
                    }
                    // x
                }
                // now do all of the autopops
                for (x = 1; x <= Information.UBound(aryAutoPopIDs, 1); x++)
                {
                    if (aryAutoPopIDs[x] != string.Empty && aryAutoPopIDs[x] != "0")
                    {
                        (Detail.Controls[aryAutoPopIDs[x]] as dynamic).Text = aryAutoPopText[x];
                    }
                }
                // x
                CustomBillClass.MoveNext();
            }
        }

        private void PageHeader_Format(object sender, EventArgs e)
        {
            //FC:FINAL:SBE - #3947 - store pages tag in a separate list
            pagesTag.Add(strTag);
            //this.Canvas.Tag = strTag;
        }

    }
}
