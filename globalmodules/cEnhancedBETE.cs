﻿//Fecher vbPorter - Version 1.0.0.35
namespace Global
{
	public class cEnhancedBETE
	{
		//=========================================================
		private int intReportYear;
		private string strMunicipality = string.Empty;
		private double dblTotalValuationBETEQualifiedExemptProperty;
		private double dblTotalValuationBETENotInTIF;
		private double dblBETEPercent;
		private double dblTotalValuationBETEStandardReim;
		private double dblTotalValueBusinessPersonalProperty;
		private double dblTotalValueTaxableProperty;
		private double dblTotalValuationBETEEnhancedReimNotInTif;
		private double dblPersonalPropertyFactor;
		private double dblHalfFactor;
		private double dblHalfPlus50;
		private double dblTotalValuationBETEEnhancedReim;
		private double dblRetentionPercentage;
		private double dblEffectivePercentage;
		private double dblBETEQualifedInTIF;
		private double dblTIFBETEQualifiedREim;
		private double dblTotalReimbursableBETE;

		public int ReportYear
		{
			set
			{
				intReportYear = value;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				int ReportYear = 0;
				ReportYear = intReportYear;
				return ReportYear;
			}
		}

		public string Municipality
		{
			set
			{
				strMunicipality = value;
			}
			get
			{
				string Municipality = "";
				Municipality = strMunicipality;
				return Municipality;
			}
		}

		public double HalfFactor
		{
			set
			{
				dblHalfFactor = value;
			}
			get
			{
				double HalfFactor = 0;
				HalfFactor = dblHalfFactor;
				return HalfFactor;
			}
		}

		public double HalfPlus50
		{
			set
			{
				dblHalfPlus50 = value;
			}
			get
			{
				double HalfPlus50 = 0;
				HalfPlus50 = dblHalfPlus50;
				return HalfPlus50;
			}
		}

		public double TotalValuationBETEQualifiedExemptProperty
		{
			set
			{
				dblTotalValuationBETEQualifiedExemptProperty = value;
			}
			get
			{
				double TotalValuationBETEQualifiedExemptProperty = 0;
				TotalValuationBETEQualifiedExemptProperty = dblTotalValuationBETEQualifiedExemptProperty;
				return TotalValuationBETEQualifiedExemptProperty;
			}
		}

		public double TotalValuationBETENotInTIF
		{
			set
			{
				dblTotalValuationBETENotInTIF = value;
			}
			get
			{
				double TotalValuationBETENotInTIF = 0;
				TotalValuationBETENotInTIF = dblTotalValuationBETENotInTIF;
				return TotalValuationBETENotInTIF;
			}
		}

		public double BETEPercent
		{
			set
			{
				dblBETEPercent = value;
			}
			get
			{
				double BETEPercent = 0;
				BETEPercent = dblBETEPercent;
				return BETEPercent;
			}
		}

		public double TotalValuationBETEStandardReim
		{
			set
			{
				dblTotalValuationBETEStandardReim = value;
			}
			get
			{
				double TotalValuationBETEStandardReim = 0;
				TotalValuationBETEStandardReim = dblTotalValuationBETEStandardReim;
				return TotalValuationBETEStandardReim;
			}
		}

		public double TotalValueBusinessPersonalProperty
		{
			set
			{
				dblTotalValueBusinessPersonalProperty = value;
			}
			get
			{
				double TotalValueBusinessPersonalProperty = 0;
				TotalValueBusinessPersonalProperty = dblTotalValueBusinessPersonalProperty;
				return TotalValueBusinessPersonalProperty;
			}
		}

		public double TotalValueTaxableProperty
		{
			set
			{
				dblTotalValueTaxableProperty = value;
			}
			get
			{
				double TotalValueTaxableProperty = 0;
				TotalValueTaxableProperty = dblTotalValueTaxableProperty;
				return TotalValueTaxableProperty;
			}
		}

		public double TotalValuationBETEEnhancedReimNotInTif
		{
			set
			{
				dblTotalValuationBETEEnhancedReimNotInTif = value;
			}
			get
			{
				double TotalValuationBETEEnhancedReimNotInTif = 0;
				TotalValuationBETEEnhancedReimNotInTif = dblTotalValuationBETEEnhancedReimNotInTif;
				return TotalValuationBETEEnhancedReimNotInTif;
			}
		}

		public double PersonalPropertyFactor
		{
			set
			{
				dblPersonalPropertyFactor = value;
			}
			get
			{
				double PersonalPropertyFactor = 0;
				PersonalPropertyFactor = dblPersonalPropertyFactor;
				return PersonalPropertyFactor;
			}
		}

		public double TotalValuationBeteSubjectEnhancedReim
		{
			set
			{
				dblTotalValuationBETEEnhancedReim = value;
			}
			get
			{
				double TotalValuationBeteSubjectEnhancedReim = 0;
				TotalValuationBeteSubjectEnhancedReim = dblTotalValuationBETEEnhancedReim;
				return TotalValuationBeteSubjectEnhancedReim;
			}
		}

		public double RetentionPercentage
		{
			set
			{
				dblRetentionPercentage = value;
			}
			get
			{
				double RetentionPercentage = 0;
				RetentionPercentage = dblRetentionPercentage;
				return RetentionPercentage;
			}
		}

		public double EffectivePercentage
		{
			set
			{
				dblEffectivePercentage = value;
			}
			get
			{
				double EffectivePercentage = 0;
				EffectivePercentage = dblEffectivePercentage;
				return EffectivePercentage;
			}
		}

		public double BETEValuationInTIF
		{
			set
			{
				dblBETEQualifedInTIF = value;
			}
			get
			{
				double BETEValuationInTIF = 0;
				BETEValuationInTIF = dblBETEQualifedInTIF;
				return BETEValuationInTIF;
			}
		}

		public double TIFBETEQualifiedValuationReimbursable
		{
			set
			{
				dblTIFBETEQualifiedREim = value;
			}
			get
			{
				double TIFBETEQualifiedValuationReimbursable = 0;
				TIFBETEQualifiedValuationReimbursable = dblTIFBETEQualifiedREim;
				return TIFBETEQualifiedValuationReimbursable;
			}
		}

		public double TotalReimbursableBETE
		{
			set
			{
				dblTotalReimbursableBETE = value;
			}
			get
			{
				double TotalReimbursableBETE = 0;
				TotalReimbursableBETE = dblTotalReimbursableBETE;
				return TotalReimbursableBETE;
			}
		}
	}
}
