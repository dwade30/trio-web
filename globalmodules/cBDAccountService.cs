﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cBDAccountService
	{
		//=========================================================
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cBDAccountController bdAcctController = new cBDAccountController();
		private cBDAccountController bdAcctController_AutoInitialized;

		private cBDAccountController bdAcctController
		{
			get
			{
				if (bdAcctController_AutoInitialized == null)
				{
					bdAcctController_AutoInitialized = new cBDAccountController();
				}
				return bdAcctController_AutoInitialized;
			}
			set
			{
				bdAcctController_AutoInitialized = value;
			}
		}
	}
}
