﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using System.Runtime.InteropServices;
using Wisej.Core;
using Wisej.Web;

namespace Global
{
	//FC:COMP:MW
	public enum AppWinStyle
	{
		MaximizedFocus = 3,
		NormalNoFocus = 5,
		NormalFocus = 6
	}

	public class modAPIsConst
	{
		public struct POINTAPI
		{
			public int X;
			public int y;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public POINTAPI(int unusedParam)
			{
				this.X = 0;
				this.y = 0;
			}
		};

		public struct RECT
		{
			public int Left;
			public int Top;
			public int Right;
			public int Bottom;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public RECT(int unusedParam)
			{
				this.Left = 0;
				this.Top = 0;
				this.Right = 0;
				this.Bottom = 0;
			}
		};

		public const int TRIOCOLORRES = 16;
		// Actual color resolution
		public const int PASSTHROUGH = 19;
		public const short CB_GETDROPPEDSTATE = 0x157;
		public const short CB_SETDROPPEDWIDTH = 0x160;
		public const short CB_SHOWDROPDOWN = 0x14F;
		
		public const int HTCAPTION = 2;
		public const int ES_NUMBER = 0x2000;
		public const int ES_UPPERCASE = 0x8;
		public const int ES_LOWERCASE = 0x10;
		public const int ES_READONLY = 0x800;
	
		
		[DllImport("user32", EntryPoint = "SendMessageA")]
		public static extern int SendMessage(int hwnd, int wMsg, short wParam, IntPtr lParam);

		[DllImport("user32", EntryPoint = "SendMessageA")]
		public static extern int SendMessageByNum(int hwnd, int wMsg, short wParam, int lParam);

		

		[DllImport("user32")]
		public static extern int MoveWindow(int hwnd, int X, int y, int nWidth, int nHeight, int bRepaint);
		//VBtoInfo: 0/2
		[DllImport("kernel32", EntryPoint = "GetComputerNameA")]
		public static extern int GetComputerName(IntPtr lpBuffer, ref int nSize);
		//VBtoInfo: 1
		public static int GetComputerNameWrp(ref string lpBuffer, ref int nSize)
		{
			int ret;
			IntPtr plpBuffer = FCUtils.GetByteFromString(lpBuffer);
			ret = GetComputerName(plpBuffer, ref nSize);
			FCUtils.GetStringFromByte(ref lpBuffer, plpBuffer);
			return ret;
		}

		[DllImport("shell32.dll", EntryPoint = "ShellExecuteA")]
		public static extern int ShellExecute(int hwnd, string lpOperation, string lpFile, string lpParameters, string lpDirectory, int nShowCmd);

		[DllImport("user32")]
		public static extern int GetDC(int hwnd);
		// this will return the handle of a device
		[DllImport("gdi32")]
		public static extern int GetDeviceCaps(int hdc, int nIndex);
		
		[DllImport("kernel32", EntryPoint = "SetCurrentDirectoryA")]
		public static extern int SetCurrentDirectory(IntPtr lpPathName);
		//VBtoInfo: 1
		public static int SetCurrentDirectoryWrp(ref string lpPathName)
		{
			int ret;
			IntPtr plpPathName = FCUtils.GetByteFromString(lpPathName);
			ret = SetCurrentDirectory(plpPathName);
			FCUtils.GetStringFromByte(ref lpPathName, plpPathName);
			return ret;
		}

		
		//[DllImport("kernel32", EntryPoint = "GetEnvironmentVariableA")]
		//public static extern int GetEnvironmentVariable(IntPtr lpName, IntPtr lpBuffer, int nSize);
		//VBtoInfo: 1
		
		
	}
}
