﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace Global
{
	/// <summary>
	/// Summary description for frmInput.
	/// </summary>
	partial class frmInput : BaseForm
	{
		public T2KBackFillWhole T2KWhole;
		public T2KBackFillDecimal T2KDecimal;
		public T2KPhoneNumberBox T2KPhoneInput;
		public T2KDateBox T2KDateInput;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCTextBox txtInput;
		public fecherFoundation.FCLabel lblDescription;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInput));
            this.T2KWhole = new Global.T2KBackFillWhole();
            this.T2KDecimal = new Global.T2KBackFillDecimal();
            this.T2KPhoneInput = new Global.T2KPhoneNumberBox();
            this.T2KDateInput = new Global.T2KDateBox();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdOK = new fecherFoundation.FCButton();
            this.txtInput = new fecherFoundation.FCTextBox();
            this.lblDescription = new fecherFoundation.FCLabel();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KWhole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDecimal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KPhoneInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDateInput)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 262);
            this.BottomPanel.Size = new System.Drawing.Size(421, 0);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.T2KWhole);
            this.ClientArea.Controls.Add(this.T2KDecimal);
            this.ClientArea.Controls.Add(this.T2KPhoneInput);
            this.ClientArea.Controls.Add(this.T2KDateInput);
            this.ClientArea.Controls.Add(this.cmdCancel);
            this.ClientArea.Controls.Add(this.cmdOK);
            this.ClientArea.Controls.Add(this.txtInput);
            this.ClientArea.Controls.Add(this.lblDescription);
            this.ClientArea.Location = new System.Drawing.Point(0, 52);
            this.ClientArea.Size = new System.Drawing.Size(421, 210);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(421, 52);
            // 
            // T2KWhole
            // 
            clientEvent1.Event = "keyup";
            clientEvent1.JavaScript = resources.GetString("clientEvent1.JavaScript");
            this.javaScript1.GetJavaScriptEvents(this.T2KWhole).Add(clientEvent1);
            this.T2KWhole.Location = new System.Drawing.Point(94, 82);
            this.T2KWhole.MaxLength = 20;
            this.T2KWhole.Name = "T2KWhole";
            this.T2KWhole.Size = new System.Drawing.Size(37, 22);
            this.T2KWhole.TabIndex = 2;
            this.T2KWhole.Visible = false;
            // 
            // T2KDecimal
            // 
            this.T2KDecimal.Location = new System.Drawing.Point(290, 82);
            this.T2KDecimal.MaxLength = 20;
            this.T2KDecimal.Name = "T2KDecimal";
            this.T2KDecimal.Size = new System.Drawing.Size(43, 22);
            this.T2KDecimal.TabIndex = 5;
            this.T2KDecimal.Visible = false;
            // 
            // T2KPhoneInput
            // 
            this.T2KPhoneInput.Location = new System.Drawing.Point(227, 82);
            this.T2KPhoneInput.MaxLength = 14;
            this.T2KPhoneInput.Name = "T2KPhoneInput";
            this.T2KPhoneInput.Size = new System.Drawing.Size(43, 22);
            this.T2KPhoneInput.TabIndex = 4;
            this.T2KPhoneInput.Visible = false;
            // 
            // T2KDateInput
            // 
            this.T2KDateInput.Location = new System.Drawing.Point(30, 82);
            this.T2KDateInput.MaxLength = 10;
            this.T2KDateInput.Name = "T2KDateInput";
            this.T2KDateInput.Size = new System.Drawing.Size(45, 22);
            this.T2KDateInput.TabIndex = 1;
            this.T2KDateInput.Visible = false;
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(207, 142);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(81, 40);
            this.cmdCancel.TabIndex = 7;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.AppearanceKey = "actionButton";
            this.cmdOK.Location = new System.Drawing.Point(132, 142);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(56, 40);
            this.cmdOK.TabIndex = 6;
            this.cmdOK.Text = "OK";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // txtInput
            // 
            this.txtInput.BackColor = System.Drawing.SystemColors.Window;
            this.txtInput.Location = new System.Drawing.Point(138, 82);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(164, 40);
            this.txtInput.TabIndex = 3;
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(30, 8);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(361, 54);
            this.lblDescription.TabIndex = 8;
            this.lblDescription.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmInput
            // 
            this.AcceptButton = this.cmdOK;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(421, 262);
            this.Name = "frmInput";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmInput_Load);
            this.Activated += new System.EventHandler(this.frmInput_Activated);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.T2KWhole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDecimal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KPhoneInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.T2KDateInput)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private JavaScript javaScript1;
		private System.ComponentModel.IContainer components;
	}
}
