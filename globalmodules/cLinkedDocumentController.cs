﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;

namespace Global
{
	//public class cLinkedDocumentController
	//{
	//	//=========================================================
	//	private string strLastError = "";
	//	private int lngLastError;

	//	public string LastErrorMessage
	//	{
	//		get
	//		{
	//			string LastErrorMessage = "";
	//			LastErrorMessage = strLastError;
	//			return LastErrorMessage;
	//		}
	//	}

	//	public int LastErrorNumber
	//	{
	//		get
	//		{
	//			int LastErrorNumber = 0;
	//			LastErrorNumber = lngLastError;
	//			return LastErrorNumber;
	//		}
	//	}
	//	// vbPorter upgrade warning: 'Return' As Variant --> As bool
	//	public bool HadError
	//	{
	//		get
	//		{
	//			bool HadError = false;
	//			HadError = lngLastError != 0;
	//			return HadError;
	//		}
	//	}

	//	public void ClearErrors()
	//	{
	//		strLastError = "";
	//		lngLastError = 0;
	//	}

	//	private void SetError(int lngErrorNumber, string strErrorMessage)
	//	{
	//		lngLastError = lngErrorNumber;
	//		strLastError = strErrorMessage;
	//	}

	//	public cLinkedDocument GetDocument(int lngID, string strDB)
	//	{
	//		cLinkedDocument GetDocument = null;
	//		ClearErrors();
	//		try
	//		{
	//			// On Error GoTo ErrorHandler
	//			clsDRWrapper rsLoad = new clsDRWrapper();
	//			cLinkedDocument lDoc = new cLinkedDocument();
	//			rsLoad.OpenRecordset("select * from linkeddocuments where id = " + FCConvert.ToString(lngID), strDB);
	//			if (!rsLoad.EndOfFile())
	//			{
	//				lDoc = new cLinkedDocument();
	//				FillDocument(ref lDoc, ref rsLoad);
	//			}
	//			GetDocument = lDoc;
	//			return GetDocument;
	//		}
	//		catch (Exception ex)
	//		{
	//			// ErrorHandler:
	//			SetError(Information.Err(ex).Number, Information.Err(ex).Description);
	//		}
	//		return GetDocument;
	//	}

	//	public cGenericCollection GetDocumentsByReference(int lngRefID, string strReferenceType, string strDB)
	//	{
	//		cGenericCollection GetDocumentsByReference = null;
	//		ClearErrors();
	//		try
	//		{
	//			// On Error GoTo ErrorHandler
	//			clsDRWrapper rsLoad = new clsDRWrapper();
	//			cLinkedDocument lDoc;
	//			cGenericCollection gColl = new cGenericCollection();
	//			rsLoad.OpenRecordset("select * from linkeddocuments where docreferenceid = " + FCConvert.ToString(lngRefID) + " and docreferencetype = '" + strReferenceType + "' order by description", strDB);
	//			if (!rsLoad.EndOfFile())
	//			{
	//				lDoc = new cLinkedDocument();
	//				FillDocument(ref lDoc, ref rsLoad);
	//				gColl.AddItem(lDoc);
	//			}
	//			GetDocumentsByReference = gColl;
	//			return GetDocumentsByReference;
	//		}
	//		catch (Exception ex)
	//		{
	//			// ErrorHandler:
	//			SetError(Information.Err(ex).Number, Information.Err(ex).Description);
	//		}
	//		return GetDocumentsByReference;
	//	}

	//	private void FillDocument(ref cLinkedDocument lDoc, ref clsDRWrapper rsLoad)
	//	{
	//		if (!(lDoc == null))
	//		{
	//			if (rsLoad != null)
	//			{
	//				if (!rsLoad.EndOfFile())
	//				{
	//					lDoc.Description = rsLoad.Get_Fields_String("Description");
	//					lDoc.DocReferenceID = rsLoad.Get_Fields_Int32("DocReferenceID");
	//					// TODO: Check the table for the column [DocReferenceType] and replace with corresponding Get_Field method
	//					lDoc.DocReferenceType = rsLoad.Get_Fields_String("DocReferenceType");
	//					lDoc.FileName = rsLoad.Get_Fields_String("FileName");
	//					lDoc.IsDeleted = false;
	//					lDoc.IsUpdated = false;
	//				}
	//			}
	//		}
	//	}

	//	public void DeleteDocument(int lngID, string strDB)
	//	{
	//		ClearErrors();
	//		try
	//		{
	//			// On Error GoTo ErrorHandler
	//			clsDRWrapper rsSave = new clsDRWrapper();
	//			rsSave.Execute("delete from linkeddocuments where id = " + FCConvert.ToString(lngID), strDB);
	//			return;
	//		}
	//		catch (Exception ex)
	//		{
	//			// ErrorHandler:
	//			SetError(Information.Err(ex).Number, Information.Err(ex).Description);
	//		}
	//	}

	//	private void SaveDocument(ref cLinkedDocument lDoc, string strDB)
	//	{
	//		try
	//		{
	//			// On Error GoTo ErrorHandler
	//			if (!(lDoc == null))
	//			{
	//				if (lDoc.IsDeleted)
	//				{
	//					DeleteDocument(lDoc.ID, strDB);
	//					return;
	//				}
	//				ClearErrors();
	//				clsDRWrapper rsSave = new clsDRWrapper();
	//				rsSave.OpenRecordset("select * from linkeddocuments where id = " + lDoc.ID, strDB);
	//				if (!rsSave.EndOfFile())
	//				{
	//					rsSave.Edit();
	//				}
	//				else
	//				{
	//					if (lDoc.ID > 0)
	//					{
	//						SetError(9999, "Linked document record not found");
	//						return;
	//					}
	//					rsSave.AddNew();
	//				}
	//				rsSave.Set_Fields("FileName", lDoc.FileName);
	//				rsSave.Set_Fields("Description", lDoc.Description);
	//				rsSave.Set_Fields("DocReferenceID", lDoc.DocReferenceID);
	//				rsSave.Set_Fields("DocReferenceType", lDoc.DocReferenceType);
	//				rsSave.Update();
	//				lDoc.ID = rsSave.Get_Fields_Int32("ID");
	//				lDoc.IsUpdated = false;
	//			}
	//			return;
	//		}
	//		catch (Exception ex)
	//		{
	//			// ErrorHandler:
	//			SetError(Information.Err(ex).Number, Information.Err(ex).Description);
	//		}
	//	}
	//}
}
