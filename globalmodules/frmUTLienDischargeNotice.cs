﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmUTLienDischargeNotice.
	/// </summary>
	public partial class frmUTLienDischargeNotice : BaseForm
	{
		public frmUTLienDischargeNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUTLienDischargeNotice InstancePtr
		{
			get
			{
				return (frmUTLienDischargeNotice)Sys.GetInstance(typeof(frmUTLienDischargeNotice));
			}
		}

		protected frmUTLienDischargeNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/16/2004              *
		// *
		// MODIFIED BY    :               Melissa Lamprecht       *
		// LAST UPDATED   :               09/18/2007              *
		// ********************************************************
		clsDRWrapper rsLien = new clsDRWrapper();
		clsDRWrapper rsSettings = new clsDRWrapper();
		bool boolLoaded;
		int lngLienRecordNumber;
		bool boolPrintAllSaved;
		bool boolPrintArchive;
		bool boolAbateDefault;
		string strAbate;
		string strPayment;
		bool boolDefaultAppearedDate;
		// grid cols
		int lngColData;
		int lngColTitle;
		int lngColHidden;
		// grid rows
		int lngRowTName;
		int lngRowMuni;
		int lngRowCounty;
		int lngRowOwner1;
		int lngRowOwner2;
		int lngRowPayDate;
		int lngRowFileDate;
		int lngRowBook;
		int lngRowPage;
		int lngRowHisHer;
		int lngRowSignerName;
		int lngRowTitle;
		int lngRowSignerDesignation;
		int lngRowCommissionExpiration;
		int lngRowMapLot;
		int lngRowSignedDate;
		int lngRowAbatementDefault;
		int lngRowAppearedDate;
		int lngTempAccountNumber;
		DateTime dtPaymentDate;
		bool boolWater;
		string strWS = "";
		// vbPorter upgrade warning: dtPassPayDate As DateTime	OnWrite(DateTime, string)
		public void Init(DateTime dtPassPayDate, bool boolPassWater, int lngPassLRN = 0, bool boolPassPrintAllSaved = false, bool boolAbateDefault = false, bool boolArchive = false)
		{
			try
			{
                //FC:FINAL:SBE - #4065 - in VB6 Form Load event is triggered when a control property is accessed. Force execution of method from Form Load event to have the same order of code execution
                boolLoaded = true;
                Format_Grid(true);
                strAbate = "Paid With Abatement";
                strPayment = "Paid With Payment";
                // On Error GoTo ERROR_HANDLER
                clsDRWrapper rsTest = new clsDRWrapper();
				bool boolRecords = false;
				dtPaymentDate = dtPassPayDate;
				boolPrintAllSaved = boolPassPrintAllSaved;
				boolWater = boolPassWater;
				boolPrintArchive = boolArchive;
				if (boolWater)
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				if (boolPrintAllSaved)
				{
					// if all saved are to be printed, then check to see if there are any
					if (!boolPrintArchive)
					{
						rsTest.OpenRecordset("SELECT top 1 * FROM DischargeNeeded INNER JOIN Bill ON DischargeNeeded.LienKey = Bill." + strWS + "LienRecordNumber WHERE ISNULL(Printed,0) = 0", modExtraModules.strUTDatabase);
					}
					else
					{
						rsTest.OpenRecordset("SELECT top 1 * FROM DischargeNeededArchive INNER JOIN Bill ON DischargeNeededArchive.LienKey = Bill." + strWS + "LienRecordNumber", modExtraModules.strUTDatabase);
					}
					boolRecords = !rsTest.EndOfFile();
					lngColData = 2;
					lngColHidden = 1;
					lngColTitle = 0;
					lngRowOwner1 = -1;
					lngRowOwner2 = -1;
					lngRowPayDate = -1;
					lngRowBook = -1;
					lngRowPage = -1;
					lngRowMapLot = -1;
					lngRowFileDate = -1;
					lngRowAbatementDefault = -1;
					// kk10292015 trouts-171  Make consistent with CL - show Appeared Date   lngRowAppearedDate = -1     'MAL@20070918
					lngRowTName = 0;
					lngRowTitle = 1;
					lngRowMuni = 2;
					lngRowCounty = 3;
					lngRowSignedDate = 4;
					lngRowHisHer = 5;
					lngRowSignerName = 6;
					lngRowSignerDesignation = 7;
					lngRowCommissionExpiration = 8;
					lngRowAppearedDate = 9;
					// kk10292015 trouts-171
				}
				else
				{
					// one single Lien Discharge Notice
					lngLienRecordNumber = lngPassLRN;
					lngColData = 2;
					lngColHidden = 1;
					lngColTitle = 0;
					lngRowTName = 0;
					lngRowTitle = 1;
					lngRowMuni = 2;
					lngRowCounty = 3;
					lngRowOwner1 = 4;
					lngRowOwner2 = 5;
					lngRowPayDate = 6;
					lngRowFileDate = 7;
					lngRowSignedDate = 8;
					lngRowBook = 9;
					lngRowPage = 10;
					lngRowMapLot = 11;
					lngRowAbatementDefault = 12;
					lngRowHisHer = 13;
					lngRowSignerName = 14;
					lngRowSignerDesignation = 15;
					lngRowCommissionExpiration = 16;
					lngRowAppearedDate = 17;
					// MAL@20070918
					if (lngLienRecordNumber != 0)
					{
						lblYear.Text = "Lien : " + FCConvert.ToString(lngLienRecordNumber);
						lblYear.Visible = false;
					}
					else
					{
						lblYear.Visible = false;
					}
					rsLien.OpenRecordset("SELECT Lien.ID as LienKey,Lien.RateKey,OName,OName2,Lien.Book,Lien.Page,MapLot,AccountKey FROM Lien INNER JOIN Bill ON Lien.ID = Bill." + strWS + "LienRecordNumber WHERE Lien.ID = " + FCConvert.ToString(lngLienRecordNumber), modExtraModules.strUTDatabase);
					if (!rsLien.EndOfFile())
					{
						// keep going
					}
					else
					{
						MessageBox.Show("The lien record number " + FCConvert.ToString(lngLienRecordNumber) + " could not be found.", "Missing Lien Record", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						Close();
					}
				}
				Format_Grid(true);
				LoadSettings();
				if (lngLienRecordNumber > 0 || (boolPrintAllSaved && boolRecords))
				{
					this.Show(FormShowEnum.Modal, App.MainForm);
				}
				else
				{
					if (!(boolPrintAllSaved && boolRecords))
					{
						MessageBox.Show("No saved records to be printed.", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					else
					{
						MessageBox.Show("Cannot find a lien record for that rate record.", "Cannot Find Lien", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					Close();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmUTLienDischargeNotice_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
			}
		}

		private void frmUTLienDischargeNotice_Load(object sender, System.EventArgs e)
		{
            //Begin Unmaped Properties
            //frmUTLienDischargeNotice.FillStyle	= 0;
            //frmUTLienDischargeNotice.ScaleWidth	= 9300;
            //frmUTLienDischargeNotice.ScaleHeight	= 7545;
            //frmUTLienDischargeNotice.LinkTopic	= "Form2";
            //frmUTLienDischargeNotice.LockControls	= true;
            //frmUTLienDischargeNotice.PaletteMode	= 1  'UseZOrder;
            //End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			//FC:FINAL:SBE - #4065 - in VB6 Form Load event is triggered when a control property is accessed. Force execution of method from Form Load event to have the same order of code execution
			//boolLoaded = true;
			//Format_Grid(true);
			//strAbate = "Paid With Abatement";
			//strPayment = "Paid With Payment";
		}

		private void frmUTLienDischargeNotice_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmUTLienDischargeNotice_Resize(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				FormatGrid();
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void SaveSettings()
		{
			// this will save all of the settings for the lien discharge notice
			clsDRWrapper rsTemp = new clsDRWrapper();
			// vbPorter upgrade warning: dtComExp As DateTime	OnWrite(DateTime, short)
			DateTime dtComExp;
			// vbPorter upgrade warning: dtAppearedDate As DateTime	OnWrite(DateTime, short)
			DateTime dtAppearedDate;
			bool boolTemp;
			clsDRWrapper rsArchive = new clsDRWrapper();
			clsDRWrapper rsPayments = new clsDRWrapper();
			clsDRWrapper rsUpdate = new clsDRWrapper();
			if (ValidateAnswers())
			{
				rsSettings.OpenRecordset("SELECT * FROM " + strWS + "Control_DischargeNotice", modExtraModules.strUTDatabase);
				if (rsSettings.EndOfFile())
				{
					rsSettings.AddNew();
				}
				else
				{
					rsSettings.Edit();
				}
				// county
				rsSettings.Set_Fields("County", vsData.TextMatrix(lngRowCounty, lngColData));
				// treasurer name
				rsSettings.Set_Fields("Treasurer", vsData.TextMatrix(lngRowTName, lngColData));
				// his/her
				if (Strings.UCase(Strings.Left(vsData.TextMatrix(lngRowHisHer, lngColData), 3)) == "HIS")
				{
					rsSettings.Set_Fields("Male", true);
				}
				else
				{
					rsSettings.Set_Fields("Male", false);
				}
				// write the signers name and designation for default info
				// Signer's name
				rsSettings.Set_Fields("SignerName", Strings.Trim(vsData.TextMatrix(lngRowSignerName, lngColData)));
				// Signer's Designation
				rsSettings.Set_Fields("SignerDesignation", Strings.Trim(vsData.TextMatrix(lngRowSignerDesignation, lngColData)));
				// Commission Expiration Date
				if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) != "")
				{
					if (Information.IsDate(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)))
					{
						rsSettings.Set_Fields("CommissionExpiration", vsData.TextMatrix(lngRowCommissionExpiration, lngColData));
					}
					else
					{
						rsSettings.Set_Fields("CommissionExpiration", 0);
					}
				}
				else
				{
					rsSettings.Set_Fields("CommissionExpiration", 0);
				}
				rsSettings.Update();
				if (boolPrintAllSaved)
				{
					if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) != "")
					{
						dtComExp = DateAndTime.DateValue(vsData.TextMatrix(lngRowCommissionExpiration, lngColData));
					}
					else
					{
						dtComExp = DateTime.FromOADate(0);
					}
					// kk10292015 trouts-171  Allow Appeared Date, same as CL         dtAppearedDate = 0
					if (Strings.InStr(1, vsData.TextMatrix(lngRowAppearedDate, lngColData), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) != "" && Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) != "/  /")
					{
						dtAppearedDate = DateAndTime.DateValue(vsData.TextMatrix(lngRowAppearedDate, lngColData));
					}
					else
					{
						dtAppearedDate = DateTime.FromOADate(0);
					}
					if (!boolPrintArchive)
					{
						arUTAllLienDischargeNotice.InstancePtr.Init(FCConvert.CBool(strWS == "W"), vsData.TextMatrix(lngRowTName, lngColData), vsData.TextMatrix(lngRowMuni, lngColData), vsData.TextMatrix(lngRowCounty, lngColData), vsData.TextMatrix(lngRowHisHer, lngColData), vsData.TextMatrix(lngRowSignerName, lngColData), vsData.TextMatrix(lngRowSignerDesignation, lngColData), dtComExp, vsData.TextMatrix(lngRowTitle, lngColData), DateAndTime.DateValue(vsData.TextMatrix(lngRowSignedDate, lngColData)), dtAppearedDate, boolDefaultAppearedDate);
					}
					else
					{
						arUTAllLienDischargeNotice.InstancePtr.Init(FCConvert.CBool(strWS == "W"), vsData.TextMatrix(lngRowTName, lngColData), vsData.TextMatrix(lngRowMuni, lngColData), vsData.TextMatrix(lngRowCounty, lngColData), vsData.TextMatrix(lngRowHisHer, lngColData), vsData.TextMatrix(lngRowSignerName, lngColData), vsData.TextMatrix(lngRowSignerDesignation, lngColData), dtComExp, vsData.TextMatrix(lngRowTitle, lngColData), DateAndTime.DateValue(vsData.TextMatrix(lngRowSignedDate, lngColData)), dtAppearedDate, boolDefaultAppearedDate, true);
					}
					
					rsTemp.OpenRecordset("SELECT DischargeNeeded.* FROM DischargeNeeded INNER JOIN Bill ON DischargeNeeded.LienKey = Bill." + strWS + "LienRecordNumber WHERE ISNULL(Printed,0) = 0", modExtraModules.strUTDatabase);
					if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
					{
						rsArchive.Execute("DELETE FROM DischargeNeededArchive", modExtraModules.strUTDatabase);
						rsArchive.OpenRecordset("SELECT * FROM DischargeNeededArchive", modExtraModules.strUTDatabase);
						while (!rsTemp.EndOfFile())
						{
							// kgk rsTemp.Edit   ' can't update join
							// rsTemp.Fields("UpdatedDate") = Date
							// rsTemp.Fields("Printed") = True
							// rsTemp.Update
							rsUpdate.Execute("UPDATE DischargeNeeded SET UpdatedDate = '" + DateTime.Today.ToShortDateString() + "', Printed = 1 WHERE ID = " + rsTemp.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
							rsArchive.AddNew();
							rsArchive.Set_Fields("DatePaid", rsTemp.Get_Fields_DateTime("DatePaid"));
							rsArchive.Set_Fields("UpdatedDate", rsTemp.Get_Fields_DateTime("UpdatedDate"));
							rsArchive.Set_Fields("Teller", rsTemp.Get_Fields_String("Teller"));
							rsArchive.Set_Fields("LienKey", rsTemp.Get_Fields_Int32("LienKey"));
							rsArchive.Set_Fields("BillKey", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields_Int32("BillKey"))));
							rsArchive.Set_Fields("Printed", rsTemp.Get_Fields_Boolean("Printed"));
							rsArchive.Set_Fields("Batch", rsTemp.Get_Fields_Boolean("Batch"));
							rsArchive.Set_Fields("Canceled", rsTemp.Get_Fields_Boolean("Canceled"));
							// TODO: Field [DischargeNeeded.Book] not found!! (maybe it is an alias?)
							rsArchive.Set_Fields("Book", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("DischargeNeeded.Book"))));
							// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
							rsArchive.Set_Fields("Page", FCConvert.ToString(Conversion.Val(rsTemp.Get_Fields("Page"))));
							rsArchive.Update();
							rsTemp.MoveNext();
						}
					}
				}
				else
				{
					// loads the lien discharge report
					vsData.Select(0, 0);
					if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) != "")
					{
						dtComExp = DateAndTime.DateValue(vsData.TextMatrix(lngRowCommissionExpiration, lngColData));
					}
					else
					{
						dtComExp = DateTime.FromOADate(0);
					}
					// MAL@20070918
					if (Strings.InStr(1, vsData.TextMatrix(lngRowAppearedDate, lngColData), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) != "" && Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) != "/  /")
					{
						dtAppearedDate = DateAndTime.DateValue(vsData.TextMatrix(lngRowAppearedDate, lngColData));
					}
					else
					{
						dtAppearedDate = DateTime.FromOADate(0);
					}
					if (Strings.UCase(Strings.Trim(vsData.TextMatrix(lngRowAbatementDefault, lngColData))) == Strings.UCase(Strings.Trim(strAbate)))
					{
						boolAbateDefault = true;
					}
					else
					{
						boolAbateDefault = false;
					}
					arUTLienDischargeNotice.InstancePtr.Init(FCConvert.CBool(strWS == "W"), vsData.TextMatrix(lngRowTName, lngColData), vsData.TextMatrix(lngRowMuni, lngColData), vsData.TextMatrix(lngRowCounty, lngColData), vsData.TextMatrix(lngRowOwner1, lngColData), Strings.Trim(vsData.TextMatrix(lngRowOwner2, lngColData)), DateAndTime.DateValue(vsData.TextMatrix(lngRowPayDate, lngColData)), DateAndTime.DateValue(vsData.TextMatrix(lngRowFileDate, lngColData)), vsData.TextMatrix(lngRowBook, lngColData), vsData.TextMatrix(lngRowPage, lngColData), vsData.TextMatrix(lngRowHisHer, lngColData), vsData.TextMatrix(lngRowSignerName, lngColData), vsData.TextMatrix(lngRowSignerDesignation, lngColData), dtComExp, this.Modal, vsData.TextMatrix(lngRowMapLot, lngColData), vsData.TextMatrix(lngRowTitle, lngColData), lngTempAccountNumber, DateAndTime.DateValue(vsData.TextMatrix(lngRowSignedDate, lngColData)), dtAppearedDate, boolAbateDefault);
					// frmReportViewer.Init arUTLienDischargeNotice   'kk10312014 trout-1093  Preview popping up behind receipt form
					rsTemp.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + rsLien.Get_Fields_Int32("LienKey"), modExtraModules.strUTDatabase);
					if (!rsTemp.EndOfFile())
					{
						rsTemp.Edit();
						rsTemp.Set_Fields("UpdatedDate", DateTime.Today);
						rsTemp.Set_Fields("Printed", true);
						rsTemp.Update();
					}
					else
					{
						rsPayments.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + rsLien.Get_Fields_Int32("LienKey") + " AND Lien = 1 AND Code = 'P' ORDER BY RecordedTransactionDate DESC", "twut0000.vb1");
						if (rsPayments.EndOfFile() != true && rsPayments.BeginningOfFile() != true)
						{
							rsTemp.AddNew();
							rsTemp.Set_Fields("LienKey", rsLien.Get_Fields_Int32("LienKey"));
							rsTemp.Set_Fields("UpdatedDate", DateTime.Today);
							rsTemp.Set_Fields("Printed", true);
							rsTemp.Set_Fields("DatePaid", rsPayments.Get_Fields_DateTime("RecordedTransactionDate"));
							rsTemp.Set_Fields("Teller", rsPayments.Get_Fields_String("Teller"));
							rsTemp.Set_Fields("Batch", false);
							rsTemp.Set_Fields("Canceled", false);
							rsTemp.Update();
						}
					}
					// kgk  rsLien.Edit    ' Can't update a JOIN
					// rsLien.Fields("PrintedLDN") = True
					// rsLien.Update
					rsUpdate.Execute("UPDATE Lien SET PrintedLDN = 1 WHERE ID = " + rsLien.Get_Fields_Int32("LienKey"), modExtraModules.strUTDatabase);
				}
			}
		}

		private void LoadSettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will load the settings from the database
				clsDRWrapper rsCTRL = new clsDRWrapper();
				rsSettings.OpenRecordset("SELECT * FROM " + strWS + "Control_DischargeNotice", modExtraModules.strUTDatabase);
				if (!boolPrintAllSaved)
				{
					rsCTRL.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsLien.Get_Fields_Int32("RateKey"), modExtraModules.strUTDatabase);
				}
				if (lngRowAbatementDefault > 0)
				{
					if (boolAbateDefault)
					{
						vsData.TextMatrix(lngRowAbatementDefault, lngColData, strAbate);
					}
					else
					{
						vsData.TextMatrix(lngRowAbatementDefault, lngColData, strPayment);
					}
				}
				if (rsSettings.EndOfFile() != true && rsSettings.BeginningOfFile() != true)
				{
					// load all of the rows
					// Row - 1  'Treasurer Name
					vsData.TextMatrix(lngRowTName, lngColData, FCConvert.ToString(rsSettings.Get_Fields_String("Treasurer")));
					// Row - 3  'County
					// TODO: Check the table for the column [County] and replace with corresponding Get_Field method
					vsData.TextMatrix(lngRowCounty, lngColData, FCConvert.ToString(rsSettings.Get_Fields("County")));
					// Row - 10 'His/Her
					if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("Male")))
					{
						vsData.TextMatrix(lngRowHisHer, lngColData, "His");
					}
					else
					{
						vsData.TextMatrix(lngRowHisHer, lngColData, "Her");
					}
					// Signer's name - 2
					vsData.TextMatrix(lngRowSignerName, lngColData, FCConvert.ToString(rsSettings.Get_Fields_String("SignerName")));
					// Signer's Designation
					vsData.TextMatrix(lngRowSignerDesignation, lngColData, FCConvert.ToString(rsSettings.Get_Fields_String("SignerDesignation")));
					// Commission Expiration Date
					vsData.TextMatrix(lngRowCommissionExpiration, lngColData, FCConvert.ToString(rsSettings.Get_Fields_DateTime("CommissionExpiration")));
				}
				else
				{
					rsSettings.AddNew();
					// add defaults....
					rsSettings.Update(true);
					// load all of the rows
					rsSettings.OpenRecordset("SELECT * FROM " + strWS + "Control_LienProcess", modExtraModules.strUTDatabase);
					if (!rsSettings.EndOfFile())
					{
						// Row - 1  'Treasurer Name
						vsData.TextMatrix(lngColTitle, lngColData, FCConvert.ToString(rsSettings.Get_Fields_String("CollectorName")));
						// Row - 3  'County
						// TODO: Check the table for the column [County] and replace with corresponding Get_Field method
						vsData.TextMatrix(lngRowCounty, lngColData, FCConvert.ToString(rsSettings.Get_Fields("County")));
						// Row - 10 'His/Her
						vsData.TextMatrix(lngRowHisHer, lngColData, "Her");
						// Signer's name
						vsData.TextMatrix(lngRowSignerName, lngColData, FCConvert.ToString(rsSettings.Get_Fields_String("Signer")));
						// Signer's Designation
						vsData.TextMatrix(lngRowSignerDesignation, lngColData, FCConvert.ToString(rsSettings.Get_Fields_String("Designation")));
						// Commission Expiration Date
						vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
					}
					else
					{
						// Row - 1  'Treasurer Name
						vsData.TextMatrix(lngRowTName, lngColData, "");
						// Row - 3  'County
						vsData.TextMatrix(lngRowCounty, lngColData, "");
						// Row - 10 'His/Her
						vsData.TextMatrix(lngRowHisHer, lngColData, "Her");
						// Signer's name
						vsData.TextMatrix(lngRowSignerName, lngColData, "");
						// Signer's Designation
						vsData.TextMatrix(lngRowSignerDesignation, lngColData, "");
						// Commission Expiration Date
						vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
					}
				}
				// Row - 0  'titles
				vsData.TextMatrix(lngRowTitle, lngColData, "Treasurer");
				// Row - 2  'Muni Name
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					vsData.TextMatrix(lngRowMuni, lngColData, Strings.Trim(modGlobalConstants.Statics.gstrCityTown) + " of " + modGlobalConstants.Statics.MuniName);
				}
				else
				{
					vsData.TextMatrix(lngRowMuni, lngColData, modGlobalConstants.Statics.MuniName);
				}
				if (!boolPrintAllSaved)
				{
					// Row - 4  'Name1
					vsData.TextMatrix(lngRowOwner1, lngColData, FCConvert.ToString(rsLien.Get_Fields_String("OName")));
					// Row - 5  'Name2
					vsData.TextMatrix(lngRowOwner2, lngColData, FCConvert.ToString(rsLien.Get_Fields_String("OName2")));
					// Row - 6  'Payment Date and Signed Date
					if (dtPaymentDate.ToOADate() == 0)
					{
						vsData.TextMatrix(lngRowPayDate, lngColData, Strings.Format(DateTime.Now, "MM/dd/yyyy"));
						vsData.TextMatrix(lngRowSignedDate, lngColData, Strings.Format(DateTime.Now, "MM/dd/yyyy"));
					}
					else
					{
						vsData.TextMatrix(lngRowPayDate, lngColData, Strings.Format(dtPaymentDate, "MM/dd/yyyy"));
						vsData.TextMatrix(lngRowSignedDate, lngColData, Strings.Format(dtPaymentDate, "MM/dd/yyyy"));
					}
					// Row - 7  'Filing Date
					// use the date created field from the lien record
					if (!rsCTRL.IsFieldNull("BillDate"))
					{
						vsData.TextMatrix(lngRowFileDate, lngColData, Strings.Format(rsCTRL.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy"));
					}
					else
					{
						vsData.TextMatrix(lngRowFileDate, lngColData, "");
					}
					// Row - 8  'Book
					// TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
					vsData.TextMatrix(lngRowBook, lngColData, FCConvert.ToString(rsLien.Get_Fields("Book")));
					// Row - 9  'Page
					// TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
					vsData.TextMatrix(lngRowPage, lngColData, FCConvert.ToString(rsLien.Get_Fields("Page")));
					// Row - Map Lot
					if (FCConvert.ToString(rsLien.Get_Fields_String("MapLot")) != "")
					{
						vsData.TextMatrix(lngRowMapLot, lngColData, FCConvert.ToString(rsLien.Get_Fields_String("MapLot")));
					}
					else
					{
						// if the bill record does not have the map lot, then get it from the RE Master file
						rsCTRL.OpenRecordset("SELECT * FROM Master WHERE ID = " + rsLien.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
						if (!rsCTRL.EndOfFile())
						{
							vsData.TextMatrix(lngRowMapLot, lngColData, FCConvert.ToString(rsCTRL.Get_Fields_String("MapLot")));
						}
						else
						{
							vsData.TextMatrix(lngRowMapLot, lngColData, "");
						}
					}
					lngTempAccountNumber = modUTFunctions.GetUTAccountNumber_2(FCConvert.ToInt32(rsLien.Get_Fields_Int32("AccountKey")));
				}
				else
				{
					vsData.TextMatrix(lngRowSignedDate, lngColData, Strings.Format(DateTime.Now, "MM/dd/yyyy"));
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
			// this will keep the data and print the lien discharge notice
		}

		private void Format_Grid(bool boolReset)
		{
			FormatGrid(boolReset);
		}

		private void FormatGrid(bool boolReset = false)
		{
			int lngWid = 0;
			lngWid = vsData.WidthOriginal;
			if (boolPrintAllSaved)
			{
				if (boolReset)
				{
					vsData.Rows = 0;
					vsData.Rows = 10;
					// kk10292015 trouts-171     '9
				}
				vsData.ColWidth(lngColTitle, FCConvert.ToInt32(lngWid * 0.45));
				// title
				vsData.ColWidth(lngColHidden, 0);
				// hidden row
				vsData.ColWidth(lngColData, FCConvert.ToInt32(lngWid * 0.53));
				// data
				vsData.TextMatrix(lngRowTName, lngColTitle, "Treasurer Name");
				vsData.TextMatrix(lngRowMuni, lngColTitle, "Organization");
				vsData.TextMatrix(lngRowCounty, lngColTitle, "County");
				vsData.TextMatrix(lngRowSignedDate, lngColTitle, "Treasurer Signing Date");
				vsData.TextMatrix(lngRowHisHer, lngColTitle, "His / Her  (Treasurer)");
				vsData.TextMatrix(lngRowSignerName, lngColTitle, "Signer's Name");
				vsData.TextMatrix(lngRowTitle, lngColTitle, "Treasurer Title");
				vsData.TextMatrix(lngRowSignerDesignation, lngColTitle, "Signer's Designation");
				vsData.TextMatrix(lngRowCommissionExpiration, lngColTitle, "Commission Expiration");
				vsData.TextMatrix(lngRowAppearedDate, lngColTitle, "Appeared Date");
				// kk10292015 trouts-171
			}
			else
			{
				if (boolReset)
				{
					vsData.Rows = 0;
					vsData.Rows = 18;
				}
				vsData.ColWidth(lngColTitle, FCConvert.ToInt32(lngWid * 0.38));
				// title
				vsData.ColWidth(lngColHidden, 0);
				// hidden row
				vsData.ColWidth(lngColData, FCConvert.ToInt32(lngWid * 0.62));
				// data
				vsData.TextMatrix(lngRowTName, lngColTitle, "Treasurer Name");
				vsData.TextMatrix(lngRowMuni, lngColTitle, "Organization");
				vsData.TextMatrix(lngRowCounty, lngColTitle, "County");
				vsData.TextMatrix(lngRowOwner1, lngColTitle, "Owner Name");
				vsData.TextMatrix(lngRowOwner2, lngColTitle, "Second Owner");
				vsData.TextMatrix(lngRowPayDate, lngColTitle, "Payment Date");
				vsData.TextMatrix(lngRowFileDate, lngColTitle, "Filing Date");
				vsData.TextMatrix(lngRowSignedDate, lngColTitle, "Treasurer Signing Date");
				vsData.TextMatrix(lngRowBook, lngColTitle, "Book");
				vsData.TextMatrix(lngRowPage, lngColTitle, "Page");
				vsData.TextMatrix(lngRowMapLot, lngColTitle, "Map Lot");
				vsData.TextMatrix(lngRowAbatementDefault, lngColTitle, "How was the lien paid?");
				vsData.TextMatrix(lngRowHisHer, lngColTitle, "His / Her  (Treasurer)");
				vsData.TextMatrix(lngRowSignerName, lngColTitle, "Signer's Name");
				vsData.TextMatrix(lngRowTitle, lngColTitle, "Treasurer Title");
				vsData.TextMatrix(lngRowSignerDesignation, lngColTitle, "Signer's Designation");
				vsData.TextMatrix(lngRowCommissionExpiration, lngColTitle, "Commission Expiration");
				vsData.TextMatrix(lngRowAppearedDate, lngColTitle, "Appeared Date");
				// MAL@20070918
			}
			// set the grid height
			//if ((vsData.Rows * vsData.RowHeight(0)) + 80 > (fraVariables.Height * 0.9))
			//{
			//    vsData.Height = FCConvert.ToInt32((fraVariables.Height * 0.9));
			//    vsData.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//    vsData.Height = (vsData.Rows * vsData.RowHeight(0)) + 80;
			//    vsData.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}

		private void vsData_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsData.EditMask = string.Empty;
			vsData.ComboList = "";
			if (vsData.Row == lngRowPayDate || vsData.Row == lngRowFileDate || vsData.Row == lngRowCommissionExpiration || vsData.Row == lngRowSignedDate || vsData.Row == lngRowAppearedDate)
			{
				vsData.EditMask = "##/##/####";
			}
			else if (vsData.Row == lngRowAbatementDefault)
			{
				vsData.ComboList = "0;" + strPayment + "|1;" + strAbate;
			}
		}

		private void vsData_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			FCGrid grid = sender as FCGrid;
			int keyAscii = Strings.Asc(e.KeyChar);
			if (grid.Row == lngRowPayDate || grid.Row == lngRowFileDate || grid.Row == lngRowBook || grid.Row == lngRowPage || grid.Row == lngRowCommissionExpiration || grid.Row == lngRowAppearedDate)
			{
				// only allow numbers
				if ((keyAscii >= FCConvert.ToInt32(Keys.D0)) && keyAscii <= FCConvert.ToInt32(Keys.D9) || (keyAscii == FCConvert.ToInt32(Keys.Back)) || (keyAscii == FCConvert.ToInt32(Keys.Decimal)) || (keyAscii == 46))
				{
					// do nothing
				}
				else
				{
					keyAscii = 0;
				}
			}
			else
			{
				// default: 
			}
		}

		private void vsData_RowColChange(object sender, System.EventArgs e)
		{
			if (vsData.Col == lngColData)
			{
				if (vsData.Row == lngRowTName)
				{
					// Treasurer Name
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowMuni)
				{
					// Muni Name
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowCounty)
				{
					// County
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowOwner1)
				{
					// Name1
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowOwner2)
				{
					// Name2
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowPayDate)
				{
					// Payment Date
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowSignedDate)
				{
					// Treas Signing Date
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowFileDate)
				{
					// Filing Date
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowBook)
				{
					// Book
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowPage)
				{
					// Page
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowHisHer)
				{
					// His/Her
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowSignerName || vsData.Row == lngRowTitle)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowSignerDesignation)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowCommissionExpiration)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowAbatementDefault)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowAppearedDate)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
			}
			else
			{
				vsData.Editable = FCGrid.EditableSettings.flexEDNone;
				vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private bool ValidateAnswers()
		{
			bool ValidateAnswers = false;
			ValidateAnswers = true;
			vsData.Select(0, 1);
			// Row - 0  'Treasurer Name
			if (Strings.Trim(vsData.TextMatrix(lngRowTName, lngColData)) == "")
			{
				ValidateAnswers = false;
				MessageBox.Show("Please enter the Treasurer's name.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				vsData.Select(lngRowTName, lngColData);
				return ValidateAnswers;
			}
			// Row - 1  'Muni Name
			if (Strings.Trim(vsData.TextMatrix(lngRowMuni, lngColData)) == "")
			{
				ValidateAnswers = false;
				MessageBox.Show("Please enter the Municipality name.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				vsData.Select(lngRowMuni, lngColData);
				return ValidateAnswers;
			}
			// Row - 2  'County
			if (Strings.Trim(vsData.TextMatrix(lngRowCounty, lngColData)) == "")
			{
				ValidateAnswers = false;
				MessageBox.Show("Please enter the County name.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				vsData.Select(lngRowCounty, lngColData);
				return ValidateAnswers;
			}
			// Row - 5  'Payment Date
			if (Strings.Trim(vsData.TextMatrix(lngRowSignedDate, lngColData)) == "")
			{
				ValidateAnswers = false;
				MessageBox.Show("Please enter a treasurer signing date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				vsData.Select(lngRowSignedDate, lngColData);
				return ValidateAnswers;
			}
			else
			{
				if (!Information.IsDate(vsData.TextMatrix(lngRowSignedDate, lngColData)))
				{
					ValidateAnswers = false;
					MessageBox.Show("Please enter a valid treasurer signing date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsData.Select(lngRowSignedDate, lngColData);
					return ValidateAnswers;
				}
			}
			if (!boolPrintAllSaved)
			{
				// Row - 3  'Name1
				if (Strings.Trim(vsData.TextMatrix(lngRowOwner1, lngColData)) == "")
				{
					ValidateAnswers = false;
					MessageBox.Show("Please enter the Owner's name.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsData.Select(lngRowOwner1, lngColData);
					return ValidateAnswers;
				}
				// Row - 4  'Name2
				// no validations
				// Row - 5  'Payment Date
				if (Strings.Trim(vsData.TextMatrix(lngRowPayDate, lngColData)) == "")
				{
					ValidateAnswers = false;
					MessageBox.Show("Please enter a payment date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsData.Select(lngRowPayDate, lngColData);
					return ValidateAnswers;
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowPayDate, lngColData)))
					{
						ValidateAnswers = false;
						MessageBox.Show("Please enter a valid payment date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vsData.Select(lngRowPayDate, lngColData);
						return ValidateAnswers;
					}
				}
				// Row - 6  'Filing Date
				if (Strings.Trim(vsData.TextMatrix(lngRowFileDate, lngColData)) == "")
				{
					ValidateAnswers = false;
					MessageBox.Show("Please enter a lien filing date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsData.Select(lngRowFileDate, lngColData);
					return ValidateAnswers;
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowFileDate, lngColData)))
					{
						ValidateAnswers = false;
						MessageBox.Show("Please enter a valid lien filing date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vsData.Select(lngRowFileDate, lngColData);
						return ValidateAnswers;
					}
				}
				// Row - 7  'Book
				// Row - 8  'Page
			}
			else
			{
				// kk10292015 trouts-171  Make UT Batch LDN process like CL
				// Appeared Date
				if (Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) == "" || Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) == "__/__/____" || Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) == "/  /")
				{
					//switch (MessageBox.Show("Would you like the appeared date to default to the payment date?", null, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
					// FC:FINAL:VGE - #i931 Explicit caption for the popup.
					switch (MessageBox.Show("Would you like the appeared date to default to the payment date?", "Utility Billing", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
					{
						case DialogResult.Yes:
							{
								boolDefaultAppearedDate = true;
								break;
							}
						case DialogResult.No:
							{
								boolDefaultAppearedDate = false;
								break;
							}
						case DialogResult.Cancel:
							{
								boolDefaultAppearedDate = false;
								ValidateAnswers = false;
								break;
							}
					}
					//end switch
				}
				else
				{
					// force this date
				}
			}
			// Row - 9  'His/Her
			// Row - 10 'Signer Name
			// If Trim(.TextMatrix(lngRowSignerName, lngColData)) = "" Then
			// ValidateAnswers = False
			// MsgBox "Please enter the Signer's name.", vbExclamation, "Validation"
			// .Select lngRowSignerName, lngColData
			// Exit Function
			// End If
			// allow these to be blank
			// Row        'Signer's Title
			// If Trim(.TextMatrix(lngRowTitle, lngColData)) = "" Then
			// ValidateAnswers = False
			// MsgBox "Please enter the Signer's title.  (Treasurer)", vbExclamation, "Validation"
			// .Select lngRowTitle, lngColData
			// Exit Function
			// End If
			// 
			// Row - 12 'Signer's Designation
			// If Trim(.TextMatrix(lngRowSignerDesignation, lngColData)) = "" Then
			// ValidateAnswers = False
			// MsgBox "Please enter the Signer's designation.", vbExclamation, "Validation"
			// .Select lngRowSignerDesignation, lngColData
			// Exit Function
			// End If
			// 
			// Row - 13 ' Commistion expiration date
			if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) == "")
			{
				// allow it to be blank
				// ValidateAnswers = False
				// MsgBox "Please enter a Commission Expiration date.", vbExclamation, "Validation"
				// .Select lngRowCommissionExpiration, lngColData
				// Exit Function
			}
			else
			{
				if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) == "/  /")
				{
					vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
					// change it back to blank
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)))
					{
						if (Conversion.Val(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) == 0)
						{
							vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
							// change it back to blank
						}
						else
						{
							ValidateAnswers = false;
							MessageBox.Show("Please enter a valid Commission Expiration date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vsData.Select(lngRowCommissionExpiration, lngColData);
							return ValidateAnswers;
						}
					}
				}
			}
			return ValidateAnswers;
		}

		private void vsData_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsData.GetFlexRowIndex(e.RowIndex);
			int col = vsData.GetFlexColIndex(e.ColumnIndex);
			if (row == lngRowCommissionExpiration)
			{
				if (vsData.EditText == "  /  /    " || vsData.EditText == "__/__/____")
				{
					vsData.EditText = "";
					vsData.TextMatrix(row, col, "");
					vsData.EditMask = "";
				}
			}
		}
	}
}
