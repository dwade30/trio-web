﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;

#if TWCR0000
using TWCR0000;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;
using modStatusPayments = TWUT0000.modMain;
#endif
using System.Diagnostics;

namespace Global
{
	public class modUTFunctions
	{
		public struct UTPayment
		{
			// these are the fields that each payment needs to save
			public int ID;
			public int Account;
			public int Year;
			public int BillNumber;
			public int BillKey;
			public int CHGINTNumber;
			public DateTime CHGINTDate;
			// vbPorter upgrade warning: ActualSystemDate As DateTime	OnWrite(short, DateTime)
			public DateTime ActualSystemDate;
			// vbPorter upgrade warning: EffectiveInterestDate As DateTime	OnWrite(DateTime, short)
			public DateTime EffectiveInterestDate;
			// vbPorter upgrade warning: RecordedTransactionDate As DateTime	OnWrite(DateTime, short)
			public DateTime RecordedTransactionDate;
			public string Teller;
			public string Reference;
			// vbPorter upgrade warning: Period As string	OnWrite(string, short)
			public string Period;
			public string Service;
			public string Code;
			// vbPorter upgrade warning: ReceiptNumber As string	OnWrite(short, string)
			public string ReceiptNumber;
			// vbPorter upgrade warning: Principal As Decimal	OnWrite(short, Decimal, double)	OnReadFCConvert.ToDouble(
			public Decimal Principal;
			// vbPorter upgrade warning: PreLienInterest As Decimal	OnWrite(short, Decimal, double)	OnReadFCConvert.ToDouble(
			public Decimal PreLienInterest;
			// vbPorter upgrade warning: CurrentInterest As Decimal	OnWrite(short, Decimal, double)	OnRead(double, Decimal)
			public Decimal CurrentInterest;
			// vbPorter upgrade warning: Tax As Decimal	OnWrite(short, Decimal, double)	OnReadFCConvert.ToDouble(
			public Decimal Tax;
			// vbPorter upgrade warning: LienCost As Decimal	OnWrite(short, Decimal, double)	OnReadFCConvert.ToDouble(
			public Decimal LienCost;
			// vbPorter upgrade warning: TransNumber As string	OnWriteFCConvert.ToInt16(
			public string TransNumber;
			public string PaidBy;
			public string Comments;
			public string CashDrawer;
			public string GeneralLedger;
			public string BudgetaryAccountNumber;
			public string BillCode;
			public int ReversalID;
			public int ResCode;
			public bool Reversal;
			public DateTime SetEIDate;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			public UTPayment(int unusedParam)
			{
				this.Account = 0;
				this.ActualSystemDate = DateTime.FromOADate(0);
				this.BillCode = string.Empty;
				this.BillKey = 0;
				this.BillNumber = 0;
				this.BudgetaryAccountNumber = string.Empty;
				this.CashDrawer = string.Empty;
				this.CHGINTDate = DateTime.FromOADate(0);
				this.CHGINTNumber = 0;
				this.Code = string.Empty;
				this.Comments = string.Empty;
				this.CurrentInterest = 0;
				this.EffectiveInterestDate = DateTime.FromOADate(0);
				this.GeneralLedger = string.Empty;
				this.ID = 0;
				this.LienCost = 0;
				this.PaidBy = string.Empty;
				this.Period = string.Empty;
				this.PreLienInterest = 0;
				this.Principal = 0;
				this.ReceiptNumber = string.Empty;
				this.RecordedTransactionDate = DateTime.FromOADate(0);
				this.Reference = string.Empty;
				this.ResCode = 0;
				//FC:FINAL:DDU:#i1047 - fixed reversalID to -1 as in original
				this.ReversalID = -1;
				this.Service = string.Empty;
				this.Tax = 0;
				this.Teller = string.Empty;
				this.TransNumber = string.Empty;
				this.Year = 0;
				this.Reversal = false;
				this.SetEIDate = DateTime.FromOADate(0);
			}
		};
		// DJW@11/17/2010 Changed array size from 1000 to 20000
		public const int MAX_UTPAYMENTS = 20000;
		// this will hold the payments from the payments screen
		// this will be used to store abatements by period in frmUTCLStatus, frmTaxService, Reminder Notices, and Bills
		public struct UTBillPeriods
		{
			public double Per1;
			public double Per2;
			public double Per3;
			public double Per4;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public UTBillPeriods(int unusedParam)
			{
				this.Per1 = 0;
				this.Per2 = 0;
				this.Per3 = 0;
				this.Per4 = 0;
			}
		};

		public struct PerdiemInfo
		{
			public short intBill;
			public double dblPerDiem;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public PerdiemInfo(int unusedParam)
			{
				this.intBill = 0;
				this.dblPerDiem = 0;
			}
		};

		//public static bool CreateUTPaymentRecord(int lngIndex, ref clsDRWrapper rsCPy, ref int lngRow)
		//{
		//	bool CreateUTPaymentRecord = false;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		clsDRWrapper rsTemp = new clsDRWrapper();
		//		clsDRWrapper rsCreatePy = new clsDRWrapper();
		//		// vbPorter upgrade warning: intTemp As short, int --> As DialogResult
		//		DialogResult intTemp = DialogResult.None;
		//		int lngCount;
		//		bool boolFound;
		//		bool boolPaid30DN;
		//		bool boolPaidLien;
		//		double dblXInt = 0;
		//		// vbPorter upgrade warning: dblAmt As double	OnWrite(double, Decimal)
		//		double dblAmt = 0;
		//		bool blnPrompted = false;
		//		boolPaidLien = false;
		//		boolPaid30DN = false;
		//		// Doevents
		//		// this will save one payment record to the database
		//		if (MAX_UTPAYMENTS > lngIndex && lngIndex > 0)
		//		{
		//			// checks for out of bounds array call
		//			// MAL@20080709 ; Tracker Reference: 14159
		//			if (lngIndex > 1)
		//			{
		//				// not the first record
		//				if (Statics.UTPaymentArray[lngIndex - 1].BillNumber != Statics.UTPaymentArray[lngIndex].BillNumber)
		//				{
		//					blnPrompted = false;
		//				}
		//				else
		//				{
		//					blnPrompted = true;
		//				}
		//			}
		//			else
		//			{
		//				blnPrompted = false;
		//			}
		//			// If .BillNumber = 0 Then
		//			// CreateUTPaymentRecord = False
		//			// Exit Function
		//			// End If
		//			rsCPy.AddNew();
		//			rsCPy.Update();
		//			Statics.UTPaymentArray[lngIndex].ID = rsCPy.Get_Fields_Int32("ID");
		//			frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRow, frmUTCLStatus.InstancePtr.lngPayGridColKeyNumber, FCConvert.ToString(Statics.UTPaymentArray[lngIndex].ID));
		//			rsCPy.Set_Fields("AccountKey", Statics.UTPaymentArray[lngIndex].Account);
		//			rsCPy.Set_Fields("Year", Statics.UTPaymentArray[lngIndex].Year);
		//			rsCPy.Set_Fields("BillNumber", Statics.UTPaymentArray[lngIndex].BillNumber);
		//			rsCPy.Set_Fields("Reversal", Statics.UTPaymentArray[lngIndex].Reversal);
		//			//kk11022016 trout-1225
		//			rsCPy.Set_Fields("SetEIDate", Statics.UTPaymentArray[lngIndex].SetEIDate);
		//			rsCPy.Set_Fields("BillKey", Statics.UTPaymentArray[lngIndex].BillKey);
		//			if (!modExtraModules.IsThisCR())
		//			{
		//				// this checks to see where UT is shelling to
		//				// if it is not going to shell back to CR
		//				// then update all of the records and finish the transaction
		//				rsCPy.Set_Fields("ReceiptNumber", 0);
		//				if (Statics.UTPaymentArray[lngIndex].BillCode != "L")
		//				{
		//					// regular billing (RE or PP)
		//					boolPaidLien = false;
		//					// MAL@20080205
		//					rsTemp.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].BillKey), modExtraModules.strUTDatabase);
		//					// rsTemp.OpenRecordset "SELECT * FROM (" & strUTCurrentAccountBills & ") WHERE Bill = " & .BillKey, strUTDatabase
		//					if (rsTemp.RecordCount() != 0)
		//					{
		//						rsTemp.Edit();
		//						rsTemp.Set_Fields(Statics.UTPaymentArray[lngIndex].Service + "IntPaid", (rsTemp.Get_Fields_Decimal(Statics.UTPaymentArray[lngIndex].Service + "IntPaid") + Statics.UTPaymentArray[lngIndex].CurrentInterest));
		//						// rsTemp.Fields("PLIPaid") = (rsTemp.Fields("PLIPaid") + .PreLienInterest)
		//						rsTemp.Set_Fields(Statics.UTPaymentArray[lngIndex].Service + "CostPaid", (rsTemp.Get_Fields_Decimal(Statics.UTPaymentArray[lngIndex].Service + "CostPaid") + Statics.UTPaymentArray[lngIndex].LienCost));
		//						rsTemp.Set_Fields(Statics.UTPaymentArray[lngIndex].Service + "TaxPaid", (rsTemp.Get_Fields_Decimal(Statics.UTPaymentArray[lngIndex].Service + "TaxPaid") + Statics.UTPaymentArray[lngIndex].Tax));
		//						rsTemp.Set_Fields(Statics.UTPaymentArray[lngIndex].Service + "PrinPaid", (rsTemp.Get_Fields_Decimal(Statics.UTPaymentArray[lngIndex].Service + "PrinPaid") + Statics.UTPaymentArray[lngIndex].Principal));
		//						if (!Statics.UTPaymentArray[lngIndex].Reversal)
		//						{
		//							rsTemp.Set_Fields(Statics.UTPaymentArray[lngIndex].Service + "IntPaidDate", Statics.UTEffectiveDate);
		//							// this is added 11/01/05
		//						}
		//						if (!rsTemp.Update())
		//						{
		//							MessageBox.Show("Error updating bill record #" + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].BillKey) + ".", "Error In Create Payment Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//							CreateUTPaymentRecord = false;
		//							goto END_OF_PAYMENT;
		//						}
		//					}
		//					else
		//					{
		//						CreateUTPaymentRecord = false;
		//						goto END_OF_PAYMENT;
		//					}
		//				}
		//				else
		//				{
		//					// lien
		//					rsTemp.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].BillKey), modExtraModules.strUTDatabase);
		//					if (rsTemp.RecordCount() != 0)
		//					{
		//						rsTemp.Edit();
		//						// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//						rsTemp.Set_Fields("IntPaid", (rsTemp.Get_Fields_Decimal("IntPaid") + Statics.UTPaymentArray[lngIndex].CurrentInterest));
		//						// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//						rsTemp.Set_Fields("PLIPaid", (rsTemp.Get_Fields_Decimal("PLIPaid") + Statics.UTPaymentArray[lngIndex].PreLienInterest));
		//						rsTemp.Set_Fields("CostPaid", (rsTemp.Get_Fields_Decimal("CostPaid") + Statics.UTPaymentArray[lngIndex].LienCost));
		//						// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//						rsTemp.Set_Fields("TaxPaid", (rsTemp.Get_Fields_Decimal("TaxPaid") + Statics.UTPaymentArray[lngIndex].Tax));
		//						// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//						rsTemp.Set_Fields("PrinPaid", (rsTemp.Get_Fields_Decimal("PrinPaid") + Statics.UTPaymentArray[lngIndex].Principal));
		//						if (!Statics.UTPaymentArray[lngIndex].Reversal)//06.29.17 TROUT-1222 kjr don't update if this is a reversal pymt
		//						{
		//							rsTemp.Set_Fields(Statics.UTPaymentArray[lngIndex].Service + "IntPaidDate", Statics.UTEffectiveDate);
		//							// this is added 07/10/03
		//						}
		//						if (!rsTemp.Update())
		//						{
		//							MessageBox.Show("Error updating lien record #" + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].BillKey) + ".", "Error In Create Payment Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//							CreateUTPaymentRecord = false;
		//							goto END_OF_PAYMENT;
		//						}
		//					}
		//					else
		//					{
		//						CreateUTPaymentRecord = false;
		//						goto END_OF_PAYMENT;
		//					}
		//				}
		//				// check for tax club
		//				if (Statics.UTPaymentArray[lngIndex].Code == "U")
		//				{
		//					rsTemp.OpenRecordset("SELECT * FROM TaxClub", modExtraModules.strUTDatabase);
		//					rsTemp.AddNew();
		//					rsTemp.Set_Fields("BillKey", Statics.UTPaymentArray[lngIndex].BillKey);
		//					rsTemp.Set_Fields("Amount", Statics.UTPaymentArray[lngIndex].Principal);
		//					rsTemp.Set_Fields("TaxClubDate", DateTime.Today);
		//					rsTemp.Set_Fields("PaymentKey", rsCPy.Get_Fields_Int32("ID"));
		//					if (!rsTemp.Update())
		//					{
		//						MessageBox.Show("Error updating tax club for bill record #" + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].BillKey) + ".", "Error In Create Payment Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//						CreateUTPaymentRecord = false;
		//						goto END_OF_PAYMENT;
		//					}
		//				}
		//			}
		//			else
		//			{
		//				rsCPy.Set_Fields("ReceiptNumber", -1);
		//			}
		//			// check for a reversal, create the CHGINT line if needed and adjust the arrax index so it does not get created a second time
		//			//XXXXXXXX kk02062017           For lngCount = 1 To MAX_UTPAYMENTS
		//			for (lngCount = MAX_UTPAYMENTS; lngCount >= 1; lngCount--)
		//			{
		//				if (Statics.UTPaymentArray[lngCount].ReversalID != -1)
		//				{
		//					//xxxxxx'kk01242017 trout-1225  Make this work more like CL
		//					//if (UTPaymentArray[lngCount].ReversalID == lngIndex)
		//					//{ // And blnCreatedReverseInterest = False
		//					// rsCreatePy.OpenRecordset "SELECT * FROM (" & strUTCurrentAccountPayments & ") WHERE Account = 0", strUTDatabase    'MAL@20071026
		//					//rsCreatePy.OpenRecordset("SELECT * FROM (" + Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE BillNumber = 0", modExtraModules.strUTDatabase);
		//					//CreateUTCHGINTPaymentRecord_20(FCConvert.ToInt16(lngCount), rsCreatePy, true);
		//					rsCreatePy.OpenRecordset("SELECT * FROM (" + Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE ID = -1", modExtraModules.strUTDatabase);
		//					//kk04132016 trout-1224
		//					//XXXXX    CreateUTCHGINTPaymentRecord CInt(lngCount), rsCreatePy, True
		//					//XXXXX kk02062017 trout-1225  Make this work like CL
		//					AddUTCHGINTToList(frmUTCLStatus.InstancePtr.vsPayments.Rows, FCConvert.ToBoolean(Statics.UTPaymentArray[lngCount].Service == "W"), Statics.UTPaymentArray[lngCount].ReversalID, true, true, FCConvert.ToInt32(lngCount), Statics.UTPaymentArray[lngCount].BillKey, Statics.UTPaymentArray[lngCount].BillNumber);
		//					//trout-1222/1225 kjr 7.31.17
		//					// MAL@20080228: Check if Interest reversal line
		//					// Tracker Reference: 12219
		//					// If UTPaymentArray(lngCount).Reference = "Interest" Then
		//					// blnCreatedReverseInterest = True
		//					// End If
		//					//kk02062017 trout-1225
		//					//rsCreatePy.MoveFirst();
		//					//UTPaymentArray[lngIndex].CHGINTNumber = FCConvert.ToInt32(rsCreatePy.Get_Fields("ID"));
		//					//boolFound = true;
		//					//break;
		//					//}
		//				}
		//			}
		//			rsCPy.Set_Fields("CHGINTNumber", Statics.UTPaymentArray[lngIndex].CHGINTNumber);
		//			//FC:FINAL:DDU:#i1036 - prevent date to go too low for DB update
		//			if (Statics.UTPaymentArray[lngIndex].CHGINTDate == DateTime.MinValue)
		//			{
		//				Statics.UTPaymentArray[lngIndex].CHGINTDate = DateTime.FromOADate(0);
		//			}
		//			rsCPy.Set_Fields("CHGINTDate", Statics.UTPaymentArray[lngIndex].CHGINTDate);
		//			// kk 10032013 trouts-39; Need CHGINTDate to be zero, not null
		//			rsCPy.Set_Fields("ActualSystemDate", Statics.UTPaymentArray[lngIndex].ActualSystemDate);
		//			rsCPy.Set_Fields("EffectiveInterestDate", Statics.UTPaymentArray[lngIndex].EffectiveInterestDate);
		//			rsCPy.Set_Fields("RecordedTransactionDate", Statics.UTPaymentArray[lngIndex].RecordedTransactionDate);
		//			rsCPy.Set_Fields("Teller", Strings.Trim(Statics.UTPaymentArray[lngIndex].Teller + " "));
		//			rsCPy.Set_Fields("Reference", Strings.Trim(Statics.UTPaymentArray[lngIndex].Reference + " "));
		//			rsCPy.Set_Fields("Period", Strings.Trim(Statics.UTPaymentArray[lngIndex].Period + " "));
		//			rsCPy.Set_Fields("Code", Strings.Trim(Statics.UTPaymentArray[lngIndex].Code + " "));
		//			rsCPy.Set_Fields("Principal", Statics.UTPaymentArray[lngIndex].Principal);
		//			rsCPy.Set_Fields("PreLienInterest", Statics.UTPaymentArray[lngIndex].PreLienInterest);
		//			rsCPy.Set_Fields("CurrentInterest", Statics.UTPaymentArray[lngIndex].CurrentInterest);
		//			rsCPy.Set_Fields("Tax", Statics.UTPaymentArray[lngIndex].Tax);
		//			rsCPy.Set_Fields("LienCost", Statics.UTPaymentArray[lngIndex].LienCost);
		//			rsCPy.Set_Fields("Service", Statics.UTPaymentArray[lngIndex].Service);
		//			rsCPy.Set_Fields("TransNumber", Statics.UTPaymentArray[lngIndex].TransNumber);
		//			rsCPy.Set_Fields("PaidBy", Statics.UTPaymentArray[lngIndex].PaidBy);
		//			rsCPy.Set_Fields("Comments", Statics.UTPaymentArray[lngIndex].Comments);
		//			rsCPy.Set_Fields("CashDrawer", Statics.UTPaymentArray[lngIndex].CashDrawer);
		//			rsCPy.Set_Fields("GeneralLedger", Statics.UTPaymentArray[lngIndex].GeneralLedger);
		//			rsCPy.Set_Fields("BudgetaryAccountNumber", Statics.UTPaymentArray[lngIndex].BudgetaryAccountNumber);
		//			if (Statics.UTPaymentArray[lngIndex].BillCode == "L")
		//			{
		//				rsCPy.Set_Fields("Lien", true);
		//			}
		//			else
		//			{
		//				rsCPy.Set_Fields("Lien", false);
		//			}
		//			// rsCPy.Fields("BillCode") = CBool(.BillCode = "L")
		//			if (!rsCPy.Update())
		//			{
		//				MessageBox.Show("Error creating payment record for account #" + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].Account) + ".", "Error In Create Payment Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//				CreateUTPaymentRecord = false;
		//				goto END_OF_PAYMENT;
		//			}
		//			CreateUTPaymentRecord = true;
		//			// check to see if this lien has been paid off yet
		//			// if so, give the user an option to print the Lien Discharge notice
		//			boolPaidLien = false;
		//			boolPaid30DN = false;
		//			if (Statics.UTPaymentArray[lngIndex].BillCode == "L" && Statics.UTPaymentArray[lngIndex].Reference != "CHGINT")
		//			{
		//				// this is a lien
		//				rsTemp.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].BillKey), modExtraModules.strUTDatabase);
		//				if (rsTemp.RecordCount() != 0)
		//				{
		//					// MAL@20080610: Changed to take current uncharged interest into account to determine payoff
		//					// Tracker Reference: 14159
		//					dblAmt = modUTCalculations.CalculateAccountUTLien(rsTemp, Statics.UTPaymentArray[lngIndex].EffectiveInterestDate, ref dblXInt, FCConvert.CBool(Statics.UTPaymentArray[lngIndex].Service == "W"));
		//					// DJW@20090306: Added this section to subtract the amounts made on this payment from the total because when coming from CR we don't update the lien with payment amounts until back in CR
		//					// Tracker Reference: 16496
		//					if (modExtraModules.IsThisCR())
		//					{
		//						dblAmt -= FCConvert.ToDouble(Statics.UTPaymentArray[lngIndex].Principal + Statics.UTPaymentArray[lngIndex].PreLienInterest + Statics.UTPaymentArray[lngIndex].CurrentInterest + Statics.UTPaymentArray[lngIndex].LienCost + Statics.UTPaymentArray[lngIndex].Tax);
		//					}
		//					if (dblAmt <= 0 && (Statics.UTPaymentArray[lngIndex].Principal + Statics.UTPaymentArray[lngIndex].PreLienInterest + Statics.UTPaymentArray[lngIndex].CurrentInterest + Statics.UTPaymentArray[lngIndex].LienCost + Statics.UTPaymentArray[lngIndex].Tax) > 0)
		//					{
		//						// If (rsTemp.Fields("Principal") - rsTemp.Fields("PrinPaid")) + (rsTemp.Fields("Interest") - rsTemp.Fields("IntAdded") + dblXInt - rsTemp.Fields("IntPaid") - rsTemp.Fields("PLIPaid")) - rsTemp.Fields("MaturityFee") + (rsTemp.Fields("Costs") - rsTemp.Fields("CostPaid")) - (.Principal + .PreLienInterest + .CurrentInterest + .LienCost) <= 0 And (.Principal + .PreLienInterest + .CurrentInterest + .LienCost) > 0 Then
		//						// If (rsTemp.Fields("Principal") - rsTemp.Fields("PrinPaid")) + (rsTemp.Fields("Interest") - rsTemp.Fields("IntAdded") - rsTemp.Fields("IntPaid") - rsTemp.Fields("PLIPaid")) - rsTemp.Fields("MaturityFee") + (rsTemp.Fields("Costs") - rsTemp.Fields("CostPaid")) - (.Principal + .PreLienInterest + .CurrentInterest + .LienCost) <= 0 And (.Principal + .PreLienInterest + .CurrentInterest + .LienCost) > 0 Then
		//						boolPaidLien = true;
		//					}
		//				}
		//			}
		//			else if (Statics.UTPaymentArray[lngIndex].Reference != "CHGINT" && Statics.UTPaymentArray[lngIndex].BillCode != "L")
		//			{
		//				rsTemp.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].BillKey), modExtraModules.strUTDatabase);
		//				if (rsTemp.RecordCount() != 0)
		//				{
		//					if (FCConvert.ToInt32(rsTemp.Get_Fields(Statics.UTPaymentArray[lngIndex].Service + "LienStatusEligibility")) > 2 && frmUTCLStatus.InstancePtr.boolCheckOnlyOldestBill)
		//					{
		//						// MAL@20080610: Change to take uncharged current interest into account to determine payoff
		//						// Tracker Reference: 14159
		//						dblAmt = modUTCalculations.CalculateAccountUT(rsTemp, Statics.UTPaymentArray[lngIndex].EffectiveInterestDate, ref dblXInt, FCConvert.CBool(Statics.UTPaymentArray[lngIndex].Service == "W"));
		//						// DJW@20090306: Added this section to subtract the amounts made on this payment from the total because when coming from CR we don't update the lien with payment amounts until back in CR
		//						// Tracker Reference: 16496
		//						if (modExtraModules.IsThisCR())
		//						{
		//							dblAmt -= FCConvert.ToDouble(Statics.UTPaymentArray[lngIndex].Principal + Statics.UTPaymentArray[lngIndex].PreLienInterest + Statics.UTPaymentArray[lngIndex].CurrentInterest + Statics.UTPaymentArray[lngIndex].LienCost + Statics.UTPaymentArray[lngIndex].Tax);
		//						}
		//						if (dblAmt <= 0)
		//						{
		//							// If (rsTemp.Fields(.Service & "PrinOwed") - rsTemp.Fields(.Service & "PrinPaid")) + (rsTemp.Fields(.Service & "IntOwed") - rsTemp.Fields(.Service & "IntAdded") + dblXInt - rsTemp.Fields(.Service & "IntPaid")) + (rsTemp.Fields(.Service & "CostOwed") - rsTemp.Fields(.Service & "CostAdded") - rsTemp.Fields(.Service & "CostPaid")) - (.Principal + .PreLienInterest + .CurrentInterest + .LienCost) <= 0 Then
		//							// If (rsTemp.Fields(.Service & "PrinOwed") - rsTemp.Fields(.Service & "PrinPaid")) + (rsTemp.Fields(.Service & "IntOwed") - rsTemp.Fields(.Service & "IntAdded") - rsTemp.Fields(.Service & "IntPaid")) + (rsTemp.Fields(.Service & "CostOwed") - rsTemp.Fields(.Service & "CostAdded") - rsTemp.Fields(.Service & "CostPaid")) - (.Principal + .PreLienInterest + .CurrentInterest + .LienCost) <= 0 Then
		//							boolPaid30DN = true;
		//						}
		//					}
		//				}
		//			}
		//			if (boolPaidLien)
		//			{
		//				if (!modExtraModules.IsThisCR())
		//				{
		//					// kk10312014  trout-1093 If in CR, wait until Receipt is completed to do this
		//					// this will ask the user if they want to proceed with the lien discharge notice or save it until later
		//					if (frmUTCLStatus.InstancePtr.intLDNAnswer == 0)
		//					{
		//						if (!blnPrompted)
		//						{
		//							// kk04012015 trocr-442 Change default response to No
		//							intTemp = MessageBox.Show("Would you like to print the Lien Discharge Notice now?" + "\r\n" + "\r\n" + "Yes" + "\t" + "- Print Notice" + "\r\n" + "No" + "\t" + "- Save in Discharge Needed file", "Lien Discharge Notice", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
		//							// & vbCrLf & "Cancel" & vbTab & "- Ignore"
		//							frmUTCLStatus.InstancePtr.intLDNAnswer = intTemp;
		//						}
		//					}
		//					else
		//					{
		//						intTemp = frmUTCLStatus.InstancePtr.intLDNAnswer;
		//					}
		//					if (intTemp == DialogResult.Yes)
		//					{
		//						rsTemp.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].BillKey), modExtraModules.strUTDatabase);
		//						if (rsTemp.EndOfFile())
		//						{
		//							rsTemp.AddNew();
		//						}
		//						else
		//						{
		//							rsTemp.Edit();
		//						}
		//						rsTemp.Set_Fields("DatePaid", Statics.UTPaymentArray[lngIndex].RecordedTransactionDate);
		//						rsTemp.Set_Fields("Teller", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
		//						rsTemp.Set_Fields("LienKey", Statics.UTPaymentArray[lngIndex].BillKey);
		//						rsTemp.Set_Fields("UpdatedDate", DateTime.Today);
		//						rsTemp.Set_Fields("Printed", true);
		//						rsTemp.Set_Fields("Batch", false);
		//						rsTemp.Update();
		//						frmUTLienDischargeNotice.InstancePtr.Init(Statics.UTPaymentArray[lngIndex].RecordedTransactionDate, FCConvert.CBool(Statics.UTPaymentArray[lngIndex].Service == "W"), Statics.UTPaymentArray[lngIndex].BillKey);
		//					}
		//					else if (intTemp == DialogResult.No)
		//					{
		//						rsTemp.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].BillKey), modExtraModules.strUTDatabase);
		//						if (rsTemp.EndOfFile())
		//						{
		//							rsTemp.AddNew();
		//						}
		//						else
		//						{
		//							rsTemp.Edit();
		//						}
		//						rsTemp.Set_Fields("DatePaid", Statics.UTPaymentArray[lngIndex].RecordedTransactionDate);
		//						rsTemp.Set_Fields("Teller", modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID());
		//						rsTemp.Set_Fields("LienKey", Statics.UTPaymentArray[lngIndex].BillKey);
		//						rsTemp.Set_Fields("UpdatedDate", DateTime.Today);
		//						rsTemp.Set_Fields("Printed", false);
		//						rsTemp.Set_Fields("Batch", false);
		//						rsTemp.Update();
		//					}
		//					else
		//					{
		//						// cancel
		//						modGlobalFunctions.AddCYAEntry_26("CL", "Ignored Lien Discharge Notice", "Lien ID = " + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].BillKey));
		//					}
		//				}
		//			}
		//			else if (boolPaid30DN)
		//			{
		//				if (!modExtraModules.IsThisCR())
		//				{
		//					// kk10072014 trout-1093  If in CR, wait until Receipt is completed to do this
		//					rsTemp.OpenRecordset("SELECT * FROM Bill WHERE " + Statics.UTPaymentArray[lngIndex].Service + "LienStatusEligibility > 2 AND " + Statics.UTPaymentArray[lngIndex].Service + "LienProcessStatus <= 3 AND " + Statics.UTPaymentArray[lngIndex].Service + "PrinOwed > " + Statics.UTPaymentArray[lngIndex].Service + "PrinPaid AND ID <> " + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].BillKey) + " AND AccountKey = " + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].Account), modExtraModules.strUTDatabase);
		//					if (!rsTemp.EndOfFile())
		//					{
		//						// MAL@20071011: Changed to always be 'yes'
		//						// Call Reference: 117084
		//						// intTemp = MsgBox("Would you like to remove the other bills that are associated with this bill from 30 DN status?", vbYesNo + vbQuestion, "Remove from Lien Status")
		//						intTemp = DialogResult.Yes;
		//						if (intTemp == DialogResult.Yes)
		//						{
		//							modGlobalFunctions.AddCYAEntry_8("UT", "Paid bill in 30 DN status.");
		//							while (!rsTemp.EndOfFile())
		//							{
		//								rsTemp.Edit();
		//								rsTemp.Set_Fields(Statics.UTPaymentArray[lngIndex].Service + "LienStatusEligibility", 0);
		//								rsTemp.Set_Fields(Statics.UTPaymentArray[lngIndex].Service + "LienProcessStatus", 0);
		//								rsTemp.Set_Fields(Statics.UTPaymentArray[lngIndex].Service + "DemandGroupID", 0);
		//								rsTemp.Update();
		//								rsTemp.MoveNext();
		//							}
		//						}
		//					}
		//				}
		//			}
		//			else
		//			{
		//				// MAL@20080709: Add option for Partial Payment Waiver
		//				// Tracker Reference: 14159
		//				if (Statics.UTPaymentArray[lngIndex].BillCode == "L" && Statics.UTPaymentArray[lngIndex].ReversalID == -1)
		//				{
		//					// this will be when the person .Reversalpays a partial payment rather than the LDN and
		//					// should fill out the waiver for partial payments before the town accepts the payment
		//					if (!blnPrompted)
		//					{
		//						intTemp = MessageBox.Show("Would you like to print the Lien Partial Payment waiver now?", "Partial Payment Waiver", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
		//						switch (intTemp)
		//						{
		//							case DialogResult.Yes:
		//								{
		//									modGlobalFunctions.AddCYAEntry_26("UT", "Printed Lien Partial Payment Waiver", "Lien ID = " + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].BillKey));
		//									frmUTLienPartialPayment.InstancePtr.Init(Statics.UTPaymentArray[lngIndex].EffectiveInterestDate, Statics.UTPaymentArray[lngIndex].BillKey, modExtraModules.FormatYear(Statics.UTPaymentArray[lngIndex].Year.ToString()), GetUTAccountNumber(Statics.UTPaymentArray[lngIndex].Account), FCConvert.ToDouble(Statics.UTPaymentArray[lngIndex].Principal + Statics.UTPaymentArray[lngIndex].PreLienInterest + Statics.UTPaymentArray[lngIndex].CurrentInterest + Statics.UTPaymentArray[lngIndex].LienCost), Statics.UTPaymentArray[lngIndex].Service, Statics.UTPaymentArray[lngIndex].BillNumber);
		//									break;
		//								}
		//							case DialogResult.No:
		//							case DialogResult.Cancel:
		//								{
		//									modGlobalFunctions.AddCYAEntry_26("UT", "Ignored Lien Partial Payment Waiver", "Lien ID = " + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].BillKey));
		//									break;
		//								}
		//						}
		//						//end switch
		//					}
		//				}
		//			}
		//		}
		//		else
		//		{
		//			MessageBox.Show("Out of Bounds Array Index", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//		}
		//		rsCreatePy.Reset();
		//		rsTemp.Reset();
		//		return CreateUTPaymentRecord;
		//		END_OF_PAYMENT:
		//		;
		//		MessageBox.Show("Account " + FCConvert.ToString(Statics.UTPaymentArray[lngIndex].Account) + " has had an error while processing.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//		rsCPy.Delete();
		//		rsCreatePy.Reset();
		//		rsTemp.Reset();
		//		return CreateUTPaymentRecord;
		//	}
		//	catch (Exception ex)
		//	{
				
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Payment Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//		/*? Resume; */
		//	}
		//	return CreateUTPaymentRecord;
		//}
		//// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		//public static short AddUTPaymentToList_21(int cRow, bool boolSkipRecalc = false, bool boolDiscount = false, bool boolAddToOtherGrid = false, double dblOverrideAmount = 0, int lngReversalKey = 0, bool boolExcessPayment = false, int lngOverridebill = 0, bool boolOverrideType = false, short intOverrideType = 0)
		//{
		//	return AddUTPaymentToList(cRow, false, boolSkipRecalc, boolDiscount, boolAddToOtherGrid, dblOverrideAmount, lngReversalKey, boolExcessPayment, lngOverridebill, boolOverrideType, intOverrideType);
		//}

		//public static short AddUTPaymentToList_24(int cRow, bool boolCalcInterest = false, bool boolSkipRecalc = false, bool boolDiscount = false, bool boolAddToOtherGrid = false, double dblOverrideAmount = 0, int lngReversalKey = 0, bool boolExcessPayment = false, int lngOverridebill = 0, bool boolOverrideType = false, short intOverrideType = 0)
		//{
		//	return AddUTPaymentToList(cRow, boolCalcInterest, boolSkipRecalc, boolDiscount, boolAddToOtherGrid, dblOverrideAmount, lngReversalKey, boolExcessPayment, lngOverridebill, boolOverrideType, intOverrideType);
		//}

		//public static short AddUTPaymentToList_1133(int cRow, bool boolCalcInterest = false, bool boolSkipRecalc = false, bool boolDiscount = false, bool boolExcessPayment = false, int lngOverridebill = 0, bool boolOverrideType = false, short intOverrideType = 0)
		//{
		//	return AddUTPaymentToList(cRow, boolCalcInterest, boolSkipRecalc, boolDiscount, false, 0, 0, boolExcessPayment, lngOverridebill, boolOverrideType, intOverrideType);
		//}

		//public static short AddUTPaymentToList_5343(int cRow, bool boolCalcInterest = false, bool boolSkipRecalc = false, bool boolDiscount = false, bool boolAddToOtherGrid = false, double dblOverrideAmount = 0, bool boolExcessPayment = false, int lngOverridebill = 0, bool boolOverrideType = false, short intOverrideType = 0)
		//{
		//	return AddUTPaymentToList(cRow, boolCalcInterest, boolSkipRecalc, boolDiscount, boolAddToOtherGrid, dblOverrideAmount, 0, boolExcessPayment, lngOverridebill, boolOverrideType, intOverrideType);
		//}

		//public static short AddUTPaymentToList_5466(int cRow, bool boolExcessPayment = false, int lngOverridebill = 0, bool boolOverrideType = false, short intOverrideType = 0)
		//{
		//	return AddUTPaymentToList(cRow, false, false, false, false, 0, 0, boolExcessPayment, lngOverridebill, boolOverrideType, intOverrideType);
		//}

		//public static short AddUTPaymentToList_5475(int cRow, bool boolSkipRecalc = false, bool boolExcessPayment = false, int lngOverridebill = 0, bool boolOverrideType = false, short intOverrideType = 0)
		//{
		//	return AddUTPaymentToList(cRow, false, boolSkipRecalc, false, false, 0, 0, boolExcessPayment, lngOverridebill, boolOverrideType, intOverrideType);
		//}

		//public static short AddUTPaymentToList_160755(int cRow, bool boolCalcInterest = false, bool boolSkipRecalc = false, int lngOverridebill = 0, bool boolOverrideType = false, short intOverrideType = 0)
		//{
		//	return AddUTPaymentToList(cRow, boolCalcInterest, boolSkipRecalc, false, false, 0, 0, false, lngOverridebill, boolOverrideType, intOverrideType);
		//}

		//public static short AddUTPaymentToList(int cRow, bool boolCalcInterest = false, bool boolSkipRecalc = false, bool boolDiscount = false, bool boolAddToOtherGrid = false, double dblOverrideAmount = 0, int lngReversalKey = 0, bool boolExcessPayment = false, int lngOverridebill = 0, bool boolOverrideType = false, short intOverrideType = 0)
		//{
		//	short AddUTPaymentToList = 0;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// this sub will add a row to the grid and fill the payment info into it
		//		double dblTot;
		//		// total left of monies paid
		//		// vbPorter upgrade warning: dblPrin As double	OnWrite(Decimal, short, double)
		//		double dblPrin;
		//		// principal total left
		//		double dblPrinNeed = 0;
		//		// principal needed to totally pay off principal of bill
		//		// vbPorter upgrade warning: dblTax As double	OnWrite(Decimal, short, double)
		//		double dblTax;
		//		double dblTaxNeed = 0;
		//		// vbPorter upgrade warning: dblInt As double	OnWrite(Decimal, double, short)
		//		double dblInt;
		//		// interest total left
		//		double dblIntNeed = 0;
		//		// interest needed to pay off interest of bill
		//		// vbPorter upgrade warning: dblCost As double	OnWrite(Decimal, short, double)
		//		double dblCost;
		//		// cost total left
		//		double dblCostNeed = 0;
		//		// cost needed to pay off costs of bill
		//		clsDRWrapper rsTemp = new clsDRWrapper();
		//		clsDRWrapper rsLienRec = new clsDRWrapper();
		//		clsDRWrapper rsPeriod = new clsDRWrapper();
		//		int intCT;
		//		string strTemp = "";
		//		string PayCode = "";
		//		// this is either (W)ater or (S)ewer
		//		string BillCode = "";
		//		// this is either (R)egular or (L)ien
		//		// vbPorter upgrade warning: lngBill As int	OnWrite(int, string)
		//		int lngBill = 0;
		//		// holds the correct BillKey for the payment record
		//		int lngBillNumber = 0;
		//		// holds the correct BillNumber for the payment record
		//		string strType = "";
		//		int lngPaymentRow;
		//		bool boolFound = false;
		//		bool boolNoLoop = false;
		//		bool boolWater = false;
		//		int intType = 0;
		//		int intOtherType;
		//		int lngTesterRow = 0;
		//		int lngStarterRow = 0;
		//		string strOppType = "";
		//		bool boolBothService = false;
		//		string strWS;
		//		bool boolLienPayment = false;
		//		double dblTempCutInt;
		//		string strTempPayCode = "";
		//		string strOppTempPayCode = "";
		//		int intTempType = 0;
		//		// vbPorter upgrade warning: dblTempP As double	OnRead(Decimal, double)
		//		double dblTempP = 0;
		//		// vbPorter upgrade warning: dblTempT As double	OnRead(Decimal, double)
		//		double dblTempT = 0;
		//		// vbPorter upgrade warning: dblTempI As double	OnRead(Decimal, double)
		//		double dblTempI = 0;
		//		// vbPorter upgrade warning: dblTempC As double	OnRead(Decimal, double)
		//		double dblTempC = 0;
		//		// vbPorter upgrade warning: dblTempPI As double	OnRead(Decimal, double)
		//		double dblTempPI = 0;
		//		// MAL@20071012
		//		double dblPLINeed = 0;
		//		// MAL@20071012
		//		double dblPLI = 0;
		//		// MAL@20071012
		//		int lngRWCT = 0;
		//		// kk01162015
		//		// force all unsused indexes of UTPaymentArray to have a -1 value in the ReversalID
		//		for (intCT = 1; intCT <= MAX_UTPAYMENTS; intCT++)
		//		{
		//			if (Statics.UTPaymentArray[intCT].Account == 0)
		//				Statics.UTPaymentArray[intCT].ReversalID = -1;
		//		}
		//		if (boolDiscount)
		//		{
		//			strType = "D";
		//		}
		//		else
		//		{
		//			// this is the type of payment that is being processed
		//			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//			//strType = Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Items[frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex].ToString(), 1);
		//			strType = Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Text, 1);
		//		}
		//		if (boolOverrideType)
		//		{
		//			intType = intOverrideType;
		//		}
		//		else
		//		{
		//			intType = frmUTCLStatus.InstancePtr.FindPaymentType();
		//		}
		//		if (boolAddToOtherGrid)
		//		{
		//			strType = "P";
		//			if (intType == 0)
		//			{
		//				intOtherType = 1;
		//				boolWater = false;
		//				PayCode = "S";
		//				strOppType = "Water";
		//			}
		//			else
		//			{
		//				intOtherType = 0;
		//				boolWater = true;
		//				PayCode = "W";
		//				strOppType = "Sewer";
		//			}
		//		}
		//		else
		//		{
		//			if (intType == 0)
		//			{
		//				intOtherType = 1;
		//				boolWater = true;
		//				PayCode = "W";
		//				strOppType = "Sewer";
		//			}
		//			else
		//			{
		//				intOtherType = 0;
		//				boolWater = false;
		//				PayCode = "S";
		//				strOppType = "Water";
		//			}
		//		}
		//		strWS = PayCode;
		//		if (strType == "U")
		//		{
		//			boolSkipRecalc = true;
		//		}
		//		if (lngOverridebill == 0)
		//		{
		//			lngBill = FindDefaultUTBill(ref frmUTCLStatus.InstancePtr.CurrentAccountKey, ref boolWater);
		//		}
		//		else
		//		{
		//			lngBill = lngOverridebill;
		//		}
		//		dblInt = FCConvert.ToDouble(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.txtInterest.Text));
		//		dblTax = FCConvert.ToDouble(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.txtTax.Text));
		//		dblCost = FCConvert.ToDouble(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.txtCosts.Text));
		//		dblPrin = FCConvert.ToDouble(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.txtPrincipal.Text));
		//		dblTot = dblInt + dblCost + dblPrin + dblTax;
		//		if (boolAddToOtherGrid)
		//		{
		//			dblInt = dblOverrideAmount;
		//			dblTax = 0;
		//			dblCost = 0;
		//			dblPrin = 0;
		//			dblTot = dblOverrideAmount;
		//		}
		//		// make sure that this has a valid account if needed
		//		if (frmUTCLStatus.InstancePtr.txtCD.Text == "N" && frmUTCLStatus.InstancePtr.txtCash.Text == "N" && !boolDiscount)
		//		{
		//			// check to make sure that this is only negative interest
		//			if (modValidateAccount.AccountValidate(frmUTCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0)))
		//			{
		//				// all set
		//			}
		//			else if (dblInt < 0 && dblCost == 0 && dblPrin == 0 && dblTax == 0)
		//			{
		//				// this is ok too
		//			}
		//			else
		//			{
		//				MessageBox.Show("Please enter a valid account for this payment that will not affect today's cash or the cash drawer.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//				if (frmUTCLStatus.InstancePtr.txtAcctNumber.Enabled && frmUTCLStatus.InstancePtr.txtAcctNumber.Visible)
		//				{
		//					frmUTCLStatus.InstancePtr.txtAcctNumber.Focus();
		//				}
		//				AddUTPaymentToList = -1;
		//				return AddUTPaymentToList;
		//			}
		//		}
		//		else if (boolDiscount)
		//		{
		//			frmUTCLStatus.InstancePtr.txtCD.Text = "N";
		//			frmUTCLStatus.InstancePtr.txtCash.Text = "N";
		//			if (boolWater)
		//			{
		//				frmUTCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0, frmUTCLStatus.InstancePtr.GetUTAccount("D", 93));
		//			}
		//			else
		//			{
		//				frmUTCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0, frmUTCLStatus.InstancePtr.GetUTAccount("D", 94));
		//			}
		//		}
		//		// this will force the correct monies into the correct place
		//		if (strType == "A" || strType == "R")
		//		{
		//		}
		//		else if (strType == "Y")
		//		{
		//			dblInt = 0;
		//			dblTax = 0;
		//			dblPrin = dblTot;
		//			dblCost = 0;
		//			boolSkipRecalc = true;
		//		}
		//		else if (!((strType == "C" || strType == "P") && (dblInt < 0 || dblTot < 0)) && !boolSkipRecalc)
		//		{
		//			dblInt = dblTot;
		//			dblTax = 0;
		//			dblPrin = 0;
		//			dblCost = 0;
		//		}
		//		else
		//		{
		//			dblTot = 0;
		//		}
		//		// ************************************************************************************
		//		// This is where I check to see if it is an Auto payment or a payment to one bill     *
		//		// ************************************************************************************
		//		bool executeSTARTNOLOOP = false;
		//		//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//		//if (Strings.Trim(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString()) == "Auto")
		//		if (Strings.Trim(frmUTCLStatus.InstancePtr.cmbBillNumber.Text) == "Auto")
		//		{
		//			// if "Auto" is choosen in the Year Combobox
		//			if (frmUTCLStatus.InstancePtr.vsPayments.Rows > 1)
		//			{
		//				// check to see if the other payments are auto payemnts
		//				boolFound = false;
		//				for (lngPaymentRow = 1; lngPaymentRow <= frmUTCLStatus.InstancePtr.vsPayments.Rows - 1; lngPaymentRow++)
		//				{
		//					if ((Strings.UCase(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngPaymentRow, frmUTCLStatus.InstancePtr.lngPayGridColService)) == "W" && boolWater) || (Strings.UCase(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngPaymentRow, frmUTCLStatus.InstancePtr.lngPayGridColService)) == "S" && !boolWater))
		//					{
		//						if (!(Strings.UCase(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngPaymentRow, frmUTCLStatus.InstancePtr.lngPayGridColRef)) == "AUTO" && Strings.UCase(frmUTCLStatus.InstancePtr.txtReference.Text) == "AUTO"))
		//						{
		//							// kk07142015 trouts-140  Allow multiple AUTO On F11 to move credit
		//							boolFound = true;
		//							// kk01162015 trout-1037  Do not allow multiple auto payments to the same service
		//						}
		//					}
		//				}
		//				if (boolFound)
		//				{
		//					// And Not boolAddToOtherGrid Then  'another payment is found from this year
		//					// so do not let anyone get through it
		//					MessageBox.Show("You cannot use a code of Auto because there are already payments made to the selected service.", "Existing Payments", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//					if (frmUTCLStatus.InstancePtr.cmbCode.Enabled && frmUTCLStatus.InstancePtr.cmbCode.Visible)
		//					{
		//						frmUTCLStatus.InstancePtr.cmbCode.Focus();
		//					}
		//					AddUTPaymentToList = -1;
		//					return AddUTPaymentToList;
		//				}
		//				// else if so let them through
		//			}
		//			// MAL@200711013: Changed to sort by Bill Date instead of Bill Number
		//			// Tracker Reference: 11320
		//			// MAL@20080624: Changed to include secondary sort by Bill Number
		//			// Tracker Reference: 14381
		//			// strTemp = "SELECT * FROM  (" & strUTCurrentAccountBills & ")  WHERE ID = " & frmUTCLStatus.CurrentAccountKey & " ORDER BY BillNumber asc"
		//			strTemp = "SELECT * FROM  (" + Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(frmUTCLStatus.InstancePtr.CurrentAccountKey) + " ORDER BY BillDate asc, BillNumber asc";
		//			// make sure that there is a bill that can be found to add the payment to
		//			rsTemp.OpenRecordset(strTemp, modExtraModules.strUTDatabase);
		//			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
		//			{
		//			}
		//			else
		//			{
		//				MessageBox.Show("Error opening database.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//				AddUTPaymentToList = -1;
		//				return AddUTPaymentToList;
		//			}
		//			// -----------------------------------------------------------------------------------
		//			boolNoLoop = true;
		//			// this will check each bill in order no matter what service and pay one service
		//			// and then the other before moving onto the next bill  09/07/2006
		//			if (Statics.gboolAutoPayOldestBillFirst)
		//			{
		//				while (!(rsTemp.EndOfFile() || dblTot <= 0))
		//				{
		//					boolNoLoop = false;
		//					for (intCT = 1; intCT <= MAX_UTPAYMENTS; intCT++)
		//					{
		//						// this finds a place in the array
		//						if (Statics.UTPaymentArray[intCT].Account == 0)
		//						{
		//							// to store the new payment record
		//							break;
		//						}
		//					}
		//					if (rsTemp.EndOfFile() != true)
		//					{
		//						// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//						lngBill = FCConvert.ToInt32(rsTemp.Get_Fields("Bill"));
		//						lngBillNumber = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber"));
		//					}
		//					// set the res code
		//					Statics.UTPaymentArray[intCT].ResCode = frmUTCLStatus.InstancePtr.lngUTResCode;
		//					if (Statics.TownService == "B")
		//					{
		//						if (Statics.gboolPayWaterFirst)
		//						{
		//							strTempPayCode = "W";
		//							strOppTempPayCode = "S";
		//							intTempType = 0;
		//						}
		//						else
		//						{
		//							strTempPayCode = "S";
		//							strOppTempPayCode = "W";
		//							intTempType = 1;
		//						}
		//					}
		//					else
		//					{
		//						strTempPayCode = PayCode;
		//						if (strTempPayCode == "W")
		//						{
		//							intTempType = 0;
		//						}
		//						else
		//						{
		//							intTempType = 1;
		//						}
		//					}
		//					// get the next year and its total due values
		//					if (FCConvert.ToInt32(rsTemp.Get_Fields(strTempPayCode + "LienRecordNumber")) == 0)
		//					{
		//						if (((!boolExcessPayment && lngBillNumber != 0) || (boolExcessPayment && lngBillNumber == 0)) || !boolAddToOtherGrid)
		//						{
		//							Statics.UTPaymentArray[intCT].BillNumber = lngBillNumber;
		//							Statics.UTPaymentArray[intCT].BillKey = lngBill;
		//							BillCode = "R";
		//							dblPrinNeed = modGlobal.Round(FCConvert.ToDouble(rsTemp.Get_Fields(strTempPayCode + "PrinOwed")) - FCConvert.ToDouble(rsTemp.Get_Fields(strTempPayCode + "PrinPaid")), 2);
		//							dblTaxNeed = modGlobal.Round(FCConvert.ToDouble(rsTemp.Get_Fields(strTempPayCode + "TaxOwed")) - FCConvert.ToDouble(rsTemp.Get_Fields(strTempPayCode + "TaxPaid")), 2);
		//							dblIntNeed = modGlobal.Round((FCConvert.ToDouble(rsTemp.Get_Fields(strTempPayCode + "IntAdded")) * -1) + FCConvert.ToDouble(rsTemp.Get_Fields(strTempPayCode + "IntOwed")) - FCConvert.ToDouble(rsTemp.Get_Fields(strTempPayCode + "IntPaid")) - Statics.dblUTCurrentInt[FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber")), intTempType], 2);
		//							dblCostNeed = modGlobal.Round(FCConvert.ToDouble(rsTemp.Get_Fields(strTempPayCode + "CostOwed")) - FCConvert.ToDouble(rsTemp.Get_Fields(strTempPayCode + "CostAdded")) - FCConvert.ToDouble(rsTemp.Get_Fields(strTempPayCode + "CostPaid")), 2);
		//							dblPLINeed = 0;
		//						}
		//					}
		//					else
		//					{
		//						BillCode = "L";
		//						rsLienRec.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTemp.Get_Fields(strWS + "LienRecordNumber"), modExtraModules.strUTDatabase);
		//						if (rsLienRec.RecordCount() != 0)
		//						{
		//							Statics.UTPaymentArray[intCT].BillNumber = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("RateKey"));
		//							Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields(strWS + "LienRecordNumber"));
		//							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//							dblPrinNeed = modGlobal.Round(rsLienRec.Get_Fields("Principal") - rsLienRec.Get_Fields("PrinPaid"), 2);
		//							// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//							dblTaxNeed = modGlobal.Round(rsLienRec.Get_Fields("Tax") - rsLienRec.Get_Fields("TaxPaid"), 2);
		//							// MAL@20071012: Changed to break out PLI and Interest
		//							// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//							dblIntNeed = modGlobal.Round(((FCConvert.ToDouble(rsLienRec.Get_Fields("IntAdded")) * -1) - FCConvert.ToDouble(rsLienRec.Get_Fields("IntPaid"))) - Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intTempType], 2);
		//							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//							dblPLINeed = modGlobal.Round((rsLienRec.Get_Fields("Interest") - rsLienRec.Get_Fields("PLIPaid")), 2);
		//							// dblIntNeed = Round(((rsLienRec.Fields("IntAdded") * -1) - rsLienRec.Fields("IntPaid")) + rsLienRec.Fields("Interest") - rsLienRec.Fields("PLIPaid") - dblUTCurrentInt(rsTemp.Fields("BillNumber"), intTempType), 2)
		//							// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//							dblCostNeed = modGlobal.Round(rsLienRec.Get_Fields("Costs") - rsLienRec.Get_Fields("MaturityFee") - rsLienRec.Get_Fields_Double("CostPaid"), 2);
		//						}
		//					}
		//					// kk01162016 trout-1037  Check pending payments here
		//					lngRWCT = frmUTCLStatus.InstancePtr.vsPayments.Rows - 1;
		//					while (!(lngRWCT <= 0))
		//					{
		//						if (FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColBill)) == lngBillNumber && Strings.UCase(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColService)) == strTempPayCode)
		//						{
		//							// adjust the need
		//							dblPrinNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColPrincipal));
		//							dblTaxNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColTax));
		//							if (dblPLINeed > 0)
		//							{
		//								if (dblPLINeed <= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest)))
		//								{
		//									dblPLINeed = 0;
		//									dblIntNeed -= (FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest)) - dblPLINeed);
		//								}
		//								else
		//								{
		//									dblPLINeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest));
		//								}
		//							}
		//							else
		//							{
		//								dblIntNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest));
		//							}
		//							dblCostNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColCosts));
		//						}
		//						lngRWCT -= 1;
		//					}
		//					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//					//if ((BillCode != "L" || (rsTemp.Get_Fields(strTempPayCode + "CombinationLienKey") == rsTemp.Get_Fields("Bill"))) && strOppTempPayCode != Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Items[frmUTCLStatus.InstancePtr.cmbService.SelectedIndex].ToString(), 1))
		//					// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//					if ((BillCode != "L" || (rsTemp.Get_Fields(strTempPayCode + "CombinationLienKey") == rsTemp.Get_Fields("Bill"))) && strOppTempPayCode != Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Text, 1))
		//					{
		//						if ((dblPrinNeed > 0 || dblTaxNeed > 0 || dblIntNeed > 0) && (dblPrinNeed + dblTaxNeed + dblIntNeed + dblCostNeed + dblPLINeed) > 0)
		//						{
		//							if (dblPrinNeed + dblTaxNeed + dblIntNeed + dblCostNeed + dblPLINeed != 0)
		//							{
		//								// if it is actually a payment then
		//								if (intCT < MAX_UTPAYMENTS)
		//								{
		//									dblTempP = 0;
		//									dblTempT = 0;
		//									dblTempI = 0;
		//									dblTempC = 0;
		//									dblTempPI = 0;
		//									// MAL@20071012
		//									dblInt = dblTot;
		//									// these are the money fields
		//									// AdjustUTPaymentAmounts dblPrin, dblTax, dblInt, dblCost, dblPrinNeed, dblTaxNeed, dblIntNeed, dblCostNeed, dblTempP, dblTempT, dblTempI, dblTempC
		//									AdjustUTPaymentAmounts(ref dblPrin, ref dblTax, ref dblInt, ref dblCost, ref dblPrinNeed, ref dblTaxNeed, ref dblIntNeed, ref dblCostNeed, ref dblTempP, ref dblTempT, ref dblTempI, ref dblTempC, dblPLI, dblPLINeed, dblTempPI);
		//									Statics.UTPaymentArray[intCT].Principal = FCConvert.ToDecimal(dblTempP);
		//									Statics.UTPaymentArray[intCT].Tax = FCConvert.ToDecimal(dblTempT);
		//									Statics.UTPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(dblTempI);
		//									Statics.UTPaymentArray[intCT].LienCost = FCConvert.ToDecimal(dblTempC);
		//									Statics.UTPaymentArray[intCT].Service = strTempPayCode;
		//									Statics.UTPaymentArray[intCT].PreLienInterest = FCConvert.ToDecimal(dblTempPI);
		//									// MAL@20071012
		//									// dblTot = Round(dblTot - dblTempP - dblTempT - dblTempI - dblTempC, 2)
		//									dblTot = modGlobal.Round(dblTot - dblTempP - dblTempT - dblTempI - dblTempC - dblTempPI, 2);
		//									// this adds it to the array of payments
		//									Statics.UTPaymentArray[intCT].Account = frmUTCLStatus.InstancePtr.CurrentAccountKey;
		//									Statics.UTPaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//									Statics.UTPaymentArray[intCT].BillCode = BillCode;
		//									// this is either (R)egular or (L)ien
		//									if (BillCode == "L")
		//									{
		//										boolLienPayment = true;
		//										Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
		//									}
		//									else
		//									{
		//										boolLienPayment = false;
		//										// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//										Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields("Bill"));
		//									}
		//									Statics.UTPaymentArray[intCT].CashDrawer = frmUTCLStatus.InstancePtr.txtCD.Text;
		//									if (Statics.UTPaymentArray[intCT].CashDrawer == "N")
		//									{
		//										// kk06092014 TROCRS-27  Need to remove all "_" from the account number or it isn't detected
		//										Statics.UTPaymentArray[intCT].BudgetaryAccountNumber = frmUTCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0).Replace("_", "");
		//										// frmUTCLStatus.txtAcctNumber.TextMatrix(0, 0)
		//									}
		//									else
		//									{
		//										Statics.UTPaymentArray[intCT].BudgetaryAccountNumber = "";
		//									}
		//									Statics.UTPaymentArray[intCT].Code = strType;
		//									Statics.UTPaymentArray[intCT].Comments = frmUTCLStatus.InstancePtr.txtComments.Text;
		//									Statics.UTPaymentArray[intCT].EffectiveInterestDate = Statics.UTEffectiveDate;
		//									Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(frmUTCLStatus.InstancePtr.txtTransactionDate.Text);
		//									Statics.UTPaymentArray[intCT].GeneralLedger = frmUTCLStatus.InstancePtr.txtCash.Text;
		//									// .PreLienInterest = 0       'MAL@20071012
		//									Statics.UTPaymentArray[intCT].ReceiptNumber = "P";
		//									if (frmUTCLStatus.InstancePtr.txtTransactionDate.Text == "")
		//									{
		//										Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//									}
		//									else
		//									{
		//										Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmUTCLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//									}
		//									Statics.UTPaymentArray[intCT].Reference = Strings.Trim(frmUTCLStatus.InstancePtr.txtReference.Text + " ");
		//									Statics.UTPaymentArray[intCT].ReversalID = -1;
		//									// .Teller =
		//									// .TransNumber=
		//									// .PaidBy =
		//									if (boolCalcInterest && lngReversalKey != 0)
		//									{
		//										// this is where the CHGINT Number needs to be put in the
		//										// lngReversalKey is the payment ID of the payment being reversed
		//										// intCT is the array index of the reversal payment
		//										// AddCHGINTToList frmRECLStatus.vsPayments.rows, lngReversalKey, True, intCT
		//									}
		//									else if (FCConvert.CBool(Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intTempType] != 0))
		//									{
		//										// AddCHGINTToList frmRECLStatus.vsPayments.rows, , True
		//										// this will create a CHGINT line for normal payments
		//										if (boolLienPayment)
		//										{
		//											Statics.UTPaymentArray[intCT].CHGINTNumber = CreateUTCHGINTLine_18(intCT, ref Statics.UTPaymentArray[intCT].EffectiveInterestDate, FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber")), boolAddToOtherGrid);
		//										}
		//										else
		//										{
		//											Statics.UTPaymentArray[intCT].CHGINTNumber = CreateUTCHGINTLine_9(intCT, ref Statics.UTPaymentArray[intCT].EffectiveInterestDate, boolAddToOtherGrid);
		//										}
		//									}
		//									// .Year = lngBill
		//									frmUTCLStatus.InstancePtr.FillPaymentGrid(cRow, intCT, Statics.UTPaymentArray[intCT].BillNumber);
		//									// actually fills the number into the payment grid
		//								}
		//								else
		//								{
		//									MessageBox.Show("You have reached your maximum payments per screen.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//								}
		//							}
		//							else if (dblPrinNeed + dblTaxNeed + dblIntNeed + dblCostNeed < 0)
		//							{
		//								// add the negative to the total used to pay of later payments
		//								// add a payment that will make this bill 0
		//							}
		//						}
		//						else
		//						{
		//							Statics.UTPaymentArray[intCT].BillKey = 0;
		//							Statics.UTPaymentArray[intCT].Account = 0;
		//						}
		//					}
		//					// now check the other service of this same bill if the town has both services
		//					if (Statics.TownService == "B" && dblTot > 0)
		//					{
		//						for (intCT = 1; intCT <= MAX_UTPAYMENTS; intCT++)
		//						{
		//							// this finds a place in the array
		//							if (Statics.UTPaymentArray[intCT].Account == 0)
		//							{
		//								// to store the new payment record
		//								break;
		//							}
		//						}
		//						if (Statics.gboolPayWaterFirst)
		//						{
		//							strTempPayCode = "S";
		//							strOppTempPayCode = "W";
		//							intTempType = 1;
		//						}
		//						else
		//						{
		//							strTempPayCode = "W";
		//							strOppTempPayCode = "S";
		//							intTempType = 0;
		//						}
		//						// get the next year and its total due values
		//						if (FCConvert.ToInt32(rsTemp.Get_Fields(strTempPayCode + "LienRecordNumber")) == 0)
		//						{
		//							if (((!boolExcessPayment && lngBillNumber != 0) || (boolExcessPayment && lngBillNumber == 0)) || !boolAddToOtherGrid)
		//							{
		//								Statics.UTPaymentArray[intCT].BillNumber = lngBillNumber;
		//								Statics.UTPaymentArray[intCT].BillKey = lngBill;
		//								BillCode = "R";
		//								dblPrinNeed = modGlobal.Round(rsTemp.Get_Fields(strTempPayCode + "PrinOwed") - rsTemp.Get_Fields(strTempPayCode + "PrinPaid"), 2);
		//								dblTaxNeed = modGlobal.Round(rsTemp.Get_Fields(strTempPayCode + "TaxOwed") - rsTemp.Get_Fields(strTempPayCode + "TaxPaid"), 2);
		//								dblIntNeed = modGlobal.Round((FCConvert.ToDouble(rsTemp.Get_Fields(strTempPayCode + "IntAdded")) * -1) + rsTemp.Get_Fields(strTempPayCode + "IntOwed") - rsTemp.Get_Fields(strTempPayCode + "IntPaid") - Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intTempType], 2);
		//								dblCostNeed = modGlobal.Round(rsTemp.Get_Fields(strTempPayCode + "CostOwed") - rsTemp.Get_Fields(strTempPayCode + "CostAdded") - rsTemp.Get_Fields(strTempPayCode + "CostPaid"), 2);
		//								dblPLINeed = 0;
		//							}
		//						}
		//						else
		//						{
		//							BillCode = "L";
		//							rsLienRec.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTemp.Get_Fields(strTempPayCode + "LienRecordNumber"), modExtraModules.strUTDatabase);
		//							if (rsLienRec.RecordCount() != 0)
		//							{
		//								Statics.UTPaymentArray[intCT].BillNumber = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("RateKey"));
		//								Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields(strWS + "LienRecordNumber"));
		//								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//								dblPrinNeed = modGlobal.Round(rsLienRec.Get_Fields("Principal") - rsLienRec.Get_Fields("PrinPaid"), 2);
		//								// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//								dblTaxNeed = modGlobal.Round(rsLienRec.Get_Fields("Tax") - rsLienRec.Get_Fields("TaxPaid"), 2);
		//								// MAL@20071012: Changed to break out PLI and Interest
		//								// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//								dblIntNeed = modGlobal.Round(((FCConvert.ToDouble(rsLienRec.Get_Fields("IntAdded")) * -1) - FCConvert.ToDouble(rsLienRec.Get_Fields("IntPaid"))) - Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intTempType], 2);
		//								// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//								dblPLINeed = modGlobal.Round((rsLienRec.Get_Fields("Interest") - rsLienRec.Get_Fields("PLIPaid")), 2);
		//								// dblIntNeed = Round(((rsLienRec.Fields("IntAdded") * -1) - rsLienRec.Fields("IntPaid")) + rsLienRec.Fields("Interest") - rsLienRec.Fields("PLIPaid") - dblUTCurrentInt(rsTemp.Fields("BillNumber"), intTempType), 2)
		//								// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//								dblCostNeed = modGlobal.Round(rsLienRec.Get_Fields("Costs") - rsLienRec.Get_Fields("MaturityFee") - rsLienRec.Get_Fields_Double("CostPaid"), 2);
		//							}
		//						}
		//						// kk01162016 trout-1037  Check pending payments here
		//						lngRWCT = frmUTCLStatus.InstancePtr.vsPayments.Rows - 1;
		//						while (!(lngRWCT <= 0))
		//						{
		//							if (FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColBill)) == lngBillNumber && Strings.UCase(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColService)) == strTempPayCode)
		//							{
		//								// adjust the need
		//								dblPrinNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColPrincipal));
		//								dblTaxNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColTax));
		//								if (dblPLINeed > 0)
		//								{
		//									if (dblPLINeed <= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest)))
		//									{
		//										dblPLINeed = 0;
		//										dblIntNeed -= (FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest)) - dblPLINeed);
		//									}
		//									else
		//									{
		//										dblPLINeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest));
		//									}
		//								}
		//								else
		//								{
		//									dblIntNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest));
		//								}
		//								dblCostNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColCosts));
		//							}
		//							lngRWCT -= 1;
		//						}
		//						//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//						//if ((BillCode != "L" || (rsTemp.Get_Fields(strTempPayCode + "CombinationLienKey") == rsTemp.Get_Fields("Bill"))) && strOppTempPayCode != Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Items[frmUTCLStatus.InstancePtr.cmbService.SelectedIndex].ToString(), 1))
		//						// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//						if ((BillCode != "L" || (rsTemp.Get_Fields(strTempPayCode + "CombinationLienKey") == rsTemp.Get_Fields("Bill"))) && strOppTempPayCode != Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Text, 1))
		//						{
		//							if ((dblPrinNeed > 0 || dblTaxNeed > 0 || dblIntNeed > 0) && (dblCostNeed + dblPrinNeed + dblTaxNeed + dblIntNeed + dblCostNeed + dblPLINeed) > 0)
		//							{
		//								if (dblPrinNeed + dblTaxNeed + dblIntNeed + dblCostNeed + dblPLINeed != 0)
		//								{
		//									// if it is actually a payment then
		//									if (intCT < MAX_UTPAYMENTS)
		//									{
		//										dblTempP = 0;
		//										dblTempT = 0;
		//										dblTempI = 0;
		//										dblTempC = 0;
		//										dblTempPI = 0;
		//										// MAL@20071012
		//										dblInt = dblTot;
		//										// these are the money fields
		//										// AdjustUTPaymentAmounts dblPrin, dblTax, dblInt, dblCost, dblPrinNeed, dblTaxNeed, dblIntNeed, dblCostNeed, dblTempP, dblTempT, dblTempI, dblTempC
		//										AdjustUTPaymentAmounts(ref dblPrin, ref dblTax, ref dblInt, ref dblCost, ref dblPrinNeed, ref dblTaxNeed, ref dblIntNeed, ref dblCostNeed, ref dblTempP, ref dblTempT, ref dblTempI, ref dblTempC, dblPLI, dblPLINeed, dblTempPI);
		//										Statics.UTPaymentArray[intCT].Principal = FCConvert.ToDecimal(dblTempP);
		//										Statics.UTPaymentArray[intCT].Tax = FCConvert.ToDecimal(dblTempT);
		//										Statics.UTPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(dblTempI);
		//										Statics.UTPaymentArray[intCT].LienCost = FCConvert.ToDecimal(dblTempC);
		//										Statics.UTPaymentArray[intCT].Service = strTempPayCode;
		//										Statics.UTPaymentArray[intCT].PreLienInterest = FCConvert.ToDecimal(dblTempPI);
		//										// MAL@20071012
		//										dblTot = modGlobal.Round(dblTot - dblTempP - dblTempT - dblTempI - dblTempC - dblTempPI, 2);
		//										// this adds it to the array of payments
		//										Statics.UTPaymentArray[intCT].Account = frmUTCLStatus.InstancePtr.CurrentAccountKey;
		//										Statics.UTPaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//										Statics.UTPaymentArray[intCT].BillCode = BillCode;
		//										// this is either (R)egular or (L)ien
		//										if (BillCode == "L")
		//										{
		//											boolLienPayment = true;
		//											Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
		//											// kk05312016 trocrs-51  Does this keep getting changed?   'rsLienRec.Fields("LienRec")
		//										}
		//										else
		//										{
		//											boolLienPayment = false;
		//											// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//											Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields("Bill"));
		//										}
		//										Statics.UTPaymentArray[intCT].CashDrawer = frmUTCLStatus.InstancePtr.txtCD.Text;
		//										if (Statics.UTPaymentArray[intCT].CashDrawer == "N")
		//										{
		//											// kk06092014 TROCRS-27  Need to remove all "_" from the account number or it isn't detected
		//											Statics.UTPaymentArray[intCT].BudgetaryAccountNumber = frmUTCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0).Replace("_", "");
		//											// frmUTCLStatus.txtAcctNumber.TextMatrix(0, 0)
		//										}
		//										else
		//										{
		//											Statics.UTPaymentArray[intCT].BudgetaryAccountNumber = "";
		//										}
		//										Statics.UTPaymentArray[intCT].Code = strType;
		//										Statics.UTPaymentArray[intCT].Comments = frmUTCLStatus.InstancePtr.txtComments.Text;
		//										Statics.UTPaymentArray[intCT].EffectiveInterestDate = Statics.UTEffectiveDate;
		//										Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(frmUTCLStatus.InstancePtr.txtTransactionDate.Text);
		//										Statics.UTPaymentArray[intCT].GeneralLedger = frmUTCLStatus.InstancePtr.txtCash.Text;
		//										// .PreLienInterest = 0
		//										Statics.UTPaymentArray[intCT].ReceiptNumber = "P";
		//										if (frmUTCLStatus.InstancePtr.txtTransactionDate.Text == "")
		//										{
		//											Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//										}
		//										else
		//										{
		//											Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmUTCLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//										}
		//										Statics.UTPaymentArray[intCT].Reference = Strings.Trim(frmUTCLStatus.InstancePtr.txtReference.Text + " ");
		//										Statics.UTPaymentArray[intCT].ReversalID = -1;
		//										// .Teller =
		//										// .TransNumber=
		//										// .PaidBy =
		//										if (boolCalcInterest && lngReversalKey != 0)
		//										{
		//											// this is where the CHGINT Number needs to be put in the
		//											// lngReversalKey is the payment ID of the payment being reversed
		//											// intCT is the array index of the reversal payment
		//											// AddCHGINTToList frmRECLStatus.vsPayments.rows, lngReversalKey, True, intCT
		//										}
		//										else if (FCConvert.CBool(Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intTempType] != 0))
		//										{
		//											// AddCHGINTToList frmRECLStatus.vsPayments.rows, , True
		//											// this will create a CHGINT line for normal payments
		//											if (boolLienPayment)
		//											{
		//												Statics.UTPaymentArray[intCT].CHGINTNumber = CreateUTCHGINTLine_18(intCT, ref Statics.UTPaymentArray[intCT].EffectiveInterestDate, FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber")), boolAddToOtherGrid);
		//											}
		//											else
		//											{
		//												Statics.UTPaymentArray[intCT].CHGINTNumber = CreateUTCHGINTLine_9(intCT, ref Statics.UTPaymentArray[intCT].EffectiveInterestDate, boolAddToOtherGrid);
		//											}
		//										}
		//										// .Year = lngBill
		//										frmUTCLStatus.InstancePtr.FillPaymentGrid(cRow, intCT, Statics.UTPaymentArray[intCT].BillNumber);
		//										// actually fills the number into the payment grid
		//									}
		//									else
		//									{
		//										MessageBox.Show("You have reached your maximum payments per screen.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//									}
		//								}
		//								else if (dblPrinNeed + dblTaxNeed + dblIntNeed + dblCostNeed < 0)
		//								{
		//									// add the negative to the total used to pay of later payments
		//									// add a payment that will make this bill 0
		//								}
		//							}
		//							else
		//							{
		//								Statics.UTPaymentArray[intCT].BillKey = 0;
		//								Statics.UTPaymentArray[intCT].Account = 0;
		//							}
		//						}
		//					}
		//					rsTemp.MoveNext();
		//				}
		//			}
		//			else
		//			{
		//				while (!(rsTemp.EndOfFile() || dblTot <= 0))
		//				{
		//					boolNoLoop = false;
		//					for (intCT = 1; intCT <= MAX_UTPAYMENTS; intCT++)
		//					{
		//						// this finds a place in the array
		//						if (Statics.UTPaymentArray[intCT].Account == 0)
		//						{
		//							// to store the new payment record
		//							break;
		//						}
		//					}
		//					if (rsTemp.EndOfFile() != true)
		//					{
		//						// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//						lngBill = FCConvert.ToInt32(rsTemp.Get_Fields("Bill"));
		//						lngBillNumber = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber"));
		//					}
		//					// set the res code
		//					Statics.UTPaymentArray[intCT].ResCode = frmUTCLStatus.InstancePtr.lngUTResCode;
		//					if (Statics.TownService == "B")
		//					{
		//						if (Statics.gboolPayWaterFirst)
		//						{
		//							strTempPayCode = "W";
		//							strOppTempPayCode = "S";
		//							intTempType = 0;
		//						}
		//						else
		//						{
		//							strTempPayCode = "S";
		//							strOppTempPayCode = "W";
		//							intTempType = 1;
		//						}
		//					}
		//					else
		//					{
		//						strTempPayCode = PayCode;
		//						if (strTempPayCode == "W")
		//						{
		//							intTempType = 0;
		//						}
		//						else
		//						{
		//							intTempType = 1;
		//						}
		//					}
		//					// get the next year and its total due values
		//					if (FCConvert.ToInt32(rsTemp.Get_Fields(strTempPayCode + "LienRecordNumber")) == 0)
		//					{
		//						if (((!boolExcessPayment && lngBillNumber != 0) || (boolExcessPayment && lngBillNumber == 0)) || !boolAddToOtherGrid)
		//						{
		//							Statics.UTPaymentArray[intCT].BillNumber = lngBillNumber;
		//							Statics.UTPaymentArray[intCT].BillKey = lngBill;
		//							BillCode = "R";
		//							dblPrinNeed = modGlobal.Round(rsTemp.Get_Fields(strTempPayCode + "PrinOwed") - rsTemp.Get_Fields(strTempPayCode + "PrinPaid"), 2);
		//							dblTaxNeed = modGlobal.Round(rsTemp.Get_Fields(strTempPayCode + "TaxOwed") - rsTemp.Get_Fields(strTempPayCode + "TaxPaid"), 2);
		//							dblIntNeed = modGlobal.Round((FCConvert.ToDouble(rsTemp.Get_Fields(strTempPayCode + "IntAdded")) * -1) + rsTemp.Get_Fields(strTempPayCode + "IntOwed") - rsTemp.Get_Fields(strTempPayCode + "IntPaid") - Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intTempType], 2);
		//							dblCostNeed = modGlobal.Round(rsTemp.Get_Fields(strTempPayCode + "CostOwed") - rsTemp.Get_Fields(strTempPayCode + "CostAdded") - rsTemp.Get_Fields(strTempPayCode + "CostPaid"), 2);
		//							dblPLINeed = 0;
		//						}
		//					}
		//					else
		//					{
		//						BillCode = "L";
		//						rsLienRec.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTemp.Get_Fields(strWS + "LienRecordNumber"), modExtraModules.strUTDatabase);
		//						if (rsLienRec.RecordCount() != 0)
		//						{
		//							Statics.UTPaymentArray[intCT].BillNumber = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("RateKey"));
		//							Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields(strWS + "LienRecordNumber"));
		//							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//							dblPrinNeed = modGlobal.Round(rsLienRec.Get_Fields("Principal") - rsLienRec.Get_Fields("PrinPaid"), 2);
		//							// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//							dblTaxNeed = modGlobal.Round(rsLienRec.Get_Fields("Tax") - rsLienRec.Get_Fields("TaxPaid"), 2);
		//							// MAL@20071012: Changed to break out PLI and Interest
		//							// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//							dblIntNeed = modGlobal.Round(((FCConvert.ToDouble(rsLienRec.Get_Fields("IntAdded")) * -1) - FCConvert.ToDouble(rsLienRec.Get_Fields("IntPaid"))) - Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intTempType], 2);
		//							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//							dblPLINeed = modGlobal.Round((rsLienRec.Get_Fields("Interest") - rsLienRec.Get_Fields("PLIPaid")), 2);
		//							// dblIntNeed = Round(((rsLienRec.Fields("IntAdded") * -1) - rsLienRec.Fields("IntPaid")) + rsLienRec.Fields("Interest") - rsLienRec.Fields("PLIPaid") - dblUTCurrentInt(rsTemp.Fields("BillNumber"), intTempType), 2)
		//							// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//							dblCostNeed = modGlobal.Round(rsLienRec.Get_Fields("Costs") - rsLienRec.Get_Fields("MaturityFee") - rsLienRec.Get_Fields_Double("CostPaid"), 2);
		//						}
		//					}
		//					// kk01162016 trout-1037  Check pending payments here
		//					lngRWCT = frmUTCLStatus.InstancePtr.vsPayments.Rows - 1;
		//					while (!(lngRWCT <= 0))
		//					{
		//						if (FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColBill)) == lngBillNumber && Strings.UCase(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColService)) == strTempPayCode)
		//						{
		//							// adjust the need
		//							dblPrinNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColPrincipal));
		//							dblTaxNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColTax));
		//							if (dblPLINeed > 0)
		//							{
		//								if (dblPLINeed <= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest)))
		//								{
		//									dblPLINeed = 0;
		//									dblIntNeed -= (FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest)) - dblPLINeed);
		//								}
		//								else
		//								{
		//									dblPLINeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest));
		//								}
		//							}
		//							else
		//							{
		//								dblIntNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest));
		//							}
		//							dblCostNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColCosts));
		//						}
		//						lngRWCT -= 1;
		//					}
		//					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//					//if ((BillCode != "L" || (rsTemp.Get_Fields(strTempPayCode + "CombinationLienKey") == rsTemp.Get_Fields("Bill"))) && strOppTempPayCode != Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Items[frmUTCLStatus.InstancePtr.cmbService.SelectedIndex].ToString(), 1))
		//					// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//					if ((BillCode != "L" || (rsTemp.Get_Fields(strTempPayCode + "CombinationLienKey") == rsTemp.Get_Fields("Bill"))) && strOppTempPayCode != Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Text, 1))
		//					{
		//						if ((dblPrinNeed > 0 || dblTaxNeed > 0 || dblIntNeed > 0) && (dblPrinNeed + dblTaxNeed + dblIntNeed + dblCostNeed + dblPLINeed) > 0)
		//						{
		//							if (dblPrinNeed + dblTaxNeed + dblIntNeed + dblCostNeed + dblPLINeed != 0)
		//							{
		//								// if it is actually a payment then
		//								if (intCT < MAX_UTPAYMENTS)
		//								{
		//									dblTempP = 0;
		//									dblTempT = 0;
		//									dblTempI = 0;
		//									dblTempC = 0;
		//									dblTempPI = 0;
		//									// MAL@20071012
		//									dblInt = dblTot;
		//									// these are the money fields
		//									// AdjustUTPaymentAmounts dblPrin, dblTax, dblInt, dblCost, dblPrinNeed, dblTaxNeed, dblIntNeed, dblCostNeed, dblTempP, dblTempT, dblTempI, dblTempC
		//									AdjustUTPaymentAmounts(ref dblPrin, ref dblTax, ref dblInt, ref dblCost, ref dblPrinNeed, ref dblTaxNeed, ref dblIntNeed, ref dblCostNeed, ref dblTempP, ref dblTempT, ref dblTempI, ref dblTempC, dblPLI, dblPLINeed, dblTempPI);
		//									Statics.UTPaymentArray[intCT].Principal = FCConvert.ToDecimal(dblTempP);
		//									Statics.UTPaymentArray[intCT].Tax = FCConvert.ToDecimal(dblTempT);
		//									Statics.UTPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(dblTempI);
		//									Statics.UTPaymentArray[intCT].LienCost = FCConvert.ToDecimal(dblTempC);
		//									Statics.UTPaymentArray[intCT].Service = strTempPayCode;
		//									Statics.UTPaymentArray[intCT].PreLienInterest = FCConvert.ToDecimal(dblTempPI);
		//									// MAL@20071012
		//									dblTot = modGlobal.Round(dblTot - dblTempP - dblTempT - dblTempI - dblTempC - dblTempPI, 2);
		//									// this adds it to the array of payments
		//									Statics.UTPaymentArray[intCT].Account = frmUTCLStatus.InstancePtr.CurrentAccountKey;
		//									Statics.UTPaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//									Statics.UTPaymentArray[intCT].BillCode = BillCode;
		//									// this is either (R)egular or (L)ien
		//									if (BillCode == "L")
		//									{
		//										boolLienPayment = true;
		//										Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
		//										// kk05312016 trocrs-51  Does this keep getting changed?   'rsLienRec.Fields("LienKey")
		//									}
		//									else
		//									{
		//										boolLienPayment = false;
		//										// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//										Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields("Bill"));
		//									}
		//									Statics.UTPaymentArray[intCT].CashDrawer = frmUTCLStatus.InstancePtr.txtCD.Text;
		//									if (Statics.UTPaymentArray[intCT].CashDrawer == "N")
		//									{
		//										// kk06092014 TROCRS-27  Need to remove all "_" from the account number or it isn't detected
		//										Statics.UTPaymentArray[intCT].BudgetaryAccountNumber = frmUTCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0).Replace("_", "");
		//										// frmUTCLStatus.txtAcctNumber.TextMatrix(0, 0)
		//									}
		//									else
		//									{
		//										Statics.UTPaymentArray[intCT].BudgetaryAccountNumber = "";
		//									}
		//									Statics.UTPaymentArray[intCT].Code = strType;
		//									Statics.UTPaymentArray[intCT].Comments = frmUTCLStatus.InstancePtr.txtComments.Text;
		//									Statics.UTPaymentArray[intCT].EffectiveInterestDate = Statics.UTEffectiveDate;
		//									Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(frmUTCLStatus.InstancePtr.txtTransactionDate.Text);
		//									Statics.UTPaymentArray[intCT].GeneralLedger = frmUTCLStatus.InstancePtr.txtCash.Text;
		//									// .PreLienInterest = 0
		//									Statics.UTPaymentArray[intCT].ReceiptNumber = "P";
		//									if (frmUTCLStatus.InstancePtr.txtTransactionDate.Text == "")
		//									{
		//										Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//									}
		//									else
		//									{
		//										Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmUTCLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//									}
		//									Statics.UTPaymentArray[intCT].Reference = Strings.Trim(frmUTCLStatus.InstancePtr.txtReference.Text + " ");
		//									Statics.UTPaymentArray[intCT].ReversalID = -1;
		//									// .Teller =
		//									// .TransNumber=
		//									// .PaidBy =
		//									if (boolCalcInterest && lngReversalKey != 0)
		//									{
		//										// this is where the CHGINT Number needs to be put in the
		//										// lngReversalKey is the payment ID of the payment being reversed
		//										// intCT is the array index of the reversal payment
		//										// AddCHGINTToList frmRECLStatus.vsPayments.rows, lngReversalKey, True, intCT
		//									}
		//									else if (FCConvert.CBool(Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intTempType] != 0))
		//									{
		//										// AddCHGINTToList frmRECLStatus.vsPayments.rows, , True
		//										// this will create a CHGINT line for normal payments
		//										if (boolLienPayment)
		//										{
		//											Statics.UTPaymentArray[intCT].CHGINTNumber = CreateUTCHGINTLine_18(intCT, ref Statics.UTPaymentArray[intCT].EffectiveInterestDate, FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber")), boolAddToOtherGrid);
		//										}
		//										else
		//										{
		//											Statics.UTPaymentArray[intCT].CHGINTNumber = CreateUTCHGINTLine_9(intCT, ref Statics.UTPaymentArray[intCT].EffectiveInterestDate, boolAddToOtherGrid);
		//										}
		//									}
		//									// .Year = lngBill
		//									frmUTCLStatus.InstancePtr.FillPaymentGrid(cRow, intCT, Statics.UTPaymentArray[intCT].BillNumber);
		//									// actually fills the number into the payment grid
		//								}
		//								else
		//								{
		//									MessageBox.Show("You have reached your maximum payments per screen.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//								}
		//							}
		//							else if (dblPrinNeed + dblTaxNeed + dblIntNeed + dblCostNeed < 0)
		//							{
		//								// add the negative to the total used to pay of later payments
		//								// add a payment that will make this bill 0
		//							}
		//						}
		//						else
		//						{
		//							Statics.UTPaymentArray[intCT].BillKey = 0;
		//							Statics.UTPaymentArray[intCT].Account = 0;
		//						}
		//					}
		//					rsTemp.MoveNext();
		//				}
		//				rsTemp.MoveFirst();
		//				// now check the other service of this same bill if the town has both services
		//				if (Statics.TownService == "B" && dblTot > 0)
		//				{
		//					while (!(rsTemp.EndOfFile() || dblTot <= 0))
		//					{
		//						for (intCT = 1; intCT <= MAX_UTPAYMENTS; intCT++)
		//						{
		//							// this finds a place in the array
		//							if (Statics.UTPaymentArray[intCT].Account == 0)
		//							{
		//								// to store the new payment record
		//								break;
		//							}
		//						}
		//						if (rsTemp.EndOfFile() != true)
		//						{
		//							// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//							lngBill = FCConvert.ToInt32(rsTemp.Get_Fields("Bill"));
		//							lngBillNumber = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber"));
		//						}
		//						if (Statics.gboolPayWaterFirst)
		//						{
		//							strTempPayCode = "S";
		//							strOppTempPayCode = "W";
		//							intTempType = 1;
		//						}
		//						else
		//						{
		//							strTempPayCode = "W";
		//							strOppTempPayCode = "S";
		//							intTempType = 0;
		//						}
		//						// get the next year and its total due values
		//						if (FCConvert.ToInt32(rsTemp.Get_Fields(strTempPayCode + "LienRecordNumber")) == 0)
		//						{
		//							if (((!boolExcessPayment && lngBillNumber != 0) || (boolExcessPayment && lngBillNumber == 0)) || !boolAddToOtherGrid)
		//							{
		//								Statics.UTPaymentArray[intCT].BillNumber = lngBillNumber;
		//								Statics.UTPaymentArray[intCT].BillKey = lngBill;
		//								BillCode = "R";
		//								dblPrinNeed = modGlobal.Round(rsTemp.Get_Fields(strTempPayCode + "PrinOwed") - rsTemp.Get_Fields(strTempPayCode + "PrinPaid"), 2);
		//								dblTaxNeed = modGlobal.Round(rsTemp.Get_Fields(strTempPayCode + "TaxOwed") - rsTemp.Get_Fields(strTempPayCode + "TaxPaid"), 2);
		//								dblIntNeed = modGlobal.Round((FCConvert.ToDouble(rsTemp.Get_Fields(strTempPayCode + "IntAdded")) * -1) + rsTemp.Get_Fields(strTempPayCode + "IntOwed") - rsTemp.Get_Fields(strTempPayCode + "IntPaid") - Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intTempType], 2);
		//								dblCostNeed = modGlobal.Round(rsTemp.Get_Fields(strTempPayCode + "CostOwed") - rsTemp.Get_Fields(strTempPayCode + "CostAdded") - rsTemp.Get_Fields(strTempPayCode + "CostPaid"), 2);
		//								dblPLINeed = 0;
		//							}
		//						}
		//						else
		//						{
		//							BillCode = "L";
		//							rsLienRec.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTemp.Get_Fields(strTempPayCode + "LienRecordNumber"), modExtraModules.strUTDatabase);
		//							if (rsLienRec.RecordCount() != 0)
		//							{
		//								Statics.UTPaymentArray[intCT].BillNumber = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("RateKey"));
		//								Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields(strWS + "LienRecordNumber"));
		//								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//								dblPrinNeed = modGlobal.Round(rsLienRec.Get_Fields("Principal") - rsLienRec.Get_Fields("PrinPaid"), 2);
		//								// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//								dblTaxNeed = modGlobal.Round(rsLienRec.Get_Fields("Tax") - rsLienRec.Get_Fields("TaxPaid"), 2);
		//								// MAL@20071012: Changed to break out PLI and Interest
		//								// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//								dblIntNeed = modGlobal.Round(((FCConvert.ToDouble(rsLienRec.Get_Fields("IntAdded")) * -1) - FCConvert.ToDouble(rsLienRec.Get_Fields("IntPaid"))) - Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intTempType], 2);
		//								// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//								dblPLINeed = modGlobal.Round((rsLienRec.Get_Fields("Interest") - rsLienRec.Get_Fields("PLIPaid")), 2);
		//								// dblIntNeed = Round(((rsLienRec.Fields("IntAdded") * -1) - rsLienRec.Fields("IntPaid")) + rsLienRec.Fields("Interest") - rsLienRec.Fields("PLIPaid") - dblUTCurrentInt(rsTemp.Fields("BillNumber"), intTempType), 2)
		//								// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//								dblCostNeed = modGlobal.Round(rsLienRec.Get_Fields("Costs") - rsLienRec.Get_Fields("MaturityFee") - rsLienRec.Get_Fields_Double("CostPaid"), 2);
		//							}
		//						}
		//						// kk01162016 trout-1037  Check pending payments here
		//						lngRWCT = frmUTCLStatus.InstancePtr.vsPayments.Rows - 1;
		//						while (!(lngRWCT <= 0))
		//						{
		//							if (FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColBill)) == lngBillNumber && Strings.UCase(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColService)) == strTempPayCode)
		//							{
		//								// adjust the need
		//								dblPrinNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColPrincipal));
		//								dblTaxNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColTax));
		//								if (dblPLINeed > 0)
		//								{
		//									if (dblPLINeed <= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest)))
		//									{
		//										dblPLINeed = 0;
		//										dblIntNeed -= (FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest)) - dblPLINeed);
		//									}
		//									else
		//									{
		//										dblPLINeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest));
		//									}
		//								}
		//								else
		//								{
		//									dblIntNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest));
		//								}
		//								dblCostNeed -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRWCT, frmUTCLStatus.InstancePtr.lngPayGridColCosts));
		//							}
		//							lngRWCT -= 1;
		//						}
		//						//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//						//if ((BillCode != "L" || (rsTemp.Get_Fields(strTempPayCode + "CombinationLienKey") == rsTemp.Get_Fields("Bill"))) && strOppTempPayCode != Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Items[frmUTCLStatus.InstancePtr.cmbService.SelectedIndex].ToString(), 1))
		//						// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//						if ((BillCode != "L" || (rsTemp.Get_Fields(strTempPayCode + "CombinationLienKey") == rsTemp.Get_Fields("Bill"))) && strOppTempPayCode != Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Text, 1))
		//						{
		//							if ((dblPrinNeed > 0 || dblTaxNeed > 0 || dblIntNeed > 0) && (dblCostNeed + dblPrinNeed + dblTaxNeed + dblIntNeed + dblCostNeed + dblPLINeed) > 0)
		//							{
		//								if (dblPrinNeed + dblTaxNeed + dblIntNeed + dblCostNeed + dblPLINeed != 0)
		//								{
		//									// if it is actually a payment then
		//									if (intCT < MAX_UTPAYMENTS)
		//									{
		//										dblTempP = 0;
		//										dblTempT = 0;
		//										dblTempI = 0;
		//										dblTempC = 0;
		//										dblTempPI = 0;
		//										// MAL@20071012
		//										dblInt = dblTot;
		//										// these are the money fields
		//										// AdjustUTPaymentAmounts dblPrin, dblTax, dblInt, dblCost, dblPrinNeed, dblTaxNeed, dblIntNeed, dblCostNeed, dblTempP, dblTempT, dblTempI, dblTempC
		//										AdjustUTPaymentAmounts(ref dblPrin, ref dblTax, ref dblInt, ref dblCost, ref dblPrinNeed, ref dblTaxNeed, ref dblIntNeed, ref dblCostNeed, ref dblTempP, ref dblTempT, ref dblTempI, ref dblTempC, dblPLI, dblPLINeed, dblTempPI);
		//										Statics.UTPaymentArray[intCT].Principal = FCConvert.ToDecimal(dblTempP);
		//										Statics.UTPaymentArray[intCT].Tax = FCConvert.ToDecimal(dblTempT);
		//										Statics.UTPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(dblTempI);
		//										Statics.UTPaymentArray[intCT].LienCost = FCConvert.ToDecimal(dblTempC);
		//										Statics.UTPaymentArray[intCT].Service = strTempPayCode;
		//										Statics.UTPaymentArray[intCT].PreLienInterest = FCConvert.ToDecimal(dblTempPI);
		//										// MAL@20071012
		//										dblTot = modGlobal.Round(dblTot - dblTempP - dblTempT - dblTempI - dblTempC - dblTempPI, 2);
		//										// this adds it to the array of payments
		//										Statics.UTPaymentArray[intCT].Account = frmUTCLStatus.InstancePtr.CurrentAccountKey;
		//										Statics.UTPaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//										Statics.UTPaymentArray[intCT].BillCode = BillCode;
		//										// this is either (R)egular or (L)ien
		//										if (BillCode == "L")
		//										{
		//											boolLienPayment = true;
		//											Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
		//											// kk05312016 trocrs-51  Does this keep getting changed?   'rsLienRec.Fields("LienKey")
		//										}
		//										else
		//										{
		//											boolLienPayment = false;
		//											// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//											Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields("Bill"));
		//										}
		//										Statics.UTPaymentArray[intCT].CashDrawer = frmUTCLStatus.InstancePtr.txtCD.Text;
		//										if (Statics.UTPaymentArray[intCT].CashDrawer == "N")
		//										{
		//											// kk06092014 TROCRS-27  Need to remove all "_" from the account number or it isn't detected
		//											Statics.UTPaymentArray[intCT].BudgetaryAccountNumber = frmUTCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0).Replace("_", "");
		//											// frmUTCLStatus.txtAcctNumber.TextMatrix(0, 0)
		//										}
		//										else
		//										{
		//											Statics.UTPaymentArray[intCT].BudgetaryAccountNumber = "";
		//										}
		//										Statics.UTPaymentArray[intCT].Code = strType;
		//										Statics.UTPaymentArray[intCT].Comments = frmUTCLStatus.InstancePtr.txtComments.Text;
		//										Statics.UTPaymentArray[intCT].EffectiveInterestDate = Statics.UTEffectiveDate;
		//										Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(frmUTCLStatus.InstancePtr.txtTransactionDate.Text);
		//										Statics.UTPaymentArray[intCT].GeneralLedger = frmUTCLStatus.InstancePtr.txtCash.Text;
		//										// .PreLienInterest = 0
		//										Statics.UTPaymentArray[intCT].ReceiptNumber = "P";
		//										if (frmUTCLStatus.InstancePtr.txtTransactionDate.Text == "")
		//										{
		//											Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//										}
		//										else
		//										{
		//											Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmUTCLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//										}
		//										Statics.UTPaymentArray[intCT].Reference = Strings.Trim(frmUTCLStatus.InstancePtr.txtReference.Text + " ");
		//										Statics.UTPaymentArray[intCT].ReversalID = -1;
		//										// .Teller =
		//										// .TransNumber=
		//										// .PaidBy =
		//										if (boolCalcInterest && lngReversalKey != 0)
		//										{
		//											// this is where the CHGINT Number needs to be put in the
		//											// lngReversalKey is the payment ID of the payment being reversed
		//											// intCT is the array index of the reversal payment
		//											// AddCHGINTToList frmRECLStatus.vsPayments.rows, lngReversalKey, True, intCT
		//										}
		//										else if (FCConvert.CBool(Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intTempType] != 0))
		//										{
		//											// AddCHGINTToList frmRECLStatus.vsPayments.rows, , True
		//											// this will create a CHGINT line for normal payments
		//											if (boolLienPayment)
		//											{
		//												Statics.UTPaymentArray[intCT].CHGINTNumber = CreateUTCHGINTLine_18(intCT, ref Statics.UTPaymentArray[intCT].EffectiveInterestDate, FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber")), boolAddToOtherGrid);
		//											}
		//											else
		//											{
		//												Statics.UTPaymentArray[intCT].CHGINTNumber = CreateUTCHGINTLine_9(intCT, ref Statics.UTPaymentArray[intCT].EffectiveInterestDate, boolAddToOtherGrid);
		//											}
		//										}
		//										// .Year = lngBill
		//										frmUTCLStatus.InstancePtr.FillPaymentGrid(cRow, intCT, Statics.UTPaymentArray[intCT].BillNumber);
		//										// actually fills the number into the payment grid
		//									}
		//									else
		//									{
		//										MessageBox.Show("You have reached your maximum payments per screen.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//									}
		//								}
		//								else if (dblPrinNeed + dblTaxNeed + dblIntNeed + dblCostNeed < 0)
		//								{
		//									// add the negative to the total used to pay of later payments
		//									// add a payment that will make this bill 0
		//								}
		//							}
		//							else
		//							{
		//								Statics.UTPaymentArray[intCT].BillKey = 0;
		//								Statics.UTPaymentArray[intCT].Account = 0;
		//							}
		//						}
		//						rsTemp.MoveNext();
		//					}
		//				}
		//			}
		//			if (boolNoLoop)
		//			{
		//				for (intCT = 1; intCT <= MAX_UTPAYMENTS; intCT++)
		//				{
		//					// this finds a place in the array
		//					if (Statics.UTPaymentArray[intCT].Account == 0)
		//					{
		//						// to store the new payment record
		//						executeSTARTNOLOOP = true;
		//						goto STARTNOLOOP;
		//					}
		//				}
		//				// frmUTCLStatus.FillPaymentGrid cRow, intCT, lngBill
		//			}
		//			if (rsTemp.EndOfFile() && modGlobal.Round(dblTot, 2) > 0)
		//			{
		//				// have to put the excess money in the most current year
		//				int intLastYear;
		//				if (boolExcessPayment)
		//				{
		//					if (Statics.TownService == "B")
		//					{
		//						//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//						//if (Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Items[frmUTCLStatus.InstancePtr.cmbService.SelectedIndex].ToString(), 1) == "B")
		//						if (Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Text, 1) == "B")
		//						{
		//							if (Statics.gboolPayWaterFirst)
		//							{
		//								if (frmUTCLStatus.InstancePtr.WGRID.Rows > 1)
		//								{
		//									intLastYear = CreateExcessUTPaymentRecord_69(dblTot, true, true);
		//								}
		//								else
		//								{
		//									intLastYear = CreateExcessUTPaymentRecord_69(dblTot, false, true);
		//								}
		//							}
		//							else if (!Statics.gboolPayWaterFirst)
		//							{
		//								if (frmUTCLStatus.InstancePtr.SGRID.Rows > 1)
		//								{
		//									intLastYear = CreateExcessUTPaymentRecord_69(dblTot, false, true);
		//								}
		//								else
		//								{
		//									intLastYear = CreateExcessUTPaymentRecord_69(dblTot, true, true);
		//								}
		//							}
		//							else
		//							{
		//								intLastYear = CreateExcessUTPaymentRecord_63(dblTot, Statics.gboolPayWaterFirst, true);
		//							}
		//						}
		//						//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//						//else if (Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Items[frmUTCLStatus.InstancePtr.cmbService.SelectedIndex].ToString(), 1) == "W")
		//						else if (Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Text, 1) == "W")
		//						{
		//							intLastYear = CreateExcessUTPaymentRecord_69(dblTot, true, true);
		//						}
		//							//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//							//else if (Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Items[frmUTCLStatus.InstancePtr.cmbService.SelectedIndex].ToString(), 1) == "S")
		//							else if (Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Text, 1) == "S")
		//						{
		//							intLastYear = CreateExcessUTPaymentRecord_69(dblTot, false, true);
		//						}
		//					}
		//					else if (Statics.TownService == "S")
		//					{
		//						intLastYear = CreateExcessUTPaymentRecord_69(dblTot, false, true);
		//					}
		//					else
		//					{
		//						intLastYear = CreateExcessUTPaymentRecord_69(dblTot, true, true);
		//					}
		//					MessageBox.Show("Amount paid in excess of total bill amount : " + Strings.Format(dblTot, "$#,##0.00") + "\r\n" + "If this is incorrect please delete the payments manually.", "Excess of Bill Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//				}
		//				else
		//				{
		//					if (Statics.TownService == "B")
		//					{
		//						if (!boolWater)
		//						{
		//							// check the sewer total
		//							if (frmUTCLStatus.InstancePtr.SGRID.Rows > 1)
		//							{
		//								boolExcessPayment = FCConvert.ToDouble(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(frmUTCLStatus.InstancePtr.SGRID.Rows - 1, frmUTCLStatus.InstancePtr.lngGRIDColTotal)) >= 0;
		//							}
		//							else
		//							{
		//								boolExcessPayment = false;
		//							}
		//						}
		//						else
		//						{
		//							// check the water total
		//							if (frmUTCLStatus.InstancePtr.WGRID.Rows > 1)
		//							{
		//								boolExcessPayment = FCConvert.ToDouble(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(frmUTCLStatus.InstancePtr.WGRID.Rows - 1, frmUTCLStatus.InstancePtr.lngGRIDColTotal)) >= 0;
		//							}
		//							else
		//							{
		//								boolExcessPayment = false;
		//							}
		//						}
		//						if (boolExcessPayment)
		//						{
		//							if (boolAddToOtherGrid)
		//							{
		//								intLastYear = CreateExcessUTPaymentRecord_72(dblTot, boolWater, true, true);
		//							}
		//							else
		//							{
		//								if (MessageBox.Show("Would you like to apply the excess " + Strings.Format(dblTot, "$#,##0.00") + " to the " + strOppType + " bills?", "Excess Amount", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
		//								{
		//									intLastYear = CreateExcessUTPaymentRecord_78(dblTot, !boolWater, true, true);
		//								}
		//								else
		//								{
		//									intLastYear = CreateExcessUTPaymentRecord_63(dblTot, boolWater, true);
		//									MessageBox.Show("Amount paid in excess of total bill amount: " + Strings.Format(dblTot, "$#,##0.00"), "Excess of Bill Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//								}
		//							}
		//						}
		//					}
		//					else
		//					{
		//						intLastYear = CreateExcessUTPaymentRecord_72(dblTot, boolWater, false, true);
		//					}
		//				}
		//			}
		//			frmUTCLStatus.InstancePtr.txtPrincipal.Text = "0.00";
		//			frmUTCLStatus.InstancePtr.txtTax.Text = "0.00";
		//			frmUTCLStatus.InstancePtr.txtInterest.Text = "0.00";
		//			frmUTCLStatus.InstancePtr.txtCosts.Text = "0.00";
		//			// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
		//			// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
		//			// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------
		//		}
		//		else
		//		{
		//			// if a bill is choosen
		//			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//			//boolBothService = Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Items[frmUTCLStatus.InstancePtr.cmbService.SelectedIndex].ToString(), 1) == "B";
		//			boolBothService = Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Text, 1) == "B";
		//			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//			//if (strType == "Y" || Strings.InStr(1, frmUTCLStatus.InstancePtr.cmbBillNumber.Items[frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString(), "*", CompareConstants.vbBinaryCompare) != 0)
		//			if (strType == "Y" || Strings.InStr(1, frmUTCLStatus.InstancePtr.cmbBillNumber.Text, "*", CompareConstants.vbBinaryCompare) != 0)
		//			{
		//				// if this is a prepayment
		//				//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//				//lngBill = PrePayUTYear_2(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString()); // use the correct year (next year)
		//				lngBill = PrePayUTYear_2(frmUTCLStatus.InstancePtr.cmbBillNumber.Text);
		//				// use the correct year (next year)
		//				// Doevents
		//				//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//				//strType = Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Items[frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex].ToString(), 1);
		//				strType = Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Text, 1);
		//			}
		//			else
		//			{
		//				// else use the bill choosen
		//				//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//				//if (Conversion.Val(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString()) > 0)
		//				if (Conversion.Val(frmUTCLStatus.InstancePtr.cmbBillNumber.Text) > 0)
		//				{
		//					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//					//lngBill = FCConvert.ToInt32(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString());
		//					lngBill = FCConvert.ToInt32(frmUTCLStatus.InstancePtr.cmbBillNumber.Text);
		//				}
		//				else
		//				{
		//					MessageBox.Show("Please select a bill or use the prepayment (Y) option.", "Error Checking Bill.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//					return AddUTPaymentToList;
		//				}
		//			}
		//			lngStarterRow = 1;
		//			executeSTARTNOLOOP = true;
		//			goto STARTNOLOOP;
		//		}
		//		STARTNOLOOP:
		//		;
		//		if (executeSTARTNOLOOP)
		//		{
		//			#region STARTNOLOOP
		//			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//			//lngTesterRow = frmUTCLStatus.InstancePtr.vsPayments.FindRow(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString(), lngStarterRow, frmUTCLStatus.InstancePtr.lngPayGridColBill);
		//			lngTesterRow = frmUTCLStatus.InstancePtr.vsPayments.FindRow(frmUTCLStatus.InstancePtr.cmbBillNumber.Text, lngStarterRow, frmUTCLStatus.InstancePtr.lngPayGridColBill);
		//			if (lngTesterRow > 0)
		//			{
		//				//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//				//if (frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngTesterRow, 3) == Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Items[frmUTCLStatus.InstancePtr.cmbService.SelectedIndex].ToString(), 1))
		//				if (frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngTesterRow, 3) == Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Text, 1))
		//				{
		//					// another payment is found from this bill
		//					MessageBox.Show("Payments already exist for this bill and service.", "Existing Payments", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//					AddUTPaymentToList = -1;
		//					return AddUTPaymentToList;
		//				}
		//				else
		//				{
		//					lngStarterRow = lngTesterRow + 1;
		//					goto STARTNOLOOP;
		//				}
		//			}
		//			if (rsTemp.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngBill), modExtraModules.strUTDatabase))
		//			{
		//				if (!rsTemp.EndOfFile())
		//				{
		//					if (FCConvert.ToString(rsTemp.Get_Fields_String("RateType")) != "L")
		//					{
		//						strTemp = "SELECT * FROM (" + Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(frmUTCLStatus.InstancePtr.CurrentAccountKey) + " AND BillNumber = " + FCConvert.ToString(lngBill);
		//						boolLienPayment = false;
		//					}
		//					else
		//					{
		//						strTemp = "SELECT * FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID WHERE Bill.ID = " + strWS + "CombinationLienKey AND AccountKey = " + FCConvert.ToString(frmUTCLStatus.InstancePtr.CurrentAccountKey) + " AND RateKey = " + FCConvert.ToString(lngBill);
		//						boolLienPayment = true;
		//						// If Not rsTemp.EndOfFile Then
		//						// strTemp = "SELECT * FROM (" & strUTCurrentAccountBills & ") WHERE AccountKey = " & frmUTCLStatus.CurrentAccountKey & " AND BillNumber = "
		//						// Else
		//						// MsgBox "Error finding rate ID " & lngBill & ".", vbOKOnly + vbCritical, "ERROR Adding Payment To List"
		//						// AddUTPaymentToList = -1
		//						// Exit Function
		//						// End If
		//					}
		//				}
		//				else
		//				{
		//					strTemp = "SELECT * FROM (" + Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(frmUTCLStatus.InstancePtr.CurrentAccountKey) + " AND ISNULL(BillNumber, 0) = " + FCConvert.ToString(lngBill);
		//					boolLienPayment = false;
		//					// MsgBox "Error finding rate ID " & lngBill & ".", vbOKOnly + vbCritical, "ERROR Adding Payment To List"
		//					// AddUTPaymentToList = -1
		//					// Exit Function
		//				}
		//			}
		//			else
		//			{
		//				MessageBox.Show("Error opening Rate Table.", "Rate Table Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//			}
		//			if (rsTemp.OpenRecordset(strTemp, modExtraModules.strUTDatabase))
		//			{
		//				if (rsTemp.EndOfFile() != true)
		//				{
		//				}
		//				else
		//				{
		//					MessageBox.Show("Error opening TWUT0000.VB1.", "ERROR Adding Payment To List - " + FCConvert.ToString(lngBill), MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//					AddUTPaymentToList = -1;
		//					return AddUTPaymentToList;
		//				}
		//			}
		//			else
		//			{
		//				MessageBox.Show("Error opening Bill Record.", "Bill Record Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//			}
		//			for (intCT = 1; intCT <= MAX_UTPAYMENTS; intCT++)
		//			{
		//				// this finds a place in the array
		//				if (Statics.UTPaymentArray[intCT].Account == 0)
		//				{
		//					// to store the new payment record
		//					break;
		//				}
		//			}
		//			dblPrinNeed = 0;
		//			dblTaxNeed = 0;
		//			dblIntNeed = 0;
		//			dblCostNeed = 0;
		//			dblPLINeed = 0;
		//			// MAL@20071012
		//			// get the next year and its total due values
		//			if (FCConvert.ToInt32(rsTemp.Get_Fields(PayCode + "LienRecordNumber")) == 0)
		//			{
		//				Statics.UTPaymentArray[intCT].BillNumber = lngBill;
		//				dblPrinNeed = modGlobal.Round(rsTemp.Get_Fields(PayCode + "PrinOwed") - rsTemp.Get_Fields(PayCode + "PrinPaid"), 2);
		//				dblTaxNeed = rsTemp.Get_Fields(PayCode + "TaxOwed") - rsTemp.Get_Fields(PayCode + "TaxPaid");
		//				dblIntNeed = rsTemp.Get_Fields(PayCode + "IntOwed") - rsTemp.Get_Fields(PayCode + "IntAdded") - rsTemp.Get_Fields(PayCode + "IntPaid") - Statics.dblUTCurrentInt[lngBill, intType];
		//				dblCostNeed = rsTemp.Get_Fields(PayCode + "CostOwed") - rsTemp.Get_Fields(PayCode + "CostAdded") - rsTemp.Get_Fields(PayCode + "CostPaid");
		//				dblPLINeed = 0;
		//			}
		//			else
		//			{
		//				rsLienRec.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTemp.Get_Fields(strWS + "LienRecordNumber"), modExtraModules.strUTDatabase);
		//				if (rsLienRec.RecordCount() != 0)
		//				{
		//					Statics.UTPaymentArray[intCT].BillNumber = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("RateKey"));
		//					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//					// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//					dblPrinNeed = rsLienRec.Get_Fields("Principal") - rsLienRec.Get_Fields("PrinPaid");
		//					// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
		//					// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//					dblTaxNeed = rsLienRec.Get_Fields("Tax") - rsLienRec.Get_Fields("TaxPaid");
		//					// MAL@20071012: Changed to break out PLI and Interest
		//					// dblIntNeed = Round(((rsLienRec.Fields("IntAdded") * -1) - rsLienRec.Fields("IntPaid")) - dblUTCurrentInt(rsTemp.Fields("BillNumber"), intTempType), 2)
		//					// MAL@20080108: Change to check for negative Interest Added amount
		//					// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//					if (FCConvert.ToDouble(rsLienRec.Get_Fields("IntAdded")) < 0)
		//					{
		//						// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//						// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//						dblIntNeed = modGlobal.Round((FCConvert.ToDouble(rsLienRec.Get_Fields("IntAdded")) * -1) - rsLienRec.Get_Fields("IntPaid") - Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intType], 2);
		//					}
		//					else
		//					{
		//						// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//						// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//						dblIntNeed = modGlobal.Round(rsLienRec.Get_Fields("IntAdded") - rsLienRec.Get_Fields("IntPaid") - Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intType], 2);
		//					}
		//					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//					// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//					dblPLINeed = modGlobal.Round((rsLienRec.Get_Fields("Interest") - rsLienRec.Get_Fields("PLIPaid")), 2);
		//					// dblIntNeed = rsLienRec.Fields("Interest") - rsLienRec.Fields("IntAdded") - rsLienRec.Fields("IntPaid") - rsLienRec.Fields("PLIPaid") - dblUTCurrentInt(rsTemp.Fields("BillNumber"), intType)
		//					// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//					// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//					dblCostNeed = rsLienRec.Get_Fields("Costs") - rsLienRec.Get_Fields_Double("CostPaid") - rsLienRec.Get_Fields("MaturityFee");
		//				}
		//			}
		//			if ((strType == "P" || strType == "U" || !(strType == "C" && dblInt < 0 && FCConvert.ToInt32(rsTemp.Get_Fields(PayCode + "LienRecordNumber")) != 0)) && strType != "A" && strType != "R" && !boolSkipRecalc)
		//			{
		//				// MAL@20071012
		//				// If (Round(dblIntNeed, 2) <= 0 And Round(dblCostNeed, 2) <= 0 And Round(dblTaxNeed, 2) <= 0) And (Not boolBothService And dblTot >= Round((dblIntNeed + dblCostNeed + dblTaxNeed + dblPrinNeed), 2)) Then
		//				if ((modGlobal.Round(dblIntNeed, 2) <= 0 && modGlobal.Round(dblCostNeed, 2) <= 0 && modGlobal.Round(dblTaxNeed, 2) <= 0) && (!boolBothService && dblTot >= modGlobal.Round((dblPLINeed + dblIntNeed + dblCostNeed + dblTaxNeed + dblPrinNeed), 2)))
		//				{
		//					dblPrin = dblCost + dblTax + dblInt;
		//					dblTax = 0;
		//					dblCost = 0;
		//					dblInt = 0;
		//					dblPLI = 0;
		//					// MAL@20071012
		//				}
		//				else
		//				{
		//					// MAL@20071012
		//					AdjustUTPaymentAmounts(ref dblPrin, ref dblTax, ref dblInt, ref dblCost, ref dblPrinNeed, ref dblTaxNeed, ref dblIntNeed, ref dblCostNeed, ref dblTempP, ref dblTempT, ref dblTempI, ref dblTempC, dblPLI, dblPLINeed, dblTempPI);
		//					// AdjustUTPaymentAmounts dblPrin, dblTax, dblInt, dblCost, dblPrinNeed, dblTaxNeed, dblIntNeed, dblCostNeed, dblTempP, dblTempT, dblTempI, dblTempC
		//					// dblTot = Round(dblTot - dblTempP - dblTempT - dblTempI - dblTempC, 2)
		//					dblTot = modGlobal.Round(dblTot - dblTempP - dblTempT - dblTempI - dblTempC - dblTempPI, 2);
		//					dblPrin = dblTempP;
		//					dblTax = dblTempT;
		//					dblInt = dblTempI;
		//					dblCost = dblTempC;
		//					dblPLI = dblTempPI;
		//					// MAL@20071012
		//				}
		//			}
		//			else
		//			{
		//				dblTempP = dblPrin;
		//				dblTempT = dblTax;
		//				// dblTempI = dblInt
		//				dblTempC = dblCost;
		//				// MAL@20080528: Changed to account for PLI needed
		//				// Tracker Reference: 13904
		//				if (dblInt != 0)
		//				{
		//					if (dblPLINeed > 0)
		//					{
		//						if (dblInt - dblPLINeed <= 0)
		//						{
		//							dblTempPI = dblInt;
		//							dblTempI = 0;
		//						}
		//						else
		//						{
		//							dblTempI = dblInt - dblPLINeed;
		//							dblTempPI = dblInt - dblTempI;
		//						}
		//					}
		//					else
		//					{
		//						dblTempPI = 0;
		//						dblTempI = dblInt;
		//					}
		//				}
		//				else if (dblPLI != 0)
		//				{
		//					dblTempPI = dblPLINeed;
		//					dblTempI = dblInt;
		//				}
		//				else
		//				{
		//					dblTempPI = 0;
		//					dblTempI = 0;
		//				}
		//				if ((dblTempP + dblTempT + dblTempI + dblTempC + dblTempPI) < 0)
		//				{
		//					dblTot += (dblTempP + dblTempT + dblTempI + dblTempC + dblTempPI);
		//				}
		//				else
		//				{
		//					dblTot = modGlobal.Round(dblTot - dblTempP - dblTempT - dblTempI - dblTempC - dblTempPI, 2);
		//				}
		//				// If (dblTempP + dblTempT + dblTempI + dblTempC) < 0 Then
		//				// dblTot = dblTot + (dblTempP + dblTempT + dblTempI + dblTempC)
		//				// Else
		//				// dblTot = Round(dblTot - dblTempP - dblTempT - dblTempI - dblTempC, 2)
		//				// End If
		//			}
		//			if (modGlobal.Round(dblTot, 2) > 0)
		//			{
		//				if (boolBothService)
		//				{
		//					// check to see if there is any amount due for the other bill
		//					dblPrinNeed = 0;
		//					dblTaxNeed = 0;
		//					dblIntNeed = 0;
		//					dblCostNeed = 0;
		//					if (PayCode == "W")
		//					{
		//						PayCode = "S";
		//					}
		//					else
		//					{
		//						PayCode = "W";
		//					}
		//					// XXXXXXX THIS IS COLLECTIONS CODE.  NEED SLienRecordNumber / WLienRecordNumber
		//					// get the next year and its total due values
		//					if (FCConvert.ToInt32(rsTemp.Get_Fields_Int32("LienRecordNumber")) == 0)
		//					{
		//						dblPrinNeed = rsTemp.Get_Fields(PayCode + "PrinOwed") - rsTemp.Get_Fields(PayCode + "PrinPaid");
		//						dblTaxNeed = rsTemp.Get_Fields(PayCode + "TaxOwed") - rsTemp.Get_Fields(PayCode + "TaxPaid");
		//						dblIntNeed = rsTemp.Get_Fields(PayCode + "IntOwed") - rsTemp.Get_Fields(PayCode + "IntAdded") - rsTemp.Get_Fields(PayCode + "IntPaid") - Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intType];
		//						dblCostNeed = rsTemp.Get_Fields(PayCode + "CostOwed") - rsTemp.Get_Fields(PayCode + "CostAdded") - rsTemp.Get_Fields(PayCode + "CostPaid");
		//						dblPLINeed = 0;
		//					}
		//					else
		//					{
		//						rsLienRec.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTemp.Get_Fields_Int32("LienRecordNumber"), modExtraModules.strUTDatabase);
		//						if (rsLienRec.RecordCount() != 0)
		//						{
		//							// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//							dblPrinNeed = rsTemp.Get_Fields_Decimal("PrinOwed") - rsTemp.Get_Fields("PrinPaid");
		//							// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//							dblTaxNeed = rsTemp.Get_Fields_Decimal("TaxOwed") - rsTemp.Get_Fields("TaxPaid");
		//							// MAL@20071012: Changed to break out PLI and Interest
		//							// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//							dblIntNeed = modGlobal.Round(((FCConvert.ToDouble(rsLienRec.Get_Fields("IntAdded")) * -1) - FCConvert.ToDouble(rsLienRec.Get_Fields("IntPaid"))) - Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intTempType], 2);
		//							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//							dblPLINeed = modGlobal.Round((rsLienRec.Get_Fields("Interest") - rsLienRec.Get_Fields("PLIPaid")), 2);
		//							// dblIntNeed = rsTemp.Fields("IntOwed") - rsTemp.Fields("IntPaid") - dblUTCurrentInt(rsTemp.Fields("BillNumber"), intType)
		//							// TODO: Field [CostOwed] not found!! (maybe it is an alias?)
		//							// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//							dblCostNeed = rsTemp.Get_Fields("CostOwed") - rsTemp.Get_Fields("MaturityFee") - rsTemp.Get_Fields_Double("CostPaid");
		//						}
		//					}
		//					// If dblPrinNeed + dblTaxNeed + dblIntNeed + dblCostNeed <> 0 Then
		//					if (dblPrinNeed + dblTaxNeed + dblIntNeed + dblCostNeed + dblPLINeed != 0)
		//					{
		//						// this will remove the current payment amount from the total
		//						// dblTot = dblTot - dblTempP - dblTempT - dblTempI - dblTempC
		//						// add a payment rec for the leftover to the other part of this bill
		//						CreateExcessUTPaymentRecord_24(dblTot, !boolWater, true);
		//					}
		//					else
		//					{
		//						// just set all of the excess amount to the principal of this bill
		//						dblTempP += dblTot;
		//						dblTot = 0;
		//					}
		//					if (PayCode == "W")
		//					{
		//						PayCode = "S";
		//					}
		//					else
		//					{
		//						PayCode = "W";
		//					}
		//				}
		//				else
		//				{
		//					// just set all of the excess amount to the principal of this bill
		//					dblTempP += dblTot;
		//					dblTot = 0;
		//				}
		//			}
		//			else
		//			{
		//				dblTempP = dblPrin;
		//				dblTempT = dblTax;
		//				// MAL@20080528: Change to break out PLI as needed
		//				// Tracker Reference: 13904
		//				// MAL@20080703: Corrected to not erase the calculated PLI amount
		//				// Tracker Reference: 14553
		//				if (dblInt != 0)
		//				{
		//					if (dblPLINeed > 0 && dblPLI == 0)
		//					{
		//						if (dblInt - dblPLINeed <= 0)
		//						{
		//							dblTempPI = dblInt;
		//							dblTempI = 0;
		//						}
		//						else
		//						{
		//							dblTempI = dblInt - dblPLINeed;
		//							dblTempPI = dblInt - dblTempI;
		//						}
		//					}
		//					else
		//					{
		//						dblTempPI = dblPLI;
		//						dblTempI = dblInt;
		//					}
		//					// MAL@20081126: Corrected calculations in situations where no current interest exists but PLI does
		//					// Tracker Reference: 16306
		//				}
		//				else if (dblPLI != 0)
		//				{
		//					dblTempPI = dblPLI;
		//					dblTempI = dblInt;
		//				}
		//				else
		//				{
		//					dblTempPI = 0;
		//					dblTempI = 0;
		//				}
		//				// dblTempI = dblInt
		//				dblTempC = dblCost;
		//			}
		//			if (intCT < MAX_UTPAYMENTS)
		//			{
		//				// this adds it to the array of payments
		//				Statics.UTPaymentArray[intCT].Account = frmUTCLStatus.InstancePtr.CurrentAccountKey;
		//				Statics.UTPaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//				if (boolLienPayment)
		//				{
		//					// this will store the lien record number in the bill ID if it is a lien payment
		//					Statics.UTPaymentArray[intCT].BillNumber = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("RateKey"));
		//					Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
		//					Statics.UTPaymentArray[intCT].BillCode = "L";
		//					BillCode = "L";
		//				}
		//				else
		//				{
		//					Statics.UTPaymentArray[intCT].BillNumber = lngBill;
		//					// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//					Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields("Bill"));
		//					Statics.UTPaymentArray[intCT].BillCode = "R";
		//					BillCode = "R";
		//				}
		//				Statics.UTPaymentArray[intCT].Service = PayCode;
		//				Statics.UTPaymentArray[intCT].CashDrawer = frmUTCLStatus.InstancePtr.txtCD.Text;
		//				if (Statics.UTPaymentArray[intCT].CashDrawer == "N")
		//				{
		//					// kk06092014 TROCRS-27  Need to remove all "_" from the account number or it isn't detected
		//					Statics.UTPaymentArray[intCT].BudgetaryAccountNumber = frmUTCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0).Replace("_", "");
		//					// frmUTCLStatus.txtAcctNumber.TextMatrix(0, 0)
		//				}
		//				else
		//				{
		//					// do nothing
		//				}
		//				Statics.UTPaymentArray[intCT].Code = strType;
		//				Statics.UTPaymentArray[intCT].Comments = frmUTCLStatus.InstancePtr.txtComments.Text;
		//				if (frmUTCLStatus.InstancePtr.txtReference.Text == "REVERSE")
		//				{
		//					Statics.UTPaymentArray[intCT].EffectiveInterestDate = GetLastInterestDate(FCConvert.ToInt32(frmUTCLStatus.InstancePtr.lblAccount.Text), Statics.UTPaymentArray[intCT].BillNumber, strWS, Statics.UTEffectiveDate, 0);
		//					if (boolLienPayment)
		//					{
		//						Statics.UTPaymentArray[intCT].EffectiveInterestDate = GetLastInterestDate_Lien(Statics.UTPaymentArray[intCT].Account, Statics.UTPaymentArray[intCT].BillNumber, strWS, Statics.UTEffectiveDate, 0, Statics.UTPaymentArray[intCT].BillKey);
		//						//06.29.17 TROUT-1222 kjr pass AccountKey
		//					}
		//					else
		//					{
		//						Statics.UTPaymentArray[intCT].EffectiveInterestDate = GetLastInterestDate(Statics.UTPaymentArray[intCT].Account, Statics.UTPaymentArray[intCT].BillNumber, strWS, Statics.UTEffectiveDate, 0);
		//						//06.29.17 TROUT-1222 kjr pass AccountKey
		//					}
		//				}
		//				else
		//				{
		//					Statics.UTPaymentArray[intCT].EffectiveInterestDate = Statics.UTEffectiveDate;
		//				}
		//				Statics.UTPaymentArray[intCT].GeneralLedger = frmUTCLStatus.InstancePtr.txtCash.Text;
		//				// .PreLienInterest = 0       'MAL@20071012
		//				Statics.UTPaymentArray[intCT].PreLienInterest = FCConvert.ToDecimal(dblTempPI);
		//				Statics.UTPaymentArray[intCT].Principal = FCConvert.ToDecimal(dblTempP);
		//				Statics.UTPaymentArray[intCT].Tax = FCConvert.ToDecimal(dblTempT);
		//				//kk10312016 trout-1225  Add this back in to handle PLI correctly in reversals
		//				// MAL@20071012
		//				if (boolLienPayment)
		//				{
		//					//if this is a lien, then make sure that the correct prelien interest is paid seperately
		//					// If (rsLienRec.Fields("InterestCharged") - dblCurrentInt((.Year) - DEFAULTSUBTRACTIONVALUE)) * -1 >= rsLienRec.Fields("InterestPaid") + dblInt - rsLienRec.Fields("Interest") And rsLienRec.Fields("Interest") > 0 Then
		//					// TODO: Check the table for the column [Intadded] and replace with corresponding Get_Field method
		//					// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//					if (((rsLienRec.Get_Fields("Intadded") * -1) + (Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intType] * -1) - rsLienRec.Get_Fields("IntPaid") < dblTempI) || (dblTempI < 0))
		//					{
		//						// we know that there is more interest paid than is needed for the interest charged
		//						// and current interest, so we can apply it to the pre lien interest as well, if needed
		//						// If dblInt < ((rsLienRec.Fields("InterestCharged") - dblCurrentInt((.Year) - DEFAULTSUBTRACTIONVALUE)) * -1) - rsLienRec.Fields("InterestPaid") Then
		//						// 
		//						if (dblTempI < 0)// Interest is negative
		//						{
		//							// reversal
		//							// I should get the payment ID and find out how much was applied to the PLI and how much was applied to the current interest
		//							rsPeriod.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + lngReversalKey, modExtraModules.strUTDatabase);
		//							if (!rsPeriod.EndOfFile())
		//							{
		//								Statics.UTPaymentArray[intCT].PreLienInterest = FCConvert.ToDecimal(Conversion.Val(rsPeriod.Get_Fields_Decimal("PreLienInterest")) * -1);
		//								Statics.UTPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(Conversion.Val(rsPeriod.Get_Fields_Decimal("CurrentInterest")) * -1);
		//							}
		//							else
		//							{
		//								// If (rsLienRec.Fields("IntAdded") + dblUTCurrentInt(rsTemp.Fields("BillNumber"), intType)) - dblTempI > 0 Then
		//								// If rsLienRec.Fields("Interest") >= ((rsLienRec.Fields("IntAdded") + dblUTCurrentInt(rsTemp.Fields("BillNumber"), intType)) - dblTempI) Then
		//								// .PreLienInterest = -1 * ((rsLienRec.Fields("IntAdded") + dblUTCurrentInt(rsTemp.Fields("BillNumber"), intType)) - dblTempI)
		//								// .CurrentInterest = ((rsLienRec.Fields("IntAdded") + dblUTCurrentInt(rsTemp.Fields("BillNumber"), intType)))
		//								// Else
		//								// .PreLienInterest = rsLienRec.Fields("Interest") * -1
		//								// .CurrentInterest = dblInt + rsLienRec.Fields("Interest")
		//								// End If
		//								// Else
		//								// all current interest
		//								Statics.UTPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(dblTempI);
		//								// End If
		//							}
		//						}
		//						else
		//						{
		//							// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//							if (dblTempI > ((rsLienRec.Get_Fields("IntAdded") * -1) - Statics.dblUTCurrentInt[FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber")), intType] - rsLienRec.Get_Fields("IntPaid")) + (rsLienRec.Get_Fields("Interest") - rsLienRec.Get_Fields("PLIPaid")))
		//							{
		//								// if this will overpay the int amount then put the extra in the current interest
		//								// so pay the current int
		//								// .CurrentInterest = (rsLienRec.Fields("IntAdded") * -1) - dblUTCurrentInt(rsTemp.Fields("BillNumber"), intType) - rsLienRec.Fields("IntPaid")
		//								// pay the PLI
		//								// .PreLienInterest = rsLienRec.Fields("Interest") - rsLienRec.Fields("PLIPaid")
		//								// add the difference to current int
		//								// .CurrentInterest = .CurrentInterest + (dblTempI - (.CurrentInterest + .PreLienInterest)) ' ((rsLienRec.Fields("IntAdded") * -1) + dblUTCurrentInt(rsTemp.Fields("BillNumber"), intType) - rsLienRec.Fields("IntPaid")) + (rsLienRec.Fields("Interest") - rsLienRec.Fields("PLIPaid")))
		//							}
		//							else
		//							{
		//								// pay the current interest first, then pay the remaining to the PLI
		//								// .CurrentInterest = (rsLienRec.Fields("IntAdded") * -1) - dblUTCurrentInt(rsTemp.Fields("BillNumber"), intType) - rsLienRec.Fields("IntPaid")
		//								// .PreLienInterest = dblTempI - .CurrentInterest
		//							}
		//						}
		//					}
		//					else
		//					{
		//						// all in current interest because the interest being paid is
		//						// less then what is owed for charged interest + curent interest
		//						Statics.UTPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(dblTempI);
		//					}
		//				}
		//				else
		//				{
		//					Statics.UTPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(dblTempI);
		//				}
		//				//kk10312016 trout-1225		UTPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(dblTempI); // MAL@20071012
		//				Statics.UTPaymentArray[intCT].LienCost = FCConvert.ToDecimal(dblTempC);
		//				Statics.UTPaymentArray[intCT].ReceiptNumber = "P";
		//				if (frmUTCLStatus.InstancePtr.txtTransactionDate.Text == "")
		//				{
		//					Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//				}
		//				else
		//				{
		//					Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmUTCLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//				}
		//				if (boolCalcInterest && lngReversalKey != 0)
		//				{
		//					// this is where the CHGINT Number needs to be put in the
		//					// lngReversalKey is the payment ID of the payment being reversed
		//					// intCT is the array index of the reversal payment
		//					// AddCHGINTToList frmRECLStatus.vsPayments.rows, lngReversalKey, True, intCT
		//					//kk12152017 trout-1308  Set this back to what CL does. I fixed the code below and it started creating 2 CHGINT records
		//					//**************
		//					lngReversalKey = 0;
		//					//kk12152017 trout-1308  Set this back to what CL does. I fixed the code below and it started creating 2 CHGINT records
		//					//**************
		//					// lngReversalKey = 0
		//					//.CHGINTNumber = CreateCHGINTLine(intCT, .EffectiveInterestDate, dblInt)
		//					//**************
		//					//kk11032016 trout-1225  Make UT work like CL when reversing payments with interest
		//					//set the CHGINT up to save
		//					//.Reversal = True
		//					//Dim rsCHGINTTemp            As New clsDRWrapper
		//					//rsCHGINTTemp.OpenRecordset "SELECT * FROM PaymentRec WHERE ID = " & lngReversalKey, strUTDatabase   'kk12072016 still finding CL refs!strCLDatabase
		//					//If Not rsCHGINTTemp.EndOfFile Then
		//					//rsCHGINTTemp.OpenRecordset "SELECT * FROM PaymentRec WHERE ID = " & rsCHGINTTemp.Fields("CHGINTNumber"), strUTDatabase   'kk12072016 still finding CL refs!strCLDatabase
		//					//If Not rsCHGINTTemp.EndOfFile Then
		//					//If IsThisCR Then    'only do this for CR
		//					//.CHGINTNumber = CreateUTCHGINTLine(intCT, .EffectiveInterestDate, -1 * rsCHGINTTemp.Fields("CurrentInterest"))
		//					//End If
		//					//.SetEIDate = .EffectiveInterestDate
		//					//Else
		//					//lngReversalKey = 0
		//					//.CHGINTNumber = CreateCHGINTLine(intCT, .EffectiveInterestDate, dblInt)
		//					//End If
		//					//Else
		//					//.CHGINTNumber = 0
		//					//End If
		//				}
		//				else if (FCConvert.CBool(Statics.dblUTCurrentInt[rsTemp.Get_Fields_Int32("BillNumber"), intType] != 0))
		//				{
		//					// AddCHGINTToList frmRECLStatus.vsPayments.rows, , True
		//					// this will create a CHGINT line for normal payments
		//					if (boolLienPayment)
		//					{
		//						Statics.UTPaymentArray[intCT].CHGINTNumber = CreateUTCHGINTLine_18(intCT, ref Statics.UTPaymentArray[intCT].EffectiveInterestDate, FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber")));
		//					}
		//					else if (Statics.UTPaymentArray[intCT].ReversalID == -1)
		//					{
		//						// MAL@20080228: Add check for non-reversal records
		//						Statics.UTPaymentArray[intCT].CHGINTNumber = CreateUTCHGINTLine(ref intCT, ref Statics.UTPaymentArray[intCT].EffectiveInterestDate);
		//					}
		//				}
		//				Statics.UTPaymentArray[intCT].Reference = Strings.Trim(frmUTCLStatus.InstancePtr.txtReference.Text + " ");
		//				//kk01232017 trout-1225  Make reversal work like CL -XXXXXXXXXXXXXX       .ReversalID = -1
		//				if (lngReversalKey != 0)
		//				{
		//					Statics.UTPaymentArray[intCT].ReversalID = lngReversalKey;
		//				}
		//				else
		//				{
		//					Statics.UTPaymentArray[intCT].ReversalID = -1;
		//				}
		//				// .Teller =
		//				// .TransNumber =
		//				// .PaidBy =
		//				// .CHGINTNumber =
		//				frmUTCLStatus.InstancePtr.FillPaymentGrid(cRow, intCT, Statics.UTPaymentArray[intCT].BillNumber);
		//				frmUTCLStatus.InstancePtr.txtPrincipal.Text = "0.00";
		//				frmUTCLStatus.InstancePtr.txtInterest.Text = "0.00";
		//				frmUTCLStatus.InstancePtr.txtCosts.Text = "0.00";
		//			}
		//			else
		//			{
		//				MessageBox.Show("You have reached your maximum payments per screen.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//			}
		//			#endregion
		//		}
		//		AddUTPaymentToList = FCConvert.ToInt16(intCT);
		//		if (boolWater)
		//		{
		//			UTBillPending_72(frmUTCLStatus.InstancePtr.WGRID, lngBill, true, 1);
		//		}
		//		else
		//		{
		//			UTBillPending_72(frmUTCLStatus.InstancePtr.SGRID, lngBill, false, 1);
		//		}
		//		return AddUTPaymentToList;
		//	}
		//	catch (Exception ex)
		//	{
				
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding UT Payment To List", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return AddUTPaymentToList;
		//}
		// vbPorter upgrade warning: intPArrInd As short	OnWriteFCConvert.ToInt32(
		//public static void AddUTCHGINTToList_72(int cRow, bool boolWater, bool boolRev = true, bool boolCHGINT = false, int intPArrInd = -1)
		//{
		//	AddUTCHGINTToList(cRow, boolWater, 0, boolRev, boolCHGINT, intPArrInd);
		//}

		//public static void AddUTCHGINTToList(int cRow, bool boolWater, int lngRevID = 0, bool boolRev = true, bool boolCHGINT = false, int intPArrInd = -1, int lngBillKey = 0, int lngBill = 0)
		//{
		//	// this will add the CHGINT line to the payment list
		//	// in order to return the values to the state before the last payment happened
		//	int intCT;
		//	//int lngBill = 0;
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	clsDRWrapper rsLienRec = new clsDRWrapper();
		//	clsDRWrapper rsCHG = new clsDRWrapper();
		//	string strTemp;
		//	string PayCode = "";
		//	string BillCode = "";
		//	int CHGINTNumber;
		//	int lngRevPayKey;
		//	string strType;
		//	int intType;
		//	//int lngBillKey = 0;
		//	int lngLienKey = 0;
		//	clsDRWrapper rsLien = new clsDRWrapper();
		//	DateTime dtIntDate;
		//	DateTime dttemp = DateTime.FromOADate(0);
		//	// this is the type of payment that is being processed
		//	//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//	//strType = Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Items[frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex].ToString(), 1);
		//	strType = Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Text, 1);
		//	intType = frmUTCLStatus.InstancePtr.FindPaymentType();
		//	if (lngBillKey == 0 && lngBill == 0)
		//	{
		//		//trout-1225 kjr 7.31.17 rev cRow not correct.  pass known info instead.
		//		if (strType == "Y")
		//		{
		//			// if this is a prepayment
		//			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//			//lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString())); // use the correct year (next year)
		//			lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(frmUTCLStatus.InstancePtr.cmbBillNumber.Text)));
		//			// use the correct year (next year)
		//			//XXXXX        lngBillKey = Val(frmUTCLStatus.cmbBillNumber.list(frmUTCLStatus.cmbBillNumber.ListIndex))        'use the correct bill(next bill)
		//			//XXXXXXXXXXXXXXXXXXXXXX kk02062017
		//		}
		//		//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//		//else if (frmUTCLStatus.InstancePtr.cmbBillNumber.Items[frmUTCLStatus.InstancePtr.cmbBillNumber.ListIndex] == "Auto")
		//		else if (frmUTCLStatus.InstancePtr.cmbBillNumber.Text == "Auto")
		//		{
		//			if (boolWater)
		//			{
		//				lngBill = FCConvert.ToInt32(Conversion.Val(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(cRow - 1, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber)));
		//				//use the last payment's bill number
		//				lngBillKey = FCConvert.ToInt32(Conversion.Val(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(cRow - 1, frmUTCLStatus.InstancePtr.lngGridColBillKey)));
		//				//use the last payment's bill number
		//			}
		//			else
		//			{
		//				lngBill = FCConvert.ToInt32(Conversion.Val(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(cRow - 1, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber)));
		//				lngBillKey = FCConvert.ToInt32(Conversion.Val(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(cRow - 1, frmUTCLStatus.InstancePtr.lngGridColBillKey)));
		//			}
		//			//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		//		}
		//		else
		//		{
		//			// else use the year choosen
		//			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//			//lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString()));
		//			lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(frmUTCLStatus.InstancePtr.cmbBillNumber.Text)));
		//			//XXXXX        lngBillKey = Val(frmUTCLStatus.cmbBillNumber.list(frmUTCLStatus.cmbBillNumber.ListIndex))
		//		}
		//	}
		//	if (boolWater)
		//	{
		//		PayCode = "W";
		//	}
		//	else
		//	{
		//		PayCode = "S";
		//	}
		//	// this makes sure that thre is a bill created for this year
		//	strTemp = "SELECT * FROM (" + Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(frmUTCLStatus.InstancePtr.CurrentAccountKey) + " AND BillNumber = " + FCConvert.ToString(lngBill);
		//	rsTemp.OpenRecordset(strTemp, modExtraModules.strUTDatabase);
		//	if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
		//	{
		//	}
		//	else
		//	{
		//		// MAL@20071204: Check for Lien records
		//		strTemp = "SELECT * FROM (" + Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(frmUTCLStatus.InstancePtr.CurrentAccountKey) + " AND " + PayCode + "LienRecordNumber > 0";
		//		rsTemp.OpenRecordset(strTemp, modExtraModules.strUTDatabase);
		//		if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
		//		{
		//			rsTemp.MoveFirst();
		//			while (!rsTemp.EndOfFile())
		//			{
		//				strTemp = "SELECT * FROM Lien WHERE ID = " + rsTemp.Get_Fields(PayCode + "LienRecordNumber");
		//				rsLien.OpenRecordset(strTemp, modExtraModules.strUTDatabase);
		//				if (rsLien.RecordCount() > 0)
		//				{
		//					if (FCConvert.ToInt32(rsLien.Get_Fields_Int32("RateKey")) == lngBill)
		//					{
		//						lngLienKey = FCConvert.ToInt32(rsLien.Get_Fields_Int32("ID"));
		//						break;
		//					}
		//				}
		//				rsTemp.MoveNext();
		//			}
		//			strTemp = "SELECT * FROM (" + Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(frmUTCLStatus.InstancePtr.CurrentAccountKey) + " AND " + PayCode + "LienRecordNumber = " + FCConvert.ToString(lngLienKey);
		//			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
		//			{
		//			}
		//			else
		//			{
		//				MessageBox.Show("Error opening database, no bill record.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//				return;
		//			}
		//		}
		//		else
		//		{
		//			MessageBox.Show("Error opening database, no bill record.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//			return;
		//		}
		//	}
		//	for (intCT = 1; intCT <= MAX_UTPAYMENTS; intCT++)
		//	{
		//		// this finds a place in the array
		//		if (Statics.UTPaymentArray[intCT].Account == 0)
		//		{
		//			// to store the new payment record
		//			break;
		//		}
		//	}
		//	//XXXXXXXX kk02072017 trout-1225   Pulled this in from CL - Get the int paid date from the Lien or Bill
		//	if (rsTemp.Get_Fields(PayCode + "LienRecordNumber") != 0)
		//	{
		//		rsCHG.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTemp.Get_Fields(PayCode + "LienRecordNumber"), modExtraModules.strUTDatabase);
		//		if (!rsCHG.EndOfFile())
		//		{
		//			dtIntDate = Convert.ToDateTime(rsCHG.Get_Fields_DateTime("IntPaidDate"));
		//		}
		//		else
		//		{
		//			dtIntDate = Convert.ToDateTime(rsTemp.Get_Fields(PayCode + "IntPaidDate"));
		//		}
		//	}
		//	else
		//	{
		//		dtIntDate = Convert.ToDateTime(rsTemp.Get_Fields(PayCode + "IntPaidDate"));
		//	}
		//	if (lngRevID != 0 && intPArrInd != 0)
		//	{
		//		//this I know that this is a reversal and I need a CHGINT line to go with it
		//		if (Statics.UTPaymentArray[intPArrInd].BillNumber <= 0)
		//		{
		//			Statics.UTPaymentArray[intPArrInd].BillNumber = lngBill;
		//		}
		//		else
		//		{
		//			lngBill = Statics.UTPaymentArray[intPArrInd].BillNumber;
		//		}
		//		if ((Statics.UTPaymentArray[intPArrInd].CurrentInterest > 0 || dtIntDate.ToOADate() == 0) && Statics.UTPaymentArray[intPArrInd].ReversalID != 0)
		//		{
		//			CHGINTNumber = CreateUTCHGINTLine(ref intPArrInd, ref dttemp);
		//			//XXXXXXXXXXXXXXXXX    , FindUTCHGINTValue(lngBillKey))    'x  , UTEffectiveDate))     ' lngRevID
		//		}
		//		else
		//		{
		//			CHGINTNumber = CreateUTCHGINTLine(ref intPArrInd, ref dttemp, 0, false, 0, FindUTCHGINTValue(FCConvert.ToInt32(lngBillKey)));
		//			//x  , dtIntDate))         ' lngRevID  '''trout-1225 kjr 7/31/17 pass Int Value from FindUTCHGINTValue
		//		}
		//		Statics.UTPaymentArray[intPArrInd].CHGINTNumber = CHGINTNumber;
		//		//this will fill the main reversal line with the correct CHGINT number
		//	}
		//	else
		//	{
		//		// get the ID for the CHGINT payment that corresponds to this payment or reversal
		//		CHGINTNumber = Math.Abs(Statics.lngUTCHGINT);
		//		// rsTemp.Fields("CHGINTNumber")
		//	}
		//	//XXXXXXXXXXXXXXXX kk02062017 trout-1225
		//	// get the ID for the CHGINT payment that corresponds to this payment or reversal
		//	//CHGINTNumber = Math.Abs(Statics.lngUTCHGINT); // rsTemp.Fields("CHGINTNumber")
		//	//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		//	// get the next year and its total due values
		//	if (FCConvert.ToInt32(rsTemp.Get_Fields(PayCode + "LienRecordNumber")) == 0)
		//	{
		//		BillCode = "R";
		//	}
		//	else
		//	{
		//		BillCode = "L";
		//		rsLienRec.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTemp.Get_Fields(PayCode + "LienRecordNumber"), modExtraModules.strUTDatabase);
		//	}
		//	//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//	//if (CHGINTNumber != 0 || Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Items[frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex].ToString(), 1) == "S" || boolCHGINT)
		//	if (CHGINTNumber != 0 || Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Text, 1) == "S" || boolCHGINT)
		//	{
		//		if (intCT < MAX_UTPAYMENTS)
		//		{
		//			// this adds it to the array of payments
		//			Statics.UTPaymentArray[intCT].Account = frmUTCLStatus.InstancePtr.CurrentAccountKey;
		//			Statics.UTPaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//			Statics.UTPaymentArray[intCT].BillCode = BillCode;
		//			Statics.UTPaymentArray[intCT].Service = PayCode;
		//			Statics.UTPaymentArray[intCT].BillNumber = lngBill;
		//			if (BillCode == "L")
		//			{
		//				// this shows which bill record that this CHGINT is linked to
		//				Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
		//				lngBillKey = FCConvert.ToInt32(rsLienRec.Get_Fields_Int32("ID"));
		//			}
		//			else
		//			{
		//				// .BillKey = rsTemp.Fields("BillKey")
		//				// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//				Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields("Bill"));
		//				// MAL@20070920
		//				// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//				lngBillKey = FCConvert.ToInt32(rsTemp.Get_Fields("Bill"));
		//				// MAL@20071026
		//			}
		//			Statics.UTPaymentArray[intCT].CashDrawer = frmUTCLStatus.InstancePtr.txtCD.Text;
		//			if (Statics.UTPaymentArray[intCT].CashDrawer == "N")
		//			{
		//				// kk06092014 TROCRS-27  Need to remove all "_" from the account number or it isn't detected
		//				Statics.UTPaymentArray[intCT].BudgetaryAccountNumber = frmUTCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0).Replace("_", "");
		//				// frmUTCLStatus.txtAcctNumber.TextMatrix(0, 0)
		//			}
		//			else
		//			{
		//			}
		//			Statics.UTPaymentArray[intCT].Code = "I";
		//			// Left$(frmUTCLStatus.cmbCode.List(frmUTCLStatus.cmbCode.ListIndex), 1)
		//			Statics.UTPaymentArray[intCT].Comments = frmUTCLStatus.InstancePtr.txtComments.Text;
		//			// MAL@20071030
		//			if (boolRev)
		//			{
		//				Statics.UTPaymentArray[intCT].EffectiveInterestDate = GetLastInterestDate(FCConvert.ToInt32(frmUTCLStatus.InstancePtr.CurrentAccountKey), lngBill, PayCode, Statics.UTEffectiveDate, 0);
		//				// trout-1222 kjr use acct key
		//			}
		//			else
		//			{
		//				Statics.UTPaymentArray[intCT].EffectiveInterestDate = Statics.UTEffectiveDate;
		//			}
		//			Statics.UTPaymentArray[intCT].GeneralLedger = frmUTCLStatus.InstancePtr.txtCash.Text;
		//			Statics.UTPaymentArray[intCT].Period = FCConvert.ToString(1);
		//			Statics.UTPaymentArray[intCT].PreLienInterest = 0;
		//			Statics.UTPaymentArray[intCT].Principal = 0;
		//			Statics.UTPaymentArray[intCT].LienCost = 0;
		//			Statics.UTPaymentArray[intCT].ReceiptNumber = FCConvert.ToString(0);
		//			if (frmUTCLStatus.InstancePtr.txtTransactionDate.Text == "")
		//			{
		//				Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//			}
		//			else
		//			{
		//				Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmUTCLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//			}
		//			// .Teller =
		//			// .TransNumber =
		//			// .PaidBy =
		//			rsCHG.OpenRecordset("SELECT * FROM (" + Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE ID = " + FCConvert.ToString(CHGINTNumber), modExtraModules.strUTDatabase);
		//			if (rsCHG.EndOfFile() != true && rsCHG.BeginningOfFile() != true)
		//			{
		//				Statics.UTPaymentArray[intCT].CurrentInterest = (FCConvert.ToDecimal(rsCHG.Get_Fields_Decimal("CurrentInterest")) * -1);
		//			}
		//			else
		//			{
		//				Statics.UTPaymentArray[intCT].CurrentInterest = 0;
		//			}
		//			if (Statics.lngUTCHGINT > 0)
		//			{
		//				Statics.UTPaymentArray[intCT].CHGINTNumber = Statics.lngUTCHGINT;
		//				Statics.UTPaymentArray[intCT].ReversalID = -1;
		//			}
		//			else
		//			{
		//				Statics.UTPaymentArray[intCT].CHGINTNumber = 0;
		//				Statics.UTPaymentArray[intCT].ReversalID = intPArrInd;
		//				if (rsCHG.EndOfFile() != true)
		//				{
		//					// if this is a supplemental payment then this will be empty
		//					Statics.UTPaymentArray[intCT].CHGINTDate = (DateTime)rsCHG.Get_Fields_DateTime("CHGINTDate");
		//				}
		//				else
		//				{
		//					Statics.UTPaymentArray[intCT].CHGINTDate = Statics.UTEffectiveDate;
		//				}
		//			}
		//			Statics.UTPaymentArray[intCT].Year = lngBill;
		//			if (boolRev)
		//			{
		//				if (Statics.UTPaymentArray[intCT].CurrentInterest > 0)
		//				{
		//					Statics.UTPaymentArray[intCT].CurrentInterest = Statics.UTPaymentArray[intCT].CurrentInterest;
		//				}
		//				else
		//				{
		//					Statics.UTPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(FindUTCHGINTValue(lngBillKey));
		//				}
		//			}
		//			else if (boolCHGINT)
		//			{
		//				Statics.UTPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(Statics.dblUTCurrentInt[lngBill, intType] * -1);
		//			}
		//			else
		//			{
		//				Statics.UTPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(Statics.dblUTCurrentInt[lngBill, intType]);
		//			}
		//			if (Statics.UTPaymentArray[intCT].CurrentInterest > 0 && !boolRev)
		//			{
		//				// MAL@20071026
		//				Statics.UTPaymentArray[intCT].Reference = "CHGINT";
		//				// Trim$(frmUTCLStatus.txtReference & " ")
		//			}
		//			else if (Statics.UTPaymentArray[intCT].CurrentInterest > 0 && boolRev)
		//			{
		//				// MAL@20071030
		//				Statics.UTPaymentArray[intCT].Reference = "Interest";
		//			}
		//			else
		//			{
		//				Statics.UTPaymentArray[intCT].Reference = "EARNINT";
		//			}
		//			Statics.UTPaymentArray[intCT].LienCost = 0;
		//			Statics.UTPaymentArray[intCT].Principal = 0;
		//			// this adds a row to the payment grid and fills the payment information into it
		//			frmUTCLStatus.InstancePtr.FillPaymentGrid(cRow, intCT * -1, lngBill);
		//		}
		//		else
		//		{
		//			// maximum payments can be set
		//			MessageBox.Show("You have reached your maximum payments per screen.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//		}
		//	}
		//	// reset the values
		//	Statics.boolUTMultiLine = false;
		//	Statics.lngUTCHGINT = 0;
		//	rsLienRec.Reset();
		//	rsTemp.Reset();
		//	rsCHG.Reset();
		//}

//		public static void ResetUTComboBoxes(ref bool boolWater, ref bool boolResetAfterPmt)
//		{
//			// this is to reset the combo boxes on the payment frame
//			// kk10072015 trocr-446  Add option to reset after saving payment
//			int lngBill = 0;
//			int intCT;
//			frmUTCLStatus.InstancePtr.txtCD.Text = "Y";
//			frmUTCLStatus.InstancePtr.txtCash.Text = "Y";
//			if (!boolResetAfterPmt)
//			{
//				// Don't automatically reset the service after a pmt
//				if (frmUTCLStatus.InstancePtr.cmbService.Items.Count > 0)
//				{
//					frmUTCLStatus.InstancePtr.cmbService.SelectedIndex = 0;
//				}
//			}
//			// this will find the payment option in the list
//			for (intCT = 0; intCT <= frmUTCLStatus.InstancePtr.cmbCode.Items.Count - 1; intCT++)
//			{
//				if ("P" == Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Items[intCT].ToString(), 1))
//				{
//					frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex = intCT;
//					break;
//				}
//			}
//			// this finds the last year that is non zero
//			lngBill = FillUTBillNumber_6(boolWater, true);
//			if (frmUTCLStatus.InstancePtr.boolOnlyPrepayYear)
//			{
//				frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex = 0;
//				frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex = 4;
//			}
//			else
//			{
//				#if TWUT0000
//								                if (modUTUseCR.Statics.gboolDefaultYear)
//#else
//				if (modGlobal.Statics.gboolDefaultYear)
//				#endif
				
//				{
//					for (intCT = 0; intCT < frmUTCLStatus.InstancePtr.cmbBillNumber.Items.Count; intCT++)
//					{
//						if (modExtraModules.FormatYear(lngBill.ToString()) == frmUTCLStatus.InstancePtr.cmbBillNumber.Items[intCT].ToString())
//						{
//							frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex = intCT;
//							break;
//						}
//					}
//					if (frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex < 0)
//						frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex = frmUTCLStatus.InstancePtr.cmbBillNumber.Items.Count - 1;
//				}
//				else
//				{
//					frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex = frmUTCLStatus.InstancePtr.cmbBillNumber.Items.Count - 1;
//				}
//			}
//		}

//		public static void FillUTPaymentComboBoxes(string strOverride = "", bool blnSBill = false, bool blnWBill = false)
//		{
//			FillUTServiceCombo(strOverride, blnSBill, blnWBill);
//			FillUTCodeCombo();
//		}

		//private static void FillUTServiceCombo(string strOverride = "", bool blnSBill = false, bool blnWBill = false)
		//{
		//	frmUTCLStatus.InstancePtr.cmbService.Clear();
		//	if (strOverride == "")
		//	{
		//		strOverride = Statics.TownService;
		//	}
		//	if (strOverride == "B")
		//	{
		//		frmUTCLStatus.InstancePtr.cmbService.AddItem("Both");
		//		frmUTCLStatus.InstancePtr.cmbService.AddItem("Water");
		//		frmUTCLStatus.InstancePtr.cmbService.AddItem("Sewer");
		//	}
		//	else if (strOverride == "W")
		//	{
		//		frmUTCLStatus.InstancePtr.cmbService.AddItem("Water");
		//		// MAL@20080610: Add option to pay outstanding Sewer bills even if service is "W"
		//		// Tracker Reference: 14076
		//		if (blnSBill)
		//		{
		//			frmUTCLStatus.InstancePtr.cmbService.AddItem("Sewer");
		//			frmUTCLStatus.InstancePtr.cmbService.AddItem("Both");
		//		}
		//	}
		//	else if (strOverride == "S")
		//	{
		//		frmUTCLStatus.InstancePtr.cmbService.AddItem("Sewer");
		//		// MAL@20080610 ; Tracker Reference: 14076
		//		if (blnWBill)
		//		{
		//			frmUTCLStatus.InstancePtr.cmbService.AddItem("Water");
		//			frmUTCLStatus.InstancePtr.cmbService.AddItem("Both");
		//		}
		//	}
		//	else
		//	{
		//		frmUTCLStatus.InstancePtr.cmbService.AddItem("Both");
		//		frmUTCLStatus.InstancePtr.cmbService.AddItem("Water");
		//		frmUTCLStatus.InstancePtr.cmbService.AddItem("Sewer");
		//	}
		//}

		//private static void FillUTCodeCombo()
		//{
		//	frmUTCLStatus.InstancePtr.cmbCode.Clear();
		//	frmUTCLStatus.InstancePtr.cmbCode.AddItem("Y   - PrePayment");
		//	frmUTCLStatus.InstancePtr.cmbCode.AddItem("A   - Abatement");
		//	frmUTCLStatus.InstancePtr.cmbCode.AddItem("C   - Correction");
		//	// .AddItem "D   - Discount"
		//	frmUTCLStatus.InstancePtr.cmbCode.AddItem("P   - Regular Payment");
		//	frmUTCLStatus.InstancePtr.cmbCode.AddItem("R   - Refunded Abatement");
		//	// .AddItem "S   - Supplemental"
		//	// .AddItem "U   - Tax Club Payment"
		//}

		//public static void ResetUTPaymentFrame_6(bool boolWater, bool boolResetAfterPmt)
		//{
		//	ResetUTPaymentFrame(ref boolWater, ref boolResetAfterPmt);
		//}

		//public static void ResetUTPaymentFrame_8(bool boolWater, bool boolResetAfterPmt)
		//{
		//	ResetUTPaymentFrame(ref boolWater, ref boolResetAfterPmt);
		//}

//		public static void ResetUTPaymentFrame(ref bool boolWater, ref bool boolResetAfterPmt)
//		{
//			// kk10072015 trocr-446  Add option to reset after a payment is saved
//			ResetUTComboBoxes(ref boolWater, ref boolResetAfterPmt);
//			// .txtTransactionDate.Text = Format(Date, "MM/dd/yyyy")
//			frmUTCLStatus.InstancePtr.txtReference.Text = "";
//			frmUTCLStatus.InstancePtr.txtPrincipal.Text = Strings.Format(0, "#,##0.00");
//			frmUTCLStatus.InstancePtr.txtInterest.Text = Strings.Format(0, "#,##0.00");
//			frmUTCLStatus.InstancePtr.txtCosts.Text = Strings.Format(0, "#,##0.00");
//			frmUTCLStatus.InstancePtr.txtTax.Text = Strings.Format(0, "#,##0.00");
//			frmUTCLStatus.InstancePtr.txtComments.Text = "";
//			if (!frmUTCLStatus.InstancePtr.boolDoNotResetRTD)
//			{
//				frmUTCLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(Statics.UTEffectiveDate, "MM/dd/yyyy");
//				frmUTCLStatus.InstancePtr.boolDoNotResetRTD = false;
//			}
//			// MAL@20080130: Disable Cash Textbox if BD or CR doesn't exist
//			// Tracker Reference: 11087
//			if (!modGlobalConstants.Statics.gboolCR && !modGlobalConstants.Statics.gboolBD)
//			{
//				// Set focus to controls so values show on screen
//				if (!frmUTCLStatus.InstancePtr.txtCash.Enabled)
//				{
//					frmUTCLStatus.InstancePtr.txtCash.Enabled = true;
//				}
//				frmUTCLStatus.InstancePtr.txtComments.Focus();
//				frmUTCLStatus.InstancePtr.txtCash.Focus();
//				frmUTCLStatus.InstancePtr.txtCD.Focus();
//				frmUTCLStatus.InstancePtr.txtCash.Enabled = false;
//				frmUTCLStatus.InstancePtr.txtCash.Text = "Y";
//				frmUTCLStatus.InstancePtr.txtCD.Text = "Y";
//			}
//			else
//			{
//				if (modExtraModules.IsThisCR())
//				{
//					frmUTCLStatus.InstancePtr.txtCD.Text = "Y";
//					frmUTCLStatus.InstancePtr.txtCash.Text = "Y";
//				}
//				else
//				{
//					frmUTCLStatus.InstancePtr.txtCD.Text = "N";
//					frmUTCLStatus.InstancePtr.txtCash.Text = "N";
//				}
//				frmUTCLStatus.InstancePtr.txtCash.Enabled = true;
//				frmUTCLStatus.InstancePtr.txtCash.Focus();
//				frmUTCLStatus.InstancePtr.txtComments.Focus();
//				frmUTCLStatus.InstancePtr.txtCash.Enabled = false;
//				frmUTCLStatus.InstancePtr.txtCD.Focus();
//			}
//		}
//		// vbPorter upgrade warning: curCost As double	OnWriteFCConvert.ToDecimal(
//		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToDouble(
////		public static Decimal DetermineUTInterest(ref clsDRWrapper rsCollection, ref clsDRWrapper rsLien, ref string BillingCode, ref double curCost, ref double curPLInt, ref double curPrin, ref DateTime dateTemp, ref double curPD, ref bool boolWater)
////		{
////			Decimal DetermineUTInterest = 0;
////			// VB6 Bad Scope Dim:
////			Decimal Tax1 = 0;
////			// Determine Interest Calculation Date
////			// vbPorter upgrade warning: MostRecentInterestDate As DateTime	OnWrite(string, DateTime)
////			DateTime MostRecentInterestDate;
////			// vbPorter upgrade warning: AppliedDate As DateTime	OnWrite(string)
////			DateTime AppliedDate;
////			// vbPorter upgrade warning: InterestAmount As Decimal	OnWriteFCConvert.ToInt16(	OnReadFCConvert.ToDouble(
////			Decimal InterestAmount;
////			int intUsingStartDate;
////			// vbPorter upgrade warning: curCInt As double	OnWrite(short, Decimal, double)
////			double curCInt = 0;
////			double curPDTemp = 0;
////			clsDRWrapper rsRR = new clsDRWrapper();
////			bool boolIntAddedToBill;
////			DateTime dtIntStartDate;
////			intUsingStartDate = 0;
////			if (BillingCode == "BILL")
////			{
////				curCost = FCConvert.ToDouble(DetermineUTBillCosts(ref rsCollection, ref boolWater));
////				InterestAmount = 0;
////				// do not include current charged interest
////				// InterestAmount = rsCollection.fields("InterestCharged")
////				if (InterestAmount < 0)
////					InterestAmount = 0;
////				if (boolWater)
////				{
////					if (rsCollection.Get_Fields_DateTime("WIntPaidDate").ToOADate() != 0)
////					{
////						AppliedDate = FCConvert.ToDateTime(Strings.Format(rsCollection.Get_Fields_DateTime("WIntPaidDate"), "MM/dd/yyyy"));
////						intUsingStartDate = 0;
////						// 0
////					}
////					else
////					{
////						AppliedDate = FCConvert.ToDateTime(Statics.strUTInterestDate1);
////						intUsingStartDate = 0;
////					}
////				}
////				else
////				{
////					if (rsCollection.Get_Fields_DateTime("SIntPaidDate").ToOADate() != 0)
////					{
////						AppliedDate = FCConvert.ToDateTime(Strings.Format(rsCollection.Get_Fields_DateTime("SIntPaidDate"), "MM/dd/yyyy"));
////						intUsingStartDate = 0;
////						// 0
////					}
////					else
////					{
////						AppliedDate = FCConvert.ToDateTime(Statics.strUTInterestDate1);
////						intUsingStartDate = 0;
////					}
////				}
////				// strUTInterestDate1 = Format(DateDiff("d", 1, strUTInterestDate1), "MM/dd/yyyy")
////			}
////			else
////			{
////				// lien
////				curCost = FCConvert.ToDouble(DetermineUTLienCosts(ref rsLien, ref boolWater));
////				// If IsNull(rsLien.fields("InterestCharged")) Then
////				InterestAmount = 0;
////				// Else
////				// InterestAmount = rsLien.fields("InterestCharged")
////				// End If
////				if (InterestAmount < 0)
////					InterestAmount = 0;
////				// If rsCollection.Fields("BillingYear") = 19981 Then Stop
////				if (FCConvert.ToString(rsLien.Get_Fields_DateTime("InterestAppliedThroughDate")) == string.Empty)
////				{
////					AppliedDate = FCConvert.ToDateTime(Strings.Format(Statics.strUTInterestDate1, "MM/dd/yyyy"));
////					intUsingStartDate = 0;
////				}
////				else
////				{
////					AppliedDate = FCConvert.ToDateTime(Strings.Format(rsLien.Get_Fields_DateTime("InterestAppliedThroughDate"), "MM/dd/yyyy"));
////					intUsingStartDate = 0;
////				}
////				if (rsLien.Get_Fields_Decimal("InterestPaid") > rsLien.Get_Fields_Decimal("InterestCharged"))
////				{
////					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
////					curPLInt = rsLien.Get_Fields("Interest") - (rsLien.Get_Fields_Decimal("InterestCharged") - rsLien.Get_Fields_Decimal("InterestPaid"));
////				}
////				else
////				{
////					// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
////					curPLInt = rsLien.Get_Fields("Interest");
////				}
////				if (curPLInt < 0)
////					curPLInt = 0;
////			}
////			// check to make sure that the applied through date is a valid date
////			if (Information.IsDate(AppliedDate) == true)
////			{
////				// if the applied through date is after the interestStart date then
////				if (DateAndTime.DateValue(Statics.strUTInterestDate1).ToOADate() < DateAndTime.DateValue(FCConvert.ToString(AppliedDate)).ToOADate())
////				{
////					// use the applied date to calculate from
////					MostRecentInterestDate = FCConvert.ToDateTime(Strings.Format(AppliedDate, "MM/dd/yyyy"));
////				}
////				else
////				{
////					MostRecentInterestDate = DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(Strings.Format(Statics.strUTInterestDate1, "MM/dd/yyyy")));
////					intUsingStartDate = 0;
////				}
////			}
////			else
////			{
////				MostRecentInterestDate = DateAndTime.DateAdd("d", -1, FCConvert.ToDateTime(Strings.Format(Statics.strUTInterestDate1, "MM/dd/yyyy")));
////				intUsingStartDate = 0;
////			}
////			// just in case the dates have not been affected
////			// If MostRecentInterestDate = 0 Then  'if it is still empty, then find the bill date
////			// If BillingCode = "BILL" Then
////			// rsRR.OpenRecordset "SELECT * FROM RateRec WHERE RateKey = " & rsCollection.Fields("RateKey")
////			// If rsRR.EndOfFile <> True Then
////			// MostRecentInterestDate = rsRR.Fields("BillDate")
////			// End If
////			// Else
////			// rsRR.OpenRecordset "SELECT * FROM RateRec WHERE RateKey = " & rsLien.Fields("RateKey")
////			// If rsRR.EndOfFile <> True Then
////			// MostRecentInterestDate = rsRR.Fields("BillDate")
////			// End If
////			// End If
////			// End If
////			// this just passes the date back out to store in the grid
////			dateTemp = MostRecentInterestDate;
////			boolIntAddedToBill = false;
////			if (BillingCode == "BILL")
////			{
////				// Determine How much is due for each period
////				Decimal Tax2;
////				Decimal Tax3;
////				Decimal Tax4;
////				if (boolWater)
////				{
////					Tax1 = FCConvert.ToDecimal(rsCollection.Get_Fields_Double("WPrinOwed") + rsCollection.Get_Fields_Double("WTaxOwed") - rsCollection.Get_Fields_Double("WTaxPaid") - rsCollection.Get_Fields_Double("WPrinPaid"));
////					// initialize the period taxes
////					if (rsCollection.Get_Fields_Double("WIntAdded") == 0)
////					{
////						boolIntAddedToBill = true;
////					}
////				}
////				else
////				{
////					Tax1 = FCConvert.ToDecimal(rsCollection.Get_Fields_Double("SPrinOwed") + rsCollection.Get_Fields_Double("STaxOwed") - rsCollection.Get_Fields_Double("STaxPaid") - rsCollection.Get_Fields_Double("SPrinPaid"));
////					// initialize the period taxes
////					if (rsCollection.Get_Fields_Double("SIntAdded") == 0)
////					{
////						boolIntAddedToBill = true;
////					}
////				}
////			}
////			if (BillingCode == "LIEN")
////			{
////				// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
////				if (rsLien.Get_Fields("IntAdded") == 0)
////				{
////					boolIntAddedToBill = true;
////				}
////				rsRR.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsLien.Get_Fields_Int32("RateKey"), modExtraModules.strUTDatabase);
////				if (rsRR.EndOfFile() != true)
////				{
////					dtIntStartDate = (DateTime)rsRR.Get_Fields_DateTime("IntStart");
////				}
////				if (Information.IsDate(Statics.strUTInterestDate1) == true)
////				{
////					if (MostRecentInterestDate.ToOADate() >= Statics.UTEffectiveDate.ToOADate())
////					{
////						curCInt = 0;
////					}
////					else
////					{
////						if (modExtraModules.Statics.gstrRegularIntMethod == "P")
////						{
////							curCInt = FCConvert.ToDouble(modCLCalculations.CalculateInterest(FCConvert.ToDecimal(curPrin), MostRecentInterestDate, DateAndTime.DateValue(Strings.Format(Statics.UTEffectiveDate, "MM/dd/yyyy")), Statics.dblUTInterestRate, modGlobal.Statics.gintBasis, intUsingStartDate, modExtraModules.Statics.dblOverPayRate, ref curPD));
////						}
////						else if (modExtraModules.Statics.gstrRegularIntMethod == "F")
////						{
////							// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
////							if (rsLien.Get_Fields("IntAdded") == 0)
////							{
////								#if TWUT0000
////																		                                curCInt = modUTUseCR.Statics.gdblFlatInterestAmount;
////#else
////								curCInt = modStatusPayments.Statics.gdblFlatInterestAmount;
////								#endif
////							}
////						}
////						// curCInt = curCInt + InterestAmount
////					}
////				}
////			}
////			else
////			{
////				// vbPorter upgrade warning: PeriodInterest As Decimal	OnWrite(short, Decimal)
////				Decimal PeriodInterest = 0;
////				// Period 1
////				curCInt = FCConvert.ToDouble(InterestAmount);
////				rsRR.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsCollection.Get_Fields_Int32("RateKey"), modExtraModules.strUTDatabase);
////				if (rsRR.EndOfFile() != true)
////				{
////					dtIntStartDate = (DateTime)rsRR.Get_Fields_DateTime("IntStart");
////				}
////				if (Tax1 != 0)
////				{
////					if (Information.IsDate(Statics.strUTInterestDate1) == true)
////					{
////						if (MostRecentInterestDate.ToOADate() >= Statics.UTEffectiveDate.ToOADate())
////						{
////							PeriodInterest = 0;
////						}
////						else
////						{
////							curPDTemp = 0;
////							if (modExtraModules.Statics.gstrRegularIntMethod == "P")
////							{
////								PeriodInterest = modCLCalculations.CalculateInterest(Tax1, MostRecentInterestDate, DateAndTime.DateValue(Strings.Format(Statics.UTEffectiveDate, "MM/dd/yyyy")), Statics.dblUTInterestRate, modGlobal.Statics.gintBasis, intUsingStartDate, modExtraModules.Statics.dblOverPayRate, ref curPDTemp);
////							}
////							else if (modExtraModules.Statics.gstrRegularIntMethod == "F")
////							{
////								if (boolWater)
////								{
////									if (rsCollection.Get_Fields_Double("WIntAdded") == 0)
////									{
////										#if TWUT0000
////																						                                        curCInt = modUTUseCR.Statics.gdblFlatInterestAmount;
////#else
////										curCInt = modStatusPayments.Statics.gdblFlatInterestAmount;
////										#endif
////									}
////								}
////								else
////								{
////									if (rsCollection.Get_Fields_Double("SIntAdded") == 0)
////									{
////										#if TWUT0000
////																						                                        curCInt = modUTUseCR.Statics.gdblFlatInterestAmount;
////#else
////										curCInt = modStatusPayments.Statics.gdblFlatInterestAmount;
////										#endif
////									}
////								}
////							}
////							curCInt += FCConvert.ToDouble(PeriodInterest);
////							curPD += curPDTemp;
////						}
////					}
////				}
////			}
////			DetermineUTInterest = FCConvert.ToDecimal(curCInt);
////			rsRR.Reset();
////			return DetermineUTInterest;
////		}
//		// vbPorter upgrade warning: 'Return' As Decimal	OnWriteFCConvert.ToDouble(
//		private static Decimal DetermineUTBillCosts(ref clsDRWrapper rsCollection, ref bool boolWater)
//		{
//			Decimal DetermineUTBillCosts = 0;
//			string strWS = "";
//			double dblOwed = 0;
//			double dblAdded = 0;
//			double dblPaid = 0;
//			if (boolWater)
//			{
//				strWS = "W";
//			}
//			else
//			{
//				strWS = "S";
//			}
//			if (fecherFoundation.FCUtils.IsNull(rsCollection.Get_Fields(strWS + "CostOwed")) == true)
//			{
//				dblOwed = 0;
//			}
//			else
//			{
//				dblOwed = rsCollection.Get_Fields(strWS + "CostOwed");
//			}
//			if (fecherFoundation.FCUtils.IsNull(rsCollection.Get_Fields(strWS + "CostPaid")) == true)
//			{
//				dblPaid = 0;
//			}
//			else
//			{
//				dblPaid = rsCollection.Get_Fields(strWS + "CostPaid");
//			}
//			if (fecherFoundation.FCUtils.IsNull(rsCollection.Get_Fields(strWS + "CostAdded")) == true)
//			{
//				dblAdded = 0;
//			}
//			else
//			{
//				dblAdded = rsCollection.Get_Fields(strWS + "CostAdded");
//			}
//			DetermineUTBillCosts = FCConvert.ToDecimal(dblOwed - dblAdded - dblPaid);
//			return DetermineUTBillCosts;
//		}

//		private static Decimal DetermineUTLienCosts(ref clsDRWrapper rsLien, ref bool boolWater)
//		{
//			Decimal DetermineUTLienCosts = 0;
//			// TODO: Field [CostOwed] not found!! (maybe it is an alias?)
//			// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
//			DetermineUTLienCosts = rsLien.Get_Fields("CostOwed") - rsLien.Get_Fields("MaturityFee") - rsLien.Get_Fields_Double("CostPaid");
//			return DetermineUTLienCosts;
//		}
		// vbPorter upgrade warning: BillingCode As string	OnWrite(FixedString)
		//public static decimal DetermineUTPrincipal_54(clsDRWrapper rsCollection, clsDRWrapper rsLien, string BillingCode, bool boolWater)
		//{
		//	return DetermineUTPrincipal(ref rsCollection, ref rsLien, ref BillingCode, ref boolWater);
		//}

		//public static decimal DetermineUTPrincipal(ref clsDRWrapper rsCollection, ref clsDRWrapper rsLien, ref string BillingCode, ref bool boolWater)
		//{
		//	decimal DetermineUTPrincipal = 0;
		//	decimal curPDue = 0;
		//	string strWS = "";
		//	if (boolWater)
		//	{
		//		strWS = "W";
		//	}
		//	else
		//	{
		//		strWS = "S";
		//	}
		//	if (BillingCode == "BILL")
		//	{
		//		curPDue = FCConvert.ToDecimal(rsCollection.Get_Fields(strWS + "PrinOwed"));
		//		curPDue -= FCConvert.ToDecimal(rsCollection.Get_Fields(strWS + "PrinPaid"));
		//	}
		//	else if (BillingCode == "LIEN")
		//	{
		//		// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//		// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//		curPDue = FCConvert.ToDecimal(rsLien.Get_Fields("Principal")) - FCConvert.ToDecimal(rsLien.Get_Fields("PrinPaid"));
		//	}
		//	DetermineUTPrincipal = curPDue;
		//	return DetermineUTPrincipal;
		//}
		// vbPorter upgrade warning: BillingCode As string	OnWrite(FixedString)
		//public static decimal DetermineUTTax_54(clsDRWrapper rsCollection, clsDRWrapper rsLien, string BillingCode, bool boolWater)
		//{
		//	return DetermineUTTax(ref rsCollection, ref rsLien, ref BillingCode, ref boolWater);
		//}

		//public static decimal DetermineUTTax(ref clsDRWrapper rsCollection, ref clsDRWrapper rsLien, ref string BillingCode, ref bool boolWater)
		//{
		//	decimal DetermineUTTax = 0;
		//	decimal curPDue = 0;
		//	string strWS = "";
		//	if (boolWater)
		//	{
		//		strWS = "W";
		//	}
		//	else
		//	{
		//		strWS = "S";
		//	}
		//	if (BillingCode == "BILL")
		//	{
		//		curPDue = FCConvert.ToDecimal(rsCollection.Get_Fields(strWS + "TaxOwed"));
		//		curPDue -= FCConvert.ToDecimal(rsCollection.Get_Fields(strWS + "TaxPaid"));
		//	}
		//	else if (BillingCode == "LIEN")
		//	{
		//		// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
		//		// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//		curPDue = FCConvert.ToDecimal(rsLien.Get_Fields("Tax")) - FCConvert.ToDecimal(rsLien.Get_Fields("TaxPaid"));
		//	}
		//	DetermineUTTax = curPDue;
		//	return DetermineUTTax;
		//}

		//private static int FindDefaultUTBill(ref int Acct, ref bool boolWater, bool boolReversal = false)
		//{
		//	int FindDefaultUTBill = 0;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		double dblTemp;
		//		string strSQL;
		//		clsDRWrapper rsDefYear = new clsDRWrapper();
		//		clsDRWrapper rsDefLYear = new clsDRWrapper();
		//		int intCount;
		//		string strWS = "";
		//		dblTemp = 0;
		//		if (boolWater)
		//		{
		//			strWS = "W";
		//		}
		//		else
		//		{
		//			strWS = "S";
		//		}
		//		strSQL = "SELECT * FROM (" + Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(Acct) + " ORDER BY BillNumber";
		//		if (rsDefYear.OpenRecordset(strSQL, modExtraModules.strUTDatabase))
		//		{
		//			if (rsDefYear.EndOfFile() != true && rsDefYear.BeginningOfFile() != true)
		//			{
		//				rsDefYear.MoveLast();
		//				FindDefaultUTBill = FCConvert.ToInt32(rsDefYear.Get_Fields_Int32("BillNumber"));
		//				rsDefYear.MoveFirst();
		//				do
		//				{
		//					dblTemp = 0;
		//					if (FCConvert.ToInt32(rsDefYear.Get_Fields(strWS + "LienRecordNumber")) == 0)
		//					{
		//						// if this record does not have a lienrecord associated
		//						dblTemp = modGlobal.Round(rsDefYear.Get_Fields(strWS + "PrinOwed") + rsDefYear.Get_Fields(strWS + "TaxOwed") + rsDefYear.Get_Fields(strWS + "IntOwed") + rsDefYear.Get_Fields(strWS + "CostOwed") - rsDefYear.Get_Fields(strWS + "CostAdded") - rsDefYear.Get_Fields(strWS + "IntAdded"), 2);
		//						dblTemp -= modGlobal.Round(rsDefYear.Get_Fields(strWS + "PrinPaid") + rsDefYear.Get_Fields(strWS + "TaxPaid") + rsDefYear.Get_Fields(strWS + "IntPaid") + rsDefYear.Get_Fields(strWS + "CostPaid"), 2);
		//						// If boolReversal Then
		//						if (frmUTCLStatus.InstancePtr.vsPayments.FindRow(rsDefYear.Get_Fields_Int32("BillingRateKey"), 1, frmUTCLStatus.InstancePtr.lngPayGridColBill, true, true) != -1)
		//						{
		//							dblTemp = 0;
		//						}
		//						// End If
		//					}
		//					else
		//					{
		//						strSQL = "SELECT * FROM Lien WHERE ID = " + rsDefYear.Get_Fields(strWS + "LienRecordNumber");
		//						rsDefLYear.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
		//						if (rsDefLYear.RecordCount() != 0)
		//						{
		//							if ((boolWater && FCConvert.ToBoolean(rsDefLYear.Get_Fields_Boolean("Water"))) || (!boolWater && !FCConvert.ToBoolean(rsDefLYear.Get_Fields_Boolean("Water"))))
		//							{
		//								// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//								// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//								dblTemp = modGlobal.Round((Conversion.Val(rsDefLYear.Get_Fields("Principal")) + Conversion.Val(rsDefLYear.Get_Fields("Interest")) - (Conversion.Val(rsDefLYear.Get_Fields("IntAdded")) + Conversion.Val(rsDefLYear.Get_Fields("MaturityFee"))) + Conversion.Val(rsDefLYear.Get_Fields("Costs"))) - (Conversion.Val(rsDefLYear.Get_Fields("PrinPaid")) + Conversion.Val(rsDefLYear.Get_Fields("IntPaid")) + Conversion.Val(rsDefLYear.Get_Fields_Double("CostPaid"))), 2);
		//							}
		//						}
		//						// If boolReversal Then
		//						if (frmUTCLStatus.InstancePtr.vsPayments.FindRow(rsDefLYear.Get_Fields_Int32("RateKey"), 1, frmUTCLStatus.InstancePtr.lngPayGridColBill, true, true) != 1)
		//						{
		//							dblTemp = 0;
		//						}
		//						// End If
		//					}
		//					if (dblTemp != 0)
		//					{
		//						if (FCConvert.ToInt32(rsDefYear.Get_Fields_Int32("BillNumber")) < FindDefaultUTBill)
		//						{
		//							FindDefaultUTBill = FCConvert.ToInt32(rsDefYear.Get_Fields_Int32("BillNumber"));
		//						}
		//						return FindDefaultUTBill;
		//					}
		//					rsDefYear.MoveNext();
		//				}
		//				while (!(rsDefYear.EndOfFile() == true));
		//				rsDefYear.MoveLast();
		//				FindDefaultUTBill = FCConvert.ToInt32(rsDefYear.Get_Fields_Int32("BillNumber"));
		//			}
		//		}
		//		else
		//		{
		//			MessageBox.Show("Error finding default bill.", "Open Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//		}
		//		rsDefYear.Reset();
		//		rsDefLYear.Reset();
		//		return FindDefaultUTBill;
		//	}
		//	catch (Exception ex)
		//	{
				
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Default Bill", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return FindDefaultUTBill;
		//}

		//public static short FillUTBillNumber_6(bool boolWater, bool boolBoth)
		//{
		//	return FillUTBillNumber(ref boolWater, ref boolBoth);
		//}

		//public static short FillUTBillNumber_8(bool boolWater, bool boolBoth)
		//{
		//	return FillUTBillNumber(ref boolWater, ref boolBoth);
		//}

		//public static short FillUTBillNumber(ref bool boolWater, ref bool boolBoth)
		//{
		//	short FillUTBillNumber = 0;
		//	// this will find all of the bills that are needed to be shown for this account by
		//	// checking the bill records and adding two for a prepay year and an "Auto" selection
		//	double dblTemp;
		//	string strSQL;
		//	clsDRWrapper rsDefYear = new clsDRWrapper();
		//	clsDRWrapper rsDefLYear = new clsDRWrapper();
		//	int intCount;
		//	string strWS = "";
		//	bool boolNoRateKeyBill = false;
		//	int intCT;
		//	if (boolWater)
		//	{
		//		strWS = "W";
		//	}
		//	else
		//	{
		//		strWS = "S";
		//	}
		//	frmUTCLStatus.InstancePtr.boolOnlyPrepayYear = true;
		//	dblTemp = 0;
		//	frmUTCLStatus.InstancePtr.cmbBillNumber.Clear();
		//	// If Val(gstrLastYearBilled) > 0 Then
		//	// FillUTBillNumber = Val(gstrLastYearBilled)
		//	// Else
		//	// FillUTBillNumber = Year(Now)  'this better not happen...if so then it will probably be 50% wrong
		//	// End If
		//	strSQL = "SELECT * FROM (" + Statics.strUTCurrentAccountBills + ") AS Info LEFT JOIN RateKeys ON Info.BillingRateKey = RateKeys.ID ORDER BY BillNumber desc, Start";
		//	rsDefYear.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
		//	if (rsDefYear.BeginningOfFile() != true && rsDefYear.EndOfFile() != true)
		//	{
		//		FillUTBillNumber = FCConvert.ToInt16(rsDefYear.Get_Fields_Int32("BillNumber"));
		//		// find the highest number bill
		//		do
		//		{
		//			// DJW@03112010 Comented otu section checking balance and then moved following section where bill number was added into an if else statement so it is only added if it is not liened
		//			// If rsDefYear.Fields(strWS & "LienRecordNumber") = 0 Then    'if this record does not have a lienrecord associated
		//			// With rsDefYear
		//			// dblTemp = .Fields("WPrinOwed") - .Fields("WPrinPaid") + .Fields("WTaxOwed") - .Fields("WTaxPaid") + .Fields("WIntOwed") + .Fields("WIntAdded") - .Fields("WIntPaid") + .Fields("WCostOwed") - .Fields("WCostAdded") - .Fields("WCostPaid")
		//			// dblTemp = Round(dblTemp + .Fields("SPrinOwed") - .Fields("SPrinPaid") + .Fields("STaxOwed") - .Fields("STaxPaid") + .Fields("SIntOwed") + .Fields("SIntAdded") - .Fields("SIntPaid") + .Fields("SCostOwed") - .Fields("SCostAdded") - .Fields("SCostPaid"), 2)
		//			// End With
		//			// Else
		//			// With rsDefLYear
		//			// strSQL = "SELECT * FROM Lien WHERE LienKey = " & rsDefYear.Fields(strWS & "LienRecordNumber")
		//			// rsDefLYear.OpenRecordset strSQL, strUTDatabase
		//			// If rsDefLYear.RecordCount <> 0 Then
		//			// dblTemp = Round(.Fields("Principal") - .Fields("PrinPaid") + .Fields("Tax") - .Fields("TaxPaid") + .Fields("Interest") + .Fields("IntAdded") - .Fields("IntPaid") + .Fields("Costs") - .Fields("CostPaid"), 2)
		//			// End If
		//			// End With
		//			// End If
		//			// 
		//			// If dblTemp <> 0 Then
		//			// If rsDefYear.Fields("BillNumber") < FillUTBillNumber Then
		//			// FillUTBillNumber = rsDefYear.Fields("BillNumber")
		//			// End If
		//			// End If
		//			if (fecherFoundation.FCUtils.IsNull(rsDefYear.Get_Fields_Int32("BillNumber")) == false)
		//			{
		//				if (Conversion.Val(rsDefYear.Get_Fields_Int32("BillNumber")) <= FillUTBillNumber)
		//				{
		//					if ((boolBoth || boolWater) && (FCConvert.ToString(rsDefYear.Get_Fields_String("Service")) == "W" || FCConvert.ToString(rsDefYear.Get_Fields_String("Service")) == "B"))
		//					{
		//						if (FCConvert.ToInt32(rsDefYear.Get_Fields_Int32("WLienRecordNumber")) != 0)
		//						{
		//							// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//							if (rsDefYear.Get_Fields("Bill") == rsDefYear.Get_Fields_Int32("WCombinationLienKey"))
		//							{
		//								// add the lien ID if this is the primary bill
		//								frmUTCLStatus.InstancePtr.cmbBillNumber.AddItem(FCConvert.ToString(ReturnLienRateKey_2(FCConvert.ToInt32(rsDefYear.Get_Fields_Int32("WLienRecordNumber")))));
		//							}
		//							else
		//							{
		//								// do not add bills that people should not be able to adjust since they have been liened
		//							}
		//						}
		//						else
		//						{
		//							frmUTCLStatus.InstancePtr.cmbBillNumber.AddItem(FCConvert.ToString(rsDefYear.Get_Fields_Int32("BillNumber")));
		//						}
		//					}
		//					if ((boolBoth || !boolWater) && (FCConvert.ToString(rsDefYear.Get_Fields_String("Service")) == "S" || FCConvert.ToString(rsDefYear.Get_Fields_String("Service")) == "B"))
		//					{
		//						if (FCConvert.ToInt32(rsDefYear.Get_Fields_Int32("SLienRecordNumber")) != 0)
		//						{
		//							// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//							if (rsDefYear.Get_Fields("Bill") == rsDefYear.Get_Fields_Int32("SCombinationLienKey"))
		//							{
		//								// add the lien ID if this is the primary bill
		//								frmUTCLStatus.InstancePtr.cmbBillNumber.AddItem(FCConvert.ToString(ReturnLienRateKey_2(FCConvert.ToInt32(rsDefYear.Get_Fields_Int32("SLienRecordNumber")))));
		//							}
		//							else
		//							{
		//								// do not add bills that people should not be able to adjust since they have been liened
		//							}
		//						}
		//						else
		//						{
		//							frmUTCLStatus.InstancePtr.cmbBillNumber.AddItem(FCConvert.ToString(rsDefYear.Get_Fields_Int32("BillNumber")));
		//						}
		//					}
		//				}
		//			}
		//			if (FCConvert.ToInt32(rsDefYear.Get_Fields_Int32("BillNumber")) != 0)
		//			{
		//				frmUTCLStatus.InstancePtr.boolOnlyPrepayYear = false;
		//			}
		//			rsDefYear.MoveNext();
		//		}
		//		while (!rsDefYear.EndOfFile());
		//		if (rsDefYear.EndOfFile())
		//			rsDefYear.MoveFirst();
		//		while (!rsDefYear.EndOfFile())
		//		{
		//			if (rsDefYear.Get_Fields_Double("WPrinOwed") > 0 || rsDefYear.Get_Fields_Double("SPrinOwed") > 0)
		//			{
		//				break;
		//			}
		//			rsDefYear.MoveNext();
		//		}
		//		// no need to add extra bills because they will see a bill as soon as one is DE
		//		// If rsDefYear.BeginningOfFile <> True Then   'take the last billed year that has Tax billed to it
		//		// frmUTCLStatus.cmbBillNumber.AddItem rsDefYear.Fields("BillNumber") + 1 & "*"
		//		// frmUTCLStatus.cmbBillNumber.AddItem rsDefYear.Fields("BillNumber") + 2 & "*"
		//		// Else
		//		// frmUTCLStatus.cmbBillNumber.AddItem "1*"
		//		// frmUTCLStatus.cmbBillNumber.AddItem "2*"
		//		// End If
		//	}
		//	for (intCT = 0; intCT <= frmUTCLStatus.InstancePtr.cmbBillNumber.Items.Count - 1; intCT++)
		//	{
		//		if (FCConvert.ToDouble(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[intCT].ToString()) == 0)
		//		{
		//			boolNoRateKeyBill = true;
		//		}
		//	}
		//	if (!boolNoRateKeyBill)
		//	{
		//		// this will add a blank bill option to select and it will create a new blank bill for prepays
		//		frmUTCLStatus.InstancePtr.cmbBillNumber.AddItem("0*");
		//	}
		//	// add the Auto Option
		//	frmUTCLStatus.InstancePtr.cmbBillNumber.AddItem("Auto");
		//	//FC:FINAL:DDU:#i1009 - set cmbBillNumber index too
		//	frmUTCLStatus.InstancePtr.cmbBillNumber.Text = "Auto";
		//	return FillUTBillNumber;
		//}

		//public static void Format_UTvsPeriod()
		//{
		//	int wid = 0;
		//	wid = frmUTCLStatus.InstancePtr.vsPeriod.WidthOriginal;
		//	frmUTCLStatus.InstancePtr.vsPeriod.ColWidth(0, FCConvert.ToInt32(wid * 0.34));
		//	frmUTCLStatus.InstancePtr.vsPeriod.ColWidth(1, FCConvert.ToInt32(wid * 0.61));
		//	frmUTCLStatus.InstancePtr.vsPeriod.ColWidth(2, 0);
		//	frmUTCLStatus.InstancePtr.vsPeriod.ColWidth(3, 0);
		//	frmUTCLStatus.InstancePtr.vsPeriod.ColWidth(4, 0);
		//}

		//public static void CreateUTOppositionLine_6(int Row, bool boolWater, bool boolReversal = false)
		//{
		//	CreateUTOppositionLine(ref Row, ref boolWater, boolReversal);
		//}

		//public static void CreateUTOppositionLine_8(int Row, bool boolWater, bool boolReversal = false)
		//{
		//	CreateUTOppositionLine(ref Row, ref boolWater, boolReversal);
		//}

		//public static void CreateUTOppositionLine_24(int Row, bool boolWater, bool boolReversal = false)
		//{
		//	CreateUTOppositionLine(ref Row, ref boolWater, boolReversal);
		//}

		//public static void CreateUTOppositionLine_26(int Row, bool boolWater, bool boolReversal = false)
		//{
		//	CreateUTOppositionLine(ref Row, ref boolWater, boolReversal);
		//}

		//public static void CreateUTOppositionLine(ref int Row, ref bool boolWater, bool boolReversal = false)
		//{
		//	int Parent = 0;
		//	int intCT;
		//	int lngBill = 0;
		//	string strCode = "";
		//	string strPaymentCode = "";
		//	string strService = "";
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	clsDRWrapper rsTimeChk = new clsDRWrapper();
		//	int lngPaymentNumber = 0;
		//	bool boolGrandTotal = false;
		//	bool boolPending;
		//	bool boolUsePending;
		//	// vbPorter upgrade warning: dblP As double	OnWrite(double, short, string)
		//	double dblP = 0;
		//	// vbPorter upgrade warning: dblTax As double	OnWrite(double, short, string)
		//	double dblTax = 0;
		//	// vbPorter upgrade warning: dblInt As double	OnWrite(Decimal, double, string)
		//	double dblInt = 0;
		//	// vbPorter upgrade warning: dblCost As double	OnWrite(Decimal, short, string, double)
		//	double dblCost = 0;
		//	bool boolSavePayment = false;
		//	int lngRows = 0;
		//	bool boolPrepayment = false;
		//	int lngChgInterest = 0;
		//	DateTime dtPassDate = DateTime.FromOADate(0);
		//	DateTime dtNewIntDate;
		//	string strWS = "";
		//	int lngBillKey = 0;
		//	string strTempCD;
		//	//trout-1222
		//	string strTempCA;
		//	string strTempAcct;
		//	Statics.lngUTCHGINT = 0;
		//	if (Row > 0)
		//	{
		//		if (boolWater)
		//		{
		//			boolGrandTotal = FCConvert.CBool(frmUTCLStatus.InstancePtr.WGRID.RowOutlineLevel(Row) == 0 || (frmUTCLStatus.InstancePtr.WGRID.RowOutlineLevel(Row) == 2 && Strings.Trim(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColRef)) == "Total"));
		//		}
		//		else
		//		{
		//			boolGrandTotal = FCConvert.CBool(frmUTCLStatus.InstancePtr.SGRID.RowOutlineLevel(Row) == 0 || (frmUTCLStatus.InstancePtr.SGRID.RowOutlineLevel(Row) == 2 && Strings.Trim(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColRef)) == "Total"));
		//		}
		//		if (boolGrandTotal)
		//		{
		//			Parent = Row;
		//			if (boolWater)
		//			{
		//				lngRows = frmUTCLStatus.InstancePtr.WGRID.Rows - 1;
		//			}
		//			else
		//			{
		//				lngRows = frmUTCLStatus.InstancePtr.SGRID.Rows - 1;
		//			}
		//			if (Row == lngRows)
		//			{
		//				// this will set the year to auto
		//				for (intCT = 0; intCT <= frmUTCLStatus.InstancePtr.cmbBillNumber.Items.Count - 1; intCT++)
		//				{
		//					if ("Auto" == Strings.Trim(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[intCT].ToString()).Replace("*", ""))
		//					{
		//						if (intCT != frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex)
		//						{
		//							frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex = intCT;
		//						}
		//						break;
		//					}
		//				}
		//			}
		//			else
		//			{
		//				// this will set the year to the correct year
		//				if (boolWater)
		//				{
		//					for (intCT = 0; intCT <= frmUTCLStatus.InstancePtr.cmbBillNumber.Items.Count - 1; intCT++)
		//					{
		//						if (Strings.Trim(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(frmUTCLStatus.InstancePtr.LastParentRow(Row, ref boolWater), frmUTCLStatus.InstancePtr.lngGRIDColBillNumber)) == Strings.Trim(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[intCT].ToString()).Replace("*", ""))
		//						{
		//							if (intCT != frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex)
		//							{
		//								frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex = intCT;
		//							}
		//							break;
		//						}
		//					}
		//				}
		//				else
		//				{
		//					for (intCT = 0; intCT <= frmUTCLStatus.InstancePtr.cmbBillNumber.Items.Count - 1; intCT++)
		//					{
		//						if (Strings.Trim(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(frmUTCLStatus.InstancePtr.LastParentRow(Row, ref boolWater), frmUTCLStatus.InstancePtr.lngGRIDColBillNumber)) == Strings.Trim(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[intCT].ToString()).Replace("*", ""))
		//						{
		//							if (intCT != frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex)
		//							{
		//								frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex = intCT;
		//							}
		//							break;
		//						}
		//					}
		//				}
		//			}
		//			// collect info from the payment clicked on
		//			strCode = "=";
		//			strPaymentCode = "P";
		//			strService = "A";
		//		}
		//		else
		//		{
		//			Parent = frmUTCLStatus.InstancePtr.LastParentRow(Row, ref boolWater);
		//			// this finds the last parent row and stores it into Parent
		//			if (boolWater)
		//			{
		//				// MAL@20080610: Change to refer to the correct column for bill number
		//				// lngBill = Val(.WGRID.TextMatrix(Parent, frmUTCLStatus.lngGRIDCOLYear))
		//				lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Parent, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber))));
		//				// this will set the year
		//				for (intCT = 0; intCT <= frmUTCLStatus.InstancePtr.cmbBillNumber.Items.Count - 1; intCT++)
		//				{
		//					if (Conversion.Val(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Parent, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber)) == Conversion.Val(Strings.Trim(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[intCT].ToString()).Replace("*", "")))
		//					{
		//						frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex = intCT;
		//						break;
		//					}
		//				}
		//				// collect info from the payment clicked on
		//				strCode = frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColLineCode);
		//				strPaymentCode = frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColPaymentCode);
		//				strService = frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColService);
		//			}
		//			else
		//			{
		//				lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Parent, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber))));
		//				// this will set the year
		//				for (intCT = 0; intCT <= frmUTCLStatus.InstancePtr.cmbBillNumber.Items.Count - 1; intCT++)
		//				{
		//					if (Conversion.Val(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Parent, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber)) == Conversion.Val(Strings.Trim(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[intCT].ToString()).Replace("*", "")))
		//					{
		//						frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex = intCT;
		//						break;
		//					}
		//				}
		//				// collect info from the payment clicked on
		//				strCode = frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColLineCode);
		//				strPaymentCode = frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColPaymentCode);
		//				strService = frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColService);
		//			}
		//			frmUTCLStatus.InstancePtr.txtTransactionDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
		//			if (strService == "")
		//				strService = "A";
		//		}
		//		// default values
		//		// Dave 5/7/2007
		//		// .txtCD.Text = "Y"
		//		// .txtCash.Text = "Y"
		//		if (boolWater)
		//		{
		//			if (!boolReversal)//trout-1222 5.30.17 kjr Reversal to behave same as CL
		//			{
		//				frmUTCLStatus.InstancePtr.txtCD.Text = "Y";
		//				frmUTCLStatus.InstancePtr.txtCash.Text = "Y";
		//			}
		//			else
		//			{
		//				//Find the reversal payment and copy the CD and AC settings from the payment being reversed
		//				rsTemp.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColPaymentKey), modExtraModules.strUTDatabase);
		//				if (!rsTemp.EndOfFile())
		//				{
		//					if (FCConvert.ToString(rsTemp.Get_Fields_String("CashDrawer")) == "Y" && (FCConvert.ToDouble(rsTemp.Get_Fields_Int32("DailyCloseOut")) == 0 || (FCConvert.ToDouble(rsTemp.Get_Fields_Int32("DailyCloseOut")) == -100 && modGlobal.Statics.gboolPendingTransactions && modGlobal.Statics.gboolPendingRI)))
		//					{
		//						//if the payment is Y Y and has not been closed out then
		//						frmUTCLStatus.InstancePtr.txtCD.Text = "Y";
		//						frmUTCLStatus.InstancePtr.txtCash.Text = "Y";
		//					}
		//					else
		//					{
		//						frmUTCLStatus.InstancePtr.txtCD.Text = "N";
		//						if (FCConvert.ToString(rsTemp.Get_Fields_String("GeneralLedger")) == "N")
		//						{
		//							frmUTCLStatus.InstancePtr.txtCash.Text = "N";
		//							//check the account number as well
		//							frmUTCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0, rsTemp.Get_Fields_String("BudgetaryAccountNumber"));
		//						}
		//						else
		//						{
		//							frmUTCLStatus.InstancePtr.txtCash.Text = "Y";
		//						}
		//					}
		//				}
		//			}
		//			if (Strings.Trim(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColRef)) != "Total" && Strings.Trim(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColRef)) != "CURINT" && frmUTCLStatus.InstancePtr.WGRID.RowOutlineLevel(Row) != 0)
		//			{
		//				frmUTCLStatus.InstancePtr.txtReference.Text = "REVERSE";
		//				// DJW@20090209 Stopped clearing otu reference becuase it was erasing what peop[le had already entered in
		//				// Else
		//				// .txtReference = ""
		//			}
		//		}
		//		else
		//		{
		//			if (!boolReversal)
		//			{
		//				//trout-1222 5.30.17 kjr Reversal to behave same as CL
		//				frmUTCLStatus.InstancePtr.txtCD.Text = "Y";
		//				frmUTCLStatus.InstancePtr.txtCash.Text = "Y";
		//			}
		//			else
		//			{
		//				//Find the reversal payment and copy the CD and AC settings from the payment being reversed
		//				rsTemp.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColPaymentKey), modExtraModules.strUTDatabase);
		//				if (!rsTemp.EndOfFile())
		//				{
		//					if ((FCConvert.ToString(rsTemp.Get_Fields_String("CashDrawer")) == "Y" && FCConvert.ToDouble(rsTemp.Get_Fields_Int32("DailyCloseOut")) == 0 || (FCConvert.ToDouble(rsTemp.Get_Fields_Int32("DailyCloseOut")) == -100 && modGlobal.Statics.gboolPendingTransactions && modGlobal.Statics.gboolPendingRI)))
		//					{
		//						//if the payment is Y Y and has not been closed out then
		//						frmUTCLStatus.InstancePtr.txtCD.Text = "Y";
		//						frmUTCLStatus.InstancePtr.txtCash.Text = "Y";
		//					}
		//					else
		//					{
		//						frmUTCLStatus.InstancePtr.txtCD.Text = "N";
		//						if (FCConvert.ToString(rsTemp.Get_Fields_String("GeneralLedger")) == "N")
		//						{
		//							frmUTCLStatus.InstancePtr.txtCash.Text = "N";
		//							//check the account number as well
		//							frmUTCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0, rsTemp.Get_Fields_String("BudgetaryAccountNumber"));
		//						}
		//						else
		//						{
		//							frmUTCLStatus.InstancePtr.txtCash.Text = "Y";
		//						}
		//					}
		//				}
		//			}
		//			if (Strings.Trim(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColRef)) != "Total" && Strings.Trim(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColRef)) != "CURINT" && frmUTCLStatus.InstancePtr.SGRID.RowOutlineLevel(Row) != 0)
		//			{
		//				frmUTCLStatus.InstancePtr.txtReference.Text = "REVERSE";
		//				// Else
		//				// .txtReference = ""
		//			}
		//		}
		//		if (strPaymentCode == "P")
		//		{
		//			// if it is a payment, then I have to find out if a CHGINT line was created for it
		//			// and if it is the last payment
		//			strTempCA = frmUTCLStatus.InstancePtr.txtCash.Text;
		//			//trout-1222 5.30.17 kjr Reversal to behave same as CL
		//			strTempCD = frmUTCLStatus.InstancePtr.txtCD.Text;
		//			strTempAcct = frmUTCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0);
		//			// this will find the payment option in the list
		//			for (intCT = 0; intCT <= frmUTCLStatus.InstancePtr.cmbCode.Items.Count - 1; intCT++)
		//			{
		//				if (boolGrandTotal)
		//				{
		//					if ("P" == Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Items[intCT].ToString(), 1))
		//					{
		//						frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex = intCT;
		//						break;
		//					}
		//				}
		//				else
		//				{
		//					if ("C" == Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Items[intCT].ToString(), 1))
		//					{
		//						frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex = intCT;
		//						break;
		//					}
		//				}
		//			}
		//			if (boolReversal)//trout-1222 5.30.17 kjr Reversal to behave same as CL
		//			{
		//				frmUTCLStatus.InstancePtr.txtCash.Text = strTempCA;
		//				frmUTCLStatus.InstancePtr.txtCD.Text = strTempCD;
		//				frmUTCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0, strTempAcct);
		//			}
		//			if (boolGrandTotal)
		//			{
		//				// do not check the payments...just show the total
		//			}
		//			else
		//			{
		//				// find out if this is a correction on the last payment or the account and if it had interest charged
		//				if (boolWater)
		//				{
		//					lngPaymentNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColPaymentKey))));
		//					// holds the ID of the payment in question
		//				}
		//				else
		//				{
		//					lngPaymentNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColPaymentKey))));
		//					// holds the ID of the payment in question
		//				}
		//				// open the recordset containing all CHGINT records for this account and year in decending order by date
		//				// MAL@20070920: Changed the way it retrieves the CHGINT line for payment reversal
		//				rsTimeChk.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngPaymentNumber), modExtraModules.strUTDatabase);
		//				if (rsTimeChk.RecordCount() > 0)
		//				{
		//					lngChgInterest = FCConvert.ToInt32(rsTimeChk.Get_Fields_Int32("CHGINTNumber"));
		//				}
		//				rsTimeChk.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngChgInterest), modExtraModules.strUTDatabase);
		//				// If boolWater Then
		//				// rsTimeChk.OpenRecordset "SELECT * FROM (" & strUTCurrentAccountPayments & ") WHERE Reference = 'CHGINT' AND AccountKey = " & lngCurrentAccountUT & " AND BillNumber = " & lngBill & " ORDER BY ActualSystemDate desc", strUTDatabase
		//				// Else
		//				// rsTimeChk.OpenRecordset "SELECT * FROM (" & strUTCurrentAccountPayments & ") WHERE Reference = 'CHGINT' AND AccountKey = " & lngCurrentAccountUT & " AND BillNumber = " & lngBill & " ORDER BY ActualSystemDate desc", strUTDatabase
		//				// End If
		//				// check to see if there are any records
		//				if (rsTimeChk.RecordCount() != 0)
		//				{
		//					// If there is a CGHINT line for this account then open up all the payments for this account to find out if the payment chosen is the last payment to be applied
		//					// rsTemp.OpenRecordset "SELECT * FROM (" & strUTCurrentAccountPayments & ") WHERE AccountKey = " & lngCurrentAccountUT & " AND Reference <> 'CHGINT' AND BillNumber = " & lngBill & " AND ReceiptNumber <> -1 ORDER BY EffectiveInterestDate desc, ID desc", strUTDatabase
		//					rsTemp.OpenRecordset("SELECT * FROM (" + Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE Reference <> 'CHGINT' AND BillNumber = " + FCConvert.ToString(lngBill) + " AND ReceiptNumber <> -1 ORDER BY EffectiveInterestDate desc, ID desc", modExtraModules.strUTDatabase);
		//					if (rsTemp.RecordCount() != 0)
		//					{
		//						if (Strings.Trim(rsTemp.Get_Fields_String("Reference") + " ") != "CNVRSN")
		//						{
		//							// MAL@20071102: Check for empty bill number
		//							// Call Reference: 113872
		//							if (FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber")) > 0)
		//							{
		//								// and the latest one is not the conversion
		//								if (lngPaymentNumber == FCConvert.ToInt32(rsTemp.Get_Fields_Int32("ID")))
		//								{
		//									// is this payment the last one?
		//									if (boolWater)
		//									{
		//										strWS = "W";
		//										if (Conversion.Val(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColCHGINTNumber)) != 0)
		//										{
		//											// did it have a CHGINT applied to it...
		//											// if it is, then
		//											// rsTemp.OpenRecordset "SELECT * FROM PaymentRec WHERE ID = " & Val(.WGRID.TextMatrix(Row, 12)), strUTDatabase
		//											rsTemp.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngChgInterest), modExtraModules.strUTDatabase);
		//											// create a correction for the CHGINT but do not store it in the boxes
		//											if (rsTemp.RecordCount() > 0)
		//											{
		//												lngBillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillKey"));
		//												if (rsTemp.Get_Fields_DateTime("CHGINTDate").ToOADate() != 0)
		//												{
		//													// boolUseOldCHGINTDate = True
		//													dtPassDate = (DateTime)rsTemp.Get_Fields_DateTime("CHGINTDate");
		//												}
		//												else
		//												{
		//													dtPassDate = DateTime.Today;
		//												}
		//											}
		//											// set lngUTCHGINT so that it can be processed correctly when F9 is pressed
		//											Statics.lngUTCHGINT = FCConvert.ToInt32(Conversion.Val(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColCHGINTNumber)) * -1);
		//											// set boolUTMultiLine so that F10 cannot be pressed to save the payment, this has to be dealt with a F9 keystroke
		//											//Statics.boolUTMultiLine = false;
		//											//modStatusPayments.Statics.boolUseOldCHGINTDate = true;
		//											if (boolReversal)//trout-1222 5.30.17 kjr Reversal to behave same as CL
		//											{
		//												//this will revert the InterestAppliedThroughDate to the last one when the user save this payment
		//												Statics.boolUTUseOldCHGINTDate = true;
		//												boolSavePayment = true;
		//												Statics.boolUTMultiLine = false;
		//												//this does not need to be there because it will add the payment to the pending w/o an F11
		//											}
		//										}
		//										else
		//										{
		//											if (boolReversal)//trout-1222 5.30.17 kjr Reversal to behave same as CL
		//											{
		//												//this will revert the InterestAppliedThroughDate to the last one when the user save this payment
		//												Statics.boolUTUseOldCHGINTDate = true;
		//												boolSavePayment = true;
		//											}
		//										}
		//									}
		//									else
		//									{
		//										strWS = "S";
		//										if (Conversion.Val(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColCHGINTNumber)) != 0)
		//										{
		//											// did it have a CHGINT applied to it...
		//											// if it is, then
		//											// rsTemp.OpenRecordset "SELECT * FROM PaymentRec WHERE ID = " & Val(.SGRID.TextMatrix(Row, 12)), strUTDatabase
		//											rsTemp.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(lngChgInterest), modExtraModules.strUTDatabase);
		//											// create a correction for the CHGINT but do not store it in the boxes
		//											if (rsTemp.RecordCount() > 0)
		//											{
		//												lngBillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillKey"));
		//												if (rsTemp.Get_Fields_DateTime("CHGINTDate").ToOADate() != 0)
		//												{
		//													// boolUseOldCHGINTDate = True
		//													dtPassDate = (DateTime)rsTemp.Get_Fields_DateTime("CHGINTDate");
		//												}
		//												else
		//												{
		//													dtPassDate = DateTime.Today;
		//												}
		//											}
		//											// set lngUTCHGINT so that it can be processed correctly when F9 is pressed
		//											Statics.lngUTCHGINT = FCConvert.ToInt32(Conversion.Val(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColCHGINTNumber)) * -1);
		//											// set boolUTMultiLine so that F10 cannot be pressed to save the payment, this has to be dealt with a F9 keystroke
		//											//Statics.boolUTMultiLine = false;
		//											//modStatusPayments.Statics.boolUseOldCHGINTDate = true;
		//											if (boolReversal)//trout-1222 5.30.17 kjr Reversal to behave same as CL
		//											{
		//												//this will revert the InterestAppliedThroughDate to the last one when the user save this payment
		//												Statics.boolUTUseOldCHGINTDate = true;
		//												boolSavePayment = true;
		//												Statics.boolUTMultiLine = false;
		//												//this does not need to be there because it will add the payment to the pending w/o an F11
		//											}
		//										}
		//										else
		//										{
		//											if (boolReversal)//trout-1222 5.30.17 kjr Reversal to behave same as CL
		//											{
		//												//this will revert the InterestAppliedThroughDate to the last one when the user save this payment
		//												Statics.boolUTUseOldCHGINTDate = true;
		//												boolSavePayment = true;
		//												dtPassDate = DateTime.Now;
		//											}
		//										}
		//									}
		//								}
		//								else
		//								{
		//									MessageBox.Show("Only the last payment applied can be reversed when interest was charged.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//									return;
		//								}
		//							}
		//							else
		//							{
		//								MessageBox.Show("This transaction occured before the conversion, therefore it cannot be affected automatically", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//								return;
		//							}
		//						}
		//						else
		//						{
		//							MessageBox.Show("This transaction occured before the conversion, therefore it cannot be affected automatically", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//							return;
		//						}
		//					}
		//					else
		//					{
		//						MessageBox.Show("ERROR in Opposition Line Creation", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//						return;
		//					}
		//					// MAL@20070920: Finish the CHGINTdate reversal
		//					// Call Reference: 113872
		//					if (!modExtraModules.IsThisCR())
		//					{
		//						//kk04132016 trout-1224  Don't update the bill until reciept completed
		//						if (modStatusPayments.Statics.boolUseOldCHGINTDate)
		//						{
		//							//dtNewIntDate = GetLastInterestDate(lngCurrentAccountUT, lngBill, strWS, dtPassDate, lngPaymentNumber)
		//							//dtOldInterestDate = dtNewIntDate
		//							//If Not IsNull(dtNewIntDate) Then
		//							//rsTemp.OpenRecordset "SELECT * FROM Bill WHERE ID = " & lngBillKey, strUTDatabase
		//							//If rsTemp.RecordCount > 0 Then
		//							//rsTemp.Edit
		//							//If strWS = "W" Then
		//							//rsTemp.Fields("WIntPaidDate") = dtNewIntDate
		//							//Else
		//							//rsTemp.Fields("SIntPaidDate") = dtNewIntDate
		//							//End If
		//							//rsTemp.Update
		//							//End If
		//							//End If
		//							rsTemp.OpenRecordset("SELECT * FROM PaymentRec WHERE AccountKey = " + Statics.lngCurrentAccountKeyUT + " And BillNumber = " + lngBill, modExtraModules.strUTDatabase);
		//							//Is this a Lien?
		//							if (rsTemp.RecordCount() > 0)
		//							{
		//								if (FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("Lien")))
		//								{
		//									lngBillKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillKey"));
		//									dtNewIntDate = GetLastInterestDate_Lien(Statics.lngCurrentAccountKeyUT, lngBill, strWS, dtPassDate, lngPaymentNumber, lngBillKey);
		//									modGlobalConstants.Statics.dtOldInterestDate = dtNewIntDate;
		//									if (!FCUtils.IsNull(dtNewIntDate))
		//									{
		//										rsTemp.OpenRecordset("SELECT * FROM Lien WHERE ID = " + lngBillKey, modExtraModules.strUTDatabase);
		//										if (rsTemp.RecordCount() > 0)
		//										{
		//											rsTemp.Edit();
		//											rsTemp.Set_Fields("IntPaidDate", dtNewIntDate);
		//											rsTemp.Update();
		//											//trout-1222 kjr IntPaidDate set correctly here
		//										}
		//									}
		//								}
		//								else
		//								{
		//									dtNewIntDate = GetLastInterestDate(Statics.lngCurrentAccountKeyUT, lngBill, strWS, dtPassDate, lngPaymentNumber);
		//									modGlobalConstants.Statics.dtOldInterestDate = dtNewIntDate;
		//									if (!FCUtils.IsNull(dtNewIntDate))
		//									{
		//										rsTemp.OpenRecordset("SELECT * FROM Bill WHERE ID = " + lngBillKey, modExtraModules.strUTDatabase);
		//										if (rsTemp.RecordCount() > 0)
		//										{
		//											rsTemp.Edit();
		//										}
		//										if (strWS == "W")
		//										{
		//											rsTemp.Set_Fields("WIntPaidDate", dtNewIntDate);
		//										}
		//										else
		//										{
		//											rsTemp.Set_Fields("SIntPaidDate", dtNewIntDate);
		//										}
		//									}
		//									rsTemp.Update();
		//									//trout-1222 kjr SIntPaidDate set correctly here
		//								}
		//							}
		//						}
		//					}
		//				}
		//				else
		//				{
		//					// MAL@20071105: Check for older payments where CHGINT was not attached to bill
		//					rsTimeChk.OpenRecordset("SELECT * FROM PaymentRec WHERE BillNumber = " + FCConvert.ToString(lngBill) + " AND Reference = 'CHGINT'", modExtraModules.strUTDatabase);
		//					if (rsTimeChk.RecordCount() > 0)
		//					{
		//						MessageBox.Show("This transaction contains older information, please correct manually.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//						return;
		//					}
		//					//XXX'kk11022016 Not reversing CHGINT -leave out for now
		//					//kk10312016 trout-1225  Make reversal work like CL - automatically add to payment grid
		//					//no chg int line    '''trout - 1222
		//					if (boolReversal)
		//					{
		//						Statics.boolUTUseOldCHGINTDate = false;
		//						boolSavePayment = true;
		//					}
		//				}
		//			}
		//		}
		//		else if (strPaymentCode == "C")
		//		{
		//			strPaymentCode = "C";
		//			// this will find the payment option in the list
		//			for (intCT = 0; intCT <= frmUTCLStatus.InstancePtr.cmbCode.Items.Count - 1; intCT++)
		//			{
		//				if (strPaymentCode == Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Items[intCT].ToString(), 1))
		//				{
		//					frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex = intCT;
		//					break;
		//				}
		//			}
		//		}
		//		else if (strPaymentCode == "D")
		//		{
		//			// this needs to create a discount line
		//			strPaymentCode = "D";
		//			boolSavePayment = true;
		//		}
		//		else if (strPaymentCode == "Y")
		//		{
		//			// Prepayment
		//			for (intCT = 0; intCT <= frmUTCLStatus.InstancePtr.cmbCode.Items.Count - 1; intCT++)
		//			{
		//				if ("Y" == Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Items[intCT].ToString(), 1))
		//				{
		//					strPaymentCode = "Y";
		//					frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex = intCT;
		//					break;
		//				}
		//			}
		//			boolPrepayment = true;
		//		}
		//		else
		//		{
		//			// check to see if the payment that is going to be reversed is before the conversion
		//			// this will find the payment option in the list
		//			for (intCT = 0; intCT <= frmUTCLStatus.InstancePtr.cmbCode.Items.Count - 1; intCT++)
		//			{
		//				if (strPaymentCode == Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Items[intCT].ToString(), 1))
		//				{
		//					if ("C" == Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Items[intCT].ToString(), 1))
		//					{
		//						strPaymentCode = "C";
		//						frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex = intCT;
		//						break;
		//					}
		//					else
		//					{
		//						frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex = intCT;
		//						break;
		//					}
		//				}
		//			}
		//		}
		//		if (boolWater)
		//		{
		//			// set the total variables
		//			if (strCode == "=" || strCode == "-1" || strPaymentCode == "S")
		//			{
		//				// totals or lien row
		//				dblP = FCConvert.ToDouble(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColPrincipal));
		//				dblTax = FCConvert.ToDouble(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColTax));
		//				dblInt = FCConvert.ToDouble(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColInterest)));
		//				dblCost = FCConvert.ToDouble(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColCosts)));
		//				if (strPaymentCode != "C")
		//				{
		//					dblInt = dblP + dblInt + dblCost + dblTax;
		//					dblTax = 0;
		//					dblP = 0;
		//					dblCost = 0;
		//				}
		//				frmUTCLStatus.InstancePtr.txtComments.Text = "";
		//			}
		//			else
		//			{
		//				dblP = FCConvert.ToDouble(Strings.Format(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColPrincipal)) * -1, "#,##0.00"));
		//				dblTax = FCConvert.ToDouble(Strings.Format(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColTax)) * -1, "#,##0.00"));
		//				dblInt = FCConvert.ToDouble(Strings.Format(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColInterest)) * -1, "#,##0.00"));
		//				dblCost = FCConvert.ToDouble(Strings.Format(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColCosts)) * -1, "#,##0.00"));
		//				if (strPaymentCode == "Y")
		//				{
		//					dblInt = dblP + dblInt + dblCost;
		//					dblP = 0;
		//					dblCost = 0;
		//				}
		//				frmUTCLStatus.InstancePtr.txtComments.Text = "";
		//			}
		//		}
		//		else
		//		{
		//			// set the total variables
		//			if (strCode == "=" || strCode == "-1" || strPaymentCode == "S")
		//			{
		//				// totals or lien row
		//				dblP = FCConvert.ToDouble(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColPrincipal));
		//				dblTax = FCConvert.ToDouble(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColTax));
		//				dblInt = FCConvert.ToDouble(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColInterest)));
		//				dblCost = FCConvert.ToDouble(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColCosts)));
		//				if (strPaymentCode != "C")
		//				{
		//					dblInt = dblP + dblInt + dblCost + dblTax;
		//					dblP = 0;
		//					dblTax = 0;
		//					dblCost = 0;
		//				}
		//				frmUTCLStatus.InstancePtr.txtComments.Text = "";
		//			}
		//			else
		//			{
		//				dblP = FCConvert.ToDouble(Strings.Format(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColPrincipal)) * -1, "#,##0.00"));
		//				dblTax = FCConvert.ToDouble(Strings.Format(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColTax)) * -1, "#,##0.00"));
		//				dblInt = FCConvert.ToDouble(Strings.Format(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColInterest)) * -1, "#,##0.00"));
		//				dblCost = FCConvert.ToDouble(Strings.Format(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(Row, frmUTCLStatus.InstancePtr.lngGRIDColCosts)) * -1, "#,##0.00"));
		//				frmUTCLStatus.InstancePtr.txtComments.Text = "";
		//				if (strPaymentCode == "Y")
		//				{
		//					dblInt = dblP + dblInt + dblCost;
		//					dblP = 0;
		//					dblCost = 0;
		//				}
		//			}
		//		}
		//		// if the user is creating an opposition line for a total line then
		//		if (strCode == "=")
		//		{
		//			// check for pending payments
		//			if (boolGrandTotal)
		//			{
		//				// this will take into account any pending payments
		//				for (intCT = 1; intCT <= frmUTCLStatus.InstancePtr.vsPayments.Rows - 1; intCT++)
		//				{
		//					if (frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmUTCLStatus.InstancePtr.lngPayGridColCode) == "P")
		//					{
		//					}
		//					else
		//					{
		//					}
		//				}
		//			}
		//			else
		//			{
		//				// this will take into account any pending payments for the year of the bill
		//				for (intCT = 1; intCT <= frmUTCLStatus.InstancePtr.vsPayments.Rows - 1; intCT++)
		//				{
		//					if (Conversion.Val(modExtraModules.FormatYear(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmUTCLStatus.InstancePtr.lngPayGridColBill))) == lngBill)
		//					{
		//						if (frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmUTCLStatus.InstancePtr.lngPayGridColCode) == "P")
		//						{
		//							dblP -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmUTCLStatus.InstancePtr.lngPayGridColPrincipal));
		//							dblTax -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmUTCLStatus.InstancePtr.lngPayGridColTax));
		//							dblInt -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest));
		//							dblCost -= FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmUTCLStatus.InstancePtr.lngPayGridColCosts));
		//						}
		//						else
		//						{
		//							dblP += FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmUTCLStatus.InstancePtr.lngPayGridColPrincipal));
		//							dblTax += FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmUTCLStatus.InstancePtr.lngPayGridColTax));
		//							dblInt += FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmUTCLStatus.InstancePtr.lngPayGridColInterest));
		//							dblCost += FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(intCT, frmUTCLStatus.InstancePtr.lngPayGridColCosts));
		//						}
		//					}
		//				}
		//			}
		//		}
		//		else
		//		{
		//			// this is a partial payment opposition line
		//		}
		//		frmUTCLStatus.InstancePtr.txtPrincipal.Text = Strings.Format(dblP, "#,##0.00");
		//		frmUTCLStatus.InstancePtr.txtTax.Text = Strings.Format(dblTax, "#,##0.00");
		//		frmUTCLStatus.InstancePtr.txtInterest.Text = Strings.Format(dblInt, "#,##0.00");
		//		frmUTCLStatus.InstancePtr.txtCosts.Text = Strings.Format(dblCost, "#,##0.00");
		//		// FC:FINAL:VGE - #i1020 Setting condition parameter to method's one (Similar to VB6 behavior)
		//		//boolSavePayment = boolSavePayment || boolReversal;
		//		if (boolSavePayment || boolPrepayment)
		//		{
		//			//AddUTPaymentToList_1133(1, false, true, FCConvert.CBool(strPaymentCode == "D"), boolPrepayment); //kk10312016 trout-1225  Add lngPaymentNumber for reversal
		//			AddUTPaymentToList(frmUTCLStatus.InstancePtr.vsPayments.Rows, true, true, FCConvert.CBool(strPaymentCode == "D"), false, 0, lngPaymentNumber, boolPrepayment);
		//			//XXX'kk12072016 trout-1225  Try adding the ChgInt reversal back in (this was commented out
		//			// If lngUTCHGINT > 0 Then
		//			// AddUTCHGINTToList 2, boolWater, False, False, -1     'then add the reverse of the interest charged to the payment grid
		//			// End If
		//			// kk10072015 trocr-446  Reset payment boxes after payment
		//			//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
		//			//ResetUTPaymentFrame_8(FCConvert.CBool(Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Items[frmUTCLStatus.InstancePtr.cmbService.SelectedIndex].ToString(), 1) == "W"), true);
		//			ResetUTPaymentFrame_8(FCConvert.CBool(Strings.Left(frmUTCLStatus.InstancePtr.cmbService.Text, 1) == "W"), true);
		//		}
		//	}
		//	rsTimeChk.Reset();
		//	rsTemp.Reset();
		//}

		//public static bool UTPaymentGreaterThanTotalOwed()
		//{
		//	bool UTPaymentGreaterThanTotalOwed = false;
		//	// vbPorter upgrade warning: sumPayment As double	OnWriteFCConvert.ToDecimal(
		//	double sumPayment = 0;
		//	double sumOwed = 0;
		//	// this will test for both water and sewer totals
		//	if (Conversion.Val(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(frmUTCLStatus.InstancePtr.WGRID.Rows - 1, 11)) != 0)
		//	{
		//		sumOwed += FCConvert.ToDouble(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(frmUTCLStatus.InstancePtr.WGRID.Rows - 1, 11));
		//	}
		//	sumPayment = FCConvert.ToDouble(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.txtPrincipal.Text) + FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.txtTax.Text) + FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.txtInterest.Text) + FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.txtCosts.Text));
		//	UTPaymentGreaterThanTotalOwed = modGlobal.Round(sumPayment, 2) > modGlobal.Round(sumOwed, 2);
		//	return UTPaymentGreaterThanTotalOwed;
		//}

		//public static bool UTPaymentGreaterThanOwed_24(bool boolWater, bool boolBill, bool boolBoth)
		//{
		//	return UTPaymentGreaterThanOwed(ref boolWater, ref boolBill, ref boolBoth);
		//}

		//public static bool UTPaymentGreaterThanOwed(ref bool boolWater, ref bool boolBill, ref bool boolBoth)
		//{
		//	bool UTPaymentGreaterThanOwed = false;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// vbPorter upgrade warning: sumPayment As double	OnWriteFCConvert.ToDecimal(
		//		double sumPayment;
		//		// vbPorter upgrade warning: sumOwed As double	OnWrite(Decimal, short, string, double)
		//		double sumOwed = 0;
		//		if (boolBill)
		//		{
		//			if (Strings.Trim(frmUTCLStatus.InstancePtr.vsPeriod.TextMatrix(0, 1)) != "")
		//			{
		//				// Trim(.vsPeriod.TextMatrix(0, 2)) <> "" And Trim(.vsPeriod.TextMatrix(0, 3)) <> "" And Trim(.vsPeriod.TextMatrix(0, 4)) <> "" Then
		//				sumOwed = FCConvert.ToDouble(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.vsPeriod.TextMatrix(0, 1)));
		//			}
		//		}
		//		else if (boolBoth)
		//		{
		//			sumOwed = 0;
		//			if (frmUTCLStatus.InstancePtr.WGRID.Rows > 1)
		//			{
		//				sumOwed = FCConvert.ToDouble(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(frmUTCLStatus.InstancePtr.WGRID.Rows - 1, frmUTCLStatus.InstancePtr.lngGRIDColTotal));
		//			}
		//			if (frmUTCLStatus.InstancePtr.SGRID.Rows > 1)
		//			{
		//				sumOwed += FCConvert.ToDouble(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(frmUTCLStatus.InstancePtr.SGRID.Rows - 1, frmUTCLStatus.InstancePtr.lngGRIDColTotal));
		//			}
		//		}
		//		else
		//		{
		//			if (boolWater)
		//			{
		//				if (frmUTCLStatus.InstancePtr.WGRID.Rows > 1)
		//				{
		//					sumOwed = FCConvert.ToDouble(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(frmUTCLStatus.InstancePtr.WGRID.Rows - 1, frmUTCLStatus.InstancePtr.lngGRIDColTotal));
		//				}
		//			}
		//			else
		//			{
		//				if (frmUTCLStatus.InstancePtr.SGRID.Rows > 1)
		//				{
		//					sumOwed = FCConvert.ToDouble(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(frmUTCLStatus.InstancePtr.SGRID.Rows - 1, frmUTCLStatus.InstancePtr.lngGRIDColTotal));
		//				}
		//			}
		//		}
		//		sumPayment = FCConvert.ToDouble(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.txtPrincipal.Text) + FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.txtTax.Text) + FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.txtInterest.Text) + FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.txtCosts.Text));
		//		if (sumPayment > 0)
		//		{
		//			UTPaymentGreaterThanOwed = modGlobal.Round(sumPayment, 2) > modGlobal.Round(sumOwed, 2);
		//		}
		//		else
		//		{
		//			// automatically pass adjustments being made to the account
		//			UTPaymentGreaterThanOwed = false;
		//			// Round(sumPayment, 2) < Round(sumOwed, 2)
		//		}
		//		return UTPaymentGreaterThanOwed;
		//	}
		//	catch (Exception ex)
		//	{
				
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Overpayment Check", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return UTPaymentGreaterThanOwed;
		//}

		//private static double FindUTCHGINTValue(int lngBillKey)//trout-1225 kjr 7.31.17 chg parm from year (CL?) to Billkey for UT
		//{
		//	double FindUTCHGINTValue = 0;
		//	// this function takes the year and returns the last CHGINT line's value for interest
		//	//int lngRow;
		//	bool boolFound;
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	string strTemp = "";
		//	string strSQL = "";
		//	boolFound = false;
		//	//kk02082017 trout-1225  WHAT IS THIS DOING?
		//	//if (modStatusPayments.Statics.boolRE)
		//	//         {
		//	//             // MAL@20071026: Removed redundant where statement for Account
		//	//             // strSQL = "SELECT * FROM (" & strUTCurrentAccountPayments & ") WHERE (Reference = 'CHGINT' OR Reference = 'CNVRSN') AND Account = " & CurrentAccountRE & " AND Year = " & intYR & " AND BillCode <> 'P' ORDER BY RecordedTransactionDate desc, ActualSystemDate desc"
		//	//             strSQL = "SELECT * FROM (" + Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE (Reference = 'CHGINT' OR Reference = 'CNVRSN') AND BillKey = " + FCConvert.ToString(intYR) + " AND Code <> 'P' ORDER BY RecordedTransactionDate desc, ActualSystemDate desc";
		//	//         }
		//	//         else
		//	//         {
		//	//             // strSQL = "SELECT * FROM (" & strUTCurrentAccountPayments & ") WHERE (Reference = 'CHGINT' OR Reference = 'CNVRSN') AND Account = " & CurrentAccountPP & " AND Year = " & intYR & " AND BillCode = 'P' ORDER BY RecordedTransactionDate desc, ActualSystemDate desc"
		//	//             strSQL = "SELECT * FROM (" + Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE (Reference = 'CHGINT' OR Reference = 'CNVRSN') AND BillKey = " + FCConvert.ToString(intYR) + " AND Code = 'P' ORDER BY RecordedTransactionDate desc, ActualSystemDate desc";
		//	//         }
		//	strSQL = "SELECT * FROM (" + Statics.strUTCurrentAccountPayments + ") As qTmp WHERE (Reference = 'CHGINT' OR Reference = 'CNVRSN') AND BillKey = " + lngBillKey + " AND Code <> 'P' ORDER BY RecordedTransactionDate desc, ActualSystemDate desc";
		//	rsTemp.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
		//	if (rsTemp.RecordCount() != 0)
		//	{
		//		strTemp = Strings.Trim(rsTemp.Get_Fields_String("Reference") + " ");
		//		if (strTemp == "CNVRSN")
		//		{
		//			MessageBox.Show("This transaction occured before the conversion, therefore it cannot be affected automatically", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//			boolFound = false;
		//		}
		//		else
		//		{
		//			boolFound = true;
		//			FindUTCHGINTValue = FCConvert.ToDouble(rsTemp.Get_Fields_Decimal("CurrentInterest"));
		//		}
		//	}
		//	rsTemp.Reset();
		//	return FindUTCHGINTValue;
		//}
		// vbPorter upgrade warning: intArrayIndex As short	OnWriteFCConvert.ToInt32(
		//7.31.17 trout-1225 kjr - dblUTCurrentInt not updated with Rev Int, passed in value instead
		//public static int CreateUTCHGINTLine_9(int intArrayIndex, ref DateTime dateTemp, bool boolUseOtherType = false, short intForceType = 0)
		//{
		//	return CreateUTCHGINTLine(ref intArrayIndex, ref dateTemp, 0, boolUseOtherType, intForceType);
		//}

		//public static int CreateUTCHGINTLine_18(int intArrayIndex, ref DateTime dateTemp, int lngLienOriginalBillKey = 0, bool boolUseOtherType = false, int intForceType = 0)
		//{
		//	return CreateUTCHGINTLine(ref intArrayIndex, ref dateTemp, lngLienOriginalBillKey, boolUseOtherType, intForceType);
		//}

		//public static int CreateUTCHGINTLine(ref int intArrayIndex, ref DateTime dateTemp, int lngLienOriginalBillKey = 0, bool boolUseOtherType = false, int intForceType = 0, double dblIntValue = 0)
		//{
		//	int CreateUTCHGINTLine = 0;
		//	// this function will create a charge interest line for the last payment in the series
		//	// and return its autonumber to the last payment's CHGINTNumber field
		//	clsDRWrapper rsCHGINT = new clsDRWrapper();
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	int intType = 0;
		//	int lngIntIndex = 0;
		//	string strService = "";
		//	double dblIntAmtLocal = 0;
		//	//kk12182017 Make a local copy so we don't accidentally change the value of the passed interest
		//	if (lngLienOriginalBillKey == 0)
		//	{
		//		lngIntIndex = Statics.UTPaymentArray[intArrayIndex].BillNumber;
		//	}
		//	else
		//	{
		//		lngIntIndex = lngLienOriginalBillKey;
		//	}
		//	if (Statics.UTPaymentArray[intArrayIndex].Service == "W")
		//	{
		//		intType = 0;
		//		// frmUTCLStatus.FindPaymentType
		//	}
		//	else
		//	{
		//		intType = 1;
		//	}
		//	// This will force the type to be the opposite in order for payments against both types
		//	// intType = intForceType
		//	if (intType == 0)
		//	{
		//		strService = "W";
		//	}
		//	else
		//	{
		//		strService = "S";
		//	}
		//	// If boolUseOtherType Then
		//	// If intType = 0 Then
		//	// intType = 1
		//	// Else
		//	// intType = 0
		//	// End If
		//	// End If
		//	//last seen on line 3395 revision 2779
		//	if (modGlobal.Round(dblIntValue, 2) != 0)
		//	{
		//		dblIntAmtLocal = modGlobal.Round(dblIntValue, 2);
		//	}
		//	else if (modGlobal.Round(Statics.dblUTCurrentInt[lngIntIndex, intType], 2) != 0)//12.14.17 hotfix  trout-1308  '7.31.17 trout - 1225 kjr - dblUTCurrentInt not updated with Rev Int
		//	{
		//		dblIntAmtLocal = modGlobal.Round(Statics.dblUTCurrentInt[lngIntIndex, intType], 2);
		//	}
		//	else
		//	{
		//		dblIntAmtLocal = 0;
		//	}
		//	if (modGlobal.Round(dblIntAmtLocal, 2) != 0)// 7.31.17 trout-1225 kjr - dblUTCurrentInt not updated with Rev Int
		//	{
		//		rsCHGINT.OpenRecordset("SELECT * FROM (" + Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE AccountKey = 0", modExtraModules.strUTDatabase);
		//		// use the other bills info
		//		rsCHGINT.AddNew();
		//		CreateUTCHGINTLine = rsCHGINT.Get_Fields_Int32("ID");
		//		rsCHGINT.Set_Fields("AccountKey", Statics.UTPaymentArray[intArrayIndex].Account);
		//		rsCHGINT.Set_Fields("Year", Statics.UTPaymentArray[intArrayIndex].Year);
		//		rsCHGINT.Set_Fields("Reversal", Statics.UTPaymentArray[intArrayIndex].Reversal);
		//		// trout-1225
		//		rsCHGINT.Set_Fields("SetEIDate", Statics.UTPaymentArray[intArrayIndex].SetEIDate);
		//		rsCHGINT.Set_Fields("BillKey", Statics.UTPaymentArray[intArrayIndex].BillKey);
		//		if (!modExtraModules.IsThisCR())
		//		{
		//			// check for cash receipting
		//			rsCHGINT.Set_Fields("ReceiptNumber", 0);
		//			// if this is from CR then do not update the billing or lien records and leave the Status as pending
		//			if (Statics.UTPaymentArray[intArrayIndex].BillCode != "L")
		//			{
		//				// regular billing
		//				rsTemp.OpenRecordset("SELECT Bill FROM (" + Statics.strUTCurrentAccountBills + ") AS qTmp WHERE Bill = " + FCConvert.ToString(Statics.UTPaymentArray[intArrayIndex].BillKey), modExtraModules.strUTDatabase);
		//				if (rsTemp.RecordCount() != 0)
		//				{
		//					rsTemp.OpenRecordset("select * from bill where id = " + Statics.UTPaymentArray[intArrayIndex].BillKey, modExtraModules.strUTDatabase);
		//					rsTemp.Edit();
		//					rsTemp.Set_Fields(strService + "IntAdded", (rsTemp.Get_Fields(strService + "IntAdded") + modGlobal.Round(dblIntAmtLocal, 2)));
		//					rsTemp.Update();
		//				}
		//				else
		//				{
		//					// CreateUTCHGINTPaymentRecord = False
		//					// GoTo END_OF_PAYMENT
		//				}
		//			}
		//			else
		//			{
		//				// lien
		//				rsTemp.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(Statics.UTPaymentArray[intArrayIndex].BillKey), modExtraModules.strUTDatabase);
		//				if (rsTemp.RecordCount() != 0)
		//				{
		//					rsTemp.Edit();
		//					// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//					rsTemp.Set_Fields("IntAdded", (rsTemp.Get_Fields("IntAdded") + modGlobal.Round(dblIntAmtLocal, 2)));
		//					rsTemp.Update();
		//				}
		//				else
		//				{
		//					// CreateUTCHGINTPaymentRecord = False
		//					// GoTo END_OF_PAYMENT
		//				}
		//			}
		//		}
		//		else
		//		{
		//			if (Statics.UTPaymentArray[intArrayIndex].ReversalID != -1)
		//			{
		//				// MAL@20071030
		//				rsCHGINT.Set_Fields("ReceiptNumber", 0);
		//			}
		//			else
		//			{
		//				rsCHGINT.Set_Fields("ReceiptNumber", -1);
		//			}
		//		}
		//		rsCHGINT.Set_Fields("CHGINTNumber", 0);
		//		if (Statics.UTPaymentArray[intArrayIndex].ReversalID == -1)
		//		{
		//			rsCHGINT.Set_Fields("CHGINTDate", Statics.UTPaymentArray[intArrayIndex].EffectiveInterestDate);
		//		}
		//		else
		//		{
		//			rsCHGINT.Set_Fields("CHGINTDate", Statics.dateUTOldDate[lngIntIndex, intType]);
		//		}
		//		rsCHGINT.Set_Fields("BillKey", Statics.UTPaymentArray[intArrayIndex].BillKey);
		//		rsCHGINT.Set_Fields("ActualSystemDate", Statics.UTPaymentArray[intArrayIndex].ActualSystemDate);
		//		rsCHGINT.Set_Fields("EffectiveInterestDate", Statics.UTPaymentArray[intArrayIndex].EffectiveInterestDate);
		//		rsCHGINT.Set_Fields("RecordedTransactionDate", Statics.UTPaymentArray[intArrayIndex].RecordedTransactionDate);
		//		rsCHGINT.Set_Fields("Teller", Statics.UTPaymentArray[intArrayIndex].Teller);
		//		rsCHGINT.Set_Fields("Period", "A");
		//		rsCHGINT.Set_Fields("Service", strService);
		//		rsCHGINT.Set_Fields("Code", "I");
		//		rsCHGINT.Set_Fields("Principal", 0);
		//		rsCHGINT.Set_Fields("PreLienInterest", 0);
		//		rsCHGINT.Set_Fields("CurrentInterest", modGlobal.Round(dblIntAmtLocal, 2));
		//		//7.31.17 trout-1225 kjr - dblUTCurrentInt not updated with Rev Int
		//		//rsCHGINT.Fields("CurrentInterest") = dblUTCurrentInt(lngIntIndex, intType)     ' 7.27.17 trout - 1225 kjr same as CL
		//		rsCHGINT.Set_Fields("CurrentInterest", Statics.dblUTCurrentInt[lngIntIndex, intType]);
		//		if (rsCHGINT.Get_Fields_Decimal("CurrentInterest") > 0)
		//		{
		//			rsCHGINT.Set_Fields("Reference", "EARNINT");
		//		}
		//		else
		//		{
		//			rsCHGINT.Set_Fields("Reference", "CHGINT");
		//		}
		//		rsCHGINT.Set_Fields("LienCost", 0);
		//		rsCHGINT.Set_Fields("TransNumber", FCConvert.ToString(Conversion.Val(Statics.UTPaymentArray[intArrayIndex].TransNumber)));
		//		rsCHGINT.Set_Fields("PaidBy", Statics.UTPaymentArray[intArrayIndex].PaidBy);
		//		rsCHGINT.Set_Fields("Comments", "");
		//		rsCHGINT.Set_Fields("CashDrawer", Statics.UTPaymentArray[intArrayIndex].CashDrawer);
		//		rsCHGINT.Set_Fields("GeneralLedger", Statics.UTPaymentArray[intArrayIndex].GeneralLedger);
		//		rsCHGINT.Set_Fields("BudgetaryAccountNumber", Statics.UTPaymentArray[intArrayIndex].BudgetaryAccountNumber);
		//		rsCHGINT.Set_Fields("Lien", FCConvert.CBool(Statics.UTPaymentArray[intArrayIndex].BillCode == "L"));
		//		dateTemp = Statics.UTEffectiveDate;
		//		// dateUTOldDate(.Year - utDEFAULTSUBTRACTIONVALUE)
		//		rsCHGINT.Update();
		//		CreateUTCHGINTLine = FCConvert.ToInt32(rsCHGINT.Get_Fields_Int32("ID"));
		//	}
		//	rsTemp.Reset();
		//	rsCHGINT.Reset();
		//	return CreateUTCHGINTLine;
		//}

		//public static void CreateUTPreview_6(int lngBill, bool boolWater)
		//{
		//	CreateUTPreview(ref lngBill, ref boolWater);
		//}

		//public static void CreateUTPreview(ref int lngBill, ref bool boolWater)
		//{
		//	// this will create a preview for the year passed in and show it to the user
		//	int lngStartRow = 0;
		//	int lngEndRow = 0;
		//	int indexPay;
		//	int rowPrev = 0;
		//	bool boolFound;
		//	FormatUTPreviewGrid();
		//	if (boolWater)
		//	{
		//		lngStartRow = frmUTCLStatus.InstancePtr.WGRID.FindRow(lngBill, 1, 2);
		//		if (lngStartRow < 0)
		//		{
		//			return;
		//		}
		//		lngEndRow = frmUTCLStatus.InstancePtr.WGRID.FindRow("=", lngStartRow, frmUTCLStatus.InstancePtr.lngGRIDColLineCode);
		//		frmUTCLStatus.InstancePtr.vsPreview.Rows = lngEndRow - lngStartRow + 2;
		//		frmUTCLStatus.InstancePtr.WGRID.Select(lngStartRow, 1, lngEndRow, frmUTCLStatus.InstancePtr.WGRID.Cols - 1);
		//		frmUTCLStatus.InstancePtr.vsPreview.Select(1, 1, frmUTCLStatus.InstancePtr.vsPreview.Rows - 1, frmUTCLStatus.InstancePtr.vsPreview.Cols - 1);
		//		frmUTCLStatus.InstancePtr.vsPreview.Clip = frmUTCLStatus.InstancePtr.WGRID.Clip;
		//	}
		//	else
		//	{
		//		lngStartRow = frmUTCLStatus.InstancePtr.SGRID.FindRow(lngBill, 1, 2);
		//		if (lngStartRow < 0)
		//		{
		//			return;
		//		}
		//		lngEndRow = frmUTCLStatus.InstancePtr.SGRID.FindRow("=", lngStartRow, frmUTCLStatus.InstancePtr.lngGRIDColLineCode);
		//		frmUTCLStatus.InstancePtr.vsPreview.Rows = lngEndRow - lngStartRow + 2;
		//		frmUTCLStatus.InstancePtr.SGRID.Select(lngStartRow, 1, lngEndRow, frmUTCLStatus.InstancePtr.SGRID.Cols - 1);
		//		frmUTCLStatus.InstancePtr.vsPreview.Select(1, 1, frmUTCLStatus.InstancePtr.vsPreview.Rows - 1, frmUTCLStatus.InstancePtr.vsPreview.Cols - 1);
		//		frmUTCLStatus.InstancePtr.vsPreview.Clip = frmUTCLStatus.InstancePtr.SGRID.Clip;
		//	}
		//	// Add Payment Lines
		//	for (indexPay = 1; indexPay <= frmUTCLStatus.InstancePtr.vsPayments.Rows - 1; indexPay++)
		//	{
		//		if (lngBill == Conversion.Val(modExtraModules.FormatYear(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColBill))) && frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColRef) != "CHGINT")
		//		{
		//			if (boolWater && frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColService) == "W")
		//			{
		//				AddPaymentToUTPreview_18(indexPay, rowPrev, true);
		//			}
		//			else if (!boolWater && frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColService) == "S")
		//			{
		//				AddPaymentToUTPreview_18(indexPay, rowPrev, false);
		//			}
		//		}
		//	}
		//	frmUTCLStatus.InstancePtr.vsPreview.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
		//	// merge the Original Bill line
		//	frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(1, 4, frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(1, 3));
		//	frmUTCLStatus.InstancePtr.vsPreview.MergeRow(1, true, 3 , 4);
		//	// merge the name field if needed
		//	if (frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(2, frmUTCLStatus.InstancePtr.lngGRIDColRef) == "Billed To:")
		//	{
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(2, 6, frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(2, 5));
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(2, 7, "");
		//		// .vsPreview.TextMatrix(2, 5)
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(2, 8, "");
  //              // .vsPreview.TextMatrix(2, 5)
  //              //FC:FIANL:SBE - #3809 - copy cell info to next visible column. Row is merged in original, and on web we don't have the same result
  //              frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(2, 10, frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(2, 9));
		//		//frmUTCLStatus.InstancePtr.vsPreview.MergeRow(2, true);
  //              frmUTCLStatus.InstancePtr.vsPreview.MergeRow(2, true, 5, 8);
  //              frmUTCLStatus.InstancePtr.vsPreview.MergeRow(2, true, 10, 12);
  //          }
		//	// Calculate Totals
		//	// Set The Colors
		//	frmUTCLStatus.InstancePtr.vsPreview.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, frmUTCLStatus.InstancePtr.vsPreview.Rows - 1, frmUTCLStatus.InstancePtr.vsPreview.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
		//	// &H80000016    'grey
		//	// Total Gridlines
		//	frmUTCLStatus.InstancePtr.vsPreview.Select(frmUTCLStatus.InstancePtr.vsPreview.Rows - 1, frmUTCLStatus.InstancePtr.lngGRIDColPrincipal, frmUTCLStatus.InstancePtr.vsPreview.Rows - 1, frmUTCLStatus.InstancePtr.lngGRIDColTotal);
		//	frmUTCLStatus.InstancePtr.vsPreview.CellBorder(Color.Blue, 0, 1, 0, 0, 0, 0);
		//	// turn the status grid off and the preview grid on
		//	// If boolWater Then
		//	frmUTCLStatus.InstancePtr.WGRID.Visible = false;
		//	frmUTCLStatus.InstancePtr.lblWaterHeading.Visible = false;
		//	// Else
		//	frmUTCLStatus.InstancePtr.SGRID.Visible = false;
		//	frmUTCLStatus.InstancePtr.lblSewerHeading.Visible = false;
		//	// End If
		//	frmUTCLStatus.InstancePtr.vsPreview.Visible = true;
		//	frmUTCLStatus.InstancePtr.cmdPaymentPreview.Text = "Exit Preview";
		//	frmUTCLStatus.InstancePtr.vsPreview.Select(0, 0);
		//}

		//private static void AddPaymentToUTPreview_18(int indexPay, int rowPrev, bool boolWater)
		//{
		//	AddPaymentToUTPreview(ref indexPay, ref rowPrev, ref boolWater);
		//}

		//private static void AddPaymentToUTPreview(ref int indexPay, ref int rowPrev, ref bool boolWater)
		//{
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		int intCT;
		//		string strSwap = "";
		//		int rowNew = 0;
		//		double dblAmt = 0;
		//		string[] strSwapArr = new string[20 + 1];
		//		rowNew = frmUTCLStatus.InstancePtr.vsPreview.Rows - 1;
		//		// strSwap = vbTab & vbTab & .vsPayments.TextMatrix(indexPay, frmUTCLStatus.lngPayGridColBill) & vbTab & .vsPayments.TextMatrix(indexPay, frmUTCLStatus.lngPayGridColRef) & vbTab & .vsPayments.TextMatrix(indexPay, frmUTCLStatus.lngPayGridColService) & vbTab & .vsPayments.TextMatrix(indexPay, frmUTCLStatus.lngPayGridColCode) & vbTab & .vsPayments.TextMatrix(indexPay, frmUTCLStatus.lngPayGridColPrincipal) & vbTab & .vsPayments.TextMatrix(indexPay, frmUTCLStatus.lngPayGridColTax) & vbTab & .vsPayments.TextMatrix(indexPay, frmUTCLStatus.lngPayGridColInterest)
		//		// .vsPreview.AddItem strSwap, rowNew
		//		frmUTCLStatus.InstancePtr.vsPreview.AddItem("", rowNew);
		//		// .vsPreview.TextMatrix(rowNew, frmUTCLStatus.lngGRIDColBillNumber) = .vsPayments.TextMatrix(indexPay, frmUTCLStatus.lngPayGridColBill)
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmUTCLStatus.InstancePtr.lngGRIDColRef, frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColRef));
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmUTCLStatus.InstancePtr.lngGRIDColService, frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColService));
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmUTCLStatus.InstancePtr.lngGRIDColPaymentCode, frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColCode));
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmUTCLStatus.InstancePtr.lngGRIDColPrincipal, frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColPrincipal));
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmUTCLStatus.InstancePtr.lngGRIDColTax, frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColTax));
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmUTCLStatus.InstancePtr.lngGRIDColInterest, frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColInterest));
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmUTCLStatus.InstancePtr.lngGRIDColCosts, frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColCosts));
		//		// fill in the total col
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(frmUTCLStatus.InstancePtr.vsPreview.Rows - 2, frmUTCLStatus.InstancePtr.lngGRIDColTotal, FCConvert.ToString(FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColPrincipal)) + FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColTax)) + FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColInterest)) + FCConvert.ToDecimal(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(indexPay, frmUTCLStatus.InstancePtr.lngPayGridColCosts))));
		//		// fill in the sign of the payment
		//		// .vsPreview.TextMatrix(rowNew, frmUTCLStatus.) = "-"
		//		// calculate the bottom total line
		//		if (Conversion.Val(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmUTCLStatus.InstancePtr.lngGRIDColPrincipal)) != 0)
		//		{
		//			dblAmt = FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmUTCLStatus.InstancePtr.lngGRIDColPrincipal));
		//		}
		//		else
		//		{
		//			dblAmt = 0;
		//		}
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmUTCLStatus.InstancePtr.lngGRIDColPrincipal, FCConvert.ToString(dblAmt - FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmUTCLStatus.InstancePtr.lngGRIDColPrincipal))));
		//		if (Conversion.Val(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmUTCLStatus.InstancePtr.lngGRIDColTax)) != 0)
		//		{
		//			dblAmt = FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(1, frmUTCLStatus.InstancePtr.lngGRIDColTax));
		//		}
		//		else
		//		{
		//			dblAmt = 0;
		//		}
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmUTCLStatus.InstancePtr.lngGRIDColTax, FCConvert.ToString(dblAmt - FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmUTCLStatus.InstancePtr.lngGRIDColTax))));
		//		if (Conversion.Val(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmUTCLStatus.InstancePtr.lngGRIDColInterest)) != 0)
		//		{
		//			dblAmt = FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmUTCLStatus.InstancePtr.lngGRIDColInterest));
		//		}
		//		else
		//		{
		//			dblAmt = 0;
		//		}
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmUTCLStatus.InstancePtr.lngGRIDColInterest, FCConvert.ToString(dblAmt - FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmUTCLStatus.InstancePtr.lngGRIDColInterest))));
		//		if (Conversion.Val(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmUTCLStatus.InstancePtr.lngGRIDColCosts)) != 0)
		//		{
		//			dblAmt = FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmUTCLStatus.InstancePtr.lngGRIDColCosts));
		//		}
		//		else
		//		{
		//			dblAmt = 0;
		//		}
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmUTCLStatus.InstancePtr.lngGRIDColCosts, FCConvert.ToString(dblAmt - FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmUTCLStatus.InstancePtr.lngGRIDColCosts))));
		//		if (Conversion.Val(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmUTCLStatus.InstancePtr.lngGRIDColTotal)) != 0)
		//		{
		//			dblAmt = FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmUTCLStatus.InstancePtr.lngGRIDColTotal));
		//		}
		//		else
		//		{
		//			dblAmt = 0;
		//		}
		//		frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew + 1, frmUTCLStatus.InstancePtr.lngGRIDColTotal, FCConvert.ToString(dblAmt - FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(rowNew, frmUTCLStatus.InstancePtr.lngGRIDColTotal))));
		//		return;
		//	}
		//	catch (Exception ex)
		//	{
				
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Preview", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//}

		//private static void FormatUTPreviewGrid()
		//{
		//	int wid = 0;
		//	frmUTCLStatus.InstancePtr.vsPreview.Width = FCConvert.ToInt32(frmUTCLStatus.InstancePtr.Width * 0.95);
		//	// frmUTCLStatus.WGRID.Width
		//	wid = frmUTCLStatus.InstancePtr.vsPreview.WidthOriginal;
		//	frmUTCLStatus.InstancePtr.vsPreview.Left = FCConvert.ToInt32((frmUTCLStatus.InstancePtr.Width * 0.05) / 2);
		//	// frmUTCLStatus.WGRID.Left
		//	frmUTCLStatus.InstancePtr.vsPreview.Top = frmUTCLStatus.InstancePtr.WGRID.Top;
  //          frmUTCLStatus.InstancePtr.vsPreview.Height = frmUTCLStatus.InstancePtr.WGRID.Height;
  //          frmUTCLStatus.InstancePtr.vsPreview.MergeCells = FCGrid.MergeCellsSettings.flexMergeSpill;
		//	// this allows the name to spill into the next column
		//	frmUTCLStatus.InstancePtr.vsPreview.Cols = 20;
		//	// column headers
		//	frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmUTCLStatus.InstancePtr.lngGRIDCOLYear, "Year");
		//	frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber, "Bill");
		//	frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmUTCLStatus.InstancePtr.lngGRIDColDate, "Date");
		//	frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmUTCLStatus.InstancePtr.lngGRIDColRef, "Ref");
		//	frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmUTCLStatus.InstancePtr.lngGRIDColService, "P");
		//	frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmUTCLStatus.InstancePtr.lngGRIDColPaymentCode, "C");
		//	frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmUTCLStatus.InstancePtr.lngGRIDColPrincipal, "Principal");
		//	frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmUTCLStatus.InstancePtr.lngGRIDColPTC, "PTC");
		//	frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmUTCLStatus.InstancePtr.lngGRIDColTax, "Tax");
		//	frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmUTCLStatus.InstancePtr.lngGRIDColInterest, "Interest");
		//	frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmUTCLStatus.InstancePtr.lngGRIDColCosts, "Costs");
		//	frmUTCLStatus.InstancePtr.vsPreview.TextMatrix(0, frmUTCLStatus.InstancePtr.lngGRIDColTotal, "Total");
		//	frmUTCLStatus.InstancePtr.vsPreview.ColFormat(frmUTCLStatus.InstancePtr.lngGRIDColDate, "MM/dd/yy");
		//	frmUTCLStatus.InstancePtr.vsPreview.ColFormat(frmUTCLStatus.InstancePtr.lngGRIDColPrincipal, "#,##0.00");
		//	frmUTCLStatus.InstancePtr.vsPreview.ColFormat(frmUTCLStatus.InstancePtr.lngGRIDColPTC, "#,##0.00");
		//	frmUTCLStatus.InstancePtr.vsPreview.ColFormat(frmUTCLStatus.InstancePtr.lngGRIDColTax, "#,##0.00");
		//	frmUTCLStatus.InstancePtr.vsPreview.ColFormat(frmUTCLStatus.InstancePtr.lngGRIDColInterest, "#,##0.00");
		//	frmUTCLStatus.InstancePtr.vsPreview.ColFormat(frmUTCLStatus.InstancePtr.lngGRIDColCosts, "#,##0.00");
		//	frmUTCLStatus.InstancePtr.vsPreview.ColFormat(frmUTCLStatus.InstancePtr.lngGRIDColTotal, "#,##0.00");
		//	frmUTCLStatus.InstancePtr.vsPreview.ColAlignment(frmUTCLStatus.InstancePtr.lngGRIDCOLYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//	frmUTCLStatus.InstancePtr.vsPreview.ColAlignment(frmUTCLStatus.InstancePtr.lngGRIDColBillNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//	frmUTCLStatus.InstancePtr.vsPreview.ColAlignment(frmUTCLStatus.InstancePtr.lngGRIDColPending, FCGrid.AlignmentSettings.flexAlignLeftCenter);

  //          frmUTCLStatus.InstancePtr.vsPreview.ColAlignment(frmUTCLStatus.InstancePtr.lngGRIDColPrincipal, FCGrid.AlignmentSettings.flexAlignRightCenter);
  //          frmUTCLStatus.InstancePtr.vsPreview.ColAlignment(frmUTCLStatus.InstancePtr.lngGRIDColTax, FCGrid.AlignmentSettings.flexAlignRightCenter);
  //          frmUTCLStatus.InstancePtr.vsPreview.ColAlignment(frmUTCLStatus.InstancePtr.lngGRIDColInterest, FCGrid.AlignmentSettings.flexAlignRightCenter);
  //          frmUTCLStatus.InstancePtr.vsPreview.ColAlignment(frmUTCLStatus.InstancePtr.lngGRIDColCosts, FCGrid.AlignmentSettings.flexAlignRightCenter);
  //          frmUTCLStatus.InstancePtr.vsPreview.ColAlignment(frmUTCLStatus.InstancePtr.lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);

		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(0, 0);
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDCOLYear, 0);
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColBillNumber, FCConvert.ToInt32(wid * 0.075));
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColDate, FCConvert.ToInt32(wid * 0.12));
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColRef, FCConvert.ToInt32(wid * 0.098));
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColService, 0);
		//	// wid * 0.04
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColPaymentCode, FCConvert.ToInt32(wid * 0.04));
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColPrincipal, FCConvert.ToInt32(wid * 0.12));
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(8, 0);
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColPTC, 0);
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColTax, FCConvert.ToInt32(wid * 0.12));
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColInterest, FCConvert.ToInt32(wid * 0.12));
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColCosts, FCConvert.ToInt32(wid * 0.12));
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColTotal, FCConvert.ToInt32(wid * 0.13));
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColLineCode, 0);
		//	// Row code
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColPaymentKey, 0);
		//	// Payment ID
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColCHGINTNumber, 0);
		//	// CHGINT Number
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColPerDiem, 0);
		//	// Per Diem in Master Line
		//	frmUTCLStatus.InstancePtr.vsPreview.ColWidth(frmUTCLStatus.InstancePtr.lngGRIDColPending, FCConvert.ToInt32(wid * 0.025));
		//	// * Col
		//}
		//// vbPorter upgrade warning: ArrayIndex As short	OnWriteFCConvert.ToInt32(
		//public static bool CreateUTCHGINTPaymentRecord_2(short ArrayIndex, clsDRWrapper rsCreatePy, bool boolRev = false)
		//{
		//	return CreateUTCHGINTPaymentRecord(ref ArrayIndex, ref rsCreatePy, boolRev);
		//}

		//public static bool CreateUTCHGINTPaymentRecord_20(short ArrayIndex, clsDRWrapper rsCreatePy, bool boolRev = false)
		//{
		//	return CreateUTCHGINTPaymentRecord(ref ArrayIndex, ref rsCreatePy, boolRev);
		//}

		//public static bool CreateUTCHGINTPaymentRecord(ref short ArrayIndex, ref clsDRWrapper rsCreatePy, bool boolRev = false)
		//{
		//	bool CreateUTCHGINTPaymentRecord = false;
		//	// this adds a record to the database that offsets the charged interest line and
		//	// adjusts the PaidThroughDate
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	int lngR;
		//	// this will save one payment record to the database
		//	if (MAX_UTPAYMENTS > ArrayIndex)
		//	{
		//		// checks for out of bounds array call
		//		rsCreatePy.AddNew();
		//		rsCreatePy.Update();
		//		Statics.UTPaymentArray[ArrayIndex].ID = rsCreatePy.Get_Fields_Int32("ID");
		//		// when this is created, the ID has to be stored in the grid so that it does not create another CHGINT line for this one
		//		for (lngR = 1; lngR <= frmUTCLStatus.InstancePtr.vsPayments.Rows - 1; lngR++)
		//		{
		//			if (Math.Abs(FCConvert.ToInt16(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngR, frmUTCLStatus.InstancePtr.lngPayGridColArrayIndex))) == ArrayIndex)
		//			{
		//				frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngR, frmUTCLStatus.InstancePtr.lngPayGridColKeyNumber, FCConvert.ToString(Statics.UTPaymentArray[ArrayIndex].ID));
		//			}
		//		}
		//		// rsCreatePy.Fields("Account") = .Account        'MAL@20071026
		//		rsCreatePy.Set_Fields("AccountKey", Statics.UTPaymentArray[ArrayIndex].Account);
		//		rsCreatePy.Set_Fields("Year", Statics.UTPaymentArray[ArrayIndex].Year);
		//		rsCreatePy.Set_Fields("Reversal", Statics.UTPaymentArray[ArrayIndex].Reversal);
		//		//trout-1225
		//		rsCreatePy.Set_Fields("SetEIDate", Statics.UTPaymentArray[ArrayIndex].SetEIDate);
		//		rsCreatePy.Set_Fields("BillKey", Statics.UTPaymentArray[ArrayIndex].BillKey);
		//		rsCreatePy.Set_Fields("BillNumber", Statics.UTPaymentArray[ArrayIndex].BillNumber);
		//		// MAL@20071026
		//		if (!modExtraModules.IsThisCR())
		//		{
		//			// check for cash receipting
		//			rsCreatePy.Set_Fields("ReceiptNumber", 0);
		//			if (Statics.UTPaymentArray[ArrayIndex].BillCode != "L")
		//			{
		//				// regular billing
		//				rsTemp.OpenRecordset("SELECT * FROM (" + Statics.strUTCurrentAccountBills + ") AS qTmp WHERE Bill = " + FCConvert.ToString(Statics.UTPaymentArray[ArrayIndex].BillKey), modExtraModules.strUTDatabase);
		//				if (rsTemp.RecordCount() != 0)
		//				{
		//					rsTemp.Edit();
		//					// MAL@20080205: Adjust for utility payments
		//					// rsTemp.Fields("InterestPaid") = (rsTemp.Fields("InterestPaid") + .CurrentInterest)
		//					// rsTemp.Fields("DemandFeesPaid") = (rsTemp.Fields("DemandFeesPaid") + .LienCost)
		//					// rsTemp.Fields("PrincipalPaid") = (rsTemp.Fields("PrincipalPaid") + .Principal)
		//					rsTemp.Set_Fields(Statics.UTPaymentArray[ArrayIndex].Service + "IntPaid", (rsTemp.Get_Fields_Decimal(Statics.UTPaymentArray[ArrayIndex].Service + "IntPaid") + Statics.UTPaymentArray[ArrayIndex].CurrentInterest));
		//					rsTemp.Set_Fields(Statics.UTPaymentArray[ArrayIndex].Service + "CostPaid", (rsTemp.Get_Fields_Decimal(Statics.UTPaymentArray[ArrayIndex].Service + "CostPaid") + Statics.UTPaymentArray[ArrayIndex].LienCost));
		//					rsTemp.Set_Fields(Statics.UTPaymentArray[ArrayIndex].Service + "PrinPaid", (rsTemp.Get_Fields_Decimal(Statics.UTPaymentArray[ArrayIndex].Service + "PrinPaid") + Statics.UTPaymentArray[ArrayIndex].Principal));
		//					rsTemp.Update();
		//				}
		//				else
		//				{
		//					CreateUTCHGINTPaymentRecord = false;
		//					goto END_OF_PAYMENT;
		//				}
		//			}
		//			else
		//			{
		//				// lien
		//				rsTemp.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(Statics.UTPaymentArray[ArrayIndex].BillKey), modExtraModules.strUTDatabase);
		//				if (rsTemp.RecordCount() != 0)
		//				{
		//					rsTemp.Edit();
		//					rsTemp.Set_Fields("InterestPaid", (rsTemp.Get_Fields_Decimal("InterestPaid") + Statics.UTPaymentArray[ArrayIndex].CurrentInterest));
		//					rsTemp.Set_Fields("CostsPaid", (rsTemp.Get_Fields_Decimal("CostsPaid") + Statics.UTPaymentArray[ArrayIndex].LienCost));
		//					rsTemp.Set_Fields("PrincipalPaid", (rsTemp.Get_Fields_Decimal("PrincipalPaid") + Statics.UTPaymentArray[ArrayIndex].Principal));
		//					// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//					rsTemp.Set_Fields("PLIPaid", (rsTemp.Get_Fields_Decimal("PLIPaid") + Statics.UTPaymentArray[ArrayIndex].PreLienInterest));
		//					// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//					rsTemp.Set_Fields("TaxPaid", (rsTemp.Get_Fields_Decimal("TaxPaid") + Statics.UTPaymentArray[ArrayIndex].Tax));
		//					rsTemp.Update();
		//				}
		//				else
		//				{
		//					CreateUTCHGINTPaymentRecord = false;
		//					goto END_OF_PAYMENT;
		//				}
		//			}
		//		}
		//		else
		//		{
		//			// MAL@20071030: Add support for charge interest reversal
		//			if (boolRev)
		//			{
		//				rsCreatePy.Set_Fields("ReceiptNumber", 0);
		//			}
		//			else
		//			{
		//				rsCreatePy.Set_Fields("ReceiptNumber", -1);
		//			}
		//		}
		//		// this is to check for a reversal
		//		if (boolRev)
		//		{
		//			rsCreatePy.Set_Fields("CHGINTNumber", 0);
		//			// rsCreatePy.Fields("CHGINTDate") = dateUTOldDate((.Year \ 10) - utDEFAULTSUBTRACTIONVALUE)
		//		}
		//		else
		//		{
		//			rsCreatePy.Set_Fields("CHGINTNumber", Statics.UTPaymentArray[ArrayIndex].CHGINTNumber);
		//		}
		//		rsCreatePy.Set_Fields("CHGINTDate", Statics.UTPaymentArray[ArrayIndex].CHGINTDate);
		//		rsCreatePy.Set_Fields("ActualSystemDate", Statics.UTPaymentArray[ArrayIndex].ActualSystemDate);
		//		rsCreatePy.Set_Fields("EffectiveInterestDate", Statics.UTPaymentArray[ArrayIndex].EffectiveInterestDate);
		//		rsCreatePy.Set_Fields("RecordedTransactionDate", Statics.UTPaymentArray[ArrayIndex].RecordedTransactionDate);
		//		rsCreatePy.Set_Fields("Teller", Statics.UTPaymentArray[ArrayIndex].Teller);
		//		rsCreatePy.Set_Fields("Reference", Statics.UTPaymentArray[ArrayIndex].Reference);
		//		rsCreatePy.Set_Fields("Period", Statics.UTPaymentArray[ArrayIndex].Period);
		//		rsCreatePy.Set_Fields("Service", Statics.UTPaymentArray[ArrayIndex].Service);
		//		rsCreatePy.Set_Fields("Code", Statics.UTPaymentArray[ArrayIndex].Code);
		//		rsCreatePy.Set_Fields("Principal", Statics.UTPaymentArray[ArrayIndex].Principal);
		//		rsCreatePy.Set_Fields("PreLienInterest", Statics.UTPaymentArray[ArrayIndex].PreLienInterest);
		//		rsCreatePy.Set_Fields("CurrentInterest", Statics.UTPaymentArray[ArrayIndex].CurrentInterest);
		//		rsCreatePy.Set_Fields("LienCost", Statics.UTPaymentArray[ArrayIndex].LienCost);
		//		rsCreatePy.Set_Fields("TransNumber", Statics.UTPaymentArray[ArrayIndex].TransNumber);
		//		rsCreatePy.Set_Fields("PaidBy", Statics.UTPaymentArray[ArrayIndex].PaidBy);
		//		rsCreatePy.Set_Fields("Comments", Statics.UTPaymentArray[ArrayIndex].Comments);
		//		rsCreatePy.Set_Fields("CashDrawer", Statics.UTPaymentArray[ArrayIndex].CashDrawer);
		//		rsCreatePy.Set_Fields("GeneralLedger", Statics.UTPaymentArray[ArrayIndex].GeneralLedger);
		//		rsCreatePy.Set_Fields("BudgetaryAccountNumber", Statics.UTPaymentArray[ArrayIndex].BudgetaryAccountNumber);
		//		// rsCreatePy.Fields("BillCode") = CBool(.BillCode = "L")     'MAL@20071026
		//		rsCreatePy.Set_Fields("Lien", FCConvert.CBool(Statics.UTPaymentArray[ArrayIndex].BillCode == "L"));
		//		rsCreatePy.Update();
		//		Statics.UTPaymentArray[ArrayIndex].ID = rsCreatePy.Get_Fields_Int32("ID");
		//		CreateUTCHGINTPaymentRecord = true;
		//	}
		//	else
		//	{
		//		MessageBox.Show("Out of Bounds Array Index", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	if (!modExtraModules.IsThisCR())
		//	{
		//		// if this is not from CR then adust the Interest Applied Through Date
		//		if (rsCreatePy.FindFirstRecord("ID", Statics.UTPaymentArray[ArrayIndex].ID))
		//		{
		//			rsTemp.Edit();
		//			// adjust the paid through date...
		//			// MAL@20080205
		//			// rsTemp.Fields("InterestAppliedThroughDate") = FindLastUTPaidThroughDate(rsCreatePy.Fields("Year"), rsCreatePy.Fields("BillKey"), UTPaymentArray(ArrayIndex).CHGINTNumber * -1)
		//			if (Statics.UTPaymentArray[ArrayIndex].BillCode != "L")
		//			{
		//				// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
		//				rsTemp.Set_Fields(Statics.UTPaymentArray[ArrayIndex].Service + "IntPaidDate", FindLastUTPaidThroughDate_18(FCConvert.ToInt32(rsCreatePy.Get_Fields("Year")), FCConvert.ToInt32(rsCreatePy.Get_Fields_Int32("BillKey")), Statics.UTPaymentArray[ArrayIndex].CHGINTNumber * -1));
		//			}
		//			else
		//			{
		//				// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
		//				rsTemp.Set_Fields("IntPaidDate", FindLastUTPaidThroughDate_18(FCConvert.ToInt32(rsCreatePy.Get_Fields("Year")), FCConvert.ToInt32(rsCreatePy.Get_Fields_Int32("BillKey")), Statics.UTPaymentArray[ArrayIndex].CHGINTNumber * -1));
		//			}
		//			rsTemp.Update();
		//		}
		//		else//kk110322016 trout-1225  This is CR - make UT work more like CL
		//		{
		//			//find the next empty one
		//			//kk01242017 trout-1225   XXXXXXXXXXXXXXXX  TOOK THIS OUT, IT'S FROM CL.NOT SURE IF WE NEED TO ADD IT HERE XXXXXXXXXXX
		//			//For lngR = 0 To UBound(CHGINTYearDate) - 1
		//			//If Not CHGINTYearDate(lngR).Reversal Then Exit For
		//			//Next
		//			if (rsCreatePy.FindFirstRecord("ID", Statics.UTPaymentArray[ArrayIndex].ID))
		//			{
		//				//this will allow CR to know what bill to adjust the int date and what date to set it to
		//				//CHGINTYearDate(lngR).Reversal = True
		//				//CHGINTYearDate(lngR).InterestAppliedThroughDate = FindLastUTPaidThroughDate(rsCreatePy.Fields("BillNumber"), rsCreatePy.Fields("BillKey"), PaymentArray(ArrayIndex).CHGINTNumber * -1)
		//				// CHGINTYearDate(lngR).BillKey = PaymentArray(ArrayIndex).BillKey
		//			}
		//		}
		//	}
		//	rsTemp.Reset();
		//	return CreateUTCHGINTPaymentRecord;
		//	END_OF_PAYMENT:
		//	;
		//	MessageBox.Show("Account " + FCConvert.ToString(Statics.UTPaymentArray[ArrayIndex].Account) + " has had an error while processing.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	rsCreatePy.Delete();
		//	rsTemp.Reset();
		//	return CreateUTCHGINTPaymentRecord;
		//}

		//private static DateTime FindLastUTPaidThroughDate_18(int intYR, int lngBK, int lngCHGINTKey)
		//{
		//	return FindLastUTPaidThroughDate(ref intYR, ref lngBK, ref lngCHGINTKey);
		//}

		//private static DateTime FindLastUTPaidThroughDate(ref int intYR, ref int lngBK, ref int lngCHGINTKey)
		//{
		//	DateTime FindLastUTPaidThroughDate = System.DateTime.Now;
		//	// this will find the last paid through date on a CHGINT line and return it, else return the original bill date
		//	// and reset the CHGINTDate of the payment passed in so that future searches are not affected by it
		//	// intyear is the billing year
		//	// lngBK is the BillKey for this account
		//	// lngCHGINTKey is the ID of the CHGINT that is being reversed
		//	clsDRWrapper rsCHGINT = new clsDRWrapper();
		//	string strCHGINT;
		//	int lngCT;
		//	DateTime dateReverse = DateTime.FromOADate(0);
		//	// dateReverse is the date that is currently the Effective Date from the CHGINT payment that is being reversed
		//	// vbPorter upgrade warning: dateOBill As DateTime	OnWriteFCConvert.ToInt16(
		//	DateTime dateOBill;
		//	// vbPorter upgrade warning: dateCNVRSN As DateTime	OnWriteFCConvert.ToInt16(
		//	DateTime dateCNVRSN;
		//	// get the original bill date
		//	// If boolRE Then
		//	// strCHGINT = "SELECT * FROM Bill INNER JOIN RateRec ON Bill.RateKey = RateRec.RateKey WHERE Account = " & CurrentAccountRE & " And Year = " & intYr & " AND BillingType = 'RE'"
		//	// Else
		//	// strCHGINT = "SELECT * FROM Bill INNER JOIN RateRec ON Bill.RateKey = RateRec.RateKey WHERE Account = " & CurrentAccountPP & " And Year = " & intYr & " AND BillingType = 'RE'"
		//	// End If
		//	// MAL@20080205
		//	// If boolRE Then
		//	strCHGINT = "SELECT * FROM Bill INNER JOIN RateKeys ON Bill.BillingRateKey = RateKeys.ID WHERE Bill.ID = " + FCConvert.ToString(lngBK);
		//	// Else
		//	// strCHGINT = "SELECT * FROM Bill INNER JOIN RateKeys ON Bill.BillingRateKey = RateKeys.RateKey WHERE Account = " & CurrentAccountPP & " And BillNumber = " & intYR
		//	// End If
		//	rsCHGINT.OpenRecordset(strCHGINT, modExtraModules.strUTDatabase);
		//	if (rsCHGINT.EndOfFile() != true)
		//	{
		//		dateOBill = (DateTime)rsCHGINT.Get_Fields_DateTime("IntStart");
		//	}
		//	else
		//	{
		//		dateOBill = DateTime.FromOADate(0);
		//	}
		//	// get the conversion date
		//	// MAL@20080205
		//	// If boolRE Then
		//	// strCHGINT = "SELECT * FROM (" & strUTCurrentAccountPayments & ") WHERE Account = " & CurrentAccountRE & " And Year = " & intYR & " AND Reference = 'CNVRSN' AND BillCode <> 'P'"
		//	strCHGINT = "SELECT * FROM (" + Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE BillKey = " + FCConvert.ToString(lngBK) + " AND Reference = 'CNVRSN' AND Code <> 'P'";
		//	// Else
		//	// strCHGINT = "SELECT * FROM (" & strUTCurrentAccountPayments & ") WHERE Account = " & CurrentAccountPP & " And Year = " & intYR & " AND Reference = 'CNVRSN' AND BillCode = 'PP'"
		//	// End If
		//	rsCHGINT.OpenRecordset(strCHGINT, modExtraModules.strUTDatabase);
		//	if (rsCHGINT.RecordCount() != 0)
		//	{
		//		dateCNVRSN = (DateTime)rsCHGINT.Get_Fields_DateTime("CHGINTDate");
		//	}
		//	else
		//	{
		//		dateCNVRSN = DateTime.FromOADate(0);
		//	}
		//	strCHGINT = "SELECT * FROM (" + Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE ID = " + FCConvert.ToString(Math.Abs(lngCHGINTKey));
		//	rsCHGINT.OpenRecordset(strCHGINT, modExtraModules.strUTDatabase);
		//	if (rsCHGINT.RecordCount() != 0)
		//	{
		//		dateReverse = (DateTime)rsCHGINT.Get_Fields_DateTime("CHGINTDate");
		//	}
		//	// get the reverse date
		//	// first get the latest date and finds any earlier one
		//	// If lngCHGINTKey <> 0 Then        'if there is an CHGINT record before this on then
		//	// rsCHGINT.OpenRecordset "SELECT * FROM PaymentRec WHERE ID = " & Abs(lngCHGINTKey)   'get it
		//	// If rsCHGINT.RecordCount <> 0 Then
		//	// dateReverse = rsCHGINT.fields("EffectiveInterestDate")                         'and use the effective interest date
		//	// End If
		//	// Else
		//	// dateReverse = 0
		//	// End If
		//	// 
		//	// If dateReverse > 0 Then
		//	// strCHGINT = "SELECT * FROM PaymentRec "
		//	// strCHGINT = strCHGINT & "WHERE Account = " & CurrentAccountRE & " AND Year = " & intYr
		//	// strCHGINT = strCHGINT & " AND CHGINTDate < " & Format(dateReverse, "MM/dd/yyyy") & " AND ID <> " & lngCHGINTKey & " AND CHGINTDate <> 0 "
		//	// strCHGINT = strCHGINT & "ORDER BY CHGINTDate desc"
		//	// rsCHGINT.OpenRecordset strCHGINT
		//	// 
		//	// If rsCHGINT.RecordCount <> 0 Then
		//	// dateReverse = rsCHGINT.fields("EffectiveInterestDate")
		//	// Else
		//	// dateReverse = 0
		//	// End If
		//	// End If
		//	// compare the dates
		//	if (dateReverse.ToOADate() != 0)
		//	{
		//		// if there is another CHGINT payment other than the one that is being reversed
		//		if (dateCNVRSN.ToOADate() != 0)
		//		{
		//			// if there is a conversion done
		//			if (dateReverse.ToOADate() > dateReverse.ToOADate())
		//			{
		//				// if the CHGINT line is more recent then the conversion
		//				FindLastUTPaidThroughDate = dateReverse;
		//				// then use the CHGINT date
		//			}
		//			else
		//			{
		//				// else
		//				FindLastUTPaidThroughDate = dateCNVRSN;
		//				// then use the Conversion date
		//			}
		//		}
		//		else
		//		{
		//			// else
		//			FindLastUTPaidThroughDate = dateReverse;
		//			// use the CHGINT date
		//		}
		//	}
		//	else
		//	{
		//		// if there are no CHGINT other than the one that is being reversed
		//		if (dateCNVRSN.ToOADate() == 0)
		//		{
		//			// if there is no conversion date
		//			FindLastUTPaidThroughDate = dateOBill;
		//			// then use the original bill date
		//		}
		//		else
		//		{
		//			// if there is a conversion done
		//			if (dateCNVRSN.ToOADate() > dateOBill.ToOADate())
		//			{
		//				// if it is more current then the original bill (which it better be)
		//				FindLastUTPaidThroughDate = dateCNVRSN;
		//				// then use the conversion date
		//			}
		//			else
		//			{
		//				// else
		//				FindLastUTPaidThroughDate = dateOBill;
		//				// use the original bill date (this should not happen)
		//			}
		//		}
		//	}
		//	// reset the CHGINTDate of the payment passed in
		//	strCHGINT = "UPDATE (" + Statics.strUTCurrentAccountPayments + ") SET CHGINTDate = 0 WHERE ID = " + FCConvert.ToString(Math.Abs(lngCHGINTKey));
		//	rsCHGINT.Execute(strCHGINT, modExtraModules.strUTDatabase);
		//	rsCHGINT.Reset();
		//	return FindLastUTPaidThroughDate;
		//}

		public static bool UTYearCodeValidate(string billNumberText ,string codeText)
		{
			bool UTYearCodeValidate = false;
			// this function will return true if the Year and Code comboboxes are a valid pair
			UTYearCodeValidate = true;
			//FC:FINAL:DDU:#i1009 - fixed error when there is nothing in cmbBillNumber
			//if (frmUTCLStatus.InstancePtr.cmbBillNumber.Items.Count > 0)
			//{
				//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
				//if (Strings.Left(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[frmUTCLStatus.InstancePtr.cmbBillNumber.SelectedIndex].ToString(), 4) == "Auto")
				if (Strings.Left(billNumberText, 4) == "Auto")
				{
					//FC:FINAL:MSH - i.issue #1185: Selected index can be less than 0 and will throw an exception in this case. Using 'Text' will prevent this, because '.Text' equal to value from '.Items[SelectedIndex]'
					//if (Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Items[frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex].ToString(), 1) != "P" && Strings.Left(frmUTCLStatus.InstancePtr.cmbCode.Items[frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex].ToString(), 1) != "C")
					if (Strings.Left(codeText, 1) != "P" && Strings.Left(codeText, 1) != "C")
					{
						UTYearCodeValidate = false;
					}
				}
			//}
			return UTYearCodeValidate;
		}

		//public static string Return_UT_Payments()
		//{
		//	string Return_UT_Payments = "";
		//	// this function will return a comma delimited string that contains the ID of the payments
		//	// saved this past time in Collections so that CR can use the payments in its own
		//	int intCT;
		//	string strTemp;
		//	strTemp = "";
		//	for (intCT = 1; intCT <= MAX_UTPAYMENTS; intCT++)
		//	{
		//		strTemp += FCConvert.ToString(Statics.UTPaymentArray[intCT].ID) + ", ";
		//	}
		//	strTemp = Strings.Left(strTemp, strTemp.Length - 2);
		//	return Return_UT_Payments;
		//}

		//public static bool FillPendingUTTransactions()
		//{
		//	bool FillPendingUTTransactions = false;
		//	clsDRWrapper rsPend = new clsDRWrapper();
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	UTPayment payTemp = new UTPayment(0);
		//	int intCT;
		//	FillPendingUTTransactions = true;
		//	frmUTCLStatus.InstancePtr.vsPayments.Rows = 1;
		//	rsPend.OpenRecordset("SELECT * FROM (" + Statics.strUTCurrentAccountPayments + ") AS qTmp WHERE ReceiptNumber = -1 AND Code <> 'I' ORDER BY RecordedTransactionDate desc", modExtraModules.strUTDatabase);
		//	if (rsPend.EndOfFile() != true && rsPend.BeginningOfFile() != true)
		//	{
		//		if (MessageBox.Show("There are pending transactions on this account.  This could be caused by another user already in this account.  Would you like to continue?", "Pending Transactions", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
		//		{
		//			do
		//			{
		//				for (intCT = 1; intCT <= MAX_UTPAYMENTS; intCT++)
		//				{
		//					if (Statics.UTPaymentArray[intCT].Account == 0)
		//					{
		//						break;
		//					}
		//				}
		//				Statics.UTPaymentArray[intCT].ID = FCConvert.ToInt32(rsPend.Get_Fields_Int32("ID"));
		//				Statics.UTPaymentArray[intCT].BillNumber = FCConvert.ToInt32(rsPend.Get_Fields_Int32("BillNumber"));
		//				// MAL@20081222 ; Tracker Reference: 16111
		//				// TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
		//				Statics.UTPaymentArray[intCT].Year = FCConvert.ToInt32(rsPend.Get_Fields("Year"));
		//				Statics.UTPaymentArray[intCT].Reversal = FCConvert.ToBoolean(rsPend.Get_Fields_Boolean("Reversal"));
		//				//trout-1225
		//				Statics.UTPaymentArray[intCT].SetEIDate = (DateTime)rsPend.Get_Fields_DateTime("SetEIDate");
		//				Statics.UTPaymentArray[intCT].RecordedTransactionDate = (DateTime)rsPend.Get_Fields_DateTime("RecordedTransactionDate");
		//				Statics.UTPaymentArray[intCT].Reference = FCConvert.ToString(rsPend.Get_Fields_String("Reference"));
		//				// TODO: Check the table for the column [Period] and replace with corresponding Get_Field method
		//				Statics.UTPaymentArray[intCT].Period = FCConvert.ToString(rsPend.Get_Fields("Period"));
		//				Statics.UTPaymentArray[intCT].Code = rsPend.Get_Fields_String("Code");
		//				if (Strings.Trim(rsPend.Get_Fields_String("GeneralLedger") + " ") == "")
		//				{
		//					Statics.UTPaymentArray[intCT].CashDrawer = rsPend.Get_Fields_String("CashDrawer") + "   Y";
		//				}
		//				else
		//				{
		//					Statics.UTPaymentArray[intCT].CashDrawer = rsPend.Get_Fields_String("CashDrawer") + "   " + Strings.Trim(rsPend.Get_Fields_String("GeneralLedger") + " ");
		//				}
		//				Statics.UTPaymentArray[intCT].Service = FCConvert.ToString(rsPend.Get_Fields_String("Service"));
		//				// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//				Statics.UTPaymentArray[intCT].Principal = FCConvert.ToDecimal(rsPend.Get_Fields("Principal"));
		//				Statics.UTPaymentArray[intCT].CurrentInterest = FCConvert.ToDecimal(rsPend.Get_Fields_Decimal("CurrentInterest"));
		//				// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
		//				Statics.UTPaymentArray[intCT].Tax = FCConvert.ToDecimal(rsPend.Get_Fields("Tax"));
		//				Statics.UTPaymentArray[intCT].LienCost = FCConvert.ToDecimal(rsPend.Get_Fields_Decimal("LienCost"));
		//				Statics.UTPaymentArray[intCT].CHGINTNumber = FCConvert.ToInt32(rsPend.Get_Fields_Int32("CHGINTNumber"));
		//				Statics.UTPaymentArray[intCT].Account = FCConvert.ToInt32(rsPend.Get_Fields_Int32("AccountKey"));
		//				Statics.UTPaymentArray[intCT].EffectiveInterestDate = (DateTime)rsPend.Get_Fields_DateTime("EffectiveInterestDate");
		//				Statics.UTPaymentArray[intCT].ActualSystemDate = (DateTime)rsPend.Get_Fields_DateTime("ActualSystemDate");
		//				if (FCConvert.ToBoolean(rsPend.Get_Fields_Boolean("Lien")))
		//				{
		//					// if it is a lien payment, then check to see if a lien discharge notice is needed
		//					// check to see if this lien has been paid off yet
		//					// if so, give the user an option to print the Lien Discharge notice
		//					if (FCConvert.ToString(rsPend.Get_Fields_String("Reference")) != "CHGINT")
		//					{
		//						// this is a lien payment that is not CHGINT
		//						rsTemp.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsPend.Get_Fields_Int32("BillKey"), modExtraModules.strUTDatabase);
		//						if (rsTemp.RecordCount() != 0)
		//						{
		//							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//							// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//							if ((rsTemp.Get_Fields("Principal") - rsTemp.Get_Fields("PrinPaid")) + (rsTemp.Get_Fields("Tax") - rsTemp.Get_Fields("TaxPaid")) + (rsTemp.Get_Fields("Interest") + rsTemp.Get_Fields("IntAdded") - rsTemp.Get_Fields("IntPaid")) + (rsTemp.Get_Fields("Costs") - rsTemp.Get_Fields_Double("CostPaid")) - (rsPend.Get_Fields("Principal") + rsPend.Get_Fields_Decimal("PreLienInterest") + rsPend.Get_Fields_Decimal("CurrentInterest") + rsPend.Get_Fields_Decimal("LienCost")) <= 0)
		//							{
		//								// boolPaidLien = True
		//								// FIX THIS!!!
		//							}
		//						}
		//					}
		//				}
		//				if (intCT <= MAX_UTPAYMENTS)
		//				{
		//					frmUTCLStatus.InstancePtr.FillPaymentGrid(frmUTCLStatus.InstancePtr.vsPayments.Rows, intCT, Statics.UTPaymentArray[intCT].BillNumber, Statics.UTPaymentArray[intCT].ID);
		//					rsPend.MoveNext();
		//				}
		//				else
		//				{
		//					MessageBox.Show("Payment list is too full.  Too many Pending payments.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
		//				}
		//			}
		//			while (!rsPend.EndOfFile());
		//			modGlobalFunctions.AddCYAEntry_26("UT", "Entered Account with Pending Transactions", "Account : " + FCConvert.ToString(Statics.lngCurrentAccountUT));
		//		}
		//		else
		//		{
		//			FillPendingUTTransactions = false;
		//		}
		//	}
		//	rsPend.Reset();
		//	rsTemp.Reset();
		//	return FillPendingUTTransactions;
		//}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		//public static void ResetUTPayment(ref int intIndex, bool boolCHGINTErase = true, int lngID = 0)
		//{
		//	// this will delete if from the database if it is already stored and clear the payment array
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	string strSQL = "";
		//	if (intIndex < 0)
		//	{
		//		intIndex = Math.Abs(intIndex);
		//	}
		//	strSQL = "SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(Statics.UTPaymentArray[intIndex].ID);
		//	rsTemp.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
		//	if (rsTemp.EndOfFile() != true)
		//	{
		//		strSQL = "DELETE FROM PaymentRec WHERE ID = " + FCConvert.ToString(Statics.UTPaymentArray[intIndex].ID);
		//		if (FCConvert.ToInt32(rsTemp.Get_Fields_Int32("CHGINTNumber")) != 0 && boolCHGINTErase)
		//		{
		//			strSQL += " OR ID = " + rsTemp.Get_Fields_Int32("CHGINTNumber");
		//		}
		//		rsTemp.Execute(strSQL, modExtraModules.strUTDatabase);
		//	}
		//	if (lngID > 0)
		//	{
		//		strSQL = "SELECT * FROM PaymentRec WHERE ID = " + FCConvert.ToString(Statics.UTPaymentArray[intIndex].ID);
		//		rsTemp.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
		//		if (rsTemp.EndOfFile() != true)
		//		{
		//			strSQL = "DELETE FROM PaymentRec WHERE ID = " + FCConvert.ToString(Statics.UTPaymentArray[intIndex].ID);
		//			if (FCConvert.ToInt32(rsTemp.Get_Fields_Int32("CHGINTNumber")) != 0 && boolCHGINTErase)
		//			{
		//				strSQL += " OR ID = " + rsTemp.Get_Fields_Int32("CHGINTNumber");
		//			}
		//			rsTemp.Execute(strSQL, modExtraModules.strUTDatabase);
		//		}
		//	}
		//	Statics.UTPaymentArray[intIndex].ID = 0;
		//	Statics.UTPaymentArray[intIndex].Account = 0;
		//	Statics.UTPaymentArray[intIndex].Year = 0;
		//	Statics.UTPaymentArray[intIndex].BillNumber = 0;
		//	Statics.UTPaymentArray[intIndex].BillKey = 0;
		//	Statics.UTPaymentArray[intIndex].CHGINTNumber = 0;
		//	Statics.UTPaymentArray[intIndex].ActualSystemDate = DateTime.FromOADate(0);
		//	Statics.UTPaymentArray[intIndex].EffectiveInterestDate = DateTime.FromOADate(0);
		//	Statics.UTPaymentArray[intIndex].RecordedTransactionDate = DateTime.FromOADate(0);
		//	Statics.UTPaymentArray[intIndex].Teller = "";
		//	Statics.UTPaymentArray[intIndex].Reference = "";
		//	Statics.UTPaymentArray[intIndex].Period = "";
		//	Statics.UTPaymentArray[intIndex].Service = "";
		//	Statics.UTPaymentArray[intIndex].Code = "";
		//	Statics.UTPaymentArray[intIndex].ReceiptNumber = "";
		//	Statics.UTPaymentArray[intIndex].ReversalID = -1;
		//	Statics.UTPaymentArray[intIndex].Principal = 0;
		//	Statics.UTPaymentArray[intIndex].Tax = 0;
		//	Statics.UTPaymentArray[intIndex].PreLienInterest = 0;
		//	Statics.UTPaymentArray[intIndex].CurrentInterest = 0;
		//	Statics.UTPaymentArray[intIndex].LienCost = 0;
		//	Statics.UTPaymentArray[intIndex].TransNumber = FCConvert.ToString(0);
		//	Statics.UTPaymentArray[intIndex].PaidBy = "";
		//	Statics.UTPaymentArray[intIndex].Comments = "";
		//	Statics.UTPaymentArray[intIndex].CashDrawer = "";
		//	Statics.UTPaymentArray[intIndex].GeneralLedger = "";
		//	Statics.UTPaymentArray[intIndex].BudgetaryAccountNumber = "";
		//	Statics.UTPaymentArray[intIndex].BillCode = "";
		//	rsTemp.Reset();
		//}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		//private static short PrePayUTYear_2(string strYear)
		//{
		//	return PrePayUTYear(ref strYear);
		//}

		//private static short PrePayUTYear(ref string strYear)
		//{
		//	short PrePayUTYear = 0;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// this function will return the next billing year and create a bill record if needed
		//		clsDRWrapper rsRT = new clsDRWrapper();
		//		clsDRWrapper rsBR = new clsDRWrapper();
		//		clsDRWrapper rsUpdAdd = new clsDRWrapper();
		//		int intTempYear;
		//		// this will take the asterisk off the year
		//		intTempYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(strYear).Replace("*", ""))));
		//		// Val(Left(strYear, Len(strYear) - 1))   'kk10072015 trocr-446  There shouldn't be an asterisk
		//		rsBR.OpenRecordset("SELECT Bill FROM (" + Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(frmUTCLStatus.InstancePtr.CurrentAccountKey) + " AND BillStatus <> 'B'", modExtraModules.strUTDatabase);
		//		if (rsBR.BeginningOfFile() != true && rsBR.EndOfFile() != true)
		//		{
		//			// return the year only
		//			// rsBR.Edit    'SQLServer not like .Edit on Joined tables
		//			// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//			rsUpdAdd.Execute("UPDATE Bill SET WIntPaidDate = '" + DateTime.Today.ToShortDateString() + "', SIntPaidDate = '" + DateTime.Today.ToShortDateString() + "' WHERE ID = " + rsBR.Get_Fields("Bill"), modExtraModules.strUTDatabase);
		//		}
		//		else
		//		{
		//			// create a new billing record with this account number and year
		//			// rsBR.AddNew  'SQLServer not like .Edit on Joined tables
		//			rsUpdAdd.OpenRecordset("SELECT * FROM Bill WHERE ID = -1", "twut0000.vb1");
		//			rsUpdAdd.AddNew();
		//			rsBR.OpenRecordset("SELECT TOP 1 * FROM (" + Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(frmUTCLStatus.InstancePtr.CurrentAccountKey) + " ORDER BY BillNumber Desc", modExtraModules.strUTDatabase);
		//			// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//			rsRT.OpenRecordset("SELECT * FROM Bill WHERE ID = " + rsBR.Get_Fields("Bill"), modExtraModules.strUTDatabase);
		//			if (rsRT.RecordCount() != 0)
		//			{
		//				rsUpdAdd.Set_Fields("AccountKey", rsRT.Get_Fields_Int32("AccountKey"));
		//				rsUpdAdd.Set_Fields("ActualAccountNumber", GetAccountNumber_2(FCConvert.ToInt32(rsRT.Get_Fields_Int32("AccountKey"))));
		//				rsUpdAdd.Set_Fields("MeterKey", rsRT.Get_Fields_Int32("MeterKey"));
		//				rsUpdAdd.Set_Fields("BillNumber", 0);
		//				// kk 05312013
		//				rsUpdAdd.Set_Fields("BillingRateKey", 0);
		//				// "
		//				rsUpdAdd.Set_Fields("Service", Statics.TownService);
		//				// rsRT.Fields("Service")
		//				rsUpdAdd.Set_Fields("BillStatus", "C");
		//				rsUpdAdd.Set_Fields("CombinationCode", rsRT.Get_Fields_String("CombinationCode"));
		//				rsUpdAdd.Set_Fields("BName", rsRT.Get_Fields_String("BName"));
		//				rsUpdAdd.Set_Fields("BName2", rsRT.Get_Fields_String("BName2"));
		//				rsUpdAdd.Set_Fields("BAddress1", rsRT.Get_Fields_String("BAddress1"));
		//				rsUpdAdd.Set_Fields("BAddress2", rsRT.Get_Fields_String("BAddress2"));
		//				rsUpdAdd.Set_Fields("BAddress3", rsRT.Get_Fields_String("BAddress3"));
		//				rsUpdAdd.Set_Fields("BCity", rsRT.Get_Fields_String("BCity"));
		//				rsUpdAdd.Set_Fields("BState", rsRT.Get_Fields_String("BState"));
		//				rsUpdAdd.Set_Fields("BZip", rsRT.Get_Fields_String("BZip"));
		//				rsUpdAdd.Set_Fields("BZip4", rsRT.Get_Fields_String("BZip4"));
		//				rsUpdAdd.Set_Fields("OName", rsRT.Get_Fields_String("OName"));
		//				rsUpdAdd.Set_Fields("OName2", rsRT.Get_Fields_String("OName2"));
		//				rsUpdAdd.Set_Fields("OAddress1", rsRT.Get_Fields_String("OAddress1"));
		//				rsUpdAdd.Set_Fields("OAddress2", rsRT.Get_Fields_String("OAddress2"));
		//				rsUpdAdd.Set_Fields("OAddress3", rsRT.Get_Fields_String("OAddress3"));
		//				rsUpdAdd.Set_Fields("OCity", rsRT.Get_Fields_String("OCity"));
		//				rsUpdAdd.Set_Fields("OState", rsRT.Get_Fields_String("OState"));
		//				rsUpdAdd.Set_Fields("OZip", rsRT.Get_Fields_String("OZip"));
		//				rsUpdAdd.Set_Fields("OZip4", rsRT.Get_Fields_String("OZip4"));
		//				rsUpdAdd.Set_Fields("WCat", rsRT.Get_Fields_Int32("WCat"));
		//				rsUpdAdd.Set_Fields("SCat", rsRT.Get_Fields_Int32("SCat"));
		//				rsUpdAdd.Set_Fields("WIntPaidDate", DateTime.Today);
		//				rsUpdAdd.Set_Fields("SIntPaidDate", DateTime.Today);
		//				// kk 06242013 trouts-19  Bill Fields with Nulls messing up prepays
		//				rsUpdAdd.Set_Fields("BillingYear", 0);
		//				rsUpdAdd.Set_Fields("NoBill", 0);
		//				rsUpdAdd.Set_Fields("WLienStatusEligibility", 0);
		//				rsUpdAdd.Set_Fields("WLienProcessStatus", 0);
		//				rsUpdAdd.Set_Fields("WLienRecordNumber", 0);
		//				rsUpdAdd.Set_Fields("WCombinationLienKey", 0);
		//				rsUpdAdd.Set_Fields("SLienStatusEligibility", 0);
		//				rsUpdAdd.Set_Fields("SLienProcessStatus", 0);
		//				rsUpdAdd.Set_Fields("SLienRecordNumber", 0);
		//				rsUpdAdd.Set_Fields("SCombinationLienKey", 0);
		//				rsUpdAdd.Set_Fields("CurDate", DateAndTime.DateValue(FCConvert.ToString(0)));
		//				rsUpdAdd.Set_Fields("CurReading", 0);
		//				rsUpdAdd.Set_Fields("PrevDate", DateAndTime.DateValue(FCConvert.ToString(0)));
		//				rsUpdAdd.Set_Fields("PrevReading", 0);
		//				rsUpdAdd.Set_Fields("Consumption", 0);
		//				rsUpdAdd.Set_Fields("WaterOverrideCons", 0);
		//				rsUpdAdd.Set_Fields("WaterOverrideAmount", 0);
		//				rsUpdAdd.Set_Fields("SewerOverrideCons", 0);
		//				rsUpdAdd.Set_Fields("SewerOverrideAmount", 0);
		//				rsUpdAdd.Set_Fields("WMiscAmount", 0);
		//				rsUpdAdd.Set_Fields("WAdjustAmount", 0);
		//				rsUpdAdd.Set_Fields("WDEAdjustAmount", 0);
		//				rsUpdAdd.Set_Fields("WFlatAmount", 0);
		//				rsUpdAdd.Set_Fields("WUnitsAmount", 0);
		//				rsUpdAdd.Set_Fields("WConsumptionAmount", 0);
		//				rsUpdAdd.Set_Fields("SMiscAmount", 0);
		//				rsUpdAdd.Set_Fields("SAdjustAmount", 0);
		//				rsUpdAdd.Set_Fields("SDEAdjustAmount", 0);
		//				rsUpdAdd.Set_Fields("SFlatAmount", 0);
		//				rsUpdAdd.Set_Fields("SUnitsAmount", 0);
		//				rsUpdAdd.Set_Fields("SConsumptionAmount", 0);
		//				rsUpdAdd.Set_Fields("WTax", 0);
		//				rsUpdAdd.Set_Fields("STax", 0);
		//				rsUpdAdd.Set_Fields("TotalWBillAmount", 0);
		//				rsUpdAdd.Set_Fields("TotalSBillAmount", 0);
		//				rsUpdAdd.Set_Fields("WPrinOwed", 0);
		//				rsUpdAdd.Set_Fields("WTaxOwed", 0);
		//				rsUpdAdd.Set_Fields("WIntOwed", 0);
		//				rsUpdAdd.Set_Fields("WIntAdded", 0);
		//				rsUpdAdd.Set_Fields("WCostOwed", 0);
		//				rsUpdAdd.Set_Fields("WCostAdded", 0);
		//				rsUpdAdd.Set_Fields("WPrinPaid", 0);
		//				rsUpdAdd.Set_Fields("WTaxPaid", 0);
		//				rsUpdAdd.Set_Fields("WIntPaid", 0);
		//				rsUpdAdd.Set_Fields("WCostPaid", 0);
		//				rsUpdAdd.Set_Fields("SPrinOwed", 0);
		//				rsUpdAdd.Set_Fields("STaxOwed", 0);
		//				rsUpdAdd.Set_Fields("SIntOwed", 0);
		//				rsUpdAdd.Set_Fields("SIntAdded", 0);
		//				rsUpdAdd.Set_Fields("SCostOwed", 0);
		//				rsUpdAdd.Set_Fields("SCostAdded", 0);
		//				rsUpdAdd.Set_Fields("SPrinPaid", 0);
		//				rsUpdAdd.Set_Fields("STaxPaid", 0);
		//				rsUpdAdd.Set_Fields("SIntPaid", 0);
		//				rsUpdAdd.Set_Fields("SCostPaid", 0);
		//				rsUpdAdd.Set_Fields("WRT1", 0);
		//				rsUpdAdd.Set_Fields("WRT2", 0);
		//				rsUpdAdd.Set_Fields("WRT3", 0);
		//				rsUpdAdd.Set_Fields("WRT4", 0);
		//				rsUpdAdd.Set_Fields("WRT5", 0);
		//				rsUpdAdd.Set_Fields("SRT1", 0);
		//				rsUpdAdd.Set_Fields("SRT2", 0);
		//				rsUpdAdd.Set_Fields("SRT3", 0);
		//				rsUpdAdd.Set_Fields("SRT4", 0);
		//				rsUpdAdd.Set_Fields("SRT5", 0);
		//				rsUpdAdd.Set_Fields("Copies", 0);
		//				rsUpdAdd.Set_Fields("Loadback", 0);
		//				rsUpdAdd.Set_Fields("Final", 0);
		//				rsUpdAdd.Set_Fields("WBillOwner", rsRT.Get_Fields_Boolean("WBillOwner"));
		//				rsUpdAdd.Set_Fields("SBillOwner", rsRT.Get_Fields_Boolean("SBillOwner"));
		//				rsUpdAdd.Set_Fields("BillDate", DateAndTime.DateValue(FCConvert.ToString(0)));
		//				rsUpdAdd.Set_Fields("ReadingUnits", 0);
		//				rsUpdAdd.Set_Fields("WOrigBillAmount", 0);
		//				rsUpdAdd.Set_Fields("SOrigBillAmount", 0);
		//				rsUpdAdd.Set_Fields("SHasOverride", 0);
		//				rsUpdAdd.Set_Fields("WHasOverride", 0);
		//				rsUpdAdd.Set_Fields("SDemandGroupID", 0);
		//				rsUpdAdd.Set_Fields("WDemandGroupID", 0);
		//				rsUpdAdd.Set_Fields("SendEBill", 0);
		//				rsUpdAdd.Set_Fields("WinterBill", 0);
		//				rsUpdAdd.Set_Fields("MeterChangedOut", 0);
		//				rsUpdAdd.Set_Fields("SUploaded", 0);
		//				rsUpdAdd.Set_Fields("WUploaded", 0);
		//				rsUpdAdd.Update();
		//			}
		//			// rsBR.Fields("BillingYear") = Year(Date)
		//			// If TownService = "B" Then
		//			// rsBR.Fields("Service") = "B"
		//			// ElseIf TownService = "W" Then
		//			// rsBR.Fields("Service") = "W"
		//			// Else
		//			// rsBR.Fields("Service") = "S"
		//			// End If
		//			// xxx        rsBR.Fields("WIntPaidDate") = Date
		//			// xxx        rsBR.Fields("SIntPaidDate") = Date
		//			// xxx        rsBR.Update
		//		}
		//		frmUTCLStatus.InstancePtr.cmbCode.SelectedIndex = frmUTCLStatus.InstancePtr.cmbCode.Items.Count - 1;
		//		PrePayUTYear = FCConvert.ToInt16(intTempYear);
		//		// Return the Year
		//		rsBR.Reset();
		//		rsRT.Reset();
		//		return PrePayUTYear;
		//	}
		//	catch (Exception ex)
		//	{
				
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Blank Bill", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return PrePayUTYear;
		//}
		// vbPorter upgrade warning: dblTotal As double	OnReadFCConvert.ToDecimal(
		//private static short CreateExcessUTPaymentRecord_24(double dblTotal, bool boolWater, bool boolOtherType = false, bool boolForceYear = false)
		//{
		//	return CreateExcessUTPaymentRecord(ref dblTotal, ref boolWater, boolOtherType, boolForceYear);
		//}

		//private static short CreateExcessUTPaymentRecord_63(double dblTotal, bool boolWater, bool boolForceYear = false)
		//{
		//	return CreateExcessUTPaymentRecord(ref dblTotal, ref boolWater, false, boolForceYear);
		//}

		//private static short CreateExcessUTPaymentRecord_69(double dblTotal, bool boolWater, bool boolForceYear = false)
		//{
		//	return CreateExcessUTPaymentRecord(ref dblTotal, ref boolWater, false, boolForceYear);
		//}

		//private static short CreateExcessUTPaymentRecord_72(double dblTotal, bool boolWater, bool boolOtherType = false, bool boolForceYear = false)
		//{
		//	return CreateExcessUTPaymentRecord(ref dblTotal, ref boolWater, boolOtherType, boolForceYear);
		//}

		//private static short CreateExcessUTPaymentRecord_78(double dblTotal, bool boolWater, bool boolOtherType = false, bool boolForceYear = false)
		//{
		//	return CreateExcessUTPaymentRecord(ref dblTotal, ref boolWater, boolOtherType, boolForceYear);
		//}

		//private static short CreateExcessUTPaymentRecord(ref double dblTotal, ref bool boolWater, bool boolOtherType = false, bool boolForceYear = false)
		//{
		//	short CreateExcessUTPaymentRecord = 0;
		//	// this function will create a payment record for the extra amount of money and
		//	// return the year that the payment was put towards
		//	clsDRWrapper rsTemp = new clsDRWrapper();
		//	clsDRWrapper rsM = new clsDRWrapper();
		//	string strType = "";
		//	int lngBill = 0;
		//	int intCT;
		//	string PayCode = "";
		//	string BillCode = "";
		//	string strWS = "";
		//	int lngNewBill = 0;
		//	if (boolWater)
		//	{
		//		strWS = "W";
		//	}
		//	else
		//	{
		//		strWS = "S";
		//	}
		//	if (boolOtherType && Statics.TownService == "B")
		//	{
		//		// this type will call the addpaymenttolist function and get more payments added to the second grid
		//		intCT = AddUTPaymentToList_5343(frmUTCLStatus.InstancePtr.vsPayments.Rows, false, false, false, true, dblTotal, true);
		//	}
		//	else
		//	{
		//		if (boolForceYear)
		//		{
		//			// This will add a bill for the prepayment to be added to
		//			rsTemp.OpenRecordset("SELECT * FROM (" + Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(frmUTCLStatus.InstancePtr.CurrentAccountKey) + " AND BillNumber = 0", modExtraModules.strUTDatabase);
		//			if (rsTemp.EndOfFile())
		//			{
		//				rsM.OpenRecordset("SELECT ID,MeterNumber,AccountKey FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(frmUTCLStatus.InstancePtr.CurrentAccountKey) + " ORDER BY MeterNumber", modExtraModules.strUTDatabase);
		//				if (rsM.EndOfFile())
		//				{
		//					CreateExcessUTPaymentRecord = -1;
		//					MessageBox.Show("No Meter for this account.  Please add a meter to this account before adding a prepayment.", "No Meter", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		//					return CreateExcessUTPaymentRecord;
		//				}
		//				lngNewBill = CreateBlankUTBill_6(frmUTCLStatus.InstancePtr.CurrentAccountKey, FCConvert.ToInt32(rsM.Get_Fields_Int32("ID")), strWS);
		//				// kk04232014 trout-1081  Pass service to CreateBlankUTBill
		//				// Doevents
		//				rsTemp.OpenRecordset("SELECT ID AS Bill, * FROM Bill WHERE ID = " + FCConvert.ToString(lngNewBill), modExtraModules.strUTDatabase);
		//			}
		//			// Force the type to a prepayment
		//			strType = "Y";
		//		}
		//		else
		//		{
		//			rsTemp.OpenRecordset("SELECT * FROM (" + Statics.strUTCurrentAccountBills + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(frmUTCLStatus.InstancePtr.CurrentAccountKey) + " ORDER BY BillNumber Desc", modExtraModules.strUTDatabase);
		//			// this is the type of payment that is being processed
		//			strType = "P";
		//		}
		//		if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
		//		{
		//			lngBill = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("BillNumber"));
		//		}
		//		else
		//		{
		//			lngBill = FCConvert.ToInt32(Math.Round(Conversion.Val(frmUTCLStatus.InstancePtr.cmbBillNumber.Items[frmUTCLStatus.InstancePtr.cmbBillNumber.Items.Count - 2].ToString())));
		//			rsTemp.OpenRecordset("SELECT * FROM (" + Statics.strUTCurrentAccountBills + ") AS qTmp WHERE Accountkey = " + FCConvert.ToString(frmUTCLStatus.InstancePtr.CurrentAccountKey) + " AND BillNumber = " + FCConvert.ToString(lngBill), modExtraModules.strUTDatabase);
		//			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
		//			{
		//			}
		//			else
		//			{
		//				MessageBox.Show("Error opening database.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//				return CreateExcessUTPaymentRecord;
		//			}
		//		}
		//		for (intCT = 1; intCT <= MAX_UTPAYMENTS; intCT++)
		//		{
		//			// this finds a place in the array
		//			if (Statics.UTPaymentArray[intCT].Account == 0)
		//			{
		//				// to store the new payment record
		//				break;
		//			}
		//		}
		//		if (boolWater)
		//		{
		//			PayCode = "W";
		//		}
		//		else
		//		{
		//			PayCode = "S";
		//		}
		//		// kk 01042012 Duplicate work; setting BillNumber below     UTPaymentArray(intCT).BillNumber = lngBill
		//		// get the next year and its total due values
		//		if (FCConvert.ToInt32(rsTemp.Get_Fields(strWS + "LienRecordNumber")) == 0)
		//		{
		//			BillCode = "R";
		//		}
		//		else
		//		{
		//			BillCode = "L";
		//		}
		//		if (intCT < MAX_UTPAYMENTS)
		//		{
		//			// this adds it to the array of payments
		//			Statics.UTPaymentArray[intCT].BillNumber = lngBill;
		//			Statics.UTPaymentArray[intCT].ResCode = frmUTCLStatus.InstancePtr.lngUTResCode;
		//			// kk 01042012 trout-894  Set the res code
		//			Statics.UTPaymentArray[intCT].Account = frmUTCLStatus.InstancePtr.CurrentAccountKey;
		//			Statics.UTPaymentArray[intCT].ActualSystemDate = DateTime.Today;
		//			Statics.UTPaymentArray[intCT].BillCode = BillCode;
		//			Statics.UTPaymentArray[intCT].Service = PayCode;
		//			if (BillCode == "L")
		//			{
		//				rsTemp.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTemp.Get_Fields(strWS + "LienRecordNumber"), modExtraModules.strUTDatabase);
		//				if (rsTemp.EndOfFile() != true)
		//					Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields(strWS + "LienRecordNumber"));
		//			}
		//			else
		//			{
		//				// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//				Statics.UTPaymentArray[intCT].BillKey = FCConvert.ToInt32(rsTemp.Get_Fields("Bill"));
		//			}
		//			Statics.UTPaymentArray[intCT].CashDrawer = frmUTCLStatus.InstancePtr.txtCD.Text;
		//			if (Statics.UTPaymentArray[intCT].CashDrawer == "Y")
		//			{
		//				Statics.UTPaymentArray[intCT].BudgetaryAccountNumber = frmUTCLStatus.InstancePtr.txtAcctNumber.TextMatrix(0, 0);
		//			}
		//			else
		//			{
		//				Statics.UTPaymentArray[intCT].BudgetaryAccountNumber = "";
		//			}
		//			Statics.UTPaymentArray[intCT].Code = strType;
		//			Statics.UTPaymentArray[intCT].Comments = frmUTCLStatus.InstancePtr.txtComments.Text;
		//			Statics.UTPaymentArray[intCT].EffectiveInterestDate = Statics.UTEffectiveDate;
		//			// DJW@08202009 Changed GeneralLedger from being hard coded to Y to tkaing the value entered into the payment screen
		//			Statics.UTPaymentArray[intCT].GeneralLedger = frmUTCLStatus.InstancePtr.txtCash.Text;
		//			Statics.UTPaymentArray[intCT].Period = "A";
		//			Statics.UTPaymentArray[intCT].PreLienInterest = 0;
		//			Statics.UTPaymentArray[intCT].Principal = FCConvert.ToDecimal(dblTotal);
		//			Statics.UTPaymentArray[intCT].LienCost = 0;
		//			Statics.UTPaymentArray[intCT].CurrentInterest = 0;
		//			Statics.UTPaymentArray[intCT].PreLienInterest = 0;
		//			Statics.UTPaymentArray[intCT].ReceiptNumber = "P";
		//			if (frmUTCLStatus.InstancePtr.txtTransactionDate.Text == "")
		//			{
		//				Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateTime.Today;
		//			}
		//			else
		//			{
		//				Statics.UTPaymentArray[intCT].RecordedTransactionDate = DateAndTime.DateValue(Strings.Format(frmUTCLStatus.InstancePtr.txtTransactionDate.Text, "MM/dd/yyyy"));
		//			}
		//			Statics.UTPaymentArray[intCT].Reference = "PREPAY-A";
		//			Statics.UTPaymentArray[intCT].ReversalID = -1;
		//			frmUTCLStatus.InstancePtr.FillPaymentGrid(1, intCT, lngBill);
		//		}
		//	}
		//	rsTemp.Reset();
		//	return CreateExcessUTPaymentRecord;
		//}
		// vbPorter upgrade warning: Grid As object	OnWrite(VSFlex7LCtl.VSFlexGrid)
		// vbPorter upgrade warning: lngBill As int	OnWrite(int, double)
		//public static bool UTBillPending_72(FCGrid Grid, int lngBill, bool boolWater, short intPending = 0, bool boolValue = false)
		//{
		//	return UTBillPending(ref Grid, ref lngBill, ref boolWater, intPending, boolValue);
		//}

		//public static bool UTBillPending_240(FCGrid Grid, int lngBill, bool boolWater, short intPending = 0, bool boolValue = false)
		//{
		//	return UTBillPending(ref Grid, ref lngBill, ref boolWater, intPending, boolValue);
		//}

		//public static bool UTBillPending(ref FCGrid Grid, ref int lngBill, ref bool boolWater, short intPending = 0, bool boolValue = false)
		//{
		//	bool UTBillPending = false;
		//	// this function can find the year in question and return true if there are pending transactions
		//	// against it, set the astrisk or automatically set it
		//	int lngBillRow = 0;
		//	string strBill;
		//	int lngRW = 0;
		//	string strTest = "";
		//	int lngStartRow = 0;
		//	if (boolWater)
		//	{
		//		strTest = "W";
		//	}
		//	else
		//	{
		//		strTest = "S";
		//	}
		//	strBill = lngBill.ToString();
		//	lngStartRow = 1;
		//	lngBillRow = Grid.FindRow(strBill, 1, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber);
		//	if (lngBillRow >= 0)
		//	{
		//		switch (intPending)
		//		{
		//			case 0:
		//				{
		//					// this function just tells if there is an asterisk in the grid
		//					if (Grid.TextMatrix(lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColPending) == "*")
		//					{
		//						UTBillPending = true;
		//					}
		//					else
		//					{
		//						UTBillPending = false;
		//					}
		//					break;
		//				}
		//			case 1:
		//				{
		//					// this function will check the payment grid and set the asterisk accordingly (automatic setting)
		//					TRY_AGAIN:
		//					;
		//					lngRW = frmUTCLStatus.InstancePtr.vsPayments.FindRow(strBill, lngStartRow, frmUTCLStatus.InstancePtr.lngGRIDCOLTree);
		//					if (lngRW != -1)
		//					{
		//						if (frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(lngRW, 3) == strTest)
		//						{
		//							UTBillPending = true;
		//							Grid.TextMatrix(lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColPending, "*");
		//							Grid.TextMatrix(Grid.FindRow("=", lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColLineCode), frmUTCLStatus.InstancePtr.lngGRIDColPending, "*");
		//							Grid.TextMatrix(Grid.FindRow("=", lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColLineCode) + 1, frmUTCLStatus.InstancePtr.lngGRIDColPending, "*");
		//						}
		//						else
		//						{
		//							UTBillPending = false;
		//							Grid.TextMatrix(lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColPending, " ");
		//							Grid.TextMatrix(Grid.FindRow("=", lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColLineCode), frmUTCLStatus.InstancePtr.lngGRIDColPending, " ");
		//							Grid.TextMatrix(Grid.FindRow("=", lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColLineCode) + 1, frmUTCLStatus.InstancePtr.lngGRIDColPending, " ");
		//							// check for more
		//							lngStartRow = lngRW + 1;
		//							goto TRY_AGAIN;
		//						}
		//					}
		//					else
		//					{
		//						UTBillPending = false;
		//						Grid.TextMatrix(lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColPending, " ");
		//						Grid.TextMatrix(Grid.FindRow("=", lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColLineCode), frmUTCLStatus.InstancePtr.lngGRIDColPending, " ");
		//						Grid.TextMatrix(Grid.FindRow("=", lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColLineCode) + 1, frmUTCLStatus.InstancePtr.lngGRIDColPending, " ");
		//					}
		//					break;
		//				}
		//			case 2:
		//				{
		//					// this will set the asterisk in the grid depending on the value passed in
		//					if (boolValue)
		//					{
		//						Grid.TextMatrix(lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColPending, "*");
		//						Grid.TextMatrix(Grid.FindRow("=", lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColLineCode), frmUTCLStatus.InstancePtr.lngGRIDColPending, "*");
		//						Grid.TextMatrix(Grid.FindRow("=", lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColLineCode) + 1, frmUTCLStatus.InstancePtr.lngGRIDColPending, "*");
		//					}
		//					else
		//					{
		//						Grid.TextMatrix(lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColPending, " ");
		//						Grid.TextMatrix(Grid.FindRow("=", lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColLineCode), frmUTCLStatus.InstancePtr.lngGRIDColPending, " ");
		//						Grid.TextMatrix(Grid.FindRow("=", lngBillRow, frmUTCLStatus.InstancePtr.lngGRIDColLineCode) + 1, frmUTCLStatus.InstancePtr.lngGRIDColPending, " ");
		//					}
		//					break;
		//				}
		//			case 3:
		//				{
		//					// this function will check to see if there is a pending transaction in the payment grid for the year that was passed in
		//					if (frmUTCLStatus.InstancePtr.vsPayments.FindRow(strBill, 1, frmUTCLStatus.InstancePtr.lngGRIDCOLTree) != -1)
		//					{
		//						UTBillPending = true;
		//					}
		//					else
		//					{
		//						UTBillPending = false;
		//					}
		//					break;
		//				}
		//		}
		//		//end switch
		//	}
		//	Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, frmUTCLStatus.InstancePtr.lngGRIDColPending, Grid.Rows - 1, frmUTCLStatus.InstancePtr.lngGRIDColPending, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		//	frmUTCLStatus.InstancePtr.txtTotalPendingDue.Text = Strings.Format(TotalUTPendingPaymentsDue(), "#,##0.00");
		//	return UTBillPending;
		//}

		//private static double TotalUTPendingPaymentsDue()
		//{
		//	double TotalUTPendingPaymentsDue = 0;
		//	// this function will total the payments that are pending and return the value
		//	double dblTotal;
		//	int i;
		//	dblTotal = 0;
		//	for (i = 0; i <= frmUTCLStatus.InstancePtr.vsPayments.Rows - 1; i++)
		//	{
		//		if (Conversion.Val(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(i, frmUTCLStatus.InstancePtr.lngPayGridColTotal)) != 0)
		//			dblTotal += FCConvert.ToDouble(frmUTCLStatus.InstancePtr.vsPayments.TextMatrix(i, frmUTCLStatus.InstancePtr.lngPayGridColTotal));
		//	}
		//	TotalUTPendingPaymentsDue = modGlobal.Round(dblTotal, 2);
		//	return TotalUTPendingPaymentsDue;
		//}

		//public static void CheckAllUTPending()
		//{
		//	// this will check all years and update the "Pending" marker
		//	int lngR;
		//	for (lngR = 1; lngR <= frmUTCLStatus.InstancePtr.WGRID.Rows - 1; lngR++)
		//	{
		//		if (frmUTCLStatus.InstancePtr.WGRID.RowOutlineLevel(lngR) == 1)
		//		{
		//			if (Conversion.Val(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(lngR, 2)) != 0)
		//			{
		//				UTBillPending_240(frmUTCLStatus.InstancePtr.WGRID, FCConvert.ToInt32(Conversion.Val(frmUTCLStatus.InstancePtr.WGRID.TextMatrix(lngR, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber))), true, 1, true);
		//			}
		//		}
		//	}
		//	for (lngR = 1; lngR <= frmUTCLStatus.InstancePtr.SGRID.Rows - 1; lngR++)
		//	{
		//		if (frmUTCLStatus.InstancePtr.SGRID.RowOutlineLevel(lngR) == 1)
		//		{
		//			if (Conversion.Val(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(lngR, 2)) != 0)
		//			{
		//				UTBillPending_240(frmUTCLStatus.InstancePtr.SGRID, FCConvert.ToInt32(Conversion.Val(frmUTCLStatus.InstancePtr.SGRID.TextMatrix(lngR, frmUTCLStatus.InstancePtr.lngGRIDColBillNumber))), false, 1, true);
		//			}
		//		}
		//	}
		//}

		//public static void ShowUTInterestPaidThroughDate(ref string strTag)
		//{
		//	// this will show a messagebox with the effective interest date and the year
		//	clsDRWrapper rsDate = new clsDRWrapper();
		//	string strMessage = "";
		//	rsDate.OpenRecordset("SELECT * FROM (" + Statics.strUTCurrentAccountBills + ") AS qTMP WHERE AccountKey = " + FCConvert.ToString(frmUTCLStatus.InstancePtr.CurrentAccountKey) + " ORDER BY AccountKey", modExtraModules.strUTDatabase);
		//	if (rsDate.EndOfFile() != true)
		//	{
		//		strMessage = "Bill - Interest Paid Through Date" + "\r\n";
		//		do
		//		{
		//			strMessage += modGlobal.PadToString_8(rsDate.Get_Fields_Int32("BillNumber"), 4) + " - " + Strings.Format(rsDate.Get_Fields_DateTime("InterestAppliedThroughDate"), "MM/dd/yyyy") + "\r\n";
		//			rsDate.MoveNext();
		//		}
		//		while (!rsDate.EndOfFile());
		//	}
		//	MessageBox.Show(strMessage, strTag, MessageBoxButtons.OK, MessageBoxIcon.Information);
		//	rsDate.Reset();
		//}

		//public static void AdjustUTPaymentAmounts(ref double dblP, ref double dblT, ref double dblI, ref double dblC, ref double dblPNeed, ref double dblTNeed, ref double dblINeed, ref double dblCNeed, ref double dblPPay, ref double dblTPay, ref double dblIPay, ref double dblCPay, double dblPLI = 0, double dblPLINeed = 0, double dblPIPay = 0)
		//{
		//	double dblTot;
		//	dblTot = dblP + dblT + dblI + dblC;
		//	// MAL@20071012: Add PLI Check
		//	// PLI First
		//	if (dblPLINeed >= dblTot && dblPLINeed != 0)
		//	{
		//		dblPIPay = dblTot;
		//		dblTot = 0;
		//	}
		//	else
		//	{
		//		dblPIPay = dblPLINeed;
		//		dblTot -= dblPLINeed;
		//	}
		//	// interest first
		//	if (dblINeed >= dblTot && dblINeed != 0)
		//	{
		//		// if the need is greater than what is left
		//		dblIPay = dblTot;
		//		// then take all of it and set the total to 0
		//		dblTot = 0;
		//	}
		//	else
		//	{
		//		dblIPay = dblINeed;
		//		// else take what you need and leave the difference
		//		dblTot -= dblINeed;
		//		// is dblTot
		//	}
		//	// costs second
		//	if (dblCNeed >= dblTot && dblCNeed != 0)
		//	{
		//		dblCPay = dblTot;
		//		dblTot = 0;
		//	}
		//	else
		//	{
		//		dblCPay = dblCNeed;
		//		dblTot -= dblCNeed;
		//	}
		//	// tax third
		//	if (dblTNeed >= dblTot && dblTNeed != 0)
		//	{
		//		dblTPay = dblTot;
		//		dblTot = 0;
		//	}
		//	else
		//	{
		//		dblTPay = dblTNeed;
		//		dblTot -= dblTNeed;
		//	}
		//	// principal last
		//	if (dblPNeed >= dblTot)
		//	{
		//		dblPPay = dblTot;
		//		dblTot = 0;
		//	}
		//	else if (dblPNeed < 0)
		//	{
		//		dblPPay = dblTot;
		//		dblTot = 0;
		//	}
		//	else
		//	{
		//		dblPPay = dblPNeed;
		//		dblTot -= dblPNeed;
		//	}
		//}

		public static int GetUTAccountNumber_2(int lngKey)
		{
			return GetUTAccountNumber(lngKey);
		}

		public static int GetUTAccountNumber(int lngKey)
		{
			int GetUTAccountNumber = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsMaster = new clsDRWrapper();
				rsMaster.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
				if (!rsMaster.EndOfFile())
				{
					// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					GetUTAccountNumber = FCConvert.ToInt32(rsMaster.Get_Fields("AccountNumber"));
				}
				else
				{
					GetUTAccountNumber = 0;
				}
				return GetUTAccountNumber;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Returning Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetUTAccountNumber;
		}

		//public static int ReturnLienRateKey_2(int lngLienKey)
		//{
		//	return ReturnLienRateKey(ref lngLienKey);
		//}

		//public static int ReturnLienRateKey(ref int lngLienKey)
		//{
		//	int ReturnLienRateKey = 0;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		clsDRWrapper rsLien = new clsDRWrapper();
		//		rsLien.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(lngLienKey), modExtraModules.strUTDatabase);
		//		if (!rsLien.EndOfFile())
		//		{
		//			ReturnLienRateKey = FCConvert.ToInt32(rsLien.Get_Fields_Int32("RateKey"));
		//		}
		//		else
		//		{
		//			ReturnLienRateKey = 0;
		//		}
		//		return ReturnLienRateKey;
		//	}
		//	catch (Exception ex)
		//	{
				
		//		frmWait.InstancePtr.Unload();
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Lien Rate ID", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return ReturnLienRateKey;
		//}

		public static int GetAccountNumber_2(int lngAccountKey)
		{
			return GetAccountNumber(ref lngAccountKey);
		}

		public static int GetAccountNumber(ref int lngAccountKey)
		{
			int GetAccountNumber = 0;
			// this function will accept the account ID and return the account number
			// if there is no matching account then it will return 0
			// and if there is an error then it will return a -1
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rs = new clsDRWrapper();
				rs.OpenRecordset("SELECT AccountNumber FROM Master WHERE ID = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
				if (!rs.EndOfFile())
				{
					// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					GetAccountNumber = FCConvert.ToInt32(rs.Get_Fields("AccountNumber"));
				}
				else
				{
					GetAccountNumber = 0;
				}
				return GetAccountNumber;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				GetAccountNumber = -1;
				MessageBox.Show("Error # " + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Retrieving Account Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetAccountNumber;
		}

		public static string GetAccountEmail(ref int lngAccountKey)
		{
			string GetAccountEmail = "";
			// this function will accept the account ID and return the account number
			// if there is no matching account then it will return 0
			// and if there is an error then it will return a -1
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rs = new clsDRWrapper();
				rs.OpenRecordset("SELECT Email FROM Master WHERE ID = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
				if (!rs.EndOfFile())
				{
					GetAccountEmail = FCConvert.ToString(rs.Get_Fields_String("Email"));
				}
				else
				{
					GetAccountEmail = "";
				}
				return GetAccountEmail;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				GetAccountEmail = "";
				MessageBox.Show("Error # " + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Retrieving Account Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetAccountEmail;
		}
		// vbPorter upgrade warning: lngAccountNumber As int	OnWrite(int, double, string)
		public static int GetAccountKeyUT_2(int lngAccountNumber)
		{
			return GetAccountKeyUT(ref lngAccountNumber);
		}

		public static int GetAccountKeyUT(ref int lngAccountNumber)
		{
			int GetAccountKeyUT = 0;
			// this function will accept the account number and return the account ID
			// if there is no matching account then it will return 0
			// and if there is an error then it will return a -1
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rs = new clsDRWrapper();
				rs.OpenRecordset("SELECT AccountNumber, ID FROM Master WHERE AccountNumber = " + FCConvert.ToString(lngAccountNumber), modExtraModules.strUTDatabase);
				if (!rs.EndOfFile())
				{
					GetAccountKeyUT = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
				}
				else
				{
					GetAccountKeyUT = 0;
				}
				return GetAccountKeyUT;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				GetAccountKeyUT = -1;
				MessageBox.Show("Error # " + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Retrieving Account Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetAccountKeyUT;
		}

		public static void CreateCHGINTForBill_20(bool boolWater, int lngBill, int lngAcct, bool boolLien, double dblAmt, DateTime? dtRTD = null)
		{
			CreateCHGINTForBill(ref boolWater, ref lngBill, ref lngAcct, ref boolLien, ref dblAmt, dtRTD);
		}

		public static void CreateCHGINTForBill(ref bool boolWater, ref int lngBill, ref int lngAcct, ref bool boolLien, ref double dblAmt, DateTime? dtRTDTemp = null)
		{
			try
			{
				DateTime dtRTD = DateTime.FromOADate(0);
				if (dtRTDTemp == null)
				{
					dtRTD = DateTime.Now;
				}
				// On Error GoTo ERROR_HANDLER
				// this routine will create a charged interest line for the bill passed in
				// and will not update the lien or bill record
				clsDRWrapper rsP = new clsDRWrapper();
				rsP.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0", modExtraModules.strUTDatabase);
				rsP.AddNew();
				rsP.Set_Fields("AccountKey", lngAcct);
				rsP.Set_Fields("BillKey", lngBill);
				rsP.Set_Fields("ActualSystemDate", DateTime.Today);
				rsP.Set_Fields("EffectiveInterestDate", DateTime.Today);
				if (dtRTD.ToOADate() == 0)
				{
					// if nothing gets passed in then use the current date
					rsP.Set_Fields("RecordedTransactionDate", DateTime.Today);
				}
				else
				{
					rsP.Set_Fields("RecordedTransactionDate", dtRTD);
				}
				rsP.Set_Fields("Reference", "CHGINT");
				rsP.Set_Fields("Period", "A");
				rsP.Set_Fields("Code", "I");
				rsP.Set_Fields("CurrentInterest", dblAmt * -1);
				rsP.Set_Fields("CashDrawer", "Y");
				rsP.Set_Fields("GeneralLedger", "Y");
				if (boolWater)
				{
					rsP.Set_Fields("Service", "W");
				}
				else
				{
					rsP.Set_Fields("Service", "S");
				}
				rsP.Set_Fields("Lien", boolLien);
				rsP.Update();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + FCConvert.ToString(Information.Err(ex).Number) + ".", "Error Creating Charged Interest", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		//public static bool CheckForFlatInterest_2(int lngAccountKey)
		//{
		//	return CheckForFlatInterest(ref lngAccountKey);
		//}

		//public static bool CheckForFlatInterest(ref int lngAccountKey)
		//{
		//	bool CheckForFlatInterest = false;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// This will check the whole account for the need of the flat rate to be added
		//		clsDRWrapper rsFlat = new clsDRWrapper();
		//		clsDRWrapper rsLien = new clsDRWrapper();
		//		clsDRWrapper rsRate = new clsDRWrapper();
		//		double dblTotal = 0;
		//		double dblTempInt = 0;
		//		double dblCurrentInterest = 0;
		//		double dblCHGInt = 0;
		//		bool boolLien = false;
		//		int lngKey = 0;
		//		CheckForFlatInterest = true;
		//		rsFlat.OpenRecordset("SELECT FlatInterestAmount FROM UtilityBilling", modExtraModules.strUTDatabase);
		//		if (!rsFlat.EndOfFile())
		//		{
		//			if (rsFlat.Get_Fields_Decimal("FlatInterestAmount") > 0)
		//			{
		//				dblCHGInt = FCConvert.ToDouble(rsFlat.Get_Fields_Decimal("FlatInterestAmount"));
		//			}
		//			else
		//			{
		//				dblCHGInt = 0;
		//				return CheckForFlatInterest;
		//			}
		//		}
		//		else
		//		{
		//			dblCHGInt = 0;
		//			return CheckForFlatInterest;
		//		}
		//		// Water first
		//		if (Statics.TownService != "S")
		//		{
		//			rsFlat.OpenRecordset("SELECT * FROM Bill WHERE BillStatus = 'B' AND AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND WIntAdded = 0", modExtraModules.strUTDatabase);
		//			while (!rsFlat.EndOfFile())
		//			{
		//				dblCurrentInterest = 0;
		//				boolLien = false;
		//				dblTempInt = 0;
		//				if (FCConvert.ToInt32(rsFlat.Get_Fields_Int32("WLienRecordNumber")) != 0)
		//				{
		//					// no flat interest on liens
		//					// rsLien.OpenRecordset "SELECT * FROM Lien WHERE LienKey = " & rsFlat.Fields("WLienRecordNumber"), strUTDatabase
		//					// If Not rsLien.EndOfFile Then
		//					// rsRate.OpenRecordset "SELECT * FROM RateKeys WHERE RateKey = " & rsLien.Fields("RateKey"), strUTDatabase
		//					// If Not rsRate.EndOfFile Then
		//					// If DateDiff("D", rsRate.Fields("IntStart"), Date) >= 0 Then
		//					// dblTempInt = 0
		//					// dblTotal = CalculateAccountUTLien(rsLien, Date, dblTempInt, True)
		//					// If dblTotal > 0 Then
		//					// boolLien = True
		//					// lngKey = rsLien.Fields("LienKey")
		//					// dblCurrentInterest = dblCHGINT
		//					// End If
		//					// End If
		//					// End If
		//					// End If
		//				}
		//				else
		//				{
		//					rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsFlat.Get_Fields_Int32("BillingRateKey"), modExtraModules.strUTDatabase);
		//					if (!rsRate.EndOfFile())
		//					{
		//						if (DateAndTime.DateDiff("D", (DateTime)rsRate.Get_Fields_DateTime("IntStart"), DateTime.Today) >= 0)
		//						{
		//							dblTempInt = 0;
		//							dblTotal = modUTCalculations.CalculateAccountUT(rsFlat, DateTime.Today, ref dblTempInt, true);
		//							if (dblTotal > 0)
		//							{
		//								boolLien = true;
		//								lngKey = FCConvert.ToInt32(rsFlat.Get_Fields_Int32("ID"));
		//								dblCurrentInterest = dblCHGInt;
		//							}
		//						}
		//					}
		//				}
		//				if (dblCurrentInterest != 0)
		//				{
		//					if (boolLien)
		//					{
		//						// this will edit the lien records charged interest
		//						// rsLien.Edit
		//						// rsLien.Fields("IntAdded") = rsLien.Fields("IntAdded") - dblCurrentInterest
		//						// rsLien.Update
		//					}
		//					else
		//					{
		//						// this will edit the bill records charged interest
		//						rsFlat.Edit();
		//						rsFlat.Set_Fields("WIntAdded", rsFlat.Get_Fields_Double("WIntAdded") - dblCurrentInterest);
		//						rsFlat.Update();
		//						// moved here so non lien accounts are affected
		//						CreateCHGINTForBill_20(true, lngKey, FCConvert.ToInt32(rsFlat.Get_Fields_Int32("AccountKey")), boolLien, dblCurrentInterest);
		//					}
		//				}
		//				rsFlat.MoveNext();
		//			}
		//		}
		//		// Sewer Bills
		//		if (Statics.TownService != "W")
		//		{
		//			rsFlat.OpenRecordset("SELECT * FROM Bill WHERE BillStatus = 'B' AND AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND SIntAdded = 0", modExtraModules.strUTDatabase);
		//			while (!rsFlat.EndOfFile())
		//			{
		//				dblCurrentInterest = 0;
		//				boolLien = false;
		//				dblTempInt = 0;
		//				if (FCConvert.ToInt32(rsFlat.Get_Fields_Int32("SLienRecordNumber")) != 0)
		//				{
		//					// no flat interest on liens
		//					// regular per diem interest calculation
		//					rsLien.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsFlat.Get_Fields_Int32("SLienRecordNumber"), modExtraModules.strUTDatabase);
		//					if (!rsLien.EndOfFile())
		//					{
		//						rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsLien.Get_Fields_Int32("RateKey"), modExtraModules.strUTDatabase);
		//						if (!rsRate.EndOfFile())
		//						{
		//							if (DateAndTime.DateDiff("D", (DateTime)rsRate.Get_Fields_DateTime("IntStart"), DateTime.Today) >= 0)
		//							{
		//								dblTempInt = 0;
		//								dblTotal = modUTCalculations.CalculateAccountUTLien(rsLien, DateTime.Today, ref dblTempInt, false);
		//								if (dblTotal > 0)
		//								{
		//									boolLien = true;
		//									lngKey = FCConvert.ToInt32(rsLien.Get_Fields_Int32("LienKey"));
		//									dblCurrentInterest = dblCHGInt;
		//								}
		//							}
		//						}
		//					}
		//				}
		//				else
		//				{
		//					rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsFlat.Get_Fields_Int32("BillingRateKey"), modExtraModules.strUTDatabase);
		//					if (!rsRate.EndOfFile())
		//					{
		//						if (DateAndTime.DateDiff("D", (DateTime)rsRate.Get_Fields_DateTime("IntStart"), DateTime.Today) >= 0)
		//						{
		//							dblTempInt = 0;
		//							dblTotal = modUTCalculations.CalculateAccountUT(rsFlat, DateTime.Today, ref dblTempInt, false);
		//							if (dblTotal > 0)
		//							{
		//								boolLien = true;
		//								lngKey = FCConvert.ToInt32(rsFlat.Get_Fields_Int32("ID"));
		//								dblCurrentInterest = dblCHGInt;
		//							}
		//						}
		//					}
		//				}
		//				if (dblCurrentInterest != 0)
		//				{
		//					if (boolLien)
		//					{
		//						// this will edit the lien records charged interest
		//						// rsLien.Edit
		//						// rsLien.Fields("IntAdded") = rsLien.Fields("IntAdded") - dblCurrentInterest
		//						// rsLien.Update
		//					}
		//					else
		//					{
		//						// this will edit the bill records charged interest
		//						rsFlat.Edit();
		//						rsFlat.Set_Fields("WIntAdded", rsFlat.Get_Fields_Double("SIntAdded") - dblCurrentInterest);
		//						rsFlat.Update();
		//						// moved this so only non liened accounts are charged the flat interest rate #96056
		//						CreateCHGINTForBill_20(false, lngKey, FCConvert.ToInt32(rsFlat.Get_Fields_Int32("AccountKey")), boolLien, dblCurrentInterest);
		//					}
		//				}
		//				rsFlat.MoveNext();
		//			}
		//		}
		//		CheckForFlatInterest = true;
		//		return CheckForFlatInterest;
		//	}
		//	catch (Exception ex)
		//	{
				
		//		CheckForFlatInterest = false;
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking For Flat Interest", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return CheckForFlatInterest;
		//}

		//public static int CreateBlankUTBill_6(int lngAccountKey, int lngMK, string strWS = "")
		//{
		//	return CreateBlankUTBill(ref lngAccountKey, ref lngMK, strWS);
		//}

		public static int CreateBlankUTBill(ref int lngAccountKey, ref int lngMK, string strWS = "")
		{
			int CreateBlankUTBill = 0;
			int intError = 0;
			// this function will create a blank bill on the account passed in
			// using the lear passed for RE or PP whatever is passed into strType
			// then it will return the billkey that was just created.
			// kk04232014 trout-1081  Added optional strWS param
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngBK = 0;
				clsDRWrapper rsMaster = new clsDRWrapper();
				clsDRWrapper rsMeter = new clsDRWrapper();
				clsDRWrapper rsNew = new clsDRWrapper();
				intError = 2;
				rsMaster.OpenRecordset(UTMasterQuery(lngAccountKey), modExtraModules.strUTDatabase);
				if (!rsMaster.EndOfFile())
				{
					rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND ID = " + FCConvert.ToString(lngMK), modExtraModules.strUTDatabase);
					if (rsMeter.EndOfFile())
					{
						MessageBox.Show("No Meter for this account.  Please add a meter to this account before adding a prepayment.", "No Meter", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						lngBK = -1;
						return CreateBlankUTBill;
					}
					rsNew.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND BillingRateKey = 0", modExtraModules.strUTDatabase);
					if (!rsNew.EndOfFile())
					{
						// MsgBox "There is already a prepayment bill created.", vbExclamation, "Bill Not Created"
						lngBK = -1;
						return CreateBlankUTBill;
					}
					intError = 4;
					rsNew.AddNew();
					rsNew.Update();
					intError = 5;
					intError = 7;
					rsNew.Set_Fields("AccountKey", lngAccountKey);
					rsNew.Set_Fields("MeterKey", lngMK);
					rsNew.Set_Fields("ActualAccountNumber", GetAccountNumber(ref lngAccountKey));
					intError = 9;
					rsNew.Set_Fields("Book", rsMeter.Get_Fields_Int32("BookNumber"));
					// rsNew.Fields("Seq") = rsMeter.Fields("Sequence")
					// kk04232014 trout-1081  Force Bill to selected Service if Service has been turned off on Meter
					if (strWS != "" && FCConvert.ToString(rsMeter.Get_Fields_String("Service")) != strWS)
					{
						rsNew.Set_Fields("Service", "B");
					}
					else
					{
						rsNew.Set_Fields("Service", rsMeter.Get_Fields_String("Service"));
					}
					rsNew.Set_Fields("CombinationCode", rsMeter.Get_Fields_String("Combine"));
					intError = 10;
					rsNew.Set_Fields("CreationDate", DateTime.Today);
					rsNew.Set_Fields("LastUpdatedDate", DateTime.Today);
					rsNew.Set_Fields("BillStatus", "D");
					// kk 06172013 trouts-15  Bill Fields with Nulls messing up prepays
					rsNew.Set_Fields("BillNumber", 0);
					rsNew.Set_Fields("BillingYear", 0);
					rsNew.Set_Fields("BillingRateKey", 0);
					rsNew.Set_Fields("NoBill", 0);
					rsNew.Set_Fields("WLienStatusEligibility", 0);
					rsNew.Set_Fields("WLienProcessStatus", 0);
					rsNew.Set_Fields("WLienRecordNumber", 0);
					rsNew.Set_Fields("WCombinationLienKey", 0);
					rsNew.Set_Fields("SLienStatusEligibility", 0);
					rsNew.Set_Fields("SLienProcessStatus", 0);
					rsNew.Set_Fields("SLienRecordNumber", 0);
					rsNew.Set_Fields("SCombinationLienKey", 0);
					rsNew.Set_Fields("CurDate", DateAndTime.DateValue(FCConvert.ToString(0)));
					rsNew.Set_Fields("CurReading", 0);
					rsNew.Set_Fields("PrevDate", DateAndTime.DateValue(FCConvert.ToString(0)));
					rsNew.Set_Fields("PrevReading", 0);
					rsNew.Set_Fields("Consumption", 0);
					rsNew.Set_Fields("WaterOverrideCons", 0);
					rsNew.Set_Fields("WaterOverrideAmount", 0);
					rsNew.Set_Fields("SewerOverrideCons", 0);
					rsNew.Set_Fields("SewerOverrideAmount", 0);
					rsNew.Set_Fields("WMiscAmount", 0);
					rsNew.Set_Fields("WAdjustAmount", 0);
					rsNew.Set_Fields("WDEAdjustAmount", 0);
					rsNew.Set_Fields("WFlatAmount", 0);
					rsNew.Set_Fields("WUnitsAmount", 0);
					rsNew.Set_Fields("WConsumptionAmount", 0);
					rsNew.Set_Fields("SMiscAmount", 0);
					rsNew.Set_Fields("SAdjustAmount", 0);
					rsNew.Set_Fields("SDEAdjustAmount", 0);
					rsNew.Set_Fields("SFlatAmount", 0);
					rsNew.Set_Fields("SUnitsAmount", 0);
					rsNew.Set_Fields("SConsumptionAmount", 0);
					rsNew.Set_Fields("WTax", 0);
					rsNew.Set_Fields("STax", 0);
					rsNew.Set_Fields("TotalWBillAmount", 0);
					rsNew.Set_Fields("TotalSBillAmount", 0);
					rsNew.Set_Fields("WIntPaidDate", DateAndTime.DateValue(FCConvert.ToString(0)));
					rsNew.Set_Fields("WPrinOwed", 0);
					rsNew.Set_Fields("WTaxOwed", 0);
					rsNew.Set_Fields("WIntOwed", 0);
					rsNew.Set_Fields("WIntAdded", 0);
					rsNew.Set_Fields("WCostOwed", 0);
					rsNew.Set_Fields("WCostAdded", 0);
					rsNew.Set_Fields("WPrinPaid", 0);
					rsNew.Set_Fields("WTaxPaid", 0);
					rsNew.Set_Fields("WIntPaid", 0);
					rsNew.Set_Fields("WCostPaid", 0);
					rsNew.Set_Fields("SIntPaidDate", DateAndTime.DateValue(FCConvert.ToString(0)));
					rsNew.Set_Fields("SPrinOwed", 0);
					rsNew.Set_Fields("STaxOwed", 0);
					rsNew.Set_Fields("SIntOwed", 0);
					rsNew.Set_Fields("SIntAdded", 0);
					rsNew.Set_Fields("SCostOwed", 0);
					rsNew.Set_Fields("SCostAdded", 0);
					rsNew.Set_Fields("SPrinPaid", 0);
					rsNew.Set_Fields("STaxPaid", 0);
					rsNew.Set_Fields("SIntPaid", 0);
					rsNew.Set_Fields("SCostPaid", 0);
					rsNew.Set_Fields("WRT1", 0);
					rsNew.Set_Fields("WRT2", 0);
					rsNew.Set_Fields("WRT3", 0);
					rsNew.Set_Fields("WRT4", 0);
					rsNew.Set_Fields("WRT5", 0);
					rsNew.Set_Fields("SRT1", 0);
					rsNew.Set_Fields("SRT2", 0);
					rsNew.Set_Fields("SRT3", 0);
					rsNew.Set_Fields("SRT4", 0);
					rsNew.Set_Fields("SRT5", 0);
					rsNew.Set_Fields("Copies", 0);
					rsNew.Set_Fields("WCat", 0);
					rsNew.Set_Fields("SCat", 0);
					rsNew.Set_Fields("Loadback", 0);
					rsNew.Set_Fields("Final", 0);
					rsNew.Set_Fields("WBillOwner", rsMaster.Get_Fields_Boolean("WBillToOwner"));
					rsNew.Set_Fields("SBillOwner", rsMaster.Get_Fields_Boolean("SBillToOwner"));
					rsNew.Set_Fields("BillDate", DateAndTime.DateValue(FCConvert.ToString(0)));
					rsNew.Set_Fields("ReadingUnits", 0);
					rsNew.Set_Fields("WOrigBillAmount", 0);
					rsNew.Set_Fields("SOrigBillAmount", 0);
					rsNew.Set_Fields("SHasOverride", 0);
					rsNew.Set_Fields("WHasOverride", 0);
					rsNew.Set_Fields("SDemandGroupID", 0);
					rsNew.Set_Fields("WDemandGroupID", 0);
					rsNew.Set_Fields("SendEBill", 0);
					rsNew.Set_Fields("WinterBill", 0);
					rsNew.Set_Fields("MeterChangedOut", 0);
					rsNew.Set_Fields("SUploaded", 0);
					rsNew.Set_Fields("WUploaded", 0);
					intError = 11;
					if (FCConvert.ToString(rsMaster.Get_Fields_String("Name")) != "")
					{
						rsNew.Set_Fields("BName", rsMaster.Get_Fields_String("Name"));
						rsNew.Set_Fields("BName2", rsMaster.Get_Fields_String("Name2"));
						rsNew.Set_Fields("BAddress1", rsMaster.Get_Fields_String("BAddress1"));
						rsNew.Set_Fields("BAddress2", rsMaster.Get_Fields_String("BAddress2"));
						rsNew.Set_Fields("BAddress3", rsMaster.Get_Fields_String("BAddress3"));
						rsNew.Set_Fields("BCity", rsMaster.Get_Fields_String("BCity"));
						rsNew.Set_Fields("BState", rsMaster.Get_Fields_String("BState"));
						rsNew.Set_Fields("BZip", rsMaster.Get_Fields_String("BZip"));
						rsNew.Set_Fields("BZip4", rsMaster.Get_Fields_String("BZip4"));
					}
					else
					{
						// TODO: Field [OwnerName] not found!! (maybe it is an alias?)
						rsNew.Set_Fields("BName", rsMaster.Get_Fields("OwnerName"));
						// TODO: Field [SecondOwnerName] not found!! (maybe it is an alias?)
						rsNew.Set_Fields("BName2", rsMaster.Get_Fields("SecondOwnerName"));
						rsNew.Set_Fields("BAddress1", rsMaster.Get_Fields_String("OAddress1"));
						rsNew.Set_Fields("BAddress2", rsMaster.Get_Fields_String("OAddress2"));
						rsNew.Set_Fields("BAddress3", rsMaster.Get_Fields_String("OAddress3"));
						rsNew.Set_Fields("BCity", rsMaster.Get_Fields_String("OCity"));
						rsNew.Set_Fields("BState", rsMaster.Get_Fields_String("OState"));
						rsNew.Set_Fields("BZip", rsMaster.Get_Fields_String("OZip"));
						rsNew.Set_Fields("BZip4", rsMaster.Get_Fields_String("OZip4"));
					}
					intError = 12;
					// TODO: Field [OwnerName] not found!! (maybe it is an alias?)
					rsNew.Set_Fields("OName", rsMaster.Get_Fields("OwnerName"));
					// TODO: Field [SecondOwnerName] not found!! (maybe it is an alias?)
					rsNew.Set_Fields("OName2", rsMaster.Get_Fields("SecondOwnerName"));
					rsNew.Set_Fields("OAddress1", rsMaster.Get_Fields_String("OAddress1"));
					rsNew.Set_Fields("OAddress2", rsMaster.Get_Fields_String("OAddress2"));
					rsNew.Set_Fields("OAddress3", rsMaster.Get_Fields_String("OAddress3"));
					rsNew.Set_Fields("OCity", rsMaster.Get_Fields_String("OCity"));
					rsNew.Set_Fields("OState", rsMaster.Get_Fields_String("OState"));
					rsNew.Set_Fields("OZip", rsMaster.Get_Fields_String("OZip"));
					rsNew.Set_Fields("OZip4", rsMaster.Get_Fields_String("OZip4"));
					intError = 15;
					intError = 17;
					if (!rsMaster.IsFieldNull("StreetNumber"))
					{
						rsNew.Set_Fields("Location", rsMaster.Get_Fields_Int32("StreetNumber") + " " + rsMaster.Get_Fields_String("StreetName"));
					}
					else
					{
						rsNew.Set_Fields("Location", rsMaster.Get_Fields_String("StreetName"));
					}
					intError = 18;
					rsNew.Set_Fields("MapLot", Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("MapLot"))));
					if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BookPage"))) != "B0P0")
					{
						// kk02122016 trout-1205  Transfer Book & Page to Bill record
						rsNew.Set_Fields("BookPage", rsMaster.Get_Fields_String("BookPage"));
					}
					intError = 19;
					if (rsNew.Update())
					{
						lngBK = FCConvert.ToInt32(rsNew.Get_Fields_Int32("ID"));
					}
					else
					{
						lngBK = -1;
					}
				}
				else
				{
					MessageBox.Show("Cannot find account " + FCConvert.ToString(GetAccountNumber(ref lngAccountKey)) + " in the UT Master file.", "Bill Not Created", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					lngBK = -1;
				}
				CreateBlankUTBill = lngBK;
				return CreateBlankUTBill;
			}
			catch (Exception ex)
			{
				
				CreateBlankUTBill = -1;
				// denotes an error or noncompletion of the creation of the bill
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Blank Bill - " + FCConvert.ToString(intError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateBlankUTBill;
		}

		//public static string GetUTAccountComment_2(int lngAcctKey)
		//{
		//	return GetUTAccountComment(ref lngAcctKey);
		//}

		public static string GetUTAccountComment(ref int lngAcctKey)
		{
			string GetUTAccountComment = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsComment = new clsDRWrapper();
				rsComment.OpenRecordset("SELECT * FROM CommentRec WHERE AccountKey = " + FCConvert.ToString(lngAcctKey), modExtraModules.strUTDatabase);
				if (!rsComment.EndOfFile())
				{
					GetUTAccountComment = Strings.Trim(FCConvert.ToString(rsComment.Get_Fields_String("Comment")));
				}
				return GetUTAccountComment;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Returning Comment", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetUTAccountComment;
		}

		//private static bool CheckForValidReversal(ref int lngPayKey, ref DateTime dtCHGINTDate, int lngAccount, int intYear)
		//{
		//	bool CheckForValidReversal = false;
		//	try
		//	{
		//		// On Error GoTo ERROR_HANDLER
		//		// this function will return true if the payment made has an
		//		// effective transaction date of the applied int date or after
		//		// else it will return false
		//		clsDRWrapper rsPay = new clsDRWrapper();
		//		DateTime dtDate;
		//		int lngKey;
		//		// copied from modStatusPayments for trout-1222 05.25.17
		//		rsPay.OpenRecordset("SELECT Max(RecordedTransactionDate) as LastDate FROM PaymentRec WHERE AccountKey = " + lngAccount + " AND Reference <> 'CHGINT' AND Year = " + intYear, modExtraModules.strUTDatabase);
		//		// TODO: Field [LastDate] not found!! (maybe it is an alias?)
		//		dtDate = Convert.ToDateTime(rsPay.Get_Fields("LastDate"));
		//		rsPay.OpenRecordset("SELECT MAX(ID) as LastPaymentID FROM PaymentRec WHERE Account = " + lngAccount + " AND Reference <> 'CHGINT' AND Year = " + intYear, modExtraModules.strUTDatabase);
		//		// TODO: Field [LastPaymentID] not found!! (maybe it is an alias?)
		//		lngKey = FCConvert.ToInt32(rsPay.Get_Fields("LastPaymentID"));
		//		rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = " + lngPayKey, modExtraModules.strUTDatabase);
		//		if (rsPay.RecordCount() > 0)
		//		{
		//			if (DateAndTime.DateDiff("d", rsPay.Get_Fields_DateTime("RecordedTransactionDate"), dtDate) <= 0)
		//			{
		//				if (FCConvert.ToInt32(rsPay.Get_Fields_Int32("ID")) == lngKey)
		//				{
		//					CheckForValidReversal = true;
		//				}
		//				else
		//				{
		//					CheckForValidReversal = false;
		//				}
		//			}
		//			else
		//			{
		//				CheckForValidReversal = false;
		//			}
		//		}
		//		else
		//		{
		//			CheckForValidReversal = true;
		//		}
		//		return CheckForValidReversal;
		//	}
		//	catch (Exception ex)
		//	{
				
		//		MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating Reversal", MessageBoxButtons.OK, MessageBoxIcon.Hand);
		//	}
		//	return CheckForValidReversal;
		//}
		// vbPorter upgrade warning: lngCurrentAccountUT As int	OnWrite(int, string)
		// vbPorter upgrade warning: lngBill As int	OnWrite(string, int)
		//public static DateTime GetLastInterestDate(int lngCurAcctKeyUT, int lngBill, string strWS, DateTime dtPassDate, int lngCurrKey)
		//{
		//	DateTime GetLastInterestDate = System.DateTime.Now;
		//	// MAL@20070920: New function to update to the last Interest Paid Date after reversal
		//	// Call Reference: 113872
		//	bool blnSuccess;
		//	bool blnContinue;
		//	DateTime dtNewDate = DateTime.FromOADate(0);
		//	int lngRK = 0;
		//	int lngAcctKey = 0;
		//	int lngBillKey = 0;
		//	int lngCIKey = 0;
		//	clsDRWrapper rsBill = new clsDRWrapper();
		//	clsDRWrapper rsRK = new clsDRWrapper();
		//	clsDRWrapper rsUpdate = new clsDRWrapper();
		//	string strFieldNm;
		//	blnContinue = true;
		//	// Optimistic
		//	strFieldNm = strWS + "IntPaidDate";
		//	//rsBill.OpenRecordset("SELECT ID FROM Master WHERE AccountNumber = " + FCConvert.ToString(lngCurAcctKeyUT), modExtraModules.strUTDatabase);
		//	//if (rsBill.RecordCount() > 0)
		//	//{
		//	//    lngAcctKey = FCConvert.ToInt32(rsBill.Get_Fields("ID"));
		//	//    blnContinue = true;
		//	//}
		//	//else
		//	//{
		//	//    blnContinue = false;
		//	//}
		//	if (blnContinue)
		//	{
		//		rsBill.OpenRecordset("SELECT Bill.ID AS Bill FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngCurAcctKeyUT) + " AND BillNumber = " + FCConvert.ToString(lngBill), modExtraModules.strUTDatabase);
		//		if (rsBill.RecordCount() > 0)
		//		{
		//			// TODO: Field [Bill] not found!! (maybe it is an alias?)
		//			lngBillKey = FCConvert.ToInt32(rsBill.Get_Fields("Bill"));
		//			blnContinue = true;
		//		}
		//		else
		//		{
		//			blnContinue = false;
		//		}
		//	}
		//	if (blnContinue)
		//	{
		//		rsBill.OpenRecordset("SELECT * FROM PaymentRec WHERE AccountKey = " + FCConvert.ToString(lngCurAcctKeyUT) + " AND BillKey = " + FCConvert.ToString(lngBillKey) + " AND CHGINTNumber <> 0 order by effectiveinterestdate desc,ID DESC", modExtraModules.strUTDatabase);
		//		//trout-1222 kjr sort order same as CL
		//		if (rsBill.RecordCount() > 0)
		//		{
		//			lngCIKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("CHGINTNumber"));
		//			if (lngCurrKey == 0)
		//			{
		//				lngCurrKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
		//			}
		//			blnContinue = true;
		//		}
		//		else
		//		{
		//			blnContinue = false;
		//		}
		//	}
		//	if (blnContinue)
		//	{
		//		rsBill.OpenRecordset("SELECT Max(EffectiveInterestDate) as LastDate, AccountKey FROM PaymentRec WHERE AccountKey = " + FCConvert.ToString(lngCurAcctKeyUT) + " AND ID <> " + FCConvert.ToString(lngCIKey) + " AND ID <> " + FCConvert.ToString(lngCurrKey) + " AND BillKey = " + FCConvert.ToString(lngBillKey) + " AND RecordedTransactionDate < '" + FCConvert.ToString(dtPassDate) + "' GROUP BY AccountKey", modExtraModules.strUTDatabase);
		//		if (rsBill.RecordCount() > 0)
		//		{
		//			// TODO: Field [LastDate] not found!! (maybe it is an alias?)
		//			dtNewDate = (DateTime)rsBill.Get_Fields("LastDate");
		//		}
		//		else
		//		{
		//			// No previous payments - use Rate ID
		//			rsRK.OpenRecordset("SELECT BillingRateKey FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngCurAcctKeyUT) + " AND ID = " + FCConvert.ToString(lngBillKey), modExtraModules.strUTDatabase);
		//			if (rsRK.RecordCount() > 0)
		//			{
		//				lngRK = FCConvert.ToInt32(rsRK.Get_Fields_Int32("BillingRateKey"));
		//				rsRK.OpenRecordset("SELECT IntStart FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRK), modExtraModules.strUTDatabase);
		//				if (rsRK.RecordCount() > 0)
		//				{
		//					dtNewDate = (DateTime)rsRK.Get_Fields_DateTime("IntStart");
		//				}
		//			}
		//		}
		//		if (!fecherFoundation.FCUtils.IsNull(dtNewDate))
		//		{
		//			GetLastInterestDate = dtNewDate;
		//		}
		//	}
		//	else
		//	{
		//		// No other payments - use Rate ID
		//		rsRK.OpenRecordset("SELECT BillingRateKey FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngCurAcctKeyUT) + " AND ID = " + FCConvert.ToString(lngBillKey), modExtraModules.strUTDatabase);
		//		if (rsRK.RecordCount() > 0)
		//		{
		//			lngRK = FCConvert.ToInt32(rsRK.Get_Fields_Int32("BillingRateKey"));
		//			rsRK.OpenRecordset("SELECT IntStart FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRK), modExtraModules.strUTDatabase);
		//			if (rsRK.RecordCount() > 0)
		//			{
		//				dtNewDate = (DateTime)rsRK.Get_Fields_DateTime("IntStart");
		//			}
		//		}
		//		if (!fecherFoundation.FCUtils.IsNull(dtNewDate))
		//		{
		//			GetLastInterestDate = dtNewDate;
		//		}
		//	}
		//	return GetLastInterestDate;
		//}

		//public static DateTime GetLastInterestDate_Lien(int lngCurAcctKeyUT, int lngBill, string strWS, DateTime dtPassDate, int lngCurrKey, int lngLienKey)
		//{
		//	DateTime GetLastInterestDate_Lien = System.DateTime.Now;
		//	// MAL@20070920: New function to update to the last Interest Paid Date after reversal
		//	// Call Reference: 113872/10850
		//	//this function was unused until trout-1222
		//	bool blnSuccess;
		//	bool blnContinue;
		//	DateTime dtNewDate = DateTime.FromOADate(0);
		//	// vbPorter upgrade warning: intPer As short --> As int	OnWrite(string)
		//	int intPer;
		//	int lngRK = 0;
		//	int lngAcctKey = 0;
		//	//int lngBillKey = 0;
		//	int lngCIKey = 0;
		//	//int lngLienKey = 0;
		//	clsDRWrapper rsBill = new clsDRWrapper();
		//	clsDRWrapper rsRK = new clsDRWrapper();
		//	clsDRWrapper rsUpdate = new clsDRWrapper();
		//	string strFieldNm = "";
		//	blnContinue = true;
		//	// Optimistic
		//	intPer = FCConvert.ToInt32(Strings.Right(FCConvert.ToString(lngBill), 1));
		//	//if (blnContinue)
		//	//{
		//	//    rsBill.OpenRecordset("SELECT Bill, SLienRecordNumber, WLienRecordNumber FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(lngCurAcctKeyUT), modExtraModules.strUTDatabase);
		//	//    if (rsBill.RecordCount() > 0)
		//	//    {
		//	//        lngBillKey = FCConvert.ToInt32(rsBill.Get_Fields("BillKey"));
		//	//        lngLienKey = FCConvert.ToInt32(rsBill.Get_Fields(strWS + "LienRecordNumber"));
		//	//        lngAcctKey = FCConvert.ToInt32(rsBill.Get_Fields("AccountKey"));
		//	//        blnContinue = true;
		//	//    }
		//	//    else
		//	//    {
		//	//        blnContinue = false;
		//	//    }
		//	//}
		//	if (blnContinue)
		//	{
		//		rsBill.OpenRecordset("SELECT * FROM PaymentRec WHERE AccountKey = " + FCConvert.ToString(lngCurAcctKeyUT) + " AND BillKey = " + FCConvert.ToString(lngLienKey) + " AND CHGINTNumber <> 0 order by effectiveinterestdate desc,ID DESC", modExtraModules.strUTDatabase);
		//		//trout-1222 kjr sort order same as CL
		//		if (rsBill.RecordCount() > 0)
		//		{
		//			lngCIKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("CHGINTNumber"));
		//			if (lngCurrKey == 0)
		//			{
		//				lngCurrKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
		//			}
		//			blnContinue = true;
		//		}
		//		else
		//		{
		//			blnContinue = false;
		//		}
		//	}
		//	if (blnContinue)
		//	{
		//		rsBill.OpenRecordset("SELECT Max(EffectiveInterestDate) as LastDate, AccountKey FROM PaymentRec WHERE AccountKey = " + FCConvert.ToString(lngCurAcctKeyUT) + " AND ID <> " + FCConvert.ToString(lngCIKey) + " AND ID <> " + FCConvert.ToString(lngCurrKey) + " AND BillKey = " + FCConvert.ToString(lngLienKey) + " AND RecordedTransactionDate < '" + FCConvert.ToString(dtPassDate) + "' GROUP BY AccountKey", modExtraModules.strUTDatabase);
		//		if (rsBill.RecordCount() > 0)
		//		{
		//			// TODO: Field [LastDate] not found!! (maybe it is an alias?)
		//			dtNewDate = (DateTime)rsBill.Get_Fields("LastDate");
		//		}
		//		else
		//		{
		//			// No previous payments - use Rate ID
		//			rsRK.OpenRecordset("SELECT RateKey FROM Lien WHERE LienKey = " + FCConvert.ToString(lngLienKey), modExtraModules.strUTDatabase);
		//			if (rsRK.RecordCount() > 0)
		//			{
		//				lngRK = FCConvert.ToInt32(rsRK.Get_Fields_Int32("RateKey"));
		//				rsRK.OpenRecordset("SELECT IntStart FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRK), modExtraModules.strUTDatabase);
		//				if (rsRK.RecordCount() > 0)
		//				{
		//					dtNewDate = (DateTime)rsRK.Get_Fields_DateTime("IntStart");
		//				}
		//			}
		//		}
		//		if (!fecherFoundation.FCUtils.IsNull(dtNewDate))
		//		{
		//			GetLastInterestDate_Lien = dtNewDate;
		//		}
		//	}
		//	else
		//	{
		//		GetLastInterestDate_Lien = DateTime.Today;
		//	}
		//	return GetLastInterestDate_Lien;
		//}
		// vbPorter upgrade warning: boolLien As object	OnWrite(bool)
		//public static void VerifyUTBillAmounts_18(int lngBillKey, string strService, object boolLien)
		//{
		//	VerifyUTBillAmounts(lngBillKey, strService, ref boolLien);
		//}

		//public static void VerifyUTBillAmounts(int lngBillKey, string strService, ref object boolLien)
		//{
		//	// Verify that All Bill Amounts Match the Current Payments
		//	// Tracker Reference: 12219
		//	// kk02282014 Change to detect lien correctly; Consolidate code
		//	clsDRWrapper rsBill = new clsDRWrapper();
		//	clsDRWrapper rsPayment = new clsDRWrapper();
		//	double dblPrinOwed_Bill = 0;
		//	double dblTaxOwed_Bill = 0;
		//	double dblIntOwed_Bill = 0;
		//	double dblCostOwed_Bill = 0;
		//	double dblPLIOwed_Bill = 0;
		//	double dblIntAdded_Bill = 0;
		//	double dblCostAdded_Bill;
		//	double dblPrinPaid_Bill = 0;
		//	double dblTaxPaid_Bill = 0;
		//	double dblIntPaid_Bill = 0;
		//	double dblCostPaid_Bill = 0;
		//	double dblPLIPaid_Bill = 0;
		//	double dblPrinPaid_Pymt = 0;
		//	double dblTaxPaid_Pymt = 0;
		//	double dblIntPaid_Pymt = 0;
		//	double dblIntChg_Pymt = 0;
		//	double dblCostPaid_Pymt = 0;
		//	double dblPLIPaid_Pymt = 0;
		//	double dblCostChg_Pymt = 0;
		//	double dblTotalOwed = 0;
		//	double dblTotalPaid = 0;
		//	double dblTotal;
		//	if (!FCConvert.ToBoolean(boolLien))
		//	{
		//		// Non-Liened Records
		//		rsBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBillKey) + " ORDER BY ActualAccountNumber, ID", modExtraModules.strUTDatabase);
		//		if (rsBill.RecordCount() > 0)
		//		{
		//			if (FCConvert.ToInt32(rsBill.Get_Fields(strService + "LienRecordNumber")) == 0)
		//			{
		//				// Reset Values
		//				dblPrinPaid_Pymt = 0;
		//				dblTaxPaid_Pymt = 0;
		//				dblIntPaid_Pymt = 0;
		//				dblCostPaid_Pymt = 0;
		//				dblIntChg_Pymt = 0;
		//				dblCostChg_Pymt = 0;
		//				dblPrinOwed_Bill = rsBill.Get_Fields(strService + "PrinOwed");
		//				dblTaxOwed_Bill = rsBill.Get_Fields(strService + "TaxOwed");
		//				dblIntOwed_Bill = rsBill.Get_Fields(strService + "IntOwed");
		//				dblIntAdded_Bill = FCConvert.ToDouble(rsBill.Get_Fields(strService + "IntAdded")) * -1;
		//				dblCostOwed_Bill = FCConvert.ToDouble(rsBill.Get_Fields(strService + "CostOwed")) + FCConvert.ToDouble(rsBill.Get_Fields(strService + "CostAdded")) * -1;
		//				dblTotalOwed = dblPrinOwed_Bill + dblTaxOwed_Bill + dblIntOwed_Bill + dblIntAdded_Bill + dblCostOwed_Bill;
		//				dblPrinPaid_Bill = rsBill.Get_Fields(strService + "PrinPaid");
		//				dblTaxPaid_Bill = rsBill.Get_Fields(strService + "TaxPaid");
		//				dblIntPaid_Bill = rsBill.Get_Fields(strService + "IntPaid");
		//				dblCostPaid_Bill = rsBill.Get_Fields(strService + "CostPaid");
		//				dblTotalPaid = dblPrinPaid_Bill + dblTaxPaid_Bill + dblIntPaid_Bill + dblCostPaid_Bill;
		//				dblTotal = dblTotalOwed - dblTotalPaid;
		//				// Get Charged Interest to Compare
		//				rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Service = '" + strService + "' AND (Lien = 0 OR Lien IS NULL) AND (Reference = 'CHGINT' Or UPPER(Reference) = 'INTEREST') AND ReceiptNumber >= 0", modExtraModules.strUTDatabase);
		//				if (rsPayment.RecordCount() > 0)
		//				{
		//					while (!rsPayment.EndOfFile())
		//					{
		//						dblIntChg_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")) * -1;
		//						rsPayment.MoveNext();
		//					}
		//				}
		//				// Get Charged Costs (Demand Fees) to Compare
		//				rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Service = '" + strService + "' AND (Lien = 0 OR Lien IS NULL) AND (Code = '3' Or Code = 'L') AND ReceiptNumber >= 0", modExtraModules.strUTDatabase);
		//				if (rsPayment.RecordCount() > 0)
		//				{
		//					while (!rsPayment.EndOfFile())
		//					{
		//						dblCostChg_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")) * -1;
		//						rsPayment.MoveNext();
		//					}
		//				}
		//				rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Service = '" + strService + "' AND (Lien = 0 OR Lien IS NULL) AND Reference <> 'CHGINT' AND UPPER(Reference) <> 'INTEREST' AND Code <> '3' AND Code <> 'L' AND ReceiptNumber >= 0", modExtraModules.strUTDatabase);
		//				if (rsPayment.RecordCount() > 0)
		//				{
		//					while (!rsPayment.EndOfFile())
		//					{
		//						// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//						dblPrinPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields("Principal"));
		//						// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
		//						dblTaxPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields("Tax"));
		//						dblIntPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
		//						if (rsPayment.Get_Fields_String("Code") != "X")
		//						{
		//							dblCostPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
		//						}
		//						rsPayment.MoveNext();
		//					}
		//					// Principal ; Only Change the Paid Amount, Should Not Change the Charged Amount
		//					if (dblPrinPaid_Bill != dblPrinPaid_Pymt)
		//					{
		//						rsBill.Edit();
		//						rsBill.Set_Fields(strService + "PrinPaid", dblPrinPaid_Pymt);
		//						rsBill.Update();
		//					}
		//					// Tax
		//					if (dblTaxPaid_Bill != dblTaxPaid_Pymt)
		//					{
		//						rsBill.Edit();
		//						rsBill.Set_Fields(strService + "TaxPaid", dblTaxPaid_Pymt);
		//						rsBill.Update();
		//					}
		//					// Costs
		//					if (dblCostOwed_Bill != dblCostChg_Pymt)
		//					{
		//						rsBill.Edit();
		//						rsBill.Set_Fields(strService + "CostAdded", dblCostChg_Pymt * -1);
		//						rsBill.Update();
		//					}
		//					if (dblCostPaid_Bill != dblCostPaid_Pymt)
		//					{
		//						rsBill.Edit();
		//						rsBill.Set_Fields(strService + "CostPaid", dblCostPaid_Pymt);
		//						rsBill.Update();
		//					}
		//					// Charged Interest ; Check All Pieces (Paid and Charged)
		//					if (dblIntAdded_Bill != dblIntChg_Pymt)
		//					{
		//						rsBill.Edit();
		//						rsBill.Set_Fields(strService + "IntAdded", dblIntChg_Pymt * -1);
		//						rsBill.Update();
		//					}
		//					// Check Payments
		//					if (dblIntPaid_Bill != dblIntPaid_Pymt)
		//					{
		//						rsBill.Edit();
		//						rsBill.Set_Fields(strService + "IntPaid", dblIntPaid_Pymt);
		//						rsBill.Update();
		//					}
		//				}
		//				else
		//				{
		//					// Do Nothing - No Payments, Nothing to Fix
		//				}
		//			}
		//			else
		//			{
		//				// Do Nothing - Bill is Liened but payment is to orig bill?
		//			}
		//		}
		//		else
		//		{
		//			// Do Nothing - No Bill Record
		//		}
		//	}
		//	else
		//	{
		//		// Liened Bills
		//		rsBill.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(lngBillKey), modExtraModules.strUTDatabase);
		//		if (rsBill.RecordCount() > 0)
		//		{
		//			// Reset Values
		//			dblPrinPaid_Pymt = 0;
		//			dblTaxPaid_Pymt = 0;
		//			dblIntPaid_Pymt = 0;
		//			dblCostPaid_Pymt = 0;
		//			dblPLIPaid_Pymt = 0;
		//			dblIntChg_Pymt = 0;
		//			// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//			dblPrinOwed_Bill = rsBill.Get_Fields("Principal");
		//			// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
		//			dblTaxOwed_Bill = rsBill.Get_Fields("Tax");
		//			// TODO: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
		//			dblIntOwed_Bill = FCConvert.ToDouble(rsBill.Get_Fields("IntAdded")) * -1;
		//			// TODO: Check the table for the column [Interest] and replace with corresponding Get_Field method
		//			dblPLIOwed_Bill = rsBill.Get_Fields("Interest");
		//			// TODO: Check the table for the column [Costs] and replace with corresponding Get_Field method
		//			// TODO: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
		//			dblCostOwed_Bill = FCConvert.ToDouble(rsBill.Get_Fields("Costs")) + FCConvert.ToDouble(rsBill.Get_Fields("MaturityFee")) * -1;
		//			dblTotalOwed = dblPrinOwed_Bill + dblTaxOwed_Bill + dblIntOwed_Bill + dblPLIOwed_Bill + dblCostOwed_Bill;
		//			// TODO: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
		//			dblPrinPaid_Bill = rsBill.Get_Fields("PrinPaid");
		//			// TODO: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
		//			dblTaxPaid_Bill = rsBill.Get_Fields("TaxPaid");
		//			// TODO: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
		//			dblIntPaid_Bill = rsBill.Get_Fields("IntPaid");
		//			// TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
		//			dblPLIPaid_Bill = rsBill.Get_Fields("PLIPaid");
		//			dblCostPaid_Bill = rsBill.Get_Fields_Double("CostPaid");
		//			dblTotalPaid = dblPrinPaid_Bill + dblTaxPaid_Bill + dblIntPaid_Bill + dblPLIPaid_Bill + dblCostPaid_Bill;
		//			dblTotal = dblTotalOwed - dblTotalPaid;
		//			rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Service = '" + strService + "' AND Lien = 1 AND (Reference = 'CHGINT' OR UPPER(Reference) = 'INTEREST') AND ReceiptNumber >= 0", modExtraModules.strUTDatabase);
		//			if (rsPayment.RecordCount() > 0)
		//			{
		//				while (!rsPayment.EndOfFile())
		//				{
		//					dblIntChg_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")) * -1;
		//					rsPayment.MoveNext();
		//				}
		//			}
		//			rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Service = '" + strService + "' AND Lien = 1 AND Code <> '3' AND Code <> 'L' AND Code <> 'I' AND ReceiptNumber >= 0", modExtraModules.strUTDatabase);
		//			if (rsPayment.RecordCount() > 0)
		//			{
		//				while (!rsPayment.EndOfFile())
		//				{
		//					// TODO: Check the table for the column [Principal] and replace with corresponding Get_Field method
		//					dblPrinPaid_Pymt += rsPayment.Get_Fields("Principal");
		//					// TODO: Check the table for the column [Tax] and replace with corresponding Get_Field method
		//					dblTaxPaid_Pymt += rsPayment.Get_Fields("Tax");
		//					dblIntPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
		//					dblPLIPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest"));
		//					if (rsPayment.Get_Fields_String("Code") != "X")
		//					{
		//						dblCostPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
		//					}
		//					rsPayment.MoveNext();
		//				}
		//				// Principal ; Only Change the Paid Amount, Should Not Change the Charged Amount
		//				if (dblPrinPaid_Bill != dblPrinPaid_Pymt)
		//				{
		//					rsBill.Edit();
		//					rsBill.Set_Fields("PrinPaid", dblPrinPaid_Pymt);
		//					rsBill.Update();
		//				}
		//				// Costs ; Check All Pieces (Paid and Charged)
		//				if (dblCostPaid_Bill != dblCostPaid_Pymt)
		//				{
		//					rsBill.Edit();
		//					rsBill.Set_Fields("CostPaid", dblCostPaid_Pymt);
		//					rsBill.Update();
		//				}
		//				// Charged Interest ; Check All Pieces (Paid and Charged)
		//				if (dblIntOwed_Bill != dblIntChg_Pymt)
		//				{
		//					rsBill.Edit();
		//					rsBill.Set_Fields("IntAdded", dblIntChg_Pymt * -1);
		//					rsBill.Update();
		//				}
		//				// Check Payments
		//				if (dblIntPaid_Bill != dblIntPaid_Pymt)
		//				{
		//					rsBill.Edit();
		//					rsBill.Set_Fields("IntPaid", dblIntPaid_Pymt);
		//					rsBill.Update();
		//				}
		//				// Pre-Lien Interest ; Check All Pieces (Paid and Charged)
		//				if (dblPLIPaid_Bill != dblPLIPaid_Pymt)
		//				{
		//					rsBill.Edit();
		//					rsBill.Set_Fields("PLIPaid", dblPLIPaid_Pymt);
		//					rsBill.Update();
		//				}
		//				// Tax ; Check All Pieces (Paid and Charged)
		//				if (dblTaxPaid_Bill != dblTaxPaid_Pymt)
		//				{
		//					rsBill.Edit();
		//					rsBill.Set_Fields("TaxPaid", dblTaxPaid_Pymt);
		//					rsBill.Update();
		//				}
		//			}
		//			else
		//			{
		//				// Do Nothing - No Payments, Nothing to Fix
		//			}
		//		}
		//		else
		//		{
		//			// Do Nothing - Lien Record Does Not Exist
		//		}
		//	}
		//}

		public static bool IsAccountTaxAcquired_7(int lngAcct)
		{
			return IsAccountTaxAcquired(0, lngAcct);
		}

		public static bool IsAccountTaxAcquired(int lngBill = 0, int lngAcct = 0)
		{
			bool IsAccountTaxAcquired = false;
			bool blnResult = false;
			int lngAccount = 0;
			clsDRWrapper rsMaster = new clsDRWrapper();
			clsDRWrapper rsBill = new clsDRWrapper();
			clsDRWrapper rsRE = new clsDRWrapper();
			if (lngAcct != 0)
			{
				lngAccount = lngAcct;
			}
			else
			{
				rsBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBill), modExtraModules.strUTDatabase);
				if (rsBill.RecordCount() > 0)
				{
					lngAccount = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ActualAccountNumber"));
				}
			}
			rsMaster.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + FCConvert.ToString(lngAccount), modExtraModules.strUTDatabase);
			if (rsMaster.RecordCount() > 0)
			{
				blnResult = FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("TaxAcquired"));
			}
			if (blnResult)
			{
				// do nothing
			}
			else
			{
				if (modGlobalConstants.Statics.gboolRE && modGlobalConstants.Statics.gblnCheckRETaxAcq && FCConvert.ToInt32(rsMaster.Get_Fields_Int32("REAccount")) != 0)
				{
					rsRE.OpenRecordset("SELECT * FROM Master WHERE ISNull(RSDeleted,0) = 0 AND RSCard = 1 AND RSAccount = " + FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields_Int32("REAccount"))), modExtraModules.strREDatabase);
					if (rsRE.EndOfFile() != true && rsRE.BeginningOfFile() != true)
					{
						blnResult = FCConvert.ToBoolean(rsRE.Get_Fields_Boolean("TaxAcquired"));
					}
				}
			}
			IsAccountTaxAcquired = blnResult;
			return IsAccountTaxAcquired;
		}

		public static DateTime GetTaxAcquiredDate(int lngBill = 0, int lngAcct = 0)
		{
			DateTime GetTaxAcquiredDate = System.DateTime.Now;
			DateTime datResult = DateTime.FromOADate(0);
			int lngAccount = 0;
			clsDRWrapper rsMaster = new clsDRWrapper();
			clsDRWrapper rsBill = new clsDRWrapper();
			clsDRWrapper rsRE = new clsDRWrapper();
			if (lngBill != 0)
			{
				rsBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBill), modExtraModules.strUTDatabase);
				if (rsBill.RecordCount() > 0)
				{
					lngAccount = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ActualAccountNumber"));
				}
			}
			else
			{
				lngAccount = lngAcct;
			}
			rsMaster.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + FCConvert.ToString(lngAccount), modExtraModules.strUTDatabase);
			if (rsMaster.RecordCount() > 0)
			{
				if (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("TaxAcquired")))
				{
					datResult = (DateTime)rsMaster.Get_Fields_DateTime("TADate");
				}
			}
			if (modGlobalConstants.Statics.gboolRE && modGlobalConstants.Statics.gblnCheckRETaxAcq && FCConvert.ToInt32(rsMaster.Get_Fields_Int32("REAccount")) != 0)
			{
				rsRE.OpenRecordset("SELECT * FROM Master WHERE ISNULL(RSDeleted,0) = 0 AND RSCard = 1 AND RSAccount = " + FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields_Int32("REAccount"))), modExtraModules.strREDatabase);
				if (rsRE.EndOfFile() != true && rsRE.BeginningOfFile() != true)
				{
					if (datResult.ToOADate() > rsRE.Get_Fields_DateTime("TADate").ToOADate())
					{
						datResult = (DateTime)rsRE.Get_Fields_DateTime("TADate");
					}
				}
			}
			GetTaxAcquiredDate = datResult;
			return GetTaxAcquiredDate;
		}

		public static string GetTaxAcquiredAccountList()
		{
			string GetTaxAcquiredAccountList = "";
			clsDRWrapper rsUT = new clsDRWrapper();
			clsDRWrapper rsRE = new clsDRWrapper();
			GetTaxAcquiredAccountList = "";
			rsUT.OpenRecordset("SELECT * FROM Master WHERE ISNULL(TaxAcquired,0) = 1 or REAccount <> 0", "twut0000.vb1");
			if (rsUT.EndOfFile() != true && rsUT.BeginningOfFile() != true)
			{
				do
				{
					if (FCConvert.ToBoolean(rsUT.Get_Fields_Boolean("TaxAcquired")))
					{
						// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						GetTaxAcquiredAccountList = GetTaxAcquiredAccountList + rsUT.Get_Fields("AccountNumber") + ", ";
					}
					else
					{
						if (modGlobalConstants.Statics.gboolRE && modGlobalConstants.Statics.gblnCheckRETaxAcq)
						{
							rsRE.OpenRecordset("SELECT * FROM Master WHERE ISNULL(RSDeleted,0) = 0 AND RSCard = 1 AND RSAccount = " + FCConvert.ToString(Conversion.Val(rsUT.Get_Fields_Int32("REAccount"))), modExtraModules.strREDatabase);
							if (rsRE.EndOfFile() != true && rsRE.BeginningOfFile() != true)
							{
								if (FCConvert.ToBoolean(rsRE.Get_Fields_Boolean("TaxAcquired")))
								{
									// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
									GetTaxAcquiredAccountList = GetTaxAcquiredAccountList + rsUT.Get_Fields("AccountNumber") + ", ";
								}
							}
						}
					}
					rsUT.MoveNext();
				}
				while (rsUT.EndOfFile() != true);
			}
			if (GetTaxAcquiredAccountList != "")
			{
				GetTaxAcquiredAccountList = Strings.Left(GetTaxAcquiredAccountList, GetTaxAcquiredAccountList.Length - 2);
			}
			return GetTaxAcquiredAccountList;
		}

		public static string UTMasterQuery(int lngAcctKey)
		{
			string UTMasterQuery = "";
			string strSQL;
			strSQL =
                "SELECT m.*,m.DeedName1 AS OwnerName,m.DeedName2 AS SecondOwnerName,pOwn.Address1 AS OAddress1,pOwn.Address2 AS OAddress2,pOwn.Address3 AS OAddress3,pOwn.City AS OCity,pOwn.State AS OState, pOwn.Zip AS OZip," +
                "       pBill.FullNameLF AS Name,pBill2.FullNameLF AS Name2,pBill.Address1 AS BAddress1,pBill.Address2 AS BAddress2,pBill.Address3 AS BAddress3,pBill.City AS BCity,pBill.State AS BState, pBill.Zip AS BZip " +
                "FROM Master m INNER JOIN " + modGlobal.Statics.strDbCP +
                "PartyAndAddressView pOwn ON pOwn.ID = m.OwnerPartyID INNER JOIN " + modGlobal.Statics.strDbCP +
                "PartyAndAddressView pBill ON pBill.ID = m.BillingPartyID " + "     LEFT JOIN " +
                modGlobal.Statics.strDbCP + "PartyNameView pOwn2 ON pOwn2.ID = m.SecondOwnerPartyID LEFT JOIN " +
                modGlobal.Statics.strDbCP + "PartyNameView pBill2 ON pBill2.ID = m.SecondBillingPartyID " +
                "WHERE m.ID = " + FCConvert.ToString(lngAcctKey);
			UTMasterQuery = strSQL;
			return UTMasterQuery;
		}

		public class StaticVariables
		{
	
			// vbPorter upgrade warning: UTEffectiveDate As DateTime	OnWrite(string, DateTime)
			public DateTime UTEffectiveDate;
		
			public bool gboolShowLastUTAccountInCR;
			public bool gboolPayWaterFirst;
			// this is true if the user wants to apply money to water payments before sewer, false for sewer before water
			public string TownService = "";
			// (W)ater (S)ewer or (B)oth
			public int lngCurrentAccountUT;
			//public int lngCurrentAccountKeyUT;
			public double gdblUTMuniTaxRate;
			// holds the municipality's tax rate, held in the work record ID 13
			public bool gboolAutoPayOldestBillFirst;
		
			public int intPerdiemCounter;
			public PerdiemInfo[] pdiInfo = null;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
