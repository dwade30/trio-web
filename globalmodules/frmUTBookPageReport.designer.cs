//Fecher vbPorter - Version 1.0.0.40
using System;
using Wisej.Web;

namespace Global
{
    /// <summary>
    /// Summary description for frmUTBookPageReport.
    /// </summary>
    partial class frmUTBookPageReport : BaseForm
    {
        public fecherFoundation.FCComboBox cmbWS;
        public fecherFoundation.FCLabel lblWS;
        public fecherFoundation.FCComboBox cmbBPType;
        public fecherFoundation.FCLabel lblBPType;
        public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtAccount;
        public fecherFoundation.FCFrame fraAccountRange;
        public fecherFoundation.FCTextBox txtAccount_1;
        public fecherFoundation.FCTextBox txtAccount_0;
        public fecherFoundation.FCLabel lblAccount;
        public fecherFoundation.FCLabel Label2;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
                _InstancePtr = null;
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUTBookPageReport));
            this.cmbWS = new fecherFoundation.FCComboBox();
            this.lblWS = new fecherFoundation.FCLabel();
            this.cmbBPType = new fecherFoundation.FCComboBox();
            this.lblBPType = new fecherFoundation.FCLabel();
            this.fraAccountRange = new fecherFoundation.FCFrame();
            this.txtAccount_1 = new fecherFoundation.FCTextBox();
            this.txtAccount_0 = new fecherFoundation.FCTextBox();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.cmdSave = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountRange)).BeginInit();
            this.fraAccountRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 365);
            this.BottomPanel.Size = new System.Drawing.Size(664, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdSave);
            this.ClientArea.Controls.Add(this.fraAccountRange);
            this.ClientArea.Controls.Add(this.cmbWS);
            this.ClientArea.Controls.Add(this.lblWS);
            this.ClientArea.Controls.Add(this.cmbBPType);
            this.ClientArea.Controls.Add(this.lblBPType);
            this.ClientArea.Size = new System.Drawing.Size(664, 305);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(664, 60);
            // 
            // HeaderText
            //
            this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(213, 30);
            this.HeaderText.Text = "Book Page Report";
            // 
            // cmbWS
            // 
            this.cmbWS.AutoSize = false;
            this.cmbWS.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbWS.FormattingEnabled = true;   
         this.cmbWS.Items.AddRange(new object[] {
            "Water",
            "Sewer"});
            this.cmbWS.Location = new System.Drawing.Point(169, 30);
            this.cmbWS.Name = "cmbWS";
            this.cmbWS.Size = new System.Drawing.Size(170, 40);
            this.cmbWS.TabIndex = 4;
            this.cmbWS.Text = "Water";
            // 
            // lblWS
            // 
            this.lblWS.AutoSize = true;
            this.lblWS.Location = new System.Drawing.Point(30, 44);
            this.lblWS.Name = "lblWS";
            this.lblWS.Size = new System.Drawing.Size(62, 15);
            this.lblWS.TabIndex = 1;
            this.lblWS.Text = "SERVICE";
            // 
            // cmbBPType
            // 

            this.cmbBPType.AutoSize = false;
            this.cmbBPType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbBPType.FormattingEnabled = true;        
            this.cmbBPType.Items.AddRange(new object[] {
            "All Accounts",
            "Eligible for Lien"});
            this.cmbBPType.Location = new System.Drawing.Point(169, 90);
            this.cmbBPType.Name = "cmbBPType";
            this.cmbBPType.Size = new System.Drawing.Size(170, 40);
            this.cmbBPType.TabIndex = 4;
            this.cmbBPType.Text = "All Accounts";
            // 
            // lblBPType
            // 
            this.lblBPType.AutoSize = true;
            this.lblBPType.Location = new System.Drawing.Point(30, 104);
            this.lblBPType.Name = "lblBPType";
            this.lblBPType.Size = new System.Drawing.Size(104, 15);
            this.lblBPType.TabIndex = 5;
            this.lblBPType.Text = "ACCOUNT TYPE";
            // 
            // fraAccountRange
            // 
            this.fraAccountRange.Controls.Add(this.txtAccount_1);
            this.fraAccountRange.Controls.Add(this.txtAccount_0);
            this.fraAccountRange.Controls.Add(this.lblAccount);
            this.fraAccountRange.Controls.Add(this.Label2);
            this.fraAccountRange.Location = new System.Drawing.Point(30, 150);
            this.fraAccountRange.Name = "fraAccountRange";
            this.fraAccountRange.Size = new System.Drawing.Size(400, 128);
            this.fraAccountRange.TabIndex = 3;
            this.fraAccountRange.Text = "Account Range";
            // 
            // txtAccount_1
            // 
			
            this.txtAccount_1.AutoSize = false;
            this.txtAccount_1.BackColor = System.Drawing.SystemColors.Window;

            this.txtAccount_1.LinkItem = null;
            this.txtAccount_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAccount_1.LinkTopic = null;      
            this.txtAccount_1.Location = new System.Drawing.Point(210, 68);
            this.txtAccount_1.Name = "txtAccount_1";
            this.txtAccount_1.Size = new System.Drawing.Size(170, 40);
            this.txtAccount_1.TabIndex = 6;
            this.txtAccount_1.Enter += new System.EventHandler(this.txtAccount_Enter);
            // 
            // txtAccount_0
            // 
			
            this.txtAccount_0.AutoSize = false;
            this.txtAccount_0.BackColor = System.Drawing.SystemColors.Window;
			
            this.txtAccount_0.LinkItem = null;
            this.txtAccount_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
            this.txtAccount_0.LinkTopic = null;
            this.txtAccount_0.Location = new System.Drawing.Point(20, 68);
            this.txtAccount_0.Name = "txtAccount_0";
            this.txtAccount_0.Size = new System.Drawing.Size(170, 40);
            this.txtAccount_0.TabIndex = 4;
            this.txtAccount_0.Enter += new System.EventHandler(this.txtAccount_Enter);
            // 
            // lblAccount
            // 
            this.lblAccount.Location = new System.Drawing.Point(210, 30);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(86, 18);
            this.lblAccount.TabIndex = 7;
            this.lblAccount.Text = "END";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(66, 18);
            this.Label2.TabIndex = 5;
            this.Label2.Text = "START";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(30, 297);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
            // 
            // frmUTBookPageReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(664, 473);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmUTBookPageReport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Book Page Report";
            this.Load += new System.EventHandler(this.frmUTBookPageReport_Load);
            this.Activated += new System.EventHandler(this.frmUTBookPageReport_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUTBookPageReport_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUTBookPageReport_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountRange)).EndInit();
            this.fraAccountRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

        }
		#endregion

		private fecherFoundation.FCButton cmdSave;
	}
}
