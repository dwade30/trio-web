//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;


namespace Global
{
	/// <summary>
	/// Summary description for rptUTDailyAuditMaster.
	/// </summary>
	public class rptUTDailyAuditMaster : fecherFoundation.FCForm
	{

// nObj = 1
//   0	rptUTDailyAuditMaster	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/06/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/01/2005              *
		// ********************************************************
		public bool boolWide; // True if this report is in Wide Format
		public int lngWidth; // This is the Print Width of the report
		bool boolDone;
		public string strType = "";
		public bool boolOrderByReceipt; // order by receipt
		public bool boolCloseOutAudit; // Actually close out the payments
		public bool boolLandscape; // is the report to be printed landscape?
		public int intType; // 0 - Water, 1 - Sewer, 2 - Both
		bool boolSaveReport;
		bool blnBoth;
		bool blnBothSelected;

		private void ActiveReport_FetchData(ref bool EOF)
		{
			// this should let the detail section fire once
			EOF = boolDone;
			if (blnBothSelected) {
				if (blnBoth) {
					blnBoth = false;
				} else {
					boolDone = true;
					intType = 1;
				}
			} else {
				boolDone = true;
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			switch (KeyCode) {
				
				case Keys.Escape:
				{
					Close();
					break;
				}
			} //end switch
		}

		private void ActiveReport_PageStart()
		{
			FCGlobal.Screen.MousePointer = 1;
			frmWait.InstancePtr.Hide();
		}

		private void ActiveReport_ReportEnd()
		{
			FCGlobal.Screen.MousePointer = 1;
			frmWait.InstancePtr.Hide();

			// MAL@20080826: Add check for recreate before saving
			// Tracker Reference: 14245
			if (boolCloseOutAudit && boolSaveReport && !modGlobalConstants.gblnRecreate) {
				// save the report only if they are actually getting closed out
				if (this.Pages.Count>0) {
					modGlobalFunctions.IncrementSavedReports_2("LastUTDailyAudit");
					this.Pages.Save("RPT\\LastUTDailyAudit1.RDF");
				}
			}

			// MAL@20081020: This was causing issues with the report printing
			// Tracker Reference: 15930
			// If Not IsThisCR Then
			// If boolCloseOutAudit Then
			// Unload Me
			// End If
			// Else
			// Unload Me
			// End If
			// 
		}

		private void ActiveReport_ReportStart()
		{
			FCGlobal.Screen.MousePointer = 11;
			frmWait.InstancePtr.Init("Please Wait..."+"\r\n"+"Loading", true);
		}

		private void SetupFields()
		{
			// find out which way the user wants to print

			boolWide = boolLandscape;

			if (boolWide) { // wide format
				lngWidth = 13500;
				Printer.Orientation = ddOLandscape;
			} else { // narrow format
				lngWidth = 10800;
				Printer.Orientation = ddOPortrait;
			}

			this.PrintWidth = lngWidth;
			sarUTDailyAudit.Width = lngWidth;
		}

		public void StartUp(ref bool boolCloseOut, ref bool boolPassLandscape, ref short intPassType = 2, ref bool boolShowPrinterDialog = true, ref bool boolPrint = false, ref string strAuditPrinter = "")
		{
			try
			{	// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsC = new clsDRWrapper();

				boolCloseOutAudit = boolCloseOut;
				if (boolCloseOutAudit) {
					if (modUTStatusPayments.TownService=="W") {
						intType = 0;
					} else if (modUTStatusPayments.TownService=="S") {
						intType = 1;
					} else {
						intType = 2; // automatically both
					}
				} else {
					intType = intPassType;
				}

				if (intType==2) {
					blnBoth = true;
					blnBothSelected = true;
					intType = 0;
				}

				boolLandscape = boolPassLandscape;

				modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);

				SetupFields();

				frmWait.InstancePtr.IncrementProgress();

				// If IsThisCR Then
				rsC.OpenRecordset("SELECT * FROM PaymentRec WHERE ISNULL(DailyCloseOut,0) = "+Convert.ToString(modCR.glngCurrentCloseOut), modExtraModules.strUTDatabase);
				// Else
				// rsC.OpenRecordset "SELECT * FROM PaymentRec WHERE DailyCloseOut = 0", strUTDatabase
				// End If
				if (rsC.EndOfFile()) {
					boolSaveReport = false;
				} else {
					boolSaveReport = true;
				}

				// MAL@20080812: Change to accomodate recreate option
				// Tracker Reference: 14245
				// If Not IsThisCR Then
				// If boolCloseOutAudit Then
				// rptUTDailyAuditMaster.Run
				// rptUTDailyAuditMaster.PrintReport True
				// Else
				// frmReportViewer.Init Me
				// End If
				// Else
				// rptUTDailyAuditMaster.Run False
				// Doevents
				// ActiveReport_ReportEnd
				// If boolPrint Then
				// DuplexPrintReport rptUTDailyAuditMaster, strAuditPrinter, , boolLandscape
				// End If
				// End If
				return;
			}
			catch
			{	// ERROR_HANDLER:
				MessageBox.Show("Error #"+Convert.ToString(Information.Err().Number)+" - "+Information.Err().Description+".", "Error In Start Up", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format()
		{
			sarUTDailyAudit.Object = new arUTDailyAudit();

			// 0 - Water, 1 - Sewer, 2 - Both
			sarUTDailyAudit.Object.Tag = intType;
		}

		private void rptUTDailyAuditMaster_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptUTDailyAuditMaster.Caption	= "Daily Audit Report";
			//rptUTDailyAuditMaster.Icon	= "rptUTDailtAuditMaster.dsx":0000";
			//rptUTDailyAuditMaster.Left	= 0;
			//rptUTDailyAuditMaster.Top	= 0;
			//rptUTDailyAuditMaster.Width	= 11880;
			//rptUTDailyAuditMaster.Height	= 8595;
			//rptUTDailyAuditMaster.StartUpPosition	= 3;
			//rptUTDailyAuditMaster.SectionData	= "rptUTDailtAuditMaster.dsx":058A;
			//End Unmaped Properties
		}
	}
}