﻿using fecherFoundation;
using System;
using System.Collections.Generic;
using Wisej.Web;
using System.Linq;
using SharedApplication.Commands;
using TWSharedLibrary;

namespace Global
{
    public partial class SelectArchive : Form
    {
        const string CNSTREPPARCHIVETYPE = "CommitmentArchive";

        public SelectArchive()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>

        private void SelectArchive_Load(object sender, EventArgs e)
        {
            this.comboBox1.Clear();
#if TWPP0000
            if (TWPP0000.modPPGN.Statics.boolInArchiveMode)
#endif
#if TWRE0000
            if (TWRE0000.modGlobalVariables.Statics.boolInArchiveMode)
#endif      
            {
                this.comboBox1.AddItem("CURRENT - CURRENT YEAR DATA");
                this.comboBox1.ItemData(this.comboBox1.NewIndex, 0);
                this.comboBox1.SelectedIndex = 0;
            }
            cArchiveUtility tArch = new cArchiveUtility();
            FCCollection tColl = new FCCollection();
            tColl = tArch.GetArchiveConnectionGroups(CNSTREPPARCHIVETYPE, 0);
            if (!(tColl == null))
            {
                foreach (ConnectionGroup tcGroup in tColl)
                {
                    this.comboBox1.AddItem($"{tcGroup.ID} - {tcGroup.Description}");
                    this.comboBox1.ItemData(this.comboBox1.NewIndex, tcGroup.ID);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string moduleName = "";
            string moduleCaption = "";
#if TWPP0000
            moduleName = "TWPP0000";
            moduleCaption = "Personal Property";
#endif
#if TWRE0000
            moduleName = "TWRE0000";
            moduleCaption = "Real Estate";
#endif
            int moduleOpenForms = Application.OpenForms.OfType<Form>().Where(form => form.GetType().Assembly.GetName().Name.ToUpper() == moduleName).Count();
            if (moduleOpenForms > 1)
            {
                MessageBox.Show($"All {moduleCaption} tabs must be closed before changing the working year", "Run Archive", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            clsDRWrapper rsLoad = new clsDRWrapper();
            int intYear = 0;
            intYear = this.comboBox1.ItemData(this.comboBox1.SelectedIndex);
            if (intYear > 2000)
            {
                rsLoad.DefaultGroup = CNSTREPPARCHIVETYPE + "_" + FCConvert.ToString(intYear);
                modGlobalConstants.Statics.gstrArchiveYear = intYear.ToString();
                App.MainForm.StatusBarText1 = TWSharedLibrary.Variables.Statics.DataGroup + " " + modGlobalConstants.Statics.gstrArchiveYear;
                modGlobalConstants.Statics.MuniName = TWSharedLibrary.Variables.Statics.DataGroup + " " + modGlobalConstants.Statics.gstrArchiveYear;
#if TWPP0000
                TWPP0000.modPPGN.Statics.boolInArchiveMode = true;
                TWPP0000.modPPGN.ModSpecificReset();
#endif
#if TWRE0000
                TWRE0000.modGlobalVariables.Statics.boolInArchiveMode = true;
#endif
	            Close();
	            StaticSettings.GlobalCommandDispatcher.Send(new SetArchiveMode
	            {
		            ArchiveMode = modGlobalConstants.Statics.gstrArchiveYear != "",
		            ArchiveTag = modGlobalConstants.Statics.gstrArchiveYear == "" ? "Live" : rsLoad.DefaultGroup
				});
			}
            else if (intYear == 0)
            {
                rsLoad.DefaultGroup = "Live";
                modGlobalConstants.Statics.gstrArchiveYear = "";
                App.MainForm.StatusBarText1 = TWSharedLibrary.Variables.Statics.DataGroup;
                modGlobalConstants.Statics.MuniName = TWSharedLibrary.Variables.Statics.DataGroup;
#if TWPP0000
                TWPP0000.modPPGN.Statics.boolInArchiveMode = false;
#endif
#if TWRE0000
                TWRE0000.modGlobalVariables.Statics.boolInArchiveMode = false;
#endif
	            Close();
				StaticSettings.GlobalCommandDispatcher.Send(new SetArchiveMode
				{
					ArchiveMode = modGlobalConstants.Statics.gstrArchiveYear != "",
					ArchiveTag = modGlobalConstants.Statics.gstrArchiveYear == "" ? "Live" : rsLoad.DefaultGroup
				});
			}
            else
            {
                MessageBox.Show("The archive year is not valid.", "Invalid Archive Year", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
