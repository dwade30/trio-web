﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Wisej.Web;
using System.Runtime.InteropServices;
using System.ComponentModel;
using TWSharedLibrary;
#if TWCL0000
using TWCL0000;


#elif TWBD0000
using TWBD0000;


#elif TWCR0000
using TWCR0000;
#endif
namespace Global
{
    /// <summary>
    /// Summary description for frmCLGetAccount.
    /// </summary>
    public partial class frmCLGetAccount : BaseForm
    {
        public frmCLGetAccount()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmCLGetAccount InstancePtr
        {
            get
            {
                return (frmCLGetAccount)Sys.GetInstance(typeof(frmCLGetAccount));
            }
        }

        protected frmCLGetAccount _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               09/20/2002              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               11/02/2006              *
        // ********************************************************
        public string auxCmbSearchTypeItem2 = "Map / Lot";
        bool boolToMainMenu;
        clsDRWrapper rsBatch = new clsDRWrapper();
        clsDRWrapper rsBatchBackup = new clsDRWrapper();
        clsDRWrapper rsBatchLien;
        clsDRWrapper rsBLN = new clsDRWrapper();
        int lngYear;
        string strPaidBy = "";
        string strRef = "";
        string strTLRID = "";
        DateTime dtPaymentDate;
        DateTime dtEffectiveDate;
        double dblIntRate;
        double dblCurPrin;
        // will hold the value of the prin, int and cost to add to the grid
        double dblCurInt;
        // and will be cleared by the AccountValidate Account
        double dblPLInt;
        double dblCurCost;
        // these will be filled as a side effect of the CalculateAccount function
        DateTime dtIntPaidDate;
        // this will hold the InterestStartDate
        int lngBatchCHGINTNumber;
        // this is how the CHGINT Number will be passed back to the PaymentRec
        double dblXtraCurInt;
        bool boolResultsScreen;
        // this is true when the result grid is being shown
        double dblTotalDue;
        // this will hold the total to warn the user if they enter too much
        public int intPrePayYear;
        // this will be where the prepayyear form will put the year the user select
        bool boolSearch;
        string strSearchString = "";
        string strPassSortOrder = "";
        bool boolPassActivate;
        double dblOriginalAmount;
        bool boolBatchImport;
        bool boolLoaded;
        bool isTxtGetAccountNeedFocus = false;
        int selectionStart = 0;
        int selectionLength = 0;
        public int lngColBatchAcct;
        public int lngColBatchName;
        public int lngColBatchYear;
        public int lngColBatchRef;
        public int lngColBatchPrin;
        public int lngColBatchInt;
        public int lngColBatchCost;
        public int lngColBatchTotal;
        public int lngColBatchKey;
        public int lngColBatchPrintReceipt;
        public int lngColBatchCorrectAmount;
        // these constants are for the grid columns
        int lngColAcct;
        int lngColName;
        int lngcolLocationNumber;
        int lngColLocation;
        int lngColMapLot;
        int lngColBillRecords;
        int lngColBookPage;
        int lngColBSKey;
        int lngColBSName;
        int lngColBSMapLot;
        int lngColBSAccount;

        [StructLayout(LayoutKind.Sequential, Pack = 1)]
        private struct Import
        {
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] AccountCharArray;
            public string Account
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AccountCharArray);
                }
                set
                {
                    AccountCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 34)]
            public char[] NameCharArray;
            public string Name
            {
                get
                {
                    return FCUtils.FixedStringFromArray(NameCharArray);
                }
                set
                {
                    NameCharArray = FCUtils.FixedStringToArray(value, 34);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
            public char[] YearCharArray;
            public string Year
            {
                get
                {
                    return FCUtils.FixedStringFromArray(YearCharArray);
                }
                set
                {
                    YearCharArray = FCUtils.FixedStringToArray(value, 4);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 14)]
            public char[] AmountCharArray;
            public string Amount
            {
                get
                {
                    return FCUtils.FixedStringFromArray(AmountCharArray);
                }
                set
                {
                    AmountCharArray = FCUtils.FixedStringToArray(value, 14);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
            public char[] PaymentDateCharArray;
            public string PaymentDate
            {
                get
                {
                    return FCUtils.FixedStringFromArray(PaymentDateCharArray);
                }
                set
                {
                    PaymentDateCharArray = FCUtils.FixedStringToArray(value, 8);
                }
            }
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
            public char[] CRLFCharArray;
            public string CRLF
            {
                get
                {
                    return FCUtils.FixedStringFromArray(CRLFCharArray);
                }
                set
                {
                    CRLFCharArray = FCUtils.FixedStringToArray(value, 2);
                }
            }
        };

        Import CRImport = new Import();
        Import[] arrCRImportError = null;

        public void AutoShowAccount(bool boolREAcct, int lngAcct)
        {
            // this routien will start the payment screen with the correct account
            txtGetAccountNumber.Text = FCConvert.ToString(lngAcct);
            // Set the account number
            txtHold.Text = "P";
            // set the screen to Payment
            modStatusPayments.Statics.boolRE = boolREAcct;
            // Doevents
            cmdGetAccountNumber_Click();
        }

        private void cmbBatchList_DropDown(object sender, System.EventArgs e)
        {
            modAPIsConst.SendMessageByNum(this.cmbBatchList.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
        }

        private void cmbBatchList_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            int KeyCode = FCConvert.ToInt32(e.KeyCode);
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == FCConvert.ToInt32(Keys.Space))
            {
                if (modAPIsConst.SendMessageByNum(cmbBatchList.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
                {
                    modAPIsConst.SendMessageByNum(cmbBatchList.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
                    KeyCode = 0;
                }
            }
        }

        private void cmdBatch_Click(object sender, System.EventArgs e)
        {
            if (CheckBatchQuestions())
            {
                LockBatch_2(true);
            }
        }

        public void cmdBatch_Click()
        {
            cmdBatch_Click(cmdBatch, new System.EventArgs());
        }

        private void LockBatch_2(bool boolLock)
        {
            LockBatch(ref boolLock);
        }

        private void LockBatch(ref bool boolLock)
        {
            fraValidate.Visible = boolLock;
            //fraValidate.Top = fraBatchQuestions.Top + cmdBatch.Top - 100;
            //fraValidate.Left = FCConvert.ToInt32((frmCLGetAccount.InstancePtr.Width - fraValidate.Width) / 2.0);
            lblValidate.Text = "IMPORTANT: Verify that the information entered is correct and press Yes.";
            cmdBatch.Enabled = !boolLock;
            txtTellerID.Enabled = !boolLock;
            txtTaxYear.Enabled = !boolLock;
            txtPaymentDate2.Enabled = !boolLock;
            txtPaidBy.Enabled = !boolLock;
            chkReceipt.Enabled = !boolLock;
        }

        private void cmdBatchChoice_Click(object sender, System.EventArgs e)
        {
            if (cmbBatchList.SelectedIndex != -1)
            {
                string VBtoVar = fraRecover.Tag?.ToString();
                if (VBtoVar == "P")
                {
                    // Purge
                    PurgeRecords_2(cmbBatchList.Items[cmbBatchList.SelectedIndex].ToString());
                }
                else if (VBtoVar == "R")
                {
                    // Recover
                    RecoverRecords_2(cmbBatchList.Items[cmbBatchList.SelectedIndex].ToString());
                }
                else
                {
                }
            }
            else
            {
                FCMessageBox.Show("Please select a batch from the drop down list.", MsgBoxStyle.Information, "Data Error");
            }
        }

        private void cmdBatchSearch_Click(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                clsDRWrapper rsAcct = new clsDRWrapper();
                string strSQL = "";
                if (txtBatchSearchName.Text != "" || txtBatchSearchMapLot.Text != "" && Conversion.Val(txtTaxYear.Text) > 0)
                {
                    strSQL = "SELECT DISTINCT Account, ID, Name1, MapLot FROM BillingMaster WHERE BillingType = 'RE' AND BillingYear / 10 = " + txtTaxYear.Text;
                    if (txtBatchSearchName.Text != "")
                    {
                        strSQL += " AND ((Name1 > '" + Strings.Trim(txtBatchSearchName.Text) + "     ' AND Name1 < '" + Strings.Trim(txtBatchSearchName.Text) + "zzzzz') OR (Name2 > '" + Strings.Trim(txtBatchSearchName.Text) + "     ' AND Name2 < '" + Strings.Trim(txtBatchSearchName.Text) + "zzzzz'))";
                    }
                    if (txtBatchSearchMapLot.Text != "")
                    {
                        strSQL += " AND MapLot LIKE '" + Strings.Trim(txtBatchSearchMapLot.Text) + "%'";
                    }
                    strSQL += " ORDER BY Account";
                    rsAcct.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
                    if (rsAcct.EndOfFile())
                    {
                        FCMessageBox.Show("No accounts match the criteria entered.", MsgBoxStyle.Information, "No Matches");
                    }
                    else
                    {
                        if (rsAcct.RecordCount() == 1)
                        {
                            // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                            txtBatchAccount.Text = FCConvert.ToString(rsAcct.Get_Fields("Account"));
                            fraBatch.Enabled = true;
                            fraBatchSearchCriteria.Visible = false;
                            txtBatchAccount_Validate(false);
                        }
                        else
                        {
                            fraBatchSearchCriteria.Visible = false;
                            // fill the results into the grid
                            vsBatchSearch.Rows = 1;
                            while (!rsAcct.EndOfFile())
                            {
                                vsBatchSearch.AddItem("");
                                // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                vsBatchSearch.TextMatrix(vsBatchSearch.Rows - 1, lngColBSAccount, FCConvert.ToString(rsAcct.Get_Fields("Account")));
                                vsBatchSearch.TextMatrix(vsBatchSearch.Rows - 1, lngColBSKey, FCConvert.ToString(rsAcct.Get_Fields_Int32("ID")));
                                vsBatchSearch.TextMatrix(vsBatchSearch.Rows - 1, lngColBSName, FCConvert.ToString(rsAcct.Get_Fields_String("Name1")));
                                vsBatchSearch.TextMatrix(vsBatchSearch.Rows - 1, lngColBSMapLot, FCConvert.ToString(rsAcct.Get_Fields_String("MapLot")));
                                rsAcct.MoveNext();
                            }
                            FormatBatchSearchGrid();
                            //fraBatchSearchResults.Top = FCConvert.ToInt32((this.Height - fraBatchSearchResults.Height) / 2.0);
                            //fraBatchSearchResults.Left = FCConvert.ToInt32((this.Width - fraBatchSearchResults.Width) / 2.0);
                            fraBatchSearchResults.Visible = true;
                        }
                    }
                }
                else
                {
                    if (Conversion.Val(txtTaxYear.Text) > 0)
                    {
                        FCMessageBox.Show("Please enter criteria to search by.", MsgBoxStyle.Exclamation, "Invalid Data");
                    }
                    else
                    {
                        FCMessageBox.Show("The tax year entered is not valid, please go back and save it again.", MsgBoxStyle.Exclamation, "Invalid Data");
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Start Search");
                fraBatch.Enabled = true;
                fraBatchSearchCriteria.Visible = false;
                fraBatchSearchResults.Visible = false;
            }
        }

        private void cmdBatchSearchCancel_Click(object sender, System.EventArgs e)
        {
            fraBatchSearchCriteria.Visible = false;
            fraBatch.Enabled = true;
        }

        private void cmdClear_Click(object sender, System.EventArgs e)
        {
            if (boolResultsScreen)
            {
                // fraSearch.Visible = True
                // mnuProcessGetAccount.Enabled = True
                // mnuProcessSearch.Enabled = True
                // vsSearch.Visible = False
                // lblSearchListInstruction.Visible = False
                vsSearch.Visible = false;
                lblSearchListInstruction.Visible = false;
                fraSearch.Visible = true;
                lblInstructions.Visible = true;
            }
            else
            {
                cmbSearchType.SelectedIndex = 0;
                txtSearch.Text = "";
            }
        }

        private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
        {
            int intError = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                int holdaccount = 0;
                clsDRWrapper rsCL = new clsDRWrapper();
                clsDRWrapper rsRE = new clsDRWrapper();
                string strSQL = "";
                DialogResult intAns;
            // if this is a Real Estate transaction then boolRE will be true if it is Personal Property transaction then it will be false
            // If optRE.Value = True Then
            // boolRE = True
            // Else
            // boolRE = False
            // End If
            TryAgain:
                ;
                if (!fraBatch.Visible && !fraBatchQuestions.Visible)
                {
                    if (Conversion.Val(txtGetAccountNumber.Text) != 0)
                    {
                        holdaccount = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
                        intError = 1;
                        if (modStatusPayments.Statics.boolRE)
                        {
                            StaticSettings.TaxCollectionValues.LastAccountRE = holdaccount;
                            strSQL = "SELECT TOP(1) Account FROM BillingMaster WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE) + " AND BillingType = 'RE'";
                        }
                        else
                        {
                            StaticSettings.TaxCollectionValues.LastAccountPP = holdaccount;
                            strSQL = "SELECT TOP(1) Account FROM BillingMaster WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP) + " AND BillingType = 'PP'";
                        }
                        intError = 2;
                        rsCL.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
                        intError = 3;
                        if (rsCL.EndOfFile() != true && rsCL.BeginningOfFile() != true)
                        {
                            frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Loading Data", false);
                            // Create Queries on the database for the next screen to use
                            // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                            CreateStatusQueries_2(FCConvert.ToInt32(rsCL.Get_Fields("Account")));
                            // Doevents
                            frmRECLStatus.InstancePtr.boolUnloadOK = true;
                            frmRECLStatus.InstancePtr.Unload();
                            App.DoEvents();
                            // kk01262017 trocrs-55  Allow frmRECLStatus to finish unloading
                            frmRECLStatus.InstancePtr.FormLoaded = false;
                            // frmRECLStatus.FormReset = True
                            if (boolSearch)
                            {
                                frmRECLStatus.InstancePtr.strSearchSQL = strSearchString;
                                frmRECLStatus.InstancePtr.strPassSortOrder = strPassSortOrder;
                            }
                            else
                            {
                                frmRECLStatus.InstancePtr.strSearchSQL = "";
                                // this will only show one account
                            }
                            frmRECLStatus.InstancePtr.Show(App.MainForm);
                            frmWait.InstancePtr.Unload();
                            //FC:FINAL:SBE - #278 - Close() is called later at the same level, but another form is opened in the meantime. Use Hide() before showing another form
                            this.Hide();
                        }
                        else
                        {
                            // there are not bill records...check to see if there is a master file
                            intError = 4;
                            if (modStatusPayments.Statics.boolRE)
                            {
                                rsRE.OpenRecordset("SELECT ID FROM Master WHERE RSAccount = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE), modExtraModules.strREDatabase);
                            }
                            else
                            {
                                rsRE.OpenRecordset("SELECT ID FROM PPMaster WHERE Account = " + FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP), modExtraModules.strPPDatabase);
                            }
                            intError = 5;
                            if (rsRE.EndOfFile())
                            {
                                FCMessageBox.Show("The account selected does not exist.", MsgBoxStyle.Information, "Error");
                            }
                            else
                            {
                                if (Strings.Trim(txtHold.Text) == "P")
                                {
                                    intAns = FCMessageBox.Show("There are no bill records for this account.  Is this a prepayment for account " + FCConvert.ToString(holdaccount) + "?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Prepayment");
                                    if (intAns == DialogResult.Yes)
                                    {
                                        // default it to -1
                                        intPrePayYear = -1;
                                        intError = 6;
                                        intPrePayYear = frmPrePayYear.InstancePtr.Init(0);
                                        // Show vbModal, MDIParent   'this will set the intprepayyear
                                        intError = 7;
                                        if (intPrePayYear > 0)
                                        {
                                            if (modStatusPayments.Statics.boolRE)
                                            {
                                                intError = 8;
                                                modStatusPayments.CreateBlankCLBill_20(intPrePayYear, StaticSettings.TaxCollectionValues.LastAccountRE, "RE");
                                            }
                                            else
                                            {
                                                intError = 9;
                                                modStatusPayments.CreateBlankCLBill_20(intPrePayYear, StaticSettings.TaxCollectionValues.LastAccountPP, "PP");
                                            }

                                            intError = 37;
                                            goto TryAgain;
                                            // this will try this routine again to load the account
                                            // that should have one blank bill now
                                        }
                                        else
                                        {
                                            FCMessageBox.Show("Valid year not selected.", MsgBoxStyle.Exclamation, "Invalid Year");
                                        }
                                    }
                                    else if (intAns == DialogResult.No)
                                    {
                                    }
                                    else if (intAns == DialogResult.Cancel)
                                    {
                                    }
                                }
                                else
                                {
                                    // found accounts, but no payments
                                    FCMessageBox.Show("No bill records have been created for this account.", MsgBoxStyle.Critical, "No Bills");
                                }
                            }
                            if (modStatusPayments.Statics.boolRE)
                            {
                                intError = 38;
                                if (modExtraModules.IsThisCR() && !modGlobal.Statics.gboolShowLastCLAccountInCR)
                                {
                                    txtGetAccountNumber.Text = "";
                                }
                                else
                                {
                                    txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE);
                                }
                                intError = 39;
                                cmbRE.SelectedIndex = 0;
                                txtGetAccountNumber.SelectionStart = 0;
                                intError = 40;
                                txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
                                intError = 41;
                                if (txtGetAccountNumber.Enabled && txtGetAccountNumber.Visible)
                                {
                                    txtGetAccountNumber.Focus();
                                }
                                intError = 42;
                                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                                intError = 43;
                                return;
                            }
                            else
                            {
                                intError = 44;
                                if (modExtraModules.IsThisCR() && !modGlobal.Statics.gboolShowLastCLAccountInCR)
                                {
                                    txtGetAccountNumber.Text = "";
                                }
                                else
                                {
                                    txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP);
                                }
                                intError = 45;
                                cmbRE.SelectedIndex = 1;
                                txtGetAccountNumber.SelectionStart = 0;
                                intError = 46;
                                txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
                                intError = 47;
                                if (txtGetAccountNumber.Enabled && txtGetAccountNumber.Visible)
                                {
                                    txtGetAccountNumber.Focus();
                                }
                                intError = 48;
                                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                                intError = 49;
                                return;
                            }
                        }
                        intError = 50;
                        // set the last account field in the registry to the current account
                        if (cmbRE.SelectedIndex == 0)
                        {
                            intError = 51;
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "RELastAccountNumber", modGlobal.PadToString(StaticSettings.TaxCollectionValues.LastAccountRE, 6));
                            modStatusPayments.Statics.lngSearchListPointer = GetPointerValue(StaticSettings.TaxCollectionValues.LastAccountRE);
                        }
                        else
                        {
                            intError = 52;
                            modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "PPLastAccountNumber", modGlobal.PadToString(StaticSettings.TaxCollectionValues.LastAccountPP, 6));
                            modStatusPayments.Statics.lngSearchListPointer = GetPointerValue(StaticSettings.TaxCollectionValues.LastAccountPP);
                        }
                        intError = 53;
                    }
                    else
                    {
                        FCMessageBox.Show("Please enter a valid account number.", MsgBoxStyle.Information, "Invalid Account");
                        return;
                    }
                    Close();
                }
                else
                {
                    if (fraBatch.Visible)
                    {
                        cmdProcessBatch_Click();
                    }
                    else if (fraBatchQuestions.Visible)
                    {
                        cmdBatch_Click();
                    }
                    else
                    {
                        // do nothing
                    }
                }
                intError = 54;
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show("ERROR #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Get Account Error - " + FCConvert.ToString(intError));
                Close();
            }
        }

        public void cmdGetAccountNumber_Click()
        {
            cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
        }

        private void cmdProcessBatch_Click(object sender, System.EventArgs e)
        {
            if (FCMessageBox.Show("Are you sure you want to process the batch with total of " + txtTotal.Text + "?", MsgBoxStyle.YesNoCancel, "Process Batch") == DialogResult.Yes)
            {
                // print the batch receipts
                PrintBatchReceipts();
                // this will process the whole batch
                ProcessBatch();
            }
        }

        public void cmdProcessBatch_Click()
        {
            cmdProcessBatch_Click(cmdProcessBatch, new System.EventArgs());
        }

        private void cmdQuit_Click()
        {
            int lngAns/*unused?*/;
            boolToMainMenu = true;
            if (vsSearch.Visible == true)
            {
                vsSearch.Visible = false;
                lblSearchListInstruction.Visible = false;
                if (modExtraModules.IsThisCR() && !modGlobal.Statics.gboolShowLastCLAccountInCR)
                {
                }
                else
                {
                    if (modStatusPayments.Statics.boolRE)
                    {
                        txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE);
                    }
                    else
                    {
                        txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP);
                    }
                }
            }
            else
            {
                if (vsBatch.Rows > 1)
                {
                    if (!(FCMessageBox.Show("There are pending transactions in the batch.  They can be restored at a later date.  Would you like to continue?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Pending Transactions") == DialogResult.Yes))
                    {
                        // if no or cancel, then stay on this screen
                        return;
                    }
                }
                if (txtHold.Text == "C")
                {
                    frmCLGetAccount.InstancePtr.Close();
                }
                else
                {
                }
                frmCLGetAccount.InstancePtr.Close();
            }
        }

        private void cmdSearch_Click(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                int lngSelection = 0;
                string strAcctList = "";
                string strLeftOverAccts = "";
                string SQL = "";
                clsDRWrapper rs = new clsDRWrapper();
                clsDRWrapper rsBook = new clsDRWrapper();
                clsDRWrapper rsCLAccounts = new clsDRWrapper();
                clsDRWrapper rsBillRecords = new clsDRWrapper();
                string strList = "";
                string strBook = "";
                string strLastName = "";
                string strLastSecName = "";
                int lngLastAccount = 0;
                string strDoubleSearch = "";
                bool boolStartsWith;
                string strStartsWithSearch = "";
                string strStartsWithMaster = "";
                string strStartsWithMaster2 = "";
                bool boolShowPrevOwner;
                bool boolShowDeletedAccounts;
                string strOwnerName = "";
                string strSecOwnerName = "";
                boolStartsWith = cmbSearchPlace.SelectedIndex == 1;
                boolShowPrevOwner = FCConvert.CBool(chkShowPreviousOwnerInfo.CheckState == Wisej.Web.CheckState.Checked);
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "CLSearchShowPreviousOwner", boolShowPrevOwner ? "TRUE" : "FALSE");
                boolShowDeletedAccounts = FCConvert.CBool(chkShowDeletedAccountInfo.CheckState == Wisej.Web.CheckState.Checked);
                modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "CLSearchShowDeletedAccounts", boolShowDeletedAccounts ? "TRUE" : "FALSE");

                try
                {

                    switch (cmbSearchType.SelectedIndex)
                    {
                        //fraSearch.Visible = false;
                        // find out which search option has been checked
                        case 0:
                            lngSelection = 0;
                            // Name
                            break;
                        case 1:
                            lngSelection = 1;
                            // Street Name
                            break;
                        case 2:
                            lngSelection = 2;
                            // Map Lot
                            break;
                        default:
                            FCMessageBox.Show("You must select a field to search by from the 'Search By' box.", MsgBoxStyle.Information, "Search By");
                            return;
                    }
                    // make sure that the user typed search criteria in
                    if (Strings.Trim(txtSearch.Text) == "")
                    {
                        FCMessageBox.Show("You must type a search criteria in the white box.", MsgBoxStyle.Information, "Search Criteria");
                        fraSearch.Visible = true;
                        return;
                    }
                    frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Searching");
                    // Doevents
                    rsBillRecords.OpenRecordset("SELECT Distinct Account FROM BillingMaster", modExtraModules.strCLDatabase);
                    switch (lngSelection)
                    {
                        case 0:
                            {
                                // Name
                                if (boolStartsWith)
                                {
                                    if (modStatusPayments.Statics.boolRE)
                                    {
                                        strStartsWithSearch = " OR (Name1 LIKE ('%" + txtSearch.Text + "%')) ";
                                        strStartsWithMaster = " OR (DeedName1 LIKE ('%" + txtSearch.Text + "%')) ";
                                        strStartsWithMaster2 = " OR (DeedName2 LIKE ('%" + txtSearch.Text + "%')) ";
                                    }
                                    else
                                    {
                                        strStartsWithSearch = " OR (Name1 LIKE ('%" + txtSearch.Text + "%')) ";
                                        strStartsWithMaster = " OR (pOwn.FullNameLF LIKE ('%" + txtSearch.Text + "%')) ";
                                        strStartsWithMaster2 = "";
                                    }
                                }
                                else
                                {
                                    strStartsWithSearch = "";
                                    strStartsWithMaster = "";
                                    strStartsWithMaster2 = "";
                                }
                                // check for the type of account to look for Real Estate or Personal Property
                                if (modStatusPayments.Statics.boolRE)
                                {
                                    SQL = "SELECT * FROM BillingMaster INNER JOIN (SELECT ID AS BK FROM BillingMaster WHERE BillingType = 'RE' AND ((Name1 >= '" + txtSearch.Text + "  ' AND Name1 < '" + txtSearch.Text + "ZZZ') OR (Name2 >= '" + txtSearch.Text + "  ' AND Name2 < '" + txtSearch.Text + "ZZZ') " + strStartsWithSearch + ")) AS List ON BillingMaster.ID = List.BK ORDER BY Name1";
                                }
                                else
                                {
                                    SQL = "SELECT * FROM BillingMaster INNER JOIN (SELECT ID AS BK FROM BillingMaster WHERE BillingType = 'PP' AND ((Name1 >= '" + txtSearch.Text + "  ' AND Name1 < '" + txtSearch.Text + "ZZZ') OR (Name2 >= '" + txtSearch.Text + "  ' AND Name2 < '" + txtSearch.Text + "ZZZ') " + strStartsWithSearch + ")) AS List ON BillingMaster.ID = List.BK ORDER BY Name1";
                                }
                                // this will create a string of accounts that will be added in the grid as well as the RE matches (for old collection files with that name)
                                rsCLAccounts.OpenRecordset(SQL, modExtraModules.strCLDatabase);
                                strAcctList = ",";
                                while (!rsCLAccounts.EndOfFile())
                                {
                                    App.DoEvents();
                                    strAcctList += FCConvert.ToString(rsCLAccounts.Get_Fields("Account")) + ",";
                                    rsCLAccounts.MoveNext();
                                }
                                rsCLAccounts.MoveFirst();
                                if (modStatusPayments.Statics.boolRE)
                                {
                                    if (boolShowDeletedAccounts)
                                    {
                                        SQL = "SELECT DeedName1 AS Name, DeedName2 AS Name2, RSDeleted, RSLocStreet, RSLocNumAlph, RSAccount AS Account, RSMapLot AS MapLot " + "FROM Master WHERE RSCard = 1 AND (((DeedName1 >= '" + txtSearch.Text + "' AND DeedName1 < '" + txtSearch.Text + "ZZZ') " + strStartsWithMaster + ") OR ((DeedName2 >= '" + txtSearch.Text + "' AND DeedName2 < '" + txtSearch.Text + "ZZZ') " + strStartsWithMaster + ")) ORDER BY Name, Account";
                                    }
                                    else
                                    {
                                        SQL = "SELECT DeedName1 AS Name, DeedName2 AS Name2, RSDeleted, RSLocStreet, RSLocNumAlph, RSAccount AS Account, RSMapLot AS MapLot " + "FROM Master WHERE RSDeleted <> 1 AND RSCard = 1 AND (((DeedName1 >= '" + txtSearch.Text + "' AND DeedName1 < '" + txtSearch.Text + "ZZZ') " + strStartsWithMaster + ") OR ((DeedName2 >= '" + txtSearch.Text + "' AND DeedName2 < '" + txtSearch.Text + "ZZZ') " + strStartsWithMaster + ")) ORDER BY Name, Account";
                                    }
                                    rs.OpenRecordset(SQL, modExtraModules.strREDatabase);
                                    rs.MoveFirst();
                                    strDoubleSearch = "SELECT RSAccount AS Account, DeedName1 AS Name, RSMapLot AS MapLot FROM Master WHERE RSDELETED <> 1 AND (DeedName1 >= '" + txtSearch.Text + "' AND DeedName1 < '" + txtSearch.Text + "ZZZ') OR (DeedName2 >= '" + txtSearch.Text + "' AND DeedName2 < '" + txtSearch.Text + "ZZZ')) ";
                                    strDoubleSearch += "UNION ALL (SELECT Account, Name1 AS Name, MapLot FROM BillingMaster WHERE BillingType = 'RE' AND Name1 >= '" + txtSearch.Text + "' AND Name1 < '" + txtSearch.Text + "ZZZ')";
                                    strDoubleSearch = "SELECT DISTINCT Account, Name, Maplot FROM (" + strDoubleSearch + ") ORDER BY Name, RSCard asc";
                                }
                                else
                                {
                                    if (boolShowDeletedAccounts)
                                    {
                                        SQL = "SELECT pOwn.FullNameLF AS Name, Deleted, Street, StreetNumber, Account " + "FROM PPMaster INNER JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pOwn ON PPMaster.PartyID = pOwn.ID " + "WHERE ((pOwn.FullNameLF >= '" + txtSearch.Text + "' AND pOwn.FullNameLF < '" + txtSearch.Text + "ZZZ') " + strStartsWithMaster + " ) ORDER BY Name, Account";
                                    }
                                    else
                                    {
                                        SQL = "SELECT pOwn.FullNameLF AS Name, Deleted, Street, StreetNumber, Account " + "FROM PPMaster INNER JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pOwn ON PPMaster.PartyID = pOwn.ID " + "WHERE DELETED <> 1 AND ((pOwn.FullNameLF >= '" + txtSearch.Text + "' AND pOwn.FullNameLF < '" + txtSearch.Text + "ZZZ') " + strStartsWithMaster + " ) ORDER BY Name, Account";
                                    }
                                    strDoubleSearch = "(SELECT Account, p1.FullNameLF AS Name FROM PPMaster  CROSS APPLY " + rs.CurrentPrefix + "CentralParties.dbo.GetPartyNameAndAddress(PPMaster.PartyID,NULL,'PP',PPMaster.Account) AS p1 " + "WHERE DELETED <> 1 AND ((p1.FullNameLF >= '" + txtSearch.Text + "' AND p1.FullNameLF < '" + txtSearch.Text + "ZZZ') " + strStartsWithMaster + ") ";
                                    strDoubleSearch += "UNION ALL (SELECT Account, Name1 AS Name FROM BillingMaster WHERE (BillingType = 'PP' AND Name1 >= '" + txtSearch.Text + "' AND Name1 < '" + txtSearch.Text + "ZZZ') " + strStartsWithSearch + ")";
                                    strDoubleSearch = "SELECT DISTINCT Account, Name FROM (" + strDoubleSearch + ")) ORDER BY Name asc";
                                }
                                strSearchString = strDoubleSearch;
                                break;
                            }
                        case 1:
                            {
                                // Street Name
                                if (boolStartsWith)
                                {
                                    strStartsWithSearch = " OR StreetName LIKE ('%" + txtSearch.Text + "%') ";
                                }
                                else
                                {
                                    strStartsWithSearch = "";
                                }
                                if (modStatusPayments.Statics.boolRE)
                                {
                                    // kgk 04052012 SQL = "SELECT * FROM BillingMaster INNER JOIN (SELECT ID AS BK FROM BillingMaster WHERE BillingType = 'RE' AND ((StreetName >= '" & txtSearch.Text & "  ' AND StreetName < '" & txtSearch.Text & "ZZZ') " & strStartsWithSearch & ")) AS List ON BillingMaster.ID = List.BK ORDER BY StreetName,NAME1,ACCOUNT"
                                    SQL = "SELECT *, ID AS BK FROM BillingMaster WHERE BillingType = 'RE' AND ((StreetName >= '" + txtSearch.Text + "  ' AND StreetName < '" + txtSearch.Text + "ZZZ') " + strStartsWithSearch + ") ORDER BY StreetName,NAME1,ACCOUNT";
                                }
                                else
                                {
                                    // SQL = "SELECT * FROM BillingMaster INNER JOIN (SELECT ID AS BK FROM BillingMaster WHERE BillingType = 'PP' AND ((StreetName >= '" & txtSearch.Text & "  ' AND StreetName < '" & txtSearch.Text & "ZZZ') " & strStartsWithSearch & ")) AS List ON BillingMaster.ID = List.BK ORDER BY StreetName,NAME1,ACCOUNT"
                                    SQL = "SELECT *, ID AS BK FROM BillingMaster WHERE BillingType = 'PP' AND ((StreetName >= '" + txtSearch.Text + "  ' AND StreetName < '" + txtSearch.Text + "ZZZ') " + strStartsWithSearch + ") ORDER BY StreetName,NAME1,ACCOUNT";
                                }
                                // this will create a string of accounts that will be added in the grid as well as the RE matches (for old collection files with that name)
                                rsCLAccounts.OpenRecordset(SQL, modExtraModules.strCLDatabase);
                                strAcctList = ",";
                                while (!rsCLAccounts.EndOfFile())
                                {
                                    // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    strAcctList += FCConvert.ToString(rsCLAccounts.Get_Fields("Account")) + ",";
                                    rsCLAccounts.MoveNext();
                                }
                                rsCLAccounts.MoveFirst();
                                if (boolStartsWith)
                                {
                                    if (modStatusPayments.Statics.boolRE)
                                    {
                                        strStartsWithSearch = " OR RSLOCSTREET LIKE ('%" + txtSearch.Text + "%') ";
                                    }
                                    else
                                    {
                                        strStartsWithSearch = " OR STREET LIKE ('%" + txtSearch.Text + "%') ";
                                    }
                                }
                                else
                                {
                                    strStartsWithSearch = "";
                                }
                                if (modStatusPayments.Statics.boolRE)
                                {
                                    if (boolShowDeletedAccounts)
                                    {
                                        SQL =
                                            "SELECT DeedName1 AS Name, DeedName2 AS Name2, RSDeleted, RSLocStreet, RSLocNumAlph, RSAccount AS Account, RSMapLot AS Maplot FROM Master ";
                                        SQL += "Where ((RSLOCSTREET >= '" + txtSearch.Text + "' AND RSLOCSTREET < '" + txtSearch.Text + "ZZZ') " + strStartsWithSearch + ") ORDER BY RSLOCSTREET, RSLOCNUMALPH, RSAccount";
                                    }
                                    else
                                    {
                                        SQL = "SELECT DeedName1 AS Name, DeedName2 AS Name2, RSDeleted, RSLocStreet, RSLocNumAlph, RSAccount AS Account, RSMapLot AS Maplot " + "FROM Master WHERE RSDELETED <> 1 AND ((RSLOCSTREET >= '" + txtSearch.Text + "' AND RSLOCSTREET < '" + txtSearch.Text + "ZZZ') " + strStartsWithSearch + ") ORDER BY RSLOCSTREET, RSLOCNUMALPH, RSAccount";
                                    }
                                }
                                else
                                {
                                    if (boolShowDeletedAccounts)
                                    {
                                        // kgk 12072012 SQL = "SELECT * FROM PPMaster WHERE (STREET >= '" & txtSearch.Text & "' AND STREET < '" & txtSearch.Text & "ZZZ'" & strStartsWithSearch & ") ORDER BY STREET, CONVERT(int, STREETNUMBER), Account"
                                        SQL = "SELECT pOwn.FullNameLF AS Name, Deleted, Street, StreetNumber, Account " + "FROM PPMaster INNER JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pOwn ON PPMaster.PartyID = pOwn.ID " + "WHERE (Street >= '" + txtSearch.Text + "' AND Street < '" + txtSearch.Text + "ZZZ'" + strStartsWithSearch + ") ORDER BY Street, CONVERT(int, StreetNumber), Account";
                                    }
                                    else
                                    {
                                        // kgk 12072012 SQL = "SELECT * FROM PPMaster WHERE DELETED <> 1 AND (STREET >= '" & txtSearch.Text & "' AND STREET < '" & txtSearch.Text & "ZZZ'" & strStartsWithSearch & ") ORDER BY STREET, CONVERT(int, STREETNUMBER), Account"
                                        SQL = "SELECT pOwn.FullNameLF AS Name, Deleted, Street, StreetNumber, Account " + "FROM PPMaster INNER JOIN " + modGlobal.Statics.strDbCP + "PartyNameView pOwn ON PPMaster.PartyID = pOwn.ID " + "WHERE DELETED <> 1 AND (Street >= '" + txtSearch.Text + "' AND Street < '" + txtSearch.Text + "ZZZ'" + strStartsWithSearch + ") ORDER BY Street, CONVERT(int, StreetNumber), Account";
                                    }
                                    // kgk 12072012 rs.OpenRecordset SQL, strPPDatabase
                                    // rs.InsertName "PartyID", "Own1", False, True, False, "RE", False, "", True, ""
                                    // rs.MoveFirst
                                }
                                strSearchString = SQL;
                                break;
                            }
                        case 2:
                            {
                                // Map Lot
                                if (boolStartsWith)
                                {
                                    strStartsWithSearch = " OR MapLot LIKE ('%" + txtSearch.Text + "%') ";
                                }
                                else
                                {
                                    strStartsWithSearch = "";
                                }
                                // kgk 04052012 SQL = "SELECT * FROM BillingMaster INNER JOIN (SELECT ID AS BK FROM BillingMaster WHERE BillingType = 'RE' AND ((MapLot >= '" & txtSearch.Text & "  ' AND MapLot < '" & txtSearch.Text & "ZZZ')" & strStartsWithSearch & ")) AS List ON BillingMaster.ID = List.BK ORDER BY MapLot,NAME1,ACCOUNT"
                                SQL = "SELECT *, ID AS BK FROM BillingMaster WHERE BillingType = 'RE' AND ((MapLot >= '" + txtSearch.Text + "  ' AND MapLot < '" + txtSearch.Text + "ZZZ')" + strStartsWithSearch + ") ORDER BY MapLot,NAME1,ACCOUNT";
                                // this will create a string of accounts that will be added in the grid as well as the RE matches (for old collection files with that name)
                                rsCLAccounts.OpenRecordset(SQL, modExtraModules.strCLDatabase);
                                strAcctList = ",";
                                while (!rsCLAccounts.EndOfFile())
                                {
                                    // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    strAcctList += FCConvert.ToString(rsCLAccounts.Get_Fields("Account")) + ",";
                                    rsCLAccounts.MoveNext();
                                }
                                rsCLAccounts.MoveFirst();
                                if (modStatusPayments.Statics.boolRE)
                                {
                                    if (boolStartsWith)
                                    {
                                        strStartsWithSearch = " OR RSMapLot LIKE ('%" + txtSearch.Text + "%') ";
                                    }
                                    else
                                    {
                                        strStartsWithSearch = "";
                                    }
                                    if (boolShowDeletedAccounts)
                                    {
                                        SQL = "SELECT DeedName1 AS Name, DeedName2 AS Name2, RSDeleted, RSLocStreet, RSLocNumAlph, RSAccount AS Account, RSMapLot AS MapLot " + "FROM Master WHERE ((RSMapLot >= '" + txtSearch.Text + "' AND RSMapLot < '" + txtSearch.Text + "ZZZ')" + strStartsWithSearch + ") ORDER BY RSMapLot, RSAccount, RSCard";
                                    }
                                    else
                                    {
                                        SQL = "SELECT DeedName1 AS Name, DeedName2 AS Name2, RSDeleted, RSLocStreet, RSLocNumAlph, RSAccount AS Account, RSMapLot AS MapLot " + "FROM Master WHERE RSDELETED <> 1 AND ((RSMapLot >= '" + txtSearch.Text + "' AND RSMapLot < '" + txtSearch.Text + "ZZZ')" + strStartsWithSearch + ") ORDER BY RSMapLot, RSAccount, RSCard";
                                    }
                                }
                                else
                                {
                                    // SQL = "SELECT * FROM Master WHERE (RSMAPLOT >= '" & txtSearch.Text & "' AND RSMAPLOT < '" & txtSearch.Text & "ZZZ') ORDER BY RSMAPLOT, Account"
                                    return;
                                }
                                strSearchString = SQL;
                                break;
                            }
                    }
                    //end switch
                    if (modStatusPayments.Statics.boolRE)
                    {
                        // RE
                        rs.OpenRecordset(SQL, modExtraModules.strREDatabase);
                        if (!rs.EndOfFile() || (lngSelection == 0 && rsCLAccounts.RecordCount() > 0))
                        {
                            frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Searching", true, rs.RecordCount(), true);
                            this.Refresh();
                            // clear the listbox
                            vsSearch.Rows = 1;
                            FormatSearchGrid();
                            rsBook.OpenRecordset("SELECT * FROM BookPage WHERE [Current] = 1 ORDER BY Line desc", modExtraModules.strREDatabase);
                            // fill the listbox with all the records returned
                            while (!rs.EndOfFile())
                            {
                                frmWait.InstancePtr.IncrementProgress();
                                App.DoEvents();
                                // get any book page info from
                                // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                rsBook.FindFirstRecord2("Account,Card", FCConvert.ToString(rs.Get_Fields("Account")) + ",1", ",");
                                if (rsBook.NoMatch)
                                {
                                    strBook = "";
                                }
                                else
                                {
                                    strBook = FCConvert.ToString(rsBook.Get_Fields("Book")) + " " + FCConvert.ToString(rsBook.Get_Fields("Page"));
                                }
                                if (lngSelection == 0)
                                {
                                    // Search by Name    'is this from CL and RE
                                    // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    if (Conversion.Val(rs.Get_Fields("Account")) != Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct), 6)))
                                    {
                                        vsSearch.AddItem("");
                                        // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rs.Get_Fields("Account")));
                                        // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                        rsBillRecords.FindFirstRecord("Account", rs.Get_Fields("Account"));
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBillRecords, rsBillRecords.NoMatch ? FCConvert.ToString(0) : FCConvert.ToString(1));
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, strBook);
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rs.Get_Fields_String("RSLOCSTREET")));
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("RSLOCNUMALPH"))));
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("MAPLOT"))));
                                        strOwnerName = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Name")));
                                        strSecOwnerName = "";
                                        if (Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Name2"))) != "")
                                        {
                                            strSecOwnerName = Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("Name2")));
                                        }
                                        if ((Strings.CompareString(Strings.UCase(strOwnerName), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(strOwnerName), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
                                        {
                                            if (Strings.Trim(strSecOwnerName) != "")
                                            {
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, strOwnerName + " & " + strSecOwnerName);
                                            }
                                            else
                                            {
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, strOwnerName);
                                            }
                                        }
                                        else
                                        {
                                            if (strSecOwnerName == "")
                                            {
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, strOwnerName);
                                            }
                                            else
                                            {
                                                if (Strings.Trim(strOwnerName) != "")
                                                {
                                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, strSecOwnerName + ", " + strOwnerName);
                                                }
                                                else
                                                {
                                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, strSecOwnerName);
                                                }
                                            }
                                        }

                                        // vsSearch.TextMatrix(vsSearch.rows - 1, lngColName) = .Get_Fields("Name") & " " & .Get_Fields("RSSECOWNER")
                                    }
                                }
                                else
                                {
                                    // Search by Street or MapLot         'data from RE
                                    if (Conversion.Val(rs.Get_Fields("Account")) != Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct), 6)))
                                    {
                                        // kgk 04052012 Lookup Central Party
                                        strOwnerName = FCConvert.ToString(rs.Get_Fields_String("Name"));
                                        strSecOwnerName = FCConvert.ToString(rs.Get_Fields_String("Name2"));

                                        strList = FCConvert.ToString(rs.Get_Fields("Account")) + "\t" + strOwnerName + "\t" + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("RSLOCNUMALPH"))) + "\t" + FCConvert.ToString(rs.Get_Fields_String("RSLOCSTREET")) + "\t" + strBook + "\t" + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("MAPLOT")));
                                        vsSearch.AddItem("");
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rs.Get_Fields("Account")));
                                        rsBillRecords.FindFirstRecord("Account", rs.Get_Fields("Account"));
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBillRecords, rsBillRecords.NoMatch ? FCConvert.ToString(0) : FCConvert.ToString(1));
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, strBook);
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rs.Get_Fields_String("RSLOCSTREET")));
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("RSLOCNUMALPH"))));
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("MAPLOT"))));
                                        // kgk 04052012 We're in the lngSelection <> 0 part of the outer If statement
                                        System.Diagnostics.Debug.Assert(lngSelection != 0);

                                        if (Strings.Trim(strSecOwnerName) != "")
                                        {
                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, strOwnerName + " & " + strSecOwnerName);
                                        }
                                        else
                                        {
                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, strOwnerName);
                                        }

                                    }
                                }
                                rs.MoveNext();
                            }
                            // now check all of the billing records to see if any did not match the master record,
                            // if they did not, then add a row for that bill as well
                            if (boolShowPrevOwner)
                            {
                                if (!rsCLAccounts.EndOfFile())
                                {
                                    rsCLAccounts.MoveFirst();
                                    while (!rsCLAccounts.EndOfFile())
                                    {
                                        App.DoEvents();
                                        rs.FindFirstRecord("Account", rsCLAccounts.Get_Fields("Account"));
                                        if (!rs.NoMatch)
                                        {
                                            if (rs.Get_Fields_Boolean("RSDeleted") == false || (rs.Get_Fields_Boolean("RSDeleted") == true && boolShowDeletedAccounts == true))
                                            {
                                                // kgk 04052012 Lookup Central Party
                                                strOwnerName = FCConvert.ToString(rs.Get_Fields_String("Name"));
                                                strSecOwnerName = FCConvert.ToString(rs.Get_Fields_String("Name2"));

                                                // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                                if (Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) == Strings.Trim(strOwnerName) || (Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) == Strings.Trim(Strings.UCase(strLastName)) && ((rsCLAccounts.Get_Fields("Account"))) == lngLastAccount))
                                                {

                                                }
                                                else
                                                {
                                                    // add the row
                                                    vsSearch.AddItem("");
                                                    // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rsCLAccounts.Get_Fields("Account")));
                                                    // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                                    rsBillRecords.FindFirstRecord("Account", rsCLAccounts.Get_Fields("Account"));
                                                    if (rsBillRecords.NoMatch)
                                                    {
                                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBillRecords, FCConvert.ToString(0));
                                                    }
                                                    else
                                                    {
                                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBillRecords, FCConvert.ToString(1));
                                                    }
                                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, FCConvert.ToString(rsCLAccounts.Get_Fields_String("BookPage")));
                                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rsCLAccounts.Get_Fields_String("StreetName")));
                                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, FCConvert.ToString(rsCLAccounts.Get_Fields_Int32("StreetNumber")));
                                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(FCConvert.ToString(rsCLAccounts.Get_Fields_String("MapLot"))));
                                                    if (lngSelection == 0)
                                                    {
                                                        if ((Strings.CompareString(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
                                                        {
                                                            if (Strings.Trim(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")) + " ") != "")
                                                            {
                                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " & " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));
                                                            }
                                                            else
                                                            {
                                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")));
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (rsCLAccounts.Get_Fields_String("Name2") == "")
                                                            {
                                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")));
                                                            }
                                                            else
                                                            {
                                                                if (Strings.Trim(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " ") != "")
                                                                {
                                                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")) + ", " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")));
                                                                }
                                                                else
                                                                {
                                                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (Strings.Trim(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")) + " ") != "")
                                                        {
                                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " & " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));
                                                        }
                                                        else
                                                        {
                                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")));
                                                        }
                                                    }
                                                    // highlight the added rows that are only past owners
                                                    vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
                                                    strLastName = FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"));
                                                    // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                                    lngLastAccount = FCConvert.ToInt32(rsCLAccounts.Get_Fields("Account"));
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) != Strings.Trim(Strings.UCase(strLastName)) || ((rsCLAccounts.Get_Fields("Account"))) != lngLastAccount)
                                            {
                                                // add the row
                                                vsSearch.AddItem("");
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rsCLAccounts.Get_Fields("Account")));
                                                rsBillRecords.FindFirstRecord("Account", rsCLAccounts.Get_Fields("Account"));
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBillRecords, rsBillRecords.NoMatch ? FCConvert.ToString(0) : FCConvert.ToString(1));
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, FCConvert.ToString(rsCLAccounts.Get_Fields_String("BookPage")));
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rsCLAccounts.Get_Fields_String("StreetName")));
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, FCConvert.ToString(rsCLAccounts.Get_Fields_Int32("StreetNumber")));
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(FCConvert.ToString(rsCLAccounts.Get_Fields_String("MapLot"))));
                                                if (lngSelection == 0)
                                                {
                                                    if ((Strings.CompareString(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
                                                    {
                                                        if (Strings.Trim(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")) + " ") != "")
                                                        {
                                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " & " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));
                                                        }
                                                        else
                                                        {
                                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (Strings.Trim(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " ") != "")
                                                        {
                                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")) + ", " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")));
                                                        }
                                                        else
                                                        {
                                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (Strings.Trim(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")) + " ") != "")
                                                    {
                                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " & " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));
                                                    }
                                                    else
                                                    {
                                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")));
                                                    }
                                                }
                                                // highlight the added rows that are only past owners
                                                vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
                                                strLastName = FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"));
                                                lngLastAccount = FCConvert.ToInt32(rsCLAccounts.Get_Fields("Account"));
                                            }
                                        }

                                        if (Strings.Left(Strings.Trim(vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName)), 1) == ",")
                                        {
                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, Strings.Trim(Strings.Right(vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName), vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName).Length - 1)));
                                        }
                                        rsCLAccounts.MoveNext();
                                    }
                                }
                            }
                           
                            vsSearch.Visible = true;
                            
                            lblSearchListInstruction.Visible = true;
                            boolResultsScreen = true;
                            //btnProcess.Enabled = false;
                            mnuProcessSearch.Enabled = false;
                            if (lngSelection == 0)
                            {
                                // force the name order
                                vsSearch.Col = lngColName;
                                vsSearch.Sort = FCGrid.SortSettings.flexSortGenericAscending;
                            }
                            frmWait.InstancePtr.Unload();
                            if (vsSearch.Rows == 2)
                            {
                                txtGetAccountNumber.Text = vsSearch.TextMatrix(1, lngColAcct);
                                cmdGetAccountNumber_Click();
                            }
                        }
                        else
                        {
                            frmWait.InstancePtr.Unload();
                            FCMessageBox.Show("No results were matched to the criteria.  Please try again.", MsgBoxStyle.Information, "No RE Results");
                            fraSearch.Visible = true;
                        }
                    }
                    else
                    {
                        // PP
                        FormatSearchGrid();
                        rs.OpenRecordset(SQL, modExtraModules.strPPDatabase);
                        if ((rs.EndOfFile() != true && rs.BeginningOfFile() != true) || boolShowPrevOwner)
                        {
                            if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                            {
                                frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Searching", true, rs.RecordCount(), true);
                                this.Refresh();
                                rs.MoveLast();
                                rs.MoveFirst();
                                // clear the listbox
                                vsSearch.Rows = 1;
                                // fill the listbox with all the records returned
                                while (!rs.EndOfFile())
                                {
                                    frmWait.InstancePtr.IncrementProgress();
                                    App.DoEvents();
                                    if (Conversion.Val(rs.Get_Fields("Account")) != Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct), 6)))
                                    {
                                        vsSearch.AddItem("");
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rs.Get_Fields("Account")));
                                        rsBillRecords.FindFirstRecord("Account", rs.Get_Fields("Account"));
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBillRecords, rsBillRecords.NoMatch ? FCConvert.ToString(0) : FCConvert.ToString(1));
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, "");
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rs.Get_Fields_String("Street")));
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, FCConvert.ToString(rs.Get_Fields_Int32("StreetNumber")));
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, "");
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rs.Get_Fields_String("Name")));
                                    }
                                    // End If
                                    rs.MoveNext();
                                }
                            }
                            if (boolShowPrevOwner)
                            {
                                // now check all of the billing records to see if any did not match the master record,
                                // if they did not, then add a row for that bill as well
                                rsCLAccounts.MoveFirst();
                                while (!rsCLAccounts.EndOfFile())
                                {
                                    App.DoEvents();
                                    // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    rs.FindFirstRecord("Account", rsCLAccounts.Get_Fields("Account"));
                                    if (!rs.NoMatch)
                                    {
                                        if (rs.Get_Fields_Boolean("Deleted") == false || (rs.Get_Fields_Boolean("Deleted") == true && boolShowDeletedAccounts == true))
                                        {
                                            if (Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) == Strings.Trim(Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("Name")))) || Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) == Strings.Trim(Strings.UCase(strLastName)))
                                            {
                                                // if this bill has the same name as its master account or has the
                                                // same name as the last bill then do nothing
                                            }
                                            else
                                            {
                                                // add the row
                                                vsSearch.AddItem("");
                                                // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rsCLAccounts.Get_Fields("Account")));
                                                // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                                rsBillRecords.FindFirstRecord("Account", rsCLAccounts.Get_Fields("Account"));
                                                if (rsBillRecords.NoMatch)
                                                {
                                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBillRecords, FCConvert.ToString(0));
                                                }
                                                else
                                                {
                                                    vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBillRecords, FCConvert.ToString(1));
                                                }
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, FCConvert.ToString(rsCLAccounts.Get_Fields_String("BookPage")));
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rsCLAccounts.Get_Fields_String("StreetName")));
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, FCConvert.ToString(rsCLAccounts.Get_Fields_Int32("StreetNumber")));
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(FCConvert.ToString(rsCLAccounts.Get_Fields_String("MapLot"))));
                                                vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));
                                                // highlight the added rows that are only past owners
                                                vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
                                            }
                                        }
                                    }
                                    // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    else if ((Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")))) != Strings.Trim(Strings.UCase(strLastName)) || Strings.Trim(Strings.UCase(strLastSecName)) != Strings.Trim(Strings.UCase(FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2"))))) || lngLastAccount != ((rsCLAccounts.Get_Fields("Account"))))
                                    {
                                        // add the row
                                        vsSearch.AddItem("");
                                        // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rsCLAccounts.Get_Fields("Account")));
                                        // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                        rsBillRecords.FindFirstRecord("Account", rsCLAccounts.Get_Fields("Account"));
                                        if (rsBillRecords.NoMatch)
                                        {
                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBillRecords, FCConvert.ToString(0));
                                        }
                                        else
                                        {
                                            vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBillRecords, FCConvert.ToString(1));
                                        }
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, "");
                                        // rsCLAccounts.Get_Fields("BookPage")
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rsCLAccounts.Get_Fields_String("StreetName")));
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngcolLocationNumber, FCConvert.ToString(rsCLAccounts.Get_Fields_Int32("StreetNumber")));
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, "");
                                        // Trim(rsCLAccounts.Get_Fields("MapLot"))
                                        vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1")) + " " + FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2")));
                                        // highlight the added rows that are only past owners
                                        vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
                                    }
                                    strLastName = FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name1"));
                                    strLastSecName = FCConvert.ToString(rsCLAccounts.Get_Fields_String("Name2"));
                                    // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    lngLastAccount = FCConvert.ToInt32(rsCLAccounts.Get_Fields("Account"));
                                    rsCLAccounts.MoveNext();
                                }
                            }
                            //FC:FINAL:AM: don't set the height; used anchoring instead
                            // set the height of the grid
                            //if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.Height - vsSearch.Top) - 1000)
                            //{
                            //	vsSearch.Height = (vsSearch.Rows * vsSearch.RowHeight(0)) + 70;
                            //	vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
                            //}
                            //else
                            //{
                            //	vsSearch.Height = (this.Height - vsSearch.Top) - 1000;
                            //	vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
                            //}
                            vsSearch.Visible = true;
                            lblSearchListInstruction.Visible = true;
                            boolResultsScreen = true;
                            btnProcess.Enabled = false;
                            mnuProcessSearch.Enabled = false;
                            //fraSearch.Visible = false;
                            frmWait.InstancePtr.Unload();
                        }
                        else
                        {
                            frmWait.InstancePtr.Unload();
                            FCMessageBox.Show("No results were matched to the criteria.  Please try again.", MsgBoxStyle.Information, "No PP Results");
                            ShowSearch();
                        }
                    }
                    FillSearchList();
                }
                finally
                {
                    rs.DisposeOf();
                    rsCLAccounts.DisposeOf();
                    rsBook.DisposeOf();
                    rsBillRecords.DisposeOf();

                }
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show("ERROR #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Open Recordset ERROR");
                Close();
            }
        }

        public void cmdSearch_Click()
        {
            cmdSearch_Click(cmdSearch, new System.EventArgs());
        }

        private void cmdValidateCancel_Click(object sender, System.EventArgs e)
        {
            LockBatch_2(false);
            // this will enable the boxes again
        }

        private void cmdValidateNo_Click(object sender, System.EventArgs e)
        {
            LockBatch_2(false);
            // this will enable the boxes again
        }

        private void cmdValidateYes_Click(object sender, System.EventArgs e)
        {
            LockBatch_2(false);
            // this will enable the boxes again
            ShowBatch();
            fraBatchQuestions.Visible = false;
            mnuFileImport.Enabled = true;
            mnuImportFirstAmericanBatch.Enabled = true;
        }

        public void cmdValidateYes_Click()
        {
            cmdValidateYes_Click(cmdValidateYes, new System.EventArgs());
        }

        private void frmCLGetAccount_Activated(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ErrorTag
                // If FormExist(Me) Then Exit Sub
                if (boolPassActivate)
                {
                    boolPassActivate = false;
                    return;
                }
                isTxtGetAccountNeedFocus = false;
                selectionStart = 0;
                selectionLength = 0;
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                lblInstructions.Text = "To view the outstanding balance, enter an account number. If that balance is the amount being paid, press Enter to continue. When you have finished, click Process Batch.";
                
                if (!boolLoaded)
                {
                    if (modStatusPayments.Statics.boolRE)
                    {
                        // set the menu options on for RE
                        mnuBatchPurge.Enabled = true;
                        mnuBatchRecover.Enabled = true;
                        mnuBatchSave.Enabled = false;
                        mnuBatchStart.Enabled = true;
                        if (modExtraModules.IsThisCR())
                        {
                            if (modGlobal.Statics.gboolShowLastCLAccountInCR)
                            {
                                txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE);
                            }
                            mnuBatch.Visible = true;
                        }
                        else
                        {
                            if (modGlobalConstants.Statics.gboolCR)
                            {
                                mnuBatch.Visible = false;
                                //FC:FINAL:AM:#223 - disable the menu
                                this.Menu = null;
                            }
                            else
                            {
                                mnuBatch.Visible = true;
                            }
                            txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE);
                        }
                        cmbRE.SelectedIndex = 0;
                    }
                    else
                    {
                        this.Menu = null;
                        // turn off the menu options for PP
                        mnuBatch.Visible = false;
                        if (modExtraModules.IsThisCR())
                        {
                            if (modGlobal.Statics.gboolShowLastCLAccountInCR)
                            {
                                txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP);
                            }
                        }
                        else
                        {
                            txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP);
                        }
                        cmbRE.SelectedIndex = 1;
                    }
                    if (!vsSearch.Visible)
                    {
                        ShowSearch();
                    }
                    if (txtGetAccountNumber.Enabled && txtGetAccountNumber.Visible)
                    {
                        txtGetAccountNumber.SelectionStart = selectionStart = 0;
                        txtGetAccountNumber.SelectionLength = selectionLength = txtGetAccountNumber.Text.Length;
                        txtGetAccountNumber.Focus();
                        //FC:FINAL:MSH - issue #882: mark that after showing form will need set focus to txtGetAccountNumber
                        isTxtGetAccountNeedFocus = true;
                    }
                    boolLoaded = true;
                    //FC:FINAL:SBE - remove wrong fix. Issue #880 works without this code
                    //// FC:FINAL:VGE - #880 Hiding menu bar (tab's menu remains intact)
                    //if (this.Menu != null)
                    //	this.Menu.Hide();
                }
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                // Doevents
                if (modGlobal.IsFormLoaded_2("frmRECLStatus"))
                {
                    frmRECLStatus.InstancePtr.boolUnloadOK = true;
                    //FC:FINAL:KS: #227: two forms overlap
                    //frmRECLStatus.InstancePtr.Hide();
                    frmRECLStatus.InstancePtr.Close();
                }
                else
                {
                    if (txtGetAccountNumber.Enabled && txtGetAccountNumber.Visible)
                    {
                        txtGetAccountNumber.Focus();
                        //FC:FINAL:MSH - issue #882: mark that after showing form will need set focus to txtGetAccountNumber
                        isTxtGetAccountNeedFocus = true;
                    }
                }
                //FC:FINAL:MSH - issue #882: assing handler to set focus to txtGetAccountNumber after showing form, 
                //because Focus() doesn't set focus during initialization
                this.Appear += FrmCLGetAccount_Appear;
                return;
            }
            catch (Exception ex)
            {
                // ErrorTag:
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Activating Get Account Screen");
            }
        }

        private void FrmCLGetAccount_Appear(object sender, EventArgs e)
        {
            //FC:FINAL:MSH - issue #882: set focus to txtGetAccountNumber if necessary
            if (isTxtGetAccountNeedFocus)
            {
                //FC:FINAL:MSH - issue #882: only in this order text selection will be set correctly
                txtGetAccountNumber.Text = txtGetAccountNumber.Text;
                txtGetAccountNumber.SelectionStart = selectionStart;
                txtGetAccountNumber.SelectionLength = selectionLength;
                txtGetAccountNumber.Focus();
                isTxtGetAccountNeedFocus = false;
            }
        }

        private void frmCLGetAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            // Case vbKeyF9    'save payment
            if (KeyCode == Keys.F10)
            {
                // process batch
                KeyCode = 0;
                // ProcessBatch
            }
        }

        private void frmCLGetAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            if (KeyAscii == 27)
            {
                if (vsSearch.Visible == true)
                {
                    vsSearch.Visible = false;
                    lblSearchListInstruction.Visible = false;
                    fraSearch.Visible = true;
                    lblInstructions.Visible = true;
                    boolSearch = false;
                }
                else if (fraBatchSearchResults.Visible)
                {
                    fraBatchSearchResults.Visible = false;
                    frmCLGetAccount.InstancePtr.fraBatch.Enabled = true;
                }
                else if (fraBatchSearchCriteria.Visible)
                {
                    fraBatchSearchCriteria.Visible = false;
                    frmCLGetAccount.InstancePtr.fraBatch.Enabled = true;
                }
                else
                {
                    KeyAscii = 0;
                    cmdQuit_Click();
                }
            }
            else if (KeyAscii == 13)
            {
                KeyAscii = 0;
            }
            e.KeyChar = Strings.Chr(KeyAscii);
        }

        private void frmCLGetAccount_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmCLGetAccount.Icon	= "frmCLGetAccount.frx":0000";
            //frmCLGetAccount.ScaleWidth	= 9045;
            //frmCLGetAccount.ScaleHeight	= 7455;
            //frmCLGetAccount.LinkTopic	= "Form1";
            //Font.Size	= "8.25";
            //Font.Name	= "Tahoma";
            //Font.Weight	= 400;
            //Font.Italic	= 0;
            //Font.Underline	= 0;
            //Font.Strikethrough	= 0;
            //Font.Charset	= 0;
            //vsBatchSearch.BackColor	= "-2147483643";
            //			//vsBatchSearch.ForeColor	= "-2147483640";
            //vsBatchSearch.BorderStyle	= 1;
            //vsBatchSearch.FillStyle	= 0;
            //vsBatchSearch.Appearance	= 1;
            //vsBatchSearch.GridLines	= 1;
            //vsBatchSearch.WordWrap	= 0;
            //vsBatchSearch.ScrollBars	= 0;
            //vsBatchSearch.RightToLeft	= 0;
            //vsBatchSearch._cx	= 11986;
            //vsBatchSearch._cy	= 5558;
            //vsBatchSearch._ConvInfo	= 1;
            //vsBatchSearch.MousePointer	= 0;
            //vsBatchSearch.BackColorFixed	= -2147483633;
            //			//vsBatchSearch.ForeColorFixed	= -2147483630;
            //vsBatchSearch.BackColorSel	= -2147483635;
            //			//vsBatchSearch.ForeColorSel	= -2147483634;
            //vsBatchSearch.BackColorBkg	= -2147483636;
            //vsBatchSearch.BackColorAlternate	= -2147483643;
            //vsBatchSearch.GridColor	= -2147483633;
            //vsBatchSearch.GridColorFixed	= -2147483632;
            //vsBatchSearch.TreeColor	= -2147483632;
            //vsBatchSearch.FloodColor	= 192;
            //vsBatchSearch.SheetBorder	= -2147483642;
            //vsBatchSearch.FocusRect	= 1;
            //vsBatchSearch.HighLight	= 2;
            //vsBatchSearch.AllowSelection	= -1  'True;
            //vsBatchSearch.AllowBigSelection	= -1  'True;
            //vsBatchSearch.AllowUserResizing	= 0;
            //vsBatchSearch.SelectionMode	= 1;
            //vsBatchSearch.GridLinesFixed	= 2;
            //vsBatchSearch.GridLineWidth	= 1;
            //vsBatchSearch.RowHeightMin	= 0;
            //vsBatchSearch.RowHeightMax	= 0;
            //vsBatchSearch.ColWidthMin	= 0;
            //vsBatchSearch.ColWidthMax	= 0;
            //vsBatchSearch.ExtendLastCol	= -1  'True;
            //vsBatchSearch.FormatString	= "";
            //vsBatchSearch.ScrollTrack	= -1  'True;
            //vsBatchSearch.ScrollTips	= 0   'False;
            //vsBatchSearch.MergeCells	= 0;
            //vsBatchSearch.MergeCompare	= 0;
            //vsBatchSearch.AutoResize	= -1  'True;
            //vsBatchSearch.AutoSizeMode	= 0;
            //vsBatchSearch.AutoSearch	= 0;
            //vsBatchSearch.AutoSearchDelay	= 2;
            //vsBatchSearch.MultiTotals	= -1  'True;
            //vsBatchSearch.SubtotalPosition	= 1;
            //vsBatchSearch.OutlineBar	= 0;
            //vsBatchSearch.OutlineCol	= 0;
            //vsBatchSearch.Ellipsis	= 0;
            //vsBatchSearch.ExplorerBar	= 1;
            //vsBatchSearch.PicturesOver	= 0   'False;
            //vsBatchSearch.PictureType	= 0;
            //vsBatchSearch.TabBehavior	= 0;
            //vsBatchSearch.OwnerDraw	= 0;
            //vsBatchSearch.ShowComboButton	= -1  'True;
            //vsBatchSearch.TextStyle	= 0;
            //vsBatchSearch.TextStyleFixed	= 0;
            //vsBatchSearch.OleDragMode	= 0;
            //vsBatchSearch.OleDropMode	= 0;
            //vsBatchSearch.ComboSearch	= 3;
            //vsBatchSearch.AutoSizeMouse	= -1  'True;
            //vsBatchSearch.AllowUserFreezing	= 0;
            //vsBatchSearch.BackColorFrozen	= 0;
            //			//vsBatchSearch.ForeColorFrozen	= 0;
            //vsBatchSearch.WallPaperAlignment	= 9;
            //vsSearch.BackColor	= "-2147483643";
            //			//vsSearch.ForeColor	= "-2147483640";
            //vsSearch.BorderStyle	= 1;
            //vsSearch.FillStyle	= 0;
            //vsSearch.Appearance	= 1;
            //vsSearch.GridLines	= 1;
            //vsSearch.WordWrap	= 0;
            //vsSearch.ScrollBars	= 2;
            //vsSearch.RightToLeft	= 0;
            //vsSearch._cx	= 2990;
            //vsSearch._cy	= 1466;
            //vsSearch._ConvInfo	= 1;
            //vsSearch.MousePointer	= 0;
            //vsSearch.BackColorFixed	= -2147483633;
            //			//vsSearch.ForeColorFixed	= -2147483630;
            //vsSearch.BackColorSel	= -2147483635;
            //			//vsSearch.ForeColorSel	= -2147483634;
            //vsSearch.BackColorBkg	= -2147483636;
            //vsSearch.BackColorAlternate	= -2147483643;
            //vsSearch.GridColor	= -2147483633;
            //vsSearch.GridColorFixed	= -2147483632;
            //vsSearch.TreeColor	= -2147483632;
            //vsSearch.FloodColor	= 192;
            //vsSearch.SheetBorder	= -2147483642;
            //vsSearch.FocusRect	= 1;
            //vsSearch.HighLight	= 1;
            //vsSearch.AllowSelection	= -1  'True;
            //vsSearch.AllowBigSelection	= -1  'True;
            //vsSearch.AllowUserResizing	= 1;
            //vsSearch.SelectionMode	= 0;
            //vsSearch.GridLinesFixed	= 2;
            //vsSearch.GridLineWidth	= 1;
            //vsSearch.RowHeightMin	= 0;
            //vsSearch.RowHeightMax	= 0;
            //vsSearch.ColWidthMin	= 0;
            //vsSearch.ColWidthMax	= 0;
            //vsSearch.ExtendLastCol	= 0   'False;
            //vsSearch.FormatString	= "";
            //vsSearch.ScrollTrack	= -1  'True;
            //vsSearch.ScrollTips	= 0   'False;
            //vsSearch.MergeCells	= 0;
            //vsSearch.MergeCompare	= 0;
            //vsSearch.AutoResize	= -1  'True;
            //vsSearch.AutoSizeMode	= 0;
            //vsSearch.AutoSearch	= 0;
            //vsSearch.AutoSearchDelay	= 2;
            //vsSearch.MultiTotals	= -1  'True;
            //vsSearch.SubtotalPosition	= 1;
            //vsSearch.OutlineBar	= 0;
            //vsSearch.OutlineCol	= 0;
            //vsSearch.Ellipsis	= 0;
            //vsSearch.ExplorerBar	= 1;
            //vsSearch.PicturesOver	= 0   'False;
            //vsSearch.PictureType	= 0;
            //vsSearch.TabBehavior	= 0;
            //vsSearch.OwnerDraw	= 0;
            //vsSearch.ShowComboButton	= -1  'True;
            //vsSearch.TextStyle	= 0;
            //vsSearch.TextStyleFixed	= 0;
            //vsSearch.OleDragMode	= 0;
            //vsSearch.OleDropMode	= 0;
            //vsSearch.ComboSearch	= 3;
            //vsSearch.AutoSizeMouse	= -1  'True;
            //vsSearch.AllowUserFreezing	= 0;
            //vsSearch.BackColorFrozen	= 0;
            //			//vsSearch.ForeColorFrozen	= 0;
            //vsSearch.WallPaperAlignment	= 9;
            //vsBatch.BackColor	= "-2147483643";
            //			//vsBatch.ForeColor	= "-2147483640";
            //vsBatch.BorderStyle	= 0;
            //vsBatch.FillStyle	= 0;
            //vsBatch.Appearance	= 0;
            //vsBatch.GridLines	= 1;
            //vsBatch.WordWrap	= 0;
            //vsBatch.ScrollBars	= 2;
            //vsBatch.RightToLeft	= 0;
            //vsBatch._cx	= 15319;
            //vsBatch._cy	= 8655;
            //vsBatch._ConvInfo	= 1;
            //vsBatch.MousePointer	= 0;
            //vsBatch.BackColorFixed	= -2147483633;
            //			//vsBatch.ForeColorFixed	= -2147483630;
            //vsBatch.BackColorSel	= -2147483635;
            //			//vsBatch.ForeColorSel	= -2147483634;
            //vsBatch.BackColorBkg	= -2147483633;
            //vsBatch.BackColorAlternate	= -2147483643;
            //vsBatch.GridColor	= -2147483633;
            //vsBatch.GridColorFixed	= -2147483632;
            //vsBatch.TreeColor	= -2147483632;
            //vsBatch.FloodColor	= 192;
            //vsBatch.SheetBorder	= -2147483642;
            //vsBatch.FocusRect	= 1;
            //vsBatch.HighLight	= 1;
            //vsBatch.AllowSelection	= -1  'True;
            //vsBatch.AllowBigSelection	= -1  'True;
            //vsBatch.AllowUserResizing	= 0;
            //vsBatch.SelectionMode	= 0;
            //vsBatch.GridLinesFixed	= 2;
            //vsBatch.GridLineWidth	= 1;
            //vsBatch.RowHeightMin	= 0;
            //vsBatch.RowHeightMax	= 0;
            //vsBatch.ColWidthMin	= 0;
            //vsBatch.ColWidthMax	= 0;
            //vsBatch.ExtendLastCol	= 0   'False;
            //vsBatch.FormatString	= "";
            //vsBatch.ScrollTrack	= -1  'True;
            //vsBatch.ScrollTips	= 0   'False;
            //vsBatch.MergeCells	= 0;
            //vsBatch.MergeCompare	= 0;
            //vsBatch.AutoResize	= -1  'True;
            //vsBatch.AutoSizeMode	= 0;
            //vsBatch.AutoSearch	= 0;
            //vsBatch.AutoSearchDelay	= 2;
            //vsBatch.MultiTotals	= -1  'True;
            //vsBatch.SubtotalPosition	= 1;
            //vsBatch.OutlineBar	= 0;
            //vsBatch.OutlineCol	= 0;
            //vsBatch.Ellipsis	= 0;
            //vsBatch.ExplorerBar	= 0;
            //vsBatch.PicturesOver	= 0   'False;
            //vsBatch.PictureType	= 0;
            //vsBatch.TabBehavior	= 0;
            //vsBatch.OwnerDraw	= 0;
            //vsBatch.ShowComboButton	= -1  'True;
            //vsBatch.TextStyle	= 0;
            //vsBatch.TextStyleFixed	= 0;
            //vsBatch.OleDragMode	= 0;
            //vsBatch.OleDropMode	= 0;
            //vsBatch.ComboSearch	= 3;
            //vsBatch.AutoSizeMouse	= -1  'True;
            //vsBatch.AllowUserFreezing	= 0;
            //vsBatch.BackColorFrozen	= 15;
            //			//vsBatch.ForeColorFrozen	= 0;
            //vsBatch.WallPaperAlignment	= 9;
            //txtPaymentDate2.BorderStyle	= 1;
            //txtPaymentDate2.Appearance	= 1;
            //txtPaymentDate2.MaxLength	= 10;
            //txtTellerID.MaxLength	= 3;
            //txtTaxYear.MaxLength	= 4;
            //txtPaymentDate.MaxLength	= 10;
            //DataFormat.Type	= 0;
            //DataFormat.Format	= "M/d/yyyy";
            //DataFormat.HaveTrueFalseNull	= 0;
            //DataFormat.FirstDayOfWeek	= 0;
            //DataFormat.FirstWeekOfYear	= 0;
            //DataFormat.LCID	= 1033;
            //DataFormat.SubFormatType	= 0;
            //txtPaidBy.MaxLength	= 25;
            //txtEffectiveDate.BorderStyle	= 1;
            //txtEffectiveDate.Appearance	= 1;
            //txtEffectiveDate.MaxLength	= 10;
            //vsElasticLight1.OleObjectBlob	= "frmCLGetAccount.frx":058A";
            //End Unmaped Properties
            try
            {
                //FC:FINAL:BCU #i171 - set selected index to match VB6 selection
                this.cmbPeriod1.SelectedIndex = 3;
                // On Error GoTo ERROR_HANDLER
                string strTemp = "";
                modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
                // this just sets the columns constant values
                lngColBatchCorrectAmount = 0;
                lngColBatchPrintReceipt = 1;
                lngColBatchAcct = 2;
                lngColBatchName = 3;
                lngColBatchYear = 4;
                lngColBatchRef = 5;
                lngColBatchPrin = 6;
                lngColBatchInt = 7;
                lngColBatchCost = 8;
                lngColBatchTotal = 9;
                lngColBatchKey = 10;
                lngColAcct = 0;
                lngColName = 1;
                lngcolLocationNumber = 2;
                lngColLocation = 3;
                lngColBookPage = 4;
                lngColBillRecords = 5;
                lngColMapLot = 6;
                lngColBSAccount = 0;
                lngColBSKey = 1;
                lngColBSName = 2;
                lngColBSMapLot = 3;
                strSearchString = "";
                if (modStatusPayments.Statics.boolRE)
                {
                    if (cmbSearchType.Items[2].ToString() != auxCmbSearchTypeItem2)
                    {
                        cmbSearchType.Items.Insert(2, auxCmbSearchTypeItem2);
                    }
                }
                else
                {
                    cmbSearchType.Items.RemoveAt(2);
                }
                //modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "CLSearchShowPreviousOwner", ref strTemp);
                strTemp = modGlobal.Statics.setCont.GetSettingValue("CLSearchShowPreviousOwner", "", "", "", "");
                if (strTemp != "FALSE")
                {
                    chkShowPreviousOwnerInfo.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkShowPreviousOwnerInfo.CheckState = Wisej.Web.CheckState.Unchecked;
                }
                //modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "CL\\", "CLSearchShowDeletedAccounts", ref strTemp);
                strTemp = modGlobal.Statics.setCont.GetSettingValue("CLSearchShowDeletedAccounts", "", "", "", "");
                if (strTemp != "FALSE")
                {
                    chkShowDeletedAccountInfo.CheckState = Wisej.Web.CheckState.Checked;
                }
                else
                {
                    chkShowDeletedAccountInfo.CheckState = Wisej.Web.CheckState.Unchecked;
                }
                modGlobalFunctions.SetTRIOColors(this);

                //FC:FINAL:AM:#3534 - Activate is not fired
                frmCLGetAccount_Activated(this, System.EventArgs.Empty);
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Form Load Error");
            }
        }

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            if ((boolToMainMenu || e.CloseReason == FCCloseReason.FormControlMenu) && !modExtraModules.IsThisCR())
            {
                // this will check to see if the get account screen should show the MDIParent
                App.MainForm.Show();
            }
            boolLoaded = false;
        }

        private void frmCLGetAccount_Resize(object sender, System.EventArgs e)
        {
            if (fraSearch.Visible == true && vsSearch.Visible == false)
            {
                ShowSearch();
            }
            else if (vsSearch.Visible == true)
            {
                FormatSearchGrid();
                // set the height of the grid
                //if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.Height - vsSearch.Top) - 1000)
                //{
                //	vsSearch.Height = (vsSearch.Rows * vsSearch.RowHeight(0)) + 70;
                //	vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
                //}
                //else
                //{
                //	vsSearch.Height = Math.Abs((this.Height - vsSearch.Top) - 1000);
                //	vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
                //}
            }
            else if (fraBatchQuestions.Visible)
            {
                //fraBatchQuestions.Top = FCConvert.ToInt32((this.Height - fraBatchQuestions.Height) / 2.0);
                //fraBatchQuestions.Left = FCConvert.ToInt32((this.Width - fraBatchQuestions.Width) / 2.0);
                //fraValidate.Top = fraBatchQuestions.Top + cmdBatch.Top - 100;
                //fraValidate.Left = FCConvert.ToInt32((this.Width - fraValidate.Width) / 2.0);
            }
            else if (fraRecover.Visible)
            {
                //fraRecover.Left = FCConvert.ToInt32((this.Width - fraRecover.Width) / 2.0);
                //fraRecover.Top = FCConvert.ToInt32((this.Height - fraRecover.Height) / 3.0);
                fraRecover.Visible = true;
            }
            else if (vsBatch.Visible)
            {
                FormatGrid();
                //fraBatch.Left = FCConvert.ToInt32((this.Width - fraBatch.Width) / 2.0);
                //fraBatch.Top = FCConvert.ToInt32((this.Height - fraBatch.Height) / 3.0);
                fraBatch.Visible = true;
                if (fraBatchSearchCriteria.Visible)
                {
                    //fraBatchSearchCriteria.Left = FCConvert.ToInt32((this.Width - fraBatchSearchCriteria.Width) / 2.0);
                    //fraBatchSearchCriteria.Top = FCConvert.ToInt32((this.Height - fraBatchSearchCriteria.Height) / 3.0);
                    fraBatchSearchCriteria.Visible = true;
                }
            }
        }

        private void mnuBatchPrint_Click(object sender, System.EventArgs e)
        {
            DateTime dtDate;
            int lngYR/*unused?*/;
            if (vsBatch.Rows > 1 && vsBatch.Visible)
            {
                // check for a year and a payment date
                boolPassActivate = true;
                arBatchListing.InstancePtr.Init(lngYear, dtPaymentDate, dtEffectiveDate);
            }
            else
            {
                FCMessageBox.Show("Save payments before trying to print a listing.", MsgBoxStyle.Exclamation, "No Payments");
            }
        }

        private void mnuFileBatchSearch_Click(object sender, System.EventArgs e)
        {
            if (fraBatch.Visible && fraBatch.Enabled)
            {
                // this will only happen when the batch screen is showing and enabled
                fraBatch.Enabled = false;
                //fraBatchSearchCriteria.Left = FCConvert.ToInt32((this.Width - fraBatchSearchCriteria.Width) / 2.0);
                //fraBatchSearchCriteria.Top = FCConvert.ToInt32((this.Height - fraBatchSearchCriteria.Height) / 2.0);
                fraBatchSearchCriteria.Visible = true;
                fraBatchSearchCriteria.ZOrder(0);
                txtBatchSearchName.Focus();
                txtBatchSearchName.SelectionStart = 0;
                txtBatchSearchName.SelectionLength = txtBatchSearchName.Text.Length;
            }
        }

        private void mnuFileImport_Click(object sender, System.EventArgs e)
        {
            ImportRecords();
        }

        private void mnuImportFirstAmericanBatch_Click(object sender, System.EventArgs e)
        {
            ImportFirstAmerican();
        }

        private void optPeriod_Click(int Index, object sender, System.EventArgs e)
        {
            txtBatchAccount_Validate(false);
        }

        private void optPeriod_Click(object sender, System.EventArgs e)
        {
            int index = cmbPeriod.SelectedIndex;
            optPeriod_Click(index, sender, e);
        }

        private void txtBatchSearchMapLot_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if ((KeyCode == Keys.Return) || (KeyCode == Keys.Down))
            {
                KeyCode = 0;
                if (cmdBatchSearch.Visible && cmdBatchSearch.Enabled)
                {
                    cmdBatchSearch.Focus();
                }
            }
            else if (KeyCode == Keys.Up)
            {
                KeyCode = 0;
                if (txtBatchSearchName.Visible && txtBatchSearchName.Enabled)
                {
                    txtBatchSearchName.Focus();
                }
            }
        }

        private void txtBatchSearchName_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if ((KeyCode == Keys.Return) || (KeyCode == Keys.Down))
            {
                KeyCode = 0;
                if (txtBatchSearchMapLot.Visible && txtBatchSearchMapLot.Enabled)
                {
                    txtBatchSearchMapLot.Focus();
                }
                else
                {
                    if (cmdBatchSearch.Visible && cmdBatchSearch.Enabled)
                    {
                        cmdBatchSearch.Focus();
                    }
                }
            }
        }

        private void vsBatch_Click(object sender, EventArgs e)
        {
            int lngMC;
            int lngMR;
            lngMC = vsBatch.MouseCol;
            lngMR = vsBatch.MouseRow;
            if (lngMC == lngColBatchPrintReceipt)
            {
            }
            else if (lngMC == lngColBatchCorrectAmount)
            {
                // vsBatch.EditText = vsBatch.TextMatrix(lngMR, lngMC)
            }
            else
            {
            }
        }

        private void vsBatchSearch_DblClick(object sender, EventArgs e)
        {
            // show the account
            if (Conversion.Val(vsBatchSearch.TextMatrix(vsBatchSearch.MouseRow, lngColBSAccount)) > 0)
            {
                ShowAccount(FCConvert.ToInt32(vsBatchSearch.TextMatrix(vsBatchSearch.MouseRow, lngColBSAccount)));
            }
        }

        private void vsBatchSearch_KeyDown(object sender, KeyEventArgs e)
        {
            int KeyCode = e.KeyValue;
            if (e.KeyCode == Keys.Return)
            {
                if (vsBatchSearch.Row > 0)
                {
                    ShowAccount(FCConvert.ToInt32(vsBatchSearch.TextMatrix(vsBatchSearch.Row, lngColBSAccount)));
                }
                KeyCode = 0;
            }
        }

        private void vsSearch_AfterSort(object sender, EventArgs e)
        {
            int Col = vsSearch.GetFlexColIndex(vsSearch.SortedColumn.Index);
            short Order = FCConvert.ToInt16(vsSearch.SortOrder);
            string strField = "";
            string strOrder = "";
            if (Order == 1)
            {
                strOrder = "asc";
            }
            else
            {
                strOrder = "desc";
            }
            if (Col == lngColAcct)
            {
                strField = "Account";
            }
            else if (Col == lngColName)
            {
                strField = "Name";
            }
            else if (Col == lngcolLocationNumber)
            {
                strField = "StreetNumber";
            }
            else if (Col == lngColLocation)
            {
                strField = "Street";
            }
            else if (Col == lngColBookPage)
            {
                strField = "BookPage";
            }
            else if (Col == lngColMapLot)
            {
                strField = "MapLot";
            }
            strPassSortOrder = "ORDER BY " + strField + " " + strOrder;
        }

        private void vsSearch_AfterUserResize(int Row, int Col)
        {
            int intCT;
            int lngWid = 0;
            for (intCT = 0; intCT <= vsSearch.Cols - 1; intCT++)
            {
                lngWid += vsSearch.ColWidth(intCT);
            }
            if (lngWid > vsSearch.WidthOriginal)
            {
                vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollBoth;
                // set the height of the grid
                //FC:FINAL:AM: don't set the height; used anchoring instead
                //if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.Height - vsSearch.Top) - 1000)
                //{
                //	vsSearch.Height = ((vsSearch.Rows + 1) * vsSearch.RowHeight(0));
                //	vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollHorizontal;
                //}
                //else
                //{
                //	vsSearch.Height = (this.Height - vsSearch.Top) - 1000;
                //	vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollBoth;
                //}
            }
            else
            {
                // set the height of the grid
                //if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.Height - vsSearch.Top) - 1000)
                //{
                //	vsSearch.Height = (vsSearch.Rows * vsSearch.RowHeight(0)) + 70;
                //	vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
                //}
                //else
                //{
                //	vsSearch.Height = (this.Height - vsSearch.Top) - 1000;
                //	vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
                //}
            }
        }

        private void vsSearch_BeforeSort(int Col)
        {
            if (Col == lngColLocation || Col == lngcolLocationNumber)
            {
                if (vsSearch.Rows > 1)
                {
                    vsSearch.Select(1, lngcolLocationNumber, 1, lngColLocation);
                }
            }
        }

        private void vsSearch_Click(object sender, EventArgs e)
        {
            if (vsSearch.HasRows)
            {
                txtGetAccountNumber.Text = Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6);
            }
        }

        private void VsSearch_DoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //FC:FINAL:AM:#3534 - check the index
            //if (vsSearch.Row > 0)
            if (e.RowIndex > -1)
            {
                txtGetAccountNumber.Text = vsSearch.TextMatrix(vsSearch.Row, lngColAcct);
                cmdGetAccountNumber_Click();
            }
        }

        private void vsSearch_KeyDown(object sender, KeyEventArgs e)
        {
            // captures the return ID to accept the account highlighted in the listbox
            if (e.KeyCode == Keys.Return)
            {
                cmdGetAccountNumber_Click();
            }
        }

        private void mnuBatchPurge_Click(object sender, System.EventArgs e)
        {
            // this will delete a Batch from the BatchRecover table
            // set tag to (P)urge
            fraRecover.Tag = "P";
            // purge
            cmdBatchChoice.Text = "Purge";
            lblRecover.Text = "Choose the Batch that you would like to purge.";
            // this will refresh the combo list
            if (FillBatchCombo())
            {
                // this will clear all the other frames out
                ShowBatchQuestions();
                fraBatchQuestions.Visible = false;
                // hide the questions frame
                //fraRecover.Left = FCConvert.ToInt32((this.Width - fraRecover.Width) / 2.0);
                //fraRecover.Top = FCConvert.ToInt32((this.Height - fraRecover.Height) / 3.0);
                fraRecover.Visible = true;
            }
        }

        private void mnuBatchRecover_Click(object sender, System.EventArgs e)
        {
            // this will recover a batch from the BatchRecover table
            // set tag to (R)ecover
            fraRecover.Tag = "R";
            // recover
            cmdBatchChoice.Text = "Recover";
            lblRecover.Text = "Choose the Batch that you would like to recover.";
            // this will refresh the combo list
            FillBatchCombo();
            // this will clear all the other frames out
            ShowBatchQuestions();
            fraBatchQuestions.Visible = false;
            // hide the questions frame
            //fraRecover.Left = FCConvert.ToInt32((this.Width - fraRecover.Width) / 2.0);
            //fraRecover.Top = FCConvert.ToInt32((this.Height - fraRecover.Height) / 3.0);
            fraRecover.Visible = true;
        }

        private void mnuBatchSave_Click(object sender, System.EventArgs e)
        {
            // this will save the information that has been put into the grid, into the BatchRecover table
            vsBatch.Rows = 1;
            Close();
        }

        private void mnuBatchStart_Click(object sender, System.EventArgs e)
        {
            modUseCR.AttemptToStartBatch();
            FormatBatchSearchGrid(true);
        }

        private void mnuProcessClearSearch_Click(object sender, System.EventArgs e)
        {
            //FC:FINAL:DDU:#i143 add clear method to clean the grid
            cmdClear_Click(sender, e);
        }

        private void mnuProcessGetAccount_Click()//(object sender, System.EventArgs e)
        {
            // MAL@20080208
            if (txtGetAccountNumber.Text == "")
            {
                FCMessageBox.Show("Please enter a valid account number.", MsgBoxStyle.Information, "Invalid Account");
                return;
            }
            if (fraValidate.Visible)
            {
                cmdValidateYes_Click();
            }
            else if (fraBatchQuestions.Visible)
            {
                cmdBatch_Click();
            }
            else if (fraBatch.Visible)
            {
                cmdProcessBatch_Click();
            }
            else
            {
                //FC:FINAL:AM:#14 - process first the account number
                //            if (Strings.Trim(txtSearch.Text) != "")
                //{
                //	cmdSearch_Click();
                //}
                //else
                //{
                //	if (Conversion.Val(txtGetAccountNumber.Text) != 0)
                //	{
                //		cmdGetAccountNumber_Click();
                //	}
                //}
                if (Conversion.Val(txtGetAccountNumber.Text) != 0)
                {
                    cmdGetAccountNumber_Click();
                }
                else if (Strings.Trim(txtSearch.Text) != "")
                {
                    cmdSearch_Click();
                }
            }
        }

        private void mnuProcessQuit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        private void mnuProcessSearch_Click(object sender, System.EventArgs e)
        {
            cmdSearch_Click();
        }

        private void optPP_Click(object sender, System.EventArgs e)
        {
            if (modExtraModules.IsThisCR() && !modGlobal.Statics.gboolShowLastCLAccountInCR)
            {
            }
            else
            {
                txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP);
            }
        }

        private void optRE_Click(object sender, System.EventArgs e)
        {
            //FC:FINAL:AM:#2086 - moved code from optPP_Click
            if (this.cmbRE.SelectedIndex == 0)
            {
                if (modExtraModules.IsThisCR() && !modGlobal.Statics.gboolShowLastCLAccountInCR)
                {
                }
                else
                {
                    txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE);
                }
            }
            else
            {
                if (modExtraModules.IsThisCR() && !modGlobal.Statics.gboolShowLastCLAccountInCR)
                {
                }
                else
                {
                    txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP);
                }
            }
        }

        private void txtAmount_Enter(object sender, System.EventArgs e)
        {
            txtAmount.SelectionStart = 0;
            txtAmount.SelectionLength = txtAmount.Text.Length;
        }

        private void txtAmount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if ((KeyCode >= Keys.D0 && KeyCode <= Keys.D9) || (KeyCode == Keys.Divide) || (KeyCode == Keys.Back) || (KeyCode == Keys.Delete) || (KeyCode == Keys.Right))
            {
            }
            else if (KeyCode == Keys.Left)
            {
                if (txtAmount.SelectionStart == 0)
                {
                    txtBatchAccount.Focus();
                }
            }
            else if (KeyCode == Keys.Return)
            {
                if (Conversion.Val(txtAmount.Text) != 0)
                {
                    if (Conversion.Val(txtBatchAccount.Text) != 0)
                    {
                        if (FCMessageBox.Show("Would you like to save this payment?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Payment") == DialogResult.Yes)
                        {
                            // If txtAmount.DataChanged Then
                            if (Conversion.Val(txtAmount.Text) != 0)
                            {
                                if (AdjustPaymentAmount_2(FCConvert.ToDouble(txtAmount.Text)) != 0)
                                {
                                    KeyCode = 0;
                                    return;
                                    // error has occured, exit sub and do not save payment
                                }
                            }
                            else
                            {
                                FCMessageBox.Show("Payment must be numeric and greater than 0.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Invalid Payment Amount");
                                return;
                            }
                            // End If
                            AddPaymentToBatch_6(rsBatch, FCConvert.ToInt32(FCConvert.ToDouble(txtBatchAccount.Text)));
                        }
                        else
                        {
                            txtBatchAccount.Focus();
                        }
                    }
                    else
                    {
                        FCMessageBox.Show("The account entered cannot have a value of zero.", MsgBoxStyle.Information, "Invalid Account");
                        txtBatchAccount.Focus();
                    }
                }
                else
                {
                    FCMessageBox.Show("The payment entered cannot be zero.", MsgBoxStyle.Information, "Invalid Payment Amount");
                    txtAmount.Focus();
                }
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtBatchAccount_Enter(object sender, System.EventArgs e)
        {
            txtBatchAccount.SelectionStart = 0;
            txtBatchAccount.SelectionLength = txtBatchAccount.Text.Length;
        }

        private void txtBatchAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            bool boolMove = false;
            if ((KeyCode >= Keys.D0 && KeyCode <= Keys.D9) || (KeyCode == Keys.Divide) || (KeyCode == Keys.Back) || (KeyCode == Keys.Delete) || (KeyCode == Keys.Left))
            {
            }
            else if (KeyCode == Keys.Right)
            {
                if (txtBatchAccount.SelectionStart == txtBatchAccount.Text.Length || txtBatchAccount.SelectionStart == 0 && txtBatchAccount.SelectionLength == txtBatchAccount.Text.Length)
                {
                    txtBatchAccount_Validate(boolMove);
                    if (boolMove == false)
                    {
                        txtAmount.Focus();
                    }
                }
            }
            else if (KeyCode == Keys.Return)
            {
                txtBatchAccount_Validate(boolMove);
                if (boolMove == false)
                {
                    txtAmount.Focus();
                }
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtBatchAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 8))
            {
            }
            else
            {
                KeyAscii = 0;
            }
            e.KeyChar = Strings.Chr(KeyAscii);
        }

        private void txtBatchAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // this sub will check to make sure that this is a valid account and if so, will calculate
            // the current Principal, Interest and Costs and display them as default in the Amount box
            int lngAcct;
            double dblTemp = 0;
            int lngYearKey;
            double dblCurrInt = 0;
            bool boolPrevBill = false;
            int intPer = 0;
            clsDRWrapper rsPrevBill = new clsDRWrapper();
            // during the validate, this will get account information and display it for the user
            if (txtBatchAccount.Text == "")
            {
                return;
            }
            lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtBatchAccount.Text)));
            // make sure that only one payment can be in the batch from each account
            if (vsBatch.FindRow(lngAcct, -1, -1, false, false) == -1)
            {
                if (cmbPeriod1.SelectedIndex == 0)
                {
                    intPer = 1;
                }
                else if (cmbPeriod1.SelectedIndex == 1)
                {
                    intPer = 2;
                }
                else if (cmbPeriod1.SelectedIndex == 2)
                {
                    intPer = 3;
                }
                else
                {
                    intPer = 4;
                }
                // reset the totals
                dblCurPrin = 0;
                dblCurInt = 0;
                dblPLInt = 0;
                dblCurCost = 0;
                dblTotalDue = 0;
                boolPrevBill = false;
                if (lngAcct != 0)
                {
                    // find the account
                    rsBatch.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " AND BillingType = 'RE' ORDER BY BillingYear", modExtraModules.strCLDatabase);
                    if (rsBatch.EndOfFile() != true && rsBatch.BeginningOfFile() != true)
                    {
                        rsBatch.FindFirstRecord("BillingYear", Conversion.Val(lngYear.ToString() + "1"));
                        if (rsBatch.NoMatch)
                        {
                            if (!boolBatchImport)
                            {
                                FCMessageBox.Show("Bill for account " + FCConvert.ToString(lngAcct) + " and year " + FCConvert.ToString(lngYear) + " not found.  Please mark this account and add it at the end of the batch.", MsgBoxStyle.Information, "Missing Bill Record");
                            }
                            txtBatchAccount.Text = "";
                            txtAmount.Text = "";
                            e.Cancel = true;
                        }
                        else
                        {
                            lngYearKey = FCConvert.ToInt32(rsBatch.Get_Fields_Int32("ID"));
                            // rsBatch.FindPreviousRecord "BillingYear < " & val(CStr(lngYear) & "1") & " AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > 0"
                            rsPrevBill.OpenRecordset("SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct) + " AND BillingType = 'RE' AND BillingYear < " + FCConvert.ToString(Conversion.Val(lngYear.ToString() + "1")) + " AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > 0 ORDER BY BillingYear DESC", modExtraModules.strCLDatabase);
                            while (!rsPrevBill.EndOfFile() && !boolPrevBill)
                            {
                                // rsBatch.NoMatch Then
                                if (((rsPrevBill.Get_Fields_Int32("LienRecordNumber"))) != 0)
                                {
                                    // rsBatch.Get_Fields("LienRecordNumber") <> 0 Then
                                    rsBLN.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsPrevBill.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
                                    if (modCLCalculations.CalculateAccountCLLien(rsBLN, dtEffectiveDate, ref dblCurrInt) != 0)
                                    {
                                        boolPrevBill = true;
                                    }
                                }
                                else
                                {
                                    // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                                    if (modCLCalculations.CalculateAccountCL(ref rsPrevBill, FCConvert.ToInt32(rsPrevBill.Get_Fields("Account")), dtEffectiveDate, ref dblCurrInt, FCConvert.ToInt16(intPer)) != 0)
                                    {
                                        boolPrevBill = true;
                                    }
                                }
                                if (boolPrevBill && !boolBatchImport)
                                {
                                    // there are previous years with balances
                                    if (FCMessageBox.Show("There are previous year(s) with account balances.  Would you like to continue adding payment?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Previous Account Balance") == DialogResult.Yes)
                                    {
                                        modGlobalFunctions.AddCYAEntry_8("CR", "Batch payment with previous balances. - Acct: " + txtBatchAccount.Text);
                                    }
                                    else
                                    {
                                        e.Cancel = false;
                                        return;
                                    }
                                }
                                rsPrevBill.MoveNext();
                            }
                            // xx                    'this will put the recordset back to the original bill
                            // xx                    rsBatch.FindFirstRecord "ID", lngYearKey
                            // this will move the labels to right right of the amount box
                            //FC:BCU - location is set in designer
                            //lblName.Left = txtAmount.Left + txtAmount.Width + 10;
                            //lblLocation.Left = lblName.Left;
                            // fill the labels with the account information
                            lblName.Text = "Name: " + Strings.Trim(FCConvert.ToString(rsBatch.Get_Fields_String("Name1")) + " ");
                            if (Strings.Trim(FCConvert.ToString(rsBatch.Get_Fields_String("StreetName")) + " ") != "")
                            {
                                lblLocation.Text = "Location: " + Strings.Trim(FCConvert.ToString(rsBatch.Get_Fields_Int32("StreetNumber")) + " " + FCConvert.ToString(rsBatch.Get_Fields_String("StreetName")) + " ");
                            }
                            else
                            {
                                lblLocation.Text = "Location: " + Strings.Trim(FCConvert.ToString(rsBatch.Get_Fields_String("Address1")) + " ");
                            }
                            if (((rsBatch.Get_Fields_Int32("LienRecordNumber"))) == 0)
                            {
                                dblOriginalAmount = modCLCalculations.CalculateAccountCL(ref rsBatch, lngAcct, dtEffectiveDate, ref dblTemp, ref dblCurPrin, ref dblCurInt, ref dblCurCost, FCConvert.ToInt16(intPer));
                                dblPLInt = 0;
                                txtAmount.Text = Strings.Format(dblOriginalAmount, "#,##0.00");
                            }
                            else
                            {
                                rsBatchLien = new clsDRWrapper();
                                rsBatchLien.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsBatch.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
                                if (rsBatchLien.EndOfFile() != true && rsBatchLien.BeginningOfFile() != true)
                                {
                                    dblOriginalAmount = modCLCalculations.CalculateAccountCLLien(rsBatchLien, dtEffectiveDate, ref dblTemp, ref dblCurPrin, ref dblCurInt, ref dblCurCost);
                                    txtAmount.Text = Strings.Format(dblOriginalAmount, "#,##0.00");
                                    dblPLInt = modGlobal.Round(dblOriginalAmount - (dblCurPrin + dblCurInt + dblCurCost), 2);
                                }
                                else
                                {
                                    FCMessageBox.Show("This account has gone to Lien and the lien record was not found.", MsgBoxStyle.Critical, "Account Validation");
                                    txtBatchAccount.Text = "";
                                    txtAmount.Text = "";
                                }
                            }
                            if (Conversion.Val(txtAmount.Text) == 0)
                            {
                                dblTotalDue = 0;
                            }
                            else
                            {
                                dblTotalDue = FCConvert.ToDouble(txtAmount.Text);
                            }
                        }
                    }
                    else
                    {
                        if (!boolBatchImport)
                        {
                            FCMessageBox.Show("Please enter a valid account number.", MsgBoxStyle.Information, "Account Validation");
                        }
                        txtBatchAccount.Text = "";
                        txtAmount.Text = "";
                        txtBatchAccount.Focus();
                    }
                }
                else
                {
                    if (!boolBatchImport)
                    {
                        FCMessageBox.Show("Please enter a valid account number.", MsgBoxStyle.Information, "Account Validation");
                    }
                    txtBatchAccount.Text = "";
                    txtAmount.Text = "";
                    txtBatchAccount.Focus();
                }
            }
            else
            {
                if (!boolBatchImport)
                {
                    FCMessageBox.Show("Please enter an account number that has not been entered already or delete the current payment for this account.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Multiple Payments");
                }
                txtBatchAccount.Text = "";
                txtAmount.Text = "";
            }
        }

        public void txtBatchAccount_Validate(bool Cancel)
        {
            txtBatchAccount_Validating(txtBatchAccount, new System.ComponentModel.CancelEventArgs(Cancel));
        }

        private void txtGetAccountNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Return)
            {
                KeyCode = 0;
                cmdGetAccountNumber_Click();
            }
        }

        private void txtGetAccountNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            if (KeyAscii < 48 || KeyAscii > 57)
            {
                if (KeyAscii == 8)
                {
                }
                else
                {
                    KeyAscii = 0;
                }
            }
            e.KeyChar = Strings.Chr(KeyAscii);
        }

        private void txtPaidBy_Enter(object sender, System.EventArgs e)
        {
            txtPaidBy.SelectionStart = 0;
            txtPaidBy.SelectionLength = txtPaidBy.Text.Length;
        }

        private void txtPaidBy_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if ((KeyCode >= Keys.D0 && KeyCode <= Keys.D9) || (KeyCode == Keys.Divide) || (KeyCode == Keys.Back) || (KeyCode == Keys.Delete) || (KeyCode == Keys.Left) || (KeyCode == Keys.Right))
            {
            }
            else if ((KeyCode == Keys.Return) || (KeyCode == Keys.Down))
            {
                KeyCode = 0;
                cmdBatch.Focus();
            }
            else if (KeyCode == Keys.Up)
            {
                KeyCode = 0;
                txtPaymentDate2.Focus();
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtPaymentDate_Enter(object sender, System.EventArgs e)
        {
            txtPaymentDate.SelectionStart = 0;
            txtPaymentDate.SelectionLength = txtPaymentDate.Text.Length;
        }

        private void txtPaymentDate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if ((KeyCode >= Keys.D0 && KeyCode <= Keys.D9) || (KeyCode == Keys.Divide) || (KeyCode == Keys.Back) || (KeyCode == Keys.Delete) || (KeyCode == Keys.Left) || (KeyCode == Keys.Right))
            {
            }
            else if ((KeyCode == Keys.Return) || (KeyCode == Keys.Down))
            {
                KeyCode = 0;
                txtPaidBy.Focus();
            }
            else if (KeyCode == Keys.Up)
            {
                KeyCode = 0;
                txtTaxYear.Focus();
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtPaymentDate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            if ((KeyAscii >= 47 && KeyAscii <= 57) || (KeyAscii == 8))
            {
            }
            else
            {
                KeyAscii = 0;
            }
            e.KeyChar = Strings.Chr(KeyAscii);
        }
        //FC:FINAL:DDU:#i135 added functionality and date mask to textboxes
        private void txtPaymentDate2_GotFocus(object sender, EventArgs e)
        {
            txtPaymentDate2.SelectionStart = 0;
            txtPaymentDate2.SelectionLength = txtPaymentDate2.Text.Length;
        }

        private void txtPaymentDate2_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            if ((KeyCode >= Keys.D0 && KeyCode <= Keys.D9) || (KeyCode == Keys.Divide) || (KeyCode == Keys.Back) || (KeyCode == Keys.Delete) || (KeyCode == Keys.Left) || (KeyCode == Keys.Right))
            {
            }
            else if ((KeyCode == Keys.Return) || (KeyCode == Keys.Down))
            {
                KeyCode = 0;
                txtPaidBy.Focus();
            }
            else if (KeyCode == Keys.Up)
            {
                KeyCode = 0;
                txtTaxYear.Focus();
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtPaymentDate2_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            if ((KeyAscii >= 47 && KeyAscii <= 57) || (KeyAscii == 8))
            {
            }
            else
            {
                KeyAscii = 0;
            }
        }

        private void txtEffectiveDate_GotFocus(object sender, EventArgs e)
        {
            txtEffectiveDate.SelectionStart = 0;
            txtEffectiveDate.SelectionLength = txtEffectiveDate.Text.Length;
        }

        private void txtEffectiveDate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            if ((KeyCode >= Keys.D0 && KeyCode <= Keys.D9) || (KeyCode == Keys.Divide) || (KeyCode == Keys.Back) || (KeyCode == Keys.Delete) || (KeyCode == Keys.Left) || (KeyCode == Keys.Right))
            {
            }
            else if ((KeyCode == Keys.Return) || (KeyCode == Keys.Down))
            {
                KeyCode = 0;
                txtPaidBy.Focus();
            }
            else if (KeyCode == Keys.Up)
            {
                KeyCode = 0;
                txtTaxYear.Focus();
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtEffectiveDate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            if ((KeyAscii >= 47 && KeyAscii <= 57) || (KeyAscii == 8))
            {
            }
            else
            {
                KeyAscii = 0;
            }
        }

        private void txtSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if (KeyCode == Keys.Return)
            {
                KeyCode = 0;
                cmdSearch_Click();
            }
        }

        private void ShowSearch()
        {
            // this will align the Search Screen and Show it
            fraBatch.Visible = false;
            fraBatchQuestions.Visible = false;
            fraRecover.Visible = false;
            boolSearch = true;
            //fraSearch.Left = FCConvert.ToInt32((this.Width - fraSearch.Width) / 2.0);
            //fraSearch.Top = FCConvert.ToInt32((this.Height - fraSearch.Height) / 2.0);
            fraSearch.Visible = true;
        }

        public void ShowBatchQuestions()
        {
            // this will align the Batch Questions Frame and Show it
            fraBatch.Visible = false;
            fraSearch.Visible = false;
            fraRecover.Visible = false;
            vsSearch.Visible = false;
            lblSearchListInstruction.Visible = false;
            mnuProcessClearSearch.Visible = false;
            //fraBatchQuestions.Left = FCConvert.ToInt32((this.Width - fraBatchQuestions.Width) / 2.0);
            //fraBatchQuestions.Top = FCConvert.ToInt32((this.Height - fraBatchQuestions.Height) / 3.0);
            fraBatchQuestions.Visible = true;
            // set the defalts for the new batch
            // txtPaymentDate2.Text = Date        'no default
            //FC:FINAL:AM:#1954 - set the focus
            this.txtTellerID.Focus();
        }

        private void ShowBatch()
        {
            int lngRW = 0;
            // this will align the Search Screen and Show it
            FormatGrid();
            this.Text = "Batch";
            rsBatchBackup.OpenRecordset("SELECT * FROM BatchRecover WHERE TellerID = '" + strTLRID + "' AND PaidBy = '" + strPaidBy + "' AND BatchRecoverDate = '" + FCConvert.ToString(dtPaymentDate) + "' AND ETDate = '" + FCConvert.ToString(dtEffectiveDate) + "'", modExtraModules.strCLDatabase);
            if (rsBatchBackup.EndOfFile() != true && rsBatchBackup.BeginningOfFile() != true)
            {
                // if there are already records out there
                if (FCMessageBox.Show("There are already some files saved in the recovery with this Teller ID and Paid By.  These may be pending transactions, would you like to load them?  If you do not, they will be removed.", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Recover Previous Batch?") == DialogResult.Yes)
                {
                    do
                    {
                        App.DoEvents();
                        vsBatch.AddItem("");
                        lngRW = vsBatch.Rows - 1;
                        // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                        vsBatch.TextMatrix(lngRW, lngColBatchAcct, FCConvert.ToString(rsBatchBackup.Get_Fields("AccountNumber")));
                        vsBatch.TextMatrix(lngRW, lngColBatchName, FCConvert.ToString(rsBatchBackup.Get_Fields_String("Name")));
                        // TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
                        vsBatch.TextMatrix(lngRW, lngColBatchYear, FCConvert.ToString(rsBatchBackup.Get_Fields("Year")));
                        vsBatch.TextMatrix(lngRW, lngColBatchRef, FCConvert.ToString(rsBatchBackup.Get_Fields_String("Ref")));
                        vsBatch.TextMatrix(lngRW, lngColBatchPrin, Strings.Format(rsBatchBackup.Get_Fields_Double("Prin"), "#,##0.00"));
                        vsBatch.TextMatrix(lngRW, lngColBatchInt, Strings.Format(rsBatchBackup.Get_Fields_Double("Int"), "#,##0.00"));
                        // TODO: Check the table for the column [Cost] and replace with corresponding Get_Field method
                        vsBatch.TextMatrix(lngRW, lngColBatchCost, Strings.Format(rsBatchBackup.Get_Fields("Cost"), "#,##0.00"));
                        vsBatch.TextMatrix(lngRW, lngColBatchTotal, FCConvert.ToString(FCConvert.ToDouble(vsBatch.TextMatrix(lngRW, lngColBatchPrin)) + FCConvert.ToDouble(vsBatch.TextMatrix(lngRW, lngColBatchInt)) + FCConvert.ToDouble(vsBatch.TextMatrix(lngRW, lngColBatchCost))));
                        vsBatch.TextMatrix(lngRW, lngColBatchKey, FCConvert.ToString(rsBatchBackup.Get_Fields_Int32("ID")));
                        if (FCConvert.ToBoolean(rsBatchBackup.Get_Fields_Boolean("PayCorrect")))
                        {
                            vsBatch.TextMatrix(lngRW, lngColBatchCorrectAmount, "Yes");
                        }
                        else
                        {
                            vsBatch.TextMatrix(lngRW, lngColBatchCorrectAmount, "No");
                        }
                        rsBatchBackup.MoveNext();
                    }
                    while (!rsBatchBackup.EndOfFile());
                    // total the payments
                    txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");
                    mnuBatchSave.Enabled = true;
                    mnuBatchRecover.Enabled = false;
                    mnuFileImport.Enabled = false;
                    mnuImportFirstAmericanBatch.Enabled = false;
                    rsBatchBackup.MoveFirst();
                }
                else
                {
                    do
                    {
                        rsBatchBackup.Delete();
                        rsBatchBackup.MoveNext();
                    }
                    while (!rsBatchBackup.EndOfFile());
                }
            }
            fraSearch.Visible = false;
            fraBatchQuestions.Visible = false;
            fraRecover.Visible = false;
            //fraBatch.Left = FCConvert.ToInt32((this.Width - fraBatch.Width) / 2.0 / 1440F);
            //fraBatch.Top = FCConvert.ToInt32((this.Height - fraBatch.Height) / 2.0 / 1440F);
            fraBatch.Visible = true;
        }

        private void FillPeriodCombo()
        {
            cmbPeriod.Clear();
            cmbPeriod.AddItem("1");
            cmbPeriod.AddItem("2");
            cmbPeriod.AddItem("3");
            cmbPeriod.AddItem("4");
        }

        private bool FillBatchCombo()
        {
            bool FillBatchCombo = false;
            clsDRWrapper rsBatchList = new clsDRWrapper();
            cmbBatchList.Clear();
            rsBatchList.OpenRecordset("SELECT DISTINCT TellerID, BatchRecoverDate, PaidBy FROM BatchRecover", modExtraModules.strCLDatabase);
            if (rsBatchList.EndOfFile() != true && rsBatchList.BeginningOfFile() != true)
            {
                do
                {
                    App.DoEvents();
                    cmbBatchList.AddItem(modGlobalFunctions.PadStringWithSpaces(FCConvert.ToString(rsBatchList.Get_Fields_DateTime("BatchRecoverDate")), 10) + " " + FCConvert.ToString(rsBatchList.Get_Fields_String("TellerID")) + " " + FCConvert.ToString(rsBatchList.Get_Fields_String("PaidBy")));
                    rsBatchList.MoveNext();
                }
                while (!rsBatchList.EndOfFile());
                if (cmbBatchList.Items.Count > 0)
                {
                    cmbBatchList.SelectedIndex = 0;
                }
                FillBatchCombo = true;
            }
            else
            {
                FCMessageBox.Show("No batch found.", MsgBoxStyle.Information, "Empty Batch Record");
                // mnuBatchRecover.Enabled = False
                // mnuBatchPurge.Enabled = False
                FillBatchCombo = false;
            }
            return FillBatchCombo;
        }

        private void FormatGrid()
        {
            int lngWid = 0;
            vsBatch.Cols = 11;
            lngWid = vsBatch.WidthOriginal;
            // width of each column
            vsBatch.ColWidth(lngColBatchAcct, FCConvert.ToInt32(lngWid * 0.055));
            // Account
            vsBatch.ColWidth(lngColBatchName, FCConvert.ToInt32(lngWid * 0.26));
            // Name
            vsBatch.ColWidth(lngColBatchYear, FCConvert.ToInt32(lngWid * 0.06));
            // Year
            vsBatch.ColWidth(lngColBatchRef, FCConvert.ToInt32(lngWid * 0.08));
            // Ref
            vsBatch.ColWidth(lngColBatchPrin, FCConvert.ToInt32(lngWid * 0.1));
            // Principal
            vsBatch.ColWidth(lngColBatchInt, FCConvert.ToInt32(lngWid * 0.1));
            // Interest
            vsBatch.ColWidth(lngColBatchCost, FCConvert.ToInt32(lngWid * 0.1));
            // Costs
            vsBatch.ColWidth(lngColBatchTotal, FCConvert.ToInt32(lngWid * 0.13));
            // Total
            vsBatch.ColWidth(lngColBatchKey, 0);
            vsBatch.ColWidth(lngColBatchPrintReceipt, FCConvert.ToInt32(lngWid * 0.04));
            // Print Receipt
            vsBatch.ColWidth(lngColBatchCorrectAmount, FCConvert.ToInt32(lngWid * 0.06));
            // Correct Amount
            // Headers
            vsBatch.TextMatrix(0, lngColBatchAcct, "Acct");
            vsBatch.TextMatrix(0, lngColBatchName, "Name");
            vsBatch.TextMatrix(0, lngColBatchYear, "Year");
            vsBatch.TextMatrix(0, lngColBatchRef, "Ref");
            vsBatch.TextMatrix(0, lngColBatchPrin, "Principal");
            vsBatch.TextMatrix(0, lngColBatchInt, "Interest");
            vsBatch.TextMatrix(0, lngColBatchCost, "Cost");
            vsBatch.TextMatrix(0, lngColBatchTotal, "Total");
            vsBatch.TextMatrix(0, lngColBatchKey, "");
            vsBatch.TextMatrix(0, lngColBatchPrintReceipt, "Rct");
            vsBatch.TextMatrix(0, lngColBatchCorrectAmount, "Auto");
            // Alignment and formating for the currency
            vsBatch.ColFormat(lngColBatchPrin, "#,##0.00");
            vsBatch.ColFormat(lngColBatchInt, "#,##0.00");
            vsBatch.ColFormat(lngColBatchCost, "#,##0.00");
            vsBatch.ColFormat(lngColBatchTotal, "#,##0.00");
            //FC:FINAL:BSE #1951 Alignment should match standart alignment
            vsBatch.ColAlignment(lngColBatchAcct, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsBatch.ColAlignment(lngColBatchYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsBatch.ColAlignment(lngColBatchPrin, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBatch.ColAlignment(lngColBatchInt, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBatch.ColAlignment(lngColBatchCost, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBatch.ColAlignment(lngColBatchTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsBatch.ColAlignment(lngColBatchRef, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsBatch.ColAlignment(lngColBatchCorrectAmount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsBatch.ColAlignment(lngColBatchName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsBatch.ColDataType(lngColBatchPrintReceipt, FCGrid.DataTypeSettings.flexDTBoolean);
            //vsBatch.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsBatch.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
        }

        private void ProcessBatch()
        {
            // this will go through each payment and add the correct payments to the
            // CL database and the correct receipt records to the CR database
            // this will cycle through the grid and find the total
            int lngCT;
            int lngRowCount;
            double dblSum;
            bool blnPrint;
            UpdatePrintReceiptChoice();
            lngRowCount = vsBatch.Rows - 1;
            dblSum = 0;
            blnPrint = false;
            for (lngCT = 1; lngCT <= lngRowCount; lngCT++)
            {
                App.DoEvents();
                dblSum += FCConvert.ToDouble(vsBatch.TextMatrix(lngCT, 8));
                if (Strings.Trim(vsBatch.TextMatrix(lngCT, lngColBatchPrintReceipt)) != "")
                {
                    //FC:FINAL:BCU #i171 - use FCConvert.CBool instead
                    //if (FCConvert.ToBoolean(vsBatch.TextMatrix(lngCT, lngColBatchPrintReceipt)) == true)
                    if (FCConvert.CBool(vsBatch.TextMatrix(lngCT, lngColBatchPrintReceipt)) == true)
                    {
                        blnPrint = true;
                    }
                }
            }
            if (lngRowCount > 0)
            {
                // Create Payment Records in CL
                if (CreateBatchCLRecords())
                {
                    // this will create all of the payment records for the Batch Update
                    if (modExtraModules.IsThisCR())
                    {
                        // If there is CR then send the info back there
                        modUseCR.CreateCLBatchReceiptEntry(ref blnPrint);
                    }
                    FCMessageBox.Show("Batch Update Successful.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Batch Update");
                    // clear the grid
                    vsBatch.Rows = 1;
                    // clear the BatchRecover table
                    // PurgeRecords PadStringWithSpaces(CStr(dtPaymentDate), 10) & " " & strTLRID & " " & strPaidBy
                    Close();
                }
            }
        }

        private void txtTaxYear_Enter(object sender, System.EventArgs e)
        {
            txtTaxYear.SelectionStart = 0;
            txtTaxYear.SelectionLength = txtTaxYear.Text.Length;
        }

        private void txtTaxYear_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if ((KeyCode >= Keys.D0 && KeyCode <= Keys.D9) || (KeyCode == Keys.Back) || (KeyCode == Keys.Delete) || (KeyCode == Keys.Left) || (KeyCode == Keys.Right))
            {
            }
            else if ((KeyCode == Keys.Return) || (KeyCode == Keys.Down))
            {
                KeyCode = 0;
                txtPaymentDate2.Focus();
            }
            else if (KeyCode == Keys.Up)
            {
                KeyCode = 0;
                txtTellerID.Focus();
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtTaxYear_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 8))
            {
            }
            else
            {
                KeyAscii = 0;
            }
            e.KeyChar = Strings.Chr(KeyAscii);
        }

        private void txtTellerID_Enter(object sender, System.EventArgs e)
        {
            txtTellerID.SelectionStart = 0;
            txtTellerID.SelectionLength = txtTellerID.Text.Length;
        }

        private void txtTellerID_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            if ((KeyCode >= Keys.A && KeyCode <= Keys.Z) || (KeyCode == Keys.Delete) || (KeyCode == Keys.Back) || (KeyCode == Keys.Left) || (KeyCode == Keys.Right) || (KeyCode >= Keys.D0 && KeyCode <= Keys.D9))
            {
            }
            else if ((KeyCode == Keys.Return) || (KeyCode == Keys.Down))
            {
                KeyCode = 0;
                txtTaxYear.Focus();
            }
            else if (KeyCode == Keys.Up)
            {
                KeyCode = 0;
                chkReceipt.Focus();
            }
            else
            {
                KeyCode = 0;
            }
        }

        private void txtTellerID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            int KeyAscii = Strings.Asc(e.KeyChar);
            if ((KeyAscii >= 65 && KeyAscii <= 90) || (KeyAscii == 8) || (KeyAscii >= 48 && KeyAscii <= 57))
            {
            }
            else if (KeyAscii >= 97 && KeyAscii <= 122)
            {
                KeyAscii -= 32;
            }
            else
            {
                KeyAscii = 0;
            }
            e.KeyChar = Strings.Chr(KeyAscii);
        }
        // VBto upgrade warning: dblNewAmount As double	OnWrite(double, string, short)
        private short AdjustPaymentAmount_2(double dblNewAmount, bool boolDoNotQueryOverPay = false)
        {
            return AdjustPaymentAmount(ref dblNewAmount, boolDoNotQueryOverPay);
        }

        private short AdjustPaymentAmount_8(double dblNewAmount, bool boolDoNotQueryOverPay = false)
        {
            return AdjustPaymentAmount(ref dblNewAmount, boolDoNotQueryOverPay);
        }

        private short AdjustPaymentAmount(ref double dblNewAmount, bool boolDoNotQueryOverPay = false)
        {
            short AdjustPaymentAmount = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this sub will adjust the payment amounts if the total amount is changed
                // it will affect dblCurPrin, dblCurInt, dblCurCost
                if (dblNewAmount >= dblCurInt + dblPLInt)
                {
                    dblNewAmount -= (dblCurInt + dblPLInt);
                    if (dblNewAmount >= dblCurCost)
                    {
                        dblNewAmount -= dblCurCost;
                        if (dblNewAmount >= dblCurPrin)
                        {
                            dblNewAmount = FCConvert.ToDouble(Strings.Format(dblNewAmount - dblCurPrin, "0.00"));
                        }
                        else
                        {
                            // less than interest, costs and principal
                            dblCurPrin = dblNewAmount;
                            dblNewAmount = 0;
                        }
                    }
                    else
                    {
                        // less than interest and costs
                        dblCurPrin = 0;
                        dblCurCost = dblNewAmount;
                        dblNewAmount = 0;
                    }
                }
                else if (dblNewAmount >= dblCurInt)
                {
                    // less than interest
                    dblNewAmount -= dblCurInt;
                    dblPLInt = dblNewAmount;
                    dblNewAmount = 0;
                }
                else
                {
                    dblCurInt = dblNewAmount;
                    dblPLInt = 0;
                    dblCurPrin = 0;
                    dblCurCost = 0;
                    dblNewAmount = 0;
                }
                if (modGlobal.Round(dblNewAmount, 2) > 0)
                {
                    // if there is leftover, then the payment is too much
                    if (boolDoNotQueryOverPay)
                    {
                        dblCurPrin += dblNewAmount;
                    }
                    else
                    {
                        if (!boolBatchImport)
                        {
                            if (FCMessageBox.Show("This payment is more than owed, would you like to continue?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Overpayment") == DialogResult.Yes)
                            {
                                dblCurPrin += dblNewAmount;
                            }
                            else
                            {
                                // they do not want the overpayment
                                AdjustPaymentAmount = -1;
                            }
                        }
                        else
                        {
                            dblCurPrin += dblNewAmount;
                        }
                    }
                }
                return AdjustPaymentAmount;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                frmWait.InstancePtr.Unload();
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Adjusting Payment Amount");
            }
            return AdjustPaymentAmount;
        }

        private double CalculateGridTotal()
        {
            double CalculateGridTotal = 0;
            // this will cycle through the grid and find the total
            int lngCT;
            double dblSum;
            dblSum = 0;
            for (lngCT = 1; lngCT <= vsBatch.Rows - 1; lngCT++)
            {
                App.DoEvents();
                if (vsBatch.TextMatrix(lngCT, lngColBatchTotal) != "" && Conversion.Val(vsBatch.TextMatrix(lngCT, lngColBatchTotal)) != 0)
                {
                    dblSum += FCConvert.ToDouble(vsBatch.TextMatrix(lngCT, lngColBatchTotal));
                }
            }
            if (dblSum > 0)
            {
                // turns on the menu option when calculating the total
                mnuBatchSave.Enabled = true;
                mnuBatchRecover.Enabled = false;
            }
            CalculateGridTotal = dblSum;
            return CalculateGridTotal;
        }

        private void AddPaymentToBatch_6(clsDRWrapper rsTemp, int lngAcct)
        {
            AddPaymentToBatch(ref rsTemp, ref lngAcct);
        }

        private void AddPaymentToBatch(ref clsDRWrapper rsTemp, ref int lngAcct)
        {
            int lngRow;
            int lngKey = 0;
            int lngTYear = 0;
            // add it to the temporary table
            // .OpenRecordset "SELECT * FROM BatchRecover", strCLDatabase
            rsBatchBackup.AddNew();
            // set the ID value from the batch recover table
            // lngKey = .Get_Fields("ID")
            lngTYear = FCConvert.ToInt32(rsBatch.Get_Fields_Int32("BillingYear"));
            strRef = FCConvert.ToString(lngAcct) + "-" + FCConvert.ToString(lngTYear);
            // add the values to the table
            rsBatchBackup.Set_Fields("TellerID", strTLRID);
            rsBatchBackup.Set_Fields("PaidBy", strPaidBy + " ");
            rsBatchBackup.Set_Fields("Ref", strRef + " ");
            rsBatchBackup.Set_Fields("Year", lngTYear / 10);
            rsBatchBackup.Set_Fields("AccountNumber", lngAcct);
            rsBatchBackup.Set_Fields("Name", Strings.Right(lblName.Text, lblName.Text.Length - 6));
            rsBatchBackup.Set_Fields("Prin", dblCurPrin);
            rsBatchBackup.Set_Fields("Int", dblCurInt + dblPLInt);
            rsBatchBackup.Set_Fields("Cost", dblCurCost);
            rsBatchBackup.Set_Fields("BatchRecoverDate", dtPaymentDate);
            rsBatchBackup.Set_Fields("ETDate", dtEffectiveDate);
            rsBatchBackup.Set_Fields("Type", " ");
            // save the record
            if (!rsBatchBackup.Update(true))
            {
                FCMessageBox.Show("Error adding record batch.  Please try again.", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Update Error");
                return;
            }
            lngKey = FCConvert.ToInt32(rsBatchBackup.Get_Fields_Int32("ID"));
            vsBatch.AddItem("");
            lngRow = vsBatch.Rows - 1;
            // add it to the grid
            vsBatch.TextMatrix(lngRow, lngColBatchAcct, FCConvert.ToString(lngAcct));
            vsBatch.TextMatrix(lngRow, lngColBatchName, Strings.Right(lblName.Text, lblName.Text.Length - 6));
            vsBatch.TextMatrix(lngRow, lngColBatchYear, FCConvert.ToString(lngTYear / 10));
            vsBatch.TextMatrix(lngRow, lngColBatchRef, strRef);
            vsBatch.TextMatrix(lngRow, lngColBatchPrin, FCConvert.ToString(dblCurPrin));
            vsBatch.TextMatrix(lngRow, lngColBatchInt, FCConvert.ToString(dblCurInt + dblPLInt));
            vsBatch.TextMatrix(lngRow, lngColBatchCost, FCConvert.ToString(dblCurCost));
            vsBatch.TextMatrix(lngRow, lngColBatchTotal, FCConvert.ToString(dblCurPrin + dblCurInt + dblPLInt + dblCurCost));
            vsBatch.TextMatrix(lngRow, lngColBatchKey, FCConvert.ToString(lngKey));
            if (chkReceipt.CheckState == Wisej.Web.CheckState.Checked)
            {
                vsBatch.TextMatrix(lngRow, lngColBatchPrintReceipt, FCConvert.ToString(-1));
            }
            else
            {
                vsBatch.TextMatrix(lngRow, lngColBatchPrintReceipt, FCConvert.ToString(0));
            }
            if (dblCurPrin + dblCurInt + dblPLInt + dblCurCost == dblOriginalAmount)
            {
                vsBatch.TextMatrix(lngRow, lngColBatchCorrectAmount, "Yes");
            }
            else
            {
                vsBatch.TextMatrix(lngRow, lngColBatchCorrectAmount, "No");
            }
            // total grid
            txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");
            // This will not allow an import after a row has already been added
            mnuFileImport.Enabled = false;
            mnuImportFirstAmericanBatch.Enabled = false;
            // clear the payment boxes and setfocus to the account box
            txtAmount.Text = "";
            txtBatchAccount.Text = "";
            lblName.Text = "";
            lblLocation.Text = "";
            txtBatchAccount.Focus();
        }

        private void DeletePaymentFromBatch(int lngRow)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // VBto upgrade warning: lngKey As int	OnWrite(string)
                int lngKey = 0;
                if (FCMessageBox.Show("Are you sure that you would like to delete this payment line.", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Delete Payment") != DialogResult.Yes)
                {
                    return;
                }
                // get the ID number for the BatchRecover table
                if (vsBatch.TextMatrix(lngRow, lngColBatchKey) != "")
                {
                    lngKey = FCConvert.ToInt32(vsBatch.TextMatrix(lngRow, lngColBatchKey));
                    // delete payment from the database
                    rsBatchBackup.OpenRecordset("SELECT * FROM BatchRecover WHERE [ID] = " + FCConvert.ToString(lngKey), modExtraModules.strCLDatabase);
                    if (!rsBatchBackup.EndOfFile())
                    {
                        rsBatchBackup.Delete();
                    }
                    else
                    {
                        FCMessageBox.Show("Error Executing SQL Statement on removal of backup record.", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Deletion Error");
                    }
                    // delete this payment from the grid
                    vsBatch.RemoveItem(lngRow);
                    if (vsBatch.Rows == 1)
                    {
                        mnuBatchSave.Enabled = false;
                        mnuBatchRecover.Enabled = true;
                    }
                }
                else
                {
                    // empty row
                    // delete this line from the grid
                    vsBatch.RemoveItem(lngRow);
                }
                txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Deleting Row");
            }
        }

        private void vsBatch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                // delete a row from the grid
                DeletePaymentFromBatch(vsBatch.Row);
                if (vsBatch.Rows == 1)
                {
                    mnuFileImport.Enabled = true;
                    mnuImportFirstAmericanBatch.Enabled = true;
                }
            }
            else
            {
            }
        }

        private void PurgeRecords_2(string strParam)
        {
            PurgeRecords(ref strParam);
        }

        private void PurgeRecords(ref string strParam)
        {
            // this will find all the records that match these parameters and will delete them from the temp table
            string strPB;
            string strTID = "";
            DateTime dtDate = DateTime.FromOADate(0);
            if (strParam.Length >= 10)
            {
                dtDate = DateAndTime.DateValue(Strings.Left(strParam, 10));
                strParam = Strings.Trim(Strings.Right(strParam, strParam.Length - 10));
            }
            if (strParam.Length >= 4)
            {
                strTID = Strings.Trim(Strings.Left(strParam, 4));
                strParam = Strings.Trim(Strings.Right(strParam, strParam.Length - 4));
            }
            strPB = Strings.Trim(strParam);
            if (!rsBatchBackup.Execute("DELETE FROM BatchRecover WHERE BatchRecoverDate = '" + FCConvert.ToString(dtDate) + "' AND TellerID = '" + strTID + "' AND PaidBy = '" + strPB + "'", modExtraModules.strCLDatabase, false))
            {
                FCMessageBox.Show("Error #" + FCConvert.ToString(rsBatchBackup.ErrorNumber) + " - " + rsBatchBackup.ErrorDescription + ".", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Purge Error");
            }
            fraRecover.Visible = false;
            fraBatchQuestions.Visible = false;
            fraBatch.Visible = false;
            fraSearch.Visible = true;
        }

        private void RecoverRecords_2(string strParam)
        {
            RecoverRecords(ref strParam);
        }

        private void RecoverRecords(ref string strParam)
        {
            // this will find all the records that match these parameters and will delete them from the temp table
            string strPB;
            string strTID = "";
            DateTime dtDate = DateTime.FromOADate(0);
            if (strParam.Length >= 10)
            {
                dtDate = DateAndTime.DateValue(Strings.Left(strParam, 10));
                strParam = Strings.Trim(Strings.Right(strParam, strParam.Length - 10));
            }
            if (strParam.Length >= 4)
            {
                strTID = Strings.Trim(Strings.Left(strParam, 4));
                strParam = Strings.Trim(Strings.Right(strParam, strParam.Length - 4));
            }
            strPB = Strings.Trim(strParam);
            FormatGrid();
            if (!rsBatchBackup.OpenRecordset("SELECT * FROM BatchRecover WHERE BatchRecoverDate = '" + FCConvert.ToString(dtDate) + "' AND TellerID = '" + strTID + "' AND PaidBy = '" + strPB + "'", modExtraModules.strCLDatabase))
            {
                FCMessageBox.Show("Error #" + FCConvert.ToString(rsBatchBackup.ErrorNumber) + " - " + rsBatchBackup.ErrorDescription + ".", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Batch Recover Error");
            }
            else
            {
                if (rsBatchBackup.EndOfFile() != true && rsBatchBackup.BeginningOfFile() != true)
                {
                    vsBatch.Rows = 1;
                    rsBatchBackup.MoveFirst();
                    do
                    {
                        App.DoEvents();
                        vsBatch.AddItem("");
                        // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchAcct, FCConvert.ToString(rsBatchBackup.Get_Fields("AccountNumber")));
                        // Account Number
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchName, FCConvert.ToString(rsBatchBackup.Get_Fields_String("Name")));
                        // Name
                        // TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchYear, FCConvert.ToString(rsBatchBackup.Get_Fields("Year")));
                        // Year
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchRef, FCConvert.ToString(rsBatchBackup.Get_Fields_String("Ref")));
                        // Ref
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchPrin, FCConvert.ToString(rsBatchBackup.Get_Fields_Double("Prin")));
                        // Prin
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchInt, FCConvert.ToString(rsBatchBackup.Get_Fields_Double("Int")));
                        // Int
                        // TODO: Check the table for the column [Cost] and replace with corresponding Get_Field method
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchCost, FCConvert.ToString(rsBatchBackup.Get_Fields("Cost")));
                        // Cost
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchTotal, FCConvert.ToString(FCConvert.ToDouble(vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchPrin)) + FCConvert.ToDouble(vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchInt)) + FCConvert.ToDouble(vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchCost))));
                        // Total
                        vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchKey, FCConvert.ToString(rsBatchBackup.Get_Fields_Int32("ID")));
                        // ID in Table
                        if (FCConvert.ToBoolean(rsBatchBackup.Get_Fields_Boolean("PrintReceipt")))
                        {
                            // if Print Receipt is true
                            vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchPrintReceipt, FCConvert.ToString(-1));
                            // then check the box in the grid
                        }
                        else
                        {
                            vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchPrintReceipt, FCConvert.ToString(0));
                        }
                        rsBatchBackup.MoveNext();
                    }
                    while (!rsBatchBackup.EndOfFile());
                    rsBatchBackup.MoveFirst();
                    txtTellerID.Text = FCConvert.ToString(rsBatchBackup.Get_Fields_String("TellerID"));
                    txtPaymentDate2.Text = FCConvert.ToString(rsBatchBackup.Get_Fields_DateTime("BatchRecoverDate"));
                    txtEffectiveDate.Text = FCConvert.ToString(rsBatchBackup.Get_Fields_DateTime("ETDate"));
                    txtPaidBy.Text = FCConvert.ToString(rsBatchBackup.Get_Fields_String("PaidBy"));
                    // TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
                    txtTaxYear.Text = FCConvert.ToString(rsBatchBackup.Get_Fields("Year"));
                }
                else
                {
                    FCMessageBox.Show("Error processing batch with Payment Date: " + FCConvert.ToString(dtDate) + ", Teller ID: " + strTID + " and Paid By: " + strPB + ".", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Batch Error");
                }
            }
            // total the payments
            txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");
            mnuFileImport.Enabled = false;
            mnuImportFirstAmericanBatch.Enabled = false;
            if (CheckBatchQuestions())
            {
                fraSearch.Visible = false;
                fraBatchQuestions.Visible = false;
                fraRecover.Visible = false;
                //fraBatch.Left = FCConvert.ToInt32((this.Width - fraBatch.Width) / 2.0);
                //fraBatch.Top = FCConvert.ToInt32((this.Height - fraBatch.Height) / 3.0);
                fraBatch.Visible = true;
            }
            else
            {
                fraSearch.Visible = false;
                fraBatch.Visible = true;
                fraRecover.Visible = false;
                //fraBatchQuestions.Left = FCConvert.ToInt32((this.Width - fraBatchQuestions.Width) / 2.0);
                //fraBatchQuestions.Top = FCConvert.ToInt32((this.Height - fraBatchQuestions.Height) / 3.0);
                fraBatchQuestions.Visible = true;
            }
        }

        private void vsBatch_RowColChange(object sender, EventArgs e)
        {
            if (vsBatch.Col == lngColBatchPrintReceipt)
            {
                vsBatch.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
            }
            else
            {
                vsBatch.Editable = FCGrid.EditableSettings.flexEDNone;
            }
        }

        private bool CreateBatchCLRecords()
        {
            bool CreateBatchCLRecords = false;
            clsDRWrapper rsBatchCL = new clsDRWrapper();
            // Payment Rec
            clsDRWrapper rsBillRec = new clsDRWrapper();
            // Billing Master
            clsDRWrapper rsLien = new clsDRWrapper();
            // MAL@20071210
            string strSQL;
            double dblTotalDue = 0;
            int lYear;
            lYear = 0;
            // strSQL = "SELECT * FROM BatchRecover WHERE TellerID = '" & strTLRID & "' AND Date = #" & dtPaymentDate & "# AND ETDate = #" & dtEffectiveDate & "# AND PaidBy = '" & strPaidBy & "' AND Year   = " & lngYear
            strSQL = "SELECT * FROM BatchRecover WHERE TellerID = '" + strTLRID + "' AND BatchRecoverDate = '" + FCConvert.ToString(dtPaymentDate) + "' AND ETDate = '" + FCConvert.ToString(dtEffectiveDate) + "' AND PaidBy = '" + strPaidBy + "' ";
            rsBatchBackup.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
            if (rsBatchBackup.EndOfFile() != true && rsBatchBackup.BeginningOfFile() != true)
            {
                do
                {
                    // cycle through all of the batch list
                    // this function will return the total due and seperate the total into interest, cost and principal due
                    // it also returns the current interest in the variable dblXtraCurInt in order to create an Interest Line
                    App.DoEvents();
                    // TODO: Check the table for the column [YEAR] and replace with corresponding Get_Field method
                    lYear = FCConvert.ToInt32(Math.Round(Conversion.Val(rsBatchBackup.Get_Fields("YEAR"))));
                    if (lYear == 0)
                        lYear = lngYear;
                    // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                    strSQL = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(rsBatchBackup.Get_Fields("AccountNumber")) + " AND BillingYear = " + FCConvert.ToString(lYear) + "1 AND BillingType = 'RE'";
                    if (rsBillRec.OpenRecordset(strSQL, modExtraModules.strCLDatabase))
                    {
                        dblCurInt = 0;
                        dblXtraCurInt = 0;
                        // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                        dblTotalDue = modCLCalculations.CalculateAccountCL(ref rsBillRec, FCConvert.ToInt32(rsBatchBackup.Get_Fields("AccountNumber")), rsBatchBackup.Get_Fields_DateTime("ETDate"), ref dblXtraCurInt, ref dblCurPrin, ref dblCurInt, ref dblCurCost);
                        dblPLInt = modGlobal.Round(dblTotalDue - (dblCurPrin + dblCurInt + dblCurCost), 2);
                        strSQL = "SELECT * FROM PaymentRec WHERE Account = 0";
                        rsBatchCL.OpenRecordset(strSQL, modExtraModules.strCLDatabase);
                        rsBatchCL.AddNew();
                        // rsBatchBackup.edit
                        // rsBatchBackup.Get_Fields("PaymentKey") = rsBatchCL.Get_Fields("ID")
                        // rsBatchBackup.Update True
                        // TODO: Check the table for the column [Cost] and replace with corresponding Get_Field method
                        AdjustPaymentAmount_8(Conversion.Val(rsBatchBackup.Get_Fields_Double("Prin")) + Conversion.Val(rsBatchBackup.Get_Fields_Double("Int")) + Conversion.Val(rsBatchBackup.Get_Fields("Cost")), true);
                        // Account
                        // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                        rsBatchCL.Set_Fields("Account", rsBatchBackup.Get_Fields("AccountNumber"));
                        // Year
                        // TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
                        rsBatchCL.Set_Fields("Year", FCConvert.ToString(rsBatchBackup.Get_Fields("Year")) + "1");
                        // CLng(rsBatchBackup.Get_Fields("Year") & "1")
                        // BillKey
                        if (((rsBillRec.Get_Fields_Int32("LienRecordNumber"))) == 0)
                        {
                            rsBatchCL.Set_Fields("BillKey", rsBillRec.Get_Fields_Int32("ID"));
                        }
                        else
                        {
                            rsBatchCL.Set_Fields("BillKey", rsBillRec.Get_Fields_Int32("LienRecordNumber"));
                        }
                        // CHGINTNumber
                        rsBatchCL.Set_Fields("CHGINTNumber", 0);
                        // CHGINTDate
                        rsBatchCL.Set_Fields("CHGINTDate", 0);
                        // ActualSystemDate
                        rsBatchCL.Set_Fields("ActualSystemDate", DateTime.Today);
                        // EffectiveInterestDate
                        rsBatchCL.Set_Fields("EffectiveInterestDate", rsBatchBackup.Get_Fields_DateTime("ETDate"));
                        // RecordedTransactionDate
                        rsBatchCL.Set_Fields("RecordedTransactionDate", rsBatchBackup.Get_Fields_DateTime("BatchRecoverDate"));
                        // Teller
                        rsBatchCL.Set_Fields("Teller", strTLRID);
                        // Reference
                        if (Strings.Trim(FCConvert.ToString(rsBatchBackup.Get_Fields_String("Ref"))).Length > 10)
                        {
                            rsBatchCL.Set_Fields("Reference", Strings.Left(Strings.Trim(FCConvert.ToString(rsBatchBackup.Get_Fields_String("Ref"))), 10));
                        }
                        else
                        {
                            rsBatchCL.Set_Fields("Reference", Strings.Trim(FCConvert.ToString(rsBatchBackup.Get_Fields_String("Ref"))));
                        }
                        // Period
                        rsBatchCL.Set_Fields("Period", "A");
                        // Code
                        rsBatchCL.Set_Fields("Code", "P");
                        // ReceiptNumber
                        // this has to be a check to see if it is coming from CR not just if they have it
                        if (modExtraModules.IsThisCR())
                        {
                            rsBatchCL.Set_Fields("ReceiptNumber", -1);
                        }
                        else
                        {
                            rsBatchCL.Set_Fields("ReceiptNumber", 0);
                        }
                        // Principal
                        rsBatchCL.Set_Fields("Principal", modGlobal.Round(dblCurPrin, 2));
                        // PreLienInterest
                        rsBatchCL.Set_Fields("PreLienInterest", modGlobal.Round(dblPLInt, 2));
                        // CurrentInterest
                        rsBatchCL.Set_Fields("CurrentInterest", modGlobal.Round(dblCurInt, 2));
                        // LienCost
                        rsBatchCL.Set_Fields("LienCost", modGlobal.Round(dblCurCost, 2));
                        // TransNumber
                        rsBatchCL.Set_Fields("TransNumber", 0);
                        // PaidBy
                        rsBatchCL.Set_Fields("PaidBy", strPaidBy);
                        // Comments
                        rsBatchCL.Set_Fields("Comments", "This was a batch transaction.");
                        // CashDrawer
                        rsBatchCL.Set_Fields("CashDrawer", "Y");
                        // GeneralLedger
                        rsBatchCL.Set_Fields("GeneralLedger", "Y");
                        // BudgetaryAccountNumber
                        rsBatchCL.Set_Fields("BudgetaryAccountNumber", "");
                        // BillCode
                        if (((rsBillRec.Get_Fields_Int32("LienRecordNumber"))) != 0)
                        {
                            rsBatchCL.Set_Fields("BillCode", "L");
                        }
                        else
                        {
                            rsBatchCL.Set_Fields("BillCode", "R");
                        }
                        if (!rsBatchCL.Update(false))
                        {
                            // if there is an error creating this record
                            // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                            FCMessageBox.Show("Error #" + FCConvert.ToString(rsBatchCL.ErrorNumber) + " - " + rsBatch.ErrorDescription + " for account number " + FCConvert.ToString(rsBatchBackup.Get_Fields("AccountNumber")), MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Update Error");
                            CreateBatchCLRecords = false;
                        }
                        else
                        {
                            // if the record was created correctly
                            rsBatchBackup.Edit();
                            rsBatchBackup.Set_Fields("PaymentKey", rsBatchCL.Get_Fields_Int32("ID"));
                            rsBatchBackup.Update(true);
                            if (dblXtraCurInt != 0)
                            {
                                // if there is interest that has to be charged, then this will do it
                                // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                                rsBatchCL.FindFirstRecord("Account", rsBatchBackup.Get_Fields("AccountNumber"));
                                if (CreateBatchCHGINTRecord(ref rsBatchCL, ref dblXtraCurInt))
                                {
                                    rsBatchCL.Edit();
                                    rsBatchCL.Set_Fields("CHGINTNumber", lngBatchCHGINTNumber);
                                    rsBatchCL.Update(true);
                                }
                                else
                                {
                                    // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                                    FCMessageBox.Show("Error creating Charged Interest Record for account number " + FCConvert.ToString(rsBatchBackup.Get_Fields("AccountNumber")) + ".", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Batch CHGINT Error");
                                }
                            }
                            // MAL@20080603: Change to not update CR records yet
                            // Tracker Reference: 14055
                            if (!modExtraModules.IsThisCR())
                            {
                                // MAL@20071210: Update Bill Record with Amounts Listed
                                // Tracker Reference: 11594
                                if (((rsBillRec.Get_Fields_Int32("LienRecordNumber"))) != 0)
                                {
                                    // Update Lien Record
                                    rsLien.OpenRecordset("SELECT * FROM LienRec WHERE ID = " + FCConvert.ToString(rsBillRec.Get_Fields_Int32("LienRecordNumber")), modExtraModules.strCLDatabase);
                                    if (rsLien.RecordCount() > 0)
                                    {
                                        rsLien.Edit();
                                        rsLien.Set_Fields("PrincipalPaid", Conversion.Val(rsLien.Get_Fields_Decimal("PrincipalPaid")) + modGlobal.Round(dblCurPrin, 2));
                                        rsLien.Set_Fields("InterestPaid", Conversion.Val(rsLien.Get_Fields_Decimal("InterestPaid")) + modGlobal.Round(dblCurInt, 2));
                                        rsLien.Set_Fields("CostsPaid", Conversion.Val(rsLien.Get_Fields_Decimal("CostsPaid")) + modGlobal.Round(dblCurCost, 2));
                                        // TODO: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
                                        rsLien.Set_Fields("PLIPaid", Conversion.Val(rsLien.Get_Fields("PLIPaid")) + modGlobal.Round(dblPLInt, 2));
                                        rsLien.Update();
                                    }
                                }
                                else
                                {
                                    // Update Bill Record
                                    rsBillRec.Edit();
                                    rsBillRec.Set_Fields("PrincipalPaid", Conversion.Val(rsBillRec.Get_Fields_Decimal("PrincipalPaid")) + modGlobal.Round(dblCurPrin, 2));
                                    rsBillRec.Set_Fields("InterestPaid", Conversion.Val(rsBillRec.Get_Fields_Decimal("InterestPaid")) + modGlobal.Round(dblCurInt, 2));
                                    rsBillRec.Set_Fields("DemandFeesPaid", Conversion.Val(rsBillRec.Get_Fields_Decimal("DemandFeesPaid")) + modGlobal.Round(dblCurCost, 2));
                                    rsBillRec.Update();
                                }
                            }
                        }
                    }
                    else
                    {
                        // TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                        FCMessageBox.Show("Error processing account number " + FCConvert.ToString(rsBatchBackup.Get_Fields("AccountNumber")) + ", no bill found.", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Bill Error");
                    }
                    rsBatchBackup.MoveNext();
                }
                while (!rsBatchBackup.EndOfFile());
                CreateBatchCLRecords = true;
            }
            else
            {
                FCMessageBox.Show("Error loading batch payments.", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Batch Error");
                CreateBatchCLRecords = false;
            }
            return CreateBatchCLRecords;
        }

        private bool CreateBatchCHGINTRecord(ref clsDRWrapper rsTemp, ref double dblInt)
        {
            bool CreateBatchCHGINTRecord = false;
            // this will create an CHGINT interest record with a status of Pending if there is CR
            DateTime dtStartingDate;
            // this will be the beginning date of interest
            DateTime dtEffectiveInterestDate;
            clsDRWrapper rsCHGINT = new clsDRWrapper();
            string strSQL;
            lngBatchCHGINTNumber = 0;
            strSQL = "SELECT * FROM PaymentRec WHERE Account = 0";
            if (rsCHGINT.OpenRecordset(strSQL, modExtraModules.strCLDatabase))
            {
                rsCHGINT.AddNew();
                // lngBatchCHGINTNumber = rsCHGINT.Get_Fields("ID")
                // Account
                // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                rsCHGINT.Set_Fields("Account", rsTemp.Get_Fields("Account"));
                // Year
                // TODO: Check the table for the column [Year] and replace with corresponding Get_Field method
                rsCHGINT.Set_Fields("Year", rsTemp.Get_Fields("Year"));
                // BillKey
                rsCHGINT.Set_Fields("BillKey", rsTemp.Get_Fields_Int32("BillKey"));
                // CHGINTNumber
                rsCHGINT.Set_Fields("CHGINTNumber", rsTemp.Get_Fields_Int32("ID"));
                // CHGINTDate - this is the last interest date in order to backtrack (delete payments)
                rsCHGINT.Set_Fields("CHGINTDate", dtIntPaidDate);
                // ActualSystemDate
                rsCHGINT.Set_Fields("ActualSystemDate", DateTime.Today);
                // EffectiveInterestDate
                rsCHGINT.Set_Fields("EffectiveInterestDate", rsTemp.Get_Fields_DateTime("EffectiveInterestDate"));
                // RecordedTransactionDate
                rsCHGINT.Set_Fields("RecordedTransactionDate", DateTime.Today);
                // Teller
                rsCHGINT.Set_Fields("Teller", strTLRID);
                // Reference
                rsCHGINT.Set_Fields("Reference", "CHGINT");
                // Period
                rsCHGINT.Set_Fields("Period", "A");
                // Code
                rsCHGINT.Set_Fields("Code", "I");
                // ReceiptNumber
                // this has to be a check to see if it is coming from CR not just if they have it
                if (modExtraModules.IsThisCR())
                {
                    rsCHGINT.Set_Fields("ReceiptNumber", -1);
                }
                else
                {
                    rsCHGINT.Set_Fields("ReceiptNumber", 0);
                }
                // Principal
                rsCHGINT.Set_Fields("Principal", 0);
                // PreLienInterest
                rsCHGINT.Set_Fields("PreLienInterest", 0);
                // CurrentInterest
                rsCHGINT.Set_Fields("CurrentInterest", modGlobal.Round(dblXtraCurInt * -1, 2));
                // LienCost
                rsCHGINT.Set_Fields("LienCost", 0);
                // TransNumber
                rsCHGINT.Set_Fields("TransNumber", 0);
                // PaidBy
                rsCHGINT.Set_Fields("PaidBy", strPaidBy);
                // Comments
                rsCHGINT.Set_Fields("Comments", "This was a batch transaction.");
                // CashDrawer
                rsCHGINT.Set_Fields("CashDrawer", "Y");
                // GeneralLedger
                rsCHGINT.Set_Fields("GeneralLedger", "Y");
                // BudgetaryAccountNumber
                rsCHGINT.Set_Fields("BudgetaryAccountNumber", "");
                // BillCode
                rsCHGINT.Set_Fields("BillCode", rsTemp.Get_Fields_String("BillCode"));
                if (rsCHGINT.Update(false))
                {
                    // make sure that it updated
                    CreateBatchCHGINTRecord = true;
                    lngBatchCHGINTNumber = FCConvert.ToInt32(rsCHGINT.Get_Fields_Int32("ID"));
                    // kk 03152013 trocls-24 Missed during conversion
                }
                else
                {
                    FCMessageBox.Show("Error #" + FCConvert.ToString(rsCHGINT.ErrorNumber) + " - " + rsCHGINT.ErrorDescription + ".", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Batch CHGINT Error");
                    CreateBatchCHGINTRecord = false;
                }
            }
            else
            {
                FCMessageBox.Show("Error #" + FCConvert.ToString(rsCHGINT.ErrorNumber) + " - " + rsCHGINT.ErrorDescription + ".", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Batch CHGINT Error");
                CreateBatchCHGINTRecord = false;
            }
            return CreateBatchCHGINTRecord;
        }
        // VBto upgrade warning: 'Return' As DateTime	OnWriteFCConvert.ToInt16(
        private DateTime GetIntStartDate(ref int lngRateKey, ref int lngYear)
        {
            DateTime GetIntStartDate = System.DateTime.Now;
            // this will return the interest start date of the bill from the rate record in case no payments have been made
            // to set the InterestPaidThroughDate
            clsDRWrapper rsRateRec = new clsDRWrapper();
            rsRateRec.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(lngRateKey));
            if (rsRateRec.EndOfFile() != true && rsRateRec.BeginningOfFile() != true)
            {
                if (rsRateRec.Get_Fields_DateTime("InterestStartDate1") is DateTime)
                {
                    GetIntStartDate = (DateTime)rsRateRec.Get_Fields_DateTime("InterestStartDate1");
                }
            }
            if (GetIntStartDate.ToOADate() == 0)
            {
                // it should not get to this stage...but if so, this is a backup way to find the correct rate record from DOS bill records
                rsRateRec.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString((lngYear - 19800) / 10.0));
                if (rsRateRec.EndOfFile() != true && rsRateRec.BeginningOfFile() != true)
                {
                    if (rsRateRec.Get_Fields_DateTime("InterestStartDate1") is DateTime)
                    {
                        GetIntStartDate = (DateTime)rsRateRec.Get_Fields_DateTime("InterestStartDate1");
                    }
                }
                else
                {
                    GetIntStartDate = DateTime.FromOADate(0);
                }
            }
            return GetIntStartDate;
        }

        private bool CheckBatchQuestions()
        {
            bool CheckBatchQuestions = false;
            bool boolBatch = false;
            clsDRWrapper rsCheck = new clsDRWrapper();
            // check the Teller ID
            if (Strings.Trim(txtTellerID.Text) != "")
            {
                if (modGlobal.ValidateTellerID(Strings.Trim(txtTellerID.Text)) <= 1)
                {
                    boolBatch = true;
                    strTLRID = Strings.Trim(txtTellerID.Text);
                }
                else
                {
                    FCMessageBox.Show("Please enter a valid Teller ID.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Teller Validation Error");
                    boolBatch = false;
                }
            }
            else
            {
                FCMessageBox.Show("Please enter a valid Teller ID.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Teller Validation Error");
                boolBatch = false;
            }
            // check the Tax Year
            if (boolBatch)
            {
                if (Strings.Trim(txtTaxYear.Text) != "")
                {
                    if (Conversion.Val(txtTaxYear.Text) != 0)
                    {
                        rsCheck.OpenRecordset("SELECT Year FROM RateRec WHERE [Year] = " + Strings.Trim(txtTaxYear.Text), modExtraModules.strCLDatabase);
                        if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
                        {
                            boolBatch = true;
                            lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(txtTaxYear.Text)));
                        }
                        else
                        {
                            FCMessageBox.Show("There is no Rate Record for the year " + Strings.Trim(txtTaxYear.Text) + ", therefore no bills have been created.  Please enter a valid Tax Year.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Invalid Tax Year");
                            boolBatch = false;
                        }
                    }
                    else
                    {
                        FCMessageBox.Show("Please enter a valid Tax Year.", MsgBoxStyle.Information, "Invalid Tax Year");
                        boolBatch = false;
                    }
                }
                else
                {
                    FCMessageBox.Show("Please enter a valid Tax Year.", MsgBoxStyle.Information, "Invalid Tax Year");
                    boolBatch = false;
                }
            }
            // check the Payment date
            if (boolBatch)
            {
                if (Strings.Trim(txtPaymentDate2.Text) != "")
                {
                    if (Information.IsDate(txtPaymentDate2.Text))
                    {
                        if (FCConvert.ToDateTime(txtPaymentDate2.Text).Year >= lngYear - 1)
                        {
                            dtPaymentDate = DateAndTime.DateValue(txtPaymentDate2.Text);
                            boolBatch = true;
                        }
                        else
                        {
                            FCMessageBox.Show("Please enter a valid payment date.", MsgBoxStyle.Information, "Invalid Payment Date");
                            boolBatch = false;
                        }
                    }
                    else
                    {
                        FCMessageBox.Show("Please enter a valid payment date.", MsgBoxStyle.Information, "Invalid Payment Date");
                        boolBatch = false;
                    }
                }
                else
                {
                    FCMessageBox.Show("Please enter a payment date.", MsgBoxStyle.Information, "Invalid Payment Date");
                    boolBatch = false;
                }
                if (Strings.Trim(txtEffectiveDate.Text) != "")
                {
                    if (Information.IsDate(txtEffectiveDate.Text))
                    {
                        if (FCConvert.ToDateTime(txtEffectiveDate.Text).Year >= lngYear - 1)
                        {
                            dtEffectiveDate = DateAndTime.DateValue(txtEffectiveDate.Text);
                            boolBatch = true;
                        }
                        else
                        {
                            FCMessageBox.Show("Please enter a valid effective date.", MsgBoxStyle.Information, "Invalid Effective Date");
                            boolBatch = false;
                        }
                    }
                    else
                    {
                        FCMessageBox.Show("Please enter a valid effective date.", MsgBoxStyle.Information, "Invalid Effective Date");
                        boolBatch = false;
                    }
                }
                else
                {
                    FCMessageBox.Show("Please enter a effective date.", MsgBoxStyle.Information, "Invalid Effective Date");
                    boolBatch = false;
                }
            }
            // check the Paid by name
            if (boolBatch)
            {
                if (Strings.Trim(txtPaidBy.Text) != "")
                {
                    strPaidBy = Strings.Trim(txtPaidBy.Text);
                    boolBatch = true;
                }
                else
                {
                    FCMessageBox.Show("Please enter a Paid By name.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Invalid Paid By Name");
                    boolBatch = false;
                }
            }
            CheckBatchQuestions = boolBatch;
            return CheckBatchQuestions;
        }

        private void UpdatePrintReceiptChoice()
        {
            // this sub will update the BatchRecover table with the print receipt checkbox values
            clsDRWrapper rsCHK = new clsDRWrapper();
            string strPrint = "";
            int intCT;
            string strSQL = "";
            int lngErrNum = 0;
            string strErrDesc = "";
            for (intCT = 1; intCT <= vsBatch.Rows - 1; intCT++)
            {
                if (Conversion.Val(vsBatch.TextMatrix(intCT, 10)) == -1)
                {
                    strPrint = "1";
                    // "True"
                }
                else
                {
                    strPrint = "0";
                    // "False"
                }
                strSQL = "UPDATE BatchRecover Set PrintReceipt = " + strPrint + " WHERE TellerID = '" + strTLRID + "' AND BatchRecoverDate = '" + FCConvert.ToString(dtPaymentDate) + "' AND ETDate = '" + FCConvert.ToString(dtEffectiveDate) + "' AND [Year] = " + FCConvert.ToString(lngYear) + " AND AccountNumber = " + vsBatch.TextMatrix(intCT, lngColBatchAcct);
                if (!rsCHK.Execute(strSQL, modExtraModules.strCLDatabase, false))
                {
                    lngErrNum = rsCHK.ErrorNumber;
                    strErrDesc = rsCHK.ErrorDescription;
                }
            }
            if (lngErrNum != 0)
            {
                FCMessageBox.Show("Error #" + FCConvert.ToString(lngErrNum) + " - " + strErrDesc + " has occurred updating the Print Receipt Column.", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Update Error");
            }
        }

        private void CreateStatusQueries_2(int lngAcct)
        {
            CreateStatusQueries(ref lngAcct);
        }

        private void CreateStatusQueries(ref int lngAcct)
        {
            // this will create the two queries strings that the status screen will use
            string strSQL;
            // this will load all of the bills
            strSQL = "SELECT * FROM BillingMaster WHERE Account = " + FCConvert.ToString(lngAcct);
            modStatusPayments.Statics.strCurrentAccountBills = strSQL;
            // this will load all of the accounts
            strSQL = "SELECT * FROM PaymentRec WHERE Account = " + FCConvert.ToString(lngAcct);
            modStatusPayments.Statics.strCurrentAccountPayments = strSQL;
        }

        private void vsSearch_MouseMove(object sender, DataGridViewCellMouseEventArgs e)
        {
            int lngR;
            int lngC;
            lngC = vsSearch.MouseCol;
            lngR = vsSearch.MouseRow;
            // vssearch.ToolTipText =
        }

        private void vsSearch_RowColChange(object sender, EventArgs e)
        {
            if (Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6)) != 0)
            {
                txtGetAccountNumber.Text = Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6);
            }
        }

        private void FormatSearchGrid()
        {
            int lngWid;
            vsSearch.Cols = 7;
            // set the height of the grid
            //vsSearch.Height = FCConvert.ToInt32(frmCLGetAccount.InstancePtr.Height * 0.8);
            //vsSearch.Width = FCConvert.ToInt32(frmCLGetAccount.InstancePtr.Width * 0.9);
            //vsSearch.Left = FCConvert.ToInt32(((frmCLGetAccount.InstancePtr.Width - vsSearch.Width) / 2.0));
            //vsSearch.Top = FCConvert.ToInt32(frmCLGetAccount.InstancePtr.Height * 0.1);
            vsSearch.ExtendLastCol = true;
            lngWid = vsSearch.WidthOriginal;
            // set the column widths
            if (modStatusPayments.Statics.boolRE)
            {
                vsSearch.ColWidth(lngColAcct, FCConvert.ToInt32(lngWid * 0.09));
                vsSearch.ColWidth(lngColName, FCConvert.ToInt32(lngWid * 0.32));
                vsSearch.ColWidth(lngcolLocationNumber, FCConvert.ToInt32(lngWid * 0.06));
                vsSearch.ColWidth(lngColLocation, FCConvert.ToInt32(lngWid * 0.31));
                vsSearch.ColWidth(lngColBookPage, 0);
                vsSearch.ColWidth(lngColMapLot, FCConvert.ToInt32(lngWid * 0.18));
                vsSearch.ColWidth(lngColBillRecords, 0);
            }
            else
            {
                vsSearch.ColWidth(lngColAcct, FCConvert.ToInt32(lngWid * 0.09));
                vsSearch.ColWidth(lngColName, FCConvert.ToInt32(lngWid * 0.4));
                vsSearch.ColWidth(lngcolLocationNumber, FCConvert.ToInt32(lngWid * 0.06));
                vsSearch.ColWidth(lngColLocation, FCConvert.ToInt32(lngWid * 0.36));
                vsSearch.ColWidth(lngColBookPage, 0);
                vsSearch.ColWidth(lngColMapLot, 0);
                vsSearch.ColWidth(lngColBillRecords, 0);
            }
            vsSearch.ColAlignment(lngColAcct, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsSearch.ColAlignment(lngColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsSearch.ColAlignment(lngcolLocationNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsSearch.ColAlignment(lngColLocation, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsSearch.ColAlignment(lngColBookPage, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsSearch.ColAlignment(lngColMapLot, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsSearch.TextMatrix(0, lngColAcct, "Account");
            vsSearch.TextMatrix(0, lngColName, "Name");
            vsSearch.TextMatrix(0, lngcolLocationNumber, "#");
            vsSearch.TextMatrix(0, lngColLocation, "Street");
            vsSearch.TextMatrix(0, lngColBookPage, "Book/Page");
            vsSearch.TextMatrix(0, lngColMapLot, "Map/Lot");
            lblSearchListInstruction.Text = "Select an account by double clicking or pressing 'Enter' while the account is highlighted.";
            //lblSearchListInstruction.Top = 0;
            lblSearchListInstruction.Width = (vsSearch.Width + vsSearch.Left) - lblSearchListInstruction.Left;
            //lblSearchListInstruction.Left = vsSearch.Left;
        }

        private void SavePayment()
        {
            if (txtAmount.Text != "")
            {
                if (FCMessageBox.Show("Would you like to save this payment?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, "Save Payment") == DialogResult.Yes)
                {
                    if (txtAmount.DataChanged)
                    {
                        if (Conversion.Val(txtAmount.Text) != 0)
                        {
                            if (FCConvert.ToDouble(txtAmount.Text) > dblTotalDue)
                            {
                                if (FCMessageBox.Show("Payment is more than due, would you like to continue.", MsgBoxStyle.YesNoCancel, "Overpayment") != DialogResult.Yes)
                                {
                                    return;
                                }
                                else
                                {
                                    modGlobalFunctions.AddCYAEntry_8("CR", "Added CL batch overpayment. - Acct: " + txtBatchAccount.Text);
                                }
                            }
                            if (AdjustPaymentAmount_2(FCConvert.ToDouble(txtAmount.Text)) != 0)
                            {
                                return;
                                // error has occured, exit sub and do not save payment
                            }
                        }
                        else
                        {
                            FCMessageBox.Show("Payment amount must be numeric and greater than 0.", MsgBoxStyle.OkOnly | MsgBoxStyle.Information, "Invalid Payment Amount");
                            return;
                        }
                    }
                    AddPaymentToBatch_6(rsBatch, FCConvert.ToInt32(FCConvert.ToDouble(txtBatchAccount.Text)));
                }
                else
                {
                    txtBatchAccount.Focus();
                }
            }
        }

        private void FormatBatchSearchGrid(bool boolReset = false)
        {
            int lngWid = 0;
            vsBatchSearch.Cols = 4;
            lngWid = vsBatchSearch.WidthOriginal;
            if (boolReset)
            {
                vsBatchSearch.Rows = 1;
                // headers
                vsBatchSearch.TextMatrix(0, lngColBSAccount, "Acct");
                vsBatchSearch.TextMatrix(0, lngColBSKey, "");
                vsBatchSearch.TextMatrix(0, lngColBSName, "Name");
                vsBatchSearch.TextMatrix(0, lngColBSMapLot, "Map / Lot");
                vsBatchSearch.ColAlignment(lngColBSKey, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsBatchSearch.ColAlignment(lngColBSName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                vsBatchSearch.ColAlignment(lngColBSMapLot, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                // header alignment
                //vsBatchSearch.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 0, vsBatchSearch.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
            }
            // width of each column
            vsBatchSearch.ColWidth(lngColBSAccount, FCConvert.ToInt32(lngWid * 0.15));
            vsBatchSearch.ColWidth(lngColBSKey, FCConvert.ToInt32(0.0));
            vsBatchSearch.ColWidth(lngColBSName, FCConvert.ToInt32(lngWid * 0.6));
            vsBatchSearch.ColWidth(lngColBSMapLot, FCConvert.ToInt32(lngWid * 0.2));
            // this will reset the height
            //FC:FINAL:AM: don't set the height; used anchoring instead
            //if (vsBatchSearch.Rows * vsBatchSearch.RowHeight(0) + 70 > fraBatchSearchResults.Height - 500)
            //{
            //	vsBatchSearch.Height = FCConvert.ToInt32((fraBatchSearchResults.Height - 400) * 0.89);
            //	vsBatchSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
            //}
            //else
            //{
            //	vsBatchSearch.Height = vsBatchSearch.Rows * vsBatchSearch.RowHeight(0) + 70;
            //	vsBatchSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
            //}
        }
        // VBto upgrade warning: lngAcct As int	OnWrite(string)
        private void ShowAccount(int lngAcct)
        {
            // this will show the account selected in acct box
            txtBatchAccount.Text = FCConvert.ToString(lngAcct);
            fraBatchSearchResults.Visible = false;
            fraBatchSearchCriteria.Visible = false;
            fraBatch.Enabled = true;
        }

        private void FillSearchList()
        {
            // VBto upgrade warning: arrSearchList As Variant --> As string()
            // - "AutoDim"
            int lngTotal;
            int lngCT;
            lngCT = 0;
            modStatusPayments.Statics.arrSearchList = null;
            modStatusPayments.Statics.arrSearchList = new int[vsSearch.Rows - 1 + 1];
            // reset the array to the max possible
            for (lngTotal = 1; lngTotal <= vsSearch.Rows - 1; lngTotal++)
            {
                App.DoEvents();
                // If vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngTotal, lngColAcct) = 0 Then
                if (FCConvert.ToDouble(vsSearch.TextMatrix(lngTotal, lngColBillRecords)) == 0)
                {
                    // this is filled when the grid is filled a non zero value will have records
                }
                else
                {
                    modStatusPayments.Statics.arrSearchList[lngCT] = FCConvert.ToInt32(vsSearch.TextMatrix(lngTotal, lngColAcct));
                    lngCT += 1;
                }
            }
            if (lngCT > 0)
            {
                Array.Resize(ref modStatusPayments.Statics.arrSearchList, lngCT + 1);
                // resize the array to the max needed
            }
        }
        // VBto upgrade warning: lngAcct As object	OnWriteFCConvert.ToInt32(
        private int GetPointerValue(object lngAcct)
        {
            int GetPointerValue = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                int lngIndex;
                if (!FCUtils.IsNull(modStatusPayments.Statics.arrSearchList))
                {
                    for (lngIndex = 0; lngIndex <= Information.UBound(modStatusPayments.Statics.arrSearchList, 1) - 1; lngIndex++)
                    {
                        App.DoEvents();
                        if (FCConvert.ToInt32(lngAcct) == modStatusPayments.Statics.arrSearchList[lngIndex])
                        {
                            GetPointerValue = lngIndex;
                            break;
                        }
                    }
                }
                else
                {
                    GetPointerValue = -1;
                }
                return GetPointerValue;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                if (Information.Err(ex).Number == 9)
                {
                    // no message
                }
                else
                {
                    FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Finding Pointer Value");
                }
                GetPointerValue = -1;
            }
            return GetPointerValue;
        }

        private void ImportFirstAmerican()
        {
            try
            {
                string strFileName = "";
                Information.Err().Clear();
                // App.MainForm.CommonDialog1.Flags = cdlOFNNoChangeDir+cdlOFNFileMustExist+cdlOFNExplorer+cdlOFNLongNames	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                App.MainForm.CommonDialog1.CancelError = true;
                App.MainForm.CommonDialog1.FileName = "";
                App.MainForm.CommonDialog1.InitDir = FCFileSystem.Statics.UserDataFolder;
                /*? On Error Resume Next  */
                try
                {
                    App.MainForm.CommonDialog1.ShowOpen();
                }
                catch
                {
                }
                if (Information.Err().Number != 0)
                {
                    Information.Err().Clear();
                    return;
                }
                strFileName = App.MainForm.CommonDialog1.FileName;
                bool boolOpen;
                boolBatchImport = true;
                boolOpen = false;
                StreamReader ts;
                string strLine = "";
                double dblFileTot;
                int lngLoadedYear/*unused?*/;
                dblFileTot = 0;
                if (File.Exists(strFileName))
                {
                    ts = new StreamReader(strFileName);
                    boolOpen = true;
                    while ((strLine = ts.ReadLine()) != null)
                    {
                        App.DoEvents();
                        // Call ParseFirstAmerican(strLine, txtPaidBy.Text)
                        dblFileTot += TestFirstAmerican(strLine);
                    }
                    ts.Close();
                    boolOpen = false;
                    if (FCMessageBox.Show("The total file amount is " + Strings.Format(dblFileTot, "#,###,###,##0.00") + " is this correct?", MsgBoxStyle.YesNo | MsgBoxStyle.Question | MsgBoxStyle.DefaultButton2, "Confirm Amount") != DialogResult.Yes)
                    {
                        return;
                    }
                    ts = new StreamReader(strFileName);
                    boolOpen = true;
                    while ((strLine = ts.ReadLine()) != null)
                    {
                        App.DoEvents();
                        ParseFirstAmerican(strLine, txtPaidBy.Text);
                    }
                    ts.Close();
                    boolOpen = false;
                }
                else
                {
                    FCMessageBox.Show("File " + strFileName + " not found", MsgBoxStyle.Critical, "File Not Found");
                }
                boolBatchImport = false;
                txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");
                txtBatchAccount.Text = "";
                txtAmount.Text = "";
                return;
            ErrorHandler:
                ;
                if (boolOpen)
                {
                    ts.Close();
                }
            }
            catch (Exception ex)
            {
                FCMessageBox.Show("Error Number " + ex.GetBaseException().Message + "\r\n" + "In Import", MsgBoxStyle.Critical, "Error");
            }
        }

        private double TestFirstAmerican(string strLine)
        {
            double TestFirstAmerican = 0;
            // VBto upgrade warning: dblReturn As double	OnWrite(short, string)
            double dblReturn;
            dblReturn = 0;
            int lYear = 0;
            if (strLine.Length >= 47)
            {
                lYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strLine, 1, 4))));
                if (lYear > 0)
                {
                    if (modExtraModules.IsThisCR())
                    {
                        if (lYear != Conversion.Val(txtTaxYear.Text))
                        {
                            txtTaxYear.Text = FCConvert.ToString(lYear);
                        }
                    }
                }
                if (Conversion.Val(Strings.Mid(strLine, 5, 10)) > 0)
                {
                    // account is greater than 0
                    dblReturn = FCConvert.ToDouble(Strings.Format((Conversion.Val(Strings.Trim(Strings.Mid(strLine, 35, 11))) / 100), "0.00"));
                }
            }
            TestFirstAmerican = dblReturn;
            return TestFirstAmerican;
        }

        private void ParseFirstAmerican(string strLine, string strPaidName)
        {
            int lngAccount = 0;
            string strMapLot = "";
            // VBto upgrade warning: crPayment As double	OnWriteFCConvert.ToDouble(
            decimal crPayment;
            string strPaidBy = "";
            bool boolCancel = false;
            int lYear = 0;
            if (strLine.Length >= 51)
            {
                lYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strLine, 1, 4))));
                lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strLine, 5, 10))));
                if (lngAccount > 0)
                {
                    strMapLot = Strings.Trim(Strings.Mid(strLine, 15, 20));
                    crPayment = FCConvert.ToDecimal((Conversion.Val(Strings.Trim(Strings.Mid(strLine, 35, 11))) / 100));
                    if (crPayment != 0)
                    {
                        strPaidBy = Strings.Trim(Strings.Mid(strLine, 46, 10));
                        if (strPaidBy == "" || modExtraModules.IsThisCR())
                            strPaidBy = strPaidName;
                        // Call AddPaymentLine(lngAccount, intDefaultYear, dtDefaultEffDate, dtDefaultTranDate, strPaidBy, crPayment)
                        txtBatchAccount.Text = FCConvert.ToString(lngAccount);
                        if (crPayment != 0)
                        {
                            boolCancel = false;
                            txtAmount.Text = FCConvert.ToString(crPayment);
                            txtBatchAccount_Validate(boolCancel);
                            if (!boolCancel)
                            {
                                txtAmount.Text = FCConvert.ToString(crPayment);
                                if (AdjustPaymentAmount_2(FCConvert.ToDouble(crPayment)) == 0)
                                {
                                    CreateBatchPaymentLine_18(lngAccount, dblCurPrin, dblCurInt + dblPLInt, dblCurCost, dtPaymentDate, dtEffectiveDate, ref lYear);
                                }
                                else
                                {
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ImportRecords()
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // This will check the file selected and import the payments within it
                string strFileName = "";
                string strFullName = "";
                string strPathName = "";
                string strTest = "";
                int lngCT = 0;
                bool boolCancel = false;
                int lngErrorCount = 0;
                // Find the file that they want to import from
                Information.Err().Clear();
                // App.MainForm.CommonDialog1.Flags = cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                App.MainForm.CommonDialog1.CancelError = true;
                /*? On Error Resume Next  */
                App.MainForm.CommonDialog1.FileName = "";
                App.MainForm.CommonDialog1.Filter = "*.*";
                App.MainForm.CommonDialog1.InitDir = FCFileSystem.Statics.UserDataFolder;
                try
                {
                    App.MainForm.CommonDialog1.ShowOpen();
                }
                catch
                {
                }
                if (Information.Err().Number == 0)
                {
                    strFileName = Path.GetFileNameWithoutExtension(App.MainForm.CommonDialog1.FileName);
                    // name without extension
                    strFullName = Path.GetFileName(App.MainForm.CommonDialog1.FileName);
                    // name with extension
                    strPathName = Directory.GetParent(App.MainForm.CommonDialog1.FileName).FullName;
                    boolBatchImport = true;
                    // import the file
                    FCFileSystem.FileOpen(1, App.MainForm.CommonDialog1.FileName, OpenMode.Random, OpenAccess.Default, OpenShare.Default, -1);
                    lngCT += 1;
                    FCFileSystem.FileGet(1, ref CRImport, lngCT);
                    while (!FCFileSystem.EOF(1))
                    {
                        App.DoEvents();
                        txtBatchAccount.Text = FCConvert.ToString(Conversion.Val(CRImport.Account));
                        if (Conversion.Val(CRImport.Amount) != 0)
                        {
                            boolCancel = false;
                            txtAmount.Text = FCConvert.ToString(FCConvert.ToDouble(CRImport.Amount));
                            txtBatchAccount_Validate(boolCancel);
                            if (!boolCancel)
                            {
                                txtAmount.Text = FCConvert.ToString(FCConvert.ToDouble(CRImport.Amount));
                                if (AdjustPaymentAmount_2(FCConvert.ToDouble(txtAmount.Text)) == 0)
                                {
                                    CreateBatchPaymentLine_20(FCConvert.ToInt32(Conversion.Val(CRImport.Account)), dblCurPrin, dblCurInt + dblPLInt, dblCurCost, dtPaymentDate, dtEffectiveDate, ref lngYear);
                                }
                                else
                                {
                                    // error while processing
                                    lngErrorCount += 1;
                                    Array.Resize(ref arrCRImportError, lngErrorCount + 1);
                                    arrCRImportError[lngErrorCount] = CRImport;
                                    // arrCRImportError(lngErrorCount).Account = CRImport.Account
                                    // arrCRImportError(lngErrorCount).Amount = CRImport.Amount
                                    // arrCRImportError(lngErrorCount).Name = CRImport.Name
                                    // arrCRImportError(lngErrorCount).PaymentDate = CRImport.PaymentDate
                                    // arrCRImportError(lngErrorCount).Year = CRImport.Year
                                }
                            }
                            else
                            {
                                // error with the account
                                lngErrorCount += 1;
                                Array.Resize(ref arrCRImportError, lngErrorCount + 1);
                                arrCRImportError[lngErrorCount] = CRImport;
                                // arrCRImportError(lngErrorCount).Account = CRImport.Account
                                // arrCRImportError(lngErrorCount).Amount = CRImport.Amount
                                // arrCRImportError(lngErrorCount).Name = CRImport.Name
                                // arrCRImportError(lngErrorCount).PaymentDate = CRImport.PaymentDate
                                // arrCRImportError(lngErrorCount).Year = CRImport.Year
                            }
                        }
                        else
                        {
                            // error with the amount
                            lngErrorCount += 1;
                            Array.Resize(ref arrCRImportError, lngErrorCount + 1);
                            arrCRImportError[lngErrorCount] = CRImport;
                            // arrCRImportError(lngErrorCount).Account = CRImport.Account
                            // arrCRImportError(lngErrorCount).Amount = CRImport.Amount
                            // arrCRImportError(lngErrorCount).Name = CRImport.Name
                            // arrCRImportError(lngErrorCount).PaymentDate = CRImport.PaymentDate
                            // arrCRImportError(lngErrorCount).Year = CRImport.Year
                        }
                        lngCT += 1;
                        FCFileSystem.FileGet(1, ref CRImport, lngCT);
                    }
                    FCFileSystem.FileClose(1);
                    boolBatchImport = false;
                    txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");
                    txtBatchAccount.Text = "";
                    txtAmount.Text = "";
                }
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                boolBatchImport = false;
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Importing Records");
            }
        }
        // VBto upgrade warning: lngAcct As int	OnWrite(int, double)
        private void CreateBatchPaymentLine_18(int lngAcct, double dblPrin, double dblInt, double dblCost, DateTime dtDate, DateTime dtETDate, ref int lYear)
        {
            CreateBatchPaymentLine(ref lngAcct, ref dblPrin, ref dblInt, ref dblCost, ref dtDate, ref dtETDate, ref lYear);
        }

        private void CreateBatchPaymentLine_20(int lngAcct, double dblPrin, double dblInt, double dblCost, DateTime dtDate, DateTime dtETDate, ref int lYear)
        {
            CreateBatchPaymentLine(ref lngAcct, ref dblPrin, ref dblInt, ref dblCost, ref dtDate, ref dtETDate, ref lYear);
        }

        private void CreateBatchPaymentLine(ref int lngAcct, ref double dblPrin, ref double dblInt, ref double dblCost, ref DateTime dtDate, ref DateTime dtETDate, ref int lYear)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                int lngRow;
                int lngKey = 0;
                // add it to the temporary table
                // .OpenRecordset "SELECT * FROM BatchRecover", strCLDatabase
                rsBatchBackup.AddNew();
                if (lYear == 0)
                    lYear = lngYear;
                // set the ID value from the batch recover table
                // lngKey = .Get_Fields("ID")
                strRef = FCConvert.ToString(lngAcct) + "-" + FCConvert.ToString(lYear);
                // add the values to the table
                rsBatchBackup.Set_Fields("TellerID", strTLRID);
                rsBatchBackup.Set_Fields("PaidBy", strPaidBy + " ");
                rsBatchBackup.Set_Fields("Ref", strRef + " ");
                // .Get_Fields("Year") = lngYear
                rsBatchBackup.Set_Fields("year", lYear);
                rsBatchBackup.Set_Fields("AccountNumber", lngAcct);
                if (lblName.Text.Length > 6)
                {
                    rsBatchBackup.Set_Fields("Name", Strings.Right(lblName.Text, lblName.Text.Length - 6));
                }
                else
                {
                    rsBatchBackup.Set_Fields("Name", lblName.Text);
                }
                rsBatchBackup.Set_Fields("Prin", dblPrin);
                rsBatchBackup.Set_Fields("Int", dblInt);
                rsBatchBackup.Set_Fields("Cost", dblCost);
                rsBatchBackup.Set_Fields("BatchRecoverDate", dtDate);
                rsBatchBackup.Set_Fields("ETDate", dtETDate);
                rsBatchBackup.Set_Fields("Type", " ");
                // save the record
                if (!rsBatchBackup.Update(true))
                {
                    FCMessageBox.Show("Error adding record batch.  Please try again.", MsgBoxStyle.OkOnly | MsgBoxStyle.Critical, "Update Error");
                    return;
                }
                lngKey = FCConvert.ToInt32(rsBatchBackup.Get_Fields_Int32("ID"));
                vsBatch.AddItem("");
                lngRow = vsBatch.Rows - 1;
                // add it to the grid
                vsBatch.TextMatrix(lngRow, lngColBatchAcct, FCConvert.ToString(lngAcct));
                if (lblName.Text.Length > 6)
                {
                    vsBatch.TextMatrix(lngRow, lngColBatchName, Strings.Right(lblName.Text, lblName.Text.Length - 6));
                }
                else
                {
                    vsBatch.TextMatrix(lngRow, lngColBatchName, lblName.Text);
                }
                // .TextMatrix(lngRow, lngColBatchYear) = lngYear
                vsBatch.TextMatrix(lngRow, lngColBatchYear, FCConvert.ToString(lYear));
                vsBatch.TextMatrix(lngRow, lngColBatchRef, strRef);
                vsBatch.TextMatrix(lngRow, lngColBatchPrin, FCConvert.ToString(dblPrin));
                vsBatch.TextMatrix(lngRow, lngColBatchInt, FCConvert.ToString(dblInt));
                vsBatch.TextMatrix(lngRow, lngColBatchCost, FCConvert.ToString(dblCost));
                vsBatch.TextMatrix(lngRow, lngColBatchTotal, FCConvert.ToString(dblPrin + dblInt + dblCost));
                vsBatch.TextMatrix(lngRow, lngColBatchKey, FCConvert.ToString(lngKey));
                if (chkReceipt.CheckState == Wisej.Web.CheckState.Checked)
                {
                    vsBatch.TextMatrix(lngRow, lngColBatchPrintReceipt, FCConvert.ToString(-1));
                }
                if (dblCurPrin + dblCurInt + dblPLInt + dblCurCost == dblOriginalAmount)
                {
                    vsBatch.TextMatrix(lngRow, lngColBatchCorrectAmount, "Yes");
                }
                else
                {
                    vsBatch.TextMatrix(lngRow, lngColBatchCorrectAmount, "No");
                }
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Batch Records");
            }
        }

        private void PrintBatchReceipts()
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                int lngRW;
                for (lngRW = 1; lngRW <= vsBatch.Rows - 1; lngRW++)
                {
                    if (Conversion.Val(vsBatch.TextMatrix(lngRW, lngColBatchPrintReceipt)) == -1)
                    {
                        arBatchReceipt.InstancePtr.Init(lngRW);
                        // Doevents
                        //arBatchReceipt.InstancePtr.Hide();
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                // ERROR_HANDLER:
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Creating Batch Receipts");
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            mnuProcessGetAccount_Click();
        }

        private void cmdProcessClearSearch_Click(object sender, EventArgs e)
        {
            this.mnuProcessClearSearch_Click(sender, e);
        }

        private void mnuFileImportPayportBatch_Click(object sender, EventArgs e) //kk01252018 troges-88        CODE FREEZE TROGES-88
        {
            ImportPayportBatch();
        }

        //CODE FREEZE TROGES-88
        //kk01242018 troges-88  Add Payport Import to the CL Batch Payment process
        private void ImportPayportBatch()
        {

            string strFileName;
            bool boolOpen = false;
            StreamReader ts = null;
            Information.Err().Clear();
            //MDIParent.InstancePtr.CommonDialog1.Flags = cdlOFNNoChangeDir + cdlOFNFileMustExist + cdlOFNExplorer + cdlOFNLongNames;
            MDIParent.InstancePtr.CommonDialog1.CancelError = true;
            MDIParent.InstancePtr.CommonDialog1.FileName = "";
            MDIParent.InstancePtr.CommonDialog1.InitDir = Environment.CurrentDirectory;
            try
            {
                MDIParent.InstancePtr.CommonDialog1.ShowOpen();
                if (Information.Err().Number != 0)
                {
                    Information.Err().Clear();
                    return;
                }
                strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
                boolBatchImport = true;
                string strLine;
                double dblFileTot = 0;
                int lngLoadedYear;
                if (FCFileSystem.FileExists(strFileName))
                {
                    ts = FCFileSystem.OpenText(strFileName);
                    boolOpen = true;
                    while (!ts.EndOfStream)
                    {
                        //Application.DoEvents();
                        strLine = ts.ReadLine();
                        if (Strings.UCase(Strings.Left(strLine, 6)) != "BILLID")
                        {
                            dblFileTot = dblFileTot + TestPayport(strLine);
                        }
                    }
                    ts.Close();
                    boolOpen = false;
                    if (MessageBox.Show("The total file amount is " + Strings.Format(dblFileTot, "#,###,###,##0.00") + " is this correct?", "Confirm Amount", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                    {
                        return;
                    }
                    ts = FCFileSystem.OpenText(strFileName);
                    boolOpen = true;
                    while (!ts.EndOfStream)
                    {
                        strLine = ts.ReadLine();
                        if (Strings.UCase(Strings.Left(strLine, 6)) != "BILLID")
                        {
                            ParsePayport(strLine, txtPaidBy.Text);
                        }
                        ts.Close();
                        boolOpen = false;
                    }
                }
                else
                {
                    MessageBox.Show("File " + strFileName + " not found", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                boolBatchImport = false;
                txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");
                txtBatchAccount.Text = "";
                txtAmount.Text = "";
            }
            catch (Exception ex)
            {
                if (boolOpen)
                {
                    ts.Close();
                }
                MessageBox.Show("Error Number " + Information.Err().Description + Environment.NewLine + "In Import", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private double TestPayport(string strLine)
        {
            double dblReturn = 0;
            string[] arrDet;
            int lYear = 0;
            dblReturn = 0;
            arrDet = Strings.Split(strLine, "|");

            lYear = FCConvert.ToInt32(Conversion.Val(Strings.Left(arrDet[2], 4)));

            if (lYear > 0)
            {
                if (modExtraModules.IsThisCR())
                {
                    if (lYear != FCConvert.ToInt32(txtTaxYear.Text))
                    {
                        txtTaxYear.Text = lYear.ToString();
                    }
                }
            }

            if (Conversion.Val(arrDet[3]) > 0)
            {
                //account is greater than 0
                dblReturn = Conversion.Val(arrDet[5]);
            }
            return dblReturn;
        }

        private void ParsePayport(string strLine, string strPaidName)
        {
            string[] arrDet;
            int lngAccount;
            int lYear;
            decimal crPayment = 0;
            string strPaidBy = "";
            bool boolCancel;
            //Payment detail format "|" Delimited record - Bill ID|Bill Type|Year|Account|Pmt Date|Pmt Amount|Name
            arrDet = Strings.Split(strLine, "|");
            lYear = FCConvert.ToInt32(Conversion.Val(Strings.Left(arrDet[2], 4)));
            lngAccount = FCConvert.ToInt32(Conversion.Val(arrDet[3]));
            if (lngAccount > 0)
            {
                crPayment = FCConvert.ToDecimal(Conversion.Val(Strings.Trim(arrDet[5])));
            }
            if (crPayment != 0)
            {
                strPaidBy = Strings.Trim(arrDet[6]);
            }
            if (strPaidBy == "" || modExtraModules.IsThisCR()) strPaidBy = strPaidName;
            txtBatchAccount.Text = lngAccount.ToString();
            if (crPayment != 0)
            {
                boolCancel = false;
                txtAmount.Text = crPayment.ToString();
                txtBatchAccount_Validate(boolCancel);
                if (!boolCancel)
                {
                    txtAmount.Text = crPayment.ToString();
                    if (AdjustPaymentAmount_8(Conversion.Val(crPayment)) == 0)
                    {
                        CreateBatchPaymentLine_20(lngAccount, dblCurPrin, dblCurInt + dblPLInt, dblCurCost, dtPaymentDate, dtEffectiveDate, ref lYear);
                    }
                }
            }
        }
    }
}
