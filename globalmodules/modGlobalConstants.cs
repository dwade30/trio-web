﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;

namespace Global
{
	public class modGlobalConstants
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :                                       *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/14/2004              *
		// ********************************************************
		public const string DATABASEPASSWORD = "TRIO2001";
		public const uint HKEY_CURRENT_USER = 0x80000001;
		public const string REGISTRYKEY = "SOFTWARE\\TrioVB\\";
		public const string Provider = "Microsoft.Jet.OLEDB.3.51";
		public const int EleventyBillion = 2147483647;
		public const int TRIOWINDOWSIZEBIGGIE = 0;
		public const int TRIOWINDOWSIZEMEDIUM = 1;
		public const int TRIOWINDOWSIZESMALL = 2;
		public const int TRIOWINDOWMINI = 3;
		public const int TRIOWINDOWSIZEMEDIUMSHORT = 4;

		public enum InputDTypes
		{
			// this is used by frmInput
			idtString = 1,
			idtDate = 2,
			idtNumber = 3,
			idtWholeNumber = 4,
			idtPhoneNumber = 5,
		}
		// MAL@20090119: Browse for folder option
		public struct BrowseInfo
		{
			public int hWndOwner;
			public int pIDLRoot;
			public int pszDisplayName;
			public int lpszTitle;
			public int ulFlags;
			public int lpfnCallback;
			public int lParam;
			public int iImage;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public BrowseInfo(int unusedParam)
			{
				this.hWndOwner = 0;
				this.pIDLRoot = 0;
				this.pszDisplayName = 0;
				this.lpszTitle = 0;
				this.ulFlags = 0;
				this.lpfnCallback = 0;
				this.lParam = 0;
				this.iImage = 0;
			}
		};

		public class StaticVariables
		{
			// Public wrkWorkSpace                     As Workspace    'for use in clsDRWrapper
			public bool boolMaxForms;
			// Municipality Name
			public string gstrArchiveYear = "";
			public clsTrioSecurity secUser;
			//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
			//public clsTrioSecurity clsSecurityClass = new clsTrioSecurity();
			public clsTrioSecurity clsSecurityClass_AutoInitialized;

			public clsTrioSecurity clsSecurityClass
			{
				get
				{
					if (clsSecurityClass_AutoInitialized == null)
					{
						clsSecurityClass_AutoInitialized = new clsTrioSecurity();
					}
					return clsSecurityClass_AutoInitialized;
				}
				set
				{
					clsSecurityClass_AutoInitialized = value;
				}
			}

			public string gstrEntryFlag = "";
			public string gstrUserID = "";
			// this will hold the user id that signed into TRIO
			public string gstrPassAccountFromForm = string.Empty;
			public bool gboolUseRegistryOnly;
			public bool FieldsAdded;
			public bool gboolASKPermission;
			public string gstrDeptDiv = "";
			public bool gboolPrintALLPayRuns;
			public bool blnAddCancel;
			public bool blnHasRecords;
			//public DateTime dtOldInterestDate;
			// MAL@20071026
			public bool gblnShowControlFields;
			// MAL@20080107
			public bool gblnAutoPrepayments;
			// MAL@20080116
			public int gintApplyPrepayTo;
			// MAL@20080122
			public bool gblnSortTypeAlpha;
			// MAL@20080123
			public bool gblnMVIsOpen;
			// MAL@20080414
			public bool gblnShowTaxAcquiredCaption;
			// MAL@20080624 ; Tracker Reference: 14371
			public bool gblnShowReceiptNumonDetail;
			// MAL@20080718 ; Tracker Reference: 11810
			public bool gblnAutoUpdateRETaxAcq;
			// MAL@20080812 ; Tracker Reference: 14035
			public bool gblnCheckRETaxAcq;
			// DJW@20090720
			public bool gblnRecreate;
			// VBto upgrade warning: PrintWidth As string	OnWrite(int, double, bool)	OnReadFCConvert.ToInt32(
			public float PrintWidth = 0;

			// TRIO Colors
			public int TRIOCOLORGRAYEDOUT;
			// dark gray
			public int TRIOCOLORBLACK;
			// VBto upgrade warning: TRIOCOLORGRAYBACKGROUND As int	OnWrite(Color, int)
			// black
			public int TRIOCOLORGRAYBACKGROUND;
			// light gray
			public int TRIOCOLORRED;
			// red
			public int TRIOCOLORBLUE;
			// blue
			public int TRIOCOLORHIGHLIGHT;
			// yellow highlight color
			public int TRIOCOLORFORECOLOR;
			// black
			public int TRIOCOLORDISABLEDOPTION;
			// VBto upgrade warning: TRIOCOLORGRAYEDOUTTEXTBOX As int	OnWrite(Color, int)
			// gray - a little darker than TRIOCOLORGRAYBACKGROUND
			public int TRIOCOLORGRAYEDOUTTEXTBOX;
			// light gray for the background color of a disabled textbox
			public bool gboolUse256Colors;
			// this is True if the user has CR, false otherwise
			// true if the user has their video settings at 256 colors
			public bool gboolAR;
			// Accounts Receivable
			public bool gboolBD;
			// Budgetary
			public bool gboolBL;
			// Billing
			public bool gboolCE;
			// Code Enforcement
			public bool gboolCL;
			// Collections
			public bool gboolCK;
			// Clerk
			public bool gboolCR;
			// Cash Receiptions
			public bool gboolE9;
			// Enhanced 911
			public bool gboolFA;
			// Fixed Assests
			public bool gboolHR;
			// Human Resources
			public bool gboolIV;
			// Inventory
			public bool gboolMS;
			// MOSES
			public bool gboolMV;
			// Motor Vehicle
			public bool gboolPP;
			// Personal Property
			public bool gboolPY;
			// Payroll
			public bool gboolRB;
			// Red / Blue / Black Book
			public bool gboolRE;
			// Real Estate
			public bool gboolTS;
			// Tax Service in TWCL
			public bool gboolUT;
			// Utility Billing
			public bool gboolVR;
			// MAL@20080806 ; Tracker Reference: 10680
			// Voter Registration
			public bool gboolIC;
			// IConnect Interface
			public bool gboolInvCld;
			
			public string MuniName = "";
			public string gstrCityTown = "";
			public string gstrTownSealPath = "";
            public bool gboolPayPort = false;
        }

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
