﻿using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using SharedApplication;
using System;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using SharedApplication.Extensions;
using TWSharedLibrary;
using Wisej.Core;
using Wisej.Web;
namespace Global
{
    public class modGlobalFunctions
    {
        //public static cGlobalSettings gGlobalSettings
        //{
        //	get
        //	{
        //		if (modGlobalFunctions.Statics.gGlobalSettings_AutoInitialized == null)
        //		{
        //			modGlobalFunctions.Statics.gGlobalSettings_AutoInitialized = new cGlobalSettings();
        //		}
        //		return modGlobalFunctions.Statics.gGlobalSettings_AutoInitialized;
        //	}
        //	set
        //	{
        //		modGlobalFunctions.Statics.gGlobalSettings_AutoInitialized = value;
        //	}
        //}
        // VBto upgrade warning: strModule As object	OnWrite(string)
        // VBto upgrade warning: intCode1 As short	OnWriteFCConvert.ToInt32(
        // VBto upgrade warning: intCode2 As short	OnWriteFCConvert.ToInt32(
        // VBto upgrade warning: intCode3 As short	OnWriteFCConvert.ToInt32(
        public static void GetExemptCodesandDescriptions(object strModule, int lngAccountNumber, ref int intCode1, ref string strDesc1, ref int intCode2, ref string strDesc2, ref int intCode3, ref string strDesc3)
        {
            // accepts RE or PP as module and then sends back the exempt codes and descriptions
            try
            {
                // On Error GoTo ErrorHandler
                clsDRWrapper clsLoad = new clsDRWrapper();
                int intC1 = 0;
                int intC2 = 0;
                int intC3 = 0;
                bool boolSkip = false;
                intCode1 = 0;
                intCode2 = 0;
                intCode3 = 0;
                strDesc1 = "";
                strDesc2 = "";
                strDesc3 = "";
                if (Strings.UCase(FCConvert.ToString(strModule)) == "PP")
                {
                    clsLoad.OpenRecordset("select exemptcode1,exemptcode2 from ppmaster where account = " + FCConvert.ToString(lngAccountNumber), "TWPP0000.vb1");
                    if (!clsLoad.EndOfFile())
                    {
                        // TODO: Check the table for the column [exemptcode1] and replace with corresponding Get_Field method
                        intC1 = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("exemptcode1"))));
                        // TODO: Check the table for the column [exemptcode2] and replace with corresponding Get_Field method
                        intC2 = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("exemptcode2"))));
                        if (intC1 > 0 || intC2 > 0)
                        {
                            clsLoad.OpenRecordset("select * from exemptcodes order by code", "TWPP0000.vb1");
                            if (!clsLoad.EndOfFile())
                            {
                                if (intC1 > 0)
                                {
                                    // If clsLoad.FindFirstRecord("code", intC1) Then
                                    if (clsLoad.FindFirst("code = " + FCConvert.ToString(intC1)))
                                    {
                                        intCode1 = intC1;
                                        strDesc1 = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
                                    }
                                }
                                if (intC2 > 0)
                                {
                                    if (clsLoad.FindFirst("code = " + FCConvert.ToString(intC2)))
                                    {
                                        intCode2 = intC2;
                                        strDesc2 = FCConvert.ToString(clsLoad.Get_Fields_String("description"));
                                    }
                                }
                            }
                        }
                    }
                }
                else if (Strings.UCase(FCConvert.ToString(strModule)) == "RE")
                {
                    clsLoad.OpenRecordset("select riexemptcd1,riexemptcd2,riexemptcd3 from master where rsaccount = " + FCConvert.ToString(lngAccountNumber) + " and rscard = 1", "TWRE0000.vb1");
                    if (!clsLoad.EndOfFile())
                    {
                        intC1 = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("riexemptcd1"))));
                        intC2 = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("riexemptcd2"))));
                        intC3 = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("riexemptcd3"))));
                        if (intC1 > 0 || intC2 > 0 || intC3 > 0)
                        {
                            clsLoad.OpenRecordset("select * from exemptcode order by code", "twre0000.vb1");
                            boolSkip = false;
                            if (intC1 > 0)
                            {
                                if (clsLoad.FindFirst("code = " + FCConvert.ToString(intC1)))
                                {
                                    intCode1 = intC1;
                                    strDesc1 = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
                                }
                            }
                            if (intC2 > 0)
                            {
                                if (!boolSkip)
                                {
                                    if (clsLoad.FindFirst("code = " + FCConvert.ToString(intC2)))
                                    {
                                        intCode2 = intC2;
                                        strDesc2 = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
                                    }
                                }
                                else
                                {
                                    boolSkip = false;
                                }
                            }
                            else
                            {
                                boolSkip = false;
                            }
                            if (intC3 > 0 && !boolSkip)
                            {
                                if (clsLoad.FindFirst("code = " + FCConvert.ToString(intC3)))
                                {
                                    intCode3 = intC3;
                                    strDesc3 = Strings.Trim(FCConvert.ToString(clsLoad.Get_Fields_String("description")));
                                }
                            }
                        }
                    }
                }
                else
                {
                    return;
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In GetExemptCodesandDescriptions", MsgBoxStyle.Critical, "Error");
            }
        }
        // This is a TRIO Global Routines module
        //public static void CollapseRows(short intLevel, FCGrid vsGrid, short Max_Levels = 8)
        //{
        //    // this will collapse the grid to the level passed in intLevel, a negative value will open the grid until the level passed in intLevel
        //    // passing in a smaller Max_Levels value will increase the efficiency of the sub during the collapse routine
        //    int Row;
        //    int intCurLevel;
        //    if (vsGrid == null)
        //    {
        //        if (intLevel >= 0)
        //        {
        //            // collapse
        //            for (intCurLevel = Max_Levels; intCurLevel >= intLevel; intCurLevel--)
        //            {
        //                // cycle through the levels to collapse
        //                for (Row = 1; Row <= vsGrid.Rows - 1; Row++)
        //                {
        //                    // start at the lowest level and start closing rows
        //                    if (vsGrid.RowOutlineLevel(Row) == intCurLevel)
        //                    {
        //                        if (vsGrid.IsCollapsed(Row) != FCGrid.CollapsedSettings.flexOutlineCollapsed)
        //                        {
        //                            vsGrid.IsCollapsed(Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            intLevel = Math.Abs(intLevel);
        //            // expand
        //            for (intCurLevel = 0; intCurLevel <= intLevel; intCurLevel++)
        //            {
        //                // cycle through the levels to expand
        //                for (Row = 1; Row <= vsGrid.Rows - 1; Row++)
        //                {
        //                    // start expanding rows
        //                    if (vsGrid.RowOutlineLevel(Row) == intCurLevel)
        //                    {
        //                        if (vsGrid.IsCollapsed(Row) != FCGrid.CollapsedSettings.flexOutlineExpanded)
        //                        {
        //                            vsGrid.IsCollapsed(Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        public static int HelpShell(string Program, AppWinStyle ShowCmd = AppWinStyle.NormalNoFocus, string WorkDir = "")
        {
            int HelpShell = 0;
            int FirstSpace = 0, Slash;
            if (Strings.Left(Program, 1) == "\"")
            {
                FirstSpace = Strings.InStr(2, Program, "\"");
                if (FirstSpace != 0)
                {
                    Program = Strings.Mid(Program, 2, FirstSpace - 2) + Strings.Mid(Program, FirstSpace + 1);
                    FirstSpace -= 1;
                }
            }
            else
            {
                FirstSpace = Strings.InStr(Program, " ", CompareConstants.vbTextCompare/*?*/);
            }
            if (FirstSpace == 0)
                FirstSpace = Program.Length + 1;
            if (Information.IsNothing(WorkDir))
            {
                for (Slash = FirstSpace - 1; Slash >= 1; Slash--)
                {
                    if (Strings.Mid(Program, Slash, 1) == "\\")
                        break;
                }
                if (Slash == 0)
                {
                    WorkDir = FCFileSystem.Statics.UserDataFolder;
                }
                else if (Slash == 1 || Strings.Mid(Program, Slash - 1, 1) == ":")
                {
                    WorkDir = Strings.Left(Program, Slash);
                }
                else
                {
                    WorkDir = Strings.Left(Program, Slash - 1);
                }
            }
            HelpShell = modAPIsConst.ShellExecute(0, string.Empty, Strings.Left(Program, FirstSpace - 1), Strings.LTrim(Strings.Mid(Program, FirstSpace)), WorkDir, FCConvert.ToInt32(ShowCmd));
            if (HelpShell < 32)
                Interaction.Shell(Program, System.Diagnostics.ProcessWindowStyle.Normal);
            // To raise Error
            return HelpShell;
        }
        // VBto upgrade warning: Grid As vsFlexGrid	OnWrite(VSFlex6DAOCtl.vsFlexGrid)
        //public static void AutoSizeMenu_6(FCGrid Grid, int intLabels)
        //{
        //    AutoSizeMenu(ref Grid, ref intLabels);
        //}

        //public static void AutoSizeMenu(ref FCGrid Grid, ref int intLabels)
        //{
        //    int i;
        //    // VBto upgrade warning: FullRowHeight As int	OnWriteFCConvert.ToDouble(
        //    int FullRowHeight = 0;
        //    // this will store the height of the labels with text
        //    // .RowHeight(0) = 0                   'the first row in the grid is height = 0
        //    FullRowHeight = FCConvert.ToInt32(Grid.HeightOriginal / intLabels);
        //    // find the average height of the row
        //    for (i = 1; i <= Grid.Rows - 1; i++)
        //    {
        //        // this just assigns the row height
        //        Grid.RowHeight(i, FullRowHeight);
        //    }
        //    Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        //    Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
        //}

        public static void IncrementSavedReports(string strName)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will move the reports back one number so that 9 reports can be saved in chronological order
                // after using this function all you have to do is save your report in as RPT\<strName>1.RDF
                int intCT;
                //FCFileSystem fso = new FCFileSystem();
                string strFile = "";
                if (ReportDirectoryStatus())
                {
                    for (intCT = 8; intCT >= 1; intCT--)
                    {
                        // this steps back from the second to last and copies it to the number above it
                        strFile = System.IO.Path.Combine("RPT", strName + FCConvert.ToString(intCT) + ".RDF");
                        if (File.Exists(strFile))
                        {
                            File.Copy(strFile, System.IO.Path.Combine("RPT", strName + FCConvert.ToString(intCT + 1) + ".RDF"), true);
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message, MsgBoxStyle.Critical, "Directory Creation Error");
            }
        }

        public static bool ReportDirectoryStatus()
        {
            bool ReportDirectoryStatus = false;
            try
            {
                // this function will return true if it is ready to save the file
                // if the folder does not exist, then this function will create the
                // directory and still return true unless an error occurs
                if (System.IO.Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
                {
                    ReportDirectoryStatus = true;
                }
                else
                {
                    System.IO.Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
                    ReportDirectoryStatus = true;
                }
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                ReportDirectoryStatus = false;
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message, MsgBoxStyle.Critical, "Directory Creation Error");
            }
            return ReportDirectoryStatus;
        }

        public static int GetGroupNumber_6(int lngAccountNumber, string strModule)
        {
            return GetGroupNumber(lngAccountNumber, ref strModule);
        }

        public static int GetGroupNumber(int lngAccountNumber, ref string strModule)
        {
            int GetGroupNumber = 0;
            // this function will return the groupnumber if it is found for the specific account in the specific module
            // it will return a 0 if no group is found
            // 
            // This is the Group Number (GroupMaster.GroupNumber) not the Group ID (GroupMaster.ID and ModuleAssociation.GroupNumber)
            // 
            try
            {
                // On Error GoTo ERROR_HANDLER
                clsDRWrapper rsGroup = new clsDRWrapper();
                string strSQL;
                strSQL = "SELECT *, GroupMaster.GroupNumber as GMGroupNumber FROM ModuleAssociation INNER JOIN GroupMaster ON ModuleAssociation.GroupNumber = GroupMaster.ID WHERE Module = '" + strModule + "' AND Account = " + FCConvert.ToString(lngAccountNumber);
                if (rsGroup.OpenRecordset(strSQL, "CentralData"))
                {
                    if (rsGroup.EndOfFile() != true && rsGroup.BeginningOfFile() != true)
                    {
                        GetGroupNumber = FCConvert.ToInt32(rsGroup.Get_Fields_Int32("GMGroupNumber"));
                    }
                    else
                    {
                        // check for an association
                        GetGroupNumber = 0;
                    }
                }
                return GetGroupNumber;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                GetGroupNumber = 0;
            }
            return GetGroupNumber;
        }

        //public static string ShowGroupMessage(ref int lngGN)
        //{
        //    string ShowGroupMessage = "";
        //    try
        //    {
        //        // On Error GoTo ERROR_HANDLER
        //        // this will check the group for a comment and return it as a string
        //        clsDRWrapper rsGroup = new clsDRWrapper();
        //        ShowGroupMessage = "";
        //        rsGroup.OpenRecordset("SELECT * FROM GroupMaster WHERE GroupNumber = " + FCConvert.ToString(lngGN), "CentralData");
        //        if (rsGroup.EndOfFile() != true && rsGroup.BeginningOfFile() != true)
        //        {
        //            if (Strings.Trim(FCConvert.ToString(rsGroup.Get_Fields_String("Comment")) + " ") != "")
        //            {
        //                ShowGroupMessage = Strings.Trim(FCConvert.ToString(rsGroup.Get_Fields_String("Comment")));
        //            }
        //        }
        //        return ShowGroupMessage;
        //    }
        //    catch (Exception ex)
        //    {
                
        //        ShowGroupMessage = "";
        //    }
        //    return ShowGroupMessage;
        //}

        public static void LoadTRIOColors()
        {
            int intCT;
            // this will check to see if the user has their monitor set to 256 colors
            // if so, then a flag will be set and TRIO COLORS will not be used
            intCT = 1;
            modGlobalConstants.Statics.gboolUse256Colors = FCConvert.CBool(modAPIsConst.GetDeviceCaps(modAPIsConst.GetDC(0), 12) <= 8);
            intCT = 2;
        USE256COLORS:
            ;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will set all of the TRIO Color variables to the hex number
                // of the color desired  This function should be called in every MDIParent_Load
                intCT = 3;
                modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT = Information.RGB(99, 99, 99);
                // dark gray
                modGlobalConstants.Statics.TRIOCOLORBLACK = Information.RGB(0, 0, 1);
                // black
                modGlobalConstants.Statics.TRIOCOLORRED = Information.RGB(255, 0, 0);
                // red
                modGlobalConstants.Statics.TRIOCOLORBLUE = Information.RGB(45, 46, 188);
                // blue
                modGlobalConstants.Statics.TRIOCOLORFORECOLOR = Information.RGB(0, 0, 1);
                // black
                modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION = Information.RGB(150, 150, 150);
                // gray - a little darker than TRIOCOLORGRAYBACKGROUND
                intCT = 4;
                if (modGlobalConstants.Statics.gboolUse256Colors)
                {
                    modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX = ColorTranslator.ToOle(App.MainForm.BackColor);
                    modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND = ColorTranslator.ToOle(App.MainForm.BackColor);
                    modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT = ColorTranslator.ToOle(Color.Yellow);
                }
                else
                {
                    modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX = Information.RGB(215, 215, 215);
                    // light gray for the background color of a disabled textbox
                    modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND = Information.RGB(190, 190, 190);
                    // light gray
                    modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT = Information.RGB(254, 247, 122);
                    // RGB(254, 254, 204)         'yellow highlight color
                }
                // TRIOCOLORGRAYBACKGROUND = RGB(255, 255, 255)
                intCT = 5;
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, null);
                modGlobalConstants.Statics.gboolUse256Colors = true;
                goto USE256COLORS;
            }
        }

        public static void SetTRIOColors(Form frmName, bool boolUseFontResizer = true)
        {
            //FC:FINAL:AM: not needed anymore
            return;

        }
        // SetFixed Size and Form Exist are for using the MDI Parent Window and will size each form
        // to fit within the window and check to see if the form is already loaded respectively
        // VBto upgrade warning: FormName As Form	OnWrite(frmPrintTaxClub, frmReminderMailer, frmFreeReport, frmLoadBack, frmRateRecChoice, frmAbout, frmReminderNotices, frmTransferTaxToLien, frmGetInterestedParties, frmMortgageHolder, frmMortgageSearch, frmAuditInfo, frmLienExclusion, frmCustomLabels, frmReprintCMForm, frmRemoveLien, frmRemove30DNFee, frmReverseDemandFees, frmRPTViewer, frmNumPages, frmSelectRateKey, frmPrintLienDates, frmTaxAcquiredRemoval, frmTaxClubBooklet, frmLienDischargeGetAcct, frmCLEditBillInfo, frmDateChange, frmCertOfSettlement, frmCLDailyAudit, frmReprintPurgeList, frmRemoveLienMatFee, frmQuestion, frmLienPartialPayment, frmDatabaseExtract, frmLienDates, frmCustomize, frmTaxRateReport, frmCLGetAccount, frmPrintDuplex, frmPrePayYear, frmTaxService, frmLienDischargeNoticeBP, frmPurge, frmCustomReport, frmEditBookPage, frmLienDischargeNotice, frmUpdateAddresses, frmTaxAcquiredList, frmCLComment, frmLoadValidAccounts, frmSignaturePassword, frmApplyDemandFees, frmEMail, frmEmailChooseContactGroup, frmEmailConfiguration, frmEmailContacts, frmEmailGroups, frmEmailSignatures, frmListChoiceWithIDs, frmEmailGetEmailID, frmLienMaturityFees, frmRECLStatus, frmRECLComment, frmInterestedParties, frmGetMortgage, frmGetAddress, frmBookPageReport, frmTaxClub, frmReprintPartialPaymentWaiver, frmImportBatch, frmProcessBatch, frmUnifund, frmCentralPartySearch, frmEditCentralParties, frmPartyList, frmLienFeeIncr, frmSelectBankNumber, frmMultiModulePostingOpID, frmSelectPostingJournal)
        // VBto upgrade warning: intFormSize As short	OnWriteFCConvert.ToInt32(
        public static void SetFixedSize(Form FormName, int intFormSize = 0)
        {
            // The form is passed into the routine as well as a boolean that will determine if the
            // form is set to the normal size or to a smaller size (about 1/4 the screen size)
            //apply only for modal forms
            if (!FormName.Modal)
            {
                return;
            }
            switch (intFormSize)
            {
                case modGlobalConstants.TRIOWINDOWSIZEBIGGIE:
                    {
                        //// large sized form
                        //if (modGlobalConstants.Statics.boolMaxForms)
                        //{
                        //    FormName.WindowState = FormWindowState.Maximized;
                        //}
                        //else
                        //{
                        //    if (App.MainForm.Width - App.MainForm.GRID.Width - 250 > 0)
                        //    {
                        //        FormName.Width = App.MainForm.Width - App.MainForm.GRID.Width - 250;
                        //    }
                        //    else
                        //    {
                        //        App.MainForm.WindowState = FormWindowState.Maximized;
                        //        FormName.Width = App.MainForm.Width - App.MainForm.GRID.Width - 250;
                        //    }
                        //    FormName.Height = App.MainForm.GRID.Height - 100;
                        //    FormName.Left = App.MainForm.GRID.Left + App.MainForm.GRID.Width + 50;
                        //    FormName.Top = App.MainForm.GRID.Top + 600;
                        //}
                        //add Mdi TabControl header height to Top position
                        FormName.Top = App.MainForm.MdiClient.Top + 46;
                        FormName.Left = App.MainForm.MdiClient.Left;
                        FormName.Width = App.MainForm.MdiClient.Width;
                        //extract Mdi TabControl header height from height
                        FormName.Height = App.MainForm.MdiClient.Height - 46;
                        break;
                    }
                    //	case modGlobalConstants.TRIOWINDOWSIZEMEDIUM:
                    //		{
                    //			// medium
                    //			// if boolLargeForm = False then the form will be sized smaller
                    //			// than the normal window
                    //			// size the form to 1/4 of the size of the screen
                    //			FormName.Width = FCConvert.ToInt32(Math.Abs(App.MainForm.Width - App.MainForm.GRID.Width) * 0.6543);
                    //			FormName.Height = FCConvert.ToInt32((App.MainForm.GRID.Height) * 0.5902);
                    //			// center the form
                    //			FormName.Left = App.MainForm.GRID.Left + App.MainForm.GRID.Width + 50;
                    //			FormName.Top = ((FCGlobal.Screen.Height - FormName.Height) / 2) - 100;
                    //			break;
                    //		}
                    //	case modGlobalConstants.TRIOWINDOWSIZEMEDIUMSHORT:
                    //		{
                    //			FormName.Width = FCConvert.ToInt32(Math.Abs(App.MainForm.Width - App.MainForm.GRID.Width) * 0.6543);
                    //			FormName.Height = FCConvert.ToInt32((App.MainForm.GRID.Height) * 0.3508);
                    //			// center the form
                    //			FormName.Left = App.MainForm.GRID.Left + App.MainForm.GRID.Width + 50;
                    //			FormName.Top = ((FCGlobal.Screen.Height - FormName.Height) / 2) - 100;
                    //			break;
                    //		}
                    //	case modGlobalConstants.TRIOWINDOWSIZESMALL:
                    //		{
                    //			// small form
                    //			// size the form to 1/4 of the size of the screen
                    //			if (App.MainForm.Width - App.MainForm.GRID.Width - 250 > 0)
                    //			{
                    //				FormName.Width = FCConvert.ToInt32(Math.Abs(App.MainForm.Width - App.MainForm.GRID.Width) * 0.4246);
                    //			}
                    //			else
                    //			{
                    //				// MDIParent.WindowState = FormWindowState.Maximized
                    //				FormName.Width = FCConvert.ToInt32(Math.Abs(App.MainForm.Width - App.MainForm.GRID.Width) * 0.4246);
                    //			}
                    //			FormName.Height = FCConvert.ToInt32((App.MainForm.GRID.Height) * 0.3508);
                    //			// center the form
                    //			FormName.Left = (FCGlobal.Screen.Width - FormName.Width) / 2;
                    //			FormName.Top = ((FCGlobal.Screen.Height - FormName.Height) / 2) - 100;
                    //			break;
                    //		}
                    //	case modGlobalConstants.TRIOWINDOWMINI:
                    //		{
                    //			// mini form
                    //			// size the form to 1/4 of the size of the screen
                    //			if (App.MainForm.Width - App.MainForm.GRID.Width - 250 > 0)
                    //			{
                    //				FormName.Width = FCConvert.ToInt32((App.MainForm.Width - App.MainForm.GRID.Width) * 0.32);
                    //			}
                    //			else
                    //			{
                    //				// MDIParent.WindowState = FormWindowState.Maximized
                    //				FormName.Width = FCConvert.ToInt32((App.MainForm.Width - App.MainForm.GRID.Width) * 0.32);
                    //			}
                    //			FormName.Height = FCConvert.ToInt32((App.MainForm.GRID.Height) * 0.2282);
                    //			// center the form
                    //			FormName.Left = (FCGlobal.Screen.Width - FormName.Width) / 2;
                    //			FormName.Top = ((FCGlobal.Screen.Height - FormName.Height) / 2) - 100;
                    //			break;
                    //		}
            } //end switch
        }

        //public static int SetMenuOptionColor(ref bool boolState)
        //{
        //    int SetMenuOptionColor = 0;
        //    // this function will return the forecolor for the grid row
        //    if (!boolState)
        //    {
        //        SetMenuOptionColor = modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION;
        //        //				// GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, 1, intRow, 1) = TRIOCOLORGRAYEDOUT
        //    }
        //    else
        //    {
        //        SetMenuOptionColor = modGlobalConstants.Statics.TRIOCOLORBLACK;
        //        //				// GRID.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intRow, 1, intRow, 1) = TRIOCOLORBLACK
        //    }
        //    return SetMenuOptionColor;
        //}

        public static void AddCYAEntry_6(string strModule, string strDescription1, string strDescription2 = "", string strDescription3 = "", string strDescription4 = "", string strDescription5 = "")
        {
            AddCYAEntry(strModule, strDescription1, strDescription2, strDescription3, strDescription4, strDescription5);
        }

        public static void AddCYAEntry_8(string strModule, string strDescription1, string strDescription2 = "", string strDescription3 = "", string strDescription4 = "", string strDescription5 = "")
        {
            AddCYAEntry(strModule, strDescription1, strDescription2, strDescription3, strDescription4, strDescription5);
        }

        public static void AddCYAEntry_26(string strModule, string strDescription1, string strDescription2 = "", string strDescription3 = "", string strDescription4 = "", string strDescription5 = "")
        {
            AddCYAEntry(strModule, strDescription1, strDescription2, strDescription3, strDescription4, strDescription5);
        }

        public static void AddCYAEntry_62(string strModule, string strDescription1, string strDescription2 = "", string strDescription3 = "", string strDescription4 = "", string strDescription5 = "")
        {
            AddCYAEntry(strModule, strDescription1, strDescription2, strDescription3, strDescription4, strDescription5);
        }

        public static void AddCYAEntry_80(string strModule, string strDescription1, string strDescription2 = "", string strDescription3 = "", string strDescription4 = "", string strDescription5 = "")
        {
            AddCYAEntry(strModule, strDescription1, strDescription2, strDescription3, strDescription4, strDescription5);
        }

        public static void AddCYAEntry_240(string strModule, string strDescription1, string strDescription2 = "", string strDescription3 = "", string strDescription4 = "", string strDescription5 = "")
        {
            AddCYAEntry(strModule, strDescription1, strDescription2, strDescription3, strDescription4, strDescription5);
        }

        public static void AddCYAEntry_242(string strModule, string strDescription1, string strDescription2 = "", string strDescription3 = "", string strDescription4 = "", string strDescription5 = "")
        {
            AddCYAEntry(strModule, strDescription1, strDescription2, strDescription3, strDescription4, strDescription5);
        }

        public static void AddCYAEntry_728(string strModule, string strDescription1, string strDescription2 = "", string strDescription3 = "", string strDescription4 = "", string strDescription5 = "")
        {
            AddCYAEntry(strModule, strDescription1, strDescription2, strDescription3, strDescription4, strDescription5);
        }

        public static void AddCYAEntry(string strModule, string strDescription1, string strDescription2, string strDescription3, string strDescription4, string strDescription5)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                clsDRWrapper rsCYA = new clsDRWrapper();
                string strDatebaseName;
                // this will create the database name with the module initials that are passed in
                strDatebaseName = "TW" + strModule + "0000.vb1";
                // this will open the CYA table in the specific database
                rsCYA.OpenRecordset("SELECT * FROM CYA WHERE ID = 0", strDatebaseName);
                rsCYA.AddNew();
                rsCYA.Set_Fields("boolUseSecurity", modGlobalConstants.Statics.clsSecurityClass.Using_Security());
                rsCYA.Set_Fields("UserID", FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID()) + " ");
                rsCYA.Set_Fields("cyaDate", DateTime.Now);
                rsCYA.Set_Fields("cyaTime", DateAndTime.TimeOfDay);
                if (strDescription1.Length <= 255)
                {
                    rsCYA.Set_Fields("Description1", strDescription1);
                }
                else
                {
                    rsCYA.Set_Fields("Description1", Strings.Left(strDescription1, 255));
                }
                if (strDescription2.Length <= 255)
                {
                    rsCYA.Set_Fields("Description2", strDescription2);
                }
                else
                {
                    rsCYA.Set_Fields("Description2", Strings.Left(strDescription2, 255));
                }
                if (strDescription3.Length <= 255)
                {
                    rsCYA.Set_Fields("Description3", strDescription3);
                }
                else
                {
                    rsCYA.Set_Fields("Description3", Strings.Left(strDescription3, 255));
                }
                if (strDescription4.Length <= 255)
                {
                    rsCYA.Set_Fields("Description4", strDescription4);
                }
                else
                {
                    rsCYA.Set_Fields("Description4", Strings.Left(strDescription4, 255));
                }
                if (strDescription5.Length <= 255)
                {
                    rsCYA.Set_Fields("Description5", strDescription5);
                }
                else
                {
                    rsCYA.Set_Fields("Description5", Strings.Left(strDescription5, 255));
                }
                rsCYA.Update();
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "CYA Update Error");
            }
        }
        // VBto upgrade warning: str1 As string	OnWrite(string, int)
        // VBto upgrade warning: intSpaces As short	OnWrite(short, double)
        // VBto upgrade warning: 'Return' As Variant --> As string
        public static string PadStringWithSpaces(string str1, int intSpaces, bool boolFront = true)
        {
            string PadStringWithSpaces = "";
            // this function will add spaces to the front or end of a string to make the string as long as the integer passed in
            // if the string is longer than the value wanted, the string is truncated and passed back
            str1 = Strings.Trim(str1);
            if (!boolFront)
            {
                if (intSpaces >= Strings.Trim(str1).Length)
                {
                    PadStringWithSpaces = str1 + Strings.StrDup(intSpaces - Strings.Trim(str1).Length, " ");
                }
                else
                {
                    PadStringWithSpaces = Strings.Left(str1, intSpaces);
                }
            }
            else
            {
                if (intSpaces >= str1.Length)
                {
                    PadStringWithSpaces = Strings.StrDup(intSpaces - str1.Length, " ") + str1;
                }
                else
                {
                    PadStringWithSpaces = Strings.Left(str1, intSpaces);
                }
            }
            return PadStringWithSpaces;
        }

        public static void UpdateUsedModules()
        {
            try
            {
                clsDRWrapper rs = new clsDRWrapper();

                try
                {
                    rs.OpenRecordset("SELECT * FROM Modules", "SystemSettings");
                    if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
                    {
                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("ARDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("AR")))
                        {
                            modGlobalConstants.Statics.gboolAR = FCConvert.ToBoolean(rs.Get_Fields_Boolean("AR"));
                            StaticSettings.gGlobalActiveModuleSettings.AccountsReceivableIsActive = modGlobalConstants.Statics.gboolAR;
                        }
	                    else
	                    {
							modGlobalConstants.Statics.gboolAR = false;
							StaticSettings.gGlobalActiveModuleSettings.AccountsReceivableIsActive = false;
	                    }

                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("BDDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("BD")))
                        {
                            modGlobalConstants.Statics.gboolBD = true;
                            StaticSettings.gGlobalActiveModuleSettings.BudgetaryIsActive = true;
                        }
	                    else
	                    {
		                    modGlobalConstants.Statics.gboolBD = false;
		                    StaticSettings.gGlobalActiveModuleSettings.BudgetaryIsActive = false;
	                    }

						if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("BLDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("BL")))
                        {
                            modGlobalConstants.Statics.gboolBL = true;
                            StaticSettings.gGlobalActiveModuleSettings.TaxBillingIsActive = true;
                        }
						else
						{
							modGlobalConstants.Statics.gboolBL = false;
							StaticSettings.gGlobalActiveModuleSettings.TaxBillingIsActive = false;
						}

						if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("CEDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("CE")))
                        {
                            modGlobalConstants.Statics.gboolCE = true;
                            StaticSettings.gGlobalActiveModuleSettings.CodeEnforcementIsActive = true;
                        }
						else
						{
							modGlobalConstants.Statics.gboolCE = false;
							StaticSettings.gGlobalActiveModuleSettings.CodeEnforcementIsActive = false;
						}

                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("CLDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("CL")))
                        {
                            modGlobalConstants.Statics.gboolCL = true;
                            StaticSettings.gGlobalActiveModuleSettings.TaxCollectionsIsActive = true;
                        }
						else
						{
							modGlobalConstants.Statics.gboolCL = false;
							StaticSettings.gGlobalActiveModuleSettings.TaxCollectionsIsActive = false;
						}

                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("CKDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("CK")))
                        {
                            modGlobalConstants.Statics.gboolCK = true;
                            StaticSettings.gGlobalActiveModuleSettings.ClerkIsActive = true;
                        }
						else
						{
							modGlobalConstants.Statics.gboolCK = false;
							StaticSettings.gGlobalActiveModuleSettings.ClerkIsActive = false;
						}

                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("CRDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("CR")))
                        {
                            modGlobalConstants.Statics.gboolCR = true;
                            StaticSettings.gGlobalActiveModuleSettings.CashReceiptingIsActive = true;
                        }
						else
						{
							modGlobalConstants.Statics.gboolCR = false;
							StaticSettings.gGlobalActiveModuleSettings.CashReceiptingIsActive = false;
						}

                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("E9Date")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("E9")))
                        {
                            modGlobalConstants.Statics.gboolE9 = FCConvert.ToBoolean(rs.Get_Fields_Boolean("E9"));
                        }
						else
						{
							modGlobalConstants.Statics.gboolE9 = false;
						}

                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("FADate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("FA")))
                        {
                            modGlobalConstants.Statics.gboolFA = FCConvert.ToBoolean(rs.Get_Fields_Boolean("FA"));
                            StaticSettings.gGlobalActiveModuleSettings.FixedAssetsIsActive = modGlobalConstants.Statics.gboolFA;
                        }
						else
						{
							modGlobalConstants.Statics.gboolFA = false;
							StaticSettings.gGlobalActiveModuleSettings.FixedAssetsIsActive = false;
						}

                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("HRDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("HR")))
                        {
                            modGlobalConstants.Statics.gboolHR = FCConvert.ToBoolean(rs.Get_Fields_Boolean("HR"));
                        }
						else
						{
							modGlobalConstants.Statics.gboolHR = false;
						}

                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("IVDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("IV")))
                        {
                            modGlobalConstants.Statics.gboolIV = FCConvert.ToBoolean(rs.Get_Fields_Boolean("IV"));
                        }
						else
						{
							modGlobalConstants.Statics.gboolIV = false;
						}

                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("MVDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("MV")))
                        {
                            modGlobalConstants.Statics.gboolMV = FCConvert.ToBoolean(rs.Get_Fields_Boolean("MV"));
                            StaticSettings.gGlobalActiveModuleSettings.MotorVehicleIsActive = modGlobalConstants.Statics.gboolMV;
                        }
						else
						{
							modGlobalConstants.Statics.gboolMV = false;
							StaticSettings.gGlobalActiveModuleSettings.MotorVehicleIsActive = false;
						}

                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("MSDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("MS")))
                        {
                            modGlobalConstants.Statics.gboolMS = FCConvert.ToBoolean(rs.Get_Fields_Boolean("MS"));
                            StaticSettings.gGlobalActiveModuleSettings.MosesIsActive = modGlobalConstants.Statics.gboolMS;
                        }
						else
						{
							modGlobalConstants.Statics.gboolMS = false;
							StaticSettings.gGlobalActiveModuleSettings.MosesIsActive = false;
						}

                        // TODO: Check the table for the column [PP] and replace with corresponding Get_Field method
                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("PPDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields("PP")))
                        // TODO: Check the table for the column [PP] and replace with corresponding Get_Field method
                        {
                            modGlobalConstants.Statics.gboolPP = FCConvert.ToBoolean(rs.Get_Fields("PP"));
                            StaticSettings.gGlobalActiveModuleSettings.PersonalPropertyIsActive = modGlobalConstants.Statics.gboolPP;
                        }
						else
						{
							modGlobalConstants.Statics.gboolPP = false;
							StaticSettings.gGlobalActiveModuleSettings.PersonalPropertyIsActive = false;
						}

                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("PYDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("PY")))
                        {
                            modGlobalConstants.Statics.gboolPY = FCConvert.ToBoolean(rs.Get_Fields_Boolean("PY"));
                            StaticSettings.gGlobalActiveModuleSettings.PayrollIsActive = modGlobalConstants.Statics.gboolPY;
                        }
						else
						{
							modGlobalConstants.Statics.gboolPY = false;
							StaticSettings.gGlobalActiveModuleSettings.PayrollIsActive = false;
						}

                        // TODO: Check the table for the column [RB] and replace with corresponding Get_Field method
                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("RBDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields("RB")))
                        // TODO: Check the table for the column [RB] and replace with corresponding Get_Field method
                        {
                            modGlobalConstants.Statics.gboolRB = FCConvert.ToBoolean(rs.Get_Fields("RB"));
                            StaticSettings.gGlobalActiveModuleSettings.RedbookIsActive = modGlobalConstants.Statics.gboolRB;
                        }
						else
						{
							modGlobalConstants.Statics.gboolRB = false;
							StaticSettings.gGlobalActiveModuleSettings.RedbookIsActive = false;
						}

                        // TODO: Check the table for the column [RE] and replace with corresponding Get_Field method
                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("REDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields("RE")))
                        // TODO: Check the table for the column [RE] and replace with corresponding Get_Field method
                        {
                            modGlobalConstants.Statics.gboolRE = FCConvert.ToBoolean(rs.Get_Fields("RE"));
                            StaticSettings.gGlobalActiveModuleSettings.RealEstateIsActive = modGlobalConstants.Statics.gboolRE;
                        }
						else
						{
							modGlobalConstants.Statics.gboolRE = false;
							StaticSettings.gGlobalActiveModuleSettings.RealEstateIsActive = false;
						}

                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("TSDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("TS")))
                        {
                            modGlobalConstants.Statics.gboolTS = FCConvert.ToBoolean(rs.Get_Fields_Boolean("TS"));
                            StaticSettings.gGlobalActiveModuleSettings.TaxServiceIsActive = modGlobalConstants.Statics.gboolTS;
                        }
						else
						{
							modGlobalConstants.Statics.gboolTS = false;
							StaticSettings.gGlobalActiveModuleSettings.TaxServiceIsActive = false;
						}

                        if (FCConvert.ToDateTime(rs.Get_Fields_DateTime("UTDate")) >= DateTime.Today &&
                            FCConvert.ToBoolean(rs.Get_Fields_Boolean("UT")))
                        {
                            modGlobalConstants.Statics.gboolUT = FCConvert.ToBoolean(rs.Get_Fields_Boolean("UT"));
                            StaticSettings.gGlobalActiveModuleSettings.UtilityBillingIsActive = modGlobalConstants.Statics.gboolUT;
                        }
						else
						{
							modGlobalConstants.Statics.gboolUT = false;
							StaticSettings.gGlobalActiveModuleSettings.UtilityBillingIsActive = false;
                        }

                        if (rs.Get_Fields_Boolean("MR") &&  rs.Get_Fields_DateTime("MRDate")
                            .IsLaterThan(DateTime.Today.Subtract(new TimeSpan(1, 0, 0, 0))))
                        {
                            StaticSettings.gGlobalActiveModuleSettings.CRMyRecIsActive = true;
                        }
                        else
                        {
                            StaticSettings.gGlobalActiveModuleSettings.CRMyRecIsActive = false;
                        }
                }
                }
                finally
                {
                    rs.DisposeOf();
                }
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + ex.GetBaseException().Message + "\r\n" + "In UpdateUsedModules", MsgBoxStyle.Critical, "Error");
            }
        }

        public static string RemoveApostrophe(string strText)
        {
            string RemoveApostrophe = "";
            // this will take a string and remove any apostrophes found
            RemoveApostrophe = strText.Replace("'", "");
            return RemoveApostrophe;
        }
        //public static void SetMenuBackColor()
        //{
        //	clsDRWrapper rsColorInfo = new clsDRWrapper();
        //	try
        //	{   // On Error GoTo ErrorHandler
        //		rsColorInfo.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
        //		if (rsColorInfo.EndOfFile() != true && rsColorInfo.BeginningOfFile() != true)
        //		{
        //			if (Conversion.Val(rsColorInfo.Get_Fields("MenuBackColor"))) != 0)
        //			{
        //				App.MainForm.GRID.BackColor = ColorTranslator.FromOle(FCConvert.ToInt32(Math.Round(Conversion.Val(rsColorInfo.Get_Fields("MenuBackColor")))));
        //				App.MainForm.GRID.BackColorAlternate = ColorTranslator.FromOle(FCConvert.ToInt32(Math.Round(Conversion.Val(rsColorInfo.Get_Fields("MenuBackColor")))));
        //			}
        //		}
        //		return;
        //	}
        //	catch(Exception ex)
        //	{   // ErrorHandler:
        //	}
        //}
        public static bool GetGlobalRegistrySetting()
        {
            bool GetGlobalRegistrySetting = false;
            modGlobalConstants.Statics.gboolUseRegistryOnly = true;
            return GetGlobalRegistrySetting;
        }

        public static string GetCurrentBookPageString(int lngAccount, short intEntriesLimit = 0, bool boolAddSpaces = false)
        {
            string GetCurrentBookPageString = "";
            clsDRWrapper clsBookPage = new clsDRWrapper();
            string strBookPage;
            string strBookPageDate = "";
            int x;
            bool boolContinue;
            string strSpace = "";
            try
            {
                // On Error GoTo ErrorHandler
                GetCurrentBookPageString = "";
                if (boolAddSpaces)
                {
                    strSpace = " ";
                }
                else
                {
                    strSpace = "";
                }
                clsBookPage.OpenRecordset("select * from bookpage where account = " + FCConvert.ToString(lngAccount) + " and [current] = 1 order by line desc", "twre0000.vb1");
                strBookPage = "";
                boolContinue = true;
                x = 0;
                while (!clsBookPage.EndOfFile() && boolContinue)
                {
                    x += 1;
                    if (x >= intEntriesLimit && intEntriesLimit > 0)
                    {
                        boolContinue = false;
                    }
                    // TODO: Check the table for the column [Book] and replace with corresponding Get_Field method
                    // TODO: Check the table for the column [Page] and replace with corresponding Get_Field method
                    strBookPage += "B" + FCConvert.ToString(clsBookPage.Get_Fields("Book")) + strSpace + "P" + FCConvert.ToString(clsBookPage.Get_Fields("Page"));
                    strBookPageDate = "";
                    if (Information.IsDate(clsBookPage.Get_Fields("bpdate")))
                    {
                        //FC:FINAL:MSH - wrong conversion from VB6
                        //if (clsBookPage.Get_Fields("bpdate") is DateTime)
                        if (clsBookPage.Get_Fields_DateTime("bpdate").ToOADate() != 0)
                        {
                            strBookPageDate = Strings.Format(clsBookPage.Get_Fields_DateTime("bpdate"), "MM/dd/yyyy") + " ";
                        }
                    }
                    strBookPage += " " + strBookPageDate;
                    clsBookPage.MoveNext();
                }
                GetCurrentBookPageString = Strings.Trim(strBookPage);
                return GetCurrentBookPageString;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In GetCurrentBookPageString", MsgBoxStyle.Critical, "Error");
            }
            return GetCurrentBookPageString;
        }

        public static string EscapeQuotes(string a)
        {
            string EscapeQuotes = "";
            // searches for a singlequote and puts another next to it
            string tstring = "";
            string t2string = "";
            int stringindex;
            int intpos = 0;
            EscapeQuotes = a;
            if (a == string.Empty)
                return EscapeQuotes;
            stringindex = 1;
            while (stringindex <= a.Length)
            {
                intpos = Strings.InStr(stringindex, a, "'");
                if (intpos == 0)
                    break;
                tstring = Strings.Mid(a, 1, intpos);
                t2string = Strings.Mid(a, intpos + 1);
                if (intpos == a.Length)
                {
                    // a = tstring & "' "
                    a = Strings.Mid(a, 1, intpos - 1);
                    break;
                }
                else
                {
                    a = tstring + "'" + t2string;
                }
                stringindex = intpos + 2;
            }
            EscapeQuotes = a;
            return EscapeQuotes;
        }

        public static bool IsBounded(Array vntArray)
        {
            bool IsBounded = false;
            // if this has an error then it is not bounded and will return false
            /*? On Error Resume Next  */
            IsBounded = Information.IsNumeric(Information.UBound(vntArray, 1));
            /*? On Error GoTo 0 */
            return IsBounded;
        }

        public static string ReturnCorrectedMakeCode(string strType, string strMake, string strCode)
        {
            string ReturnCorrectedMakeCode = "";
            if ((strType == "C") || (strType == "T"))
            {
                if (Strings.UCase(strCode) == "TRIU")
                {
                    ReturnCorrectedMakeCode = "TRUM";
                }
                else if (Strings.UCase(strCode) == "SAAB")
                {
                    ReturnCorrectedMakeCode = "SAAB";
                }
                else if ((Strings.UCase(strCode) == "STER") || (Strings.UCase(strCode) == "STLG"))
                {
                    ReturnCorrectedMakeCode = "STRG";
                }
                else
                {
                    ReturnCorrectedMakeCode = Strings.UCase(strCode);
                }
            }
            else if (strType == "H")
            {
                if ((Strings.UCase(strCode) == "INT'L") || (Strings.UCase(strCode) == "INTE") || (Strings.UCase(strCode) == "INT'"))
                {
                    ReturnCorrectedMakeCode = "INTL";
                }
                else if (Strings.UCase(strCode) == "FREI")
                {
                    ReturnCorrectedMakeCode = "FRHT";
                }
                else if (Strings.UCase(strCode) == "PETE")
                {
                    ReturnCorrectedMakeCode = "PTRB";
                }
                else if (Strings.UCase(strCode) == "KENW")
                {
                    ReturnCorrectedMakeCode = "KNNW";
                }
                else if (Strings.UCase(strCode) == "GENE")
                {
                    ReturnCorrectedMakeCode = "GMC";
                }
                else if (Strings.UCase(strCode) == "MARM")
                {
                    ReturnCorrectedMakeCode = "MAHA";
                }
                else if (Strings.UCase(strCode) == "NDMC")
                {
                    ReturnCorrectedMakeCode = "UD";
                }
                else if (Strings.UCase(strCode) == "WEST")
                {
                    ReturnCorrectedMakeCode = "WSTR";
                }
                else
                {
                    ReturnCorrectedMakeCode = Strings.UCase(strCode);
                }
            }
            else if (strType == "M")
            {
                if (Strings.UCase(strMake) == "AMERICAN EAGLE")
                {
                    ReturnCorrectedMakeCode = "AEAG";
                }
                else if (Strings.UCase(strMake) == "AMERICAN IRONHORSE")
                {
                    ReturnCorrectedMakeCode = "AIH";
                }
                else if (Strings.UCase(strMake) == "AMERICAN PERFORMANCE CYCLE")
                {
                    ReturnCorrectedMakeCode = "AMPF";
                }
                else if (Strings.UCase(strMake) == "AMERICAN QUANTUM")
                {
                    ReturnCorrectedMakeCode = "AMQT";
                }
                else if (Strings.UCase(strMake) == "ARCTIC CAT")
                {
                    ReturnCorrectedMakeCode = "ARCA";
                }
                else if (Strings.UCase(strMake) == "ARGO ATV")
                {
                    ReturnCorrectedMakeCode = "ARGG";
                }
                else if (Strings.UCase(strMake) == "BIG DOG")
                {
                    ReturnCorrectedMakeCode = "BGDG";
                }
                else if (Strings.UCase(strMake) == "BOURGET'S BIKE WORKS")
                {
                    ReturnCorrectedMakeCode = "BBW";
                }
                else if (Strings.UCase(strMake) == "CALIFORNIA MOTORCYCLE")
                {
                    ReturnCorrectedMakeCode = "CMCC";
                }
                else if (Strings.UCase(strMake) == "CAN-AM")
                {
                    ReturnCorrectedMakeCode = "CANA";
                }
                else if (Strings.UCase(strMake) == "CARABELA")
                {
                    ReturnCorrectedMakeCode = "CAAR";
                }
                else if (Strings.UCase(strMake) == "CAREFREE CUSTOM CYCLES")
                {
                    ReturnCorrectedMakeCode = "CCCY";
                }
                else if (Strings.UCase(strMake) == "CCM")
                {
                    ReturnCorrectedMakeCode = "CMCW";
                }
                else if (Strings.UCase(strMake) == "CLASSIC MOTORCYCLES")
                {
                    ReturnCorrectedMakeCode = "CLSC";
                }
                else if (Strings.UCase(strMake) == "COBRA")
                {
                    ReturnCorrectedMakeCode = "COBM";
                }
                else if (Strings.UCase(strMake) == "DESPERADO MOTORCYCLES")
                {
                    ReturnCorrectedMakeCode = "DSPD";
                }
                else if (Strings.UCase(strMake) == "ECSTACY CYCLE")
                {
                    ReturnCorrectedMakeCode = "ECTA";
                }
                else if (Strings.UCase(strMake) == "E-TON AMERICA")
                {
                    ReturnCorrectedMakeCode = "ETON";
                }
                else if (Strings.UCase(strMake) == "EXCELSIOR-HENDERSON")
                {
                    ReturnCorrectedMakeCode = "EXHE";
                }
                else if (Strings.UCase(strMake) == "HARLEY-DAVIDSON")
                {
                    ReturnCorrectedMakeCode = "HD";
                }
                else if (Strings.UCase(strMake) == "JOHNNY PAG MOTORCYCLES")
                {
                    ReturnCorrectedMakeCode = "JHNY";
                }
                else if (Strings.UCase(strMake) == "KASEA")
                {
                    ReturnCorrectedMakeCode = "KSEA";
                }
                else if (Strings.UCase(strMake) == "KAWASAKI")
                {
                    ReturnCorrectedMakeCode = "KAWK";
                }
                else if (Strings.UCase(strMake) == "KINETIC MOPEDS & SCOOTERS")
                {
                    ReturnCorrectedMakeCode = "KNTC";
                }
                else if (Strings.UCase(strMake) == "MONTESA")
                {
                    ReturnCorrectedMakeCode = "MOTS";
                }
                else if (Strings.UCase(strMake) == "MOTO GUZZI")
                {
                    ReturnCorrectedMakeCode = "MOGU";
                }
                else if (Strings.UCase(strMake) == "MV AGUSTA")
                {
                    ReturnCorrectedMakeCode = "MVAU";
                }
                else if (Strings.UCase(strMake) == "PANZER MOTORCYCLES")
                {
                    ReturnCorrectedMakeCode = "PNZR";
                }
                else if (Strings.UCase(strMake) == "PRO-ONE MOTORCYCLES")
                {
                    ReturnCorrectedMakeCode = "PRNE";
                }
                else if (Strings.UCase(strMake) == "RED HORSE MOTORWORKS")
                {
                    ReturnCorrectedMakeCode = "REDH";
                }
                else if (Strings.UCase(strMake) == "ROKON")
                {
                    ReturnCorrectedMakeCode = "ROKN";
                }
                else if (Strings.UCase(strMake) == "ROYAL ENFIELD")
                {
                    ReturnCorrectedMakeCode = "ROEN";
                }
                else if (Strings.UCase(strMake) == "ROYAL RYDER")
                {
                    ReturnCorrectedMakeCode = "RRMM";
                }
                else if (Strings.UCase(strMake) == "SAXON MOTORCYCLES")
                {
                    ReturnCorrectedMakeCode = "SAXN";
                }
                else if (Strings.UCase(strMake) == "SUZUKI")
                {
                    ReturnCorrectedMakeCode = "SUZI";
                }
                else if (Strings.UCase(strMake) == "TITAN MOTORCYCLES")
                {
                    ReturnCorrectedMakeCode = "TITN";
                }
                else if (Strings.UCase(strMake) == "TRIUMPH")
                {
                    ReturnCorrectedMakeCode = "TRUM";
                }
                else if (Strings.UCase(strMake) == "ULTRA MOTORCYCLE")
                {
                    ReturnCorrectedMakeCode = "ULM";
                }
                else if (Strings.UCase(strMake) == "UNITED MOTORS")
                {
                    ReturnCorrectedMakeCode = "UNMO";
                }
                else if (Strings.UCase(strMake) == "VICTORY")
                {
                    ReturnCorrectedMakeCode = "VCTY";
                }
                else if (Strings.UCase(strMake) == "VOR")
                {
                    ReturnCorrectedMakeCode = "VREN";
                }
                else if (Strings.UCase(strMake) == "WHIZZER")
                {
                    ReturnCorrectedMakeCode = "WHZR";
                }
                else if (Strings.UCase(strMake) == "WILD WEST")
                {
                    ReturnCorrectedMakeCode = "WWMC";
                }
                else if (Strings.UCase(strMake) == "XTREME BY UNILLI MOTORS")
                {
                    ReturnCorrectedMakeCode = "XTRM";
                }
                else
                {
                    ReturnCorrectedMakeCode = Strings.UCase(strCode);
                }
            }
            else if ((strType == "X") || (strType == "R"))
            {
                if (Strings.UCase(strMake) == "BARRETT")
                {
                    ReturnCorrectedMakeCode = "BARE";
                }
                else if (Strings.UCase(strMake) == "C & C INDUSTRIES")
                {
                    ReturnCorrectedMakeCode = "CCTI";
                }
                else if (Strings.UCase(strMake) == "CHAMBERLIN")
                {
                    ReturnCorrectedMakeCode = "CHAL";
                }
                else if (Strings.UCase(strMake) == "J&L")
                {
                    ReturnCorrectedMakeCode = "JLTL";
                }
                else if (Strings.UCase(strMake) == "LOAD KING")
                {
                    ReturnCorrectedMakeCode = "LOAK";
                }
                else if (Strings.UCase(strMake) == "ROAD SYSTEMS INC.")
                {
                    ReturnCorrectedMakeCode = "RSYS";
                }
                else if (Strings.UCase(strMake) == "TRAIL KING")
                {
                    ReturnCorrectedMakeCode = "TRKG";
                }
                else if (Strings.UCase(strMake) == "TRAVIS TRAILERS")
                {
                    ReturnCorrectedMakeCode = "TRVS";
                }
                else if (Strings.UCase(strMake) == "VANGUARD NATIONAL")
                {
                    ReturnCorrectedMakeCode = "VANR";
                }
                else if (Strings.UCase(strMake) == "VIM")
                {
                    ReturnCorrectedMakeCode = "MOSE";
                }
                else if (Strings.UCase(strMake) == "WILSON")
                {
                    ReturnCorrectedMakeCode = "WILX";
                }
                else if (Strings.UCase(strMake) == "WESTERN TRAILERS")
                {
                    ReturnCorrectedMakeCode = "WSTI";
                }
                else if (Strings.UCase(strMake) == "ABI LEISURE PRODUCTS")
                {
                    ReturnCorrectedMakeCode = "ABIA";
                }
                else if (Strings.UCase(strMake) == "AERO COACH")
                {
                    ReturnCorrectedMakeCode = "AROM";
                }
                else if (Strings.UCase(strMake) == "AEROLITE")
                {
                    ReturnCorrectedMakeCode = "AELT";
                }
                else if (Strings.UCase(strMake) == "ALFA LEISURE")
                {
                    ReturnCorrectedMakeCode = "ALFL";
                }
                else if (Strings.UCase(strMake) == "AMERI-CAMP")
                {
                    ReturnCorrectedMakeCode = "AMCM";
                }
                else if (Strings.UCase(strMake) == "AREICAN TRAVEL SYSTEMS")
                {
                    ReturnCorrectedMakeCode = "AMTS";
                }
                else if (Strings.UCase(strMake) == "AMERIGO")
                {
                    ReturnCorrectedMakeCode = "AMES";
                }
                else if (Strings.UCase(strMake) == "ARGOSY")
                {
                    ReturnCorrectedMakeCode = "ARGS";
                }
                else if (Strings.UCase(strMake) == "ARISTOCRAT")
                {
                    ReturnCorrectedMakeCode = "ARII";
                }
                else if (Strings.UCase(strMake) == "AUTO-MATE RECREATIONAL")
                {
                    ReturnCorrectedMakeCode = "AUTM";
                }
                else if (Strings.UCase(strMake) == "AVALON RV")
                {
                    ReturnCorrectedMakeCode = "AVAN";
                }
                else if (Strings.UCase(strMake) == "B & B HOMES")
                {
                    ReturnCorrectedMakeCode = "BBHO";
                }
                else if (Strings.UCase(strMake) == "BARTH")
                {
                    ReturnCorrectedMakeCode = "BARH";
                }
                else if (Strings.UCase(strMake) == "BELAIR")
                {
                    ReturnCorrectedMakeCode = "BELI";
                }
                else if (Strings.UCase(strMake) == "BOLES-AERO")
                {
                    ReturnCorrectedMakeCode = "BOLA";
                }
                else if (Strings.UCase(strMake) == "BONAIR LEISURE")
                {
                    ReturnCorrectedMakeCode = "BONR";
                }
                else if (Strings.UCase(strMake) == "BONANZE TRAVELER")
                {
                    ReturnCorrectedMakeCode = "BONC";
                }
                else if (Strings.UCase(strMake) == "BORN FREE MOTORCOACH")
                {
                    ReturnCorrectedMakeCode = "BORF";
                }
                else if (Strings.UCase(strMake) == "CAMP INDUSTRIES")
                {
                    ReturnCorrectedMakeCode = "CAIN";
                }
                else if (Strings.UCase(strMake) == "CARRAIGE INC.")
                {
                    ReturnCorrectedMakeCode = "CARG";
                }
                else if (Strings.UCase(strMake) == "CARSON TRAILER")
                {
                    ReturnCorrectedMakeCode = "CATI";
                }
                else if (Strings.UCase(strMake) == "CHARIOT EAGLE")
                {
                    ReturnCorrectedMakeCode = "CHRE";
                }
                else if (Strings.UCase(strMake) == "CHATEAU RECREATIONAL")
                {
                    ReturnCorrectedMakeCode = "CHRV";
                }
                else if (Strings.UCase(strMake) == "CHEVRON")
                {
                    ReturnCorrectedMakeCode = "CHEP";
                }
                else if (Strings.UCase(strMake) == "CHINOOK BY TRAIL WAGONS")
                {
                    ReturnCorrectedMakeCode = "CHNO";
                }
                else if (Strings.UCase(strMake) == "CLASSIC MANUFACTURING")
                {
                    ReturnCorrectedMakeCode = "CLSS";
                }
                else if (Strings.UCase(strMake) == "COACH HOUSE")
                {
                    ReturnCorrectedMakeCode = "COAI";
                }
                else if (Strings.UCase(strMake) == "COBRA INDUSTRIES")
                {
                    ReturnCorrectedMakeCode = "COBI";
                }
                else if (Strings.UCase(strMake) == "COLUMBIA NORTHWEST")
                {
                    ReturnCorrectedMakeCode = "COLN";
                }
                else if (Strings.UCase(strMake) == "CONESTOGA")
                {
                    ReturnCorrectedMakeCode = "CONI";
                }
                else if (Strings.UCase(strMake) == "CROSSROADS RV")
                {
                    ReturnCorrectedMakeCode = "CROR";
                }
                else if (Strings.UCase(strMake) == "CRUISER RV")
                {
                    ReturnCorrectedMakeCode = "CRRV";
                }
                else if (Strings.UCase(strMake) == "CUSTOM CAMPERS")
                {
                    ReturnCorrectedMakeCode = "CUCA";
                }
                else if (Strings.UCase(strMake) == "DUTCHMEN")
                {
                    ReturnCorrectedMakeCode = "DUCH";
                }
                else if (Strings.UCase(strMake) == "DYNAMAX")
                {
                    ReturnCorrectedMakeCode = "COAH";
                }
                else if (Strings.UCase(strMake) == "ECLIPSE RV")
                {
                    ReturnCorrectedMakeCode = "ECLP";
                }
                else if (Strings.UCase(strMake) == "EL DORADO")
                {
                    ReturnCorrectedMakeCode = "ELDO";
                }
                else if (Strings.UCase(strMake) == "ELECTRA RV")
                {
                    ReturnCorrectedMakeCode = "ELRV";
                }
                else if (Strings.UCase(strMake) == "ELITE DIV. OF TL IND.")
                {
                    ReturnCorrectedMakeCode = "ELIM";
                }
                else if (Strings.UCase(strMake) == "ELKHART TRAVELER")
                {
                    ReturnCorrectedMakeCode = "ELTC";
                }
                else if (Strings.UCase(strMake) == "EN ROUTE")
                {
                    ReturnCorrectedMakeCode = "ENRT";
                }
                else if (Strings.UCase(strMake) == "ENDURA MAX")
                {
                    ReturnCorrectedMakeCode = "EDMX";
                }
                else if (Strings.UCase(strMake) == "EXCORT R.V.'S")
                {
                    ReturnCorrectedMakeCode = "ESCT";
                }
                else if (Strings.UCase(strMake) == "EVERGREEN RV")
                {
                    ReturnCorrectedMakeCode = "EVGR";
                }
                else if (Strings.UCase(strMake) == "FIELDS")
                {
                    ReturnCorrectedMakeCode = "FIEM";
                }
                else if (Strings.UCase(strMake) == "FIREBALL")
                {
                    ReturnCorrectedMakeCode = "FIRB";
                }
                else if (Strings.UCase(strMake) == "FIRESIDE RECREATIONAL")
                {
                    ReturnCorrectedMakeCode = "FIRV";
                }
                else if (Strings.UCase(strMake) == "FLEETWOOD ENTERPRISES")
                {
                    ReturnCorrectedMakeCode = "FTWD";
                }
                else if (Strings.UCase(strMake) == "FOREST RIVER")
                {
                    ReturnCorrectedMakeCode = "FOCI";
                }
                else if (Strings.UCase(strMake) == "FOUR STATES")
                {
                    ReturnCorrectedMakeCode = "FOUT";
                }
                else if (Strings.UCase(strMake) == "FOUR WINDS")
                {
                    ReturnCorrectedMakeCode = "FOUW";
                }
                else if (Strings.UCase(strMake) == "FRANKLIN COACH")
                {
                    ReturnCorrectedMakeCode = "FRAI";
                }
                else if (Strings.UCase(strMake) == "FREE SPIRIT")
                {
                    ReturnCorrectedMakeCode = "HOLR";
                }
                else if (Strings.UCase(strMake) == "FTCA INC. / COLEMAN")
                {
                    ReturnCorrectedMakeCode = "COLE";
                }
                else if (Strings.UCase(strMake) == "GARDINER PACIFIC")
                {
                    ReturnCorrectedMakeCode = "GARP";
                }
                else if (Strings.UCase(strMake) == "GENERAL COACH")
                {
                    ReturnCorrectedMakeCode = "GENG";
                }
                else if (Strings.UCase(strMake) == "GILES INDUSTRIES")
                {
                    ReturnCorrectedMakeCode = "GILD";
                }
                else if (Strings.UCase(strMake) == "GLENDALE RECREATIONAL")
                {
                    ReturnCorrectedMakeCode = "GLRE";
                }
                else if (Strings.UCase(strMake) == "GLENDALE RECREATIONAL VEHICLES")
                {
                    ReturnCorrectedMakeCode = "GLRV";
                }
                else if (Strings.UCase(strMake) == "GLOBESTAR INDUSTRIES")
                {
                    ReturnCorrectedMakeCode = "GLOS";
                }
                else if (Strings.UCase(strMake) == "GOLDEN FALCON")
                {
                    ReturnCorrectedMakeCode = "SKYL";
                }
                else if (Strings.UCase(strMake) == "GOLDEN NUGGET")
                {
                    ReturnCorrectedMakeCode = "GONU";
                }
                else if (Strings.UCase(strMake) == "GO-TAG-A-LONG")
                {
                    ReturnCorrectedMakeCode = "GOGA";
                }
                else if (Strings.UCase(strMake) == "GULF STREAM COACH")
                {
                    ReturnCorrectedMakeCode = "GULS";
                }
                else if (Strings.UCase(strMake) == "HAPPY TRAVELER")
                {
                    ReturnCorrectedMakeCode = "HAPY";
                }
                else if (Strings.UCase(strMake) == "HAULMARK MOTORCOACH")
                {
                    ReturnCorrectedMakeCode = "HAUI";
                }
                else if (Strings.UCase(strMake) == "HEARTLAND RECREATIONAL VEHICLES")
                {
                    ReturnCorrectedMakeCode = "HRLD";
                }
                else if (Strings.UCase(strMake) == "HI-LO TRAILER")
                {
                    ReturnCorrectedMakeCode = "HI";
                }
                else if (Strings.UCase(strMake) == "HOLIDAY HOUSE")
                {
                    ReturnCorrectedMakeCode = "HOLE";
                }
                else if (Strings.UCase(strMake) == "HOLIDAY RAMBLER")
                {
                    ReturnCorrectedMakeCode = "HOLR";
                }
                else if (Strings.UCase(strMake) == "HOMESTEADER")
                {
                    ReturnCorrectedMakeCode = "HMST";
                }
                else if (Strings.UCase(strMake) == "HOP-CAP")
                {
                    ReturnCorrectedMakeCode = "HOP";
                }
                else if (Strings.UCase(strMake) == "HORIZONS")
                {
                    ReturnCorrectedMakeCode = "NHRZ";
                }
                else if (Strings.UCase(strMake) == "HORNET INDUSTRIES")
                {
                    ReturnCorrectedMakeCode = "HOIN";
                }
                else if (Strings.UCase(strMake) == "HY-LINE ENTERPRISES")
                {
                    ReturnCorrectedMakeCode = "HYLN";
                }
                else if (Strings.UCase(strMake) == "JAYCO")
                {
                    ReturnCorrectedMakeCode = "JAY";
                }
                else if (Strings.UCase(strMake) == "KEYSTONE RV")
                {
                    ReturnCorrectedMakeCode = "KERV";
                }
                else if (Strings.UCase(strMake) == "KING'S HIGHWAY MOBILE")
                {
                    ReturnCorrectedMakeCode = "KGHY";
                }
                else if (Strings.UCase(strMake) == "LANCE CAMPER")
                {
                    ReturnCorrectedMakeCode = "LNCE";
                }
                else if (Strings.UCase(strMake) == "LEISURE TRAVEL VANS LTD.")
                {
                    ReturnCorrectedMakeCode = "LSUR";
                }
                else if (Strings.UCase(strMake) == "MARATHON HOMES")
                {
                    ReturnCorrectedMakeCode = "MARP";
                }
                else if (Strings.UCase(strMake) == "MARAUDER TRAVELERS")
                {
                    ReturnCorrectedMakeCode = "MRAU";
                }
                else if (Strings.UCase(strMake) == "MIDAS INTERNATIONAL")
                {
                    ReturnCorrectedMakeCode = "MIDS";
                }
                else if (Strings.UCase(strMake) == "MOBILE SCOUT")
                {
                    ReturnCorrectedMakeCode = "MOBT";
                }
                else if (Strings.UCase(strMake) == "MOBILE TRAVELER")
                {
                    ReturnCorrectedMakeCode = "MBIL";
                }
                else if (Strings.UCase(strMake) == "MONACO COACH")
                {
                    ReturnCorrectedMakeCode = "MNAC";
                }
                else if (Strings.UCase(strMake) == "NEW HORIZON")
                {
                    ReturnCorrectedMakeCode = "NHRZ";
                }
                else if (Strings.UCase(strMake) == "NEW HORIZONS")
                {
                    ReturnCorrectedMakeCode = "NHRZ";
                }
                else if (Strings.UCase(strMake) == "NEWMAR CORP.")
                {
                    ReturnCorrectedMakeCode = "NEWR";
                }
                else if (Strings.UCase(strMake) == "NORRIS")
                {
                    ReturnCorrectedMakeCode = "NORI";
                }
                else if (Strings.UCase(strMake) == "NORTHLAND INDUSTRIES")
                {
                    ReturnCorrectedMakeCode = "NORV";
                }
                else if (Strings.UCase(strMake) == "NORTHWEST")
                {
                    ReturnCorrectedMakeCode = "NWCT";
                }
                else if (Strings.UCase(strMake) == "NORTHWOOD MFG")
                {
                    ReturnCorrectedMakeCode = "NRWM";
                }
                else if (Strings.UCase(strMake) == "NU-WA INDUSTRIES")
                {
                    ReturnCorrectedMakeCode = "NUWA";
                }
                else if (Strings.UCase(strMake) == "OPEN RANGE RV")
                {
                    ReturnCorrectedMakeCode = "ORRV";
                }
                else if (Strings.UCase(strMake) == "OPTIMA INDUSTRIES LLC")
                {
                    ReturnCorrectedMakeCode = "OPTI";
                }
                else if (Strings.UCase(strMake) == "OVERLAND INDUSTRIES")
                {
                    ReturnCorrectedMakeCode = "OVEL";
                }
                else if (Strings.UCase(strMake) == "PACE AMERICAN")
                {
                    ReturnCorrectedMakeCode = "PAGC";
                }
                else if (Strings.UCase(strMake) == "PALOMINO/VANGUARD RV")
                {
                    ReturnCorrectedMakeCode = "PLMO";
                }
                else if (Strings.UCase(strMake) == "PARK HAVEN")
                {
                    ReturnCorrectedMakeCode = "PRKH";
                }
                else if (Strings.UCase(strMake) == "PARK HOMES")
                {
                    ReturnCorrectedMakeCode = "PARH";
                }
                else if (Strings.UCase(strMake) == "PENTHOUSE R.V.")
                {
                    ReturnCorrectedMakeCode = "ADDI";
                }
                else if (Strings.UCase(strMake) == "PLAY-MOR TRAILERS")
                {
                    ReturnCorrectedMakeCode = "PLAL";
                }
                else if (Strings.UCase(strMake) == "PLEASURE-WAY IND. LTD")
                {
                    ReturnCorrectedMakeCode = "PLEM";
                }
                else if (Strings.UCase(strMake) == "R & R CUSTOM COACHWORKS")
                {
                    ReturnCorrectedMakeCode = "RANR";
                }
                else if (Strings.UCase(strMake) == "RANGER TRAILER")
                {
                    ReturnCorrectedMakeCode = "WOOM";
                }
                else if (Strings.UCase(strMake) == "RAWHIDE")
                {
                    ReturnCorrectedMakeCode = "RAW";
                }
                else if (Strings.UCase(strMake) == "RECREATION BY DESIGN")
                {
                    ReturnCorrectedMakeCode = "RBYD";
                }
                else if (Strings.UCase(strMake) == "RECREATIONAL VEHICLES")
                {
                    ReturnCorrectedMakeCode = "RECL";
                }
                else if (Strings.UCase(strMake) == "RED DALE COACH")
                {
                    ReturnCorrectedMakeCode = "REDD";
                }
                else if (Strings.UCase(strMake) == "RIVERSIDE RV")
                {
                    ReturnCorrectedMakeCode = "RIVT";
                }
                else if (Strings.UCase(strMake) == "ROADMASTER")
                {
                    ReturnCorrectedMakeCode = "ROAF";
                }
                else if (Strings.UCase(strMake) == "ROADRUNNER")
                {
                    ReturnCorrectedMakeCode = "ROAG";
                }
                else if (Strings.UCase(strMake) == "ROCKWOOD")
                {
                    ReturnCorrectedMakeCode = "ROKW";
                }
                else if (Strings.UCase(strMake) == "ROLL-A-LONG")
                {
                    ReturnCorrectedMakeCode = "ROLH";
                }
                else if (Strings.UCase(strMake) == "ROYAL DIAMOND")
                {
                    ReturnCorrectedMakeCode = "RODI";
                }
                else if (Strings.UCase(strMake) == "ROYALS INTERNATIONAL")
                {
                    ReturnCorrectedMakeCode = "ROYS";
                }
                else if (Strings.UCase(strMake) == "R-VISION")
                {
                    ReturnCorrectedMakeCode = "RVIS";
                }
                else if (Strings.UCase(strMake) == "S 7 S HOMES")
                {
                    ReturnCorrectedMakeCode = "SSTR";
                }
                else if (Strings.UCase(strMake) == "S&S CAMPERS MFG. INC")
                {
                    ReturnCorrectedMakeCode = "SSTR";
                }
                else if (Strings.UCase(strMake) == "SAFARI MOTOR COACHES")
                {
                    ReturnCorrectedMakeCode = "SAFA";
                }
                else if (Strings.UCase(strMake) == "SCAMP BY EVELAND'S INC")
                {
                    ReturnCorrectedMakeCode = "SCMP";
                }
                else if (Strings.UCase(strMake) == "SCAMPER CANADA, LTD.")
                {
                    ReturnCorrectedMakeCode = "SCAP";
                }
                else if (Strings.UCase(strMake) == "SCHULT HOMES")
                {
                    ReturnCorrectedMakeCode = "SCHT";
                }
                else if (Strings.UCase(strMake) == "SEBRING HOMES")
                {
                    ReturnCorrectedMakeCode = "SBTT";
                }
                else if (Strings.UCase(strMake) == "SIESTA")
                {
                    ReturnCorrectedMakeCode = "PARH";
                }
                else if (Strings.UCase(strMake) == "SILVER EAGLE COACH")
                {
                    ReturnCorrectedMakeCode = "SIEA";
                }
                else if (Strings.UCase(strMake) == "SILVER STAR R.V.")
                {
                    ReturnCorrectedMakeCode = "SIST";
                }
                else if (Strings.UCase(strMake) == "SILVER STREAK TRAILER")
                {
                    ReturnCorrectedMakeCode = "SILV";
                }
                else if (Strings.UCase(strMake) == "SKYLARK")
                {
                    ReturnCorrectedMakeCode = "SKLP";
                }
                else if (Strings.UCase(strMake) == "SMALLWOOD TRAILER")
                {
                    ReturnCorrectedMakeCode = "SMWD";
                }
                else if (Strings.UCase(strMake) == "SPACE CRAFT")
                {
                    ReturnCorrectedMakeCode = "SPAE";
                }
                else if (Strings.UCase(strMake) == "SPORT BY DUTCHMEN")
                {
                    ReturnCorrectedMakeCode = "DUTC";
                }
                else if (Strings.UCase(strMake) == "STARCRAFT RV")
                {
                    ReturnCorrectedMakeCode = "STRV";
                }
                else if (Strings.UCase(strMake) == "SUN VALLEY")
                {
                    ReturnCorrectedMakeCode = "SUNN";
                }
                else if (Strings.UCase(strMake) == "SUNLINE COACH")
                {
                    ReturnCorrectedMakeCode = "SUNO";
                }
                else if (Strings.UCase(strMake) == "SUN-LITE")
                {
                    ReturnCorrectedMakeCode = "SNLT";
                }
                else if (Strings.UCase(strMake) == "SUNNYBROOK RV")
                {
                    ReturnCorrectedMakeCode = "SUBK";
                }
                else if (Strings.UCase(strMake) == "SUNRADER")
                {
                    ReturnCorrectedMakeCode = "GARP";
                }
                else if (Strings.UCase(strMake) == "SUNRAY RV")
                {
                    ReturnCorrectedMakeCode = "SUNY";
                }
                else if (Strings.UCase(strMake) == "TAB BY DUTCHMEN")
                {
                    ReturnCorrectedMakeCode = "DUTC";
                }
                else if (Strings.UCase(strMake) == "TADA BY DUTCHMEN")
                {
                    ReturnCorrectedMakeCode = "DUTC";
                }
                else if (Strings.UCase(strMake) == "TETON HOMES")
                {
                    ReturnCorrectedMakeCode = "BBHD";
                }
                else if (Strings.UCase(strMake) == "THOR CALIFORNIA")
                {
                    ReturnCorrectedMakeCode = "THOC";
                }
                else if (Strings.UCase(strMake) == "THOR INDUSTRIES")
                {
                    ReturnCorrectedMakeCode = "THOI";
                }
                else if (Strings.UCase(strMake) == "TIMBERLAND RV")
                {
                    ReturnCorrectedMakeCode = "TMBL";
                }
                else if (Strings.UCase(strMake) == "TOPO BY DUTCHMEN")
                {
                    ReturnCorrectedMakeCode = "DUTC";
                }
                else if (Strings.UCase(strMake) == "TRAIL-HARBOR OF R-VISION")
                {
                    ReturnCorrectedMakeCode = "RVIS";
                }
                else if (Strings.UCase(strMake) == "TRAIL-LITE BY R-VISION")
                {
                    ReturnCorrectedMakeCode = "RVIS";
                }
                else if (Strings.UCase(strMake) == "TRAILMANOR")
                {
                    ReturnCorrectedMakeCode = "TRMR";
                }
                else if (Strings.UCase(strMake) == "TRAILS WEST")
                {
                    ReturnCorrectedMakeCode = "TWMI";
                }
                else if (Strings.UCase(strMake) == "TRAVEL EQUIPMENT")
                {
                    ReturnCorrectedMakeCode = "TRAS";
                }
                else if (Strings.UCase(strMake) == "TRAVEL QUEEN, INC.")
                {
                    ReturnCorrectedMakeCode = "TRQU";
                }
                else if (Strings.UCase(strMake) == "TRAVEL SUPREME")
                {
                    ReturnCorrectedMakeCode = "TRSU";
                }
                else if (Strings.UCase(strMake) == "TRAVEL UNITS")
                {
                    ReturnCorrectedMakeCode = "TRUN";
                }
                else if (Strings.UCase(strMake) == "TRAVEL WORLD")
                {
                    ReturnCorrectedMakeCode = "TRWO";
                }
                else if (Strings.UCase(strMake) == "TRAVELAIRE TRAILER CANADA")
                {
                    ReturnCorrectedMakeCode = "TRAW";
                }
                else if (Strings.UCase(strMake) == "TRAVELER")
                {
                    ReturnCorrectedMakeCode = "TRMI";
                }
                else if (Strings.UCase(strMake) == "TRAVELEZE INDUSTRIES")
                {
                    ReturnCorrectedMakeCode = "TRVE";
                }
                else if (Strings.UCase(strMake) == "TRAVEL-LINE ENTERPRISES")
                {
                    ReturnCorrectedMakeCode = "TRVI";
                }
                else if (Strings.UCase(strMake) == "TRAVELMASTER RV")
                {
                    ReturnCorrectedMakeCode = "TRAY";
                }
                else if (Strings.UCase(strMake) == "TRAVETTE")
                {
                    ReturnCorrectedMakeCode = "TRBA";
                }
                else if (Strings.UCase(strMake) == "TROPHY HOMES")
                {
                    ReturnCorrectedMakeCode = "TRPH";
                }
                else if (Strings.UCase(strMake) == "TROPHY TRAVELERS")
                {
                    ReturnCorrectedMakeCode = "TROQ";
                }
                else if (Strings.UCase(strMake) == "USA MOTOR")
                {
                    ReturnCorrectedMakeCode = "USAC";
                }
                else if (Strings.UCase(strMake) == "VAN AMERICAN")
                {
                    ReturnCorrectedMakeCode = "VANM";
                }
                else if (Strings.UCase(strMake) == "VANGUARD MANUFACTURING")
                {
                    ReturnCorrectedMakeCode = "VNGM";
                }
                else if (Strings.UCase(strMake) == "VEGA")
                {
                    ReturnCorrectedMakeCode = "FLIN";
                }
                else if (Strings.UCase(strMake) == "VENTURE")
                {
                    ReturnCorrectedMakeCode = "VTNR";
                }
                else if (Strings.UCase(strMake) == "WEEKEND WARRIOR")
                {
                    ReturnCorrectedMakeCode = "WWTI";
                }
                else if (Strings.UCase(strMake) == "WESTERN RECREATIONAL")
                {
                    ReturnCorrectedMakeCode = "WLWE";
                }
                else if (Strings.UCase(strMake) == "WILLETT, R.C.")
                {
                    ReturnCorrectedMakeCode = "TEXL";
                }
                else if (Strings.UCase(strMake) == "WINDWARD CAMPERS")
                {
                    ReturnCorrectedMakeCode = "WINE";
                }
                else if (Strings.UCase(strMake) == "WOODLAND PARK")
                {
                    ReturnCorrectedMakeCode = "WOOA";
                }
                else if (Strings.UCase(strMake) == "XPLORER MOTOR HOMES")
                {
                    ReturnCorrectedMakeCode = "XPLO";
                }
                else if (Strings.UCase(strMake) == "ZOOM BY AEROLITE")
                {
                    ReturnCorrectedMakeCode = "AELT";
                }
                else
                {
                    ReturnCorrectedMakeCode = Strings.UCase(strCode);
                }
            }
            else
            {
                ReturnCorrectedMakeCode = Strings.UCase(strCode);
            }
            return ReturnCorrectedMakeCode;
        }
        // kk09242015 trouts-157/trocls-63  Move CMF Code to modGlobalFunctions
        // Implement USPS Intelligent Mail Package Barcode(IMPB) for CL and UT
        public static string GetNextMailFormCode_6(string strMFCode, bool boolIMPB = false)
        {
            return GetNextMailFormCode(ref strMFCode, boolIMPB);
        }

        public static string GetNextMailFormCode(ref string strMFCode, bool boolIMPB = false)
        {
            string GetNextMailFormCode = "";
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will return the next Certified Mail Code in the series
                // used for:
                // PS Form 3800, Certified Mail Form               Service Type Code = 71
                // PS Form 3813-P, Insured Mail Receipt            Service Type Code = 73
                // Label 200, Registered Mail                      Service Type Code = 77
                // PS Form 3804, Return Receipt for Merchandise    Service Type Code = 81
                // 
                string strChannel = "";
                string strServiceTypeCode = "";
                string strDUNSNumber = "";
                string strSequence = "";
                string strCheckDigit = "";
                // VBto upgrade warning: strTemp As string	OnWrite(string, short)
                string strTemp = "";
                int lngEven/*unused?*/;
                int lngOdd/*unused?*/;
                int intCT/*unused?*/;
                bool boolGood;
                boolGood = false;
                if (!boolIMPB)
                {
                    if (strMFCode.Length == 20)
                    {
                        boolGood = true;
                        // two digit code to tell the post office that this is certified mail
                        strServiceTypeCode = Strings.Left(strMFCode, 2);
                        // 9 digit customer number
                        strDUNSNumber = Strings.Mid(strMFCode, 3, 9);
                        // 8 digit sequence number (this will increment by 1 for each piece of mail)
                        strSequence = Strings.Mid(strMFCode, 12, 8);
                        // this will be incremented
                        // 1 digit check digit
                        strCheckDigit = Strings.Right(strMFCode, 1);
                        // this will be recalculated
                        // increment the sequence
                        if (Strings.CompareString(strSequence, "<", "99999999"))
                        {
                            strSequence = Strings.Format(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strSequence))) + 1, "00000000");
                            // PadToString((Val(strSequence) + 1), 8)
                        }
                        else
                        {
                            strSequence = "00000000";
                        }
                        // put all of the digits together
                        strTemp = strServiceTypeCode + strDUNSNumber + strSequence;
                    }
                }
                else
                {
                    if (strMFCode.Length == 22)
                    {
                        boolGood = true;
                        // two digit Channel - 92 and 93 are commercial mailers
                        strChannel = Strings.Left(strMFCode, 2);
                        // three digit Service Type
                        strServiceTypeCode = Strings.Mid(strMFCode, 3, 3);
                        // 9 digit mailer id
                        strDUNSNumber = Strings.Mid(strMFCode, 6, 9);
                        // 7 digit serial number (this will increment by 1 for each piece of mail)
                        strSequence = Strings.Mid(strMFCode, 15, 7);
                        // this will be incremented
                        // 1 digit check digit
                        strCheckDigit = Strings.Right(strMFCode, 1);
                        // this will be recalculated
                        // increment the sequence
                        if (Strings.CompareString(strSequence, "<", "9999999"))
                        {
                            strSequence = Strings.Format(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(strSequence))) + 1, "0000000");
                            // PadToString((Val(strSequence) + 1), 7)
                        }
                        else
                        {
                            strSequence = "0000000";
                        }
                        // put all of the digits together
                        strTemp = strChannel + strServiceTypeCode + strDUNSNumber + strSequence;
                    }
                }
                if (boolGood)
                {
                    // calculate the checkdigit
                    strCheckDigit = CalcCMFCheckDigit(ref strTemp);
                    if (Strings.Trim(strCheckDigit) != "")
                    {
                        strTemp += strCheckDigit;
                    }
                    else
                    {
                        strTemp = FCConvert.ToString(-1);
                    }
                }
                else
                {
                    // MF Code problem
                    strTemp = "-1";
                }
                GetNextMailFormCode = strTemp;
                return GetNextMailFormCode;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Mail Form Code Calculation");
                GetNextMailFormCode = "-1";
            }
            return GetNextMailFormCode;
        }
        // VBto upgrade warning: strCMFCode As object	OnWrite(string)
        public static string CalcCMFCheckDigit(ref string strCMFCode)
        {
            string CalcCMFCheckDigit = "";
            // Given the 19 or 21 left side digits of the barcode number, return the check digit which will be the rightmost digit
            // VBto upgrade warning: lngEven As int	OnWrite(int, double)
            int lngEven;
            // VBto upgrade warning: lngOdd As int	OnWrite(short, double)
            int lngOdd;
            int intCT;
            string strTemp;
            // calculate the checkdigit
            lngEven = 0;
            lngOdd = 0;
            strTemp = "";
            if (Strings.Trim(FCConvert.ToString(strCMFCode)) != "")
            {
                for (intCT = 1; intCT <= FCConvert.ToString(strCMFCode).Length; intCT++)
                {
                    // this looks backwards, because it is.  The array is numbered 20 to 1 from right to left
                    // so to start from left to right I have to take the opposite mod value
                    // Ex position 1 from left to right is actually position 20 from right to left
                    if (intCT % 2 == 1)
                    {
                        lngEven += FCConvert.ToInt32(Strings.Mid(FCConvert.ToString(strCMFCode), intCT, 1));
                        // sum the values for even and odd elements
                    }
                    else
                    {
                        lngOdd += FCConvert.ToInt32(Strings.Mid(FCConvert.ToString(strCMFCode), intCT, 1));
                    }
                }
                lngEven *= 3;
                // Weight the even numbers
                strTemp = (10 - ((lngEven + lngOdd) % 10)).ToString();
                strTemp = Strings.Right(strTemp, 1);
                // this will only take the last digit
            }
            CalcCMFCheckDigit = strTemp;
            return CalcCMFCheckDigit;
        }
        // kk04132015 troges-39  Load single config file
        // VBto upgrade warning: 'Return' As Variant --> As bool
        public static bool LoadSQLConfig(string strDefDatabase = "")
        {
                clsDRWrapper tRs = new clsDRWrapper();
            
                bool blnLoaded = false;

                try
                {
                    string strConfigPath;
                    string strConnGroup;
                    string clientName = "";
                    SettingsInfo tSet = new SettingsInfo();
                    blnLoaded = false;
                    strConfigPath = Application.MapPath("\\");
                    strConnGroup = TWSharedLibrary.Variables.Statics.DataGroup;

                    // If the environment variables are not set then just fail
                    if (strConfigPath != "" && strConnGroup != "")
                    {
                        // Load the config file
                        if (Strings.Right(strConfigPath, 1) != "\\")
                        {
                            strConfigPath += "\\";
                        }

                        string url = Application.Url;

                        if (url.ToUpper().Contains(".TRIO-WEB.COM"))
                        {
                            clientName = url.Left(url.ToUpper().IndexOf(".TRIO-WEB.COM"));
                            clientName = clientName.ToUpper().Replace("HTTPS://", "");
                        }

                        if (!tSet.LoadSettings(strConfigPath + tSet.GetDefaultSettingsFileName(), clientName)) return blnLoaded;

                        StaticSettings.gGlobalSettings.DataEnvironment = strConnGroup;
                        StaticSettings.gGlobalSettings.MasterPath = tSet.MasterDirectory;
                        StaticSettings.gGlobalSettings.ClientEnvironment = tSet.ClientName;
                        StaticSettings.gGlobalSettings.ApplicationPath = tSet.ApplicationDirectory;
                        StaticSettings.gGlobalSettings.LocalDataDirectory = tSet.LocalDataDirectory;
                        //StaticSettings.gGlobalSettings.GlobalDataDirectory = tSet.GlobalDataDirectory;
                        StaticSettings.gGlobalSettings.BackupPath = tSet.BackupDirectory;
                        var currentGroup = SettingsInfo.Statics.WorkstationGroups
                            .Where(g => g.GroupName == strConnGroup).FirstOrDefault();
                        if (currentGroup != null)
                        {
                            StaticSettings.gGlobalSettings.GlobalDataDirectory = currentGroup.GlobalDataDirectory;
                        }
                        else
                        {
                            StaticSettings.gGlobalSettings.GlobalDataDirectory = "";
                        }
                    tSet.LoadArchives();

                        // Configure the connection to SQL Server
                        tRs.DefaultMegaGroup = strConnGroup;
                        tRs.DefaultGroup = "Live";
                        var connectionBuilder = new SqlConnectionStringBuilder(tRs.Get_ConnectionInformation("SystemSettings"));
                        StaticSettings.gGlobalSettings.DataSource = connectionBuilder.DataSource;
                        StaticSettings.gGlobalSettings.UserName = connectionBuilder.UserID;
                        StaticSettings.gGlobalSettings.Password = connectionBuilder.Password;

                        if (strDefDatabase != "")
                        {
                            tRs.SharedDefaultDB = strDefDatabase;

                            // Verify a connection to the DB
                            if (tRs.DBExists(strDefDatabase))
                            {
                                blnLoaded = true;
                            }
                        }
                        else
                        {
                            if (tRs.DBExists("SystemSettings"))
                            {
                                blnLoaded = true;
                            }
                        }

                        tRs.OpenRecordset("SELECT * FROM Clients", "ClientSettings");

                        if (tRs.RecordCount() == 1)
                        {
                            StaticSettings.gGlobalSettings.ClientIdentifier = tRs.Get_Fields_Guid("ClientIdentifier");
                        }
                        else
                        {
                            if (tRs.FindFirst("Name = '" + (StaticSettings.gGlobalSettings.ClientEnvironment == "" ? "Default" : StaticSettings.gGlobalSettings.ClientEnvironment).ToLower() + "'"))
                            {
                                StaticSettings.gGlobalSettings.ClientIdentifier = tRs.Get_Fields_Guid("ClientIdentifier");
                            }
                            else
                            {
                                throw new Exception("Unable to Load Client Information for " + (StaticSettings.gGlobalSettings.ClientEnvironment == "" ? "Default" : StaticSettings.gGlobalSettings.ClientEnvironment));
                            }
                        }
                    }

                    return blnLoaded;
                }
                catch (Exception ex)
                {
                    StaticSettings.GlobalTelemetryService.TrackException(ex);
                    FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading configuration");
                }
                finally
                {
                    tRs.DisposeOf();
                }
            return blnLoaded;
        }


        public class StaticVariables
        {
            //public static cGlobalSettings gGlobalSettings = new cGlobalSettings();
            //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
            public cGlobalSettings gGlobalSettings_AutoInitialized;
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
            }
        }
    }
}
