﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for frmPickRegionalTown.
	/// </summary>
	partial class frmPickRegionalTown : BaseForm
	{
		public fecherFoundation.FCComboBox cmbTown;
		public fecherFoundation.FCLabel lblMessage;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private System.ComponentModel.IContainer components;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPickRegionalTown));
			this.cmbTown = new fecherFoundation.FCComboBox();
			this.lblMessage = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSaveContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 237);
			this.BottomPanel.Size = new System.Drawing.Size(299, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbTown);
			this.ClientArea.Controls.Add(this.lblMessage);
			this.ClientArea.Size = new System.Drawing.Size(299, 177);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(299, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(164, 30);
			this.HeaderText.Text = "Choose Town";
			// 
			// cmbTown
			// 
			this.cmbTown.AutoSize = false;
			this.cmbTown.BackColor = System.Drawing.SystemColors.Window;
			this.cmbTown.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbTown.FormattingEnabled = true;
			this.cmbTown.Location = new System.Drawing.Point(30, 90);
			this.cmbTown.Name = "cmbTown";
			this.cmbTown.Size = new System.Drawing.Size(254, 40);
			this.cmbTown.TabIndex = 1;
			// 
			// lblMessage
			// 
			this.lblMessage.Location = new System.Drawing.Point(30, 30);
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Size = new System.Drawing.Size(237, 31);
			this.lblMessage.TabIndex = 0;
			this.lblMessage.Text = "LABEL1";
			this.lblMessage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveContinue,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveContinue
			// 
			this.cmdSaveContinue.AppearanceKey = "acceptButton";
			this.cmdSaveContinue.Location = new System.Drawing.Point(78, 30);
			this.cmdSaveContinue.Name = "cmdSaveContinue";
			this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveContinue.Size = new System.Drawing.Size(153, 48);
			this.cmdSaveContinue.TabIndex = 0;
			this.cmdSaveContinue.Text = "Save & Continue";
			this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// frmPickRegionalTown
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(299, 345);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmPickRegionalTown";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Choose Town";
			this.Load += new System.EventHandler(this.frmPickRegionalTown_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPickRegionalTown_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdSaveContinue;
	}
}
