﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWBL0000
using TWBL0000;
using modGlobal = TWBL0000.modGlobalVariables;


#elif TWRE0000
using modGlobal = TWRE0000.modGlobalVariables;


#elif TWUT0000
using modGlobal = TWUT0000.modUTBilling;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmQuestion.
	/// </summary>
	public partial class frmQuestion : BaseForm
	{
		public frmQuestion()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmQuestion InstancePtr
		{
			get
			{
				return (frmQuestion)Sys.GetInstance(typeof(frmQuestion));
			}
		}

		protected frmQuestion _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// Date           :               12/30/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               06/30/2004              *
		// ********************************************************
		int intType;
		// VBto upgrade warning: intDefSelection As short	OnWriteFCConvert.ToInt32(
		public void Init(short intPassType, string strQuestion1, string strQuestion2, string strHeader = "Select One", int intDefSelection = -1)
		{
			this.Text = strHeader;
			intType = intPassType;
			cmbQuestion.Clear();
			cmbQuestion.AddItem(strQuestion1);
			cmbQuestion.AddItem(strQuestion2);
			// kk09192014 trocl-1156  Add optional default selection
			if (intDefSelection != -1)
			{
				cmbQuestion.SelectedIndex = FCConvert.CBool(intDefSelection == 0) ? 0 : 1;
			}
			this.Show(FormShowEnum.Modal);
		}

		private void frmQuestion_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo ERROR_HANDLER
				// MAL@20071109: Removed ESC as an option
				// If KeyCode = vbKeyEscape Then
				// no answer
				// SetOrigVariable -1
				// End If
				if (KeyCode == Keys.F12)
				{
					if (cmbQuestion.SelectedIndex == 0 || cmbQuestion.SelectedIndex == 1)
					{
						if (cmbQuestion.SelectedIndex == 0)
						{
							SetOrigVariable_2(0);
						}
						else if (cmbQuestion.SelectedIndex == 1)
						{
							SetOrigVariable_2(1);
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Prepayment Keydown Error");
			}
		}

		private void frmQuestion_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (KeyAscii == 13)
				{
					// return ID
					if (cmbQuestion.SelectedIndex == 0)
					{
						SetOrigVariable_2(0);
					}
					else if (cmbQuestion.SelectedIndex == 1)
					{
						SetOrigVariable_2(1);
					}
					else
					{
						// no answer
						SetOrigVariable_2(-1);
					}
				}
				// this will make sure that the characters are all uppercase
				if (KeyAscii >= 97 && KeyAscii <= 122)
				{
					KeyAscii -= 32;
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Prepayment Keypress Error");
				e.KeyChar = Strings.Chr(KeyAscii);
			}
		}

		private void frmQuestion_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmQuestion.Icon	= "frmQuestion.frx":0000";
			//frmQuestion.ScaleWidth	= 2895;
			//frmQuestion.ScaleHeight	= 1815;
			//frmQuestion.LinkTopic	= "Form1";
			//frmQuestion.LockControls	= true;
			//Font.Size	= "8.25";
			//Font.Name	= "Tahoma";
			//Font.Weight	= 400;
			//Font.Italic	= 0;
			//Font.Underline	= 0;
			//Font.Strikethrough	= 0;
			//Font.Charset	= 0;
			//vsElasticLight1.OleObjectBlob	= "frmQuestion.frx":058A";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWMINI);
			modGlobalFunctions.SetTRIOColors(this);
			this.Left = FCConvert.ToInt32((FCGlobal.Screen.Width - this.Width) / 2.0);
			this.Top = FCConvert.ToInt32((FCGlobal.Screen.Height - this.Height) / 2.0);
		}

		private void SetOrigVariable_2(short intAnswer)
		{
			SetOrigVariable(ref intAnswer);
		}

		private void SetOrigVariable(ref short intAnswer)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				switch (intType)
				{
					case 10:
						{
							// this is the interest date for lien maturity
							modGlobal.Statics.gintPassQuestion = intAnswer;
							break;
						}
					case 20:
						{
							// mortgage holder master list question
							modGlobal.Statics.gintPassQuestion = intAnswer;
							break;
						}
				}
				//end switch
				Close();
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Set Date Variable Error");
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			// no answer
			SetOrigVariable_2(-1);
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (cmbQuestion.SelectedIndex == 0)
			{
				SetOrigVariable_2(0);
			}
			else if (cmbQuestion.SelectedIndex == 1)
			{
				SetOrigVariable_2(1);
			}
			else
			{
				// no answer
				SetOrigVariable_2(-1);
			}
		}

		private void optQuestion_DoubleClick(short Index, object sender, System.EventArgs e)
		{
			if (cmbQuestion.SelectedIndex == 0)
			{
				SetOrigVariable_2(0);
			}
			else if (cmbQuestion.SelectedIndex == 1)
			{
				SetOrigVariable_2(1);
			}
			else
			{
				// no answer
				SetOrigVariable_2(-1);
			}
		}

		private void optQuestion_1_DoubleClick(object sender, System.EventArgs e)
		{
			optQuestion_DoubleClick(1, sender, e);
		}

		private void optQuestion_0_DoubleClick(object sender, System.EventArgs e)
		{
			optQuestion_DoubleClick(0, sender, e);
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}
	}
}
