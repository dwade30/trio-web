﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;

namespace Global
{
	public class cBudgetaryDB
	{
		//=========================================================
		private string strLastError = "";
		private int lngLastError;

		public string LastErrorMessage
		{
			get
			{
				string LastErrorMessage = "";
				LastErrorMessage = strLastError;
				return LastErrorMessage;
			}
		}

		public int LastErrorNumber
		{
			get
			{
				int LastErrorNumber = 0;
				LastErrorNumber = lngLastError;
				return LastErrorNumber;
			}
		}

		public void ClearErrors()
		{
			strLastError = "";
			lngLastError = 0;
		}

		public cVersionInfo GetVersion()
		{
			cVersionInfo GetVersion = null;
			try
			{
				// On Error GoTo ErrorHandler
				ClearErrors();
				cVersionInfo tVer = new cVersionInfo();
				clsDRWrapper rsLoad = new clsDRWrapper();
				rsLoad.OpenRecordset("select * from dbversion", "Budgetary");
				if (!rsLoad.EndOfFile())
				{
					tVer.Build = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Build"));
					tVer.Major = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Major"));
					tVer.Minor = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Minor"));
					tVer.Revision = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Revision"));
				}
				GetVersion = tVer;
				return GetVersion;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = Information.Err(ex).Description;
				lngLastError = Information.Err(ex).Number;
			}
			return GetVersion;
		}

		public void SetVersion(ref cVersionInfo nVersion)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (!(nVersion == null))
				{
					clsDRWrapper rsSave = new clsDRWrapper();
					rsSave.OpenRecordset("select * from dbversion", "Budgetary");
					if (rsSave.EndOfFile())
					{
						rsSave.AddNew();
					}
					else
					{
						rsSave.Edit();
					}
					rsSave.Set_Fields("Major", nVersion.Major);
					rsSave.Set_Fields("Minor", nVersion.Minor);
					rsSave.Set_Fields("Revision", nVersion.Revision);
					rsSave.Set_Fields("Build", nVersion.Build);
					rsSave.Update();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = Information.Err(ex).Description;
				lngLastError = Information.Err(ex).Number;
			}
		}

		public bool CheckVersion()
		{
			bool CheckVersion = false;
			ClearErrors();
			try
			{
				// On Error GoTo ErrorHandler
				cVersionInfo nVer = new cVersionInfo();
				cVersionInfo tVer = new cVersionInfo();
				cVersionInfo cVer;
				bool boolNeedUpdate;
				clsDRWrapper rsTest = new clsDRWrapper();
				if (!rsTest.DBExists("Budgetary"))
				{
					return CheckVersion;
				}
				cVer = GetVersion();
				if (cVer == null)
				{
					cVer = new cVersionInfo();
				}
				nVer.Major = 1;
				// default to 1.0.0.0
				tVer.Major = 1;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					UpdateEOYAPStandardAccounts();
					nVer.Copy(tVer);
					if (cVer.IsOlder(nVer))
					{
						SetVersion(ref nVer);
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 0;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddCashFund())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 1;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddOverrideCashFunds())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 2;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddAutoPostAP())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 3;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (UpdateLinkedDocuments())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 4;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (AddBankCheckHistoryTable())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}
				
				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 5;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (UpdateVendorLengths())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 6;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (CheckAPJournalReturnedField())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 7;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (CheckArchiveLengths())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

				tVer.Major = 6;
				tVer.Minor = 0;
				tVer.Revision = 8;
				tVer.Build = 0;
				if (tVer.IsNewer(cVer))
				{
					if (Add1099NECFormChanges())
					{
						nVer.Copy(tVer);
						if (cVer.IsOlder(nVer))
						{
							SetVersion(ref nVer);
						}
					}
					else
					{
						return CheckVersion;
					}
				}

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 9;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (RemoveArticleNumber())
                    {
						nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
							SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 10;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AlterClassFieldInVendorInfoAndVendorInfoArchive())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 11;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (Add1099FormTotalsTable())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 12;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (RemoveFormTotalFieldsFrom1099Information())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(ref nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

				CheckVersion = true;
				return CheckVersion;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				strLastError = Information.Err(ex).Description;
				lngLastError = Information.Err(ex).Number;
			}
			return CheckVersion;
		}


        private bool RemoveFormTotalFieldsFrom1099Information()
        {
            try
            {
                var rsSave = new clsDRWrapper();
                var sql =
                    "if exists(select 1 from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = '1099Information' and COLUMN_NAME = 'TotalAmountReported') Begin Alter Table [1099Information] Drop Column TotalAmountReported End";

                rsSave.Execute(sql, "Budgetary");

                sql =
                    "if exists(select 1 from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = '1099Information' and COLUMN_NAME = 'TotalNumberOfFormsPrinted') Begin Alter Table [1099Information] Drop Column TotalNumberOfFormsPrinted End";

                rsSave.Execute(sql, "Budgetary");

                return true;
            }
            catch
            {
                return false;
            }
        }

		private bool Add1099FormTotalsTable()
        {
            try
            {
                // On Error GoTo ErrorHandler
                string strSql = "";
                clsDRWrapper rsSave = new clsDRWrapper();
                if (!TableExists("1099FormTotals", "Budgetary"))
                {
                    strSql = GetTableCreationHeaderSQL("1099FormTotals");
                    strSql += "[FormName] [nvarchar] (50) NULL,";
                    strSql += "[TotalAmountReported] [money] NULL,";
                    strSql += "[TotalNumberOfFormsPrinted] [int] NULL,";
                    strSql += GetTablePKSql("1099FormTotals");
                    rsSave.Execute(strSql, "Budgetary", false);
                }

                return true;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
            }
            return false;
		}

		private bool AlterClassFieldInVendorInfoAndVendorInfoArchive()
        {
            try
            {
                clsDRWrapper rsUpdate = new clsDRWrapper();
                rsUpdate.OpenRecordset(
                    "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'VendorTaxInfo' AND COLUMN_NAME = 'Class'",
                    "Budgetary");
                if (rsUpdate.Get_Fields("Data_Type") == "nvarchar")
                {
                    rsUpdate.Execute("Alter Table VendorTaxInfo alter column Class int NULL",
                        "Budgetary");
                }

                rsUpdate.OpenRecordset(
                    "SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'VendorTaxInfoArchive' AND COLUMN_NAME = 'Class'",
                    "Budgetary");
                if (rsUpdate.Get_Fields("Data_Type") == "nvarchar")
                {
                    rsUpdate.Execute("Alter Table VendorTaxInfoArchive alter column Class int NULL",
                        "Budgetary");
                }

				return true;
            }
			catch (Exception ex)
            {
                // ErrorHandler:
            }

            return false;
		}

		private bool Add1099NECFormChanges()
		{
			clsDRWrapper rsUpdate = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'TaxTitles' AND COLUMN_NAME = 'FormName'", "Budgetary");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("ALTER TABLE TaxTitles ADD FormName nvarchar(50) NULL", "Budgetary");
					rsUpdate.Execute("UPDATE TaxTitles SET FormName = 'MISC' WHERE Category <> 7", "Budgetary");
					rsUpdate.Execute("UPDATE TaxTitles SET FormName = 'NEC', Category = 1 WHERE Category = 7", "Budgetary");
				}

				return true;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}

			return false;
		}

		private bool AddCashFund()
		{
			bool AddCashFund = false;
			clsDRWrapper rsUpdate = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LedgerTitles' AND COLUMN_NAME = 'CashFund'", "Budgetary");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("ALTER TABLE LedgerTitles ADD CashFund nvarchar(20) NULL", "Budgetary");
				}
				AddCashFund = true;
				return AddCashFund;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddCashFund;
		}

		private bool AddOverrideCashFunds()
		{
			bool AddOverrideCashFunds = false;
			clsDRWrapper rsUpdate = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LedgerTitles' AND COLUMN_NAME = 'PYOverrideCashFund'", "Budgetary");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("ALTER TABLE LedgerTitles ADD PYOverrideCashFund nvarchar(20) NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE LedgerTitles ADD CROverrideCashFund nvarchar(20) NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE LedgerTitles ADD APOverrideCashFund nvarchar(20) NULL", "Budgetary");
					modBudgetaryAccounting.InitializeFundDueToFromInfo();
				}
				AddOverrideCashFunds = true;
				return AddOverrideCashFunds;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddOverrideCashFunds;
		}

		private bool AddAutoPostAP()
		{
			bool AddAutoPostAP = false;
			clsDRWrapper rsUpdate = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Budgetary' AND COLUMN_NAME = 'AutoPostAP'", "Budgetary");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostAP bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostGJ bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostEN bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostCR bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostCD bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostCM bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostAPCorr bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostReversingJournals bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostBudgetTransfer bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostCreateOpeningAdjustments bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostARBills bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostCRDailyAudit bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostFADepreciation bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostMARR bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostPYProcesses bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostCLLiens bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostCLTA bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostBLCommitments bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostBLSupplementals bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostUBBills bit NULL", "Budgetary");
					rsUpdate.Execute("ALTER TABLE Budgetary ADD AutoPostUBLiens bit NULL", "Budgetary");
				}
				AddAutoPostAP = true;
				return AddAutoPostAP;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddAutoPostAP;
		}

		private bool UpdateLinkedDocuments()
		{
			bool UpdateLinkedDocuments = false;
			clsDRWrapper rsUpdate = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LinkedDocuments' AND COLUMN_NAME = 'VendorNumber'", "Budgetary");
				if (rsUpdate.RecordCount() > 0)
				{
					rsUpdate.Execute("Exec sp_rename 'LinkedDocuments.VendorNumber', 'DocReferenceID', 'COLUMN'", "Budgetary");
				}
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'LinkedDocuments' AND COLUMN_NAME = 'DocReferenceType'", "Budgetary");
				if (rsUpdate.RecordCount() == 0)
				{
					rsUpdate.Execute("ALTER TABLE LinkedDocuments ADD DocReferenceType varchar (255) NULL", "Budgetary");
					rsUpdate.Execute("Update LinkedDocuments set DocReferenceType = 'Budgetary_Vendor'", "Budgetary");
				}
				UpdateLinkedDocuments = true;
				return UpdateLinkedDocuments;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return UpdateLinkedDocuments;
		}

		private void UpdateEOYAPStandardAccounts()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Type = 'T'", "Budgetary");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				if (rsInfo.RecordCount() < 26)
				{
					rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Row >= 13 AND Type = 'T' ORDER BY Row DESC", "Budgetary");
					if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
					{
						do
						{
							rsInfo.Edit();
							rsInfo.Set_Fields("Row", FCConvert.ToInt16(rsInfo.Get_Fields_Int32("Row")) + 1);
							rsInfo.Update();
							rsInfo.MoveNext();
						}
						while (rsInfo.EndOfFile() != true);
					}
					rsInfo.AddNew();
					rsInfo.Set_Fields("Row", 13);
					rsInfo.Set_Fields("Account", "");
					rsInfo.Set_Fields("ShortDescription", "EOY Acct Pay");
					rsInfo.Set_Fields("LongDescription", "EOY Accounts Payable");
					rsInfo.Set_Fields("Code", "AP");
					rsInfo.Set_Fields("Type", "T");
					rsInfo.Update();
				}
			}
			rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Type = 'S'", "Budgetary");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				if (rsInfo.RecordCount() < 13)
				{
					rsInfo.OpenRecordset("SELECT * FROM StandardAccounts WHERE Row >= 13 AND Type = 'S' ORDER BY Row DESC", "Budgetary");
					if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
					{
						do
						{
							rsInfo.Edit();
							rsInfo.Set_Fields("Row", FCConvert.ToInt16(rsInfo.Get_Fields_Int32("Row")) + 1);
							rsInfo.Update();
							rsInfo.MoveNext();
						}
						while (rsInfo.EndOfFile() != true);
					}
					rsInfo.AddNew();
					rsInfo.Set_Fields("Row", 12);
					rsInfo.Set_Fields("Account", "");
					rsInfo.Set_Fields("ShortDescription", "EOY Acct Pay");
					rsInfo.Set_Fields("LongDescription", "EOY Accounts Payable");
					rsInfo.Set_Fields("Code", "SAP");
					rsInfo.Set_Fields("Type", "S");
					rsInfo.Update();
				}
			}
		}

		private bool AddBankCheckHistoryTable()
		{
			bool AddBankCheckHistoryTable = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strSql = "";
				clsDRWrapper rsSave = new clsDRWrapper();
				if (!TableExists("BankCheckHistory", "Budgetary"))
				{
					strSql = GetTableCreationHeaderSQL("BankCheckHistory");
					strSql += "[BankID] [int] NULL,";
					strSql += "[CheckType] [nvarchar] (255) NULL,";
					strSql += "[LastCheckNumber] [int] NULL,";
					strSql += GetTablePKSql("BankCheckHistory");
					rsSave.Execute(strSql, "Budgetary", false);
				}
				AddBankCheckHistoryTable = true;
				return AddBankCheckHistoryTable;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return AddBankCheckHistoryTable;
		}

		private bool FieldExists(string strTable, string strField, string strDB)
		{
			bool FieldExists = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strSql;
				clsDRWrapper rsLoad = new clsDRWrapper();
				strSql = "Select column_name from information_schema.columns where column_name = '" + strField + "' and table_name = '" + strTable + "'";
				rsLoad.OpenRecordset(strSql, strDB);
				if (!rsLoad.EndOfFile())
				{
					FieldExists = true;
				}
				return FieldExists;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return FieldExists;
		}

		private bool TableExists(string strTable, string strDB)
		{
			bool TableExists = false;
			try
			{
				// On Error GoTo ErrorHandler
				string strSql;
				clsDRWrapper rsLoad = new clsDRWrapper();
				strSql = "select * from sys.tables where name = '" + strTable + "' and type = 'U'";
				rsLoad.OpenRecordset(strSql, strDB);
				if (!rsLoad.EndOfFile())
				{
					TableExists = true;
				}
				return TableExists;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return TableExists;
		}

		private string GetTableCreationHeaderSQL(string strTableName)
		{
			string GetTableCreationHeaderSQL = "";
			string strSql;
			strSql = "if not exists (select * from information_schema.tables where table_name = N'" + strTableName + "') " + "\r\n";
			strSql += "Create Table [dbo].[" + strTableName + "] (";
			strSql += "[ID] [int] IDENTITY (1,1) NOT NULL,";
			GetTableCreationHeaderSQL = strSql;
			return GetTableCreationHeaderSQL;
		}

		private string GetTablePKSql(string strTableName)
		{
			string GetTablePKSql = "";
			string strSql;
			strSql = " Constraint [PK_" + strTableName + "] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
			GetTablePKSql = strSql;
			return GetTablePKSql;
		}

		private bool UpdateVendorLengths()
		{
			bool UpdateVendorLengths = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsUpdate = new clsDRWrapper();
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'VendorMaster' AND COLUMN_NAME = 'CheckName'", "Budgetary");
				// TODO: Field [Character_Maximum_Length] not found!! (maybe it is an alias?)
				if (FCConvert.ToInt32(rsUpdate.Get_Fields("Character_Maximum_Length")) < 50)
				{
					rsUpdate.Execute("Alter Table VendorMaster alter column CheckName [nvarchar] (50) NULL", "Budgetary");
				}
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'VendorMaster' AND COLUMN_NAME = 'CorrespondName'", "Budgetary");
				// TODO: Field [Character_Maximum_Length] not found!! (maybe it is an alias?)
				if (FCConvert.ToInt32(rsUpdate.Get_Fields("Character_Maximum_Length")) < 50)
				{
					rsUpdate.Execute("Alter Table VendorMaster alter column CorrespondName [nvarchar] (50) NULL", "Budgetary");
				}
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'APJournal' AND COLUMN_NAME = 'TempVendorName'", "Budgetary");
				// TODO: Field [Character_Maximum_Length] not found!! (maybe it is an alias?)
				if (FCConvert.ToInt32(rsUpdate.Get_Fields("Character_Maximum_Length")) < 50)
				{
					rsUpdate.Execute("Alter Table APJournal alter column TempVendorName [nvarchar] (50) NULL", "Budgetary");
				}
				rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'Encumbrances' AND COLUMN_NAME = 'TempVendorName'", "Budgetary");
				// TODO: Field [Character_Maximum_Length] not found!! (maybe it is an alias?)
				if (FCConvert.ToInt32(rsUpdate.Get_Fields("Character_Maximum_Length")) < 50)
				{
					rsUpdate.Execute("Alter Table Encumbrances alter column TempVendorName [nvarchar] (50) NULL", "Budgetary");
				}
				UpdateVendorLengths = true;
				return UpdateVendorLengths;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return UpdateVendorLengths;
		}

		private bool CheckAPJournalReturnedField()
		{
			bool CheckAPJournalReturnedField = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.Execute("update APJournal set Returned = 'N' where isnull(Returned,'') = ''", "Budgetary");
				CheckAPJournalReturnedField = true;
				return CheckAPJournalReturnedField;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return CheckAPJournalReturnedField;
		}
		
		private bool CreateRecalcStats()
		{
			bool CreateRecalcStats = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				string strSql;
				strSql = "select * from sys.indexes where object_id = object_id('ExpenseReportInfo') and name = '_dta_index_ExpenseReportInfo_68_1803153469__K9_K8_K10_K11_K13_K14_K12_K20_K17_K18_1'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_ExpenseReportInfo_68_1803153469__K9_K8_K10_K11_K13_K14_K12_K20_K17_K18_1] ON [dbo].[ExpenseReportInfo](";
					strSql += " [OriginalBudget] ASC,[BudgetAdjustments] ASC,[PostedDebits] ASC,[PostedCredits] ASC,[PendingDebits] ASC,";
					strSql += "[PendingCredits] ASC,[EncumbActivity] ASC,[AutomaticBudgetAdjustments] ASC,[MonthlyBudget] ASC,[MonthlyBudgetAdjustments] Asc)";
					strSql += "INCLUDE (   [ID]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('ExpenseReportInfo') and name = '_dta_index_ExpenseReportInfo_68_1803153469__K2'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_ExpenseReportInfo_68_1803153469__K2] ON [dbo].[ExpenseReportInfo](";
					strSql += " [Account] Asc)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1803153469_2_3' and  object_id = OBJECT_ID('ExpenseReportInfo')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1803153469_2_3] ON [dbo].[ExpenseReportInfo]([Account], [Period])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('LedgerReportInfo') and name = '_dta_index_LedgerReportInfo_68_1988202133__K17_K8_K7_K9_K10_K12_K13_K11_1'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_LedgerReportInfo_68_1988202133__K17_K8_K7_K9_K10_K12_K13_K11_1] ON [dbo].[LedgerReportInfo] ";
					strSql += "([AutomaticBudgetAdjustments] ASC,[OriginalBudget] ASC,[BudgetAdjustments] ASC,[PostedDebits] ASC,";
					strSql += "[PostedCredits] ASC,[PendingDebits] ASC,[PendingCredits] ASC,[EncumbActivity] Asc)";
					strSql += " INCLUDE (   [ID]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('LedgerReportInfo') and name = '_dta_index_LedgerReportInfo_68_1988202133__K2'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_LedgerReportInfo_68_1988202133__K2] ON [dbo].[LedgerReportInfo](";
					strSql += "[Account] Asc)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1988202133_2_3' and  object_id = OBJECT_ID('LedgerReportInfo')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1988202133_2_3] ON [dbo].[LedgerReportInfo]([Account], [Period])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1988202133_8_7_9' and  object_id = OBJECT_ID('LedgerReportInfo')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1988202133_8_7_9] ON [dbo].[LedgerReportInfo]([OriginalBudget], [BudgetAdjustments], [PostedDebits])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1988202133_9_10_8_17' and  object_id = OBJECT_ID('LedgerReportInfo')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1988202133_9_10_8_17] ON [dbo].[LedgerReportInfo]([PostedDebits], [PostedCredits], [OriginalBudget], [AutomaticBudgetAdjustments])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1988202133_12_8_7_9' and  object_id = OBJECT_ID('LedgerReportInfo')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1988202133_12_8_7_9] ON [dbo].[LedgerReportInfo]([PendingDebits], [OriginalBudget], [BudgetAdjustments], [PostedDebits])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1988202133_13_8_7_9_10' and  object_id = OBJECT_ID('LedgerReportInfo')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1988202133_13_8_7_9_10] ON [dbo].[LedgerReportInfo]([PendingCredits], [OriginalBudget], [BudgetAdjustments], [PostedDebits], [PostedCredits])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1988202133_11_8_7_9_10_12' and  object_id = OBJECT_ID('LedgerReportInfo')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1988202133_11_8_7_9_10_12] ON [dbo].[LedgerReportInfo]([EncumbActivity], [OriginalBudget], [BudgetAdjustments], [PostedDebits], [PostedCredits], [PendingDebits])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1988202133_10_8_7_9_12_13_11' and  object_id = OBJECT_ID('LedgerReportInfo')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1988202133_10_8_7_9_12_13_11] ON [dbo].[LedgerReportInfo]([PostedCredits], [OriginalBudget], [BudgetAdjustments], [PostedDebits], [PendingDebits], [PendingCredits], [EncumbActivity])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('RevenueReportInfo') and name = '_dta_index_RevenueReportInfo_68_893246237__K8_K7_K9_K10_K12_K13_K11_K19_K16_K17_1'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_RevenueReportInfo_68_893246237__K8_K7_K9_K10_K12_K13_K11_K19_K16_K17_1] ON [dbo].[RevenueReportInfo] ";
					strSql += "([OriginalBudget] ASC,[BudgetAdjustments] ASC,[PostedDebits] ASC,[PostedCredits] ASC,";
					strSql += "[PendingDebits] ASC,[PendingCredits] ASC,[EncumbActivity] ASC,[AutomaticBudgetAdjustments] ASC,";
					strSql += "[MonthlyBudget] ASC,[MonthlyBudgetAdjustments] Asc";
					strSql += ") INCLUDE (   [ID]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('RevenueReportInfo') and name = '_dta_index_RevenueReportInfo_68_893246237__K2'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_RevenueReportInfo_68_893246237__K2] ON [dbo].[RevenueReportInfo]";
					strSql += "([Account] Asc)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_893246237_2_3' and  object_id = OBJECT_ID('RevenueReportInfo')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_893246237_2_3] ON [dbo].[RevenueReportInfo]([Account], [Period])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('JournalEntries') and name = '_dta_index_JournalEntries_68_1607676775__K14_K13_K6_K8_K12_9_10'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_JournalEntries_68_1607676775__K14_K13_K6_K8_K12_9_10] ON [dbo].[JournalEntries]";
					strSql += "([Status] ASC,[RCB] ASC,[JournalNumber] ASC,[Account] ASC,[Period] Asc)";
					strSql += "INCLUDE (   [Project],[Amount]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('JournalEntries') and name = '_dta_index_JournalEntries_68_1607676775__K14_K13_K2_K12_K8_9_10'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_JournalEntries_68_1607676775__K14_K13_K2_K12_K8_9_10] ON [dbo].[JournalEntries]";
					strSql += "([Status] ASC,[RCB] ASC,[Type] ASC,[Period] ASC,[Account] ASC)";
					strSql += "INCLUDE (   [Project],[Amount]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('JournalEntries') and name = '_dta_index_JournalEntries_68_1607676775__K14_K2_K13_K12_K10_K8_9'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_JournalEntries_68_1607676775__K14_K2_K13_K12_K10_K8_9] ON [dbo].[JournalEntries]";
					strSql += "([Status] ASC,[Type] ASC,[RCB] ASC,[Period] ASC,[Amount] ASC,[Account] ASC)";
					strSql += "INCLUDE (   [Project]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('JournalEntries') and name = '_dta_index_JournalEntries_68_1607676775__K13_K18_K14_K8_K12_9_10'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_JournalEntries_68_1607676775__K13_K18_K14_K8_K12_9_10] ON [dbo].[JournalEntries]";
					strSql += "([RCB] ASC,[CarryForward] ASC,[Status] ASC,[Account] ASC,[Period] ASC)";
					strSql += "INCLUDE (   [Project],[Amount]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('JournalEntries') and name = '_dta_index_JournalEntries_68_1607676775__K14_K13_K12_K10_K8_9'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_JournalEntries_68_1607676775__K14_K13_K12_K10_K8_9] ON [dbo].[JournalEntries]";
					strSql += "([Status] ASC,[RCB] ASC,[Period] ASC,[Amount] ASC,[Account] ASC)";
					strSql += "INCLUDE (   [Project]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('JournalEntries') and name = '_dta_index_JournalEntries_68_1607676775__K10_K14_K13_K12_K8_9'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_JournalEntries_68_1607676775__K10_K14_K13_K12_K8_9] ON [dbo].[JournalEntries]";
					strSql += "([Amount] ASC,[Status] ASC,[RCB] ASC,[Period] ASC,[Account] ASC";
					strSql += ")INCLUDE (   [Project]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('JournalEntries') and name = '_dta_index_JournalEntries_68_1607676775__K13_K10_K14_K12_K8_9'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_JournalEntries_68_1607676775__K13_K10_K14_K12_K8_9] ON [dbo].[JournalEntries]";
					strSql += "([RCB] ASC,[Amount] ASC,[Status] ASC,[Period] ASC,[Account] ASC)";
					strSql += "INCLUDE (   [Project]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('JournalEntries') and name = '_dta_index_JournalEntries_68_1607676775__K12_K8_9'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_JournalEntries_68_1607676775__K12_K8_9] ON [dbo].[JournalEntries]";
					strSql += "([Period] ASC,[Account] ASC)";
					strSql += "INCLUDE (   [Project]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1607676775_6_14' and  object_id = OBJECT_ID('JournalEntries')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1607676775_6_14] ON [dbo].[JournalEntries]([JournalNumber], [Status])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1607676775_13_6' and  object_id = OBJECT_ID('JournalEntries')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1607676775_13_6] ON [dbo].[JournalEntries]([RCB], [JournalNumber])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1607676775_12_10_14' and  object_id = OBJECT_ID('JournalEntries')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1607676775_12_10_14] ON [dbo].[JournalEntries]([Period], [Amount], [Status])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1607676775_8_12_14_13' and  object_id = OBJECT_ID('JournalEntries')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1607676775_8_12_14_13] ON [dbo].[JournalEntries]([Account], [Period], [Status], [RCB])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1607676775_2_10_14_13' and  object_id = OBJECT_ID('JournalEntries')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1607676775_2_10_14_13] ON [dbo].[JournalEntries]([Type], [Amount], [Status], [RCB])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1607676775_18_14_13_8' and  object_id = OBJECT_ID('JournalEntries')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1607676775_18_14_13_8] ON [dbo].[JournalEntries]([CarryForward], [Status], [RCB], [Account])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1607676775_14_13_8_12_6' and  object_id = OBJECT_ID('JournalEntries')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1607676775_14_13_8_12_6] ON [dbo].[JournalEntries]([Status], [RCB], [Account], [Period], [JournalNumber])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1607676775_14_13_8_12_18' and  object_id = OBJECT_ID('JournalEntries')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1607676775_14_13_8_12_18] ON [dbo].[JournalEntries]([Status], [RCB], [Account], [Period], [CarryForward])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1607676775_8_12_14_2_13' and  object_id = OBJECT_ID('JournalEntries')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1607676775_8_12_14_2_13] ON [dbo].[JournalEntries]([Account], [Period], [Status], [Type], [RCB])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1607676775_13_10_14_12_2_8' and  object_id = OBJECT_ID('JournalEntries')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1607676775_13_10_14_12_2_8] ON [dbo].[JournalEntries]([RCB], [Amount], [Status], [Period], [Type], [Account])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('APJournalDetail') and name = '_dta_index_APJournalDetail_68_1525580473__K2_3_4_5_6_7_8_10_12'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_APJournalDetail_68_1525580473__K2_3_4_5_6_7_8_10_12] ON [dbo].[APJournalDetail]";
					strSql += "([APJournalID] ASC)";
					strSql += " INCLUDE (   [Description],[Account],[Amount],[Project],[Discount],[Encumbrance],[RCB],";
					strSql += " [EncumbranceDetailRecord]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('APJournalDetail') and name = '_dta_index_APJournalDetail_68_1525580473__K10_K5_K2_K4_6_7'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_APJournalDetail_68_1525580473__K10_K5_K2_K4_6_7] ON [dbo].[APJournalDetail]";
					strSql += "([RCB] ASC,[Amount] ASC,[APJournalID] ASC,[Account] ASC)";
					strSql += "INCLUDE (   [Project],[Discount]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('APJournalDetail') and name = '_dta_index_APJournalDetail_68_1525580473__K5_K10_K2_K4_6_7'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_APJournalDetail_68_1525580473__K5_K10_K2_K4_6_7] ON [dbo].[APJournalDetail]";
					strSql += "([Amount] ASC,[RCB] ASC,[APJournalID] ASC,[Account] ASC)";
					strSql += "INCLUDE (   [Project],[Discount]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('APJournalDetail') and name = '_dta_index_APJournalDetail_68_1525580473__K4_2_6_8'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_APJournalDetail_68_1525580473__K4_2_6_8] ON [dbo].[APJournalDetail]";
					strSql += "([Account] ASC)";
					strSql += "INCLUDE (   [APJournalID],[Project],[Encumbrance]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('APJournalDetail') and name = '_dta_index_APJournalDetail_68_1525580473__K2_K4_6_8'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_APJournalDetail_68_1525580473__K2_K4_6_8] ON [dbo].[APJournalDetail]";
					strSql += "([APJournalID] ASC,[Account] ASC)";
					strSql += "INCLUDE (   [Project],[Encumbrance]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('APJournalDetail') and name = '_dta_index_APJournalDetail_68_1525580473__K2_K4_6'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_APJournalDetail_68_1525580473__K2_K4_6] ON [dbo].[APJournalDetail]";
					strSql += "([APJournalID] ASC,[Account] ASC)";
					strSql += "INCLUDE (   [Project]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1525580473_2_10' and  object_id = OBJECT_ID('APJournalDetail')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1525580473_2_10] ON [dbo].[APJournalDetail]([APJournalID], [RCB])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1525580473_4_10_5' and  object_id = OBJECT_ID('APJournalDetail')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1525580473_4_10_5] ON [dbo].[APJournalDetail]([Account], [RCB], [Amount])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_1525580473_5_2_10_4' and  object_id = OBJECT_ID('APJournalDetail')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_1525580473_5_2_10_4] ON [dbo].[APJournalDetail]([Amount], [APJournalID], [RCB], [Account])";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('APJournal') and name = '_dta_index_APJournal_68_949578421__K1_K9_2_3_17_18_19_20_21'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_APJournal_68_949578421__K1_K9_2_3_17_18_19_20_21] ON [dbo].[APJournal]";
					strSql += "([ID] ASC,[Period] ASC)";
					strSql += "INCLUDE (   [JournalNumber],[VendorNumber],[CheckNumber],[Status],[PostedDate],[Warrant],";
					strSql += "[CheckDate]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('APJournal') and name = '_dta_index_APJournal_68_949578421__K18_K9_K1'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_APJournal_68_949578421__K18_K9_K1] ON [dbo].[APJournal]";
					strSql += "([Status] ASC,[Period] ASC,[ID] ASC)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('APJournal') and name = '_dta_index_APJournal_68_949578421__K18_K1_K9'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_APJournal_68_949578421__K18_K1_K9] ON [dbo].[APJournal]";
					strSql += "([Status] ASC,[ID] ASC,[Period] ASC) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('APJournal') and name = '_dta_index_APJournal_68_949578421__K1_K18_K9'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_APJournal_68_949578421__K1_K18_K9] ON [dbo].[APJournal]";
					strSql += "([ID] ASC,[Status] ASC,[Period] ASC)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('APJournal') and name = '_dta_index_APJournal_68_949578421__K1_K9_K18'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_APJournal_68_949578421__K1_K9_K18] ON [dbo].[APJournal]";
					strSql += "([ID] ASC,[Period] ASC,[Status] ASC)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('APJournal') and name = '_dta_index_APJournal_68_949578421__K9_K1'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_APJournal_68_949578421__K9_K1] ON [dbo].[APJournal]";
					strSql += "([Period] ASC,[ID] ASC)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('LedgerDetailInfo') and name = '_dta_index_LedgerDetailInfo_68_1028198713__K10_1'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_LedgerDetailInfo_68_1028198713__K10_1] ON [dbo].[LedgerDetailInfo]";
					strSql += "([RCB] ASC)INCLUDE (   [ID]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('LedgerDetailInfo') and name = '_dta_index_LedgerDetailInfo_68_1028198713__K3'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_LedgerDetailInfo_68_1028198713__K3] ON [dbo].[LedgerDetailInfo]";
					strSql += "([Period] ASC)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('ExpenseDetailInfo') and name = '_dta_index_ExpenseDetailInfo_68_1195151303__K11_1'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_ExpenseDetailInfo_68_1195151303__K11_1] ON [dbo].[ExpenseDetailInfo]";
					strSql += "([RCB] ASC) INCLUDE (   [ID]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('ExpenseDetailInfo') and name = '_dta_index_ExpenseDetailInfo_68_1195151303__K3'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_ExpenseDetailInfo_68_1195151303__K3] ON [dbo].[ExpenseDetailInfo]";
					strSql += "([Period] ASC)WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('RevenueDetailInfo') and name = '_dta_index_RevenueDetailInfo_68_253243957__K3_1'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_RevenueDetailInfo_68_253243957__K3_1] ON [dbo].[RevenueDetailInfo]";
					strSql += "([Period] ASC)INCLUDE (   [ID]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.indexes where object_id = object_id('RevenueDetailInfo') and name = '_dta_index_RevenueDetailInfo_68_253243957__K10_1'";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE NONCLUSTERED INDEX [_dta_index_RevenueDetailInfo_68_253243957__K10_1] ON [dbo].[RevenueDetailInfo]";
					strSql += "([RCB] ASC)INCLUDE (   [ID]) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [PRIMARY]";
					rsSave.Execute(strSql, "Budgetary");
				}
				strSql = "select * from sys.stats where name = '_dta_stat_597577167_3_25_26_27_28_29_30_31_32_33_34_35_36' and  object_id = OBJECT_ID('AccountMaster')";
				rsSave.OpenRecordset(strSql, "Budgetary");
				if (rsSave.EndOfFile())
				{
					strSql = "CREATE STATISTICS [_dta_stat_597577167_3_25_26_27_28_29_30_31_32_33_34_35_36] ON [dbo].[AccountMaster]([AccountType], [JanPercent], [FebPercent], [MarPercent], [AprPercent], [MayPercent], [JunePercent], [JulyPercent], [AugPercent], [SeptPercent], [OctPercent], [NovPercent], [DecPercent])";
					rsSave.Execute(strSql, "Budgetary");
				}
				CreateRecalcStats = true;
				return CreateRecalcStats;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return CreateRecalcStats;
		}

		private bool CheckArchiveLengths()
		{
			bool CheckArchiveLengths = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select column_name, character_maximum_length from information_schema.columns where table_name = 'APJournalArchive' and column_name = 'tempvendorname'", "Budgetary");
				if (!rsSave.EndOfFile())
				{
					// TODO: Field [character_maximum_length] not found!! (maybe it is an alias?)
					if (FCConvert.ToInt32(rsSave.Get_Fields("character_maximum_length")) < 50)
					{
						rsSave.Execute("alter table apjournalarchive alter column TempVendorName nvarchar(50) NULL", "Budgetary");
					}
				}
				CheckArchiveLengths = true;
				return CheckArchiveLengths;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
			}
			return CheckArchiveLengths;
		}

        private bool RemoveArticleNumber()
        {
            var rsSave = new clsDRWrapper();
            var sql =
                "if exists(select 1 from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = 'AccountMaster' and COLUMN_NAME = 'ArticleNumber') Begin Alter Table AccountMaster Drop Column ArticleNumber End";

            rsSave.Execute(sql, "Budgetary");

            return true;
        }
	}
}
