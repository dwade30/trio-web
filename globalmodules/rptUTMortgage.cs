﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Global;
using TWSharedLibrary;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptUTMortgage : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Corey Grey              *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :               Melissa Lamprecht       *
		// LAST UPDATED   :               09/05/2007              *
		// ********************************************************
		// MAL@20070905: Changed code (borrowed from RE report) to conform to utility requirements
		clsDRWrapper clsre = new clsDRWrapper();
		int lngLastAccount;
		double dblTotalSum;

		public rptUTMortgage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Mortgage Holder List";
		}

		public static rptUTMortgage InstancePtr
		{
			get
			{
				return (rptUTMortgage)Sys.GetInstance(typeof(rptUTMortgage));
			}
		}

		protected rptUTMortgage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				clsre?.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = clsre.EndOfFile();
			//Detail_Format();
		}

		public void Init(bool boolNameOrder)
		{
			string strSQL;
			string strOrder = "";
			string strDBName;
			string strPartyDBName = "";
			string strJoin;
			if (boolNameOrder)
			{
				// strOrder = " ORDER BY Master.OwnerName, Master.AccountNumber "
				strOrder = " ORDER BY OwnerName, Mparty.AccountNumber ";
				// trouts-226 kjr 04.13.2017
			}
			else
			{
				// strOrder = " ORDER BY Master.AccountNumber "
				strOrder = " ORDER BY Mparty.AccountNumber ";
			}
			lblTotalDue.Text = "Total Due as of " + Strings.Format(DateTime.Today, "MM/dd/yy");
			strDBName = clsre.Get_GetFullDBName("CentralData") + ".dbo.";
			strJoin = GetMasterJoinForJoin();
			// trouts-226 kjr 04.13.2017
			// Call clsre.OpenRecordset("SELECT * from Master INNER JOIN (" & strDBName & "MortgageAssociation MortAssoc inner join " & strDBName & "MortgageHolders on (MortAssoc.MortgageHolderID = MortgageHolders.ID)) on (Master.AccountNumber = MortAssoc.Account) where MortAssoc.Module = 'UT' and NOT (Master.Deleted = 1) " & strOrder) ', "twre0000.vb1")
			strSQL = "select *,mortgageholders.id as mortid from " + strJoin + " inner join (" + strDBName + "mortgageassociation inner join " + strDBName + "mortgageholders on (mortgageassociation.mortgageholderid = mortgageholders.id)) on (mparty.accountnumber = mortgageassociation.account) where mortgageassociation.module = 'UT' and not (mparty.deleted = 1)" + strOrder;
			// 
			clsre.OpenRecordset(strSQL, "UtilityBilling");
			// trouts-226 kjr 04.13.2017
			// Me.Show , MDIParent
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			lngLastAccount = 0;
			txtDate.Text = DateTime.Today.ToString();
			txtTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			txtMuniname.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				double dblDue = 0;
				bool boolDup = false;
				if (!clsre.EndOfFile())
				{
					if (Conversion.Val(clsre.Get_Fields_Int32("ID")) != lngLastAccount)
					{
						// trouts-226 kjr 04.13.2017
						//FC:FINAL:MSH - can't implicitly convert from int to string
						// TODO: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						txtaccount.Text = FCConvert.ToString(clsre.Get_Fields("AccountNumber"));
						// TODO: Field [OwnerName] not found!! (maybe it is an alias?)
						txtName.Text = clsre.Get_Fields("OwnerName");
						lngLastAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(clsre.Get_Fields_Int32("ID"))));
						boolDup = false;
					}
					else
					{
						txtaccount.Text = "";
						txtName.Text = "";
						boolDup = true;
					}
					if (FCConvert.ToBoolean(clsre.Get_Fields_Boolean("Escrowed")))
					{
						txtEscrow.Text = "Yes";
					}
					else
					{
						txtEscrow.Text = "No";
					}
					if (FCConvert.ToBoolean(clsre.Get_Fields_Boolean("ReceiveBill")))
					{
						txtBill.Text = "Yes";
					}
					else
					{
						txtBill.Text = "No";
					}
					txtHolderNumber.Text = FCConvert.ToString(clsre.Get_Fields_Int32("MortgageHolderID"));
					// "MortAssoc.MortgageHolderID")
					txtHolderName.Text = clsre.Get_Fields_String("Name");
					// "MortAssoc.Name")
					if (!boolDup)
					{
						// Water Total
						dblDue = modUTCalculations.CalculateAccountUTTotal(lngLastAccount, true);
						// Sewer Total
						dblDue += modUTCalculations.CalculateAccountUTTotal(lngLastAccount, false);
						fldTotalDue.Text = Strings.Format(dblDue, "$#,##0.00");
					}
					else
					{
						fldTotalDue.Text = "";
					}
					dblTotalSum += dblDue;
					clsre.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error In Detail Format");
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotalTotalDue.Text = Strings.Format(dblTotalSum, "$#,##0.00");
		}

		private string GetMasterJoinForJoin()
		{
			string GetMasterJoinForJoin = "";
			// trouts-226 kjr 04.13.2017
			string strReturn;
			strReturn = "select master.*, cpo.FullNameLF as OwnerName from ";
			clsDRWrapper tLoad = new clsDRWrapper();
			string strTemp;
			strTemp = tLoad.Get_GetFullDBName("UtilityBilling");
			strReturn += strTemp + ".dbo.master left join ";
			strTemp = tLoad.Get_GetFullDBName("CentralParties");
			strTemp += ".dbo.PartyAndAddressView ";
			strReturn += strTemp + " as cpo on (master.ownerpartyid = cpo.ID) left join ";
			strReturn += strTemp + " as cpso on (master.SecondOwnerPartyID = cpso.ID)";
			strReturn = " (" + strReturn + ") mparty ";
			GetMasterJoinForJoin = strReturn;
			tLoad.Dispose();
			return GetMasterJoinForJoin;
		}

		
	}
}
