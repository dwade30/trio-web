﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;

namespace Global
{
	public class cE9SecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cE9SecuritySetup() : base()
		{
			strThisModule = "E9";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem(" Enter Enhanced 911", "", 1, false, ""));
			AddItem_2(CreateItem("Account Maintenance", "", 8, true, ""));
			AddItem_2(CreateItem("Account Maintenance", "Add New Location", 9, false, ""));
			AddItem_2(CreateItem("Account Maintenance", "Delete Location", 10, false, ""));
			AddItem_2(CreateItem("Account Maintenance", "Save Location", 7, false, ""));
			AddItem_2(CreateItem("Default Parameter Information", "", 4, true, ""));
			AddItem_2(CreateItem("Default Parameter Information", "Save Information", 12, false, ""));
			AddItem_2(CreateItem("Print Account Information", "", 3, false, ""));
			AddItem_2(CreateItem("Search for Account", "", 2, false, ""));
			AddItem_2(CreateItem("Street Maintenance", "", 5, true, ""));
			AddItem_2(CreateItem("Street Maintenance", "Delete Street Information", 16, false, ""));
			AddItem_2(CreateItem("Street Maintenance", "Edit Street Information", 15, false, ""));
			AddItem_2(CreateItem("Street Maintenance", "Save Street Information", 14, false, ""));
			AddItem_2(CreateItem("Street Maintenance", "Search for Street", 17, false, ""));
		}
	}
}
