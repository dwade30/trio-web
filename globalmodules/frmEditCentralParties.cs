﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

#if TWCL0000
using TWCL0000;


#elif TWAR0000
using TWAR0000;


#elif TWCR0000
using TWCR0000;


#elif TWPP0000
using modGlobal = TWPP0000.modGNWork;


#elif TWGNENTY
using TWGNENTY;
using modGlobal = TWGNENTY.modGlobalRoutines;


#elif TWUT0000
using modGlobal = TWUT0000.modMain;


#elif TWRE0000
using modGlobal = TWRE0000.modMDIParent;


#elif TWCE0000
using modGlobal = TWCE0000.modMDIParent;


#elif TWCK0000
using modGlobal = TWCK0000.modGNBas;


#elif TWMV0000
using modGlobal = TWMV0000.modGlobalRoutines;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmEditCentralParties.
	/// </summary>
	public partial class frmEditCentralParties : BaseForm
	{
		public frmEditCentralParties()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEditCentralParties InstancePtr
		{
			get
			{
				return (frmEditCentralParties)Sys.GetInstance(typeof(frmEditCentralParties));
			}
		}

		protected frmEditCentralParties _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		// Dim rsParties As New clsDRWrapper
		const int ReferenceIDCol = 0;
		const int ReferenceModuleCol = 1;
		const int ReferenceDescription = 4;
		const int ReferenceIdentifier = 2;
		const int ReferenceAltIdentifier = 3;
		int AddressIDCol;
		int AddressTypeCol;
		int AddressModuleCol;
		int AddressDateRangeCol;
		int AddressStartMonthCol;
		int AddressStartDayCol;
		int AddressEndMonthCol;
		int AddressEndDayCol;
		int AddressAddressCol;
		int AddressOverrideNameCol;
		int AddressModuleEntityCol;
		int AddressCommentCol;
		int AddressAddress1Col;
		int AddressAddress2Col;
		int AddressAddress3Col;
		int AddressCityCol;
		int AddressStateCol;
		int AddressZipCol;
		int AddressCountryCol;
		int AddressSeasonalCol;
		int ContactsIDCol;
		int ContactsNameCol;
		int ContactsDescriptionCol;
		int ContactsEmailCol;
		int ContactsPhoneCol;
		int ContactsCommentCol;
		int CommentsIDCol;
		int CommentsEnteredByCol;
		int CommentsLastModifiedCol;
		int CommentsModuleCol;
		int CommentsCommentCol;
		int PhoneIDCol;
		int PhoneDescriptionCol;
		int PhonePhoneCol;
		int PhoneExtCol;
		int SavedPhoneIDCol;
		int SavedPhoneContactIDCol;
		int SavedPhoneDescriptionCol;
		int SavedPhonePhoneCol;
		int SavedPhoneLineNumberCol;
		int SavedPhoneExtCol;
		bool blnNew;
		int intCurrentRow;
		bool blnDirty;

        public bool blnFromAccountScreen;
		public int lngEditID;
		public string strModule = "";
		// Dim clsCheck As New PartyUtil
		cPartyUtil clsCheck = new cPartyUtil();
		// Dim curPartyIndex As Long
		FCCollection collParties = new FCCollection();
		clsDRWrapper rsIDs = new clsDRWrapper();
		// Dave 12/14/2006---------------------------------------------------
		clsAuditControlInformation[] clsControlInfo = null;
		// Class to keep track of control information
		clsAuditChangesReporting clsReportChanges = new clsAuditChangesReporting();
		// Class to keep track of changes made to screen
		int intTotalNumberOfControls;
		// Counter keeps track of how many controls you are keeping track of
		// -------------------------------------------------------------------
		private void cboAddressType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboAddressType.Text == "Primary")
			{
				cboModule.SelectedIndex = -1;
				cboAccount.SelectedIndex = -1;
				chkSeasonal.CheckState = Wisej.Web.CheckState.Unchecked;
				fraOverrideCriteria.Enabled = false;
			}
			else
			{
				fraOverrideCriteria.Enabled = true;
			}
		}

		private void cboEndMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			SetupEndDateCombo(cboEndMonth.SelectedIndex + 1);
		}

		private void cboModule_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboModule.SelectedIndex > 0)
			{
				LoadPossibleAccounts();
				cboAccount.Enabled = true;
			}
			else
			{
				cboAccount.Clear();
				cboAccount.SelectedIndex = -1;
				cboAccount.Enabled = false;
			}
		}

		private void LoadPossibleAccounts()
		{
			FCCollection varAccounts = new FCCollection();
			int counter;
			int lngID;
			// lngID = collParties[curPartyIndex]
			lngID = FCConvert.ToInt32(rsIDs.Get_Fields_Int32("id"));
			cboAccount.Clear();
			cboAccount.AddItem("");
			// TODO: Add code that load acocunts specific to module selected
			// -----------------------------------------------------------------
			if (modCentralParties.GetModuleCode(FCConvert.ToInt16(cboModule.ItemData(cboModule.SelectedIndex))) == "RE" || modCentralParties.GetModuleCode(FCConvert.ToInt16(cboModule.ItemData(cboModule.SelectedIndex))) == "AR" || modCentralParties.GetModuleCode(FCConvert.ToInt16(cboModule.ItemData(cboModule.SelectedIndex))) == "UT")
			{
				varAccounts = clsCheck.AccountsReferencingParty(lngID, modCentralParties.GetModuleCode(FCConvert.ToInt16(cboModule.ItemData(cboModule.SelectedIndex))));
				for (counter = 0; counter <= varAccounts.Count - 1; counter++)
				{
					// UBound(varAccounts)
					cboAccount.AddItem(FCConvert.ToString(varAccounts[counter]));
				}
			}
			// -----------------------------------------------------------------
		}

		private void cboStartMonth_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			SetupStartDateCombo(cboStartMonth.SelectedIndex + 1);
		}

		private void cboType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// If cboType.Text <> "individual" Then
			if (cboType.SelectedIndex > 0)
			{
				lblCompanyName.Visible = true;
				txtCompanyName.Visible = true;
				lblFirstName.Visible = false;
				lblLast.Visible = false;
				lblMI.Visible = false;
				lblDesignation.Visible = false;
				txtFirst.Visible = false;
				txtLast.Visible = false;
				txtMI.Visible = false;
				txtDesignation.Visible = false;
			}
			else
			{
				lblCompanyName.Visible = false;
				txtCompanyName.Visible = false;
				lblFirstName.Visible = true;
				lblLast.Visible = true;
				lblMI.Visible = true;
				lblDesignation.Visible = true;
				txtFirst.Visible = true;
				txtLast.Visible = true;
				txtMI.Visible = true;
				txtDesignation.Visible = true;
			}
			blnDirty = true;
		}

		private void chkSeasonal_Click(object sender, System.EventArgs e)
		{
			if (chkSeasonal.CheckState == Wisej.Web.CheckState.Checked)
			{
				fraSeasonal.Enabled = true;
			}
			else
			{
				cboEndDay.SelectedIndex = -1;
				cboStartDay.SelectedIndex = -1;
				cboEndMonth.SelectedIndex = -1;
				cboStartMonth.SelectedIndex = -1;
				fraSeasonal.Enabled = false;
			}
		}

		private void cmdCancelAddress_Click(object sender, System.EventArgs e)
		{
			fraAddress.Visible = false;
			DisableEnableForm_2(true);
		}

		public void cmdCancelAddress_Click()
		{
			cmdCancelAddress_Click(cmdCancelAddress, new System.EventArgs());
		}

		private void cmdCancelComment_Click(object sender, System.EventArgs e)
		{
			fraComment.Visible = false;
			DisableEnableForm_2(true);
		}

		public void cmdCancelComment_Click()
		{
			cmdCancelComment_Click(cmdCancelComment, new System.EventArgs());
		}

		private void cmdCancelContact_Click(object sender, System.EventArgs e)
		{
			fraContact.Visible = false;
			DisableEnableForm_2(true);
		}

		public void cmdCancelContact_Click()
		{
			cmdCancelContact_Click(cmdCancelContact, new System.EventArgs());
		}

		private void cmdDeleteAddress_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDelete = new clsDRWrapper();
            bool changeToPrimary = false;

            int rowNumber = fgrdAddress.Row;

			if (rowNumber > 0)
			{
				if (FCConvert.ToDouble(fgrdAddress.TextMatrix(rowNumber, AddressIDCol)) != 0)
				{
					if (FCMessageBox.Show("Are you sure you wish to delete this address?", MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question, null) != DialogResult.Yes)
					{
						return;
					}
					else
					{
						rsDelete.Execute("DELETE FROM Addresses WHERE ID = " + fgrdAddress.TextMatrix(rowNumber, AddressIDCol), "CentralParties");
						modAuditReporting.RemoveGridRow_8("fgrdAddress", intTotalNumberOfControls - 1, rowNumber, clsControlInfo);
						// Dave 12/14/2006---------------------------------------------------
						// Add change record for deleting a row in the grid
						clsReportChanges.AddChange("Removed Address Row " + FCConvert.ToString(rowNumber));
						// ------------------------------------------------------------------

                        if (fgrdAddress.Rows > 2)
                        {
                            if (getPrimaryRowNumber() == rowNumber)
                            {
                                changeToPrimary = true;
							}
						}
                    }
				}
			}
			fgrdAddress.RemoveItem(fgrdAddress.Row);
            if (changeToPrimary)
			{
                fgrdAddress.TextMatrix(1, AddressTypeCol, "Primary");
                blnDirty = true;
			}
        }

		private void cmdDeleteComment_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDelete = new clsDRWrapper();
			if (fgrdComments.Row > 0)
			{
				if (FCConvert.ToDouble(fgrdComments.TextMatrix(fgrdComments.Row, CommentsIDCol)) != 0)
				{
					if (FCMessageBox.Show("Are you sure you wish to delete this comment?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, null) != DialogResult.Yes)
					{
						return;
					}
					else
					{
						rsDelete.Execute("DELETE FROM CentralComments WHERE ID = " + fgrdComments.TextMatrix(fgrdComments.Row, CommentsIDCol), "CentralParties");
						modAuditReporting.RemoveGridRow_8("fgrdComments", intTotalNumberOfControls - 1, fgrdComments.Row, clsControlInfo);
						// Dave 12/14/2006---------------------------------------------------
						// Add change record for adding a row to the grid
						clsReportChanges.AddChange("Removed Comments Row " + FCConvert.ToString(fgrdComments.Row));
						// ------------------------------------------------------------------
					}
				}
			}
			fgrdComments.RemoveItem(fgrdComments.Row);
		}

		private void cmdDeleteContact_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDelete = new clsDRWrapper();
			int counter;
			if (fgrdContacts.Row > 0)
			{
				if (FCConvert.ToDouble(fgrdContacts.TextMatrix(fgrdContacts.Row, ContactsIDCol)) != 0)
				{
					if (FCMessageBox.Show("Are you sure you wish to delete this contact?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, null) != DialogResult.Yes)
					{
						return;
					}
					else
					{
						rsDelete.Execute("DELETE FROM ContactPhoneNumbers WHERE ContactID = " + fgrdContacts.TextMatrix(fgrdContacts.Row, ContactsIDCol), "CentralParties");
						rsDelete.Execute("DELETE FROM Contacts WHERE ID = " + fgrdContacts.TextMatrix(fgrdContacts.Row, ContactsIDCol), "CentralParties");
						modAuditReporting.RemoveGridRow_8("fgrdContacts", intTotalNumberOfControls - 1, fgrdContacts.Row, clsControlInfo);
						// Dave 12/14/2006---------------------------------------------------
						// Add change record for adding a row to the grid
						clsReportChanges.AddChange("Removed Contacts Row " + FCConvert.ToString(fgrdContacts.Row));
						// ------------------------------------------------------------------
					}
				}
			}
			for (counter = 0; counter <= fgrdSavedContactPhones.Rows - 1; counter++)
			{
				if (fgrdSavedContactPhones.TextMatrix(counter, SavedPhoneContactIDCol) == fgrdContacts.TextMatrix(fgrdContacts.Row, ContactsIDCol))
				{
					fgrdSavedContactPhones.RemoveItem(counter);
				}
			}
			fgrdContacts.RemoveItem(fgrdContacts.Row);
		}

		private void cmdNewAddress_Click(object sender, System.EventArgs e)
		{
			intCurrentRow = -1;
			LoadAddressFrame();
			blnDirty = true;
		}

		private void cmdNewComment_Click(object sender, System.EventArgs e)
		{
			intCurrentRow = -1;
			LoadCommentFrame();
			blnDirty = true;
		}

		private void cmdNewContact_Click(object sender, System.EventArgs e)
		{
			intCurrentRow = -1;
			LoadContactFrame();
			blnDirty = true;
		}

		private void LoadAddressFrame()
		{
			int counter;
			ClearAddressFrame();
			if (intCurrentRow > 0)
			{
				txtAddress1.Text = fgrdAddress.TextMatrix(intCurrentRow, AddressAddress1Col);
				txtAddress2.Text = fgrdAddress.TextMatrix(intCurrentRow, AddressAddress2Col);
				txtAddress3.Text = fgrdAddress.TextMatrix(intCurrentRow, AddressAddress3Col);
				txtCity.Text = fgrdAddress.TextMatrix(intCurrentRow, AddressCityCol);
				txtZip.Text = fgrdAddress.TextMatrix(intCurrentRow, AddressZipCol);
				txtOverrideName.Text = fgrdAddress.TextMatrix(intCurrentRow, AddressOverrideNameCol);
				txtComments.TextRTF = fgrdAddress.TextMatrix(intCurrentRow, AddressCommentCol);
				cboState.Text = fgrdAddress.TextMatrix(intCurrentRow, AddressStateCol);
				txtCountry.Text = fgrdAddress.TextMatrix(intCurrentRow, AddressCountryCol);
				if (fgrdAddress.TextMatrix(intCurrentRow, AddressTypeCol) == "")
				{
					cboAddressType.SelectedIndex = 0;
				}
				else
				{
					cboAddressType.Text = fgrdAddress.TextMatrix(intCurrentRow, AddressTypeCol);
				}
				if (fgrdAddress.TextMatrix(intCurrentRow, AddressModuleCol) != "")
				{
					for (counter = 1; counter <= cboModule.Items.Count - 1; counter++)
					{
						if (Strings.LCase(modCentralParties.GetModuleCode(FCConvert.ToInt16(cboModule.ItemData(counter)))) == Strings.LCase(fgrdAddress.TextMatrix(intCurrentRow, AddressModuleCol)))
						{
							cboModule.SelectedIndex = counter;
						}
					}
					// counter
					// cboModule.Text = fgrdAddress.TextMatrix(intCurrentRow, AddressModuleCol)
					if (Conversion.Val(fgrdAddress.TextMatrix(intCurrentRow, AddressModuleEntityCol)) != 0)
					{
						cboAccount.Text = fgrdAddress.TextMatrix(intCurrentRow, AddressModuleEntityCol);
					}
				}
				if (FCConvert.CBool(fgrdAddress.TextMatrix(intCurrentRow, AddressSeasonalCol)))
				{
					chkSeasonal.CheckState = Wisej.Web.CheckState.Checked;
					cboStartDay.SelectedIndex = FCConvert.ToInt32(fgrdAddress.TextMatrix(intCurrentRow, AddressStartDayCol)) - 1;
					cboStartMonth.SelectedIndex = FCConvert.ToInt32(fgrdAddress.TextMatrix(intCurrentRow, AddressStartMonthCol)) - 1;
					cboEndDay.SelectedIndex = FCConvert.ToInt32(fgrdAddress.TextMatrix(intCurrentRow, AddressEndDayCol)) - 1;
					cboEndMonth.SelectedIndex = FCConvert.ToInt32(fgrdAddress.TextMatrix(intCurrentRow, AddressEndMonthCol)) - 1;
				}
				else
				{
					chkSeasonal.CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				cboState.Text = "ME";
			}
			fraAddress.CenterToContainer(this.ClientArea);
			DisableEnableForm_2(false);
			fraAddress.Visible = true;
		}

		private void LoadCommentFrame()
		{
			ClearCommentsFrame();
			int intIndex = 0;
			if (intCurrentRow > 0)
			{
				intIndex = modCentralParties.GetModuleIndex(fgrdComments.TextMatrix(intCurrentRow, CommentsModuleCol));
				//cboCommentModule.Text = fgrdComments.TextMatrix(intCurrentRow, CommentsModuleCol);
				lblCreatedBy.Text = fgrdComments.TextMatrix(intCurrentRow, CommentsEnteredByCol);
				lblCommentDateLastModified.Text = fgrdComments.TextMatrix(intCurrentRow, CommentsLastModifiedCol);
				txtCommentComment.TextRTF = fgrdComments.TextMatrix(intCurrentRow, CommentsCommentCol);
			}
			else
			{
				if (strModule != "")
				{
					intIndex = modCentralParties.GetModuleIndex(strModule);
					//cboCommentModule.Text = modCentralParties.GetModuleDescriptionByCode(ref strModule);
				}
				else
				{
					intIndex = modCentralParties.GetModuleIndex("GN");
					//cboCommentModule.Text = modCentralParties.GetModuleDescriptionByCode_2("GN");
				}
			}
			for (int x = 0; x < cboCommentModule.Items.Count - 1; x++)
			{
				if (cboCommentModule.ItemData(x) == intIndex)
				{
					cboCommentModule.SelectedIndex = x;
				}
			}
			fraComment.Left = FCConvert.ToInt32((this.Width - fraComment.Width) / 2.0);
			fraComment.Top = FCConvert.ToInt32((this.Height - fraComment.Height) / 2.0);
			DisableEnableForm_2(false);
			fraComment.Visible = true;
		}

		private void LoadContactFrame()
		{
			ClearContactFrame();
			if (intCurrentRow > 0)
			{
				txtContactComments.TextRTF = fgrdContacts.TextMatrix(intCurrentRow, ContactsCommentCol);
				txtContactDescription.Text = fgrdContacts.TextMatrix(intCurrentRow, ContactsDescriptionCol);
				txtContactEmail.Text = fgrdContacts.TextMatrix(intCurrentRow, ContactsEmailCol);
				txtContactName.Text = fgrdContacts.TextMatrix(intCurrentRow, ContactsNameCol);
			}
			LoadContactsPhoneGrid();
			fraContact.Left = FCConvert.ToInt32((this.Width - fraContact.Width) / 2.0);
			fraContact.Top = FCConvert.ToInt32((this.Height - fraContact.Height) / 2.0);
			DisableEnableForm_2(false);
			fraContact.Visible = true;
		}

		private void DisableEnableForm_2(bool blnEnabled)
		{
			DisableEnableForm(ref blnEnabled);
		}

		private void DisableEnableForm(ref bool blnEnabled)
		{
			mnuProcess.Enabled = blnEnabled;
			fraPhone.Enabled = blnEnabled;
			txtCompanyName.Enabled = blnEnabled;
			txtDesignation.Enabled = blnEnabled;
			txtLast.Enabled = blnEnabled;
			txtMI.Enabled = blnEnabled;
			txtFirst.Enabled = blnEnabled;
			txtEmail.Enabled = blnEnabled;
			txtWebAddress.Enabled = blnEnabled;
			lblCreatedDate.Enabled = blnEnabled;
			lblParyNumber.Enabled = blnEnabled;
			tabDetails.Enabled = blnEnabled;
			cboType.Enabled = blnEnabled;
			if (blnEnabled == true)
			{
				CheckPermissions();
			}
		}

		private void CheckPermissions()
		{
			if (modGlobalConstants.Statics.clsSecurityClass.CheckOtherPermissions(modCentralParties.CNSTCentralPartiesEdit, "CP") != "F")
			{
				DisableEnableForm_2(false);
			}
		}

        private void cmdOKAddress_Click(object sender, System.EventArgs e)
        {
            if (ValidateAddress())
            {
                if (intCurrentRow == -1)
                {
                    AddAddress();
                }
                else
                {
                    UpdateAddress();
                }

                cmdCancelAddress_Click();
            }
        }

		private void AddAddress()
        {
            if (cboAddressType.Text == "Primary")
			{
                SetAddressesToOverride();
			}
            else
			{
                if (!HasPrimary())
                {
                    cboAddressType.Text = "Primary";
                }
			}

            fgrdAddress.Rows += 1;
            intCurrentRow = fgrdAddress.Rows - 1;
            fgrdAddress.TextMatrix(intCurrentRow, AddressIDCol, "0");
            // Dave 12/14/2006---------------------------------------------------
            // Add change record for adding a row to the grid
            clsReportChanges.AddChange("Added Address");
			// ------------------------------------------------------------------

            UpdateAddressGrid();
        }

        private void SetAddressesToOverride()
		{
            for (int i = 1; i <= fgrdAddress.Rows - 1; i++)
            {
                fgrdAddress.TextMatrix(i, AddressTypeCol, "Override");
            }
        }

        private void UpdateAddress()
        {
            var isBecomingPrimary = cboAddressType.Text == "Primary";
            var isBecomingOverride = !isBecomingPrimary;

            if (isBecomingOverride && HasNoOtherPrimaryAddress())
            {
                cboAddressType.Text = "Primary";
            }

            if (isBecomingPrimary)
            {
                SetAddressesToOverride();
            }

            UpdateAddressGrid();
        }


        private void UpdateAddressGrid()
		{

		    fgrdAddress.TextMatrix(intCurrentRow, AddressAddress1Col, Strings.Trim(txtAddress1.Text));
			fgrdAddress.TextMatrix(intCurrentRow, AddressAddress2Col, Strings.Trim(txtAddress2.Text));
			fgrdAddress.TextMatrix(intCurrentRow, AddressAddress3Col, Strings.Trim(txtAddress3.Text));
			fgrdAddress.TextMatrix(intCurrentRow, AddressAddressCol, CreateAddressString_2186(Strings.Trim(txtAddress1.Text), Strings.Trim(txtAddress2.Text), Strings.Trim(txtAddress3.Text), Strings.Trim(txtCity.Text), cboState.Text, Strings.Trim(txtZip.Text), Strings.Trim(txtCountry.Text)));
			fgrdAddress.TextMatrix(intCurrentRow, AddressCityCol, Strings.Trim(txtCity.Text));
			fgrdAddress.TextMatrix(intCurrentRow, AddressStateCol, cboState.Text);
			fgrdAddress.TextMatrix(intCurrentRow, AddressZipCol, Strings.Trim(txtZip.Text));
			fgrdAddress.TextMatrix(intCurrentRow, AddressCountryCol, Strings.Trim(txtCountry.Text));
			fgrdAddress.TextMatrix(intCurrentRow, AddressTypeCol, cboAddressType.Text);
			fgrdAddress.TextMatrix(intCurrentRow, AddressOverrideNameCol, Strings.Trim(txtOverrideName.Text));
			if (cboModule.SelectedIndex >= 0)
			{
				fgrdAddress.TextMatrix(intCurrentRow, AddressModuleCol, modCentralParties.GetModuleCode(FCConvert.ToInt16(cboModule.ItemData(cboModule.SelectedIndex))));
			}
			else
			{
				fgrdAddress.TextMatrix(intCurrentRow, AddressModuleCol, "");
			}
			if (cboAccount.SelectedIndex >= 0)
			{
				fgrdAddress.TextMatrix(intCurrentRow, AddressModuleEntityCol, FCConvert.ToString(cboAccount.ItemData(cboAccount.SelectedIndex)));
			}
			else
			{
				fgrdAddress.TextMatrix(intCurrentRow, AddressModuleEntityCol, "");
			}
			fgrdAddress.TextMatrix(intCurrentRow, AddressSeasonalCol, FCConvert.ToString(chkSeasonal.CheckState == Wisej.Web.CheckState.Checked));
			if (chkSeasonal.CheckState == Wisej.Web.CheckState.Checked)
			{
				fgrdAddress.TextMatrix(intCurrentRow, AddressStartDayCol, FCConvert.ToString(cboStartDay.SelectedIndex + 1));
				fgrdAddress.TextMatrix(intCurrentRow, AddressStartMonthCol, FCConvert.ToString(cboStartMonth.SelectedIndex + 1));
				fgrdAddress.TextMatrix(intCurrentRow, AddressEndDayCol, FCConvert.ToString(cboEndDay.SelectedIndex + 1));
				fgrdAddress.TextMatrix(intCurrentRow, AddressEndMonthCol, FCConvert.ToString(cboEndMonth.SelectedIndex + 1));
				fgrdAddress.TextMatrix(intCurrentRow, AddressDateRangeCol, CreateDateRangeString(FCConvert.ToInt16(cboStartMonth.SelectedIndex + 1), FCConvert.ToInt16(cboStartDay.SelectedIndex + 1), FCConvert.ToInt16(cboEndMonth.SelectedIndex + 1), FCConvert.ToInt16(cboEndDay.SelectedIndex + 1)));
			}
			else
			{
				fgrdAddress.TextMatrix(intCurrentRow, AddressStartDayCol, FCConvert.ToString(0));
				fgrdAddress.TextMatrix(intCurrentRow, AddressStartMonthCol, FCConvert.ToString(0));
				fgrdAddress.TextMatrix(intCurrentRow, AddressEndDayCol, FCConvert.ToString(0));
				fgrdAddress.TextMatrix(intCurrentRow, AddressEndMonthCol, FCConvert.ToString(0));
				fgrdAddress.TextMatrix(intCurrentRow, AddressDateRangeCol, "");
			}
			fgrdAddress.TextMatrix(intCurrentRow, AddressCommentCol, Strings.Trim(txtComments.Text));
			fgrdAddress.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
			//FC:FINAL:MSH - issue #1247: AutoSize doesn't work in WiseJ, so temp solution - increase height of row depend on number of line breaks(because in some cases part of data will miss)
			//fgrdAddress.AutoSize(AddressAddressCol);
			for (int i = 0; i < fgrdAddress.RowCount; i++)
			{
				int rowIndex = fgrdAddress.GetFlexRowIndex(i);
				int lineMultiplier = fgrdAddress.TextMatrix(rowIndex, AddressAddressCol).Split('\n').Length;
				if (lineMultiplier > 0)
				{
					fgrdAddress.RowHeight(rowIndex, lineMultiplier * 300);
				}
			}
            
		}

		private void cmdOKComment_Click(object sender, System.EventArgs e)
		{
			if (ValidateComment())
			{
				if (intCurrentRow == -1)
				{
					fgrdComments.Rows += 1;
					intCurrentRow = fgrdComments.Rows - 1;
					fgrdComments.TextMatrix(intCurrentRow, CommentsIDCol, "0");
					fgrdComments.TextMatrix(intCurrentRow, CommentsEnteredByCol, FCConvert.ToString(modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID()));
					// Dave 12/14/2006---------------------------------------------------
					// Add change record for adding a row to the grid
					clsReportChanges.AddChange("Added Comment");
					// ------------------------------------------------------------------
				}
				if (fgrdComments.TextMatrix(intCurrentRow, CommentsCommentCol) != Strings.Trim(txtCommentComment.Text) || fgrdComments.TextMatrix(intCurrentRow, CommentsModuleCol) != Strings.Trim(cboCommentModule.Text))
				{
					fgrdComments.TextMatrix(intCurrentRow, CommentsLastModifiedCol, DateTime.Today.ToShortDateString());
				}
				fgrdComments.TextMatrix(intCurrentRow, CommentsCommentCol, Strings.Trim(txtCommentComment.Text));
				// kk01052015 trouts-119  Change to store the 2 char module id, like override address
				// fgrdComments.TextMatrix(intCurrentRow, CommentsModuleCol) = Trim(cboCommentModule.Text)
				if (cboCommentModule.SelectedIndex >= 0)
				{
					fgrdComments.TextMatrix(intCurrentRow, CommentsModuleCol, modCentralParties.GetModuleCode(FCConvert.ToInt16(cboCommentModule.ItemData(cboCommentModule.SelectedIndex))));
				}
				else
				{
					fgrdComments.TextMatrix(intCurrentRow, CommentsModuleCol, "");
				}
				cmdCancelComment_Click();
			}
		}

		private void cmdOKContact_Click(object sender, System.EventArgs e)
		{
			int counter;
			int counter2;
			// VBto upgrade warning: intNextID As short --> As int	OnWrite(int, string)
			int intNextID = 0;
			if (ValidateContact())
			{
				if (intCurrentRow == -1)
				{
					intNextID = 0;
					for (counter = 1; counter <= fgrdContacts.Rows - 1; counter++)
					{
						if (FCConvert.ToDouble(fgrdContacts.TextMatrix(counter, ContactsIDCol)) < intNextID)
						{
							intNextID = FCConvert.ToInt32(fgrdContacts.TextMatrix(counter, ContactsIDCol));
						}
					}
					intNextID -= 1;
					fgrdContacts.Rows += 1;
					intCurrentRow = fgrdContacts.Rows - 1;
					fgrdContacts.TextMatrix(intCurrentRow, ContactsIDCol, FCConvert.ToString(intNextID));
					// Dave 12/14/2006---------------------------------------------------
					// Add change record for adding a row to the grid
					clsReportChanges.AddChange("Added Contact " + Strings.Trim(txtContactName.Text));
					// ------------------------------------------------------------------
				}
				fgrdContacts.TextMatrix(intCurrentRow, ContactsCommentCol, Strings.Trim(txtContactComments.Text));
				fgrdContacts.TextMatrix(intCurrentRow, ContactsDescriptionCol, Strings.Trim(txtContactDescription.Text));
				fgrdContacts.TextMatrix(intCurrentRow, ContactsEmailCol, Strings.Trim(txtContactEmail.Text));
				fgrdContacts.TextMatrix(intCurrentRow, ContactsNameCol, Strings.Trim(txtContactName.Text));
				if (fgrdContactPhone.Rows > 1)
				{
					fgrdContacts.TextMatrix(intCurrentRow, ContactsPhoneCol, fgrdContactPhone.TextMatrix(1, PhonePhoneCol));
				}
				else
				{
					fgrdContacts.TextMatrix(intCurrentRow, ContactsPhoneCol, "");
				}
				for (counter = 1; counter <= fgrdContactPhone.Rows - 1; counter++)
				{
					if (Strings.Trim(fgrdContactPhone.TextMatrix(counter, PhoneDescriptionCol)) != "" && Strings.Trim(fgrdContactPhone.TextMatrix(counter, PhonePhoneCol)) != "")
					{
						if (FCConvert.ToDouble(fgrdContactPhone.TextMatrix(counter, PhoneIDCol)) != 0)
						{
							for (counter2 = 0; counter2 <= fgrdSavedContactPhones.Rows - 1; counter2++)
							{
								if (fgrdSavedContactPhones.TextMatrix(counter2, SavedPhoneIDCol) == fgrdContactPhone.TextMatrix(counter, PhoneIDCol))
								{
									break;
								}
							}
						}
						else
						{
							fgrdSavedContactPhones.Rows += 1;
							counter2 = fgrdSavedContactPhones.Rows - 1;
						}
						fgrdSavedContactPhones.TextMatrix(counter2, SavedPhoneDescriptionCol, fgrdContactPhone.TextMatrix(counter, PhoneDescriptionCol));
						fgrdSavedContactPhones.TextMatrix(counter2, SavedPhoneLineNumberCol, FCConvert.ToString(counter));
						fgrdSavedContactPhones.TextMatrix(counter2, SavedPhonePhoneCol, fgrdContactPhone.TextMatrix(counter, PhonePhoneCol));
						fgrdSavedContactPhones.TextMatrix(counter2, SavedPhoneExtCol, fgrdContactPhone.TextMatrix(counter, PhoneExtCol));
						fgrdSavedContactPhones.TextMatrix(counter2, SavedPhoneContactIDCol, fgrdContacts.TextMatrix(intCurrentRow, ContactsIDCol));
					}
				}
				cmdCancelContact_Click();
			}
		}

		private bool ValidateAddress()
		{
			bool ValidateAddress = false;
			int counter;
			if (Strings.Trim(txtAddress1.Text) == "")
			{
				FCMessageBox.Show("You must enter a street address before you may continue.", MsgBoxStyle.Information, "Invalid Data");
				txtAddress1.Focus();
				ValidateAddress = false;
				return ValidateAddress;
			}
			if (Strings.Trim(txtCity.Text) == "")
			{
				FCMessageBox.Show("You must enter a city before you may continue.", MsgBoxStyle.Information, "Invalid Data");
				txtCity.Focus();
				ValidateAddress = false;
				return ValidateAddress;
			}
			if (Strings.Trim(txtZip.Text) == "")
			{
				FCMessageBox.Show("You must enter a zip code before you may continue.", MsgBoxStyle.Information, "Invalid Data");
				txtZip.Focus();
				ValidateAddress = false;
				return ValidateAddress;
			}

			if (chkSeasonal.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (cboStartDay.SelectedIndex < 0 || cboStartMonth.SelectedIndex < 0 || cboEndDay.SelectedIndex < 0 || cboEndMonth.SelectedIndex < 0)
				{
					FCMessageBox.Show("You must enter a valid date range before you may continue.", MsgBoxStyle.Information, "Invalid Data");
					if (cboStartMonth.SelectedIndex < 0)
					{
						cboStartMonth.Focus();
						ValidateAddress = false;
						return ValidateAddress;
					}
					else if (cboStartDay.SelectedIndex < 0)
					{
						cboStartDay.Focus();
						ValidateAddress = false;
						return ValidateAddress;
					}
					else if (cboEndMonth.SelectedIndex < 0)
					{
						cboEndMonth.Focus();
						ValidateAddress = false;
						return ValidateAddress;
					}
					else
					{
						cboEndDay.Focus();
						ValidateAddress = false;
						return ValidateAddress;
					}
				}
			}

            if (!ValidateAddressType())
            {
                return false;
            }

			if (chkSeasonal.CheckState == Wisej.Web.CheckState.Checked)
			{
				for (counter = 1; counter <= fgrdAddress.Rows - 1; counter++)
				{
					if (counter != intCurrentRow)
					{
						//FC:FINAL:BCU #i171 - use FCConvert.CBool instead
						//if (FCConvert.ToBoolean(fgrdAddress.TextMatrix(counter, AddressSeasonalCol)) == true)
						if (FCConvert.CBool(fgrdAddress.TextMatrix(counter, AddressSeasonalCol)) == true)
						{
							// Start Month of an existing date range is inside the new range by month
							if (FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressModuleCol)) == cboModule.ItemData(cboModule.SelectedIndex))
							{
								if (FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressStartMonthCol)) > cboStartMonth.SelectedIndex + 1 && FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressStartMonthCol)) < cboEndMonth.SelectedIndex + 1)
								{
									FCMessageBox.Show("The date range you have entered overlaps an existing date range.  This must be corrected before you may proceed.", MsgBoxStyle.Information, "Invalid Info");
									cboStartMonth.Focus();
									ValidateAddress = false;
									return ValidateAddress;
									// End Month of an existing date range is inside the new range by month
								}
								else if (FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressEndMonthCol)) > cboStartMonth.SelectedIndex + 1 && FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressEndMonthCol)) < cboEndMonth.SelectedIndex + 1)
								{
									FCMessageBox.Show("The date range you have entered overlaps an existing date range.  This must be corrected before you may proceed.", MsgBoxStyle.Information, "Invalid Info");
									cboStartMonth.Focus();
									ValidateAddress = false;
									return ValidateAddress;
									// Start Month is the same but start day is later and End Month is later
								}
								else if (FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressStartMonthCol)) == cboStartMonth.SelectedIndex + 1 && FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressStartDayCol)) > cboStartDay.SelectedIndex + 1 && FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressStartMonthCol)) < cboEndMonth.SelectedIndex + 1)
								{
									FCMessageBox.Show("The date range you have entered overlaps an existing date range.  This must be corrected before you may proceed.", MsgBoxStyle.Information, "Invalid Info");
									cboStartMonth.Focus();
									ValidateAddress = false;
									return ValidateAddress;
									// Start Month is the same start day is later and end month is the same but strt day is earlier than end day
								}
								else if (FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressStartMonthCol)) == cboStartMonth.SelectedIndex + 1 && FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressStartDayCol)) > cboStartDay.SelectedIndex + 1 && FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressStartMonthCol)) == cboEndMonth.SelectedIndex + 1 && FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressStartDayCol)) < cboEndDay.SelectedIndex + 1)
								{
									FCMessageBox.Show("The date range you have entered overlaps an existing date range.  This must be corrected before you may proceed.", MsgBoxStyle.Information, "Invalid Info");
									cboStartMonth.Focus();
									ValidateAddress = false;
									return ValidateAddress;
									// End Month is the same end day is earlier and start month is earlier
								}
								else if (FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressEndMonthCol)) == cboEndMonth.SelectedIndex + 1 && FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressEndDayCol)) < cboEndDay.SelectedIndex + 1 && FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressEndMonthCol)) > cboStartMonth.SelectedIndex + 1)
								{
									FCMessageBox.Show("The date range you have entered overlaps an existing date range.  This must be corrected before you may proceed.", MsgBoxStyle.Information, "Invalid Info");
									cboStartMonth.Focus();
									ValidateAddress = false;
									return ValidateAddress;
									// End Month is the same end day is earlier and start month is the same but end day is later than start day
								}
								else if (FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressEndMonthCol)) == cboEndMonth.SelectedIndex + 1 && FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressEndDayCol)) < cboEndDay.SelectedIndex + 1 && FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressEndMonthCol)) == cboStartMonth.SelectedIndex + 1 && FCConvert.ToDouble(fgrdAddress.TextMatrix(counter, AddressEndDayCol)) > cboStartDay.SelectedIndex + 1)
								{
									FCMessageBox.Show("The date range you have entered overlaps an existing date range.  This must be corrected before you may proceed.", MsgBoxStyle.Information, "Invalid Info");
									cboStartMonth.Focus();
									ValidateAddress = false;
									return ValidateAddress;
								}
							}
						}
					}
				}
			}
			ValidateAddress = true;
			return ValidateAddress;
		}

        private int OtherPrimaryRowNumber()
        {
            int result = -1;

            for (int i = 1; i <= fgrdAddress.Rows - 1; i++)
            {
				if (i != intCurrentRow)
				{
                    if (fgrdAddress.TextMatrix(i, AddressTypeCol) == "Primary")
                    {
                        result = i;
                        break;
                    }
				}
			}

            return result;
        }


        private bool HasNoOtherPrimaryAddress()
        {
            return OtherPrimaryRowNumber() == -1;
        }


		private bool ValidateComment()
		{
			bool ValidateComment = false;
			if (Strings.Trim(txtCommentComment.Text) == "")
			{
				FCMessageBox.Show("You must enter a comment before you may continue.", MsgBoxStyle.Information, "Invalid Date");
				txtCommentComment.Focus();
				ValidateComment = false;
				return ValidateComment;
			}
			ValidateComment = true;
			return ValidateComment;
		}

		private bool ValidateContact()
		{
			bool ValidateContact = false;
			if (Strings.Trim(txtContactName.Text) == "")
			{
				FCMessageBox.Show("You must enter a contact name before you may continue.", MsgBoxStyle.Information, "Invalid Date");
				txtContactName.Focus();
				ValidateContact = false;
				return ValidateContact;
			}
			ValidateContact = true;
			return ValidateContact;
		}

		private void fgrdAddress_DblClick(object sender, EventArgs e)
		{
			if (fgrdAddress.MouseRow > 0)
			{
				intCurrentRow = fgrdAddress.MouseRow;
				LoadAddressFrame();
			}
		}

		private void fgrdAddress_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (fgrdAddress.CurrentCell.IsInEditMode)
			{
				blnDirty = true;
			}
		}

		private void fgrdComments_DblClick(object sender, EventArgs e)
		{
			if (fgrdComments.MouseRow > 0)
			{
				intCurrentRow = fgrdComments.MouseRow;
				LoadCommentFrame();
			}
		}

		private void fgrdComments_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (fgrdComments.CurrentCell.IsInEditMode)
			{
				blnDirty = true;
			}
		}

		private void fgrdContacts_DblClick(object sender, EventArgs e)
		{
			if (fgrdContacts.MouseRow > 0)
			{
				intCurrentRow = fgrdContacts.MouseRow;
				LoadContactFrame();
			}
		}

		private void fgrdContacts_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (fgrdContacts.CurrentCell.IsInEditMode)
			{
				blnDirty = true;
			}
		}

		private void fgrdPhone_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (fgrdPhone.CurrentCell.IsInEditMode)
			{
				blnDirty = true;
			}
		}

		private void frmEditCentralParties_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			else
			{
				blnDirty = false;
			}
			// Me.Refresh
		}

		private void frmEditCentralParties_Load(object sender, System.EventArgs e)
		{
			FormatAddressGrid();
			FormatContactsGrid();
			FormatCommentsGrid();
			FormatPhoneGrids();
			FormatReferenceGrid();
			LoadStateCombos();
			LoadTypeCombo();
			LoadAddressTypeCombo();
			LoadDateCombos();
			SavedPhoneIDCol = 0;
			SavedPhoneContactIDCol = 1;
			SavedPhoneDescriptionCol = 2;
			SavedPhonePhoneCol = 3;
			SavedPhoneLineNumberCol = 4;
			SavedPhoneExtCol = 5;
			clsDRWrapper rsParties = new clsDRWrapper();
			CheckPermissions();
			if (!blnFromAccountScreen)
			{
				rsIDs.OpenRecordset("select ID from parties order by ID", "CentralParties");
				if (rsIDs.FindFirst("id = " + FCConvert.ToString(Statics.lngReturnedID)))
				{
				}
			}
			else
			{
				rsIDs.OpenRecordset("select * from parties where id = " + FCConvert.ToString(Statics.lngReturnedID), "CentralParties");
			}
			// VBto upgrade warning: tParty As cParty	OnWrite(cParty)
			cParty tParty = new cParty();
			cPartyController tPCont = new cPartyController();
			if (Statics.lngReturnedID > 0)
			{
				tParty = tPCont.GetParty(Statics.lngReturnedID);
				if (!(tParty == null))
				{
					blnNew = false;
					SetMovementMenuItems();
					LoadParty(tParty);
					blnDirty = false;
				}
				else
				{
                    if (tPCont.LastError == 0)
                    {
                        mnuFileNew_Click();
                    }
                    else
                    {
                        throw new Exception(tPCont.LastErrorMessage);
                    }
                }
			}
			else
			{
				mnuFileNew_Click();
			}

			SetMovementMenuItems();
			FillReferencesGrid(Statics.lngReturnedID);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this, false);
			App.DoEvents();
			blnDirty = false;
        }

		private void SetMovementMenuItems()
		{
			if (blnFromAccountScreen)
			{
				mnuPrevious.Enabled = false;
				mnuFileNext.Enabled = false;
			}
			else
			{
				if (blnNew)
				{
					if (rsIDs.RecordCount() > 1)
					{
						// If collParties.Count > 1 Then
						mnuPrevious.Enabled = true;
					}
					else
					{
						mnuPrevious.Enabled = false;
					}
					mnuFileNext.Enabled = false;
				}
				else
				{

					if (rsIDs.BeginningOfFile() || rsIDs.isFirstRecord())
					{
						mnuPrevious.Enabled = false;
					}
					else
					{
						mnuPrevious.Enabled = true;
					}
					if (rsIDs.EndOfFile() || rsIDs.IsLastRecord())
					{
						mnuFileNext.Enabled = false;
					}
					else
					{
						mnuFileNext.Enabled = true;
					}

				}
			}
		}

		private void FormatAddressGrid()
		{
			AddressIDCol = 1;
			AddressCommentCol = 2;
			AddressStartMonthCol = 3;
			AddressStartDayCol = 4;
			AddressEndMonthCol = 5;
			AddressEndDayCol = 6;
			AddressAddress1Col = 7;
			AddressAddress2Col = 8;
			AddressAddress3Col = 9;
			AddressCityCol = 10;
			AddressStateCol = 11;
			AddressZipCol = 12;
			AddressCountryCol = 13;
			AddressSeasonalCol = 14;
			AddressTypeCol = 15;
			AddressModuleCol = 16;
			AddressModuleEntityCol = 17;
			AddressDateRangeCol = 18;
			AddressOverrideNameCol = 19;
			AddressAddressCol = 20;
			fgrdAddress.Rows = 1;
			fgrdAddress.ColHidden(AddressIDCol, true);
			fgrdAddress.ColHidden(AddressCommentCol, true);
			fgrdAddress.ColHidden(AddressStartMonthCol, true);
			fgrdAddress.ColHidden(AddressStartDayCol, true);
			fgrdAddress.ColHidden(AddressEndMonthCol, true);
			fgrdAddress.ColHidden(AddressEndDayCol, true);
			fgrdAddress.ColHidden(AddressAddress1Col, true);
			fgrdAddress.ColHidden(AddressAddress2Col, true);
			fgrdAddress.ColHidden(AddressAddress3Col, true);
			fgrdAddress.ColHidden(AddressCityCol, true);
			fgrdAddress.ColHidden(AddressStateCol, true);
			fgrdAddress.ColHidden(AddressZipCol, true);
			fgrdAddress.ColHidden(AddressCountryCol, true);
			fgrdAddress.ColHidden(AddressSeasonalCol, true);
			fgrdAddress.TextMatrix(0, AddressTypeCol, "Type");
			fgrdAddress.TextMatrix(0, AddressModuleCol, "Module");
			fgrdAddress.TextMatrix(0, AddressDateRangeCol, "Date Range");
			fgrdAddress.TextMatrix(0, AddressAddressCol, "Address");
			fgrdAddress.TextMatrix(0, AddressOverrideNameCol, "Override Name");
			fgrdAddress.TextMatrix(0, AddressModuleEntityCol, "Module Entity");
			fgrdAddress.ColWidth(0, FCConvert.ToInt32(fgrdAddress.WidthOriginal * 0.03));
			fgrdAddress.ColWidth(AddressTypeCol, FCConvert.ToInt32(fgrdAddress.WidthOriginal * 0.1));
			fgrdAddress.ColWidth(AddressModuleCol, FCConvert.ToInt32(fgrdAddress.WidthOriginal * 0.06));
			fgrdAddress.ColWidth(AddressDateRangeCol, FCConvert.ToInt32(fgrdAddress.WidthOriginal * 0.15));
			fgrdAddress.ColWidth(AddressAddressCol, FCConvert.ToInt32(fgrdAddress.WidthOriginal * 0.25));
			fgrdAddress.ColWidth(AddressOverrideNameCol, FCConvert.ToInt32(fgrdAddress.WidthOriginal * 0.2));
			fgrdAddress.ColWidth(AddressModuleEntityCol, FCConvert.ToInt32(fgrdAddress.WidthOriginal * 0.15));
		}

		private void FormatContactsGrid()
		{
			ContactsIDCol = 1;
			ContactsNameCol = 3;
			ContactsDescriptionCol = 4;
			ContactsEmailCol = 5;
			ContactsPhoneCol = 6;
			ContactsCommentCol = 2;
			fgrdContacts.Rows = 1;
			fgrdContacts.ColHidden(ContactsIDCol, true);
			fgrdContacts.ColHidden(ContactsCommentCol, true);
			fgrdContacts.TextMatrix(0, ContactsNameCol, "Name");
			fgrdContacts.TextMatrix(0, ContactsDescriptionCol, "Description");
			fgrdContacts.TextMatrix(0, ContactsEmailCol, "E-Mail");
			fgrdContacts.TextMatrix(0, ContactsPhoneCol, "Phone");
			fgrdContacts.ColWidth(0, FCConvert.ToInt32(fgrdContacts.WidthOriginal * 0.03));
			fgrdContacts.ColWidth(ContactsNameCol, FCConvert.ToInt32(fgrdContacts.WidthOriginal * 0.3));
			fgrdContacts.ColWidth(ContactsDescriptionCol, FCConvert.ToInt32(fgrdContacts.WidthOriginal * 0.3));
			fgrdContacts.ColWidth(ContactsEmailCol, FCConvert.ToInt32(fgrdContacts.WidthOriginal * 0.2));
			fgrdContacts.ColWidth(ContactsPhoneCol, FCConvert.ToInt32(fgrdContacts.WidthOriginal * 0.25));
		}

		private void FormatCommentsGrid()
		{
			CommentsIDCol = 1;
			CommentsEnteredByCol = 2;
			CommentsLastModifiedCol = 3;
			CommentsModuleCol = 4;
			CommentsCommentCol = 5;
			fgrdComments.Rows = 1;
			fgrdComments.ColHidden(CommentsIDCol, true);
			fgrdComments.TextMatrix(0, CommentsEnteredByCol, "Entered By");
			fgrdComments.TextMatrix(0, CommentsLastModifiedCol, "Last Modified");
			fgrdComments.TextMatrix(0, CommentsModuleCol, "Module");
			fgrdComments.TextMatrix(0, CommentsCommentCol, "Comment");
			fgrdComments.ColWidth(0, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.03));
			fgrdComments.ColWidth(CommentsEnteredByCol, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.1));
            //FC:FINAL:MSH - increase width of column to avoid cutting off header text
			//fgrdComments.ColWidth(CommentsLastModifiedCol, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.1));
			fgrdComments.ColWidth(CommentsLastModifiedCol, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.15));
			fgrdComments.ColWidth(CommentsModuleCol, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.07));
			fgrdComments.ColWidth(CommentsCommentCol, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.25));
		}

		private void FormatPhoneGrids()
		{
			PhoneIDCol = 1;
			PhoneDescriptionCol = 2;
			PhonePhoneCol = 3;
			PhoneExtCol = 4;
			fgrdPhone.Rows = 1;
			fgrdPhone.ColHidden(PhoneIDCol, true);
			fgrdPhone.TextMatrix(0, PhoneDescriptionCol, "Description");
			fgrdPhone.TextMatrix(0, PhonePhoneCol, "Phone #");
			fgrdPhone.TextMatrix(0, PhoneExtCol, "Ext");
			fgrdPhone.ColEditMask(PhonePhoneCol, "(###) ###-####");
			fgrdPhone.ColWidth(0, FCConvert.ToInt32(fgrdPhone.WidthOriginal * 0.05));
			fgrdPhone.ColWidth(PhoneDescriptionCol, FCConvert.ToInt32(fgrdPhone.WidthOriginal * 0.4));
			fgrdPhone.ColWidth(PhonePhoneCol, FCConvert.ToInt32(fgrdPhone.WidthOriginal * 0.35));
			fgrdPhone.ColWidth(PhoneExtCol, FCConvert.ToInt32(fgrdPhone.WidthOriginal * 0.1));
			fgrdContactPhone.Rows = 1;
			fgrdContactPhone.ColHidden(PhoneIDCol, true);
			fgrdContactPhone.TextMatrix(0, PhoneDescriptionCol, "Description");
			fgrdContactPhone.TextMatrix(0, PhonePhoneCol, "Phone #");
			fgrdContactPhone.TextMatrix(0, PhoneExtCol, "Ext");
			fgrdContactPhone.ColEditMask(PhonePhoneCol, "(###) 000-0000");
			fgrdContactPhone.ColWidth(0, FCConvert.ToInt32(fgrdContactPhone.WidthOriginal * 0.05));
			fgrdContactPhone.ColWidth(PhoneDescriptionCol, FCConvert.ToInt32(fgrdContactPhone.WidthOriginal * 0.4));
			fgrdContactPhone.ColWidth(PhonePhoneCol, FCConvert.ToInt32(fgrdContactPhone.WidthOriginal * 0.35));
			fgrdContactPhone.ColWidth(PhoneExtCol, FCConvert.ToInt32(fgrdContactPhone.WidthOriginal * 0.1));
			fgrdPhone.EditingControlShowing -= FgrdPhone_EditingControlShowing;
			fgrdPhone.EditingControlShowing += FgrdPhone_EditingControlShowing;
			fgrdContactPhone.EditingControlShowing -= FgrdContactPhone_EditingControlShowing;
			fgrdContactPhone.EditingControlShowing += FgrdContactPhone_EditingControlShowing;
		}

		private void FgrdContactPhone_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				//FC:FINAL:MSH - issue #1246: assign handler for adding new row if 'Enter' key pressed
				e.Control.KeyDown -= fgrdContactPhone_KeyDownEdit;
				e.Control.KeyDown += fgrdContactPhone_KeyDownEdit;
			}
		}

		private void FgrdPhone_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				//FC:FINAL:MSH - issue #1246: assign handler for adding new row if 'Enter' key pressed
				e.Control.KeyDown -= fgrdPhone_KeyDownEdit;
				e.Control.KeyDown += fgrdPhone_KeyDownEdit;
			}
		}

		private void LoadStateCombos()
		{
			clsDRWrapper rsCodes = new clsDRWrapper();
			cboState.Clear();
			rsCodes.OpenRecordset("SELECT * FROM tblStates ORDER BY StateCode", "CentralData");
			if (rsCodes.BeginningOfFile() != true && rsCodes.EndOfFile() != true)
			{
				do
				{
					cboState.AddItem(FCConvert.ToString(rsCodes.Get_Fields_String("StateCode")));
					rsCodes.MoveNext();
				}
				while (rsCodes.EndOfFile() != true);
			}
		}

		private void LoadTypeCombo()
		{
			int counter;
			cboType.Clear();
			cboType.AddItem("Individual");
			cboType.AddItem("Other (Company, Group etc.)");
			cboModule.Clear();
			cboCommentModule.Clear();
			for (counter = 0; counter <= modCentralParties.gintNumberOfModuleTypes; counter++)
			{
				if (modCentralParties.IsModuleActive_2(modCentralParties.GetModuleCode(counter)))
				{
					cboModule.AddItem(modCentralParties.GetModuleDescription(counter));
					cboModule.ItemData(cboModule.NewIndex, counter);
					cboCommentModule.AddItem(modCentralParties.GetModuleDescription(counter));
					cboCommentModule.ItemData(cboCommentModule.NewIndex, counter);
				}
			}
		}

		private void LoadDateCombos()
		{
			int counter;
			cboStartMonth.Clear();
			cboEndMonth.Clear();
			for (counter = 1; counter <= 12; counter++)
			{
				cboStartMonth.AddItem(Strings.Format(FCConvert.ToString(counter) + "/1/2012", "MMMM"));
				cboEndMonth.AddItem(Strings.Format(FCConvert.ToString(counter) + "/1/2012", "MMMM"));
			}
			cboStartDay.Clear();
			cboEndDay.Clear();
			for (counter = 1; counter <= 31; counter++)
			{
				cboStartDay.AddItem(FCConvert.ToString(counter));
				cboEndDay.AddItem(FCConvert.ToString(counter));
			}
		}

		private void SetupStartDateCombo(int intMonth)
		{
			if (intMonth > 0 && intMonth < 13)
			{
				int intDays = 0;
				int intCurDay = 0;
				intCurDay = cboStartDay.SelectedIndex;
				cboStartDay.Clear();
				intDays = GetDays(intMonth);
				int x;
				for (x = 1; x <= intDays; x++)
				{
					cboStartDay.AddItem(FCConvert.ToString(x));
				}
				// x
				if (intCurDay < cboStartDay.Items.Count)
				{
					cboStartDay.SelectedIndex = intCurDay;
				}
				else
				{
					cboStartDay.SelectedIndex = cboStartDay.Items.Count - 1;
				}
			}
		}

		private void SetupEndDateCombo(int intMonth)
		{
			if (intMonth > 0 && intMonth < 13)
			{
				int intDays = 0;
				int intCurDay = 0;
				intCurDay = cboEndDay.SelectedIndex;
				cboEndDay.Clear();
				intDays = GetDays(intMonth);
				int x;
				for (x = 1; x <= intDays; x++)
				{
					cboEndDay.AddItem(FCConvert.ToString(x));
				}
				// x
				if (intCurDay < cboStartDay.Items.Count)
				{
					cboEndDay.SelectedIndex = intCurDay;
				}
				else
				{
					cboEndDay.SelectedIndex = cboEndDay.Items.Count - 1;
				}
			}
		}
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short GetDays(int intMonth)
		{
			short GetDays = 0;
			int intDays = 0;
			switch (intMonth)
			{
				case 4:
				case 6:
				case 9:
				case 11:
					{
						intDays = 30;
						break;
					}
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					{
						intDays = 31;
						break;
					}
				case 2:
					{
						intDays = 28;
						break;
					}
				default:
					{
						intDays = 0;
						break;
					}
			}
			//end switch
			GetDays = FCConvert.ToInt16(intDays);
			return GetDays;
		}

		private void LoadAddressTypeCombo(string strModule = "")
		{
			cboAddressType.Clear();
			if (strModule == "BD")
			{
				cboAddressType.AddItem("Check");
				cboAddressType.AddItem("Correspondence");
			}
			else
			{
				cboAddressType.AddItem("Primary");
				cboAddressType.AddItem("Override");
			}
		}

		private void frmEditCentralParties_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			DialogResult answer = 0;
			if (blnDirty)
			{
				answer = FCMessageBox.Show("Data has been changed but not saved. Would you like to save it now?", MsgBoxStyle.YesNo | MsgBoxStyle.Question, "Save Changes?");
				if (answer == DialogResult.Yes)
				{
					if (NewSaveParty())
					{
						blnDirty = false;
					}
					else
					{
						e.Cancel = true;
						this.BringToFront();
						return;
					}
				}
				else
				{
					e.Cancel = false;
					blnDirty = false;
				}
			}
		}

		private void frmEditCentralParties_Resize(object sender, System.EventArgs e)
		{
			ResizeReferencesGrid();
			fgrdAddress.ColWidth(0, FCConvert.ToInt32(fgrdAddress.WidthOriginal * 0.03));
			fgrdAddress.ColWidth(AddressTypeCol, FCConvert.ToInt32(fgrdAddress.WidthOriginal * 0.1));
			fgrdAddress.ColWidth(AddressModuleCol, FCConvert.ToInt32(fgrdAddress.WidthOriginal * 0.06));
			fgrdAddress.ColWidth(AddressDateRangeCol, FCConvert.ToInt32(fgrdAddress.WidthOriginal * 0.15));
			fgrdAddress.ColWidth(AddressAddressCol, FCConvert.ToInt32(fgrdAddress.WidthOriginal * 0.25));
			fgrdAddress.ColWidth(AddressOverrideNameCol, FCConvert.ToInt32(fgrdAddress.WidthOriginal * 0.2));
			fgrdAddress.ColWidth(AddressModuleEntityCol, FCConvert.ToInt32(fgrdAddress.WidthOriginal * 0.15));
			fgrdComments.ColWidth(0, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.03));
			fgrdComments.ColWidth(CommentsEnteredByCol, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.1));
            //FC:FINAL:MSH - increase width of column to avoid cutting off header text
			//fgrdComments.ColWidth(CommentsLastModifiedCol, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.1));
			fgrdComments.ColWidth(CommentsLastModifiedCol, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.15));
			fgrdComments.ColWidth(CommentsModuleCol, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.07));
			fgrdComments.ColWidth(CommentsCommentCol, FCConvert.ToInt32(fgrdComments.WidthOriginal * 0.25));
			fgrdContacts.ColWidth(0, FCConvert.ToInt32(fgrdContacts.WidthOriginal * 0.03));
			fgrdContacts.ColWidth(ContactsNameCol, FCConvert.ToInt32(fgrdContacts.WidthOriginal * 0.3));
			fgrdContacts.ColWidth(ContactsDescriptionCol, FCConvert.ToInt32(fgrdContacts.WidthOriginal * 0.3));
			fgrdContacts.ColWidth(ContactsEmailCol, FCConvert.ToInt32(fgrdContacts.WidthOriginal * 0.2));
			fgrdContacts.ColWidth(ContactsPhoneCol, FCConvert.ToInt32(fgrdContacts.WidthOriginal * 0.25));
			fgrdPhone.ColWidth(0, FCConvert.ToInt32(fgrdPhone.WidthOriginal * 0.05));
			fgrdPhone.ColWidth(PhoneDescriptionCol, FCConvert.ToInt32(fgrdPhone.WidthOriginal * 0.4));
			fgrdPhone.ColWidth(PhonePhoneCol, FCConvert.ToInt32(fgrdPhone.WidthOriginal * 0.35));
			fgrdPhone.ColWidth(PhoneExtCol, FCConvert.ToInt32(fgrdPhone.WidthOriginal * 0.1));
			fgrdContactPhone.ColWidth(0, FCConvert.ToInt32(fgrdContactPhone.WidthOriginal * 0.05));
			fgrdContactPhone.ColWidth(PhoneDescriptionCol, FCConvert.ToInt32(fgrdContactPhone.WidthOriginal * 0.4));
			fgrdContactPhone.ColWidth(PhonePhoneCol, FCConvert.ToInt32(fgrdContactPhone.WidthOriginal * 0.35));
			fgrdContactPhone.ColWidth(PhoneExtCol, FCConvert.ToInt32(fgrdContactPhone.WidthOriginal * 0.1));
			fgrdAddress.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
			//FC:FINAL:MSH - issue #1247: AutoSize doesn't work in WiseJ, so temp solution - increase height of row depend on number of line breaks(because in some cases part of data will miss)
			//fgrdAddress.AutoSize(AddressAddressCol);
			for (int i = 0; i < fgrdAddress.RowCount; i++)
			{
				int rowIndex = fgrdAddress.GetFlexRowIndex(i);
				int lineMultiplier = fgrdAddress.TextMatrix(rowIndex, AddressAddressCol).Split('\n').Length;
				if (lineMultiplier > 0)
				{
					fgrdAddress.RowHeight(rowIndex, lineMultiplier * 300);
				}
			}
			fraAddress.CenterToContainer(this.ClientArea);
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			if (!blnNew)
			{
				// If clsCheck.IsPartyReferenced(collParties[curPartyIndex]) Then
				if (clsCheck.IsPartyReferenced(FCConvert.ToInt32(rsIDs.Get_Fields_Int32("id"))))
				{
					FCMessageBox.Show("Unable to delete party as it is being used in one or more programs.", MsgBoxStyle.Information, "Unable to Delete");
					return;
				}
			}
		}

		private void mnuFileNew_Click(object sender, System.EventArgs e)
		{
			blnNew = true;
			// rsParties.AddNew
			cParty tParty = new cParty();
			// collParties.Add (0)
			// curPartyIndex = collParties.Count
			SetMovementMenuItems();
			LoadParty(tParty);
		}

		public void mnuFileNew_Click()
		{
			mnuFileNew_Click(mnuFileNew, new System.EventArgs());
		}

		private void mnuFileNext_Click(object sender, System.EventArgs e)
		{

			if (!rsIDs.EndOfFile() && !rsIDs.IsLastRecord())
			{
				rsIDs.MoveNext();
				if (FCConvert.ToInt32(rsIDs.Get_Fields_Int32("id")) > 0)
				{
					// VBto upgrade warning: tParty As cParty	OnWrite(cParty)
					cParty tParty = new cParty();
					cPartyController tPCont = new cPartyController();
					tParty = tPCont.GetParty(FCConvert.ToInt32(rsIDs.Get_Fields_Int32("id")));
					if (!(tParty == null))
					{
						SetMovementMenuItems();
						LoadParty(tParty);
					}
					else
					{
						FCMessageBox.Show("Party with ID " + FCConvert.ToString(rsIDs.Get_Fields_Int32("id")) + " could not be found", MsgBoxStyle.Critical, "Record Not Found");
						rsIDs.MovePrevious();
					}
				}
				else
				{
					rsIDs.MovePrevious();
				}
			}

		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (NewSaveParty())
			{
				blnDirty = false;
				rsIDs.OpenRecordset("select ID from parties order by ID", "CentralParties");
				// If rsIDs.FindFirstRecord("ID", lngReturnedID) Then
				if (rsIDs.FindFirst("id = " + FCConvert.ToString(Statics.lngReturnedID)))
				{
					cParty tParty = new cParty();
					cPartyController tPCont = new cPartyController();
					tParty = tPCont.GetParty(FCConvert.ToInt32(rsIDs.Get_Fields_Int32("id")));
					SetMovementMenuItems();
					LoadParty(tParty);
				}
			}
		}

		public int Init(ref int lngID)
		{
			int Init = 0;
            Statics.lngReturnedID = lngID;
			blnFromAccountScreen = true;
			//FC:FINAL:CHN - issue #1299: Remove delete button. 
			cmdFileDelete.Enabled = false;
			mnuFileDelete.Enabled = false;
			blnDirty = false;
			//FC:FINAL:AM: always show in tab
			this.Show(FormShowEnum.Modal);
			//this.Show(App.MainForm);
			Init = Statics.lngReturnedID;
			return Init;
		}

		private void mnuPrevious_Click(object sender, System.EventArgs e)
		{

			if (!rsIDs.BeginningOfFile() && !rsIDs.isFirstRecord())
			{
				rsIDs.MovePrevious();
				// VBto upgrade warning: tParty As cParty	OnWrite(cParty)
				cParty tParty = new cParty();
				cPartyController tPCont = new cPartyController();
				tParty = tPCont.GetParty(FCConvert.ToInt32(rsIDs.Get_Fields_Int32("id")));
				if (!(tParty == null))
				{
					if (blnNew)
					{
						blnNew = false;
					}
					SetMovementMenuItems();
					LoadParty(tParty);
				}
				else
				{
					FCMessageBox.Show("Party with ID " + FCConvert.ToString(rsIDs.Get_Fields_Int32("id")) + " could not be found", MsgBoxStyle.Critical, "Record Not Found");
					rsIDs.MoveNext();
				}
			}

		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

        private void LoadParty(cParty tParty)
		{
			int counter;
			ClearScreen();
			// If rsParties.Get_Fields("PartyType") = 1 Then
			if (tParty.PartyType == 1)
			{
				cboType.SelectedIndex = 1;
				// txtCompanyName = rsParties.Get_Fields("FirstName")
				txtCompanyName.Text = tParty.FirstName;
			}
			else
			{
				cboType.SelectedIndex = 0;

				txtFirst.Text = tParty.FirstName;
				txtLast.Text = tParty.LastName;
				txtMI.Text = tParty.MiddleName;
				txtDesignation.Text = tParty.Designation;
			}
			lblGuid.Text = tParty.PartyGUID;
			lblPartyCreatedBy.Text = tParty.CreatedBy;

			txtEmail.Text = tParty.Email;
			txtWebAddress.Text = tParty.WebAddress;
			lblParyNumber.Text = tParty.ID.ToString();
			if (tParty.DateCreated.Year > 2000)
			{
				lblCreatedDate.Text = Strings.Format(tParty.DateCreated, "MM/dd/yyyy");
			}
			else
			{
				lblCreatedDate.Text = "";
			}
			LoadPhoneGrid(tParty);
			LoadAddressGrid(tParty);
			LoadContactsGrid(tParty);
			LoadCommentsGrid(tParty);
			App.DoEvents();
			// Reset all information pertianing to changes and start again
			intTotalNumberOfControls = 0;
			clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
			clsReportChanges.Reset();
			// Initialize all the control and old data values
			FillControlInformationClass();
			FillControlInformationClassFromGrid();
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}
			blnDirty = false;
		}

		private void LoadAddressGrid(cParty tParty)
		{
			clsDRWrapper rs = new clsDRWrapper();
			// rs.OpenRecordset "SELECT * FROM Addresses WHERE PartyID = " & rsParties.Get_Fields("ID"), "CentralParties"
			foreach (cPartyAddress tAddress in tParty.Addresses)
			{
				fgrdAddress.Rows += 1;
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressAddress1Col, tAddress.Address1);
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressAddress2Col, tAddress.Address2);
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressAddress3Col, tAddress.Address3);
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressCityCol, tAddress.City);
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressStateCol, tAddress.State);
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressZipCol, tAddress.Zip);
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressCountryCol, tAddress.Country);
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressAddressCol, CreateAddressString_2186(tAddress.Address1, tAddress.Address2, tAddress.Address3, tAddress.City, tAddress.State, tAddress.Zip, tAddress.Country));
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressCommentCol, tAddress.Comment);
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressEndDayCol, FCConvert.ToString(tAddress.EndDay));
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressEndMonthCol, FCConvert.ToString(tAddress.EndMonth));
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressStartDayCol, FCConvert.ToString(tAddress.StartDay));
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressStartMonthCol, FCConvert.ToString(tAddress.StartMonth));
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressSeasonalCol, FCConvert.ToString(tAddress.Seasonal));
				if (tAddress.Seasonal)
				{
					fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressDateRangeCol, CreateDateRangeString(tAddress.StartMonth, tAddress.StartDay, tAddress.EndMonth, tAddress.EndDay));
				}
				else
				{
					fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressDateRangeCol, "");
				}
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressIDCol, FCConvert.ToString(tAddress.ID));
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressModuleCol, tAddress.ProgModule);
				if (tAddress.ModAccountID > 0)
				{
					fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressModuleEntityCol, CreateModuleEntityString_8(tAddress.ModAccountID, tAddress.ProgModule));
				}
				else
				{
					fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressModuleEntityCol, "");
				}
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressOverrideNameCol, tAddress.OverrideName);
				fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressTypeCol, tAddress.AddressType);
			}
			// tAddress
			// If rs.EndOfFile <> True And rs.BeginningOfFile <> True Then
			// Do
			// fgrdAddress.Rows = fgrdAddress.Rows + 1
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressAddress1Col) = Trim(rs.Get_Fields("Address1"))
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressAddress2Col) = Trim(rs.Get_Fields("Address2"))
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressAddress3Col) = Trim(rs.Get_Fields("Address3"))
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressCityCol) = Trim(rs.Get_Fields("City"))
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressStateCol) = Trim(rs.Get_Fields("State"))
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressZipCol) = Trim(rs.Get_Fields("Zip"))
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressCountryCol) = Trim(rs.Get_Fields("Country"))
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressAddressCol) = CreateAddressString(Trim(rs.Get_Fields("Address1")), Trim(rs.Get_Fields("Address2")), Trim(rs.Get_Fields("Address3")), Trim(rs.Get_Fields("City")), Trim(rs.Get_Fields("State")), Trim(rs.Get_Fields("Zip")), Trim(rs.Get_Fields("Country")))
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressCommentCol) = rs.Get_Fields("Comment")
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressEndDayCol) = rs.Get_Fields("EndDay")
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressEndMonthCol) = rs.Get_Fields("EndMonth")
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressStartDayCol) = rs.Get_Fields("StartDay")
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressStartMonthCol) = rs.Get_Fields("StartMonth")
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressSeasonalCol) = rs.Get_Fields("Seasonal")
			// If rs.Get_Fields("Seasonal") Then
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressDateRangeCol) = CreateDateRangeString(rs.Get_Fields("StartMonth"), rs.Get_Fields("StartDay"), rs.Get_Fields("EndMonth"), rs.Get_Fields("EndDay"))
			// Else
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressDateRangeCol) = ""
			// End If
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressIDCol) = Trim(rs.Get_Fields("ID"))
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressModuleCol) = Trim(rs.Get_Fields("ProgModule"))
			// If rs.Get_Fields("ModAccountID") > 0 Then
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressModuleEntityCol) = CreateModuleEntityString(rs.Get_Fields("ModAccountID"), Trim(rs.Get_Fields("ProgModule")))
			// Else
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressModuleEntityCol) = ""
			// End If
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressOverrideNameCol) = Trim(rs.Get_Fields("OverrideName"))
			// fgrdAddress.TextMatrix(fgrdAddress.Rows - 1, AddressTypeCol) = Trim(rs.Get_Fields("AddressType"))
			// rs.MoveNext
			// Loop While rs.EndOfFile <> True
			// End If
			// fgrdAddress.Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, fgrdAddress.Rows - 1, fgrdAddress.Cols - 1) = 10
			fgrdAddress.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
			//FC:FINAL:MSH - issue #1247: AutoSize doesn't work in WiseJ, so temp solution - increase height of row depend on number of line breaks(because in some cases part of data will miss)
			//fgrdAddress.AutoSize(AddressAddressCol);
			for (int i = 0; i < fgrdAddress.RowCount; i++)
			{
				int rowIndex = fgrdAddress.GetFlexRowIndex(i);
				int lineMultiplier = fgrdAddress.TextMatrix(rowIndex, AddressAddressCol).Split('\n').Length;
				if (lineMultiplier > 0)
				{
					fgrdAddress.RowHeight(rowIndex, lineMultiplier * 300);
				}
			}
		}

		private string CreateModuleEntityString_8(int lngID, string strModule)
		{
			return CreateModuleEntityString(ref lngID, ref strModule);
		}

		private string CreateModuleEntityString(ref int lngID, ref string strModule)
		{
			string CreateModuleEntityString = "";
			return CreateModuleEntityString;
		}

		private string CreateDateRangeString(int intStartMonth, int intStartDay, int intEndMonth, int intEndDay)
		{
			string CreateDateRangeString = "";
			CreateDateRangeString = Strings.Format(FCConvert.ToString(intStartMonth) + "/1/2012", "mmm") + " " + FCConvert.ToString(intStartDay) + " to " + Strings.Format(FCConvert.ToString(intEndMonth) + "/1/2012", "mmm") + " " + FCConvert.ToString(intEndDay);
			return CreateDateRangeString;
		}

		private string CreateAddressString_2186(string strAdd1, string strAdd2, string strAdd3, string strCity, string strState, string strZip, string strCountry)
		{
			return CreateAddressString(ref strAdd1, ref strAdd2, ref strAdd3, ref strCity, ref strState, ref strZip, ref strCountry);
		}

		private string CreateAddressString(ref string strAdd1, ref string strAdd2, ref string strAdd3, ref string strCity, ref string strState, ref string strZip, ref string strCountry)
		{
			string CreateAddressString = "";
			string strAddressLine = "";
			string strReturnValue = "";
			if (Strings.Trim(strAdd1) != "")
			{
				strReturnValue = strAdd1;
			}
			if (Strings.Trim(strAdd2) != "")
			{
				if (Strings.Trim(strReturnValue) == "")
				{
					strReturnValue += Strings.Trim(strAdd2);
				}
				else
				{
					strReturnValue += "\r\n" + Strings.Trim(strAdd2);
				}
			}
			if (Strings.Trim(strAdd3) != "")
			{
				if (Strings.Trim(strReturnValue) == "")
				{
					strReturnValue += Strings.Trim(strAdd3);
				}
				else
				{
					strReturnValue += "\r\n" + Strings.Trim(strAdd3);
				}
			}
			if (Strings.Trim(strCity) != "" || Strings.Trim(strState) != "" || Strings.Trim(strZip) != "")
			{
				if (Strings.Trim(strState) != "")
				{
					strAddressLine = Strings.Trim(Strings.Trim(strCity) + ", " + Strings.Trim(strState)) + " ";
				}
				else
				{
					strAddressLine = Strings.Trim(strCity) + " ";
				}
				if (Strings.Trim(strZip) != "")
				{
					strAddressLine += Strings.Trim(strZip);
				}
				if (Strings.Trim(strReturnValue) == "")
				{
					strReturnValue += strAddressLine;
				}
				else
				{
					strReturnValue += "\r\n" + strAddressLine;
				}
			}
			CreateAddressString = strReturnValue;
			return CreateAddressString;
		}

		private void LoadContactsGrid(cParty tParty)
		{
			// Dim rs As New clsDRWrapper
			// Dim rsPhone As New clsDRWrapper
			fgrdSavedContactPhones.Rows = 0;
			// VBto upgrade warning: tPhone As cPartyPhoneNumber	OnWrite(Collection)
			cPartyPhoneNumber tPhone;
			foreach (cContact tContact in tParty.Contacts)
			{
				fgrdContacts.Rows += 1;
				fgrdContacts.TextMatrix(fgrdContacts.Rows - 1, ContactsCommentCol, tContact.Comment);
				fgrdContacts.TextMatrix(fgrdContacts.Rows - 1, ContactsDescriptionCol, tContact.Description);
				fgrdContacts.TextMatrix(fgrdContacts.Rows - 1, ContactsEmailCol, tContact.Email);
				fgrdContacts.TextMatrix(fgrdContacts.Rows - 1, ContactsIDCol, FCConvert.ToString(tContact.ID));
				fgrdContacts.TextMatrix(fgrdContacts.Rows - 1, ContactsNameCol, tContact.Name);
				if (tContact.PhoneNumbers.Count > 0)
				{
					tPhone = tContact.PhoneNumbers[1];
					fgrdContacts.TextMatrix(fgrdContacts.Rows - 1, ContactsPhoneCol, tPhone.PhoneNumber);
				}
				else
				{
					fgrdContacts.TextMatrix(fgrdContacts.Rows - 1, ContactsPhoneCol, "");
				}
				foreach (cPartyPhoneNumber tPhone_foreach in tContact.PhoneNumbers)
				{
					tPhone = tPhone_foreach;
					fgrdSavedContactPhones.Rows += 1;
					fgrdSavedContactPhones.TextMatrix(fgrdSavedContactPhones.Rows - 1, SavedPhoneDescriptionCol, tPhone.Description);
					fgrdSavedContactPhones.TextMatrix(fgrdSavedContactPhones.Rows - 1, SavedPhoneIDCol, FCConvert.ToString(tPhone.ID));
					fgrdSavedContactPhones.TextMatrix(fgrdSavedContactPhones.Rows - 1, SavedPhoneLineNumberCol, FCConvert.ToString(tPhone.PhoneOrder));
					fgrdSavedContactPhones.TextMatrix(fgrdSavedContactPhones.Rows - 1, SavedPhonePhoneCol, tPhone.PhoneNumber);
					fgrdSavedContactPhones.TextMatrix(fgrdSavedContactPhones.Rows - 1, SavedPhoneExtCol, tPhone.Extension);
					fgrdSavedContactPhones.TextMatrix(fgrdSavedContactPhones.Rows - 1, SavedPhoneContactIDCol, FCConvert.ToString(tContact.ID));
					tPhone = null;
				}
				// tPhone
			}
			
		}

		private void LoadCommentsGrid(cParty tParty)
		{
			foreach (cPartyComment tComment in tParty.Comments)
			{
				fgrdComments.Rows += 1;
				fgrdComments.TextMatrix(fgrdComments.Rows - 1, CommentsCommentCol, tComment.Comment);
				fgrdComments.TextMatrix(fgrdComments.Rows - 1, CommentsEnteredByCol, tComment.EnteredBy);
				fgrdComments.TextMatrix(fgrdComments.Rows - 1, CommentsIDCol, FCConvert.ToString(tComment.ID));
				fgrdComments.TextMatrix(fgrdComments.Rows - 1, CommentsLastModifiedCol, Strings.Format(tComment.LastModified, "MM/dd/yyyy"));
				fgrdComments.TextMatrix(fgrdComments.Rows - 1, CommentsModuleCol, tComment.ProgModule);
			}
			
		}

		private void LoadPhoneGrid(cParty tParty)
		{
			fgrdPhone.Rows = 1;
			foreach (cPartyPhoneNumber tPhone in tParty.PhoneNumbers)
			{
				fgrdPhone.AddItem("");
				fgrdPhone.TextMatrix(fgrdPhone.Rows - 1, PhoneIDCol, FCConvert.ToString(tPhone.ID));
				fgrdPhone.TextMatrix(fgrdPhone.Rows - 1, PhoneDescriptionCol, tPhone.Description);
				fgrdPhone.TextMatrix(fgrdPhone.Rows - 1, PhonePhoneCol, tPhone.PhoneNumber);
				fgrdPhone.TextMatrix(fgrdPhone.Rows - 1, PhoneExtCol, tPhone.Extension);
			}
			// tPhone
			// Dim rsPhoneInfo As New clsDRWrapper
			int counter/*unused?*/;
			
			if (fgrdPhone.Rows < 2)
			{
				fgrdPhone.AddItem("");
				fgrdPhone.TextMatrix(fgrdPhone.Rows - 1, PhoneIDCol, "0");
			}

		}

		private void LoadContactsPhoneGrid()
		{
			int counter;
			fgrdContactPhone.Rows = 1;
			for (counter = 0; counter <= fgrdSavedContactPhones.Rows - 1; counter++)
			{
				if (fgrdSavedContactPhones.TextMatrix(counter, SavedPhoneContactIDCol) == fgrdContacts.TextMatrix(intCurrentRow, ContactsIDCol))
				{
					fgrdContactPhone.AddItem("");
					fgrdContactPhone.TextMatrix(fgrdContactPhone.Rows - 1, PhoneIDCol, fgrdSavedContactPhones.TextMatrix(counter, SavedPhoneIDCol));
					fgrdContactPhone.TextMatrix(fgrdContactPhone.Rows - 1, PhoneDescriptionCol, fgrdSavedContactPhones.TextMatrix(counter, SavedPhoneDescriptionCol));
					fgrdContactPhone.TextMatrix(fgrdContactPhone.Rows - 1, PhonePhoneCol, fgrdSavedContactPhones.TextMatrix(counter, SavedPhonePhoneCol));
					fgrdContactPhone.TextMatrix(fgrdContactPhone.Rows - 1, PhoneExtCol, fgrdSavedContactPhones.TextMatrix(counter, SavedPhoneExtCol));
				}
			}
			for (counter = 1; counter <= 3; counter++)
			{
				fgrdContactPhone.AddItem("");
				fgrdContactPhone.TextMatrix(fgrdContactPhone.Rows - 1, PhoneIDCol, "0");
			}
		}

		private void ClearScreen()
		{
			txtCompanyName.Text = "";
			txtDesignation.Text = "";
			txtLast.Text = "";
			txtMI.Text = "";
			txtFirst.Text = "";
			txtEmail.Text = "";
			txtWebAddress.Text = "";
			lblCreatedDate.Text = "";
			lblParyNumber.Text = "";
			fgrdAddress.Rows = 1;
			fgrdContacts.Rows = 1;
			fgrdComments.Rows = 1;
			fgrdPhone.Rows = 1;
			fgrdSavedContactPhones.Rows = 0;
			tabDetails.SelectedIndex = 0;
			blnDirty = false;
		}

		private void ClearAddressFrame()
		{
			txtAddress1.Text = "";
			txtAddress2.Text = "";
			txtAddress3.Text = "";
			txtCity.Text = "";
			txtZip.Text = "";
			txtOverrideName.Text = "";
			txtComments.TextRTF = "";
			txtCountry.Text = "";
			cboState.Text = "ME";
			cboModule.SelectedIndex = 0;
			cboAccount.Clear();
			cboAccount.Enabled = false;
			LoadAddressTypeCombo();
			cboAddressType.SelectedIndex = 0;
			cboStartDay.SelectedIndex = -1;
			cboStartMonth.SelectedIndex = -1;
			cboEndDay.SelectedIndex = -1;
			cboEndMonth.SelectedIndex = -1;
			chkSeasonal.CheckState = Wisej.Web.CheckState.Unchecked;
		}

		private void ClearContactFrame()
		{
			txtContactComments.TextRTF = "";
			txtContactDescription.Text = "";
			txtContactEmail.Text = "";
			txtContactName.Text = "";
			fgrdContactPhone.Rows = 1;
		}

		private void ClearCommentsFrame()
		{
			cboCommentModule.SelectedIndex = 0;
			lblCreatedBy.Text = "";
			lblCommentDateLastModified.Text = "";
			txtCommentComment.TextRTF = "";
		}

		private void fgrdPhone_KeyDown(object sender, KeyEventArgs e)
		{
			DialogResult ans = 0;
			int intRow = 0;
			int KeyCode = e.KeyValue;
			if (e.KeyCode == Keys.Delete)
			{
				if (fgrdPhone.Row > 0)
				{
					KeyCode = 0;
					// If fgrdPhone.Row > 1 Then
					intRow = fgrdPhone.Row;
					if (fgrdPhone.TextMatrix(fgrdPhone.Row, PhoneIDCol) == "0")
					{
						// Dave 12/14/2006---------------------------------------------------
						// Add change record for adding a row to the grid
						clsReportChanges.AddChange("Removed Phone Number Row " + FCConvert.ToString(intRow));
						// ------------------------------------------------------------------
						fgrdPhone.RemoveItem(fgrdPhone.Row);
					}
					else
					{
						ans = FCMessageBox.Show("Are you sure you wish to delete this entry?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Delete Entry?");
						if (ans == DialogResult.Yes)
						{
							clsDRWrapper rsDel = new clsDRWrapper();
							rsDel.Execute("delete from centralphonenumbers where id = " + FCConvert.ToString(Conversion.Val(fgrdPhone.TextMatrix(fgrdPhone.Row, PhoneIDCol))), "CentralParties");
							modAuditReporting.RemoveGridRow_8("fgrdPhone", intTotalNumberOfControls - 1, fgrdPhone.Row, clsControlInfo);
							// Dave 12/14/2006---------------------------------------------------
							// Add change record for adding a row to the grid
							clsReportChanges.AddChange("Removed Phone Number Row " + FCConvert.ToString(intRow));
							// ------------------------------------------------------------------
							fgrdPhone.RemoveItem(fgrdPhone.Row);
						}
					}
					// End If
				}
			}
			else if (e.KeyCode == Keys.Insert)
			{
				KeyCode = 0;
				fgrdPhone.Rows += 1;
				// If fgrdPhone.Row = 0 Then
				// fgrdPhone.AddItem "", fgrdPhone.Row + 1
				// Else
				// fgrdPhone.AddItem "", fgrdPhone.Row
				// End If
				fgrdPhone.TextMatrix(fgrdPhone.Rows - 1, PhoneIDCol, FCConvert.ToString(0));
				fgrdPhone.TextMatrix(fgrdPhone.Rows - 1, PhoneDescriptionCol, "");
				fgrdPhone.TextMatrix(fgrdPhone.Rows - 1, PhonePhoneCol, "");
				fgrdPhone.TextMatrix(fgrdPhone.Rows - 1, PhoneExtCol, "");
				// Dave 12/14/2006---------------------------------------------------
				// Add change record for adding a row to the grid
				clsReportChanges.AddChange("Added Phone Number Row " + FCConvert.ToString(fgrdPhone.Rows - 1));
				// ------------------------------------------------------------------
			}
			else if (e.KeyCode == Keys.Return)
			{
				KeyCode = 0;
				fgrdPhone.EditCell();
			}
		}

		private void fgrdPhone_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				if (fgrdPhone.Row < fgrdPhone.Rows - 1)
				{
					//e.KeyCode = 0;
					e.Handled = true;
					//Support.SendKeys("{TAB}", false);
					//FC:FINAL:MSH - issue #1246: SendKeys not implemented in web, so implement same functionality as in original
					if (fgrdPhone.Col < fgrdPhone.Cols - 1)
					{
						fgrdPhone.Col += 1;
					}
					else
					{
						fgrdPhone.Row += 1;
						fgrdPhone.Col = 2;
					}
				}
				else
				{
					//e.KeyCode = 0;
					e.Handled = true;
					fgrdPhone.Rows += 1;
					// If fgrdPhone.Row = 0 Then
					// fgrdPhone.AddItem "", fgrdPhone.Row + 1
					// Else
					// fgrdPhone.AddItem "", fgrdPhone.Row
					// End If
					fgrdPhone.TextMatrix(fgrdPhone.Rows - 1, PhoneIDCol, FCConvert.ToString(0));
					fgrdPhone.TextMatrix(fgrdPhone.Rows - 1, PhoneDescriptionCol, "");
					fgrdPhone.TextMatrix(fgrdPhone.Rows - 1, PhonePhoneCol, "");
					fgrdPhone.TextMatrix(fgrdPhone.Rows - 1, PhoneExtCol, "");
				}
			}
		}

		private void fgrdContactPhone_KeyDown(object sender, KeyEventArgs e)
		{
			DialogResult ans = 0;
			int KeyCode = e.KeyValue;
			if (e.KeyCode == Keys.Delete)
			{
				if (fgrdContactPhone.Row > 0)
				{
					KeyCode = 0;
					// If fgrdContactPhone.Row > 1 Then
					if (fgrdContactPhone.TextMatrix(fgrdContactPhone.Row, PhoneIDCol) == "0")
					{
						fgrdContactPhone.RemoveItem(fgrdContactPhone.Row);
					}
					else
					{
						ans = FCMessageBox.Show("Are you sure you wish to delete this entry?", MsgBoxStyle.Question | MsgBoxStyle.YesNo, "Delete Entry?");
						if (ans == DialogResult.Yes)
						{
							clsDRWrapper rsDel = new clsDRWrapper();
							rsDel.Execute("delete from contactphonenumbers where id = " + FCConvert.ToString(Conversion.Val(fgrdContactPhone.TextMatrix(fgrdContactPhone.Row, PhoneIDCol))), "CentralParties");
							fgrdContactPhone.RemoveItem(fgrdContactPhone.Row);
						}
					}
					// End If
				}
			}
			else if (e.KeyCode == Keys.Insert)
			{
				KeyCode = 0;
				if (fgrdContactPhone.Row == 0)
				{
					fgrdContactPhone.AddItem("", fgrdContactPhone.Row + 1);
				}
				else
				{
					fgrdContactPhone.AddItem("", fgrdContactPhone.Row);
				}
				fgrdContactPhone.TextMatrix(fgrdContactPhone.Rows - 1, PhoneIDCol, FCConvert.ToString(0));
				fgrdContactPhone.TextMatrix(fgrdContactPhone.Rows - 1, PhoneDescriptionCol, "");
				fgrdContactPhone.TextMatrix(fgrdContactPhone.Rows - 1, PhonePhoneCol, "");
				fgrdContactPhone.TextMatrix(fgrdContactPhone.Rows - 1, PhoneExtCol, "");
			}
			else if (e.KeyCode == Keys.Return)
			{
				KeyCode = 0;
				fgrdContactPhone.EditCell();
			}
		}

		private void fgrdContactPhone_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				if (fgrdContactPhone.Row < fgrdContactPhone.Rows - 1)
				{
					//KeyCode = 0;
					e.Handled = true;
					//Support.SendKeys("{TAB}", false);
					//FC:FINAL:MSH - issue #1246: SendKeys not implemented in web, so implement same functionality as in original
					if (fgrdContactPhone.Col < fgrdContactPhone.Cols - 1)
					{
						fgrdContactPhone.Col += 1;
					}
					else
					{
						fgrdContactPhone.Row += 1;
						fgrdContactPhone.Col = 2;
					}
				}
				else
				{
					//KeyCode = 0;
					e.Handled = true;
					if (fgrdContactPhone.Row == 0)
					{
						fgrdContactPhone.AddItem("", fgrdContactPhone.Row + 1);
					}
					else
					{
						fgrdContactPhone.AddItem("", fgrdContactPhone.Row);
					}
					fgrdContactPhone.TextMatrix(fgrdContactPhone.Rows - 1, PhoneIDCol, FCConvert.ToString(0));
					fgrdContactPhone.TextMatrix(fgrdContactPhone.Rows - 1, PhoneDescriptionCol, "");
					fgrdContactPhone.TextMatrix(fgrdContactPhone.Rows - 1, PhonePhoneCol, "");
					fgrdContactPhone.TextMatrix(fgrdContactPhone.Rows - 1, PhoneExtCol, "");
					Support.SendKeys("{TAB}", false);
				}
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (NewSaveParty())
			{
				blnDirty = false;
				mnuProcessQuit_Click();
			}
		}

		private bool NewSaveParty()
		{
			bool NewSaveParty = false;
			cParty tParty = new cParty();
			cPartyController tPCont = new cPartyController();
			int counter;
			bool nameChange = false;

			if (cboType.SelectedIndex == 0)
			{
				if (Strings.Trim(txtLast.Text) == "" && Strings.Trim(txtFirst.Text) == "")
				{
					// MsgBox "You must enter a first and last name before you may continue.", MsgBoxStyle.Information, "Invalid Data"
					FCMessageBox.Show("You must enter a name before you may continue.", MsgBoxStyle.Information, "Invalid Data");
					NewSaveParty = false;
					return NewSaveParty;
				}
			}
			else
			{
				if (Strings.Trim(txtCompanyName.Text) == "")
				{
					FCMessageBox.Show("You must enter a company name before you may continue.", MsgBoxStyle.Information, "Invalid Data");
					NewSaveParty = false;
					return NewSaveParty;
				}
			}
			if (fgrdAddress.Rows <= 1)
			{
				if (FCMessageBox.Show("This party has no address information, are you sure you want to save it?", MsgBoxStyle.Exclamation | MsgBoxStyle.YesNo, "Save With No Address?") != DialogResult.Yes)
				{
					return NewSaveParty;
				}
			}
			fgrdPhone.Row = -1;
			App.DoEvents();
			clsDRWrapper rsGlobalVar = new clsDRWrapper();
			FCCollection varParties = new FCCollection();
			int partID/*unused?*/;
			if (blnNew)
			{
				rsGlobalVar.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
				if (rsGlobalVar.EndOfFile() != true && rsGlobalVar.BeginningOfFile() != true)
				{
					if (FCConvert.ToBoolean(rsGlobalVar.Get_Fields_Boolean("CentralPartyCheck")))
					{
						fraWait.Visible = true;
						// 
						// frmWait.lblMessage = "Checking for duplicates"
						// frmWait.StartBusy
						// frmWait.Show vbModal
						App.DoEvents();
						if (cboType.SelectedIndex == 0)
						{
							varParties = clsCheck.GetPartyHintsByParty(Strings.Trim(txtFirst.Text), Strings.Trim(txtMI.Text), Strings.Trim(txtLast.Text), Strings.Trim(txtDesignation.Text), cboType.SelectedIndex);
						}
						else
						{
							varParties = clsCheck.GetPartyHintsByParty(Strings.Trim(txtCompanyName.Text), "", "", "", cboType.SelectedIndex);
						}
						// frmWait.StopBusy
						// Unload frmWait
						fraWait.Visible = false;
						if (!(varParties == null))
						{
							if (varParties.Count > 0)
							{
								int intAns = 0;
								intAns = frmPartyList.InstancePtr.Init(ref varParties);
								if (intAns != FCConvert.ToInt32(DialogResult.Yes))
								{
									NewSaveParty = false;
									return NewSaveParty;
								}
							}
						}

					}
				}
			}
			// Dave 12/14/2006--------------------------------------------
			// Set New Information so we can compare
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillNewValue(this);
			}
			// This function compares old and new values and creates change records for any differences
			modAuditReporting.ReportChanges_2(intTotalNumberOfControls - 1, clsControlInfo, clsReportChanges);
			// ----------------------------------------------------------------
			cCreateGUID clsGuid = new cCreateGUID();
			if (blnNew)
			{
				tParty.PartyGUID = clsGuid.CreateGUID();
				tParty.DateCreated = DateTime.Today;
				tParty.CreatedBy = modGlobalConstants.Statics.clsSecurityClass.Get_UsersUserID();
			}
			else
			{
				if (Information.IsDate(lblCreatedDate.Text))
				{
					tParty.DateCreated = Convert.ToDateTime(lblCreatedDate.Text);
				}
				tParty.PartyGUID = lblGuid.Text;
				tParty.CreatedBy = lblPartyCreatedBy.Text;
				tParty.ID = FCConvert.ToInt32(lblParyNumber.Text);
			}
			tParty.PartyType = cboType.SelectedIndex;
			if (cboType.SelectedIndex == 0)
			{
				tParty.FirstName = Strings.Trim(txtFirst.Text);
				tParty.LastName = Strings.Trim(txtLast.Text);
				tParty.MiddleName = Strings.Trim(txtMI.Text);
				tParty.Designation = Strings.Trim(txtDesignation.Text);
			}
			else
			{
				tParty.FirstName = Strings.Trim(txtCompanyName.Text);
				tParty.LastName = "";
				tParty.MiddleName = "";
				tParty.Designation = "";
			}

			tParty.WebAddress = Strings.Trim(txtWebAddress.Text);
			tParty.Email = Strings.Trim(txtEmail.Text);
			cPartyAddress tAddress;
			for (counter = 1; counter <= fgrdAddress.Rows - 1; counter++)
			{
				tAddress = new cPartyAddress();
				tAddress.ID = FCConvert.ToInt32(Math.Round(Conversion.Val(fgrdAddress.TextMatrix(counter, AddressIDCol))));
				tAddress.Address1 = fgrdAddress.TextMatrix(counter, AddressAddress1Col);
				tAddress.Address2 = fgrdAddress.TextMatrix(counter, AddressAddress2Col);
				tAddress.Address3 = fgrdAddress.TextMatrix(counter, AddressAddress3Col);
				tAddress.City = fgrdAddress.TextMatrix(counter, AddressCityCol);
				tAddress.Zip = fgrdAddress.TextMatrix(counter, AddressZipCol);
				tAddress.OverrideName = fgrdAddress.TextMatrix(counter, AddressOverrideNameCol);
				tAddress.Comment = fgrdAddress.TextMatrix(counter, AddressCommentCol);
				tAddress.State = fgrdAddress.TextMatrix(counter, AddressStateCol);
				tAddress.Country = fgrdAddress.TextMatrix(counter, AddressCountryCol);
				tAddress.AddressType = fgrdAddress.TextMatrix(counter, AddressTypeCol);
				// tAddress.ProgModule = GetModuleDescription(Val(fgrdAddress.TextMatrix(counter, AddressModuleCol)))
				tAddress.ProgModule = fgrdAddress.TextMatrix(counter, AddressModuleCol);
				tAddress.ModAccountID = FCConvert.ToInt32(Math.Round(Conversion.Val(fgrdAddress.TextMatrix(counter, AddressModuleEntityCol))));
				tAddress.Seasonal = FCConvert.CBool(fgrdAddress.TextMatrix(counter, AddressSeasonalCol));
				tAddress.StartDay = FCConvert.ToInt32(Math.Round(Conversion.Val(fgrdAddress.TextMatrix(counter, AddressStartDayCol))));
				tAddress.StartMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(fgrdAddress.TextMatrix(counter, AddressStartMonthCol))));
				tAddress.EndDay = FCConvert.ToInt32(Math.Round(Conversion.Val(fgrdAddress.TextMatrix(counter, AddressEndDayCol))));
				tAddress.EndMonth = FCConvert.ToInt32(Math.Round(Conversion.Val(fgrdAddress.TextMatrix(counter, AddressEndMonthCol))));
				tParty.Addresses.Add(tAddress);
			}
			cPartyComment tPartyComment;
			for (counter = 1; counter <= fgrdComments.Rows - 1; counter++)
			{
				tPartyComment = new cPartyComment();
				tPartyComment.ID = FCConvert.ToInt32(Math.Round(Conversion.Val(fgrdComments.TextMatrix(counter, CommentsIDCol))));
				tPartyComment.ProgModule = fgrdComments.TextMatrix(counter, CommentsModuleCol);
				tPartyComment.EnteredBy = fgrdComments.TextMatrix(counter, CommentsEnteredByCol);
				tPartyComment.LastModified = FCConvert.ToDateTime(fgrdComments.TextMatrix(counter, CommentsLastModifiedCol));
				tPartyComment.Comment = fgrdComments.TextMatrix(counter, CommentsCommentCol);
				tParty.Comments.Add(tPartyComment);
			}
			cContact tContact;
			cPartyPhoneNumber tPhone;
			int counter2;
			string strPhone = "";
			for (counter = 1; counter <= fgrdContacts.Rows - 1; counter++)
			{
				tContact = new cContact();
				tContact.ID = FCConvert.ToInt32(Math.Round(Conversion.Val(fgrdContacts.TextMatrix(counter, ContactsIDCol))));
				tContact.Comment = fgrdContacts.TextMatrix(counter, ContactsCommentCol);
				tContact.Description = fgrdContacts.TextMatrix(counter, ContactsDescriptionCol);
				tContact.Email = fgrdContacts.TextMatrix(counter, ContactsEmailCol);
				tContact.Name = fgrdContacts.TextMatrix(counter, ContactsNameCol);
				tParty.Contacts.Add(tContact);
				for (counter2 = 0; counter2 <= fgrdSavedContactPhones.Rows - 1; counter2++)
				{
					if (fgrdSavedContactPhones.TextMatrix(counter2, SavedPhoneContactIDCol) == fgrdContacts.TextMatrix(counter, ContactsIDCol))
					{
						tPhone = new cPartyPhoneNumber();
						tPhone.ID = FCConvert.ToInt32(Math.Round(Conversion.Val(fgrdSavedContactPhones.TextMatrix(counter2, SavedPhoneIDCol))));
						tPhone.PhoneOrder = FCConvert.ToInt32(fgrdSavedContactPhones.TextMatrix(counter2, SavedPhoneLineNumberCol));
						tPhone.Description = fgrdSavedContactPhones.TextMatrix(counter2, SavedPhoneDescriptionCol);
						tPhone.PhoneNumber = fgrdSavedContactPhones.TextMatrix(counter2, SavedPhonePhoneCol);
						tPhone.Extension = fgrdSavedContactPhones.TextMatrix(counter2, SavedPhoneExtCol);
						tContact.PhoneNumbers.Add(tPhone);
					}
				}
			}
			int intLine;
			intLine = 0;
			for (counter = 1; counter <= fgrdPhone.Rows - 1; counter++)
			{
				strPhone = Strings.Trim(fgrdPhone.TextMatrix(counter, PhonePhoneCol).Replace("(", "").Replace(")", "").Replace("-", ""));
				// If Trim(fgrdPhone.TextMatrix(counter, PhoneDescriptionCol)) <> "" And strPhone <> "" Then
				if (strPhone != "")
				{
					tPhone = new cPartyPhoneNumber();
					intLine += 1;
					tPhone.ID = FCConvert.ToInt32(Math.Round(Conversion.Val(fgrdPhone.TextMatrix(counter, PhoneIDCol))));
					tPhone.PhoneOrder = intLine;
					tPhone.Description = Strings.Trim(fgrdPhone.TextMatrix(counter, PhoneDescriptionCol));
					if (Strings.Trim(fgrdPhone.TextMatrix(counter, PhonePhoneCol).Replace("(", "").Replace(")", "").Replace("-", "")) != "")
					{
						tPhone.PhoneNumber = fgrdPhone.TextMatrix(counter, PhonePhoneCol);
					}
					else
					{
						tPhone.PhoneNumber = "";
					}
					tPhone.Extension = fgrdPhone.TextMatrix(counter, PhoneExtCol);
					tParty.PhoneNumbers.Add(tPhone);
				}
			}
			NewSaveParty = tPCont.SaveParty(ref tParty);
			if (NewSaveParty)
			{
				MessageBox.Show("Saved Successfully", "Saved", MessageBoxButtons.OK);
			}

            Statics.lngReturnedID = tParty.ID;

			if (blnNew)
			{
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
				clsReportChanges.AddChange("New Party #" + FCConvert.ToString(Statics.lngReturnedID));
				clsReportChanges.SaveToAuditChangesTable("Edit Central Parties", Strings.Trim(Statics.lngReturnedID.ToString()), tParty.FullNameLastFirst, "", "", "SystemSettings");
				clsReportChanges.Reset();
			}
			else
			{
				// This function takes all the change records and writes them into the AuditChanges table in the database
				clsReportChanges.SaveToAuditChangesTable("Edit Central Parties", Strings.Trim(Statics.lngReturnedID.ToString()), tParty.FullNameLastFirst, "", "", "SystemSettings");
				// Reset all information pertianing to changes and start again
				intTotalNumberOfControls = 0;
				clsControlInfo = new clsAuditControlInformation[intTotalNumberOfControls + 1];
				clsReportChanges.Reset();
			}
			// Initialize all the control and old data values
			FillControlInformationClass();
			FillControlInformationClassFromGrid();
			for (counter = 0; counter <= intTotalNumberOfControls - 1; counter++)
			{
				clsControlInfo[counter].FillOldValue(this);
			}
			blnNew = false;
			blnDirty = false;
			// NewSaveParty = True
			return NewSaveParty;
		}

		private void txtComments_Change()
		{
			blnDirty = true;
		}

		private void txtCompanyName_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtContactComments_Change()
		{
			blnDirty = true;
		}

		private void txtDesignation_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtEmail_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtFirst_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtLast_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtMI_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}

		private void txtWebAddress_TextChanged(object sender, System.EventArgs e)
		{
			blnDirty = true;
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will go through each cell in the grid and create an
		// instance of the Control Information class to keep track of the data in it
		// ------------------------------------------------------------------
		private void FillControlInformationClassFromGrid()
		{
			int intRows;
			int intCols/*unused?*/;
			// Address
			for (intRows = 1; intRows <= fgrdAddress.Rows - 1; intRows++)
			{
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdAddress";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = AddressCommentCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Address";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdAddress";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = AddressSeasonalCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Address";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdAddress";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = AddressTypeCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Address";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdAddress";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = AddressModuleCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Address";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdAddress";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = AddressModuleEntityCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Address";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdAddress";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = AddressDateRangeCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Address";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdAddress";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = AddressOverrideNameCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Address";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdAddress";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = AddressAddressCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Address";
				intTotalNumberOfControls += 1;
			}
			// Comments
			for (intRows = 2; intRows <= fgrdComments.Rows - 1; intRows++)
			{
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdComments";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = CommentsEnteredByCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Comment";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdComments";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = CommentsModuleCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Comment";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdComments";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = CommentsCommentCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Comment";
				intTotalNumberOfControls += 1;
			}
			// Contacts
			for (intRows = 2; intRows <= fgrdContacts.Rows - 1; intRows++)
			{
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdContacts";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = ContactsNameCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Contact";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdContacts";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = ContactsDescriptionCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Contact";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdContacts";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = ContactsEmailCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Contact";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdContacts";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = ContactsPhoneCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Contact";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdContacts";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = ContactsCommentCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Contact";
				intTotalNumberOfControls += 1;
			}
			// Phone Numbers
			// Comments
			for (intRows = 2; intRows <= fgrdPhone.Rows - 1; intRows++)
			{
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdPhone";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = PhoneDescriptionCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Phone Number";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdPhone";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = PhonePhoneCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Phone Number";
				intTotalNumberOfControls += 1;
				Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
				clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
				clsControlInfo[intTotalNumberOfControls].ControlName = "fgrdPhone";
				clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.FlexGrid;
				clsControlInfo[intTotalNumberOfControls].GridRow = intRows;
				clsControlInfo[intTotalNumberOfControls].GridCol = PhoneExtCol;
				clsControlInfo[intTotalNumberOfControls].DataDescription = "Phone Number";
				intTotalNumberOfControls += 1;
			}
		}
		// Dave 12/14/2006---------------------------------------------------
		// This function will be used to set up all information about controls
		// on a form except multi cell grid
		// -----------------------------------------------------------------
		private void FillControlInformationClass()
		{
			intTotalNumberOfControls = 0;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "cboType";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.ComboBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Customer Type";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtCompanyName";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Company NAme";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtFirst";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "First Name";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtMI";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Middle Initial";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtLast";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Last Name";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtDesignation";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Designation";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtEmail";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "E-Mail Address";
			intTotalNumberOfControls += 1;
			Array.Resize(ref clsControlInfo, intTotalNumberOfControls + 1);
			clsControlInfo[intTotalNumberOfControls] = new clsAuditControlInformation();
			clsControlInfo[intTotalNumberOfControls].ControlName = "txtWebAddress";
			clsControlInfo[intTotalNumberOfControls].ControlType = clsAuditControlInformation.enuControlType.TextBox;
			clsControlInfo[intTotalNumberOfControls].DataDescription = "Web Address";
			intTotalNumberOfControls += 1;
		}

		private void FormatReferenceGrid()
		{
			gridReferences.Cols = 5;
			gridReferences.Rows = 1;
			gridReferences.ColHidden(ReferenceIDCol, true);
			gridReferences.ColDataType(ReferenceAltIdentifier, FCGrid.DataTypeSettings.flexDTString);
			gridReferences.ColDataType(ReferenceIdentifier, FCGrid.DataTypeSettings.flexDTString);
			gridReferences.TextMatrix(0, ReferenceDescription, "Description");
			gridReferences.TextMatrix(0, ReferenceIdentifier, "Identifier");
			gridReferences.TextMatrix(0, ReferenceAltIdentifier, "Alternate Identifier");
			gridReferences.TextMatrix(0, ReferenceModuleCol, "Module");
		}

		private void ResizeReferencesGrid()
		{
			int lngGridWidth;
			lngGridWidth = gridReferences.WidthOriginal;
			gridReferences.ColWidth(ReferenceModuleCol, FCConvert.ToInt32(lngGridWidth * 0.08));
			gridReferences.ColWidth(ReferenceIdentifier, FCConvert.ToInt32(lngGridWidth * 0.14));
			gridReferences.ColWidth(ReferenceAltIdentifier, FCConvert.ToInt32(lngGridWidth * 0.2));

		}

		private void FillReferencesGrid(int lngID)
		{
			FCCollection refColl = new FCCollection();
			gridReferences.Rows = 1;
			if (lngID > 0)
			{
				refColl = clsCheck.GetPartyReferences(lngID);
			}
			else
			{
				refColl = new FCCollection();
			}
			int lngX;
			int lngRow;
			cCentralPartyReference pRef;
			for (lngX = 1; lngX <= refColl.Count; lngX++)
			{
				gridReferences.Rows += 1;
				lngRow = gridReferences.Rows - 1;
				pRef = refColl[lngX];
				gridReferences.TextMatrix(lngRow, ReferenceIDCol, FCConvert.ToString(pRef.ID));
				gridReferences.TextMatrix(lngRow, ReferenceIdentifier, pRef.Identifier);
				gridReferences.TextMatrix(lngRow, ReferenceAltIdentifier, pRef.AlternateIdentifier);
				gridReferences.TextMatrix(lngRow, ReferenceDescription, pRef.Description);
				gridReferences.TextMatrix(lngRow, ReferenceModuleCol, pRef.ModuleCode);
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			this.mnuProcessSave_Click(sender, e);
		}

		private void cmdFileNew_Click(object sender, EventArgs e)
		{
			this.mnuFileNew_Click(sender, e);
		}

		private void cmdFileDelete_Click(object sender, EventArgs e)
		{
			this.mnuFileDelete_Click(sender, e);
		}

		private void cmdPrevious_Click(object sender, EventArgs e)
		{
			this.mnuPrevious_Click(sender, e);
		}

		private void cmdFileNext_Click(object sender, EventArgs e)
		{
			this.mnuFileNext_Click(sender, e);
		}

		private void cmdFileSave_Click(object sender, EventArgs e)
		{
			this.mnuFileSave_Click(sender, e);
		}

        private bool HasPrimary()
        {
            return getPrimaryRowNumber() > 0;
		}

        private int getPrimaryRowNumber()
        {
            int result = -1;

            for (int i = 1; i <= fgrdAddress.Rows - 1; i++)
            {
				if (fgrdAddress.TextMatrix(i, AddressTypeCol) == "Primary")
                {
                    result = i;
                    break;
                }
			}

            return result;
        }

        private bool ValidateAddressType()
        {
            bool result = true;

            if (cboAddressType.Text == "Primary")
            {
                if (!HasNoOtherPrimaryAddress())
                {
                    if (MessageBox.Show(
                            "There is already an address marked as primary.  This existing address will be changed to an override so your new address will be primary.  Do you wish to continue?",
                            "Convert to Override?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                        DialogResult.Yes)
                    {
                        fgrdAddress.TextMatrix(getPrimaryRowNumber(), AddressTypeCol, "Override");
                        blnDirty = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            else
            {
                if (!HasPrimary())
                {
                    if (MessageBox.Show(
                            "There is no primary address for this account.  This new address will be changed to be the Primary.  Do you wish to continue?",
                            "Set Primary?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                        DialogResult.Yes)
                    {
                        cboAddressType.Text = "Primary";
                    }
                    else
                    {
                        result = false;
                    }
                }
            }

            return result;
        }


        public class StaticVariables
        {
            public int lngReturnedID;
		}

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
            }
        }
    }
   
}
