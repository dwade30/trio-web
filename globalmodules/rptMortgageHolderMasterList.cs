﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using TWSharedLibrary;
#if TWCL0000
using TWCL0000;


#elif TWCR0000
using TWCR0000;


#elif TWBL0000
using TWBL0000;
using modGlobal = TWBL0000.modGlobalVariables;


#elif TWRE0000
using modGlobal = TWRE0000.modGlobalVariables;


#elif TWUT0000
using modGlobal = TWUT0000.modUTBilling;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class rptMortgageHolderMasterList : BaseSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/26/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/30/2004              *
		// ********************************************************
		string strSQL = "";
		clsDRWrapper rsData = new clsDRWrapper();
		int lngTotalMortgageHolders;

		public rptMortgageHolderMasterList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Mortgage Holder Master List";
            this.ReportEnd += RptMortgageHolderMasterList_ReportEnd;
		}

        private void RptMortgageHolderMasterList_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
        }

        public static rptMortgageHolderMasterList InstancePtr
		{
			get
			{
				return (rptMortgageHolderMasterList)Sys.GetInstance(typeof(rptMortgageHolderMasterList));
			}
		}

		protected rptMortgageHolderMasterList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}

            if (disposing)
            {
			    rsData.Dispose();	
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			BindFields();
			//Detail_Format();
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			//if (KeyCode == vbKeyEscape)
			//{
			//	Close();
			//}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "t");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, MDIParent.InstancePtr.GRID);
			//FC:FINAL:RPU:#i1167 - Moved the question before showing the report otherwise is not shown
			//frmQuestion.InstancePtr.Init(20, "By Mortgage Holder ID", "By Mortgage Holder Name", "Order Report By");
			if (modGlobal.Statics.gintPassQuestion == 0)
			{
				strSQL = "SELECT * FROM MortgageHolders ORDER BY ID";
			}
			else
			{
				strSQL = "SELECT * FROM MortgageHolders ORDER BY Name";
			}
			rsData.OpenRecordset(strSQL, "CentralData");
			lngTotalMortgageHolders = rsData.RecordCount();
			if (lngTotalMortgageHolders == 0)
			{
				// hide the headers if there are no records
				lblMHNumber.Visible = false;
				lblName.Visible = false;
				lblAddress.Visible = false;
			}
		}

		private void SetupFields()
		{
			// Set each field and label's visible property to True/False depending on which fields the user
			// has selected from the Custom Report screen then move them accordingly
			int intRow;
			float lngHt;
			intRow = 0;
			//lngHt = 270;
			lngHt = fldAddress1.Height;
			if (Strings.Trim(fldAddress1.Text) != "")
			{
				fldAddress1.Top = lngHt * intRow;
				intRow += 1;
			}
			else
			{
				fldAddress1.Top = 0;
			}
			if (Strings.Trim(fldAddress2.Text) != "")
			{
				fldAddress2.Top = lngHt * intRow;
				intRow += 1;
			}
			else
			{
				fldAddress2.Top = 0;
			}
			if (Strings.Trim(fldAddress3.Text) != "")
			{
				fldAddress3.Top = lngHt * intRow;
				intRow += 1;
			}
			else
			{
				fldAddress3.Top = 0;
			}
			if (Strings.Trim(fldAddress4.Text) != "")
			{
				fldAddress4.Top = lngHt * intRow;
				intRow += 1;
			}
			else
			{
				fldAddress4.Top = 0;
			}
			this.Detail.Height = intRow * lngHt + 0.15f;
			//+ 200;
		}

		private void BindFields()
		{
			// this will fill the information into the fields
			if (!rsData.EndOfFile())
			{
				fldMHNumber.Text = FCConvert.ToString(rsData.Get_Fields_Int32("ID"));
				fldName.Text = rsData.Get_Fields_String("Name");
				fldAddress1.Text = rsData.Get_Fields_String("Address1");
				fldAddress2.Text = rsData.Get_Fields_String("Address2");
				fldAddress3.Text = rsData.Get_Fields_String("Address3");
				fldAddress4.Text = FCConvert.ToString(rsData.Get_Fields_String("City")) + ", " + FCConvert.ToString(rsData.Get_Fields_String("State")) + " " + FCConvert.ToString(rsData.Get_Fields_String("Zip"));
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip4"))) != "")
				{
					fldAddress4.Text = fldAddress4.Text + "-" + FCConvert.ToString(rsData.Get_Fields_String("Zip4"));
				}
				//SetupFields();
				// move to the next record in the query
				rsData.MoveNext();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			//FC:FINAL:AM:#i56 - moved code to FetchData
			//BindFields();
			SetupFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			if (lngTotalMortgageHolders == 1)
			{
				lblFooter.Text = "There was " + FCConvert.ToString(lngTotalMortgageHolders) + " mortgage holder processed.";
			}
			else
			{
				lblFooter.Text = "There were " + FCConvert.ToString(lngTotalMortgageHolders) + " mortgage holders processed.";
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		private void rptMortgageHolderMasterList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMortgageHolderMasterList.Text	= "Mortgage Holder Master List";
			//rptMortgageHolderMasterList.Icon	= "rptMortgageHolderMasterList.dsx":0000";
			//rptMortgageHolderMasterList.Left	= 0;
			//rptMortgageHolderMasterList.Top	= 0;
			//rptMortgageHolderMasterList.Width	= 11880;
			//rptMortgageHolderMasterList.Height	= 8490;
			//rptMortgageHolderMasterList.StartUpPosition	= 3;
			//rptMortgageHolderMasterList.SectionData	= "rptMortgageHolderMasterList.dsx":058A;
			//End Unmaped Properties
		}
	}
}
