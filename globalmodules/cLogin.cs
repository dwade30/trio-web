﻿using fecherFoundation;
using fecherFoundation.Extensions;
using System;
using TWSharedLibrary;

namespace Global
{
    public class cLogin
    {
        //=========================================================
        public cSecurityUser GetUserByID(int lngUserID)
        {
            clsDRWrapper rs = new clsDRWrapper();

            try
            {
                rs.OpenRecordset("select * from users where ClientIdentifier = '" + StaticSettings.gGlobalSettings.ClientIdentifier + "' and id = " + FCConvert.ToString(lngUserID), "ClientSettings");
                cSecurityUser tUser = new cSecurityUser();
                if (!rs.EndOfFile())
                {
                    tUser.ID = FCConvert.ToInt32(rs.Get_Fields_Int32("ID"));
                    if (Information.IsDate(rs.Get_Fields("DateChanged")))
                    {
                        tUser.DateChanged = (DateTime)rs.Get_Fields_DateTime("DateChanged");
                    }
                    tUser.DefaultAdvancedSearch = FCConvert.ToBoolean(rs.Get_Fields_Boolean("DefaultAdvancedSearch"));
                    // TODO: Check the table for the column [Frequency] and replace with corresponding Get_Field method
                    tUser.Frequency = FCConvert.ToInt16(rs.Get_Fields("Frequency"));
                    tUser.OpID = FCConvert.ToString(rs.Get_Fields_String("opid"));
                    tUser.PasswordHash = FCConvert.ToString(rs.Get_Fields_String("Password"));
                    if (Information.IsDate(rs.Get_Fields("updatedate")))
                    {
                        tUser.UpdateDate = (DateTime)rs.Get_Fields_DateTime("updatedate");
                    }
                    // TODO: Check the table for the column [userid] and replace with corresponding Get_Field method
                    tUser.User = FCConvert.ToString(rs.Get_Fields("userid"));
                    tUser.UserName = FCConvert.ToString(rs.Get_Fields_String("username"));
                    tUser.UseSecurity = true;
                    return tUser;
                }
                else
                {
                    if (lngUserID == -1)
                    {
                        tUser = new cSecurityUser();
                        tUser.ID = -1;
                        tUser.User = "SuperUser";
                        tUser.UserName = "SuperUser";
                        tUser.UseSecurity = false;
                        return tUser;
                    }
                }
            }
            finally
            {
                rs.DisposeOf();
            }

            return null;
        }
    }
}
