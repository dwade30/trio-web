﻿namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	partial class rptUTbyHolder
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptUTbyHolder));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.txtaccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.txtMuniname = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTaxYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.txtNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMortgageName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReturnAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReturnAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReturnAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtReturnAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblReturnTo = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lnTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTotalDueTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.txtaccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMortgageName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReturnAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReturnAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReturnAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReturnAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnTo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDueTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			//
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtaccount,
				this.txtName,
				this.txtLocation,
				this.lblLocation,
				this.txtTotalDue
			});
			this.Detail.Height = 0.3541667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// txtaccount
			// 
			this.txtaccount.Height = 0.15625F;
			this.txtaccount.Left = 0F;
			this.txtaccount.MultiLine = false;
			this.txtaccount.Name = "txtaccount";
			this.txtaccount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtaccount.Text = null;
			this.txtaccount.Top = 0F;
			this.txtaccount.Width = 0.5416667F;
			// 
			// txtName
			// 
			this.txtName.Height = 0.1875F;
			this.txtName.Left = 0.5625F;
			this.txtName.MultiLine = false;
			this.txtName.Name = "txtName";
			this.txtName.Style = "font-family: \'Tahoma\'";
			this.txtName.Text = null;
			this.txtName.Top = 0F;
			this.txtName.Width = 3.4375F;
			// 
			// txtLocation
			// 
			this.txtLocation.Height = 0.1875F;
			this.txtLocation.Left = 1.1875F;
			this.txtLocation.MultiLine = false;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Style = "font-family: \'Tahoma\'";
			this.txtLocation.Text = null;
			this.txtLocation.Top = 0.1875F;
			this.txtLocation.Width = 2.8125F;
			// 
			// lblLocation
			// 
			this.lblLocation.Height = 0.1875F;
			this.lblLocation.HyperLink = null;
			this.lblLocation.Left = 0.5625F;
			this.lblLocation.MultiLine = false;
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Style = "font-family: \'Tahoma\'";
			this.lblLocation.Text = "Location:";
			this.lblLocation.Top = 0.1875F;
			this.lblLocation.Width = 0.625F;
			// 
			// txtTotalDue
			// 
			this.txtTotalDue.Height = 0.1875F;
			this.txtTotalDue.Left = 6.375F;
			this.txtTotalDue.MultiLine = false;
			this.txtTotalDue.Name = "txtTotalDue";
			this.txtTotalDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalDue.Text = null;
			this.txtTotalDue.Top = 0F;
			this.txtTotalDue.Visible = false;
			this.txtTotalDue.Width = 1.125F;
			// 
			// PageHeader
			//
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtMuniname,
				this.txtDate,
				this.txtTitle,
				this.txtPage,
				this.txtTime,
				this.lblTaxYear
			});
			this.PageHeader.Height = 0.5208333F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// txtMuniname
			// 
			this.txtMuniname.Height = 0.15625F;
			this.txtMuniname.Left = 0F;
			this.txtMuniname.MultiLine = false;
			this.txtMuniname.Name = "txtMuniname";
			this.txtMuniname.Style = "font-family: \'Tahoma\'";
			this.txtMuniname.Text = null;
			this.txtMuniname.Top = 0F;
			this.txtMuniname.Width = 1.5F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.15625F;
			this.txtDate.Left = 6.4375F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtDate.Text = null;
			this.txtDate.Top = 0F;
			this.txtDate.Width = 1.0625F;
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.2708333F;
			this.txtTitle.Left = 0F;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Utility Account List by Mortgage Holder";
			this.txtTitle.Top = 0F;
			this.txtTitle.Width = 7.5F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.15625F;
			this.txtPage.Left = 6.479167F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtPage.SummaryRunning = GrapeCity.ActiveReports.SectionReportModel.SummaryRunning.All;
			this.txtPage.SummaryType = GrapeCity.ActiveReports.SectionReportModel.SummaryType.PageCount;
			this.txtPage.Text = null;
			this.txtPage.Top = 0.15625F;
			this.txtPage.Width = 1.020833F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.15625F;
			this.txtTime.Left = 0F;
			this.txtTime.MultiLine = false;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'";
			this.txtTime.Text = null;
			this.txtTime.Top = 0.15625F;
			this.txtTime.Width = 1.5625F;
			// 
			// lblTaxYear
			// 
			this.lblTaxYear.Height = 0.15625F;
			this.lblTaxYear.Left = 0F;
			this.lblTaxYear.MultiLine = false;
			this.lblTaxYear.Name = "lblTaxYear";
			this.lblTaxYear.Style = "font-family: \'Tahoma\'; text-align: center";
			this.lblTaxYear.Text = null;
			this.lblTaxYear.Top = 0.1875F;
			this.lblTaxYear.Width = 7.5F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			//
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblAccount,
				this.lblName,
				this.txtNumber,
				this.txtMortgageName,
				this.txtAddress1,
				this.txtAddress2,
				this.txtAddress3,
				this.txtAddress4,
				this.txtReturnAddress1,
				this.txtReturnAddress2,
				this.txtReturnAddress3,
				this.txtReturnAddress4,
				this.lblReturnTo,
				this.lblTotalDue
			});
			this.GroupHeader1.DataField = "TheBinder";
			this.GroupHeader1.Height = 2.041667F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1770833F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.MultiLine = false;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 1.8125F;
			this.lblAccount.Width = 0.5416667F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1770833F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.5625F;
			this.lblName.MultiLine = false;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblName.Text = "Name";
			this.lblName.Top = 1.8125F;
			this.lblName.Width = 1.1875F;
			// 
			// txtNumber
			// 
			this.txtNumber.Height = 0.15625F;
			this.txtNumber.Left = 0.4375F;
			this.txtNumber.Name = "txtNumber";
			this.txtNumber.Style = "font-family: \'Tahoma\'";
			this.txtNumber.Text = "Field1";
			this.txtNumber.Top = 0.8125F;
			this.txtNumber.Width = 2.6875F;
			// 
			// txtMortgageName
			// 
			this.txtMortgageName.Height = 0.15625F;
			this.txtMortgageName.Left = 0.4375F;
			this.txtMortgageName.Name = "txtMortgageName";
			this.txtMortgageName.Style = "font-family: \'Tahoma\'";
			this.txtMortgageName.Text = "Field1";
			this.txtMortgageName.Top = 0.96875F;
			this.txtMortgageName.Width = 2.6875F;
			// 
			// txtAddress1
			// 
			this.txtAddress1.Height = 0.15625F;
			this.txtAddress1.Left = 0.4375F;
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Style = "font-family: \'Tahoma\'";
			this.txtAddress1.Text = "Field1";
			this.txtAddress1.Top = 1.125F;
			this.txtAddress1.Width = 2.6875F;
			// 
			// txtAddress2
			// 
			this.txtAddress2.Height = 0.15625F;
			this.txtAddress2.Left = 0.4375F;
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Style = "font-family: \'Tahoma\'";
			this.txtAddress2.Text = null;
			this.txtAddress2.Top = 1.28125F;
			this.txtAddress2.Width = 2.6875F;
			// 
			// txtAddress3
			// 
			this.txtAddress3.Height = 0.15625F;
			this.txtAddress3.Left = 0.4375F;
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Style = "font-family: \'Tahoma\'";
			this.txtAddress3.Text = "Field1";
			this.txtAddress3.Top = 1.4375F;
			this.txtAddress3.Width = 2.6875F;
			// 
			// txtAddress4
			// 
			this.txtAddress4.Height = 0.15625F;
			this.txtAddress4.Left = 0.4375F;
			this.txtAddress4.Name = "txtAddress4";
			this.txtAddress4.Style = "font-family: \'Tahoma\'";
			this.txtAddress4.Text = "Field2";
			this.txtAddress4.Top = 1.59375F;
			this.txtAddress4.Width = 2.6875F;
			// 
			// txtReturnAddress1
			// 
			this.txtReturnAddress1.Height = 0.15625F;
			this.txtReturnAddress1.Left = 4.75F;
			this.txtReturnAddress1.Name = "txtReturnAddress1";
			this.txtReturnAddress1.Style = "font-family: \'Tahoma\'";
			this.txtReturnAddress1.Text = null;
			this.txtReturnAddress1.Top = 0.1875F;
			this.txtReturnAddress1.Width = 2.6875F;
			// 
			// txtReturnAddress2
			// 
			this.txtReturnAddress2.Height = 0.15625F;
			this.txtReturnAddress2.Left = 4.75F;
			this.txtReturnAddress2.Name = "txtReturnAddress2";
			this.txtReturnAddress2.Style = "font-family: \'Tahoma\'";
			this.txtReturnAddress2.Text = null;
			this.txtReturnAddress2.Top = 0.34375F;
			this.txtReturnAddress2.Width = 2.6875F;
			// 
			// txtReturnAddress3
			// 
			this.txtReturnAddress3.Height = 0.15625F;
			this.txtReturnAddress3.Left = 4.75F;
			this.txtReturnAddress3.Name = "txtReturnAddress3";
			this.txtReturnAddress3.Style = "font-family: \'Tahoma\'";
			this.txtReturnAddress3.Text = null;
			this.txtReturnAddress3.Top = 0.5F;
			this.txtReturnAddress3.Width = 2.6875F;
			// 
			// txtReturnAddress4
			// 
			this.txtReturnAddress4.Height = 0.15625F;
			this.txtReturnAddress4.Left = 4.75F;
			this.txtReturnAddress4.Name = "txtReturnAddress4";
			this.txtReturnAddress4.Style = "font-family: \'Tahoma\'";
			this.txtReturnAddress4.Text = null;
			this.txtReturnAddress4.Top = 0.65625F;
			this.txtReturnAddress4.Width = 2.6875F;
			// 
			// lblReturnTo
			// 
			this.lblReturnTo.Height = 0.15625F;
			this.lblReturnTo.HyperLink = null;
			this.lblReturnTo.Left = 4.75F;
			this.lblReturnTo.Name = "lblReturnTo";
			this.lblReturnTo.Style = "font-family: \'Tahoma\'";
			this.lblReturnTo.Text = "Return to:";
			this.lblReturnTo.Top = 0.03125F;
			this.lblReturnTo.Width = 1F;
			// 
			// lblTotalDue
			// 
			this.lblTotalDue.Height = 0.4375F;
			this.lblTotalDue.HyperLink = null;
			this.lblTotalDue.Left = 6.375F;
			this.lblTotalDue.Name = "lblTotalDue";
			this.lblTotalDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTotalDue.Text = "Total Due";
			this.lblTotalDue.Top = 1.5625F;
			this.lblTotalDue.Width = 1.125F;
			// 
			// GroupFooter1
			//
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lnTotal,
				this.txtTotalDueTotal
			});
			this.GroupFooter1.Height = 0.375F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// lnTotal
			// 
			this.lnTotal.Height = 0F;
			this.lnTotal.Left = 6F;
			this.lnTotal.LineWeight = 1F;
			this.lnTotal.Name = "lnTotal";
			this.lnTotal.Top = 0.0625F;
			this.lnTotal.Width = 1.5F;
			this.lnTotal.X1 = 6F;
			this.lnTotal.X2 = 7.5F;
			this.lnTotal.Y1 = 0.0625F;
			this.lnTotal.Y2 = 0.0625F;
			// 
			// txtTotalDueTotal
			// 
			this.txtTotalDueTotal.Height = 0.1875F;
			this.txtTotalDueTotal.Left = 6.375F;
			this.txtTotalDueTotal.MultiLine = false;
			this.txtTotalDueTotal.Name = "txtTotalDueTotal";
			this.txtTotalDueTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.txtTotalDueTotal.Text = null;
			this.txtTotalDueTotal.Top = 0.125F;
			this.txtTotalDueTotal.Visible = false;
			this.txtTotalDueTotal.Width = 1.125F;
			// 
			// rptUTbyHolder
			//
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.txtaccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniname)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMortgageName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReturnAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReturnAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReturnAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReturnAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnTo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTotalDueTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtaccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniname;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblTaxYear;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMortgageName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReturnAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReturnAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReturnAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtReturnAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReturnTo;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTotalDueTotal;
	}
}
