﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using SharedApplication.Budgetary.Enums;

namespace Global
{
	public class cBDSecuritySetup
	{
		//=========================================================
		private string strThisModule = "";
		private FCCollection theCollection = new FCCollection();

		private cSecuritySetupItem CreateItem(string strParentFunction, string strChildFunction, int lngFunctionID, bool boolUseViewOnly, string strConstant)
		{
			cSecuritySetupItem CreateItem = null;
			cSecuritySetupItem tItem = new cSecuritySetupItem();
			tItem.ModuleName = strThisModule;
			tItem.ChildFunction = strChildFunction;
			tItem.ConstantName = strConstant;
			tItem.FunctionID = lngFunctionID;
			tItem.ParentFunction = strParentFunction;
			tItem.UseViewOnly = boolUseViewOnly;
			CreateItem = tItem;
			return CreateItem;
		}

		public FCCollection Setup
		{
			get
			{
				FCCollection Setup = null;
				Setup = theCollection;
				return Setup;
			}
		}

		public void AddItem_2(cSecuritySetupItem tItem)
		{
			AddItem(ref tItem);
		}

		public void AddItem(ref cSecuritySetupItem tItem)
		{
			if (!(tItem == null))
			{
				if (!(theCollection == null))
				{
					theCollection.Add(tItem);
				}
			}
		}

		public cBDSecuritySetup() : base()
		{
			strThisModule = "BD";
			FillSetup();
		}

		private void FillSetup()
		{
			AddItem_2(CreateItem(" Enter Budgetary", "", (int)BudgetarySecurityItems.ProgramAccess, false, ""));
			AddItem_2(CreateItem("Journals", "", (int)BudgetarySecurityItems.Journals, true, ""));
			AddItem_2(CreateItem("Journals", "Cash Disbursements", (int)BudgetarySecurityItems.CashDisbursements, false, ""));
			AddItem_2(CreateItem("Journals", "Cash Receipts", (int)BudgetarySecurityItems.CashReceipts, false, ""));
			AddItem_2(CreateItem("Journals", "General Journal", (int)BudgetarySecurityItems.GeneralJournal, false, ""));
			AddItem_2(CreateItem("Journals", "Create Reversing Journal", (int)BudgetarySecurityItems.CreateReversingEntries, false, ""));
			AddItem_2(CreateItem("Journals", "Edit Journal Descriptions", (int)BudgetarySecurityItems.EditJournalDescriptions, false, ""));
			AddItem_2(CreateItem("Journals", "Delete Unposted Journal", (int)BudgetarySecurityItems.DeleteUnpostedJournal, false, ""));
			AddItem_2(CreateItem("Journals", "Posting", (int)BudgetarySecurityItems.Posting, false, ""));
			AddItem_2(CreateItem("Payables", "", (int)BudgetarySecurityItems.Payables, true, ""));
			AddItem_2(CreateItem("Payables", "Invoice Entry", (int)BudgetarySecurityItems.AccountsPayable, false, ""));
			AddItem_2(CreateItem("Payables", "AP Processing", (int)BudgetarySecurityItems.AccountsPayableProcessing, false, ""));
			AddItem_2(CreateItem("Payables", "AP Corrections", (int)BudgetarySecurityItems.AccountsPayableCorrections, false, ""));
			AddItem_2(CreateItem("Payables", "AP Overspend", (int)BudgetarySecurityItems.AccountsPayableOverSpend, false, ""));
			AddItem_2(CreateItem("Payables", "Credit Memo Corrections", (int)BudgetarySecurityItems.CreditMemoCorrection, false, ""));
			AddItem_2(CreateItem("Payables", "Credit Memos", (int)BudgetarySecurityItems.CreditMemo, false, ""));
			AddItem_2(CreateItem("Payables", "Encumbrances", (int)BudgetarySecurityItems.Encumbrances, false, ""));
			AddItem_2(CreateItem("Payables", "Purchase Orders", (int)BudgetarySecurityItems.PurchaseOrders, false, ""));
			AddItem_2(CreateItem("Payables", "Encumbrance Corrections", (int)BudgetarySecurityItems.EncumbranceCorrections, false, ""));
			AddItem_2(CreateItem("Vendors", "", (int)BudgetarySecurityItems.Vendors, true, ""));
			AddItem_2(CreateItem("Vendors", "Class Codes Table", (int)BudgetarySecurityItems.ClassCodes, false, ""));
			AddItem_2(CreateItem("Vendors", "Vendor Maintenance", (int)BudgetarySecurityItems.VendorMaintenance, false, ""));
			AddItem_2(CreateItem("Bank Reconciliation", "", (int)BudgetarySecurityItems.BankReconcilliation, true, ""));
			AddItem_2(CreateItem("Bank Reconciliation", "Add Check or Deposit", (int)BudgetarySecurityItems.AddCheckOrDeposit, false, ""));
			AddItem_2(CreateItem("Bank Reconciliation", "Flag Cashed", (int)BudgetarySecurityItems.FlagCashed, false, ""));
			AddItem_2(CreateItem("Bank Reconciliation", "Purge Checks / Reset Balance", (int)BudgetarySecurityItems.PurgeChecks, false, ""));
			AddItem_2(CreateItem("Bank Reconciliation", "Update Status Check / Dep", (int)BudgetarySecurityItems.UpdateStatusCheckOrDeposit, false, ""));
			AddItem_2(CreateItem("Accounts", "", (int)BudgetarySecurityItems.Accounts, true, ""));
			AddItem_2(CreateItem("Accounts", "Account Setup", (int)BudgetarySecurityItems.AccountSetup, false, ""));
			AddItem_2(CreateItem("Accounts", "Add/Update Valid Accounts", (int)BudgetarySecurityItems.ValidAccounts, false, ""));
			AddItem_2(CreateItem("Accounts", "G/L Ctrl/STand Accounts", (int)BudgetarySecurityItems.DefaultAccounts, false, ""));
			AddItem_2(CreateItem("Accounts", "G/L Subtotal Ranges", (int)BudgetarySecurityItems.LedgerRanges, false, ""));
			AddItem_2(CreateItem("Budget Process", "", (int)BudgetarySecurityItems.BudgetProcess, true, ""));
			AddItem_2(CreateItem("Budget Process", "Budget Breakdown", (int)BudgetarySecurityItems.BudgetBreakdown, false, ""));
			AddItem_2(CreateItem("Budget Process", "Clear Budget Information", (int)BudgetarySecurityItems.ClearBudget, false, ""));
			AddItem_2(CreateItem("Budget Process", "Load Budget Info", (int)BudgetarySecurityItems.LoadBudget, false, ""));
			AddItem_2(CreateItem("Budget Process", "Transfer Approved to Budget", (int)BudgetarySecurityItems.TransferApprovedToBudget, false, ""));
			AddItem_2(CreateItem("End of Year", "", (int)BudgetarySecurityItems.EndofYearMenu, true, ""));
			AddItem_2(CreateItem("End of Year", "Fiscal End of Year", (int)BudgetarySecurityItems.EndOfYear, false, ""));
			AddItem_2(CreateItem("End of Year", "Create Opening Adjustments", (int)BudgetarySecurityItems.CreateOpeningAdjustments, false, ""));
			AddItem_2(CreateItem("End of Year", "1099 Processing", (int)BudgetarySecurityItems.TaxProcessing, true, ""));
			AddItem_2(CreateItem("End of Year", "Clear 1099 Adjustments", (int)BudgetarySecurityItems.Clear1099Adjustments, false, ""));
			AddItem_2(CreateItem("End of Year", "Restore Archive Extract File", (int)BudgetarySecurityItems.RestoreArchive, false, ""));
			AddItem_2(CreateItem("Projects", "", (int)BudgetarySecurityItems.Projects, false, ""));
			AddItem_2(CreateItem("Printing", "", (int)BudgetarySecurityItems.Printing, true, ""));
			AddItem_2(CreateItem("Printing", "Report Writer", (int)BudgetarySecurityItems.ReportWriter, false, ""));
			AddItem_2(CreateItem("Printing", "Custom Report Setup", (int)BudgetarySecurityItems.CustomReportSetup, false, ""));
			AddItem_2(CreateItem("File Maintenance", "", (int)BudgetarySecurityItems.FileMaintenance, true, ""));
			AddItem_2(CreateItem("File Maintenance", "Customize", (int)BudgetarySecurityItems.Customize, false, ""));
			AddItem_2(CreateItem("File Maintenance", "Warrant Message", (int)BudgetarySecurityItems.WarrantMessage, false, ""));
		}
	}
}
