﻿//Fecher vbPorter - Version 1.0.0.59
using Global;

namespace TWXF0000
{
	public class cRECard
	{
		//=========================================================
		private int lngAccountNumber;
		private int lngRecordID;
		private int lngParentRecordID;
		private short intCardNumber;
		private string strAccountID = "";
		private string strCardID = "";
		private cREValuationItem viCurrentValues = new cREValuationItem();
		private cREValuationItem viBillingValues = new cREValuationItem();
		private cREValuationItem viCalculatedValues = new cREValuationItem();
		private cGenericCollection collLand = new cGenericCollection();
		private cGenericCollection collOutbuildings = new cGenericCollection();
		private cREDwelling dwellRecord;
		private cRECommercial commercialRecord;
		private bool boolUpdated;
		private bool boolDeleted;

		public cREValuationItem CurrentValues
		{
			get
			{
				cREValuationItem CurrentValues = null;
				CurrentValues = viCurrentValues;
				return CurrentValues;
			}
		}

		public cREValuationItem BillingValues
		{
			get
			{
				cREValuationItem BillingValues = null;
				BillingValues = viBillingValues;
				return BillingValues;
			}
		}

		public cREValuationItem CalculatedValues
		{
			get
			{
				cREValuationItem CalculatedValues = null;
				CalculatedValues = viCalculatedValues;
				return CalculatedValues;
			}
		}

		public cGenericCollection Land
		{
			get
			{
				cGenericCollection Land = null;
				Land = collLand;
				return Land;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}
	}
}
