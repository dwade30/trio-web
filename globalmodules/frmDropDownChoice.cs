﻿//Fecher vbPorter - Version 1.0.0.59
using System;
using Wisej.Web;
using Microsoft.VisualBasic;
using fecherFoundation;
#if TWPY0000
using TWPY0000;
#endif

namespace Global
{
	public partial class frmDropDownChoice : BaseForm
	{
		public frmDropDownChoice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null )
				_InstancePtr = this;
		}

		/// <summary>
		/// default instance for form
		/// </summary>
		public static frmDropDownChoice InstancePtr
		{
			get
			{
				return (frmDropDownChoice)Sys.GetInstance(typeof(frmDropDownChoice));
			}
		}
		protected static frmDropDownChoice _InstancePtr = null;


		//=========================================================
		private int lngReturn;
		// returning -1 indicates no choice was made
		public int GetChoiceByData(string strChoices, int lngDefaultData, string strTitle, string strMessage)
		{
			int GetChoiceByData = 0;
			Setup(strTitle, strMessage, strChoices, lngDefaultData);
			if (cmbChoices.Items.Count > 0)
			{
				this.Show(FCForm.FormShowEnum.Modal);
			}
			GetChoiceByData = lngReturn;
			return GetChoiceByData;
		}

		public int GetChoiceByItemNumber(string strChoices, short intDefaultItemNumber, string strTitle, string strMessage)
		{
			int GetChoiceByItemNumber = 0;
			Setup(strTitle, strMessage, strChoices, intDefaultItemNumber);
			if (cmbChoices.Items.Count > 0)
			{
				this.Show(FCForm.FormShowEnum.Modal);
			}
			GetChoiceByItemNumber = lngReturn;
			return GetChoiceByItemNumber;
		}

		private object ParseChoices(string strChoices)
		{
			object ParseChoices = null;
			if (strChoices != "")
			{
				string[] ary = null;
				ary = Strings.Split(strChoices, "|", -1, CompareConstants.vbBinaryCompare);
				// vbPorter upgrade warning: x As short	OnWriteFCConvert.ToInt32(
				short x;
				for (x = 0; x <= (Information.UBound(ary, 1)); x++)
				{
					ParseItem(ary[x]);
				}
			}
			return ParseChoices;
		}

		private object ParseItem(string strItem)
		{
			object ParseItem = null;
			if (strItem != "")
			{
				string[] ary = null;
				ary = Strings.Split(strItem, ";", -1, CompareConstants.vbBinaryCompare);
				if (Information.UBound(ary, 1) > 0)
				{
					cmbChoices.AddItem(fecherFoundation.Strings.Trim(ary[1]));
					cmbChoices.ItemData(cmbChoices.NewIndex, FCConvert.ToInt32(Conversion.Val(ary[0])));
				}
				else
				{
					cmbChoices.AddItem(fecherFoundation.Strings.Trim(ary[0]));
					cmbChoices.ItemData(cmbChoices.NewIndex, cmbChoices.Items.Count);
				}
			}
			return ParseItem;
		}
		// vbPorter upgrade warning: strTitle As object	OnWrite(string)
		private void Setup(object strTitle, string strMessage, string strChoices, int lngDefaultValue)
		{
			lngReturn = -1;
			this.Text = FCConvert.ToString(strTitle);
			lblMessage.Text = strMessage;
			ParseChoices(strChoices);
			if (cmbChoices.Items.Count > 1)
			{
				short x;
				for (x = 0; x <= cmbChoices.Items.Count - 1; x++)
				{
					if (cmbChoices.ItemData(x) == lngDefaultValue)
					{
						cmbChoices.SelectedIndex = x;
						break;
					}
				}
			}
			else
			{
				cmbChoices.SelectedIndex = 0;
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			if (cmbChoices.SelectedIndex < 0)
			{
				MessageBox.Show("No selection was made", "No Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			lngReturn = cmbChoices.ItemData(cmbChoices.SelectedIndex);
			Close();
		}

		private void frmDropDownChoice_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDropDownChoice properties;
			//frmDropDownChoice.ScaleWidth	= 5850;
			//frmDropDownChoice.ScaleHeight	= 3405;
			//frmDropDownChoice.LinkTopic	= "Form1";
			//End Unmaped Properties
		}
	}
}
