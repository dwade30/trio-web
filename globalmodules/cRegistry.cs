﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using System.Text;
using System.Runtime.InteropServices;
using TWSharedLibrary;

namespace Global
{
	public class cRegistry
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :                                       *
		// Date           :                                       *
		// *
		// MODIFIED BY    :                                       *
		// Last Updated   :               11/17/2002              *
		// ********************************************************
		// Registry Specific Access Rights
		const short ID_QUERY_VALUE = 0x1;
		const int ID_SET_VALUE = 0x2;
		const int ID_CREATE_SUB_ID = 0x4;
		const short ID_ENUMERATE_SUB_KEYS = 0x8;
		const int ID_NOTIFY = 0x10;
		const int ID_CREATE_LINK = 0x20;
		const short ID_ALL_ACCESS = 0x3F;
		// Open/Create Options
		const short REG_OPTION_NON_VOLATILE = 0;
		const int REG_OPTION_VOLATILE = 0x1;
		// ID creation/open disposition
		const int REG_CREATED_NEW_ID = 0x1;
		const int REG_OPENED_EXISTING_ID = 0x2;
		// masks for the predefined standard access types
		const uint STANDARD_RIGHTS_ALL = 0x1F0000;
		const uint SPECIFIC_RIGHTS_ALL = 0xFFFF;
		// Define severity codes
		const int ERROR_SUCCESS = 0;
		const int ERROR_ACCESS_DENIED = 5;
		const int ERROR_INVALID_DATA = 13;
		const int ERROR_MORE_DATA = 234;
		// dderror
		const int ERROR_NO_MORE_ITEMS = 259;
		// Structures Needed For Registry Prototypes
		[StructLayout(LayoutKind.Sequential)]
		private struct SECURITY_ATTRIBUTES
		{
			public int nLength;
			public int lpSecurityDescriptor;
			public bool bInheritHandle;
		};

		private struct FILETIME
		{
			public int dwLowDateTime;
			public int dwHighDateTime;
		};
		// Registry Function Prototypes
		//VBtoInfo: 0/2
		[DllImport("advapi32", EntryPoint = "RegOpenKeyExA")]
		private static extern int RegOpenKeyEx(int hKey, IntPtr lpSubKey, int ulOptions, int samDesired, ref int phkResult);
		//VBtoInfo: 1
		private int RegOpenKeyExWrp(int hKey, ref string lpSubKey, short ulOptions, short samDesired, ref int phkResult)
		{
			int ret;
			IntPtr plpSubKey = FCUtils.GetByteFromString(lpSubKey);
			ret = RegOpenKeyEx(hKey, plpSubKey, ulOptions, samDesired, ref phkResult);
			FCUtils.GetStringFromByte(ref lpSubKey, plpSubKey);
			return ret;
		}
		//VBtoInfo: 0/2
		[DllImport("advapi32", EntryPoint = "RegSetValueExA")]
		private static extern int RegSetValueExStr(int hKey, IntPtr lpValueName, int Reserved, int dwType, IntPtr szData, int cbData);
		//VBtoInfo: 1
		private int RegSetValueExStrWrp(int hKey, ref string lpValueName, short Reserved, int dwType, ref string szData, int cbData)
		{
			int ret;
			IntPtr plpValueName = FCUtils.GetByteFromString(lpValueName);
			IntPtr pszData = FCUtils.GetByteFromString(szData);
			ret = RegSetValueExStr(hKey, plpValueName, Reserved, dwType, pszData, cbData);
			FCUtils.GetStringFromByte(ref lpValueName, plpValueName);
			FCUtils.GetStringFromByte(ref szData, pszData);
			return ret;
		}
		//VBtoInfo: 0/2
		[DllImport("advapi32", EntryPoint = "RegSetValueExA")]
		private static extern int RegSetValueExLong(int hKey, IntPtr lpValueName, int Reserved, int dwType, ref int szData, int cbData);
		//VBtoInfo: 1
		private int RegSetValueExLongWrp(int hKey, ref string lpValueName, short Reserved, int dwType, ref int szData, short cbData)
		{
			int ret;
			IntPtr plpValueName = FCUtils.GetByteFromString(lpValueName);
			ret = RegSetValueExLong(hKey, plpValueName, Reserved, dwType, ref szData, cbData);
			FCUtils.GetStringFromByte(ref lpValueName, plpValueName);
			return ret;
		}
		//VBtoInfo: 0/2
		[DllImport("advapi32", EntryPoint = "RegSetValueExA")]
		private static extern int RegSetValueExByte(int hKey, IntPtr lpValueName, int Reserved, int dwType, ref byte szData, int cbData);
		//VBtoInfo: 1
		private int RegSetValueExByteWrp(int hKey, ref string lpValueName, short Reserved, int dwType, ref byte szData, int cbData)
		{
			int ret;
			IntPtr plpValueName = FCUtils.GetByteFromString(lpValueName);
			ret = RegSetValueExByte(hKey, plpValueName, Reserved, dwType, ref szData, cbData);
			FCUtils.GetStringFromByte(ref lpValueName, plpValueName);
			return ret;
		}

		[DllImport("advapi32")]
		private static extern int RegCloseKey(int hKey);
		//VBtoInfo: 0/2
		[DllImport("advapi32", EntryPoint = "RegQueryValueExA")]
		private static extern int RegQueryValueExStr(int hKey, IntPtr lpValueName, int lpReserved, ref int lpType, IntPtr szData, ref int lpcbData);
		//VBtoInfo: 1
		private int RegQueryValueExStrWrp(int hKey, ref string lpValueName, short lpReserved, ref int lpType, ref string szData, ref int lpcbData)
		{
			int ret;
			IntPtr plpValueName = FCUtils.GetByteFromString(lpValueName);
			IntPtr pszData = FCUtils.GetByteFromString(szData);
			ret = RegQueryValueExStr(hKey, plpValueName, lpReserved, ref lpType, pszData, ref lpcbData);
			FCUtils.GetStringFromByte(ref lpValueName, plpValueName);
			FCUtils.GetStringFromByte(ref szData, pszData);
			return ret;
		}
		//VBtoInfo: 0/2
		[DllImport("advapi32", EntryPoint = "RegQueryValueExA")]
		private static extern int RegQueryValueExLong(int hKey, IntPtr lpValueName, int lpReserved, ref int lpType, ref short szData, ref int lpcbData);
		//VBtoInfo: 1
		private int RegQueryValueExLongWrp(int hKey, ref string lpValueName, short lpReserved, ref int lpType, short szData, ref int lpcbData)
		{
			int ret;
			IntPtr plpValueName = FCUtils.GetByteFromString(lpValueName);
			ret = RegQueryValueExLong(hKey, plpValueName, lpReserved, ref lpType, ref szData, ref lpcbData);
			FCUtils.GetStringFromByte(ref lpValueName, plpValueName);
			return ret;
		}
		//VBtoInfo: 0/2
		[DllImport("advapi32", EntryPoint = "RegQueryValueExA")]
		private static extern int RegQueryValueExByte(int hKey, IntPtr lpValueName, int lpReserved, ref int lpType, ref byte szData, ref int lpcbData);
		//VBtoInfo: 1
		private int RegQueryValueExByteWrp(int hKey, ref string lpValueName, short lpReserved, ref int lpType, ref byte szData, ref int lpcbData)
		{
			int ret;
			IntPtr plpValueName = FCUtils.GetByteFromString(lpValueName);
			ret = RegQueryValueExByte(hKey, plpValueName, lpReserved, ref lpType, ref szData, ref lpcbData);
			FCUtils.GetStringFromByte(ref lpValueName, plpValueName);
			return ret;
		}
		//VBtoInfo: 0/2
		[DllImport("advapi32", EntryPoint = "RegCreateKeyExA")]
		private static extern int RegCreateKeyEx(int hKey, IntPtr lpSubKey, int Reserved, string lpClass, int dwOptions, int samDesired, ref SECURITY_ATTRIBUTES lpSecurityAttributes, ref int phkResult, ref int lpdwDisposition);
		//VBtoInfo: 1
		private int RegCreateKeyExWrp(int hKey, ref string lpSubKey, short Reserved, string lpClass, short dwOptions, short samDesired, ref SECURITY_ATTRIBUTES lpSecurityAttributes, ref int phkResult, ref int lpdwDisposition)
		{
			int ret;
			IntPtr plpSubKey = FCUtils.GetByteFromString(lpSubKey);
			ret = RegCreateKeyEx(hKey, plpSubKey, Reserved, lpClass, dwOptions, samDesired, ref lpSecurityAttributes, ref phkResult, ref lpdwDisposition);
			FCUtils.GetStringFromByte(ref lpSubKey, plpSubKey);
			return ret;
		}

		[DllImport("advapi32.dll", EntryPoint = "RegEnumKeyExA")]
		private static extern int RegEnumKeyEx(int hKey, int dwIndex, string lpName, ref int lpcbName, int lpReserved, string lpClass, ref int lpcbClass, ref FILETIME lpftLastWriteTime);
		//VBtoInfo: 0/2
		[DllImport("advapi32.dll", EntryPoint = "RegEnumKeyA")]
		private static extern int RegEnumKey(int hKey, int dwIndex, IntPtr lpName, int cbName);
		//VBtoInfo: 1
		private int RegEnumKeyWrp(int hKey, int dwIndex, ref string lpName, int cbName)
		{
			int ret;
			IntPtr plpName = FCUtils.GetByteFromString(lpName);
			ret = RegEnumKey(hKey, dwIndex, plpName, cbName);
			FCUtils.GetStringFromByte(ref lpName, plpName);
			return ret;
		}
		//VBtoInfo: 0/2
		[DllImport("advapi32.dll", EntryPoint = "RegEnumValueA")]
		private static extern int RegEnumValue(int hKey, int dwIndex, IntPtr lpValueName, ref int lpcbValueName, int lpReserved, int lpType, int lpData, int lpcbData);
		//VBtoInfo: 1
		private int RegEnumValueWrp(int hKey, int dwIndex, ref string lpValueName, ref int lpcbValueName, short lpReserved, short lpType, short lpData, short lpcbData)
		{
			int ret;
			IntPtr plpValueName = FCUtils.GetByteFromString(lpValueName);
			ret = RegEnumValue(hKey, dwIndex, plpValueName, ref lpcbValueName, lpReserved, lpType, lpData, lpcbData);
			FCUtils.GetStringFromByte(ref lpValueName, plpValueName);
			return ret;
		}

		[DllImport("advapi32.dll", EntryPoint = "RegEnumValueA")]
		private static extern int RegEnumValueLong(int hKey, int dwIndex, string lpValueName, ref int lpcbValueName, int lpReserved, ref int lpType, ref int lpData, ref int lpcbData);

		[DllImport("advapi32.dll", EntryPoint = "RegEnumValueA")]
		private static extern int RegEnumValueStr(int hKey, int dwIndex, string lpValueName, ref int lpcbValueName, int lpReserved, ref int lpType, string lpData, ref int lpcbData);

		[DllImport("advapi32.dll", EntryPoint = "RegEnumValueA")]
		private static extern int RegEnumValueByte(int hKey, int dwIndex, string lpValueName, ref int lpcbValueName, int lpReserved, ref int lpType, ref byte lpData, ref int lpcbData);

		[DllImport("advapi32.dll", EntryPoint = "RegQueryInfoKeyA")]
		private static extern int RegQueryInfoKey(int hKey, string lpClass, ref int lpcbClass, int lpReserved, ref int lpcSubKeys, ref int lpcbMaxSubKeyLen, ref int lpcbMaxClassLen, ref int lpcValues, ref int lpcbMaxValueNameLen, ref int lpcbMaxValueLen, ref int lpcbSecurityDescriptor, ref double lpftLastWriteTime);
		//VBtoInfo: 0/2
		[DllImport("advapi32.dll", EntryPoint = "RegDeleteKeyA")]
		private static extern int RegDeleteKey(int hKey, IntPtr lpSubKey);
		//VBtoInfo: 1
		private int RegDeleteKeyWrp(int hKey, ref string lpSubKey)
		{
			int ret;
			IntPtr plpSubKey = FCUtils.GetByteFromString(lpSubKey);
			ret = RegDeleteKey(hKey, plpSubKey);
			FCUtils.GetStringFromByte(ref lpSubKey, plpSubKey);
			return ret;
		}
		//VBtoInfo: 0/2
		[DllImport("advapi32.dll", EntryPoint = "RegDeleteValueA")]
		private static extern int RegDeleteValue(int hKey, IntPtr lpValueName);
		//VBtoInfo: 1
		private int RegDeleteValueWrp(int hKey, ref string lpValueName)
		{
			int ret;
			IntPtr plpValueName = FCUtils.GetByteFromString(lpValueName);
			ret = RegDeleteValue(hKey, plpValueName);
			FCUtils.GetStringFromByte(ref lpValueName, plpValueName);
			return ret;
		}
		// Other declares:
		//VBtoInfo: 0/3
		[DllImport("kernel32", EntryPoint = "RtlMoveMemory")]
		private static extern void CopyMemory(int lpvDest, int lpvSource, int cbCopy);
		//VBtoInfo: 1/3
		[DllImport("kernel32", EntryPoint = "RtlMoveMemory")]
		private static extern void CopyMemory(ref short lpvDest, ref short lpvSource, int cbCopy);
		//VBtoInfo: 2/3
		[DllImport("kernel32", EntryPoint = "RtlMoveMemory")]
		private static extern void CopyMemory(ref int lpvDest, ref short lpvSource, int cbCopy);
		//VBtoInfo: 0/2
		[DllImport("kernel32", EntryPoint = "ExpandEnvironmentStringsA")]
		private static extern int ExpandEnvironmentStrings(IntPtr lpSrc, IntPtr lpDst, int nSize);
		//VBtoInfo: 1
		private int ExpandEnvironmentStringsWrp(ref string lpSrc, ref string lpDst, int nSize)
		{
			int ret;
			IntPtr plpSrc = FCUtils.GetByteFromString(lpSrc);
			IntPtr plpDst = FCUtils.GetByteFromString(lpDst);
			ret = ExpandEnvironmentStrings(plpSrc, plpDst, nSize);
			FCUtils.GetStringFromByte(ref lpSrc, plpSrc);
			FCUtils.GetStringFromByte(ref lpDst, plpDst);
			return ret;
		}

		public enum ERegistryClassConstants : uint
		{
			HKEY_CLASSES_ROOT = 0x80000000,
			// HKEY_CURRENT_USER = &H80000001
			// HKEY_CURRENT_USER = &H80000002
			HKEY_USERS = 0x80000003,
		}

		public enum ERegistryValueTypes
		{
			// Predefined Value Types
			REG_NONE = (0),
			// No value type
			REG_SZ = (1),
			// Unicode nul terminated string
			REG_EXPAND_SZ = (2),
			// Unicode nul terminated string w/enviornment var
			REG_BINARY = (3),
			// Free form binary
			REG_DWORD = (4),
			// 32-bit number
			REG_DWORD_LITTLE_ENDIAN = (4),
			// 32-bit number (same as REG_DWORD)
			REG_DWORD_BIG_ENDIAN = (5),
			// 32-bit number
			REG_LINK = (6),
			// Symbolic Link (unicode)
			REG_MULTI_SZ = (7),
			// Multiple Unicode strings
			REG_RESOURCE_LIST = (8),
			// Resource list in the resource map
			REG_FULL_RESOURCE_DESCRIPTOR = (9),
			// Resource list in the hardware description
			REG_RESOURCE_REQUIREMENTS_LIST = (10),
		}
		// VBto upgrade warning: m_hClassKey As int	OnWrite(ERegistryClassConstants)
		private int m_hClassKey;
		private string m_sSectionKey = "";
		private string m_sValueKey = "";
		private object m_vValue;
		private string m_sSetValue = "";
		private object m_vDefault;
		private ERegistryValueTypes m_eValueType;

		public bool KeyExists
		{
			get
			{
				bool KeyExists = false;
				// KeyExists = bCheckKeyExists(
				// m_hClassKey,
				// m_sSectionKey
				// )
				int hKey = 0;
				if (RegOpenKeyExWrp(m_hClassKey, ref m_sSectionKey, 0, 1, ref hKey) == ERROR_SUCCESS)
				{
					KeyExists = true;
					RegCloseKey(hKey);
				}
				else
				{
					KeyExists = false;
				}
				return KeyExists;
			}
		}

		public bool CreateKey()
		{
			bool CreateKey = false;
			SECURITY_ATTRIBUTES tSA = new SECURITY_ATTRIBUTES();
			int hKey = 0;
			int lCreate = 0;
			int e;
			// Open or Create the ID
			e = RegCreateKeyExWrp(m_hClassKey, ref m_sSectionKey, 0, "", REG_OPTION_NON_VOLATILE, ID_ALL_ACCESS, ref tSA, ref hKey, ref lCreate);
			if (FCConvert.ToBoolean(e))
			{
				Information.Err().Raise(26001, null, "Failed to create registry ID: '" + m_sSectionKey, null, null);
			}
			else
			{
				CreateKey = (e == ERROR_SUCCESS);
				// Close the ID
				RegCloseKey(hKey);
			}
			return CreateKey;
		}

		public bool DeleteKey()
		{
			bool DeleteKey = false;
			int e;
			e = RegDeleteKeyWrp(m_hClassKey, ref m_sSectionKey);
			if (FCConvert.ToBoolean(e))
			{
				Information.Err().Raise(26001, null, "Failed to delete registry ID: '" + FCConvert.ToString(m_hClassKey) + "',Section: '" + m_sSectionKey, null, null);
			}
			else
			{
				DeleteKey = (e == ERROR_SUCCESS);
			}
			return DeleteKey;
		}

		public bool DeleteValue()
		{
			bool DeleteValue = false;
			int e;
			int hKey = 0;
			e = RegOpenKeyExWrp(m_hClassKey, ref m_sSectionKey, 0, ID_ALL_ACCESS, ref hKey);
			if (FCConvert.ToBoolean(e))
			{
				Information.Err().Raise(26001, null, "Failed to open ID '" + FCConvert.ToString(m_hClassKey) + "',Section: '" + m_sSectionKey + "' for delete access", null, null);
			}
			else
			{
				e = RegDeleteValueWrp(hKey, ref m_sValueKey);
				if (FCConvert.ToBoolean(e))
				{
					Information.Err().Raise(26001, null, "Failed to delete registry ID: '" + FCConvert.ToString(m_hClassKey) + "',Section: '" + m_sSectionKey + "',ID: '" + m_sValueKey, null, null);
				}
				else
				{
					DeleteValue = (e == ERROR_SUCCESS);
				}
			}
			return DeleteValue;
		}

		public object Value
		{
			get
			{
				object Value = null;
				// VBto upgrade warning: vValue As object	OnWrite(int, string, byte())
				object vValue = null;
				// VBto upgrade warning: ordType As int	OnRead(ERegistryValueTypes)
				int cData = 0;
				string sData = "";
				int ordType = 0, e;
				int hKey = 0;
				e = RegOpenKeyExWrp(m_hClassKey, ref m_sSectionKey, 0, ID_QUERY_VALUE, ref hKey);
				// ApiRaiseIf�e
				e = RegQueryValueExLongWrp(hKey, ref m_sValueKey, 0, ref ordType, 0, ref cData);
				if (FCConvert.ToBoolean(e) & e != ERROR_MORE_DATA)
				{
					Value = m_vDefault;
					return Value;
				}
				m_eValueType = (ERegistryValueTypes)ordType;
				if (ordType == FCConvert.ToInt32(ERegistryValueTypes.REG_DWORD) || ordType == FCConvert.ToInt32(ERegistryValueTypes.REG_DWORD_LITTLE_ENDIAN))
				{
					short iData = 0;
					e = RegQueryValueExLongWrp(hKey, ref m_sValueKey, 0, ref ordType, iData, ref cData);
					vValue = iData;
				}
				else if (ordType == FCConvert.ToInt32(ERegistryValueTypes.REG_DWORD_BIG_ENDIAN))
				{
					// Unlikely, but you never know
					short dwData = 0;
					e = RegQueryValueExLongWrp(hKey, ref m_sValueKey, 0, ref ordType, dwData, ref cData);
					vValue = SwapEndian(dwData);
				}
				else if (ordType == FCConvert.ToInt32(ERegistryValueTypes.REG_SZ) || ordType == FCConvert.ToInt32(ERegistryValueTypes.REG_MULTI_SZ))
				{
					// Same thing to Visual Basic
					sData = Strings.StrDup(cData - 1, FCConvert.ToString(0));
					e = RegQueryValueExStrWrp(hKey, ref m_sValueKey, 0, ref ordType, ref sData, ref cData);
					vValue = sData;
				}
				else if (ordType == FCConvert.ToInt32(ERegistryValueTypes.REG_EXPAND_SZ))
				{
					sData = Strings.StrDup(cData - 1, FCConvert.ToString(0));
					e = RegQueryValueExStrWrp(hKey, ref m_sValueKey, 0, ref ordType, ref sData, ref cData);
					vValue = ExpandEnvStr(ref sData);
				}
				else
				{
					byte[] abData = null;
					abData = new byte[cData + 1];
					e = RegQueryValueExByteWrp(hKey, ref m_sValueKey, 0, ref ordType, ref abData[0], ref cData);
					vValue = abData;
				}
				Value = vValue;
				return Value;
			}
			// VBto upgrade warning: vValue As Variant --> As byte	OnRead(byte, int, string)
			set
			{
				// VBto upgrade warning: ordType As int	OnWrite(ERegistryValueTypes)
				int ordType = 0;
				int c = 0;
				int hKey = 0;
				int e;
				int lCreate = 0;
				SECURITY_ATTRIBUTES tSA = new SECURITY_ATTRIBUTES();
				// Open or Create the ID
				e = RegCreateKeyExWrp(m_hClassKey, ref m_sSectionKey, 0, "", REG_OPTION_NON_VOLATILE, ID_ALL_ACCESS, ref tSA, ref hKey, ref lCreate);
				if (FCConvert.ToBoolean(e))
				{
					Information.Err().Raise(26001, null, "Failed to set registry value ID: '" + FCConvert.ToString(m_hClassKey) + "',Section: '" + m_sSectionKey + "',ID: '" + m_sValueKey + "' to value: '" + FCConvert.ToString(m_vValue) + "'", null, null);
				}
				else
				{
					if (m_eValueType == ERegistryValueTypes.REG_BINARY)
					{
						if (value.GetType() == typeof(Array) || value.GetType() == typeof(byte))
						{
							byte[] ab = null;
							ab = (byte[])value;
							ordType = FCConvert.ToInt32(ERegistryValueTypes.REG_BINARY);
							c = Information.UBound(ab, 1) - Information.LBound(ab) - 1;
							e = RegSetValueExByteWrp(hKey, ref m_sValueKey, 0, ordType, ref ab[0], c);
						}
						else
						{
							Information.Err().Raise(26001, null, null, null, null);
						}
					}
					else if (m_eValueType == ERegistryValueTypes.REG_DWORD || m_eValueType == ERegistryValueTypes.REG_DWORD_BIG_ENDIAN || m_eValueType == ERegistryValueTypes.REG_DWORD_LITTLE_ENDIAN)
					{
						if (value.GetType() == typeof(int) || value.GetType() == typeof(long))
						{
							int i = 0;
							i = FCConvert.ToInt32(value);
							ordType = FCConvert.ToInt32(ERegistryValueTypes.REG_DWORD);
							e = RegSetValueExLongWrp(hKey, ref m_sValueKey, 0, ordType, ref i, 4);
						}
					}
					else if (m_eValueType == ERegistryValueTypes.REG_SZ || m_eValueType == ERegistryValueTypes.REG_EXPAND_SZ)
					{
						string s = "";
						int iPos = 0;
						s = Encoding.UTF8.GetString((byte[])value);
						ordType = FCConvert.ToInt32(ERegistryValueTypes.REG_SZ);
						// Assume anything with two non-adjacent percents is expanded string
						iPos = Strings.InStr(s, "%", CompareConstants.vbTextCompare/*?*/);
						if (FCConvert.ToBoolean(iPos))
						{
							if (FCConvert.ToBoolean(Strings.InStr(iPos + 2, s, "%")))
							{
								ordType = FCConvert.ToInt32(ERegistryValueTypes.REG_EXPAND_SZ);
							}
						}
						c = s.Length + 1;
						e = RegSetValueExStrWrp(hKey, ref m_sValueKey, 0, ordType, ref s, c);
						// User should convert to a compatible type before calling
					}
					else
					{
						e = ERROR_INVALID_DATA;
					}
					if (FCConvert.ToBoolean(~e))
					{
						m_vValue = value;
					}
					else
					{
						Information.Err().Raise(Constants.vbObjectError + 1048 + 26001, null, "Failed to set registry value ID: '" + FCConvert.ToString(m_hClassKey) + "',Section: '" + m_sSectionKey + "',ID: '" + m_sValueKey + "' to value: '" + FCConvert.ToString(m_vValue) + "'", null, null);
					}
					// Close the ID
					RegCloseKey(hKey);
				}
			}
		}

		public bool EnumerateValues(ref string[] sKeyNames, ref int iKeyCount)
		{
			bool EnumerateValues = false;
			int lResult;
			int hKey = 0;
			string sName = "";
			int lNameSize = 0;
			string sData = "";
			int lIndex;
			int cJunk = 0;
			int cNameMax = 0;
			double ft = 0;
			// Log "EnterEnumerateValues"
			iKeyCount = 0;
			Array.Clear(sKeyNames, 0, sKeyNames.Length);
			lIndex = 0;
			lResult = RegOpenKeyExWrp(m_hClassKey, ref m_sSectionKey, 0, ID_QUERY_VALUE, ref hKey);
			if (lResult == ERROR_SUCCESS)
			{
				// Log "OpenedKey:" & m_hClassKey & "," & m_sSectionKey
				lResult = RegQueryInfoKey(hKey, "", ref cJunk, 0, ref cJunk, ref cJunk, ref cJunk, ref cJunk, ref cNameMax, ref cJunk, ref cJunk, ref ft);
				while (lResult == ERROR_SUCCESS)
				{
					// Set buffer space
					lNameSize = cNameMax + 1;
					sName = Strings.StrDup(lNameSize, FCConvert.ToString(0));
					if (lNameSize == 0)
						lNameSize = 1;
					// Log "Requesting Next Value"
					// Get value name:
					lResult = RegEnumValueWrp(hKey, lIndex, ref sName, ref lNameSize, 0, 0, 0, 0);
					// Log "RegEnumValue returned:" & lResult
					if (lResult == ERROR_SUCCESS)
					{
						// Although in theory you can also retrieve the actual
						// value and type here, I found it always (ultimately) resulted in
						// a GPF, on Win95 and NT.  Why?  Can anyone help?
						sName = Strings.Left(sName, lNameSize);
						// Log "Enumerated value:" & sName
						iKeyCount += 1;
						Array.Resize(ref sKeyNames, iKeyCount + 1);
						sKeyNames[iKeyCount] = sName;
					}
					lIndex += 1;
				}
			}
			if (hKey != 0)
			{
				RegCloseKey(hKey);
			}
			// Log "Exit Enumerate Values"
			EnumerateValues = true;
			return EnumerateValues;
			EnumerateValuesError:
			;
			if (hKey != 0)
			{
				RegCloseKey(hKey);
			}
			Information.Err().Raise(Constants.vbObjectError + 1048 + 26003, null, Information.Err().Description, null, null);
			return EnumerateValues;
		}

		public bool EnumerateSections(ref string[] sSect, ref int iSectCount)
		{
			bool EnumerateSections = false;
			int lResult;
			int hKey = 0;
			int dwReserved/*unused?*/;
			string szBuffer = "";
			int lBuffSize = 0;
			int lIndex;
			int lType/*unused?*/;
			string sCompKey = "";
			int iPos = 0;
			try
			{
				// On Error GoTo EnumerateSectionsError
				iSectCount = 0;
				Array.Clear(sSect, 0, sSect.Length);
				// 
				lIndex = 0;
				lResult = RegOpenKeyExWrp(m_hClassKey, ref m_sSectionKey, 0, ID_ENUMERATE_SUB_KEYS, ref hKey);
				while (lResult == ERROR_SUCCESS)
				{
					// Set buffer space
					szBuffer = Strings.StrDup(255, FCConvert.ToString(0));
					lBuffSize = szBuffer.Length;
					// Get next value
					lResult = RegEnumKeyWrp(hKey, lIndex, ref szBuffer, lBuffSize);
					if (lResult == ERROR_SUCCESS)
					{
						iSectCount += 1;
						Array.Resize(ref sSect, iSectCount + 1);
						iPos = Strings.InStr(szBuffer, FCConvert.ToString(Convert.ToChar(0)), CompareConstants.vbTextCompare/*?*/);
						if (iPos > 0)
						{
							sSect[iSectCount] = Strings.Left(szBuffer, iPos - 1);
						}
						else
						{
							sSect[iSectCount] = Strings.Left(szBuffer, lBuffSize);
						}
					}
					lIndex += 1;
				}
				if (hKey != 0)
				{
					RegCloseKey(hKey);
				}
				EnumerateSections = true;
				return EnumerateSections;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				if (hKey != 0)
				{
					RegCloseKey(hKey);
				}
				Information.Err().Raise(Constants.vbObjectError + 1048 + 26002, null, Information.Err().Description, null, null);
				return EnumerateSections;
			}
		}

		public void CreateEXEAssociation(string sExePath, string sClassName, string sClassDescription, string sAssociation, string sOpenMenuText = "Open", bool bSupportPrint = false, string sPrintMenuText = "Print", bool bSupportNew = false, string sNewMenuText = "New", bool bSupportInstall = false, string sInstallMenuText = "", int lDefaultIconIndex = -1)
		{
			// Check if path is wrapped in quotes:
			sExePath = Strings.Trim(sExePath);
			if (Strings.Left(sExePath, 1) != "\"")
			{
				sExePath = "\"" + sExePath;
			}
			if (Strings.Right(sExePath, 1) != "\"")
			{
				sExePath += "\"";
			}
			// Create the .File to Class association:
			ClassKey = ERegistryClassConstants.HKEY_CLASSES_ROOT;
			SectionKey = "." + sAssociation;
			ValueType = ERegistryValueTypes.REG_SZ;
			ValueKey = "";
			Value = Encoding.Unicode.GetBytes(sClassName);
			// Create the Class shell open command:
			SectionKey = sClassName;
			Value = Encoding.Unicode.GetBytes(sClassDescription);
			SectionKey = sClassName + "\\shell\\open";
			if (sOpenMenuText == "")
				sOpenMenuText = "Open";
			ValueKey = "";
			Value = Encoding.Unicode.GetBytes(sOpenMenuText);
			SectionKey = sClassName + "\\shell\\open\\command";
			ValueKey = "";
			Value = Encoding.Unicode.GetBytes(sExePath + " \"%1\"");
			if (bSupportPrint)
			{
				SectionKey = sClassName + "\\shell\\print";
				if (sPrintMenuText == "")
					sPrintMenuText = "Print";
				ValueKey = "";
				Value = Encoding.Unicode.GetBytes(sPrintMenuText);
				SectionKey = sClassName + "\\shell\\print\\command";
				ValueKey = "";
				Value = Encoding.Unicode.GetBytes(sExePath + " /p \"%1\"");
			}
			if (bSupportInstall)
			{
				if (sInstallMenuText == "")
				{
					sInstallMenuText = "&Install " + sAssociation;
				}
				SectionKey = sClassName + "\\shell\\add";
				ValueKey = "";
				Value = Encoding.Unicode.GetBytes(sInstallMenuText);
				SectionKey = sClassName + "\\shell\\add\\command";
				ValueKey = "";
				Value = Encoding.Unicode.GetBytes(sExePath + " /a \"%1\"");
			}
			if (bSupportNew)
			{
				SectionKey = sClassName + "\\shell\\new";
				ValueKey = "";
				if (sNewMenuText == "")
					sNewMenuText = "New";
				Value = Encoding.Unicode.GetBytes(sNewMenuText);
				SectionKey = sClassName + "\\shell\\new\\command";
				ValueKey = "";
				Value = Encoding.Unicode.GetBytes(sExePath + " /n \"%1\"");
			}
			if (lDefaultIconIndex > -1)
			{
				SectionKey = sClassName + "\\DefaultIcon";
				ValueKey = "";
				Value = Encoding.Unicode.GetBytes(sExePath + "," + lDefaultIconIndex.ToString());
			}
		}

		public void CreateAdditionalEXEAssociations(string sClassName, object[] vItems)
		{
			int iItems;
			int iItem;
			/*? On Error Resume Next  */
			iItems = Information.UBound(vItems, 1) + 1;
			if ((iItems % 3) != 0 || (Information.Err().Number != 0))
			{
				Information.Err().Raise(Constants.vbObjectError + 1048 + 26004, null, "Invalid parameter list passed to CreateAdditionalEXEAssociations - expected Name/Text/Command", null, null);
			}
			else
			{
				// Check if it exists:
				SectionKey = sClassName;
				if (!(KeyExists))
				{
					Information.Err().Raise(Constants.vbObjectError + 1048 + 26005, null, "Error - attempt to create additional associations before class defined.", null, null);
				}
				else
				{
					for (iItem = 0; iItem <= iItems - 1; iItem += 3)
					{
						ValueType = ERegistryValueTypes.REG_SZ;
						SectionKey = sClassName + "\\shell\\" + FCConvert.ToString(vItems[iItem]);
						ValueKey = "";
						Value = (byte[])vItems[iItem + 1];
						SectionKey = sClassName + "\\shell\\" + FCConvert.ToString(vItems[iItem]) + "\\command";
						ValueKey = "";
						Value = (byte[])vItems[iItem + 2];
					}
					// iItem
				}
			}
		}

		public ERegistryValueTypes ValueType
		{
			get
			{
				ERegistryValueTypes ValueType = (ERegistryValueTypes)0;
				ValueType = m_eValueType;
				return ValueType;
			}
			set
			{
				m_eValueType = value;
			}
		}

		public ERegistryClassConstants ClassKey
		{
			get
			{
				ERegistryClassConstants ClassKey = (ERegistryClassConstants)0;
				ClassKey = (ERegistryClassConstants)m_hClassKey;
				return ClassKey;
			}
			set
			{
				m_hClassKey = FCConvert.ToInt32(value);
			}
		}

		public string SectionKey
		{
			get
			{
				string SectionKey = "";
				SectionKey = m_sSectionKey;
				return SectionKey;
			}
			set
			{
				m_sSectionKey = value;
			}
		}

		public string ValueKey
		{
			get
			{
				string ValueKey = "";
				ValueKey = m_sValueKey;
				return ValueKey;
			}
			set
			{
				m_sValueKey = value;
			}
		}

		public object Default
		{
			get
			{
				object Default = null;
				Default = m_vDefault;
				return Default;
			}
			set
			{
				m_vDefault = value;
			}
		}

		private int SwapEndian(int dw)
		{
			int SwapEndian = 0;
			CopyMemory(SwapEndian + 3, dw, 1);
			CopyMemory(SwapEndian + 2, dw + 1, 1);
			CopyMemory(SwapEndian + 1, dw + 2, 1);
			CopyMemory(SwapEndian, dw + 3, 1);
			return SwapEndian;
		}

		private string ExpandEnvStr(ref string sData)
		{
			string ExpandEnvStr = "";
			int c = 0;
			string s;
			// Get the length
			s = "";
			// Needed to get around Windows 95 limitation
			c = ExpandEnvironmentStringsWrp(ref sData, ref s, c);
			// Expand the string
			s = Strings.StrDup(c - 1, FCConvert.ToString(0));
			c = ExpandEnvironmentStringsWrp(ref sData, ref s, c);
			ExpandEnvStr = s;
			return ExpandEnvStr;
		}
	}
}
