﻿//Fecher vbPorter - Version 1.0.0.35
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

#if TWPP0000
using TWPP0000;
using modGlobalVariables = TWPP0000.modSecurity;


#elif TWBL0000
using TWBL0000;


#elif TWRE0000
using TWRE0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmGroup.
	/// </summary>
	public partial class frmGroup : BaseForm
	{
		public frmGroup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:CHN - issue #1587: Fix "Tab" order.
			Frame1.SendToBack();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmGroup InstancePtr
		{
			get
			{
				return (frmGroup)Sys.GetInstance(typeof(frmGroup));
			}
		}

		protected frmGroup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		int lngGroupID;
		int intGroupPrimaryRow;
		/// <summary>
		/// keeps track of the group primary
		/// </summary>
		bool boolFilling;
		/// <summary>
		/// used to ignore cell changed event when filling
		/// </summary>
		const int AccountCol = 1;
		const int TYPECOL = 0;
		const int NameCol = 2;
		const int GROUPCOL = 3;
		const int PRIMARYCOL = 4;
		const int RECOL = 5;
		bool boolDataChanged;
		bool boolAllowedtoChange;
		bool boolHaveRe;
		bool boolHavePP;
		bool boolHaveUT;
		private string strREDB = "";
		private string strPPDB = "";
		private string strGNDB = string.Empty;
		private string strUTDB = "";

		private void AccountsGrid_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
			//FC:FINAL:MSH - i.issue #1275: saving and using correct indexes of the cell
			int row = AccountsGrid.GetFlexRowIndex(e.RowIndex);
			int col = AccountsGrid.GetFlexColIndex(e.ColumnIndex);
			if (row < 2 || row > AccountsGrid.Rows - 1)
				return;
			if (boolFilling)
				return;
			boolDataChanged = true;
			switch (col)
			{
				case AccountCol:
					{
						if (Conversion.Val(AccountsGrid.TextMatrix(row, col)) == 0)
						{
							// just deleted the account, so clear everything
							// don't worry about the primary cols since the validate edit section already took care of it
							AccountsGrid.TextMatrix(row, RECOL, "");
						}
						else
						{
						}
						break;
					}
			}
			//end switch
		}

		private void AccountsGrid_ComboDropDown(object sender, System.EventArgs e)
		{
			AccountsGrid.TextMatrix(AccountsGrid.Row, RECOL, "");
			AccountsGrid.TextMatrix(AccountsGrid.Row, NameCol, "");
			AccountsGrid.TextMatrix(AccountsGrid.Row, AccountCol, "");
		}

		private void AccountsGrid_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			int intloop;
			if (boolAllowedtoChange == false)
			{
				return;
			}
			if (e.Button == MouseButtons.Left)
			{
				if (AccountsGrid.MouseRow < 2 || AccountsGrid.MouseRow > AccountsGrid.Rows - 1)
					return;
				AccountsGrid.Row = AccountsGrid.MouseRow;
				AccountsGrid.Col = AccountsGrid.MouseCol;
				switch (AccountsGrid.Col)
				{
					case AccountCol:
						{
							AccountsGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							break;
						}
					case TYPECOL:
						{
							// .Editable = flexEDKbdMouse
							break;
						}
					case NameCol:
						{
							AccountsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
							break;
						}
					case GROUPCOL:
						{
							AccountsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
							if (Strings.Trim(AccountsGrid.TextMatrix(AccountsGrid.Row, GROUPCOL)) != string.Empty)
							{
								// If .TextMatrix(.Row, GROUPCOL) = True Then
								if (AccountsGrid.Row == intGroupPrimaryRow)
								{
									MessageBox.Show("You must have a group primary account.  Make another account the primary to de-select this one.", null, MessageBoxButtons.OK, MessageBoxIcon.Warning);
									return;
								}
								else
								{
									// make this one true then set the original primary to false
									if (Conversion.Val(AccountsGrid.TextMatrix(AccountsGrid.Row, AccountCol)) < 1)
									{
										// nothing to make primary
										return;
									}
									AccountsGrid.TextMatrix(AccountsGrid.Row, GROUPCOL, FCConvert.ToString(true));
									if (intGroupPrimaryRow > 1)
									{
										AccountsGrid.TextMatrix(intGroupPrimaryRow, GROUPCOL, FCConvert.ToString(false));
									}
									intGroupPrimaryRow = AccountsGrid.Row;
									FillInfo();
									return;
								}
							}
							else
							{
								// make this one true then set the original primary to false
								AccountsGrid.TextMatrix(AccountsGrid.Row, GROUPCOL, FCConvert.ToString(true));
								if (intGroupPrimaryRow > 0)
								{
									AccountsGrid.TextMatrix(intGroupPrimaryRow, GROUPCOL, FCConvert.ToString(false));
								}
								intGroupPrimaryRow = AccountsGrid.Row;
								FillInfo();
								return;
							}
							break;
						}
					case PRIMARYCOL:
						{
							AccountsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
							// don't worry if they have no primary
							if (Conversion.Val(AccountsGrid.TextMatrix(AccountsGrid.Row, AccountCol)) < 1)
							{
								return;
							}
							// can't be primary if there is no association to be primary of
							if (Conversion.Val(AccountsGrid.TextMatrix(AccountsGrid.Row, RECOL)) < 1)
							{
								return;
							}
							if (AccountsGrid.TextMatrix(AccountsGrid.Row, TYPECOL) == "RE")
							{
								// can't associate a RE account with an RE account
								return;
							}
							if (AccountsGrid.TextMatrix(AccountsGrid.Row, PRIMARYCOL) == string.Empty)
							{
								for (intloop = 2; intloop <= AccountsGrid.Rows - 1; intloop++)
								{
									if ((intloop != AccountsGrid.Row) && (Conversion.Val(AccountsGrid.TextMatrix(intloop, RECOL)) == Conversion.Val(AccountsGrid.TextMatrix(AccountsGrid.Row, RECOL))))
									{
										if (Strings.Trim(AccountsGrid.TextMatrix(intloop, PRIMARYCOL)) != string.Empty)
										{
											if (fecherFoundation.FCUtils.CBool(AccountsGrid.TextMatrix(intloop, PRIMARYCOL)) == true)
											{
												AccountsGrid.TextMatrix(intloop, PRIMARYCOL, FCConvert.ToString(false));
												break;
											}
										}
									}
								}
								// intloop
								AccountsGrid.TextMatrix(AccountsGrid.Row, PRIMARYCOL, FCConvert.ToString(true));
							}
							else
							{
								if (fecherFoundation.FCUtils.CBool(AccountsGrid.TextMatrix(AccountsGrid.Row, PRIMARYCOL)) == false)
								{
									// check to see if there is another that is already primary
									for (intloop = 2; intloop <= AccountsGrid.Rows - 1; intloop++)
									{
										if ((intloop != AccountsGrid.Row) && (Conversion.Val(AccountsGrid.TextMatrix(intloop, RECOL)) == Conversion.Val(AccountsGrid.TextMatrix(AccountsGrid.Row, RECOL))))
										{
											if (Strings.Trim(AccountsGrid.TextMatrix(intloop, PRIMARYCOL)) != string.Empty)
											{
												if (fecherFoundation.FCUtils.CBool(AccountsGrid.TextMatrix(intloop, PRIMARYCOL)) == true)
												{
													AccountsGrid.TextMatrix(intloop, PRIMARYCOL, FCConvert.ToString(false));
													break;
												}
											}
										}
									}
									// intloop
									AccountsGrid.TextMatrix(AccountsGrid.Row, PRIMARYCOL, FCConvert.ToString(true));
								}
								else
								{
									AccountsGrid.TextMatrix(AccountsGrid.Row, PRIMARYCOL, FCConvert.ToString(false));
								}
							}
							break;
						}
					case RECOL:
						{
							AccountsGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							break;
						}
				}
				//end switch
			}
			else
			{
				// it's the right button
				// .Row = .MouseRow
				// .Col = .MouseCol
				// PopupMenu mnuPop
			}
		}

		private void AccountsGrid_RowColChange(object sender, System.EventArgs e)
		{
			if (AccountsGrid.Row < 2)
				return;
			if (boolAllowedtoChange == false)
			{
				return;
			}
			switch (AccountsGrid.Col)
			{
				case AccountCol:
					{
						if (Strings.Trim(AccountsGrid.TextMatrix(AccountsGrid.Row, TYPECOL)) != string.Empty)
						{
							AccountsGrid.EditCell();
						}
						break;
					}
				case TYPECOL:
					{
						if (Conversion.Val(AccountsGrid.TextMatrix(AccountsGrid.Row, AccountCol)) == 0)
						{
							AccountsGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							return;
						}
						// already valid data here
						// make sure it's not a primary
						if (fecherFoundation.FCUtils.CBool(AccountsGrid.TextMatrix(AccountsGrid.Row, GROUPCOL)) == true)
						{
							MessageBox.Show("This is the group primary. You must pick a different primary before you change the type of this entry.", "Pick a Primary", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							AccountsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
							return;
						}
						if (fecherFoundation.FCUtils.CBool(AccountsGrid.TextMatrix(AccountsGrid.Row, PRIMARYCOL)) == true)
						{
							AccountsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
							MessageBox.Show("This is the primary associate of this account type. You must pick another before changing the type of this entry.", "Pick Another", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
						// it's not a primary
						// get rid of the re association.  If they're changing the account type it probably isn't valid
						AccountsGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				case GROUPCOL:
					{
						AccountsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
				case PRIMARYCOL:
					{
						AccountsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
				case NameCol:
					{
						AccountsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void AccountsGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			int x;
			//FC:FINAL:MSH - i.issue #1275: saving and using correct indexes of the cell
			int row = AccountsGrid.GetFlexRowIndex(e.RowIndex);
			int col = AccountsGrid.GetFlexColIndex(e.ColumnIndex);
			if (row < 2)
				return;
			if (row > AccountsGrid.Rows - 1)
				return;
			if (col == AccountCol)
			{
				if (Conversion.Val(AccountsGrid.EditText) == 0)
				{
					AccountsGrid.TextMatrix(row, RECOL, "");
					AccountsGrid.TextMatrix(row, NameCol, "");
				}
				else
				{
					for (x = 2; x <= AccountsGrid.Rows - 1; x++)
					{
						// check if it's already in the list
						if ((x != row) && (Conversion.Val(AccountsGrid.TextMatrix(x, AccountCol)) == Conversion.Val(AccountsGrid.EditText)) && (AccountsGrid.TextMatrix(x, TYPECOL) == AccountsGrid.TextMatrix(row, TYPECOL)))
						{
							MessageBox.Show("This account is already listed.", "Account Already Listed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
							return;
						}
						// now check if it's already in another group and if it exists
						clsTemp.OpenRecordset("select * from moduleassociation where account = " + FCConvert.ToString(Conversion.Val(AccountsGrid.EditText)) + " and module = '" + AccountsGrid.TextMatrix(row, TYPECOL) + "' and groupnumber <> " + FCConvert.ToString(lngGroupID), "CentralData");
						if (!clsTemp.EndOfFile())
						{
							MessageBox.Show("This account is already in group " + clsTemp.Get_Fields_Int32("groupnumber"), "Cannot Add Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = true;
							return;
						}
						// now does it exist?
						string vbPorterVar = AccountsGrid.TextMatrix(row, TYPECOL);
						if (vbPorterVar == "RE")
						{
							if (clsTemp.DBExists("RealEstate"))
							{
								clsTemp.OpenRecordset("select * from master CROSS APPLY " + clsTemp.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(OwnerPartyID) as p where rsaccount = " + FCConvert.ToString(Conversion.Val(AccountsGrid.EditText)) + " and rscard = 1 and rsdeleted <> 1", strREDB);
								if (clsTemp.EndOfFile())
								{
									MessageBox.Show("This account either doesn't exist or is deleted.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									e.Cancel = true;
									return;
								}
								AccountsGrid.TextMatrix(row, NameCol, FCConvert.ToString(clsTemp.Get_Fields_String("FullNameLF")));
							}
							else
							{
								MessageBox.Show("Could not find Real Estate Database", "No Real Estate Database", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								e.Cancel = true;
								return;
							}
						}
						else if (vbPorterVar == "PP")
						{
							if (clsTemp.DBExists("PersonalProperty"))
							{
								clsTemp.OpenRecordset("select * from ppmaster CROSS APPLY " + clsTemp.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(PartyID) as p where account = " + FCConvert.ToString(Conversion.Val(AccountsGrid.EditText)) + " and deleted  = 0 ", strPPDB);
								if (clsTemp.EndOfFile())
								{
									MessageBox.Show("This account either doesn't exist or is deleted.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									e.Cancel = true;
									return;
								}
								AccountsGrid.TextMatrix(row, NameCol, FCConvert.ToString(clsTemp.Get_Fields_String("FullNameLF")));
							}
							else
							{
								MessageBox.Show("Could not find Personal Property Database", "No Personal Property Database", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								e.Cancel = true;
								return;
							}
						}
						else if (vbPorterVar == "UT")
						{
							// If File.Exists(strUTDB) Then
							if (clsTemp.DBExists("UtilityBilling"))
							{
								clsTemp.OpenRecordset("select * from master CROSS APPLY " + clsTemp.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(BillingPartyID) as p where accountnumber = " + FCConvert.ToString(Conversion.Val(AccountsGrid.EditText)), strUTDB);
								if (clsTemp.EndOfFile())
								{
									MessageBox.Show("This account doesn't exist.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									e.Cancel = true;
									return;
								}
								AccountsGrid.TextMatrix(row, NameCol, FCConvert.ToString(clsTemp.Get_Fields_String("FullNameLF")));
							}
							else
							{
								MessageBox.Show("Could not find Utility Billing Database", "No Utility Billing Database", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								e.Cancel = true;
								return;
							}
						}
					}
					// x
				}
				return;
			}
			else if (col == RECOL)
			{
				// make sure this isn't a RE account
				if (Conversion.Val(AccountsGrid.EditText) < 1)
				{
					AccountsGrid.TextMatrix(row, PRIMARYCOL, FCConvert.ToString(false));
					return;
				}
				if (AccountsGrid.TextMatrix(row, TYPECOL) == "RE")
				{
					e.Cancel = true;
					MessageBox.Show("Can't associate real estate accounts with real estate accounts.", "Invalid Association", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				// see if it exists
				clsTemp.OpenRecordset("select * from master where rsaccount = " + FCConvert.ToString(Conversion.Val(AccountsGrid.EditText)) + " and rscard = 1 and rsdeleted <> 1", strREDB);
				if (clsTemp.EndOfFile())
				{
					MessageBox.Show("This account either doesn't exist or is deleted.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					return;
				}
				// make sure it's not in another group
				clsTemp.OpenRecordset("select * from moduleassociation where account = " + AccountsGrid.EditText + " and module = 'RE' and GROUPNUMBER <> " + FCConvert.ToString(lngGroupID), "CentralData");
				if (!clsTemp.EndOfFile())
				{
					e.Cancel = true;
					MessageBox.Show("This RE account is already in group number " + clsTemp.Get_Fields_Int32("groupnumber"), "Cannot Add Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
		}

		private void frmGroup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuExit_Click();
			}
		}

		private void frmGroup_Load(object sender, System.EventArgs e)
		{
			boolDataChanged = false;
			modGlobalFunctions.SetFixedSize(this);
			if (!modSecurity.ValidPermissions_6(this, modGlobalVariables.CNSTEDITGROUPS, false))
			{
				// disable everything
				boolAllowedtoChange = false;
				txtGroupNumber.ReadOnly = true;
				AccountsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
				mnuAdd.Enabled = false;
				mnuDelete.Enabled = false;
				mnuSave.Enabled = false;
				mnuSaveExit.Enabled = false;
				rtbComment.Locked = true;
			}
			else
			{
				boolAllowedtoChange = true;
			}
		}

		private void frmGroup_Resize(object sender, System.EventArgs e)
		{
			ResizeAccountsGrid();
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
		}

		private void mnuAdd_Click(object sender, System.EventArgs e)
		{
			AccountsGrid.Rows += 1;
			AccountsGrid.TextMatrix(AccountsGrid.Rows - 1, PRIMARYCOL, FCConvert.ToString(false));
			AccountsGrid.TextMatrix(AccountsGrid.Rows - 1, GROUPCOL, FCConvert.ToString(false));
			boolDataChanged = true;
		}

		private void mnuAddEntry_Click()
		{
			AccountsGrid.Rows += 1;
			AccountsGrid.TextMatrix(AccountsGrid.Rows - 1, PRIMARYCOL, FCConvert.ToString(false));
			AccountsGrid.TextMatrix(AccountsGrid.Rows - 1, GROUPCOL, FCConvert.ToString(false));
			boolDataChanged = true;
		}

		private void mnuBalance_Click(object sender, System.EventArgs e)
		{
			frmGroupBalance.InstancePtr.Init(lngGroupID);
		}

		public void mnuDelete_Click()
		{
			mnuDelete_Click(mnuDelete, new System.EventArgs());
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			int x;
			boolDataChanged = true;
			// check to see if it's okay to delete this row
			if (AccountsGrid.Row < 2)
				return;
			if (Conversion.Val(AccountsGrid.TextMatrix(AccountsGrid.Row, AccountCol)) > 0)
			{
				// if this is the primary group don't delete the row
				//FC:FINAL:RPU #i1179 - use FCConvert.CBool instead
				//if (FCConvert.ToBoolean(AccountsGrid.TextMatrix(AccountsGrid.Row, GROUPCOL)) == true)
				if (FCConvert.CBool(AccountsGrid.TextMatrix(AccountsGrid.Row, GROUPCOL)) == true)
				{
					MessageBox.Show("This is the group primary.  Make another account the group primary before you delete this entry.", "Cannot Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			if (AccountsGrid.TextMatrix(AccountsGrid.Row, TYPECOL) == "RE")
			{
				// if an account is associated with this one then don't delete the entry
				for (x = 2; x <= AccountsGrid.Rows - 1; x++)
				{
					if (Conversion.Val(AccountsGrid.TextMatrix(x, RECOL)) == Conversion.Val(AccountsGrid.TextMatrix(AccountsGrid.Row, AccountCol)) && Conversion.Val(AccountsGrid.TextMatrix(x, RECOL)) > 0)
					{
						MessageBox.Show("You can't delete this entry since there are accounts still associated with it.", "Cannot Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				// x
			}
			// should be okay to delete it
			AccountsGrid.RemoveItem(AccountsGrid.Row);
		}

		private void mnuDeleteEntry_Click()
		{
			boolDataChanged = true;
			mnuDelete_Click();
		}

		private void mnuDeleteGroup_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsTemp = new clsDRWrapper();
			// vbPorter upgrade warning: intResp As short, int --> As DialogResult
			DialogResult intResp;
			intResp = MessageBox.Show("This will remove the entire group and delete all entries for the group." + "\r\n" + "Do you wish to continue?", "Remove Group?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (intResp == DialogResult.Yes)
			{
				clsTemp.Execute("delete from groupmaster where id = " + FCConvert.ToString(lngGroupID), "CentralData");
				clsTemp.Execute("delete from moduleassociation where groupnumber = " + FCConvert.ToString(lngGroupID), "CentralData");
				Close();
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			if (boolDataChanged)
			{
				intResponse = MessageBox.Show("You have made changes. Do you wish to save them?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intResponse == DialogResult.Yes)
				{
					mnuSave_Click();
				}
			}
			Close();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void SetupAccountsGrid()
		{
			AccountsGrid.Cols = 6;
			//AccountsGrid.Rows = 2;
			AccountsGrid.FixedRows = 2;
			AccountsGrid.Rows = 2;
			AccountsGrid.ColDataType(GROUPCOL, FCGrid.DataTypeSettings.flexDTBoolean);
			AccountsGrid.ColDataType(PRIMARYCOL, FCGrid.DataTypeSettings.flexDTBoolean);
			AccountsGrid.Col = -1;
			AccountsGrid.ColComboList(TYPECOL, "PP|RE|UT");
			AccountsGrid.TextMatrix(1, AccountCol, "Account");
			AccountsGrid.TextMatrix(1, TYPECOL, "Type");
			AccountsGrid.TextMatrix(1, NameCol, "Name");
			AccountsGrid.TextMatrix(0, GROUPCOL, "Group");
			AccountsGrid.TextMatrix(0, PRIMARYCOL, "Primary");
			AccountsGrid.TextMatrix(0, RECOL, "Associated");
			AccountsGrid.TextMatrix(1, GROUPCOL, "Primary");
			AccountsGrid.TextMatrix(1, PRIMARYCOL, "Association");
			AccountsGrid.TextMatrix(1, RECOL, "RE Account");
			AccountsGrid.ColAlignment(GROUPCOL, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			AccountsGrid.ColAlignment(PRIMARYCOL, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			//FC:FINAL:MSH - issue #1719: change columns alignment
			//AccountsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, RECOL, 1, RECOL, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			AccountsGrid.ColAlignment(TYPECOL, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			AccountsGrid.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			AccountsGrid.ColAlignment(NameCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			AccountsGrid.ColAlignment(RECOL, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			AccountsGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		}

		private void ResizeAccountsGrid()
		{
			int GridWidth = 0;
			GridWidth = AccountsGrid.WidthOriginal;
			AccountsGrid.ColWidth(AccountCol, FCConvert.ToInt32(0.1 * GridWidth));
			AccountsGrid.ColWidth(TYPECOL, FCConvert.ToInt32(0.08 * GridWidth));
			AccountsGrid.ColWidth(NameCol, FCConvert.ToInt32(0.43 * GridWidth));
			AccountsGrid.ColWidth(GROUPCOL, FCConvert.ToInt32(0.1 * GridWidth));
			AccountsGrid.ColWidth(PRIMARYCOL, FCConvert.ToInt32(0.12 * GridWidth));
			AccountsGrid.ColWidth(RECOL, FCConvert.ToInt32(0.1 * GridWidth));
		}

		private void FillAccountsGrid()
		{
			clsDRWrapper clsGroup = new clsDRWrapper();
			clsDRWrapper clsModule = new clsDRWrapper();
			bool boolUseName = false;
			// must be called before fillinfo the first time loaded
			try
			{
				// On Error GoTo ErrorHandler
				boolFilling = true;
				// get every account in group and put them in grid in order of module type
				intGroupPrimaryRow = -1;
				clsGroup.OpenRecordset("select * from moduleassociation where groupnumber = " + FCConvert.ToString(lngGroupID) + " order by GROUPPRIMARY,primaryassociate,module,account", "CentralData");
				while (!clsGroup.EndOfFile())
				{
					// TODO: Check the table for the column [account] and replace with corresponding Get_Field method
					AccountsGrid.AddItem(clsGroup.Get_Fields_String("module") + "\t" + clsGroup.Get_Fields("account") + "\t" + "\t" + FCConvert.ToString(FCConvert.CBool(clsGroup.Get_Fields_Boolean("groupprimary"))) + "\t" + FCConvert.ToString(FCConvert.CBool(clsGroup.Get_Fields_Boolean("PrimaryAssociate"))) + "\t" + clsGroup.Get_Fields_Int32("remasteracct"));
					if (fecherFoundation.FCUtils.CBool(AccountsGrid.TextMatrix(AccountsGrid.Rows - 1, GROUPCOL)) == true)
					{
						intGroupPrimaryRow = AccountsGrid.Rows - 1;
					}
					// now fill in the name
					boolUseName = true;
					if (Strings.UCase(AccountsGrid.TextMatrix(AccountsGrid.Rows - 1, TYPECOL)) == "RE")
					{
						if (boolHaveRe)
						{
							clsModule.OpenRecordset("select DeedName1 as thename from master  where rsaccount = " + AccountsGrid.TextMatrix(AccountsGrid.Rows - 1, AccountCol) + " and rscard = 1", strREDB);
						}
						else
						{
							boolUseName = false;
						}
					}
					else if (Strings.UCase(AccountsGrid.TextMatrix(AccountsGrid.Rows - 1, TYPECOL)) == "PP")
					{
						if (boolHavePP)
						{
							clsModule.OpenRecordset("select FullNameLF as thename from ppmaster CROSS APPLY " + clsModule.CurrentPrefix + "CentralParties.dbo.GetCentralPartyName(PartyID) as p where account = " + AccountsGrid.TextMatrix(AccountsGrid.Rows - 1, AccountCol), strPPDB);
						}
						else
						{
							boolUseName = false;
						}
					}
					else if (Strings.UCase(AccountsGrid.TextMatrix(AccountsGrid.Rows - 1, TYPECOL)) == "UT")
					{
						if (boolHaveUT)
						{
							clsModule.OpenRecordset("select DeedName1 as thename from master  where accountnumber = " + AccountsGrid.TextMatrix(AccountsGrid.Rows - 1, AccountCol), strUTDB);
						}
						else
						{
							boolUseName = false;
						}
					}
					if (boolUseName && !clsModule.EndOfFile())
					{
						// TODO: Field [thename] not found!! (maybe it is an alias?)
						AccountsGrid.TextMatrix(AccountsGrid.Rows - 1, NameCol, FCConvert.ToString(clsModule.Get_Fields("thename")));
					}
					else
					{
						AccountsGrid.TextMatrix(AccountsGrid.Rows - 1, NameCol, "Record or Database Missing");
					}
					clsGroup.MoveNext();
				}
				if (intGroupPrimaryRow == -1)
				{
					// no primary for some reason so assign one
					intGroupPrimaryRow = 2;
					AccountsGrid.TextMatrix(2, GROUPCOL, FCConvert.ToString(true));
				}
				boolFilling = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				boolFilling = false;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Fill Accounts Grid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: lngGroupNumber As int	OnWrite(string, short)
		public void Init(int lngGroupNumber, string strREDBToUse = "twre0000.vb1", string strPPDBToUse = "twpp0000.vb1", string strGNDBToUse = "CentralData", string strUTDBToUse = "twut0000.vb1")
		{
			clsDRWrapper clsinfo = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				intGroupPrimaryRow = 0;
				strREDB = strREDBToUse;
				strPPDB = strPPDBToUse;
				strGNDB = strGNDBToUse;
				strUTDB = strUTDBToUse;
				// If Not File.Exists(CurDir & "\" & strREDB) Then
				if (!clsinfo.DBExists("RealEstate"))
				{
					boolHaveRe = false;
				}
				else
				{
					boolHaveRe = true;
				}
				// If Not File.Exists(CurDir & "\" & strPPDB) Then
				if (!clsinfo.DBExists("PersonalProperty"))
				{
					boolHavePP = false;
				}
				else
				{
					boolHavePP = true;
				}
				// If Not File.Exists(CurDir & "\" & strUTDB) Then
				if (!clsinfo.DBExists("UtilityBilling"))
				{
					boolHaveUT = false;
				}
				else
				{
					boolHaveUT = true;
				}
				lngGroupID = lngGroupNumber;
				//FC:FINAL:DSE:#i1200 Code moved to constructor
				SetupAccountsGrid();
				// if groupid is 0 then this is an add new
				if (lngGroupID > 0)
				{
					// txtGroupNumber.Enabled = False
					clsinfo.OpenRecordset("select * from groupmaster where id = " + FCConvert.ToString(lngGroupID), "CentralData");
					txtGroupNumber.Text = FCConvert.ToString(Conversion.Val(clsinfo.Get_Fields_Int32("groupnumber")));
					FillAccountsGrid();
					FillInfo();
					FillComment();
				}
				this.Show(App.MainForm);
				this.BringToFront();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillComment()
		{
			clsDRWrapper clsComment = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				clsComment.OpenRecordset("select * from groupmaster where id = " + FCConvert.ToString(lngGroupID), "CentralData");
				rtbComment.Text = "";
				if (FCConvert.ToString(clsComment.Get_Fields_String("comment")) != string.Empty)
				{
					rtbComment.Text = FCConvert.ToString(clsComment.Get_Fields_String("comment"));
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Fill Comment", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillInfo()
		{
			// fills address info in based on the account and type specified in intgroupprimaryrow
			clsDRWrapper clsinfo = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				if (intGroupPrimaryRow > 0)
				{
					if (Strings.UCase(AccountsGrid.TextMatrix(intGroupPrimaryRow, TYPECOL)) == "RE")
					{
						clsinfo.OpenRecordset("select * from master CROSS APPLY " + clsinfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(OwnerPartyID, NULL, 'RE', rsAccount) as p where rsaccount = " + AccountsGrid.TextMatrix(intGroupPrimaryRow, AccountCol) + " and rscard = 1", strREDB);
						if (!clsinfo.EndOfFile())
						{
							txtName.Text = FCConvert.ToString(clsinfo.Get_Fields_String("DeedName1"));
							txtAddress1.Text = FCConvert.ToString(clsinfo.Get_Fields_String("Address1"));
							txtAddress2.Text = FCConvert.ToString(clsinfo.Get_Fields_String("Address2"));
							txtCity.Text = FCConvert.ToString(clsinfo.Get_Fields_String("City"));
							// TODO: Field [PartyState] not found!! (maybe it is an alias?)
							txtState.Text = FCConvert.ToString(clsinfo.Get_Fields("PartyState"));
							txtZip.Text = FCConvert.ToString(clsinfo.Get_Fields_String("Zip"));
							txtZip4.Text = "";
							txtStreetNumber.Text = "";
							txtApt.Text = "";
							txtStreetName.Text = "";
						}
					}
					else if (Strings.UCase(AccountsGrid.TextMatrix(intGroupPrimaryRow, TYPECOL)) == "UT")
					{
						clsinfo.OpenRecordset("select * from master CROSS APPLY " + clsinfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(BillingPartyID, NULL, 'UT', AccountNumber) as p where accountnumber = " + AccountsGrid.TextMatrix(intGroupPrimaryRow, AccountCol), strUTDB);
						if (!clsinfo.EndOfFile())
						{
							txtName.Text = FCConvert.ToString(clsinfo.Get_Fields_String("DeedName1"));
							txtAddress1.Text = FCConvert.ToString(clsinfo.Get_Fields_String("Address1"));
							txtAddress2.Text = FCConvert.ToString(clsinfo.Get_Fields_String("Address2"));
							txtCity.Text = FCConvert.ToString(clsinfo.Get_Fields_String("City"));
							// TODO: Field [PartyState] not found!! (maybe it is an alias?)
							txtState.Text = FCConvert.ToString(clsinfo.Get_Fields("PartyState"));
							txtZip.Text = FCConvert.ToString(clsinfo.Get_Fields_String("Zip"));
							txtZip4.Text = "";
							txtStreetNumber.Text = FCConvert.ToString(clsinfo.Get_Fields_Int32("streetnumber"));
							txtApt.Text = "";
							txtStreetName.Text = FCConvert.ToString(clsinfo.Get_Fields_String("streetname"));
						}
					}
					else if (Strings.UCase(AccountsGrid.TextMatrix(intGroupPrimaryRow, TYPECOL)) == "PP")
					{
						clsinfo.OpenRecordset("select * from ppmaster CROSS APPLY " + clsinfo.CurrentPrefix + "CentralParties.dbo.GetCentralPartyNameAndAddress(PartyID, NULL, 'PP', Account) as p where account = " + AccountsGrid.TextMatrix(intGroupPrimaryRow, AccountCol), strPPDB);
						if (!clsinfo.EndOfFile())
						{
							txtName.Text = FCConvert.ToString(clsinfo.Get_Fields_String("FullNameLF"));
							txtAddress1.Text = FCConvert.ToString(clsinfo.Get_Fields_String("Address1"));
							txtAddress2.Text = FCConvert.ToString(clsinfo.Get_Fields_String("Address2"));
							txtCity.Text = FCConvert.ToString(clsinfo.Get_Fields_String("City"));
							// TODO: Field [PartyState] not found!! (maybe it is an alias?)
							txtState.Text = FCConvert.ToString(clsinfo.Get_Fields("PartyState"));
							txtZip.Text = FCConvert.ToString(clsinfo.Get_Fields_String("Zip"));
							txtZip4.Text = "";
							txtStreetNumber.Text = FCConvert.ToString(clsinfo.Get_Fields_Int32("streetnumber"));
							txtApt.Text = "";
							txtStreetName.Text = FCConvert.ToString(clsinfo.Get_Fields_String("street"));
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Fill Info", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsCheck = new clsDRWrapper();
			int x;
			// loop counters
			int y;
			int intNumGPrimaries = 0;
			// count the number of group primaries
			string strSQL = "";
			// for insert statement
			try
			{
				// On Error GoTo ErrorHandler
				// make sure the group number isn't already in use
				if (Conversion.Val(txtGroupNumber.Text) > 0)
				{
					clsCheck.OpenRecordset("select * from groupmaster where groupnumber = " + FCConvert.ToString(Conversion.Val(txtGroupNumber.Text)) + " and id <> " + FCConvert.ToString(lngGroupID), "CentralData");
					if (!clsCheck.EndOfFile())
					{
						MessageBox.Show("This groupnumber is already being used. Please pick another.", "Invalid Group Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				if (lngGroupID > 0)
				{
					// let's see if the group number is legit
					if (Conversion.Val(txtGroupNumber.Text) <= 0)
					{
						MessageBox.Show("You must have a groupnumber in order to save.", "Enter Group Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
				}
				// check for group primaries
				// check for valid accounts as well
				intNumGPrimaries = 0;
				for (x = 2; x <= AccountsGrid.Rows - 1; x++)
				{
					if (AccountsGrid.TextMatrix(x, GROUPCOL) != string.Empty)
					{
						if (FCConvert.CBool(AccountsGrid.TextMatrix(x, GROUPCOL)) == true)
							intNumGPrimaries += 1;
					}
					if (AccountsGrid.TextMatrix(x, TYPECOL) == "RE")
					{
						// force correctness
						AccountsGrid.TextMatrix(x, PRIMARYCOL, FCConvert.ToString(false));
						AccountsGrid.TextMatrix(x, RECOL, "");
					}
					if (AccountsGrid.TextMatrix(x, TYPECOL) == "")
					{
						MessageBox.Show("You have at least one row that has invalid data.  The account type is missing.", "Invalid Account Type", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (Conversion.Val(AccountsGrid.TextMatrix(x, AccountCol)) < 1)
					{
						MessageBox.Show("You have at least one row that has invalid data. The account number is missing.", "Invalid Account Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					// check to see if this account is listed in another group
					clsCheck.OpenRecordset("select * from moduleassociation where account = " + FCConvert.ToString(Conversion.Val(AccountsGrid.TextMatrix(x, AccountCol))) + " and module = '" + AccountsGrid.TextMatrix(x, TYPECOL) + "' and groupnumber <> " + FCConvert.ToString(lngGroupID), "CentralData");
					if (!clsCheck.EndOfFile())
					{
						MessageBox.Show(AccountsGrid.TextMatrix(x, TYPECOL) + " account " + AccountsGrid.TextMatrix(x, AccountCol) + " is already in another group.  Cannot continue saving.", "Error Saving", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					if (fecherFoundation.FCUtils.CBool(AccountsGrid.TextMatrix(x, PRIMARYCOL)) == true)
					{
						// check all other entries to see if there is a conflict in primary accounts
						for (y = x + 1; y <= AccountsGrid.Rows - 1; y++)
						{
							if (AccountsGrid.TextMatrix(y, PRIMARYCOL) != string.Empty)
							{
								if (FCConvert.CBool(AccountsGrid.TextMatrix(y, PRIMARYCOL)) == true)
								{
									if (Conversion.Val(AccountsGrid.TextMatrix(y, RECOL)) == Conversion.Val(AccountsGrid.TextMatrix(x, RECOL)))
									{
										if (AccountsGrid.TextMatrix(y, TYPECOL) == AccountsGrid.TextMatrix(x, TYPECOL))
										{
											// Two primary accounts for the same re association with the same module type
											MessageBox.Show("You have two primary accounts of type " + AccountsGrid.TextMatrix(x, TYPECOL) + " for RE account " + FCConvert.ToString(Conversion.Val(AccountsGrid.TextMatrix(x, RECOL))) + ". Unable to continue saving.", "Two Many Primaries", MessageBoxButtons.OK, MessageBoxIcon.Warning);
											return;
										}
									}
								}
							}
						}
						// y
					}
				}
				// x
				if (intNumGPrimaries > 1)
				{
					MessageBox.Show("You can only have one group primary. Cannot continue saving.", "Too Many Primaries", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				// should be safe to save now
				if (lngGroupID == 0)
				{
					if (Conversion.Val(txtGroupNumber.Text) <= 0)
					{
						clsCheck.OpenRecordset("select max(groupnumber) as maxgroup from groupmaster", "CentralData");
						if (!clsCheck.EndOfFile())
						{
							// TODO: Field [maxgroup] not found!! (maybe it is an alias?)
							if (Conversion.Val(clsCheck.Get_Fields("maxgroup")) > 0)
							{
								// TODO: Field [maxgroup] not found!! (maybe it is an alias?)
								txtGroupNumber.Text = FCConvert.ToString(Conversion.Val(clsCheck.Get_Fields("maxgroup")) + 1);
							}
							else
							{
								txtGroupNumber.Text = FCConvert.ToString(1);
							}
						}
						else
						{
							txtGroupNumber.Text = FCConvert.ToString(1);
						}
					}
					clsCheck.OpenRecordset("select * from groupmaster where id = -1", "CentralData");
					clsCheck.AddNew();
					clsCheck.Set_Fields("groupnumber", FCConvert.ToString(Conversion.Val(txtGroupNumber.Text)));
					clsCheck.Update();
					lngGroupID = FCConvert.ToInt32(clsCheck.Get_Fields_Int32("id"));
				}
				// save comment if any
				clsCheck.OpenRecordset("select * from groupmaster where id = " + FCConvert.ToString(lngGroupID), "CentralData");
				clsCheck.Edit();
				clsCheck.Set_Fields("comment", Strings.Trim(rtbComment.Text));
				if (Conversion.Val(txtGroupNumber.Text) > 0)
				{
					clsCheck.Set_Fields("groupnumber", FCConvert.ToString(Conversion.Val(txtGroupNumber.Text)));
				}
				clsCheck.Update();
				// Clear all previous info for this group and replace it with new stuff
				clsCheck.Execute("delete from moduleassociation where groupnumber = " + FCConvert.ToString(lngGroupID), "CentralData");
				for (x = 2; x <= AccountsGrid.Rows - 1; x++)
				{
					strSQL = "(";
					strSQL += FCConvert.ToString(lngGroupID);
					strSQL += "," + FCConvert.ToString(Conversion.Val(AccountsGrid.TextMatrix(x, AccountCol)));
					strSQL += ",'" + AccountsGrid.TextMatrix(x, TYPECOL) + "'";
					if (AccountsGrid.TextMatrix(x, GROUPCOL) != string.Empty)
					{
						if (FCConvert.CBool(AccountsGrid.TextMatrix(x, GROUPCOL)))
						{
							strSQL += ",1";
						}
						else
						{
							strSQL += ",0";
						}
					}
					else
					{
						strSQL += ",0";
					}
					if (AccountsGrid.TextMatrix(x, PRIMARYCOL) != string.Empty)
					{
						if (FCConvert.CBool(AccountsGrid.TextMatrix(x, PRIMARYCOL)))
						{
							strSQL += ",1";
						}
						else
						{
							strSQL += ",0";
						}
					}
					else
					{
						strSQL += ",0";
					}
					strSQL += "," + FCConvert.ToString(Conversion.Val(AccountsGrid.TextMatrix(x, RECOL)));
					strSQL += ")";
					clsCheck.Execute("Insert into moduleassociation (groupnumber,account,module,groupprimary,primaryassociate,remasteracct) values " + strSQL, "CentralData");
				}
				// x
				MessageBox.Show("Save Completed.", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				boolDataChanged = false;
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In save", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuSave_Click()
		{
			mnuSave_Click(mnuSave, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			mnuSave_Click();
			mnuExit_Click();
		}
	}
}
