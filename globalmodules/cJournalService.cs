﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using fecherFoundation.Extensions;
using Org.BouncyCastle.X509;
using System;
using System.Collections.Generic;
using TWSharedLibrary;
using Wisej.Web;
#if TWBD0000
using TWBD0000;
#endif
namespace Global
{
    public class cJournalService
    {
        //=========================================================
        private string strLastError = "";
        // vbPorter upgrade warning: lngLastError As int	OnWrite(int, string)
        private int lngLastError;

        private cBDAccountController bactController_AutoInitialized;

        private cBDAccountController bactController
        {
            get
            {
                if (bactController_AutoInitialized == null)
                {
                    bactController_AutoInitialized = new cBDAccountController();
                }
                return bactController_AutoInitialized;
            }
            set
            {
                bactController_AutoInitialized = value;
            }
        }

        private cJournalController jController_AutoInitialized;

        private cJournalController jController
        {
            get
            {
                if (jController_AutoInitialized == null)
                {
                    jController_AutoInitialized = new cJournalController();
                }
                return jController_AutoInitialized;
            }
            set
            {
                jController_AutoInitialized = value;
            }
        }

        private cAPJournalController apjController_AutoInitialized;

        private cAPJournalController apjController
        {
            get
            {
                if (apjController_AutoInitialized == null)
                {
                    apjController_AutoInitialized = new cAPJournalController();
                }
                return apjController_AutoInitialized;
            }
            set
            {
                apjController_AutoInitialized = value;
            }
        }

        private cEncumbranceController encController_AutoInitialized;

        private cEncumbranceController encController
        {
            get
            {
                if (encController_AutoInitialized == null)
                {
                    encController_AutoInitialized = new cEncumbranceController();
                }
                return encController_AutoInitialized;
            }
            set
            {
                encController_AutoInitialized = value;
            }
        }

        private cCreditMemoController credController_AutoInitialized;

        private cCreditMemoController credController
        {
            get
            {
                if (credController_AutoInitialized == null)
                {
                    credController_AutoInitialized = new cCreditMemoController();
                }
                return credController_AutoInitialized;
            }
            set
            {
                credController_AutoInitialized = value;
            }
        }

        private cVendorController vendController_AutoInitialized;

        private cVendorController vendController
        {
            get
            {
                if (vendController_AutoInitialized == null)
                {
                    vendController_AutoInitialized = new cVendorController();
                }
                return vendController_AutoInitialized;
            }
            set
            {
                vendController_AutoInitialized = value;
            }
        }

        private cBDSettings bdSettings;
        //FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
        //private cBDSettingsController bdSetCont = new cBDSettingsController();
        private cBDSettingsController bdSetCont_AutoInitialized;

        private cBDSettingsController bdSetCont
        {
            get
            {
                if (bdSetCont_AutoInitialized == null)
                {
                    bdSetCont_AutoInitialized = new cBDSettingsController();
                }
                return bdSetCont_AutoInitialized;
            }
            set
            {
                bdSetCont_AutoInitialized = value;
            }
        }

        const int POSTING = 13;

        private struct BegBalInfo
        {
            public string Account;
            // vbPorter upgrade warning: OldBalance As Decimal	OnWrite(double, Decimal, short)
            public Decimal OldBalance;
            // vbPorter upgrade warning: NewBalance As Decimal	OnWrite(Decimal, short, double)
            public Decimal NewBalance;
            //FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
            public BegBalInfo(int unusedParam)
            {
                this.Account = String.Empty;
                this.OldBalance = 0;
                this.NewBalance = 0;
            }
        };

        public string LastErrorMessage
        {
            get
            {
                string LastErrorMessage = "";
                LastErrorMessage = strLastError;
                return LastErrorMessage;
            }
        }

        public int LastErrorNumber
        {
            get
            {
                int LastErrorNumber = 0;
                LastErrorNumber = lngLastError;
                return LastErrorNumber;
            }
        }
        // vbPorter upgrade warning: 'Return' As Variant --> As bool
        public bool HadError
        {
            get
            {
                bool HadError = false;
                HadError = lngLastError != 0;
                return HadError;
            }
        }

        public void ClearErrors()
        {
            strLastError = "";
            lngLastError = 0;
        }

        public cJournal GetJournalByNumber(int lngJournalNumber)
        {
            cJournal GetJournalByNumber = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                GetJournalByNumber = jController.GetJournalByNumber(lngJournalNumber);
                if (jController.LastErrorNumber != 0)
                {
                    lngLastError = jController.LastErrorNumber;
                    strLastError = jController.LastErrorMessage;
                }
                return GetJournalByNumber;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
            return GetJournalByNumber;
        }

        public cJournal GetFullJournalByNumber(int lngJournalNumber)
        {
            cJournal GetFullJournalByNumber = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                GetFullJournalByNumber = jController.GetFullJournalByNumber(lngJournalNumber);
                if (jController.LastErrorNumber != 0)
                {
                    lngLastError = jController.LastErrorNumber;
                    strLastError = jController.LastErrorMessage;
                }
                return GetFullJournalByNumber;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
            return GetFullJournalByNumber;
        }

        public cJournal GetFullJournal(int lngID)
        {
            cJournal GetFullJournal = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                GetFullJournal = jController.GetFullJournal(lngID);
                if (jController.LastErrorNumber != 0)
                {
                    lngLastError = jController.LastErrorNumber;
                    strLastError = jController.LastErrorMessage;
                }
                return GetFullJournal;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
            return GetFullJournal;
        }

        public cGenericCollection GetUnpostedJournalInfos()
        {
            cGenericCollection GetUnpostedJournalInfos = null;
            cGenericCollection gColl;
            cGenericCollection iColl = new cGenericCollection();
            cJournalInfo jInfo;
            cJournal journ;

            ClearErrors();
            try
            {
	            gColl = jController.GetUnpostedJournals();
	            gColl.MoveFirst();
	            while (gColl.IsCurrent())
	            {
		            ////////Application.DoEvents();
		            jInfo = new cJournalInfo();
		            journ = (cJournal) gColl.GetCurrentItem();
		            jInfo.Description = journ.Description;
		            jInfo.Id = journ.ID;
		            jInfo.IsSelected = false;
		            jInfo.JournalNumber = journ.JournalNumber;
		            jInfo.IsCreditMemo = journ.IsCreditMemo;
		            jInfo.IsCreditMemoCorrection = journ.IsCreditMemoCorrection;
		            jInfo.Status = journ.Status;
		            jInfo.JournalType = journ.JournalType;
		            jInfo.Period = journ.Period;
		            if (jInfo.JournalType != "AP" && jInfo.JournalType != "AC")
		            {
			            jInfo.NumberOfEntries = jController.GetJournalEntryCount(jInfo.JournalNumber);
			            jInfo.TotalAmount = jController.GetJournalEntriesAmount(jInfo.JournalNumber);
			            jInfo.IsOutOfBalance = jController.IsJournalOutOfBalance(jInfo.JournalNumber, jInfo.JournalType);
		            }
		            else
		            {
			            jInfo.NumberOfEntries = apjController.GetJournalEntryCount(jInfo.JournalNumber);
			            jInfo.TotalAmount = apjController.GetJournalAmount(jInfo.JournalNumber);
			            jInfo.IsOutOfBalance = false;
		            }

		            jInfo.ContainsBadAccount = modBudgetaryAccounting.BadAccountsInJournal(jInfo.JournalNumber);

		            if (jInfo.JournalType == "GJ" || jInfo.JournalType == "CW")
		            {
			            jInfo.IsOutOfBalanceByFund = !modBudgetaryAccounting.CheckOBF(jInfo.JournalNumber, jInfo.JournalType);
                    }
		            else
		            {
			            jInfo.IsOutOfBalanceByFund = false;
		            }
                        
                    
                    iColl.AddItem(jInfo);
		            gColl.MoveNext();
	            }

	            GetUnpostedJournalInfos = iColl;
	            return GetUnpostedJournalInfos;
            }
            catch (Exception ex)
            {
	            lngLastError = Information.Err(ex).Number;
	            strLastError = ex.GetBaseException().Message;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                return GetUnpostedJournalInfos;
            }
        }

        public cGenericCollection GetPostedJournalInfos()
        {
            cGenericCollection GetPostedJournalInfos = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                cGenericCollection gColl;
                cGenericCollection iColl = new cGenericCollection();
                cJournalInfo jInfo;
                cJournal journ;
                gColl = jController.GetPostedJournals();
                gColl.MoveFirst();
                while (gColl.IsCurrent())
                {
                    ////////Application.DoEvents();
                    jInfo = new cJournalInfo();
                    journ = (cJournal)gColl.GetCurrentItem();
                    jInfo.Description = journ.Description;
                    jInfo.Id = journ.ID;
                    jInfo.IsSelected = false;
                    jInfo.JournalNumber = journ.JournalNumber;
                    jInfo.Status = journ.Status;
                    jInfo.JournalType = journ.JournalType;
                    jInfo.Period = journ.Period;
                    jInfo.IsCreditMemo = journ.IsCreditMemo;
                    jInfo.IsCreditMemoCorrection = journ.IsCreditMemoCorrection;
                    jInfo.TotalAmount = journ.TotalAmount;
                    if (jInfo.JournalType != "AP" && jInfo.JournalType != "AC")
                    {
                        jInfo.NumberOfEntries = jController.GetJournalEntryCount(jInfo.JournalNumber);
                        jInfo.PostedDate = jController.GetPostedDate(jInfo.JournalNumber);
                    }
                    else
                    {
                        jInfo.NumberOfEntries = apjController.GetJournalEntryCount(jInfo.JournalNumber);
                        jInfo.PostedDate = apjController.GetPostedDate(jInfo.JournalNumber);
                    }
                    iColl.AddItem(jInfo);
                    gColl.MoveNext();
                }
                GetPostedJournalInfos = iColl;
                return GetPostedJournalInfos;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
            return GetPostedJournalInfos;
        }

        public bool PostedJournalExists(int lngJournalNumber)
        {
            bool PostedJournalExists = false;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                cJournal journ;
                journ = jController.GetJournalByNumber(lngJournalNumber);
                if (!(journ == null))
                {
                    if (journ.Status != "P")
                    {
                        PostedJournalExists = false;
                    }
                    else
                    {
                        PostedJournalExists = true;
                    }
                }
                else
                {
                    PostedJournalExists = false;
                }
                return PostedJournalExists;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
            return PostedJournalExists;
        }

        public bool UnpostedJournalExists(int lngJournalNumber)
        {
            bool UnpostedJournalExists = false;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                cJournal journ;
                journ = jController.GetJournalByNumber(lngJournalNumber);
                if (!(journ == null))
                {
                    if (journ.Status == "P" || journ.Status == "D")
                    {
                        UnpostedJournalExists = false;
                    }
                    else
                    {
                        UnpostedJournalExists = true;
                    }
                }
                else
                {
                    UnpostedJournalExists = false;
                }
                return UnpostedJournalExists;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
            return UnpostedJournalExists;
        }

        public void DeleteUnpostedJournal(int lngID)
        {
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                cJournal journ;
                int lngRecordCount;
                journ = jController.GetJournal(lngID);
                lngRecordCount = 0;
                if (!(journ == null))
                {
                    if (Strings.LCase(journ.Status) == "e")
                    {
                        cGenericCollection encCollection;
                        cEncumbrance enc;
                        cAPJournal apJourn;
                        cGenericCollection apJournals;
                        cGenericCollection credMemos;
                        cCreditMemo credMemo;
                        modGlobalFunctions.AddCYAEntry_26("BD", "Deleted journal " + FCConvert.ToString(journ.JournalNumber) + " on " + DateTime.Today.ToShortDateString(), "Delete Unposted Journal");
                        jController.SetJournalToDeleted(lngID);
                        if (jController.LastErrorNumber == 0)
                        {
                            if (Strings.LCase(journ.JournalType) == "en")
                            {
                                encCollection = encController.GetEncumbrancesByJournalNumber(journ.JournalNumber);
                                encCollection.MoveFirst();
                                while (encCollection.IsCurrent())
                                {
                                    enc = (cEncumbrance)encCollection.GetCurrentItem();
                                    // lngRecordCount = lngRecordCount + encController.GetEncumbranceDetailCount(enc.ID)
                                    encController.DeleteEncumbranceDetailsByEncumbranceID(enc.ID);
                                    encController.DeleteEncumbrance(enc.ID);
                                    encCollection.MoveNext();
                                }
                            }
                            else if (Strings.LCase(journ.JournalType) == "ap")
                            {
                                RemoveExistingCreditMemos_2(journ.JournalNumber);
                                apJournals = apjController.GetEnteredJournalsByJournalNumber(journ.JournalNumber);
                                apJournals.MoveFirst();
                                while (apJournals.IsCurrent())
                                {
                                    apJourn = (cAPJournal)apJournals.GetCurrentItem();
                                    // lngRecordCount = lngRecordCount + apjController.GetAPJournalDetailCount(apJourn.ID)
                                    apjController.DeleteAPJournalDetailsByAPJournalID(apJourn.ID);
                                    apjController.DeleteAPJournal(apJourn.ID);
                                    apJournals.MoveNext();
                                }
                            }
                            else if (Strings.LCase(journ.JournalType) == "ac")
                            {
                                credMemos = credController.GetEnteredCreditMemosByJournalNumber(journ.JournalNumber);
                                credMemos.MoveFirst();
                                while (credMemos.IsCurrent())
                                {
                                    credMemo = (cCreditMemo)credMemos.GetCurrentItem();
                                    credController.DeleteCreditMemoDetailsByCreditMemoID(credMemo.ID);
                                    credController.DeleteCreditMemo(credMemo.ID);
                                    credMemos.MoveNext();
                                }
                                apJournals = apjController.GetEnteredJournalsByJournalNumber(journ.JournalNumber);
                                apJournals.MoveFirst();
                                while (apJournals.IsCurrent())
                                {
                                    apJourn = (cAPJournal)apJournals.GetCurrentItem();
                                    // lngRecordCount = lngRecordCount + apjController.GetAPJournalDetailCount(apJourn.ID)
                                    apjController.DeleteAPJournalDetailsByAPJournalID(apJourn.ID);
                                    apjController.DeleteAPJournal(apJourn.ID);
                                    apJournals.MoveNext();
                                }
                            }
                            else
                            {
                                jController.DeleteJournalDetailsByJournalNumber(journ.JournalNumber);
                            }
                            modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(journ.JournalNumber), true);
                        }
                        else
                        {
                            lngLastError = jController.LastErrorNumber;
                            strLastError = jController.LastErrorMessage;
                            return;
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
        }

        public void PostJournals(ref cGenericCollection journalList, bool boolPageBreakAfterJournals)
        {
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                if (!(journalList == null))
                {
                    if (journalList.ItemCount() > 0)
                    {
                        clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
                        clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
                        cJournalInfo jInfo;
                        cJournal journ;
                        clsPostInfo.ClearJournals();
                        journalList.MoveFirst();
                        while (journalList.IsCurrent())
                        {
                            ////////Application.DoEvents();
                            jInfo = (cJournalInfo)journalList.GetCurrentItem();
                            journ = jController.GetJournal(jInfo.Id);
                            clsJournalInfo = new clsPostingJournalInfo();
                            clsJournalInfo.JournalNumber = jInfo.JournalNumber;
                            clsJournalInfo.Period = FCConvert.ToString(jInfo.Period);
                            if (Strings.LCase(Strings.Trim(jInfo.JournalType)) == "crw")
                            {
                                clsJournalInfo.JournalType = "CW";
                            }
                            else
                            {
                                clsJournalInfo.JournalType = Strings.Trim(jInfo.JournalType);
                            }
                            clsJournalInfo.CheckDate = journ.CheckDate;
                            clsPostInfo.AddJournal(clsJournalInfo);
                            journalList.MoveNext();
                        }
                        clsPostInfo.PageBreakBetweenJournalsOnReport = boolPageBreakAfterJournals;
                        clsPostInfo.AllowPreview = true;
                        if (clsPostInfo.PostJournals())
                        {
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
        }

        public void PostJournal(cJournal journ)
        {
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                if (!(journ == null))
                {
                    clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
                    clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
                    clsJournalInfo.JournalNumber = journ.JournalNumber;
                    clsJournalInfo.Period = FCConvert.ToString(journ.Period);
                    if (Strings.LCase(Strings.Trim(journ.JournalType)) == "crw")
                    {
                        clsJournalInfo.JournalType = "CW";
                    }
                    else
                    {
                        clsJournalInfo.JournalType = Strings.Trim(journ.JournalType);
                    }
                    clsJournalInfo.CheckDate = journ.CheckDate;
                    clsPostInfo.AddJournal(clsJournalInfo);
                    clsPostInfo.AllowPreview = true;
                    clsPostInfo.PostJournals();
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
            }
        }

        public int CreateJournal(string strDescription, string strType, int intPeriod)
        {
            int CreateJournal = 0;
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                int lngJournalID;
                lngJournalID = jController.CreateJournal(strDescription, strType, intPeriod);
                if (lngJournalID == 0)
                {
                    lngLastError = jController.LastErrorNumber;
                    strLastError = jController.LastErrorMessage;
                }
                CreateJournal = lngJournalID;
                return CreateJournal;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
            return CreateJournal;
        }

        public void UpdateJournalStatus(int lngJournalNumber, string strorigStatus, string strNewStatus)
        {
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                jController.UpdateJournalStatus(lngJournalNumber, strorigStatus, strNewStatus);
                if (jController.LastErrorNumber != 0)
                {
                    lngLastError = jController.LastErrorNumber;
                    strLastError = jController.LastErrorMessage;
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
        }

        public void UpdateAPJournalStatus(int lngJournalNumber, string strorigStatus, string strNewStatus)
        {
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                apjController.UpdateJournalStatus(lngJournalNumber, strorigStatus, strNewStatus);
                if (apjController.LastErrorNumber != 0)
                {
                    lngLastError = apjController.LastErrorNumber;
                    strLastError = apjController.LastErrorMessage;
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                lngLastError = Information.Err(ex).Number;
                strLastError = ex.GetBaseException().Message;
            }
        }

        public void UpdateJournalDescription(int lngID, string strDescription)
        {
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                if (strDescription != "")
                {
                    apjController.UpdateJournalDescription(lngID, strDescription);
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
            }
        }

        public void CreateOpeningAdjustments()
        {
            string strCurDir;
            string strArchivePath = "";
            clsDRWrapper rsArchiveInfo = new clsDRWrapper();
            clsDRWrapper rsInfo = new clsDRWrapper();
            // vbPorter upgrade warning: ans As short, int --> As DialogResult
            DialogResult ans;
            clsDRWrapper rsArchiveLedgerYTDActivity = new clsDRWrapper();
            clsDRWrapper rsLedgerBegBalInfo = new clsDRWrapper();
            clsDRWrapper rsLedgerEndingBalanceInfo = new clsDRWrapper();
            int lngJournalNumber = 0;
            int lngEOYAPJournalNumber = 0;
            clsDRWrapper rsJournalInfo = new clsDRWrapper();
            Decimal curOldBalance;
            Decimal curNewBalance;
            clsDRWrapper Master = new clsDRWrapper();
            string strTempAccount = "";
            bool blnAlternateCloseoutAccounts;
            int lngYear = 0;
            int counter = 0;
            int lngIndex = 0;
            int lngFundBalIndex = 0;
            clsDRWrapper rsDoNotCalc = new clsDRWrapper();
            clsDRWrapper rsCheckRecArchive = new clsDRWrapper();
            clsDRWrapper rsCheckRec = new clsDRWrapper();
            clsDRWrapper rsEOYProcess = new clsDRWrapper();
            cArchiveUtility autPastYear = new cArchiveUtility();
            int intDataID = 0;
            int intCounter;
            clsBudgetaryPosting clsPostInfo = new clsBudgetaryPosting();
            clsPostingJournalInfo clsJournalInfo = new clsPostingJournalInfo();
            BegBalInfo[] bbiInfo = null;
            string[] strAry;
            bool boolIsFBCtr;
            bbiInfo = new BegBalInfo[0 + 1];
            ans = MessageBox.Show("This function should ONLY be used if additional entries have been made in your archive directory and these changes need to be reflected in the beginning balances for the current year.  Do you wish to continue?", "Proceed?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (ans == DialogResult.No)
                return;

            try
            {
                strCurDir = Application.StartupPath;
                if (modGlobalConstants.Statics.gstrArchiveYear != "")
                {
                    rsInfo.OpenRecordset("SELECT * FROM Archives WHERE ArchiveType = 'Archive' AND ArchiveID < " + FCConvert.ToString(Conversion.Val(modGlobalConstants.Statics.gstrArchiveYear)) + " ORDER BY ArchiveID DESC", "SystemSettings");
                }
                else
                {
                    rsInfo.OpenRecordset("SELECT * FROM Archives WHERE ArchiveType = 'Archive' ORDER BY ArchiveID DESC", "SystemSettings");
                }
                if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                {
                    intDataID = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ArchiveID"));
                    strArchivePath = autPastYear.DeriveGroupName("Archive", intDataID);
                    clsDRWrapper rsTest = new clsDRWrapper();
                    rsTest.GroupName = strArchivePath;
                    if (!rsTest.DBExists("Budgetary"))
                    {
                        MessageBox.Show("Could not connect to Budgetary archive database for " + FCConvert.ToString(intDataID), "Bad Connection Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        return;
                    }
                    if (MessageBox.Show("This will create opening adjustments from archive year of " + FCConvert.ToString(intDataID) + ". Is this correct?", FCConvert.ToString(lngYear) + " Correct?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                    {
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("No previous years available.", "No Previous Year Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                frmWait.InstancePtr.Init("Please Wait" + "\r\n" + "Preparing Data...");
                // troges126
                // CalculateAccountInfo True, True, False, "G"
                // CalculateAccountInfo True, True, False, "G"
                modBudgetaryAccounting.CalculateAccountInfo(strArchivePath);
                modBudgetaryAccounting.CalculateAccountInfo();
                rsArchiveLedgerYTDActivity.GroupName = strArchivePath;
                rsLedgerEndingBalanceInfo.OpenRecordset("SELECT DISTINCT Year FROM PastBudgets ORDER BY Year DESC");
                // frmWait.StopBusy
                // Unload frmWait
                if (rsLedgerEndingBalanceInfo.EndOfFile() != true && rsLedgerEndingBalanceInfo.BeginningOfFile() != true)
                {
                    lngYear = FCConvert.ToInt32(rsLedgerEndingBalanceInfo.Get_Fields("Year"));
                }
                else
                {
                    MessageBox.Show("No previous balance info found.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                // frmWait.Init "Please Wait" & vbNewLine & "Preparing Data..."
                // If gboolSchoolAccounts Then
                // rsLedgerEndingBalanceInfo.OpenRecordset "SELECT * FROM PastBudgets WHERE Year = " & lngYear & " AND (Left(Account, 1) = 'G' or Left(Account, 1) = 'L') ORDER BY Account"
                // Else
                rsLedgerEndingBalanceInfo.OpenRecordset("SELECT * FROM PastBudgets WHERE Year = " + FCConvert.ToString(lngYear) + " AND Left(Account, 1) = 'G' ORDER BY Account");
                // End If
                if (rsLedgerEndingBalanceInfo.EndOfFile() != true && rsLedgerEndingBalanceInfo.BeginningOfFile() != true)
                {
                    counter = 0;

                    do
                    {
                        ////////Application.DoEvents();
                        Array.Resize(ref bbiInfo, counter + 1);
                        // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                        bbiInfo[counter].Account = FCConvert.ToString(rsLedgerEndingBalanceInfo.Get_Fields("Account"));
                        //bbiInfo[counter].OldBalance = FCConvert.ToDecimal(rsLedgerEndingBalanceInfo.Get_Fields("EndBudget") + rsLedgerEndingBalanceInfo.Get_Fields("ActualSpent")); // + rsLedgerEndingBalanceInfo.Get_Fields("BalanceAdjustments")
                        boolIsFBCtr = false;
                        // TODO: Check the table for the column [account] and replace with corresponding Get_Field method
                        if (Strings.Left(rsLedgerEndingBalanceInfo.Get_Fields("account"), 1) == "G")
                        {
                            strAry = Strings.Split(bbiInfo[counter].Account, "-");
                            if (strAry[1] == modBudgetaryAccounting.Statics.strFundBalCtrAccount)
                            {
                                boolIsFBCtr = true;
                            }
                        }
                        if (!boolIsFBCtr)
                        {
                            bbiInfo[counter].OldBalance = rsLedgerEndingBalanceInfo.Get_Fields_Decimal("StartBudget") + rsLedgerEndingBalanceInfo.Get_Fields_Decimal("ActualSpent") + rsLedgerEndingBalanceInfo.Get_Fields_Decimal("BudgetAdjustments");
                        }
                        else
                        {
                            bbiInfo[counter].OldBalance = rsLedgerEndingBalanceInfo.Get_Fields_Decimal("EndBudget") + rsLedgerEndingBalanceInfo.Get_Fields_Decimal("ActualSpent");
                            // + Val(rsLedgerEndingBalanceInfo.Fields("BudgetAdjustments"))
                        }
                        counter += 1;
                        rsLedgerEndingBalanceInfo.MoveNext();
                    }
                    while (rsLedgerEndingBalanceInfo.EndOfFile() != true);
                }
                GetNewBalanceInfo(ref strArchivePath, ref bbiInfo);
                for (counter = 1; counter <= 99; counter++)
                {
                    ////////Application.DoEvents();
                    if (modAccountTitle.Statics.YearFlag)
                    {
                        lngIndex = GetCorrectIndex_2("G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))))) + "-" + modBudgetaryAccounting.Statics.strExpCtrAccount, bbiInfo);
                    }
                    else
                    {
                        lngIndex = GetCorrectIndex_2("G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))))) + "-" + modBudgetaryAccounting.Statics.strExpCtrAccount + "-00", bbiInfo);
                    }
                    if (lngIndex >= 0)
                    {
                        if (modAccountTitle.Statics.YearFlag)
                        {
                            lngFundBalIndex = GetCorrectIndex_2("G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))))) + "-" + modBudgetaryAccounting.Statics.strFundBalCtrAccount, bbiInfo);
                        }
                        else
                        {
                            lngFundBalIndex = GetCorrectIndex_2("G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))))) + "-" + modBudgetaryAccounting.Statics.strFundBalCtrAccount + "-00", bbiInfo);
                        }
                        if (lngFundBalIndex < 0)
                        {
                            Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                            if (modAccountTitle.Statics.YearFlag)
                            {
                                bbiInfo[Information.UBound(bbiInfo)].Account = "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modBudgetaryAccounting.Statics.strFundBalCtrAccount;
                            }
                            else
                            {
                                bbiInfo[Information.UBound(bbiInfo)].Account = "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modBudgetaryAccounting.Statics.strFundBalCtrAccount + "-00";
                            }
                            bbiInfo[Information.UBound(bbiInfo)].NewBalance = bbiInfo[lngIndex].NewBalance;
                            bbiInfo[lngIndex].NewBalance = 0;
                        }
                        else
                        {
                            bbiInfo[lngFundBalIndex].NewBalance += bbiInfo[lngIndex].NewBalance;
                            bbiInfo[lngIndex].NewBalance = 0;
                        }
                    }
                    if (modAccountTitle.Statics.YearFlag)
                    {
                        lngIndex = GetCorrectIndex_2("G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))))) + "-" + modBudgetaryAccounting.Statics.strRevCtrAccount, bbiInfo);
                    }
                    else
                    {
                        lngIndex = GetCorrectIndex_2("G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))))) + "-" + modBudgetaryAccounting.Statics.strRevCtrAccount + "-00", bbiInfo);
                    }
                    if (lngIndex >= 0)
                    {
                        if (modAccountTitle.Statics.YearFlag)
                        {
                            lngFundBalIndex = GetCorrectIndex_2("G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))))) + "-" + modBudgetaryAccounting.Statics.strFundBalCtrAccount, bbiInfo);
                        }
                        else
                        {
                            lngFundBalIndex = GetCorrectIndex_2("G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))))) + "-" + modBudgetaryAccounting.Statics.strFundBalCtrAccount + "-00", bbiInfo);
                        }
                        if (lngFundBalIndex < 0)
                        {
                            Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                            if (modAccountTitle.Statics.YearFlag)
                            {
                                bbiInfo[Information.UBound(bbiInfo)].Account = "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modBudgetaryAccounting.Statics.strFundBalCtrAccount;
                            }
                            else
                            {
                                bbiInfo[Information.UBound(bbiInfo)].Account = "G " + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modBudgetaryAccounting.Statics.strFundBalCtrAccount + "-00";
                            }
                            bbiInfo[Information.UBound(bbiInfo)].NewBalance = bbiInfo[lngIndex].NewBalance;
                            bbiInfo[lngIndex].NewBalance = 0;
                        }
                        else
                        {
                            bbiInfo[lngFundBalIndex].NewBalance += bbiInfo[lngIndex].NewBalance;
                            bbiInfo[lngIndex].NewBalance = 0;
                        }
                    }
                }
                rsLedgerBegBalInfo.OpenRecordset("SELECT Account, SUM(AutomaticBudgetAdjustments) as BudgetAdjustmentsTotal FROM LedgerReportInfo GROUP BY Account HAVING SUM(AutomaticBudgetAdjustments) <> 0 ORDER BY Account");
                if (rsLedgerBegBalInfo.EndOfFile() != true && rsLedgerBegBalInfo.BeginningOfFile() != true)
                {
                    do
                    {
                        ////////Application.DoEvents();
                        // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                        lngIndex = GetCorrectIndex_2(rsLedgerBegBalInfo.Get_Fields_String("Account"), bbiInfo);
                        if (lngIndex < 0)
                        {
                            Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                            // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                            bbiInfo[Information.UBound(bbiInfo)].Account = FCConvert.ToString(rsLedgerBegBalInfo.Get_Fields("Account"));
                            // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                            bbiInfo[Information.UBound(bbiInfo)].OldBalance = FCConvert.ToDecimal(rsLedgerBegBalInfo.Get_Fields("BudgetAdjustmentsTotal"));
                            bbiInfo[Information.UBound(bbiInfo)].NewBalance = 0;
                        }
                        else
                        {
                            // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                            bbiInfo[lngIndex].OldBalance += rsLedgerBegBalInfo.Get_Fields("BudgetAdjustmentsTotal");
                        }
                        rsLedgerBegBalInfo.MoveNext();
                    }
                    while (rsLedgerBegBalInfo.EndOfFile() != true);
                }
                // EOY Adjustments
                rsLedgerBegBalInfo.OpenRecordset("SELECT * FROM EOYAdjustments");
                if (rsLedgerBegBalInfo.BeginningOfFile() != true && rsLedgerBegBalInfo.EndOfFile() != true)
                {
                    do
                    {
                        ////////Application.DoEvents();
                        // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                        lngIndex = GetCorrectIndex_2(rsLedgerBegBalInfo.Get_Fields_String("Account"), bbiInfo);
                        if (lngIndex < 0)
                        {
                            Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                            // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                            bbiInfo[Information.UBound(bbiInfo)].Account = FCConvert.ToString(rsLedgerBegBalInfo.Get_Fields("Account"));
                            // TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            bbiInfo[Information.UBound(bbiInfo)].NewBalance = FCConvert.ToDecimal(rsLedgerBegBalInfo.Get_Fields("Amount"));
                            bbiInfo[Information.UBound(bbiInfo)].NewBalance = 0;
                        }
                        else
                        {
                            // TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            bbiInfo[lngIndex].NewBalance += rsLedgerBegBalInfo.Get_Fields("Amount");
                        }
                        rsLedgerBegBalInfo.MoveNext();
                    }
                    while (rsLedgerBegBalInfo.EndOfFile() != true);
                }
                clsPostInfo.ClearJournals();
                if (Information.UBound(bbiInfo, 1) > 0)
                {
                    frmWait.InstancePtr.Init("Please Wait" + "\r\n" + "Creating Entries...", true, Information.UBound(bbiInfo, 1));
                    frmWait.InstancePtr.prgProgress.Value = 0;
                    lngJournalNumber = 0;
                    rsJournalInfo.OpenRecordset("SELECT * FROM JournalEntries");
                    rsDoNotCalc.OpenRecordset("SELECT * FROM DoNotCalculate");
                    for (counter = 0; counter <= Information.UBound(bbiInfo, 1); counter++)
                    {
                        ////////Application.DoEvents();
                        if (frmWait.InstancePtr.prgProgress.Value < frmWait.InstancePtr.prgProgress.Maximum)
                        {
                            frmWait.InstancePtr.prgProgress.Value = frmWait.InstancePtr.prgProgress.Value + 1;
                        }
                        if (rsDoNotCalc.FindFirstRecord("Account", bbiInfo[counter].Account))
                        {
                            goto CheckNext;
                        }
                        curOldBalance = bbiInfo[counter].OldBalance;
                        curNewBalance = bbiInfo[counter].NewBalance;
                        if (curOldBalance != curNewBalance)
                        {
                            if (lngJournalNumber == 0)
                            {
                                if (modBudgetaryAccounting.LockJournal() == false)
                                {
                                    frmWait.InstancePtr.Unload();
                                    ////////Application.DoEvents();
                                    MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
                                if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
                                {
                                    Master.MoveLast();
                                    Master.MoveFirst();
                                    // TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                                    lngJournalNumber = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
                                }
                                else
                                {
                                    lngJournalNumber = 1;
                                }
                                Master.AddNew();
                                Master.Set_Fields("JournalNumber", lngJournalNumber);
                                Master.Set_Fields("Status", "E");
                                Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
                                Master.Set_Fields("StatusChangeDate", DateTime.Today);
                                Master.Set_Fields("Description", "Beg Bal Adjustments");
                                Master.Set_Fields("Type", "GJ");
                                Master.Set_Fields("Period", FCConvert.ToString(Conversion.Val(bdSettings.FiscalStart)));
                                Master.Set_Fields("AutomaticJournal", true);
                                clsJournalInfo = new clsPostingJournalInfo();
                                clsJournalInfo.JournalNumber = lngJournalNumber;
                                clsJournalInfo.JournalType = "GJ";
                                clsJournalInfo.Period = bdSettings.FiscalStart;
                                clsJournalInfo.CheckDate = "";
                                clsPostInfo.AddJournal(clsJournalInfo);
                                Master.Update();
                                Master.Reset();
                                modBudgetaryAccounting.UnlockJournal();
                            }
                            rsJournalInfo.OmitNullsOnInsert = true;
                            rsJournalInfo.AddNew();
                            rsJournalInfo.Set_Fields("Type", "G");
                            rsJournalInfo.Set_Fields("JournalEntriesDate", DateTime.Today);
                            rsJournalInfo.Set_Fields("Description", "Beg Bal Adjustments");
                            rsJournalInfo.Set_Fields("JournalNumber", lngJournalNumber);
                            rsJournalInfo.Set_Fields("Period", bdSettings.FiscalStart);
                            rsJournalInfo.Set_Fields("Project", "");
                            rsJournalInfo.Set_Fields("account", bbiInfo[counter].Account);
                            rsJournalInfo.Set_Fields("RCB", "B");
                            rsJournalInfo.Set_Fields("Amount", curNewBalance - curOldBalance);
                            rsJournalInfo.Set_Fields("Status", "E");
                            rsJournalInfo.Update();
                        }
                    CheckNext:
                        ;
                    }
                    rsCheckRecArchive.GroupName = strArchivePath;
                    rsEOYProcess.OpenRecordset("SELECT * FROM EOYProcess");
                    if (Information.IsDate(rsEOYProcess.Get_Fields("EOYCompletedDate")))
                    {
                        rsCheckRecArchive.Execute("UPDATE CheckRecMaster SET Transferred = 1 WHERE StatusDate <= '" + rsEOYProcess.Get_Fields_DateTime("EOYCompletedDate") + "'", "Budgetary");
                    }
                    rsCheckRecArchive.OpenRecordset("SELECT * FROM CheckRecMaster WHERE Transferred = 0");
                    if (rsCheckRecArchive.EndOfFile() != true && rsCheckRecArchive.BeginningOfFile() != true)
                    {
                        do
                        {
                            ////////Application.DoEvents();
                            rsCheckRec.OmitNullsOnInsert = true;
                            // TODO: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                            // TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            rsCheckRec.OpenRecordset("SELECT * FROM CheckRecMaster WHERE CheckNumber = " + rsCheckRecArchive.Get_Fields("CheckNumber") + " AND Amount = " + rsCheckRecArchive.Get_Fields("Amount"));
                            // TODO: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                            if (FCConvert.ToInt32(rsCheckRecArchive.Get_Fields("CheckNumber")) == 0)
                            {
                                rsCheckRec.AddNew();
                            }
                            else
                            {
                                if (rsCheckRec.EndOfFile() != true && rsCheckRec.BeginningOfFile() != true)
                                {
                                    rsCheckRec.Edit();
                                }
                                else
                                {
                                    rsCheckRec.AddNew();
                                }
                            }
                            // TODO: Check the table for the column [CheckNumber] and replace with corresponding Get_Field method
                            rsCheckRec.Set_Fields("CheckNumber", rsCheckRecArchive.Get_Fields("CheckNumber"));
                            // TODO: Check the table for the column [Type] and replace with corresponding Get_Field method
                            rsCheckRec.Set_Fields("Type", rsCheckRecArchive.Get_Fields("Type"));
                            rsCheckRec.Set_Fields("CheckDate", rsCheckRecArchive.Get_Fields_DateTime("CheckDate"));
                            rsCheckRec.Set_Fields("Name", rsCheckRecArchive.Get_Fields_String("Name"));
                            // TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            rsCheckRec.Set_Fields("Amount", rsCheckRecArchive.Get_Fields("Amount"));
                            rsCheckRec.Set_Fields("Status", rsCheckRecArchive.Get_Fields_String("Status"));
                            rsCheckRec.Set_Fields("StatusDate", rsCheckRecArchive.Get_Fields_DateTime("StatusDate"));
                            // TODO: Check the table for the column [BankNumber] and replace with corresponding Get_Field method
                            rsCheckRec.Set_Fields("BankNumber", rsCheckRecArchive.Get_Fields("BankNumber"));
                            rsCheckRec.Set_Fields("DirectDeposit", rsCheckRecArchive.Get_Fields_Boolean("DirectDeposit"));
                            rsCheckRec.Set_Fields("CRPeriodCloseoutKey", rsCheckRecArchive.Get_Fields_Int32("CRPeriodCloseoutKey"));
                            rsCheckRec.Set_Fields("PYPayDate", rsCheckRecArchive.Get_Fields_DateTime("PYPayDate"));
                            rsCheckRec.Set_Fields("PYPayRunID", rsCheckRecArchive.Get_Fields_Int32("PYPayRunID"));
                            rsCheckRec.Set_Fields("CDEntry", rsCheckRecArchive.Get_Fields_Boolean("CDEntry"));
                            rsCheckRec.Set_Fields("Transferred", true);
                            rsCheckRec.Update();
                            rsCheckRecArchive.Edit();
                            rsCheckRecArchive.Set_Fields("Transferred", true);
                            rsCheckRecArchive.Update();
                            rsCheckRecArchive.MoveNext();
                        }
                        while (rsCheckRecArchive.EndOfFile() != true);
                    }
                    rsArchiveInfo.GroupName = strArchivePath;
                    rsArchiveInfo.OpenRecordset("SELECT * FROM ArchiveAPAmounts WHERE Transferred = 0");
                    if (rsArchiveInfo.EndOfFile() != true && rsArchiveInfo.BeginningOfFile() != true)
                    {
                        rsJournalInfo.OpenRecordset("SELECT * FROM JournalEntries WHERE ID = 0");
                        do
                        {
                            ////////Application.DoEvents();
                            if (lngEOYAPJournalNumber == 0)
                            {
                                if (modBudgetaryAccounting.LockJournal() == false)
                                {
                                    frmWait.InstancePtr.Unload();
                                    ////////Application.DoEvents();
                                    MessageBox.Show("User " + modBudgetaryAccounting.Statics.strLockedBy + " is currently trying to save a record.  You must wait until they are finished to save.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return;
                                }
                                Master.OpenRecordset("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC");
                                if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
                                {
                                    Master.MoveLast();
                                    Master.MoveFirst();
                                    // TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                                    lngEOYAPJournalNumber = FCConvert.ToInt32(Master.Get_Fields("JournalNumber")) + 1;
                                }
                                else
                                {
                                    lngEOYAPJournalNumber = 1;
                                }
                                Master.AddNew();
                                Master.Set_Fields("JournalNumber", lngEOYAPJournalNumber);
                                Master.Set_Fields("Status", "E");
                                Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
                                Master.Set_Fields("StatusChangeDate", DateTime.Today);
                                Master.Set_Fields("Description", "EOY AP Cash Adjustments");
                                Master.Set_Fields("Type", "GJ");
                                Master.Set_Fields("Period", bdSettings.FiscalStart);
                                Master.Set_Fields("AutomaticJournal", true);
                                Master.Update();
                                Master.Reset();
                                modBudgetaryAccounting.UnlockJournal();
                            }
                            rsJournalInfo.OmitNullsOnInsert = true;
                            rsJournalInfo.AddNew();
                            rsJournalInfo.Set_Fields("Type", "G");
                            rsJournalInfo.Set_Fields("JournalEntriesDate", DateTime.Today);
                            // TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                            rsJournalInfo.Set_Fields("Description", "Prior Year Jrnl " + Strings.Format(rsArchiveInfo.Get_Fields("JournalNumber"), "0000"));
                            rsJournalInfo.Set_Fields("JournalNumber", lngEOYAPJournalNumber);
                            rsJournalInfo.Set_Fields("Period", bdSettings.FiscalStart);
                            rsJournalInfo.Set_Fields("Project", "");
                            // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                            rsJournalInfo.Set_Fields("account", rsArchiveInfo.Get_Fields("Account"));
                            if (rsArchiveInfo.Get_Fields_Boolean("Correction") == true)
                            {
                                rsJournalInfo.Set_Fields("RCB", "C");
                            }
                            else
                            {
                                rsJournalInfo.Set_Fields("RCB", "R");
                            }
                            // TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            rsJournalInfo.Set_Fields("Amount", rsArchiveInfo.Get_Fields("Amount") * -1);
                            rsJournalInfo.Set_Fields("Status", "E");
                            rsJournalInfo.Update();
                            rsJournalInfo.AddNew();
                            rsJournalInfo.Set_Fields("Type", "G");
                            rsJournalInfo.Set_Fields("JournalEntriesDate", DateTime.Today);
                            // TODO: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
                            rsJournalInfo.Set_Fields("Description", "Prior Year Jrnl " + Strings.Format(rsArchiveInfo.Get_Fields("JournalNumber"), "0000"));
                            rsJournalInfo.Set_Fields("JournalNumber", lngEOYAPJournalNumber);
                            rsJournalInfo.Set_Fields("Period", bdSettings.FiscalStart);
                            rsJournalInfo.Set_Fields("Project", "");
                            rsJournalInfo.Set_Fields("account", rsArchiveInfo.Get_Fields_String("CashAccount"));
                            if (rsArchiveInfo.Get_Fields_Boolean("Correction") == true)
                            {
                                rsJournalInfo.Set_Fields("RCB", "C");
                            }
                            else
                            {
                                rsJournalInfo.Set_Fields("RCB", "R");
                            }
                            // TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
                            rsJournalInfo.Set_Fields("Amount", rsArchiveInfo.Get_Fields("Amount"));
                            rsJournalInfo.Set_Fields("Status", "E");
                            rsJournalInfo.Update();
                            rsArchiveInfo.Edit();
                            rsArchiveInfo.Set_Fields("transferred", true);
                            rsArchiveInfo.Update();
                            rsArchiveInfo.MoveNext();
                        }
                        while (rsArchiveInfo.EndOfFile() != true);
                        if (lngEOYAPJournalNumber != 0)
                        {
                            clsJournalInfo = new clsPostingJournalInfo();
                            clsJournalInfo.JournalNumber = lngEOYAPJournalNumber;
                            clsJournalInfo.JournalType = "GJ";
                            clsJournalInfo.Period = bdSettings.FiscalStart;
                            clsJournalInfo.CheckDate = "";
                            clsPostInfo.AddJournal(clsJournalInfo);
                        }
                    }
                    frmWait.InstancePtr.Unload();
                    ////////Application.DoEvents();
                    modBudgetaryAccounting.UpdateCalculationNeededTable(FCConvert.ToInt16(lngJournalNumber));
                    if (lngJournalNumber != 0 && lngEOYAPJournalNumber != 0)
                    {
                        if (AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptCreateOpeningAdjustments))
                        {
                            if (MessageBox.Show("Your adjusting entries have been saved in journal " + Strings.Format(lngJournalNumber, "0000") + "\r\n" + "Your EOY AP entries have been saved in journal " + Strings.Format(lngEOYAPJournalNumber, "0000") + ".  Would you like to post these journals?", "Entries Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                clsPostInfo.AllowPreview = true;
                                clsPostInfo.PageBreakBetweenJournalsOnReport = true;
                                clsPostInfo.AddJournal(clsJournalInfo);
                                clsPostInfo.PostJournals();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Your adjusting entries have been saved in journal " + Strings.Format(lngJournalNumber, "0000") + "\r\n" + "Your EOY AP entries have been saved in journal " + Strings.Format(lngEOYAPJournalNumber, "0000"), "Entries Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else if (lngJournalNumber != 0)
                    {
                        if (AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptCreateOpeningAdjustments))
                        {
                            if (MessageBox.Show("Your adjusting entries have been saved in journal " + Strings.Format(lngJournalNumber, "0000") + ".  Would you like to post this journal?", "Entries Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                clsPostInfo.AllowPreview = true;
                                clsPostInfo.PageBreakBetweenJournalsOnReport = true;
                                clsPostInfo.AddJournal(clsJournalInfo);
                                clsPostInfo.PostJournals();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Your adjusting entries have been saved in journal " + Strings.Format(lngJournalNumber, "0000"), "Entries Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else if (lngEOYAPJournalNumber != 0)
                    {
                        if (AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptCreateOpeningAdjustments))
                        {
                            if (MessageBox.Show("Your EOY AP entries have been saved in journal " + Strings.Format(lngEOYAPJournalNumber, "0000") + ".  Would you like to post this journal?", "Entries Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                clsPostInfo.AllowPreview = true;
                                clsPostInfo.PageBreakBetweenJournalsOnReport = true;
                                clsPostInfo.AddJournal(clsJournalInfo);
                                clsPostInfo.PostJournals();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Your EOY AP entries have been saved in journal " + Strings.Format(lngEOYAPJournalNumber, "0000"), "Entries Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("No adjusting entries needed", "No Entries Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("No adjusting entries needed", "No Entries Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            finally
            {
                rsArchiveInfo.DisposeOf();
                rsArchiveLedgerYTDActivity.DisposeOf();
                rsCheckRec.DisposeOf();
                rsCheckRecArchive.DisposeOf();
                rsDoNotCalc.DisposeOf();
                rsEOYProcess.DisposeOf();
                rsInfo.DisposeOf();
                rsJournalInfo.DisposeOf();
                rsLedgerBegBalInfo.DisposeOf();
                rsLedgerEndingBalanceInfo.DisposeOf();
            }
        }

        private bool GetNewBalanceInfo(ref string strPath, ref BegBalInfo[] bbiInfo)
        {
            bool GetNewBalanceInfo = false;
            // ByRef bbiInfoParam() As BegBalInfo) As Boolean
            clsDRWrapper rsControlInfo = new clsDRWrapper();
            // vbPorter upgrade warning: curExpControlAmount As Decimal	OnWrite(short, double, Decimal)
            Decimal[] curExpControlAmount = new Decimal[99 + 1];
            // vbPorter upgrade warning: curRevControlAmount As Decimal	OnWrite(short, double, Decimal)
            Decimal[] curRevControlAmount = new Decimal[99 + 1];
            // Dim curSchoolExpControlAmount(999) As Currency
            // Dim curSchoolRevControlAmount(999) As Currency
            // vbPorter upgrade warning: curExpTotal As Decimal	OnWriteFCConvert.ToInt16
            Decimal curExpTotal;
            // vbPorter upgrade warning: curRevTotal As Decimal	OnWriteFCConvert.ToInt16
            Decimal curRevTotal;
            int counter;
            bool blnInfoToClose;
            clsDRWrapper rsDepartmentInfo = new clsDRWrapper();
            clsDRWrapper rsDivisionInfo = new clsDRWrapper();
            clsDRWrapper rsRevenueInfo = new clsDRWrapper();
            // vbPorter upgrade warning: curDeptExpTotal As Decimal	OnWrite(double, short, Decimal)
            Decimal curDeptExpTotal = 0;
            // vbPorter upgrade warning: curDeptRevTotal As Decimal	OnWrite(double, short, Decimal)
            Decimal curDeptRevTotal = 0;
            // vbPorter upgrade warning: curDivExpTotal As Decimal	OnWriteFCConvert.ToDouble(
            Decimal curDivExpTotal;
            // vbPorter upgrade warning: curDivRevTotal As Decimal	OnWrite(double, Decimal)
            Decimal curDivRevTotal;
            clsDRWrapper rsCloseout = new clsDRWrapper();
            clsDRWrapper rsRevYTDActivity = new clsDRWrapper();
            clsDRWrapper rsYTDActivity = new clsDRWrapper();
            clsDRWrapper rsLedgerYTDActivity = new clsDRWrapper();
            clsDRWrapper rsRevActivityDetail = new clsDRWrapper();
            clsDRWrapper rsActivityDetail = new clsDRWrapper();
            clsDRWrapper rsLedgerActivityDetail = new clsDRWrapper();
            // Dim rsSchoolRevYTDActivity As New clsDRWrapper
            // Dim rsSchoolExpYTDActivity As New clsDRWrapper
            // Dim rsSchoolLedgerYTDActivity As New clsDRWrapper
            // Dim rsSchoolRevActivityDetail As New clsDRWrapper
            // Dim rsSchoolExpActivityDetail As New clsDRWrapper
            // Dim rsSchoolLedgerActivityDetail As New clsDRWrapper
            clsDRWrapper rsExpDeptYTDActivity = new clsDRWrapper();
            clsDRWrapper rsExpDivYTDActivity = new clsDRWrapper();
            clsDRWrapper rsRevDeptYTDActivity = new clsDRWrapper();
            clsDRWrapper rsRevDivYTDActivity = new clsDRWrapper();
            clsDRWrapper rsRevRevYTDActivity = new clsDRWrapper();
            int lngIndex = 0;
            Decimal curOriginalExpControl;
            // Dim bbiInfo() As BegBalInfo
            // bbiInfo = bbiInfoParam
            // If gboolTownAccounts Then
            if (Conversion.Val(modBudgetaryAccounting.Statics.strExpCtrAccount) == 0)
            {
                MessageBox.Show("End of Year Processing is Stopping because there is a problem with your Town Expense Control Account", "Invalid Town Expense Control Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                GetNewBalanceInfo = false;
                return GetNewBalanceInfo;
            }
            if (Conversion.Val(modBudgetaryAccounting.Statics.strRevCtrAccount) == 0)
            {
                MessageBox.Show("End of Year Processing is Stopping because there is a problem with your Town Revenue Control Account", "Invalid Town Revenue Control Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                GetNewBalanceInfo = false;
                return GetNewBalanceInfo;
            }
            if (Conversion.Val(modBudgetaryAccounting.Statics.strFundBalCtrAccount) == 0)
            {
                MessageBox.Show("End of Year Processing is Stopping because there is a problem with your Town Fund Balance Account", "Invalid Town Fund Balance Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                GetNewBalanceInfo = false;
                return GetNewBalanceInfo;
            }
            // End If
            rsRevYTDActivity.GroupName = strPath;
            rsYTDActivity.GroupName = strPath;
            rsLedgerYTDActivity.GroupName = strPath;
            rsRevActivityDetail.GroupName = strPath;
            rsActivityDetail.GroupName = strPath;
            rsLedgerActivityDetail.GroupName = strPath;
            rsExpDeptYTDActivity.GroupName = strPath;
            rsExpDivYTDActivity.GroupName = strPath;
            rsRevDeptYTDActivity.GroupName = strPath;
            rsRevDivYTDActivity.GroupName = strPath;
            rsRevRevYTDActivity.GroupName = strPath;
            rsDepartmentInfo.GroupName = strPath;
            rsDivisionInfo.GroupName = strPath;
            rsRevenueInfo.GroupName = strPath;
            // troges126
            // CalculateAccountInfo True, True, False, "E", strPath
            // CalculateAccountInfo True, True, False, "R", strPath
            // CalculateAccountInfo True, True, False, "G", strPath
            // CalculateAccountInfo True, True, False, "P", strPath
            // CalculateAccountInfo True, True, False, "V", strPath
            // CalculateAccountInfo True, True, False, "L", strPath
            try
            {
                rsYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Account");
                rsLedgerYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo GROUP BY Account");
                rsRevYTDActivity.OpenRecordset("SELECT Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo GROUP BY Account");
                rsActivityDetail.OpenRecordset("SELECT Period, Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Account, Period");
                rsLedgerActivityDetail.OpenRecordset("SELECT Period, Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM LedgerReportInfo GROUP BY Account, Period");
                rsRevActivityDetail.OpenRecordset("SELECT Period, Account, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo GROUP BY Account, Period");
                rsExpDeptYTDActivity.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Department");
                rsExpDivYTDActivity.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM ExpenseReportInfo GROUP BY Department, Division");
                rsRevDeptYTDActivity.OpenRecordset("SELECT Department, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo GROUP BY Department");
                rsRevDivYTDActivity.OpenRecordset("SELECT Department, Division, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo GROUP BY Department, Division");
                rsRevRevYTDActivity.OpenRecordset("SELECT Department, Division, Revenue, SUM(OriginalBudget) AS OriginalBudgetTotal, SUM(BudgetAdjustments) as BudgetAdjustmentsTotal, SUM(PostedDebits) as PostedDebitsTotal, SUM(PostedCredits) as PostedCreditsTotal, SUM(EncumbActivity) as EncumbActivityTotal, SUM(PendingDebits) as PendingDebitsTotal, SUM(PendingCredits) as PendingCreditsTotal FROM RevenueReportInfo GROUP BY Department, Division, Revenue");
                if (rsLedgerYTDActivity.EndOfFile() != true && rsLedgerYTDActivity.BeginningOfFile() != true)
                {
                    do
                    {
                        ////////Application.DoEvents();
                        // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                        lngIndex = GetCorrectIndex_2(rsLedgerYTDActivity.Get_Fields_String("Account"), bbiInfo);
                        if (lngIndex < 0)
                        {
                            Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                            // TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
                            bbiInfo[Information.UBound(bbiInfo)].Account = FCConvert.ToString(rsLedgerYTDActivity.Get_Fields("Account"));
                            bbiInfo[Information.UBound(bbiInfo)].OldBalance = 0;
                            // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                            // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                            // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                            // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                            // TODO: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                            bbiInfo[Information.UBound(bbiInfo)].NewBalance = FCConvert.ToDecimal(rsLedgerYTDActivity.Get_Fields("OriginalBudgetTotal") + rsLedgerYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsLedgerYTDActivity.Get_Fields("PostedDebitsTotal") + rsLedgerYTDActivity.Get_Fields("PostedCreditsTotal") + rsLedgerYTDActivity.Get_Fields("EncumbActivityTotal"));
                        }
                        else
                        {
                            // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                            // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                            // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                            // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                            // TODO: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                            bbiInfo[lngIndex].NewBalance = FCConvert.ToDecimal(rsLedgerYTDActivity.Get_Fields("OriginalBudgetTotal") + rsLedgerYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsLedgerYTDActivity.Get_Fields("PostedDebitsTotal") + rsLedgerYTDActivity.Get_Fields("PostedCreditsTotal") + rsLedgerYTDActivity.Get_Fields("EncumbActivityTotal"));
                        }
                        rsLedgerYTDActivity.MoveNext();
                    }
                    while (rsLedgerYTDActivity.EndOfFile() != true);
                }
                GetNewBalanceInfo = true;
                curExpTotal = 0;
                curRevTotal = 0;
                for (counter = 1; counter <= 99; counter++)
                {
                    ////////Application.DoEvents();
                    curExpControlAmount[counter] = 0;
                    curRevControlAmount[counter] = 0;
                }
                blnInfoToClose = false;
                for (counter = 1; counter <= 99; counter++)
                {
                    ////////Application.DoEvents();
                    if (!modAccountTitle.Statics.YearFlag)
                    {
                        curExpControlAmount[counter] = FCConvert.ToDecimal(GetYTDNet_2("G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strExpCtrAccount + "-00", strPath));
                        curRevControlAmount[counter] = FCConvert.ToDecimal(GetYTDNet_2("G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strRevCtrAccount + "-00", strPath));
                    }
                    else
                    {
                        curExpControlAmount[counter] = FCConvert.ToDecimal(GetYTDNet_2("G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strExpCtrAccount, strPath));
                        curRevControlAmount[counter] = FCConvert.ToDecimal(GetYTDNet_2("G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strRevCtrAccount, strPath));
                    }
                    if (curRevControlAmount[counter] != 0 || curExpControlAmount[counter] != 0)
                    {
                        blnInfoToClose = true;
                    }
                }
                if (blnInfoToClose)
                {
                    for (counter = 1; counter <= 99; counter++)
                    {
                        ////////Application.DoEvents();
                        rsDepartmentInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Fund = '" + modValidateAccount.GetFormat_6(FCConvert.ToString(counter), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "' ORDER BY Department");
                        if (rsDepartmentInfo.EndOfFile() != true && rsDepartmentInfo.BeginningOfFile() != true)
                        {
                            do
                            {
                                if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
                                {
                                    if (rsExpDeptYTDActivity.FindFirstRecord("Department", rsDepartmentInfo.Get_Fields_String("Department")))
                                    {
                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        curDeptExpTotal = FCConvert.ToDecimal(rsExpDeptYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDeptYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDeptYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDeptYTDActivity.Get_Fields("PostedCreditsTotal"));
                                    }
                                    else
                                    {
                                        curDeptExpTotal = 0;
                                    }
                                    curExpControlAmount[counter] += curDeptExpTotal;
                                    if (rsRevDeptYTDActivity.FindFirstRecord("Department", rsDepartmentInfo.Get_Fields_String("Department")))
                                    {
                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                        curDeptRevTotal = FCConvert.ToDecimal(rsRevDeptYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDeptYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDeptYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDeptYTDActivity.Get_Fields("PostedCreditsTotal"));
                                    }
                                    else
                                    {
                                        curDeptRevTotal = 0;
                                    }
                                    curRevControlAmount[counter] -= curDeptRevTotal;
                                }
                                if (!modAccountTitle.Statics.ExpDivFlag)
                                {
                                    rsDivisionInfo.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Division <> '" + modValidateAccount.GetFormat_6("0", FCConvert.ToInt16(Strings.Mid(modAccountTitle.Statics.Exp, 3, 2))) + "' ORDER BY Division");
                                    if (rsDivisionInfo.EndOfFile() != true && rsDivisionInfo.BeginningOfFile() != true)
                                    {
                                        do
                                        {
                                            ////////Application.DoEvents();
                                            if (Strings.Trim(FCConvert.ToString(rsDivisionInfo.Get_Fields_String("CloseoutAccount"))) != "")
                                            {
                                                if (!modAccountTitle.Statics.RevDivFlag)
                                                {
                                                    if (rsExpDivYTDActivity.FindFirstRecord2("Department, Division", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsDivisionInfo.Get_Fields_String("Division"), ","))
                                                    {
                                                        if (rsRevDivYTDActivity.FindFirstRecord2("Department, Division", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsDivisionInfo.Get_Fields_String("Division"), ","))
                                                        {
                                                            // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                            if (((rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal")) + (rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal"))) != 0)
                                                            {
                                                                if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
                                                                {
                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                    curDeptExpTotal -= rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal");
                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                    curDeptRevTotal -= rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal");
                                                                }
                                                                else
                                                                {
                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                    curExpControlAmount[counter] += (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                    curRevControlAmount[counter] -= (rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                }
                                                                // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                curDivExpTotal = FCConvert.ToDecimal((rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal")));
                                                                // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                curDivRevTotal = FCConvert.ToDecimal((rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal")));
                                                                rsRevenueInfo.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Division = '" + rsDivisionInfo.Get_Fields_String("Division") + "' AND rtrim(CloseoutAccount) <> '' ORDER BY Revenue");
                                                                if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
                                                                {
                                                                    do
                                                                    {
                                                                        ////////Application.DoEvents();
                                                                        if (rsRevRevYTDActivity.FindFirstRecord2("Department, Division, Revenue", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsDivisionInfo.Get_Fields_String("Division") + "," + rsRevenueInfo.Get_Fields_String("Revenue"), ","))
                                                                        {
                                                                            // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                            // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                            // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                            // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                            if (((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))) != 0)
                                                                            {
                                                                                if (Strings.Trim(FCConvert.ToString(rsDivisionInfo.Get_Fields_String("CloseoutAccount"))) != "")
                                                                                {
                                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                                    curDivRevTotal -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                                }
                                                                                else if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
                                                                                {
                                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                                    curDeptRevTotal -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                                }
                                                                                else
                                                                                {
                                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                                    curRevControlAmount[counter] -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                                }
                                                                                lngIndex = GetCorrectIndex_2(rsRevenueInfo.Get_Fields_String("CloseoutAccount"), bbiInfo);
                                                                                if (lngIndex < 0)
                                                                                {
                                                                                    Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                                                                                    bbiInfo[Information.UBound(bbiInfo)].Account = FCConvert.ToString(rsRevenueInfo.Get_Fields_String("CloseoutAccount"));
                                                                                    bbiInfo[Information.UBound(bbiInfo)].OldBalance = 0;
                                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                                    bbiInfo[Information.UBound(bbiInfo)].NewBalance = bbiInfo[Information.UBound(bbiInfo, 1)].NewBalance + rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal");
                                                                                }
                                                                                else
                                                                                {
                                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                                    bbiInfo[lngIndex].NewBalance += rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal");
                                                                                }
                                                                            }
                                                                        }
                                                                        rsRevenueInfo.MoveNext();
                                                                    }
                                                                    while (rsRevenueInfo.EndOfFile() != true);
                                                                }
                                                                lngIndex = GetCorrectIndex_2(rsDivisionInfo.Get_Fields_String("CloseoutAccount"), bbiInfo);
                                                                if (lngIndex < 0)
                                                                {
                                                                    Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                                                                    bbiInfo[Information.UBound(bbiInfo)].Account = FCConvert.ToString(rsDivisionInfo.Get_Fields_String("CloseoutAccount"));
                                                                    bbiInfo[Information.UBound(bbiInfo)].OldBalance = 0;
                                                                    bbiInfo[Information.UBound(bbiInfo)].NewBalance = bbiInfo[Information.UBound(bbiInfo, 1)].NewBalance - curDivExpTotal + curDivRevTotal;
                                                                }
                                                                else
                                                                {
                                                                    bbiInfo[lngIndex].NewBalance += -curDivExpTotal + curDivRevTotal;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (rsExpDivYTDActivity.FindFirstRecord2("Department, Division", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsDivisionInfo.Get_Fields_String("Division"), ","))
                                                            {
                                                                // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                if ((rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal")) != 0)
                                                                {
                                                                    if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
                                                                    {
                                                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                        curDeptExpTotal -= (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                    }
                                                                    else
                                                                    {
                                                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                        curExpControlAmount[counter] += (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                    }
                                                                    lngIndex = GetCorrectIndex_2(rsDivisionInfo.Get_Fields_String("CloseoutAccount"), bbiInfo);
                                                                    if (lngIndex < 0)
                                                                    {
                                                                        Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                                                                        bbiInfo[Information.UBound(bbiInfo)].Account = FCConvert.ToString(rsDivisionInfo.Get_Fields_String("CloseoutAccount"));
                                                                        bbiInfo[Information.UBound(bbiInfo)].OldBalance = 0;
                                                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                        bbiInfo[Information.UBound(bbiInfo)].NewBalance = bbiInfo[Information.UBound(bbiInfo, 1)].NewBalance - (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                    }
                                                                    else
                                                                    {
                                                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                        bbiInfo[lngIndex].NewBalance -= (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (rsRevDivYTDActivity.FindFirstRecord2("Department, Division", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsDivisionInfo.Get_Fields_String("Division"), ","))
                                                        {
                                                            // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                            if ((rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal")) != 0)
                                                            {
                                                                if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
                                                                {
                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                    curDeptRevTotal -= (rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                }
                                                                else
                                                                {
                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                    curRevControlAmount[counter] -= (rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                }
                                                                // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                curDivRevTotal = FCConvert.ToDecimal((rsRevDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevDivYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevDivYTDActivity.Get_Fields("PostedCreditsTotal")));
                                                                rsRevenueInfo.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Division = '" + rsDivisionInfo.Get_Fields_String("Division") + "' AND rtrim(CloseoutAccount) <> '' ORDER BY Revenue");
                                                                if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
                                                                {
                                                                    do
                                                                    {
                                                                        ////////Application.DoEvents();
                                                                        if (rsRevRevYTDActivity.FindFirstRecord2("Department, Division, Revenue", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsDivisionInfo.Get_Fields_String("Division") + "," + rsRevenueInfo.Get_Fields_String("Revenue"), ","))
                                                                        {
                                                                            // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                            // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                            // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                            // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                            if (((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))) != 0)
                                                                            {
                                                                                if (Strings.Trim(FCConvert.ToString(rsDivisionInfo.Get_Fields_String("CloseoutAccount"))) != "")
                                                                                {
                                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                                    curDivRevTotal -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                                }
                                                                                else if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
                                                                                {
                                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                                    curDeptRevTotal -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                                }
                                                                                else
                                                                                {
                                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                                    curRevControlAmount[counter] -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                                }
                                                                                lngIndex = GetCorrectIndex_2(rsRevenueInfo.Get_Fields_String("CloseoutAccount"), bbiInfo);
                                                                                if (lngIndex < 0)
                                                                                {
                                                                                    Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                                                                                    bbiInfo[Information.UBound(bbiInfo)].Account = FCConvert.ToString(rsRevenueInfo.Get_Fields_String("CloseoutAccount"));
                                                                                    bbiInfo[Information.UBound(bbiInfo)].OldBalance = 0;
                                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                                    bbiInfo[Information.UBound(bbiInfo)].NewBalance = bbiInfo[Information.UBound(bbiInfo, 1)].NewBalance + rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal");
                                                                                }
                                                                                else
                                                                                {
                                                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                                    bbiInfo[lngIndex].NewBalance += rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal");
                                                                                }
                                                                            }
                                                                        }
                                                                        rsRevenueInfo.MoveNext();
                                                                    }
                                                                    while (rsRevenueInfo.EndOfFile() != true);
                                                                }
                                                                if (curDivRevTotal != 0)
                                                                {
                                                                    lngIndex = GetCorrectIndex_2(rsDivisionInfo.Get_Fields_String("CloseoutAccount"), bbiInfo);
                                                                    if (lngIndex < 0)
                                                                    {
                                                                        Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                                                                        bbiInfo[Information.UBound(bbiInfo)].Account = FCConvert.ToString(rsDivisionInfo.Get_Fields_String("CloseoutAccount"));
                                                                        bbiInfo[Information.UBound(bbiInfo)].OldBalance = 0;
                                                                        bbiInfo[Information.UBound(bbiInfo)].NewBalance = bbiInfo[Information.UBound(bbiInfo, 1)].NewBalance + curDivRevTotal;
                                                                    }
                                                                    else
                                                                    {
                                                                        bbiInfo[lngIndex].NewBalance += curDivRevTotal;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (rsExpDivYTDActivity.FindFirstRecord2("Department, Division", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsDivisionInfo.Get_Fields_String("Division"), ","))
                                                    {
                                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                        if ((rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal")) != 0)
                                                        {
                                                            if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
                                                            {
                                                                // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                curDeptExpTotal -= (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                            }
                                                            else
                                                            {
                                                                // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                curExpControlAmount[counter] += (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                            }
                                                            lngIndex = GetCorrectIndex_2(rsDivisionInfo.Get_Fields_String("CloseoutAccount"), bbiInfo);
                                                            if (lngIndex < 0)
                                                            {
                                                                Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                                                                bbiInfo[Information.UBound(bbiInfo)].Account = FCConvert.ToString(rsDivisionInfo.Get_Fields_String("CloseoutAccount"));
                                                                bbiInfo[Information.UBound(bbiInfo)].OldBalance = 0;
                                                                // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                bbiInfo[Information.UBound(bbiInfo)].NewBalance = bbiInfo[Information.UBound(bbiInfo, 1)].NewBalance - (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") + rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                            }
                                                            else
                                                            {
                                                                // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                bbiInfo[lngIndex].NewBalance -= (rsExpDivYTDActivity.Get_Fields("OriginalBudgetTotal") - rsExpDivYTDActivity.Get_Fields("BudgetAdjustmentsTotal") - rsExpDivYTDActivity.Get_Fields("PostedDebitsTotal") - rsExpDivYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (!modAccountTitle.Statics.RevDivFlag)
                                                {
                                                    rsRevenueInfo.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND Division = '" + rsDivisionInfo.Get_Fields_String("Division") + "' AND rtrim(CloseoutAccount) <> '' ORDER BY Revenue");
                                                    if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
                                                    {
                                                        do
                                                        {
                                                            ////////Application.DoEvents();
                                                            if (rsRevRevYTDActivity.FindFirstRecord2("Department, Revenue", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsRevenueInfo.Get_Fields_String("Revenue"), ","))
                                                            {
                                                                // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                if (((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))) != 0)
                                                                {
                                                                    if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
                                                                    {
                                                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                        curDeptRevTotal -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                    }
                                                                    else
                                                                    {
                                                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                        curRevControlAmount[counter] -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                                    }
                                                                    lngIndex = GetCorrectIndex_2(rsRevenueInfo.Get_Fields_String("CloseoutAccount"), bbiInfo);
                                                                    if (lngIndex < 0)
                                                                    {
                                                                        Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                                                                        bbiInfo[Information.UBound(bbiInfo)].Account = FCConvert.ToString(rsRevenueInfo.Get_Fields_String("CloseoutAccount"));
                                                                        bbiInfo[Information.UBound(bbiInfo)].OldBalance = 0;
                                                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                        bbiInfo[Information.UBound(bbiInfo)].NewBalance = bbiInfo[Information.UBound(bbiInfo, 1)].NewBalance + rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal");
                                                                    }
                                                                    else
                                                                    {
                                                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                                        bbiInfo[lngIndex].NewBalance += rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal");
                                                                    }
                                                                }
                                                            }
                                                            rsRevenueInfo.MoveNext();
                                                        }
                                                        while (rsRevenueInfo.EndOfFile() != true);
                                                    }
                                                }
                                            }
                                            rsDivisionInfo.MoveNext();
                                        }
                                        while (rsDivisionInfo.EndOfFile() != true);
                                    }
                                    if (modAccountTitle.Statics.RevDivFlag)
                                    {
                                        rsRevenueInfo.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND rtrim(CloseoutAccount) <> '' ORDER BY Revenue");
                                        if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
                                        {
                                            do
                                            {
                                                ////////Application.DoEvents();
                                                if (rsRevRevYTDActivity.FindFirstRecord2("Department, Revenue", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsRevenueInfo.Get_Fields_String("Revenue"), ","))
                                                {
                                                    // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                    // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                    // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                    // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                    if (((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))) != 0)
                                                    {
                                                        if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
                                                        {
                                                            // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                            curDeptRevTotal -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                        }
                                                        else
                                                        {
                                                            // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                            curRevControlAmount[counter] -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                        }
                                                        lngIndex = GetCorrectIndex_2(rsRevenueInfo.Get_Fields_String("CloseoutAccount"), bbiInfo);
                                                        if (lngIndex < 0)
                                                        {
                                                            Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                                                            bbiInfo[Information.UBound(bbiInfo)].Account = FCConvert.ToString(rsRevenueInfo.Get_Fields_String("CloseoutAccount"));
                                                            bbiInfo[Information.UBound(bbiInfo)].OldBalance = 0;
                                                            // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                            bbiInfo[Information.UBound(bbiInfo)].NewBalance = bbiInfo[Information.UBound(bbiInfo, 1)].NewBalance + rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal");
                                                        }
                                                        else
                                                        {
                                                            // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                            // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                            bbiInfo[lngIndex].NewBalance += rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal");
                                                        }
                                                    }
                                                }
                                                rsRevenueInfo.MoveNext();
                                            }
                                            while (rsRevenueInfo.EndOfFile() != true);
                                        }
                                    }
                                }
                                else
                                {
                                    rsRevenueInfo.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + rsDepartmentInfo.Get_Fields_String("Department") + "' AND rtrim(CloseoutAccount) <> '' ORDER BY Revenue");
                                    if (rsRevenueInfo.EndOfFile() != true && rsRevenueInfo.BeginningOfFile() != true)
                                    {
                                        do
                                        {
                                            ////////Application.DoEvents();
                                            if (rsRevRevYTDActivity.FindFirstRecord2("Department, Revenue", rsDepartmentInfo.Get_Fields_String("Department") + "," + rsRevenueInfo.Get_Fields_String("Revenue"), ","))
                                            {
                                                // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                if (((rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"))) != 0)
                                                {
                                                    if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
                                                    {
                                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                        curDeptRevTotal -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                    }
                                                    else
                                                    {
                                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                        curRevControlAmount[counter] -= (rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal"));
                                                    }
                                                    lngIndex = GetCorrectIndex_2(rsRevenueInfo.Get_Fields_String("CloseoutAccount"), bbiInfo);
                                                    if (lngIndex < 0)
                                                    {
                                                        Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                                                        bbiInfo[Information.UBound(bbiInfo)].Account = FCConvert.ToString(rsRevenueInfo.Get_Fields_String("CloseoutAccount"));
                                                        bbiInfo[Information.UBound(bbiInfo)].OldBalance = 0;
                                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                        bbiInfo[Information.UBound(bbiInfo)].NewBalance = bbiInfo[Information.UBound(bbiInfo, 1)].NewBalance + rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal");
                                                    }
                                                    else
                                                    {
                                                        // TODO: Field [OriginalBudgetTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [BudgetAdjustmentsTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                                                        // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                                                        bbiInfo[lngIndex].NewBalance += rsRevRevYTDActivity.Get_Fields("OriginalBudgetTotal") + rsRevRevYTDActivity.Get_Fields("BudgetAdjustmentsTotal") + rsRevRevYTDActivity.Get_Fields("PostedDebitsTotal") + rsRevRevYTDActivity.Get_Fields("PostedCreditsTotal");
                                                    }
                                                }
                                            }
                                            rsRevenueInfo.MoveNext();
                                        }
                                        while (rsRevenueInfo.EndOfFile() != true);
                                    }
                                }
                                if (Strings.Trim(FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"))) != "")
                                {
                                    if (curDeptRevTotal - curDeptExpTotal != 0)
                                    {
                                        lngIndex = GetCorrectIndex_2(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"), bbiInfo);
                                        if (lngIndex < 0)
                                        {
                                            Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                                            bbiInfo[Information.UBound(bbiInfo)].Account = FCConvert.ToString(rsDepartmentInfo.Get_Fields_String("CloseoutAccount"));
                                            bbiInfo[Information.UBound(bbiInfo)].OldBalance = 0;
                                            bbiInfo[Information.UBound(bbiInfo)].NewBalance = bbiInfo[Information.UBound(bbiInfo, 1)].NewBalance - curDeptExpTotal + curDeptRevTotal;
                                        }
                                        else
                                        {
                                            bbiInfo[lngIndex].NewBalance += -curDeptExpTotal + curDeptRevTotal;
                                        }
                                    }
                                }
                                rsDepartmentInfo.MoveNext();
                            }
                            while (rsDepartmentInfo.EndOfFile() != true);
                        }
                        // Dave 7/28/06
                        // If curExpControlAmount(counter) + curRevControlAmount(counter) <> 0 Then
                        if (!modAccountTitle.Statics.YearFlag)
                        {
                            lngIndex = GetCorrectIndex_2("G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strExpCtrAccount + "-00", bbiInfo);
                        }
                        else
                        {
                            lngIndex = GetCorrectIndex_2("G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strExpCtrAccount, bbiInfo);
                        }
                        if (lngIndex < 0)
                        {
                            Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                            if (!modAccountTitle.Statics.YearFlag)
                            {
                                bbiInfo[Information.UBound(bbiInfo)].Account = "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strExpCtrAccount + "-00";
                            }
                            else
                            {
                                bbiInfo[Information.UBound(bbiInfo)].Account = "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strExpCtrAccount;
                            }
                            bbiInfo[Information.UBound(bbiInfo)].OldBalance = 0;
                            bbiInfo[Information.UBound(bbiInfo)].NewBalance = curExpControlAmount[counter];
                        }
                        else
                        {
                            bbiInfo[lngIndex].NewBalance = curExpControlAmount[counter];
                        }
                        if (!modAccountTitle.Statics.YearFlag)
                        {
                            lngIndex = GetCorrectIndex_2("G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strRevCtrAccount + "-00", bbiInfo);
                        }
                        else
                        {
                            lngIndex = GetCorrectIndex_2("G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strRevCtrAccount, bbiInfo);
                        }
                        if (lngIndex < 0)
                        {
                            Array.Resize(ref bbiInfo, Information.UBound(bbiInfo, 1) + 1 + 1);
                            if (!modAccountTitle.Statics.YearFlag)
                            {
                                bbiInfo[Information.UBound(bbiInfo)].Account = "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strRevCtrAccount + "-00";
                            }
                            else
                            {
                                bbiInfo[Information.UBound(bbiInfo)].Account = "G " + modValidateAccount.GetFormat_6(Strings.Trim(Conversion.Str(counter)), FCConvert.ToInt16(Strings.Left(modAccountTitle.Statics.Ledger, 2))) + "-" + modBudgetaryAccounting.Statics.strRevCtrAccount;
                            }
                            bbiInfo[Information.UBound(bbiInfo)].OldBalance = 0;
                            bbiInfo[Information.UBound(bbiInfo)].NewBalance = curRevControlAmount[counter];
                        }
                        else
                        {
                            bbiInfo[lngIndex].NewBalance = curRevControlAmount[counter];
                        }
                        // End If
                    }
                }
            }
            finally
            {
                rsControlInfo.DisposeOf();
                rsDepartmentInfo.DisposeOf();
                rsDivisionInfo.DisposeOf();
                rsRevenueInfo.DisposeOf();
                rsCloseout.DisposeOf();
                rsRevYTDActivity.DisposeOf();
                rsYTDActivity.DisposeOf();
                rsLedgerYTDActivity.DisposeOf();
                rsRevActivityDetail.DisposeOf();
                rsActivityDetail.DisposeOf();
                rsLedgerActivityDetail.DisposeOf();
                rsExpDeptYTDActivity.DisposeOf();
                rsExpDivYTDActivity.DisposeOf();
                rsRevDeptYTDActivity.DisposeOf();
                rsRevDivYTDActivity.DisposeOf();
                rsRevRevYTDActivity.DisposeOf();
            }
            // School Part
            curExpTotal = 0;
            curRevTotal = 0;
            blnInfoToClose = false;
            return GetNewBalanceInfo;
        }

        private int GetCorrectIndex_2(string strAcct, BegBalInfo[] bbiInfoParam)
        {
            return GetCorrectIndex(ref strAcct, ref bbiInfoParam);
        }

        private int GetCorrectIndex(ref string strAcct, ref BegBalInfo[] bbiInfoParam)
        {
            int GetCorrectIndex = 0;
            int counter;
            BegBalInfo[] bbiInfo = null;
            bbiInfo = bbiInfoParam;
            GetCorrectIndex = -1;
            for (counter = 0; counter <= Information.UBound(bbiInfo, 1); counter++)
            {
                if (bbiInfo[counter].Account == strAcct)
                {
                    GetCorrectIndex = counter;
                    return GetCorrectIndex;
                }
            }
            return GetCorrectIndex;
        }

        private double GetYTDNet_2(string strAcct, string strPath = "")
        {
            return GetYTDNet(ref strAcct, strPath);
        }

        private double GetYTDNet(ref string strAcct, string strPath = "")
        {
            double GetYTDNet = 0;
            int temp;
            GetYTDNet = GetYTDDebit(ref strAcct, strPath) - GetYTDCredit(strAcct, strPath);
            return GetYTDNet;
        }

        private double GetYTDDebit(ref string strAcct, string strPath = "")
        {
            double GetYTDDebit = 0;
            int HighDate = 0;
            int LowDate;
            string strPeriodCheck = "";
            clsDRWrapper rs2 = new clsDRWrapper();
            string strTable = "";
            if (strPath != "")
            {
                rs2.GroupName = strPath;
            }
            if (Conversion.Val(bdSettings.FiscalStart) == 1)
            {
                HighDate = modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(12));
            }
            else
            {
                HighDate = modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Math.Round(Conversion.Val(bdSettings.FiscalStart))) - 1));
            }
            LowDate = modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Math.Round(Conversion.Val(bdSettings.FiscalStart)))));
            if (LowDate > HighDate)
            {
                strPeriodCheck = "OR";
            }
            else
            {
                strPeriodCheck = "AND";
            }
            if (Strings.Left(strAcct, 1) == "E")
            {
                strTable = "ExpenseReportInfo";
                // CalculateAccountInfo True, True, True, "E", strPath
            }
            else if (Strings.Left(strAcct, 1) == "R")
            {
                strTable = "RevenueReportInfo";
                // CalculateAccountInfo True, True, True, "R", strPath
            }
            else if (Strings.Left(strAcct, 1) == "G")
            {
                strTable = "LedgerReportInfo";
                // CalculateAccountInfo True, True, True, "G", strPath
            }
            rs2.OpenRecordset("SELECT SUM(PostedDebits) AS PostedDebitsTotal FROM " + strTable + " WHERE Account = '" + strAcct + "' AND (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ") GROUP BY Account");
            if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
            {
                // TODO: Field [PostedDebitsTotal] not found!! (maybe it is an alias?)
                GetYTDDebit = Conversion.Val(rs2.Get_Fields("PostedDebitsTotal"));
            }
            else
            {
                GetYTDDebit = 0;
            }
            GetYTDDebit += GetEncumbrance(strAcct, strPath);
            return GetYTDDebit;
        }

        private double GetYTDCredit(string strAcct, string strPath = "")
        {
            double GetYTDCredit = 0;
            int HighDate = 0;
            int LowDate;
            string strPeriodCheck = "";
            clsDRWrapper rs2 = new clsDRWrapper();
            string strTable = "";
            if (strPath != "")
            {
                rs2.GroupName = strPath;
            }
            if (Conversion.Val(bdSettings.FiscalStart) == 1)
            {
                HighDate = modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(12));
            }
            else
            {
                HighDate = modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Math.Round(Conversion.Val(bdSettings.FiscalStart)) - 1)));
            }
            LowDate = modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Math.Round(Conversion.Val(bdSettings.FiscalStart)))));
            if (LowDate > HighDate)
            {
                strPeriodCheck = "OR";
            }
            else
            {
                strPeriodCheck = "AND";
            }
            if (Strings.Left(strAcct, 1) == "E")
            {
                strTable = "ExpenseReportInfo";
                // troges126
                // CalculateAccountInfo True, True, True, "E", strPath
            }
            else if (Strings.Left(strAcct, 1) == "R")
            {
                strTable = "RevenueReportInfo";
                // CalculateAccountInfo True, True, True, "R", strPath
            }
            else if (Strings.Left(strAcct, 1) == "G")
            {
                strTable = "LedgerReportInfo";
                // CalculateAccountInfo True, True, True, "G", strPath
            }
            rs2.OpenRecordset("SELECT SUM(PostedCredits) AS PostedCreditsTotal FROM " + strTable + " WHERE Account = '" + strAcct + "' AND (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ") GROUP BY Account");
            if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
            {
                // TODO: Field [PostedCreditsTotal] not found!! (maybe it is an alias?)
                GetYTDCredit = Conversion.Val(rs2.Get_Fields("PostedCreditsTotal")) * -1;
            }
            else
            {
                GetYTDCredit = 0;
            }
            return GetYTDCredit;
        }

        private double GetEncumbrance(string strAcct, string strPath = "")
        {
            double GetEncumbrance = 0;
            int HighDate = 0;
            int LowDate;
            string strPeriodCheck = "";
            clsDRWrapper rs2 = new clsDRWrapper();
            string strTable = "";
            if (strPath != "")
            {
                rs2.GroupName = strPath;
            }
            if (Conversion.Val(bdSettings.FiscalStart) == 1)
            {
                HighDate = modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(12));
            }
            else
            {
                HighDate = modBudgetaryAccounting.HighMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Math.Round(Conversion.Val(bdSettings.FiscalStart)) - 1)));
            }
            LowDate = modBudgetaryAccounting.LowMonthCalc(modBudgetaryAccounting.MonthCalc(FCConvert.ToInt32(Math.Round(Conversion.Val(bdSettings.FiscalStart)))));
            if (LowDate > HighDate)
            {
                strPeriodCheck = "OR";
            }
            else
            {
                strPeriodCheck = "AND";
            }
            if (Strings.Left(strAcct, 1) == "E")
            {
                strTable = "ExpenseReportInfo";
                // troges126
                // CalculateAccountInfo True, True, True, "E", strPath
            }
            else if (Strings.Left(strAcct, 1) == "R")
            {
                strTable = "RevenueReportInfo";
                // CalculateAccountInfo True, True, True, "R", strPath
            }
            else if (Strings.Left(strAcct, 1) == "G")
            {
                strTable = "LedgerReportInfo";
                // CalculateAccountInfo True, True, True, "G", strPath
            }
            rs2.OpenRecordset("SELECT SUM(EncumbActivity) AS EncumbActivityTotal FROM " + strTable + " WHERE Account = '" + strAcct + "' AND (Period >= " + FCConvert.ToString(LowDate) + strPeriodCheck + " Period <= " + FCConvert.ToString(HighDate) + ") GROUP BY Account");
            if (rs2.EndOfFile() != true && rs2.BeginningOfFile() != true)
            {
                // TODO: Field [EncumbActivityTotal] not found!! (maybe it is an alias?)
                GetEncumbrance = Conversion.Val(rs2.Get_Fields("EncumbActivityTotal"));
            }
            else
            {
                GetEncumbrance = 0;
            }
            return GetEncumbrance;
        }

        public void CreateAReversingJournal(ref cJournalInfo jInfo, bool boolCorrect)
        {
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                if (!(jInfo == null))
                {
                    if (jInfo.Id > 0)
                    {
                        if (IsValidToReverse(jInfo.JournalType))
                        {
                            string strDescription = "";
                            int lngJournalID = 0;
                            cJournal rJournal;
                            strDescription = "Reversing Entry for Journal " + Strings.Format(jInfo.JournalNumber, "0000");
                            lngJournalID = CreateJournal(strDescription, "GJ", jInfo.Period);
                            if (lngJournalID > 0)
                            {
                                rJournal = jController.GetFullJournal(lngJournalID);
                                cJournal OldJournal;
                                OldJournal = jController.GetFullJournal(jInfo.Id);
                                string strAccountNumber = "";
                                bool boolCreate = false;
                                cJournalEntry jEntry;
                                cJournalEntry nEntry;
                                OldJournal.JournalEntries.MoveFirst();
                                while (OldJournal.JournalEntries.IsCurrent())
                                {
                                    boolCreate = false;
                                    jEntry = (cJournalEntry)OldJournal.JournalEntries.GetCurrentItem();
                                    if (Strings.Left(jEntry.Account + " ", 1) != "G")
                                    {
                                        boolCreate = true;
                                    }
                                    else
                                    {
                                        strAccountNumber = GetAccountNumber_2(jEntry.Account);
                                        if ((strAccountNumber != modBudgetaryAccounting.Statics.strExpCtrAccount && strAccountNumber != modBudgetaryAccounting.Statics.strRevCtrAccount) && jEntry.RCB != "L")
                                        {
                                            boolCreate = true;
                                        }
                                    }
                                    if (boolCreate)
                                    {
                                        nEntry = new cJournalEntry();
                                        nEntry.JournalType = "G";
                                        nEntry.JournalEntriesDate = jEntry.JournalEntriesDate;
                                        nEntry.Description = jEntry.Description;
                                        nEntry.JournalNumber = rJournal.JournalNumber;
                                        nEntry.Account = jEntry.Account;
                                        nEntry.Amount = -1 * jEntry.Amount;
                                        nEntry.WarrantNumber = 0;
                                        nEntry.Period = jEntry.Period;
                                        if (boolCorrect)
                                        {
                                            nEntry.RCB = "C";
                                        }
                                        else
                                        {
                                            if (jEntry.RCB == "B")
                                            {
                                                nEntry.RCB = "B";
                                            }
                                            else
                                            {
                                                nEntry.RCB = "R";
                                            }
                                        }
                                        nEntry.Status = "E";
                                        rJournal.JournalEntries.AddItem(nEntry);
                                    }
                                    OldJournal.JournalEntries.MoveNext();
                                }
                                jController.SaveJournal(ref rJournal);
                                if (jController.HadError)
                                {
                                    SetError(jController.LastErrorNumber, jController.LastErrorMessage);
                                    return;
                                }
                                if (rJournal.JournalEntries.ItemCount() > 0)
                                {
                                    if (AllowAutoPostBD(modBudgetaryAccounting.AutoPostType.aptReversingJournal))
                                    {
                                        if (MessageBox.Show("Process Complete" + "\r\n" + "\r\n" + "Entries have been entered into Journal " + FCConvert.ToString(rJournal.JournalNumber) + "\r\n" + "Would you like to post this journal?", "Entries Saved", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                        {
                                            PostJournal(rJournal);
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Process Complete" + "\r\n" + "\r\n" + "Entries have been entered into Journal " + FCConvert.ToString(rJournal.JournalNumber), "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                }
                            }
                            else
                            {
                                SetError(9999, "Unable to get a new journal number");
                                return;
                            }
                        }
                        else
                        {
                            strLastError = "Invalid Journal Type";
                            lngLastError = FCConvert.ToInt32("9999");
                        }
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
            }
        }

        private void SetError(int lngErrorNumber, string strErrorDescription)
        {
            strLastError = strErrorDescription;
            lngLastError = lngErrorNumber;
        }

        private bool IsValidToReverse(string strType)
        {
            bool IsValidToReverse = false;
            if ((Strings.LCase(strType) == "gj") || (Strings.LCase(strType) == "cr") || (Strings.LCase(strType) == "cd"))
            {
                IsValidToReverse = true;
            }
            return IsValidToReverse;
        }

        public cGenericCollection GetVendorInfosByEncumbranceJournal(int lngJournalNumber)
        {
            cGenericCollection GetVendorInfosByEncumbranceJournal = null;
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                cGenericCollection collEncs;
                cGenericCollection collVends = new cGenericCollection();
                collEncs = encController.GetEncumbrancesByJournalNumber(lngJournalNumber);
                if (!encController.HadError)
                {
                    cVendorInfo vInfo;
                    cEncumbrance enc;
                    collEncs.MoveFirst();
                    cVendor vend;
                    while (collEncs.IsCurrent())
                    {
                        ////////Application.DoEvents();
                        vInfo = new cVendorInfo();
                        enc = (cEncumbrance)collEncs.GetCurrentItem();
                        vInfo.ID = enc.ID;
                        vInfo.VendorNumber = enc.VendorNumber;
                        if (enc.VendorNumber > 0)
                        {
                            vend = vendController.GetVendorByVendorNumber(enc.VendorNumber);
                            if (!(vend == null))
                            {
                                vInfo.CheckAddress1 = vend.CheckAddress1;
                                vInfo.CheckAddress2 = vend.CheckAddress2;
                                vInfo.CheckAddress3 = vend.CheckAddress3;
                                vInfo.CheckAddress4 = vend.CheckAddress4;
                                vInfo.CheckName = vend.CheckName;
                                vInfo.EMail = vend.EMail;
                            }
                        }
                        else
                        {
                            vInfo.CheckName = enc.TempVendorName;
                            vInfo.CheckAddress1 = enc.TempVendorAddress1;
                            vInfo.CheckAddress2 = enc.TempVendorAddress2;
                            vInfo.CheckAddress3 = enc.TempVendorAddress3;
                            vInfo.CheckAddress4 = enc.TempVendorAddress4;
                        }
                        vInfo.Amount = Conversion.Val(enc.EncumbranceAmount);
                        vInfo.Description = enc.Description;
                        collVends.AddItem(vInfo);
                        collEncs.MoveNext();
                    }
                    GetVendorInfosByEncumbranceJournal = collVends;
                }
                else
                {
                    SetError(encController.LastErrorNumber, encController.LastErrorMessage);
                }
                return GetVendorInfosByEncumbranceJournal;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
            }
            return GetVendorInfosByEncumbranceJournal;
        }

        public cGenericCollection GetVendorInfosByAPJournal(int lngJournalNumber)
        {
            cGenericCollection GetVendorInfosByAPJournal = null;
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                cGenericCollection collJourns;
                cGenericCollection collVends = new cGenericCollection();
                collJourns = apjController.GetAPJournalsByJournalNumber(lngJournalNumber);
                if (!apjController.HadError)
                {
                    cVendorInfo vInfo;
                    cAPJournal aJourn;
                    collJourns.MoveFirst();
                    cVendor vend;
                    while (collJourns.IsCurrent())
                    {
                        ////////Application.DoEvents();
                        vInfo = new cVendorInfo();
                        aJourn = (cAPJournal)collJourns.GetCurrentItem();
                        vInfo.ID = aJourn.ID;
                        vInfo.VendorNumber = aJourn.VendorNumber;
                        if (aJourn.VendorNumber > 0)
                        {
                            vend = vendController.GetVendorByVendorNumber(aJourn.VendorNumber);
                            if (!(vend == null))
                            {
                                vInfo.CheckAddress1 = vend.CheckAddress1;
                                vInfo.CheckAddress2 = vend.CheckAddress2;
                                vInfo.CheckAddress3 = vend.CheckAddress3;
                                vInfo.CheckAddress4 = vend.CheckAddress4;
                                vInfo.CheckName = vend.CheckName;
                                vInfo.EMail = vend.EMail;
                            }
                        }
                        else
                        {
                            vInfo.CheckName = aJourn.TempVendorName;
                            vInfo.CheckAddress1 = aJourn.TempVendorAddress1;
                            vInfo.CheckAddress2 = aJourn.TempVendorAddress2;
                            vInfo.CheckAddress3 = aJourn.TempVendorAddress3;
                            vInfo.CheckAddress4 = aJourn.TempVendorAddress4;
                        }
                        vInfo.Amount = Conversion.Val(aJourn.Amount);
                        vInfo.Description = aJourn.Description;
                        collVends.AddItem(vInfo);
                        collJourns.MoveNext();
                    }
                    GetVendorInfosByAPJournal = collVends;
                }
                else
                {
                    SetError(apjController.LastErrorNumber, apjController.LastErrorMessage);
                }
                return GetVendorInfosByAPJournal;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
            }
            return GetVendorInfosByAPJournal;
        }

        public cEncumbrance GetFullEncumbrance(int lngID)
        {
            cEncumbrance GetFullEncumbrance = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                cEncumbrance enc;
                enc = encController.GetFullEncumbrance(lngID);
                if (encController.HadError)
                {
                    SetError(encController.LastErrorNumber, encController.LastErrorMessage);
                }
                else
                {
                    cVendor vend;
                    vend = vendController.GetVendorByVendorNumber(enc.VendorNumber);
                    if (!(vend == null))
                    {
                        enc.Vendor = vend;
                    }
                }
                GetFullEncumbrance = enc;
                return GetFullEncumbrance;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
            }
            return GetFullEncumbrance;
        }

        public cAPJournal GetFullAPJournal(int lngID)
        {
            cAPJournal GetFullAPJournal = null;
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                cAPJournal aJourn;
                aJourn = apjController.GetFullAPJournal(lngID);
                if (apjController.HadError)
                {
                    SetError(apjController.LastErrorNumber, apjController.LastErrorMessage);
                }
                else
                {
                    cVendor vend;
                    vend = vendController.GetVendorByVendorNumber(aJourn.VendorNumber);
                    if (!(vend == null))
                    {
                        aJourn.Vendor = vend;
                    }
                    cCreditMemo cmemo;
                    // If ajourn.creditMemoRecord > 0 Then
                    cmemo = credController.GetFullCreditMemoByAPJournalID(aJourn.ID);
                    if (credController.HadError)
                    {
                        SetError(credController.LastErrorNumber, credController.LastErrorMessage);
                        return GetFullAPJournal;
                    }
                    if (!(cmemo == null))
                    {
                        if (cmemo.ID > 0)
                        {
                            aJourn.CreditMemo = cmemo;
                        }
                    }
                    // End If
                }
                GetFullAPJournal = aJourn;
                return GetFullAPJournal;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
            }
            return GetFullAPJournal;
        }

        public void CalculateSummaryDetailInfo(string strArchivePath = "")
        {
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                bool boolExpensesUseDivision;
                bool boolRevenuesUseDivision;
                bool boolUseObject;
                cGenericCollection collJournals;
                cGenericCollection collAPJournals;
                cGenericCollection collEncumbrances;
                bool boolIncludePending;
                int intFirstPeriod;
                int intStartPeriod;
                int intEndPeriod = 0;
                int intLastPeriod;
                cSummaryDetailObject sdo = new cSummaryDetailObject();
                int[] aryOrderMonth = new int[12 + 1];
                string[] tmpAry = null;
                int x;
                int intTemp;
                bool boolUseProjects;
                Dictionary<object, object> dictAccounts = new Dictionary<object, object>();
                Dictionary<object, Dictionary<object, object>> dExpenseSummaries = new Dictionary<object, Dictionary<object, object>>();
                Dictionary<object, Dictionary<object, object>> dRevenueSummaries = new Dictionary<object, Dictionary<object, object>>();
                Dictionary<object, Dictionary<object, object>> dLedgerSummaries = new Dictionary<object, Dictionary<object, object>>();
                // vbPorter upgrade warning: tempAccount As cBDAccount	OnWrite(object)
                cBDAccount tempAccount;
                dictAccounts = bactController.GetAccountsDictionary(strArchivePath);
                boolUseProjects = false;
                boolIncludePending = true;
                // boolUseObject = Not ObjFlag
                boolUseObject = bdSettings.ExpensesUseObject;
                boolExpensesUseDivision = bdSettings.ExpensesUseDivision;
                boolRevenuesUseDivision = bdSettings.RevenuesUseDivision;
                // If Val(Mid(Exp, 3, 2)) > 0 Then
                // boolExpensesUseDivision = True
                // End If
                // If Val(Mid(Rev, 3, 2)) > 0 Then
                // boolRevenuesUseDivision = True
                // End If
                // intFirstPeriod = Val(GetBDVariable("FiscalStart"))
                intFirstPeriod = FCConvert.ToInt32(Math.Round(Conversion.Val(bdSettings.FiscalStart)));
                intStartPeriod = intFirstPeriod;
                if (intStartPeriod == 1)
                {
                    intEndPeriod = 12;
                }
                else
                {
                    intEndPeriod = intStartPeriod - 1;
                }
                intLastPeriod = intEndPeriod;
                intTemp = intFirstPeriod;
                for (x = 0; x <= 11; x++)
                {
                    aryOrderMonth[intTemp - 1] = x + 1;
                    intTemp += 1;
                    if (intTemp > 12)
                    {
                        intTemp = 1;
                    }
                }
                collJournals = jController.GetFullJournalsByPeriod(intStartPeriod, intEndPeriod, boolIncludePending, strArchivePath);
                if (jController.HadError)
                {
                    SetError(jController.LastErrorNumber, jController.LastErrorMessage);
                    return;
                }
                collJournals.MoveFirst();
                cJournal journ;
                cJournalEntry jDet = null;
                cLedgerDetailInfo ldi;
                cRevenueDetailInfo rdi;
                cExpenseDetailInfo edi;
                cGenericCollection collAPJourns;
                cGenericCollection collENJourns;
                cAPJournal apJourn;
                cAPJournalDetail apDet;
                cEncumbrance enJourn;
                cEncumbranceDetail enDet;
                // vbPorter upgrade warning: lri As cLedgerReportInfo	OnWrite(object, cLedgerReportInfo)
                cLedgerReportInfo lri = null;
                // vbPorter upgrade warning: eri As cExpenseReportInfo	OnWrite(object, cExpenseReportInfo)
                cExpenseReportInfo eri;
                // vbPorter upgrade warning: rri As cRevenueReportInfo	OnWrite(object, cRevenueReportInfo)
                cRevenueReportInfo rri;
                // vbPorter upgrade warning: tempDict As Dictionary<object, object>	OnWrite(object, Dictionary<object, object>)
                Dictionary<object, object> tempDict = new Dictionary<object, object>();
                while (collJournals.IsCurrent())
                {
                    journ = (cJournal)collJournals.GetCurrentItem();
                    journ.JournalEntries.MoveFirst();
                    if (Strings.LCase(journ.JournalType) == "ap" || Strings.LCase(journ.JournalType) == "ac")
                    {
                        collAPJourns = apjController.GetFullAPJournalsByJournalNumber(journ.JournalNumber, strArchivePath);
                        collAPJourns.MoveFirst();
                        while (collAPJourns.IsCurrent())
                        {
                        iLabel100:
                            apJourn = (cAPJournal)collAPJourns.GetCurrentItem();
                            apJourn.JournalEntries.MoveFirst();
                            while (apJourn.JournalEntries.IsCurrent())
                            {
                            ////////Application.DoEvents();
                            iLabel150:
                                apDet = (cAPJournalDetail)apJourn.JournalEntries.GetCurrentItem();
                                tmpAry = Strings.Split(Strings.Mid(apDet.Account, 3), "-", -1, CompareConstants.vbBinaryCompare);
                                if (Strings.LCase(Strings.Left(apDet.Account, 1)) == "g")
                                {
                                // make detail record
                                iLabel160:
                                    ldi = new cLedgerDetailInfo();
                                    ldi.Account = apDet.Account;
                                    ldi.Fund = tmpAry[0];
                                    ldi.LedgerAccount = tmpAry[1];
                                    if (Information.UBound(tmpAry, 1) > 1)
                                    {
                                        ldi.Suffix = tmpAry[2];
                                    }
                                    ldi.Description = apDet.Description;
                                    ldi.Amount = apDet.Amount;
                                    ldi.CheckNumber = apJourn.CheckNumber;
                                    ldi.Description = apDet.Description;
                                    ldi.Encumbrance = apDet.Encumbrance;
                                    ldi.EncumbranceKey = apDet.EncumbranceDetailRecord;
                                    ldi.Discount = apDet.Discount;
                                    ldi.JournalNumber = journ.JournalNumber;
                                    ldi.JournalType = "A";
                                    ldi.Period = journ.Period;
                                    ldi.OrderMonth = FCConvert.ToInt16(aryOrderMonth[ldi.Period - 1]);
                                    ldi.PostedDate = apJourn.PostedDate;
                                    ldi.Project = apDet.Project;
                                    ldi.RCB = apDet.RCB;
                                    ldi.Status = journ.Status;
                                    ldi.TransactionDate = apJourn.CheckDate;
                                    ldi.VendorNumber = apJourn.VendorNumber;
                                    ldi.Warrant = FCConvert.ToInt32(Math.Round(Conversion.Val(apJourn.Warrant)));
                                    sdo.LedgerDetails.AddItem(ldi);
                                    // make summary record
                                    if (dLedgerSummaries.ContainsKey(apDet.Account))
                                    {
                                        tempDict = dLedgerSummaries[apDet.Account];
                                        if (tempDict.ContainsKey(journ.Period))
                                        {
                                            lri = (cLedgerReportInfo)tempDict[journ.Period];
                                        }
                                        else
                                        {
                                            lri = new cLedgerReportInfo();
                                            lri.Account = apDet.Account;
                                            lri.Fund = tmpAry[0];
                                            lri.LedgerAccount = tmpAry[1];
                                            if (Information.UBound(tmpAry, 1) > 1)
                                            {
                                                lri.Suffix = tmpAry[2];
                                            }
                                            lri.Period = journ.Period;
                                            tempDict.Add(journ.Period, lri);
                                        }
                                    }
                                    else
                                    {
                                        lri = new cLedgerReportInfo();
                                        lri.Account = apDet.Account;
                                        lri.Fund = tmpAry[0];
                                        lri.LedgerAccount = tmpAry[1];
                                        if (Information.UBound(tmpAry, 1) > 1)
                                        {
                                            lri.Suffix = tmpAry[2];
                                        }
                                        lri.Period = journ.Period;
                                        tempDict = new Dictionary<object, object>();
                                        tempDict.Add(journ.Period, lri);
                                        dLedgerSummaries.Add(apDet.Account, tempDict);
                                    }
                                    if (journ.IsPosted)
                                    {
                                        if (apDet.IsCredit())
                                        {
                                            lri.PostedCredits += apDet.Amount - apDet.Discount;
                                            if (apDet.Encumbrance != 0)
                                            {
                                                lri.EncumbranceCredits += apDet.Encumbrance * -1;
                                            }
                                        }
                                        else
                                        {
                                            lri.PostedDebits += apDet.Amount - apDet.Discount;
                                            if (apDet.Encumbrance != 0)
                                            {
                                                lri.EncumbranceCredits += apDet.Encumbrance * -1;
                                            }
                                        }
                                        if (apDet.Encumbrance != 0)
                                        {
                                            lri.EncumbranceActivity += apDet.Encumbrance * -1;
                                        }
                                    }
                                    else
                                    {
                                        if (apDet.IsCredit())
                                        {
                                            lri.PendingCredits += apDet.Amount - apDet.Discount;
                                        }
                                        else
                                        {
                                            lri.PendingDebits += apDet.Amount - apDet.Discount;
                                        }
                                        if (apDet.Encumbrance != 0)
                                        {
                                            lri.EncumbranceActivity -= apDet.Encumbrance;
                                            // * -1
                                        }
                                    }
                                }
                                else if (Strings.LCase(Strings.Left(apDet.Account, 1)) == "e")
                                {
                                iLabel200:
                                    edi = new cExpenseDetailInfo();
                                    edi.Account = apDet.Account;
                                    edi.Amount = apDet.Amount;
                                    edi.CheckNumber = apJourn.CheckNumber;
                                    edi.Status = journ.Status;
                                    edi.Department = tmpAry[0];
                                    if (boolExpensesUseDivision)
                                    {
                                        edi.Division = tmpAry[1];
                                        edi.Expense = tmpAry[2];
                                        if (boolUseObject)
                                        {
                                            edi.ExpenseObject = tmpAry[3];
                                        }
                                    }
                                    else
                                    {
                                        edi.Expense = tmpAry[1];
                                        if (boolUseObject)
                                        {
                                            edi.ExpenseObject = tmpAry[2];
                                        }
                                    }
                                    edi.Description = apDet.Description;
                                    edi.Discount = apDet.Discount;
                                    edi.Encumbrance = apDet.Encumbrance;
                                    edi.EncumbranceKey = apDet.EncumbranceDetailRecord;
                                    edi.JournalNumber = journ.JournalNumber;
                                    edi.JournalType = "A";
                                    edi.Period = journ.Period;
                                    edi.OrderMonth = FCConvert.ToInt16(aryOrderMonth[edi.Period - 1]);
                                    edi.PostedDate = apJourn.PostedDate;
                                    edi.Project = apDet.Project;
                                    edi.RCB = apDet.RCB;
                                    edi.TransactionDate = apJourn.CheckDate;
                                    edi.VendorNumber = apJourn.VendorNumber;
                                    edi.Warrant = FCConvert.ToInt32(Math.Round(Conversion.Val(apJourn.Warrant)));
                                    sdo.ExpenseDetails.AddItem(edi);
                                    // make summary record
                                    if (dExpenseSummaries.ContainsKey(apDet.Account))
                                    {
                                        tempDict = dExpenseSummaries[apDet.Account];
                                        if (tempDict.ContainsKey(journ.Period))
                                        {
                                            eri = (cExpenseReportInfo)tempDict[journ.Period];
                                        }
                                        else
                                        {
                                            eri = new cExpenseReportInfo();
                                            eri.Account = apDet.Account;
                                            eri.Department = tmpAry[0];
                                            if (boolExpensesUseDivision)
                                            {
                                                eri.Division = tmpAry[1];
                                                eri.Expense = tmpAry[2];
                                                if (boolUseObject)
                                                {
                                                    eri.AccountObject = tmpAry[3];
                                                }
                                            }
                                            else
                                            {
                                                eri.Expense = tmpAry[1];
                                                if (boolUseObject)
                                                {
                                                    eri.AccountObject = tmpAry[2];
                                                }
                                            }
                                            eri.Period = journ.Period;
                                            tempDict.Add(journ.Period, eri);
                                        }
                                    }
                                    else
                                    {
                                        eri = new cExpenseReportInfo();
                                        eri.Account = apDet.Account;
                                        eri.Department = tmpAry[0];
                                        if (boolExpensesUseDivision)
                                        {
                                            eri.Division = tmpAry[1];
                                            eri.Expense = tmpAry[2];
                                            if (boolUseObject)
                                            {
                                                eri.AccountObject = tmpAry[3];
                                            }
                                        }
                                        else
                                        {
                                            eri.Expense = tmpAry[1];
                                            if (boolUseObject)
                                            {
                                                eri.AccountObject = tmpAry[2];
                                            }
                                        }
                                        eri.Period = journ.Period;
                                        tempDict = new Dictionary<object, object>();
                                        tempDict.Add(journ.Period, eri);
                                        dExpenseSummaries.Add(apDet.Account, tempDict);
                                    }
                                    if (journ.IsPosted)
                                    {
                                        if (apDet.IsCredit())
                                        {
                                            eri.PostedCredits += apDet.Amount - apDet.Discount;
                                        }
                                        else
                                        {
                                            eri.PostedDebits += apDet.Amount - apDet.Discount;
                                            if (apDet.Encumbrance != 0)
                                            {
                                                eri.EncumbranceDebits -= apDet.Encumbrance;
                                            }
                                        }
                                        if (apDet.Encumbrance != 0)
                                        {
                                            eri.EncumbranceActivity -= apDet.Encumbrance;
                                        }
                                    }
                                    else
                                    {
                                        if (apDet.IsCredit())
                                        {
                                            eri.PendingCredits += apDet.Amount - apDet.Discount;
                                        }
                                        else
                                        {
                                            eri.PendingDebits += apDet.Amount - apDet.Discount;
                                            if (apDet.Encumbrance != 0)
                                            {
                                                // apdet.encumbrance * -1
                                                eri.PendingEncumbranceDebits -= apDet.Encumbrance;
                                                eri.PendingEncumbranceActivity -= apDet.Encumbrance;
                                            }
                                        }
                                    }
                                }
                                else if (Strings.LCase(Strings.Left(apDet.Account, 1)) == "r")
                                {
                                iLabel300:
                                    rdi = new cRevenueDetailInfo();
                                    rdi.Account = apDet.Account;
                                    rdi.Amount = apDet.Amount;
                                    rdi.Status = journ.Status;
                                    rdi.CheckNumber = apJourn.CheckNumber;
                                    rdi.Description = apDet.Description;
                                    rdi.Department = tmpAry[0];
                                    if (boolRevenuesUseDivision)
                                    {
                                        rdi.Division = tmpAry[1];
                                        rdi.Revenue = tmpAry[2];
                                    }
                                    else
                                    {
                                        rdi.Revenue = tmpAry[1];
                                    }
                                    rdi.Discount = apDet.Discount;
                                    rdi.JournalNumber = journ.JournalNumber;
                                    rdi.Encumbrance = apDet.Encumbrance;
                                    rdi.EncumbranceKey = apDet.EncumbranceDetailRecord;
                                    rdi.JournalType = "A";
                                    rdi.Period = journ.Period;
                                    rdi.OrderMonth = FCConvert.ToInt16(aryOrderMonth[rdi.Period - 1]);
                                    rdi.PostedDate = apJourn.PostedDate;
                                    rdi.Project = apDet.Project;
                                    rdi.RCB = apDet.RCB;
                                    rdi.TransactionDate = apJourn.CheckDate;
                                    rdi.VendorNumber = apJourn.VendorNumber;
                                    rdi.Warrant = FCConvert.ToInt32(Math.Round(Conversion.Val(apJourn.Warrant)));
                                    sdo.RevenueDetails.AddItem(rdi);
                                    // make summary record
                                    if (dRevenueSummaries.ContainsKey(apDet.Account))
                                    {
                                        tempDict = dRevenueSummaries[apDet.Account];
                                        if (tempDict.ContainsKey(journ.Period))
                                        {
                                            rri = (cRevenueReportInfo)tempDict[journ.Period];
                                        }
                                        else
                                        {
                                            rri = new cRevenueReportInfo();
                                            rri.Account = apDet.Account;
                                            rri.Department = tmpAry[0];
                                            if (boolRevenuesUseDivision)
                                            {
                                                rri.Division = tmpAry[1];
                                                rri.Revenue = tmpAry[2];
                                            }
                                            else
                                            {
                                                rri.Revenue = tmpAry[1];
                                            }
                                            rri.Period = journ.Period;
                                            tempDict.Add(journ.Period, rri);
                                        }
                                    }
                                    else
                                    {
                                        rri = new cRevenueReportInfo();
                                        rri.Account = apDet.Account;
                                        rri.Department = tmpAry[0];
                                        if (boolRevenuesUseDivision)
                                        {
                                            rri.Division = tmpAry[1];
                                            rri.Revenue = tmpAry[2];
                                        }
                                        else
                                        {
                                            rri.Revenue = tmpAry[1];
                                        }
                                        rri.Period = journ.Period;
                                        tempDict = new Dictionary<object, object>();
                                        tempDict.Add(journ.Period, rri);
                                        dRevenueSummaries.Add(apDet.Account, tempDict);
                                    }
                                    if (journ.IsPosted)
                                    {
                                        if (apDet.IsCredit())
                                        {
                                            rri.PostedCredits += apDet.Amount - apDet.Discount;
                                        }
                                        else
                                        {
                                            rri.PostedDebits += apDet.Amount - apDet.Discount;
                                            if (apDet.Encumbrance != 0)
                                            {
                                                rri.EncumbranceDebits -= apDet.Encumbrance;
                                            }
                                        }
                                        if (apDet.Encumbrance != 0)
                                        {
                                            rri.EncumbranceActivity -= apDet.Encumbrance;
                                        }
                                    }
                                    else
                                    {
                                        if (apDet.IsCredit())
                                        {
                                            rri.PendingCredits += apDet.Amount - apDet.Discount;
                                        }
                                        else
                                        {
                                            rri.PendingDebits += apDet.Amount - apDet.Discount;
                                            if (apDet.Encumbrance != 0)
                                            {
                                                rri.EncumbranceDebits -= apDet.Encumbrance;
                                            }
                                        }
                                        if (apDet.Encumbrance != 0)
                                        {
                                            rri.EncumbranceActivity -= apDet.Encumbrance;
                                        }
                                    }
                                }
                                apJourn.JournalEntries.MoveNext();
                            }
                            collAPJourns.MoveNext();
                        }
                    }
                    else if (Strings.LCase(journ.JournalType) == "en")
                    {
                    iLabel400:
                        collENJourns = encController.GetFullEncumbrancesByJournalNumber(journ.JournalNumber, strArchivePath);
                        collENJourns.MoveFirst();
                        while (collENJourns.IsCurrent())
                        {
                        iLabel450:
                            enJourn = (cEncumbrance)collENJourns.GetCurrentItem();
                            enJourn.EncumbranceDetails.MoveFirst();
                            while (enJourn.EncumbranceDetails.IsCurrent())
                            {
                            ////////Application.DoEvents();
                            iLabel500:
                                enDet = (cEncumbranceDetail)enJourn.EncumbranceDetails.GetCurrentItem();
                                tmpAry = Strings.Split(Strings.Mid(enDet.Account, 3), "-", -1, CompareConstants.vbBinaryCompare);
                                if (Strings.LCase(Strings.Left(enDet.Account, 1)) == "g")
                                {
                                    // make detail record
                                    ldi = new cLedgerDetailInfo();
                                    ldi.Account = enDet.Account;
                                    ldi.Fund = tmpAry[0];
                                    ldi.LedgerAccount = tmpAry[1];
                                    if (Information.UBound(tmpAry, 1) > 1)
                                    {
                                        ldi.Suffix = tmpAry[2];
                                    }
                                    ldi.Description = enDet.Description;
                                    ldi.JournalNumber = journ.JournalNumber;
                                    ldi.JournalType = "E";
                                    ldi.Period = journ.Period;
                                    ldi.OrderMonth = FCConvert.ToInt16(aryOrderMonth[ldi.Period - 1]);
                                    ldi.PostedDate = enJourn.PostedDate;
                                    ldi.Project = enDet.Project;
                                    ldi.Status = journ.Status;
                                    ldi.TransactionDate = enJourn.EncumbrancesDate;
                                    ldi.VendorNumber = enJourn.VendorNumber;
                                    ldi.Adjustments = enDet.Adjustments;
                                    ldi.Liquidated = enDet.LiquidatedAmount;
                                    ldi.EncumbranceKey = enDet.EncumbranceID;
                                    ldi.Amount = enDet.Amount;
                                    sdo.LedgerDetails.AddItem(ldi);
                                    // make summary record
                                    if (dLedgerSummaries.ContainsKey(enDet.Account))
                                    {
                                        tempDict = dLedgerSummaries[enDet.Account];
                                        if (tempDict.ContainsKey(journ.Period))
                                        {
                                            lri = (cLedgerReportInfo)tempDict[journ.Period];
                                        }
                                        else
                                        {
                                            lri = new cLedgerReportInfo();
                                            lri.Account = enDet.Account;
                                            lri.Fund = tmpAry[0];
                                            lri.LedgerAccount = tmpAry[1];
                                            if (Information.UBound(tmpAry, 1) > 1)
                                            {
                                                lri.Suffix = tmpAry[2];
                                            }
                                            lri.Period = journ.Period;
                                            tempDict.Add(journ.Period, lri);
                                        }
                                    }
                                    else
                                    {
                                        lri = new cLedgerReportInfo();
                                        lri.Account = enDet.Account;
                                        lri.Fund = tmpAry[0];
                                        lri.LedgerAccount = tmpAry[1];
                                        if (Information.UBound(tmpAry, 1) > 1)
                                        {
                                            lri.Suffix = tmpAry[2];
                                        }
                                        lri.Period = journ.Period;
                                        tempDict = new Dictionary<object, object>();
                                        tempDict.Add(journ.Period, lri);
                                        dLedgerSummaries.Add(enDet.Account, tempDict);
                                    }
                                }
                                else if (Strings.LCase(Strings.Left(enDet.Account, 1)) == "e")
                                {
                                    // make detail record
                                    edi = new cExpenseDetailInfo();
                                    edi.Account = enDet.Account;
                                    edi.Amount = enDet.Amount;
                                    edi.Status = journ.Status;
                                    edi.Department = tmpAry[0];
                                    if (boolExpensesUseDivision)
                                    {
                                        edi.Division = tmpAry[1];
                                        edi.Expense = tmpAry[2];
                                        if (boolUseObject)
                                        {
                                            edi.ExpenseObject = tmpAry[3];
                                        }
                                    }
                                    else
                                    {
                                        edi.Expense = tmpAry[1];
                                        if (boolUseObject)
                                        {
                                            edi.ExpenseObject = tmpAry[2];
                                        }
                                    }
                                    edi.Description = enDet.Description;
                                    edi.JournalNumber = journ.JournalNumber;
                                    edi.JournalType = "E";
                                    edi.Period = journ.Period;
                                    edi.OrderMonth = FCConvert.ToInt16(aryOrderMonth[edi.Period - 1]);
                                    edi.PostedDate = enJourn.PostedDate;
                                    edi.Project = enDet.Project;
                                    edi.RCB = "E";
                                    edi.TransactionDate = enJourn.EncumbrancesDate;
                                    edi.VendorNumber = enJourn.VendorNumber;
                                    edi.Warrant = 0;
                                    edi.Liquidated = enDet.LiquidatedAmount;
                                    edi.EncumbranceKey = enDet.EncumbranceID;
                                    edi.Adjustments = enDet.Adjustments;
                                    sdo.ExpenseDetails.AddItem(edi);
                                }
                                else if (Strings.LCase(Strings.Left(enDet.Account, 1)) == "r")
                                {
                                    // make detail record
                                    rdi = new cRevenueDetailInfo();
                                    rdi.Account = enDet.Account;
                                    rdi.Amount = enDet.Amount;
                                    rdi.Status = journ.Status;
                                    rdi.Description = enDet.Description;
                                    rdi.Department = tmpAry[0];
                                    if (boolRevenuesUseDivision)
                                    {
                                        rdi.Division = tmpAry[1];
                                        rdi.Revenue = tmpAry[2];
                                    }
                                    else
                                    {
                                        rdi.Revenue = tmpAry[1];
                                    }
                                    rdi.JournalNumber = journ.JournalNumber;
                                    rdi.JournalType = "E";
                                    rdi.Period = journ.Period;
                                    rdi.OrderMonth = FCConvert.ToInt16(aryOrderMonth[rdi.Period - 1]);
                                    rdi.PostedDate = enJourn.PostedDate;
                                    rdi.Project = enDet.Project;
                                    rdi.RCB = "E";
                                    rdi.TransactionDate = enJourn.EncumbrancesDate;
                                    rdi.VendorNumber = enJourn.VendorNumber;
                                    rdi.Warrant = 0;
                                    rdi.EncumbranceKey = enDet.EncumbranceID;
                                    rdi.Adjustments = enDet.Adjustments;
                                    rdi.Liquidated = enDet.LiquidatedAmount;
                                    sdo.RevenueDetails.AddItem(rdi);
                                }
                                enJourn.EncumbranceDetails.MoveNext();
                            }
                            collENJourns.MoveNext();
                        }
                    }
                    else
                    {
                    iLabel1000:
                        while (journ.JournalEntries.IsCurrent())
                        {
                        ////////Application.DoEvents();
                        iLabel1010:
                            jDet = (cJournalEntry)journ.JournalEntries.GetCurrentItem();
                            if (Strings.LCase(Strings.Left(jDet.Account, 1)) == "g")
                            {
                            iLabel1100:
                                tmpAry = Strings.Split(Strings.Mid(jDet.Account, 3), "-", -1, CompareConstants.vbBinaryCompare);
                                // make detail record
                                ldi = new cLedgerDetailInfo();
                                ldi.Account = jDet.Account;
                                ldi.Fund = tmpAry[0];
                                ldi.LedgerAccount = tmpAry[1];
                                if (Information.UBound(tmpAry, 1) > 1)
                                {
                                    ldi.Suffix = tmpAry[2];
                                }
                                ldi.Description = jDet.Description;
                                ldi.CheckNumber = jDet.CheckNumber;
                                ldi.JournalNumber = journ.JournalNumber;
                                ldi.JournalType = jDet.JournalType;
                                ldi.Period = journ.Period;
                                ldi.OrderMonth = FCConvert.ToInt16(aryOrderMonth[ldi.Period - 1]);
                                ldi.PostedDate = jDet.PostedDate;
                                ldi.Project = jDet.Project;
                                ldi.RCB = jDet.RCB;
                                ldi.Status = journ.Status;
                                ldi.TransactionDate = jDet.JournalEntriesDate;
                                ldi.VendorNumber = jDet.VendorNumber;
                                ldi.Warrant = jDet.WarrantNumber;
                                ldi.Amount = jDet.Amount;
                                sdo.LedgerDetails.AddItem(ldi);
                                // make summary record
                                if (dLedgerSummaries.ContainsKey(jDet.Account))
                                {
                                    tempDict = dLedgerSummaries[jDet.Account];
                                    if (tempDict.ContainsKey(journ.Period))
                                    {
                                        lri = (cLedgerReportInfo)tempDict[journ.Period];
                                    }
                                    else
                                    {
                                        lri = new cLedgerReportInfo();
                                        lri.Account = jDet.Account;
                                        lri.Fund = tmpAry[0];
                                        lri.LedgerAccount = tmpAry[1];
                                        if (Information.UBound(tmpAry, 1) > 1)
                                        {
                                            lri.Suffix = tmpAry[2];
                                        }
                                        lri.Period = journ.Period;
                                        tempDict.Add(journ.Period, lri);
                                    }
                                }
                                else
                                {
                                    lri = new cLedgerReportInfo();
                                    lri.Account = jDet.Account;
                                    lri.Fund = tmpAry[0];
                                    lri.LedgerAccount = tmpAry[1];
                                    if (Information.UBound(tmpAry, 1) > 1)
                                    {
                                        lri.Suffix = tmpAry[2];
                                    }
                                    lri.Period = journ.Period;
                                    tempDict = new Dictionary<object, object>();
                                    tempDict.Add(journ.Period, lri);
                                    dLedgerSummaries.Add(jDet.Account, tempDict);
                                }
                                if (journ.IsPosted)
                                {
                                    if (jDet.IsBudgetAdjustment())
                                    {
                                        if (journ.IsAutomaticJournal)
                                        {
                                            lri.AutoBudgetAdjustments += jDet.Amount;
                                        }
                                        lri.BudgetAdjustments += jDet.Amount;
                                        if (jDet.CarryForward)
                                        {
                                            lri.CarryForward += jDet.Amount;
                                        }
                                    }
                                    else
                                    {
                                        if (jDet.RCB != "E")
                                        {
                                            if (jDet.IsCredit())
                                            {
                                                lri.PostedCredits += jDet.Amount;
                                            }
                                            else
                                            {
                                                if (!jDet.IsPayrollContract())
                                                {
                                                    lri.PostedDebits += jDet.Amount;
                                                }
                                                else
                                                {
                                                    lri.PostedDebits -= jDet.Amount;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (jDet.JournalType == "G" || jDet.JournalType == "P")
                                            {
                                                lri.EncumbranceActivity += jDet.Amount;
                                                if (jDet.IsCredit())
                                                {
                                                    lri.EncumbranceCredits += jDet.Amount;
                                                }
                                                else
                                                {
                                                    lri.EncumbranceDebits += jDet.Amount;
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (jDet.IsCredit())
                                    {
                                        lri.PendingCredits += jDet.Amount;
                                    }
                                    else
                                    {
                                        if (!jDet.IsPayrollContract())
                                        {
                                            lri.PendingDebits += jDet.Amount;
                                        }
                                        else
                                        {
                                            lri.PendingDebits -= jDet.Amount;
                                        }
                                    }
                                }
                            }
                            else if (Strings.LCase(Strings.Left(jDet.Account, 1)) == "e")
                            {
                            iLabel1400:
                                tmpAry = Strings.Split(Strings.Mid(jDet.Account, 3), "-", -1, CompareConstants.vbBinaryCompare);
                                // make detail record
                                edi = new cExpenseDetailInfo();
                                edi.Account = jDet.Account;
                                edi.Amount = jDet.Amount;
                                edi.CheckNumber = jDet.CheckNumber;
                                edi.Status = journ.Status;
                                edi.Department = tmpAry[0];
                                if (boolExpensesUseDivision)
                                {
                                    edi.Division = tmpAry[1];
                                    edi.Expense = tmpAry[2];
                                    if (boolUseObject)
                                    {
                                        edi.ExpenseObject = tmpAry[3];
                                    }
                                }
                                else
                                {
                                    edi.Expense = tmpAry[1];
                                    if (boolUseObject)
                                    {
                                        edi.ExpenseObject = tmpAry[2];
                                    }
                                }
                                edi.Description = jDet.Description;
                                edi.JournalNumber = journ.JournalNumber;
                                edi.JournalType = jDet.JournalType;
                                edi.Period = journ.Period;
                                edi.OrderMonth = FCConvert.ToInt16(aryOrderMonth[edi.Period - 1]);
                                edi.PostedDate = jDet.PostedDate;
                                edi.Project = jDet.Project;
                                edi.RCB = jDet.RCB;
                                edi.TransactionDate = jDet.JournalEntriesDate;
                                edi.VendorNumber = jDet.VendorNumber;
                                edi.Warrant = jDet.WarrantNumber;
                                sdo.ExpenseDetails.AddItem(edi);
                                // make summary record
                                if (dExpenseSummaries.ContainsKey(jDet.Account))
                                {
                                    tempDict = dExpenseSummaries[jDet.Account];
                                    if (tempDict.ContainsKey(journ.Period))
                                    {
                                        eri = (cExpenseReportInfo)tempDict[journ.Period];
                                    }
                                    else
                                    {
                                        eri = new cExpenseReportInfo();
                                        eri.Account = jDet.Account;
                                        eri.Department = tmpAry[0];
                                        if (boolExpensesUseDivision)
                                        {
                                            eri.Division = tmpAry[1];
                                            eri.Expense = tmpAry[2];
                                            if (boolUseObject)
                                            {
                                                eri.AccountObject = tmpAry[3];
                                            }
                                        }
                                        else
                                        {
                                            eri.Expense = tmpAry[1];
                                            if (boolUseObject)
                                            {
                                                eri.AccountObject = tmpAry[2];
                                            }
                                        }
                                        eri.Period = journ.Period;
                                        tempDict.Add(journ.Period, eri);
                                    }
                                }
                                else
                                {
                                    eri = new cExpenseReportInfo();
                                    eri.Account = jDet.Account;
                                    eri.Department = tmpAry[0];
                                    if (boolExpensesUseDivision)
                                    {
                                        eri.Division = tmpAry[1];
                                        eri.Expense = tmpAry[2];
                                        if (boolUseObject)
                                        {
                                            eri.AccountObject = tmpAry[3];
                                        }
                                    }
                                    else
                                    {
                                        eri.Expense = tmpAry[1];
                                        if (boolUseObject)
                                        {
                                            eri.AccountObject = tmpAry[2];
                                        }
                                    }
                                    eri.Period = journ.Period;
                                    tempDict = new Dictionary<object, object>();
                                    tempDict.Add(journ.Period, eri);
                                    dExpenseSummaries.Add(jDet.Account, tempDict);
                                }
                                if (journ.IsPosted)
                                {
                                    if (jDet.IsBudgetAdjustment())
                                    {
                                        if (journ.IsAutomaticJournal)
                                        {
                                            eri.AutoBudgetAdjustments += jDet.Amount;
                                        }
                                        eri.BudgetAdjustments += jDet.Amount;
                                        if (jDet.CarryForward)
                                        {
                                            eri.CarryForward += jDet.Amount;
                                        }
                                    }
                                    else
                                    {
                                        if (jDet.RCB != "E")
                                        {
                                            if (jDet.IsCredit())
                                            {
                                                eri.PostedCredits += jDet.Amount;
                                            }
                                            else
                                            {
                                                eri.PostedDebits += jDet.Amount;
                                            }
                                        }
                                        else
                                        {
                                            eri.EncumbranceActivity += jDet.Amount;
                                        }
                                    }
                                }
                                else
                                {
                                    if (jDet.IsCredit())
                                    {
                                        eri.PendingCredits += jDet.Amount;
                                    }
                                    else
                                    {
                                        eri.PendingDebits += jDet.Amount;
                                    }
                                }
                            }
                            else if (Strings.LCase(Strings.Left(jDet.Account, 1)) == "r")
                            {
                            iLabel1500:
                                tmpAry = Strings.Split(Strings.Mid(jDet.Account, 3), "-", -1, CompareConstants.vbBinaryCompare);
                                // make detail record
                                rdi = new cRevenueDetailInfo();
                                rdi.Account = jDet.Account;
                                rdi.Amount = jDet.Amount;
                                rdi.Status = journ.Status;
                                rdi.CheckNumber = jDet.CheckNumber;
                                rdi.Description = jDet.Description;
                                rdi.Department = tmpAry[0];
                                if (boolRevenuesUseDivision)
                                {
                                    rdi.Division = tmpAry[1];
                                    rdi.Revenue = tmpAry[2];
                                }
                                else
                                {
                                    rdi.Revenue = tmpAry[1];
                                }
                                rdi.JournalNumber = journ.JournalNumber;
                                rdi.JournalType = jDet.JournalType;
                                rdi.Period = jDet.Period;
                                rdi.OrderMonth = FCConvert.ToInt16(aryOrderMonth[rdi.Period - 1]);
                                rdi.PostedDate = jDet.PostedDate;
                                rdi.Project = jDet.Project;
                                rdi.RCB = jDet.RCB;
                                rdi.TransactionDate = jDet.JournalEntriesDate;
                                rdi.VendorNumber = jDet.VendorNumber;
                                rdi.Warrant = jDet.WarrantNumber;
                                sdo.RevenueDetails.AddItem(rdi);
                                // make summary record
                                if (dRevenueSummaries.ContainsKey(jDet.Account))
                                {
                                    tempDict = dRevenueSummaries[jDet.Account];
                                    if (tempDict.ContainsKey(journ.Period))
                                    {
                                        rri = (cRevenueReportInfo)tempDict[journ.Period];
                                    }
                                    else
                                    {
                                        rri = new cRevenueReportInfo();
                                        rri.Account = jDet.Account;
                                        rri.Department = tmpAry[0];
                                        if (boolRevenuesUseDivision)
                                        {
                                            rri.Division = tmpAry[1];
                                            rri.Revenue = tmpAry[2];
                                        }
                                        else
                                        {
                                            rri.Revenue = tmpAry[1];
                                        }
                                        rri.Period = journ.Period;
                                        tempDict.Add(journ.Period, rri);
                                    }
                                }
                                else
                                {
                                    rri = new cRevenueReportInfo();
                                    rri.Account = jDet.Account;
                                    rri.Department = tmpAry[0];
                                    if (boolRevenuesUseDivision)
                                    {
                                        rri.Division = tmpAry[1];
                                        rri.Revenue = tmpAry[2];
                                    }
                                    else
                                    {
                                        rri.Revenue = tmpAry[1];
                                    }
                                    rri.Period = journ.Period;
                                    tempDict = new Dictionary<object, object>();
                                    tempDict.Add(journ.Period, rri);
                                    dRevenueSummaries.Add(jDet.Account, tempDict);
                                }
                                if (journ.IsPosted)
                                {
                                    if (jDet.IsBudgetAdjustment())
                                    {
                                        if (journ.IsAutomaticJournal)
                                        {
                                            rri.AutoBudgetAdjustments += jDet.Amount;
                                        }
                                        rri.BudgetAdjustments += jDet.Amount;
                                        if (jDet.CarryForward)
                                        {
                                            rri.CarryForward += jDet.Amount;
                                        }
                                    }
                                    else
                                    {
                                        if (!(jDet.RCB == "E"))
                                        {
                                            if (jDet.IsCredit())
                                            {
                                                rri.PostedCredits += jDet.Amount;
                                            }
                                            else
                                            {
                                                rri.PostedDebits += jDet.Amount;
                                            }
                                        }
                                        else
                                        {
                                            rri.EncumbranceActivity += jDet.Amount;
                                        }
                                    }
                                }
                                else
                                {
                                    if (jDet.IsCredit())
                                    {
                                        rri.PendingCredits += jDet.Amount;
                                    }
                                    else
                                    {
                                        rri.PendingDebits += jDet.Amount;
                                    }
                                }
                            }
                            journ.JournalEntries.MoveNext();
                        }
                    }
                    collJournals.MoveNext();
                }
            iLabel1600:
                collJournals = null;
                sdo.ExpenseDetails.MoveFirst();
                sdo.LedgerDetails.MoveFirst();
                sdo.RevenueDetails.MoveFirst();
                // vbPorter upgrade warning: aryLSums As object	OnRead(cLedgerReportInfo, cRevenueReportInfo, cExpenseReportInfo)
                object[] aryLSums = new object[] {

                };
                // vbPorter upgrade warning: aryAccts As object	OnRead(Dictionary<object, object>)
                Dictionary<object, object>[] aryAccts = new Dictionary<object, object>[dLedgerSummaries.Values.Count];
                object[] aryKeys = new object[] {

                };
                double dblOriginalBudget = 0;
                // vbPorter upgrade warning: aryAccountMaster As object	OnRead(cBDAccount)
                object[] aryAccountMaster = new object[dictAccounts.Values.Count];
                dictAccounts.Values.CopyTo(aryAccountMaster, 0);
                dLedgerSummaries.Values.CopyTo(aryAccts, 0);
                Array.Resize(ref aryKeys, dLedgerSummaries.Keys.Count);
                dLedgerSummaries.Keys.CopyTo(aryKeys, 0);
                clsDRWrapper rsSave = new clsDRWrapper();
                if (strArchivePath != "")
                {
                    rsSave.GroupName = strArchivePath;
                }
                rsSave.Execute("delete from ledgerreportinfo", "Budgetary");
                rsSave.Execute("delete from expensereportinfo", "Budgetary");
                rsSave.Execute("delete from revenuereportinfo", "Budgetary");
                rsSave.Execute("delete from ledgerdetailinfo", "Budgetary");
                rsSave.Execute("delete from expensedetailinfo", "Budgetary");
                rsSave.Execute("delete from revenuedetailinfo", "Budgetary");
                Dictionary<object, object> summDict = new Dictionary<object, object>();
                int y;
                rsSave.OpenRecordset("select * from ledgerreportinfo where id = -1", "Budgetary");
                for (x = 0; x <= Information.UBound(aryAccts, 1); x++)
                {
                    summDict = aryAccts[x];
                    Array.Resize(ref aryLSums, summDict.Values.Count);
                    summDict.Values.CopyTo(aryLSums, 0);
                    dblOriginalBudget = 0;
                    if (dictAccounts.ContainsKey(aryKeys[x]))
                    {
                        tempAccount = (cBDAccount)dictAccounts[aryKeys[x]];
                        dblOriginalBudget = tempAccount.CurrentBudget;
                    }
                    for (y = 0; y <= Information.UBound(aryLSums, 1); y++)
                    {
                        ////////Application.DoEvents();
                        lri = (cLedgerReportInfo)aryLSums[y];
                        rsSave.AddNew();
                        rsSave.Set_Fields("Account", lri.Account);
                        rsSave.Set_Fields("Period", lri.Period);
                        rsSave.Set_Fields("Fund", lri.Fund);
                        rsSave.Set_Fields("LedgerAccount", lri.LedgerAccount);
                        rsSave.Set_Fields("Suffix", lri.Suffix);
                        rsSave.Set_Fields("BudgetAdjustments", lri.BudgetAdjustments);
                        if (y > 0)
                        {
                            dblOriginalBudget = 0;
                        }
                        lri.OriginalBudget = dblOriginalBudget;
                        rsSave.Set_Fields("OriginalBudget", lri.OriginalBudget);
                        rsSave.Set_Fields("postedDebits", lri.PostedDebits);
                        rsSave.Set_Fields("PostedCredits", lri.PostedCredits);
                        rsSave.Set_Fields("EncumbranceDebits", lri.EncumbranceDebits);
                        rsSave.Set_Fields("EncumbActivity", lri.EncumbranceActivity);
                        rsSave.Set_Fields("PendingDebits", lri.PendingDebits);
                        rsSave.Set_Fields("PendingCredits", lri.PendingCredits);
                        rsSave.Set_Fields("EncumbranceCredits", lri.EncumbranceCredits);
                        rsSave.Set_Fields("Project", lri.Project);
                        rsSave.Set_Fields("AutomaticBudgetAdjustments", lri.AutoBudgetAdjustments);
                        rsSave.Set_Fields("PendingEncumbActivity", lri.PendingEncumbranceActivity);
                        rsSave.Set_Fields("PendingEncumbranceDebits", lri.PendingEncumbranceDebits);
                        rsSave.Set_Fields("CarryForward", lri.CarryForward);
                        // rsSave.Update
                    }
                }
                for (x = 0; x <= Information.UBound((object[])aryAccountMaster, 1); x++)
                {
                    tempAccount = (cBDAccount)((object[])aryAccountMaster)[x];
                    if (!dLedgerSummaries.ContainsKey(tempAccount.Account) && tempAccount.IsLedger())
                    {
                        rsSave.AddNew();
                        rsSave.Set_Fields("Account", tempAccount.Account);
                        rsSave.Set_Fields("Period", 0);
                        rsSave.Set_Fields("Fund", tempAccount.FirstAccountField);
                        rsSave.Set_Fields("LedgerAccount", tempAccount.SecondAccountField);
                        if (bdSettings.LedgersUseSuffix)
                        {
                            rsSave.Set_Fields("Suffix", tempAccount.ThirdAccountField);
                        }
                        else
                        {
                            rsSave.Set_Fields("suffix", "");
                        }
                        rsSave.Set_Fields("BudgetAdjustments", 0);
                        rsSave.Set_Fields("OriginalBudget", tempAccount.CurrentBudget);
                        rsSave.Set_Fields("postedDebits", 0);
                        rsSave.Set_Fields("PostedCredits", 0);
                        rsSave.Set_Fields("EncumbranceDebits", 0);
                        rsSave.Set_Fields("EncumbActivity", 0);
                        rsSave.Set_Fields("PendingDebits", 0);
                        rsSave.Set_Fields("PendingCredits", 0);
                        rsSave.Set_Fields("EncumbranceCredits", 0);
                        rsSave.Set_Fields("Project", "");
                        rsSave.Set_Fields("AutomaticBudgetAdjustments", 0);
                        rsSave.Set_Fields("PendingEncumbActivity", 0);
                        rsSave.Set_Fields("PendingEncumbranceDebits", 0);
                        rsSave.Set_Fields("CarryForward", 0);
                    }
                }
                rsSave.BulkCopy("LedgerReportInfo", "Budgetary", true);
                Array.Resize(ref aryAccts, dRevenueSummaries.Values.Count);
                dRevenueSummaries.Values.CopyTo(aryAccts, 0);
                Array.Resize(ref aryKeys, dRevenueSummaries.Keys.Count);
                dRevenueSummaries.Keys.CopyTo(aryKeys, 0);
                rsSave.OpenRecordset("select * from revenuereportinfo where id = -1", "Budgetary");
                for (x = 0; x <= Information.UBound(aryAccts, 1); x++)
                {
                    summDict = aryAccts[x];
                    Array.Resize(ref aryLSums, summDict.Values.Count);
                    summDict.Values.CopyTo(aryLSums, 0);
                    dblOriginalBudget = 0;
                    if (dictAccounts.ContainsKey(aryKeys[x]))
                    {
                        tempAccount = (cBDAccount)dictAccounts[aryKeys[x]];
                        dblOriginalBudget = tempAccount.CurrentBudget;
                    }
                    for (y = 0; y <= Information.UBound(aryLSums, 1); y++)
                    {
                        ////////Application.DoEvents();
                        rri = (cRevenueReportInfo)aryLSums[y];
                        rsSave.AddNew();
                        rsSave.Set_Fields("account", rri.Account);
                        rsSave.Set_Fields("period", rri.Period);
                        rsSave.Set_Fields("department", rri.Department);
                        rsSave.Set_Fields("Division", rri.Division);
                        rsSave.Set_Fields("Revenue", rri.Revenue);
                        rsSave.Set_Fields("BudgetAdjustments", rri.BudgetAdjustments);
                        if (y > 0)
                        {
                            dblOriginalBudget = 0;
                        }
                        rri.OriginalBudget = dblOriginalBudget;
                        rsSave.Set_Fields("OriginalBudget", rri.OriginalBudget);
                        rsSave.Set_Fields("PostedDebits", rri.PostedDebits);
                        rsSave.Set_Fields("PostedCredits", rri.PostedCredits);
                        rsSave.Set_Fields("PendingDebits", rri.PendingDebits);
                        rsSave.Set_Fields("PendingCredits", rri.PendingCredits);
                        rsSave.Set_Fields("EncumbranceDebits", rri.EncumbranceDebits);
                        rsSave.Set_Fields("EncumbranceCredits", rri.EncumbranceCredits);
                        rsSave.Set_Fields("MonthlyBudget", rri.MonthlyBudget);
                        rsSave.Set_Fields("MonthlyBudgetAdjustments", rri.MonthlyBudgetAdjustments);
                        rsSave.Set_Fields("Project", rri.Project);
                        rsSave.Set_Fields("AutomaticBudgetAdjustments", rri.AutoBudgetAdjustments);
                        rsSave.Set_Fields("PendingEncumbActivity", rri.PendingEncumbranceActivity);
                        rsSave.Set_Fields("PendingEncumbranceDebits", rri.PendingEncumbranceDebits);
                        rsSave.Set_Fields("CarryForward", rri.CarryForward);
                        // rsSave.Update
                    }
                }
                for (x = 0; x <= Information.UBound((object[])aryAccountMaster, 1); x++)
                {
                    tempAccount = (cBDAccount)((object[])aryAccountMaster)[x];
                    if (!dRevenueSummaries.ContainsKey(tempAccount.Account) && tempAccount.IsRevenue())
                    {
                        rsSave.AddNew();
                        rsSave.Set_Fields("Department", tempAccount.FirstAccountField);
                        if (bdSettings.RevenuesUseDivision)
                        {
                            rsSave.Set_Fields("Division", tempAccount.SecondAccountField);
                            rsSave.Set_Fields("Revenue", tempAccount.ThirdAccountField);
                        }
                        else
                        {
                            rsSave.Set_Fields("Division", "");
                            rsSave.Set_Fields("Revenue", tempAccount.SecondAccountField);
                        }
                        rsSave.Set_Fields("Account", tempAccount.Account);
                        rsSave.Set_Fields("period", 0);
                        rsSave.Set_Fields("originalbudget", tempAccount.CurrentBudget);
                        rsSave.Set_Fields("posteddebits", 0);
                        rsSave.Set_Fields("postedcredits", 0);
                        rsSave.Set_Fields("Pendingdebits", 0);
                        rsSave.Set_Fields("pendingcredits", 0);
                        rsSave.Set_Fields("EncumbranceDebits", 0);
                        rsSave.Set_Fields("encumbrancecredits", 0);
                        rsSave.Set_Fields("MonthlyBUdget", 0);
                        rsSave.Set_Fields("MonthlyBudgetAdjustments", 0);
                        rsSave.Set_Fields("project", "");
                        rsSave.Set_Fields("AutomaticBudgetAdjustments", 0);
                        rsSave.Set_Fields("PendingEncumbActivity", 0);
                        rsSave.Set_Fields("PendingEncumbranceDebits", 0);
                        rsSave.Set_Fields("carryforward", 0);
                    }
                }
                rsSave.BulkCopy("RevenueReportInfo", "Budgetary", true);
                Array.Resize(ref aryAccts, dExpenseSummaries.Values.Count);
                dExpenseSummaries.Values.CopyTo(aryAccts, 0);
                Array.Resize(ref aryKeys, dExpenseSummaries.Keys.Count);
                dExpenseSummaries.Keys.CopyTo(aryKeys, 0);
                rsSave.OpenRecordset("select * from ExpenseReportInfo where id = -1", "Budgetary");
                for (x = 0; x <= Information.UBound(aryAccts, 1); x++)
                {
                    summDict = aryAccts[x];
                    Array.Resize(ref aryLSums, summDict.Values.Count);
                    summDict.Values.CopyTo(aryLSums, 0);
                    dblOriginalBudget = 0;
                    if (dictAccounts.ContainsKey(aryKeys[x]))
                    {
                        tempAccount = (cBDAccount)dictAccounts[aryKeys[x]];
                        dblOriginalBudget = tempAccount.CurrentBudget;
                    }
                    for (y = 0; y <= Information.UBound(aryLSums, 1); y++)
                    {
                        ////////Application.DoEvents();
                        if (y > 0)
                        {
                            dblOriginalBudget = 0;
                        }
                        eri = (cExpenseReportInfo)aryLSums[y];
                        rsSave.AddNew();
                        rsSave.Set_Fields("account", eri.Account);
                        rsSave.Set_Fields("period", eri.Period);
                        rsSave.Set_Fields("department", eri.Department);
                        rsSave.Set_Fields("Division", eri.Division);
                        rsSave.Set_Fields("Expense", eri.Expense);
                        rsSave.Set_Fields("Object", eri.AccountObject);
                        rsSave.Set_Fields("BudgetAdjustments", eri.BudgetAdjustments);
                        eri.OriginalBudget = dblOriginalBudget;
                        rsSave.Set_Fields("OriginalBudget", eri.OriginalBudget);
                        rsSave.Set_Fields("PostedDebits", eri.PostedDebits);
                        rsSave.Set_Fields("PostedCredits", eri.PostedCredits);
                        rsSave.Set_Fields("PendingDebits", eri.PendingDebits);
                        rsSave.Set_Fields("PendingCredits", eri.PendingCredits);
                        rsSave.Set_Fields("EncumbranceDebits", eri.EncumbranceDebits);
                        rsSave.Set_Fields("EncumbranceCredits", eri.EncumbranceCredits);
                        rsSave.Set_Fields("MonthlyBudget", eri.MonthlyBudget);
                        rsSave.Set_Fields("MonthlyBudgetAdjustments", eri.MonthlyBudgetAdjustments);
                        rsSave.Set_Fields("Project", eri.Project);
                        rsSave.Set_Fields("AutomaticBudgetAdjustments", eri.AutoBudgetAdjustments);
                        rsSave.Set_Fields("PendingEncumbActivity", eri.PendingEncumbranceActivity);
                        rsSave.Set_Fields("PendingEncumbranceDebits", eri.PendingEncumbranceDebits);
                        rsSave.Set_Fields("CarryForward", eri.CarryForward);
                        // rsSave.Update
                    }
                }
                for (x = 0; x <= Information.UBound((object[])aryAccountMaster, 1); x++)
                {
                    tempAccount = (cBDAccount)((object[])aryAccountMaster)[x];
                    if (!dExpenseSummaries.ContainsKey(tempAccount.Account) && tempAccount.IsExpense())
                    {
                        rsSave.AddNew();
                        rsSave.Set_Fields("Department", tempAccount.FirstAccountField);
                        if (bdSettings.ExpensesUseDivision)
                        {
                            rsSave.Set_Fields("Division", tempAccount.SecondAccountField);
                            rsSave.Set_Fields("Expense", tempAccount.ThirdAccountField);
                            if (bdSettings.ExpensesUseObject)
                            {
                                rsSave.Set_Fields("object", tempAccount.FourthAccountField);
                            }
                            else
                            {
                                rsSave.Set_Fields("object", "");
                            }
                        }
                        else
                        {
                            rsSave.Set_Fields("Division", "");
                            rsSave.Set_Fields("expense", tempAccount.SecondAccountField);
                            if (bdSettings.ExpensesUseObject)
                            {
                                rsSave.Set_Fields("object", tempAccount.ThirdAccountField);
                            }
                            else
                            {
                                rsSave.Set_Fields("object", "");
                            }
                        }
                        rsSave.Set_Fields("Account", tempAccount.Account);
                        rsSave.Set_Fields("period", 0);
                        rsSave.Set_Fields("originalbudget", tempAccount.CurrentBudget);
                        rsSave.Set_Fields("posteddebits", 0);
                        rsSave.Set_Fields("postedcredits", 0);
                        rsSave.Set_Fields("Pendingdebits", 0);
                        rsSave.Set_Fields("pendingcredits", 0);
                        rsSave.Set_Fields("EncumbranceDebits", 0);
                        rsSave.Set_Fields("encumbrancecredits", 0);
                        rsSave.Set_Fields("MonthlyBUdget", 0);
                        rsSave.Set_Fields("MonthlyBudgetAdjustments", 0);
                        rsSave.Set_Fields("project", "");
                        rsSave.Set_Fields("carryforward", 0);
                    }
                }
                rsSave.BulkCopy("ExpenseReportInfo", "Budgetary", true);
                rsSave.OpenRecordset("select * from expenseDetailInfo where id = -1", "Budgetary");
                while (sdo.ExpenseDetails.IsCurrent())
                {
                    ////////Application.DoEvents();
                    edi = (cExpenseDetailInfo)sdo.ExpenseDetails.GetCurrentItem();
                    rsSave.AddNew();
                    rsSave.Set_Fields("Account", edi.Account);
                    rsSave.Set_Fields("Period", edi.Period);
                    rsSave.Set_Fields("Department", edi.Department);
                    rsSave.Set_Fields("Division", edi.Division);
                    rsSave.Set_Fields("Expense", edi.Expense);
                    rsSave.Set_Fields("Object", edi.ExpenseObject);
                    rsSave.Set_Fields("Status", edi.Status);
                    rsSave.Set_Fields("PostedDate", edi.PostedDate);
                    rsSave.Set_Fields("TransDate", edi.TransactionDate);
                    rsSave.Set_Fields("RCB", edi.RCB);
                    rsSave.Set_Fields("JournalNumber", edi.JournalNumber);
                    rsSave.Set_Fields("Description", edi.Description);
                    rsSave.Set_Fields("Warrant", edi.Warrant);
                    rsSave.Set_Fields("VendorNumber", edi.VendorNumber);
                    rsSave.Set_Fields("CheckNumber", edi.CheckNumber);
                    rsSave.Set_Fields("Amount", edi.Amount);
                    rsSave.Set_Fields("Discount", edi.Discount);
                    rsSave.Set_Fields("Encumbrance", edi.Encumbrance);
                    rsSave.Set_Fields("Adjustments", edi.Adjustments);
                    rsSave.Set_Fields("Liquidated", edi.Liquidated);
                    rsSave.Set_Fields("Type", edi.JournalType);
                    rsSave.Set_Fields("OrderMonth", edi.OrderMonth);
                    rsSave.Set_Fields("Project", edi.Project);
                    rsSave.Set_Fields("encumbrancekey", edi.EncumbranceKey);
                    // rsSave.Update
                    sdo.ExpenseDetails.MoveNext();
                }
                rsSave.BulkCopy("ExpenseDetailInfo", "Budgetary", true);
                rsSave.OpenRecordset("Select * from revenuedetailinfo where id = -1", "Budgetary");
                while (sdo.RevenueDetails.IsCurrent())
                {
                    ////////Application.DoEvents();
                    rdi = (cRevenueDetailInfo)sdo.RevenueDetails.GetCurrentItem();
                    rsSave.AddNew();
                    rsSave.Set_Fields("account", rdi.Account);
                    rsSave.Set_Fields("period", rdi.Period);
                    rsSave.Set_Fields("Department", rdi.Department);
                    rsSave.Set_Fields("Division", rdi.Division);
                    rsSave.Set_Fields("Revenue", rdi.Revenue);
                    rsSave.Set_Fields("Status", rdi.Status);
                    rsSave.Set_Fields("PostedDate", rdi.PostedDate);
                    rsSave.Set_Fields("TransDate", rdi.TransactionDate);
                    rsSave.Set_Fields("RCB", rdi.RCB);
                    rsSave.Set_Fields("JournalNumber", rdi.JournalNumber);
                    rsSave.Set_Fields("Description", rdi.Description);
                    rsSave.Set_Fields("Warrant", rdi.Warrant);
                    rsSave.Set_Fields("CheckNumber", rdi.CheckNumber);
                    rsSave.Set_Fields("VendorNumber", rdi.VendorNumber);
                    rsSave.Set_Fields("Amount", rdi.Amount);
                    rsSave.Set_Fields("Discount", rdi.Discount);
                    rsSave.Set_Fields("Encumbrance", rdi.Encumbrance);
                    rsSave.Set_Fields("Adjustments", rdi.Adjustments);
                    rsSave.Set_Fields("Liquidated", rdi.Liquidated);
                    rsSave.Set_Fields("Type", rdi.JournalType);
                    rsSave.Set_Fields("OrderMonth", rdi.OrderMonth);
                    rsSave.Set_Fields("Project", rdi.Project);
                    rsSave.Set_Fields("EncumbranceKey", rdi.EncumbranceKey);
                    // rsSave.Update
                    sdo.RevenueDetails.MoveNext();
                }
                rsSave.BulkCopy("RevenueDetailInfo", "Budgetary", true);
                rsSave.OpenRecordset("select * from ledgerdetailinfo where id = -1", "Budgetary");
                while (sdo.LedgerDetails.IsCurrent())
                {
                    ////////Application.DoEvents();
                    ldi = (cLedgerDetailInfo)sdo.LedgerDetails.GetCurrentItem();
                    rsSave.AddNew();
                    rsSave.Set_Fields("account", ldi.Account);
                    rsSave.Set_Fields("Period", ldi.Period);
                    rsSave.Set_Fields("Fund", ldi.Fund);
                    rsSave.Set_Fields("Acct", ldi.LedgerAccount);
                    rsSave.Set_Fields("Suffix", ldi.Suffix);
                    rsSave.Set_Fields("Status", ldi.Status);
                    rsSave.Set_Fields("PostedDate", ldi.PostedDate);
                    rsSave.Set_Fields("TransDate", ldi.TransactionDate);
                    rsSave.Set_Fields("RCB", ldi.RCB);
                    rsSave.Set_Fields("JournalNumber", ldi.JournalNumber);
                    rsSave.Set_Fields("Description", ldi.Description);
                    rsSave.Set_Fields("Warrant", ldi.Warrant);
                    rsSave.Set_Fields("CheckNumber", ldi.CheckNumber);
                    rsSave.Set_Fields("VendorNumber", ldi.VendorNumber);
                    rsSave.Set_Fields("Amount", ldi.Amount);
                    rsSave.Set_Fields("Discount", ldi.Discount);
                    rsSave.Set_Fields("Encumbrance", ldi.Encumbrance);
                    rsSave.Set_Fields("Adjustments", ldi.Adjustments);
                    rsSave.Set_Fields("Liquidated", ldi.Liquidated);
                    rsSave.Set_Fields("Type", ldi.JournalType);
                    rsSave.Set_Fields("OrderMonth", ldi.OrderMonth);
                    rsSave.Set_Fields("Project", ldi.Project);
                    rsSave.Set_Fields("EncumbranceKey", ldi.EncumbranceKey);
                    // rsSave.Update
                    sdo.LedgerDetails.MoveNext();
                }
                rsSave.BulkCopy("LedgerDetailInfo", "Budgetary", true);
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetError(Information.Err(ex).Number, ex.GetBaseException().Message);
            }
        }

        public cJournalService() : base()
        {
            bdSettings = bdSetCont.GetBDSettings();
        }

        public void ForceCalculate(string strArchive = "")
        {
            clsDRWrapper rsInfo = new clsDRWrapper();
            frmBusy bbusy = new frmBusy();
            //FC:FINAL:DDU:#2454 - fixed loader show
            FCUtils.StartTask(bbusy, () =>
            {
                if (strArchive != "")
                {
                    rsInfo.GroupName = strArchive;
                }
                bbusy.Message = "Recalculating Summary Information";
                bbusy.StartBusy();
                CalculateSummaryDetailInfo(strArchive);
                // 
                // CalculateAccountInfo True, True, True, "E", , True
                // 
                // bbusy.Message = "Recalculating Expense Account Detail Information"
                // DoEvents
                // 
                // CalculateAccountInfo False, True, True, "E", , True
                // 
                // 
                // bbusy.Message = "Recalculating Revenue Account Summary Information"
                // DoEvents
                // CalculateAccountInfo True, True, True, "R", , True
                // bbusy.Message = "Recalculating Revenue Account Detail Information"
                // DoEvents
                // 
                // CalculateAccountInfo False, True, True, "R", , True
                // bbusy.Message = "Recalculating Ledger Account Summary Information"
                // DoEvents
                // 
                // CalculateAccountInfo True, True, True, "G", , True
                // 
                // 
                // bbusy.Message = "Recalculating Ledger Account Detail Information"
                // DoEvents
                // 
                // CalculateAccountInfo False, True, True, "G", , True
                // 
                // bbusy.Message = "Finalizing Account Information"
                // DoEvents
                rsInfo.OpenRecordset("SELECT * FROM CalculationNeeded");
                if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                {
                    rsInfo.Edit();
                    rsInfo.Set_Fields("ExpPendingSummary", false);
                    rsInfo.Set_Fields("ExpPostedSummary", false);
                    rsInfo.Set_Fields("RevPendingSummary", false);
                    rsInfo.Set_Fields("RevPostedSummary", false);
                    rsInfo.Set_Fields("LedgerPendingSummary", false);
                    rsInfo.Set_Fields("LedgerPostedSummary", false);
                    rsInfo.Set_Fields("ExpPendingDetail", false);
                    rsInfo.Set_Fields("ExpPostedDetail", false);
                    rsInfo.Set_Fields("RevPendingDetail", false);
                    rsInfo.Set_Fields("RevPostedDetail", false);
                    rsInfo.Set_Fields("LedgerPendingDetail", false);
                    rsInfo.Set_Fields("LedgerPostedDetail", false);
                    rsInfo.Set_Fields("SchoolExpPendingSummary", false);
                    rsInfo.Set_Fields("SchoolExpPostedSummary", false);
                    rsInfo.Set_Fields("SchoolRevPendingSummary", false);
                    rsInfo.Set_Fields("SchoolRevPostedSummary", false);
                    rsInfo.Set_Fields("SchoolLedgerPendingSummary", false);
                    rsInfo.Set_Fields("SchoolLedgerPostedSummary", false);
                    rsInfo.Set_Fields("SchoolExpPendingDetail", false);
                    rsInfo.Set_Fields("SchoolExpPostedDetail", false);
                    rsInfo.Set_Fields("SchoolRevPendingDetail", false);
                    rsInfo.Set_Fields("SchoolRevPostedDetail", false);
                    rsInfo.Set_Fields("SchoolLedgerPendingDetail", false);
                    rsInfo.Set_Fields("SchoolLedgerPostedDetail", false);
                    rsInfo.Update();
                }
                else
                {
                    rsInfo.AddNew();
                    rsInfo.Set_Fields("ExpPendingSummary", false);
                    rsInfo.Set_Fields("ExpPostedSummary", false);
                    rsInfo.Set_Fields("RevPendingSummary", false);
                    rsInfo.Set_Fields("RevPostedSummary", false);
                    rsInfo.Set_Fields("LedgerPendingSummary", false);
                    rsInfo.Set_Fields("LedgerPostedSummary", false);
                    rsInfo.Set_Fields("ExpPendingDetail", false);
                    rsInfo.Set_Fields("ExpPostedDetail", false);
                    rsInfo.Set_Fields("RevPendingDetail", false);
                    rsInfo.Set_Fields("RevPostedDetail", false);
                    rsInfo.Set_Fields("LedgerPendingDetail", false);
                    rsInfo.Set_Fields("LedgerPostedDetail", false);
                    rsInfo.Set_Fields("SchoolExpPendingSummary", false);
                    rsInfo.Set_Fields("SchoolExpPostedSummary", false);
                    rsInfo.Set_Fields("SchoolRevPendingSummary", false);
                    rsInfo.Set_Fields("SchoolRevPostedSummary", false);
                    rsInfo.Set_Fields("SchoolLedgerPendingSummary", false);
                    rsInfo.Set_Fields("SchoolLedgerPostedSummary", false);
                    rsInfo.Set_Fields("SchoolExpPendingDetail", false);
                    rsInfo.Set_Fields("SchoolExpPostedDetail", false);
                    rsInfo.Set_Fields("SchoolRevPendingDetail", false);
                    rsInfo.Set_Fields("SchoolRevPostedDetail", false);
                    rsInfo.Set_Fields("SchoolLedgerPendingDetail", false);
                    rsInfo.Set_Fields("SchoolLedgerPostedDetail", false);
                    rsInfo.Update();
                }
                bbusy.StopBusy();
                bbusy.Unload();
                /*- bbusy = null; */
                MessageBox.Show("Finished Calculation", "Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                FCUtils.UnlockUserInterface();
            });
            bbusy.Show(FCForm.FormShowEnum.Modal);
        }

        public Decimal RemoveExistingCreditMemos_2(int lngJournal)
        {
            return RemoveExistingCreditMemos(ref lngJournal);
        }

        public Decimal RemoveExistingCreditMemos(ref int lngJournal)
        {
            Decimal RemoveExistingCreditMemos = 0;
            clsDRWrapper rsInfo = new clsDRWrapper();
            clsDRWrapper rsDetailInfo = new clsDRWrapper();
            clsDRWrapper rsCreditMemo = new clsDRWrapper();
            clsDRWrapper rsCreditDetail = new clsDRWrapper();
            // vbPorter upgrade warning: curDetailAmount As Decimal	OnWrite(short, Decimal)
            Decimal curDetailAmount;
            rsInfo.OpenRecordset("SELECT * FROM APJournal WHERE CreditMemoRecord <> 0 AND PrintedIndividual <> 1 AND JournalNumber = " + FCConvert.ToString(lngJournal));
            if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
            {
                do
                {
                    rsDetailInfo.Execute("DELETE FROM APJournalDetail WHERE APJournalID = " + rsInfo.Get_Fields_Int32("ID"), "Budgetary");
                    //FC:FINAL:MSH - can't implicitly convert to decimal (same with issue #742)
                    // TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
                    RemoveExistingCreditMemos += FCConvert.ToDecimal(rsInfo.Get_Fields("Amount"));
                    rsCreditMemo.OpenRecordset("SELECT * FROM CreditMemo WHERE ID = " + rsInfo.Get_Fields_Int32("CreditMemoRecord"));
                    if (rsCreditMemo.EndOfFile() != true && rsCreditMemo.BeginningOfFile() != true)
                    {
                        rsCreditMemo.Edit();
                        // TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
                        rsCreditMemo.Set_Fields("Liquidated", rsCreditMemo.Get_Fields_Decimal("Liquidated") + rsInfo.Get_Fields("Amount"));
                        rsCreditMemo.Update();
                    }
                    // TODO: Check the table for the column [Amount] and replace with corresponding Get_Field method
                    curDetailAmount = FCConvert.ToDecimal(rsInfo.Get_Fields("Amount"));
                    rsCreditDetail.OpenRecordset("SELECT * FROM CreditMemoDetail WHERE Liquidated <> 0 AND CreditMemoID = " + rsCreditMemo.Get_Fields_Int32("ID"));
                    if (rsCreditDetail.EndOfFile() != true && rsCreditDetail.BeginningOfFile() != true)
                    {
                        do
                        {
                            if (curDetailAmount * -1 <= rsCreditDetail.Get_Fields_Decimal("Liquidated"))
                            {
                                rsCreditDetail.Edit();
                                rsCreditDetail.Set_Fields("Liquidated", rsCreditDetail.Get_Fields_Decimal("Liquidated") + curDetailAmount);
                                curDetailAmount = 0;
                                rsCreditDetail.Update();
                                break;
                            }
                            else
                            {
                                rsCreditDetail.Edit();
                                curDetailAmount += rsCreditDetail.Get_Fields_Decimal("Liquidated");
                                rsCreditDetail.Set_Fields("Liquidated", 0);
                                rsCreditDetail.Update();
                            }
                            rsCreditDetail.MoveNext();
                        }
                        while (rsCreditDetail.EndOfFile() != true);
                    }
                    rsInfo.Delete();
                    rsInfo.Update();
                }
                while (rsInfo.EndOfFile() != true);
            }
            RemoveExistingCreditMemos *= -1;
            return RemoveExistingCreditMemos;
        }

        private bool AllowAutoPostBD(modBudgetaryAccounting.AutoPostType aptType)
        {
            bool AllowAutoPostBD = false;
            if (Strings.LCase(modGlobalConstants.Statics.clsSecurityClass.CheckOtherPermissions(POSTING, "BD")) != "f")
            {
                AllowAutoPostBD = false;
            }
            else
            {
                AllowAutoPostBD = modBudgetaryAccounting.AutoPostAllowed(aptType);
            }
            return AllowAutoPostBD;
        }

        private string GetAccountNumber_2(string Acct)
        {
            return GetAccountNumber(ref Acct);
        }

        private string GetAccountNumber(ref string Acct)
        {
            string GetAccountNumber = "";
            if (Strings.Left(Acct + " ", 1) == "G")
            {
                GetAccountNumber = Strings.Mid(Acct, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(bdSettings.Ledger, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(bdSettings.Ledger, 3, 2)))));
            }
            else
            {
                GetAccountNumber = "";
            }
            return GetAccountNumber;
        }

        public cGenericCollection GetUnpostedAPJournalBriefs()
        {
            return apjController.GetUnpostedBriefs();
        }
    }
}
