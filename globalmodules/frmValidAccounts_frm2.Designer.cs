﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using Global;
using fecherFoundation;

namespace Global
{
	/// <summary>
	/// Summary description for frmLoadValidAccounts.
	/// </summary>
	partial class frmLoadValidAccounts : BaseForm
	{
		public FCGrid vsAcct;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.vsAcct = new fecherFoundation.FCGrid();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 545);
            this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsAcct);
            this.ClientArea.Size = new System.Drawing.Size(1078, 568);
            this.ClientArea.Controls.SetChildIndex(this.vsAcct, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(163, 28);
            this.HeaderText.Text = "Valid Accounts";
            // 
            // vsAcct
            // 
            this.vsAcct.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsAcct.Cols = 10;
            this.vsAcct.ExtendLastCol = true;
            this.vsAcct.FixedCols = 0;
            this.vsAcct.Location = new System.Drawing.Point(30, 30);
            this.vsAcct.Name = "vsAcct";
            this.vsAcct.RowHeadersVisible = false;
            this.vsAcct.Rows = 1;
            this.vsAcct.ShowFocusCell = false;
            this.vsAcct.Size = new System.Drawing.Size(1022, 515);
            this.vsAcct.TabIndex = 0;
            this.vsAcct.Click += new System.EventHandler(this.vsAcct_ClickEvent);
            this.vsAcct.DoubleClick += new System.EventHandler(this.vsAcct_DblClick);
            this.vsAcct.KeyDown += new Wisej.Web.KeyEventHandler(this.vsAcct_KeyDownEvent);
            this.vsAcct.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsAcct_KeyPressEvent);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(407, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(75, 48);
            this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // frmLoadValidAccounts
            // 
            this.ClientSize = new System.Drawing.Size(1078, 628);
            this.KeyPreview = true;
            this.Name = "frmLoadValidAccounts";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
            this.Text = "Valid Accounts";
            this.Load += new System.EventHandler(this.frmLoadValidAccounts_Load);
            this.Resize += new System.EventHandler(this.frmLoadValidAccounts_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLoadValidAccounts_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
