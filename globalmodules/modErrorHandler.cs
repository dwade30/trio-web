﻿using fecherFoundation;
using System;

namespace Global
{
	public class modErrorHandler
	{
		public static void SetErrorHandler(Exception ex)
		{
			clsErrorHandlers clsErrorHandler;
			clsErrorHandler = new clsErrorHandlers();
			clsErrorHandler.SetErrorHandler(ex);
			clsErrorHandler = null;
		}

		public class StaticVariables
		{
			// ********************************************************
			// Last Updated   :               11/17/2002              *
			// MODIFIED BY    :               Jim Bertolino           *
			// *
			// Date           :                                       *
			// WRITTEN BY     :               Matthew Larrabee        *
			// *
			// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
			// ********************************************************
			//=========================================================
			public string gstrStack = "";
			public string gstrCurrentRoutine = "";
			public string gstrFormName = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
