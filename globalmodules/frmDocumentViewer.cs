﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using System.Collections.ObjectModel;
using System.Linq;
using Wisej.Web;
using fecherFoundation;
using SharedApplication.CentralDocuments;
using TWSharedLibrary;
#if TWRE0000
using modGlobal = TWRE0000.modMDIParent;


#elif TWBD0000
using TWBD0000;

#endif
namespace Global
{
    /// <summary>
    /// Summary description for frmDocumentViewer.
    /// </summary>
    public partial class frmDocumentViewer : BaseForm
    {
        public frmDocumentViewer()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.Load += frmDocumentViewer_Load; //new System.EventHandler(this.frmDocumentViewer_Load);
            this.Shown += FrmDocumentViewer_Shown;
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }

        private void FrmDocumentViewer_Shown(object sender, EventArgs e)
        {
            LoadList();
            ShowList();
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmDocumentViewer InstancePtr
        {
            get
            {
                return (frmDocumentViewer)Sys.GetInstance(typeof(frmDocumentViewer));
            }
        }

        protected frmDocumentViewer _InstancePtr = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>

        //=========================================================
        //private string strTempPath = "";
        private CentralDocumentService docService = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);
        private string _referenceGroup;
        private string _referenceType;
        private int _referenceId;
        private string _altReferenceId;
        private string strDB;
        private bool loadingList = false;
        public Collection<CentralDocumentHeader> docList { get; set; }


        const int CNSTIDCol = 0;
        const int CNSTLinkIDCol = 1;
        const int CNSTDescriptionCol = 4;
        const int CNSTNameCol = 3;
        const int CNSTTypeCol = 2;
        private static bool boolCantEdit;
        private bool canEdit = !boolCantEdit;

        string strFileToShow = "";

        private void SetupGridList()
        {
            GridList.Cols = 5;
            GridList.ColHidden(CNSTIDCol, true);
            GridList.ColHidden(CNSTLinkIDCol, true);
            GridList.ColHidden(CNSTDescriptionCol, true);
            GridList.TextMatrix(0, CNSTTypeCol, "Type");
            GridList.TextMatrix(0, CNSTNameCol, "Name");
            GridList.TextMatrix(0, CNSTDescriptionCol, "Description");
        }

        private void ResizeGridList()
        {
            int lngGridWidth;
            lngGridWidth = GridList.WidthOriginal;
            GridList.ColWidth(CNSTTypeCol, FCConvert.ToInt32(0.2 * lngGridWidth));
            //FC:FINAL:DDU:#i1103 - enlarge to see all text
            GridList.ColWidth(CNSTNameCol, FCConvert.ToInt32(0.78 * lngGridWidth));
        }

        private void ShowList()
        {
            loadingList = true;
            GridList.Rows = 1;

            if (docList == null) return;

            string strTemp = "";
            ClearViewer();

            foreach (var centralDocument in docList)
            {
                AddDocumentToGridList(centralDocument);
            }

            if (GridList.Rows <= 1) return;
            loadingList = false;
            //GridList.Select(1, CNSTNameCol);
            ShowSelectedDocument(1);
            GridList.Sort = FCGrid.SortSettings.flexSortGenericAscending;
        }

        private void AddDocumentToGridList(CentralDocumentHeader centralDocument)
        {
            GridList.Rows += 1;
            var lngRow = GridList.Rows - 1;

            GridList.TextMatrix(lngRow, CNSTIDCol, centralDocument.ID);
            GridList.TextMatrix(lngRow, CNSTLinkIDCol, FCConvert.ToString(centralDocument.ID));
            GridList.TextMatrix(lngRow, CNSTNameCol, centralDocument.ItemName);

            GridList.TextMatrix(lngRow, CNSTTypeCol, Strings.LCase(Strings.Right("   " + centralDocument.MediaType, 3)) == "pdf" ? "PDF" : "Image");
        }

        private void ShowSelectedDocument(int lngRow)
        {
            if (lngRow <= 0) return;

            if (lngRow >= GridList.Rows) return;

            var lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridList.TextMatrix(lngRow, CNSTIDCol))));
            if (lngID > 0)
            {
                ShowCentralDocument(lngID);
            }

            //FC:FINAL:JEI enable Print MenuItem only for images. 
            //When a PDF is displayed, use the internal print button from the viewer.
            mnuFilePrint.Enabled = ImageViewer1.Controls.Count <= 0 || !(ImageViewer1.Controls[0] is PdfViewer);
        }

        private void ShowCentralDocument(int lngID)
        {
            if (lngID <= 0) throw new ArgumentOutOfRangeException(nameof(lngID));

            try
            {
                // get the whole document, including binary data for the image viewer to use
                var cenDoc = docService.GetCentralDocument(new GetCommand { Id = lngID });
                if (cenDoc == null) return;

                ImageViewer1.LoadFileFromApplication(cenDoc.ItemName, cenDoc.ItemData, cenDoc.MediaType);
                lblDescription.Text = cenDoc.ItemName;
            }
            catch (Exception ex)
            {
                // ErrorHandler:
                MessageBox.Show($"Error {FCConvert.ToString(Information.Err(ex).Number)}  {Information.Err(ex).Description}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void frmDocumentViewer_Activated(object sender, System.EventArgs e)
        {
            //if (modGlobal.FormExist(this))
            //{
            //    return;
            //}
        }

        private void frmDocumentViewer_Load(object sender, System.EventArgs e)
        {

            ImageViewer1.BackColor = System.Drawing.Color.FromArgb(255, 255, 255);
            //ImageViewer1.LicenseKey = "14735"; // Set the license so the demo message box does not keep popping up
            SetupGridList();

            mnuFileAddDocument.Enabled = canEdit;
            mnuFileDeleteDocument.Enabled = canEdit;

            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            modGlobalFunctions.SetTRIOColors(this);

        }

        private void frmDocumentViewer_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            FormUtilities.KeyPressHandler(e, this);
        }

        private void frmDocumentViewer_Resize(object sender, System.EventArgs e)
        {
            ResizeGridList();
        }

        private void GridList_RowColChange(object sender, System.EventArgs e)
        {
            if (loadingList)
            {
                return;
            }
            var lngRow = GridList.Row;

            if (lngRow > 0)
            {
                ShowSelectedDocument(lngRow);
            }
            else
            {
                ClearViewer();
            }
        }

        private void mnuFileAddDocument_Click(object sender, System.EventArgs e)
        {
            if (!frmAddCentralDocument.InstancePtr.Init(_referenceGroup, _referenceType, _referenceId, _altReferenceId)) return;

            BringToFront();
            LoadList();
            ShowList();
        }

        private void mnuFileDeleteDocument_Click(object sender, System.EventArgs e)
        {
            int lngRow;
            lngRow = GridList.Row;
            if (lngRow > 0)
            {
                if (MessageBox.Show("Are you sure you want to remove this document?", "Remove Document?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    RemoveCurrentItem(lngRow);
                }
            }
            else
            {
                MessageBox.Show("No document selected", "No Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void ClearViewer()
        {
            //ImageViewer1.FileName = "";
            ImageViewer1.Controls.Clear();
            strFileToShow = "";
            lblDescription.Text = "";
        }

        private void RemoveCurrentItem(int lngRow)
        {
            if (lngRow > 0 && lngRow <= GridList.Rows)
            {
                int lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridList.TextMatrix(lngRow, CNSTIDCol))));
                if (lngID > 0)
                {
                    RemoveCentralDocument(lngID);
                }

                LoadList();
                ShowList();
            }
        }
        private void RemoveCentralDocument(int lngID)
        {
            if (docList == null || !docList.Any()) return;

            foreach (var centralDocument in docList)
            {
                var dInfo = centralDocument;

                if (dInfo.ID != lngID) continue;

                docService.DeleteCentralDocument(new DeleteCommand { Id = lngID });
                if (docService.HadError)
                {
                    MessageBox.Show($"Error {FCConvert.ToString(docService.LastErrorNumber)}  {docService.LastErrorMessage}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                return;
            }
        }


        private void mnuFilePrint_Click(object sender, System.EventArgs e)
        {
            if (ImageViewer1.Controls.Count > 0 && ImageViewer1.Controls[0] is PictureBox)
            {
                var script =
                    $"var divToPrint=document.getElementById('id_{ImageViewer1.Controls[0].Handle}');"
                    + "var backImage = divToPrint.currentStyle || window.getComputedStyle(divToPrint, false);"
                    + "backImage = backImage.backgroundImage.slice(4, -1).replace(/\"/g, \"\");"
                    + "var newHtml = '<img src=\\''+backImage+'\\'>';var newWin = window.open('', 'Print-Window');"
                    + "var header = '<html><body onload=\\'window.print();\\'>';var footer = '</body></html>'; "
                    + "newWin.document.open();newWin.document.write(header + newHtml + footer);"
                    + "newWin.document.close();setTimeout(function(){{ newWin.close(); }},10); ";
                ImageViewer1.Eval(script);
            }
        }

        private void mnuFileRotate_Click(object sender, System.EventArgs e)
        {
            rotation1.GetRotation(ImageViewer1).RotateZ += 90;
        }

        private void mnuFileZoomAspectRatio_Click(object sender, System.EventArgs e)
        {
            ImageViewer1.Focus();
        }

        private void mnuFileZoomFitToWindow_Click(object sender, System.EventArgs e)
        {
            rotation1.GetRotation(ImageViewer1).ScaleX = 100;
            rotation1.GetRotation(ImageViewer1).ScaleY = 100;
            rotation1.GetRotation(ImageViewer1).ScaleZ = 100;
            ImageViewer1.Focus();
        }

        private void mnuFileZoomZoomIn_Click(object sender, System.EventArgs e)
        {
            rotation1.GetRotation(ImageViewer1).ScaleX += 10;
            rotation1.GetRotation(ImageViewer1).ScaleY += 10;
            rotation1.GetRotation(ImageViewer1).ScaleZ += 10;
        }

        private void mnuFileZoomZoomOut_Click(object sender, System.EventArgs e)
        {

            rotation1.GetRotation(ImageViewer1).ScaleX -= 10;
            rotation1.GetRotation(ImageViewer1).ScaleY -= 10;
            rotation1.GetRotation(ImageViewer1).ScaleZ -= 10;
        }

        private void mnuProcessQuit_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        public void Init(string strTitle, string strRefGroup, string strRefType, int lngRefID, string strAltRef, short intModal = (short)FormShowEnum.Modeless, bool boolReadOnly = false)
        {
            _referenceGroup = strRefGroup;
            _referenceType = strRefType;
            _referenceId = lngRefID;
            _altReferenceId = strAltRef;
            Text = $"Attached Documents - {strTitle}";
            Show((FormShowEnum)intModal);
            //Text = $"Attached Documents - {strTitle}";
        }

        private void LoadList()
        {
            docService = new CentralDocumentService(StaticSettings.GlobalCommandDispatcher);

            docList?.Clear();

            docList = docService.GetHeadersByReference(new GetHeadersByReferenceCommand { ReferenceId = _referenceId, ReferenceType = _referenceType, DataGroup = _referenceGroup }) 
                      ?? new Collection<CentralDocumentHeader>();
        }
    }
}
