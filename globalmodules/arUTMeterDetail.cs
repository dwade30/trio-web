﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using TWSharedLibrary.Data;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for arUTMeterDetail.
	/// </summary>
	public partial class arUTMeterDetail : BaseSectionReport
	{
		// nObj = 1
		//   0	arUTMeterDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/06/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/22/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsMaster = new clsDRWrapper();
		int lngAccountKey;
        private cPartyController partyController = new cPartyController();

        public arUTMeterDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//FC:FINAL:MSH - Issue #935: restored missed report title
			this.Name = "Meter Detail";
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.ReportEnd += ArUTMeterDetail_ReportEnd;
		}

        private void ArUTMeterDetail_ReportEnd(object sender, EventArgs e)
        {
            rsData.DisposeOf();
			rsMaster.DisposeOf();
        }

        public static arUTMeterDetail InstancePtr
		{
			get
			{
				return (arUTMeterDetail)Sys.GetInstance(typeof(arUTMeterDetail));
			}
		}

		protected arUTMeterDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			rsData.Dispose();
			rsMaster.Dispose();
			base.Dispose(disposing);
		}

		public void Init(int lngPassAccountKey)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSQL;
				lngAccountKey = lngPassAccountKey;
				strSQL = "SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(lngAccountKey);
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (rsData.EndOfFile())
				{
					MessageBox.Show("No eligible accounts found in the meter file.", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
					Cancel();
					this.Close();
				}
				else
				{
					frmReportViewer.InstancePtr.Init(this);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Init", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			SetAccountValues();
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PrintWidth = 15100 / 1440f;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!rsData.EndOfFile())
				{
					// set the tag of the subreport object to the meter ID if not combined to show the bill detail
					if (FCConvert.ToString(rsData.Get_Fields_String("Combine")) == "N")
					{
						srptMeterBillingDetail.Report = new srptUTMeterDetail();
						srptMeterBillingDetail.Report.UserData = rsData.Get_Fields_Int32("id");
						fldAvgConsumption.Text = FCConvert.ToString(GetAvgConsumption_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"))));
                        srptMeterBillingDetail.Visible = true;
                    }
					else
					{
						srptMeterBillingDetail.Visible = false;
						fldAvgConsumption.Text = "Combined";
					}
					// fill in the meter information
					// TODO: Check the table for the column [Sequence] and replace with corresponding Get_Field method
					fldBookSeq.Text = rsData.Get_Fields_Int32("BookNumber") + "/" + rsData.Get_Fields("Sequence");
					//FC:FINAL:MSH - can't implicitly convert from int to string (same with internal issue #914)
					fldMeterSize.Text = FCConvert.ToString(rsData.Get_Fields_Int32("Size"));
					fldMeterDigits.Text = FCConvert.ToString(rsData.Get_Fields_Int32("Digits"));
					// TODO: Check the table for the column [Frequency] and replace with corresponding Get_Field method
					fldFrequency.Text = FCConvert.ToString(rsData.Get_Fields("Frequency"));
					fldService.Text = FCConvert.ToString(rsData.Get_Fields_String("Service"));
					if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("FinalBilled")))
					{
						fldRateCode.Text = "F";
					}
					else if (rsData.Get_Fields_Boolean("NoBill"))
					{
						fldRateCode.Text = "N";
					}
					else
					{
						fldRateCode.Text = "W - " + rsData.Get_Fields_Int32("WCat") + "  S - " + rsData.Get_Fields_Int32("SCat");
					}
					fldSerial.Text = Strings.Trim(rsData.Get_Fields_String("SerialNumber") + " ");
					fldRemote.Text = Strings.Trim(rsData.Get_Fields_String("Remote") + " ");
					string vbPorterVar = FCConvert.ToString(rsData.Get_Fields_String("Combine"));
					if (vbPorterVar == "N")
					{
						fldCombine.Text = "No";
					}
					else if (vbPorterVar == "C")
					{
						fldCombine.Text = "Cons";
					}
					else if (vbPorterVar == "B")
					{
						fldCombine.Text = "Bill";
					}
					else
					{
						fldCombine.Text = "No";
					}
					fldMultiplier.Text = FCConvert.ToString(rsData.Get_Fields_Int32("Multiplier"));
					if (!rsData.IsFieldNull("ReplacementDate"))
					{
						// If it is a replacement, then show the date
						fldReplacement.Text = Strings.Format(rsData.Get_Fields_DateTime("ReplacementDate"), "MM/dd/yyyy");
					}
					else
					{
						// else show NO
						fldReplacement.Text = "No";
					}
					// Water
					fldTaxableW.Text = rsData.Get_Fields_Int32("WaterTaxPercent") + "%";
					fldBillableW.Text = rsData.Get_Fields_Int32("WaterPercent") + "%";
					lblBillTableWType1.Text = ReturnType_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("WaterType1")));
					lblBillTableWType2.Text = ReturnType_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("WaterType2")));
					lblBillTableWType3.Text = ReturnType_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("WaterType3")));
					lblBillTableWType4.Text = ReturnType_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("WaterType4")));
					lblBillTableWType5.Text = ReturnType_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("WaterType5")));
					fldBTWRT1.Text = FCConvert.ToString(rsData.Get_Fields_Int32("WaterKey1"));
					fldBTWRT2.Text = FCConvert.ToString(rsData.Get_Fields_Int32("WaterKey2"));
					fldBTWRT3.Text = FCConvert.ToString(rsData.Get_Fields_Int32("WaterKey3"));
					fldBTWRT4.Text = FCConvert.ToString(rsData.Get_Fields_Int32("WaterKey4"));
					fldBTWRT5.Text = FCConvert.ToString(rsData.Get_Fields_Int32("WaterKey5"));
					fldBTWAmt1.Text = Strings.Format(rsData.Get_Fields_Double("WaterAmount1"), "#,##0.00");
					fldBTWAmt2.Text = Strings.Format(rsData.Get_Fields_Double("WaterAmount2"), "#,##0.00");
					fldBTWAmt3.Text = Strings.Format(rsData.Get_Fields_Double("WaterAmount3"), "#,##0.00");
					fldBTWAmt4.Text = Strings.Format(rsData.Get_Fields_Double("WaterAmount4"), "#,##0.00");
					fldBTWAmt5.Text = Strings.Format(rsData.Get_Fields_Double("WaterAmount5"), "#,##0.00");
					fldBTWRTAdj.Text = FCConvert.ToString(rsData.Get_Fields_Int32("WaterAdjustKey"));
					fldBTWAmtAdj.Text = Strings.Format(rsData.Get_Fields_Double("WaterAdjustAmount"), "#,##0.00");
					// Sewer
					fldTaxableS.Text = rsData.Get_Fields_Int32("SewerTaxPercent") + "%";
					fldBillableS.Text = rsData.Get_Fields_Int32("SewerPercent") + "%";
					lblBillTableSType1.Text = ReturnType_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("SewerType1")));
					lblBillTableSType2.Text = ReturnType_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("SewerType2")));
					lblBillTableSType3.Text = ReturnType_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("SewerType3")));
					lblBillTableSType4.Text = ReturnType_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("SewerType4")));
					lblBillTableSType5.Text = ReturnType_2(FCConvert.ToInt32(rsData.Get_Fields_Int32("SewerType5")));
					fldBTSRT1.Text = FCConvert.ToString(rsData.Get_Fields_Int32("SewerKey1"));
					fldBTSRT2.Text = FCConvert.ToString(rsData.Get_Fields_Int32("SewerKey2"));
					fldBTSRT3.Text = FCConvert.ToString(rsData.Get_Fields_Int32("SewerKey3"));
					fldBTSRT4.Text = FCConvert.ToString(rsData.Get_Fields_Int32("SewerKey4"));
					fldBTSRT5.Text = FCConvert.ToString(rsData.Get_Fields_Int32("SewerKey5"));
					fldBTSAmt1.Text = Strings.Format(rsData.Get_Fields_Double("SewerAmount1"), "#,##0.00");
					fldBTSAmt2.Text = Strings.Format(rsData.Get_Fields_Double("SewerAmount2"), "#,##0.00");
					fldBTSAmt3.Text = Strings.Format(rsData.Get_Fields_Double("SewerAmount3"), "#,##0.00");
					fldBTSAmt4.Text = Strings.Format(rsData.Get_Fields_Double("SewerAmount4"), "#,##0.00");
					fldBTSAmt5.Text = Strings.Format(rsData.Get_Fields_Double("SewerAmount5"), "#,##0.00");
					fldBTSRTAdj.Text = FCConvert.ToString(rsData.Get_Fields_Int32("SewerAdjustKey"));
					fldBTSAmtAdj.Text = Strings.Format(rsData.Get_Fields_Double("SewerAdjustAmount"), "#,##0.00");
					fldAdjustDesc.Text = FCConvert.ToString(rsData.Get_Fields_String("AdjustDescription"));
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetAccountValues()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill in the account information
				rsMaster.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
				if (!rsMaster.EndOfFile())
				{
					//FC:FINAL:MSH - can't implicitly convert from int to string
					lblAccount.Text = FCConvert.ToString(rsMaster.Get_Fields("AccountNumber"));
					lblBillTo.Text = FCConvert.ToString(rsMaster.Get_Fields_String("Name"));
                    lblOwner.Text = rsMaster.Get_Fields("DeedName1");
                    if (Conversion.Val(rsMaster.Get_Fields("billingpartyid")) == Conversion.Val(rsMaster.Get_Fields("Ownerpartyid"))) {
                        lblBillTo.Text = rsMaster.Get_Fields("DeedName1");
                    }
                    else
                    {
                        cParty party;
                        party = partyController.GetParty(rsMaster.Get_Fields_Int32("billingpartyid"));
                        if (party != null)
                        {
                            lblBillTo.Text = party.FullNameLastFirst;
                        }
                        else
                        {
                            lblBillTo.Text = "";
                        }
                    }
                    if (rsMaster.Get_Fields_Int32("BillingPartyID") == rsMaster.Get_Fields_Int32("OwnerPartyID"))
                    {
                        lblBillTo.Text = FCConvert.ToString(rsMaster.Get_Fields_String("DeedName1"));
                    }
                    else
                    {
                        cPartyController pCont = new cPartyController();
                        cParty pInfo = new cParty();
                        pInfo = pCont.GetParty(rsMaster.Get_Fields_Int32("BillingPartyID"));
                        lblBillTo.Text = "";
                        if (pInfo != null)
                        {
                            lblBillTo.Text = pInfo.FullNameLastFirst;
                        }
                    }

                    lblLocation.Text = Strings.Trim(rsMaster.Get_Fields_Int32("StreetNumber") + " " + rsMaster.Get_Fields_String("StreetName"));
					lblMapLot.Text = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("MapLot")));
					lblRENumber.Text = FCConvert.ToString(rsMaster.Get_Fields_Int32("REAccount"));
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Setting Account Values", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private double GetAvgConsumption_2(int lngMeterKey)
		{
			return GetAvgConsumption(ref lngMeterKey);
		}

		private double GetAvgConsumption(ref int lngMeterKey)
		{
			double GetAvgConsumption = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsAvg = new clsDRWrapper();
				// this function will return the average consumption of the bills associated with this meter
				rsAvg.OpenRecordset("SELECT AVG(Consumption) as AvgCons FROM Bill WHERE MeterKey = " + FCConvert.ToString(lngMeterKey), modExtraModules.strUTDatabase);
				if (rsAvg.EndOfFile() != true)
				{
					// TODO: Field [AvgCons] not found!! (maybe it is an alias?)
					GetAvgConsumption = FCConvert.ToDouble(rsAvg.Get_Fields("AvgCons"));
				}
				else
				{
					GetAvgConsumption = 0;
				}
				return GetAvgConsumption;
			}
			catch (Exception ex)
			{
				
				GetAvgConsumption = 0;
			}
			return GetAvgConsumption;
		}

		private string ReturnType_2(int lngType)
		{
			return ReturnType(ref lngType);
		}

		private string ReturnType(ref int lngType)
		{
			string ReturnType = "";
			switch (lngType)
			{
				case 1:
					{
						ReturnType = "Cons";
						break;
					}
				case 2:
					{
						ReturnType = "Flat";
						break;
					}
				case 3:
					{
						ReturnType = "Units";
						break;
					}
				default:
					{
						ReturnType = "";
						break;
					}
			}
			//end switch
			return ReturnType;
		}

		private void arUTMeterDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arUTMeterDetail.Caption	= "Meter Detail";
			//arUTMeterDetail.Icon	= "arUTMeterDetail.dsx":0000";
			//arUTMeterDetail.Left	= 0;
			//arUTMeterDetail.Top	= 0;
			//arUTMeterDetail.Width	= 11880;
			//arUTMeterDetail.Height	= 8595;
			//arUTMeterDetail.StartUpPosition	= 3;
			//arUTMeterDetail.SectionData	= "arUTMeterDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
