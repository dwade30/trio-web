﻿//Fecher vbPorter - Version 1.0.0.27
namespace Global
{
	public class cDeptDivTitle
	{
		//=========================================================
		private int lngRecordID;
		private string strDepartment = string.Empty;
		private string strDivision = string.Empty;
		private string strShortDescription = string.Empty;
		private string strLongDescription = string.Empty;
		private string strFund = string.Empty;
		private string strCloseoutAccount = string.Empty;
		private int lngBreakDownCode;
		private bool boolUpdated;
		private bool boolDeleted;

		public string Department
		{
			set
			{
				strDepartment = value;
				IsUpdated = true;
			}
			get
			{
				string Department = "";
				Department = strDepartment;
				return Department;
			}
		}

		public string Division
		{
			set
			{
				strDivision = value;
				IsUpdated = true;
			}
			get
			{
				string Division = "";
				Division = strDivision;
				return Division;
			}
		}

		public string ShortDescription
		{
			set
			{
				strShortDescription = value;
				IsUpdated = true;
			}
			get
			{
				string ShortDescription = "";
				ShortDescription = strShortDescription;
				return ShortDescription;
			}
		}

		public string LongDescription
		{
			set
			{
				strLongDescription = value;
				IsUpdated = true;
			}
			get
			{
				string LongDescription = "";
				LongDescription = strLongDescription;
				return LongDescription;
			}
		}

		public string Fund
		{
			set
			{
				strFund = value;
				IsUpdated = true;
			}
			get
			{
				string Fund = "";
				Fund = strFund;
				return Fund;
			}
		}

		public string CloseoutAccount
		{
			set
			{
				strCloseoutAccount = value;
				IsUpdated = true;
			}
			get
			{
				string CloseoutAccount = "";
				CloseoutAccount = strCloseoutAccount;
				return CloseoutAccount;
			}
		}

		public int BreakdownCode
		{
			set
			{
				lngBreakDownCode = value;
				IsUpdated = true;
			}
			get
			{
				int BreakdownCode = 0;
				BreakdownCode = lngBreakDownCode;
				return BreakdownCode;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}
	}
}
