﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using System;
using System.Text;
using TWSharedLibrary;

namespace Global
{
    public class cSystemSettings
    {
        //=========================================================
        private string strLastError = "";
        private int lngLastError;

        public string LastErrorMessage
        {
            get
            {
                string LastErrorMessage = "";
                LastErrorMessage = strLastError;
                return LastErrorMessage;
            }
        }

        public int LastErrorNumber
        {
            get
            {
                int LastErrorNumber = 0;
                LastErrorNumber = lngLastError;
                return LastErrorNumber;
            }
        }

        public void ClearErrors()
        {
            strLastError = "";
            lngLastError = 0;
        }

        public cVersionInfo GetVersion()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            cVersionInfo GetVersion = null;

            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                cVersionInfo tVer = new cVersionInfo();
                rsLoad.OpenRecordset("select * from dbversion", "SystemSettings");

                if (!rsLoad.EndOfFile())
                {
                    tVer.Build = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Build"));
                    tVer.Major = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Major"));
                    tVer.Minor = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Minor"));
                    tVer.Revision = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Revision"));
                }

                GetVersion = tVer;

                return GetVersion;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = Information.Err(ex).Description;
                lngLastError = Information.Err(ex).Number;
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetVersion;
        }

        public void SetVersion(cVersionInfo nVersion)
        {
            clsDRWrapper rsSave = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                if (!(nVersion == null))
                {
                    rsSave.OpenRecordset("select * from dbversion", "SystemSettings");

                    if (rsSave.EndOfFile())
                    {
                        rsSave.AddNew();
                    }
                    else
                    {
                        rsSave.Edit();
                    }

                    rsSave.Set_Fields("Major", nVersion.Major);
                    rsSave.Set_Fields("Minor", nVersion.Minor);
                    rsSave.Set_Fields("Revision", nVersion.Revision);
                    rsSave.Set_Fields("Build", nVersion.Build);
                    rsSave.Update();
                }

                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                strLastError = Information.Err(ex).Description;
                lngLastError = Information.Err(ex).Number;
            }
            finally
            {
                rsSave.DisposeOf();
            }
        }

        public bool CheckVersion()
        {
            ClearErrors();
            try
            {
                // On Error GoTo ErrorHandler
                cVersionInfo nVer = new cVersionInfo();
                cVersionInfo tVer = new cVersionInfo();
                cVersionInfo cVer;

                cVer = GetVersion();
                if (cVer == null)
                {
                    cVer = new cVersionInfo();
                }
                nVer.Major = 1;

                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddCloseoutByDeptOption())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 2;
                tVer.Build = 0;
                if (tVer.IsNewer(nVer) && tVer.IsNewer(cVer))
                {
                    if (AddEmailOptions())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                // kk 07302013   DB Ver 1.0.3.0  Add EBillingConfiguration Table
                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 3;
                tVer.Build = 0;
                if (tVer.IsNewer(nVer) && tVer.IsNewer(cVer))
                {
                    if (AddEBillingConfiguration())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 4;
                tVer.Build = 0;
                if (tVer.IsNewer(nVer) && tVer.IsNewer(cVer))
                {
                    if (AddEmailPort())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 5;
                tVer.Build = 0;
                if (tVer.IsNewer(nVer) && tVer.IsNewer(cVer))
                {
                    if (AddArchiveMenuBackColor())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                tVer.Major = 1;
                tVer.Minor = 0;
                tVer.Revision = 6;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddNewFieldsToEmailConfig())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 0;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    cSettingsController scont = new cSettingsController();

                    if (scont.AddSettingsTable("SystemSettings"))
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 1;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddNewSecurityFields())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 2;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddNewGlobalVariableFields())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 3;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddMovedToClientSettingsFieldToPermissionsTable())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 4;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddMovedToClientSettingsFieldToOperators())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                tVer.Major = 6;
                tVer.Minor = 0;
                tVer.Revision = 5;
                tVer.Build = 0;
                if (tVer.IsNewer(cVer))
                {
                    if (AddCRMyRecImportOption())
                    {
                        nVer.Copy(tVer);
                        if (cVer.IsOlder(nVer))
                        {
                            SetVersion(nVer);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetErrorVariables(ex);
            }
            return false;
        }

        private bool AddMovedToClientSettingsFieldToOperators()
        {
            clsDRWrapper rsUpdate = new clsDRWrapper();
            string strsql = "";

            try
            {
                if (!FieldExists("Operators", "MovedToClientSettings", "SystemSettings"))
                {
                    strsql = "Alter Table Operators Add MovedToClientSettings bit null";
                    rsUpdate.Execute(strsql, "SystemSettings");
                    rsUpdate.Execute("Update Operators set MovedToClientSettings = 0", "SystemSettings");
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                return false;
            }
            finally
            {
                rsUpdate.DisposeOf();
            }
        }

        private bool AddMovedToClientSettingsFieldToPermissionsTable()
        {
            clsDRWrapper rsUpdate = new clsDRWrapper();
            string strsql = "";

            try
            {
                if (!FieldExists("PermissionsTable", "MovedToClientSettings", "SystemSettings"))
                {
                    strsql = "Alter Table PermissionsTable Add MovedToClientSettings bit null";
                    rsUpdate.Execute(strsql, "SystemSettings");
                    rsUpdate.Execute("Update PermissionsTable set MovedToClientSettings = 0", "SystemSettings");
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                return false;
            }
            finally
            {
                rsUpdate.DisposeOf();
            }

        }

        private bool AddNewGlobalVariableFields()
        {
            clsDRWrapper rsUpdate = new clsDRWrapper();
            string strsql = "";

            try
            {
                if (!FieldExists("GlobalVariables", "UsePassive", "SystemSettings"))
                {
                    strsql = "Alter Table GlobalVariables Add [UsePassive] bit null";
                    rsUpdate.Execute(strsql, "SystemSettings");
                    rsUpdate.Execute("Update GlobalVariables set UsePassive = 0", "SystemSettings");
                }

                if (!FieldExists("GlobalVariables", "DLLVersion", "SystemSettings"))
                {
                    strsql = "Alter Table GlobalVariables Add [DLLVersion] nvarchar(50) null";
                    rsUpdate.Execute(strsql, "SystemSettings");
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                return false;
            }
            finally
            {
                rsUpdate.DisposeOf();
            }
        }

        private bool AddNewSecurityFields()
        {
            clsDRWrapper rsUpdate = new clsDRWrapper();
            clsDRWrapper rsLoad = new clsDRWrapper();
            clsDRWrapper rsSave = new clsDRWrapper();
            string strsql = "";
            var ret = false;

            try
            {
                if (!FieldExists("Security", "LockedOut", "SystemSettings"))
                {
                    strsql = "Alter Table Security Add [LockedOut] bit null";
                    rsUpdate.Execute(strsql, "SystemSettings");
                    rsUpdate.Execute("Update Security set LockedOut = 0", "SystemSettings");
                }

                if (!FieldExists("Security", "Inactive", "SystemSettings"))
                {
                    strsql = "Alter Table Security Add [Inactive] bit null";
                    rsUpdate.Execute(strsql, "SystemSettings");
                    rsUpdate.Execute("Update Security set Inactive = 0", "SystemSettings");
                }

                if (!FieldExists("Security", "FailedAttempts", "SystemSettings"))
                {
                    strsql = "Alter Table Security Add [FailedAttempts] int null";
                    rsUpdate.Execute(strsql, "SystemSettings");
                    rsUpdate.Execute("Update Security set FailedAttempts = 0", "SystemSettings");
                }

                if (!FieldExists("Security", "LockoutDateTime", "SystemSettings"))
                {
                    strsql = "Alter Table Security Add [LockoutDateTime] datetime null";
                    rsUpdate.Execute(strsql, "SystemSettings");
                }

                if (!FieldExists("Security", "Salt", "SystemSettings"))
                {
                    strsql = "Alter Table Security Add [Salt] nvarchar(255) null";
                    rsUpdate.Execute(strsql, "SystemSettings");
                }


                if (!TableExists("PasswordHistory", "SystemSettings"))
                {
                    var sb = new StringBuilder();
                    sb.Append(GetTableCreationHeaderSQL("PasswordHistory"));
                    sb.Append("[SecurityId] [int] Not NULL,");
                    sb.Append("[Password] [nvarchar] (max) NULL,");
                    sb.Append("[Salt] [nvarchar] (255) NULL,");
                    sb.Append("[DateAdded] [datetime] NULL,");
                    sb.Append(GetTablePKSql("PasswordHistory"));
                    rsSave.Execute(sb.ToString(), "SystemSettings");
                }

                ret = true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetErrorVariables(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
                rsUpdate.DisposeOf();
                rsSave.DisposeOf();
            }

            return ret;
        }

        private void SetErrorVariables(Exception ex)
        {
            strLastError = Information.Err(ex)
                                      .Description;
            lngLastError = Information.Err(ex)
                                      .Number;
        }

        // vbPorter upgrade warning: 'Return' As Variant --> As bool
        private bool AddNewFieldsToEmailConfig()
        {
            var ret = false;

            clsDRWrapper rsUpdate = new clsDRWrapper();
            clsDRWrapper rsLoad = new clsDRWrapper();
            string strsql = "";

            try
            {
                // On Error GoTo ErrorHandler

                if (!FieldExists("EmailConfiguration", "UseTLS", "SystemSettings"))
                {
                    strsql = "Alter Table EmailConfiguration Add [UseTLS] bit null";
                    rsUpdate.Execute(strsql, "SystemSettings");
                    rsUpdate.Execute("Update EmailConfiguration set UseTLS = 0", "SystemSettings");
                }

                if (!FieldExists("EmailConfiguration", "DomainName", "SystemSettings"))
                {
                    strsql = "Alter Table EmailConfiguration add [DomainName] [varchar] (255) null";
                    rsUpdate.Execute(strsql, "SystemSettings");
                }

                rsUpdate.OpenRecordset("select * from EmailConfiguration where userid = 0", "SystemSettings");

                if (rsUpdate.EndOfFile())
                {
                    cGlobalVariable gv;
                    cEMailConfigController emcont = new cEMailConfigController();
                    cEmailConfig econ = new cEmailConfig();
                    gv = GetGlobalVariables();
                    econ.AuthCode = gv.AuthorizationType;
                    econ.DefaultFromAddress = gv.UserEmail;
                    econ.DefaultFromName = gv.FromName;
                    econ.LoginDomain = "";
                    econ.Password = gv.SMTPPassword;
                    econ.SMTPHost = gv.SMTPServer;
                    econ.SMTPPort = gv.SMTPPort;
                    econ.UserName = gv.SMTPUser;
                    econ.UseSSL = gv.UseSSL;
                    econ.UseTransportLayerSecurity = false;
                    econ.UserID = 0;
                    emcont.SaveEmailConfiguration(ref econ);

                    if (emcont.HadError)
                    {
                        return false;
                    }
                }

                ret = true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetErrorVariables(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
                rsUpdate.DisposeOf();
            }
            return ret;
        }

        private bool AddArchiveMenuBackColor()
        {
            bool ret = false;
            clsDRWrapper rsUpdate = new clsDRWrapper();
            try
            {
                // On Error GoTo ErrorHandler
                rsUpdate.OpenRecordset("SELECT * From INFORMATION_SCHEMA.Columns WHERE TABLE_NAME = 'GlobalVariables' AND COLUMN_NAME = 'ArchiveMenuBackColor'", "SystemSettings");
                if (rsUpdate.RecordCount() == 0)
                {
                    rsUpdate.Execute("ALTER TABLE GlobalVariables ADD ArchiveMenuBackColor int NULL", "SystemSettings");
                }
                ret = true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetErrorVariables(ex);
            }
            finally
            {
                rsUpdate.DisposeOf();
            }
            return ret;
        }

        private bool AddPartyAddressPhoneView()
        {
            bool ret = false;
            clsDRWrapper rsSave = new clsDRWrapper();
            string strSQL;
            string strprefix;
            strprefix = rsSave.CurrentPrefix;

            try
            {
                // On Error GoTo ErrorHandler
                var sb = new StringBuilder();
                sb.AppendLine("CREATE VIEW [PartyAddressAndPhoneView]");
                sb.AppendLine("AS ");
                sb.AppendLine("SELECT     TOP (100) PERCENT Parties.ID, Parties.PartyGuid, Parties.PartyType, Parties.FirstName, Parties.MiddleName, Parties.LastName, ");
                sb.AppendLine("                      Parties.Designation, Parties.Email, Parties.WebAddress, Parties.DateCreated, Parties.CreatedBy, Addresses.ID AS Expr1, ");
                sb.AppendLine("                      Addresses.PartyID, Addresses.Address1, Addresses.Address2, Addresses.Address3, Addresses.City, Addresses.State, Addresses.Zip, ");
                sb.AppendLine("                      Addresses.Country, Addresses.OverrideName, Addresses.AddressType, Addresses.Seasonal, Addresses.ProgModule, Addresses.Comment, ");
                sb.AppendLine("                      Addresses.StartMonth, Addresses.StartDay, Addresses.EndMonth, Addresses.EndDay, Addresses.ModAccountID, ");
                sb.AppendLine("                      RTRIM(RTRIM(Parties.FirstName + ' ' + ISNULL(Parties.MiddleName, '')) + ' ' + RTRIM(ISNULL(Parties.LastName, '') ");
                sb.AppendLine("                      + ' ' + ISNULL(Parties.Designation, ''))) AS FullName, RTRIM(RTRIM(LTRIM(ISNULL(NULLIF (Parties.LastName + ',', ','), '') ");
                sb.AppendLine("                      + ' ' + ISNULL(Parties.FirstName, '')) + ' ' + ISNULL(Parties.MiddleName, '')) + ' ' + ISNULL(Parties.Designation, '')) AS FullNameLF");
                sb.AppendLine("                      , CentralPhoneNumbers.PhoneNumber,CentralPhoneNumbers.Extension");
                sb.AppendLine("FROM         Parties left outer join");
                sb.AppendLine("CentralPhoneNumbers on parties.id = CentralPhoneNumbers.PartyID");
                sb.AppendLine(" LEFT OUTER JOIN");
                sb.AppendLine("                      Addresses ON Parties.ID = Addresses.PartyID");
                sb.AppendLine("WHERE     ((Addresses.AddressType = 'Primary') OR");
                sb.AppendLine("                      (ISNULL(Addresses.PartyID, - 1) = - 1))");
                sb.AppendLine("           and");
                sb.AppendLine("           (CentralPhoneNumbers.PhoneOrder = 1 or isnull(CentralPhoneNumbers.ID,-1) = -1)");
                sb.AppendLine("           ");
                sb.AppendLine("ORDER BY FullName");
                rsSave.Execute(sb.ToString(), "CentralParties");
                ret = true;
            }
            catch (Exception ex)
            {
                strLastError = Information.Err(ex).Description;
                lngLastError = Information.Err(ex).Number;
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsSave.DisposeOf();
            }
            return ret;
        }

        private bool AddCloseoutByDeptOption()
        {
            bool ret = false;
            clsDRWrapper rsTemp = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                // kk 120412 trout-348  Add Close Out by Department option
                rsTemp.OpenRecordset("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'DeptDiv' AND TABLE_NAME = 'Operators'", "SystemSettings");
                if (rsTemp.EndOfFile())
                {
                    rsTemp.Execute("ALTER TABLE.[dbo].[Operators] ADD [DeptDiv] nvarchar(50) NULL", "SystemSettings");
                }
                ret = true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetErrorVariables(ex);
            }
            finally
            {
                rsTemp.DisposeOf();
            }
            return ret;
        }

        private bool AddEmailPort()
        {
            bool AddEmailPort = false;
                clsDRWrapper rsSave = new clsDRWrapper();

                try
                {
                    // On Error GoTo ErrorHandler
                    string strSQL;
                    strSQL = "Select column_name from information_schema.columns where column_name = 'ServerPort' and table_name = 'GlobalVariables'";
                    rsSave.OpenRecordset(strSQL, "SystemSettings");

                    if (rsSave.EndOfFile())
                    {
                        strSQL = "alter table.[dbo].[GlobalVariables] add [ServerPort] int NULL";
                        rsSave.Execute(strSQL, "SystemSettings", false);
                    }

                    strSQL = "Select column_name from information_schema.columns where column_name = 'ServerPort' and table_name = 'EmailConfiguration'";
                    rsSave.OpenRecordset(strSQL, "SystemSettings");

                    if (rsSave.EndOfFile())
                    {
                        strSQL = "alter table.[dbo].[EmailConfiguration] add [ServerPort] int NULL";
                        rsSave.Execute(strSQL, "SystemSettings", false);
                    }

                    AddEmailPort = true;

                    return AddEmailPort;
                }
                catch (Exception ex)
                {
                    StaticSettings.GlobalTelemetryService.TrackException(ex);
                    SetErrorVariables(ex);
                }
                finally
                {
                    rsSave.DisposeOf();
                }
            return AddEmailPort;
        }

        private bool AddEmailOptions()
        {
            bool AddEmailOptions = false;
                clsDRWrapper rsSave = new clsDRWrapper();

                try
                {
                    // On Error GoTo ErrorHandler
                    string strSQL;
                    strSQL = "Alter Table.[dbo].[EmailConfiguration] Add [FromName] nvarchar(255) NULL";
                    rsSave.Execute(strSQL, "SystemSettings", false);
                    strSQL = "Alter Table.[dbo].[EmailConfiguration] add [UseSSL] bit NULL";
                    rsSave.Execute(strSQL, "SystemSettings", false);
                    strSQL = "Alter Table.[dbo].[GlobalVariables] Add [FromName] nvarchar(255) NULL";
                    rsSave.Execute(strSQL, "SystemSettings", false);
                    strSQL = "Alter Table.[dbo].[GlobalVariables] add [UseSSL] bit NULL";
                    rsSave.Execute(strSQL, "SystemSettings", false);
                    AddEmailOptions = true;
                }
                catch (Exception ex)
                {
                    StaticSettings.GlobalTelemetryService.TrackException(ex);
                    SetErrorVariables(ex);
                }
                finally
                {
                    rsSave.DisposeOf();
                }
            return AddEmailOptions;
        }

        private bool AddEBillingConfiguration()
        {
            bool AddEBillingConfiguration = false;
            clsDRWrapper rsAdd = new clsDRWrapper();
            string strSQL;
            strSQL = "CREATE TABLE [dbo].[EBillingConfiguration](" + "[ID] [int] IDENTITY(1,1) NOT NULL," + "[EBillVendorID] [nvarchar](10) NULL," + "[EBillVendorName] [nvarchar](255) NULL," + "[EBillBillerID] [nvarchar](255) NULL," + "[EBillBillerPswd] [nvarchar](255) NULL," + "CONSTRAINT [PK_EBillingConfiguration] PRIMARY KEY CLUSTERED " + "([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]" + ") ON [PRIMARY]";
            rsAdd.Execute(strSQL, "SystemSettings", false);
            AddEBillingConfiguration = true;
            rsAdd.DisposeOf();
            return AddEBillingConfiguration;
        }

        public bool SaveGlobalVariables(ref cGlobalVariable tGlobalVariable)
        {
            bool SaveGlobalVariables = false;
            try
            {
                // On Error GoTo ErrorHandler
                SaveGlobalVariables = false;
                ClearErrors();
                if (!(tGlobalVariable == null))
                {
                    clsDRWrapper rsSave = new clsDRWrapper();
                    rsSave.OpenRecordset("select * from globalvariables", "SystemSettings");
                    if (!rsSave.EndOfFile())
                    {
                        rsSave.Edit();
                    }
                    else
                    {
                        rsSave.AddNew();
                    }
                    rsSave.Set_Fields("ArchiveYear", tGlobalVariable.ArchiveYear);
                    rsSave.Set_Fields("AuthorizationType", tGlobalVariable.AuthorizationType);
                    rsSave.Set_Fields("BulkMail1", tGlobalVariable.BulkMail1);
                    rsSave.Set_Fields("BulkMail2", tGlobalVariable.BulkMail2);
                    rsSave.Set_Fields("BulkMail3", tGlobalVariable.BulkMail3);
                    rsSave.Set_Fields("BulkMail4", tGlobalVariable.BulkMail4);
                    rsSave.Set_Fields("BulkMail5", tGlobalVariable.BulkMail5);
                    rsSave.Set_Fields("CityTown", tGlobalVariable.CityTown);
                    rsSave.Set_Fields("DefaultPermission", tGlobalVariable.DefaultPermission);
                    rsSave.Set_Fields("ftpaddress", string.Empty); // this is now stored as a constant in cGlobalConstants
                    rsSave.Set_Fields("FTPPassword", tGlobalVariable.FTPPassword);
                    rsSave.Set_Fields("FTPUser", tGlobalVariable.FTPUser);
                    rsSave.Set_Fields("MaxDate", tGlobalVariable.MaxDate);
                    rsSave.Set_Fields("MenuBackColor", tGlobalVariable.MenuBackColor);
                    rsSave.Set_Fields("MuniName", tGlobalVariable.MuniName);
                    rsSave.Set_Fields("ReleaseNumber", tGlobalVariable.ReleaseNumber);
                    rsSave.Set_Fields("SetUpdateNotification", tGlobalVariable.SetUpdateNotification);
                    rsSave.Set_Fields("SMTPPassword", tGlobalVariable.SMTPPassword);
                    rsSave.Set_Fields("SMTPServer", tGlobalVariable.SMTPServer);
                    rsSave.Set_Fields("SMTPUser", tGlobalVariable.SMTPUser);
                    rsSave.Set_Fields("UpdateDate", tGlobalVariable.UpdateDate);
                    rsSave.Set_Fields("UpdateNotification", tGlobalVariable.UpdateNotification);
                    rsSave.Set_Fields("UseMultipleTown", tGlobalVariable.UseMultipleTown);
                    rsSave.Set_Fields("UseRegistryOnly", tGlobalVariable.UseRegistryOnly);
                    rsSave.Set_Fields("UserEmail", tGlobalVariable.UserEmail);
                    rsSave.Set_Fields("UseSecurity", true);
                    rsSave.Set_Fields("Violations", tGlobalVariable.Violations);
                    rsSave.Set_Fields("ServerPort", tGlobalVariable.SMTPPort);
                    rsSave.Set_Fields("FromName", tGlobalVariable.FromName);
                    rsSave.Update();
                    tGlobalVariable.ID = rsSave.Get_Fields_Int32("id");
                    SaveGlobalVariables = true;
                    rsSave.DisposeOf();
                }
                else
                {
                    strLastError = "Parameter object globalvariable is nothing";
                    lngLastError = -1;
                }
                return SaveGlobalVariables;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetErrorVariables(ex);
            }
            return SaveGlobalVariables;
        }

        public cGlobalVariable GetGlobalVariables()
        {
            cGlobalVariable GetGlobalVariables = null;
            clsDRWrapper rsLoad = new clsDRWrapper();

            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                cGlobalVariable tReturn = new cGlobalVariable();
                GetGlobalVariables = tReturn;
                rsLoad.OpenRecordset("Select * from globalvariables", "SystemSettings");

                if (!rsLoad.EndOfFile())
                {
                    tReturn.ArchiveYear = FCConvert.ToString(rsLoad.Get_Fields_String("archiveyear"));
                    tReturn.AuthorizationType = FCConvert.ToInt16(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("AuthorizationType"))));
                    tReturn.BulkMail1 = FCConvert.ToString(rsLoad.Get_Fields_String("BulkMail1"));
                    tReturn.BulkMail2 = FCConvert.ToString(rsLoad.Get_Fields_String("BulkMail2"));
                    tReturn.BulkMail3 = FCConvert.ToString(rsLoad.Get_Fields_String("BulkMail3"));
                    tReturn.BulkMail4 = FCConvert.ToString(rsLoad.Get_Fields_String("BulkMail4"));
                    tReturn.BulkMail5 = FCConvert.ToString(rsLoad.Get_Fields_String("BulkMail5"));
                    tReturn.CityTown = FCConvert.ToString(rsLoad.Get_Fields_String("citytown"));
                    tReturn.DefaultPermission = false;

                    //tReturn.FTPAddress = FCConvert.ToString(rsLoad.Get_Fields_String("FTPSiteAddress"));  // this is now stored in cGlobalConstants
                    tReturn.FTPPassword = FCConvert.ToString(rsLoad.Get_Fields_String("FTPSitePassword"));
                    tReturn.FTPUser = FCConvert.ToString(rsLoad.Get_Fields_String("FTPSiteUser"));
                    tReturn.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
                    tReturn.MaxDate = (DateTime)rsLoad.Get_Fields_DateTime("MaxDate");
                    tReturn.MenuBackColor = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("MenuBackColor"));
                    tReturn.MuniName = FCConvert.ToString(rsLoad.Get_Fields_String("MuniName"));
                    tReturn.ReleaseNumber = FCConvert.ToString(rsLoad.Get_Fields_String("releasenumber"));
                    tReturn.SetUpdateNotification = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("setupdatenotification"));
                    tReturn.SMTPPassword = FCConvert.ToString(rsLoad.Get_Fields_String("SMTPPassword"));
                    tReturn.SMTPPort = FCConvert.ToInt32(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ServerPort"))));
                    tReturn.SMTPServer = FCConvert.ToString(rsLoad.Get_Fields_String("SMTPServer"));
                    tReturn.SMTPUser = FCConvert.ToString(rsLoad.Get_Fields_String("SMTPUser"));
                    tReturn.UpdateDate = (DateTime)rsLoad.Get_Fields_DateTime("UpdateDate");
                    tReturn.UpdateNotification = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("UpdateNotification"));
                    tReturn.UseMultipleTown = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("UseMultipleTown"));
                    tReturn.UseRegistryOnly = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("UseRegistryOnly"));
                    tReturn.UserEmail = FCConvert.ToString(rsLoad.Get_Fields_String("UserEmail"));
                    tReturn.UseSecurity = true;
                    tReturn.UseSSL = FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("UseSSL"));
                    tReturn.Violations = FCConvert.ToInt16(rsLoad.Get_Fields_Int16("Violations"));
                    tReturn.FromName = FCConvert.ToString(rsLoad.Get_Fields_String("FromName"));
                }

                GetGlobalVariables = tReturn;

                return GetGlobalVariables;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetErrorVariables(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetGlobalVariables;
        }

        public FCCollection GetArchiveItems()
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            FCCollection GetArchiveItems = null;

            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                FCCollection tReturn = new FCCollection();
                GetArchiveItems = tReturn;
                cArchiveItem tArchive;
                rsLoad.OpenRecordset("select * from archives", "SystemSettings");

                while (!rsLoad.EndOfFile())
                {
                    tArchive = new cArchiveItem();
                    tArchive.ArchiveID = FCConvert.ToInt16(Math.Round(Conversion.Val(rsLoad.Get_Fields_Int32("ArchiveID"))));
                    tArchive.ArchiveType = FCConvert.ToString(rsLoad.Get_Fields_String("ArchiveType"));
                    tArchive.Description = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));

                    if (!fecherFoundation.FCUtils.IsEmptyDateTime(rsLoad.Get_Fields_DateTime("CreationDate")))
                    {
                        if (Information.IsDate(rsLoad.Get_Fields("CreationDate")))
                        {
                            tArchive.CreationDate = (DateTime)rsLoad.Get_Fields_DateTime("CreationDate");
                        }
                    }

                    tReturn.Add(tArchive);
                    rsLoad.MoveNext();
                }

                return GetArchiveItems;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetErrorVariables(ex);
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return GetArchiveItems;
        }

        public void RemoveArchiveItem(ref cArchiveItem tArchive)
        {
            try
            {
                // On Error GoTo ErrorHandler
                ClearErrors();
                if (!(tArchive == null))
                {
                    clsDRWrapper rsSave = new clsDRWrapper();
                    rsSave.Execute("delete from archives where archiveid = " + tArchive.ArchiveID + " and archivetype = '" + tArchive.ArchiveType + "'", "SystemSettings");
                    rsSave.DisposeOf();
                }
                return;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetErrorVariables(ex);
            }
        }

        private bool FieldExists(string strTable, string strField, string strDB)
        {
            bool FieldExists = false;
            try
            {
                // On Error GoTo ErrorHandler
                string strSQL;
                clsDRWrapper rsLoad = new clsDRWrapper();
                strSQL = "Select column_name from information_schema.columns where column_name = '" + strField + "' and table_name = '" + strTable + "'";
                rsLoad.OpenRecordset(strSQL, strDB);
                if (!rsLoad.EndOfFile())
                {
                    FieldExists = true;
                }
                rsLoad.DisposeOf();
                return FieldExists;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetErrorVariables(ex);
            }
            return FieldExists;
        }

        private bool TableExists(string strTable, string strDB)
        {
            bool TableExists = false;
            try
            {
                // On Error GoTo ErrorHandler
                string strSql;
                clsDRWrapper rsLoad = new clsDRWrapper();
                strSql = "select * from sys.tables where name = '" + strTable + "' and type = 'U'";
                rsLoad.OpenRecordset(strSql, strDB);
                if (!rsLoad.EndOfFile())
                {
                    TableExists = true;
                }
                rsLoad.DisposeOf();
                return TableExists;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                SetErrorVariables(ex);
            }
            return TableExists;
        }

        private string GetTableCreationHeaderSQL(string strTableName)
        {
            string GetTableCreationHeaderSQL = "";
            string strSql;
            strSql = "if not exists (select * from information_schema.tables where table_name = N'" + strTableName + "') " + "\r\n";
            strSql += "Create Table [dbo].[" + strTableName + "] (";
            strSql += "[ID] [int] IDENTITY (1,1) NOT NULL,";
            GetTableCreationHeaderSQL = strSql;
            return GetTableCreationHeaderSQL;
        }

        private string GetTablePKSql(string strTableName)
        {
            string GetTablePKSql = "";
            string strSql;
            strSql = " Constraint [PK_" + strTableName + "] primary key clustered ([ID] Asc) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
            GetTablePKSql = strSql;
            return GetTablePKSql;
        }

        private bool AddCRMyRecImportOption()
        {
            try
            {
                var rsTemp = new clsDRWrapper();
                rsTemp.OpenRecordset(
                    "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'CRMyRec' AND TABLE_NAME = 'Modules'",
                    "SystemSettings");
                if (rsTemp.EndOfFile())
                {
                    rsTemp.Execute ("ALTER TABLE.[dbo].[Modules] ADD [CRMyRec] [nvarchar](50) NULL", "SystemSettings");
                }
                rsTemp.OpenRecordset(
                    "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'MR' AND TABLE_NAME = 'Modules'",
                    "SystemSettings");
                if (rsTemp.EndOfFile ())
                {
                    rsTemp.Execute("ALTER TABLE.[dbo].[Modules] ADD [MR] bit NULL", "SystemSettings");
                }

                rsTemp.OpenRecordset(
                    "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'MRDate' AND TABLE_NAME = 'Modules'",
                    "SystemSettings");
                if (rsTemp.EndOfFile())
                {
                rsTemp.Execute ("ALTER TABLE.[dbo].[Modules] ADD [MRDate] datetime NULL", "SystemSettings");
                }

                rsTemp.OpenRecordset(
                    "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'MRLevel' AND TABLE_NAME = 'Modules'",
                    "SystemSettings");
                if (rsTemp.EndOfFile())
                {
                    rsTemp.Execute ("ALTER TABLE.[dbo].[Modules] ADD [MRLevel] smallint NULL", "SystemSettings");
                }

                return true;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
                return false;
            }
        }
    }
}
