﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;
using System;

namespace Global
{
	/// <summary>
	/// Summary description for srptUTPerDiemList.
	/// </summary>
	public partial class srptUTPerDiemList : FCSectionReport
	{
		// nObj = 1
		//   0	srptUTPerDiemList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int intCounter;
		bool blnFirstRecord;
		double dblTotalPerDiem;

		public srptUTPerDiemList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptUTPerDiemList InstancePtr
		{
			get
			{
				return (srptUTPerDiemList)Sys.GetInstance(typeof(srptUTPerDiemList));
			}
		}

		protected srptUTPerDiemList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				intCounter += 1;
				if (intCounter < modUTFunctions.Statics.intPerdiemCounter)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, System.EventArgs e)
		{
			intCounter = 0;
			blnFirstRecord = true;
			dblTotalPerDiem = 0;
		}

		private void Detail_Format(object sender, System.EventArgs e)
		{
			lblPerDiem1.Text = FCConvert.ToString(modUTFunctions.Statics.pdiInfo[intCounter].intBill);
			fldPerDiem1.Text = Strings.Format(modUTFunctions.Statics.pdiInfo[intCounter].dblPerDiem, "0.0000");
			dblTotalPerDiem += modUTFunctions.Statics.pdiInfo[intCounter].dblPerDiem;
		}

		private void GroupFooter1_Format(object sender, System.EventArgs e)
		{
			fldPerDiemTotal.Text = Strings.Format(dblTotalPerDiem, "0.0000");
		}

		private void srptUTPerDiemList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptUTPerDiemList.Left	= 0;
			//srptUTPerDiemList.Top	= 0;
			//srptUTPerDiemList.Width	= 11730;
			//srptUTPerDiemList.Height	= 5490;
			//srptUTPerDiemList.StartUpPosition	= 3;
			//srptUTPerDiemList.SectionData	= "srptUTPerDiemList.dsx":0000;
			//End Unmaped Properties
		}
	}
}
