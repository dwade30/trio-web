﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;

namespace Global
{
	public class cArchiveItem
	{
		//=========================================================
		private int intArchiveID;
		private string strArchiveType = "";
		private string strDescription = "";
		private DateTime dtCreationDate;
		// VBto upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		public short ArchiveID
		{
			get
			{
				short ArchiveID = 0;
				ArchiveID = FCConvert.ToInt16(intArchiveID);
				return ArchiveID;
			}
			set
			{
				intArchiveID = value;
			}
		}

		public string ArchiveType
		{
			get
			{
				string ArchiveType = "";
				ArchiveType = strArchiveType;
				return ArchiveType;
			}
			set
			{
				strArchiveType = value;
			}
		}

		public string Description
		{
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
			set
			{
				strDescription = value;
			}
		}

		public DateTime CreationDate
		{
			get
			{
				DateTime CreationDate = System.DateTime.Now;
				CreationDate = dtCreationDate;
				return CreationDate;
			}
			set
			{
				dtCreationDate = value;
			}
		}
	}
}
