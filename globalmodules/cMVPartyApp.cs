﻿using fecherFoundation;
using fecherFoundation.Extensions;
using System;
using System.Collections.Generic;
using TWSharedLibrary;

namespace Global
{
    public class cMVPartyApp : SharedApplication.ICentralPartyApp
    {
        //=========================================================
        const string strModuleCode = "MV";

        public bool ReferencesParty(int lngID)
        {
            clsDRWrapper rsLoad = new clsDRWrapper();
            bool ICentralPartyApp_ReferencesParty = false;
            ICentralPartyApp_ReferencesParty = false;
            bool retAnswer;
            retAnswer = false;

            try
            {
                if (lngID > 0)
                {
                    rsLoad.OpenRecordset($"select count(id)as theCount from fleetmaster where partyid1 = {FCConvert.ToString(lngID)} or partyid2 = {FCConvert.ToString(lngID)} or partyid3 = {FCConvert.ToString(lngID)}", "MotorVehicle");
                    // TODO: Field [theCount] not found!! (maybe it is an alias?)
                    if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
                    {
                        retAnswer = true;
                    }
                    else
                    {
                        rsLoad.OpenRecordset($"select count(id) as theCount from Master where partyid1 = {FCConvert.ToString(lngID)} or partyid2 = {FCConvert.ToString(lngID)} or partyid3 = {FCConvert.ToString(lngID)}", "MotorVehicle");
                        // TODO: Field [theCount] not found!! (maybe it is an alias?)
                        if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
                        {
                            retAnswer = true;
                        }
                        else
                        {
                            rsLoad.OpenRecordset($"select count(id) as theCount from PendingActivityMaster where partyid1 = {FCConvert.ToString(lngID)} or partyid2 = {FCConvert.ToString(lngID)} or partyid3 = {FCConvert.ToString(lngID)}", "MotorVehicle");
                            // TODO: Field [theCount] not found!! (maybe it is an alias?)
                            if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
                            {
                                retAnswer = true;
                            }
                            else
                            {
                                rsLoad.OpenRecordset($"select count(id) as theCount from HeldRegistrationMaster where partyid1 = {FCConvert.ToString(lngID)} or partyid2 = {FCConvert.ToString(lngID)} or partyid3 = {FCConvert.ToString(lngID)}", "MotorVehicle");
                                // TODO: Field [theCount] not found!! (maybe it is an alias?)
                                if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
                                {
                                    retAnswer = true;
                                }
                                else
                                {
                                    rsLoad.OpenRecordset($"select count(id) as theCount from ActivityMaster where partyid1 = {FCConvert.ToString(lngID)} or partyid2 = {FCConvert.ToString(lngID)} or partyid3 = {FCConvert.ToString(lngID)}", "MotorVehicle");
                                    // TODO: Field [theCount] not found!! (maybe it is an alias?)
                                    if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
                                    {
                                        retAnswer = true;
                                    }
                                    else
                                    {
                                        rsLoad.OpenRecordset($"select count(id) as theCount from ArchiveMaster where partyid1 = {FCConvert.ToString(lngID)} or partyid2 = {FCConvert.ToString(lngID)} or partyid3 = {FCConvert.ToString(lngID)}", "MotorVehicle");
                                        // TODO: Field [theCount] not found!! (maybe it is an alias?)
                                        if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
                                        {
                                            retAnswer = true;
                                        }
                                        else
                                        {
                                            rsLoad.OpenRecordset($"select count(id) as theCount from tblMasterTemp where partyid1 = {FCConvert.ToString(lngID)} or partyid2 = {FCConvert.ToString(lngID)} or partyid3 = {FCConvert.ToString(lngID)}", "MotorVehicle");
                                            // TODO: Field [theCount] not found!! (maybe it is an alias?)
                                            if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
                                            {
                                                retAnswer = true;
                                            }
                                            else
                                            {
                                                rsLoad.OpenRecordset($"select count(id) as theCount from LongTermTrailerRegistrations where partyid1 = {FCConvert.ToString(lngID)} or partyid2 = {FCConvert.ToString(lngID)}", "MotorVehicle");
                                                // TODO: Field [theCount] not found!! (maybe it is an alias?)
                                                if (FCConvert.ToInt32(rsLoad.Get_Fields("theCount")) > 0)
                                                {
                                                    retAnswer = true;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    ICentralPartyApp_ReferencesParty = retAnswer;
                }
            }
            finally
            {
                rsLoad.DisposeOf();
            }
            return ICentralPartyApp_ReferencesParty;
        }

        public List<object> AccountsReferencingParty(int lngID)
        {
            List<object> CentralPartyApp_AccountsReferencingParty = null;
            var theCollection = new List<object>();
            clsDRWrapper rsLoad = new clsDRWrapper();
            cCentralPartyReference pRef;

            try
            {
                rsLoad.OpenRecordset($"select  * from master where partyid1 = {FCConvert.ToString(lngID)} or partyid2 = {FCConvert.ToString(lngID)} or partyid3 = {FCConvert.ToString(lngID)}", "MotorVehicle");
                while (!rsLoad.EndOfFile())
                {
                    //Application.DoEvents();
                    pRef = new cCentralPartyReference();
                    pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
                    pRef.ModuleCode = strModuleCode;
                    // TODO: Field [Plate] not found!! (maybe it is an alias?)
                    pRef.Identifier = FCConvert.ToString(rsLoad.Get_Fields("Plate"));
                    pRef.IdentifierForSort = pRef.Identifier;
                    // TODO: Field [VIN] not found!! (maybe it is an alias?)
                    pRef.AlternateIdentifier = FCConvert.ToString(rsLoad.Get_Fields("VIN"));
                    pRef.Description = "Motor Vehicle Registration";
                    theCollection.Add(pRef);
                    rsLoad.MoveNext();
                }
                rsLoad.OpenRecordset("select * from fleetmaster where partyid1 = " + FCConvert.ToString(lngID) + " or partyid2 = " + FCConvert.ToString(lngID) + " or partyid3 = " + FCConvert.ToString(lngID) + " order by fleetnumber", "MotorVehicle");
                while (!rsLoad.EndOfFile())
                {
                    //Application.DoEvents();
                    pRef = new cCentralPartyReference();
                    pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id"));
                    // TODO: Field [FleetNumber] not found!! (maybe it is an alias?)
                    pRef.Identifier = FCConvert.ToString(rsLoad.Get_Fields("FleetNumber"));
                    pRef.IdentifierForSort = Strings.Right(Strings.StrDup(12, "0") + pRef.Identifier, 12);
                    pRef.ModuleCode = strModuleCode;
                    pRef.Description = "Motor Vehicle Fleet";
                    theCollection.Add(pRef);
                    rsLoad.MoveNext();
                }
                rsLoad.OpenRecordset("select * from LongTermTrailerRegistrations where partyid1 = " + FCConvert.ToString(lngID) + " or partyid2 = " + FCConvert.ToString(lngID) + " order by plate", "MotorVehicle");
                while (!rsLoad.EndOfFile())
                {
                    //Application.DoEvents();
                    pRef = new cCentralPartyReference();
                    pRef.ModuleCode = strModuleCode;
                    pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
                    // TODO: Field [Plate] not found!! (maybe it is an alias?)
                    pRef.Identifier = FCConvert.ToString(rsLoad.Get_Fields("Plate"));
                    pRef.IdentifierForSort = pRef.Identifier;
                    // TODO: Field [VIN] not found!! (maybe it is an alias?)
                    pRef.AlternateIdentifier = FCConvert.ToString(rsLoad.Get_Fields("VIN"));
                    pRef.Description = "Long Term Trailer Registration";
                    theCollection.Add(pRef);
                    rsLoad.MoveNext();
                }
                rsLoad.OpenRecordset("select * from pendingactivitymaster where partyid1 = " + FCConvert.ToString(lngID) + " or partyid2 = " + FCConvert.ToString(lngID) + " or partyid3 = " + FCConvert.ToString(lngID), "MotorVehicle");
                while (!rsLoad.EndOfFile())
                {
                    //Application.DoEvents();
                    pRef = new cCentralPartyReference();
                    pRef.ModuleCode = strModuleCode;
                    pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
                    // TODO: Field [Plate] not found!! (maybe it is an alias?)
                    pRef.Identifier = FCConvert.ToString(rsLoad.Get_Fields("Plate"));
                    pRef.IdentifierForSort = pRef.Identifier;
                    // TODO: Field [VIN] not found!! (maybe it is an alias?)
                    pRef.AlternateIdentifier = FCConvert.ToString(rsLoad.Get_Fields("VIN"));
                    pRef.Description = "Motor Vehicle Pending Activity";
                    theCollection.Add(pRef);
                    rsLoad.MoveNext();
                }
                rsLoad.OpenRecordset("select * from activitymaster where partyid1 = " + FCConvert.ToString(lngID) + " or partyid2 = " + FCConvert.ToString(lngID) + " or partyid3 = " + FCConvert.ToString(lngID), "MotorVehicle");
                while (!rsLoad.EndOfFile())
                {
                    //Application.DoEvents();
                    pRef = new cCentralPartyReference();
                    pRef.ModuleCode = strModuleCode;
                    pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id"));
                    // TODO: Field [plate] not found!! (maybe it is an alias?)
                    pRef.Identifier = FCConvert.ToString(rsLoad.Get_Fields("plate"));
                    pRef.IdentifierForSort = pRef.Identifier;
                    // TODO: Field [VIN] not found!! (maybe it is an alias?)
                    pRef.AlternateIdentifier = FCConvert.ToString(rsLoad.Get_Fields("VIN"));
                    pRef.Description = "Motor Vehicle Activity";
                    theCollection.Add(pRef);
                    rsLoad.MoveNext();
                }
                rsLoad.OpenRecordset("select * from heldregistrationmaster where partyid1 = " + FCConvert.ToString(lngID) + " or partyid2 = " + FCConvert.ToString(lngID) + " or partyid3 = " + FCConvert.ToString(lngID), "MotorVehicle");
                while (!rsLoad.EndOfFile())
                {
                    //Application.DoEvents();
                    pRef = new cCentralPartyReference();
                    pRef.ModuleCode = strModuleCode;
                    pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id"));
                    // TODO: Field [plate] not found!! (maybe it is an alias?)
                    pRef.Identifier = FCConvert.ToString(rsLoad.Get_Fields("plate"));
                    pRef.IdentifierForSort = pRef.Identifier;
                    // TODO: Field [VIN] not found!! (maybe it is an alias?)
                    pRef.AlternateIdentifier = FCConvert.ToString(rsLoad.Get_Fields("VIN"));
                    pRef.Description = "Motor Vehicle Held Registration";
                    rsLoad.MoveNext();
                }
                rsLoad.OpenRecordset("select * from ArchiveMaster where partyid1 = " + FCConvert.ToString(lngID) + " or partyid2 = " + FCConvert.ToString(lngID) + " or partyid3 = " + FCConvert.ToString(lngID), "MotorVehicle");
                while (!rsLoad.EndOfFile())
                {
                    //Application.DoEvents();
                    pRef = new cCentralPartyReference();
                    pRef.ModuleCode = strModuleCode;
                    pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id"));
                    // TODO: Field [plate] not found!! (maybe it is an alias?)
                    pRef.Identifier = FCConvert.ToString(rsLoad.Get_Fields("plate"));
                    pRef.IdentifierForSort = pRef.Identifier;
                    // TODO: Field [VIN] not found!! (maybe it is an alias?)
                    pRef.AlternateIdentifier = FCConvert.ToString(rsLoad.Get_Fields("VIN"));
                    pRef.Description = "Motor Vehicle Archive Table";
                    rsLoad.MoveNext();
                }
                rsLoad.OpenRecordset("select * from tblMasterTemp where partyid1 = " + FCConvert.ToString(lngID) + " or partyid2 = " + FCConvert.ToString(lngID) + " or partyid3 = " + FCConvert.ToString(lngID), "MotorVehicle");
                while (!rsLoad.EndOfFile())
                {
                    //Application.DoEvents();
                    pRef = new cCentralPartyReference();
                    pRef.ModuleCode = strModuleCode;
                    pRef.ID = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("id"));
                    // TODO: Field [PLATE] not found!! (maybe it is an alias?)
                    pRef.Identifier = FCConvert.ToString(rsLoad.Get_Fields("PLATE"));
                    pRef.IdentifierForSort = pRef.Identifier;
                    // TODO: Field [VIN] not found!! (maybe it is an alias?)
                    pRef.AlternateIdentifier = FCConvert.ToString(rsLoad.Get_Fields("VIN"));
                    pRef.Description = "Motor Vehicle Temp";
                    rsLoad.MoveNext();
                }
                CentralPartyApp_AccountsReferencingParty = theCollection;
            }
            finally
            {
                 rsLoad.DisposeOf();
            }
            return CentralPartyApp_AccountsReferencingParty;
        }

        public string ModuleCode()
        {
            string ICentralPartyApp_ModuleCode = "";
            ICentralPartyApp_ModuleCode = strModuleCode;
            return ICentralPartyApp_ModuleCode;
        }

        public bool ChangePartyID(int lngOriginalPartyID, int lngNewPartyID)
        {
            bool ICentralPartyApp_ChangePartyID = false;
            clsDRWrapper rsSave = new clsDRWrapper();
            string strSQL = "";

            try
            {
                // On Error GoTo ErrorHandler
                string strTable;
                int intParty;
                strTable = "FleetMaster";

                for (intParty = 1; intParty <= 3; intParty++)
                {
                    strSQL = "Update " + strTable + " set partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngNewPartyID) + " where partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngOriginalPartyID);
                    rsSave.Execute(strSQL, "MotorVehicle");
                }

                strTable = "Master";

                for (intParty = 1; intParty <= 3; intParty++)
                {
                    strSQL = "Update " + strTable + " set partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngNewPartyID) + " where partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngOriginalPartyID);
                    rsSave.Execute(strSQL, "MotorVehicle");
                }

                strTable = "PendingActivityMaster";

                for (intParty = 1; intParty <= 3; intParty++)
                {
                    strSQL = "Update " + strTable + " set partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngNewPartyID) + " where partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngOriginalPartyID);
                    rsSave.Execute(strSQL, "MotorVehicle");
                }

                strTable = "HeldRegistrationMaster";

                for (intParty = 1; intParty <= 3; intParty++)
                {
                    strSQL = "Update " + strTable + " set partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngNewPartyID) + " where partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngOriginalPartyID);
                    rsSave.Execute(strSQL, "MotorVehicle");
                }

                strTable = "LongTermTrailerRegistrations";

                for (intParty = 1; intParty <= 2; intParty++)
                {
                    strSQL = "Update " + strTable + " set partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngNewPartyID) + " where partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngOriginalPartyID);
                    rsSave.Execute(strSQL, "MotorVehicle");
                }

                strTable = "ActivityMaster";

                for (intParty = 1; intParty <= 3; intParty++)
                {
                    strSQL = "Update " + strTable + " set partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngNewPartyID) + " where partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngOriginalPartyID);
                    rsSave.Execute(strSQL, "MotorVehicle");
                }

                strTable = "tblMasterTemp";

                for (intParty = 1; intParty <= 3; intParty++)
                {
                    strSQL = "Update " + strTable + " set partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngNewPartyID) + " where partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngOriginalPartyID);
                    rsSave.Execute(strSQL, "MotorVehicle");
                }

                strTable = "ArchiveMaster";

                for (intParty = 1; intParty <= 3; intParty++)
                {
                    strSQL = "Update " + strTable + " set partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngNewPartyID) + " where partyid" + FCConvert.ToString(intParty) + " = " + FCConvert.ToString(lngOriginalPartyID);
                    rsSave.Execute(strSQL, "MotorVehicle");
                }

                ICentralPartyApp_ChangePartyID = true;

                return ICentralPartyApp_ChangePartyID;
            }
            catch (Exception ex)
            {
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            finally
            {
                rsSave.DisposeOf();
            }
            return ICentralPartyApp_ChangePartyID;
        }
    }
}
