//Fecher vbPorter - Version 1.0.0.27
using System.Text;
using System.Runtime.InteropServices;
using fecherFoundation;

namespace Global
{
	public class cColorDialog
	{

		//=========================================================




		[DllImport("comdlg32.dll", EntryPoint = "ChooseColorA")]
		private static extern int GetCHOOSECOLOR(ref CHOOSECOLOR pChoosecolor);



		[StructLayout(LayoutKind.Sequential)]
		private struct CHOOSECOLOR
		{
			public int lStructSize;
			public int hwndOwner;
			public int hInstance;
			public int rgbResult;
			public string lpCustColors;
			public int flags;
			public int lCustData;
			public int lpfnHook;
			public string lpTemplateName;
		};

		private int lngColor;
		private bool boolFullOpen;
		private bool boolShowHelp;
		private bool boolAllowFullOpen;
		private bool boolCCRGBInit;
		byte[] CustomColors = null;

		public bool ShowHelp
		{
			set
			{
				boolShowHelp = value;
			}

			get
			{
					bool ShowHelp = false;
				ShowHelp = boolShowHelp;
				return ShowHelp;
			}
		}



		public bool AllowFullOpen
		{
			set
			{
				boolAllowFullOpen = value;
			}

			get
			{
					bool AllowFullOpen = false;
				AllowFullOpen = boolAllowFullOpen;
				return AllowFullOpen;
			}
		}



		public bool RGBInit
		{
			set
			{
				boolCCRGBInit = value;
			}

			get
			{
					bool RGBInit = false;
				RGBInit = boolCCRGBInit;
				return RGBInit;
			}
		}



		public bool FullOpen
		{
			set
			{
				boolFullOpen = value;
			}

			get
			{
					bool FullOpen = false;
				FullOpen = boolFullOpen;
				return FullOpen;
			}
		}



		public int Color
		{
			set
			{
				lngColor = value;
				boolCCRGBInit = true;
			}

			get
			{
					int Color = 0;
				Color = lngColor;
				return Color;
			}
		}



		public int RunDialog()
		{
			int RunDialog = 0;
			CHOOSECOLOR cc = new CHOOSECOLOR();
			int lngReturn;

			cc.lStructSize = Marshal.SizeOf(cc);
			cc.hInstance = Support.GetHInstance();
			cc.lpCustColors = Encoding.Default.GetString(CustomColors);
			cc.flags = 0;
			if (FullOpen) {
                //FC:TODO:PJ 
                //cc.flags += vbPorterConverter.cdlCCFullOpen;
			}
			if (RGBInit) {
                //FC:TODO:PJ 
                //cc.flags += vbPorterConverter.cdlCCRGBInit;
			}
			if (!AllowFullOpen) {
                //FC:TODO:PJ 
                //cc.flags += vbPorterConverter.cdlCCPreventFullOpen;
			}
			if (ShowHelp) {
                //FC:TODO:PJ 
                //cc.flags += cdlCCHelpButton;
			}
			lngReturn = GetCHOOSECOLOR(ref cc);
			if (lngReturn!=0) {
				Color = cc.rgbResult;
				CustomColors = Encoding.Default.GetBytes(cc.lpCustColors);
			}
			RunDialog = lngReturn;
			return RunDialog;
		}

	}
}