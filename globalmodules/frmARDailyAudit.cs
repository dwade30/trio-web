﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using Wisej.Web;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWAR0000;

namespace Global
{
	/// <summary>
	/// Summary description for frmARDailyAudit.
	/// </summary>
	public partial class frmARDailyAudit : BaseForm
	{
		public frmARDailyAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbAlignment.SelectedIndex = 0;
			cmbAudit.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmARDailyAudit InstancePtr
		{
			get
			{
				return (frmARDailyAudit)Sys.GetInstance(typeof(frmARDailyAudit));
			}
		}

		protected frmARDailyAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/10/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/02/2005              *
		// ********************************************************
		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsAutoCreate = new clsDRWrapper();
				// show the report modally so that the closeout will not happen until after the report is finished
				if (cmbAudit.SelectedIndex == 0)
				{
					// Audit Preview
					// this will look for payments that are not closed out yet
					modGlobal.Statics.glngARCurrentCloseOut = 0;
					rptARDailyAuditMaster.InstancePtr.StartUp(false, frmARDailyAudit.InstancePtr.cmbAlignment.SelectedIndex == 1);
					frmReportViewer.InstancePtr.Init(rptARDailyAuditMaster.InstancePtr);
				}
				else if (cmbAudit.SelectedIndex == 1)
				{
					// Daily Audit
					rsAutoCreate.OpenRecordset("SELECT * FROM Customize");
					if (rsAutoCreate.EndOfFile() != true && rsAutoCreate.BeginningOfFile() != true)
					{
						if (FCConvert.ToBoolean(rsAutoCreate.Get_Fields_Boolean("AutoCreateJournal")))
						{
							modARCalculations.CreateDemandJournal();
						}
					}
					// closeout the payments (only in AR)
					modGlobal.Statics.glngARCurrentCloseOut = modGlobal.ARCloseOut();
					rptARDailyAuditMaster.InstancePtr.StartUp(true, frmARDailyAudit.InstancePtr.cmbAlignment.SelectedIndex == 1);
					frmReportViewer.InstancePtr.Init(rptARDailyAuditMaster.InstancePtr);
				}
				else
				{
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Running Audit", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void cmdDone_Click()
		{
			cmdDone_Click(cmdDone, new System.EventArgs());
		}

		private void frmARDailyAudit_Activated(object sender, System.EventArgs e)
		{
			if (modGlobalConstants.Statics.gboolCR)
			{
				// this will turn off the audit report if the user has CR and will only allow
				//optAudit[1].Enabled = false; // the user to preview the AR report not actually close out the payments
				if (cmbAudit.Items.Count > 1)
				{
					cmbAudit.Items.RemoveAt(1);
				}
				ToolTip1.SetToolTip(cmbAudit, "The audit will be run when the TRIO Cash Receipting daily audit is run.");
				//ToolTip1.SetToolTip(optAudit[1], "The audit will be run when the TRIO Cash Receipting daily audit is run.");
			}
			else
			{
				//optAudit[1].Enabled = true;
				ToolTip1.SetToolTip(cmbAudit, "");
				//ToolTip1.SetToolTip(optAudit[1], "");
			}
			ShowFrame(ref fraPreAudit);
		}

		private void frmARDailyAudit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmARDailyAudit.FillStyle	= 0;
			//frmARDailyAudit.ScaleWidth	= 9045;
			//frmARDailyAudit.ScaleHeight	= 6930;
			//frmARDailyAudit.LinkTopic	= "Form2";
			//frmARDailyAudit.LockControls	= -1  'True;
			//frmARDailyAudit.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsPD = new clsDRWrapper();
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmARDailyAudit_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// Catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void ShowFrame(ref FCFrame fraFrame)
		{
			// this will place the frame in the middle of the form
			//fraFrame.Top = FCConvert.ToInt32((this.Height - fraFrame.Height) / 3.0);
			//fraFrame.Left = FCConvert.ToInt32((this.Width - fraFrame.Width) / 2.0);
			fraFrame.Visible = true;
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			cmdDone_Click();
		}
	}
}
