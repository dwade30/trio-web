﻿using fecherFoundation;
using Wisej.Web;
using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Global
{
	/// <summary>
	/// Summary description for SectionReport1.
	/// </summary>
	public partial class sarTaxAcquiredDetail : FCSectionReport
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               06/02/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/26/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLN = new clsDRWrapper();
		int lngAccount;
		double dblTotal;
		int intTotalYears;

		public sarTaxAcquiredDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "TA Detail";
		}

		public static sarTaxAcquiredDetail InstancePtr
		{
			get
			{
				return (sarTaxAcquiredDetail)Sys.GetInstance(typeof(sarTaxAcquiredDetail));
			}
		}

		protected sarTaxAcquiredDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsLN?.Dispose();
				rsData?.Dispose();
            }
			base.Dispose(disposing);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			//Detail_Format();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngAccount = FCConvert.ToInt32(this.UserData);
			rsData.OpenRecordset("SELECT LienRec.InterestPaid AS IntPd, LienRec.InterestCharged AS IntCHG, LienRec.RateKey AS RK, LienRec.InterestAppliedThroughDate AS ApplThrDT, LienRec.PrincipalPaid AS PP, * FROM BillingMaster LEFT JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.ID WHERE Account = " + FCConvert.ToString(lngAccount) + " AND BillingType = 'RE' ORDER BY BillingYear desc", modExtraModules.strCLDatabase);
			if (rsData.EndOfFile())
			{
				HideFields();
				lblLastBillingAmountTotal.Text = "0.00";
			}
			else
			{
				lblLastBillingAmountTotal.Text = Strings.Format(Conversion.Val(rsData.Get_Fields_Decimal("TaxDue1")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue2")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue3")) + Conversion.Val(rsData.Get_Fields_Decimal("TaxDue4")), "#,##0.00");
				rsLN.OpenRecordset("SELECT * FROM LienRec", modExtraModules.strCLDatabase);
				if (FindOutstaingTaxes())
				{
					rsData.MoveFirst();
				}
				else
				{
					HideFields();
				}
			}
		}

		private void HideFields()
		{
			fldPrevOwner.Visible = false;
			fldTaxes.Visible = false;
			lblOutStandingTax.Visible = false;
			lblPreviousOwners.Visible = false;
			lblTaxes.Visible = false;
			Line1.Visible = false;
			fldTotalTaxes.Visible = false;
			Detail.Height = 0;
			GroupHeader1.Height = 300 / 1440F;
			GroupFooter1.Height = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
			if (!rsData.EndOfFile())
			{
				// rsData.MoveNext
			}
		}

		private void BindFields()
		{
			string strOwner = "";
			double dblBill = 0;
			double dblXtraInt = 0;
			TRYAGAIN:
			;
			if (!rsData.EndOfFile())
			{
				if (FCConvert.ToInt32(rsData.Get_Fields_Int32("LienRecordNumber")) == 0)
				{
					// calculate the bill total
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					dblBill = modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), DateTime.Today, ref dblXtraInt);
				}
				else
				{
					rsLN.FindFirstRecord("ID", rsData.Get_Fields_Int32("LienRecordNumber"));
					if (rsLN.NoMatch)
					{
						dblBill = 0;
					}
					else
					{
						dblBill = modCLCalculations.CalculateAccountCLLien(rsLN, DateTime.Today, ref dblXtraInt);
					}
				}
				// no outstanding amounts, then find another one
				if (dblBill == 0)
				{
					rsData.MoveNext();
					goto TRYAGAIN;
				}
				dblTotal += dblBill;
				fldTaxes.Text = Strings.Format(dblBill, "#,##0.00");
				fldPrevOwner.Text = rsData.Get_Fields_String("Name1");
				lblTaxes.Text = modExtraModules.FormatYear(FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")));
				rsData.MoveNext();
			}
			else
			{
				fldTaxes.Text = "";
				fldPrevOwner.Text = "";
				lblTaxes.Text = "";
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalTaxes.Text = Strings.Format(dblTotal, "#,##0.00");
		}

		private bool FindOutstaingTaxes()
		{
			bool FindOutstaingTaxes = false;
			double dblXtraInt = 0;
			double dblBill = 0;
			while (!rsData.EndOfFile())
			{
				if (FCConvert.ToInt32(rsData.Get_Fields_Int32("LienRecordNumber")) == 0)
				{
					// calculate the bill total
					// TODO: Check the table for the column [Account] and replace with corresponding Get_Field method
					dblBill = modCLCalculations.CalculateAccountCL(ref rsData, FCConvert.ToInt32(rsData.Get_Fields("Account")), DateTime.Today, ref dblXtraInt);
				}
				else
				{
					rsLN.FindFirstRecord("ID", rsData.Get_Fields_Int32("LienRecordNumber"));
					if (rsLN.NoMatch)
					{
						dblBill = 0;
					}
					else
					{
						dblBill = modCLCalculations.CalculateAccountCLLien(rsLN, DateTime.Today, ref dblXtraInt);
					}
				}
				// no outstanding amounts, then find another one
				if (dblBill == 0)
				{
					rsData.MoveNext();
				}
				else
				{
					FindOutstaingTaxes = true;
					break;
				}
			}
			return FindOutstaingTaxes;
		}

		
	}
}
