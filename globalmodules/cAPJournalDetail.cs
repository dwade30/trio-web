﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace Global
{
	public class cAPJournalDetail
	{
		//=========================================================
		private int lngRecordID;
		private int lngAPJournalID;
		private string strDescription = string.Empty;
		private string strAccount = string.Empty;
		private double dblDetailAmount;
		private string strProject = string.Empty;
		private double dblDiscountAmount;
		private double dblEncumbranceAmount;
		private string str1099 = string.Empty;
		private string strRCB = string.Empty;
		private int lngEncumbranceDetailRecord;
		private double dblCorrectedAmount;
		private int lngPurchaseOrderID;
		private bool boolUpdated;
		private bool boolDeleted;

		public int APJournalID
		{
			set
			{
				lngAPJournalID = value;
				IsUpdated = true;
			}
			get
			{
				int APJournalID = 0;
				APJournalID = lngAPJournalID;
				return APJournalID;
			}
		}

		public string Description
		{
			set
			{
				strDescription = value;
				IsUpdated = true;
			}
			get
			{
				string Description = "";
				Description = strDescription;
				return Description;
			}
		}

		public string Account
		{
			set
			{
				strAccount = value;
				IsUpdated = true;
			}
			get
			{
				string Account = "";
				Account = strAccount;
				return Account;
			}
		}

		public double Amount
		{
			set
			{
				dblDetailAmount = value;
				IsUpdated = true;
			}
			get
			{
				double Amount = 0;
				Amount = dblDetailAmount;
				return Amount;
			}
		}

		public string Project
		{
			set
			{
				strProject = value;
				IsUpdated = true;
			}
			get
			{
				string Project = "";
				Project = strProject;
				return Project;
			}
		}

		public double Discount
		{
			set
			{
				dblDiscountAmount = value;
				IsUpdated = true;
			}
			get
			{
				double Discount = 0;
				Discount = dblDiscountAmount;
				return Discount;
			}
		}

		public double Encumbrance
		{
			set
			{
				dblEncumbranceAmount = value;
				IsUpdated = true;
			}
			get
			{
				double Encumbrance = 0;
				Encumbrance = dblEncumbranceAmount;
				return Encumbrance;
			}
		}

		public string Ten99
		{
			set
			{
				str1099 = value;
				IsUpdated = true;
			}
			get
			{
				string Ten99 = "";
				Ten99 = str1099;
				return Ten99;
			}
		}

		public string RCB
		{
			set
			{
				strRCB = value;
				IsUpdated = true;
			}
			get
			{
				string RCB = "";
				RCB = strRCB;
				return RCB;
			}
		}

		public int EncumbranceDetailRecord
		{
			set
			{
				lngEncumbranceDetailRecord = value;
				IsUpdated = true;
			}
			get
			{
				int EncumbranceDetailRecord = 0;
				EncumbranceDetailRecord = lngEncumbranceDetailRecord;
				return EncumbranceDetailRecord;
			}
		}

		public double CorrectedAmount
		{
			set
			{
				dblCorrectedAmount = value;
				IsUpdated = true;
			}
			get
			{
				double CorrectedAmount = 0;
				CorrectedAmount = dblCorrectedAmount;
				return CorrectedAmount;
			}
		}

		public int PurchaseOrderDetailsID
		{
			set
			{
				lngPurchaseOrderID = value;
				IsUpdated = true;
			}
			get
			{
				int PurchaseOrderDetailsID = 0;
				PurchaseOrderDetailsID = lngPurchaseOrderID;
				return PurchaseOrderDetailsID;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}

		public bool IsCredit()
		{
			bool IsCredit = false;
			bool boolCredit;
			boolCredit = false;
			if (Strings.LCase(RCB) != "c")
			{
				if (Amount < 0)
				{
					boolCredit = true;
				}
			}
			else
			{
				if (Amount > 0)
				{
					boolCredit = true;
				}
			}
			IsCredit = boolCredit;
			return IsCredit;
		}

		public bool IsCorrection()
		{
			bool IsCorrection = false;
			IsCorrection = Strings.LCase(RCB) == "c";
			return IsCorrection;
		}

		public bool IsBudgetAdjustment()
		{
			bool IsBudgetAdjustment = false;
			IsBudgetAdjustment = Strings.LCase(RCB) == "b";
			return IsBudgetAdjustment;
		}

		public double NetAmount()
		{
			double NetAmount = 0;
			NetAmount = Amount - Discount - Encumbrance;
			return NetAmount;
		}
	}
}
