﻿//Fecher vbPorter - Version 1.0.0.27
using fecherFoundation;

namespace Global
{
	public class cEncumbrance
	{
		//=========================================================
		private int lngRecordID;
		private int lngJournalNumber;
		private int lngVendorNumber;
		private string strTempVendorName = string.Empty;
		private string strTempVendorAddress1 = string.Empty;
		private string strTempVendorAddress2 = string.Empty;
		private string strTempVendorAddress3 = string.Empty;
		private string strTempVendorAddress4 = string.Empty;
		private int intJournalPeriod = 0;
		private string strEncumbranceDescription = string.Empty;
		private string strPO = string.Empty;
		private double dblEncumbranceAmount;
		private string strStatus = string.Empty;
		private double dblAdjustments;
		private double dblLiquidatedAmount;
		private string strPostedDate = "";
		private string strTempVendorCity = string.Empty;
		private string strTempVendorState = string.Empty;
		private string strTempVendorZip = string.Empty;
		private string strTempVendorZip4 = string.Empty;
		private bool boolPastYearEnc;
		private string strEncumbrancesDate = "";
		//FC:FINAL:DSE auto-initialize members declared with the "As New" declaration
		//private cGenericCollection collEncumbranceDetails = new cGenericCollection();
		private cGenericCollection collEncumbranceDetails_AutoInitialized;

		private cGenericCollection collEncumbranceDetails
		{
			get
			{
				if (collEncumbranceDetails_AutoInitialized == null)
				{
					collEncumbranceDetails_AutoInitialized = new cGenericCollection();
				}
				return collEncumbranceDetails_AutoInitialized;
			}
			set
			{
				collEncumbranceDetails_AutoInitialized = value;
			}
		}

		private cVendor vend;
		private bool boolUpdated;
		private bool boolDeleted;

		public int JournalNumber
		{
			set
			{
				lngJournalNumber = value;
				IsUpdated = true;
			}
			get
			{
				int JournalNumber = 0;
				JournalNumber = lngJournalNumber;
				return JournalNumber;
			}
		}

		public cVendor Vendor
		{
			set
			{
				vend = value;
				IsUpdated = true;
			}
			get
			{
				cVendor Vendor = null;
				Vendor = vend;
				return Vendor;
			}
		}

		public cGenericCollection EncumbranceDetails
		{
			get
			{
				cGenericCollection EncumbranceDetails = null;
				EncumbranceDetails = collEncumbranceDetails;
				return EncumbranceDetails;
			}
		}

		public string EncumbrancesDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strEncumbrancesDate = value;
				}
				else
				{
					strEncumbrancesDate = value;
				}
				IsUpdated = true;
			}
			get
			{
				string EncumbrancesDate = "";
				EncumbrancesDate = strEncumbrancesDate;
				return EncumbrancesDate;
			}
		}

		public int ID
		{
			set
			{
				lngRecordID = value;
			}
			get
			{
				int ID = 0;
				ID = lngRecordID;
				return ID;
			}
		}

		public int VendorNumber
		{
			set
			{
				lngVendorNumber = value;
				IsUpdated = true;
			}
			get
			{
				int VendorNumber = 0;
				VendorNumber = lngVendorNumber;
				return VendorNumber;
			}
		}

		public string TempVendorName
		{
			set
			{
				strTempVendorName = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorName = "";
				TempVendorName = strTempVendorName;
				return TempVendorName;
			}
		}

		public string TempVendorAddress1
		{
			set
			{
				strTempVendorAddress1 = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorAddress1 = "";
				TempVendorAddress1 = strTempVendorAddress1;
				return TempVendorAddress1;
			}
		}

		public string TempVendorAddress2
		{
			set
			{
				strTempVendorAddress2 = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorAddress2 = "";
				TempVendorAddress2 = strTempVendorAddress2;
				return TempVendorAddress2;
			}
		}

		public string TempVendorAddress3
		{
			set
			{
				strTempVendorAddress3 = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorAddress3 = "";
				TempVendorAddress3 = strTempVendorAddress3;
				return TempVendorAddress3;
			}
		}

		public string TempVendorAddress4
		{
			set
			{
				strTempVendorAddress4 = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorAddress4 = "";
				TempVendorAddress4 = strTempVendorAddress4;
				return TempVendorAddress4;
			}
		}

		public int Period
		{
			set
			{
				intJournalPeriod = value;
				IsUpdated = true;
			}
			// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
			get
			{
				return intJournalPeriod;
			}
		}

		public string Description
		{
			set
			{
				strEncumbranceDescription = value;
				IsUpdated = true;
			}
			get
			{
				string Description = "";
				Description = strEncumbranceDescription;
				return Description;
			}
		}

		public string PO
		{
			set
			{
				strPO = value;
				IsUpdated = true;
			}
			get
			{
				string PO = "";
				PO = strPO;
				return PO;
			}
		}

		public double EncumbranceAmount
		{
			set
			{
				dblEncumbranceAmount = value;
				IsUpdated = true;
			}
			get
			{
				double EncumbranceAmount = 0;
				EncumbranceAmount = dblEncumbranceAmount;
				return EncumbranceAmount;
			}
		}

		public string Status
		{
			set
			{
				strStatus = value;
				IsUpdated = true;
			}
			get
			{
				string Status = "";
				Status = strStatus;
				return Status;
			}
		}

		public double Adjustments
		{
			set
			{
				dblAdjustments = value;
				IsUpdated = true;
			}
			get
			{
				double Adjustments = 0;
				Adjustments = dblAdjustments;
				return Adjustments;
			}
		}

		public double LiquidatedAmount
		{
			set
			{
				dblLiquidatedAmount = value;
			}
			get
			{
				double LiquidatedAmount = 0;
				LiquidatedAmount = dblLiquidatedAmount;
				return LiquidatedAmount;
			}
		}

		public string PostedDate
		{
			set
			{
				if (Information.IsDate(value))
				{
					strPostedDate = value;
				}
				else
				{
					strPostedDate = "";
				}
				IsUpdated = true;
			}
			get
			{
				string PostedDate = "";
				PostedDate = strPostedDate;
				return PostedDate;
			}
		}

		public string TempVendorCity
		{
			set
			{
				strTempVendorCity = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorCity = "";
				TempVendorCity = strTempVendorCity;
				return TempVendorCity;
			}
		}

		public string TempVendorState
		{
			set
			{
				strTempVendorState = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorState = "";
				TempVendorState = strTempVendorState;
				return TempVendorState;
			}
		}

		public string TempVendorZip
		{
			set
			{
				strTempVendorZip = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorZip = "";
				TempVendorZip = strTempVendorZip;
				return TempVendorZip;
			}
		}

		public string TempVendorZip4
		{
			set
			{
				strTempVendorZip4 = value;
				IsUpdated = true;
			}
			get
			{
				string TempVendorZip4 = "";
				TempVendorZip4 = strTempVendorZip4;
				return TempVendorZip4;
			}
		}

		public bool IsPastYearEncumbrance
		{
			set
			{
				boolPastYearEnc = value;
				IsUpdated = true;
			}
			get
			{
				bool IsPastYearEncumbrance = false;
				IsPastYearEncumbrance = boolPastYearEnc;
				return IsPastYearEncumbrance;
			}
		}

		public bool IsUpdated
		{
			set
			{
				boolUpdated = value;
			}
			get
			{
				bool IsUpdated = false;
				IsUpdated = boolUpdated;
				return IsUpdated;
			}
		}

		public bool IsDeleted
		{
			set
			{
				boolDeleted = value;
				IsUpdated = true;
			}
			get
			{
				bool IsDeleted = false;
				IsDeleted = boolDeleted;
				return IsDeleted;
			}
		}
	}
}
