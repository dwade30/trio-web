﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;

namespace Global
{
	public class modPrintToFile
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :                                       *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :                                       *
		// LAST UPDATED   :               07/11/2002              *
		// ********************************************************
		public static void SetPrintProperties(GrapeCity.ActiveReports.SectionReport ReportName)
		{
			int intindex;
			//intindex = ReportName.Toolbar.Tools.Count;
			//ReportName.Toolbar.Tools.Add("");
			//ReportName.Toolbar.Tools(intindex).Caption = "";
			//ReportName.Toolbar.Tools(intindex).Type = 2;
			//ReportName.Toolbar.Tools(intindex).ID = "9990";
			//ReportName.Toolbar.Tools.Add("PRINT TO FILE");
			//ReportName.Toolbar.Tools(intindex+1).ID = "9950";
			//ReportName.Toolbar.Tools.Add("");
			//ReportName.Toolbar.Tools(intindex+2).Type = 2;
			//ReportName.Toolbar.Tools(intindex+2).ID = "9990";
		}
		//public static void VerifyPrintToFile(ref DDActiveReports2.ActiveReport ReportName, ref DDActiveReports2.DDTool Tool)
		//{
		//	const int curOnErrorGoToLabel_Default = 0;
		//	const int curOnErrorGoToLabel_ErrorHandler = 1;
		//	int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
		//	try
		//	{
		//		string strCurDir;
		//		strCurDir = Application.StartupPath;
		//		if (Tool.Caption=="PRINT TO FILE") {
		//			vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
		//			Information.Err(ex).Clear();
		//			ReportName.DLG1.FLAGS = 0x8;
		//			ReportName.DLG1.CancelError = true;
		//			 /*? On Error Resume Next  */
		//			ReportName.DLG1.Filter = "*.rtf";
		//			ReportName.DLG1.InitDir = Application.StartupPath;
		//			ReportName.DLG1.ShowSave();
		//			if (Information.Err(ex).Number==0) {
		//                      GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
		//                      string fileName = ReportName.DLG1.FileName;
		//				ReportName.Export(a, fileName);
		//				FCMessageBox.Show("Export to file "+ReportName.DLG1.FileName+" was completed successfully.", MsgBoxStyle.Information | MsgBoxStyle.OkOnly, "TRIO Software");
		//			}
		//		}
		//		return;
		//	ErrorHandler: ;
		//		FCMessageBox.Show(FCConvert.ToString(Information.Err(ex).Number)+" "+Information.Err(ex).Description, MsgBoxStyle.Critical | MsgBoxStyle.OkOnly, "TRIO Error");
		//	}
		//	catch (Exception ex)
		//	{
		//		switch(vOnErrorGoToLabel) {
		//			default:
		//			case curOnErrorGoToLabel_Default:
		//				// ...
		//				break;
		//			case curOnErrorGoToLabel_ErrorHandler:
		//				//? goto ErrorHandler;
		//				break;
		//		}
		//	}
		//}
	}
}
