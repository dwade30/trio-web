﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	public class modRegionalTown
	{
		public static bool IsRegionalTown()
		{
			bool IsRegionalTown = false;
			// Returns true if this is more than one town
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				IsRegionalTown = false;
				clsLoad.OpenRecordset("select usemultipletown from globalvariables", "SystemSettings", 0, false, 0, false, "", false);
				if (!clsLoad.EndOfFile())
				{
					if (FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("UseMultipleTown")))
					{
						IsRegionalTown = true;
					}
				}
				return IsRegionalTown;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + ex.GetBaseException().Message + "\r\n" + "In IsRegionalTown", MsgBoxStyle.Critical, "Error");
			}
			return IsRegionalTown;
		}

		public static string GetTownAbbreviation(ref int lngTownNumber)
		{
			string GetTownAbbreviation = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsAbbrev = new clsDRWrapper();
				rsAbbrev.OpenRecordset("SELECT * FROM tblRegions WHERE TownNumber = " + FCConvert.ToString(lngTownNumber), "CentralData");
				if (!rsAbbrev.EndOfFile())
				{
					GetTownAbbreviation = FCConvert.ToString(rsAbbrev.Get_Fields_String("TownAbbreviation"));
				}
				return GetTownAbbreviation;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Finding Abbreviation");
			}
			return GetTownAbbreviation;
		}

		public static string GetTownKeyName_2(int lngTownNumber)
		{
			return GetTownKeyName(ref lngTownNumber);
		}

		public static string GetTownKeyName(ref int lngTownNumber)
		{
			string GetTownKeyName = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsAbbrev = new clsDRWrapper();
				rsAbbrev.OpenRecordset("SELECT * FROM tblRegions WHERE TownNumber = " + FCConvert.ToString(lngTownNumber), "CentralData");
				if (!rsAbbrev.EndOfFile())
				{
					GetTownKeyName = FCConvert.ToString(rsAbbrev.Get_Fields_String("TownName"));
				}
				return GetTownKeyName;
			}
			catch (Exception ex)
			{
                StaticSettings.GlobalTelemetryService.TrackException(ex);
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Finding Town Name");
			}
			return GetTownKeyName;
		}
	}
}
