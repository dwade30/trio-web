﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

#if TWAR0000
using TWAR0000;


#elif TWBD0000
using TWBD0000;


#elif TWCR0000
using TWCR0000;


#elif TWFA0000
using TWFA0000;


#elif TWBL0000
using TWBL0000;
using modGlobal = TWBL0000.modMDIParent;


#elif TWGNENTY
using TWGNENTY;
using modGlobal = TWGNENTY.modGlobalRoutines;


#elif TWUT0000
using TWUT0000;
using modGlobal = TWUT0000.modMain;


#elif TWMV0000
using modGlobal = TWMV0000.modGlobalRoutines;


#elif TWPY0000
using modGlobal = TWPY0000.modGlobalRoutines;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmSelectBankNumber.
	/// </summary>
	public partial class frmSelectBankNumber : BaseForm
	{
		public frmSelectBankNumber()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSelectBankNumber InstancePtr
		{
			get
			{
				return (frmSelectBankNumber)Sys.GetInstance(typeof(frmSelectBankNumber));
			}
		}

		protected frmSelectBankNumber _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsDRWrapper rsInfo = new clsDRWrapper();
		int AccountCol;
		int KeyCol;
		int BankNumberCol;
		bool blnSaved;

		private void frmSelectBankNumber_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
			vsBankNumber.Select(1, BankNumberCol);
			vsBankNumber.Focus();
		}

		private void frmSelectBankNumber_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSelectBankNumber.FillStyle	= 0;
			//frmSelectBankNumber.ScaleWidth	= 3885;
			//frmSelectBankNumber.ScaleHeight	= 2625;
			//frmSelectBankNumber.LinkTopic	= "Form2";
			//frmSelectBankNumber.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			string strComboString;
			int counter;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			blnSaved = false;
			KeyCol = 0;
			AccountCol = 1;
			BankNumberCol = 2;
			vsBankNumber.ColHidden(KeyCol, true);
			vsBankNumber.ColWidth(AccountCol, FCConvert.ToInt32(vsBankNumber.WidthOriginal * 0.7));
			vsBankNumber.TextMatrix(0, AccountCol, "Account");
			vsBankNumber.TextMatrix(0, BankNumberCol, "Bank");
			vsBankNumber.ExtendLastCol = true;
			strComboString = "";
			rsInfo.OpenRecordset("SELECT * FROM Banks WHERE rTrim(Name) <> '' ORDER BY ID");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					strComboString += "'" + rsInfo.Get_Fields_Int32("ID") + ";" + rsInfo.Get_Fields_Int32("ID") + "\t" + rsInfo.Get_Fields_String("Name") + "|";
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				strComboString = Strings.Left(strComboString, strComboString.Length - 1);
			}
			vsBankNumber.ColComboList(BankNumberCol, strComboString);
			for (counter = 0; counter <= modBudgetaryAccounting.Statics.lngNumberOfAccounts - 1; counter++)
			{
				if (modBudgetaryAccounting.Statics.AcctsToPass[counter].intBank == 0)
				{
					vsBankNumber.Rows += 1;
					vsBankNumber.TextMatrix(vsBankNumber.Rows - 1, KeyCol, FCConvert.ToString(modBudgetaryAccounting.Statics.AcctsToPass[counter].lngKey));
					vsBankNumber.TextMatrix(vsBankNumber.Rows - 1, AccountCol, modBudgetaryAccounting.Statics.AcctsToPass[counter].strAccount);
				}
			}
		}

		private void frmSelectBankNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
            FormUtilities.KeyPressHandler(e, this);
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			int counter;
			vsBankNumber.Row -= 1;
			for (counter = 1; counter <= vsBankNumber.Rows - 1; counter++)
			{
				if (vsBankNumber.TextMatrix(counter, BankNumberCol) == "")
				{
					vsBankNumber.Select(counter, BankNumberCol);
					MessageBox.Show("You must fill in a bank number for each account shown before you may continue.", "Enter Bank Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
					return;
				}
			}
			if (!blnSaved)
			{
				mnuProcessSave_Click();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int counter;
			clsDRWrapper rsInfo = new clsDRWrapper();
			int lngRowCounter;
			vsBankNumber.Row = 0;
			for (counter = 1; counter <= vsBankNumber.Rows - 1; counter++)
			{
				if (vsBankNumber.TextMatrix(counter, BankNumberCol) == "")
				{
					vsBankNumber.Select(counter, BankNumberCol);
					MessageBox.Show("You must fill in a bank number for each account shown before you may continue.", "Enter Bank Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
			}
			lngRowCounter = 1;
			for (counter = 0; counter <= modBudgetaryAccounting.Statics.lngNumberOfAccounts - 1; counter++)
			{
				if (modBudgetaryAccounting.Statics.AcctsToPass[counter].intBank == 0)
				{
					modBudgetaryAccounting.Statics.AcctsToPass[counter].intBank = FCConvert.ToInt32(Math.Round(Conversion.Val(vsBankNumber.TextMatrix(lngRowCounter, BankNumberCol))));
					rsInfo.Execute("UPDATE JournalEntries SET BankNumber = " + FCConvert.ToString(Conversion.Val(vsBankNumber.TextMatrix(lngRowCounter, BankNumberCol))) + " WHERE ID = " + FCConvert.ToString(Conversion.Val(vsBankNumber.TextMatrix(lngRowCounter, KeyCol))), "Budgetary");
					rsInfo.Execute("INSERT INTO BankCashAccounts (BankNumber, Account, AllSuffix) VALUES (" + FCConvert.ToString(Conversion.Val(vsBankNumber.TextMatrix(lngRowCounter, BankNumberCol))) + ", '" + vsBankNumber.TextMatrix(lngRowCounter, AccountCol) + "', 0)", "Budgetary");
					lngRowCounter += 1;
				}
			}
			Close();
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(mnuProcessSave, new System.EventArgs());
		}

		private void vsBankNumber_ClickEvent(object sender, System.EventArgs e)
		{
			vsBankNumber.EditCell();
		}

		private void vsBankNumber_RowColChange(object sender, System.EventArgs e)
		{
			vsBankNumber.EditCell();
		}
	}
}
